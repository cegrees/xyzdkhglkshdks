﻿using System;
using Landdb.Client.Infrastructure;
using Landdb.Client.Resources;
using Xunit;

namespace UnitTests_Landdb.Client.Infrastructure {
    public class AnnotationsDisplayItemTests {
        [Fact]
        public void AnnotationsDisplayItem_Test() {
            var theRightItemWasCreated = false;
            
            var testDisplayItem = new AnnotationsDisplayItem("Drain Tiles");
            var testStyle = testDisplayItem.AnnotationStyle;
            var expectedStyle = AnnotationsDisplayItem.AnnotationStyleType.DrainTiles;
            if (testStyle.Equals(expectedStyle))
                theRightItemWasCreated = true;

            Assert.True(theRightItemWasCreated, $"AnnotationsDisplayItem's Constructor created ({testDisplayItem}) instead of the desired AnnotationsDisplayItem ({expectedStyle}).");
        }

        [Fact]
        public void GetAnnotationStyleDisplayTextFor_Test() {
            var theDisplayTextMatches = false;
            var testStyleType = AnnotationsDisplayItem.AnnotationStyleType.DrainTiles;
            var correctDisplayText = Strings.MapAnnotationCategory_DrainTiles;
            var testDisplayText = AnnotationsDisplayItem.GetAnnotationStyleDisplayTextFor(testStyleType);

            if (correctDisplayText.Equals(testDisplayText, StringComparison.Ordinal)) {
                theDisplayTextMatches = true;
            }

            Assert.True(theDisplayTextMatches, $"The display text ({testDisplayText}) for the tested AnnotationStyleType does not match the translation ({correctDisplayText}).");
        }

        [Fact]
        public void GetAnnotationStyleKeyTextFor_Test() {
            var theAnnotationStyleKeyTextMatches = false;
            var testStyleType = AnnotationsDisplayItem.AnnotationStyleType.DrainTiles;
            var correctKeyText = "Drain Tiles";
            var testKeyText = AnnotationsDisplayItem.GetAnnotationStyleKeyTextFor(testStyleType);

            if (correctKeyText.Equals(testKeyText, StringComparison.Ordinal)) {
                theAnnotationStyleKeyTextMatches = true;
            }

            Assert.True(theAnnotationStyleKeyTextMatches, $"The key text ({correctKeyText}) for the tested AnnotationStyleType does not match the expected key text ({testKeyText}).");
        }

        [Fact]
        public void Equals_Test() {
            var testDisplayItem = new AnnotationsDisplayItem("Drain Tiles");
            var correctDisplayItem = new AnnotationsDisplayItem("Drain Tiles");
            var incorrectDisplayItem = new AnnotationsDisplayItem("Deer Stand");
            //TODO: replace all of the "AnnotationsDisplayItem" and "AnnotationStyleType" with nameof or typeof or something so that a refactoring of the class name won't matter
            Assert.True(testDisplayItem.Equals(correctDisplayItem), $"The tested AnnotationsDisplayItem ({testDisplayItem}) isn't equal to the expected AnnotationsDisplayItem ({correctDisplayItem}).");
            Assert.False(testDisplayItem.Equals(incorrectDisplayItem), $"The tested AnnotationsDisplayItem ({testDisplayItem}) is equal to the incorrect AnnotationsDisplayItem ({incorrectDisplayItem}).");
        }

        [Fact]
        public void ToString_Test() {
            var testDisplayItem = new AnnotationsDisplayItem("Drain Tiles");
            bool stringsAreEqual = testDisplayItem.DisplayText == Strings.MapAnnotationCategory_DrainTiles;

            Assert.True(stringsAreEqual, $"The display text for ({testDisplayItem}) is not what was expected ({Strings.MapAnnotationCategory_DrainTiles}).");
        }

        [Fact]
        public void GetAnnotationStyleDisplayTextFor_Test_StringOverload() {
            var theDisplayTextMatches = false;
            var testStyleText = "Drain Tiles";
            var correctDisplayText = Strings.MapAnnotationCategory_DrainTiles;
            var testDisplayText = AnnotationsDisplayItem.GetAnnotationStyleDisplayTextFor(testStyleText);

            if (correctDisplayText.Equals(testDisplayText, StringComparison.Ordinal)) {
                theDisplayTextMatches = true;
            }

            Assert.True(theDisplayTextMatches, $"The display text ({testDisplayText}) for the tested AnnotationStyleType does not the translation ({correctDisplayText}).");
        }
    }
}