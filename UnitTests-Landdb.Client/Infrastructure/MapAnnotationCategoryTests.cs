﻿using Landdb.Client.Resources;
using System;
using Xunit;

namespace Landdb.Client.Infrastructure.Tests
{
    public class MapAnnotationCategoryTests
    {
        [Fact]
        public void DisplayText_Test()
        {
            var theDisplayTextMatches = false;
            var testCategory = new MapAnnotationCategory {Name = "Drain Tiles"};
            var testDisplayText = testCategory.DisplayText;
            var correctDisplayText = Strings.MapAnnotationCategory_DrainTiles;
            

            if (correctDisplayText.Equals(testDisplayText, StringComparison.Ordinal)) {
                theDisplayTextMatches = true;
            }

            Assert.True(theDisplayTextMatches, $"The display text ({testDisplayText}) for the tested MapAnnotationCategory does not match the translation ({correctDisplayText})."); ;
        }
    }
}