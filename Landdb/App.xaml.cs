﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using Landdb.Infrastructure;
using Ninject;
using Landdb.Client;
using NLog;
using Lokad.Cqrs;
using Lokad.Cqrs.AtomicStorage;
using Landdb.Wires;
using Landdb.Client.Infrastructure;
using NDesk.Options;
using System.Deployment.Application;
using System.Globalization;
using Microsoft.ApplicationInsights;
using Microsoft.ApplicationInsights.Extensibility;
using System.Management;
using System.Windows.Markup;
using Landdb.Client.Localization;

namespace Landdb {
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application {
        private static ICloudClientFactory cloudClientFactory = null;
        private static System.Threading.Mutex appMutex = new System.Threading.Mutex(true, "Land.db_BlueSky");
        private TelemetryClient telemetry = new TelemetryClient();

        public App() {
#if !DEBUG
            if (PreviousInstanceIsRunning()) { Application.Current.Shutdown(); }
#endif
            SetupTelemetry();

            try {
                if (Landdb.Properties.Settings.Default.UpgradeRequired) {
                    Landdb.Properties.Settings.Default.Upgrade();
                    Landdb.Properties.Settings.Default.UpgradeRequired = false;
                    Landdb.Properties.Settings.Default.Save();
                }
            } catch { } // Don't let this break the app. (It shouldn't, anyway.)
        }

        private void SetupTelemetry() {
            if (System.Diagnostics.Debugger.IsAttached) {
                TelemetryConfiguration.Active.DisableTelemetry = true;
            }

#if DEBUG
            telemetry.InstrumentationKey = "e76ded6a-66fd-494e-afc5-445fc0ab1895";
#else
            telemetry.InstrumentationKey = "e5df51bb-6657-4ddb-a5b3-4c95b0877ecb";
#endif

            telemetry.Context.Session.Id = Guid.NewGuid().ToString();
            telemetry.Context.Component.Version = string.Format("v {0}",
                 System.Reflection.Assembly.GetEntryAssembly().GetName().Version.ToString());

            try {
                var osName = (from x in new ManagementObjectSearcher("SELECT Caption FROM Win32_OperatingSystem").Get().OfType<ManagementObject>()
                              select x.GetPropertyValue("Caption")).FirstOrDefault() ?? "unknown";
                telemetry.Context.Device.OperatingSystem = osName.ToString();
            } catch { }

            try { // Credit to Raymond Chen (http://blogs.msdn.com/b/oldnewthing/archive/2008/12/18/9233149.aspx) for the OEM/Model lookup
                var computerInfo = (from x in new ManagementObjectSearcher("SELECT Name, Manufacturer, Model FROM Win32_ComputerSystem").Get().OfType<ManagementObject>()
                                    select new { Name = x.GetPropertyValue("Name"), Manufacturer = x.GetPropertyValue("Manufacturer"), Model = x.GetPropertyValue("Model") }).FirstOrDefault();
                string oem = computerInfo != null && !string.IsNullOrWhiteSpace(computerInfo.Manufacturer.ToString()) ? computerInfo.Manufacturer.ToString() : "Unknown";
                string model = computerInfo != null && !string.IsNullOrWhiteSpace(computerInfo.Model.ToString()) ? computerInfo.Model.ToString() : "Unknown";
                telemetry.Context.Device.Model = model;
                telemetry.Context.Device.OemName = oem;
            } catch { }
        }

        private void Application_Startup(object sender, StartupEventArgs e) {
            System.Threading.Tasks.Task.Run(() => {
                LogConfiguration.ConfigureLogging();
                LogConfiguration.LogStartupInformation();

                var log = LogManager.GetCurrentClassLogger();

                InitializeCultureSettings();

                int? esOperationMax = null;
                int? esOperationTimeout = null;

                var p = new OptionSet() {
                    { "console", v => {
                        if(!ConsoleManager.HasConsole) {
                            ConsoleManager.Show();
                            LogConfiguration.LogStartupInformation();
                        }
                    }},
                    { "uri=", v => {
                        ApplicationEnvironment.SetCustomUri(v);
                        RemoteConfigurationSettings.OverrideBaseUri(v);
                        log.Info("**REMOTE URI OVERRIDDEN TO: {0}", v);
                    }},
                    { "offline", v => {
                        NetworkStatus.SetOverride(false);
                        log.Info("**RUNNING IN OFFLINE MODE", v);
                    }},
                    { "AllowForVirtualNetworks", v => {
                        NetworkStatus.SetAllowForVirtualNetworks(true);
                        log.Info("**ALLOWING FOR VIRTUAL NETWORKS", v);
                    }},
                    { "localdata=", v => {
                        ApplicationEnvironment.OverrideDataDirectory(v);
                        log.Info("**LOCAL DATA LOCATION OVERRIDDEN TO: {0}", v);
                    }},
                    { "datasource=", v => {
                        string[] vals = v.Split( new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries);
                        if(vals.Length == 3) {
                            try {
                                Guid dsId = Guid.Parse(vals[0]);
                                string dsName = vals[1];
                                int startYear = int.Parse(vals[2]);
                                var years = Enumerable.Range(startYear, DateTime.Now.Year - startYear + 1);

                                Client.Account.DataSource fds = new Client.Account.DataSource() { AvailableYears = years, DataSourceId = dsId, Name = dsName};

                                ApplicationEnvironment.FakeLocalDataSource = fds;
                            } catch {
                                log.Info("**DATA SOURCE PARAMETER WAS FORMATTED INCORRECTLY", v);
                            }
                        } else {
                            log.Info("**DATA SOURCE PARAMETER WAS FORMATTED INCORRECTLY", v);
                        }
                    }},
                    { "EventStoreOperationsMax=", v => {
                        int parsedMax = 0;
                        if(int.TryParse(v, out parsedMax)) {
                            esOperationMax = parsedMax;
                        }
                    }},
                    { "EventStoreTimeout=", v => {
                        int parsedTimeout = 0;
                        if (int.TryParse(v, out parsedTimeout)) {
                            esOperationTimeout = parsedTimeout;
                        }
                    }},
                    { "ExperimentalStreaming", v => {
                        log.Info("**USING EXPERIMENTAL BINARY STREAMER");
                        EngineSetup.Streamer = Contracts.CreateExperimentalBinaryStreamer();
                        EngineSetup.TapesContainer += "-pb";
                        Conventions.DocsFolder += "-pb";
                    }},
                    { "culture=", v => {
                        InitializeCultureSettings(v);
                    }},
                    { "ShowFTM", v => {
                        ApplicationEnvironment.ShowSustainability = true;
                    }},
                    { "FTMUri=", v => {
                        ApplicationEnvironment.FieldToMarketRemoteUri = v;
                    }},
                    { "MLAPIUri=", v => {
                        ApplicationEnvironment.MasterlistRemoteUri = v;
                        RemoteConfigurationSettings.MasterlistRemoteUri = v;
                        log.Info("**MASTERLIST API: {0}", v);
                    }},
                    { "ConnectAPIUri=", v => {
                        RemoteConfigurationSettings.ConnectRemoteApiUri = v;
                        log.Info("**Connect API: {0}", v);
                    }},
                    { "ShowPlanetImagery", v => {
                        ApplicationEnvironment.ShowPlanetLabsImagery = true;
                    }},
                    { "PlanetOneSection", v => {
                        ApplicationEnvironment.PlanetLabsMaximumAOI = 640;
                    }},
                    { "PlanetFourSections", v => {
                        ApplicationEnvironment.PlanetLabsMaximumAOI = 2560;
                    }},
                    { "PlanetNineSections", v => {
                        ApplicationEnvironment.PlanetLabsMaximumAOI = 5760;
                    }},
                    { "PlanetTownship", v => {
                        ApplicationEnvironment.PlanetLabsMaximumAOI = 23040;
                    }},
                    { "SearchFieldHistory", v => {
                        ApplicationEnvironment.SearchFieldHistory = true;
                    }},
                    { "DefaultWindowPlacement", var => {
                        ApplicationEnvironment.SkipDefaultWindowPlacement = true;
                    }}
                };
                string[] args = null;

                if (ApplicationDeployment.IsNetworkDeployed) {
                    var inputArgs = AppDomain.CurrentDomain.SetupInformation.ActivationArguments.ActivationData;
                    if (inputArgs != null && inputArgs.Length > 0) {
                        args = inputArgs[0].Split(new char[] { '~' });
                    }
                    if (inputArgs == null) {
                        args = new string[] { };
                    }
                } else {
                    args = e.Args;
                }

                var extra = p.Parse(args);

                this.DispatcherUnhandledException += new System.Windows.Threading.DispatcherUnhandledExceptionEventHandler(Current_DispatcherUnhandledException);
                AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

                StartupTasks.Perform();

                var module = new LanddbModule() { EventStoreOperationMax = esOperationMax, EventStoreOperationTimeout = esOperationTimeout };
                ServiceLocator.Initialize(module);

                // TODO: Do the following in the background and hide with a splash screen?
                ServiceLocator.Get<IRouteConfigurator>().ConfigurizeAllTheRoutes();

                telemetry.Context.Device.Id = CloudClientFactory.DeviceId.ToString();

                var vmLocator = Application.Current.FindResource("Locator") as Landdb.ViewModel.ViewModelLocator;
                if (vmLocator != null) {
                    vmLocator.Main.OnStartupComplete();
                }


                FrameworkElement.LanguageProperty.OverrideMetadata(typeof(FrameworkElement), new FrameworkPropertyMetadata(XmlLanguage.GetLanguage(CultureInfo.CurrentUICulture.IetfLanguageTag)));
            });
        }

        private void InitializeCultureSettings(string forcedCulture = null) {
            var displayCulture = !string.IsNullOrWhiteSpace(forcedCulture) ? new CultureInfo(forcedCulture) : System.Threading.Thread.CurrentThread.CurrentUICulture;

            System.Threading.Thread.CurrentThread.CurrentUICulture = displayCulture;
            System.Threading.Thread.CurrentThread.CurrentCulture = displayCulture;
            CultureInfo.DefaultThreadCurrentCulture = displayCulture;
            CultureInfo.DefaultThreadCurrentUICulture = displayCulture;
            WPFLocalizeExtension.Engine.LocalizeDictionary.Instance.SetCultureCommand.Execute(displayCulture.Name);
            Landdb.Resources.Strings.Culture = displayCulture;
            
            if (string.IsNullOrWhiteSpace(Client.Properties.Settings.Default.MasterlistGroup)) {
                Client.Properties.Settings.Default.MasterlistGroup = CultureInfo.CurrentUICulture.Name;
                Client.Properties.Settings.Default.Save();
            }

            if (string.IsNullOrWhiteSpace(Client.Properties.Settings.Default.DatasourceCulture)) {
                Client.Properties.Settings.Default.DatasourceCulture = CultureInfo.CurrentUICulture.Name;
                Client.Properties.Settings.Default.Save();
            }
        }

        private static bool PreviousInstanceIsRunning() {
            return !(appMutex.WaitOne(0, false));
        }

        void Current_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e) {
            var log = LogManager.GetCurrentClassLogger();
            log.FatalException("Danger Will Robinson!", e.Exception);

            // The following appears to be duplication, as UI/dispatcher unhandled exceptions seem to be caught by the domain unhandled exception handler below. Keeping this around just in case...
            //
            //var realException = e.Exception is System.Reflection.TargetInvocationException ? e.Exception.InnerException : e.Exception;
            //Telemetry.TrackException(new Microsoft.ApplicationInsights.DataContracts.ExceptionTelemetry() { Exception = realException, HandledAt = Microsoft.ApplicationInsights.DataContracts.ExceptionHandledAt.Unhandled });
            //Telemetry.Flush();
        }

        void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e) {
            var log = LogManager.GetCurrentClassLogger();
            var exception = e.ExceptionObject as Exception;
            if (exception != null) {
                log.FatalException("Threaded Danger Will Robinson!", exception);

                var realException = exception is System.Reflection.TargetInvocationException ? exception.InnerException : exception;
                Telemetry.TrackException(new Microsoft.ApplicationInsights.DataContracts.ExceptionTelemetry() { Exception = realException, HandledAt = Microsoft.ApplicationInsights.DataContracts.ExceptionHandledAt.Unhandled });
                Telemetry.Flush();
            }
        }

        public static ICloudClientFactory CloudClientFactory {
            get {
                if (cloudClientFactory == null) {
                    cloudClientFactory = new CloudClientFactory();
                }

                return cloudClientFactory;
            }
        }

        public static App CurrentApp {
            get {
                return App.Current as App;
            }
        }

        public TelemetryClient Telemetry {
            get { return telemetry; }
        }

        private void Application_Exit(object sender, ExitEventArgs e) {
#if !DEBUG
            if (appMutex != null) {
                appMutex.ReleaseMutex();
            }
#endif
            Telemetry.Flush();
        }

        public static void RestartApplication() {
#if !DEBUG
            App.Current.Dispatcher.Invoke(() => { appMutex.ReleaseMutex(); appMutex.Dispose(); appMutex = null; });
#endif
            System.Windows.Forms.Application.Restart();
            System.Windows.Application.Current.Shutdown();
        }
    }
}
