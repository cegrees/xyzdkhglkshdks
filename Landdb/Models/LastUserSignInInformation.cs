﻿using Landdb.Web.ServiceContracts.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.Models {
    [DataContract]
    public class LastUserSignInInformation {
        [DataMember(Order=1)]
        public UserAccount UserAccount { get; set; }

        [DataMember(Order=2)]
        public DateTime LastLogin { get; set; }
    }
}
