{
    "LoadPropertySets" : [
        {
			"Id" : "C9931DD8-F654-44F7-9F6C-E702FD9BDF1C",
			"Name" : "Test Weight",
            "LoadPropertyIds" : [
                "804DB7AE-C605-4771-A118-1437AF7240EF",
                "22F650A3-491B-49F9-99AC-F591863624B8"
            ]
        },
		{
			"Id" : "86826F23-BCE6-4AA4-9014-E5713AB803ED",
			"Name" : "Cotton Lint/Seed Weights",
			"RecommendedCropIds" : [
				"128cb010-f6ea-49df-be3e-7e41c0e7da39",
				"e36cee22-f6c7-4808-bc3a-276f8282854f",
				"46a72002-9aa6-445c-af70-f7d4d0e1b803"
			],
			"LoadPropertyIds" : [
                "0CE65910-A1F2-4E09-906B-AC4F3C6F3C8A",
                "F70E9E2A-4D90-4A81-9F53-7780B4F61D18",
                "22F650A3-491B-49F9-99AC-F591863624B8"
			]
		},
        {
			"Id" : "853C150B-1B91-459F-96C2-205B90F35FAF",
			"Name" : "Bale Count - Square Bales",
			"RecommendedCropIds" : [
				"c0e7992c-9182-4d20-9ada-5bee392f229b",
				"6ef1467e-7fe0-4c34-a5e9-5baded575c44",
				"d5f2960e-745a-472d-a1ac-76e5e8cb18cb",
				"71d93955-1d19-45db-8e60-c60e162b5136",
				"a835b2e2-3a92-4635-a8a9-ec208b4c5c49",
				"d07a3d80-16f2-46c8-b804-bac8b1888b9e",
				"fe4b523f-47d1-4e00-aee6-e4819f9ce036",
				"f88f4e8d-8eb0-41db-b828-bc63d8d3ca48",
				"7570b736-39fe-4066-acd3-c3f9e0eadecf",
				"7c606e7a-c20c-4c2f-999f-9fac6fbe4fd5",
				"77072b39-4084-405b-b696-eb6c36360f6d",
				"e62fa7f7-2320-46a4-a3db-3db8a056cb71",
				"b6b3f5ab-3271-43df-bb62-8a36db0de611",
				"ee594616-68bf-4d65-b252-3bed93f2e202",
				"f4cf03f0-5335-4918-8041-9cd46f258898",
				"5b7a726c-162f-470b-8774-7f66c5bdcc86",
				"df9fced8-a952-446d-a949-638b1bdf61cd",
				"e0c2d768-5ad4-4947-a5ef-133b346f6d2e",
				"0aa2e116-7c08-42fb-a20d-bb749c4d21bf",
				"128cb010-f6ea-49df-be3e-7e41c0e7da39",
				"e36cee22-f6c7-4808-bc3a-276f8282854f",
				"46a72002-9aa6-445c-af70-f7d4d0e1b803"
			],
            "LoadPropertyIds" : [
                "2C268AB3-B0B3-4C5A-81B8-B6EA720D1474"
            ]
        },
		{
			"Id" : "71200E91-1717-479A-BF20-F8E2C4901969",
			"Name" : "Foreign Material",
			"Description" : "Capture the foreign material percentage of a load",
            "LoadPropertyIds" : [
                "DE4117A1-FEAE-422E-BC3D-503A30347388"
            ]
        },
		{
			"Id" : "CF0C81F5-C266-4AD7-B31D-947A3F2331EF",
			"Name" : "Damage",
			"Description" : "Capture the damage percentage of a load",
            "LoadPropertyIds" : [
                "0BB50C08-C4C1-4F1D-8CB9-C2C49382DFCE"
            ]
        },
		{
			"Status" : "Discontinued",
			"Id" : "98485270-DC07-4D6E-8D21-C6818262A3AC",
			"Name" : "Test Property With Selections",
			"Description" : "Property to test dynamic combo boxes",
			"LoadPropertyIds" : [
                "9C7A7E79-143E-4B56-88A2-F5CED2E39411"
			]
		},
		{
			"Id" : "B8A44DF0-E93B-4240-A56E-FC7393960EFB",
			"Name" : "Bale Count - Large Square Bales",
			"RecommendedCropIds" : [
				"c0e7992c-9182-4d20-9ada-5bee392f229b",
				"6ef1467e-7fe0-4c34-a5e9-5baded575c44",
				"d5f2960e-745a-472d-a1ac-76e5e8cb18cb",
				"71d93955-1d19-45db-8e60-c60e162b5136",
				"a835b2e2-3a92-4635-a8a9-ec208b4c5c49",
				"d07a3d80-16f2-46c8-b804-bac8b1888b9e",
				"fe4b523f-47d1-4e00-aee6-e4819f9ce036",
				"f88f4e8d-8eb0-41db-b828-bc63d8d3ca48",
				"7570b736-39fe-4066-acd3-c3f9e0eadecf",
				"7c606e7a-c20c-4c2f-999f-9fac6fbe4fd5",
				"77072b39-4084-405b-b696-eb6c36360f6d",
				"e62fa7f7-2320-46a4-a3db-3db8a056cb71",
				"b6b3f5ab-3271-43df-bb62-8a36db0de611",
				"ee594616-68bf-4d65-b252-3bed93f2e202",
				"f4cf03f0-5335-4918-8041-9cd46f258898",
				"5b7a726c-162f-470b-8774-7f66c5bdcc86",
				"df9fced8-a952-446d-a949-638b1bdf61cd",
				"e0c2d768-5ad4-4947-a5ef-133b346f6d2e",
				"0aa2e116-7c08-42fb-a20d-bb749c4d21bf",
				"128cb010-f6ea-49df-be3e-7e41c0e7da39",
				"e36cee22-f6c7-4808-bc3a-276f8282854f",
				"46a72002-9aa6-445c-af70-f7d4d0e1b803"
			],
			"LoadPropertyIds" : [
				"0D25AF79-C6B2-4C81-8A58-6356F71C5BA6",
			]
		},
		{
			"Status" : "Discontinued",
			"Id" : "D7183162-7F5F-47FB-88B2-84DAD3B2EC49",
			"Name" : "Bale Count - Small Square Bales",
			"RecommendedCropIds" : [
				"c0e7992c-9182-4d20-9ada-5bee392f229b",
				"6ef1467e-7fe0-4c34-a5e9-5baded575c44",
				"d5f2960e-745a-472d-a1ac-76e5e8cb18cb",
				"71d93955-1d19-45db-8e60-c60e162b5136",
				"a835b2e2-3a92-4635-a8a9-ec208b4c5c49",
				"d07a3d80-16f2-46c8-b804-bac8b1888b9e",
				"fe4b523f-47d1-4e00-aee6-e4819f9ce036",
				"f88f4e8d-8eb0-41db-b828-bc63d8d3ca48",
				"7570b736-39fe-4066-acd3-c3f9e0eadecf",
				"7c606e7a-c20c-4c2f-999f-9fac6fbe4fd5",
				"77072b39-4084-405b-b696-eb6c36360f6d",
				"e62fa7f7-2320-46a4-a3db-3db8a056cb71",
				"b6b3f5ab-3271-43df-bb62-8a36db0de611",
				"ee594616-68bf-4d65-b252-3bed93f2e202",
				"f4cf03f0-5335-4918-8041-9cd46f258898",
				"5b7a726c-162f-470b-8774-7f66c5bdcc86",
				"df9fced8-a952-446d-a949-638b1bdf61cd",
				"e0c2d768-5ad4-4947-a5ef-133b346f6d2e",
				"0aa2e116-7c08-42fb-a20d-bb749c4d21bf",
				"128cb010-f6ea-49df-be3e-7e41c0e7da39",
				"e36cee22-f6c7-4808-bc3a-276f8282854f",
				"46a72002-9aa6-445c-af70-f7d4d0e1b803"
			],
			"LoadPropertyIds" : [
				"7D5B1FB3-5190-411D-9C23-A5427D0314EC",
			]
		},
		{
			"Id" : "90EEF973-58A0-44D8-BC83-8B4690B452FC",
			"Name" : "Bale Count - Round Bales",
			"RecommendedCropIds" : [
				"c0e7992c-9182-4d20-9ada-5bee392f229b",
				"6ef1467e-7fe0-4c34-a5e9-5baded575c44",
				"d5f2960e-745a-472d-a1ac-76e5e8cb18cb",
				"71d93955-1d19-45db-8e60-c60e162b5136",
				"a835b2e2-3a92-4635-a8a9-ec208b4c5c49",
				"d07a3d80-16f2-46c8-b804-bac8b1888b9e",
				"fe4b523f-47d1-4e00-aee6-e4819f9ce036",
				"f88f4e8d-8eb0-41db-b828-bc63d8d3ca48",
				"7570b736-39fe-4066-acd3-c3f9e0eadecf",
				"7c606e7a-c20c-4c2f-999f-9fac6fbe4fd5",
				"77072b39-4084-405b-b696-eb6c36360f6d",
				"e62fa7f7-2320-46a4-a3db-3db8a056cb71",
				"b6b3f5ab-3271-43df-bb62-8a36db0de611",
				"ee594616-68bf-4d65-b252-3bed93f2e202",
				"f4cf03f0-5335-4918-8041-9cd46f258898",
				"5b7a726c-162f-470b-8774-7f66c5bdcc86",
				"df9fced8-a952-446d-a949-638b1bdf61cd",
				"e0c2d768-5ad4-4947-a5ef-133b346f6d2e",
				"0aa2e116-7c08-42fb-a20d-bb749c4d21bf",
				"128cb010-f6ea-49df-be3e-7e41c0e7da39",
				"e36cee22-f6c7-4808-bc3a-276f8282854f",
				"46a72002-9aa6-445c-af70-f7d4d0e1b803"
			],
			"LoadPropertyIds" : [
				"32C20EA8-8B41-42AE-ABF0-CDF5E7C1B34E",
			]
		},
		{
			"Id" : "0D8F01EE-5CEE-4B1A-B442-13831C5D930C",
			"Name" : "Cotton Module Count",
			"RecommendedCropIds" : [
				"128cb010-f6ea-49df-be3e-7e41c0e7da39",
				"e36cee22-f6c7-4808-bc3a-276f8282854f",
				"46a72002-9aa6-445c-af70-f7d4d0e1b803"
			],
			"LoadPropertyIds" : [
				"7E73EE8A-A401-4F1C-AF31-48CC9670A98B"
			]
		},
		{
			"Id" : "82CB84C9-B851-4A49-A02D-51A26AB1605D",
			"Name" : "Cotton Mini Module Count",
			"RecommendedCropIds" : [
				"128cb010-f6ea-49df-be3e-7e41c0e7da39",
				"e36cee22-f6c7-4808-bc3a-276f8282854f",
				"46a72002-9aa6-445c-af70-f7d4d0e1b803"
			],
			"LoadPropertyIds" : [
				"537B880A-8130-4F38-966A-6003B0E27413"
			]
		},
		{
			"Id" : "B9075209-A856-497C-BF2A-E2CFDA835EB8",
			"Name" : "Bill of Lading",
			"LoadPropertyIds" : [
				"F093ACFD-562E-4480-9B9D-268BC28D2FA8"
			]
		},
		{
			"Id" : "E394CACF-2DF5-4065-88A6-A93E0BEEDD40",
			"Name" : "Box Count: Boxes",
			"LoadPropertyIds" : [
				"D48BBA6D-E24D-4ABA-B9BE-71BAEAA9301A"
			]
		}
    ],
    "LoadProperties" : [
        {
            "Id" : "804DB7AE-C605-4771-A118-1437AF7240EF",
            "Name" : "Test Weight",
			"UnitSelectionType" : "Weight",
            "InputType" : "TextBox",
            "DataType" : "Number"
        },
        {
            "Id" : "0CE65910-A1F2-4E09-906B-AC4F3C6F3C8A",
            "Name" : "Lint Weight",
			"UnitSelectionType" : "Weight",
            "InputType" : "TextBox",
            "DataType" : "Number"
        },
        {
            "Id" : "F70E9E2A-4D90-4A81-9F53-7780B4F61D18",
            "Name" : "Seed Weight",
			"UnitSelectionType" : "Weight",
            "InputType" : "TextBox",
            "DataType" : "Number"
        },
        {
            "Id" : "2C268AB3-B0B3-4C5A-81B8-B6EA720D1474",
            "Name" : "Square Bale Count",
            "InputType" : "TextBox",
            "DataType" : "Number"
        },
        {
            "Id" : "DE4117A1-FEAE-422E-BC3D-503A30347388",
            "Name" : "Foreign Material",
            "InputType" : "TextBox",
            "DataType" : "Percentage"
        },
        {
            "Id" : "0BB50C08-C4C1-4F1D-8CB9-C2C49382DFCE",
            "Name" : "Damage",
            "InputType" : "TextBox",
            "DataType" : "Percentage"
        },
        {
            "Id" : "9C7A7E79-143E-4B56-88A2-F5CED2E39411",
            "Name" : "Combobox Test",
            "InputType" : "ComboBox",
            "DataType" : "Text",
            "ListItemIds" : [
                "C0B2F291-F87C-442D-A47F-7CC080654509",
                "C2D7784F-1FC0-4E9F-870F-992DBF38AE19",
                "14261BB5-6115-4E60-9028-36F29812BFED"
            ]
        },
		{
			"Id" : "0D25AF79-C6B2-4C81-8A58-6356F71C5BA6",
			"Name" : "Large Square Bale Count",
			"InputType" : "TextBox",
			"DataType" : "Number"
		},
		{
			"Id" : "7D5B1FB3-5190-411D-9C23-A5427D0314EC",
			"Name" : "Small Square Bale Count",
			"InputType" : "TextBox",
			"DataType" : "Number"
		},
		{
			"Id" : "32C20EA8-8B41-42AE-ABF0-CDF5E7C1B34E",
			"Name" : "Round Bale Count",
			"InputType" : "TextBox",
			"DataType" : "Number"
		},
		{
			"Id" : "7E73EE8A-A401-4F1C-AF31-48CC9670A98B",
			"Name" : "Module Count",
			"InputType" : "TextBox",
			"DataType" : "Number"
		},
		{
			"Id" : "537B880A-8130-4F38-966A-6003B0E27413",
			"Name" : "Mini Module Count",
			"InputType" : "TextBox",
			"DataType" : "Number"
		},
		{
			"Id" : "F093ACFD-562E-4480-9B9D-268BC28D2FA8",
			"Name" : "Bill of Lading",
			"InputType" : "TextBox",
			"DataType" : "Text"
		},
		{
			"Id" : "D48BBA6D-E24D-4ABA-B9BE-71BAEAA9301A",
			"Name" : "Box Count: Boxes",
			"InputType" : "TextBox",
			"DataType" : "Number"
		}
    ],
    "LoadPropertyListItems" : [
        {
            "Id" : "C0B2F291-F87C-442D-A47F-7CC080654509",
            "Value" : "Alpha"
        },
        {
            "Id" : "C2D7784F-1FC0-4E9F-870F-992DBF38AE19",
            "Value" : "Beta"
        },
        {
            "Id" : "14261BB5-6115-4E60-9028-36F29812BFED",
            "Value" : "Gamma"
        }
    ]
}