{
	"USStates": [
    			{ "Name": "New South Wales", "Abbreviation": "NSW"},
    			{ "Name": "Queensland", "Abbreviation": "Qld"},
    			{ "Name": "South Australia", "Abbreviation": "SA"},
    			{ "Name": "Tasmania", "Abbreviation": "Tas"},
    			{ "Name": "Victoria", "Abbreviation": "Vic"},
    			{ "Name": "Western Australia", "Abbreviation": "WA"},
    			{ "Name": "Northern Territory", "Abbreviation": "NT"},
    			{ "Name": "Australian Capital Territory", "Abbreviation": "ACT"},				  
    			{ "Name": "Jervis Bay Territory", "Abbreviation": "JBT"},
    			{ "Name": "Coral Sea Islands", "Abbreviation": "CSI"},
    			{ "Name": "Christmas Island", "Abbreviation": "CX"},
    			{ "Name": "Cocos (Keeling) Islands", "Abbreviation": "CC"},
    			{ "Name": "Norfolk Island", "Abbreviation": "NF"},
				{ "Name": "Ashmore and Cartier Islands", "Abbreviation": "ACI"},
				{ "Name": "Heard Island and McDonald Islands", "Abbreviation": "HIMI"},
				{ "Name": "Australian Antarctic Territory", "Abbreviation": "AAT"}
	    ]
}