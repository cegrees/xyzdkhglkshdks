﻿using Landdb.Client.Services.Credentials;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using Thinktecture.IdentityModel.Client;

namespace Landdb.Controls.OAuth2 {
    public class OAuthCredentials : IStorageCredentials {
        private const long ExpirationBuffer = 5;
        DateTime authTime;

        string token = null;
        long expiresIn = -1;

        public OAuthCredentials(AuthorizeResponse response, DateTime authTime) {
            this.authTime = authTime;

            this.token = response.AccessToken;
            this.expiresIn = response.ExpiresIn;
        }

        public OAuthCredentials(string token, long expiresIn, DateTime authTime) {
            this.authTime = authTime;

            this.token = token;
            this.expiresIn = expiresIn;
        }

        [Obsolete]
        public async Task SignWebClientAsync(WebClient webClient) {
            await this.Validate();

            if (webClient == null) {
                throw new ArgumentException("webClient");
            }

            webClient.Headers.Add("Authorization", string.Format("Bearer {0}", this.token));
        }

        public async Task SignHttpRequestMessageAsync(HttpRequestMessage message) {
            await this.Validate();

            if (message == null) {
                throw new ArgumentException(nameof(message));
            }
            message.Headers.Authorization = new AuthenticationHeaderValue("Bearer", this.token);
        }

        public async Task SetBearerTokenAsync(HttpClient client) {
            await this.Validate();

            if (client == null) {
                throw new ArgumentException("webClient");
            }

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", this.token);
        }

        public bool IsExpired {
            get {
                if (string.IsNullOrWhiteSpace(token)) { return true; }
                return DateTime.UtcNow >= authTime.AddSeconds(this.expiresIn + ExpirationBuffer);
            }
        }

        public string Token {
            get { return token; }
        }

        private async Task Validate() {
            bool valid = DateTime.UtcNow < authTime.AddSeconds(this.expiresIn + ExpirationBuffer);
            if (!valid) {
                var newAuth = await LoginViewWindow.DoAuth();

                this.token = newAuth.token;
                this.expiresIn = newAuth.expiresIn;
                this.authTime = newAuth.authTime;
                //// TODO: Re-auth
                //throw new SecurityTokenExpirationException("Your security token has expired. Please, log out the application and log in again.");
            }
        }
    }
}
