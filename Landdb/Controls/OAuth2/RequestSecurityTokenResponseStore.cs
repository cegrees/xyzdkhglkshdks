﻿using System;
using Thinktecture.IdentityModel.Client;

namespace Landdb.Controls.OAuth2 {    
    /// <summary>
    /// Provides a class for storing a RequestSecurityTokenResponse to isolatedStorage
    /// </summary>
    public class RequestSecurityTokenResponseStore {
        /// <summary>
        /// Initializes a new instance of the RequestSecurityTokenResponseStore class. 
        /// </summary>
        public RequestSecurityTokenResponseStore() { }

        OAuthCredentials creds;
        public OAuthCredentials Credentials {
            get { return creds; }
            set {
                creds = value;
                AuthorizeTime = DateTime.UtcNow;
            }
        }
        public DateTime AuthorizeTime { get; private set; }

        public bool ContainsValidAuthorization() {
            return Credentials != null;
        }
    }
}

