﻿using Landdb.Client.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.Controls.OAuth2 {
    public static class OAuthEndpoints {
        public static string BaseAddress = RemoteConfigurationSettings.GetBaseUri(); //"http://localhost:1337/identity";

        public static string AuthorizeEndpoint = BaseAddress + "/identity/connect/authorize";
        public static string LogoutEndpoint = BaseAddress + "/identity/connect/endsession";
        public static string TokenEndpoint = BaseAddress + "/identity/connect/token";
        public static string UserInfoEndpoint = BaseAddress + "/identity/connect/userinfo";
        public static string IdentityTokenValidationEndpoint = BaseAddress + "/identity/connect/identitytokenvalidation";

        public static string CallbackUri = "oob://localhost/wpfclient";
    }
}
