﻿using Landdb.Client.Infrastructure;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Landdb.Resources;
using Thinktecture.IdentityModel.Client;

namespace Landdb.Controls.OAuth2 {
    /// <summary>
    /// Interaction logic for LoginViewControl.xaml
    /// </summary>
    public partial class LoginViewControl : UserControl, INotifyPropertyChanged {
        public OAuthCredentials Credentials { get; set; }
        Uri callbackUri;
        Action<OAuthCredentials> OnComplete;

        bool showOfflineSignOn;
        bool allowOfflineSignOn;
        bool isGoogleAuthPending;

        public LoginViewControl() {
            InitializeComponent();

            this.DataContext = this; // TODO: Pull this out into a view model

            this.ShowOfflineSignOn = !NetworkStatus.IsInternetAvailable();
        }
        
        public bool ShowOfflineSignOn {
            get { return showOfflineSignOn; }
            set {
                showOfflineSignOn = value;
                RaisePropertyChanged("ShowOfflineSignOn");
            }
        }

        public bool AllowOfflineSignOn {
            get { return allowOfflineSignOn; }
            set {
                allowOfflineSignOn = value;
                RaisePropertyChanged("AllowOfflineSignOn");
            }
        }

        public void StartAuth(Action<OAuthCredentials> onComplete) {
            var startUrl = CreateAuthorizeUrl();

            StartAuth(new Uri(startUrl), new Uri(OAuthEndpoints.CallbackUri), onComplete);
        }

        public void StartAuth(Uri startUri, Uri callbackUri, Action<OAuthCredentials> onComplete) {
            webView.Navigating += webView_Navigating;

            this.callbackUri = callbackUri;
            this.OnComplete = onComplete;
            webView.Navigate(startUri);
        }

        public async Task<OAuthCredentials> DoAuthAsync() {
            ShowOfflineSignOn = false;
            AllowOfflineSignOn = false;

            var client = new OAuth2Client(new Uri(OAuthEndpoints.AuthorizeEndpoint));
            
            var startUri = new Uri(CreateAuthorizeUrl(false));
            var callbackUri = new Uri(OAuthEndpoints.CallbackUri);

            var tcs = new TaskCompletionSource<OAuthCredentials>();

            NavigatingCancelEventHandler eh = (o, e) => {
                if (e.Uri.ToString().StartsWith(callbackUri.AbsoluteUri)) {
                    var authorizeResponse = new AuthorizeResponse(e.Uri.AbsoluteUri);
                    e.Cancel = true;

                    if (authorizeResponse.Error == "login_required") {
                        webView.Navigate(new Uri(CreateAuthorizeUrl(true)));
                    } else {
                        Credentials = new OAuthCredentials(authorizeResponse, DateTime.UtcNow);
                        try {
                            tcs.TrySetResult(Credentials);
                        } catch {
                            tcs.TrySetResult(null);
                        }
                    }
                } else if (e.Uri.AbsoluteUri.Contains("external?provider=Google")) {
                    // Google sign-in.
                    e.Cancel = true;
                    var gAuthTask = DoGoogleAuth();
                    gAuthTask.ContinueWith(x => {
                        Credentials = gAuthTask.Result;

                        //if (AuthorizeResponse.Error == "login_required") {
                        //    webView.Navigate(new Uri(CreateAuthorizeUrl(true)));
                        //} else {
                            try {
                                tcs.TrySetResult(Credentials);
                            } catch {
                                tcs.TrySetResult(null);
                            }
                        //}
                    });
                }
            };
            webView.Navigating += eh;
            webView.Navigate(startUri);

            var result = await tcs.Task;
            return result;
        }

        async void webView_Navigating(object sender, NavigatingCancelEventArgs e) {
            if (e.Uri.ToString().StartsWith(callbackUri.AbsoluteUri)) {
                var authorizeResponse = new AuthorizeResponse(e.Uri.AbsoluteUri);
                Credentials = new OAuthCredentials(authorizeResponse, DateTime.UtcNow);

                e.Cancel = true;
                this.Visibility = System.Windows.Visibility.Hidden;

                if (OnComplete != null) {
                    OnComplete(Credentials);
                }
            } else if(e.Uri.AbsoluteUri.Contains("external?provider=Google")) {
                // Google login
                e.Cancel = true;
                if (!isGoogleAuthPending) {
                    isGoogleAuthPending = true;
                    this.Visibility = System.Windows.Visibility.Hidden;
                    Credentials = await DoGoogleAuth();

                    if (OnComplete != null) {
                        OnComplete(Credentials);
                    }
                }
            }
        }

        async Task<OAuthCredentials> DoGoogleAuth() {
            var googleCallbackUri = "http://localhost:3000/account/codecallback";
            var googleListenUri = "http://localhost:3000/account/";
            var googleStartUrl = CreateAuthorizeUrl(clientId: "agc-ldb-win-client-code", responseType: "code", redirectUri: googleCallbackUri, acrValues: "idp:Google");

            var http = new HttpListener();
            http.Prefixes.Add(googleListenUri);
            http.Start();

            System.Diagnostics.Process.Start(googleStartUrl);

            var context = await http.GetContextAsync();

            Application.Current.MainWindow.Activate();

            // Sends an HTTP response to the browser.
            var response = context.Response;
            string responseString = string.Format("<html><head><meta http-equiv='refresh' content='10;url=http://www.agconnections.com'></head><body>{0}</body></html>", Strings.PleaseReturnToTheApp_Text);
            var buffer = System.Text.Encoding.UTF8.GetBytes(responseString);
            response.ContentLength64 = buffer.Length;
            var responseOutput = response.OutputStream;

            Task responseTask = responseOutput.WriteAsync(buffer, 0, buffer.Length).ContinueWith((task) => {
                responseOutput.Close();
                http.Stop();
                Console.WriteLine("HTTP server stopped.");
            });

            var codeResponse = new AuthorizeResponse(context.Request.RawUrl);
            // TODO: check codeResponse.State for match...

            var authResponse = await DoGoogleCodeExchange(codeResponse.Code, googleCallbackUri);

            await responseTask; // make sure the browser response is written before returning...

            return authResponse;
        }

        async Task<OAuthCredentials> DoGoogleCodeExchange(string code, string redirectUri) {
            var clientSecret = "blehblahqwertypoiuuiopyappyyappyyappybusinesspeopletalktoomuchyapyapyap";
            string tokenRequestBody = $"code={code}&redirect_uri={redirectUri}&client_id=agc-ldb-win-client-code&client_secret={clientSecret}&scope=&grant_type=authorization_code";

            var client = new OAuth2Client(new Uri(OAuthEndpoints.AuthorizeEndpoint));

            // sends the request
            HttpWebRequest tokenRequest = (HttpWebRequest)WebRequest.Create(OAuthEndpoints.TokenEndpoint);
            tokenRequest.Method = "POST";
            tokenRequest.ContentType = "application/x-www-form-urlencoded";
            tokenRequest.Accept = "Accept=text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
            byte[] _byteVersion = Encoding.ASCII.GetBytes(tokenRequestBody);
            tokenRequest.ContentLength = _byteVersion.Length;

            Stream stream = tokenRequest.GetRequestStream();

            await stream.WriteAsync(_byteVersion, 0, _byteVersion.Length);

            stream.Close();

            try {
                // gets the response
                WebResponse tokenResponse = await tokenRequest.GetResponseAsync();

                using (StreamReader reader = new StreamReader(tokenResponse.GetResponseStream())) {
                    // reads response body
                    string responseText = await reader.ReadToEndAsync();
                    Console.WriteLine(responseText);

                    var tr = new TokenResponse(responseText);

                    OAuthCredentials creds = new OAuthCredentials(tr.AccessToken, tr.ExpiresIn, DateTime.UtcNow);
                    return creds;
                }

            } catch (WebException ex) {
                if (ex.Status == WebExceptionStatus.ProtocolError) {
                    var response = ex.Response as HttpWebResponse;
                    if (response != null) {
                        Console.WriteLine("HTTP: " + response.StatusCode);

                        using (StreamReader reader = new StreamReader(response.GetResponseStream())) {
                            // reads response body
                            string responseText = await reader.ReadToEndAsync();
                            Console.WriteLine(responseText);
                        }
                    }
                }
                return null;
            }
        }

        string CreateAuthorizeUrl(bool prompt = true, string clientId = "agc-ldb-win-client", string responseType = "token", string redirectUri = "oob://localhost/wpfclient", string acrValues = null) {
            var client = new OAuth2Client(new Uri(OAuthEndpoints.AuthorizeEndpoint));

            var additionalValues = new Dictionary<string, string>();

            if (!prompt) {
                additionalValues.Add("prompt", "none");
            }

            var startUrl = client.CreateAuthorizeUrl(
                                clientId: clientId,
                //clientId: "agc-ig-test",
                responseType: responseType,
                scope: "read write",
                redirectUri: redirectUri,
                state: Guid.NewGuid().ToString(),
                nonce: DateTime.UtcNow.Ticks.ToString(),
                additionalValues: additionalValues,
                loginHint: "",
                acrValues: acrValues);
            
            return startUrl;
        }

        private void OfflineLink_Click(object sender, RoutedEventArgs e) {
            if (OnComplete != null) {
                OnComplete(null);
            }
        }


        void RaisePropertyChanged(string propertyName) {
            var handler = PropertyChanged;
            if (handler != null) {
                PropertyChangedEventArgs e = new PropertyChangedEventArgs(propertyName);
                handler(this, e);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

    }
}
