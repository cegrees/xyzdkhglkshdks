﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Thinktecture.IdentityModel.Client;

namespace Landdb.Controls.OAuth2 {
    /// <summary>
    /// Interaction logic for LoginViewWindow.xaml
    /// </summary>
    public partial class LoginViewWindow : Window {
        public LoginViewWindow() {
            InitializeComponent();
        }

        public static async Task<OAuthCredentials> DoAuth() {
            OAuthCredentials result = null;
            await App.Current.Dispatcher.Invoke(async () => {
                LoginViewWindow window = new LoginViewWindow();
                window.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                window.Show();
                CancelEventHandler eh = (s, e) => {
                    result = null;
                    return;
                };
                window.Closing += eh;
                result = await window.authControl.DoAuthAsync();
                window.Closing -= eh;
                window.Close();
            });
            return result;
        }
    }
}
