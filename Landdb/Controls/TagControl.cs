﻿//
// Adapted from: http://stackoverflow.com/questions/15167809/how-can-i-create-a-tagging-control-similar-to-evernote-in-wpf
//

using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Landdb.Controls {
    [TemplatePart(Name = "PART_CreateTagButton", Type = typeof(Button))]
    public class TagControl : ListBox {
        public event EventHandler<TagEventArgs> TagClick;
        public event EventHandler<TagEventArgs> TagAdded;
        public event EventHandler<TagEventArgs> TagRemoved;

        static TagControl() {
            // lookless control, get default style from generic.xaml
            DefaultStyleKeyProperty.OverrideMetadata(typeof(TagControl), new FrameworkPropertyMetadata(typeof(TagControl)));
        }

        public TagControl() {
            //// some dummy data, this needs to be provided by user
            //this.ItemsSource = new List<EvernoteTagItem>() { new EvernoteTagItem("receipt"), new EvernoteTagItem("restaurant") };
            //this.AllTags = new List<string>() { "recipe", "red" };
        }

        // AllTags
        public List<string> AllTags { get { return (List<string>)GetValue(AllTagsProperty); } set { SetValue(AllTagsProperty, value); } }
        public static readonly DependencyProperty AllTagsProperty = DependencyProperty.Register("AllTags", typeof(List<string>), typeof(TagControl), new PropertyMetadata(new List<string>()));


        // IsEditing, readonly
        public bool IsEditing { get { return (bool)GetValue(IsEditingProperty); } internal set { SetValue(IsEditingPropertyKey, value); } }
        private static readonly DependencyPropertyKey IsEditingPropertyKey = DependencyProperty.RegisterReadOnly("IsEditing", typeof(bool), typeof(TagControl), new FrameworkPropertyMetadata(false));
        public static readonly DependencyProperty IsEditingProperty = IsEditingPropertyKey.DependencyProperty;

        public override void OnApplyTemplate() {
            Button createBtn = this.GetTemplateChild("PART_CreateTagButton") as Button;
            if (createBtn != null)
                createBtn.Click += createBtn_Click;

            base.OnApplyTemplate();
        }

        /// <summary>
        /// Executed when create new tag button is clicked.
        /// Adds an EvernoteTagItem to the collection and puts it in edit mode.
        /// </summary>
        void createBtn_Click(object sender, RoutedEventArgs e) {
            var newItem = new TagItem() { IsEditing = true };
            AddTag(newItem);
            this.SelectedItem = newItem;
            this.IsEditing = true;

        }

        /// <summary>
        /// Adds a tag to the collection
        /// </summary>
        internal void AddTag(TagItem tag) {
            if (this.ItemsSource == null)
                this.ItemsSource = new List<TagItem>();

            ((IList)this.ItemsSource).Add(tag); // assume IList for convenience
            this.Items.Refresh();
        }

        internal void CompleteTag(TagItem tag) {
            this.IsEditing = false;
            if (TagAdded != null)
                TagAdded(this, new TagEventArgs(tag));
        }

        internal void CancelTag(TagItem tag) {
            this.IsEditing = false;
        }

        /// <summary>
        /// Removes a tag from the collection
        /// </summary>
        internal void RemoveTag(TagItem tag, bool cancelEvent = false) {
            if (this.ItemsSource != null) {
                ((IList)this.ItemsSource).Remove(tag); // assume IList for convenience
                this.Items.Refresh();

                if (TagRemoved != null && !cancelEvent)
                    TagRemoved(this, new TagEventArgs(tag));
            }
        }


        /// <summary>
        /// Raises the TagClick event
        /// </summary>
        internal void RaiseTagClick(TagItem tag) {
            if (this.TagClick != null)
                TagClick(this, new TagEventArgs(tag));
        }
    }

    public class TagEventArgs : EventArgs {
        public TagItem Item { get; set; }

        public TagEventArgs(TagItem item) {
            this.Item = item;
        }
    }


    [TemplatePart(Name = "PART_InputBox", Type = typeof(AutoCompleteBox))]
    [TemplatePart(Name = "PART_DeleteTagButton", Type = typeof(Button))]
    [TemplatePart(Name = "PART_TagButton", Type = typeof(Button))]
    public class TagItem : Control {

        static TagItem() {
            // lookless control, get default style from generic.xaml
            DefaultStyleKeyProperty.OverrideMetadata(typeof(TagItem), new FrameworkPropertyMetadata(typeof(TagItem)));
        }

        public TagItem() { }
        public TagItem(string text)
            : this() {
            this.Text = text;
        }

        // Text
        public string Text { get { return (string)GetValue(TextProperty); } set { SetValue(TextProperty, value); } }
        public static readonly DependencyProperty TextProperty = DependencyProperty.Register("Text", typeof(string), typeof(TagItem), new PropertyMetadata(null));

        // IsEditing, readonly
        public bool IsEditing { get { return (bool)GetValue(IsEditingProperty); } internal set { SetValue(IsEditingPropertyKey, value); } }
        private static readonly DependencyPropertyKey IsEditingPropertyKey = DependencyProperty.RegisterReadOnly("IsEditing", typeof(bool), typeof(TagItem), new FrameworkPropertyMetadata(false));
        public static readonly DependencyProperty IsEditingProperty = IsEditingPropertyKey.DependencyProperty;

        /// <summary>
        /// Wires up delete button click and focus lost 
        /// </summary>
        public override void OnApplyTemplate() {
            AutoCompleteBox inputBox = this.GetTemplateChild("PART_InputBox") as AutoCompleteBox;
            if (inputBox != null) {
				inputBox.IsTextCompletionEnabled = false;
                inputBox.LostFocus += inputBox_LostFocus;
                inputBox.Loaded += inputBox_Loaded;
                inputBox.SelectionChanged += inputBox_SelectionChanged;
            }

            Button btn = this.GetTemplateChild("PART_TagButton") as Button;
            if (btn != null) {
				btn.Loaded += (s, e) => {
                    Button b = s as Button;
                    var btnDelete = b.Template.FindName("PART_DeleteTagButton", b) as Button; // will only be found once button is loaded
                    if (btnDelete != null) {
                        btnDelete.Click -= btnDelete_Click; // make sure the handler is applied just once
                        btnDelete.Click += btnDelete_Click;
                    }
                };

                btn.Click += (s, e) => {
                    var parent = GetParent();
                    if (parent != null)
                        parent.RaiseTagClick(this); // raise the TagClick event of the TagControl
                };
            }

            base.OnApplyTemplate();
        }

        /// <summary>
        /// Handles the click on the delete glyph of the tag button.
        /// Removes the tag from the collection.
        /// </summary>
        void btnDelete_Click(object sender, RoutedEventArgs e) {

            var item = FindUpVisualTree<TagItem>(sender as FrameworkElement);
            var parent = GetParent();
            if (item != null && parent != null)
                parent.RemoveTag(item);

            e.Handled = true; // bubbling would raise the tag click event
        }

        /// <summary>
        /// When an AutoCompleteBox is created, set the focus to the textbox.
        /// Wire PreviewKeyDown event to handle Escape/Enter keys
        /// </summary>
        /// <remarks>AutoCompleteBox.Focus() is broken: http://stackoverflow.com/questions/3572299/autocompletebox-focus-in-wpf</remarks>
        void inputBox_Loaded(object sender, RoutedEventArgs e) {
            AutoCompleteBox acb = sender as AutoCompleteBox;
            if (acb != null) {
                var tb = acb.Template.FindName("Text", acb) as TextBox;
                if (tb != null)
                    tb.Focus();

                // PreviewKeyDown, because KeyDown does not bubble up for Enter
                acb.PreviewKeyDown += (s, e1) => {
                    var parent = GetParent();
                    if (parent != null) {

                        switch (e1.Key) {
                            case (Key.Enter):  // accept tag
	                            parent.Focus();
								parent.CompleteTag(this);
                                break;
                            case (Key.Escape): // reject tag
                                parent.CancelTag(this);
                                parent.Focus();
                                parent.RemoveTag(this, true); // do not raise RemoveTag event
                                break;
                            case (Key.OemComma): // move to next tag
                                parent.Focus();
                                parent.CompleteTag(this);
                                parent.AddTag(new TagItem() { IsEditing = true });
                                break;
                            case (Key.Space): // insert _ instead
                                tb.Text += "_";
                                tb.Select(tb.Text.Length, 0);
                                e1.Handled = true;
                                break;
                        }
                    }
                };
            }
        }

        /// <summary>
        /// Set IsEditing to false when the AutoCompleteBox loses keyboard focus.
        /// This will change the template, displaying the tag as a button.
        /// </summary>
        void inputBox_LostFocus(object sender, RoutedEventArgs e) {
            if (!this.IsEditing) { return; }
            var acb = sender as AutoCompleteBox;
            if (acb != null && (acb.IsFocused)) { return; }
            if (acb != null && acb.IsDropDownOpen) { return; }

            this.IsEditing = false;
            var parent = GetParent();
            if (parent != null) {
                if (parent.IsEditing) {
                    parent.CompleteTag(this);
                    //parent.IsEditing = false;
                }
            }
        }

        void inputBox_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            if (!this.IsEditing) { return; }
            var acb = sender as AutoCompleteBox;
            if (acb != null && (acb.IsFocused)) { return; }
            if (acb != null && acb.IsDropDownOpen) { return; }
            if (e.AddedItems == null || e.AddedItems.Count == 0) { return; }

            this.IsEditing = false;
            var parent = GetParent();
            if (parent != null) {
                if (parent.IsEditing) {
                    parent.CompleteTag(this);
                    //parent.IsEditing = false;
                }
            }
        }

        private TagControl GetParent() {
            return FindUpVisualTree<TagControl>(this);
        }

        /// <summary>
        /// Walks up the visual tree to find object of type T, starting from initial object
        /// http://www.codeproject.com/Tips/75816/Walk-up-the-Visual-Tree
        /// </summary>
        private static T FindUpVisualTree<T>(DependencyObject initial) where T : DependencyObject {
            DependencyObject current = initial;
            while (current != null && current.GetType() != typeof(T)) {
                current = VisualTreeHelper.GetParent(current);
            }
            return current as T;
        }
    }

}