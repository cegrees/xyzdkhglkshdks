﻿#pragma checksum "..\..\..\..\..\..\Views\Fields\FieldDetails\MapView.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "6B08B90FF62176D31EEA84FC80D92E527FF9F910"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Landdb.Controls;
using Landdb.Views.Fields.Popups;
using MahApps.Metro.Controls;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using WPFLocalizeExtension.Engine;
using WPFLocalizeExtension.Extensions;
using WPFLocalizeExtension.Providers;
using WPFLocalizeExtension.TypeConverters;
using Xceed.Wpf.Toolkit;
using Xceed.Wpf.Toolkit.Chromes;
using Xceed.Wpf.Toolkit.Core.Converters;
using Xceed.Wpf.Toolkit.Core.Input;
using Xceed.Wpf.Toolkit.Core.Media;
using Xceed.Wpf.Toolkit.Core.Utilities;
using Xceed.Wpf.Toolkit.Panels;
using Xceed.Wpf.Toolkit.Primitives;
using Xceed.Wpf.Toolkit.PropertyGrid;
using Xceed.Wpf.Toolkit.PropertyGrid.Attributes;
using Xceed.Wpf.Toolkit.PropertyGrid.Commands;
using Xceed.Wpf.Toolkit.PropertyGrid.Converters;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;
using Xceed.Wpf.Toolkit.Zoombox;


namespace Landdb.Views.Fields.FieldDetails {
    
    
    /// <summary>
    /// MapView
    /// </summary>
    public partial class MapView : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 60 "..\..\..\..\..\..\Views\Fields\FieldDetails\MapView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid LayoutRoot;
        
        #line default
        #line hidden
        
        
        #line 62 "..\..\..\..\..\..\Views\Fields\FieldDetails\MapView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ContentControl MapContainer;
        
        #line default
        #line hidden
        
        
        #line 78 "..\..\..\..\..\..\Views\Fields\FieldDetails\MapView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button CLUImportToolsButton;
        
        #line default
        #line hidden
        
        
        #line 79 "..\..\..\..\..\..\Views\Fields\FieldDetails\MapView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button DeleteToolsButton;
        
        #line default
        #line hidden
        
        
        #line 108 "..\..\..\..\..\..\Views\Fields\FieldDetails\MapView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button MapToolsButton;
        
        #line default
        #line hidden
        
        
        #line 154 "..\..\..\..\..\..\Views\Fields\FieldDetails\MapView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button OptionsMapButton;
        
        #line default
        #line hidden
        
        
        #line 165 "..\..\..\..\..\..\Views\Fields\FieldDetails\MapView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button ImportToolsButton;
        
        #line default
        #line hidden
        
        
        #line 176 "..\..\..\..\..\..\Views\Fields\FieldDetails\MapView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button ApexImportToolsButton;
        
        #line default
        #line hidden
        
        
        #line 215 "..\..\..\..\..\..\Views\Fields\FieldDetails\MapView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Primitives.Popup MapToolsPopup;
        
        #line default
        #line hidden
        
        
        #line 221 "..\..\..\..\..\..\Views\Fields\FieldDetails\MapView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Primitives.Popup SearchMapPopup;
        
        #line default
        #line hidden
        
        
        #line 227 "..\..\..\..\..\..\Views\Fields\FieldDetails\MapView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Primitives.Popup OptionsMapPopup;
        
        #line default
        #line hidden
        
        
        #line 233 "..\..\..\..\..\..\Views\Fields\FieldDetails\MapView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Primitives.Popup ImportToolsPopup;
        
        #line default
        #line hidden
        
        
        #line 239 "..\..\..\..\..\..\Views\Fields\FieldDetails\MapView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Primitives.Popup ApexImportToolsPopup;
        
        #line default
        #line hidden
        
        
        #line 245 "..\..\..\..\..\..\Views\Fields\FieldDetails\MapView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Primitives.Popup ImportCLUToolsPopup;
        
        #line default
        #line hidden
        
        
        #line 251 "..\..\..\..\..\..\Views\Fields\FieldDetails\MapView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Primitives.Popup DeleteToolsPopup;
        
        #line default
        #line hidden
        
        
        #line 284 "..\..\..\..\..\..\Views\Fields\FieldDetails\MapView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid SettingsGrid;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Landdb;component/views/fields/fielddetails/mapview.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\..\..\Views\Fields\FieldDetails\MapView.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.LayoutRoot = ((System.Windows.Controls.Grid)(target));
            return;
            case 2:
            this.MapContainer = ((System.Windows.Controls.ContentControl)(target));
            return;
            case 3:
            this.CLUImportToolsButton = ((System.Windows.Controls.Button)(target));
            
            #line 78 "..\..\..\..\..\..\Views\Fields\FieldDetails\MapView.xaml"
            this.CLUImportToolsButton.Click += new System.Windows.RoutedEventHandler(this.CLUImportToolsButton_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.DeleteToolsButton = ((System.Windows.Controls.Button)(target));
            
            #line 79 "..\..\..\..\..\..\Views\Fields\FieldDetails\MapView.xaml"
            this.DeleteToolsButton.Click += new System.Windows.RoutedEventHandler(this.DeleteToolsButton_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.MapToolsButton = ((System.Windows.Controls.Button)(target));
            
            #line 108 "..\..\..\..\..\..\Views\Fields\FieldDetails\MapView.xaml"
            this.MapToolsButton.Click += new System.Windows.RoutedEventHandler(this.MapToolsButton_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.OptionsMapButton = ((System.Windows.Controls.Button)(target));
            
            #line 154 "..\..\..\..\..\..\Views\Fields\FieldDetails\MapView.xaml"
            this.OptionsMapButton.Click += new System.Windows.RoutedEventHandler(this.OptionsMapButton_Click_1);
            
            #line default
            #line hidden
            return;
            case 7:
            this.ImportToolsButton = ((System.Windows.Controls.Button)(target));
            
            #line 165 "..\..\..\..\..\..\Views\Fields\FieldDetails\MapView.xaml"
            this.ImportToolsButton.Click += new System.Windows.RoutedEventHandler(this.ImportToolsButton_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.ApexImportToolsButton = ((System.Windows.Controls.Button)(target));
            
            #line 176 "..\..\..\..\..\..\Views\Fields\FieldDetails\MapView.xaml"
            this.ApexImportToolsButton.Click += new System.Windows.RoutedEventHandler(this.ApexImportToolsButton_Click);
            
            #line default
            #line hidden
            return;
            case 9:
            
            #line 210 "..\..\..\..\..\..\Views\Fields\FieldDetails\MapView.xaml"
            ((System.Windows.Documents.Hyperlink)(target)).RequestNavigate += new System.Windows.Navigation.RequestNavigateEventHandler(this.Hyperlink_RequestNavigate);
            
            #line default
            #line hidden
            return;
            case 10:
            this.MapToolsPopup = ((System.Windows.Controls.Primitives.Popup)(target));
            return;
            case 11:
            this.SearchMapPopup = ((System.Windows.Controls.Primitives.Popup)(target));
            return;
            case 12:
            this.OptionsMapPopup = ((System.Windows.Controls.Primitives.Popup)(target));
            return;
            case 13:
            this.ImportToolsPopup = ((System.Windows.Controls.Primitives.Popup)(target));
            return;
            case 14:
            this.ApexImportToolsPopup = ((System.Windows.Controls.Primitives.Popup)(target));
            return;
            case 15:
            this.ImportCLUToolsPopup = ((System.Windows.Controls.Primitives.Popup)(target));
            return;
            case 16:
            this.DeleteToolsPopup = ((System.Windows.Controls.Primitives.Popup)(target));
            return;
            case 17:
            this.SettingsGrid = ((System.Windows.Controls.Grid)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

