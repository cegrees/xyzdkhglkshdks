﻿#pragma checksum "..\..\..\..\..\Views\Production\RecommendationsPageView.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "E25C28FF5702BF91A9F514362F7A298512327A1B"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Landdb.Behaviors;
using Landdb.Converters;
using Landdb.ViewModel.Production;
using MahApps.Metro.Controls;
using MetroToolkit.Controls;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using WPFLocalizeExtension.Engine;
using WPFLocalizeExtension.Extensions;
using WPFLocalizeExtension.Providers;
using WPFLocalizeExtension.TypeConverters;


namespace Landdb.Views.Production {
    
    
    /// <summary>
    /// RecommendationsPageView
    /// </summary>
    public partial class RecommendationsPageView : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 65 "..\..\..\..\..\Views\Production\RecommendationsPageView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button AddButton;
        
        #line default
        #line hidden
        
        
        #line 88 "..\..\..\..\..\Views\Production\RecommendationsPageView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button SetCharmButton;
        
        #line default
        #line hidden
        
        
        #line 89 "..\..\..\..\..\Views\Production\RecommendationsPageView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button CreateWorkOrderButton;
        
        #line default
        #line hidden
        
        
        #line 90 "..\..\..\..\..\Views\Production\RecommendationsPageView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button CreateApplicationButton;
        
        #line default
        #line hidden
        
        
        #line 91 "..\..\..\..\..\Views\Production\RecommendationsPageView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button ShareRecommendationButton;
        
        #line default
        #line hidden
        
        
        #line 94 "..\..\..\..\..\Views\Production\RecommendationsPageView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Primitives.Popup CharmPopup;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Landdb;component/views/production/recommendationspageview.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\..\Views\Production\RecommendationsPageView.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.AddButton = ((System.Windows.Controls.Button)(target));
            return;
            case 2:
            this.SetCharmButton = ((System.Windows.Controls.Button)(target));
            
            #line 88 "..\..\..\..\..\Views\Production\RecommendationsPageView.xaml"
            this.SetCharmButton.Click += new System.Windows.RoutedEventHandler(this.SetCharmButton_Click);
            
            #line default
            #line hidden
            return;
            case 3:
            this.CreateWorkOrderButton = ((System.Windows.Controls.Button)(target));
            return;
            case 4:
            this.CreateApplicationButton = ((System.Windows.Controls.Button)(target));
            return;
            case 5:
            this.ShareRecommendationButton = ((System.Windows.Controls.Button)(target));
            return;
            case 6:
            this.CharmPopup = ((System.Windows.Controls.Primitives.Popup)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

