﻿using System.Collections.ObjectModel;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;
using ThinkGeo.MapSuite.Core;
using ThinkGeo.MapSuite.WpfDesktopEdition;
using System;

namespace Landdb.MapStyles.Overlays
{
    class FastEditPolygonOverlay : EditInteractiveOverlay
    {
        private PointStyle controlPointStyle;
        private PointStyle draggedControlPointStyle;
        private TextStyle measureTextStyle;
        private Collection<Feature> existingFeatures;
        private PointShape startPoint;
        private Vertex movingVertex;
        private PointShape targetPoint;
        private PointShape targetPoint1;
        private PointShape targetPoint2;
        private Feature referencedfeature;
        Collection<Feature> controlPointsSaved = new Collection<Feature>();
        private DistanceUnit mapDistanceUnit;

        public FastEditPolygonOverlay()
            : base() {
            ExistingControlPointsLayer.Open();
            ExistingControlPointsLayer.Columns.Add(new FeatureSourceColumn("PointsSelected"));
            ExistingControlPointsLayer.Columns.Add(new FeatureSourceColumn("Distance"));
            ExistingControlPointsLayer.Close();
            existingFeatures = new Collection<Feature>();
            mapDistanceUnit = DistanceUnit.Feet;
        }

        public PointStyle ControlPointStyle
        {
            get { return controlPointStyle; }
            set { controlPointStyle = value; }
        }

        public PointStyle DraggedControlPointStyle
        {
            get { return draggedControlPointStyle; }
            set { draggedControlPointStyle = value; }
        }

        public TextStyle MeasureTextStyle {
            get { return measureTextStyle; }
            set { measureTextStyle = value; }
        }

        public Vertex MovingVertex {
            get { return movingVertex; }
            set { movingVertex = value; }
        }

        public PointShape TargetPoint {
            get { return targetPoint; }
            set { targetPoint = value; }
        }

        public PointShape TargetPoint1 {
            get { return targetPoint1; }
            set { targetPoint1 = value; }
        }

        public PointShape TargetPoint2 {
            get { return targetPoint2; }
            set { targetPoint2 = value; }
        }

        public DistanceUnit MapDistanceUnit
        {
            get { return mapDistanceUnit; }
            set { mapDistanceUnit = value; }
        }

        protected override InteractiveResult MouseDownCore(InteractionArguments interactionArguments) {
            InteractiveResult result = base.MouseDownCore(interactionArguments);
            startPoint = null;
            if (interactionArguments.MouseButton == MapMouseButton.Left) {
                if (EditShapesLayer.InternalFeatures.Count > 0) {
                    if (SetSelectedControlPoint(new PointShape(interactionArguments.WorldX, interactionArguments.WorldY), interactionArguments.SearchingTolerance)) {
                        startPoint = new PointShape(interactionArguments.WorldX, interactionArguments.WorldY);
                    }
                }
            }
            return result;
        }

        protected override InteractiveResult MouseMoveCore(InteractionArguments interactionArguments) {
            //InteractiveResult result = base.MouseMoveCore(interactionArguments);
            InteractiveResult result = new InteractiveResult();
            if (interactionArguments.MouseButton == MapMouseButton.None) {
                if (EditShapesLayer.InternalFeatures.Count > 0) {
                    //result = base.MouseMoveCore(interactionArguments);
                }
            }
            //end = DateTime.UtcNow;
            //double lasttime = (end - last).TotalMilliseconds;
            //Console.WriteLine("mouse move took {0} {1}", lasttime, "ms");
            //last = DateTime.UtcNow;
            return result;
        }

        protected override void OnControlPointSelected(ControlPointSelectedEditInteractiveOverlayEventArgs e) {
            base.OnControlPointSelected(e);
            existingFeatures = new Collection<Feature>();
            //referencedfeature = e.SelectedFeature;
            //referencedfeature = null;
            for (int i = 0; i < ExistingControlPointsLayer.InternalFeatures.Count; i++) {
                ExistingControlPointsLayer.InternalFeatures[i].ColumnValues["State"] = "";
                ExistingControlPointsLayer.InternalFeatures[i].ColumnValues["PointsSelected"] = "";
                ExistingControlPointsLayer.InternalFeatures[i].ColumnValues["Distance"] = "";
                //EditShapesLayer.ZoomLevelSet.ZoomLevel01.CustomStyles
            }

            for (int i = 0; i < ExistingControlPointsLayer.InternalFeatures.Count; i++) {
                Feature feature2 = ExistingControlPointsLayer.InternalFeatures[i];
                PointShape pointShape = feature2.GetShape() as PointShape;
                ExistingControlPointsLayer.InternalFeatures[i].ColumnValues["State"] = "";
                if (feature2 == e.SelectedFeature) {
                    ExistingControlPointsLayer.InternalFeatures[i].ColumnValues["State"] = "selected";
                    ExistingControlPointsLayer.InternalFeatures[i].ColumnValues["PointsSelected"] = "drag";
                    referencedfeature = e.SelectedFeature;
                    targetPoint = pointShape;
                }
                if (feature2.ColumnValues["State"] == "selected") {
                    if (i == 0) {
                        ExistingControlPointsLayer.InternalFeatures[ExistingControlPointsLayer.InternalFeatures.Count - 1].ColumnValues["PointsSelected"] = "anchor";
                        existingFeatures.Add(ExistingControlPointsLayer.InternalFeatures[ExistingControlPointsLayer.InternalFeatures.Count - 1]);
                        PointShape ps1 = ExistingControlPointsLayer.InternalFeatures[ExistingControlPointsLayer.InternalFeatures.Count - 1].GetShape() as PointShape;
                        targetPoint1 = ps1;
                    }
                    else {
                        ExistingControlPointsLayer.InternalFeatures[i - 1].ColumnValues["PointsSelected"] = "anchor";
                        existingFeatures.Add(ExistingControlPointsLayer.InternalFeatures[i - 1]);
                        PointShape ps1 = ExistingControlPointsLayer.InternalFeatures[i - 1].GetShape() as PointShape;
                        targetPoint1 = ps1;
                    }
                    if (i < ExistingControlPointsLayer.InternalFeatures.Count) {
                        ExistingControlPointsLayer.InternalFeatures[i + 1].ColumnValues["PointsSelected"] = "anchor";
                        existingFeatures.Add(ExistingControlPointsLayer.InternalFeatures[i + 1]);
                        PointShape ps2 = ExistingControlPointsLayer.InternalFeatures[i + 1].GetShape() as PointShape;
                        targetPoint2 = ps2;
                    }
                    else {
                        ExistingControlPointsLayer.InternalFeatures[0].ColumnValues["PointsSelected"] = "anchor";
                        existingFeatures.Add(ExistingControlPointsLayer.InternalFeatures[0]);
                        PointShape ps2 = ExistingControlPointsLayer.InternalFeatures[0].GetShape() as PointShape;
                        targetPoint2 = ps2;
                    }
                }
            }
        }

        protected virtual void OnFeatureEditing(FeatureEditingEditInteractiveOverlayEventArgs e) {
            base.OnFeatureEditing(e);
        }

        protected virtual void OnFeatureEdited(FeatureEditedEditInteractiveOverlayEventArgs e) {
            base.OnFeatureEdited(e);
        }

        protected override void OnDrawing(DrawingOverlayEventArgs e) {
            base.OnDrawing(e);
        }

        protected override void OnFeatureDragging(FeatureDraggingEditInteractiveOverlayEventArgs e) {
            base.OnFeatureDragging(e);
        }

        protected override void OnVertexMoving(VertexMovingEditInteractiveOverlayEventArgs e) {
            if (referencedfeature != null) {
                referencedfeature = new Feature(e.TargetVertex, referencedfeature.ColumnValues);
            }
//            movingVertex = e.MovingVertex;
//            targetgPoint = e.TargetVertex;
////            if (e.MouseButton == MapMouseButton.None) {
//                if (EditShapesLayer.InternalFeatures.Count > 0) {
//                    base.OnVertexMoving(e);
//                }
////            }

            base.OnVertexMoving(e);
        }

        protected override void OnVertexMoved(VertexMovedEditInteractiveOverlayEventArgs e) {
            if (referencedfeature != null) {
                referencedfeature = new Feature(new PointShape(e.MovedVertex), referencedfeature.ColumnValues);
            }
            base.OnVertexMoved(e);
        }

        protected override void OnVertexAdded(VertexAddedEditInteractiveOverlayEventArgs e) {
            if (referencedfeature != null) {
                referencedfeature = new Feature(new PointShape(e.AddedVertex), referencedfeature.ColumnValues);
            }
            base.OnVertexAdded(e);
        }

        protected override void OnVertexRemoved(VertexRemovedEditInteractiveOverlayEventArgs e) {
            if (referencedfeature != null) {
                referencedfeature = new Feature(new PointShape(e.RemovedVertex), referencedfeature.ColumnValues);
            }
            base.OnVertexRemoved(e);
        }

        protected override void DrawCore(RectangleShape targetExtent, OverlayRefreshType overlayRefreshType) {
            base.DrawCore(targetExtent, overlayRefreshType);

            //anchorpoints = new Collection<Feature>();
            //if (referencedfeature != null) {
            //    ExistingControlPointsLayer.Open();
            //    for (int i = 0; i < ExistingControlPointsLayer.InternalFeatures.Count; i++) {
            //        if (ExistingControlPointsLayer.InternalFeatures[i].ColumnValues["PointsSelected"] == "draw") {
            //            referencedfeature = ExistingControlPointsLayer.InternalFeatures[i];
            //        }
            //    }
            //    for (int i = 0; i < ExistingControlPointsLayer.InternalFeatures.Count; i++) {
            //        try {
            //            if (ExistingControlPointsLayer.InternalFeatures[i].ColumnValues["PointsSelected"] == "draw") { continue; }
            //            Feature feature = ExistingControlPointsLayer.InternalFeatures[i];
            //            Feature[] features = new Feature[1] { feature };
            //            if (ExistingControlPointsLayer.InternalFeatures[i].ColumnValues["PointsSelected"] == "anchor") {
            //                PointShape pointShape = feature.GetShape() as PointShape;
            //                PointShape closestPointShape = referencedfeature.GetShape().GetClosestPointTo(pointShape, GeographyUnit.DecimalDegree);
            //                if (closestPointShape != null) {
            //                    double Dist = 0;
            //                    Dist = System.Math.Round(closestPointShape.GetDistanceTo(pointShape, GeographyUnit.Meter, DistanceUnit.Feet));
            //                    ExistingControlPointsLayer.InternalFeatures[i].ColumnValues["Distance"] = Dist.ToString("N1");
            //                    anchorpoints.Add(ExistingControlPointsLayer.InternalFeatures[i]);
            //                }
            //            }
            //        }
            //        catch (Exception ex) {
            //            int jjj = 0;
            //        }
            //    }
            //}
            //EditShapesLayer.Open();
            //if (EditShapesLayer.ZoomLevelSet.ZoomLevel01.CustomStyles[0].Name == "Polygon") {
            //    ((CustomAreaPolygonHaloStyle)EditShapesLayer.ZoomLevelSet.ZoomLevel01.CustomStyles[0]).AnchorPoints = anchorpoints;
            //}
        }

        //Overrides the DrawCore function.
        //DrawCore(RectangleShape targetExtent, OverlayRefreshType overlayRefreshType);
        protected override void DrawTileCore(GeoCanvas canvas) {
            Collection<SimpleCandidate> labelsInAllLayers = new Collection<SimpleCandidate>();
            EditShapesLayer.Open();
            EditShapesLayer.Draw(canvas, labelsInAllLayers);
            canvas.Flush();

            ExistingControlPointsLayer.Open();
            Collection<Feature> controlPoints = ExistingControlPointsLayer.FeatureSource.GetAllFeatures(ReturningColumnsType.AllColumns);
            bool testdraw = true;
            if (referencedfeature != null) {
                foreach (Feature feature in controlPoints) {
                    if (feature.ColumnValues["PointsSelected"] == "draw") {
                        Feature[] features = new Feature[1] { feature };
                        draggedControlPointStyle.Draw(features, canvas, labelsInAllLayers, labelsInAllLayers);
                        break;
                    }
                }
                foreach (Feature feature in controlPoints) {
                    try {
                        if (feature.ColumnValues["PointsSelected"] == "draw") { continue; }
                        Feature[] features = new Feature[1] { feature };
                        controlPointStyle.Draw(features, canvas, labelsInAllLayers, labelsInAllLayers);
                        if (feature.ColumnValues["PointsSelected"] == "anchor") {
                            PointShape pointShape = feature.GetShape() as PointShape;
                            PointShape closestPointShape = referencedfeature.GetShape().GetClosestPointTo(pointShape, GeographyUnit.DecimalDegree);
                            if (closestPointShape != null) {
                                double Dist = 0;
                                Dist = System.Math.Round(closestPointShape.GetDistanceTo(pointShape, GeographyUnit.Meter, mapDistanceUnit));
                                ScreenPointF ScreenPointF = ExtentHelper.ToScreenCoordinate(canvas.CurrentWorldExtent, pointShape, canvas.Width, canvas.Height);
                                //canvas.DrawTextWithScreenCoordinate(System.Convert.ToString(Dist), new GeoFont("Arial", 14, DrawingFontStyles.Bold), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.Black, 3), ScreenPointF.X + 35, ScreenPointF.Y, DrawingLevel.LabelLevel);
                                testdraw = false;
                                controlPointsSaved = controlPoints;
                            }
                        }
                    }
                    catch (Exception ex) {
                        int jjj = 0;
                    }
                }
            }
            else {
                foreach (Feature feature in controlPoints) {
                    Feature[] features = new Feature[1] { feature };
                    controlPointStyle.Draw(features, canvas, labelsInAllLayers, labelsInAllLayers);
                }
            }
            if (testdraw) {
                int j = 0;
                foreach (Feature feature in controlPointsSaved) {
                    if (feature.ColumnValues["PointsSelected"] == "draw") {
                        Feature[] features = new Feature[1] { feature };
                        draggedControlPointStyle.Draw(features, canvas, labelsInAllLayers, labelsInAllLayers);
                        break;
                    }
                }
                foreach (Feature feature in controlPointsSaved) {
                    try {
                        if (feature.ColumnValues["PointsSelected"] == "draw") { continue; }
                        Feature[] features = new Feature[1] { feature };
                        controlPointStyle.Draw(features, canvas, labelsInAllLayers, labelsInAllLayers);
                        if (feature.ColumnValues["PointsSelected"] == "anchor") {
                            PointShape pointShape = feature.GetShape() as PointShape;
                            PointShape closestPointShape = referencedfeature.GetShape().GetClosestPointTo(pointShape, GeographyUnit.DecimalDegree);
                            if (closestPointShape != null) {
                                double Dist = 0;
                                Dist = System.Math.Round(closestPointShape.GetDistanceTo(pointShape, GeographyUnit.Meter, mapDistanceUnit));
                                ScreenPointF ScreenPointF = ExtentHelper.ToScreenCoordinate(canvas.CurrentWorldExtent, pointShape, canvas.Width, canvas.Height);
                                //canvas.DrawTextWithScreenCoordinate(System.Convert.ToString(Dist), new GeoFont("Arial", 14, DrawingFontStyles.Bold), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.Black, 3), ScreenPointF.X + 35, ScreenPointF.Y, DrawingLevel.LabelLevel);
                                testdraw = false;
                            }
                        }
                    }
                    catch (Exception ex) {
                        int jjj = 0;
                    }
                }
            }
        }
     }
 }
