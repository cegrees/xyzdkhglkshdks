﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThinkGeo.MapSuite.Core;
using ThinkGeo.MapSuite.WpfDesktopEdition;

namespace Landdb.MapStyles.Overlays
{
    public class TrackPolygonOverlay : TrackInteractiveOverlay
    {
        const int minVertexNum = 2;
        private PointStyle controlPointStyle;
        private PointStyle draggedControlPointStyle;
        private DistanceUnit mapDistanceUnit;

        public TrackPolygonOverlay()
            : base()
        {
            this.TrackMode = TrackMode.Polygon;
            mapDistanceUnit = DistanceUnit.Feet;
        }

        public PointStyle ControlPointStyle {
            get { return controlPointStyle; }
            set { controlPointStyle = value; }
        }

        public PointStyle DraggedControlPointStyle {
            get { return draggedControlPointStyle; }
            set { draggedControlPointStyle = value; }
        }

        public DistanceUnit MapDistanceUnit
        {
            get { return mapDistanceUnit; }
            set { mapDistanceUnit = value; }
        }

        protected override InteractiveResult MouseDownCore(InteractionArguments interactionArguments)
        {
            if (interactionArguments.MouseButton != MapMouseButton.Right)
            {
                return base.MouseDownCore(interactionArguments);
            }
            else
            {
                if (this.Vertices.Count >= 5)
                {
                    RemoveLastVertexAdded();
                    MouseDownCount = MouseDownCount - 1;
                }

                return new InteractiveResult();
            }
        }
        private void RemoveLastVertexAdded()
        {
            if (this.Vertices.Count > minVertexNum)
            {
                try
                {
                    Vertex lastVertex = this.Vertices[this.Vertices.Count - 3];
                    this.Vertices.Remove(lastVertex);
                }
                finally
                {
                    //this.Lock.ExitWriteLock();
                }
            }
        }

        protected override void OnVertexAdded(VertexAddedTrackInteractiveOverlayEventArgs e) {
            base.OnVertexAdded(e);
        }

        protected override void DrawCore(RectangleShape targetExtent, OverlayRefreshType overlayRefreshType) {
            base.DrawCore(targetExtent, overlayRefreshType);
        }

        protected override void DrawTileCore(GeoCanvas canvas) {
            Collection<SimpleCandidate> labelsInAllLayers = new Collection<SimpleCandidate>();
            TrackShapeLayer.Open();
            TrackShapeLayer.Draw(canvas, labelsInAllLayers);
            canvas.Flush();

            if (Vertices.Count <= minVertexNum) { return; }

            PointShape pointshape1 = new PointShape(Vertices[Vertices.Count - 1]);
            Feature[] features1 = new Feature[1] { new Feature(pointshape1) };
            draggedControlPointStyle.Draw(features1, canvas, labelsInAllLayers, labelsInAllLayers);

            PointShape pointshape2 = new PointShape(Vertices[Vertices.Count - 2]);
            Feature[] features2 = new Feature[1] { new Feature(pointshape2) };
            draggedControlPointStyle.Draw(features2, canvas, labelsInAllLayers, labelsInAllLayers);

            PointShape pointshape3 = new PointShape(Vertices[0]);
            Feature[] features3 = new Feature[1] { new Feature(pointshape3) };
            draggedControlPointStyle.Draw(features3, canvas, labelsInAllLayers, labelsInAllLayers);

            double dist2 = System.Math.Round(pointshape1.GetDistanceTo(pointshape2, GeographyUnit.Meter, mapDistanceUnit));
            ScreenPointF ScreenPointF2 = ExtentHelper.ToScreenCoordinate(canvas.CurrentWorldExtent, pointshape2, canvas.Width, canvas.Height);
            canvas.DrawTextWithScreenCoordinate(System.Convert.ToString(dist2), new GeoFont("Arial", 14, DrawingFontStyles.Bold), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.Black, 3), ScreenPointF2.X + 35, ScreenPointF2.Y, DrawingLevel.LabelLevel);

            double dist3 = System.Math.Round(pointshape1.GetDistanceTo(pointshape3, GeographyUnit.Meter, mapDistanceUnit));
            ScreenPointF ScreenPointF3 = ExtentHelper.ToScreenCoordinate(canvas.CurrentWorldExtent, pointshape3, canvas.Width, canvas.Height);
            canvas.DrawTextWithScreenCoordinate(System.Convert.ToString(dist3), new GeoFont("Arial", 14, DrawingFontStyles.Bold), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.Black, 3), ScreenPointF3.X + 35, ScreenPointF3.Y, DrawingLevel.LabelLevel);
        }
    }
}