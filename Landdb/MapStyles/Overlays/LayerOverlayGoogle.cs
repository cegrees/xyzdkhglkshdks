﻿using System.Collections.ObjectModel;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;
using ThinkGeo.MapSuite.Core;
using ThinkGeo.MapSuite.WpfDesktopEdition;
using System;

namespace Landdb.MapStyles.Overlays {
    class LayerOverlayGoogle : LayerOverlay {


        private string leftattribution;
        public string LeftAttribution {
            get {
                return this.leftattribution;
            }
            set {
                this.leftattribution = value;
            }
        }

        private string rightattribution;
        public string RightAttribution {
            get {
                return this.rightattribution;
            }
            set {
                this.rightattribution = value;
            }
        }
        public void Draw(RectangleShape targetExtent) {
            //for(int i = 0; i < this.Layers.Count; i++) {
            //    if(this.Layers[i] is Landdb.ViewModel.Maps.ImageryOverlay.GoogleMapLayerControl) {
            //        ((Landdb.ViewModel.Maps.ImageryOverlay.GoogleMapLayerControl)this.Layers[i]).RequestOneTile = true;
            //    }
            //}
            this.Draw(targetExtent, OverlayRefreshType.Redraw);
        }

        public void Draw(RectangleShape targetExtent, OverlayRefreshType refreshType) {
            if (!this.IsVisible && refreshType != OverlayRefreshType.Pan) {
                return;
            }
            //Nxg =.Qxg = (refreshType, < CliSecureRT >.cs("ÐJ\u008eQ­Ì_oÚ_0"));
            DrawingOverlayEventArgs drawingOverlayEventArgs = new DrawingOverlayEventArgs(targetExtent, false);
            this.OnDrawing(drawingOverlayEventArgs);
            if (drawingOverlayEventArgs.Cancel) {
                this.OnDrawn(new DrawnOverlayEventArgs(targetExtent));
                return;
            }
            this.DrawCore(targetExtent, refreshType);
            this.PreviousExtent = targetExtent;
            //this.OverlayCanvas.Dispatcher.BeginInvoke(new Action(this.xhQ =), DispatcherPriority.Background, new object[0]);
        }

        protected override void DrawCore(RectangleShape targetExtent, OverlayRefreshType overlayRefreshType) {
            for (int i = 0; i < this.Layers.Count; i++) {
                if (this.Layers[i] is Landdb.ViewModel.Maps.ImageryOverlay.GoogleMapLayerControl) {
                    ((Landdb.ViewModel.Maps.ImageryOverlay.GoogleMapLayerControl)this.Layers[i]).RequestOneTile = true;
                }
            }
            base.DrawCore(targetExtent, overlayRefreshType);
        }

        protected override void DrawAttributionCore(GeoCanvas canvas) {
            int zoomLevelsNumber = tD4(canvas);
            if (string.IsNullOrEmpty(this.leftattribution) || this.leftattribution == "Google") {
                this.leftattribution = "Google";
            }
            if (string.IsNullOrEmpty(this.rightattribution) || this.leftattribution == "Google") {
                this.rightattribution = LoadAttribution(zoomLevelsNumber);
            }
            //Catull
           //Libre Baskerville
           //Baskerville
           GeoFont fontleft = new GeoFont("Times New Roman", 15f);
            DrawingRectangleF drawingRectangleFLeft = canvas.MeasureText(this.LeftAttribution, fontleft);
            //canvas.DrawTextWithScreenCoordinate(this.LeftAttribution, fontleft, new GeoSolidBrush(GeoColor.SimpleColors.White), new GeoPen(GeoColor.SimpleColors.Black, 2f), 45, (canvas.Height - drawingRectangleFLeft.Height / 2f) - 20, DrawingLevel.LabelLevel);
            canvas.DrawTextWithScreenCoordinate(this.LeftAttribution, fontleft, new GeoSolidBrush(GeoColor.SimpleColors.Black), new GeoPen(GeoColor.SimpleColors.White, 2f), 45, (canvas.Height - drawingRectangleFLeft.Height / 2f) - 20, DrawingLevel.LabelLevel);

            GeoFont fontright = new GeoFont("Arial", 7.5f);
            DrawingRectangleF drawingRectangleFRight = canvas.MeasureText(this.RightAttribution, fontright);
            canvas.DrawTextWithScreenCoordinate(this.RightAttribution, fontright, new GeoSolidBrush(GeoColor.SimpleColors.Black), new GeoPen(GeoColor.SimpleColors.White, 2f), canvas.Width - drawingRectangleFRight.Width / 2f, canvas.Height - drawingRectangleFRight.Height / 2f, DrawingLevel.LabelLevel);
        }

        private static int tD4(GeoCanvas canvas) {
            int num = Bj8((double)canvas.Width, canvas.CurrentWorldExtent, canvas.MapUnit, canvas.Dpi);
            if (num >= 20) {
                num = 19;
            }
            else if (num == 0) {
                num = 1;
            }
            return num;
        }

        internal static int Bj8(double newWidth, RectangleShape newTileExtent, GeographyUnit mapUnit, float dpi) {
            OpenStreetMapsZoomLevelSet openStreetMapsZoomLevelSet = new OpenStreetMapsZoomLevelSet();
            ZoomLevel zoomLevel = openStreetMapsZoomLevelSet.GetZoomLevel(newTileExtent, newWidth, mapUnit, dpi);
            System.Collections.ObjectModel.Collection<ZoomLevel> zoomLevels = openStreetMapsZoomLevelSet.GetZoomLevels();
            int result = 0;
            for (int i = 0; i < zoomLevels.Count; i++) {
                if (zoomLevel.Scale == zoomLevels[i].Scale) {
                    result = i;
                    break;
                }
            }
            return result;
        }


        private string LoadAttribution(int zoomLevelsNumber) {
            string strattribution = string.Format("Map data ©{0} Google Imagery ©{0}", DateTime.Today.Year);
            switch (zoomLevelsNumber) {
                case 21:
                    strattribution = string.Format("Map data ©{0} Google Imagery ©{0}", DateTime.Today.Year);
                    break;
                case 20:
                    strattribution = string.Format("Map data ©{0} Google Imagery ©{0}", DateTime.Today.Year);
                    break;
                case 19:
                    strattribution = string.Format("Map data ©{0} Google Imagery ©{0}, DigitalGlobe", DateTime.Today.Year);
                    break;
                case 18:
                    strattribution = string.Format("Map data ©{0} Google Imagery ©{0}, DigitalGlobe", DateTime.Today.Year);
                    break;
                case 17:
                    strattribution = string.Format("Map data ©{0} Google Imagery ©{0}, DigitalGlobe, USDA Farm Service Agency", DateTime.Today.Year);
                    break;
                case 16:
                    strattribution = string.Format("Map data ©{0} Google Imagery ©{0}, DigitalGlobe, USDA Farm Service Agency", DateTime.Today.Year);
                    break;
                case 15:
                    strattribution = string.Format("Map data ©{0} Google Imagery ©{0}, DigitalGlobe, USDA Farm Service Agency", DateTime.Today.Year);
                    break;
                case 14:
                    strattribution = string.Format("Map data ©{0} Google Imagery ©{0}, DigitalGlobe, Landsat, USDA Farm Service Agency", DateTime.Today.Year);
                    break;
                case 13:
                    strattribution = string.Format("Map data ©{0} Google Imagery ©{0}, DigitalGlobe, Landsat, USDA Farm Service Agency", DateTime.Today.Year);
                    break;
                case 12:
                    strattribution = string.Format("Map data ©{0} Google Imagery ©{0}, TerraMetrics", DateTime.Today.Year);
                    break;
                case 11:
                    strattribution = string.Format("Map data ©{0} Google Imagery ©{0}, TerraMetrics", DateTime.Today.Year);
                    break;
                case 10:
                    strattribution = string.Format("Map data ©{0} Google Imagery ©{0}, TerraMetrics", DateTime.Today.Year);
                    break;
                case 9:
                    strattribution = string.Format("Map data ©{0} Google Imagery ©{0}, TerraMetrics", DateTime.Today.Year);
                    break;
                case 8:
                    strattribution = string.Format("Map data ©{0} Google Imagery ©{0}, TerraMetrics", DateTime.Today.Year);
                    break;
                case 7:
                    strattribution = string.Format("Map data ©{0} Google Imagery ©{0}, TerraMetrics", DateTime.Today.Year);
                    break;
                case 6:
                    strattribution = string.Format("Map data ©{0} Google Imagery ©{0}, TerraMetrics", DateTime.Today.Year);
                    break;
                case 5:
                    strattribution = string.Format("Map data ©{0} Google Imagery ©{0}, TerraMetrics", DateTime.Today.Year);
                    break;
                case 4:
                    strattribution = string.Format("Map data ©{0} INEGI Imagery ©{0}, TerraMetrics", DateTime.Today.Year);
                    break;
                case 3:
                    strattribution = string.Format("Map data ©{0} INEGI Imagery ©{0}, TerraMetrics", DateTime.Today.Year);
                    break;
                case 2:
                    strattribution = string.Format("Map data ©{0} Imagery ©{0} NASA", DateTime.Today.Year);
                    break;
                case 1:
                    strattribution = string.Format("Map data ©{0} Imagery ©{0} NASA", DateTime.Today.Year);
                    break;
                case 0:
                    strattribution = string.Format("Map data ©{0} Imagery ©{0}", DateTime.Today.Year);
                    break;
                default:
                    strattribution = string.Format("Map data ©{0} Google Imagery ©{0}", DateTime.Today.Year);
                    break;
            }
            return strattribution;
        }

    }
}
