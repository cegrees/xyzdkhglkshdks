﻿using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using ThinkGeo.MapSuite.Core;
using ThinkGeo.MapSuite.WpfDesktopEdition;

namespace Landdb.MapStyles.Overlays
{
    
    class RullerTrackInteractiveOverlay : TrackInteractiveOverlay
    {
        private const string currentFeatureKey = "CurrentFeature";
        private LineShape rulerLineShape;
       private int mouseDown;

        public RullerTrackInteractiveOverlay()
            : base()
        {
            TrackShapeLayer.Open();
            TrackShapeLayer.Columns.Add(new FeatureSourceColumn("length"));
            //Sets the appearance of the ruler
            TrackShapeLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = LineStyles.CreateSimpleLineStyle(GeoColor.FromArgb(150, GeoColor.StandardColors.DarkRed), 4,false);
            TrackShapeLayer.ZoomLevelSet.ZoomLevel01.DefaultTextStyle = new TextStyle("length", new GeoFont("Arial", 10, DrawingFontStyles.Bold), new GeoSolidBrush(GeoColor.SimpleColors.Black));
            TrackShapeLayer.ZoomLevelSet.ZoomLevel01.DefaultTextStyle.TextLineSegmentRatio = 100;
            TrackShapeLayer.ZoomLevelSet.ZoomLevel01.DefaultTextStyle.YOffsetInPixel = 10;
            TrackShapeLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
        }

        protected override InteractiveResult MouseDownCore(InteractionArguments interactionArguments)
        {
            //Sets the first and last point of the line where the user clicks.
            InteractiveResult interactiveResult = new InteractiveResult();
            interactiveResult.DrawThisOverlay = InteractiveOverlayDrawType.Draw;
            interactiveResult.ProcessOtherOverlaysMode  = ProcessOtherOverlaysMode.DoNotProcessOtherOverlays;

            rulerLineShape = new LineShape(new Collection<Vertex>() { new Vertex(interactionArguments.WorldX, interactionArguments.WorldY), new Vertex(interactionArguments.WorldX, interactionArguments.WorldY) });

            TrackShapeLayer.InternalFeatures.Add(currentFeatureKey, new Feature(rulerLineShape));
            
            mouseDown++;
            return interactiveResult;
        }

        protected override InteractiveResult MouseMoveCore(InteractionArguments interactionArguments)
        {
            //Updates the line with the last point to where the mouse pointer is being dragged.
            InteractiveResult interactiveResult = new InteractiveResult();

            if (mouseDown > 0)
            {
                //Lock.EnterWriteLock();
                rulerLineShape.Vertices[rulerLineShape.Vertices.Count - 1] = new Vertex(interactionArguments.WorldX, interactionArguments.WorldY);
                //Lock.ExitWriteLock();

                interactiveResult.DrawThisOverlay = InteractiveOverlayDrawType.Draw;
                interactiveResult.ProcessOtherOverlaysMode = ProcessOtherOverlaysMode.DoNotProcessOtherOverlays;
            }

            return interactiveResult;
        }

        protected override InteractiveResult MouseUpCore(InteractionArguments interactionArguments)
        {
            //Removes the line of the ruler at finishing dragging (at mouse up event).
            InteractiveResult interactiveResult = new InteractiveResult();

            interactiveResult.DrawThisOverlay = InteractiveOverlayDrawType.Draw;
            interactiveResult.ProcessOtherOverlaysMode = ProcessOtherOverlaysMode.DoNotProcessOtherOverlays;

            if (mouseDown == 1) 
            {
                mouseDown = 0;
                //Lock.EnterWriteLock();
                TrackShapeLayer.InternalFeatures.Remove(currentFeatureKey);
                //Lock.ExitWriteLock();
            }
            return interactiveResult;
        }

        protected override void DrawTileCore(GeoCanvas canvas)
        {
            //Draws the line and update the distance text of the ruler.
            Collection<SimpleCandidate> labelingInAllLayers = new Collection<SimpleCandidate>();

            try
            {
                if (rulerLineShape != null)
                {
                    Feature feature = new Feature(rulerLineShape);
                    double length = rulerLineShape.GetLength(GeographyUnit.DecimalDegree, DistanceUnit.Feet);
                    feature.ColumnValues.Add("length", ((int)length).ToString() + " feet");
                    
                    //Lock.EnterWriteLock();
                    {
                        if (TrackShapeLayer.InternalFeatures.Contains(currentFeatureKey))
                        {
                            TrackShapeLayer.InternalFeatures[currentFeatureKey] = feature;
                        }
                        else
                        {
                            TrackShapeLayer.InternalFeatures.Add(currentFeatureKey, feature);
                        }
                    }
                    //Lock.ExitWriteLock();
                }

                TrackShapeLayer.Open();
                TrackShapeLayer.Draw(canvas, labelingInAllLayers);
                canvas.Flush();
            }
            finally
            {
                TrackShapeLayer.Close();
            }
        }
    }
}