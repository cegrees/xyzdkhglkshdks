﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using ThinkGeo.MapSuite.Core;
using ThinkGeo.MapSuite.WpfDesktopEdition;

namespace Landdb.MapStyles.Overlays {
    public class MeasureTrackInteractiveOverlay : TrackInteractiveOverlay {
        private AreaStyle areaStyle;
        private LineStyle lineStyle;
        private PointStyle pointStyle;
        private TextBlock textBlock;
        private DateTime begin = DateTime.UtcNow;
        private DateTime end = DateTime.UtcNow;
        private DateTime last = DateTime.UtcNow;
        private DateTime lastbegin = DateTime.UtcNow;
        private Stopwatch sw = new Stopwatch();
        private Stopwatch sw2 = new Stopwatch();
        private Stopwatch sw3 = new Stopwatch();
        private Stopwatch sw4 = new Stopwatch();
        private InteractiveResult saveinteractiveresults;
        private bool gotresults = false;
        private int intresults = 0;

        public MeasureTrackInteractiveOverlay()
            : this(DistanceUnit.Meter, GeographyUnit.DecimalDegree) {
        }

        public MeasureTrackInteractiveOverlay(DistanceUnit distanceUnit, GeographyUnit mapUnit) {
            DistanceUnit = distanceUnit;
            MapUnit = mapUnit;

            textBlock = new TextBlock();
            textBlock.Visibility = Visibility.Collapsed;
            OverlayCanvas.Children.Add(textBlock);

            PolygonTrackMode = PolygonTrackMode.LineWithFill;
            RenderMode = RenderMode.DrawingVisual;

            areaStyle = AreaStyles.CreateSimpleAreaStyle(GeoColor.FromArgb(80, GeoColor.SimpleColors.LightRed), GeoColor.SimpleColors.Black, 2);
            pointStyle = PointStyles.CreateSimpleCircleStyle(GeoColor.SimpleColors.Yellow, 8);
            lineStyle = LineStyles.CreateSimpleLineStyle(GeoColor.SimpleColors.Green, 3, true);

            SetStylesForInMemoryFeatureLayer(TrackShapeLayer);
            SetStylesForInMemoryFeatureLayer(TrackShapesInProcessLayer);
            RenderMode = ThinkGeo.MapSuite.WpfDesktopEdition.RenderMode.GdiPlus;

            saveinteractiveresults = new InteractiveResult();
        }

        public DistanceUnit DistanceUnit { get; set; }

        public GeographyUnit MapUnit { get; set; }

        protected override RectangleShape GetBoundingBoxCore() {
            return ExtentHelper.GetBoundingBoxOfItems(TrackShapeLayer.InternalFeatures);
        }

        protected override InteractiveResult MouseMoveCore(InteractionArguments interactionArguments) {
            if (sw.IsRunning) {
                sw.Stop();
            }
            sw2 = new Stopwatch();
            sw2.Start();
            DateTime start = DateTime.Now;
            bool calculated = false;
            if (gotresults) {
                InteractiveResult arguments = base.MouseMoveCore(interactionArguments);
                saveinteractiveresults = arguments;
                gotresults = false;
                calculated = true;
                intresults = 0;
            }
            else {
                calculated = false;
                intresults++;
            }

            if (Vertices.Count == 0) {
                textBlock.Visibility = Visibility.Collapsed;
            }

            Canvas.SetLeft(textBlock, interactionArguments.ScreenX + 10);
            Canvas.SetTop(textBlock, interactionArguments.ScreenY + 10);
            begin = DateTime.UtcNow;
            if (sw2.IsRunning) {
                sw2.Stop();
            }
            if (Vertices.Count > 0) {
                //double duration = begin.Subtract(start).TotalMilliseconds;
                double duration = sw2.ElapsedMilliseconds;
                double lasttime = begin.Subtract(lastbegin).TotalMilliseconds;
                //double duration2 = start.Subtract(lastbegin).TotalMilliseconds;
                double duration2 = sw.ElapsedMilliseconds;
                //Console.WriteLine("OVR total {0} {1}  since last {2} {3}   calc {4} {5}", lasttime, "ms", duration2, "ms", duration, "ms");
                if (lasttime < 200) { gotresults = true; }
            }
            if (intresults > 2) { gotresults = true; }
            lastbegin = DateTime.UtcNow;
            sw = new Stopwatch();
            sw.Start();
            return saveinteractiveresults;
        }

        protected override InteractiveResult MouseDownCore(InteractionArguments interactionArguments) {
            gotresults = true;
            InteractiveResult arguments = base.MouseDownCore(interactionArguments);
            return arguments;
        }

        protected override void OnDrawing(DrawingOverlayEventArgs e) {
            //sw3 = new Stopwatch();
            //sw3.Start();
            //DateTime start = DateTime.Now;
            //MeasureRadius();
            base.OnDrawing(e);
            //if (sw3.IsRunning) {
            //    sw3.Stop();
            //}
            ////double duration = DateTime.UtcNow.Subtract(start).TotalMilliseconds;
            //double duration = sw3.ElapsedMilliseconds;
            //double lasttime = DateTime.UtcNow.Subtract(lastbegin).TotalMilliseconds;
            //Console.WriteLine("OVR since moved {0} {1}   drawing {2} {3}", lasttime, "ms", duration, "ms");

            //gotresults = true;
            //gotresults = false;

        }

        protected override void OnDrawn(DrawnOverlayEventArgs e) {
            //sw4 = new Stopwatch();
            //sw4.Start();
            //DateTime start = DateTime.Now;
            base.OnDrawn(e);
            //if (sw4.IsRunning) {
            //    sw4.Stop();
            //}
            ////double duration = DateTime.UtcNow.Subtract(start).TotalMilliseconds;
            //double duration = sw4.ElapsedMilliseconds;
            //double lasttime = DateTime.UtcNow.Subtract(lastbegin).TotalMilliseconds;
            //Console.WriteLine("Render since moved {0} {1}   drawing {2} {3}", lasttime, "ms", duration, "ms");

            //gotresults = true;
            //gotresults = false;
        }

        protected override void OnTrackEnded(TrackEndedTrackInteractiveOverlayEventArgs e) {
            textBlock.Visibility = Visibility.Collapsed;
            base.OnTrackEnded(e);
            gotresults = false;
        }

        protected override void OnTrackStarting(TrackStartingTrackInteractiveOverlayEventArgs e) {
            if (TrackMode == TrackMode.Line || TrackMode == TrackMode.Polygon) {
                textBlock.Visibility = Visibility.Visible;
            }
            base.OnTrackStarting(e);
            gotresults = true;
        }

        private static string GetAbbreviateDistanceUnit(DistanceUnit unit) {
            switch (unit) {
                case DistanceUnit.Mile:
                    return "mi";

                case DistanceUnit.Kilometer:
                    return "km";

                case DistanceUnit.Meter:
                    return "m";

                case DistanceUnit.Inch:
                    return "in";

                default:
                    return ".";
            }
        }

        private void MeasureRadius() {
            if (TrackMode == TrackMode.Line) {
                double length = 0;
                foreach (var f in TrackShapesInProcessLayer.InternalFeatures) {
                    if (f.GetShape() is LineBaseShape) {
                        length += (f.GetShape() as LineBaseShape).GetLength(MapUnit, DistanceUnit);
                    }
                }
                textBlock.Text = string.Format(CultureInfo.InvariantCulture, "{0:N3} {1}", length, GetAbbreviateDistanceUnit(DistanceUnit));
            }
            else if (TrackMode == TrackMode.Polygon) {
                double area = 0;
                foreach (var f in TrackShapesInProcessLayer.InternalFeatures) {
                    if (f.GetShape() is AreaBaseShape) {
                        area += (f.GetShape() as AreaBaseShape).GetArea(MapUnit, AreaUnit.Acres);
                    }
                }
                textBlock.Text = string.Format(CultureInfo.InvariantCulture, "{0:N3} {1}", area, "acre");
            }
        }

        private void SetStylesForInMemoryFeatureLayer(FeatureLayer featureLayer) {
            featureLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = areaStyle;
            featureLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = lineStyle;
            featureLayer.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = pointStyle;
            featureLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
        }
    }
}