﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Landdb.ViewModel.Secondary.Map;
using ThinkGeo.MapSuite.Core;

namespace Landdb.MapStyles
{
    public class CustomAreaLineStyle : LineStyle
    {
        private GeoFont geoFont;
        private DistanceUnit distanceUnit;
        private AreaUnit areaUnit;
        private GeoSolidBrush geoSolidBrusharea;
        private GeoSolidBrush geoSolidBrushtext;
        private GeoPen geoPenline;
        private GeoPen geoPenarea;
        private GeoPen geoPentext;
        AreaBaseShape selectedfield;
        Collection<Feature> czBoundaries = new Collection<Feature>();
        //Proj4Projection projection = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetGoogleMapParametersString());

        public CustomAreaLineStyle()
            : this(DistanceUnit.Meter, AreaUnit.SquareMeters, new GeoFont(), new GeoPen(), new GeoPen(), new GeoPen(), new GeoSolidBrush(), new GeoSolidBrush(), new MultipolygonShape())
        {}

        public CustomAreaLineStyle(DistanceUnit distanceUnit, AreaUnit areaUnit, GeoFont geoFont, GeoPen geoPenline, GeoPen geoPenarea, GeoPen geoPentext, GeoSolidBrush geoSolidBrusharea, GeoSolidBrush geoSolidBrushtext, AreaBaseShape areabaseshape)
        {
            this.distanceUnit = distanceUnit;
            this.areaUnit = areaUnit;
            this.geoFont = geoFont;
            this.geoPenline = geoPenline;
            this.geoPenarea = geoPenarea;
            this.geoPentext = geoPentext;
            this.geoSolidBrusharea = geoSolidBrusharea;
            this.geoSolidBrushtext = geoSolidBrushtext;
            this.selectedfield = areabaseshape;
        }

        //For the font of the distance
        public GeoFont GeoFont
        {
            get { return geoFont; }
            set { geoFont = value; }
        }

        //For the color of the halo of the font
        public GeoPen GeoPenline
        {
            get { return geoPenline; }
            set { geoPenline = value; }
        }

        //For the color of the halo of the font
        public GeoPen GeoPenarea {
            get { return geoPenarea; }
            set { geoPenarea = value; }
        }

        //For the color of the halo of the font
        public GeoPen GeoPentext {
            get { return geoPentext; }
            set { geoPentext = value; }
        }

        //For the distance unit used.
        public DistanceUnit DistanceUnit
        {
            get { return distanceUnit; }
            set { distanceUnit = value; }
        }

        //For the distance unit used.
        public AreaUnit AreaUnit {
            get { return areaUnit; }
            set { areaUnit = value; }
        }

        public GeoSolidBrush GeoSolidBrusharea {
            get { return geoSolidBrusharea; }
            set { geoSolidBrusharea = value; }
        }

        //For the color of the font
        public GeoSolidBrush GeoSolidBrushtext {
            get { return geoSolidBrushtext; }
            set { geoSolidBrushtext = value; }
        }

        public AreaBaseShape SelectedField {
            get { return selectedfield; }
            set { selectedfield = value; }
        }

        public Collection<Feature> CZBoundaries {
            get { return czBoundaries; }
            set { czBoundaries = value; }
        }


        protected override void DrawCore(System.Collections.Generic.IEnumerable<Feature> features, GeoCanvas canvas, System.Collections.ObjectModel.Collection<SimpleCandidate> labelsInThisLayer, System.Collections.ObjectModel.Collection<SimpleCandidate> labelsInAllLayers)
        {
            //if (!projection.IsOpen) {
            //    projection.Open();
            //}
            czBoundaries = new Collection<Feature>();
            //Collection<Feature> czlines = new Collection<Feature>();

            //foreach (Feature f1 in features) {
            //    BaseShape bs = f1.GetShape();
            //    BaseShape bs2 = projection.ConvertToInternalProjection(bs);
            //    Feature feat1 = new Feature(bs2);
            //    czlines.Add(feat1);

            //}

            try {
                IList<Feature> SplitFeatures = SplitPolgonsWithLines(features, selectedfield);
                foreach (Feature feature in SplitFeatures) {
                    czBoundaries.Add(feature);
                    //canvas.DrawArea(feature, geoPenarea, geoSolidBrusharea, DrawingLevel.LevelOne);
                    float angle = 0;
                    AreaBaseShape abs = feature.GetShape() as AreaBaseShape;
                    double featurearea = abs.GetArea(canvas.MapUnit, areaUnit);
                    PointShape midPointShape = abs.GetCenterPoint();
                    Collection<ScreenPointF> screenPointFs = new Collection<ScreenPointF>();

                    screenPointFs.Add(ExtentHelper.ToScreenCoordinate(canvas.CurrentWorldExtent, midPointShape, canvas.Width, canvas.Height));
                    canvas.DrawText(featurearea.ToString("N2"), geoFont, geoSolidBrushtext, geoPentext, screenPointFs, DrawingLevel.LabelLevel, 0, 0, angle);
                }
                foreach (Feature feature in features) {
                    canvas.DrawLine(feature, geoPenline, DrawingLevel.LevelTwo);
                }
            }
            catch (Exception ex) {
                int xxx = 0;
            }
        }

        private double GetAngleFromTwoVertices(Vertex b, Vertex c)
        {
            double result;
            double alpha = 0;
            double tangentAlpha = (c.Y - b.Y) / (c.X - b.X);
            double Peta = Math.Atan(tangentAlpha);

            if (c.X > b.X)
            {
                alpha = 90 + (Peta * (180 / Math.PI));
            }
            else if (c.X < b.X)
            {
                alpha = 270 + (Peta * (180 / Math.PI));
            }
            else
            {
                if (c.Y > b.Y) alpha = 0;
                if (c.Y < b.Y) alpha = 180;
            }

            double offset;
            if (b.X > c.X)
            { offset = 90; }
            else { offset = - 90; }

            result = alpha + offset;
            return result;   
        }

        private IList<Feature> SplitPolgonsWithLines(System.Collections.Generic.IEnumerable<Feature> features, AreaBaseShape areabaseshape) {
            IList<Feature> layerfeatures = new List<Feature>();
            IList<LineShape> lines = new List<LineShape>();

            foreach (Feature feature in features) {
                BaseShape baseshape = feature.GetShape();
                if (baseshape is LineShape) {
                    LineShape lineshape = baseshape as LineShape;
                    if (lineshape.Crosses(areabaseshape)) {
                        lines.Add(lineshape);
                    }
                }
            }
            BaseShape baseshape2 = areabaseshape;
            MultipolygonShape mps2 = new MultipolygonShape();
            if (baseshape2 is MultipolygonShape) {
                mps2 = baseshape2 as MultipolygonShape;
            }
            else if (baseshape2 is PolygonShape) {
                mps2.Polygons.Add(baseshape2 as PolygonShape);
            }
            MultipolygonShape mps1 = Splitter.Split(mps2, lines);
            foreach (PolygonShape poly4 in mps1.Polygons) {
                MultipolygonShape mps4 = new MultipolygonShape();
                mps4.Polygons.Add(poly4);
                Feature feature4 = new Feature(mps4);
                layerfeatures.Add(feature4);
            }
            return layerfeatures;
        }
    }
}
