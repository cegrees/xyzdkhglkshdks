﻿using NLog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using ThinkGeo.MapSuite.Core;

namespace Landdb.MapStyles {
    // Mark the class Serializable so that it works in SQL Server state
    // and in every serialization context
    [Serializable]
    class ClientLayerAreaStyle : AreaStyle {
        private AreaStyle selectedAreaStyle;
        private AreaStyle unselectedAreaStyle;
        private AreaStyle transparentAreaStyle;
        private TextStyle textStyle = null;
        private IList<Feature> selectedFeatures;
        private IList<Feature> hiddenFeatures;
        private bool unselectedfieldvisibility = true;
        Logger log = LogManager.GetCurrentClassLogger();

        public ClientLayerAreaStyle()
            : this(new AreaStyle(), new AreaStyle(), new TextStyle()) { }

        public ClientLayerAreaStyle(AreaStyle selectedAreaStyle, AreaStyle unselectedAreaStyle, TextStyle textStyle) {
            this.selectedAreaStyle = selectedAreaStyle;
            this.unselectedAreaStyle = unselectedAreaStyle;
            this.textStyle = textStyle;
            this.transparentAreaStyle = new AreaStyle(new GeoPen(GeoColor.StandardColors.Transparent, 2), new GeoSolidBrush(new GeoColor(0, GeoColor.StandardColors.Transparent)));
            selectedFeatures = new List<Feature>();
            hiddenFeatures = new List<Feature>();
        }

        public AreaStyle SelectedAreaStyle {
            get { return selectedAreaStyle; }
            set { selectedAreaStyle = value; }
        }

        public AreaStyle UnselectedAreaStyle {
            get { return unselectedAreaStyle; }
            set { unselectedAreaStyle = value; }
        }

        public bool UnselectedFieldVisibility {
            get { return unselectedfieldvisibility; }
            set { unselectedfieldvisibility = value; }
        }

        public TextStyle TextStyle {
            get { return textStyle; }
            set { textStyle = value; }
        }

        public IList<Feature> SelectedFeatures {
            get { return selectedFeatures; }
            set { selectedFeatures = value; }
        }

        public IList<Feature> HiddenFeatures {
            get { return hiddenFeatures; }
            set { hiddenFeatures = value; }
        }

        protected override void DrawCore(IEnumerable<Feature> features, GeoCanvas canvas, Collection<SimpleCandidate> labelsInThisLayer, Collection<SimpleCandidate> labelsInAllLayers) {
            foreach (Feature feature in features) {
                try {
                    string featureid = feature.Id.ToString();
                    bool hidefeature = false;
                    foreach (Feature feat in HiddenFeatures) {
                        string featid = feat.Id.ToString();
                        if (featureid == featid) {
                            hidefeature = true;
                            break;
                        }
                    }
                    bool foundfeature = false;
                    foreach (Feature feat in SelectedFeatures) {
                        string featid = feat.Id.ToString();
                        if (featureid == featid) {
                            foundfeature = true;
                            break;
                        }
                    }
                    if (hidefeature) {
                        transparentAreaStyle.Draw(new Collection<Feature>() { feature }, canvas, labelsInThisLayer, labelsInAllLayers);
                    } else if (foundfeature) {
                        selectedAreaStyle.Draw(new Collection<Feature>() { feature }, canvas, labelsInThisLayer, labelsInAllLayers);
                    } else {
                        if (unselectedfieldvisibility) {
                            unselectedAreaStyle.Draw(new Collection<Feature>() { feature }, canvas, labelsInThisLayer, labelsInAllLayers);
                            //if (textStyle != null) {
                            //textStyle.Draw(new Collection<Feature>() { feature }, canvas, labelsInThisLayer, labelsInAllLayers);
                            //}
                        }
                    }
                } catch (Exception ex) {
                    log.WarnException($"Exception in {nameof(DrawCore)}", ex);
                }
            }
        }

        protected override Collection<string> GetRequiredColumnNamesCore() {
            return base.GetRequiredColumnNamesCore();
        }
    }
}