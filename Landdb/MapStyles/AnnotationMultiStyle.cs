﻿using NLog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using ThinkGeo.MapSuite.Core;

namespace Landdb.MapStyles
{

    [Serializable]
    class AnnotationMultiStyle : Style
    {
        private Style selectedStyle;
        private Style unselectedStyle;
        private Style unselectedAreaStyle;
        private Style unselectedLineStyle;
        private Style unselectedPointStyle;
        private List<string> columnslist;
        private int opacity = 255;
        IStyleFactory styleFactory;
        private double maximumScale;
        private double minimumScale;
        private double mapScale;
        private float maximumLabelSize;
        private float minimumLabelSize;
        Logger log = LogManager.GetCurrentClassLogger();

        public AnnotationMultiStyle()
            : this(new AreaStyle(), new LineStyle(), new PointStyle(), 255)
        {

        }

        public AnnotationMultiStyle(Style unselectedAreaStyle, Style unselectedLineStyle, Style unselectedPointStyle, int opacity)
        {
            styleFactory = new StyleFactory();
            this.selectedStyle = unselectedAreaStyle;
            this.unselectedStyle = unselectedAreaStyle;
            this.unselectedAreaStyle = unselectedAreaStyle;
            this.unselectedLineStyle = unselectedAreaStyle;
            this.unselectedPointStyle = unselectedAreaStyle;
            this.opacity = opacity;
            maximumLabelSize = 20;
            minimumLabelSize = 1;
            maximumScale = 19000;
            minimumScale = 1200;
        }

        public Style UnselectedAreaStyle {
            get { return unselectedAreaStyle; }
            set {
                if (unselectedAreaStyle != value) { return; }
                unselectedAreaStyle = value;
            }
        }

        public Style UnselectedLineStyle {
            get { return unselectedLineStyle; }
            set {
                if (unselectedLineStyle != value) { return; }
                unselectedLineStyle = value;
            }
        }

        public Style UnselectedPointStyle {
            get { return unselectedPointStyle; }
            set {
                if (unselectedPointStyle != value) { return; }
                unselectedPointStyle = value;
            }
        }
        public double MaximumScale
        {
            get { return maximumScale; }
            set { maximumScale = value; }
        }

        public double MinimumScale
        {
            get { return minimumScale; }
            set { minimumScale = value; }
        }

        public double MapScale
        {
            get { return mapScale; }
            set { mapScale = value; }
        }

        // The MaximumSize is the size of the label at MinimumScale and lower.
        public float MaximumLabelSize
        {
            get { return maximumLabelSize; }
            set { maximumLabelSize = value; }
        }

        // The MinimumSize is the size of the label at MaximumScale and higher.
        public float MinimumLabelSize
        {
            get { return minimumLabelSize; }
            set { minimumLabelSize = value; }
        }

        public int Opacity {
            get { return opacity; }
            set {
                if (opacity != value) { return; }
                this.opacity = value;
            }
        }

        public List<string> ColumnsList {
            get { return columnslist; }
            set { columnslist = value; }
        }

        protected override void DrawCore(IEnumerable<Feature> features, GeoCanvas canvas, Collection<SimpleCandidate> labelsInThisLayer, Collection<SimpleCandidate> labelsInAllLayers)
        {
            foreach (Feature feature in features)
            {
                selectedStyle = unselectedAreaStyle;
                unselectedStyle = unselectedAreaStyle;
                BaseShape baseshape = feature.GetShape();
                string shapetype = "Polygon";
                if (baseshape is LineShape) {
                    selectedStyle = unselectedLineStyle;
                    unselectedStyle = unselectedLineStyle;
                    shapetype = "Line";
                }
                if (baseshape is PointShape) {
                    selectedStyle = unselectedPointStyle;
                    unselectedStyle = unselectedPointStyle;
                    shapetype = "Point";
                }
                try {
                    string annotationtype = string.Empty;
                    try {
                        annotationtype = feature.ColumnValues[@"TypeName"];
                    }
                    catch { }
                    feature.GetShape();
                    bool foundfeature = false;

                    AnnotationStyleItem annotatonstyle = styleFactory.GetStyle(annotationtype, shapetype);
                    if (shapetype == "Point") {
                        if (annotatonstyle.AnnotationStyle is PointStyle && ((PointStyle)annotatonstyle.AnnotationStyle).Image == null) {
                            float labelSize = (float)((PointStyle)annotatonstyle.AnnotationStyle).SymbolSize;
                            double currentScale = this.mapScale;
                            if (currentScale > maximumScale) { currentScale = maximumScale; }
                            if (currentScale < minimumScale) { currentScale = minimumScale; }

                            double diff1 = currentScale - minimumScale;
                            double range1 = maximumScale - minimumScale;
                            double percent1 = diff1 / range1;
                            double inverse1 = 1 - percent1;
                            double frange1 = maximumLabelSize - minimumLabelSize;
                            double fscale1 = inverse1 * frange1;
                            double fontsize1 = fscale1 + minimumLabelSize;
                            labelSize = (float)fontsize1;
                            ((PointStyle)annotatonstyle.AnnotationStyle).SymbolSize = labelSize;
                        }
                    }

                    if (annotatonstyle != null) {
                        if (annotatonstyle.Visible) {
                            selectedStyle = annotatonstyle.AnnotationStyle;
                        }
                        else {
                            selectedStyle = unselectedStyle;
                        }
                        foundfeature = true;
                    }
                    if (foundfeature) {
                        styleFactory.SetStyleOpacity(selectedStyle, opacity).Draw(new Collection<Feature>() { feature }, canvas, labelsInThisLayer, labelsInAllLayers);
                    }
                    else {
                        styleFactory.SetStyleOpacity(unselectedStyle, opacity).Draw(new Collection<Feature>() { feature }, canvas, labelsInThisLayer, labelsInAllLayers);
                    }
                }
                catch (Exception ex) {
                    log.WarnException($"Exception occurred in {nameof(DrawCore)}", ex);
                }
            }
        }

        protected override System.Collections.ObjectModel.Collection<string> GetRequiredColumnNamesCore() {
            Collection<string> columns = new Collection<string>();
            foreach (string col in columnslist) {
                if (!columns.Contains(col)) {
                    columns.Add(col);
                }
            }
            return columns;
        }
    }
}

