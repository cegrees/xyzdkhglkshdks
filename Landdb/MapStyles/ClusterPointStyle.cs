﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using ThinkGeo.MapSuite.Core;

namespace Landdb.MapStyles {
    public class ClusterPointStyle : PointStyle {
        private int cellSize = 100;
        private TextStyle textSytle = new TextStyle();
        Dictionary<string, IList<Feature>> clusterdictionary = new Dictionary<string, IList<Feature>>();

        public ClusterPointStyle()
            : base() { }

        public ClusterPointStyle(GeoImage image)
            : base(image) { }

        public ClusterPointStyle(GeoFont characterFont, int characterIndex, GeoSolidBrush characterSolidBrush)
            : base(characterFont, characterIndex, characterSolidBrush) { }

        public ClusterPointStyle(PointSymbolType symbolType, GeoSolidBrush symbolSolidBrush, int symbolSize)
            : base(symbolType, symbolSolidBrush, symbolSize) { }

        public ClusterPointStyle(PointSymbolType symbolType, GeoSolidBrush symbolSolidBrush, GeoPen symbolPen, int symbolSize)
            : base(symbolType, symbolSolidBrush, symbolPen, symbolSize) { }

        public TextStyle TextStyle {
            get { return textSytle; }
            set { textSytle = value; }
        }

        public int CellSize {
            get { return cellSize; }
            set { cellSize = value; }
        }

        public Dictionary<string, IList<Feature>> ClusterDictionary {
            get { return clusterdictionary; }
            set { clusterdictionary = value; }
        }

        protected override void DrawCore(IEnumerable<Feature> features, GeoCanvas canvas, Collection<SimpleCandidate> labelsInThisLayer, Collection<SimpleCandidate> labelsInAllLayers) {
            if (clusterdictionary.Count == 0) {
                Dictionary<string, string> unusedFeatures = new Dictionary<string, string>();
                foreach (Feature feature in features) {
                    unusedFeatures.Add(feature.Id, feature.Id);
                }
                double scale = ExtentHelper.GetScale(canvas.CurrentWorldExtent, canvas.Width, canvas.MapUnit);
                MapSuiteTileMatrix mapSuiteTileMatrix = new MapSuiteTileMatrix(scale, cellSize, cellSize, canvas.MapUnit);
                IEnumerable<TileMatrixCell> tileMatricCells = mapSuiteTileMatrix.GetContainedCells(canvas.CurrentWorldExtent);
                foreach (TileMatrixCell cell in tileMatricCells) {
                    int featureCount = 0;
                    MultipointShape multiPointShape = new MultipointShape();
                    foreach (Feature feature in features) {
                        if (unusedFeatures.ContainsKey(feature.Id)) {
                            if (cell.BoundingBox.Contains(feature.GetBoundingBox())) {
                                featureCount++;
                                unusedFeatures.Remove(feature.Id);
                                multiPointShape.Points.Add(feature.GetBoundingBox().GetCenterPoint());
                            }
                        }
                    }
                    if (featureCount > 0) {
                        Dictionary<string, string> featureValues = new Dictionary<string, string>();
                        featureValues.Add(textSytle.TextColumnName, featureCount.ToString());
                        base.DrawCore(new Feature[] { new Feature(multiPointShape.GetCenterPoint(), featureValues) }, canvas, labelsInThisLayer, labelsInAllLayers);
                        textSytle.Draw(new Feature[] { new Feature(multiPointShape.GetCenterPoint(), featureValues) }, canvas, labelsInThisLayer, labelsInAllLayers);
                    }
                }
            } else {
                int i = 0;
                Dictionary<string, string> usedFeatures = new Dictionary<string, string>();
                foreach (Feature feature in features) {
                    string featid = feature.Id;
                    int featureCount = 0;
                    MultipointShape multiPointShape = new MultipointShape();
                    if (usedFeatures.ContainsKey(featid)) { continue; }
                    if (clusterdictionary.ContainsKey(featid)) {
                        IList<Feature> featurelist = clusterdictionary[featid];
                        Feature firstfeature = featurelist[0];
                        string firstText = firstfeature.ColumnValues[textSytle.TextColumnName];
                        string labeltext = firstfeature.ColumnValues[textSytle.TextColumnName];
                        foreach (Feature featurechild in featurelist) {
                            if (usedFeatures.ContainsKey(featurechild.Id)) { continue; }
                            i++;
                            featureCount++;
                            string lastText = featurechild.ColumnValues[textSytle.TextColumnName];
                            if(featureCount > 1) {
                                labeltext = string.Format("{0}-{1}", firstText, lastText);
                            } else {
                                labeltext = firstText;
                            }
                            //labeltext = string.Format("{0}-{1}", firstText, lastText);
                            usedFeatures.Add(featurechild.Id, featurechild.Id);
                            multiPointShape.Points.Add(feature.GetBoundingBox().GetCenterPoint());
                        }
                        if (featureCount > 0) {
                            Dictionary<string, string> featureValues = new Dictionary<string, string>();
                            featureValues.Add(textSytle.TextColumnName, labeltext);
                            base.DrawCore(new Feature[] { new Feature(multiPointShape.GetCenterPoint(), featureValues) }, canvas, labelsInThisLayer, labelsInAllLayers);
                            textSytle.Draw(new Feature[] { new Feature(multiPointShape.GetCenterPoint(), featureValues) }, canvas, labelsInThisLayer, labelsInAllLayers);
                        }

                        //usedFeatures.Add(feature.Id, feature.Id);
                    }
                }


            }
        }

        public Dictionary<string, IList<Feature>> ClusterDictionaryMethod(Collection<Feature> featurelist, double clusterradius, GeographyUnit geographyUnit, DistanceUnit distanceUnit) {
            Dictionary<string, IList<Feature>> featuredictionary = new Dictionary<string, IList<Feature>>();
            Collection<Feature> distanceFeatures = new Collection<Feature>();
            MultipointShape multipointshape = new MultipointShape();
            for (int j = 0; j < featurelist.Count; j++) {
                Feature feature = featurelist[j];
                BaseShape baseshape = feature.GetShape();
                int fidint = System.Convert.ToInt32(feature.Id);
                distanceFeatures = new Collection<Feature>();
                distanceFeatures.Add(feature);
                featuredictionary.Add(feature.Id, distanceFeatures);
                multipointshape = new MultipointShape();
                multipointshape.Points.Add(baseshape as PointShape);
                bool allpointsmatch = true;
                int k = j + 1;
                for (int m = j + 1; m < featurelist.Count; m++) {
                    Feature feature2 = featurelist[m];
                    int fidint2 = System.Convert.ToInt32(feature2.Id);
                    if(fidint == fidint2) { continue; }
                    if(!allpointsmatch) { break; }
                    BaseShape baseshape2 = feature2.GetShape();
                    multipointshape.Points.Add(baseshape2 as PointShape);
                    PointShape centerpoint = multipointshape.GetCenterPoint();
                    EllipseShape ellipseshape = new EllipseShape(centerpoint, clusterradius, geographyUnit, distanceUnit);
                    Collection<PointShape> mpoints = multipointshape.Points;
                    foreach(PointShape pshape in mpoints) {
                        double dist = pshape.GetDistanceTo(centerpoint, geographyUnit, distanceUnit);
                        if (dist > clusterradius) {
                            allpointsmatch = false;
                        }
                    }
                    if (allpointsmatch) {
                        featuredictionary[feature.Id].Add(feature2);
                        j = m;
                    }
                }
            }
            return featuredictionary;
        }

        protected override System.Collections.ObjectModel.Collection<string> GetRequiredColumnNamesCore() {
            Collection<string> columns = new Collection<string>();
            if (!columns.Contains(textSytle.TextColumnName)) {
                columns.Add(textSytle.TextColumnName);
            }
            return columns;
        }

    }
}
