﻿using GalaSoft.MvvmLight;
using Landdb.Client.Infrastructure;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using Landdb.Resources;
using ThinkGeo.MapSuite.Core;

namespace Landdb.MapStyles {
    public class StyleData {
        private static StyleData _self;

        public StyleData() {
            InitializeStyles();
            InitializeTypeStrings();
            InitializeFontStrings();
            ReturnStorageLocationStyles();
        }

        public static StyleData Self {
            get {
                if (_self == null) {
                    lock (typeof(StyleData)) {
                        if (_self == null) {
                            _self = new StyleData();
                        }
                    }
                }
                return _self;
            }
        }

        public List<string> ListofFonts { get; private set; }
        public List<AnnotationStyleItem> ListofStyles { get; private set; }
        public Dictionary<string, MapAnnotationCategory> DefaultStyleNames { get; private set; }
        public Dictionary<string, AnnotationStyleItem> ColorStyles { get; private set; }
        public Dictionary<string, MapAnnotationCategory> StyleNames { get; private set; }

        public Dictionary<string, MapAnnotationCategory> StorageStyleNames { get; private set; }

        private List<AnnotationStyleItem> customstyles;
        public List<AnnotationStyleItem> CustomStyles {
            get { return customstyles; }
            set {
                customstyles = value;
                if (customstyles == null) { return; }
                foreach (AnnotationStyleItem asi in customstyles) {
                    if (ColorStyles.ContainsKey(asi.TypeName)) {
                        ColorStyles[asi.TypeName].AnnotationStyle = asi.AnnotationStyle;
                    }
                    else {
                        ColorStyles.Add(asi.TypeName, asi);
                    }
                }
            }
        }

        private Dictionary<string, MapAnnotationCategory> userstylenames;
        public Dictionary<string, MapAnnotationCategory> UserStyleNames {
            get { return userstylenames; }
            set {
                userstylenames = value;
                if (userstylenames == null) { return; }
                foreach (KeyValuePair<string, MapAnnotationCategory> asi in userstylenames.ToList()) {
                    if (StyleNames.ContainsKey(asi.Key)) {
                        StyleNames[asi.Key] = asi.Value;
                        //if (ColorStyles.ContainsKey(asi.Value.StyleName)) {
                        //    ColorStyles[asi.Value.StyleName].Visible = asi.Value.Visible;
                        //}
                    }
                    else {
                        StyleNames.Add(asi.Key, asi.Value);
                        //if (ColorStyles.ContainsKey(asi.Value.StyleName)) {
                        //    ColorStyles[asi.Value.StyleName].Visible = asi.Value.Visible;
                        //}
                    }
                }
            }
        }

        private int _pointsize = 20;
        public int PointSize {
            get { return _pointsize; }
            set { _pointsize = value; }
        }

        private void InitializeStyles() {
            string programDirectory = Landdb.Infrastructure.ApplicationEnvironment.ApplicationExecutableDirectory;
            string imageDirectory = Path.Combine(programDirectory, @"Resources\\Images\\");

            //Polygons
            List<AnnotationStyleItem> listofstyles = new List<AnnotationStyleItem>();
            listofstyles.Add(new AnnotationStyleItem("Crops", "Polygon", AreaStyles.Crop1));
            listofstyles.Add(new AnnotationStyleItem("Woods", "Polygon", AreaStyles.Forest1));
            listofstyles.Add(new AnnotationStyleItem("Grass", "Polygon", AreaStyles.Grass1));
            //listofstyles.Add(new AnnotationStyleItem("Wetland", "Polygon", AreaStyles.Swamp1));
            listofstyles.Add(new AnnotationStyleItem("Swamp", "Polygon", AreaStyles.Swamp2));
            listofstyles.Add(new AnnotationStyleItem("Pond", "Polygon", AreaStyles.Water1));
            listofstyles.Add(new AnnotationStyleItem("Water", "Polygon", AreaStyles.Water2));
            listofstyles.Add(new AnnotationStyleItem("WaterIntermittent", "Polygon", AreaStyles.WaterIntermittent1));
            listofstyles.Add(new AnnotationStyleItem("Wildlife", "Polygon", AreaStyles.Wildlife1));
            listofstyles.Add(new AnnotationStyleItem("Building", "Polygon", AreaStyles.Sand1));
            listofstyles.Add(new AnnotationStyleItem("NoFlyZone", "Polygon", AreaStyles.Military1));
            listofstyles.Add(new AnnotationStyleItem("Houses", "Polygon", AreaStyles.Urban1));
            listofstyles.Add(new AnnotationStyleItem("Schools", "Polygon", AreaStyles.Urban2));
            listofstyles.Add(new AnnotationStyleItem("BufferZone", "Polygon", AreaStyles.County1));
            listofstyles.Add(new AnnotationStyleItem("FarmBuildings", "Polygon", AreaStyles.CreateSimpleAreaStyle(new GeoColor(255, GeoColor.StandardColors.LightGreen), new GeoColor(255, GeoColor.StandardColors.Green))));
            listofstyles.Add(new AnnotationStyleItem("Waterway", "Polygon", AreaStyles.CreateSimpleAreaStyle(new GeoColor(255, GeoColor.StandardColors.LightBlue), new GeoColor(255, GeoColor.StandardColors.Green))));
            listofstyles.Add(new AnnotationStyleItem("ChemicalStorage", "Polygon", AreaStyles.CreateSimpleAreaStyle(new GeoColor(255, GeoColor.StandardColors.LightSalmon), new GeoColor(255, GeoColor.StandardColors.Red))));
            listofstyles.Add(new AnnotationStyleItem("GrainBins", "Polygon", AreaStyles.CreateSimpleAreaStyle(new GeoColor(255, GeoColor.StandardColors.YellowGreen), new GeoColor(255, GeoColor.StandardColors.DarkGreen))));
            listofstyles.Add(new AnnotationStyleItem("PackingHouse", "Polygon", AreaStyles.CreateSimpleAreaStyle(new GeoColor(255, GeoColor.StandardColors.Orange), new GeoColor(255, GeoColor.StandardColors.OrangeRed))));
            listofstyles.Add(new AnnotationStyleItem("FarmShop", "Polygon", AreaStyles.CreateSimpleAreaStyle(new GeoColor(255, GeoColor.StandardColors.Red), new GeoColor(255, GeoColor.StandardColors.Black))));
            listofstyles.Add(new AnnotationStyleItem("StorageFacility", "Polygon", AreaStyles.CreateSimpleAreaStyle(new GeoColor(255, GeoColor.StandardColors.LightCyan), new GeoColor(255, GeoColor.StandardColors.Cyan))));
            listofstyles.Add(new AnnotationStyleItem("ProcessorFacility", "Polygon", AreaStyles.CreateSimpleAreaStyle(new GeoColor(255, GeoColor.StandardColors.Gold), new GeoColor(255, GeoColor.StandardColors.Silver))));
            listofstyles.Add(new AnnotationStyleItem("GrainElevator", "Polygon", AreaStyles.CreateSimpleAreaStyle(new GeoColor(255, GeoColor.StandardColors.YellowGreen), new GeoColor(255, GeoColor.StandardColors.Navy))));
            listofstyles.Add(new AnnotationStyleItem("CottonGin", "Polygon", AreaStyles.CreateSimpleAreaStyle(new GeoColor(255, GeoColor.StandardColors.White), new GeoColor(255, GeoColor.StandardColors.Black))));
            listofstyles.Add(new AnnotationStyleItem("EthanolPlant", "Polygon", AreaStyles.CreateSimpleAreaStyle(new GeoColor(255, GeoColor.StandardColors.LightGreen), new GeoColor(255, GeoColor.StandardColors.Green))));
            listofstyles.Add(new AnnotationStyleItem("PotatoCellar", "Polygon", AreaStyles.CreateSimpleAreaStyle(new GeoColor(255, GeoColor.StandardColors.Brown), new GeoColor(255, GeoColor.StandardColors.Yellow))));
            listofstyles.Add(new AnnotationStyleItem("FeedLot", "Polygon", AreaStyles.CreateSimpleAreaStyle(new GeoColor(255, GeoColor.StandardColors.LightSeaGreen), new GeoColor(255, GeoColor.StandardColors.Black))));
            listofstyles.Add(new AnnotationStyleItem("SilageStorage", "Polygon", AreaStyles.CreateSimpleAreaStyle(new GeoColor(255, GeoColor.StandardColors.DarkGreen), new GeoColor(255, GeoColor.StandardColors.Yellow))));
            listofstyles.Add(new AnnotationStyleItem("SilageStorage2", "Polygon", AreaStyles.CreateSimpleAreaStyle(new GeoColor(255, GeoColor.StandardColors.DarkGreen), new GeoColor(255, GeoColor.StandardColors.Yellow))));
            listofstyles.Add(new AnnotationStyleItem("SilageStorage3", "Polygon", AreaStyles.CreateSimpleAreaStyle(new GeoColor(255, GeoColor.StandardColors.DarkGreen), new GeoColor(255, GeoColor.StandardColors.Yellow))));
            listofstyles.Add(new AnnotationStyleItem("SilageStorage4", "Polygon", AreaStyles.CreateSimpleAreaStyle(new GeoColor(255, GeoColor.StandardColors.DarkGreen), new GeoColor(255, GeoColor.StandardColors.Yellow))));
            //System.Drawing.Color color2 = System.Drawing.ColorTranslator.FromHtml(@"#0080FF");
            listofstyles.Add(new AnnotationStyleItem("Wetlands", "Polygon", AreaStyles.CreateSimpleAreaStyle(new GeoColor(255, GeoColor.FromHtml(@"#0080FF")), new GeoColor(255, GeoColor.FromHtml(@"#0080FF")))));
            //listofstyles.Add(new AnnotationStyleItem("BufferZone", "Polygon", AreaStyles.CreateHatchStyle(ViewModel.Maps.HatchStyleFactory.GetHatchStyle("ForwardDiagonal"), new GeoColor(125, GeoColor.StandardColors.SaddleBrown), new GeoColor(255, GeoColor.StandardColors.Brown))));
            //listofstyles.Add(new AnnotationStyleItem("BufferZoneLERAPA", "Polygon", AreaStyles.CreateHatchStyle(ViewModel.Maps.HatchStyleFactory.GetHatchStyle("ForwardDiagonal"), new GeoColor(125, GeoColor.StandardColors.SaddleBrown), new GeoColor(255, GeoColor.StandardColors.Brown))));
            //listofstyles.Add(new AnnotationStyleItem("BufferZoneLERAPB", "Polygon", AreaStyles.CreateHatchStyle(ViewModel.Maps.HatchStyleFactory.GetHatchStyle("ForwardDiagonal"), new GeoColor(125, GeoColor.StandardColors.SaddleBrown), new GeoColor(255, GeoColor.StandardColors.Brown))));
            //listofstyles.Add(new AnnotationStyleItem("BufferZoneInterimAquatic", "Polygon", AreaStyles.CreateHatchStyle(ViewModel.Maps.HatchStyleFactory.GetHatchStyle("ForwardDiagonal"), new GeoColor(125, GeoColor.StandardColors.SaddleBrown), new GeoColor(255, GeoColor.StandardColors.Brown))));
            //listofstyles.Add(new AnnotationStyleItem("Spray Drift", "Polygon", AreaStyles.CreateHatchStyle(ViewModel.Maps.HatchStyleFactory.GetHatchStyle("ForwardDiagonal"), new GeoColor(125, GeoColor.StandardColors.SaddleBrown), new GeoColor(255, GeoColor.StandardColors.Brown))));
            //listofstyles.Add(new AnnotationStyleItem("BufferZoneArthropodProtection", "Polygon", AreaStyles.CreateHatchStyle(ViewModel.Maps.HatchStyleFactory.GetHatchStyle("ForwardDiagonal"), new GeoColor(125, GeoColor.StandardColors.SaddleBrown), new GeoColor(255, GeoColor.StandardColors.Brown))));

            //Lines
            listofstyles.Add(new AnnotationStyleItem("BlueLine", "Line", LineStyles.CreateSimpleLineStyle(new GeoColor(255, GeoColor.StandardColors.Blue), 2, LineDashStyle.Solid, false)));
            listofstyles.Add(new AnnotationStyleItem("BlueLineDash", "Line", LineStyles.CreateSimpleLineStyle(new GeoColor(255, GeoColor.StandardColors.Blue), 2, LineDashStyle.Dash, false)));
            listofstyles.Add(new AnnotationStyleItem("BlueLineDot", "Line", LineStyles.CreateSimpleLineStyle(new GeoColor(255, GeoColor.StandardColors.Blue), 2, LineDashStyle.Dot, false)));
            listofstyles.Add(new AnnotationStyleItem("RedLine", "Line", LineStyles.CreateSimpleLineStyle(new GeoColor(255, GeoColor.StandardColors.Red), 2, LineDashStyle.Solid, false)));
            listofstyles.Add(new AnnotationStyleItem("RedLineDash", "Line", LineStyles.CreateSimpleLineStyle(new GeoColor(255, GeoColor.StandardColors.Red), 2, LineDashStyle.Dash, false)));
            listofstyles.Add(new AnnotationStyleItem("RedLineDot", "Line", LineStyles.CreateSimpleLineStyle(new GeoColor(255, GeoColor.StandardColors.Red), 2, LineDashStyle.Dot, false)));
            listofstyles.Add(new AnnotationStyleItem("GreenLine", "Line", LineStyles.CreateSimpleLineStyle(new GeoColor(255, GeoColor.StandardColors.Green), 2, LineDashStyle.Solid, false)));
            listofstyles.Add(new AnnotationStyleItem("GreenLineDash", "Line", LineStyles.CreateSimpleLineStyle(new GeoColor(255, GeoColor.StandardColors.Green), 2, LineDashStyle.Dash, false)));
            listofstyles.Add(new AnnotationStyleItem("GreenLineDot", "Line", LineStyles.CreateSimpleLineStyle(new GeoColor(255, GeoColor.StandardColors.Green), 2, LineDashStyle.Dot, false)));
            listofstyles.Add(new AnnotationStyleItem("YellowLine", "Line", LineStyles.CreateSimpleLineStyle(new GeoColor(255, GeoColor.StandardColors.Yellow), 2, LineDashStyle.Solid, false)));
            listofstyles.Add(new AnnotationStyleItem("YellowLineDash", "Line", LineStyles.CreateSimpleLineStyle(new GeoColor(255, GeoColor.StandardColors.Yellow), 2, LineDashStyle.Dash, false)));
            listofstyles.Add(new AnnotationStyleItem("YellowLineDot", "Line", LineStyles.CreateSimpleLineStyle(new GeoColor(255, GeoColor.StandardColors.Yellow), 2, LineDashStyle.Dot, false)));
            listofstyles.Add(new AnnotationStyleItem("OrangeLine", "Line", LineStyles.CreateSimpleLineStyle(new GeoColor(255, GeoColor.StandardColors.Orange), 2, LineDashStyle.Solid, false)));
            listofstyles.Add(new AnnotationStyleItem("OrangeLineDash", "Line", LineStyles.CreateSimpleLineStyle(new GeoColor(255, GeoColor.StandardColors.Orange), 2, LineDashStyle.Dash, false)));
            listofstyles.Add(new AnnotationStyleItem("OrangeLineDot", "Line", LineStyles.CreateSimpleLineStyle(new GeoColor(255, GeoColor.StandardColors.Orange), 2, LineDashStyle.Dot, false)));
            listofstyles.Add(new AnnotationStyleItem("BlueYellowLine", "Line", LineStyles.CreateSimpleLineStyle(new GeoColor(255, GeoColor.StandardColors.Yellow), 1, new GeoColor(255, GeoColor.StandardColors.Blue), 1, false)));
            listofstyles.Add(new AnnotationStyleItem("RedYellowLine", "Line", LineStyles.CreateSimpleLineStyle(new GeoColor(255, GeoColor.StandardColors.Yellow), 1, new GeoColor(255, GeoColor.StandardColors.Red), 1, false)));
            listofstyles.Add(new AnnotationStyleItem("Canal1", "Line", LineStyles.Canal1));
            listofstyles.Add(new AnnotationStyleItem("DegreeLine1", "Line", LineStyles.DegreeLine1));
            listofstyles.Add(new AnnotationStyleItem("Pipeline1", "Line", LineStyles.Pipeline1));
            listofstyles.Add(new AnnotationStyleItem("Pipeline2", "Line", LineStyles.Pipeline2));
            listofstyles.Add(new AnnotationStyleItem("Pipeline3", "Line", LineStyles.Pipeline3));
            listofstyles.Add(new AnnotationStyleItem("Railway1", "Line", LineStyles.Railway1));
            listofstyles.Add(new AnnotationStyleItem("Railway2", "Line", LineStyles.Railway2));
            listofstyles.Add(new AnnotationStyleItem("Railway3", "Line", LineStyles.Railway3));
            listofstyles.Add(new AnnotationStyleItem("Railway4", "Line", LineStyles.Railway4));
            listofstyles.Add(new AnnotationStyleItem("River1", "Line", LineStyles.River1));
            listofstyles.Add(new AnnotationStyleItem("River2", "Line", LineStyles.River2));
            listofstyles.Add(new AnnotationStyleItem("Utility1", "Line", LineStyles.Utility1));
            listofstyles.Add(new AnnotationStyleItem("Utility2", "Line", LineStyles.Utility2));
            listofstyles.Add(new AnnotationStyleItem("Utility3", "Line", LineStyles.Utility3));
            listofstyles.Add(new AnnotationStyleItem("SecondaryRoad1", "Line", LineStyles.SecondaryRoad1));
            listofstyles.Add(new AnnotationStyleItem("SecondaryRoad2", "Line", LineStyles.SecondaryRoad2));
            listofstyles.Add(new AnnotationStyleItem("LocalRoad1", "Line", LineStyles.LocalRoad1));
            listofstyles.Add(new AnnotationStyleItem("LocalRoad2", "Line", LineStyles.LocalRoad2));
            listofstyles.Add(new AnnotationStyleItem("LocalRoad3", "Line", LineStyles.LocalRoad3));
            listofstyles.Add(new AnnotationStyleItem("LocalRoad4", "Line", LineStyles.LocalRoad4));
            listofstyles.Add(new AnnotationStyleItem("Highway1", "Line", LineStyles.Highway1));
            listofstyles.Add(new AnnotationStyleItem("Highway2", "Line", LineStyles.Highway2));
            listofstyles.Add(new AnnotationStyleItem("Highway3", "Line", LineStyles.Highway3));
            listofstyles.Add(new AnnotationStyleItem("Highway4", "Line", LineStyles.Highway4));
            listofstyles.Add(new AnnotationStyleItem("Highway5", "Line", LineStyles.Highway5));
            listofstyles.Add(new AnnotationStyleItem("NavyLineDashDotDot", "Line", LineStyles.CreateSimpleLineStyle(new GeoColor(255, GeoColor.StandardColors.Navy), 2, LineDashStyle.DashDotDot, false)));
            listofstyles.Add(new AnnotationStyleItem("NavyLine", "Line", LineStyles.CreateSimpleLineStyle(new GeoColor(255, GeoColor.StandardColors.Navy), 2, LineDashStyle.Solid, false)));
            listofstyles.Add(new AnnotationStyleItem("FarmBoundary", "Line", LineStyles.CreateSimpleLineStyle(new GeoColor(255, GeoColor.StandardColors.Yellow), 2, LineDashStyle.Solid, false)));

            //Points
            listofstyles.Add(new AnnotationStyleItem("LabelPoint", "Point", PointStyles.CreateSimplePointStyle(PointSymbolType.Circle, new GeoColor(255, GeoColor.StandardColors.Transparent), 1)));
            listofstyles.Add(new AnnotationStyleItem("BlackPoint", "Point", PointStyles.CreateSimplePointStyle(PointSymbolType.Circle, new GeoColor(255, GeoColor.StandardColors.Black), _pointsize)));
            listofstyles.Add(new AnnotationStyleItem("BlackDiamond", "Point", PointStyles.CreateSimplePointStyle(PointSymbolType.Diamond, new GeoColor(255, GeoColor.StandardColors.Black), _pointsize)));
            listofstyles.Add(new AnnotationStyleItem("BlackStar", "Point", PointStyles.CreateSimpleStarStyle(new GeoColor(255, GeoColor.StandardColors.Black), _pointsize)));
            listofstyles.Add(new AnnotationStyleItem("BlackTriangle", "Point", PointStyles.CreateSimpleTriangleStyle(new GeoColor(255, GeoColor.StandardColors.Black), _pointsize)));
            listofstyles.Add(new AnnotationStyleItem("BlackSquare", "Point", PointStyles.CreateSimpleSquareStyle(new GeoColor(255, GeoColor.StandardColors.Black), _pointsize)));
            listofstyles.Add(new AnnotationStyleItem("BlackCross", "Point", PointStyles.CreateSimplePointStyle(PointSymbolType.Cross, new GeoColor(255, GeoColor.StandardColors.Black), _pointsize)));
            listofstyles.Add(new AnnotationStyleItem("BluePoint", "Point", PointStyles.CreateSimplePointStyle(PointSymbolType.Circle, new GeoColor(255, GeoColor.StandardColors.Blue), _pointsize)));
            listofstyles.Add(new AnnotationStyleItem("BlueDiamond", "Point", PointStyles.CreateSimplePointStyle(PointSymbolType.Diamond, new GeoColor(255, GeoColor.StandardColors.Blue), _pointsize)));
            listofstyles.Add(new AnnotationStyleItem("BlueStar", "Point", PointStyles.CreateSimpleStarStyle(new GeoColor(255, GeoColor.StandardColors.Blue), _pointsize)));
            listofstyles.Add(new AnnotationStyleItem("BlueTriangle", "Point", PointStyles.CreateSimpleTriangleStyle(new GeoColor(255, GeoColor.StandardColors.Blue), _pointsize)));
            listofstyles.Add(new AnnotationStyleItem("BlueSquare", "Point", PointStyles.CreateSimpleSquareStyle(new GeoColor(255, GeoColor.StandardColors.Blue), _pointsize)));
            listofstyles.Add(new AnnotationStyleItem("BlueCross", "Point", PointStyles.CreateSimplePointStyle(PointSymbolType.Cross, new GeoColor(255, GeoColor.StandardColors.Blue), _pointsize)));
            listofstyles.Add(new AnnotationStyleItem("GreenPoint", "Point", PointStyles.CreateSimplePointStyle(PointSymbolType.Circle, new GeoColor(255, GeoColor.StandardColors.Green), _pointsize)));
            listofstyles.Add(new AnnotationStyleItem("GreenDiamond", "Point", PointStyles.CreateSimplePointStyle(PointSymbolType.Diamond, new GeoColor(255, GeoColor.StandardColors.Green), _pointsize)));
            listofstyles.Add(new AnnotationStyleItem("GreenStar", "Point", PointStyles.CreateSimpleStarStyle(new GeoColor(255, GeoColor.StandardColors.Green), _pointsize)));
            listofstyles.Add(new AnnotationStyleItem("GreenTriangle", "Point", PointStyles.CreateSimpleTriangleStyle(new GeoColor(255, GeoColor.StandardColors.Green), _pointsize)));
            listofstyles.Add(new AnnotationStyleItem("GreenSquare", "Point", PointStyles.CreateSimpleSquareStyle(new GeoColor(255, GeoColor.StandardColors.Green), _pointsize)));
            listofstyles.Add(new AnnotationStyleItem("GreenCross", "Point", PointStyles.CreateSimplePointStyle(PointSymbolType.Cross, new GeoColor(255, GeoColor.StandardColors.Green), _pointsize)));
            listofstyles.Add(new AnnotationStyleItem("OrangePoint", "Point", PointStyles.CreateSimplePointStyle(PointSymbolType.Circle, new GeoColor(255, GeoColor.StandardColors.Orange), _pointsize)));
            listofstyles.Add(new AnnotationStyleItem("OrangeDiamond", "Point", PointStyles.CreateSimplePointStyle(PointSymbolType.Diamond, new GeoColor(255, GeoColor.StandardColors.Orange), _pointsize)));
            listofstyles.Add(new AnnotationStyleItem("OrangeStar", "Point", PointStyles.CreateSimpleStarStyle(new GeoColor(255, GeoColor.StandardColors.Orange), _pointsize)));
            listofstyles.Add(new AnnotationStyleItem("OrangeTriangle", "Point", PointStyles.CreateSimpleTriangleStyle(new GeoColor(255, GeoColor.StandardColors.Orange), _pointsize)));
            listofstyles.Add(new AnnotationStyleItem("OrangeSquare", "Point", PointStyles.CreateSimpleSquareStyle(new GeoColor(255, GeoColor.StandardColors.Orange), _pointsize)));
            listofstyles.Add(new AnnotationStyleItem("OrangeCross", "Point", PointStyles.CreateSimplePointStyle(PointSymbolType.Cross, new GeoColor(255, GeoColor.StandardColors.Orange), _pointsize)));
            listofstyles.Add(new AnnotationStyleItem("RedPoint", "Point", PointStyles.CreateSimplePointStyle(PointSymbolType.Circle, new GeoColor(255, GeoColor.StandardColors.Red), _pointsize)));
            listofstyles.Add(new AnnotationStyleItem("RedDiamond", "Point", PointStyles.CreateSimplePointStyle(PointSymbolType.Diamond, new GeoColor(255, GeoColor.StandardColors.Red), _pointsize)));
            listofstyles.Add(new AnnotationStyleItem("RedStar", "Point", PointStyles.CreateSimpleStarStyle(new GeoColor(255, GeoColor.StandardColors.Red), _pointsize)));
            listofstyles.Add(new AnnotationStyleItem("RedTriangle", "Point", PointStyles.CreateSimpleTriangleStyle(new GeoColor(255, GeoColor.StandardColors.Red), _pointsize)));
            listofstyles.Add(new AnnotationStyleItem("RedSquare", "Point", PointStyles.CreateSimpleSquareStyle(new GeoColor(255, GeoColor.StandardColors.Red), _pointsize)));
            listofstyles.Add(new AnnotationStyleItem("RedCross", "Point", PointStyles.CreateSimplePointStyle(PointSymbolType.Cross, new GeoColor(255, GeoColor.StandardColors.Red), _pointsize)));
            listofstyles.Add(new AnnotationStyleItem("YellowPoint", "Point", PointStyles.CreateSimplePointStyle(PointSymbolType.Circle, new GeoColor(255, GeoColor.StandardColors.Yellow), _pointsize)));
            listofstyles.Add(new AnnotationStyleItem("YellowDiamond", "Point", PointStyles.CreateSimplePointStyle(PointSymbolType.Diamond, new GeoColor(255, GeoColor.StandardColors.Yellow), _pointsize)));
            listofstyles.Add(new AnnotationStyleItem("YellowStar", "Point", PointStyles.CreateSimpleStarStyle(new GeoColor(255, GeoColor.StandardColors.Yellow), _pointsize)));
            listofstyles.Add(new AnnotationStyleItem("YellowTriangle", "Point", PointStyles.CreateSimpleTriangleStyle(new GeoColor(255, GeoColor.StandardColors.Yellow), _pointsize)));
            listofstyles.Add(new AnnotationStyleItem("YellowSquare", "Point", PointStyles.CreateSimpleSquareStyle(new GeoColor(255, GeoColor.StandardColors.Yellow), _pointsize)));
            listofstyles.Add(new AnnotationStyleItem("YellowCross", "Point", PointStyles.CreateSimplePointStyle(PointSymbolType.Cross, new GeoColor(255, GeoColor.StandardColors.Yellow), _pointsize)));
            listofstyles.Add(new AnnotationStyleItem("WhitePoint", "Point", PointStyles.CreateSimplePointStyle(PointSymbolType.Circle, new GeoColor(255, GeoColor.StandardColors.White), _pointsize)));
            listofstyles.Add(new AnnotationStyleItem("WhiteDiamond", "Point", PointStyles.CreateSimplePointStyle(PointSymbolType.Diamond, new GeoColor(255, GeoColor.StandardColors.White), _pointsize)));
            listofstyles.Add(new AnnotationStyleItem("WhiteStar", "Point", PointStyles.CreateSimpleStarStyle(new GeoColor(255, GeoColor.StandardColors.White), _pointsize)));
            listofstyles.Add(new AnnotationStyleItem("WhiteTriangle", "Point", PointStyles.CreateSimpleTriangleStyle(new GeoColor(255, GeoColor.StandardColors.White), _pointsize)));
            listofstyles.Add(new AnnotationStyleItem("WhiteSquare", "Point", PointStyles.CreateSimpleSquareStyle(new GeoColor(255, GeoColor.StandardColors.White), _pointsize)));
            listofstyles.Add(new AnnotationStyleItem("WhiteCross", "Point", PointStyles.CreateSimplePointStyle(PointSymbolType.Cross, new GeoColor(255, GeoColor.StandardColors.White), _pointsize)));
            listofstyles.Add(new AnnotationStyleItem("YellowStar2", "Point", PointStyles.CreateSimplePointStyle(PointSymbolType.Star2, new GeoColor(255, GeoColor.StandardColors.Yellow), _pointsize)));
            string pinblue = System.IO.Path.Combine(imageDirectory, "pin_blue.png");
            string pingreen = System.IO.Path.Combine(imageDirectory, "pin_green.png");
            string pingrey = System.IO.Path.Combine(imageDirectory, "pin_grey.png");
            string pinred = System.IO.Path.Combine(imageDirectory, "pin_red.png");
            string pinyellow = System.IO.Path.Combine(imageDirectory, "pin_yellow.png");
            string deerstand = System.IO.Path.Combine(imageDirectory, "stand.png");
            listofstyles.Add(new AnnotationStyleItem("PinBlue", "Point", new PointStyle(new GeoImage(pinblue))));
            listofstyles.Add(new AnnotationStyleItem("PinGreen", "Point", new PointStyle(new GeoImage(pingreen))));
            listofstyles.Add(new AnnotationStyleItem("PinGrey", "Point", new PointStyle(new GeoImage(pingrey))));
            listofstyles.Add(new AnnotationStyleItem("PinRed", "Point", new PointStyle(new GeoImage(pinred))));
            listofstyles.Add(new AnnotationStyleItem("PinYellow", "Point", new PointStyle(new GeoImage(pinyellow))));
            listofstyles.Add(new AnnotationStyleItem("DeerStand", "Point", new PointStyle(new GeoImage(deerstand))));
            listofstyles.Add(new AnnotationStyleItem("WellPoint001", "Point", new WellPointStyle(1, new GeoSolidBrush(new GeoColor(255, GeoColor.StandardColors.Black)), 10)));
            ListofStyles = listofstyles;

            ColorStyles = new Dictionary<string, AnnotationStyleItem>();
            foreach (AnnotationStyleItem asi in ListofStyles) {
                ColorStyles.Add(asi.TypeName, asi);
            }
        }

        private void InitializeTypeStrings() {
            Dictionary<string, MapAnnotationCategory> defaultstylenames = new Dictionary<string, MapAnnotationCategory>();
            defaultstylenames.Add("Drain Tiles", new MapAnnotationCategory() { Name = "Drain Tiles", StyleName = "GreenLineDash", ShapeType = "Line", Visible = true });
            defaultstylenames.Add("Irrigation Well", new MapAnnotationCategory() { Name = "Irrigation Well", StyleName = "BlueStar", ShapeType = "Point", Visible = true });
            defaultstylenames.Add("Drainage Pump", new MapAnnotationCategory() { Name = "Drainage Pump", StyleName = "BlueSquare", ShapeType = "Point", Visible = true });
            defaultstylenames.Add("Pivot Point", new MapAnnotationCategory() { Name = "Pivot Point", StyleName = "BlackPoint", ShapeType = "Point", Visible = true });
            defaultstylenames.Add("Water Line", new MapAnnotationCategory() { Name = "Water Line", StyleName = "NavyLine", ShapeType = "Line", Visible = true });
            defaultstylenames.Add("Water Riser", new MapAnnotationCategory() { Name = "Water Riser", StyleName = "PinBlue", ShapeType = "Point", Visible = true });

            defaultstylenames.Add("Storage Facility", new MapAnnotationCategory() { Name = "Storage Facility", StyleName = "StorageFacility", ShapeType = "Polygon", Visible = true });
            defaultstylenames.Add("Potato Cellar", new MapAnnotationCategory() { Name = "Potato Cellar", StyleName = "PotatoCellar", ShapeType = "Polygon", Visible = true });
            defaultstylenames.Add("Silo", new MapAnnotationCategory() { Name = "Silo", StyleName = "SilageStorage", ShapeType = "Polygon", Visible = true });
            defaultstylenames.Add("Silage Pit", new MapAnnotationCategory() { Name = "Silage Pit", StyleName = "SilageStorage2", ShapeType = "Polygon", Visible = true });
            defaultstylenames.Add("Silage Bag", new MapAnnotationCategory() { Name = "Silage Bag", StyleName = "SilageStorage3", ShapeType = "Polygon", Visible = true });
            defaultstylenames.Add("Silage Pile", new MapAnnotationCategory() { Name = "Silage Pile", StyleName = "SilageStorage4", ShapeType = "Polygon", Visible = true });
            defaultstylenames.Add("Grain Bins", new MapAnnotationCategory() { Name = "Grain Bins", StyleName = "GrainBins", ShapeType = "Polygon", Visible = true });
            defaultstylenames.Add("Packing House", new MapAnnotationCategory() { Name = "Packing House", StyleName = "PackingHouse", ShapeType = "Polygon", Visible = true });

            defaultstylenames.Add("Grain Elevator", new MapAnnotationCategory() { Name = "Grain Elevator", StyleName = "GrainElevator", ShapeType = "Polygon", Visible = true });
            defaultstylenames.Add("Cotton Gin", new MapAnnotationCategory() { Name = "Cotton Gin", StyleName = "CottonGin", ShapeType = "Polygon", Visible = true });
            defaultstylenames.Add("Processor Facility", new MapAnnotationCategory() { Name = "Processor Facility", StyleName = "ProcessorFacility", ShapeType = "Polygon", Visible = true });
            defaultstylenames.Add("Ethanol Plant", new MapAnnotationCategory() { Name = "Ethanol Plant", StyleName = "FarmBuildings", ShapeType = "Polygon", Visible = true });

            defaultstylenames.Add("Building", new MapAnnotationCategory() { Name = "Building", StyleName = "Building", ShapeType = "Polygon", Visible = true });
            defaultstylenames.Add("Farm Buildings", new MapAnnotationCategory() { Name = "Farm Buildings", StyleName = "FarmBuildings", ShapeType = "Polygon", Visible = true });
            defaultstylenames.Add("Chemical Storage", new MapAnnotationCategory() { Name = "Chemical Storage", StyleName = "ChemicalStorage", ShapeType = "Polygon", Visible = true });
            defaultstylenames.Add("Farm Shop", new MapAnnotationCategory() { Name = "Farm Shop", StyleName = "FarmShop", ShapeType = "Polygon", Visible = true });
            defaultstylenames.Add("Feed Lot", new MapAnnotationCategory() { Name = "Feed Lot", StyleName = "FeedLot", ShapeType = "Polygon", Visible = true });

            defaultstylenames.Add("Field Entrance", new MapAnnotationCategory() { Name = "Field Entrance", StyleName = "GreenDiamond", ShapeType = "Point", Visible = true });
            defaultstylenames.Add("Label", new MapAnnotationCategory() { Name = "Label", StyleName = "LabelPoint", ShapeType = "Point", Visible = true });

            defaultstylenames.Add("Power Lines", new MapAnnotationCategory() { Name = "Power Lines", StyleName = "RedLine", ShapeType = "Line", Visible = true });
            defaultstylenames.Add("Underground Electric Line", new MapAnnotationCategory() { Name = "Underground Electric Line", StyleName = "RedLineDash", ShapeType = "Line", Visible = true });
            defaultstylenames.Add("Underground Media Line", new MapAnnotationCategory() { Name = "Underground Media Line", StyleName = "OrangeLineDash", ShapeType = "Line", Visible = true });
            defaultstylenames.Add("Underground Gas Line", new MapAnnotationCategory() { Name = "Underground Gas Line", StyleName = "YellowLineDash", ShapeType = "Line", Visible = true });

            defaultstylenames.Add("Houses", new MapAnnotationCategory() { Name = "Houses", StyleName = "Houses", ShapeType = "Polygon", Visible = true });
            defaultstylenames.Add("Schools", new MapAnnotationCategory() { Name = "Schools", StyleName = "Schools", ShapeType = "Polygon", Visible = true });
            defaultstylenames.Add("Buffer Zone", new MapAnnotationCategory() { Name = "Buffer Zone", StyleName = "BufferZone", ShapeType = "Polygon", Visible = true });
            defaultstylenames.Add("No Fly Zone", new MapAnnotationCategory() { Name = "No Fly Zone", StyleName = "NoFlyZone", ShapeType = "Polygon", Visible = true });
            defaultstylenames.Add("Terrace", new MapAnnotationCategory() { Name = "Terrace", StyleName = "RedLineDash", ShapeType = "Line", Visible = true });
            defaultstylenames.Add("Waterway", new MapAnnotationCategory() { Name = "Waterway", StyleName = "Waterway", ShapeType = "Polygon", Visible = true });
            defaultstylenames.Add("Open Drainage Ditch", new MapAnnotationCategory() { Name = "Open Drainage Ditch", StyleName = "NavyLineDashDotDot", ShapeType = "Line", Visible = true });

            defaultstylenames.Add("Farm Boundary", new MapAnnotationCategory() { Name = "Farm Boundary", StyleName = "YellowLine", ShapeType = "Line", Visible = true });
            defaultstylenames.Add("Fuel Tank", new MapAnnotationCategory() { Name = "Fuel Tank", StyleName = "PinYellow", ShapeType = "Point", Visible = true });
            defaultstylenames.Add("Wetlands", new MapAnnotationCategory() { Name = "Wetlands", StyleName = "Wetlands", ShapeType = "Polygon", Visible = true });
            defaultstylenames.Add("Deer Stand", new MapAnnotationCategory() { Name = "Deer Stand", StyleName = "DeerStand", ShapeType = "Point", Visible = true });
            defaultstylenames.Add("Duck Blind", new MapAnnotationCategory() { Name = "Duck Blind", StyleName = "BlackTriangle", ShapeType = "Point", Visible = true });
            defaultstylenames.Add("Porta Potty", new MapAnnotationCategory() { Name = "Porta Potty", StyleName = "BlackPoint", ShapeType = "Point", Visible = true });
            defaultstylenames.Add("Food Plot", new MapAnnotationCategory() { Name = "Food Plot", StyleName = "Wildlife", ShapeType = "Polygon", Visible = true });


            DefaultStyleNames = defaultstylenames;
            StyleNames = defaultstylenames;

            //***Farm Boundary - Solid - Yellow
            //***Fuel Tank = Pin Yellow - size 4 
            //***Open Drainage Ditch - DashDotDot - Navy
            //***Water Pipeline - Solid - Navy
            //***Water Riser - Pin Blue - size 4
            //***Water Well - Well - size 2 - Red
            //***Wetlands - Circle - #0080FF 
        }

        private void ReturnStorageLocationStyles()
        {
            Dictionary<string, MapAnnotationCategory> storageLocationlist = new Dictionary<string, MapAnnotationCategory>();
            storageLocationlist.Add("Storage Facility", new MapAnnotationCategory() { Name = "Storage Facility", StyleName = "StorageFacility", ShapeType = "Polygon", Visible = true });
            storageLocationlist.Add("Potato Cellar", new MapAnnotationCategory() { Name = "Potato Cellar", StyleName = "PotatoCellar", ShapeType = "Polygon", Visible = true });
            storageLocationlist.Add("Grain Bins", new MapAnnotationCategory() { Name = "Grain Bins", StyleName = "GrainBins", ShapeType = "Polygon", Visible = true });
            storageLocationlist.Add("Packing House", new MapAnnotationCategory() { Name = "Packing House", StyleName = "PackingHouse", ShapeType = "Polygon", Visible = true });
            storageLocationlist.Add("Silo", new MapAnnotationCategory() { Name = "Silo", StyleName = "SilageStorage", ShapeType = "Polygon", Visible = true });
            storageLocationlist.Add("Silage Pit", new MapAnnotationCategory() { Name = "Silage Pit", StyleName = "SilageStorage2", ShapeType = "Polygon", Visible = true });
            storageLocationlist.Add("Silage Bag", new MapAnnotationCategory() { Name = "Silage Bag", StyleName = "SilageStorage3", ShapeType = "Polygon", Visible = true });
            storageLocationlist.Add("Silage Pile", new MapAnnotationCategory() { Name = "Silage Pile", StyleName = "SilageStorage4", ShapeType = "Polygon", Visible = true });

            storageLocationlist.Add("Grain Elevator", new MapAnnotationCategory() { Name = "Grain Elevator", StyleName = "GrainElevator", ShapeType = "Polygon", Visible = true });
            storageLocationlist.Add("Cotton Gin", new MapAnnotationCategory() { Name = "Cotton Gin", StyleName = "CottonGin", ShapeType = "Polygon", Visible = true });
            storageLocationlist.Add("Processor Facility", new MapAnnotationCategory() { Name = "Processor Facility", StyleName = "ProcessorFacility", ShapeType = "Polygon", Visible = true });
            storageLocationlist.Add("Ethanol Plant", new MapAnnotationCategory() { Name = "Ethanol Plant", StyleName = "FarmBuildings", ShapeType = "Polygon", Visible = true });

            storageLocationlist.Add("Building", new MapAnnotationCategory() { Name = "Building", StyleName = "Building", ShapeType = "Polygon", Visible = true });
            storageLocationlist.Add("Farm Buildings", new MapAnnotationCategory() { Name = "Farm Buildings", StyleName = "FarmBuildings", ShapeType = "Polygon", Visible = true });
            storageLocationlist.Add("Chemical Storage", new MapAnnotationCategory() { Name = "Chemical Storage", StyleName = "ChemicalStorage", ShapeType = "Polygon", Visible = true });
            storageLocationlist.Add("Farm Shop", new MapAnnotationCategory() { Name = "Farm Shop", StyleName = "FarmShop", ShapeType = "Polygon", Visible = true });
            storageLocationlist.Add("Feed Lot", new MapAnnotationCategory() { Name = "Feed Lot", StyleName = "FeedLot", ShapeType = "Polygon", Visible = true });

            StorageStyleNames = storageLocationlist;
        }

        private void InitializeFontStrings() {
            System.Drawing.Text.InstalledFontCollection fonts = new System.Drawing.Text.InstalledFontCollection();
            ListofFonts = new List<string>();
            foreach (var item in fonts.Families) {
                ListofFonts.Add(item.Name);
            }

            //Aharoni, Aldhabi, Andalus, Andy, Angsana New, AngsanaUPC, Aparajita, Arabic Typesetting, Arial, Arial Black, 
            //Arial Narrow, Arial Unicode MS, Arimo, Batang, BatangChe, Book Antiqua, Bookman Old Style, Bookshelf Symbol 7, 
            //Bradley Hand ITC, Browallia New, BrowalliaUPC, Buxton Sketch, Calibri, Calibri Light, Cambria, Cambria Math, 
            //Candara, Century, Century Gothic, Comic Sans MS, Consolas, Constantia, Corbel, Cordia New, CordiaUPC, Courier New, 
            //DaunPenh, David, DejaVu Sans, DejaVu Sans Condensed, DejaVu Sans Light, DejaVu Sans Mono, DejaVu Serif, 
            //DejaVu Serif Condensed, DengXian, DFKai-SB, DilleniaUPC, DokChampa, Dotum, DotumChe, Ebrima, Estrangelo Edessa, 
            //EucrosiaUPC, Euphemia, FangSong, Franklin Gothic Medium, FrankRuehl, FreesiaUPC, Freestyle Script, 
            //French Script MT, Gabriola, Gadugi, Garamond, Gautami, Gentium Basic, Gentium Book Basic, Georgia, Gisha, Gulim, 
            //GulimChe, Gungsuh, GungsuhChe, Impact, IrisUPC, Iskoola Pota, JasmineUPC, Javanese Text, Jing Jing, Juice ITC, 
            //KaiTi, Kalinga, Kartika, Khmer UI, KodchiangUPC, Kokila, Kootenay, Kristen ITC, Lao UI, Latha, Leelawadee, 
            //Leelawadee UI, Leelawadee UI Semilight, Levenim MT, LilyUPC, Lindsey, Lucida Console, Lucida Handwriting, 
            //Lucida Sans Unicode, 
            //Malgun Gothic, 
            //Mangal, 
            //Marlett, Meiryo, 
            //Meiryo UI, 
            //Microsoft Himalaya, 
            //Microsoft JhengHei, 
            //Microsoft JhengHei Light, 
            //Microsoft JhengHei UI, 
            //Microsoft JhengHei UI Light, 
            //Microsoft MHei, 
            //Microsoft NeoGothic, 
            //Microsoft New Tai Lue, 
            //Microsoft PhagsPa, 
            //Microsoft Sans Serif, 
            //Microsoft Tai Le, 
            //Microsoft Uighur, 
            //Microsoft YaHei, 
            //Microsoft YaHei Light, 
            //Microsoft YaHei UI, 
            //Microsoft YaHei UI Light, 
            //Microsoft Yi Baiti, 
            //MingLiU, 
            //MingLiU - ExtB, 
            //MingLiU_HKSCS, 
            //MingLiU_HKSCS - ExtB, 
            //Miramonte, 
            //Miriam, 
            //Miriam Fixed, 
            //Mistral, 
            //Moire, 
            //Moire ExtraBold, 
            //Moire Light, 
            //Mongolian Baiti, 
            //Monotype Corsiva, 
            //MoolBoran, 
            //Motorwerk, 
            //MS Gothic, 
            //MS Mincho, 
            //MS Outlook, 
            //MS PGothic, 
            //MS PMincho, 
            //MS Reference Sans Serif, 
            //MS Reference Specialty, 
            //MS UI Gothic, 
            //MT Extra, 
            //MV Boli, 
            //Myanmar Text, 
            //Narkisim, 
            //News Gothic, 
            //Nirmala UI, 
            //Nirmala UI Semilight, 
            //NSimSun, 
            //Nyala, 
            //OCR A Extended, 
            //OpenSymbol, 
            //Palatino Linotype, 
            //Papyrus, 
            //Pericles, 
            //Pericles Light, 
            //Pescadero, 
            //Plantagenet Cherokee, 
            //PMingLiU, 
            //PMingLiU -ExtB, 
            //Pristina, 
            //Quartz MS, 
            //Raavi, 
            //Rod, 
            //Sakkal Majalla, 
            //Segoe Keycaps, 
            //Segoe Marker, 
            //Segoe Script, 
            //Segoe UI, 
            //Segoe UI Black, 
            //Segoe UI Emoji, 
            //Segoe UI Light, 
            //Segoe UI Mono, 
            //Segoe UI Semibold, 
            //Segoe UI Semilight, 
            //Segoe UI Symbol, 
            //Segoe WP, 
            //Segoe WP Black, 
            //Segoe WP Light, 
            //Segoe WP Semibold, 
            //Segoe WP SemiLight, 
            //Shonar Bangla, 
            //Shruti, 
            //SimHei, 
            //Simplified Arabic, 
            //Simplified Arabic Fixed, 
            //SimSun, 
            //SimSun -ExtB, 
            //Sitka Banner, 
            //Sitka Display, 
            //Sitka Heading, 
            //Sitka Small, 
            //Sitka Subheading, 
            //Sitka Text, 
            //SketchFlow Print, 
            //Sylfaen, 
            //Symbol, 
            //Tahoma, 
            //Tempus Sans ITC, 
            //Times New Roman, 
            //Traditional Arabic, 
            //Trebuchet MS, 
            //Tunga, 
            //Urdu Typesetting, 
            //Utsaah, 
            //Vani, 
            //Verdana, 
            //Vijaya, 
            //Vrinda, 
            //Wasco Sans, 
            //Webdings, 
            //Wingdings, 
            //Wingdings 2, 
            //Wingdings 3, 
            //Yu Gothic, 
            //Yu Gothic Light, 
            //Yu Mincho, 
            //Yu Mincho Demibold, 
            //Yu Mincho Light, 




        }
    }
}
