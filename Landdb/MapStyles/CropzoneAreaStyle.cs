﻿using NLog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using ThinkGeo.MapSuite.Core;

namespace Landdb.MapStyles {
    // Mark the class Serializable so that it works in SQL Server state
    // and in every serialization context
    [Serializable]
    class CropzoneAreaStyle : AreaStyle {
        private AreaStyle selectedAreaStyle;
        private AreaStyle unselectedAreaStyle;
        private AreaStyle assignedAreaStyle;
        private TextStyle textStyle = null;
        private IList<Feature> selectedFeatures;
        private IList<Feature> assignedFeatures;
        Logger log = LogManager.GetCurrentClassLogger();

        public CropzoneAreaStyle()
            : this(new AreaStyle(), new AreaStyle(), new AreaStyle(), new TextStyle()) { }

        public CropzoneAreaStyle(AreaStyle selectedAreaStyle, AreaStyle unselectedAreaStyle, AreaStyle assignedAreaStyle, TextStyle textStyle) {
            this.selectedAreaStyle = selectedAreaStyle;
            this.unselectedAreaStyle = unselectedAreaStyle;
            this.assignedAreaStyle = assignedAreaStyle;
            this.textStyle = textStyle;
            this.selectedFeatures = new List<Feature>();
            this.assignedFeatures = new List<Feature>();
        }

        public AreaStyle SelectedAreaStyle {
            get { return selectedAreaStyle; }
            set { selectedAreaStyle = value; }
        }

        public AreaStyle UnselectedAreaStyle {
            get { return unselectedAreaStyle; }
            set { unselectedAreaStyle = value; }
        }

        public AreaStyle AssignedAreaStyle {
            get { return assignedAreaStyle; }
            set { assignedAreaStyle = value; }
        }

        public TextStyle TextStyle {
            get { return textStyle; }
            set { textStyle = value; }
        }

        public IList<Feature> SelectedFeatures {
            get { return selectedFeatures; }
            set { selectedFeatures = value; }
        }

        public IList<Feature> AssignedFeatures {
            get { return assignedFeatures; }
            set { assignedFeatures = value; }
        }


        protected override void DrawCore(IEnumerable<Feature> features, GeoCanvas canvas, Collection<SimpleCandidate> labelsInThisLayer, Collection<SimpleCandidate> labelsInAllLayers) {
            foreach (Feature feature in features) {
                try {
                    string featureid = feature.Id.ToString();
                    bool foundfeature = false;
                    foreach (Feature feat in SelectedFeatures) {
                        string featid = feat.Id.ToString();
                        if (featureid == featid) {
                            foundfeature = true;
                            break;
                        }
                    }
                    if (foundfeature) {
                        selectedAreaStyle.Draw(new Collection<Feature>() { feature }, canvas, labelsInThisLayer, labelsInAllLayers);
                    } else {
                        bool foundassignedfeature = false;
                        foreach (Feature feat in AssignedFeatures) {
                            string featid = feat.Id.ToString();
                            if (featureid == featid) {
                                foundassignedfeature = true;
                                break;
                            }
                        }
                        if (foundassignedfeature) {
                            assignedAreaStyle.Draw(new Collection<Feature>() { feature }, canvas, labelsInThisLayer, labelsInAllLayers);
                        } else {
                            unselectedAreaStyle.Draw(new Collection<Feature>() { feature }, canvas, labelsInThisLayer, labelsInAllLayers);
                            //if (textStyle != null) {
                            //textStyle.Draw(new Collection<Feature>() { feature }, canvas, labelsInThisLayer, labelsInAllLayers);
                            //}
                        }
                    }
                } catch (Exception ex) {
                    log.WarnException($"Exception in {nameof(DrawCore)}", ex);
                }
            }
        }

        protected override Collection<string> GetRequiredColumnNamesCore() {
            return base.GetRequiredColumnNamesCore();
        }
    }
}