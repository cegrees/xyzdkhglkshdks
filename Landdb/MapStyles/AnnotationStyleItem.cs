﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThinkGeo.MapSuite.Core;

namespace Landdb.MapStyles {
    public class AnnotationStyleItem {
        public AnnotationStyleItem(string typename, string shapetype, Style annotationstyle) {
            this.typename = typename;
            this.shapetype = shapetype;
            this.annotationstyle = annotationstyle;
            this.visible = true;
        }

        private string typename = string.Empty;
        public string TypeName {
            get { return typename; }
            set {
                if (typename == value) { return; }
                typename = value;
            }
        }

        private string shapetype = string.Empty;
        public string ShapeType {
            get { return shapetype; }
            set {
                if (shapetype == value) { return; }
                shapetype = value;
            }
        }

        private Style annotationstyle = null;
        public Style AnnotationStyle {
            get { return annotationstyle; }
            set {
                if (annotationstyle == value) { return; }
                annotationstyle = value;
            }
        }

        private GeoColor fillcolor = new GeoColor();
        public GeoColor FillColor {
            get { return fillcolor; }
            set {
                if (fillcolor == value) { return; }
                fillcolor = value;
            }
        }

        private GeoColor pencolor = new GeoColor();
        public GeoColor PenColor {
            get { return pencolor; }
            set {
                if (pencolor == value) { return; }
                pencolor = value;
            }
        }

        private float pensize = 10;
        public float PenSize {
            get { return pensize; }
            set {
                if (pensize == value) { return; }
                pensize = value;
            }
        }

        private bool visible = true;
        public bool Visible {
            get { return visible; }
            set {
                if (visible == value) { return; }
                visible = value;
            }
        }

        private void SetStyleOpacity(Style setstyle) {
            Style newstylevisibility = setstyle;
            if (setstyle is AreaStyle) {
                AreaStyle astyle = setstyle as AreaStyle;
                FillColor = astyle.FillSolidBrush.Color;
                PenColor = astyle.OutlinePen.Color;
                PenSize = astyle.OutlinePen.Width;
            }
            if (setstyle is LineStyle) {
                LineStyle astyle = setstyle as LineStyle;
                FillColor = astyle.CenterPen.Color;
                PenColor = astyle.CenterPen.Color;
                PenSize = astyle.CenterPen.Width;
            }
            if (setstyle is PointStyle) {
                PointStyle astyle = setstyle as PointStyle;
                FillColor = astyle.SymbolSolidBrush.Color;
                PenColor = astyle.SymbolSolidBrush.Color;
                PenSize = astyle.SymbolSize;
            }
        }
    }
}

