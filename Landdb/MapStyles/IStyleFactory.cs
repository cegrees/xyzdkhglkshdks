﻿using Landdb.Client.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThinkGeo.MapSuite.Core;

namespace Landdb.MapStyles {
    public interface IStyleFactory {
        AnnotationStyleItem GetStyle(string styledisplayname, string shapetype);
        IList<AnnotationStyleItem> GetStyleList(string shapetype);
        MapAnnotationCategory GetMapAnnotationCategory(string styledisplayname);
        Style SetStyleOpacity(Style setstyle, int opacity);
    }
}
