﻿using Landdb.Resources;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Landdb.Client.Infrastructure.DisplayItems;
using ThinkGeo.MapSuite.Core;

namespace Landdb.MapStyles
{
    // Mark the class Serializable so that it works in SQL Server state
    // and in every serialization context
    [Serializable]
    internal class ColoredMapGeneratorMultiAreaStyle : AreaStyle
    {
        private Style selectedAreaStyle;
        private Style unselectedAreaStyle;
        private TextStyle textStyle = null;
        private Dictionary<string, AreaStyle> colorstyles;
        private Dictionary<string, string> unselectedFeatures;
        private List<string> columnslist;
        private bool displayallcropzones = false;

        public ColoredMapGeneratorMultiAreaStyle()
            : this(new AreaStyle(), new AreaStyle(), new TextStyle())
        {
            colorstyles = new Dictionary<string, AreaStyle>();
            unselectedFeatures = new Dictionary<string, string>();
        }

        public ColoredMapGeneratorMultiAreaStyle(Style selectedAreaStyle, Style unselectedAreaStyle, TextStyle textStyle)
        {
            this.selectedAreaStyle = selectedAreaStyle;
            this.unselectedAreaStyle = unselectedAreaStyle;
            this.textStyle = textStyle;
            this.displayallcropzones = false;
            unselectedFeatures = new Dictionary<string, string>();
        }

        public Style SelectedAreaStyle
        {
            get { return selectedAreaStyle; }
            set { selectedAreaStyle = value; }
        }

        public Style UnselectedAreaStyle
        {
            get { return unselectedAreaStyle; }
            set { unselectedAreaStyle = value; }
        }

        public TextStyle TextStyle {
            get { return textStyle; }
            set { textStyle = value; }
        }

        public CropMapsFarmDisplayItem SelectedFarm { get; set; }

        public List<string> ColumnsList {
            get { return columnslist; }
            set { columnslist = value; }
        }

        public bool DisplayAllCropzones {
            get { return displayallcropzones; }
            set { displayallcropzones = value; }
        }

        public Dictionary<string, AreaStyle> ColorStyles {
            get { return colorstyles; }
            set { colorstyles = value; }
        }

        public Dictionary<string, string> UnselectedFeatures
        {
            get { return unselectedFeatures; }
            set { unselectedFeatures = value; }
        }

        protected override void DrawCore(IEnumerable<Feature> features, GeoCanvas canvas, Collection<SimpleCandidate> labelsInThisLayer, Collection<SimpleCandidate> labelsInAllLayers)
        {
            foreach (Feature feature in features)
            {
                try {
                    string farmName = string.Empty;
                    try {
                        farmName = feature.ColumnValues[@"Farm"];
                    }
                    catch { }
                    string featureid = feature.Id.ToString();
                    bool foundfeature = false;
                    if (ColorStyles.ContainsKey(featureid)) {
                        if (SelectedFarm.IsAllFarms || SelectedFarm.DisplayText == string.Empty || SelectedFarm.DisplayText == farmName) {
                            selectedAreaStyle = colorstyles[featureid];
                            foundfeature = true;
                        }
                    }
                    if (foundfeature && unselectedFeatures.Count > 0) {
                        if (unselectedFeatures.ContainsKey(featureid)) {
                            foundfeature = false;
                        }
                    }
                    if (foundfeature) {
                        selectedAreaStyle.Draw(new Collection<Feature>() { feature }, canvas, labelsInThisLayer, labelsInAllLayers);
                        //if (textStyle != null) {
                        //    textStyle.Draw(new Collection<Feature>() { feature }, canvas, labelsInThisLayer, labelsInAllLayers);
                        //}
                    }
                    else {
                        if (colorstyles.Count == 0 || displayallcropzones) {
                            //if (unselectedAreaStyle is GradientStyle && feature.ColumnValues.Count < 2) { continue; }
                            unselectedAreaStyle.Draw(new Collection<Feature>() { feature }, canvas, labelsInThisLayer, labelsInAllLayers);
                            //if (textStyle != null) {
                            //    textStyle.Draw(new Collection<Feature>() { feature }, canvas, labelsInThisLayer, labelsInAllLayers);
                            //}
                        }
                    }
                }
                catch (Exception ex) {
                    int xxx = 0;
                }
            }
        }

        protected override System.Collections.ObjectModel.Collection<string> GetRequiredColumnNamesCore() {
            Collection<string> columns = new Collection<string>();
            foreach (string col in columnslist) {
                if (!columns.Contains(col)) {
                    columns.Add(col);
                }
            }
            return columns;
        }

    }
}
