﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using ThinkGeo.MapSuite.Core;

namespace Landdb.MapStyles
{
    public class CustomDistanceLineStyleSimple : LineStyle
    {
        private GeoFont geoFont;
        private GeoSolidBrush geoSolidBrush;
        private GeoPen geoPen;
        private DistanceUnit distanceUnit;

        public CustomDistanceLineStyleSimple()
            : this(DistanceUnit.Meter,new GeoFont(), new GeoSolidBrush(), new GeoPen())
        {}

        public CustomDistanceLineStyleSimple( DistanceUnit distanceUnit, GeoFont geoFont, GeoSolidBrush geoSolidBrush, GeoPen geoPen)
        {
            this.distanceUnit = distanceUnit;
            this.geoFont = geoFont;
            this.geoSolidBrush = geoSolidBrush;
            this.geoPen = geoPen;
        }

        //For the font of the distance
        public GeoFont GeoFont
        {
            get { return geoFont; }
            set { geoFont = value; }
        }

        //For the color of the font
        public GeoSolidBrush GeoSolidBrush
        {
            get { return geoSolidBrush; }
            set { geoSolidBrush = value; }
        }

        //For the color of the halo of the font
        public GeoPen GeoPen
        {
            get { return geoPen; }
            set { geoPen = value; }
        }

        //For the distance unit used.
        public DistanceUnit DistanceUnit
        {
            get { return distanceUnit; }
            set { distanceUnit = value; }
        }

        protected override void DrawCore(System.Collections.Generic.IEnumerable<Feature> features, GeoCanvas canvas, System.Collections.ObjectModel.Collection<SimpleCandidate> labelsInThisLayer, System.Collections.ObjectModel.Collection<SimpleCandidate> labelsInAllLayers)
        {
            //Loops thru the features to to label the distance of each line segment at mid point.
            foreach (Feature feature in features)
            {
                    LineShape lineShape = (LineShape)feature.GetShape();
                    for (int i = 0; i < lineShape.Vertices.Count - 1; i++)
                    {
                        PointShape pointShape1 = new PointShape(lineShape.Vertices[i]);
                        PointShape pointShape2 = new PointShape(lineShape.Vertices[i + 1]);
                        double currentDist = Math.Round(pointShape1.GetDistanceTo(pointShape2, canvas.MapUnit,distanceUnit),2);

                        LineShape tempLineShape = new LineShape();
                        tempLineShape.Vertices.Add(new Vertex(pointShape1));
                        tempLineShape.Vertices.Add(new Vertex(pointShape2));

                        PointShape midPointShape = tempLineShape.GetPointOnALine(StartingPoint.FirstPoint, 50);
                        Collection<ScreenPointF> screenPointFs = new Collection<ScreenPointF>();

                        screenPointFs.Add(ExtentHelper.ToScreenCoordinate(canvas.CurrentWorldExtent, midPointShape, canvas.Width, canvas.Height));

                        double angle = GetAngleFromTwoVertices(tempLineShape.Vertices[0], tempLineShape.Vertices[1]);

                        canvas.DrawText(currentDist.ToString(),geoFont,GeoSolidBrush, geoPen,screenPointFs,DrawingLevel.LabelLevel,0,0,(float)angle);
                      }
                }
        }

        private double GetAngleFromTwoVertices(Vertex b, Vertex c)
        {
            double result;
            double alpha = 0;
            double tangentAlpha = (c.Y - b.Y) / (c.X - b.X);
            double Peta = Math.Atan(tangentAlpha);

            if (c.X > b.X)
            {
                alpha = 90 + (Peta * (180 / Math.PI));
            }
            else if (c.X < b.X)
            {
                alpha = 270 + (Peta * (180 / Math.PI));
            }
            else
            {
                if (c.Y > b.Y) alpha = 0;
                if (c.Y < b.Y) alpha = 180;
            }

            double offset;
            if (b.X > c.X)
            { offset = 90; }
            else { offset = - 90; }

            result = alpha + offset;
            return result;   
        }
    }
}
