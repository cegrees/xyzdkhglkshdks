﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using ThinkGeo.MapSuite.Core;

namespace Landdb.MapStyles
{
    public class FastEditPolygonLineStyle : LineStyle
    {
        private GeoFont geoFont;
        private GeoSolidBrush geoSolidBrush;
        private GeoPen geoPen;
        private DistanceUnit distanceUnit;
        Proj4Projection projection;

        public FastEditPolygonLineStyle()
            : this(DistanceUnit.Meter,new GeoFont(), new GeoSolidBrush(), new GeoPen())
        {}

        public FastEditPolygonLineStyle( DistanceUnit distanceUnit, GeoFont geoFont, GeoSolidBrush geoSolidBrush, GeoPen geoPen)
        {
            this.distanceUnit = distanceUnit;
            this.geoFont = geoFont;
            this.geoSolidBrush = geoSolidBrush;
            this.geoPen = geoPen;
            projection = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetGoogleMapParametersString());
            projection.Open();
        }

        public GeoFont GeoFont
        {
            get { return geoFont; }
            set { geoFont = value; }
        }

        public GeoSolidBrush GeoSolidBrush
        {
            get { return geoSolidBrush; }
            set { geoSolidBrush = value; }
        }

        public GeoPen GeoPen
        {
            get { return geoPen; }
            set { geoPen = value; }
        }

        public DistanceUnit DistanceUnit
        {
            get { return distanceUnit; }
            set { distanceUnit = value; }
        }

        protected override void DrawCore(System.Collections.Generic.IEnumerable<Feature> features, GeoCanvas canvas, System.Collections.ObjectModel.Collection<SimpleCandidate> labelsInThisLayer, System.Collections.ObjectModel.Collection<SimpleCandidate> labelsInAllLayers)
        {
            try {
                foreach (var feature in features) {
                    BaseShape shape = feature.GetShape();
                    LineShape lineShape = shape as LineShape;
                    if (lineShape != null && lineShape.Vertices.Count > 0) {

                        LineShape baselineshape3 = projection.ConvertToInternalProjection(lineShape) as LineShape;
                        double currentDist = System.Math.Round(baselineshape3.GetLength(GeographyUnit.DecimalDegree, DistanceUnit));

                        //double currentDist = System.Math.Round(lineShape.GetLength(canvas.MapUnit, distanceUnit));
                        PointShape pointShape = new PointShape(lineShape.Vertices[0]);
                        ScreenPointF ScreenPointF = ExtentHelper.ToScreenCoordinate(canvas.CurrentWorldExtent, pointShape, canvas.Width, canvas.Height);
                        canvas.DrawTextWithScreenCoordinate(System.Convert.ToString(currentDist), geoFont, geoSolidBrush, geoPen, ScreenPointF.X + 35, ScreenPointF.Y, DrawingLevel.LabelLevel);
                        //canvas.DrawTextWithScreenCoordinate(System.Convert.ToString(currentDist), new GeoFont("Arial", 14, DrawingFontStyles.Bold), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.Black, 3), ScreenPointF.X + 35, ScreenPointF.Y, DrawingLevel.LabelLevel);
                    }
                }
            }
            catch (Exception ex) {
                int xxx = 0;
            }
        }
    }
}
