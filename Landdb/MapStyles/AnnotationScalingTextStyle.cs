﻿using NLog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using ThinkGeo.MapSuite.Core;

namespace Landdb.MapStyles {
    public class AnnotationScalingTextStyle : TextStyle {
        private GeographyUnit mapUnit;
        private double maximumScale;
        private double minimumScale;
        private double mapScale;
        private float maximumLabelSize;
        private float minimumLabelSize;
        private Dictionary<string, double> rotationAngles;
        private Dictionary<string, PointShape> labelPositions;
        private Dictionary<string, string> customLabels;
        private List<string> columnslist;
        IStyleFactory styleFactory;
        Logger log = LogManager.GetCurrentClassLogger();

        public AnnotationScalingTextStyle()
            : this(GeographyUnit.DecimalDegree, string.Empty, double.MaxValue, 0, 16, 4) {
            styleFactory = new StyleFactory();
            rotationAngles = new Dictionary<string, double>();
            labelPositions = new Dictionary<string, PointShape>();
            customLabels = new Dictionary<string, string>();
        }

        public AnnotationScalingTextStyle(GeographyUnit MapUnit, string TextColumnName, double MaximumScale, double MinimumScale, float MaximumLabelSize, float MinimumLabelSize) {
            styleFactory = new StyleFactory();
            this.mapUnit = MapUnit;
            this.TextColumnName = TextColumnName;
            this.maximumScale = MaximumScale;
            this.minimumScale = MinimumScale;
            this.mapScale = MinimumScale;
            this.maximumLabelSize = MaximumLabelSize;
            this.minimumLabelSize = MinimumLabelSize;
            rotationAngles = new Dictionary<string, double>();
            labelPositions = new Dictionary<string, PointShape>();
            customLabels = new Dictionary<string, string>();
        }

        // This is the map unit to determine the scale.
        public GeographyUnit MapUnit {
            get { return mapUnit; }
            set { mapUnit = value; }
        }

        // The maximum scale will be the largest scale used to calculate the size of the label.
        // If the scale gets larger than the maximum, then we compute the scaling based on
        // this number instead.  This means that after this scale, the size of the label will not get
        // any smaller no matter how much more you zoom out.
        public double MaximumScale {
            get { return maximumScale; }
            set { maximumScale = value; }
        }

        // The minimum scale will be the smallest scale used to calculate the size of the label.
        // If the scale gets smaller than the minimum, then we compute the scaling based on
        // this number instead.  This means that after this scale, the size of the label will not get
        // any larger no matter how much more you zoom in.
        public double MinimumScale {
            get { return minimumScale; }
            set { minimumScale = value; }
        }

        public double MapScale {
            get { return mapScale; }
            set { mapScale = value; }
        }

        // The MaximumSize is the size of the label at MinimumScale and lower.
        public float MaximumLabelSize {
            get { return maximumLabelSize; }
            set { maximumLabelSize = value; }
        }

        // The MinimumSize is the size of the label at MaximumScale and higher.
        public float MinimumLabelSize {
            get { return minimumLabelSize; }
            set { minimumLabelSize = value; }
        }

        public Dictionary<string, double> RotationAngles {
            get { return rotationAngles; }
            set { rotationAngles = value; }
        }

        public new Dictionary<string, PointShape> LabelPositions {
            get { return labelPositions; }
            set { labelPositions = value; }
        }

        public Dictionary<string, string> CustomLabels {
            get { return customLabels; }
            set {
                customLabels = value;
            }
        }

        public List<string> ColumnsList {
            get { return columnslist; }
            set { columnslist = value; }
        }

        public void SetTexTColumnName(string TextColumnName) {
            this.TextColumnName = TextColumnName;
        }

        protected override Collection<LabelingCandidate> GetLabelingCandidateCore(Feature feature, GeoCanvas canvas) {
            Collection<LabelingCandidate> labelingCandidates = base.GetLabelingCandidateCore(feature, canvas);
            if (rotationAngles.ContainsKey(feature.Id)) {
                foreach (LabelingCandidate item in labelingCandidates) {
                    item.LabelInformation[0].RotationAngle = rotationAngles[feature.Id];
                }
            }
            return labelingCandidates;
        }

        protected override void DrawCore(IEnumerable<Feature> features, GeoCanvas canvas,
            System.Collections.ObjectModel.Collection<SimpleCandidate> labelsInThisLayer,
            System.Collections.ObjectModel.Collection<SimpleCandidate> labelsInAllLayers) {

            // Loop through all of the features being passed in to draw.
            foreach (Feature feature in features) {
                Collection<LabelingCandidate> labelingCandidates = GetLabelingCandidates(feature, canvas);
                foreach (LabelingCandidate labelingCandidate in labelingCandidates) {
                    try {
                        // Let's make sure the features being passed in are points or multi points.
                        WellKnownType shapeWellKnownType = feature.GetWellKnownType();
                        if (shapeWellKnownType == WellKnownType.Multipolygon || shapeWellKnownType == WellKnownType.Polygon || shapeWellKnownType == WellKnownType.Line || shapeWellKnownType == WellKnownType.Multiline || shapeWellKnownType == WellKnownType.Point || shapeWellKnownType == WellKnownType.Multipoint) {
                            double currentScale = ExtentHelper.GetScale(canvas.CurrentWorldExtent, canvas.Width, mapUnit);
                            currentScale = this.mapScale;
                            // Enforce the minimum and maximum scale properties
                            if (currentScale > maximumScale) { currentScale = maximumScale; }
                            if (currentScale < minimumScale) { currentScale = minimumScale; }

                            double diff1 = currentScale - minimumScale;
                            double range1 = maximumScale - minimumScale;
                            double percent1 = diff1 / range1;
                            double inverse1 = 1 - percent1;
                            double frange1 = maximumLabelSize - minimumLabelSize;
                            double fscale1 = inverse1 * frange1;
                            double fontsize1 = fscale1 + minimumLabelSize;
                            float labelSize = (float)fontsize1;

                            if (CheckOverlapping(labelingCandidate, canvas, labelsInThisLayer, labelsInAllLayers)) { continue; }

                            SimpleCandidate simpleCandidate = new SimpleCandidate(labelingCandidate.OriginalText, labelingCandidate.ScreenArea);
                            if (labelsInAllLayers != null) { labelsInAllLayers.Add(simpleCandidate); }
                            if (labelsInThisLayer != null) { labelsInThisLayer.Add(simpleCandidate); }

                            string Text = feature.ColumnValues[TextColumnName];
                            if (CustomLabels != null && CustomLabels.Count > 0) {
                                if (CustomLabels.ContainsKey(feature.Id)) {
                                    Text = CustomLabels[feature.Id];
                                }
                            }
                            // Call the canvas method to draw the label scaled.
                            PointShape point = new PointShape();

                            //point = poly.GetCenterPoint();

                            BaseShape bs = feature.GetShape();
                            this.YOffsetInPixel = 0;

                            if (bs is PolygonShape) {
                                PolygonShape ps = bs as PolygonShape;
                                point = ps.GetCenterPoint();
                            }

                            if (bs is MultipolygonShape) {
                                MultipolygonShape mps = bs as MultipolygonShape;
                                point = mps.GetCenterPoint();
                            }

                            if (bs is LineShape) {
                                LineShape ls = bs as LineShape;
                                point = ls.GetCenterPoint();
                            }

                            if (bs is MultilineShape) {
                                MultilineShape mls = bs as MultilineShape;
                                point = mls.GetCenterPoint();
                            }

                            if (bs is PointShape) {
                                PointShape pnt = bs as PointShape;
                                point = pnt;
                                this.YOffsetInPixel = 20;
                            }

                            if (bs is MultipointShape) {
                                MultipointShape mpnt = bs as MultipointShape;
                                point = mpnt.GetCenterPoint();
                                this.YOffsetInPixel = 20;
                            }

                            if (LabelPositions != null && LabelPositions.Count > 0) {
                                if (LabelPositions.ContainsKey(feature.Id)) {
                                    point = (PointShape)LabelPositions[feature.Id];
                                }
                            }

                            //Convert from world coordinate to screen coordinate the location of the feature.
                            ScreenPointF textScreenPoint = ExtentHelper.ToScreenCoordinate(canvas.CurrentWorldExtent, point, canvas.Width, canvas.Height);

                            this.RotationAngle = 0;
                            if (RotationAngles != null && RotationAngles.Count > 0) {
                                if (RotationAngles.ContainsKey(feature.Id)) {
                                    this.RotationAngle = (double)RotationAngles[feature.Id];
                                }
                            }

                            //Is annotation visible?
                            string annotationtype = string.Empty;
                            try {
                                annotationtype = feature.ColumnValues[@"TypeName"];
                            } catch { }
                            feature.GetShape();
                            bool foundfeature = false;
                            AnnotationStyleItem annotatonstyle = styleFactory.GetStyle(annotationtype, "");
                            if (annotatonstyle != null) {
                                if (annotatonstyle.Visible) {
                                    foundfeature = true;
                                }
                            }

                            if (foundfeature) {
                                canvas.DrawText(Text, new GeoFont("Arial", labelSize), new GeoSolidBrush(GeoColor.StandardColors.Black),
                                                null, new ThinkGeo.MapSuite.Core.ScreenPointF[] { textScreenPoint }, DrawingLevel.LabelLevel,
                                                this.XOffsetInPixel, this.YOffsetInPixel, (float)this.RotationAngle); //- labelSize
                            }
                        }
                    } catch (Exception ex) {
                        log.WarnException($"Exception in {nameof(DrawCore)}", ex);
                    }
                }
            }
        }

        protected override System.Collections.ObjectModel.Collection<string> GetRequiredColumnNamesCore() {
            Collection<string> columns = new Collection<string>();
            foreach (string col in columnslist) {
                if (!columns.Contains(col)) {
                    columns.Add(col);
                }
            }
            return columns;
        }
    }
}