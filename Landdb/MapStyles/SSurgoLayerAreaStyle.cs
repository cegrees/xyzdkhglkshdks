﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using ThinkGeo.MapSuite.Core;

namespace Landdb.MapStyles
{
    // Mark the class Serializable so that it works in SQL Server state
    // and in every serialization context
    [Serializable]
    class SSurgoLayerAreaStyle : AreaStyle
    {
        private AreaStyle selectedAreaStyle;
        private AreaStyle unselectedAreaStyle;
        private TextStyle textStyle = null;
        private Dictionary<string, string> ssurgocolors;
        private Dictionary<string, AreaStyle> areastyles;
        private string colorcolumnlabel;
        private int opacity = 100;
        private Random rand = new Random();

        public SSurgoLayerAreaStyle()
            : this(new AreaStyle(), new TextStyle(), new Dictionary<string, string>(), string.Empty, 100)
        { }

        public SSurgoLayerAreaStyle(AreaStyle selectedAreaStyle, TextStyle textStyle, Dictionary<string, string> ssurgocolors, string colorcolumnlabel, int opacity)
        {
            this.selectedAreaStyle = selectedAreaStyle;
            this.textStyle = textStyle;
            this.ssurgocolors = ssurgocolors;
            this.colorcolumnlabel = colorcolumnlabel;
            this.opacity = opacity;
        }

        public AreaStyle SelectedAreaStyle
        {
            get { return selectedAreaStyle; }
            set { selectedAreaStyle = value; }
        }

        public TextStyle TextStyle {
            get { return textStyle; }
            set { textStyle = value; }
        }

        public Dictionary<string, string> SSurgoColors {
            get { return ssurgocolors; }
            set { ssurgocolors = value; }
        }

        public string ColorColumnLabel {
            get { return colorcolumnlabel; }
            set { colorcolumnlabel = value; }
        }

        public int Opacity {
            get { return opacity; }
            set { opacity = value; }
        }

        protected override void DrawCore(IEnumerable<Feature> features, GeoCanvas canvas, Collection<SimpleCandidate> labelsInThisLayer, Collection<SimpleCandidate> labelsInAllLayers)
        {
            GeoPen pen = new GeoPen(GeoColor.StandardColors.Black, 2);
            AreaStyle astyle = new AreaStyle(pen, new GeoSolidBrush(GeoColor.StandardColors.Transparent));

            foreach (Feature feature in features)
            {
                string featureid = feature.Id.ToString();
                bool foundfeature = false;
                string featuredata = feature.ColumnValues[colorcolumnlabel];
                GeoColor color = new GeoColor();
                if (areastyles.ContainsKey(featuredata)) {
                    astyle = areastyles[featuredata];
                }
                else {
                    if (SSurgoColors.ContainsKey(featuredata)) {
                        color = new GeoColor(Opacity, GeoColor.FromHtml(SSurgoColors[featuredata]));
                    }
                    else {
                        GeoColor color2 = GeoColor.FromArgb(255, rand.Next(0, 255), rand.Next(0, 255), rand.Next(0, 255));
                        SSurgoColors.Add(featuredata, GeoColor.ToHtml(color2));
                        color = new GeoColor(Opacity, color2);
                    }
                    astyle = new AreaStyle(pen, new GeoSolidBrush(color));
                    areastyles.Add(featuredata, astyle);
                }
                astyle.Draw(new Collection<Feature>() { feature }, canvas, labelsInThisLayer, labelsInAllLayers);
            }
        }

        protected override Collection<string> GetRequiredColumnNamesCore()
        {
            return base.GetRequiredColumnNamesCore();
        }
    }
}
