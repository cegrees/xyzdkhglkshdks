﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThinkGeo.MapSuite.Core;

namespace Landdb.MapStyles {
    public class AnnotationUserItem {
        public AnnotationUserItem(string typedisplay, string typename, string shapetype, bool visibility) {
            this.typedisplay = typedisplay;
            this.typename = typename;
            this.shapetype = shapetype;
            this.visibility = visibility;
        }

        private string typedisplay = string.Empty;
        public string TypeDisplay {
            get { return typedisplay; }
            set {
                if (typedisplay == value) { return; }
                typedisplay = value;
            }
        }

        private string typename = string.Empty;
        public string TypeName {
            get { return typename; }
            set {
                if (typename == value) { return; }
                typename = value;
            }
        }

        private string shapetype = string.Empty;
        public string ShapeType {
            get { return shapetype; }
            set {
                if (shapetype == value) { return; }
                shapetype = value;
            }
        }

        private bool visibility = true;
        public bool Visibility {
            get { return visibility; }
            set {
                if (visibility == value) { return; }
                visibility = value;
            }
        }
    }
}

