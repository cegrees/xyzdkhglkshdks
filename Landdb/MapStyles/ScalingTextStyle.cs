﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using ThinkGeo.MapSuite.Core;

namespace Landdb.MapStyles
{
     public class ScalingTextStyle : TextStyle
    {
        private GeographyUnit mapUnit;
        private double maximumScale;
        private double minimumScale;
        private double mapScale;
        private float maximumLabelSize;
        private float minimumLabelSize;
        private Dictionary<string, double> rotationAngles;
        private Dictionary<string, PointShape> labelPositions;
        private Dictionary<string, string> customLabels;
        private bool fittingpolygonlabel = false;
        private bool wraplabeltext = false;
        private bool unselectedfieldvisibility = true;
        private bool mandateoverlapping = false;
        private IList<Feature> selectedFeatures;
        private Proj4Projection positionprojection;

        public ScalingTextStyle()
            : this(GeographyUnit.DecimalDegree, string.Empty, double.MaxValue, 0, 16, 4)
        {
            rotationAngles = new Dictionary<string, double>();
            labelPositions = new Dictionary<string, PointShape>();
            customLabels = new Dictionary<string, string>();
            selectedFeatures = new List<Feature>();
        }

        public ScalingTextStyle(GeographyUnit MapUnit, string TextColumnName, double MaximumScale, double MinimumScale, float MaximumLabelSize, float MinimumLabelSize)
        {
            this.mapUnit = MapUnit;
            this.TextColumnName = TextColumnName;
            this.maximumScale = MaximumScale;
            this.minimumScale = MinimumScale;
            this.mapScale = MinimumScale;
            this.maximumLabelSize = MaximumLabelSize;
            this.minimumLabelSize = MinimumLabelSize;
            rotationAngles = new Dictionary<string, double>();
            labelPositions = new Dictionary<string, PointShape>();
            customLabels = new Dictionary<string, string>();
            selectedFeatures = new List<Feature>();
        }

        // This is the map unit to determine the scale.
        public GeographyUnit MapUnit
        {
            get { return mapUnit; }
            set { mapUnit = value; }
        }

        // The maximum scale will be the largest scale used to calculate the size of the label.
        // If the scale gets larger than the maximum, then we compute the scaling based on
        // this number instead.  This means that after this scale, the size of the label will not get
        // any smaller no matter how much more you zoom out.
        public double MaximumScale
        {
            get { return maximumScale; }
            set { maximumScale = value; }
        }

        // The minimum scale will be the smallest scale used to calculate the size of the label.
        // If the scale gets smaller than the minimum, then we compute the scaling based on
        // this number instead.  This means that after this scale, the size of the label will not get
        // any larger no matter how much more you zoom in.
        public double MinimumScale
        {
            get { return minimumScale; }
            set { minimumScale = value; }
        }

        public double MapScale {
            get { return mapScale; }
            set { mapScale = value; }
        }

        // The MaximumSize is the size of the label at MinimumScale and lower.
        public float MaximumLabelSize
        {
            get { return maximumLabelSize; }
            set { maximumLabelSize = value; }
        }

        // The MinimumSize is the size of the label at MaximumScale and higher.
        public float MinimumLabelSize
        {
            get { return minimumLabelSize; }
            set { minimumLabelSize = value; }
        }

        public bool FittingPolygonLabel {
            get { return fittingpolygonlabel; }
            set { fittingpolygonlabel = value; }
        }

        public bool WrapLabelText {
            get { return wraplabeltext; }
            set { wraplabeltext = value; }
        }

        public bool MandateOverlapping {
            get { return mandateoverlapping; }
            set { mandateoverlapping = value; }
        }

        public Dictionary<string, double> RotationAngles {
            get { return rotationAngles; }
            set { rotationAngles = value; }
        }

        public new Dictionary<string, PointShape> LabelPositions {
            get { return labelPositions; }
            set { labelPositions = value; }
        }
        public Proj4Projection PositionProjection {
            get { return positionprojection; }
            set { positionprojection = value; }
        }

        public Dictionary<string, string> CustomLabels {
            get { return customLabels; }
            set {
                customLabels = value;
            }
        }

        public void SetTexTColumnName(string TextColumnName) {
            this.TextColumnName = TextColumnName;
        }

        public bool UnselectedFieldVisibility {
            get { return unselectedfieldvisibility; }
            set { unselectedfieldvisibility = value; }
        }

        public IList<Feature> SelectedFeatures {
            get { return selectedFeatures; }
            set { selectedFeatures = value; }
        }

        protected override Collection<LabelingCandidate> GetLabelingCandidateCore(Feature feature, GeoCanvas canvas) {
            Collection<LabelingCandidate> labelingCandidates = base.GetLabelingCandidateCore(feature, canvas);
            if (rotationAngles.ContainsKey(feature.Id)) {
                foreach (LabelingCandidate item in labelingCandidates) {
                    item.LabelInformation[0].RotationAngle = rotationAngles[feature.Id];
                }
            }
            return labelingCandidates;
        }

        protected override void DrawCore(IEnumerable<Feature> features, GeoCanvas canvas,
            System.Collections.ObjectModel.Collection<SimpleCandidate> labelsInThisLayer,
            System.Collections.ObjectModel.Collection<SimpleCandidate> labelsInAllLayers)
        {
            
            // Loop through all of the features being passed in to draw.
            foreach (Feature feature in features)
            {
                Collection<LabelingCandidate> labelingCandidates = GetLabelingCandidates(feature, canvas);
                foreach (LabelingCandidate labelingCandidate in labelingCandidates)
                {
                    try {
                        // Let's make sure the features being passed in are points or multi points.
                        WellKnownType shapeWellKnownType = feature.GetWellKnownType();
                        if (shapeWellKnownType == WellKnownType.Multipolygon || shapeWellKnownType == WellKnownType.Polygon || shapeWellKnownType == WellKnownType.Line || shapeWellKnownType == WellKnownType.Multiline || shapeWellKnownType == WellKnownType.Point || shapeWellKnownType == WellKnownType.Multipoint) {
                            double currentScale = ExtentHelper.GetScale(canvas.CurrentWorldExtent, canvas.Width, mapUnit);
                            currentScale = this.mapScale;
                            // Enforce the minimum and maximum scale properties
                            if (currentScale > maximumScale) { currentScale = maximumScale; }
                            if (currentScale < minimumScale) { currentScale = minimumScale; }

                            double diff1 = currentScale - minimumScale;
                            double range1 = maximumScale - minimumScale;
                            double percent1 = diff1 / range1;
                            double inverse1 = 1 - percent1;
                            double frange1 = maximumLabelSize - minimumLabelSize;
                            double fscale1 = inverse1 * frange1;
                            double fontsize1 = fscale1 + minimumLabelSize;
                            float labelSize = (float)fontsize1;

                            bool checkoverlapping = CheckOverlapping(labelingCandidate, canvas, labelsInThisLayer, labelsInAllLayers);
                            if (mandateoverlapping && checkoverlapping) {
                                checkoverlapping = false;
                            }
                            if (checkoverlapping) { continue; }

                            SimpleCandidate simpleCandidate = new SimpleCandidate(labelingCandidate.OriginalText, labelingCandidate.ScreenArea);
                            if (labelsInAllLayers != null) { labelsInAllLayers.Add(simpleCandidate); }
                            if (labelsInThisLayer != null) { labelsInThisLayer.Add(simpleCandidate); }

                            //string featureidstring = feature.Id.ToString();
                            //if (featureidstring == "0b6c7379-5988-4912-b518-de1a3082613b") {
                            //    int iiiii = 0;
                            //}
                            //if (featureidstring == "b9f3e782-fba7-4430-80ef-4b5839a0522c") {
                            //    int iiiii = 0;
                            //}
                            string Text = feature.ColumnValues[TextColumnName];
                            if (CustomLabels != null && CustomLabels.Count > 0) {
                                if (CustomLabels.ContainsKey(feature.Id)) {
                                    Text = CustomLabels[feature.Id];
                                }
                            }
                            DrawingRectangleF drawingRectangleF = canvas.MeasureText(Text, this.Font);

                            // Call the canvas method to draw the label scaled.
                            PointShape point = new PointShape();

                            //point = poly.GetCenterPoint();

                            BaseShape bs = feature.GetShape();

                            if (bs is PolygonShape) {
                                PolygonShape ps = bs as PolygonShape;
                                point = ps.GetCenterPoint();
                                MultipolygonShape mps2 = new MultipolygonShape();
                                mps2.Polygons.Add(ps);
                                point = ValidateCenterPoint(point, mps2);
                            }

                            if (bs is MultipolygonShape) {
                                MultipolygonShape mps = bs as MultipolygonShape;
                                point = mps.GetCenterPoint();
                                point = ValidateCenterPoint(point, mps);
                            }

                            if (bs is LineShape) {
                                LineShape ls = bs as LineShape;
                                point = ls.GetCenterPoint();
                            }

                            if (bs is MultilineShape) {
                                MultilineShape mls = bs as MultilineShape;
                                point = mls.GetCenterPoint();
                            }

                            if (bs is PointShape) {
                                PointShape pnt = bs as PointShape;
                                point = pnt;
                            }

                            if (bs is MultipointShape) {
                                MultipointShape mpnt = bs as MultipointShape;
                                point = mpnt.GetCenterPoint();
                            }

                            if (LabelPositions != null && LabelPositions.Count > 0) {
                                if (LabelPositions.ContainsKey(feature.Id)) {
                                    PointShape pointposition = (PointShape)LabelPositions[feature.Id];
                                    point = pointposition;
                                    if(positionprojection != null) {
                                        if(!positionprojection.IsOpen) {
                                            positionprojection.Open();
                                        }
                                        point = positionprojection.ConvertToExternalProjection(pointposition) as PointShape;
                                    }
                                }
                            }

                            //Convert from world coordinate to screen coordinate the location of the feature.
                            ScreenPointF textScreenPoint = ExtentHelper.ToScreenCoordinate(canvas.CurrentWorldExtent, point, canvas.Width, canvas.Height);

                            this.RotationAngle = 0;
                            if (RotationAngles != null && RotationAngles.Count > 0) {
                                if (RotationAngles.ContainsKey(feature.Id)) {
                                    this.RotationAngle = (double)RotationAngles[feature.Id];
                                }
                            }

                            //if (labelSize >= 4) {
                            //    string linelengthstring = Text;
                            //    DrawingRectangleF drawingrectanglef = canvas.MeasureText(linelengthstring, new GeoFont("Arial", labelSize));
                            //    DrawingRectangleF newrectangle = new DrawingRectangleF(drawingrectanglef.CenterX + textScreenPoint.X, drawingrectanglef.CenterY + textScreenPoint.Y, drawingrectanglef.Width, drawingrectanglef.Height);

                            //    float upper = drawingrectanglef.Height / 2;
                            //    float lower = upper * -1;
                            //    float right = drawingrectanglef.Width / 2;
                            //    float left = right * -1;

                            //    ScreenPointF upperleft = new ScreenPointF(textScreenPoint.X + left - 2, textScreenPoint.Y + upper + 2);
                            //    ScreenPointF upperright = new ScreenPointF(textScreenPoint.X + right + 2, textScreenPoint.Y + upper + 2);
                            //    ScreenPointF lowerright = new ScreenPointF(textScreenPoint.X + right + 2, textScreenPoint.Y + lower - 2);
                            //    ScreenPointF lowerleft = new ScreenPointF(textScreenPoint.X + left - 2, textScreenPoint.Y + lower - 2);
                            //    PointShape ul = new PointShape(upperleft.X, upperleft.Y);
                            //    PointShape ur = new PointShape(upperright.X, upperright.Y);
                            //    PointShape lr = new PointShape(lowerright.X, lowerright.Y);
                            //    PointShape ll = new PointShape(lowerleft.X, lowerleft.Y);
                            //    PointShape cp = new PointShape(textScreenPoint.X, textScreenPoint.Y);
                            //    RectangleShape rc = new RectangleShape(ul.X, ul.Y, lr.X, lr.Y);
                            //    PolygonShape ps = rc.ToPolygon();
                            //    ps.Rotate(cp, System.Convert.ToSingle(360 - this.RotationAngle));

                            //    upperleft = new ScreenPointF((float)ps.OuterRing.Vertices[0].X, (float)ps.OuterRing.Vertices[0].Y);
                            //    upperright = new ScreenPointF((float)ps.OuterRing.Vertices[1].X, (float)ps.OuterRing.Vertices[1].Y);
                            //    lowerright = new ScreenPointF((float)ps.OuterRing.Vertices[2].X, (float)ps.OuterRing.Vertices[2].Y);
                            //    lowerleft = new ScreenPointF((float)ps.OuterRing.Vertices[3].X, (float)ps.OuterRing.Vertices[3].Y);
                            //    ScreenPointF[] sparray = new[] { upperleft, upperright, lowerright, lowerleft };

                            //    GeoPen gp = new GeoPen(new GeoColor(50, GeoColor.SimpleColors.LightYellow), 2);
                            //    GeoBrush gb = new GeoSolidBrush(new GeoColor(50, GeoColor.SimpleColors.LightYellow));
                            //    DrawingLevel dl = DrawingLevel.LabelLevel;
                            //    float xoffset = 0;
                            //    float yoffset = 0;
                            //    PenBrushDrawingOrder pbdo = PenBrushDrawingOrder.BrushFirst;
                            //    canvas.DrawArea(new[] { sparray }, null, gb, dl, xoffset, yoffset, pbdo);
                            //}
                            //Use DrawText method of canvas to label according to the values previously set.
                            bool foundfeature = true;
                            string featureid = feature.Id.ToString();
                            if (!unselectedfieldvisibility) {
                                foundfeature = false;
                                foreach (Feature feat in SelectedFeatures) {
                                    string featid = feat.Id.ToString();
                                    if (featureid == featid) {
                                        foundfeature = true;
                                        break;
                                    }
                                }
                            }

                            if (foundfeature) {
                                if (wraplabeltext) {
                                    RectangleShape boundingBox = feature.GetBoundingBox();
                                    //Gets the bounding box of the feature in screen coordinates.
                                    double boundingBoxScreenWidth = ExtentHelper.GetScreenDistanceBetweenTwoWorldPoints(canvas.CurrentWorldExtent,
                                                                   new PointShape(boundingBox.UpperLeftPoint.X, (boundingBox.UpperLeftPoint.Y + boundingBox.LowerLeftPoint.Y) / 2),
                                                                   new PointShape(boundingBox.LowerRightPoint.X, (boundingBox.UpperRightPoint.Y + boundingBox.LowerRightPoint.Y) / 2),
                                                                   canvas.Width, canvas.Height);

                                    if (boundingBoxScreenWidth > drawingRectangleF.Width) {
                                        canvas.DrawText(Text, new GeoFont("Arial", labelSize), new GeoSolidBrush(GeoColor.StandardColors.Black),
                                                        null, new ThinkGeo.MapSuite.Core.ScreenPointF[] { textScreenPoint }, DrawingLevel.LabelLevel,
                                                        this.XOffsetInPixel, this.YOffsetInPixel, (float)this.RotationAngle); //- labelSize
                                    }
                                    else {
                                        //If not, applies the word wrapping.
                                        string[] Words = Wrap(Text, GetMaxStringLength(canvas, drawingRectangleF.Width));
                                        string newLineText = "";
                                        for (int i = 0; i < Words.Length; i++) {
                                            newLineText = newLineText + Words[i] + Environment.NewLine;
                                        }
                                        canvas.DrawText(newLineText, new GeoFont("Arial", labelSize), new GeoSolidBrush(GeoColor.StandardColors.Black),
                                                        null, new ThinkGeo.MapSuite.Core.ScreenPointF[] { textScreenPoint }, DrawingLevel.LabelLevel,
                                                        this.XOffsetInPixel, this.YOffsetInPixel, (float)this.RotationAngle); //- labelSize
                                    }
                                }
                                else {
                                    canvas.DrawText(Text, new GeoFont("Arial", labelSize), new GeoSolidBrush(GeoColor.StandardColors.Black),
                                                    null, new ThinkGeo.MapSuite.Core.ScreenPointF[] { textScreenPoint }, DrawingLevel.LabelLevel,
                                                    this.XOffsetInPixel, this.YOffsetInPixel, (float)this.RotationAngle); //- labelSize
                                }
                            }
                        }
                    }
                    catch (Exception ex) {
                        int ddd = 0;
                    }
                }
            }
        }

        private PointShape ValidateCenterPoint(PointShape point, MultipolygonShape ps) {
            if (!fittingpolygonlabel) { return point; }
            if (!ps.Contains(point)) {
                MultipolygonShape testps = new MultipolygonShape(ps.GetWellKnownText());
                PointShape newpoint = new PointShape(point.GetWellKnownText());
                bool testpoint = true;
                while (testpoint) {
                    testps.ScaleDown(50);
                    newpoint = testps.GetCenterPoint();
                    if (ps.Contains(newpoint)) {
                        point = newpoint;
                        testpoint = false;
                        return newpoint; 
                    }
                    try {
                        double distance = ps.GetDistanceTo(testps, GeographyUnit.DecimalDegree, DistanceUnit.Inch);
                        if (distance > 0) {
                            newpoint = newpoint.GetClosestPointTo(ps, GeographyUnit.DecimalDegree);
                            point = newpoint;
                            testpoint = false;
                            return newpoint;
                        }
                    } catch {
                        testpoint = false;
                        return point;
                    }
                }
            }
            return point;
        }

        //Function to get the maximum length of the string without having a new line.
        private int GetMaxStringLength(GeoCanvas canvas, float currentWidth) {
            //For now, the value is 10. You can write your own logic to calculate that value based on 
            // the size of the text and the bounding box to hold it.
            int result = 10;
            return result;
        }

        //Function taken from http://www.velocityreviews.com/forums/t20370-word-wrap-line-break-code-and-algorithm-for-c.html
        private string[] Wrap(string text, int maxLength) {
            text = text.Replace("\n", " ");
            text = text.Replace("\r", " ");
            text = text.Replace(".", ". ");
            text = text.Replace(">", "> ");
            text = text.Replace("\t", " ");
            text = text.Replace(",", ", ");
            text = text.Replace(";", "; ");
            text = text.Replace("<br>", " ");
            text = text.Replace(" ", " ");

            string[] Words = text.Split(' ');
            int currentLineLength = 0;
            ArrayList Lines = new ArrayList(text.Length / maxLength);
            string currentLine = "";
            bool InTag = false;

            foreach (string currentWord in Words) {
                //ignore html
                if (currentWord.Length > 0) {

                    if (currentWord.Substring(0, 1) == "<")
                        InTag = true;

                    if (InTag) {
                        //handle filenames inside html tags
                        if (currentLine.EndsWith(".")) {
                            currentLine += currentWord;
                        }
                        else
                            currentLine += " " + currentWord;

                        if (currentWord.IndexOf(">") > -1)
                            InTag = false;
                    }
                    else {
                        if (currentLineLength + currentWord.Length + 1 < maxLength) {
                            currentLine += " " + currentWord;
                            currentLineLength += (currentWord.Length + 1);
                        }
                        else {
                            Lines.Add(currentLine);
                            currentLine = currentWord;
                            currentLineLength = currentWord.Length;
                        }
                    }
                }

            }
            if (currentLine != "")
                Lines.Add(currentLine);

            string[] textLinesStr = new string[Lines.Count];
            Lines.CopyTo(textLinesStr, 0);
            return textLinesStr;
        }

        protected override System.Collections.ObjectModel.Collection<string> GetRequiredColumnNamesCore()
        {
            // Here we grab the column from the textStyle and then add
            // the required columns to make sure we pull back the column
            //  that we need for labeling.
            Collection<string> columns = new Collection<string>();
            if (!columns.Contains(TextColumnName))
            {
                columns.Add(TextColumnName);
            }
            return columns;
        }
    }
}

