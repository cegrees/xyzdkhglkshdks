﻿using NLog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using ThinkGeo.MapSuite.Core;

namespace Landdb.MapStyles {
    public class EditPolygonInteractiveStyle : AreaStyle {
        private GeoFont geoFont;
        private GeoSolidBrush geoSolidBrush;
        private GeoSolidBrush geoSolidBrushtext;
        private GeoPen geoPen;
        private GeoPen geoPentext;
        private AreaUnit areaUnit;
        Proj4Projection projection;
        Logger log = LogManager.GetCurrentClassLogger();

        private enum Side {
            Undefined = 0,
            Left = 2,
            Right = 3,
            Middle = 4
        }

        public EditPolygonInteractiveStyle()
            : this(AreaUnit.Acres, new GeoFont(), new GeoSolidBrush(), new GeoPen(), new GeoSolidBrush(), new GeoPen()) { }

        public EditPolygonInteractiveStyle(AreaUnit areaUnit, GeoFont geoFont, GeoSolidBrush geoSolidBrush, GeoPen geoPen, GeoSolidBrush geoSolidBrushtext, GeoPen geoPentext) {
            this.areaUnit = areaUnit;
            this.geoFont = geoFont;
            this.geoSolidBrush = geoSolidBrush;
            this.geoPen = geoPen;
            this.geoSolidBrushtext = geoSolidBrushtext;
            this.geoPentext = geoPentext;
            projection = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetGoogleMapParametersString());
            projection.Open();
        }

        //For the font of the distance
        public GeoFont GeoFont {
            get { return geoFont; }
            set { geoFont = value; }
        }

        //For the color of the font
        public GeoSolidBrush GeoSolidBrush {
            get { return geoSolidBrush; }
            set { geoSolidBrush = value; }
        }

        //For the color of the halo of the font
        public GeoPen GeoPen {
            get { return geoPen; }
            set { geoPen = value; }
        }

        //For the color of the font
        public GeoSolidBrush GeoSolidBrushtext {
            get { return geoSolidBrushtext; }
            set { geoSolidBrushtext = value; }
        }

        //For the color of the halo of the font
        public GeoPen GeoPentext {
            get { return geoPentext; }
            set { geoPentext = value; }
        }

        //For the distance unit used.
        public AreaUnit AreaUnit {
            get { return areaUnit; }
            set { areaUnit = value; }
        }

        protected override void DrawCore(System.Collections.Generic.IEnumerable<Feature> features, GeoCanvas canvas, System.Collections.ObjectModel.Collection<SimpleCandidate> labelsInThisLayer, System.Collections.ObjectModel.Collection<SimpleCandidate> labelsInAllLayers) {
            foreach (Feature feature in features) {
                try {
                    AreaBaseShape abs = feature.GetShape() as AreaBaseShape;
                    AreaBaseShape baseareashape = projection.ConvertToInternalProjection(abs) as AreaBaseShape;
                    double featurearea = baseareashape.GetArea(GeographyUnit.DecimalDegree, areaUnit);
                    //PointShape pointShape1 = new PointShape(lineShape.Vertices[i]);
                    RectangleShape boundingbox = abs.GetBoundingBox();
                    PointShape upperleftcorner = boundingbox.UpperLeftPoint;
                    PointShape upperRightcorner = boundingbox.UpperRightPoint;
                    PointShape upperMid = upperleftcorner;
                    //PointShape upperMid = new PointShape((upperleftcorner.X + upperRightcorner.X) / 2, (upperleftcorner.Y + upperRightcorner.Y) / 2);
                    PointShape midPointShape = abs.GetCenterPoint();
                    PointShape point = abs.GetCenterPoint();
                    //ScreenPointF textScreenPoint = ExtentHelper.ToScreenCoordinate(canvas.CurrentWorldExtent, point, canvas.Width, canvas.Height);
                    ScreenPointF textScreenPoint = ExtentHelper.ToScreenCoordinate(canvas.CurrentWorldExtent, upperMid, canvas.Width, canvas.Height);
                    float labelSize = GeoFont.Size;

                    Collection<ScreenPointF> screenPointFs = new Collection<ScreenPointF>();
                    //screenPointFs.Add(ExtentHelper.ToScreenCoordinate(canvas.CurrentWorldExtent, midPointShape, canvas.Width, canvas.Height));
                    screenPointFs.Add(ExtentHelper.ToScreenCoordinate(canvas.CurrentWorldExtent, upperleftcorner, canvas.Width, canvas.Height));
                    canvas.DrawText(featurearea.ToString("N2"), geoFont, new GeoSolidBrush(geoSolidBrushtext.Color), new GeoPen(geoPentext.Color, 3), screenPointFs, DrawingLevel.LabelLevel, -40f, 10f, 0);
                } catch (Exception ex) {
                    log.WarnException($"Exception in {nameof(DrawCore)}", ex);
                }
            }
        }
    }
}