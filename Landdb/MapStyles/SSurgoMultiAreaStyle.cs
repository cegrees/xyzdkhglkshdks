﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Landdb.Client.Infrastructure.DisplayItems;
using ThinkGeo.MapSuite.Core;

namespace Landdb.MapStyles
{
    // Mark the class Serializable so that it works in SQL Server state
    // and in every serialization context
    [Serializable]
    class SSurgoMultiAreaStyle : AreaStyle
    {
        private Style selectedAreaStyle;
        private Style unselectedAreaStyle;
        private TextStyle textStyle = null;
        private Dictionary<string, AreaStyle> colorstyles;
        private List<string> columnslist;

        public SSurgoMultiAreaStyle()
            : this(new AreaStyle(), new AreaStyle(), new TextStyle())
        {
            colorstyles = new Dictionary<string, AreaStyle>();
        }

        public SSurgoMultiAreaStyle(Style selectedAreaStyle, Style unselectedAreaStyle, TextStyle textStyle)
        {
            this.selectedAreaStyle = selectedAreaStyle;
            this.unselectedAreaStyle = unselectedAreaStyle;
            this.textStyle = textStyle;
        }

        public Style SelectedAreaStyle
        {
            get { return selectedAreaStyle; }
            set { selectedAreaStyle = value; }
        }

        public Style UnselectedAreaStyle
        {
            get { return unselectedAreaStyle; }
            set { unselectedAreaStyle = value; }
        }

        public TextStyle TextStyle {
            get { return textStyle; }
            set { textStyle = value; }
        }

        public List<string> ColumnsList {
            get { return columnslist; }
            set { columnslist = value; }
        }

        public Dictionary<string, AreaStyle> ColorStyles {
            get { return colorstyles; }
            set { colorstyles = value; }
        }

        protected override void DrawCore(IEnumerable<Feature> features, GeoCanvas canvas, Collection<SimpleCandidate> labelsInThisLayer, Collection<SimpleCandidate> labelsInAllLayers)
        {
            foreach (Feature feature in features)
            {
                try {
                    string symbolmName = string.Empty;
                    try {
                        if (feature.ColumnValues.ContainsKey(@"Musym")) {
                            symbolmName = feature.ColumnValues[@"Musym"];
                        }
                        if (feature.ColumnValues.ContainsKey(@"musym")) {
                            symbolmName = feature.ColumnValues[@"musym"];
                        }
                    }
                    catch { }
                    bool foundfeature = false;
                    if (ColorStyles.ContainsKey(symbolmName)) {
                        selectedAreaStyle = colorstyles[symbolmName];
                        foundfeature = true;
                    }
                    if (foundfeature) {
                        selectedAreaStyle.Draw(new Collection<Feature>() { feature }, canvas, labelsInThisLayer, labelsInAllLayers);
                        //if (textStyle != null) {
                        //    textStyle.Draw(new Collection<Feature>() { feature }, canvas, labelsInThisLayer, labelsInAllLayers);
                        //}
                    }
                    else {
                        if (colorstyles.Count == 0) {
                            //if (unselectedAreaStyle is GradientStyle && feature.ColumnValues.Count < 2) { continue; }
                            unselectedAreaStyle.Draw(new Collection<Feature>() { feature }, canvas, labelsInThisLayer, labelsInAllLayers);
                            //if (textStyle != null) {
                            //    textStyle.Draw(new Collection<Feature>() { feature }, canvas, labelsInThisLayer, labelsInAllLayers);
                            //}
                        }
                    }
                }
                catch (Exception ex) {
                    int xxx = 0;
                }
            }
        }

        protected override System.Collections.ObjectModel.Collection<string> GetRequiredColumnNamesCore() {
            Collection<string> columns = new Collection<string>();
            foreach (string col in columnslist) {
                if (!columns.Contains(col)) {
                    columns.Add(col);
                }
            }
            return columns;
        }

    }
}
