﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using ThinkGeo.MapSuite.Core;

namespace Landdb.MapStyles {

public class CustomLegendItem : LegendItem {
       public CustomLegendItem( ) { }
 
       public CustomLegendItem( int width, int height, float imageWidth, float imageHeight, Style imageStyle, TextStyle textStyle )
           : base( width, height, imageWidth, imageHeight, imageStyle, textStyle ) { }
       
       protected override void DrawCore( GeoCanvas adornmentGeoCanvas, Collection<SimpleCandidate> labelsInAllLayers, LegendDrawingParameters legendDrawingParameters ) {
           float num1 = 1;
           float xoffset = legendDrawingParameters.XOffset;
           float yoffset = legendDrawingParameters.YOffset;
           float num2 = yoffset + this.TopPadding + this.ImageTopPadding;
           float num3 = yoffset + this.TopPadding + this.TextTopPadding;
           DrawingRectangleF drawingRectangleF = new DrawingRectangleF( );
           if ( this.TextStyle != null ) {
               string text = this.TextStyle.TextColumnName;
               drawingRectangleF = adornmentGeoCanvas.MeasureText( text, this.TextStyle.Font );
           }
           float num4;
           float num5;
           if ( this.ImageJustificationMode == LegendImageJustificationMode.JustifyImageRight ) {
               num4 = xoffset + this.LeftPadding + this.TextLeftPadding + drawingRectangleF.Width + this.TextRightPadding + this.ImageLeftPadding;
               num5 = xoffset + this.LeftPadding + this.TextLeftPadding;
           }
           else {
               num4 = (float) ((double) xoffset + (double) this.LeftPadding * (double) num1 + (double) this.ImageLeftPadding * (double) num1);
               num5 = (float) ((double) xoffset + (double) this.LeftPadding * (double) num1 + (double) this.ImageLeftPadding * (double) num1 + (double) this.ImageWidth * (double) num1 + (double) this.ImageRightPadding * (double) num1 + (double) this.TextLeftPadding * (double) num1);
           }
           if ( this.BackgroundMask != null ) {
               DrawingRectangleF drawingExtent = new DrawingRectangleF( this.Width / 2f + xoffset + this.LeftPadding, this.Height / 2f + yoffset + this.TopPadding, this.Width, this.Height );
               this.BackgroundMask.DrawSample( adornmentGeoCanvas, drawingExtent );
           }
           if ( this.ImageStyle != null ) {
               float width = this.ImageWidth;
               float height = this.ImageHeight;
               DrawingRectangleF drawingExtent = new DrawingRectangleF( width / 2f + num4, (float) ((double) height / 2.0 + (double) num2 * (double) num1), width, height );
               this.ImageStyle.DrawSample( adornmentGeoCanvas, drawingExtent );
               if (this.ImageMask != null) {
                   this.ImageMask.DrawSample(adornmentGeoCanvas, drawingExtent);
               }
           }
           if ( this.TextStyle == null ) {
               return;
           }
           DrawingRectangleF drawingExtent1 = new DrawingRectangleF( drawingRectangleF.CenterX + num5, drawingRectangleF.CenterY + num3 * num1, drawingRectangleF.Width, drawingRectangleF.Height );
           this.TextStyle.DrawSample( adornmentGeoCanvas, drawingExtent1 );

           if (this.TextMask == null) {
               return;
           }
           this.TextMask.DrawSample( adornmentGeoCanvas, drawingExtent1 );
       }
   }
}

