﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using ThinkGeo.MapSuite.Core;

namespace Landdb.MapStyles
{
    public class CustomDistancePolygonStyle : AreaStyle
    {
        private GeoFont geoFont;
        private GeoSolidBrush geoSolidBrush;
        private GeoSolidBrush geoSolidBrushtext;
        private GeoPen geoPen;
        private GeoPen geoPentext;
        private DistanceUnit distanceUnit;
        private AreaUnit areaUnit;
        Proj4Projection projection;

        private enum Side {
            Undefined = 0,
            Left = 2,
            Right = 3,
            Middle = 4
        }

        public CustomDistancePolygonStyle()
            : this(AreaUnit.Acres,DistanceUnit.Meter, new GeoFont(), new GeoSolidBrush(), new GeoPen(), new GeoSolidBrush(), new GeoPen())
        {}

        public CustomDistancePolygonStyle(AreaUnit areaUnit, DistanceUnit distanceUnit, GeoFont geoFont, GeoSolidBrush geoSolidBrush, GeoPen geoPen, GeoSolidBrush geoSolidBrushtext, GeoPen geoPentext)
        {
            this.areaUnit = areaUnit;
            this.distanceUnit = distanceUnit;
            this.geoFont = geoFont;
            this.geoSolidBrush = geoSolidBrush;
            this.geoPen = geoPen;
            this.geoSolidBrushtext = geoSolidBrushtext;
            this.geoPentext = geoPentext;
            projection = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetGoogleMapParametersString());
            projection.Open();
        }

        //For the font of the distance
        public GeoFont GeoFont
        {
            get { return geoFont; }
            set { geoFont = value; }
        }

        //For the color of the font
        public GeoSolidBrush GeoSolidBrush
        {
            get { return geoSolidBrush; }
            set { geoSolidBrush = value; }
        }

        //For the color of the halo of the font
        public GeoPen GeoPen
        {
            get { return geoPen; }
            set { geoPen = value; }
        }

        //For the color of the font
        public GeoSolidBrush GeoSolidBrushtext {
            get { return geoSolidBrushtext; }
            set { geoSolidBrushtext = value; }
        }

        //For the color of the halo of the font
        public GeoPen GeoPentext {
            get { return geoPentext; }
            set { geoPentext = value; }
        }

        //For the distance unit used.
        public DistanceUnit DistanceUnit {
            get { return distanceUnit; }
            set { distanceUnit = value; }
        }

        public AreaUnit AreaUnit
        {
            get { return areaUnit; }
            set { areaUnit = value; }
        }

        protected override void DrawCore(System.Collections.Generic.IEnumerable<Feature> features, GeoCanvas canvas, System.Collections.ObjectModel.Collection<SimpleCandidate> labelsInThisLayer, System.Collections.ObjectModel.Collection<SimpleCandidate> labelsInAllLayers)
        {
            foreach (Feature feature in features) {
                try {
                    AreaBaseShape abs = feature.GetShape() as AreaBaseShape;
                    AreaBaseShape baseareashape = projection.ConvertToInternalProjection(abs) as AreaBaseShape;
                    PolygonShape polyshape = baseareashape as PolygonShape;
                    double featurearea = baseareashape.GetArea(GeographyUnit.DecimalDegree, areaUnit);
                    double featurelength = baseareashape.GetPerimeter(GeographyUnit.DecimalDegree, distanceUnit);
                    //PointShape pointShape1 = new PointShape(lineShape.Vertices[i]);
                    RectangleShape boundingbox = abs.GetBoundingBox();
                    PointShape upperleftcorner = boundingbox.UpperLeftPoint;
                    PointShape upperRightcorner = boundingbox.UpperRightPoint;
                    PointShape upperMid = new PointShape((upperleftcorner.X + upperRightcorner.X) / 2, (upperleftcorner.Y + upperRightcorner.Y) / 2);
                    PointShape midPointShape = abs.GetCenterPoint();
                    PointShape point = abs.GetCenterPoint();
                    //ScreenPointF textScreenPoint = ExtentHelper.ToScreenCoordinate(canvas.CurrentWorldExtent, point, canvas.Width, canvas.Height);
                    //ScreenPointF textScreenPoint = ExtentHelper.ToScreenCoordinate(canvas.CurrentWorldExtent, upperMid, canvas.Width, canvas.Height);
                    float labelSize = GeoFont.Size;
                    //if (labelSize >= 4) {
                    //    string linelengthstring = featurearea.ToString("N2");
                    //    DrawingRectangleF drawingrectanglef = canvas.MeasureText(linelengthstring, new GeoFont("Arial", labelSize));
                    //    DrawingRectangleF newrectangle = new DrawingRectangleF(drawingrectanglef.CenterX + textScreenPoint.X, drawingrectanglef.CenterY + textScreenPoint.Y, drawingrectanglef.Width, drawingrectanglef.Height);

                    //    float upper = drawingrectanglef.Height / 2;
                    //    float lower = upper * -1;
                    //    float right = drawingrectanglef.Width / 2;
                    //    float left = right * -1;

                    //    ScreenPointF upperleft = new ScreenPointF(textScreenPoint.X + left - 2, textScreenPoint.Y + upper + 2);
                    //    ScreenPointF upperright = new ScreenPointF(textScreenPoint.X + right + 2, textScreenPoint.Y + upper + 2);
                    //    ScreenPointF lowerright = new ScreenPointF(textScreenPoint.X + right + 2, textScreenPoint.Y + lower - 2);
                    //    ScreenPointF lowerleft = new ScreenPointF(textScreenPoint.X + left - 2, textScreenPoint.Y + lower - 2);
                    //    PointShape ul = new PointShape(upperleft.X, upperleft.Y);
                    //    PointShape ur = new PointShape(upperright.X, upperright.Y);
                    //    PointShape lr = new PointShape(lowerright.X, lowerright.Y);
                    //    PointShape ll = new PointShape(lowerleft.X, lowerleft.Y);
                    //    PointShape cp = new PointShape(textScreenPoint.X, textScreenPoint.Y);
                    //    RectangleShape rc = new RectangleShape(ul.X, ul.Y, lr.X, lr.Y);
                    //    PolygonShape ps = rc.ToPolygon();

                    //    upperleft = new ScreenPointF((float)ps.OuterRing.Vertices[0].X, (float)ps.OuterRing.Vertices[0].Y);
                    //    upperright = new ScreenPointF((float)ps.OuterRing.Vertices[1].X, (float)ps.OuterRing.Vertices[1].Y);
                    //    lowerright = new ScreenPointF((float)ps.OuterRing.Vertices[2].X, (float)ps.OuterRing.Vertices[2].Y);
                    //    lowerleft = new ScreenPointF((float)ps.OuterRing.Vertices[3].X, (float)ps.OuterRing.Vertices[3].Y);
                    //    ScreenPointF[] sparray = new[] { upperleft, upperright, lowerright, lowerleft };

                    //    GeoPen gp = new GeoPen(new GeoColor(255, GeoColor.SimpleColors.Yellow), 2);
                    //    GeoBrush gb = new GeoSolidBrush(new GeoColor(255, GeoColor.SimpleColors.Yellow));
                    //    DrawingLevel dl = DrawingLevel.LabelLevel;
                    //    float xoffset = 0;
                    //    float yoffset = 0;
                    //    PenBrushDrawingOrder pbdo = PenBrushDrawingOrder.BrushFirst;
                    //    canvas.DrawArea(new[] { sparray }, null, gb, dl, xoffset, yoffset, pbdo);
                    //}

                    Collection<ScreenPointF> screenPointFs = new Collection<ScreenPointF>();
                    //screenPointFs.Add(ExtentHelper.ToScreenCoordinate(canvas.CurrentWorldExtent, midPointShape, canvas.Width, canvas.Height));
                    //screenPointFs.Add(ExtentHelper.ToScreenCoordinate(canvas.CurrentWorldExtent, upperMid, canvas.Width, canvas.Height));
                    screenPointFs.Add(ExtentHelper.ToScreenCoordinate(canvas.CurrentWorldExtent, upperleftcorner, canvas.Width, canvas.Height));
                    canvas.DrawText(featurearea.ToString("N2"), geoFont, geoSolidBrushtext, geoPentext, screenPointFs, DrawingLevel.LabelLevel, -40f, 10f, 0);

                    PointShape firstpoint = new PointShape(polyshape.OuterRing.Vertices[0]);
                    //ScreenPointF[] sparray2 = CalculateTextArea(canvas, firstpoint, featurelength.ToString("N1"));
                    //canvas.DrawArea(new[] { sparray2 }, new GeoPen(GeoColor.SimpleColors.Red, 2), new GeoSolidBrush(GeoColor.SimpleColors.Red), DrawingLevel.LabelLevel, -20f, -20f, PenBrushDrawingOrder.BrushFirst);
                    ScreenPointF screenpointF2 = ExtentHelper.ToScreenCoordinate(canvas.CurrentWorldExtent, firstpoint, canvas.Width, canvas.Height);
                    Collection<ScreenPointF> screenPointFs2 = new Collection<ScreenPointF>();
                    screenPointFs2.Add(screenpointF2);
                    canvas.DrawText(featurelength.ToString("N1"), geoFont, GeoSolidBrush, geoPen, screenPointFs2, DrawingLevel.LabelLevel, -20f, -20f, 0f);
                }
                catch (Exception ex) {
                    int xxx = 0;
                }
            }
        }

        private ScreenPointF[] CalculateTextArea(GeoCanvas canvas, PointShape midPointShape, string linelengthstring) {
            ScreenPointF screenpointF3 = ExtentHelper.ToScreenCoordinate(canvas.CurrentWorldExtent, midPointShape, canvas.Width, canvas.Height);
            DrawingRectangleF drawingrectanglef = canvas.MeasureText(linelengthstring, geoFont);
            DrawingRectangleF newrectangle = new DrawingRectangleF(drawingrectanglef.CenterX + screenpointF3.X, drawingrectanglef.CenterY + screenpointF3.Y, drawingrectanglef.Width, drawingrectanglef.Height);

            float upper = drawingrectanglef.Height / 2;
            float lower = upper * -1;
            float right = drawingrectanglef.Width / 2;
            float left = right * -1;

            ScreenPointF upperleft = new ScreenPointF(screenpointF3.X + left - 2, screenpointF3.Y + upper + 2);
            ScreenPointF upperright = new ScreenPointF(screenpointF3.X + right + 2, screenpointF3.Y + upper + 2);
            ScreenPointF lowerright = new ScreenPointF(screenpointF3.X + right + 2, screenpointF3.Y + lower - 2);
            ScreenPointF lowerleft = new ScreenPointF(screenpointF3.X + left - 2, screenpointF3.Y + lower - 2);
            ScreenPointF[] sparray = new[] { upperleft, upperright, lowerright, lowerleft };
            return sparray;
        }

    }
}
