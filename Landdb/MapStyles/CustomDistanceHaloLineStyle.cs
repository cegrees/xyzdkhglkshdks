﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using ThinkGeo.MapSuite.Core;

namespace Landdb.MapStyles
{
    public class CustomDistanceHaloLineStyle : LineStyle
    {
        private GeoFont geoFont;
        private GeoSolidBrush geoSolidBrush;
        private GeoPen geoPen;
        private GeoPen geoPen2;
        private GeoPen geoPen3;
        private DistanceUnit distanceUnit;
        Proj4Projection projection;
        private bool displayangles = true;
        private bool displaytotallength = true;

        private enum Side {
            Undefined = 0,
            Left = 2,
            Right = 3,
            Middle = 4
        }

        public CustomDistanceHaloLineStyle()
            : this(DistanceUnit.Meter, new GeoFont(), new GeoSolidBrush(), new GeoPen(), new GeoPen(), new GeoPen())
        {}

        public CustomDistanceHaloLineStyle(DistanceUnit distanceUnit, GeoFont geoFont, GeoSolidBrush geoSolidBrush, GeoPen geoPen, GeoPen geoPen2, GeoPen geoPen3)
        {
            this.distanceUnit = distanceUnit;
            this.geoFont = geoFont;
            this.geoSolidBrush = geoSolidBrush;
            this.geoPen = geoPen;
            this.geoPen2 = geoPen2;
            this.geoPen3 = geoPen3;
            projection = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetGoogleMapParametersString());
            projection.Open();
        }

        //For the font of the distance
        public GeoFont GeoFont
        {
            get { return geoFont; }
            set { geoFont = value; }
        }

        //For the color of the font
        public GeoSolidBrush GeoSolidBrush
        {
            get { return geoSolidBrush; }
            set { geoSolidBrush = value; }
        }

        //For the color of the halo of the font
        public GeoPen GeoPen
        {
            get { return geoPen; }
            set { geoPen = value; }
        }

        //For the color of the halo of the font
        public GeoPen GeoPen2 {
            get { return geoPen2; }
            set { geoPen2 = value; }
        }

        //For the color of the halo of the font
        public GeoPen GeoPen3 {
            get { return geoPen3; }
            set { geoPen3 = value; }
        }

        //For the distance unit used.
        public DistanceUnit DistanceUnit
        {
            get { return distanceUnit; }
            set { distanceUnit = value; }
        }

        //For showing angle dimensions along with length dimensions
        public bool DisplayAngles {
            get { return displayangles; }
            set { displayangles = value; }
        }

        //For showing the total length dimension
        public bool DisplayTotalLength {
            get { return displaytotallength; }
            set { displaytotallength = value; }
        }

        protected override void DrawCore(System.Collections.Generic.IEnumerable<Feature> features, GeoCanvas canvas, System.Collections.ObjectModel.Collection<SimpleCandidate> labelsInThisLayer, System.Collections.ObjectModel.Collection<SimpleCandidate> labelsInAllLayers) {
            foreach (Feature feature in features) {
                BaseShape shape = feature.GetShape();
                try {
                    LineShape lineShape = shape as LineShape;
                    if (lineShape != null && lineShape.Vertices.Count > 0) {
                        double linelength = lineShape.GetLength(canvas.MapUnit, DistanceUnit);
                        LineShape baselineshape2 = projection.ConvertToInternalProjection(lineShape) as LineShape;
                        double linelength2 = baselineshape2.GetLength(GeographyUnit.DecimalDegree, DistanceUnit);
                        PointShape pointShape3 = new PointShape(lineShape.Vertices[lineShape.Vertices.Count - 1]);
                        double angle = 0;
                        double angle0 = 0;
                        for (int i = 0; i < lineShape.Vertices.Count - 1; i++) {
                            PointShape pointShape1 = new PointShape(lineShape.Vertices[i]);
                            PointShape pointShape2 = new PointShape(lineShape.Vertices[i + 1]);
                            double currentDist = Math.Round(pointShape1.GetDistanceTo(pointShape2, canvas.MapUnit, distanceUnit), 2);
                            LineShape tempLineShape = new LineShape();
                            tempLineShape.Vertices.Add(new Vertex(pointShape1));
                            tempLineShape.Vertices.Add(new Vertex(pointShape2));
                            LineShape baselineshape3 = projection.ConvertToInternalProjection(tempLineShape) as LineShape;
                            double linelength3 = baselineshape3.GetLength(GeographyUnit.DecimalDegree, DistanceUnit);
                            angle = GetAngleFromTwoVertices(tempLineShape.Vertices[0], tempLineShape.Vertices[1]);
                            pointShape3 = tempLineShape.GetPointOnALine(StartingPoint.FirstPoint, 85);
                            PointShape midPointShape = tempLineShape.GetPointOnALine(StartingPoint.FirstPoint, 50);
                            ScreenPointF screenpointF = ExtentHelper.ToScreenCoordinate(canvas.CurrentWorldExtent, midPointShape, canvas.Width, canvas.Height);
                            Collection<ScreenPointF> screenPointFs = new Collection<ScreenPointF>();
                            screenPointFs.Add(screenpointF);
                            canvas.DrawText(linelength3.ToString("N1"), geoFont, GeoSolidBrush, geoPen2, screenPointFs, DrawingLevel.LabelLevel, 0, 0, 0);
                            if (displayangles && i > 0 && i < lineShape.Vertices.Count - 1) {
                                PointShape pointShape0 = new PointShape(lineShape.Vertices[i - 1]);
                                LineShape tempLineShape0 = new LineShape();
                                tempLineShape0.Vertices.Add(new Vertex(pointShape1));
                                tempLineShape0.Vertices.Add(new Vertex(pointShape0));
                                angle0 = GetAngleFromTwoVertices(tempLineShape0.Vertices[0], tempLineShape0.Vertices[1]);
                                PointShape center = pointShape1;
                                PointShape origin1 = pointShape0;
                                PointShape target1 = pointShape2;
                                double anglestart = PointsAngle(center, origin1);
                                double angleend = PointsAngle(center, target1);
                                if (angleend < anglestart) {
                                    angleend += 360;
                                }
                                double anglechange = angleend - anglestart;
                                ScreenPointF screenPointF5 = ExtentHelper.ToScreenCoordinate(canvas.CurrentWorldExtent, pointShape1, (float)canvas.Width, (float)canvas.Height);
                                Collection<ScreenPointF> screenPointFs5 = new Collection<ScreenPointF>();
                                screenPointFs5.Add(screenPointF5);
                                canvas.DrawText(anglechange.ToString("N1"), geoFont, GeoSolidBrush, geoPen3, screenPointFs5, DrawingLevel.LabelLevel, 15f, 15f, 0f);
                            }
                            else if (displayangles && i == 0) {
                                PointShape center = pointShape1;
                                PointShape target1 = pointShape2;
                                double angleend = PointsAngle(center, target1);
                                double anglechange = angleend;
                                ScreenPointF screenPointF5 = ExtentHelper.ToScreenCoordinate(canvas.CurrentWorldExtent, pointShape1, (float)canvas.Width, (float)canvas.Height);
                                Collection<ScreenPointF> screenPointFs5 = new Collection<ScreenPointF>();
                                screenPointFs5.Add(screenPointF5);
                                canvas.DrawText(anglechange.ToString("N1"), geoFont, GeoSolidBrush, geoPen3, screenPointFs5, DrawingLevel.LabelLevel, 15f, 15f, 0f);
                            }
                        }
                        if (displaytotallength && lineShape.Vertices.Count > 2) {
                            PointShape firstpoint = new PointShape(lineShape.Vertices[0]);
                            ScreenPointF screenpointF2 = ExtentHelper.ToScreenCoordinate(canvas.CurrentWorldExtent, firstpoint, canvas.Width, canvas.Height);
                            Collection<ScreenPointF> screenPointFs2 = new Collection<ScreenPointF>();
                            screenPointFs2.Add(screenpointF2);
                            canvas.DrawText(linelength2.ToString("N1"), geoFont, GeoSolidBrush, geoPen, screenPointFs2, DrawingLevel.LabelLevel, -15f, -15f, 0f);
                        }
                    }
                    else {
                        MultilineShape mls = (MultilineShape)feature.GetShape();
                        if (mls != null && mls.Lines.Count > 0) {
                            foreach (LineShape lineShape1 in mls.Lines) {
                                lineShape = lineShape1;
                                if (lineShape != null && lineShape.Vertices.Count > 0) {
                                    double linelength = lineShape.GetLength(canvas.MapUnit, DistanceUnit);
                                    LineShape baselineshape2 = projection.ConvertToInternalProjection(lineShape) as LineShape;
                                    double linelength2 = baselineshape2.GetLength(GeographyUnit.DecimalDegree, DistanceUnit);
                                    PointShape pointShape3 = new PointShape(lineShape.Vertices[lineShape.Vertices.Count - 1]);
                                    double angle = 0;
                                    double angle0 = 0;
                                    for (int i = 0; i < lineShape.Vertices.Count - 1; i++) {
                                        PointShape pointShape1 = new PointShape(lineShape.Vertices[i]);
                                        PointShape pointShape2 = new PointShape(lineShape.Vertices[i + 1]);
                                        double currentDist = Math.Round(pointShape1.GetDistanceTo(pointShape2, canvas.MapUnit, distanceUnit), 2);
                                        LineShape tempLineShape = new LineShape();
                                        tempLineShape.Vertices.Add(new Vertex(pointShape1));
                                        tempLineShape.Vertices.Add(new Vertex(pointShape2));
                                        LineShape baselineshape3 = projection.ConvertToInternalProjection(tempLineShape) as LineShape;
                                        double linelength3 = baselineshape3.GetLength(GeographyUnit.DecimalDegree, DistanceUnit);
                                        angle = GetAngleFromTwoVertices(tempLineShape.Vertices[0], tempLineShape.Vertices[1]);
                                        pointShape3 = tempLineShape.GetPointOnALine(StartingPoint.FirstPoint, 85);
                                        PointShape midPointShape = tempLineShape.GetPointOnALine(StartingPoint.FirstPoint, 50);
                                        ScreenPointF screenpointF = ExtentHelper.ToScreenCoordinate(canvas.CurrentWorldExtent, midPointShape, canvas.Width, canvas.Height);
                                        Collection<ScreenPointF> screenPointFs = new Collection<ScreenPointF>();
                                        screenPointFs.Add(screenpointF);
                                        canvas.DrawText(linelength3.ToString("N1"), geoFont, GeoSolidBrush, geoPen2, screenPointFs, DrawingLevel.LabelLevel, 0, 0, 0);
                                        if (displayangles && i > 0 && i < lineShape.Vertices.Count - 1) {
                                            PointShape pointShape0 = new PointShape(lineShape.Vertices[i - 1]);
                                            LineShape tempLineShape0 = new LineShape();
                                            tempLineShape0.Vertices.Add(new Vertex(pointShape1));
                                            tempLineShape0.Vertices.Add(new Vertex(pointShape0));
                                            angle0 = GetAngleFromTwoVertices(tempLineShape0.Vertices[0], tempLineShape0.Vertices[1]);
                                            PointShape center = pointShape1;
                                            PointShape origin1 = pointShape0;
                                            PointShape target1 = pointShape2;
                                            double anglestart = PointsAngle(center, origin1);
                                            double angleend = PointsAngle(center, target1);
                                            if (angleend < anglestart) {
                                                angleend += 360;
                                            }
                                            double anglechange = angleend - anglestart;
                                            ScreenPointF screenPointF5 = ExtentHelper.ToScreenCoordinate(canvas.CurrentWorldExtent, pointShape1, (float)canvas.Width, (float)canvas.Height);
                                            Collection<ScreenPointF> screenPointFs5 = new Collection<ScreenPointF>();
                                            screenPointFs5.Add(screenPointF5);
                                            canvas.DrawText(anglechange.ToString("N1"), geoFont, GeoSolidBrush, geoPen3, screenPointFs5, DrawingLevel.LabelLevel, 15f, 15f, 0f);
                                        }
                                        else if (displayangles && i == 0) {
                                            PointShape center = pointShape1;
                                            PointShape target1 = pointShape2;
                                            double angleend = PointsAngle(center, target1);
                                            double anglechange = angleend;
                                            ScreenPointF screenPointF5 = ExtentHelper.ToScreenCoordinate(canvas.CurrentWorldExtent, pointShape1, (float)canvas.Width, (float)canvas.Height);
                                            Collection<ScreenPointF> screenPointFs5 = new Collection<ScreenPointF>();
                                            screenPointFs5.Add(screenPointF5);
                                            canvas.DrawText(anglechange.ToString("N1"), geoFont, GeoSolidBrush, geoPen3, screenPointFs5, DrawingLevel.LabelLevel, 15f, 15f, 0f);
                                        }
                                    }
                                    if (displaytotallength && lineShape.Vertices.Count > 2) {
                                        PointShape firstpoint = new PointShape(lineShape.Vertices[0]);
                                        ScreenPointF screenpointF2 = ExtentHelper.ToScreenCoordinate(canvas.CurrentWorldExtent, firstpoint, canvas.Width, canvas.Height);
                                        Collection<ScreenPointF> screenPointFs2 = new Collection<ScreenPointF>();
                                        screenPointFs2.Add(screenpointF2);
                                        canvas.DrawText(linelength2.ToString("N1"), geoFont, GeoSolidBrush, geoPen, screenPointFs2, DrawingLevel.LabelLevel, -15f, -15f, 0f);
                                    }
                                }
                            }
                        }
                    }
                    //}
                }
                catch (Exception ex) {
                    int i = 0;
                }
            }

        }

        private double PointsAngle(PointShape start, PointShape end) {
            return Math.Atan2(start.Y - end.Y, end.X - start.X) * (180 / Math.PI);
        }
        static public double FindAngleBetween2Vectors(PointShape origin1, PointShape target1, PointShape center) {
            PointShape origin = new PointShape(origin1.X - center.X, origin1.Y - center.Y);
            PointShape target = new PointShape(target1.X - center.X, target1.Y - center.Y);
            double angle = Math.Atan2(target.Y - origin.Y, target.X - origin.X);
            return (angle);
        }

        private ScreenPointF[] CalculateTextArea(GeoCanvas canvas, PointShape midPointShape, string linelengthstring) {
            ScreenPointF screenpointF3 = ExtentHelper.ToScreenCoordinate(canvas.CurrentWorldExtent, midPointShape, canvas.Width, canvas.Height);
            DrawingRectangleF drawingrectanglef = canvas.MeasureText(linelengthstring, geoFont);
            DrawingRectangleF newrectangle = new DrawingRectangleF(drawingrectanglef.CenterX + screenpointF3.X, drawingrectanglef.CenterY + screenpointF3.Y, drawingrectanglef.Width, drawingrectanglef.Height);

            float upper = drawingrectanglef.Height / 2;
            float lower = upper * -1;
            float right = drawingrectanglef.Width / 2;
            float left = right * -1;

            ScreenPointF upperleft = new ScreenPointF(screenpointF3.X + left - 2, screenpointF3.Y + upper + 2);
            ScreenPointF upperright = new ScreenPointF(screenpointF3.X + right + 2, screenpointF3.Y + upper + 2);
            ScreenPointF lowerright = new ScreenPointF(screenpointF3.X + right + 2, screenpointF3.Y + lower - 2);
            ScreenPointF lowerleft = new ScreenPointF(screenpointF3.X + left - 2, screenpointF3.Y + lower - 2);
            ScreenPointF[] sparray = new[] { upperleft, upperright, lowerright, lowerleft };
            return sparray;
        }

        private double GetAngleFromTwoVertices(Vertex b, Vertex c)
        {
            double result;
            double alpha = 0;
            double tangentAlpha = (c.Y - b.Y) / (c.X - b.X);
            double Peta = Math.Atan(tangentAlpha);

            if (c.X > b.X)
            {
                alpha = 90 + (Peta * (180 / Math.PI));
            }
            else if (c.X < b.X)
            {
                alpha = 270 + (Peta * (180 / Math.PI));
            }
            else
            {
                if (c.Y > b.Y) alpha = 0;
                if (c.Y < b.Y) alpha = 180;
            }

            double offset;
            if (b.X > c.X)
            { offset = 90; }
            else { offset = - 90; }

            result = alpha + offset;
            return result;   
        }

        private LineShape GetTangentForLinePosition(LineShape lineShape, double stationKM) {
            const double OFFSET_KM = 0.000000001;
            PointShape tangentPointShape1, tangentPointShape2;
            double offsetTangentStart = OFFSET_KM;
            double offsetTangentEnd = OFFSET_KM;
            LineShape tangentLineShape;

            if (stationKM == 0.0) { offsetTangentStart = 0.0; }

            if (stationKM == lineShape.GetLength(GeographyUnit.Meter, DistanceUnit.Kilometer)) { offsetTangentEnd = 0.0; }

            tangentPointShape1 = lineShape.GetPointOnALine(StartingPoint.FirstPoint, stationKM - offsetTangentStart,
                                                                                        GeographyUnit.Meter, DistanceUnit.Kilometer);
            tangentPointShape2 = lineShape.GetPointOnALine(StartingPoint.FirstPoint, stationKM + offsetTangentEnd,
                                                                                        GeographyUnit.Meter, DistanceUnit.Kilometer);
            tangentLineShape = new LineShape(new Vertex[] { new Vertex(tangentPointShape1), new Vertex(tangentPointShape2) });

            return tangentLineShape;
        }

        private double GetOrthogonalFromVertex(Vertex vertex1, Vertex vertex2, Side currentSide) {
            double alpha = 0.0;

            if (currentSide == Side.Left | currentSide == Side.Right) {
                alpha = GetTangent(vertex1, vertex2);

                if (currentSide == Side.Right) {
                    if (alpha < 180.0) { alpha += 180.0; }
                    else { alpha -= 180.0; }
                }
            }
            return alpha;
        }

        private double GetTangent(Vertex vertex1, Vertex vertex2) {
            double tangentAlpha = 0;
            double offset = 0;
            double deltaY = 0;
            double deltaX = 0;
            double alpha = -1.0;

            if ((vertex1.X != vertex2.X)) {
                offset = 0.0;
                deltaY = vertex1.Y - vertex2.Y;
                deltaX = vertex2.X - vertex1.X;

                if (Math.Sign(deltaY) == 0.0) {
                    if (deltaX > 0.0) { offset = Math.PI * 0.5; }
                    else { offset = Math.PI * -0.5; }
                }
                else {
                    if (Math.Sign(deltaX) > 0.0) { offset = 0; }
                    else { offset = Math.PI; }
                }

                tangentAlpha = (deltaY / deltaX);
                alpha = (Math.Atan(tangentAlpha) + offset) * 180.0 / Math.PI;

                if ((alpha < 0.0)) { alpha += 360.0; }
            }
            else {
                if ((vertex1.Y > vertex2.Y)) { alpha = 90.0; }
                else { alpha = 270.0; }
            }

            return alpha;
        }

    }
}
