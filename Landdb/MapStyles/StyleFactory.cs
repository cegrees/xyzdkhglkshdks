﻿using GalaSoft.MvvmLight;
using Landdb.Client.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThinkGeo.MapSuite.Core;

namespace Landdb.MapStyles {
    public class StyleFactory : IStyleFactory {
        public StyleFactory() { }

        public AnnotationStyleItem GetStyle(string styledisplayname, string shapetype) {
            if (StyleData.Self.StyleNames.ContainsKey(styledisplayname)) {
                string sname = StyleData.Self.StyleNames[styledisplayname].StyleName;
                if (StyleData.Self.ColorStyles.ContainsKey(sname)) {
                    AnnotationStyleItem asi = StyleData.Self.ColorStyles[sname];
                    return asi;
                }
            }
            return null;
        }

        public IList<AnnotationStyleItem> GetStyleList(string shapetype) {
            var q = from s in StyleData.Self.ColorStyles
                    where s.Value.ShapeType == shapetype
                    orderby s.Value.TypeName
                    select s.Value;

            return q.Cast<AnnotationStyleItem>().ToList(); ;
        }

        public MapAnnotationCategory GetMapAnnotationCategory(string styledisplayname) {
            if (StyleData.Self.StyleNames.ContainsKey(styledisplayname)) {
                return StyleData.Self.StyleNames[styledisplayname];
            }
            return null;
        }

        public Style SetStyleOpacity(Style setstyle, int opacity) {
            Style newstylevisibility = setstyle;
            if (setstyle is AreaStyle) {
                AreaStyle astyle = setstyle as AreaStyle;
                return AreaStyles.CreateSimpleAreaStyle(new GeoColor(opacity, astyle.FillSolidBrush.Color), new GeoColor(opacity, astyle.OutlinePen.Color), (int)astyle.OutlinePen.Width);
            }
            if (setstyle is LineStyle) {
                LineStyle astyle = setstyle as LineStyle;
                return LineStyles.CreateSimpleLineStyle(new GeoColor(opacity, astyle.CenterPen.Color), astyle.CenterPen.Width, true);
            }
            if (setstyle is WellPointStyle) {
                WellPointStyle astyle = setstyle as WellPointStyle;
                return astyle;
            }
            if (setstyle is PointStyle) {
                PointStyle astyle = setstyle as PointStyle;
                if (astyle.PointType == PointType.Symbol) {
                    return PointStyles.CreateSimplePointStyle(astyle.SymbolType, new GeoColor(opacity, astyle.SymbolSolidBrush.Color), astyle.SymbolSize);
                }
                else if (astyle.PointType == PointType.Bitmap) {
                    return astyle;
                }
                else {
                    return astyle;
                }
            }
            return newstylevisibility;
        }
    }
}
