﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel {
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
	public sealed class YieldLocationTypeNotUnknownValidator : ValidationAttribute {
		public override bool IsValid(object value) {
			var isValid = false;

			if (value is YieldLocationTypes) {
				isValid = (YieldLocationTypes)value != YieldLocationTypes.Unknown;
			}

			return isValid;
		}
	}
}