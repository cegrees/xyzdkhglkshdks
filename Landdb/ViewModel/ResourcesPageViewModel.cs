﻿using System;
using System.Collections.ObjectModel;
using Landdb.Infrastructure;
using System.Reactive.Linq;
using WPFLocalizeExtension.Extensions;
using Landdb.Views.Resources;
using EquipmentPageView = Landdb.Views.Resources.Equipment.EquipmentPageView;

namespace Landdb.ViewModel {

    public class ResourcesPageViewModel {
        public ResourcesPageViewModel() {
			Screens = new ObservableCollection<ScreenDescriptor>();

            var screenDescriptors = new ScreenDescriptor[] {
                new ScreenDescriptor(typeof(PersonPageView), LocExtension.GetLocalizedValue<string>("Landdb:Strings:PageHeader_Resources_People").ToUpper()),
                new ScreenDescriptor(typeof(CompanyPageView), LocExtension.GetLocalizedValue<string>("Landdb:Strings:PageHeader_Resources_Companies").ToUpper()),
                new ScreenDescriptor(typeof(EquipmentPageView), LocExtension.GetLocalizedValue<string>("Landdb:Strings:PageHeader_Resources_Equipment").ToUpper()),
                new ScreenDescriptor(typeof(ActiveIngredientPageView), LocExtension.GetLocalizedValue<string>("Landdb:Strings:PageHeader_Resources_ActiveIngredients").ToUpper()),
                new ScreenDescriptor(typeof(InventoryPageView), LocExtension.GetLocalizedValue<string>("Landdb:Strings:PageHeader_Resources_Products").ToUpper()),
                new ScreenDescriptor(typeof(ContractsPageView), LocExtension.GetLocalizedValue<string>("Landdb:Strings:PageHeader_Resources_Contracts").ToUpper()),
                //new ScreenDescriptor("Landdb.Views.BlankPage", "LOCATIONS"),
            };

            screenDescriptors.ToObservable()
                .OnTimeline(TimeSpan.FromSeconds(0.2))
                .ObserveOn(System.Threading.SynchronizationContext.Current)
                .Subscribe(AddScreen);
        }

        public ObservableCollection<ScreenDescriptor> Screens { get; }

        void AddScreen(ScreenDescriptor screen) {
            Screens.Add(screen);
        }
    }
}