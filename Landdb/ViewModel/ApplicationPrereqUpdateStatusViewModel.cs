﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel {
    public class ApplicationPrereqUpdateStatusViewModel : ViewModelBase {
        public ObservableCollection<PrerequisiteItemViewModel> UpdatedPrereqs {
            get; private set;
        } = new ObservableCollection<PrerequisiteItemViewModel>();

        public void AddPrereq(string name, string description, string uri) {
            UpdatedPrereqs.Add(new PrerequisiteItemViewModel(name, description, uri));
        }
    }

    public class PrerequisiteItemViewModel : ViewModelBase {
        public PrerequisiteItemViewModel(string name, string description, string uri) {
            Name = name;
            Description = description;
            Uri = uri;
            InstallPrereqCommand = new RelayCommand(InstallPrereq);
        }

        public RelayCommand InstallPrereqCommand { get; private set; } 

        public string Name { get; private set; }
        public string Description { get; private set; }
        public string Uri { get; private set; }

        void InstallPrereq() {
            System.Diagnostics.Process.Start(Uri);
        }
    }
}
