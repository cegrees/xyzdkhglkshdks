﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Analysis.Planet.JsonClass {


    public class AreaSubscription
    {
        public string Name { get; set; }
        public string DataSourceId { get; set; }
        public string DataSource_Name { get; set; }
        public string TreeItem_Id { get; set; }
        public string TreeItem_Name { get; set; }
        public string AreaInWKT { get; set; }
        public AreaOfInterest AreaofInterest { get; set; }
        public string Status { get; set; }
        public DateTime BeginDate { get; set; }
        public string[] List_of_Satelites { get; set; }
        public int Filter_Days { get; set; }
        public double Filter_Usable_Data { get; set; }
        public double Filter_Cloud_Cover { get; set; }
        public Dictionary<string, string> FeatureStatus { get; set; }
        public Dictionary<string, Dictionary<string, string>> FeatureProperties { get; set; }
        public Dictionary<string, string> FeatureTIFs { get; set; }
        public Dictionary<string, string> FeatureNDVI { get; set; }
        public Dictionary<string, string> FeaturePNGs { get; set; }
    }
}
