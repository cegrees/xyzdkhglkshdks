﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Analysis.Planet.JsonClass
{
    public class PlanetImageItems
    {
        public string clippedSatelliteImageUri { get; set; }
        public string vegetativeIndexPlotUri { get; set; }
        public string featurePropertiesUri { get; set; }
        //public string imageorderinfoUri { get; set; }
    }

}