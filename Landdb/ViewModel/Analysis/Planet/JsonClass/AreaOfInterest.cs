﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Analysis.Planet.JsonClass
{

    public class AreaOfInterest
    {
        public string type { get; set; }
        public Feature[] features { get; set; }
    }

    public class Feature
    {
        public string type { get; set; }
        public Properties properties { get; set; }
        public Geometry geometry { get; set; }
    }

    public class Properties
    {
    }

    public class Geometry
    {
        //public string coordinates { get; set; }
        public float[][][] coordinates { get; set; }
        public string type { get; set; }
    }
}

