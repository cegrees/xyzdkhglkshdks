﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Analysis.Planet.JsonClass
{
    public class PlanetCloudItems
    {
        public string[] ItemList { get; set; }
    }
}
