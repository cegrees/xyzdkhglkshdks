﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Analysis.Planet {

    public class AssetStatusResponse {
        public Analytic analytic { get; set; }
        public Analytic_Xml analytic_xml { get; set; }
        public Udm udm { get; set; }
        public Visual visual { get; set; }
        public Visual_Xml visual_xml { get; set; }
    }

    public class Analytic {
        public Analytic_Links _links { get; set; }
        public string[] _permissions { get; set; }
        public object md5_digest { get; set; }
        public string status { get; set; }
        public string type { get; set; }
        public DateTime expires_at { get; set; }
        public string location { get; set; }
    }

    public class Analytic_Links {
        public string _self { get; set; }
        public string activate { get; set; }
        public string type { get; set; }
    }

    public class Analytic_Xml {
        public Analytic_Xml_Links _links { get; set; }
        public string[] _permissions { get; set; }
        public object md5_digest { get; set; }
        public string status { get; set; }
        public string type { get; set; }
        public DateTime expires_at { get; set; }
        public string location { get; set; }
    }

    public class Analytic_Xml_Links {
        public string _self { get; set; }
        public string activate { get; set; }
        public string type { get; set; }
    }

    public class Udm {
        public Udm_Links _links { get; set; }
        public string[] _permissions { get; set; }
        public object md5_digest { get; set; }
        public string status { get; set; }
        public string type { get; set; }
        public DateTime expires_at { get; set; }
        public string location { get; set; }
    }

    public class Udm_Links {
        public string _self { get; set; }
        public string activate { get; set; }
        public string type { get; set; }
    }

    public class Visual {
        public Visual_Links _links { get; set; }
        public string[] _permissions { get; set; }
        public object md5_digest { get; set; }
        public string status { get; set; }
        public string type { get; set; }
        public DateTime expires_at { get; set; }
        public string location { get; set; }
    }

    public class Visual_Links {
        public string _self { get; set; }
        public string activate { get; set; }
        public string type { get; set; }
    }

    public class Visual_Xml {
        public Visual_Xml_Links _links { get; set; }
        public string[] _permissions { get; set; }
        public object md5_digest { get; set; }
        public string status { get; set; }
        public string type { get; set; }
        public DateTime expires_at { get; set; }
        public string location { get; set; }
    }

    public class Visual_Xml_Links {
        public string _self { get; set; }
        public string activate { get; set; }
        public string type { get; set; }
    }


}
