﻿using System.Drawing;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Landdb.Client.Infrastructure;
using System.Net.Http;
using ThinkGeo.MapSuite.Core;
using NLog;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using Landdb.Resources;

namespace Landdb.ViewModel.Analysis.Planet {
    public class PlanetImageViewModel : ViewModelBase {
        readonly Logger log = LogManager.GetCurrentClassLogger();
        PlanetMapPageViewModel parent;

        public PlanetImageViewModel() {
            this.parent = null;
            statuslist.Add("Show");
            statuslist.Add("Hide");
        }

        public PlanetImageViewModel(PlanetMapPageViewModel parent, string featureid, DateTime dateacquired, float usabledata, float cloudcover, float pixel_resolution, string itemtype, ImageFeature planetfeature, string showstattus)
        {
            this.parent = parent;
            this.FeatureId = featureid;
            this.DateAcquired = dateacquired;
            this.UsableData = usabledata;
            this.CloudCover = cloudcover;
            this.Pixel_Resolution = pixel_resolution;
            this.ItemType = itemtype;
            this.PlanetFeature = planetfeature;
            this.showstatus = showstattus;
            if (parent != null)
            {
                if (showstatus == "Hide")
                {
                    parent.UpdateSelectedFeatureProperties(this);
                }
            }
            statuslist.Add("Show");
            statuslist.Add("Hide");
            RaisePropertyChanged("ShowStatus");
        }

        public PlanetImageViewModel(PlanetMapPageViewModel parent, string featureid, DateTime dateacquired, float usabledata, float cloudcover, float pixel_resolution, string itemtype, ImageFeature planetfeature, string showstatus, JsonClass.AreaOfInterest aoi, string wkt)
        {
            this.parent = parent;
            this.FeatureId = featureid;
            this.DateAcquired = dateacquired;
            this.UsableData = usabledata;
            this.CloudCover = cloudcover;
            this.Pixel_Resolution = pixel_resolution;
            this.ItemType = itemtype;
            this.PlanetFeature = planetfeature;
            this.showstatus = showstatus;
            this.AreaOfInterest = aoi;
            this.AreaInWKT = wkt;
            this.showstatus = showstatus;
            if (parent != null)
            {
                if (showstatus == "Hide")
                {
                    parent.UpdateSelectedFeatureProperties(this);
                }
            }
            statuslist.Add("Show");
            statuslist.Add("Hide");
            RaisePropertyChanged("ShowStatus");
        }

        string featureId = string.Empty;
        public string FeatureId
        {
            get { return featureId; }
            set
            {
                if (featureId == value) { return; }
                featureId = value;
                RaisePropertyChanged("FeatureId");
            }
        }

        DateTime dateAcquired;
        public DateTime DateAcquired
        {
            get { return dateAcquired; }
            set
            {
                if (dateAcquired == value) { return; }
                dateAcquired = value;
                RaisePropertyChanged("DateAcquired");
            }
        }

        string status = string.Empty;
        public string Status
        {
            get {
                return status;
            }
            set
            {
                if (status == value) { return; }
                status = value;
                if (parent != null)
                {
                    if (status == "Hide")
                    {
                        parent.UpdateSelectedFeatureProperties(this);
                    }
                }
                RaisePropertyChanged("Status");
            }
        }

        string showstatus = string.Empty;
        public string ShowStatus
        {
            get
            {
                return showstatus;
            }
            set
            {
                if (showstatus == value) { return; }
                showstatus = value;
                if (parent != null)
                {
                    if (showstatus == "Hide")
                    {
                        System.Windows.MessageBoxResult result = System.Windows.MessageBox.Show(Strings.MessageBox_PlanetImageViewModel_AreYouSureYou_Text, Strings.MessageBox_PlanetImageViewModel_ConfirmHideImage_Text, System.Windows.MessageBoxButton.OKCancel, System.Windows.MessageBoxImage.Information, System.Windows.MessageBoxResult.Cancel, (System.Windows.MessageBoxOptions)0);
                        if (result == System.Windows.MessageBoxResult.OK)
                        {
                            parent.UpdateSelectedFeatureProperties(this);
                        }
                    }
                }
                RaisePropertyChanged("ShowStatus");
            }
        }
        IList<string> statuslist = new List<string>();
        public IList<string> StatusList
        {
            get
            {
                return statuslist;
            }
        }

        float cloud_cover = 0;
        public float CloudCover
        {
            get { return cloud_cover; }
            set
            {
                if (cloud_cover == value) { return; }
                cloud_cover = value;
                RaisePropertyChanged("CloudCover");
                RaisePropertyChanged("CloudCoverPercent");
            }
        }

        public string CloudCoverPercent
        {
            get { return String.Format(Strings.PercentCloudCover_Text, (CloudCover * 100)); }
        }
        
        float usable_data = 0;
        public float UsableData
        {
            get { return usable_data; }
            set
            {
                if (usable_data == value) { return; }
                usable_data = value;
                RaisePropertyChanged("UsableData");
                RaisePropertyChanged("UsableDataPercent");
            }
        }
        public string UsableDataPercent
        {
            get { return String.Format(Strings.PercentCoverage_Format_Text, UsableData * 100); }
        }

        float pixel_resolution = 0;
        public float Pixel_Resolution
        {
            get { return pixel_resolution; }
            set
            {
                if (pixel_resolution == value) { return; }
                pixel_resolution = value;
                RaisePropertyChanged("Pixel_Resolution");
                RaisePropertyChanged("Pixel_Resolution_Display");
            }
        }

        public string Pixel_Resolution_Display
        {
            get { return String.Format(Strings.PixelResolution_Text, Pixel_Resolution.ToString("N1")); }
        }

        string item_type = string.Empty;
        public string ItemType
        {
            get { return item_type; }
            set
            {
                if (item_type == value) { return; }
                item_type = value;
                RaisePropertyChanged("ItemType");
            }
        }

        Color mapcolor = new Color();
        public Color MapColor
        {
            get { return mapcolor; }
            set
            {
                if (mapcolor == value) { return; }
                mapcolor = value;
                RaisePropertyChanged("MapColor");
                RaisePropertyChanged("WpfFillColor");
            }
        }

        public System.Windows.Media.SolidColorBrush WpfFillColor
        {
            get { return new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(mapcolor.A, mapcolor.R, mapcolor.G, mapcolor.B)); }
        }

        public int MapColorInt
        {
            get { return MapColor.ToArgb(); }
            set { MapColor = System.Drawing.Color.FromArgb(value); }
        }

        bool ischecked;
        public bool Checked
        {
            get
            {
                return ischecked;
            }
            set
            {
                ischecked = value;
                RaisePropertyChanged(() => Checked, false, ischecked, true);
            }
        }

        bool isactive = false;
        public bool IsActive
        {
            get
            {
                return isactive;
            }
            set
            {
                isactive = value;
                RaisePropertyChanged(() => IsActive, false, isactive, true);
            }
        }

        bool isdownloaded = false;
        public bool IsDownloaded
        {
            get
            {
                return isdownloaded;
            }
            set
            {
                isdownloaded = value;
                RaisePropertyChanged(() => IsDownloaded, false, isdownloaded, true);
            }
        }

        string areainwkt = string.Empty;
        public string AreaInWKT
        {
            get { return areainwkt; }
            set
            {
                if (areainwkt == value) { return; }
                areainwkt = value;
                RaisePropertyChanged("AreaInWKT");
            }
        }
        JsonClass.AreaOfInterest areaofinterest;
        public JsonClass.AreaOfInterest AreaOfInterest
        {
            get { return areaofinterest; }
            set
            {
                if (areaofinterest == value) { return; }
                areaofinterest = value;
                RaisePropertyChanged("AreaOfInterest");
            }
        }

        ImageFeature planetFeature =  null;
        public ImageFeature PlanetFeature
        {
            get { return planetFeature; }
            set
            {
                if (planetFeature == value) { return; }
                planetFeature = value;
                RaisePropertyChanged("PlanetFeature");
            }
        }

        public void ShowIntegration() {
            Task<string> getMLZone = Task.Factory.StartNew<string>(() => {

                if (NetworkStatus.IsInternetAvailable()) {
                    var httpClient1 = new HttpClient();
                    var holder = PostData<string>(httpClient1);
                    holder.Wait();
                    return holder.Result;
                }
                else {
                    return string.Empty;
                }
            });
        }

        private async Task<T> PostData<T>(System.Net.Http.HttpClient httpClient1) {
            try {
                // url and api key
                string api_key = "76415afbc2dd4decba68c160e6123e20";
                string planet_labs_url = "https://api.planet.com/data/v1/quick-search";

                // bounding box
                //RectangleShape rs = fieldsLayer.GetBoundingBox();
                //RectangleShape rs2 = projection.ConvertToInternalProjection(rs) as RectangleShape;
                //PolygonShape ps = rs2.ToPolygon();
                //string psstring = ps.GetGeoJson();

                // today's date minus 90 days
                DateTime datenow = DateTime.Now;
                DateTime datepast = datenow - new TimeSpan(90, 0, 0, 0);
                string datenowstring = $"{datenow.ToString("yyyy")}-{datenow.ToString("MM")}-{datenow.ToString("dd")}T00:00:00Z";
                string datepaststring = $"{datepast.ToString("yyyy")}-{datepast.ToString("MM")}-{datepast.ToString("dd")}T00:00:00Z";

                // array of satellite assets
                String[] data = new String[] { "PSScene4Band", "PSScene3Band", "PSOrthoTile", "REOrthoTile" };
                var satellites = string.Join(",", data.Select(o => string.Concat("\"", o, "\"")));

                string ob = "{";
                string cb = "}";

                // quick search content
                string rangefilter = $"{ob}\"type\":\"RangeFilter\",\"field_name\":\"usable_data\",\"config\":{ob}\"gte\":0.25,\"lte\":1.00{cb}{cb}";
                string permissionfilter = $"{ob}\"type\":\"PermissionFilter\",\"config\":[\"assets: download\", \"assets.visual:download\"]{cb}";
                //string geometryfilter = $"{ob}\"type\":\"GeometryFilter\",\"field_name\":\"geometry\",\"config\":{psstring}{cb}";
                string datefilter = $"{ob}\"type\":\"DateRangeFilter\",\"field_name\":\"acquired\",\"config\":{ob}\"gte\":\"{datepaststring}\",\"lte\":\"{datenowstring}\"{cb}{cb}";
                string filterarray = $"{ob}\"type\":\"AndFilter\",\"config\":[{datefilter},{rangefilter}]{cb}";
                string searchcontent = $"{ob}\"item_types\":[{satellites}], \"filter\":{filterarray}{cb}";

                //log.Info($"Bounding Box:  {psstring}");
                log.Info($"Http string content:  {searchcontent}");

                // Http Client
                //var httpClient1 = new HttpClient();
                var credentials1 = Encoding.ASCII.GetBytes($"{api_key}:");
                // headers
                httpClient1.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(credentials1));
                httpClient1.DefaultRequestHeaders.Accept.Clear();
                httpClient1.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // content
                StringContent stringContent1 = new StringContent(searchcontent);
                stringContent1.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                // post
                HttpResponseMessage httpResponseMessage1 = await httpClient1.PostAsync($"{planet_labs_url}", stringContent1);
                // response
                if (httpResponseMessage1.IsSuccessStatusCode) {
                    log.Info($"Result Response:  {httpResponseMessage1.IsSuccessStatusCode.ToString()}");
                    var jsonresponse = await httpResponseMessage1.Content.ReadAsStringAsync();
                    QuickSearchResults quicksearchresults = JsonConvert.DeserializeObject<QuickSearchResults>(jsonresponse);
                    var fake = string.Empty;
                    var result = JsonConvert.DeserializeObject<T>(fake);
                    return result;
                }
                else {
                    string failureMsg = "HTTP Status: " + httpResponseMessage1.StatusCode.ToString() + " - Reason: " + httpResponseMessage1.ReasonPhrase;
                    log.Info($"Result Response:  {failureMsg}");
                    var fake = string.Empty;
                    var result = JsonConvert.DeserializeObject<T>(fake);
                    return result;
                }

            }
            catch (Exception ex) {
                throw new ApplicationException("Share Failed.");
            }
        }
    }
}
