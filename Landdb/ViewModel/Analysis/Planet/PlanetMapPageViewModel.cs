﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GalaSoft.MvvmLight;
using ThinkGeo.MapSuite.WpfDesktopEdition;
using ThinkGeo.MapSuite.Core;
using Landdb.Infrastructure;
using System.IO;
using Microsoft.Win32;
using GalaSoft.MvvmLight.Command;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Collections.ObjectModel;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure.Messages;
using Landdb.Domain.ReadModels.Tree;
using Landdb.MapStyles;
using Landdb.Client.Infrastructure;
using System.Windows.Threading;
using Landdb.ViewModel.Maps.ImageryOverlay;
using NLog;
using Landdb.ViewModel.Fields;
using Landdb.ViewModel.Fields.FieldDetails;
using Landdb.ViewModel.Fields.FieldDetails.MapModels;
using Landdb.Domain.ReadModels.Map;
using Newtonsoft.Json;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Drawing;
using System.Security.Cryptography;
using System.Reflection;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Diagnostics;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using Landdb.Resources;
using Landdb.ViewModel.Shared;

//commandline feature activation --ShowPlanetImagery

namespace Landdb.ViewModel.Analysis.Planet {
    public class PlanetMapPageViewModel : ViewModelBase {

        readonly IClientEndpoint clientEndpoint;
        readonly Dispatcher dispatcher;
        readonly Logger log = LogManager.GetCurrentClassLogger();

        readonly IDetailsViewModelFactory detailsFactory;

        CropYearId currentCropYearId;

        object detailsViewModel = new HomeDetailsViewModel(null);
        AbstractTreeItemViewModel selectedTreeItem;

        bool isLoadingTree;
        bool isLoadingTree2;
        bool isFilterVisible = false;
        string filterHeader = string.Empty;
        MapSettings mapsettings;
        AgC.UnitConversion.Area.AreaUnit agcAreaUnit;
        ThinkGeo.MapSuite.Core.AreaUnit mapAreaUnit;
        ThinkGeo.MapSuite.Core.DistanceUnit mapDistanceUnit;
        string areaUnitString = string.Empty;
        bool displayareainfieldlabel = false;
        bool displayareaonlylabel = false;
        bool displaymapzoombar = false;
        bool isOptionsMapPopupOpen = false;
        bool showfieldmapanddata = true;
        bool showgrid = false;
        bool showsubscriptiongrid = false;
        bool cancreateSubscription = false;
        string toggleimagery = Strings.Ndvi_Text;

        bool maploaded = false;
        bool startedOutOnline = false;
        string fieldname = Strings.DragLabel_Text;
        IList<MapItem> clientLayerItems;
        InMemoryFeatureLayer fieldsLayer = new InMemoryFeatureLayer();
        InMemoryFeatureLayer fieldsLayer2 = new InMemoryFeatureLayer();
        InMemoryFeatureLayer currentLayer = new InMemoryFeatureLayer();
        InMemoryFeatureLayer selectedlayer = new InMemoryFeatureLayer();
        LayerOverlay layerOverlay = new LayerOverlay();
        Proj4Projection projection;
        Proj4Projection projection2;
        Proj4Projection projection84;
        Proj4Projection projection100;
        //RectangleShape previousmapextent = new RectangleShape();
        Feature selectedfield = new Feature();
        Feature loadedfeature = new Feature();
        BaseShape fieldshape = new MultipolygonShape();
        ClientLayerAreaStyle fieldsLayerAreaStyle = null;
        string activemap = string.Empty;
        ZoomLevelFactory zoomlevelfactory = new ZoomLevelFactory();
        string coordinateformat = string.Empty;
        IList<PlanetImageViewModel> backgroundList = new List<PlanetImageViewModel>();
        IList<PlanetImageViewModel> imagelist = new List<PlanetImageViewModel>();
        IList<PlanetImageViewModel> imagecollection = new List<PlanetImageViewModel>();
        PlanetImageViewModel selectedImage = new PlanetImageViewModel();
        HttpClient httpClient1;
        PlanetImageViewModel currentimage;
        static AssetStatusResponse currentassetstatusresults;
        IList<PlanetImageViewModel> currentimagelist = new List<PlanetImageViewModel>();
        IList<AssetStatusResponse> currentassetlist = new List<AssetStatusResponse>();
        string selectedid = string.Empty;
        string selectedname = string.Empty;
        string filetoupload = string.Empty;
        string filetoupload2 = string.Empty;
        TreeDisplayModeDisplayItem selectedTreeDisplayMode { get; set; }
        RectangleShape savedextentforimages;


        Task imageryLoadTask;
        Task retrieveImageryTask;

        // TODO: Obsolete this constructor?
        public PlanetMapPageViewModel(IClientEndpoint clientEndpoint) : this(clientEndpoint, new DetailsViewModelFactory(clientEndpoint), Application.Current.Dispatcher) { }

        public PlanetMapPageViewModel(IClientEndpoint clientEndpoint, IDetailsViewModelFactory detailsFactory, Dispatcher dispatcher) {
            this.dispatcher = dispatcher;
            this.clientEndpoint = clientEndpoint;
            this.detailsFactory = detailsFactory;

            currentCropYearId = new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear);
            ShowGrid = true;
            ShowSubscriptionGrid = true;
            ShowFieldMapAndData = true;

            imagelist = new List<PlanetImageViewModel>();
            imagecollection = new List<PlanetImageViewModel>();

            mapsettings = clientEndpoint.GetMapSettings();
            areaUnitString = mapsettings.MapAreaUnit;
            agcAreaUnit = AgC.UnitConversion.UnitFactory.GetUnitByName(areaUnitString) as AgC.UnitConversion.Area.AreaUnit;
            mapAreaUnit = MapUnitFactory.AreaMeasureConversion(agcAreaUnit);
            mapDistanceUnit = MapUnitFactory.AreaDistanceMeasureConversion(agcAreaUnit);
            if (mapsettings.DisplayAreaInFieldLabel.HasValue) {
                displayareainfieldlabel = mapsettings.DisplayAreaInFieldLabel.Value;
            }
            if (mapsettings.DisplayAreaOnlyLabel.HasValue) {
                displayareaonlylabel = mapsettings.DisplayAreaOnlyLabel.Value;
            }
            displaymapzoombar = false;
            if (mapsettings.DisplayMapZoomBar.HasValue) {
                displaymapzoombar = mapsettings.DisplayMapZoomBar.Value;
            }

            ToggleImagery = "NDVI";


            currentimagelist = new List<PlanetImageViewModel>();
            currentassetlist = new List<AssetStatusResponse>();

            Messenger.Default.Register<DataSourceChangedMessage>(this, OnDataSourceChanged);

            InitializeMap();


            LoadXmlCommand = new RelayCommand(LoadXML);
            ToggleImageCommand = new RelayCommand(ToggleImageType);
            LoadGeometryCommand = new RelayCommand(LoadGeometry);
            CreateSubscriptionCommand = new RelayCommand(CreateSubScription);
            DownloadSyncCommand = new RelayCommand(DownloadFilesSyncCommand);
            FilterPlanetImageryCommand = new RelayCommand(ShowIntegration);
            ActivatePlanetImageryCommand = new RelayCommand(ActivateFiles);
            DownloadPlanetImageryCommand = new RelayCommand(DownloadFiles);
            DownloadPlanetAnalyticalCommand = new RelayCommand(DownloadAnalyticalFiles);
            ApplyFilterCommand = new RelayCommand(ApplyFilter);
            ClearFilterCommand = new RelayCommand(ClearFilter);
            PrintMapCommand = new RelayCommand(Print);
            UpdateFeaturePropertyCommand = new RelayCommand<PlanetImageViewModel>(UpdateFeatureProperty);

            FilterModel = new TreeFilterModel(clientEndpoint);

            HideRemovalPopupCommand = new RelayCommand(HideRemovalPopup);

            //RetrievePlanetPermissions();


            if (currentCropYearId.DataSourceId != Guid.Empty && currentCropYearId.Id > 1980) {

                //Task.Run(() =>
                //{
                //    BuildImageryList();
                //});
                RebuildFarms();
                //ProcessSubscriptionFiles();
                RetrieveImageryList();
                //Task popPest = Task.Factory.StartNew(PopulatePests);
                //Task.Run(() => { BuildProductList(); });

            }

            //Messenger.Default.Register<UpdateTreeMessage>(this, OnTreeUpdated);
        }

        private void HideRemovalPopup() {
            IsRemovalPopupOpen = false;
        }

        public ICommand LoadXmlCommand { get; }
        public ICommand ToggleImageCommand { get; }
        public ICommand LoadGeometryCommand { get; }
        public ICommand CreateSubscriptionCommand { get; }
        public ICommand DownloadSyncCommand { get; }
        public ICommand FilterPlanetImageryCommand { get; }
        public ICommand ActivatePlanetImageryCommand { get; }
        public ICommand DownloadPlanetImageryCommand { get; }
        public ICommand DownloadPlanetAnalyticalCommand { get; }
        public ICommand ApplyFilterCommand { get; }
        public ICommand ClearFilterCommand { get; }
        public ICommand Cancel { get; set; }
        public ICommand HideRemovalPopupCommand { get; }
        public ICommand PrintMapCommand { get; private set; }
        public ICommand UpdateFeaturePropertyCommand { get; }


        public WpfMap Map { get; set; }

        public IPageFilterViewModel FilterModel { get; protected set; }

        public IList<MapItem> ClientLayer {
            get { return clientLayerItems; }
            set {
                clientLayerItems = value;
                //mapDisplayOptionsVisibility = Visibility.Collapsed;
                //annotationsvm.ClientLayer = value;
                RaisePropertyChanged("ClientLayer");
            }
        }

        public bool IsField {
            get {
                if (RootTreeItemModelsView == null) {
                    return false;
                } else {
                    AbstractTreeItemViewModel parentNode = SelectedTreeItem;

                    if (SelectedTreeItem == null) {
                        parentNode = RootTreeItemModels.FirstOrDefault();
                    }

                    if (parentNode is FieldTreeItemViewModel) {
                        return true;
                    } else {
                        //no cz spliter
                        return false;
                    }
                }
            }
        }

        public bool IsLoadingTree {
            get { return isLoadingTree; }
            set {
                isLoadingTree = value;
                IsLoadingTree2 = isLoadingTree;
                RaisePropertyChanged(() => IsLoadingTree);
            }
        }

        public bool IsLoadingTree2 {
            get { return isLoadingTree2; }
            set {
                isLoadingTree2 = value;
                RaisePropertyChanged(() => IsLoadingTree2);
            }
        }


        public ObservableCollection<GrowerTreeItemViewModel> RootTreeItemModels { get; private set; }
        public CollectionView RootTreeItemModelsView { get; private set; }

        public object DetailsViewModel {
            get { return detailsViewModel; }
            set {
                detailsViewModel = value;
                LoadMapForItem(value, dispatcher);
                RaisePropertyChanged(() => DetailsViewModel);
            }
        }

        public FieldSummaryViewModel ApplicationSummary { get; }
        public YieldSummaryViewModel YieldSummary { get; }

        //public MapViewModel MapViewModel {
        //	get { return mapViewModel; }
        //	private set {
        //		mapViewModel = value;
        //		RaisePropertyChanged(() => MapViewModel);
        //	}
        //}

        public AbstractTreeItemViewModel SelectedTreeItem {
            get { return selectedTreeItem; }
            set {
                selectedTreeItem = value;
                //IsMassCropPopupOpen = false;
                IsRemovalPopupOpen = false;
                CanCreateSubscription = false;
                //RaisePropertyChanged(() => IsMassCropPopupOpen);
                RaisePropertyChanged(() => SelectedTreeItem);
                if (value == null) { return; }

                List<string> mapitems = GatherSelectedShapes(selectedTreeItem);


                //LoadMapShapes(mapitems);
                CanASubscriptionBeCreated(selectedTreeItem);
                LoadImageryFiles(selectedid);
                //if (backgroundList.Count < 1)
                //{
                //    if (Map.Overlays.Contains("Tiff"))
                //    {
                //        LoadBingMapsOverlay();
                //    }
                //}
                LoadMapShapes(mapitems);

                //ShowIntegration();

                //detailsFactory.CreateDetailsViewModel(SelectedTreeItem.Id, clientLayerItems, SelectedTreeItem,
                //    viewModel => {
                //        dispatcher.BeginInvoke(new Action(() => { // if waiting, this could come through a worker thread, so use the dispatcher to route it appropriately.
                //            DetailsViewModel = viewModel;
                //        }));
                //    });
            }
        }

        private void CanASubscriptionBeCreated(AbstractTreeItemViewModel selectedtreeitem) {
            var maindatasource = Path.Combine(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory, Infrastructure.ApplicationEnvironment.CurrentDataSourceId.ToString());
            if (!System.IO.Directory.Exists(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory)) {
                System.IO.Directory.CreateDirectory(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory);
            }
            if (!System.IO.Directory.Exists(maindatasource)) {
                System.IO.Directory.CreateDirectory(maindatasource);
            }
            string jsonfilename = $"{selectedid}info.json";
            var fulldirectoryname = Path.Combine(maindatasource, jsonfilename);
            if (!File.Exists(fulldirectoryname)) {
                CanCreateSubscription = true;
            } else {
                CanCreateSubscription = false;
                return;
            }
            RectangleShape rs = fieldsLayer.GetBoundingBox();
            RectangleShape rs2 = projection.ConvertToInternalProjection(rs) as RectangleShape;
            PolygonShape ps = rs2.ToPolygon();
            double polyarea = rs2.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
            if (polyarea > 0) {
                CanCreateSubscription = true;
            } else {
                CanCreateSubscription = false;
                return;
            }

            if (SelectedTreeItem.Id is FarmId) {
                CanCreateSubscription = true;
            } else if (SelectedTreeItem.Id is FieldId) {
                CanCreateSubscription = true;
            } else if (SelectedTreeItem.Id is CropZoneId) {
                CanCreateSubscription = true;
            } else if (SelectedTreeItem.Id is CropId) {
                CanCreateSubscription = true;
            } else {
                CanCreateSubscription = false;
                return;
            }
        }

        public bool IsOptionsMapPopupOpen {
            get { return isOptionsMapPopupOpen; }
            set {
                isOptionsMapPopupOpen = value;
                RaisePropertyChanged("IsOptionsMapPopupOpen");
            }
        }

        public bool ShowGrid {
            get { return showgrid; }
            set {
                if (showgrid == value) { return; }
                showgrid = value;
                RaisePropertyChanged("ShowGrid");
            }
        }

        public bool ShowSubscriptionGrid {
            get { return showsubscriptiongrid; }
            set {
                if (showsubscriptiongrid == value) { return; }
                showsubscriptiongrid = value;
                RaisePropertyChanged("ShowSubscriptionGrid");
            }
        }

        public bool ShowFieldMapAndData {
            get { return showfieldmapanddata; }
            set {
                if (showfieldmapanddata == value) { return; }
                showfieldmapanddata = value;
                RaisePropertyChanged("ShowFieldMapAndData");
            }
        }

        public bool CanCreateSubscription {
            get { return cancreateSubscription; }
            set {
                if (cancreateSubscription == value) { return; }
                cancreateSubscription = value;
                RaisePropertyChanged("CanCreateSubscription");
            }
        }
        public string ToggleImagery {
            get { return toggleimagery; }
            set {
                if (toggleimagery == value) { return; }
                toggleimagery = value;
                RaisePropertyChanged("ToggleImagery");
            }
        }

        private IList<string> list_of_Satellites = new List<string>();
        public IList<string> List_of_Satellites {
            get { return list_of_Satellites; }
            set {
                if (list_of_Satellites == value) { return; }
                list_of_Satellites = value;
                RaisePropertyChanged("List_of_Satellites");
            }
        }

        private int filter_Days = 30;
        public int Filter_Days {
            get { return filter_Days; }
            set {
                if (filter_Days == value) { return; }
                filter_Days = value;
                RaisePropertyChanged("Filter_Days");
            }
        }

        private double filter_Usable_Data = .25;
        public double Filter_Usable_Data {
            get { return filter_Usable_Data; }
            set {
                if (filter_Usable_Data == value) { return; }
                filter_Usable_Data = value;
                RaisePropertyChanged("Filter_Usable_Data");
            }
        }

        private double filter_Cloud_Cover = .75;
        public double Filter_Cloud_Cover {
            get { return filter_Cloud_Cover; }
            set {
                if (filter_Cloud_Cover == value) { return; }
                filter_Cloud_Cover = value;
                RaisePropertyChanged("Filter_Cloud_Cover");
            }
        }

        public static List<TreeDisplayModeDisplayItem> AvailableTreeDisplayModes =>
            new List<TreeDisplayModeDisplayItem> {
                new TreeDisplayModeDisplayItem("Farms"),
                new TreeDisplayModeDisplayItem("Crops")
            };

        //public TreeDisplayMode SelectedTreeDisplayMode
        //{
        //    get { return selectedTreeDisplayMode; }
        //    set
        //    {
        //        if (selectedTreeDisplayMode != value)
        //        {
        //            selectedTreeDisplayMode = value;

        //            if (RootTreeItemModels != null)
        //            {
        //                RootTreeItemModels.Clear();
        //            }

        //            BuildTreeList();

        //            RaisePropertyChanged(() => SelectedTreeDisplayMode);
        //        }
        //    }
        //}

        void OnDataSourceChanged(DataSourceChangedMessage message) {
            currentCropYearId = new CropYearId(message.DataSourceId, message.CropYear);
            ShowGrid = false;
            ShowFieldMapAndData = false;
            IsLoadingTree2 = true;
            IsFilterVisible = false;
        }

        private void RebuildFarms() {
            dispatcher.BeginInvoke(new Action(() => {
                IsLoadingTree = true;
                var begin = DateTime.UtcNow;
                var years = clientEndpoint.GetView<FieldTreeView>(new DataSourceId(currentCropYearId.DataSourceId));

                var yearItem = (years.HasValue && years.Value.CropYearList.ContainsKey(currentCropYearId.Id)) ? years.Value.CropYearList[currentCropYearId.Id] : new TreeViewYearItem();
                IList<TreeViewFarmItem> farms = yearItem.Farms != null ? yearItem.Farms : new List<TreeViewFarmItem>();

                clientLayerItems = yearItem.Maps.Where(m => m.IsVisible).ToList();

                var userSettings = clientEndpoint.GetUserSettings();
                bool sortAlphabetically = userSettings.AreFieldsSortedAlphabetically;

                RootTreeItemModels = new ObservableCollection<GrowerTreeItemViewModel>() { new GrowerTreeItemViewModel(Strings.Home_Text, farms, sortAlphabetically, filter: FilterModel.FilterItem) };
                RootTreeItemModels[0].IsExpanded = true;

                dispatcher.BeginInvoke(new Action(() => {
                    RootTreeItemModelsView = (CollectionView)CollectionViewSource.GetDefaultView(RootTreeItemModels);
                    RootTreeItemModelsView.Filter = FilterModel.FilterItem;
                    GenerateFilterHeader();
                    RaisePropertyChanged(() => RootTreeItemModelsView);
                }));

                RaisePropertyChanged(() => RootTreeItemModels);

                var end = DateTime.UtcNow;
                IsLoadingTree = false;

                log.Debug($"Tree load took {(end - begin).TotalMilliseconds}ms");
            }));
        }

        public bool IsFilterVisible {
            get { return isFilterVisible; }
            set {
                isFilterVisible = value;
                RaisePropertyChanged(() => IsFilterVisible);
            }
        }

        private string removalPopupText;
        public string RemovalPopupText {
            get { return removalPopupText; }
            set {
                removalPopupText = value;
                RaisePropertyChanged(() => RemovalPopupText);
            }
        }

        private bool isRemovalPopupOpen;
        public bool IsRemovalPopupOpen {
            get { return isRemovalPopupOpen; }
            set {
                isRemovalPopupOpen = value;
                RaisePropertyChanged(() => IsRemovalPopupOpen);
            }
        }

        public string FilterHeader {
            get { return filterHeader; }
            set {
                filterHeader = value;
                RaisePropertyChanged(() => FilterHeader);
            }
        }

        public IList<PlanetImageViewModel> ImageList {
            get { return imagelist; }
            set {
                imagelist = value;
                RaisePropertyChanged("ImageList");
            }
        }

        public IList<PlanetImageViewModel> BackgroundList {
            get { return backgroundList; }
            set {
                backgroundList = value;
                RaisePropertyChanged("BackgroundList");
            }
        }

        public IList<PlanetImageViewModel> ImageCollection {
            get { return imagecollection; }
            set {
                imagecollection = value;
                RaisePropertyChanged("ImageCollection");
            }
        }

        public PlanetImageViewModel SelectedImage {
            get { return selectedImage; }
            set {
                selectedImage = value;
                if (toggleimagery == "NDVI") {
                    LoadMapBackgroundSelection(true);
                } else {
                    LoadMapBackgroundSelection(false);
                }

                RaisePropertyChanged("SelectedImage");
            }
        }

        void ClearFilter() {
            if (FilterModel != null && RootTreeItemModelsView != null) {
                FilterModel.ClearFilter();
                FilterModel.BeforeFilter();

                if (IsFiltered()) {
                    RootTreeItemModelsView.Refresh();
                    GenerateFilterHeader();
                    IsFilterVisible = false;
                }
            }
        }

        void ApplyFilter() {
            if (FilterModel != null && RootTreeItemModelsView != null) {
                FilterModel.BeforeFilter();
                RootTreeItemModelsView.Refresh();
                GenerateFilterHeader();
                IsFilterVisible = false;
            }
        }

        bool IsFiltered() {
            var originalCount = (from r in RootTreeItemModels
                                 from fa in r.Children
                                 select fa.Children.Count).Sum();
            var filteredCount = (from r in RootTreeItemModels
                                 from fa in r.Children
                                 select fa.FilteredChildren.Count).Sum();
            return originalCount != filteredCount;
        }

        void GenerateFilterHeader() {
            var originalCount = (from r in RootTreeItemModels
                                 from fa in r.Children
                                 select fa.Children.Count).Sum();
            var filteredCount = (from r in RootTreeItemModels
                                 from fa in r.Children
                                 select fa.FilteredChildren.Count).Sum();

            string entityPluralityAwareName = originalCount == 1 ? Strings.Field_Text : Strings.Fields_Text;

            if (originalCount != filteredCount) {
                FilterHeader = String.Format(Strings.FilteredCountOfOriginalCount_Text, filteredCount, originalCount, entityPluralityAwareName);
            } else {
                //FilterHeader = string.Format("{0} {1}", filteredCount, entityPluralityAwareName);
                FilterHeader = Strings.AllFields_Text;
            }
        }


        internal void EnsureFieldsLayerIsOpen() {
            if (fieldsLayer == null) {
                fieldsLayer = new InMemoryFeatureLayer();
            }

            if (!fieldsLayer.IsOpen) {
                fieldsLayer.Open();
            }

            if (selectedlayer == null) {
                selectedlayer = new InMemoryFeatureLayer();
            }

            if (!selectedlayer.IsOpen) {
                selectedlayer.Open();
            }

            if (!projection.IsOpen) {
                projection.Open();
            }

            if (!projection2.IsOpen) {
                projection2.Open();
            }

            if (!projection84.IsOpen) {
                projection84.Open();
            }
        }

        void InitializeMap() {
            Map = new WpfMap();
            startedOutOnline = Landdb.Client.Infrastructure.NetworkStatus.IsInternetAvailableFast();
            Map.MapUnit = GeographyUnit.Meter;
            Map.MapTools.PanZoomBar.Visibility = System.Windows.Visibility.Visible;
            Map.MapTools.PanZoomBar.GlobeButtonVisibility = System.Windows.Visibility.Hidden;
            Map.MapTools.PanZoomBar.VerticalAlignment = System.Windows.VerticalAlignment.Center;
            Map.MapTools.PanZoomBar.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            if (!displaymapzoombar) {
                Map.MapTools.PanZoomBar.Visibility = System.Windows.Visibility.Collapsed;
            }

            Map.Background = System.Windows.Media.Brushes.White;
            Map.MouseLeftButtonDown += (sender, e) => {
                ((MainWindow)System.Windows.Application.Current.MainWindow).Draggable = false;
            };
            Map.ExtentOverlay.DoubleLeftClickMode = MapDoubleLeftClickMode.Disabled;
            zoomlevelfactory = new ZoomLevelFactory();
            Map.ZoomLevelSet = zoomlevelfactory.ZoomLevelSET;

            projection = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetGoogleMapParametersString());
            projection2 = new Proj4Projection(Proj4Projection.GetGoogleMapParametersString(), Proj4Projection.GetWgs84ParametersString());
            projection84 = new Proj4Projection(Proj4Projection.GetEpsgParametersString(4326), Proj4Projection.GetEpsgParametersString(4326));

            Map.Loaded += (sender, e) => {
                if (maploaded) { return; }
                try {
                    LoadBingMapsOverlay();
                    OpenAndLoadaShapefile();
                    maploaded = true;
                } catch { }
            };

            Map.CurrentScaleChanged += (sender, e) => {
                //scalingTextStyle.MapScale = e.CurrentScale;
            };

            //Map.MapClick += (sender, e) => {
            //    SavePolygon();
            //};

        }

        public void LoadPlanetMapBackground(string filename, int epsg, RectangleShape imageextent, bool isclipped) {
            if (!startedOutOnline) { return; }
            Overlay bingOverlay = new PlanetImageOverlay().LoadImageOnOverlay(filename, epsg, imageextent, isclipped);
            if (bingOverlay == null) { return; }

            if (Map.Overlays.Contains("Tiff")) {
                Map.Overlays.Remove("Tiff");
            }
            //if (Map.Overlays.Contains("Bing"))
            //{
            //    Map.Overlays.Remove("Bing");
            //}
            //if (Map.Overlays.Contains("Bing Online"))
            //{
            //    Map.Overlays.Remove("Bing Online");
            //}
            //if (Map.Overlays.Contains("Roads Only"))
            //{
            //    Map.Overlays.Remove("Roads Only");
            //}
            //if (Map.Overlays.Contains("Road Online"))
            //{
            //    Map.Overlays.Remove("Road Online");
            //}
            //if (Map.Overlays.Contains("None"))
            //{
            //    Map.Overlays.Remove("None");
            //}
            //bingOverlay.IsVisible = mapoverlay.Visibility;
            //((TileOverlay)bingOverlay).TileType = TileType.SingleTile;
            Map.Overlays.Add("Tiff", bingOverlay);
            if (Map.Overlays.Contains("Tiff")) {
                Map.Overlays.MoveToTop("Tiff");
            }
            if (Map.Overlays.Contains("fieldsOverlay")) {
                Map.Overlays.MoveToTop("fieldsOverlay");
            }
            //Map.Overlays.MoveToBottom("Tiff");
            //Map.Refresh();
        }

        private void LoadBingMapsOverlay() {
            if (!startedOutOnline) { return; }
            IMapOverlay mapoverlay = new BingMapOverlay();
            Overlay bingOverlay = mapoverlay.Overlay;
            if (bingOverlay == null) { return; }

            if (Map.Overlays.Contains("Bing")) {
                Map.Overlays.Remove("Bing");
            }
            if (Map.Overlays.Contains("Bing Online")) {
                Map.Overlays.Remove("Bing Online");
            }
            if (Map.Overlays.Contains("Roads Only")) {
                Map.Overlays.Remove("Roads Only");
            }
            if (Map.Overlays.Contains("Road Online")) {
                Map.Overlays.Remove("Road Online");
            }
            if (Map.Overlays.Contains("None")) {
                Map.Overlays.Remove("None");
            }
            //bingOverlay.IsVisible = mapoverlay.Visibility;
            //((TileOverlay)bingOverlay).TileType = TileType.SingleTile;
            Map.Overlays.Add(mapoverlay.Name, bingOverlay);
            Map.Overlays.MoveToBottom(mapoverlay.Name);
            if (Map.Overlays.Contains("fieldsOverlay")) {
                Map.Overlays.MoveToTop("fieldsOverlay");
            }

        }


        internal void LoadMapForItem(object item, Dispatcher dispatcher) {
            IList<string> list_of_Satellites1 = new List<string>();
            list_of_Satellites1.Add("PSScene4Band");
            list_of_Satellites1.Add("PSOrthoTile");
            list_of_Satellites1.Add("REOrthoTile");
            List_of_Satellites = list_of_Satellites1;
            Filter_Days = 2;
            Filter_Usable_Data = 70;
            Filter_Cloud_Cover = 10;

            IIdentity itemId = null;
            selectedid = string.Empty;
            selectedname = string.Empty;
            HomeDetailsViewModel homitem = null;
            FarmDetailsViewModel frmitem = null;
            FieldDetailsViewModel fvitem = null;
            CropZoneDetailsViewModel czitem = null;
            IIdentity selectedIdentity = null;
            string stringid = string.Empty;
            if (item is HomeDetailsViewModel) {
                homitem = (HomeDetailsViewModel)item;
                selectedfield = new Feature();
            }
            if (item is FarmDetailsViewModel) {
                itemId = ((FarmDetailsViewModel)item).FarmDetails.Id;
                frmitem = (FarmDetailsViewModel)item;
                selectedIdentity = itemId;
                selectedname = frmitem.Name;
                selectedfield = new Feature();
            }
            if (item is FieldDetailsViewModel) {
                itemId = ((FieldDetailsViewModel)item).FieldView.Id;
                stringid = ((FieldDetailsViewModel)item).FieldView.Id.Id.ToString();
                fvitem = (FieldDetailsViewModel)item;
                selectedIdentity = itemId;
                selectedname = $"{((FarmTreeItemViewModel)fvitem.FieldTreeItemVM.Parent).Name}:{fvitem.Name}";
                selectedfield = new Feature();
            }
            if (item is CropZoneDetailsViewModel) {

                itemId = ((CropZoneDetailsViewModel)item).CropZoneView.Id;
                stringid = ((CropZoneDetailsViewModel)item).CropZoneView.Id.Id.ToString();
                czitem = (CropZoneDetailsViewModel)item;
                selectedIdentity = itemId;
                selectedname = $"{czitem.Name}";
            }
            //if (itemId == null) {
            //    if (homitem != null) {
            //        foreach (var m in clientLayerItems) {
            //            stringid = m.FieldId.ToString(); ;
            //            if (m.MapData != null && !string.IsNullOrEmpty(m.MapData) && m.MapData != "MULTIPOLYGON EMPTY") {
            //                IDictionary<string, string> columnvalues = new Dictionary<string, string>();
            //                columnvalues.Add(@"LdbLabel", m.Name);
            //                var testfeat = new Feature(m.MapData);
            //                BaseShape testbase = testfeat.GetShape();
            //                var f = new Feature(m.MapData, m.FieldId.ToString(), columnvalues);
            //                MultipolygonShape mdataconvert = new MultipolygonShape();
            //                if (testbase is PolygonShape) {
            //                    mdataconvert.Polygons.Add(testbase as PolygonShape);
            //                    f = new Feature(mdataconvert.GetWellKnownText(), m.FieldId.ToString(), columnvalues);
            //                }
            //                if (testbase is MultipolygonShape) {
            //                    mdataconvert = new MultipolygonShape(m.MapData);
            //                    f = new Feature(mdataconvert.GetWellKnownText(), m.FieldId.ToString(), columnvalues);
            //                }

            //                if (!currentLayer.InternalFeatures.Contains(m.FieldId.ToString())) {
            //                    currentLayer.InternalFeatures.Add(m.FieldId.ToString(), f);
            //                }
            //            }
            //        }
            //        fieldsLayerAreaStyle.SelectedFeatures = currentLayer.InternalFeatures;
            //        if (Map.Overlays.Contains("fieldOverlay")) {
            //            try {
            //                if (currentLayer.InternalFeatures.Count > 0) {
            //                    Map.CurrentExtent = currentLayer.GetBoundingBox();
            //                    PointShape center = currentLayer.GetBoundingBox().GetCenterPoint();
            //                    Map.CenterAt(center);
            //                }
            //                Map.Overlays["fieldOverlay"].Refresh();
            //            }
            //            catch (Exception exception) {
            //                log.ErrorException("Couldn't load the current item's map.", exception);
            //            }
            //        }
            //    }
            //    if (Map.Overlays.Contains("fieldOverlay")) {
            //        try {
            //            Map.Overlays["fieldOverlay"].Refresh();
            //        }
            //        catch (Exception exception) {
            //            log.ErrorException("Couldn't load the current item's map.", exception);
            //        }

            //    }
            //    return;
            //}



            //if (frmitem != null) {
            //    MultipolygonShape mps2 = new MultipolygonShape();
            //    selectedid = frmitem.FarmDetails.Id.Id.ToString();
            //    foreach (var m in clientLayerItems) {
            //        stringid = m.FieldId.ToString(); ;
            //        if (m.FarmId.ToString() == selectedid) {
            //            if (m.MapData != null && !string.IsNullOrEmpty(m.MapData) && m.MapData != @"MULTIPOLYGON EMPTY") {
            //                if (m.FarmId.ToString() == selectedid) {
            //                    IDictionary<string, string> columnvalues = new Dictionary<string, string>();
            //                    columnvalues.Add(@"LdbLabel", m.Name);
            //                    var testfeat = new Feature(m.MapData);
            //                    BaseShape testbase = testfeat.GetShape();
            //                    var f = new Feature(m.MapData, m.FieldId.ToString(), columnvalues);
            //                    MultipolygonShape mdataconvert = new MultipolygonShape();
            //                    if (testbase is PolygonShape) {
            //                        mdataconvert.Polygons.Add(testbase as PolygonShape);
            //                        f = new Feature(mdataconvert.GetWellKnownText(), m.FieldId.ToString(), columnvalues);
            //                    }
            //                    if (testbase is MultipolygonShape) {
            //                        mdataconvert = new MultipolygonShape(m.MapData);
            //                        f = new Feature(mdataconvert.GetWellKnownText(), m.FieldId.ToString(), columnvalues);
            //                    }

            //                    if (!currentLayer.InternalFeatures.Contains(m.FieldId.ToString())) {
            //                        currentLayer.InternalFeatures.Add(m.FieldId.ToString(), f);
            //                    }
            //                }
            //            }
            //        }
            //    }
            //    fieldsLayerAreaStyle.SelectedFeatures = currentLayer.InternalFeatures;

            //    if (Map.Overlays.Contains("fieldOverlay")) {
            //        try {
            //            if (currentLayer.InternalFeatures.Count > 0) {
            //                Map.CurrentExtent = currentLayer.GetBoundingBox();
            //                PointShape center1 = currentLayer.GetBoundingBox().GetCenterPoint();
            //                Map.CenterAt(center1);
            //            }
            //            Map.Overlays["fieldOverlay"].Refresh();
            //        }
            //        catch (Exception exception) {
            //            log.ErrorException("Couldn't load the current item's map.", exception);
            //        }

            //    }
            //    return;
            //}


            //var maps = clientEndpoint.GetView<ItemMap>(itemId);
            //if (maps.HasValue) {
            //    ItemMapItem mri = maps.Value.MostRecentMapItem;
            //    if (!string.IsNullOrEmpty(mri.MapData) && mri.MapData != "MULTIPOLYGON EMPTY") {
            //        var feature = new Feature(mri.MapData, stringid);
            //        BaseShape testbase = feature.GetShape();
            //        MultipolygonShape mdataconvert = new MultipolygonShape();
            //        if (testbase is PolygonShape) {
            //            mdataconvert.Polygons.Add(testbase as PolygonShape);
            //            feature = new Feature(mdataconvert.GetWellKnownText(), stringid);
            //        }

            //        BaseShape bs1 = feature.GetShape();
            //        MultipolygonShape abs100 = feature.GetShape() as MultipolygonShape;
            //        double abs100area = abs100.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
            //        string featureid = feature.Id;
            //        MultipolygonShape mps1 = new MultipolygonShape();
            //        PointShape center = new PointShape();
            //        PointShape selectedcenter = new PointShape();
            //        if (bs1 is MultipolygonShape) {
            //            mps1 = bs1 as MultipolygonShape;
            //            center = mps1.GetCenterPoint();
            //            selectedcenter = mps1.GetCenterPoint();
            //            center = projection.ConvertToExternalProjection(center) as PointShape;
            //        }

            //        if (fvitem != null) {
            //            selectedid = fvitem.FieldView.Id.Id.ToString();
            //            feature.ColumnValues.Add(@"LdbLabel", fvitem.FieldView.FieldNameByCropYear[ApplicationEnvironment.CurrentCropYear]);
            //            currentLayer.InternalFeatures.Add(selectedid, feature);
            //            fieldsLayerAreaStyle.SelectedFeatures = currentLayer.InternalFeatures;
            //        }

            //        if (czitem != null) {
            //            selectedid = czitem.CropZoneView.FieldId.Id.ToString();
            //            //string selectedczparent = czitem.CropZoneView.Id.Id.ToString();
            //            //string selectedfieldparent = czitem.CropZoneView.FieldId.Id.ToString();
            //            feature.ColumnValues.Add(@"LdbLabel", czitem.CropZoneView.Name);
            //            currentLayer.InternalFeatures.Add(selectedid, feature);
            //            fieldsLayerAreaStyle.SelectedFeatures = currentLayer.InternalFeatures;
            //            //cropzonesLayerAreaStyle.SelectedFeatures = currentLayer.InternalFeatures;
            //        }
            //        if (Map.Overlays.Contains("fieldOverlay")) {
            //            try {
            //                if (currentLayer.InternalFeatures.Count > 0) {
            //                    Map.CurrentExtent = currentLayer.GetBoundingBox();
            //                    PointShape center1 = currentLayer.GetBoundingBox().GetCenterPoint();
            //                    Map.CenterAt(center1);
            //                }
            //                Map.Overlays["fieldOverlay"].Refresh();
            //            }
            //            catch (Exception exception) {
            //                log.ErrorException("Couldn't load the current item's map.", exception);
            //            }

            //        }

            //    }
            //}

        }


        private void OpenAndLoadaShapefile() {
            try {
                fieldsLayer.InternalFeatures.Clear();
                CreateInMemoryLayerAndOverlay();
                LoadShapeFileOverlayOntoMap();
                if (Map.Overlays.Contains("fieldsOverlay")) {
                    if (((LayerOverlay)Map.Overlays["fieldsOverlay"]).Layers.Contains("FieldsLayer")) {
                        Map.Refresh();
                    }
                }
            } catch { }
        }

        private void CreateInMemoryLayerAndOverlay() {
            layerOverlay = new LayerOverlay();
            layerOverlay.DrawingExceptionMode = DrawingExceptionMode.DrawException;
            fieldsLayer = new InMemoryFeatureLayer();

            //fieldsLayer
            fieldsLayer.FeatureSource.Open();
            FeatureSourceColumn column = new FeatureSourceColumn(@"LdbLabel", DbfColumnType.Character.ToString(), 64);
            fieldsLayer.Columns.Add(column);
            fieldsLayer.FeatureSource.Close();
            fieldsLayerAreaStyle = new ClientLayerAreaStyle();
            fieldsLayerAreaStyle.SelectedAreaStyle = new AreaStyle(new GeoPen(GeoColor.SimpleColors.Blue, 2), new GeoSolidBrush(new GeoColor(125, GeoColor.SimpleColors.Blue)));
            fieldsLayerAreaStyle.UnselectedAreaStyle = new AreaStyle(new GeoPen(GeoColor.StandardColors.Red, 2), new GeoSolidBrush(new GeoColor(0, GeoColor.StandardColors.Red)));
            fieldsLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(fieldsLayerAreaStyle);
            fieldsLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            fieldsLayer.FeatureSource.Projection = projection;
            fieldsLayerAreaStyle.SelectedFeatures = new List<Feature>();
            fieldsLayerAreaStyle.HiddenFeatures = new List<Feature>();
            layerOverlay.Layers.Add("FieldsLayer", fieldsLayer);

            selectedlayer = new InMemoryFeatureLayer();
            AreaStyle selectedLayerStyle = AreaStyles.CreateSimpleAreaStyle(new GeoColor(0, GeoColor.SimpleColors.LightRed), new GeoColor(255, GeoColor.SimpleColors.Black), 2);
            selectedlayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = selectedLayerStyle;
            selectedlayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            selectedlayer.FeatureSource.Projection = projection;
            layerOverlay.Layers.Add("SelectedLayer", selectedlayer);

            fieldsLayer2 = new InMemoryFeatureLayer();
            fieldsLayer2.Open();
            projection100 = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetGoogleMapParametersString());
            projection100.Open();
            fieldsLayer2.FeatureSource.Projection = projection100;
        }

        private void LoadShapeFileOverlayOntoMap() {
            if (Map.Overlays.Contains("fieldsOverlay")) {
                Map.Overlays.Remove("fieldsOverlay");
            }
            if (layerOverlay.Layers.Contains("FieldsLayer")) {
                Map.Overlays.Add("fieldsOverlay", layerOverlay);
                Map.Overlays.MoveToTop("fieldsOverlay");
            }

            EnsureFieldsLayerIsOpen();
            var extent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
            Map.CurrentExtent = extent;
        }

        private List<string> GatherSelectedShapes(AbstractTreeItemViewModel selectedtreeitem) {
            List<string> shapelist = new List<string>();
            if (SelectedTreeItem.Id is FarmId) {
                FarmId farmid = (FarmId)SelectedTreeItem.Id;
                selectedid = farmid.Id.ToString(); ;
                selectedname = $"{((FarmTreeItemViewModel)SelectedTreeItem).Name}";
                var filteredMapData = from mi in clientLayerItems
                                      where mi.FarmId.Equals(((FarmId)SelectedTreeItem.Id).Id) && !string.IsNullOrWhiteSpace(mi.MapData)
                                      select mi.MapData;

                shapelist = filteredMapData.ToList();
            } else if (SelectedTreeItem.Id is FieldId) {
                FieldId fieldid = (FieldId)SelectedTreeItem.Id;
                selectedid = fieldid.Id.ToString(); ;
                selectedname = $"{((FarmTreeItemViewModel)SelectedTreeItem.Parent).Name}:{((FieldTreeItemViewModel)SelectedTreeItem).Name}";
                shapelist = new List<string>();
                var maps = clientEndpoint.GetView<ItemMap>(fieldid);
                List<string> filteredMapData = new List<string>();
                if (maps.HasValue) {
                    shapelist.Add(maps.Value.MostRecentMapItem.MapData);
                }
            } else if (SelectedTreeItem.Id is CropZoneId) {
                CropZoneId cropzoneid = (CropZoneId)SelectedTreeItem.Id;
                selectedid = cropzoneid.Id.ToString(); ;
                selectedname = $"{((FarmTreeItemViewModel)SelectedTreeItem.Parent.Parent).Name}:{((FieldTreeItemViewModel)SelectedTreeItem.Parent).Name}:{((CropZoneTreeItemViewModel)SelectedTreeItem).Name}";
                shapelist = new List<string>();
                var maps = clientEndpoint.GetView<ItemMap>(cropzoneid);
                List<string> filteredMapData = new List<string>();
                if (maps.HasValue) {
                    shapelist.Add(maps.Value.MostRecentMapItem.MapData);
                }
            } else if (SelectedTreeItem.Id is CropId) {
                shapelist = new List<string>();
            } else {
                var filteredMapData = from mi in clientLayerItems
                                      where !string.IsNullOrWhiteSpace(mi.MapData)
                                      select mi.MapData;

                shapelist = filteredMapData.ToList();
            }



            return shapelist;
        }

        private void LoadMapShapes(List<string> mapitems) {
            fieldsLayer.InternalFeatures.Clear();
            fieldsLayer2.InternalFeatures.Clear();
            var extent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
            //InMemoryFeatureLayer fieldsLayer2 = new InMemoryFeatureLayer();
            //fieldsLayer2.Open();
            //Proj4Projection projection100 = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetGoogleMapParametersString());
            //projection100.Open();
            //fieldsLayer2.FeatureSource.Projection = projection100;
            try {
                if (mapitems.Count > 0) {
                    foreach (var shapedata in mapitems) {
                        var f = new Feature(shapedata);
                        if (fieldsLayer.InternalFeatures.Contains(f)) { continue; }
                        fieldsLayer.InternalFeatures.Add(f);
                    }
                    foreach (var shapedata in mapitems) {
                        var f = new Feature(shapedata);
                        if (fieldsLayer2.InternalFeatures.Contains(f)) { continue; }
                        fieldsLayer2.InternalFeatures.Add(f);
                    }
                }
                var shapelayerfeatures = fieldsLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);
                if (shapelayerfeatures.Count > 0) {
                    extent = ExtentHelper.GetBoundingBoxOfItems(shapelayerfeatures);
                    extent.ScaleUp(20);
                    Map.CurrentExtent = extent;

                    RectangleShape rs333 = ExtentHelper.GetBoundingBoxOfItems(shapelayerfeatures);
                    RectangleShape rs2333 = projection.ConvertToInternalProjection(rs333) as RectangleShape;
                    PolygonShape ps333 = rs2333.ToPolygon();
                    PolygonShape wktshape333 = new PolygonShape(ps333.GetWellKnownText()); ;
                    //savedextentforimages = wktshape333.GetBoundingBox();

                }
                var shapelayerfeatures2 = fieldsLayer2.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);
                if (shapelayerfeatures2.Count > 0) {
                    RectangleShape extent100 = ExtentHelper.GetBoundingBoxOfItems(shapelayerfeatures2);
                    RectangleShape rs2333 = projection100.ConvertToInternalProjection(extent100) as RectangleShape;
                    PolygonShape ps333 = rs2333.ToPolygon();
                    PolygonShape wktshape333 = new PolygonShape(ps333.GetWellKnownText()); ;
                    savedextentforimages = wktshape333.GetBoundingBox();
                }
            } catch {

            }
            if (Map.Overlays.Contains("fieldsOverlay")) {
                if (((LayerOverlay)Map.Overlays["fieldsOverlay"]).Layers.Contains("FieldsLayer")) {
                    Map.Refresh();
                }
            }

        }


        private async Task<int> UploadToAzureStorage() {
            try {
                string storage_account = "agcbatchtest1";
                string storage_container = "planetlabs-test";
                string sas_token = "SmhGD0DDUgMNfxHxfmw7rMDBEBxe6b7QDIXVsSXE1bj2VvQDhXpKLJgrUCLs1zKqI+PGp877ZPRUciZT7umVUw==";

                var useHttps = true;
                var connValid = true;

                StorageCredentials storageCredentials = new StorageCredentials(storage_account, sas_token);
                CloudStorageAccount storageAccount = new CloudStorageAccount(storageCredentials, useHttps);
                string conString1 = storageAccount.ToString(connValid);

                //  create Azure Storage
                CloudStorageAccount sa = CloudStorageAccount.Parse(conString1);
                //  create a blob client.
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                //  create a container 
                CloudBlobContainer container = blobClient.GetContainerReference(storage_container);

                string prefix = Infrastructure.ApplicationEnvironment.CurrentDataSourceId.ToString();

                FileInfo infofile = new FileInfo(filetoupload);
                var maininfofile = Path.Combine(prefix, infofile.Name);
                CloudBlockBlob infoblob = container.GetBlockBlobReference(maininfofile);
                string infouri = infoblob.Uri.ToString();
                if (!string.IsNullOrEmpty(infouri)) {
                    using (FileStream fs = infofile.OpenRead()) {
                        infoblob.UploadFromStream(fs);
                    }
                }
                string infouri2 = infoblob.Uri.ToString();
                if (!string.IsNullOrEmpty(infouri2)) {
                    MessageBox.Show(Strings.JsonFileDidNotUpload_Text.ToLower());
                }

                FileInfo aoifile = new FileInfo(filetoupload2);
                var mainaoifile = Path.Combine(prefix, aoifile.Name);
                CloudBlockBlob aoiblob = container.GetBlockBlobReference(mainaoifile);
                string aoiuri = aoiblob.Uri.ToString();
                if (!string.IsNullOrEmpty(aoiuri)) {
                    using (FileStream fs = infofile.OpenRead()) {
                        aoiblob.UploadFromStream(fs);
                    }
                }
                string aoiuri2 = aoiblob.Uri.ToString();
                if (!string.IsNullOrEmpty(aoiuri2)) {
                    MessageBox.Show(Strings.AoiFileDidNotUpload_Text.ToLower());
                }



                //container.SetPermissions(
                //new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });

                //CloudBlockBlob blockBlob = container.GetBlockBlobReference("myBlob");

                //// Create or overwrite the "myblob" blob with contents from a local file.
                //using (var fileStream = file.InputStream)
                //{
                //    blockBlob.UploadFromStream(fileStream);
                //    blockBlob.Uri.ToString();
                //}






                //activates the storage account on local  
                //CloudBlobContainer blobContainer = blobClient.GetContainerReference("imagesupload");
                //if (blobContainer.CreateIfNotExists())
                //{
                //    blobContainer.SetPermissions(new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });//Allowing public Access  

                //}


                //  create a local file
                //StorageFile file = await ApplicationData.Current.LocalFolder.CreateFileAsync("filename", CreationCollisionOption.ReplaceExisting);

                //  copy some txt to local file
                //MemoryStream ms = new MemoryStream();
                //DataContractSerializer serializer = new DataContractSerializer(typeof(string));
                //serializer.WriteObject(ms, "Hello Azure World!!");

                //using (Stream fileStream = await file.OpenStreamForWriteAsync())
                //{
                //    ms.Seek(0, SeekOrigin.Begin);
                //    await ms.CopyToAsync(fileStream);
                //    await fileStream.FlushAsync();
                //}

                ////  upload to Azure Storage 
                //await blockBlob.UploadFromFileAsync(file);

                return 1;
            } catch (Exception ex) {
                return 0;
            }
        }

        //private static void UploadToBlobStorage(FileInfo zipFile, string storageConnectionString, string blobContainerName)
        //{

        //    FileInfo fi1 = new FileInfo(path);

        //    // Connect to the storage account’s blob endpoint
        //    CloudStorageAccount account = CloudStorageAccount.Parse(storageConnectionString);
        //    CloudBlobClient client = account.CreateCloudBlobClient();

        //    // Create the blob storage container
        //    CloudBlobContainer container = client.GetContainerReference(blobContainerName);
        //    container.CreateIfNotExists();

        //    // Create the blob in the container
        //    CloudBlockBlob blob = container.GetBlockBlobReference(zipFile.Name);

        //    // Upload the zip and store it in the blob
        //    using (FileStream fs = zipFile.OpenRead())
        //        blob.UploadFromStream(fs);
        //}

        //public ActionResult Upload(HttpPostedFileBase image) //Accept the posted file  
        //{
        //    try
        //    {
        //        if (image.ContentLength > 0)
        //        {
        //            CloudBlobContainer blobContainer = blobStorageServices.GetCloudBlobContainer();
        //            CloudBlockBlob blob = blobContainer.GetBlockBlobReference(image.FileName);

        //            // Upload content to the blob, which will create the blob if it does not already exist.  
        //            blob.UploadFromStream(image.InputStream);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        CloudBlobContainer blobContainer = blobStorageServices.GetCloudBlobContainer();
        //        CloudBlockBlob blob2 = blobContainer.GetBlockBlobReference("myfile.txt");
        //        blob2.UploadText(ex.ToString());
        //    }
        //    return RedirectToAction("Upload");
        //}



        private async Task<int> DownloadFromAzureStorage() {
            try {
                string storage_account = "agcbatchtest1";
                string storage_container = "planetlabs-test";
                string sas_token = "SmhGD0DDUgMNfxHxfmw7rMDBEBxe6b7QDIXVsSXE1bj2VvQDhXpKLJgrUCLs1zKqI+PGp877ZPRUciZT7umVUw==";

                var useHttps = true;
                var connValid = true;

                StorageCredentials storageCredentials = new StorageCredentials(storage_account, sas_token);
                CloudStorageAccount storageAccount = new CloudStorageAccount(storageCredentials, useHttps);
                string conString1 = storageAccount.ToString(connValid);

                //  create Azure Storage
                CloudStorageAccount sa = CloudStorageAccount.Parse(conString1);
                //  create a blob client.
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                //  create a container 
                CloudBlobContainer container = blobClient.GetContainerReference(storage_container);

                string prefix = Infrastructure.ApplicationEnvironment.CurrentDataSourceId.ToString();
                IEnumerable<IListBlobItem> bloblist = container.ListBlobs(prefix, true, BlobListingDetails.None, null, null);
                foreach (IListBlobItem blobitem in bloblist) {
                    string absolutepath = blobitem.Uri.AbsolutePath;
                    var blogsegments = blobitem.Uri.Segments;

                    string acc = "";
                    string con = "";
                    string ds = "";
                    string tree = "";
                    string fn = "";
                    foreach (string seg1 in blogsegments) {
                        string seg = seg1.Replace("/", "");
                        if (seg == "") { continue; }
                        if (seg.EndsWith("ndvi.tif")) { continue; }
                        if (seg.EndsWith("AOI.geojson")) { continue; }
                        if (seg.EndsWith("info.bak")) { continue; }
                        if (seg.EndsWith("clip.tif")) {
                            fn = seg;
                        } else if (seg.EndsWith("plot.png")) {
                            fn = seg;
                        } else if (seg.EndsWith("info.json")) {
                            fn = seg;
                        } else if (seg == Infrastructure.ApplicationEnvironment.CurrentDataSourceId.ToString()) {
                            ds = seg;
                        } else if (seg == storage_account) {
                            acc = seg;
                        } else if (seg == storage_container) {
                            con = seg;
                        } else {
                            tree = seg;
                        }
                    }
                    if (fn == "") { continue; }
                    string mainfield = string.Empty;
                    string maindirectory = string.Empty;
                    var maindatasource = Path.Combine(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory, Infrastructure.ApplicationEnvironment.CurrentDataSourceId.ToString());
                    if (!Directory.Exists(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory)) {
                        Directory.CreateDirectory(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory);
                    }
                    if (!Directory.Exists(maindatasource)) {
                        Directory.CreateDirectory(maindatasource);
                    }
                    if (!string.IsNullOrEmpty(tree)) {
                        var subdirectoryname = Path.Combine(maindatasource, tree);
                        maindirectory = subdirectoryname;
                        mainfield = Path.Combine(subdirectoryname, fn);
                        if (!Directory.Exists(subdirectoryname)) {
                            Directory.CreateDirectory(subdirectoryname);
                        }
                    } else {
                        mainfield = Path.Combine(maindatasource, fn);
                        maindirectory = maindatasource;
                    }


                    if (!string.IsNullOrEmpty(mainfield)) {
                        if (blobitem is CloudBlockBlob) {
                            CloudBlockBlob blob = container.GetBlockBlobReference(((CloudBlockBlob)blobitem).Name);
                            if (blob.Name.EndsWith("info.json")) {
                                if (File.Exists(mainfield)) {
                                    string newName = mainfield.Replace("json", "bak");
                                    File.Copy(mainfield, newName, true);
                                    File.Delete(mainfield);
                                }
                                using (var fileStream = System.IO.File.OpenWrite(mainfield)) {
                                    blob.DownloadToStream(fileStream);
                                }

                                ProcessImageryMetadata(fn);
                            } else if (blob.Name.EndsWith("clip.tif") || blob.Name.EndsWith("plot.png")) {
                                if (!File.Exists(mainfield)) {
                                    using (var fileStream = System.IO.File.OpenWrite(mainfield)) {
                                        blob.DownloadToStream(fileStream);
                                    }
                                }
                            }
                        }
                    }
                }

                return 1;
            } catch (Exception ex) {
                return 0;
            }

        }
        private void ProcessSubscriptionFiles() {
            List<PlanetImageViewModel> imagelist1 = new List<PlanetImageViewModel>();
            var maindatasource = Path.Combine(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory, Infrastructure.ApplicationEnvironment.CurrentDataSourceId.ToString());
            System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(maindatasource);
            if (!System.IO.Directory.Exists(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory)) {
                System.IO.Directory.CreateDirectory(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory);
            }
            if (!System.IO.Directory.Exists(maindatasource)) {
                System.IO.Directory.CreateDirectory(maindatasource);
            }
            if (System.IO.Directory.Exists(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory)) {
                System.IO.FileInfo[] files = dir.GetFiles(@"*.json");
                foreach (System.IO.FileInfo file in files) {
                    try {
                        if (file.FullName.EndsWith(@"info.json")) {
                            ProcessImageryMetadata(file.Name);

                        }
                    } catch (Exception ex) {
                        MessageBox.Show(Strings.ProblemProcessingImageryMetadata_Text);
                    }
                }
            }
        }

        private void ProcessImageryMetadata(string subfilename) {
            var maindatasource = Path.Combine(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory, Infrastructure.ApplicationEnvironment.CurrentDataSourceId.ToString());
            var mainfield = Path.Combine(maindatasource, subfilename);
            if (File.Exists(mainfield)) {
                JsonClass.AreaSubscription imfeat = JsonConvert.DeserializeObject<JsonClass.AreaSubscription>(File.ReadAllText(mainfield));
                foreach (KeyValuePair<string, string> feattif in imfeat.FeaturePNGs) {
                    try {
                        ImageFeature imagfeat = new ImageFeature();
                        imagfeat.id = feattif.Key;
                        ImageProperties imagprop = new ImageProperties();
                        string acquireddate = imfeat.FeatureProperties[feattif.Key]["acquired"];
                        imagprop.acquired = System.Convert.ToDateTime(acquireddate);
                        string publisheddate = imfeat.FeatureProperties[feattif.Key]["published"];
                        imagprop.published = System.Convert.ToDateTime(publisheddate);
                        string cloud = imfeat.FeatureProperties[feattif.Key]["cloud_cover"];
                        if (cloud == "") {
                            imagprop.cloud_cover = 0;
                        } else {
                            imagprop.cloud_cover = System.Convert.ToSingle(cloud);
                        }
                        string usable = imfeat.FeatureProperties[feattif.Key]["usable_data"];
                        if (usable == "") {
                            imagprop.usable_data = 0;
                        } else {
                            imagprop.usable_data = System.Convert.ToSingle(usable);
                        }
                        string pixel = imfeat.FeatureProperties[feattif.Key]["pixel_resolution"];
                        if (pixel == "") {
                            imagprop.pixel_resolution = 0;
                        } else {
                            imagprop.pixel_resolution = System.Convert.ToSingle(pixel);
                        }
                        string epsg = imfeat.FeatureProperties[feattif.Key]["epsg_code"];
                        if (epsg == "") {
                            imagprop.epsg_code = 0;
                        } else {
                            imagprop.epsg_code = System.Convert.ToInt32(epsg);
                        }
                        imagprop.item_type = imfeat.FeatureProperties[feattif.Key]["item_type"];
                        imagprop.provider = imfeat.FeatureProperties[feattif.Key]["provider"];

                        imagfeat.properties = imagprop;

                        string jsonfilename = $"{feattif.Key}.json";
                        var subdirectoryname = Path.Combine(maindatasource, imfeat.TreeItem_Id);
                        var fulldirectoryname = Path.Combine(subdirectoryname, jsonfilename);
                        if (!File.Exists(fulldirectoryname)) {
                            if (!Directory.Exists(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory)) {
                                Directory.CreateDirectory(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory);
                            }
                            if (!Directory.Exists(maindatasource)) {
                                Directory.CreateDirectory(maindatasource);
                            }
                            if (!Directory.Exists(subdirectoryname)) {
                                Directory.CreateDirectory(subdirectoryname);
                            }
                            File.WriteAllText(fulldirectoryname, JsonConvert.SerializeObject(imagfeat));
                        }
                    } catch (Exception ex) {
                        int dddd = 0;
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void CreateSubScription() {
            string pilotmessage = @Strings.BeAdvisedThatYouAsSyngentaGrower_Text;
            MessageBox.Show(pilotmessage, Strings.Attention_Text.ToUpper(), MessageBoxButton.OK);
            //TO DO :: CALL API TO GET PAGE
            Task<string> getMLZone = Task.Factory.StartNew<string>(() => {

                currentimage = null;
                currentassetstatusresults = null;
                if (NetworkStatus.IsInternetAvailable()) {
                    var holder = PostSubscription<string>();
                    holder.Wait();

                    return holder.Result;
                } else {
                    return string.Empty;
                }
            });
        }

        private async Task<T> PostSubscription<T>() {
            //fake result
            var fake = string.Empty;
            var fakeresult = JsonConvert.DeserializeObject<T>(fake);
            try {
                // url and api key
                string api_key = "76415afbc2dd4decba68c160e6123e20";
                string planet_labs_url = "https://api.planet.com/data/v1/quick-search";


                // bounding box
                RectangleShape rs = fieldsLayer2.GetBoundingBox();
                //rs.ScaleTo(100);
                RectangleShape rs2 = projection100.ConvertToInternalProjection(rs) as RectangleShape;
                string geojsonstring = $"[[[{rs2.UpperLeftPoint.X},{rs2.UpperLeftPoint.Y}],[{rs2.UpperRightPoint.X},{rs2.UpperRightPoint.Y}],[{rs2.LowerRightPoint.X},{rs2.LowerRightPoint.Y}],[{rs2.LowerLeftPoint.X},{rs2.LowerLeftPoint.Y}],[{rs2.UpperLeftPoint.X},{rs2.UpperLeftPoint.Y}]]]";
                PolygonShape ps = rs2.ToPolygon();
                string psstring = ps.GetGeoJson();
                string pswkt = ps.GetWellKnownText();

                JsonClass.AreaSubscription subscription = new JsonClass.AreaSubscription();
                subscription.Name = $"{Infrastructure.ApplicationEnvironment.CurrentDataSourceName}:{selectedname}";
                subscription.DataSourceId = Infrastructure.ApplicationEnvironment.CurrentDataSourceId.ToString();
                subscription.DataSource_Name = Infrastructure.ApplicationEnvironment.CurrentDataSourceName;
                subscription.TreeItem_Id = selectedid;
                subscription.TreeItem_Name = selectedname;
                subscription.AreaofInterest = MakeAreaOfIntereest(geojsonstring);
                subscription.AreaInWKT = pswkt;
                subscription.Status = "active";
                subscription.BeginDate = DateTime.Now;
                subscription.List_of_Satelites = list_of_Satellites.ToArray();
                subscription.Filter_Days = Filter_Days;
                subscription.Filter_Usable_Data = Filter_Usable_Data;
                subscription.Filter_Cloud_Cover = Filter_Cloud_Cover;
                subscription.FeatureStatus = new Dictionary<string, string>();
                //subscription.FeatureStatus.Add("Color", "nipy_spectral");
                //subscription.FeatureStatus.Add("Color", "greens");
                //subscription.FeatureStatus.Add("Color", "RdYlGn");
                //subscription.FeatureStatus.Add("Color", "YlGn");
                //subscription.FeatureStatus.Add("Color", "PiYG");
                //subscription.FeatureStatus.Add("Color", "PRGn");
                //subscription.FeatureStatus.Add("Color", "BrGN");
                //subscription.FeatureStatus.Add("Color", "summer");
                //subscription.FeatureStatus.Add("Color", "winter");
                //subscription.FeatureProperties = new Dictionary<string, Dictionary<string, string>>();

                string subfilename = $"{selectedid}info.json";
                var maindatasource = Path.Combine(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory, subscription.DataSourceId);
                var mainfield = Path.Combine(maindatasource, subfilename);
                var subdirectoryname = Path.Combine(maindatasource, selectedid);
                filetoupload = mainfield;
                if (!Directory.Exists(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory)) {
                    Directory.CreateDirectory(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory);
                }
                if (!Directory.Exists(maindatasource)) {
                    Directory.CreateDirectory(maindatasource);
                }
                if (!Directory.Exists(subdirectoryname)) {
                    Directory.CreateDirectory(subdirectoryname);
                }
                File.WriteAllText(mainfield, JsonConvert.SerializeObject(subscription));
                string shapejsonfilename = $"{selectedid}AOI.geojson";
                var shapefulldirectoryname = Path.Combine(maindatasource, shapejsonfilename);
                filetoupload2 = shapefulldirectoryname;
                File.WriteAllText(shapefulldirectoryname, JsonConvert.SerializeObject(MakeAreaOfIntereest(geojsonstring)));

                log.Info($"Http string content:  {JsonConvert.SerializeObject(MakeAreaOfIntereest(geojsonstring))}");

                string infojsonstring = JsonConvert.SerializeObject(subscription);
                string aoigeojsonstring = JsonConvert.SerializeObject(MakeAreaOfIntereest(geojsonstring));

                //Submit to microservices
                if (NetworkStatus.IsInternetAvailable()) {
                    ////http://localhost:9016/planet-labs/api/zones/b66d49d8-8ae8-4d3b-88f6-a7e067b8e6ad/abeeb6a1-478a-44cf-a5c2-6db147b53cf2
                    //http://agc-jd-connect-t0117.eastus2.cloudapp.azure.com/planet-labs/api/swagger/
                    var planetLabBaseUri = string.Format("{0}/zones", RemoteConfigurationSettings.PlanetLabsRemoteUri);
                    var actionids = string.Format("{0}/{1}", subscription.DataSourceId, subscription.TreeItem_Id);
                    var planetLabUri = string.Format("{0}/{1}", planetLabBaseUri, actionids);

                    var cloudClientFactory = new Landdb.Infrastructure.CloudClientFactory();
                    var token = cloudClientFactory.ResolveStorageCredentials();

                    var client = new HttpClient {
                        BaseAddress = new Uri(planetLabUri)
                    };
                    client.DefaultRequestHeaders.Add("Accept", "application/json");
                    await token.SetBearerTokenAsync(client);

                    HttpResponseMessage httpResponseMessage1 = await client.PostAsJsonAsync(planetLabUri, subscription);
                    // response
                    if (httpResponseMessage1.IsSuccessStatusCode) {
                        log.Info($"Result Response:  {httpResponseMessage1.IsSuccessStatusCode.ToString()}");
                        var jsonresponse = await httpResponseMessage1.Content.ReadAsStringAsync();
                        CanCreateSubscription = false;
                        string pilotmessage = $"{subscription.Name} has been successfully registered.";
                        MessageBox.Show(pilotmessage);

                        return fakeresult;
                    } else {
                        string failureMsg = "HTTP Status: " + httpResponseMessage1.StatusCode.ToString() + " - Reason: " + httpResponseMessage1.ReasonPhrase;
                        log.Info($"Result Response:  {failureMsg}");
                        CanCreateSubscription = false;
                        return fakeresult;
                    }

                }
            } catch (Exception ex) {
                throw new ApplicationException("Share Failed.");
            }
            return fakeresult;
        }


        public void DownloadFilesSyncCommand() {
            //TO DO :: CALL API TO GET PAGE
            Task<string> getMLZone = Task.Factory.StartNew<string>(() => {

                currentimage = null;
                currentassetstatusresults = null;
                if (NetworkStatus.IsInternetAvailable()) {
                    if (httpClient1 == null) {
                        httpClient1 = new HttpClient();
                    }


                    var holder = PostSync<string>(httpClient1);
                    holder.Wait();

                    return holder.Result;
                } else {
                    return string.Empty;
                }
            });
        }

        private async Task<T> PostSync<T>(System.Net.Http.HttpClient httpClient1) {
            try {



                //// Retrieve storage account from connection string.
                //CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
                //    CloudConfigurationManager.GetSetting("StorageConnectionString"));

                //// Create the blob client.
                //CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                //// Retrieve reference to a previously created container.
                //CloudBlobContainer container = blobClient.GetContainerReference("mycontainer");

                //// Retrieve reference to a blob named "photo1.jpg".
                //CloudBlockBlob blockBlob = container.GetBlockBlobReference("photo1.jpg");

                //// Save blob contents to a file.
                //using (var fileStream = System.IO.File.OpenWrite(@"path\myfile"))
                //{
                //    blockBlob.DownloadToStream(fileStream);
                //}


                //int r = await UploadToAzureStorage();
                //if (r == 1)
                //{
                //await DownloadFromAzureStorage();
                //}



                var fake = string.Empty;
                var result = JsonConvert.DeserializeObject<T>(fake);
                return result;
            } catch (Exception ex) {
                throw new ApplicationException("Share Failed.");
            }
        }


        public void ShowIntegration() {
            //TO DO :: CALL API TO GET PAGE
            Task<string> getMLZone = Task.Factory.StartNew<string>(() => {

                currentimage = null;
                currentassetstatusresults = null;
                if (NetworkStatus.IsInternetAvailable()) {
                    if (httpClient1 == null) {
                        httpClient1 = new HttpClient();
                    }
                    var holder = PostData<string>(httpClient1);
                    holder.Wait();

                    return holder.Result;
                } else {
                    return string.Empty;
                }
            });
        }

        private static string AddDoubleQuotes(string value) {
            return "\"" + value + "\"";
        }

        private async Task<T> PostData<T>(System.Net.Http.HttpClient httpClient1) {
            try {
                // url and api key
                string api_key = "76415afbc2dd4decba68c160e6123e20";
                string planet_labs_url = "https://api.planet.com/data/v1/quick-search";

                // bounding box
                RectangleShape rs = fieldsLayer.GetBoundingBox();
                //rs.ScaleTo(100);
                RectangleShape rs2 = projection.ConvertToInternalProjection(rs) as RectangleShape;
                string geojsonstring = $"[[[{rs2.UpperLeftPoint.X},{rs2.UpperLeftPoint.Y}],[{rs2.UpperRightPoint.X},{rs2.UpperRightPoint.Y}],[{rs2.LowerRightPoint.X},{rs2.LowerRightPoint.Y}],[{rs2.LowerLeftPoint.X},{rs2.LowerLeftPoint.Y}],[{rs2.UpperLeftPoint.X},{rs2.UpperLeftPoint.Y}]]]";
                PolygonShape ps = rs2.ToPolygon();
                string psstring = ps.GetGeoJson();
                string pswkt = ps.GetWellKnownText();


                // today's date minus 90 days
                DateTime datenow = DateTime.Now;
                DateTime datepast = datenow - new TimeSpan(60, 0, 0, 0);
                string datenowstring = $"{datenow.ToString("yyyy")}-{datenow.ToString("MM")}-{datenow.ToString("dd")}T00:00:00Z";
                string datepaststring = $"{datepast.ToString("yyyy")}-{datepast.ToString("MM")}-{datepast.ToString("dd")}T00:00:00Z";

                // array of satellite assets
                //String[] data = new String[] { "PSScene4Band", "PSScene3Band", "PSOrthoTile", "REOrthoTile", "Sentinel2L1C", "Landsat8L1G" };
                //String[] data = new String[] { "PSScene4Band", "PSOrthoTile", "REOrthoTile" };
                String[] data = new String[] { "REOrthoTile" };
                var satellites = string.Join(",", data.Select(o => string.Concat("\"", o, "\"")));

                string ob = "{";
                string cb = "}";

                // quick search content
                //string elevationrangefilter = $"{ob}\"type\":\"RangeFilter\",\"field_name\":\"sun_elevation\",\"config\":{ob}\"gte\":0,\"lte\":90{cb}{cb}";
                string rangefilter = $"{ob}\"type\":\"RangeFilter\",\"field_name\":\"usable_data\",\"config\":{ob}\"gte\":0.50,\"lte\":1.00{cb}{cb}";
                //string permissionfilter = $"{ob}\"type\":\"PermissionFilter\",\"config\":[\"assets: download\", \"assets.visual:download\"]{cb}"; b}";
                string permissionfilter = $"{ob}\"type\":\"PermissionFilter\",\"config\":[\"assets.visual:download\"]{cb}";
                string geometryfilter = $"{ob}\"type\":\"GeometryFilter\",\"field_name\":\"geometry\",\"config\":{psstring}{cb}";
                string datefilter = $"{ob}\"type\":\"DateRangeFilter\",\"field_name\":\"acquired\",\"config\":{ob}\"gte\":\"{datepaststring}\",\"lte\":\"{datenowstring}\"{cb}{cb}";
                string filterarray = $"{ob}\"type\":\"AndFilter\",\"config\":[{geometryfilter},{datefilter},{rangefilter},{permissionfilter}]{cb}";
                string searchcontent = $"{ob}\"item_types\":[{satellites}], \"filter\":{filterarray}{cb}";

                log.Info($"Bounding Box:  {psstring}");
                log.Info($"Http string content:  {searchcontent}");

                // Http Client
                //var httpClient1 = new HttpClient();
                var credentials1 = Encoding.ASCII.GetBytes($"{api_key}:");
                // headers
                httpClient1.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(credentials1));
                httpClient1.DefaultRequestHeaders.Accept.Clear();
                httpClient1.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // content
                StringContent stringContent1 = new StringContent(searchcontent);
                stringContent1.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                // post
                HttpResponseMessage httpResponseMessage1 = await httpClient1.PostAsync($"{planet_labs_url}", stringContent1);
                // response
                if (httpResponseMessage1.IsSuccessStatusCode) {
                    log.Info($"Result Response:  {httpResponseMessage1.IsSuccessStatusCode.ToString()}");
                    var jsonresponse = await httpResponseMessage1.Content.ReadAsStringAsync();
                    QuickSearchResults quicksearchresults = JsonConvert.DeserializeObject<QuickSearchResults>(jsonresponse);
                    List<PlanetImageViewModel> imagelist1 = new List<PlanetImageViewModel>();
                    foreach (ImageFeature imfeat in quicksearchresults.features) {
                        DateTime convertedDate = Convert.ToDateTime(imfeat.properties.acquired);
                        JsonClass.AreaOfInterest newaoi = MakeAreaOfIntereest(geojsonstring);
                        PlanetImageViewModel pivm = new PlanetImageViewModel(this, imfeat.id, convertedDate, imfeat.properties.usable_data, imfeat.properties.cloud_cover, imfeat.properties.pixel_resolution, imfeat.properties.item_type, imfeat, "Show", newaoi, pswkt);
                        //PlanetImageViewModel pivm = new PlanetImageViewModel();
                        //pivm.FeatureId = imfeat.id;
                        //DateTime convertedDate = Convert.ToDateTime(imfeat.properties.acquired);
                        //pivm.DateAcquired = convertedDate;
                        //pivm.UsableData = imfeat.properties.usable_data;
                        //pivm.CloudCover = imfeat.properties.cloud_cover;
                        //pivm.Pixel_Resolution = imfeat.properties.pixel_resolution;
                        //pivm.ItemType = imfeat.properties.item_type;
                        //pivm.PlanetFeature = imfeat;
                        //JsonClass.AreaOfInterest newaoi = MakeAreaOfIntereest(geojsonstring);
                        //pivm.AreaOfInterest = newaoi;
                        //pivm.AreaInWKT = pswkt;
                        imagelist1.Add(pivm);
                    }
                    ImageList = imagelist1;

                    //JsonClass.AreaSubscription subscription = new JsonClass.AreaSubscription();
                    //subscription.DataSourceId = Infrastructure.ApplicationEnvironment.CurrentDataSourceId.ToString();
                    //subscription.DataSource_Name = Infrastructure.ApplicationEnvironment.CurrentDataSourceName;
                    //subscription.TreeItem_Id = selectedid;
                    //subscription.TreeItem_Name = selectedname;
                    //subscription.AreaofInterest = MakeAreaOfIntereest(geojsonstring);
                    //subscription.AreaInWKT = pswkt;
                    //subscription.Status = "active";
                    //subscription.BeginDate = DateTime.Now - new TimeSpan(30, 0, 0, 0);
                    //subscription.List_of_Satelites = list_of_Satellites.ToArray();
                    //subscription.Filter_Days = Filter_Days;
                    //subscription.Filter_Usable_Data = Filter_Usable_Data;
                    //subscription.Filter_Cloud_Cover = Filter_Cloud_Cover;
                    //subscription.FeatureStatus = new Dictionary<string, string>();
                    //Dictionary<string, string> image_properties = new Dictionary<string, string>();
                    //subscription.FeatureProperties = new Dictionary<string, Dictionary<string, string>>();
                    //subscription.FeatureTIFs = new Dictionary<string, string>();
                    //subscription.FeatureNDVI = new Dictionary<string, string>();
                    //subscription.FeaturePNGs = new Dictionary<string, string>();

                    //string subfilename = $"{selectedid}info.json";
                    //var maindatasource = Path.Combine(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory, subscription.DataSourceId);
                    //var mainfield = Path.Combine(maindatasource, subfilename);
                    //var subdirectoryname = Path.Combine(maindatasource, selectedid);
                    //if (!Directory.Exists(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory))
                    //{
                    //    Directory.CreateDirectory(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory);
                    //}
                    //if (!Directory.Exists(maindatasource))
                    //{
                    //    Directory.CreateDirectory(maindatasource);
                    //}
                    //if (!Directory.Exists(subdirectoryname))
                    //{
                    //    Directory.CreateDirectory(subdirectoryname);
                    //}
                    //File.WriteAllText(mainfield, JsonConvert.SerializeObject(subscription));

                    //string shapejsonfilename = $"{selectedid}AOI.geojson";
                    //var shapefulldirectoryname = Path.Combine(maindatasource, shapejsonfilename);
                    //File.WriteAllText(shapefulldirectoryname, JsonConvert.SerializeObject(MakeAreaOfIntereest(geojsonstring)));



                    var fake = string.Empty;
                    var result = JsonConvert.DeserializeObject<T>(fake);
                    return result;
                } else {
                    string failureMsg = "HTTP Status: " + httpResponseMessage1.StatusCode.ToString() + " - Reason: " + httpResponseMessage1.ReasonPhrase;
                    log.Info($"Result Response:  {failureMsg}");
                    var fake = string.Empty;
                    var result = JsonConvert.DeserializeObject<T>(fake);
                    return result;
                }
            } catch (Exception ex) {
                throw new ApplicationException("Share Failed.");
            }
        }

        private static JsonClass.AreaOfInterest MakeAreaOfIntereest(string psstring) {
            JsonClass.AreaOfInterest aoi = new JsonClass.AreaOfInterest();

            JsonClass.Feature feat = new JsonClass.Feature();
            feat.type = "Feature";
            feat.properties = new JsonClass.Properties();
            JsonClass.Geometry geomtry = new JsonClass.Geometry();
            geomtry.type = "Polygon";
            geomtry.coordinates = JsonConvert.DeserializeObject<float[][][]>(psstring);
            feat.geometry = geomtry;

            List<JsonClass.Feature> featlist = new List<JsonClass.Feature>();
            featlist.Add(feat);

            aoi.type = "FeatureCollection";
            aoi.features = featlist.ToArray();
            return aoi;
        }

        public void ActivateFiles() {
            Task<string> getMLZone = Task.Factory.StartNew<string>(() => {

                currentimage = null;
                currentassetstatusresults = null;

                if (NetworkStatus.IsInternetAvailable()) {
                    HttpClient httpClient2 = new HttpClient();
                    var holder = PostFileActivation<string>(httpClient2);
                    holder.Wait();

                    return holder.Result;
                } else {
                    return string.Empty;
                }
            });
        }

        private async Task<T> PostFileActivation<T>(System.Net.Http.HttpClient httpClient2) {
            try {
                // url and api key
                string api_key = "76415afbc2dd4decba68c160e6123e20";
                string planet_labs_url = "https://api.planet.com/data/v1/item-types/";
                // bounding box

                var fake = string.Empty;
                var result = JsonConvert.DeserializeObject<T>(fake);

                currentimagelist.Clear();
                currentassetlist.Clear();

                string newurl = string.Empty;
                foreach (var item in ImageList) {
                    if (item.Checked) {
                        newurl = $"{planet_labs_url}{item.ItemType}/items/{item.FeatureId}/assets/";
                        currentimage = item;
                        bool addimagetolist = true;
                        foreach (var image in currentimagelist) {
                            if (image.FeatureId == item.FeatureId) {
                                addimagetolist = false;
                            }
                        }
                        if (addimagetolist) {
                            currentimagelist.Add(item);
                        }
                        break;
                    }
                }
                if (string.IsNullOrEmpty(newurl)) {
                    return result;
                }

                // Http Client
                var credentials1 = Encoding.ASCII.GetBytes($"{api_key}:");
                // headers
                httpClient2.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(credentials1));
                httpClient2.DefaultRequestHeaders.Accept.Clear();
                httpClient2.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));


                // post
                HttpResponseMessage httpResponseMessage2 = await httpClient2.GetAsync($"{newurl}");
                // response
                if (httpResponseMessage2.IsSuccessStatusCode) {
                    log.Info($"Result Response:  {httpResponseMessage2.IsSuccessStatusCode.ToString()}");
                    var jsonresponse = await httpResponseMessage2.Content.ReadAsStringAsync();

                    AssetStatusResponse assetstatusresults = JsonConvert.DeserializeObject<AssetStatusResponse>(jsonresponse);
                    currentassetstatusresults = assetstatusresults;
                    currentassetlist.Add(assetstatusresults);


                    if (assetstatusresults.visual_xml.status == "inactive") {
                        MessageBox.Show(String.Format(Strings.ImageVisualXmlMustBeActivated_Format_Text,
                            currentimage.FeatureId));
                        HttpResponseMessage httpResponseMessage3 = await httpClient2.GetAsync($"{assetstatusresults.visual_xml._links.activate}");
                        return result;
                    } else if (assetstatusresults.visual_xml.status == "activating") {
                        MessageBox.Show(String.Format(Strings.ImageVisualXmlIsActivating_Format_Text, currentimage.FeatureId));
                        return result;
                    } else if (assetstatusresults.visual_xml.status == "active") {
                        //HttpResponseMessage httpResponseMessage3 = await httpClient2.GetAsync($"{assetstatusresults.visual_xml.location}");
                        //MessageBox.Show($"{currentimage.FeatureId} visual_xml has been activated.");
                    }


                    if (assetstatusresults.analytic_xml.status == "inactive") {
                        MessageBox.Show(String.Format(Strings.ImageAnalyticXmlMustBeActivated_Format_Text, currentimage.FeatureId));
                        HttpResponseMessage httpResponseMessage3 = await httpClient2.GetAsync($"{assetstatusresults.analytic_xml._links.activate}");
                        return result;
                    } else if (assetstatusresults.analytic_xml.status == "activating") {
                        MessageBox.Show(String.Format(Strings.ImageAnalyticXmlIsActivating_Format_Text, currentimage.FeatureId));
                        return result;
                    } else if (assetstatusresults.analytic_xml.status == "active") {
                        //HttpResponseMessage httpResponseMessage3 = await httpClient2.GetAsync($"{assetstatusresults.visual_xml.location}");
                        //MessageBox.Show($"{currentimage.FeatureId} analytic_xml has been activated.");
                    }


                    if (assetstatusresults.analytic.status == "inactive") {
                        MessageBox.Show(String.Format(Strings.ImageAnalyticMustBeActivated_Format_Text, currentimage.FeatureId));
                        HttpResponseMessage httpResponseMessage3 = await httpClient2.GetAsync($"{assetstatusresults.analytic._links.activate}");
                        return result;
                    } else if (assetstatusresults.analytic.status == "activating") {
                        MessageBox.Show(String.Format(Strings.ImageAnalyticIsActivating_Text, currentimage.FeatureId));
                        return result;
                    } else if (assetstatusresults.analytic.status == "active") {
                        //HttpResponseMessage httpResponseMessage3 = await httpClient2.GetAsync($"{assetstatusresults.visual_xml.location}");
                        //MessageBox.Show($"{currentimage.FeatureId} analytic has been activated.");
                    }


                    if (assetstatusresults.visual.status == "inactive") {
                        MessageBox.Show(String.Format(Strings.ImageVisualMustBeActivated_Text, currentimage.FeatureId));
                        HttpResponseMessage httpResponseMessage3 = await httpClient2.GetAsync($"{assetstatusresults.visual._links.activate}");
                        return result;
                    } else if (assetstatusresults.visual.status == "activating") {
                        MessageBox.Show(String.Format(Strings.ImageVisualIsActivating_Text, currentimage.FeatureId));
                        return result;
                    } else if (assetstatusresults.visual.status == "active") {
                        //HttpResponseMessage httpResponseMessage3 = await httpClient2.GetAsync($"{assetstatusresults.visual.location}");
                        MessageBox.Show(String.Format(Strings.ImageVisualHasBeenActivated_Text, currentimage.FeatureId));
                        string statusfilename = $"{currentimage.FeatureId}status.json";
                        var fullstatusname = Path.Combine(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory, statusfilename);
                        if (!Directory.Exists(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory)) {
                            Directory.CreateDirectory(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory);
                        }
                        try {
                            File.WriteAllText(fullstatusname, JsonConvert.SerializeObject(assetstatusresults));
                        } catch {
                            MessageBox.Show(Strings.UnableToWriteStatusFile_Text);
                        }
                    }

                    return result;
                } else {
                    string failureMsg = "HTTP Status: " + httpResponseMessage2.StatusCode.ToString() + " - Reason: " + httpResponseMessage2.ReasonPhrase;
                    log.Info($"Result Response:  {failureMsg}");
                    return result;
                }

            } catch (Exception ex) {
                throw new ApplicationException("Share Failed.");
            }
        }

        public void DownloadFiles() {
            Task<string> getMLZone = Task.Factory.StartNew<string>(() => {
                if (NetworkStatus.IsInternetAvailable()) {
                    try {
                        HttpGetForLargeFileInRightWay(currentimage, currentassetstatusresults).Wait();
                        HttpGetForLargeFileInRightWay5(currentimage, currentassetstatusresults).Wait();
                        HttpGetForLargeFileInRightWay4(currentimage, currentassetstatusresults).Wait();
                        HttpGetForLargeFileInRightWay3(currentimage, currentassetstatusresults).Wait();
                        MessageBox.Show(Strings.BeginDownloadingVisualTif_Text);
                        HttpGetForLargeFileInRightWay2(currentimage, currentassetstatusresults).Wait();




                        //LoadImageryFiles(selectedid);
                        //string jsonfilename = $"{currentimage.FeatureId}.json";
                        //var fulldirectoryname = Path.Combine(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory, jsonfilename);
                        //if (!File.Exists(fulldirectoryname))
                        //{
                        //    if (!Directory.Exists(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory))
                        //    {
                        //        Directory.CreateDirectory(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory);
                        //    }
                        //    File.WriteAllText(fulldirectoryname, JsonConvert.SerializeObject(currentimage.PlanetFeature));
                        //}
                        //string shapejsonfilename = $"{currentimage.FeatureId}AOI.geojson";
                        //var shapefulldirectoryname = Path.Combine(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory, shapejsonfilename);
                        //File.WriteAllText(shapefulldirectoryname, JsonConvert.SerializeObject(currentimage.AreaOfInterest));


                        //JsonClass.AreaSubscription subscription = new JsonClass.AreaSubscription();
                        //subscription.DataSourceId = Infrastructure.ApplicationEnvironment.CurrentDataSourceId.ToString();
                        //subscription.DataSource_Name = Infrastructure.ApplicationEnvironment.CurrentDataSourceName;
                        //subscription.TreeItem_Id = selectedid;
                        //subscription.TreeItem_Name = selectedname;
                        //subscription.AreaofInterest = currentimage.AreaOfInterest;
                        //subscription.AreaInWKT = currentimage.AreaInWKT;
                        //subscription.Status = "active";
                        //subscription.BeginDate = DateTime.Now - new TimeSpan(30, 0, 0, 0);
                        //subscription.Filter_Days = 2;
                        //subscription.Filter_Usable_Date = .7;
                        //subscription.Filter_Cloud_Cover = .1;

                        //string subfilename = $"{selectedid}info.json";
                        //var maindatasource = Path.Combine(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory, subscription.DataSourceId);
                        //var mainfield = Path.Combine(maindatasource, subfilename);
                        //var subdirectoryname = Path.Combine(maindatasource, selectedid);
                        //if (!Directory.Exists(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory))
                        //{
                        //    Directory.CreateDirectory(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory);
                        //}
                        //if (!Directory.Exists(maindatasource))
                        //{
                        //    Directory.CreateDirectory(maindatasource);
                        //}
                        //if (!Directory.Exists(subdirectoryname))
                        //{
                        //    Directory.CreateDirectory(subdirectoryname);
                        //}
                        //File.WriteAllText(mainfield, JsonConvert.SerializeObject(subscription));

                        MessageBox.Show(Strings.VisualImageryDownloadComplete_Text);
                        return string.Empty;
                    } catch (Exception ex) {
                        return string.Empty;
                    }
                } else {
                    return string.Empty;
                }
            });
        }

        public void DownloadAnalyticalFiles() {
            Task<string> getMLZone = Task.Factory.StartNew<string>(() => {
                if (NetworkStatus.IsInternetAvailable()) {
                    HttpGetForLargeFileInRightWay5(currentimage, currentassetstatusresults).Wait();
                    MessageBox.Show(Strings.BeginDownloadingAnalyticalTif_Text);
                    HttpGetForLargeFileInRightWay4(currentimage, currentassetstatusresults).Wait();
                    MessageBox.Show(Strings.AnalyticalImageryDownloadComplete_Text);

                    return string.Empty;
                } else {
                    return string.Empty;
                }
            });
        }

        static async Task HttpGetForLargeFileInRightWay(PlanetImageViewModel currentfeature, AssetStatusResponse assetstatusresults) {
            //currentassetstatusresults

            using (HttpClient client = new HttpClient()) {
                string jsonfilename = $"{currentfeature.FeatureId}.json";
                var fulldirectoryname = Path.Combine(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory, jsonfilename);
                if (!File.Exists(fulldirectoryname)) {
                    if (!Directory.Exists(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory)) {
                        Directory.CreateDirectory(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory);
                    }
                    File.WriteAllText(fulldirectoryname, JsonConvert.SerializeObject(currentfeature.PlanetFeature));
                }
                string shapejsonfilename = $"{currentfeature.FeatureId}AOI.geojson";
                var shapefulldirectoryname = Path.Combine(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory, shapejsonfilename);
                File.WriteAllText(shapefulldirectoryname, JsonConvert.SerializeObject(currentfeature.AreaOfInterest));

            }
        }

        static async Task HttpGetForLargeFileInRightWay2(PlanetImageViewModel currentfeature, AssetStatusResponse assetstatusresults) {
            using (HttpClient client = new HttpClient()) {
                string calculatedhashresults2 = string.Empty;
                string tiffilename = $"{currentfeature.FeatureId}.tif";
                var fulltiffilename = Path.Combine(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory, tiffilename);
                if (!File.Exists(fulltiffilename)) {

                    string url = assetstatusresults.visual.location;
                    using (HttpResponseMessage response = await client.GetAsync(url, HttpCompletionOption.ResponseHeadersRead))
                    using (Stream streamToReadFrom = await response.Content.ReadAsStreamAsync()) {
                        using (Stream streamToWriteTo = File.Open(fulltiffilename, FileMode.Create)) {
                            await streamToReadFrom.CopyToAsync(streamToWriteTo);
                        }
                    }

                    using (FileStream fStream = File.OpenRead(fulltiffilename)) {
                        string calculatedhashresults22 = GetHash<MD5>(fStream);
                        string saveresults = currentassetstatusresults.visual.md5_digest as string;
                        if (saveresults == calculatedhashresults22) {
                            MessageBox.Show(Strings.TheImageDownloadedProperly_Text);
                        } else {
                            MessageBox.Show(Strings.TheImageDownloadedDidNotCheckout_Text);
                            //laksjfdaskldfjas;lkfj
                        }
                    }
                }
            }
        }

        //public String GetHash<T>(this Stream stream) where T : HashAlgorithm, new() {
        //    StringBuilder sb = new StringBuilder();
        //    using (T crypt = new T()) {
        //        byte[] hashBytes = crypt.ComputeHash(stream);
        //        foreach (byte bt in hashBytes) {
        //            sb.Append(bt.ToString("x2"));
        //        }
        //    }
        //    return sb.ToString();
        //}

        public static string GetHash<T>(Stream stream) where T : HashAlgorithm {
            StringBuilder sb = new StringBuilder();

            MethodInfo create = typeof(T).GetMethod("Create", new Type[] { });
            using (T crypt = (T)create.Invoke(null, null)) {
                byte[] hashBytes = crypt.ComputeHash(stream);
                foreach (byte bt in hashBytes) {
                    sb.Append(bt.ToString("x2"));
                }
            }
            return sb.ToString();
        }

        static async Task HttpGetForLargeFileInRightWay3(PlanetImageViewModel currentfeature, AssetStatusResponse assetstatusresults) {
            //currentassetstatusresults
            using (HttpClient client = new HttpClient()) {
                string tiffilename2 = $"{currentfeature.FeatureId}.xml";
                var fulltiffilename2 = Path.Combine(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory, tiffilename2);
                if (!File.Exists(fulltiffilename2)) {
                    string url2 = assetstatusresults.visual_xml.location;
                    using (HttpResponseMessage response = await client.GetAsync(url2, HttpCompletionOption.ResponseHeadersRead))
                    using (Stream streamToReadFrom = await response.Content.ReadAsStreamAsync()) {
                        using (Stream streamToWriteTo = File.Open(fulltiffilename2, FileMode.Create)) {
                            await streamToReadFrom.CopyToAsync(streamToWriteTo);
                        }
                    }
                }
            }
        }

        static async Task HttpGetForLargeFileInRightWay4(PlanetImageViewModel currentfeature, AssetStatusResponse assetstatusresults) {
            //currentassetstatusresults
            using (HttpClient client = new HttpClient()) {
                string tiffilename = $"{currentfeature.FeatureId}analytic.tif";
                var fulltiffilename = Path.Combine(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory, tiffilename);
                if (!File.Exists(fulltiffilename)) {
                    string url = assetstatusresults.analytic.location;
                    using (HttpResponseMessage response = await client.GetAsync(url, HttpCompletionOption.ResponseHeadersRead))
                    using (Stream streamToReadFrom = await response.Content.ReadAsStreamAsync()) {
                        using (Stream streamToWriteTo = File.Open(fulltiffilename, FileMode.Create)) {
                            await streamToReadFrom.CopyToAsync(streamToWriteTo);
                        }
                    }
                    using (FileStream fStream = File.OpenRead(fulltiffilename)) {
                        string calculatedhashresults22 = GetHash<MD5>(fStream);
                        string saveresults = currentassetstatusresults.analytic.md5_digest as string;
                        if (saveresults == calculatedhashresults22) {
                            MessageBox.Show(Strings.TheImageDownloadedProperly_Text);
                        } else {
                            MessageBox.Show(Strings.TheImageDownloadedDidNotCheckout_Text);
                        }
                    }
                }
            }
        }

        static async Task HttpGetForLargeFileInRightWay5(PlanetImageViewModel currentfeature, AssetStatusResponse assetstatusresults) {
            //currentassetstatusresults
            using (HttpClient client = new HttpClient()) {
                string tiffilename2 = $"{currentfeature.FeatureId}analytic.xml";
                var fulltiffilename2 = Path.Combine(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory, tiffilename2);
                if (!File.Exists(fulltiffilename2)) {
                    string url2 = assetstatusresults.analytic_xml.location;
                    using (HttpResponseMessage response = await client.GetAsync(url2, HttpCompletionOption.ResponseHeadersRead))
                    using (Stream streamToReadFrom = await response.Content.ReadAsStreamAsync()) {
                        using (Stream streamToWriteTo = File.Open(fulltiffilename2, FileMode.Create)) {
                            await streamToReadFrom.CopyToAsync(streamToWriteTo);
                        }
                    }
                }
            }
        }

        //static async Task HttpGetForLargeFileInRightWay4(PlanetImageViewModel currentfeature, AssetStatusResponse assetstatusresults) {
        //    //currentassetstatusresults
        //    using (HttpClient client = new HttpClient()) {
        //        string tiffilename3 = $"{currentfeature.FeatureId}.udm";
        //        var fulltiffilename3 = Path.Combine(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory, tiffilename3);
        //        string url3 = assetstatusresults.udm.location;
        //        using (HttpResponseMessage response = await client.GetAsync(url3, HttpCompletionOption.ResponseHeadersRead))
        //        using (Stream streamToReadFrom = await response.Content.ReadAsStreamAsync()) {
        //            using (Stream streamToWriteTo = File.Open(fulltiffilename3, FileMode.Create)) {
        //                await streamToReadFrom.CopyToAsync(streamToWriteTo);
        //            }
        //        }
        //    }
        //}

        public void LoadGeometry() {
            try {

                //string filename = @"C:\Users\mharris2544\AppData\Roaming\AgConnections\Landdb\planetlabs_image_cache\422873_1655909_2017-02-27_0e3a.json";
                string filename = @"C:\Users\mharris2544\AppData\Roaming\AgConnections\Landdb\planetlabs_image_cache\20170217_171035_1655909_RapidEye-5.json";
                ImageFeature imagefeature = JsonConvert.DeserializeObject<ImageFeature>(File.ReadAllText(filename));
                if (imagefeature == null) { return; }
                string jsongeomerty = JsonConvert.SerializeObject(imagefeature.geometry);
                if (jsongeomerty == null) { return; }
                BaseShape baseshape = BaseShape.CreateShapeFromGeoJson(jsongeomerty);

                RectangleShape productlocation = new RectangleShape(-88.6164461, 36.6465821, -88.3329877, 36.417827);

                PolygonShape polyshape = productlocation.ToPolygon();

                int epsg = imagefeature.properties.epsg_code;

                selectedlayer.InternalFeatures.Clear();
                //selectedlayer.InternalFeatures.Add(new Feature(baseshape));
                selectedlayer.InternalFeatures.Add(new Feature(polyshape));

                RectangleShape newextent2 = selectedlayer.GetBoundingBox();
                RectangleShape newextent = ExtentHelper.GetBoundingBoxOfItems(selectedlayer.InternalFeatures);

                Map.CurrentExtent = newextent2;
                Map.Refresh();
                //LoadPlanetMapBackground(filename, epsg, imageextent);
            } catch (Exception ex) {
                int sdf = 0;
            }
        }

        public void ToggleImageType() {
            if (ToggleImagery == "NDVI") {
                ToggleImagery = "Imagery";
                LoadMapBackgroundSelection(false);
            } else {
                ToggleImagery = "NDVI";
                LoadMapBackgroundSelection(true);
            }
        }


        public void LoadMapBackgroundSelection(bool iscolored) {
            try {
                //ShowSubscriptionGrid = false;
                PlanetImageViewModel currentbackground = null;
                if (selectedImage != null) {
                    currentbackground = selectedImage;
                }
                if (currentbackground == null) { return; }

                var maindatasource = Path.Combine(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory, Infrastructure.ApplicationEnvironment.CurrentDataSourceId.ToString());
                var subdirectoryname = Path.Combine(maindatasource, selectedid);
                System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(subdirectoryname);
                if (!System.IO.Directory.Exists(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory)) {
                    System.IO.Directory.CreateDirectory(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory);
                }
                if (!System.IO.Directory.Exists(maindatasource)) {
                    System.IO.Directory.CreateDirectory(maindatasource);
                }
                if (!System.IO.Directory.Exists(subdirectoryname)) {
                    System.IO.Directory.CreateDirectory(subdirectoryname);
                }
                string jsonfilename2 = $"{currentbackground.FeatureId}properties.json";
                var filename2 = Path.Combine(subdirectoryname, jsonfilename2);
                if (!File.Exists(filename2)) {
                    if (BackgroundList.Count > 0) {
                        if (currentbackground.FeatureId == "") {
                            MessageBox.Show(Strings.MessageBox_PlanetMapPageViewModel_PleaseSelectImage_Text);
                            return;
                        }
                        MessageBox.Show(Strings.MessageBox_PlanetMapPageViewModel_CouldNotFindSatelliteImage_Text);
                        return;
                    } else {
                        return;
                    }
                }
                ImageFeature imagefeature = JsonConvert.DeserializeObject<ImageFeature>(File.ReadAllText(filename2));
                if (imagefeature == null) { return; }
                RectangleShape clippedextent = null;

                //RectangleShape rs333 = fieldsLayer.GetBoundingBox();
                //RectangleShape rs2333 = projection.ConvertToInternalProjection(rs333) as RectangleShape;

                RectangleShape extentshape = savedextentforimages;

                string infojsonfilename2 = $"{selectedid}info.json";
                var infofilename2 = Path.Combine(maindatasource, infojsonfilename2);
                if (File.Exists(infofilename2)) {
                    JsonClass.AreaSubscription subscriptionfile = JsonConvert.DeserializeObject<JsonClass.AreaSubscription>(File.ReadAllText(infofilename2));
                    if (subscriptionfile == null) { return; }
                    PolygonShape wktshape = new PolygonShape(subscriptionfile.AreaInWKT);
                    RectangleShape extentshapetest = wktshape.GetBoundingBox();
                    extentshape = wktshape.GetBoundingBox();
                } else {
                    log.Info("The image order doesn't exist, use the treeitem extent for {0}", selectedname);
                }
                clippedextent = extentshape;


                EnsureFieldsLayerIsOpen();
                int epsg = imagefeature.properties.epsg_code;
                projection = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetEpsgParametersString(epsg));

                selectedlayer.InternalFeatures.Clear();

                EnsureFieldsLayerIsOpen();


                RectangleShape newextent = selectedlayer.GetBoundingBox();
                //clippedextent = newextent;
                RectangleShape fieldextent = fieldsLayer.GetBoundingBox();

                string tifffilename2 = $"{currentbackground.FeatureId}.tif";
                var filename = Path.Combine(subdirectoryname, tifffilename2);

                string tifffilename3 = $"{currentbackground.FeatureId}clip.tif";
                var filename3 = Path.Combine(subdirectoryname, tifffilename3);

                string tifffilename4 = $"{currentbackground.FeatureId}spectralplot.png";
                var filename4 = Path.Combine(subdirectoryname, tifffilename4);

                bool isclipped = false;
                if (File.Exists(filename3)) {
                    isclipped = true;
                }
                if (isclipped) {
                    filename = filename3;
                    clippedextent = extentshape;
                    if (iscolored && File.Exists(filename4)) {
                        filename = filename4;
                    }
                }

                LoadPlanetMapBackground(filename, epsg, clippedextent, isclipped);

                Map.Refresh();
            } catch (Exception ex) {
                MessageBox.Show(Strings.AnErrorOccurredLoadingThisImage_Text);
            }

        }

        public void LoadXML() {
            try {
                PlanetImageViewModel currentbackground = null;
                foreach (var item in BackgroundList) {
                    if (item.Checked) {
                        currentbackground = item;
                    }
                }
                if (currentbackground == null) { return; }


                RectangleShape imageextent = LoadXmlImageExtent(currentbackground);
            } catch (Exception ex) {
                int sdf = 0;
            }
        }


        private RectangleShape DetermineImageExtent(PlanetImageViewModel currentfeature) {
            try {
                string tiffilename2 = $"{currentfeature.FeatureId}.xml";
                var filename = Path.Combine(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory, tiffilename2);

                if (!File.Exists(filename)) {
                    return null;
                }

                try {
                    XmlSerializer serializer = new XmlSerializer(typeof(EarthObservation));
                    Encoding encoding = new UTF8Encoding();
                    string placementXml = File.ReadAllText(filename);
                    if (string.IsNullOrEmpty(placementXml)) {
                        return null;
                    }

                    EarthObservation placement;
                    byte[] xmlBytes = encoding.GetBytes(placementXml);

                    using (MemoryStream memoryStream = new MemoryStream(xmlBytes)) {
                        placement = (EarthObservation)serializer.Deserialize(memoryStream);
                    }

                    FootprintGeographicLocationTopLeft upperleft = placement.target.Footprint.geographicLocation.topLeft;
                    FootprintGeographicLocationBottomRight bottomright = placement.target.Footprint.geographicLocation.bottomRight;

                    RectangleShape newextent1 = new RectangleShape(System.Convert.ToDouble(upperleft.longitude), System.Convert.ToDouble(upperleft.latitude), System.Convert.ToDouble(bottomright.longitude), System.Convert.ToDouble(bottomright.latitude));
                    //RectangleShape productlocation1 = new RectangleShape(-88.6164461, 36.6465821, -88.3329877, 36.417827);
                    return newextent1;

                } catch (InvalidOperationException) {
                    int asdfsdf = 0;
                    // Parsing placement XML failed. Fail silently.
                }


            } catch (Exception ex) {
                int sdf = 0;
            }
            return null;
        }

        private void LoadImageryFiles(string selected_id) {
            List<PlanetImageViewModel> imagelist1 = new List<PlanetImageViewModel>();
            var maindatasource = Path.Combine(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory, Infrastructure.ApplicationEnvironment.CurrentDataSourceId.ToString());
            var subdirectoryname = Path.Combine(maindatasource, selected_id);
            System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(subdirectoryname);
            if (!System.IO.Directory.Exists(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory)) {
                System.IO.Directory.CreateDirectory(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory);
            }
            if (!System.IO.Directory.Exists(maindatasource)) {
                System.IO.Directory.CreateDirectory(maindatasource);
            }
            if (!System.IO.Directory.Exists(subdirectoryname)) {
                System.IO.Directory.CreateDirectory(subdirectoryname);
            }
            try {
                System.IO.FileInfo[] files = dir.GetFiles(@"*.json");
                foreach (System.IO.FileInfo file in files) {
                    try {
                        if (file.Name.EndsWith(@"status.json")) { continue; }
                        if (file.FullName.EndsWith(@"AOI.json")) { continue; }
                        if (file.FullName.EndsWith(@"info.json")) { continue; }

                        if (file.FullName.EndsWith(@"properties.json")) {
                            ImageFeature imfeat = JsonConvert.DeserializeObject<ImageFeature>(File.ReadAllText(file.FullName));
                            if (imfeat == null) { continue; }
                            if (imfeat.properties == null) {
                                MessageBox.Show(Strings.PropertiesWereNull_Text.ToUpper() + file.Name);
                                continue;
                            }
                            DateTime convertedDate = Convert.ToDateTime(imfeat.properties.acquired);
                            string showStatus = "Show";
                            foreach (var perm in imfeat._permissions) {
                                if (perm == "Hide") {
                                    showStatus = "Hide";
                                }
                            }
                            PlanetImageViewModel pivm = new PlanetImageViewModel(this, imfeat.id, convertedDate, imfeat.properties.usable_data, imfeat.properties.cloud_cover, imfeat.properties.pixel_resolution, imfeat.properties.item_type, imfeat, showStatus);
                            if (showStatus == "Hide") {
                                continue;
                            }
                            //pivm.FeatureId = imfeat.id;
                            //pivm.DateAcquired = convertedDate;
                            //pivm.UsableData = imfeat.properties.usable_data;
                            //pivm.CloudCover = imfeat.properties.cloud_cover;
                            //pivm.Pixel_Resolution = imfeat.properties.pixel_resolution;
                            //pivm.ItemType = imfeat.properties.item_type;
                            //pivm.PlanetFeature = imfeat;
                            //pivm.ShowStatus = "Show";
                            //foreach (var perm in imfeat._permissions)
                            //{
                            //    if(perm == "Hide")
                            //    {
                            //        pivm.ShowStatus = "Hide";
                            //    }
                            //}
                            //if(pivm.ShowStatus == "Hide")
                            //{
                            //    continue;
                            //}
                            string tifffilename4 = $"{imfeat.id}spectralplot.png";
                            var filename4 = Path.Combine(subdirectoryname, tifffilename4);
                            if (File.Exists(filename4)) {
                                imagelist1.Add(pivm);
                            }
                        }
                    } catch (Exception ex) {
                        MessageBox.Show(Strings.ProblemLoadingImageFiles_Text);
                    }
                }
            } catch (Exception ex1) {
                MessageBox.Show(Strings.NoImageryFilesInTheDirectory_Text);
            }
            //imagelist1.OrderByDescending(x => x.DateAcquired).ToList();
            BackgroundList = imagelist1.OrderByDescending(x => x.DateAcquired).ToList();
        }


        public void UpdateSelectedFeatureProperties(PlanetImageViewModel featureproperties) {
            UpdateFeatureProperty(featureproperties);
        }

        private void UpdateFeatureProperty(PlanetImageViewModel featureproperties) {
            var maindatasource = Path.Combine(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory, Infrastructure.ApplicationEnvironment.CurrentDataSourceId.ToString());
            var subdirectoryname = Path.Combine(maindatasource, selectedid);
            var featurefilename = Path.Combine(subdirectoryname, featureproperties.FeatureId);
            string featurefilenamewithextention = string.Format("{0}properties.json", featurefilename);
            System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(subdirectoryname);
            if (!System.IO.Directory.Exists(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory)) {
                System.IO.Directory.CreateDirectory(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory);
            }
            if (!System.IO.Directory.Exists(maindatasource)) {
                System.IO.Directory.CreateDirectory(maindatasource);
            }
            if (!System.IO.Directory.Exists(subdirectoryname)) {
                System.IO.Directory.CreateDirectory(subdirectoryname);
            }
            try {
                if (File.Exists(featurefilenamewithextention)) {
                    ImageFeature imfeat = JsonConvert.DeserializeObject<ImageFeature>(File.ReadAllText(featurefilenamewithextention));
                    if (imfeat == null) {
                        MessageBox.Show($"{Strings.MessageBox_PlanetMapPageViewModel_FeatureWasNull_Text.ToUpper()}---" + featureproperties.FeatureId);
                        return;
                    }
                    if (imfeat.properties == null) {
                        MessageBox.Show($"{Strings.MessageBox_PlanetMapPageViewModel_PropertiesWereNull_Text}---" + featureproperties.FeatureId);
                        return;
                    }

                    foreach (var perm in imfeat._permissions) {
                        if (perm == "Hide") {
                            //MessageBox.Show("FEATURE WAS ALREADY HIDDEN---" + featureproperties.FeatureId);
                            return;
                        }
                    }

                    List<string> permissionslist = imfeat._permissions.ToList<string>();
                    permissionslist.Add(featureproperties.ShowStatus);
                    imfeat._permissions = permissionslist.ToArray<string>();

                    File.WriteAllText(featurefilenamewithextention, JsonConvert.SerializeObject(imfeat));

                    //for (int i = 0; i < BackgroundList.Count; i++)
                    //{
                    //    if (BackgroundList[i].FeatureId == featureproperties.FeatureId)
                    //    {
                    //        BackgroundList.RemoveAt(i);
                    //        RaisePropertyChanged("BackgroundList");
                    //        return;
                    //    }
                    //}
                }
            } catch (Exception ex1) {
                MessageBox.Show(Strings.MessageBox_PlanetMapPageViewModel_ProblemProcessingImageryPropertyFiles_Text);
            }
        }


        private RectangleShape LoadXmlImageExtent(PlanetImageViewModel currentfeature) {
            string tiffilename2 = $"{currentfeature.FeatureId}.xml";
            var filename = Path.Combine(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory, tiffilename2);
            if (!File.Exists(filename)) {
                return null;
            }
            try {

                double upperleftlatitude = 0;
                double upperleftlongitude = 0;
                double bottomrightlatitude = 0;
                double bottomrightlongitude = 0;
                XDocument xdoc = XDocument.Load(filename); //you'll have to edit your path

                var results1 = from st in xdoc.Descendants()
                               select st;

                foreach (var r in results1) {
                    if (r.Name.LocalName == "geographicLocation") {
                        var results4 = from ele in r.Descendants()
                                       select ele;

                        foreach (var p in results4) {
                            if (p.Name.LocalName == "topLeft") {
                                var results5 = from ele in p.Descendants()
                                               select ele;

                                foreach (var l in results5) {
                                    if (l.Name.LocalName == "latitude") {
                                        upperleftlatitude = System.Convert.ToDouble(l.Value);
                                    }
                                    if (l.Name.LocalName == "longitude") {
                                        upperleftlongitude = System.Convert.ToDouble(l.Value);
                                    }
                                }

                            }

                            if (p.Name.LocalName == "bottomRight") {
                                var results6 = from ele in p.Elements()
                                               select ele;

                                foreach (var k in results6) {
                                    if (k.Name.LocalName == "latitude") {
                                        bottomrightlatitude = System.Convert.ToDouble(k.Value);
                                    }
                                    if (k.Name.LocalName == "longitude") {
                                        bottomrightlongitude = System.Convert.ToDouble(k.Value);
                                    }
                                }

                            }
                        }
                    }
                }

                if (upperleftlatitude == 0 || upperleftlongitude == 0 || bottomrightlatitude == 0 || bottomrightlongitude == 0) {
                    return null;
                }
                RectangleShape newextent1 = new RectangleShape(upperleftlongitude, upperleftlatitude, bottomrightlongitude, bottomrightlatitude);
                return newextent1;
            } catch (Exception ex1) {
                Console.WriteLine("Exception of type " + ex1.GetType().Name);
                throw;
            }
        }

        async void BuildImageryList() {
            var currentCropYearId = new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear);
            int newndviimagescount = 0;
            int newimagescount = 0;

            imageryLoadTask = Task.Run(() => {

                try {
                    string storage_account = "agcbatchtest1";
                    string storage_container = "planetlabs-test";
                    string sas_token = "SmhGD0DDUgMNfxHxfmw7rMDBEBxe6b7QDIXVsSXE1bj2VvQDhXpKLJgrUCLs1zKqI+PGp877ZPRUciZT7umVUw==";

                    var useHttps = true;
                    var connValid = true;

                    StorageCredentials storageCredentials = new StorageCredentials(storage_account, sas_token);
                    CloudStorageAccount storageAccount = new CloudStorageAccount(storageCredentials, useHttps);
                    string conString1 = storageAccount.ToString(connValid);

                    //  create Azure Storage
                    CloudStorageAccount sa = CloudStorageAccount.Parse(conString1);
                    //  create a blob client.
                    CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                    //  create a container 
                    CloudBlobContainer container = blobClient.GetContainerReference(storage_container);

                    string prefix = Infrastructure.ApplicationEnvironment.CurrentDataSourceId.ToString();
                    IEnumerable<IListBlobItem> bloblist = container.ListBlobs(prefix, true, BlobListingDetails.None, null, null);
                    foreach (IListBlobItem blobitem in bloblist) {
                        string absolutepath = blobitem.Uri.AbsolutePath;
                        var blogsegments = blobitem.Uri.Segments;

                        string acc = "";
                        string con = "";
                        string ds = "";
                        string tree = "";
                        string fn = "";
                        foreach (string seg1 in blogsegments) {
                            string seg = seg1.Replace("/", "");
                            if (seg == "") { continue; }
                            if (seg.EndsWith("ndvi.tif")) { continue; }
                            if (seg.EndsWith("AOI.geojson")) { continue; }
                            if (seg.EndsWith("info.bak")) { continue; }
                            if (seg.EndsWith("clip.tif")) {
                                fn = seg;
                            } else if (seg.EndsWith("plot.png")) {
                                fn = seg;
                            } else if (seg.EndsWith("info.json")) {
                                fn = seg;
                            } else if (seg == Infrastructure.ApplicationEnvironment.CurrentDataSourceId.ToString()) {
                                ds = seg;
                            } else if (seg == storage_account) {
                                acc = seg;
                            } else if (seg == storage_container) {
                                con = seg;
                            } else {
                                tree = seg;
                            }
                        }
                        if (fn == "") { continue; }
                        string mainfield = string.Empty;
                        string maindirectory = string.Empty;
                        var maindatasource = Path.Combine(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory, Infrastructure.ApplicationEnvironment.CurrentDataSourceId.ToString());
                        if (!Directory.Exists(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory)) {
                            Directory.CreateDirectory(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory);
                        }
                        if (!Directory.Exists(maindatasource)) {
                            Directory.CreateDirectory(maindatasource);
                        }
                        if (!string.IsNullOrEmpty(tree)) {
                            var subdirectoryname = Path.Combine(maindatasource, tree);
                            maindirectory = subdirectoryname;
                            mainfield = Path.Combine(subdirectoryname, fn);
                            if (!Directory.Exists(subdirectoryname)) {
                                Directory.CreateDirectory(subdirectoryname);
                            }
                        } else {
                            mainfield = Path.Combine(maindatasource, fn);
                            maindirectory = maindatasource;
                        }


                        if (!string.IsNullOrEmpty(mainfield)) {
                            if (blobitem is CloudBlockBlob) {
                                CloudBlockBlob blob = container.GetBlockBlobReference(((CloudBlockBlob)blobitem).Name);
                                if (blob.Name.EndsWith("info.json")) {
                                    if (File.Exists(mainfield)) {
                                        string newName = mainfield.Replace("json", "bak");
                                        File.Copy(mainfield, newName, true);
                                        File.Delete(mainfield);
                                    }
                                    using (var fileStream = System.IO.File.OpenWrite(mainfield)) {
                                        blob.DownloadToStream(fileStream);
                                    }

                                    ProcessImageryMetadata(fn);
                                } else if (blob.Name.EndsWith("clip.tif") || blob.Name.EndsWith("plot.png")) {
                                    if (!File.Exists(mainfield)) {
                                        using (var fileStream = System.IO.File.OpenWrite(mainfield)) {
                                            blob.DownloadToStream(fileStream);
                                            if (blob.Name.EndsWith("plot.png")) {
                                                newndviimagescount += 1;
                                                log.Info("Download new ndvi image {0}.", fn);
                                            }
                                            if (blob.Name.EndsWith("clip.tif")) {
                                                newimagescount += 1;
                                                log.Info("Download new tif image {0}.", fn);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    //return 1;
                } catch (Exception ex) {
                    //return 0;
                }

            });
            await imageryLoadTask;
            log.Info("Added {0} tif images and {1} ndvi images.", newimagescount, newndviimagescount);
        }

        async void RetrieveImageryList() {
            var currentCropYearId = new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear);
            int newndviimagescount = 0;
            int newimagescount = 0;
            int currentimagescount = 0;

            retrieveImageryTask = Task.Run(() => {
                Stopwatch sw = new Stopwatch();
                try {
                    //Submit to microservices
                    if (NetworkStatus.IsInternetAvailable()) {
                        sw.Start();

                        //responsezones.Split();
                        //String pattern = @"\s-\s?[+*]?\s?-\s";
                        //String[] elements = string.Split(responsezones, pattern);
                        //string[] arrayofzones = responsezones.ToArray<string>();



                        //string subfilename = $"{selectedid}info.json";
                        //string maindatasource = Path.Combine(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory, ApplicationEnvironment.CurrentDataSourceId.ToString());
                        //var mainfield = Path.Combine(maindatasource, subfilename);
                        //var subdirectoryname = Path.Combine(maindatasource, selectedid);
                        //filetoupload = mainfield;
                        //if (!Directory.Exists(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory))
                        //{
                        //    Directory.CreateDirectory(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory);
                        //}
                        //if (!Directory.Exists(maindatasource))
                        //{
                        //    Directory.CreateDirectory(maindatasource);
                        //}
                        //if (!Directory.Exists(subdirectoryname))
                        //{
                        //    Directory.CreateDirectory(subdirectoryname);
                        //}
                        //File.WriteAllText(mainfield, JsonConvert.SerializeObject(subscription));
                        //string shapejsonfilename = $"{selectedid}AOI.geojson";
                        //var shapefulldirectoryname = Path.Combine(maindatasource, shapejsonfilename);


                        string maindatasource = Path.Combine(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory, ApplicationEnvironment.CurrentDataSourceId.ToString());
                        if (!Directory.Exists(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory)) {
                            Directory.CreateDirectory(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory);
                        }
                        if (!Directory.Exists(maindatasource)) {
                            Directory.CreateDirectory(maindatasource);
                        }

                        ////http://localhost:9016/planet-labs/api/zones/b66d49d8-8ae8-4d3b-88f6-a7e067b8e6ad/abeeb6a1-478a-44cf-a5c2-6db147b53cf2
                        //http://agc-jd-connect-t0117.eastus2.cloudapp.azure.com/planet-labs/api/swagger/
                        var planetLabBaseUri = string.Format("{0}/zones", RemoteConfigurationSettings.PlanetLabsRemoteUri);
                        var planetLabUri = string.Format("{0}/{1}", planetLabBaseUri, ApplicationEnvironment.CurrentDataSourceId);

                        var cloudClientFactory = new Landdb.Infrastructure.CloudClientFactory();
                        var token = cloudClientFactory.ResolveStorageCredentials();

                        var client1 = new HttpClient {
                            BaseAddress = new Uri(planetLabUri)
                        };
                        client1.DefaultRequestHeaders.Add("Accept", "application/json");
                        token.SetBearerTokenAsync(client1);
                        var response = client1.GetStringAsync(planetLabUri);
                        response.Wait();
                        string resultzones = response.Result;

                        List<string> listofzones = new List<string>();


                        //if (response.Result.Length > 10)
                        //{
                        //    JsonClass.PlanetCloudItems zoneresults = JsonConvert.DeserializeObject<JsonClass.PlanetCloudItems>(response.Result);
                        //    listofzones = zoneresults.ItemList.ToList();
                        //}
                        //List<string> listoffeatures = new List<string>();
                        if (response.Result.Length > 10) {
                            listofzones = JsonConvert.DeserializeObject<List<string>>(response.Result);
                        }

                        //Dictionary<string, FileInfo[]> zonedict = new Dictionary<string, FileInfo[]>();
                        DirectoryInfo directoryInfo = new DirectoryInfo(maindatasource);
                        DirectoryInfo[] subdirectories = directoryInfo.GetDirectories();
                        foreach (var thisdirectory in subdirectories) {
                            FileInfo[] pictures = thisdirectory.GetFiles().Where(f => f.Extension == ".png").ToArray();
                            currentimagescount += pictures.Count();
                        }
                        if (listofzones.Count < 1) {
                            foreach (var thisdirectory in subdirectories) {
                                FileInfo[] files = thisdirectory.GetFiles().Where(f => f.Extension == ".json").ToArray();
                                listofzones.Add(thisdirectory.Name);
                                //zonedict.Add(thisdirectory.Name, files);
                            }
                        }


                        //try
                        //{
                        //    var featuresZoneDefinitionssPlanetLabUri = string.Format("{0}/{1}/definitions", planetLabBaseUri, ApplicationEnvironment.CurrentDataSourceId);
                        //    var client333 = new HttpClient
                        //    {
                        //        BaseAddress = new Uri(featuresZoneDefinitionssPlanetLabUri)
                        //    };

                        //    client333.DefaultRequestHeaders.Add("Accept", "application/json");
                        //    token.SetBearerToken(client333);
                        //    var responseonefeature = client333.GetStringAsync(featuresZoneDefinitionssPlanetLabUri);
                        //    responseonefeature.Wait();
                        //    string resresultonefeatures = responseonefeature.Result;
                        //} catch (Exception exxxx)
                        //{
                        //    int lkjlkj = 0;
                        //}



                        //listofzones.Add(@"c553312b-6b88-4981-81a4-7ed68f6ab1ce");
                        //listofzones.Add(@"b15d9311-180a-45c0-9bbc-e1dc8967471f");
                        string[] arrayofzones = listofzones.ToArray<string>();

                        foreach (string zone in arrayofzones) {
                            var zonesPlanetLabUri = string.Format("{0}/{1}/{2}/features", planetLabBaseUri, ApplicationEnvironment.CurrentDataSourceId, zone);

                            var client2 = new HttpClient {
                                BaseAddress = new Uri(zonesPlanetLabUri)
                            };

                            client2.DefaultRequestHeaders.Add("Accept", "application/json");
                            token.SetBearerTokenAsync(client2);
                            var responsefeatures = client2.GetStringAsync(zonesPlanetLabUri);
                            responsefeatures.Wait();
                            string resultzonesfeatures = responsefeatures.Result;

                            //JsonClass.PlanetCloudItems cloudresults = null;
                            List<string> listoffeatures = new List<string>();
                            if (responsefeatures.Result.Length > 10) {
                                listoffeatures = JsonConvert.DeserializeObject<List<string>>(responsefeatures.Result);
                            }
                            if (listoffeatures.Count == 0) { continue; }
                            //if (cloudresults == null) { continue; }

                            //FileInfo[] files = zonedict[zone];
                            //Dictionary<string, string> featuredictionary = new Dictionary<string, string>();
                            //foreach(var fname in files)
                            //{
                            //    string strname = fname.Name;
                            //    string cutname = strname.Replace("properties.json", "");
                            //    featuredictionary.Add(cutname, cutname);
                            //}
                            foreach (string imagefeature in listoffeatures) {
                                string tiffilename2 = $"{imagefeature}{"properties.json"}";
                                var fulltiffilename0 = Path.Combine(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory, Infrastructure.ApplicationEnvironment.CurrentDataSourceId.ToString());
                                var fulltiffilename1 = Path.Combine(fulltiffilename0, zone);
                                var fulltiffilename2 = Path.Combine(fulltiffilename1, tiffilename2);
                                if (!Directory.Exists(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory)) {
                                    Directory.CreateDirectory(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory);
                                }
                                if (!Directory.Exists(fulltiffilename0)) {
                                    Directory.CreateDirectory(fulltiffilename0);
                                }
                                if (!Directory.Exists(fulltiffilename1)) {
                                    Directory.CreateDirectory(fulltiffilename1);
                                }

                                if (!File.Exists(fulltiffilename2)) {
                                    var featuresZonesPlanetLabUri = string.Format("{0}/{1}/{2}/features/{3}", planetLabBaseUri, ApplicationEnvironment.CurrentDataSourceId, zone, imagefeature);
                                    var client3 = new HttpClient {
                                        BaseAddress = new Uri(featuresZonesPlanetLabUri)
                                    };

                                    client3.DefaultRequestHeaders.Add("Accept", "application/json");
                                    token.SetBearerTokenAsync(client3);
                                    var responseonefeature = client3.GetStringAsync(featuresZonesPlanetLabUri);
                                    responseonefeature.Wait();
                                    string resresultonefeatures = responseonefeature.Result;

                                    JsonClass.PlanetImageItems cloudfeatureresults = null;
                                    if (responseonefeature.Result.Length > 10) {
                                        cloudfeatureresults = JsonConvert.DeserializeObject<JsonClass.PlanetImageItems>(responseonefeature.Result);
                                    }
                                    if (cloudfeatureresults == null) { continue; }

                                    if (!cloudfeatureresults.featurePropertiesUri.Contains("dummy")) {
                                        using (HttpClient client99 = new HttpClient()) {
                                            log.Info($"Downloading Planet feature {imagefeature} for {zone}.  ");
                                            Console.WriteLine($"Downloading Planet feature {imagefeature} for {zone}.  ");
                                            newimagescount++;
                                            HttpGetForLargeFileInRightWayJson(client99, imagefeature, cloudfeatureresults.featurePropertiesUri, zone, "properties.json").Wait();
                                            HttpGetForLargeFileInRightWayJson(client99, imagefeature, cloudfeatureresults.clippedSatelliteImageUri, zone, "clip.tif").Wait();
                                            HttpGetForLargeFileInRightWayJson(client99, imagefeature, cloudfeatureresults.vegetativeIndexPlotUri, zone, "spectralplot.png").Wait();
                                            //HttpGetForLargeFileInRightWayJson(client99, imagefeature, cloudfeatureresults.imageorderinfoUri, zone, "info.json").Wait();
                                        }
                                    }
                                }
                            }
                        }
                        sw.Stop();
                        Console.WriteLine(sw.ElapsedMilliseconds);
                        string counttheimages = String.Format(Strings.PlanetMapPageViewModel_AddedImages_Text, newimagescount, currentimagescount);
                        log.Info(counttheimages);
                        Console.WriteLine(counttheimages);
                        if (newimagescount > 0) {
                            MessageBox.Show(counttheimages);
                        }
                        //if (System.Diagnostics.Debugger.IsAttached)
                        //{
                        //    MessageBox.Show(counttheimages);
                        //}

                    }
                } catch (Exception ex) {
                    log.Info("Problem downloading images.");
                    sw.Stop();
                    string counttheimages = String.Format(Strings.PlanetMapPageViewModel_AddedIimagesTime_Text, newimagescount, currentimagescount, sw.ElapsedMilliseconds);
                    log.Info(counttheimages);
                    //if (newimagescount > 0)
                    //{
                    //    MessageBox.Show(counttheimages);
                    //}
                    if (System.Diagnostics.Debugger.IsAttached) {
                        MessageBox.Show(counttheimages);
                    }
                }

            });
            await retrieveImageryTask;


        }

        static async Task HttpGetForLargeFileInRightWayJson(HttpClient client, string featurename, string featureitem, string zone, string fileextension) {
            string tiffilename2 = $"{featurename}{fileextension}";
            try {
                string fulltiffilename4 = string.Empty;
                var fulltiffilename0 = Path.Combine(Infrastructure.ApplicationEnvironment.PlanetLabsImageCacheDirectory, Infrastructure.ApplicationEnvironment.CurrentDataSourceId.ToString());
                var fulltiffilename1 = Path.Combine(fulltiffilename0, zone);
                var fulltiffilename2 = Path.Combine(fulltiffilename1, tiffilename2);
                var fulltiffilename3 = Path.Combine(fulltiffilename0, tiffilename2);
                if (!File.Exists(fulltiffilename2) && fileextension != "info.json") {
                    string url2 = featureitem;
                    using (HttpResponseMessage response = await client.GetAsync(url2, HttpCompletionOption.ResponseHeadersRead))
                    using (Stream streamToReadFrom = await response.Content.ReadAsStreamAsync()) {
                        using (Stream streamToWriteTo = File.Open(fulltiffilename2, FileMode.Create)) {
                            await streamToReadFrom.CopyToAsync(streamToWriteTo);
                        }
                    }
                } else if (!File.Exists(fulltiffilename3) && fileextension == "info.json") {
                    string url2 = featureitem;
                    using (HttpResponseMessage response = await client.GetAsync(url2, HttpCompletionOption.ResponseHeadersRead))
                    using (Stream streamToReadFrom = await response.Content.ReadAsStreamAsync()) {
                        using (Stream streamToWriteTo = File.Open(fulltiffilename3, FileMode.Create)) {
                            await streamToReadFrom.CopyToAsync(streamToWriteTo);
                        }
                    }
                }
            } catch (Exception ex) {
                Console.WriteLine($"Problem downloading {tiffilename2} image.  ");
            }
        }


        async void RetrievePlanetPermissions() {
            Stopwatch sw = new Stopwatch();
            try {
                var planetLabBaseUri = string.Format("{0}/api/permission/getbydatasource?dataSourceId=", RemoteConfigurationSettings.GetBaseUri());
                var planetLabUri = string.Format("{0}{1}", planetLabBaseUri, ApplicationEnvironment.CurrentDataSourceId);
                var cloudClientFactory = new Landdb.Infrastructure.CloudClientFactory();
                var token = cloudClientFactory.ResolveStorageCredentials();
                var client1 = new HttpClient {
                    BaseAddress = new Uri(planetLabUri)
                };
                client1.DefaultRequestHeaders.Add("Accept", "application/json");
                await token.SetBearerTokenAsync(client1);
                var response = client1.GetStringAsync(planetLabUri);
                response.Wait();
                string resultzones = response.Result;
                if (response.Result.Length > 10) {
                    if (response.Result.Contains("CanUsePlanetLabs")) {
                        log.Debug("CanUsePlanetLabs permission found");
                    }
                }
            } catch (Exception ex) {
                log.Info("Problem downloading images.");
            }
            sw.Stop();
            Console.WriteLine(sw.ElapsedMilliseconds);
        }

        public Bitmap MapImage { get; set; }

        void Print() {
            log.Info("CMGTools - Print bitmap");
            IsOptionsMapPopupOpen = false;
            var width = Map.ActualWidth;
            var height = Map.ActualHeight;
            RenderTargetBitmap bmp = new RenderTargetBitmap((int)width, (int)height, 96, 96, PixelFormats.Pbgra32);
            bmp.Render(Map);
            PngBitmapEncoder png = new PngBitmapEncoder();
            png.Frames.Add(BitmapFrame.Create(bmp));

            try {

                MemoryStream fs = new MemoryStream();
                png.Save(fs);
                MapImage = new Bitmap(fs);
            } catch (Exception ex) {
                return;
            }

            var czs = from c in ClientLayer
                      where c.CropZoneId != null
                      select new Guid(c.CropZoneId.ToString());

            //Now generate Report
            var subHeader = string.IsNullOrEmpty(selectedname) ? string.Empty : selectedname;
            var vm = new Secondary.Reports.Map.PrintMapViewModel(clientEndpoint, dispatcher, MapImage, czs.ToList(), subHeader, new FieldId());
            var sd = new ScreenDescriptor("Landdb.Views.Secondary.Reports.Work_Order.SingleWorkOrderView", "viewReport", vm);
            Messenger.Default.Send(new ShowOverlayMessage() { ScreenDescriptor = sd });
        }

    }
}