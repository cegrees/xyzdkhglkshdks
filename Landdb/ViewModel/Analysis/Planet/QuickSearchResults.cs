﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Analysis.Planet {

    public class QuickSearchResults {
        public ResultLinks _links { get; set; }
        public ImageFeature[] features { get; set; }
        public string type { get; set; }
    }

    public class ResultLinks {
        public string _first { get; set; }
        public string _next { get; set; }
        public string _self { get; set; }
    }

    public class ImageFeature {
        public ImageLinks _links { get; set; }
        public string[] _permissions { get; set; }
        public Geometry geometry { get; set; }
        public string id { get; set; }
        public ImageProperties properties { get; set; }
        public string type { get; set; }
    }

    public class ImageLinks {
        public string _self { get; set; }
        public string assets { get; set; }
    }

    public class Geometry {
        public object[][][] coordinates { get; set; }
        //public double[][][] coordinates { get; set; }
        //public List<List<List<double>>>[] coordinates { get; set; }
        public string type { get; set; }
    }

    public class ImageProperties {
        public DateTime acquired { get; set; }
        public float anomalous_pixels { get; set; }
        public float cloud_cover { get; set; }
        public int columns { get; set; }
        public int epsg_code { get; set; }
        public float gsd { get; set; }
        public string instrument { get; set; }
        public string item_type { get; set; }
        public int origin_x { get; set; }
        public float origin_y { get; set; }
        public float pixel_resolution { get; set; }
        public string provider { get; set; }
        public DateTime published { get; set; }
        public int rows { get; set; }
        public string satellite_id { get; set; }
        public string strip_id { get; set; }
        public float sun_azimuth { get; set; }
        public float sun_elevation { get; set; }
        public DateTime updated { get; set; }
        public float usable_data { get; set; }
        public float view_angle { get; set; }
        public float black_fill { get; set; }
        public string catalog_id { get; set; }
        public string grid_cell { get; set; }
        public string data_type { get; set; }
        public int wrs_path { get; set; }
        public int wrs_row { get; set; }
        public int abs_orbit_number { get; set; }
        public string datatake_id { get; set; }
        public string granule_id { get; set; }
        public string mgrs_grid_id { get; set; }
        public DateTime product_generation_time { get; set; }
        public string product_id { get; set; }
        public int rel_orbit_number { get; set; }
        public string s2_processor_version { get; set; }
    }
}
