﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Analysis.Planet {

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.planet.com/products/productMetadataGeocorrected")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://schemas.planet.com/products/productMetadataGeocorrected", IsNullable = false)]
    public partial class EarthObservation {

        private metaDataProperty metaDataPropertyField;

        private validTime validTimeField;

        private @using usingField;

        private target targetField;

        private resultOf resultOfField;

        private string versionField;

        private decimal re_standard_product_versionField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.opengis.net/gml")]
        public metaDataProperty metaDataProperty
        {
            get
            {
                return this.metaDataPropertyField;
            }
            set
            {
                this.metaDataPropertyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.opengis.net/gml")]
        public validTime validTime
        {
            get
            {
                return this.validTimeField;
            }
            set
            {
                this.validTimeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.opengis.net/gml")]
        public @using @using
        {
            get
            {
                return this.usingField;
            }
            set
            {
                this.usingField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.opengis.net/gml")]
        public target target
        {
            get
            {
                return this.targetField;
            }
            set
            {
                this.targetField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.opengis.net/gml")]
        public resultOf resultOf
        {
            get
            {
                return this.resultOfField;
            }
            set
            {
                this.resultOfField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string version
        {
            get
            {
                return this.versionField;
            }
            set
            {
                this.versionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal re_standard_product_version
        {
            get
            {
                return this.re_standard_product_versionField;
            }
            set
            {
                this.re_standard_product_versionField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opengis.net/gml")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.opengis.net/gml", IsNullable = false)]
    public partial class metaDataProperty {

        private EarthObservationMetaData earthObservationMetaDataField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://schemas.planet.com/products/productMetadataGeocorrected")]
        public EarthObservationMetaData EarthObservationMetaData
        {
            get
            {
                return this.earthObservationMetaDataField;
            }
            set
            {
                this.earthObservationMetaDataField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.planet.com/products/productMetadataGeocorrected")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://schemas.planet.com/products/productMetadataGeocorrected", IsNullable = false)]
    public partial class EarthObservationMetaData {

        private string identifierField;

        private string acquisitionTypeField;

        private string productTypeField;

        private string statusField;

        private downlinkedTo downlinkedToField;

        private archivedIn archivedInField;

        private processing processingField;

        private EarthObservationMetaDataLicense licenseField;

        private decimal versionIsdField;

        private uint orderIdField;

        private uint tileIdField;

        private string pixelFormatField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://earth.esa.int/eop")]
        public string identifier
        {
            get
            {
                return this.identifierField;
            }
            set
            {
                this.identifierField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://earth.esa.int/eop")]
        public string acquisitionType
        {
            get
            {
                return this.acquisitionTypeField;
            }
            set
            {
                this.acquisitionTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://earth.esa.int/eop")]
        public string productType
        {
            get
            {
                return this.productTypeField;
            }
            set
            {
                this.productTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://earth.esa.int/eop")]
        public string status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://earth.esa.int/eop")]
        public downlinkedTo downlinkedTo
        {
            get
            {
                return this.downlinkedToField;
            }
            set
            {
                this.downlinkedToField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://earth.esa.int/eop")]
        public archivedIn archivedIn
        {
            get
            {
                return this.archivedInField;
            }
            set
            {
                this.archivedInField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://earth.esa.int/eop")]
        public processing processing
        {
            get
            {
                return this.processingField;
            }
            set
            {
                this.processingField = value;
            }
        }

        /// <remarks/>
        public EarthObservationMetaDataLicense license
        {
            get
            {
                return this.licenseField;
            }
            set
            {
                this.licenseField = value;
            }
        }

        /// <remarks/>
        public decimal versionIsd
        {
            get
            {
                return this.versionIsdField;
            }
            set
            {
                this.versionIsdField = value;
            }
        }

        /// <remarks/>
        public uint orderId
        {
            get
            {
                return this.orderIdField;
            }
            set
            {
                this.orderIdField = value;
            }
        }

        /// <remarks/>
        public uint tileId
        {
            get
            {
                return this.tileIdField;
            }
            set
            {
                this.tileIdField = value;
            }
        }

        /// <remarks/>
        public string pixelFormat
        {
            get
            {
                return this.pixelFormatField;
            }
            set
            {
                this.pixelFormatField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://earth.esa.int/eop")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://earth.esa.int/eop", IsNullable = false)]
    public partial class downlinkedTo {

        private downlinkedToDownlinkInformation downlinkInformationField;

        /// <remarks/>
        public downlinkedToDownlinkInformation DownlinkInformation
        {
            get
            {
                return this.downlinkInformationField;
            }
            set
            {
                this.downlinkInformationField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://earth.esa.int/eop")]
    public partial class downlinkedToDownlinkInformation {

        private downlinkedToDownlinkInformationAcquisitionStation acquisitionStationField;

        private System.DateTime acquisitionDateField;

        /// <remarks/>
        public downlinkedToDownlinkInformationAcquisitionStation acquisitionStation
        {
            get
            {
                return this.acquisitionStationField;
            }
            set
            {
                this.acquisitionStationField = value;
            }
        }

        /// <remarks/>
        public System.DateTime acquisitionDate
        {
            get
            {
                return this.acquisitionDateField;
            }
            set
            {
                this.acquisitionDateField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://earth.esa.int/eop")]
    public partial class downlinkedToDownlinkInformationAcquisitionStation {

        private string codeSpaceField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string codeSpace
        {
            get
            {
                return this.codeSpaceField;
            }
            set
            {
                this.codeSpaceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://earth.esa.int/eop")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://earth.esa.int/eop", IsNullable = false)]
    public partial class archivedIn {

        private archivedInArchivingInformation archivingInformationField;

        /// <remarks/>
        public archivedInArchivingInformation ArchivingInformation
        {
            get
            {
                return this.archivingInformationField;
            }
            set
            {
                this.archivingInformationField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://earth.esa.int/eop")]
    public partial class archivedInArchivingInformation {

        private archivedInArchivingInformationArchivingCenter archivingCenterField;

        private System.DateTime archivingDateField;

        private archivedInArchivingInformationArchivingIdentifier archivingIdentifierField;

        /// <remarks/>
        public archivedInArchivingInformationArchivingCenter archivingCenter
        {
            get
            {
                return this.archivingCenterField;
            }
            set
            {
                this.archivingCenterField = value;
            }
        }

        /// <remarks/>
        public System.DateTime archivingDate
        {
            get
            {
                return this.archivingDateField;
            }
            set
            {
                this.archivingDateField = value;
            }
        }

        /// <remarks/>
        public archivedInArchivingInformationArchivingIdentifier archivingIdentifier
        {
            get
            {
                return this.archivingIdentifierField;
            }
            set
            {
                this.archivingIdentifierField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://earth.esa.int/eop")]
    public partial class archivedInArchivingInformationArchivingCenter {

        private string codeSpaceField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string codeSpace
        {
            get
            {
                return this.codeSpaceField;
            }
            set
            {
                this.codeSpaceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://earth.esa.int/eop")]
    public partial class archivedInArchivingInformationArchivingIdentifier {

        private string codeSpaceField;

        private uint valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string codeSpace
        {
            get
            {
                return this.codeSpaceField;
            }
            set
            {
                this.codeSpaceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public uint Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://earth.esa.int/eop")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://earth.esa.int/eop", IsNullable = false)]
    public partial class processing {

        private processingProcessingInformation processingInformationField;

        /// <remarks/>
        public processingProcessingInformation ProcessingInformation
        {
            get
            {
                return this.processingInformationField;
            }
            set
            {
                this.processingInformationField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://earth.esa.int/eop")]
    public partial class processingProcessingInformation {

        private string processorNameField;

        private string processorVersionField;

        private string nativeProductFormatField;

        /// <remarks/>
        public string processorName
        {
            get
            {
                return this.processorNameField;
            }
            set
            {
                this.processorNameField = value;
            }
        }

        /// <remarks/>
        public string processorVersion
        {
            get
            {
                return this.processorVersionField;
            }
            set
            {
                this.processorVersionField = value;
            }
        }

        /// <remarks/>
        public string nativeProductFormat
        {
            get
            {
                return this.nativeProductFormatField;
            }
            set
            {
                this.nativeProductFormatField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.planet.com/products/productMetadataGeocorrected")]
    public partial class EarthObservationMetaDataLicense {

        private string licenseTypeField;

        private EarthObservationMetaDataLicenseResourceLink resourceLinkField;

        /// <remarks/>
        public string licenseType
        {
            get
            {
                return this.licenseTypeField;
            }
            set
            {
                this.licenseTypeField = value;
            }
        }

        /// <remarks/>
        public EarthObservationMetaDataLicenseResourceLink resourceLink
        {
            get
            {
                return this.resourceLinkField;
            }
            set
            {
                this.resourceLinkField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.planet.com/products/productMetadataGeocorrected")]
    public partial class EarthObservationMetaDataLicenseResourceLink {

        private string titleField;

        private string hrefField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = "http://www.w3.org/1999/xlink")]
        public string title
        {
            get
            {
                return this.titleField;
            }
            set
            {
                this.titleField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = "http://www.w3.org/1999/xlink")]
        public string href
        {
            get
            {
                return this.hrefField;
            }
            set
            {
                this.hrefField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opengis.net/gml")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.opengis.net/gml", IsNullable = false)]
    public partial class validTime {

        private validTimeTimePeriod timePeriodField;

        /// <remarks/>
        public validTimeTimePeriod TimePeriod
        {
            get
            {
                return this.timePeriodField;
            }
            set
            {
                this.timePeriodField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opengis.net/gml")]
    public partial class validTimeTimePeriod {

        private System.DateTime beginPositionField;

        private System.DateTime endPositionField;

        /// <remarks/>
        public System.DateTime beginPosition
        {
            get
            {
                return this.beginPositionField;
            }
            set
            {
                this.beginPositionField = value;
            }
        }

        /// <remarks/>
        public System.DateTime endPosition
        {
            get
            {
                return this.endPositionField;
            }
            set
            {
                this.endPositionField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opengis.net/gml")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.opengis.net/gml", IsNullable = false)]
    public partial class @using {

        private EarthObservationEquipment earthObservationEquipmentField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://earth.esa.int/eop")]
        public EarthObservationEquipment EarthObservationEquipment
        {
            get
            {
                return this.earthObservationEquipmentField;
            }
            set
            {
                this.earthObservationEquipmentField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://earth.esa.int/eop")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://earth.esa.int/eop", IsNullable = false)]
    public partial class EarthObservationEquipment {

        private EarthObservationEquipmentPlatform platformField;

        private EarthObservationEquipmentInstrument instrumentField;

        private EarthObservationEquipmentSensor sensorField;

        private EarthObservationEquipmentAcquisitionParameters acquisitionParametersField;

        /// <remarks/>
        public EarthObservationEquipmentPlatform platform
        {
            get
            {
                return this.platformField;
            }
            set
            {
                this.platformField = value;
            }
        }

        /// <remarks/>
        public EarthObservationEquipmentInstrument instrument
        {
            get
            {
                return this.instrumentField;
            }
            set
            {
                this.instrumentField = value;
            }
        }

        /// <remarks/>
        public EarthObservationEquipmentSensor sensor
        {
            get
            {
                return this.sensorField;
            }
            set
            {
                this.sensorField = value;
            }
        }

        /// <remarks/>
        public EarthObservationEquipmentAcquisitionParameters acquisitionParameters
        {
            get
            {
                return this.acquisitionParametersField;
            }
            set
            {
                this.acquisitionParametersField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://earth.esa.int/eop")]
    public partial class EarthObservationEquipmentPlatform {

        private EarthObservationEquipmentPlatformPlatform platformField;

        /// <remarks/>
        public EarthObservationEquipmentPlatformPlatform Platform
        {
            get
            {
                return this.platformField;
            }
            set
            {
                this.platformField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://earth.esa.int/eop")]
    public partial class EarthObservationEquipmentPlatformPlatform {

        private string shortNameField;

        private string serialIdentifierField;

        private string orbitTypeField;

        /// <remarks/>
        public string shortName
        {
            get
            {
                return this.shortNameField;
            }
            set
            {
                this.shortNameField = value;
            }
        }

        /// <remarks/>
        public string serialIdentifier
        {
            get
            {
                return this.serialIdentifierField;
            }
            set
            {
                this.serialIdentifierField = value;
            }
        }

        /// <remarks/>
        public string orbitType
        {
            get
            {
                return this.orbitTypeField;
            }
            set
            {
                this.orbitTypeField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://earth.esa.int/eop")]
    public partial class EarthObservationEquipmentInstrument {

        private EarthObservationEquipmentInstrumentInstrument instrumentField;

        /// <remarks/>
        public EarthObservationEquipmentInstrumentInstrument Instrument
        {
            get
            {
                return this.instrumentField;
            }
            set
            {
                this.instrumentField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://earth.esa.int/eop")]
    public partial class EarthObservationEquipmentInstrumentInstrument {

        private string shortNameField;

        /// <remarks/>
        public string shortName
        {
            get
            {
                return this.shortNameField;
            }
            set
            {
                this.shortNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://earth.esa.int/eop")]
    public partial class EarthObservationEquipmentSensor {

        private Sensor sensorField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://schemas.planet.com/products/productMetadataGeocorrected")]
        public Sensor Sensor
        {
            get
            {
                return this.sensorField;
            }
            set
            {
                this.sensorField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.planet.com/products/productMetadataGeocorrected")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://schemas.planet.com/products/productMetadataGeocorrected", IsNullable = false)]
    public partial class Sensor {

        private string sensorTypeField;

        private resolution resolutionField;

        private string scanTypeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://earth.esa.int/eop")]
        public string sensorType
        {
            get
            {
                return this.sensorTypeField;
            }
            set
            {
                this.sensorTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://earth.esa.int/eop")]
        public resolution resolution
        {
            get
            {
                return this.resolutionField;
            }
            set
            {
                this.resolutionField = value;
            }
        }

        /// <remarks/>
        public string scanType
        {
            get
            {
                return this.scanTypeField;
            }
            set
            {
                this.scanTypeField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://earth.esa.int/eop")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://earth.esa.int/eop", IsNullable = false)]
    public partial class resolution {

        private string uomField;

        private decimal valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string uom
        {
            get
            {
                return this.uomField;
            }
            set
            {
                this.uomField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public decimal Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://earth.esa.int/eop")]
    public partial class EarthObservationEquipmentAcquisitionParameters {

        private Acquisition acquisitionField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://schemas.planet.com/products/productMetadataGeocorrected")]
        public Acquisition Acquisition
        {
            get
            {
                return this.acquisitionField;
            }
            set
            {
                this.acquisitionField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.planet.com/products/productMetadataGeocorrected")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://schemas.planet.com/products/productMetadataGeocorrected", IsNullable = false)]
    public partial class Acquisition {

        private string orbitDirectionField;

        private incidenceAngle incidenceAngleField;

        private illuminationAzimuthAngle illuminationAzimuthAngleField;

        private illuminationElevationAngle illuminationElevationAngleField;

        private AcquisitionAzimuthAngle azimuthAngleField;

        private AcquisitionSpaceCraftViewAngle spaceCraftViewAngleField;

        private System.DateTime acquisitionDateTimeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://earth.esa.int/eop")]
        public string orbitDirection
        {
            get
            {
                return this.orbitDirectionField;
            }
            set
            {
                this.orbitDirectionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://earth.esa.int/eop")]
        public incidenceAngle incidenceAngle
        {
            get
            {
                return this.incidenceAngleField;
            }
            set
            {
                this.incidenceAngleField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://earth.esa.int/opt")]
        public illuminationAzimuthAngle illuminationAzimuthAngle
        {
            get
            {
                return this.illuminationAzimuthAngleField;
            }
            set
            {
                this.illuminationAzimuthAngleField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://earth.esa.int/opt")]
        public illuminationElevationAngle illuminationElevationAngle
        {
            get
            {
                return this.illuminationElevationAngleField;
            }
            set
            {
                this.illuminationElevationAngleField = value;
            }
        }

        /// <remarks/>
        public AcquisitionAzimuthAngle azimuthAngle
        {
            get
            {
                return this.azimuthAngleField;
            }
            set
            {
                this.azimuthAngleField = value;
            }
        }

        /// <remarks/>
        public AcquisitionSpaceCraftViewAngle spaceCraftViewAngle
        {
            get
            {
                return this.spaceCraftViewAngleField;
            }
            set
            {
                this.spaceCraftViewAngleField = value;
            }
        }

        /// <remarks/>
        public System.DateTime acquisitionDateTime
        {
            get
            {
                return this.acquisitionDateTimeField;
            }
            set
            {
                this.acquisitionDateTimeField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://earth.esa.int/eop")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://earth.esa.int/eop", IsNullable = false)]
    public partial class incidenceAngle {

        private string uomField;

        private float valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string uom
        {
            get
            {
                return this.uomField;
            }
            set
            {
                this.uomField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public float Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://earth.esa.int/opt")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://earth.esa.int/opt", IsNullable = false)]
    public partial class illuminationAzimuthAngle {

        private string uomField;

        private float valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string uom
        {
            get
            {
                return this.uomField;
            }
            set
            {
                this.uomField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public float Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://earth.esa.int/opt")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://earth.esa.int/opt", IsNullable = false)]
    public partial class illuminationElevationAngle {

        private string uomField;

        private float valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string uom
        {
            get
            {
                return this.uomField;
            }
            set
            {
                this.uomField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public float Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.planet.com/products/productMetadataGeocorrected")]
    public partial class AcquisitionAzimuthAngle {

        private string uomField;

        private float valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string uom
        {
            get
            {
                return this.uomField;
            }
            set
            {
                this.uomField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public float Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.planet.com/products/productMetadataGeocorrected")]
    public partial class AcquisitionSpaceCraftViewAngle {

        private string uomField;

        private float valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string uom
        {
            get
            {
                return this.uomField;
            }
            set
            {
                this.uomField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public float Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opengis.net/gml")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.opengis.net/gml", IsNullable = false)]
    public partial class target {

        private Footprint footprintField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://schemas.planet.com/products/productMetadataGeocorrected")]
        public Footprint Footprint
        {
            get
            {
                return this.footprintField;
            }
            set
            {
                this.footprintField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.planet.com/products/productMetadataGeocorrected")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://schemas.planet.com/products/productMetadataGeocorrected", IsNullable = false)]
    public partial class Footprint {

        private multiExtentOf multiExtentOfField;

        private centerOf centerOfField;

        private FootprintGeographicLocation geographicLocationField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.opengis.net/gml")]
        public multiExtentOf multiExtentOf
        {
            get
            {
                return this.multiExtentOfField;
            }
            set
            {
                this.multiExtentOfField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.opengis.net/gml")]
        public centerOf centerOf
        {
            get
            {
                return this.centerOfField;
            }
            set
            {
                this.centerOfField = value;
            }
        }

        /// <remarks/>
        public FootprintGeographicLocation geographicLocation
        {
            get
            {
                return this.geographicLocationField;
            }
            set
            {
                this.geographicLocationField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opengis.net/gml")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.opengis.net/gml", IsNullable = false)]
    public partial class multiExtentOf {

        private multiExtentOfMultiSurface multiSurfaceField;

        /// <remarks/>
        public multiExtentOfMultiSurface MultiSurface
        {
            get
            {
                return this.multiSurfaceField;
            }
            set
            {
                this.multiSurfaceField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opengis.net/gml")]
    public partial class multiExtentOfMultiSurface {

        private multiExtentOfMultiSurfaceSurfaceMembers surfaceMembersField;

        private string srsNameField;

        /// <remarks/>
        public multiExtentOfMultiSurfaceSurfaceMembers surfaceMembers
        {
            get
            {
                return this.surfaceMembersField;
            }
            set
            {
                this.surfaceMembersField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string srsName
        {
            get
            {
                return this.srsNameField;
            }
            set
            {
                this.srsNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opengis.net/gml")]
    public partial class multiExtentOfMultiSurfaceSurfaceMembers {

        private multiExtentOfMultiSurfaceSurfaceMembersPolygon polygonField;

        /// <remarks/>
        public multiExtentOfMultiSurfaceSurfaceMembersPolygon Polygon
        {
            get
            {
                return this.polygonField;
            }
            set
            {
                this.polygonField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opengis.net/gml")]
    public partial class multiExtentOfMultiSurfaceSurfaceMembersPolygon {

        private multiExtentOfMultiSurfaceSurfaceMembersPolygonExterior exteriorField;

        /// <remarks/>
        public multiExtentOfMultiSurfaceSurfaceMembersPolygonExterior exterior
        {
            get
            {
                return this.exteriorField;
            }
            set
            {
                this.exteriorField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opengis.net/gml")]
    public partial class multiExtentOfMultiSurfaceSurfaceMembersPolygonExterior {

        private multiExtentOfMultiSurfaceSurfaceMembersPolygonExteriorLinearRing linearRingField;

        /// <remarks/>
        public multiExtentOfMultiSurfaceSurfaceMembersPolygonExteriorLinearRing LinearRing
        {
            get
            {
                return this.linearRingField;
            }
            set
            {
                this.linearRingField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opengis.net/gml")]
    public partial class multiExtentOfMultiSurfaceSurfaceMembersPolygonExteriorLinearRing {

        private string posListField;

        /// <remarks/>
        public string posList
        {
            get
            {
                return this.posListField;
            }
            set
            {
                this.posListField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opengis.net/gml")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.opengis.net/gml", IsNullable = false)]
    public partial class centerOf {

        private centerOfPoint pointField;

        /// <remarks/>
        public centerOfPoint Point
        {
            get
            {
                return this.pointField;
            }
            set
            {
                this.pointField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opengis.net/gml")]
    public partial class centerOfPoint {

        private string posField;

        private string srsNameField;

        /// <remarks/>
        public string pos
        {
            get
            {
                return this.posField;
            }
            set
            {
                this.posField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string srsName
        {
            get
            {
                return this.srsNameField;
            }
            set
            {
                this.srsNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.planet.com/products/productMetadataGeocorrected")]
    public partial class FootprintGeographicLocation {

        private FootprintGeographicLocationTopLeft topLeftField;

        private FootprintGeographicLocationTopRight topRightField;

        private FootprintGeographicLocationBottomRight bottomRightField;

        private FootprintGeographicLocationBottomLeft bottomLeftField;

        /// <remarks/>
        public FootprintGeographicLocationTopLeft topLeft
        {
            get
            {
                return this.topLeftField;
            }
            set
            {
                this.topLeftField = value;
            }
        }

        /// <remarks/>
        public FootprintGeographicLocationTopRight topRight
        {
            get
            {
                return this.topRightField;
            }
            set
            {
                this.topRightField = value;
            }
        }

        /// <remarks/>
        public FootprintGeographicLocationBottomRight bottomRight
        {
            get
            {
                return this.bottomRightField;
            }
            set
            {
                this.bottomRightField = value;
            }
        }

        /// <remarks/>
        public FootprintGeographicLocationBottomLeft bottomLeft
        {
            get
            {
                return this.bottomLeftField;
            }
            set
            {
                this.bottomLeftField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.planet.com/products/productMetadataGeocorrected")]
    public partial class FootprintGeographicLocationTopLeft {

        private decimal latitudeField;

        private decimal longitudeField;

        /// <remarks/>
        public decimal latitude
        {
            get
            {
                return this.latitudeField;
            }
            set
            {
                this.latitudeField = value;
            }
        }

        /// <remarks/>
        public decimal longitude
        {
            get
            {
                return this.longitudeField;
            }
            set
            {
                this.longitudeField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.planet.com/products/productMetadataGeocorrected")]
    public partial class FootprintGeographicLocationTopRight {

        private decimal latitudeField;

        private decimal longitudeField;

        /// <remarks/>
        public decimal latitude
        {
            get
            {
                return this.latitudeField;
            }
            set
            {
                this.latitudeField = value;
            }
        }

        /// <remarks/>
        public decimal longitude
        {
            get
            {
                return this.longitudeField;
            }
            set
            {
                this.longitudeField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.planet.com/products/productMetadataGeocorrected")]
    public partial class FootprintGeographicLocationBottomRight {

        private decimal latitudeField;

        private decimal longitudeField;

        /// <remarks/>
        public decimal latitude
        {
            get
            {
                return this.latitudeField;
            }
            set
            {
                this.latitudeField = value;
            }
        }

        /// <remarks/>
        public decimal longitude
        {
            get
            {
                return this.longitudeField;
            }
            set
            {
                this.longitudeField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.planet.com/products/productMetadataGeocorrected")]
    public partial class FootprintGeographicLocationBottomLeft {

        private decimal latitudeField;

        private decimal longitudeField;

        /// <remarks/>
        public decimal latitude
        {
            get
            {
                return this.latitudeField;
            }
            set
            {
                this.latitudeField = value;
            }
        }

        /// <remarks/>
        public decimal longitude
        {
            get
            {
                return this.longitudeField;
            }
            set
            {
                this.longitudeField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opengis.net/gml")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.opengis.net/gml", IsNullable = false)]
    public partial class resultOf {

        private EarthObservationResult earthObservationResultField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://schemas.planet.com/products/productMetadataGeocorrected")]
        public EarthObservationResult EarthObservationResult
        {
            get
            {
                return this.earthObservationResultField;
            }
            set
            {
                this.earthObservationResultField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.planet.com/products/productMetadataGeocorrected")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://schemas.planet.com/products/productMetadataGeocorrected", IsNullable = false)]
    public partial class EarthObservationResult {

        private product productField;

        private mask maskField;

        private cloudCoverPercentage cloudCoverPercentageField;

        private string cloudCoverPercentageQuotationModeField;

        private EarthObservationResultUnusableDataPercentage unusableDataPercentageField;

        private EarthObservationResultBandSpecificMetadata[] bandSpecificMetadataField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://earth.esa.int/eop")]
        public product product
        {
            get
            {
                return this.productField;
            }
            set
            {
                this.productField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://earth.esa.int/eop")]
        public mask mask
        {
            get
            {
                return this.maskField;
            }
            set
            {
                this.maskField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://earth.esa.int/opt")]
        public cloudCoverPercentage cloudCoverPercentage
        {
            get
            {
                return this.cloudCoverPercentageField;
            }
            set
            {
                this.cloudCoverPercentageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://earth.esa.int/opt")]
        public string cloudCoverPercentageQuotationMode
        {
            get
            {
                return this.cloudCoverPercentageQuotationModeField;
            }
            set
            {
                this.cloudCoverPercentageQuotationModeField = value;
            }
        }

        /// <remarks/>
        public EarthObservationResultUnusableDataPercentage unusableDataPercentage
        {
            get
            {
                return this.unusableDataPercentageField;
            }
            set
            {
                this.unusableDataPercentageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("bandSpecificMetadata")]
        public EarthObservationResultBandSpecificMetadata[] bandSpecificMetadata
        {
            get
            {
                return this.bandSpecificMetadataField;
            }
            set
            {
                this.bandSpecificMetadataField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://earth.esa.int/eop")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://earth.esa.int/eop", IsNullable = false)]
    public partial class product {

        private ProductInformation productInformationField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://schemas.planet.com/products/productMetadataGeocorrected")]
        public ProductInformation ProductInformation
        {
            get
            {
                return this.productInformationField;
            }
            set
            {
                this.productInformationField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.planet.com/products/productMetadataGeocorrected")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://schemas.planet.com/products/productMetadataGeocorrected", IsNullable = false)]
    public partial class ProductInformation {

        private string fileNameField;

        private size sizeField;

        private string productFormatField;

        private ProductInformationSpatialReferenceSystem spatialReferenceSystemField;

        private string resamplingKernelField;

        private ushort numRowsField;

        private ushort numColumnsField;

        private byte numBandsField;

        private byte rowGsdField;

        private byte columnGsdField;

        private bool radiometricCorrectionAppliedField;

        private string geoCorrectionLevelField;

        private string elevationCorrectionAppliedField;

        private bool atmosphericCorrectionAppliedField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://earth.esa.int/eop")]
        public string fileName
        {
            get
            {
                return this.fileNameField;
            }
            set
            {
                this.fileNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://earth.esa.int/eop")]
        public size size
        {
            get
            {
                return this.sizeField;
            }
            set
            {
                this.sizeField = value;
            }
        }

        /// <remarks/>
        public string productFormat
        {
            get
            {
                return this.productFormatField;
            }
            set
            {
                this.productFormatField = value;
            }
        }

        /// <remarks/>
        public ProductInformationSpatialReferenceSystem spatialReferenceSystem
        {
            get
            {
                return this.spatialReferenceSystemField;
            }
            set
            {
                this.spatialReferenceSystemField = value;
            }
        }

        /// <remarks/>
        public string resamplingKernel
        {
            get
            {
                return this.resamplingKernelField;
            }
            set
            {
                this.resamplingKernelField = value;
            }
        }

        /// <remarks/>
        public ushort numRows
        {
            get
            {
                return this.numRowsField;
            }
            set
            {
                this.numRowsField = value;
            }
        }

        /// <remarks/>
        public ushort numColumns
        {
            get
            {
                return this.numColumnsField;
            }
            set
            {
                this.numColumnsField = value;
            }
        }

        /// <remarks/>
        public byte numBands
        {
            get
            {
                return this.numBandsField;
            }
            set
            {
                this.numBandsField = value;
            }
        }

        /// <remarks/>
        public byte rowGsd
        {
            get
            {
                return this.rowGsdField;
            }
            set
            {
                this.rowGsdField = value;
            }
        }

        /// <remarks/>
        public byte columnGsd
        {
            get
            {
                return this.columnGsdField;
            }
            set
            {
                this.columnGsdField = value;
            }
        }

        /// <remarks/>
        public bool radiometricCorrectionApplied
        {
            get
            {
                return this.radiometricCorrectionAppliedField;
            }
            set
            {
                this.radiometricCorrectionAppliedField = value;
            }
        }

        /// <remarks/>
        public string geoCorrectionLevel
        {
            get
            {
                return this.geoCorrectionLevelField;
            }
            set
            {
                this.geoCorrectionLevelField = value;
            }
        }

        /// <remarks/>
        public string elevationCorrectionApplied
        {
            get
            {
                return this.elevationCorrectionAppliedField;
            }
            set
            {
                this.elevationCorrectionAppliedField = value;
            }
        }

        /// <remarks/>
        public bool atmosphericCorrectionApplied
        {
            get
            {
                return this.atmosphericCorrectionAppliedField;
            }
            set
            {
                this.atmosphericCorrectionAppliedField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://earth.esa.int/eop")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://earth.esa.int/eop", IsNullable = false)]
    public partial class size {

        private string uomField;

        private uint valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string uom
        {
            get
            {
                return this.uomField;
            }
            set
            {
                this.uomField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public uint Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.planet.com/products/productMetadataGeocorrected")]
    public partial class ProductInformationSpatialReferenceSystem {

        private ushort epsgCodeField;

        private string geodeticDatumField;

        private string projectionField;

        private byte projectionZoneField;

        /// <remarks/>
        public ushort epsgCode
        {
            get
            {
                return this.epsgCodeField;
            }
            set
            {
                this.epsgCodeField = value;
            }
        }

        /// <remarks/>
        public string geodeticDatum
        {
            get
            {
                return this.geodeticDatumField;
            }
            set
            {
                this.geodeticDatumField = value;
            }
        }

        /// <remarks/>
        public string projection
        {
            get
            {
                return this.projectionField;
            }
            set
            {
                this.projectionField = value;
            }
        }

        /// <remarks/>
        public byte projectionZone
        {
            get
            {
                return this.projectionZoneField;
            }
            set
            {
                this.projectionZoneField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://earth.esa.int/eop")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://earth.esa.int/eop", IsNullable = false)]
    public partial class mask {

        private maskMaskInformation maskInformationField;

        /// <remarks/>
        public maskMaskInformation MaskInformation
        {
            get
            {
                return this.maskInformationField;
            }
            set
            {
                this.maskInformationField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://earth.esa.int/eop")]
    public partial class maskMaskInformation {

        private string typeField;

        private string formatField;

        private maskMaskInformationReferenceSystemIdentifier referenceSystemIdentifierField;

        private string fileNameField;

        /// <remarks/>
        public string type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }

        /// <remarks/>
        public string format
        {
            get
            {
                return this.formatField;
            }
            set
            {
                this.formatField = value;
            }
        }

        /// <remarks/>
        public maskMaskInformationReferenceSystemIdentifier referenceSystemIdentifier
        {
            get
            {
                return this.referenceSystemIdentifierField;
            }
            set
            {
                this.referenceSystemIdentifierField = value;
            }
        }

        /// <remarks/>
        public string fileName
        {
            get
            {
                return this.fileNameField;
            }
            set
            {
                this.fileNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://earth.esa.int/eop")]
    public partial class maskMaskInformationReferenceSystemIdentifier {

        private string codeSpaceField;

        private ushort valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string codeSpace
        {
            get
            {
                return this.codeSpaceField;
            }
            set
            {
                this.codeSpaceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public ushort Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://earth.esa.int/opt")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://earth.esa.int/opt", IsNullable = false)]
    public partial class cloudCoverPercentage {

        private string uomField;

        private decimal valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string uom
        {
            get
            {
                return this.uomField;
            }
            set
            {
                this.uomField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public decimal Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.planet.com/products/productMetadataGeocorrected")]
    public partial class EarthObservationResultUnusableDataPercentage {

        private string uomField;

        private decimal valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string uom
        {
            get
            {
                return this.uomField;
            }
            set
            {
                this.uomField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public decimal Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.planet.com/products/productMetadataGeocorrected")]
    public partial class EarthObservationResultBandSpecificMetadata {

        private byte bandNumberField;

        private byte percentMissingLinesField;

        private byte percentSuspectLinesField;

        private string binningField;

        private byte shiftingField;

        private byte maskingField;

        private double radiometricScaleFactorField;

        /// <remarks/>
        public byte bandNumber
        {
            get
            {
                return this.bandNumberField;
            }
            set
            {
                this.bandNumberField = value;
            }
        }

        /// <remarks/>
        public byte percentMissingLines
        {
            get
            {
                return this.percentMissingLinesField;
            }
            set
            {
                this.percentMissingLinesField = value;
            }
        }

        /// <remarks/>
        public byte percentSuspectLines
        {
            get
            {
                return this.percentSuspectLinesField;
            }
            set
            {
                this.percentSuspectLinesField = value;
            }
        }

        /// <remarks/>
        public string binning
        {
            get
            {
                return this.binningField;
            }
            set
            {
                this.binningField = value;
            }
        }

        /// <remarks/>
        public byte shifting
        {
            get
            {
                return this.shiftingField;
            }
            set
            {
                this.shiftingField = value;
            }
        }

        /// <remarks/>
        public byte masking
        {
            get
            {
                return this.maskingField;
            }
            set
            {
                this.maskingField = value;
            }
        }

        /// <remarks/>
        public double radiometricScaleFactor
        {
            get
            {
                return this.radiometricScaleFactorField;
            }
            set
            {
                this.radiometricScaleFactorField = value;
            }
        }
    }
}


