﻿using ftmc = FieldToMarket.Contracts;
using FieldToMarket.Contracts.Partial;
using FieldToMarket.Contracts.Reference;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Client.Services;
using Landdb.Client.Spatial;
using Landdb.Infrastructure.Messages;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Infrastructure;
using Landdb.Resources;

namespace Landdb.ViewModel.Analysis.FieldToMarket {
    public class FtmDetailsInputsViewModel : ViewModelBase {
        IClientEndpoint clientEndpoint;
        Dispatcher dispatcher;
        SustainabilityServices sustainabilityServices;
        SustainabilityServiceClient sustainabilityClient;
        FieldDetails latestDetailsDocument;
        ValidationSummary validationSummary;
        FieldInputs latestInputDocument;
        Point? latestCenterPoint = null;
        bool hasMessages = false;
        bool hasBlockingMessages;
        Action notifyOfChanges;

        public FtmDetailsInputsViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, SustainabilityServices sustainabilityServices, SustainabilityServiceClient sustainabilityClient, Action notifyOfChanges = null) {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            this.sustainabilityServices = sustainabilityServices;
            this.sustainabilityClient = sustainabilityClient;
            this.notifyOfChanges = notifyOfChanges;

            latestDetailsDocument = new FieldDetails();
            Messages = new ObservableCollection<FtmMessageViewModel>();

            PromptForIrrigationAmountCommand = new RelayCommand(PromptForIrrigationInformation);
            PromptForEstimatedYieldCommand = new AwaitableRelayCommand(PromptForEstimatedYield);
            PromptForDryingCommand = new AwaitableRelayCommand(PromptForDrying);
            PromptForUsleInputsCommand = new AwaitableRelayCommand(PromptForUsleInputs);
            PromptForRusle2TillagePracticeCommand = new AwaitableRelayCommand(PromptForRusle2TillagePractice);

            AddDeliveryCommand = new AwaitableRelayCommand(AddDeliveryItem);
            RemoveDeliveryCommand = new AwaitableRelayCommand(RemoveDeliveryItem);
            EditDeliveryCommand = new AwaitableRelayCommand(UpdateDeliveryItem);
        }

        public ICommand PromptForIrrigationAmountCommand { get; private set; }
        public ICommand PromptForEstimatedYieldCommand { get; private set; }
        public ICommand PromptForDryingCommand { get; private set; }
        public ICommand PromptForUsleInputsCommand { get; private set; }
        public ICommand PromptForRusle2TillagePracticeCommand { get; private set; }

        public ICommand AddDeliveryCommand { get; private set; }
        public ICommand RemoveDeliveryCommand { get; private set; }
        public ICommand EditDeliveryCommand { get; private set; }

        FtmId latestFtmId;

        public FieldInputs LatestInputDocument {
            get { return latestInputDocument ?? new FieldInputs(); }
        }

        FtmCustomDataItem customData;

        public bool HasMessages {
            get { return hasMessages; }
            private set {
                hasMessages = value;
                RaisePropertyChanged(nameof(HasMessages));
                RaisePropertyChanged(nameof(MessageCount));
            }
        }

        public bool HasBlockingMessages {
            get { return hasBlockingMessages; }
            private set {
                hasBlockingMessages = value;
                RaisePropertyChanged(nameof(HasMessages));
                RaisePropertyChanged(nameof(MessageCount));
            }
        }

        public int MessageCount {
            get {
                int messageCount = Messages != null ? Messages.Count : 0;
                messageCount += ValidationSummary?.ValidationWarnings != null ? ValidationSummary.ValidationWarnings.Count : 0;
                
                return messageCount;
            }
        }

        public ObservableCollection<FtmMessageViewModel> Messages {
            get;
            private set;
        }

        public ValidationSummary ValidationSummary {
            get { return validationSummary; }
            set {
                validationSummary = value;
                RaisePropertyChanged(nameof(ValidationSummary));
            }
        }

        internal async Task RefreshFromDetails(FieldDetails details, CropZoneId cropZoneId, Crop crop, ValidationSummary validationSummary) {
            latestDetailsDocument = details ?? new FieldDetails();
            ValidationSummary = validationSummary;
            latestInputDocument = latestDetailsDocument.Inputs ?? new FieldInputs();
            latestFtmId = new FtmId() { Crop = details.Registration.Crop, ClientId = details.Registration.Id.ClientId, FieldId = details.Registration.Id.FieldId };

            IsRusle2 = crop.CalculationMethod == CalculationMethod.RUSLE2;

            if (IsRusle2) {
                var mapItem = clientEndpoint.GetView<Landdb.Domain.ReadModels.Map.ItemMap>(cropZoneId);
                if (mapItem.HasValue && mapItem.Value.MostRecentMapItem != null) {
                    latestCenterPoint = await dispatcher.InvokeAsync<Point?>(() => { // Have to execute this on the dispatcher because it requires a STA thread...
                        var point = GeometryFromWKT.GetCenterPoint(mapItem.Value.MostRecentMapItem.MapData);
                        return point;
                    });
                } else { latestCenterPoint = null; }
            } else {
                latestCenterPoint = null;
            }
            
            DeliveryItems = new ObservableCollection<Client.Services.DeliveryItem>(latestInputDocument.DeliveryItems ?? new List<Client.Services.DeliveryItem>());

            if (latestInputDocument != null && !string.IsNullOrWhiteSpace(latestInputDocument.CustomData)) {
                customData = JsonConvert.DeserializeObject<FtmCustomDataItem>(latestInputDocument.CustomData);
                if (customData != null && customData.SugarPercentage.HasValue) {
                    CustomParameterLabel = "SUGAR";
                    CustomParameterDisplay = customData.SugarPercentage.Value.ToString("p");
                    HasCustomParameter = true;
                } else if (customData != null && customData.ProteinPercentage.HasValue) {
                    CustomParameterLabel = "PROTEIN";
                    CustomParameterDisplay = customData.ProteinPercentage.Value.ToString("p");
                    HasCustomParameter = true;
                }
            }

            var messages = await GenerateMessages(latestFtmId, latestInputDocument);

            await dispatcher.BeginInvoke(new Action(() => {
                Messages.Clear();

                foreach (var message in messages) {
                    Messages.Add(message);
                }
                
                HasMessages = Messages.Any() || (ValidationSummary ?? new ValidationSummary()).HasValidationWarnings;
                HasBlockingMessages = Messages.Any();
                RaiseAllPropertiesChanged();
            }));

            RaiseAllPropertiesChanged();
        }

        void RaiseAllPropertiesChanged() {
            RaisePropertyChanged(nameof(Area));
            RaisePropertyChanged(nameof(IsBoundarySet));
            RaisePropertyChanged(nameof(Yield));
            RaisePropertyChanged(nameof(NrcsTillageManagement));
            RaisePropertyChanged(nameof(SlopeDisplay));
            RaisePropertyChanged(nameof(IsIrrigated));
            RaisePropertyChanged(nameof(PumpDepth));
            RaisePropertyChanged(nameof(PumpPressure));
            RaisePropertyChanged(nameof(IrrigationAmount));
            RaisePropertyChanged(nameof(DryingAmount));
            RaisePropertyChanged(nameof(DryerType));
            RaisePropertyChanged(nameof(DeliveryItems));

            RaisePropertyChanged(nameof(PreviousCrop));
            RaisePropertyChanged(nameof(WindSeverity));
            RaisePropertyChanged(nameof(OperationalDieselUseValue));
            RaisePropertyChanged(nameof(SoilTexture));
            RaisePropertyChanged(nameof(TillageSystem));
            RaisePropertyChanged(nameof(ConservationPractices));

            RaisePropertyChanged(nameof(IsYieldEstimated));

            RaisePropertyChanged(nameof(HasCustomParameter));
            RaisePropertyChanged(nameof(CustomParameterLabel));
            RaisePropertyChanged(nameof(CustomParameterDisplay));

            RaisePropertyChanged(nameof(CanAddDelivery));
        }

        async Task<IList<FtmMessageViewModel>> GenerateMessages(FtmId ftmId, FieldInputs input) {
            List<FtmMessageViewModel> messages = new List<FtmMessageViewModel>();

            if (input.Yield == null || input.Yield <= 0) { 
                messages.Add(new FtmMessageViewModel(ftmId, Strings.Yield_Text, Strings.NoYieldHasBeenSpecified_Text, () => { }, true)); 
            }

            if (IsRusle2) {
                if (string.IsNullOrWhiteSpace(input.NRCS_TillageManagementProfile)) {
                    messages.Add(new FtmMessageViewModel(ftmId, Strings.NrcsTillageManagement_Text, Strings.TillageManagementProfileNotSet_Text, () => { }, true));
                }
            } else {
                if (input.USLE_DieselUsePerArea <= 0) {
                    messages.Add(new FtmMessageViewModel(ftmId, Strings.OperationalDieselUseNotSpecified_Text, Strings.NoOperationalDieselUseSpecified_Text, () => { }, true));
                }
                if (string.IsNullOrWhiteSpace(input.USLE_PreviousCrop)) {
                    messages.Add(new FtmMessageViewModel(ftmId, Strings.PreviousCropNotSpecified_Text, Strings.NoPreviousCropHasBeenSpecified_Text, () => { }, true));
                }
                if (string.IsNullOrWhiteSpace(input.USLE_SoilTexture)) {
                    messages.Add(new FtmMessageViewModel(ftmId, Strings.SoilTextureNotSpecified_Text, Strings.NoSoilTextureSpecified_Text, () => { }, true));
                }
                if (string.IsNullOrWhiteSpace(input.USLE_TillageSystem)) {
                    messages.Add(new FtmMessageViewModel(ftmId, Strings.TillageSystemNotSpecified_Text, Strings.NoTillageSystemSpecified_Text, () => { }, true));
                }
                if (string.IsNullOrWhiteSpace(input.USLE_WindErosionSeverity)) {
                    messages.Add(new FtmMessageViewModel(ftmId, Strings.WindSeverityNotSpecified_Text, Strings.NoWindSeveritySpecified_Text, () => { }, true));
                }
            }

            if (!input.IsIrrigated.HasValue || (input.IsIrrigated.GetValueOrDefault() && (!input.PumpDepth.HasValue || !input.PumpPressure.HasValue))) {
                messages.Add(new FtmMessageViewModel(ftmId, Strings.IrrigationInformationIsIncomplete_Text, Strings.IrrigationInformationNotSpecified_Text, null, true)); 
            }

            if (input.IsIrrigated.GetValueOrDefault() && (!input.IrrigationAmount.HasValue || input.IrrigationAmount <= 0)) {
                messages.Add(new FtmMessageViewModel(ftmId, Strings.NoIrrigationAmountDetected_Text, Strings.NoIrrigationAmountFound_Text, PromptForIrrigationInformation, true));
            }

            if (!input.SlopeGrade.HasValue || !input.SlopeLength.HasValue) {
                messages.Add(new FtmMessageViewModel(ftmId, Strings.SlopeInformationNotSpecified_Text, Strings.NoSlopeInformationSet_Text, null, true)); 
            }

            if (string.IsNullOrWhiteSpace(input.State)) {
                messages.Add(new FtmMessageViewModel(ftmId, Strings.StateNotSpecified_Text, Strings.StateNotSpecifiedPleaseEnterInfo_Text, null, true)); 
            }

            if (string.IsNullOrWhiteSpace(input.BoundaryWkt)) {
                messages.Add(new FtmMessageViewModel(ftmId, Strings.NoBoundaryExists_Text, Strings.FieldDoesNotHaveBoundary_Text, null, true)); 
            }

            return messages;
        }

        public bool isRusle2;
        public bool IsRusle2 {
            get { return isRusle2; }
            set {
                isRusle2 = value;
                RaisePropertyChanged(() => IsRusle2);
            }
        }

        bool isYieldEstimated;
        public bool IsYieldEstimated {
            get { return isYieldEstimated; }
            set { 
                isYieldEstimated = value;
                RaisePropertyChanged(() => IsYieldEstimated);
            }
        }

        private bool hasCustomParameter;
        public bool HasCustomParameter {
            get { return hasCustomParameter; }
            set { 
                hasCustomParameter = value;
                RaisePropertyChanged(() => HasCustomParameter);
            }
        }

        private string customParameterLabel;
        public string CustomParameterLabel {
            get { return customParameterLabel; }
            set { 
                customParameterLabel = value;
                RaisePropertyChanged(() => CustomParameterLabel);
            }
        }

        private string customParameterDisplay;
        public string CustomParameterDisplay {
            get { return customParameterDisplay; }
            set {
                customParameterDisplay = value;
                RaisePropertyChanged(() => CustomParameterDisplay);
            }
        }

        public double? Area {
            get { return LatestInputDocument.Area; }
        }

        public bool IsBoundarySet {
            get { return !string.IsNullOrWhiteSpace(LatestInputDocument.BoundaryWkt); }
        }

        public double? Yield {
            get { return LatestInputDocument.Yield; }
        }

        public string NrcsTillageManagement {
            get { return LatestInputDocument.NRCS_TillageManagementProfile; }
        }

        #region USLE
        public string PreviousCrop {
            get { return LatestInputDocument.USLE_PreviousCrop; }
        }

        public string WindSeverity {
            get { return LatestInputDocument.USLE_WindErosionSeverity; }
        }

        public double OperationalDieselUseValue {
            get { return LatestInputDocument.USLE_DieselUsePerArea.GetValueOrDefault(); }
        }

        public string SoilTexture {
            get { return LatestInputDocument.USLE_SoilTexture; }
        }

        public string TillageSystem {
            get { return LatestInputDocument.USLE_TillageSystem; }
        }

        public List<string> ConservationPractices {
            get { return LatestInputDocument.USLE_ConservationPractices; }
        }
        #endregion

        public string SlopeDisplay {
            get {
                if (LatestInputDocument.SlopeLength == null || LatestInputDocument.SlopeGrade == null) { return "not set"; }
                return string.Format(Strings.SlopePerLength_Text, LatestInputDocument.SlopeGrade, LatestInputDocument.SlopeLength); 
            }
        }

        public bool? IsIrrigated {
            get { return LatestInputDocument.IsIrrigated; }
        }

        public double? PumpDepth {
            get { return LatestInputDocument.PumpDepth; }
        }

        public double? PumpPressure {
            get { return LatestInputDocument.PumpPressure; }
        }

        public double? IrrigationAmount {
            get { return LatestInputDocument.IrrigationAmount; }
        }

        public string DryingAmount {
            get {
                if (LatestInputDocument.PointsOfMoistureToRemove == null || LatestInputDocument.PercentOfYieldToDry == null) { return "not set"; }
                return string.Format(Strings.PointsOfMoistureFromPercentYield_Text, LatestInputDocument.PointsOfMoistureToRemove.GetValueOrDefault() * 100, LatestInputDocument.PercentOfYieldToDry);
            }
        }

        public string DryerType {
            get {
                return LatestInputDocument.DryerType;
            }
        }

        public ObservableCollection<Client.Services.DeliveryItem> DeliveryItems {
            get; private set; 
        }

        public Client.Services.DeliveryItem SelectedDeliveryItem {
            get { return DeliveryItems == null ? null : DeliveryItems.FirstOrDefault(); }
        }

        void PromptForIrrigationInformation() {
            FtmId ftmId = new FtmId() { Crop = latestDetailsDocument.Registration.Crop, ClientId = latestDetailsDocument.Registration.Id.ClientId, FieldId = latestDetailsDocument.Registration.Id.FieldId };
            var irrigationInputs = new IrrigationInformation() {
                IsIrrigated = LatestInputDocument.IsIrrigated.GetValueOrDefault(),
                IrrigationAmount = LatestInputDocument.IrrigationAmount,
                PumpDepth = LatestInputDocument.PumpDepth,
                PumpPressure = LatestInputDocument.PumpPressure,
                IrrigationSystem = LatestInputDocument.IrrigationSystem,
                WaterOrigin = LatestInputDocument.WaterOrigin,
                WaterSource = LatestInputDocument.WaterSource,
                PumpEnergySource = LatestInputDocument.PumpEnergySource
            };

            var vm = new FtmSubmitIrrigationViewModel(clientEndpoint, dispatcher, sustainabilityServices, sustainabilityClient, ftmId, irrigationInputs, () => { 
                Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
                if (notifyOfChanges != null) { notifyOfChanges(); }
            });
            ScreenDescriptor sd = new ScreenDescriptor("Landdb.Views.Analysis.FieldToMarket.FtmSubmitIrrigationView", "submitIrrigationDialog", vm);
            Messenger.Default.Send<ShowPopupMessage>(new ShowPopupMessage() { ScreenDescriptor = sd });
        }

        async Task PromptForDrying() {
            FtmId ftmId = new FtmId() { Crop = latestDetailsDocument.Registration.Crop, ClientId = latestDetailsDocument.Registration.Id.ClientId, FieldId = latestDetailsDocument.Registration.Id.FieldId };
            var dryingInfo = new DryingInformation() {
                DryerType = LatestInputDocument.DryerType,
                PercentOfYieldToDry = LatestInputDocument.PercentOfYieldToDry.GetValueOrDefault(),
                PointsOfMoistureToRemove = LatestInputDocument.PointsOfMoistureToRemove.GetValueOrDefault()
            };

            var vm = await FtmSubmitDryingViewModel.Create(clientEndpoint, dispatcher, sustainabilityServices, sustainabilityClient, ftmId, dryingInfo, () => {
                Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
                notifyOfChanges?.Invoke();
            });
            ScreenDescriptor sd = new ScreenDescriptor("Landdb.Views.Analysis.FieldToMarket.FtmSubmitDryingView", "submitDryingDialog", vm);
            Messenger.Default.Send<ShowPopupMessage>(new ShowPopupMessage() { ScreenDescriptor = sd });
        }

        async Task PromptForEstimatedYield() {
            FtmId ftmId = new FtmId() { Crop = latestDetailsDocument.Registration.Crop, ClientId = latestDetailsDocument.Registration.Id.ClientId, FieldId = latestDetailsDocument.Registration.Id.FieldId };

            var vm = await FtmSetEstimatedYieldViewModel.Create(clientEndpoint, dispatcher, sustainabilityServices, sustainabilityClient, ftmId, latestDetailsDocument.Inputs.Yield.GetValueOrDefault(), () => {
                Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
                notifyOfChanges?.Invoke();
            });
            ScreenDescriptor sd = new ScreenDescriptor("Landdb.Views.Analysis.FieldToMarket.FtmSetEstimatedYieldView", "setFtmYieldDialog", vm);
            Messenger.Default.Send<ShowPopupMessage>(new ShowPopupMessage() { ScreenDescriptor = sd });
        }

        async Task PromptForUsleInputs() {
            FtmId ftmId = new FtmId() { Crop = latestDetailsDocument.Registration.Crop, ClientId = latestDetailsDocument.Registration.Id.ClientId, FieldId = latestDetailsDocument.Registration.Id.FieldId };

            var vm = await FtmSubmitUsleInputsViewModel.Create(clientEndpoint, dispatcher, sustainabilityServices, sustainabilityClient, ftmId, latestDetailsDocument.Inputs, () => {
                Messenger.Default.Send(new HidePopupMessage());
                notifyOfChanges?.Invoke();
            });
            ScreenDescriptor sd = new ScreenDescriptor("Landdb.Views.Analysis.FieldToMarket.FtmSubmitUsleInputsView", "submitUsleInputsDialog", vm);
            Messenger.Default.Send<ShowPopupMessage>(new ShowPopupMessage() { ScreenDescriptor = sd });
        }

        async Task PromptForRusle2TillagePractice() {
            FtmId ftmId = new FtmId() { Crop = latestDetailsDocument.Registration.Crop, ClientId = latestDetailsDocument.Registration.Id.ClientId, FieldId = latestDetailsDocument.Registration.Id.FieldId };

            var vm = await FtmSubmitNrcsTillageManagementViewModel.Create(clientEndpoint, dispatcher, sustainabilityServices, sustainabilityClient, ftmId, latestCenterPoint, latestDetailsDocument.Inputs.NRCS_TillageManagementProfile, () => {
                Messenger.Default.Send(new HidePopupMessage());
                notifyOfChanges?.Invoke();
            });
            ScreenDescriptor sd = new ScreenDescriptor("Landdb.Views.Analysis.FieldToMarket.FtmSubmitNrcsTillageManagementView", "submitRusle2InputsDialog", vm);
            Messenger.Default.Send(new ShowPopupMessage() { ScreenDescriptor = sd });
        }

        public bool CanAddDelivery {
            get { return DeliveryItems == null || !DeliveryItems.Any(); }
        }

        async Task AddDeliveryItem() {
            var fuelTypes = await sustainabilityServices.GetFuelTypes();
            var FuelTypes = new ObservableCollection<FuelType>(fuelTypes);

            Landdb.ViewModel.Secondary.Analysis.FieldToMarket.DeliveryItemViewModel vm = new Landdb.ViewModel.Secondary.Analysis.FieldToMarket.DeliveryItemViewModel(FuelTypes, string.Empty, async (item, complete) => {
                if (complete && item.IsValid) {
                    DeliveryItems.Add(new Client.Services.DeliveryItem() { AverageMilesPerGallon = item.AverageMilesPerGallon, FuelType = item.SelectedFuelType.FuelName, TotalTransportDistance = Math.Ceiling((latestInputDocument.Yield.GetValueOrDefault() * latestInputDocument.Area.GetValueOrDefault()) / item.LoadSize) * item.RoundtripDistance });

                    if (customData == null) { customData = new FtmCustomDataItem(); }
                    if (customData.AdditionalDeliveryInfo == null) { customData.AdditionalDeliveryInfo = new Dictionary<int, FtmDeliveryAdditionalData>(); }

                    customData.AdditionalDeliveryInfo.Add(DeliveryItems.Count - 1, new FtmDeliveryAdditionalData() { LoadSize = item.LoadSize, LoadUnit = item.LoadUnit, RoundtripDistance = item.RoundtripDistance });
                    await sustainabilityClient.SetDeliveries(latestFtmId.ClientId, latestFtmId.Crop, latestFtmId.FieldId, DeliveryItems);
                    await sustainabilityClient.SetCustomData(latestFtmId.ClientId, latestFtmId.Crop, latestFtmId.FieldId, JsonConvert.SerializeObject(customData));
                    await dispatcher.InvokeAsync(() => RaisePropertyChanged(() => DeliveryItems));
                    await dispatcher.InvokeAsync(() => RaisePropertyChanged(() => CanAddDelivery));
                }
            });
            ScreenDescriptor sd = new ScreenDescriptor("Landdb.Views.Secondary.Analysis.FieldToMarket.FtmAddDeliveryItem", "addDelivery", vm);
            Messenger.Default.Send<ShowPopupMessage>(new ShowPopupMessage() { ScreenDescriptor = sd });
            
        }

        async Task UpdateDeliveryItem() {
            if (SelectedDeliveryItem == null) { return; }

            var fuelTypes = await sustainabilityServices.GetFuelTypes();
            var FuelTypes = new ObservableCollection<FuelType>(fuelTypes);

            if (customData == null) { customData = new FtmCustomDataItem(); }
            if (customData.AdditionalDeliveryInfo == null) { customData.AdditionalDeliveryInfo = new Dictionary<int, FtmDeliveryAdditionalData>(); }

            var diIndex = DeliveryItems.IndexOf(SelectedDeliveryItem);
            FtmDeliveryAdditionalData additionalDeliveryInfo = diIndex > -1 && customData.AdditionalDeliveryInfo.ContainsKey(diIndex) ? customData.AdditionalDeliveryInfo[diIndex] : null;

            Landdb.ViewModel.Secondary.Analysis.FieldToMarket.DeliveryItemViewModel vm = new Landdb.ViewModel.Secondary.Analysis.FieldToMarket.DeliveryItemViewModel(FuelTypes, string.Empty, async (item, complete) => {
                if (complete && item.IsValid && diIndex > -1) {
                    DeliveryItems[diIndex].AverageMilesPerGallon = item.AverageMilesPerGallon;
                    DeliveryItems[diIndex].FuelType = item.SelectedFuelType.FuelName;
                    DeliveryItems[diIndex].TotalTransportDistance = Math.Ceiling((latestInputDocument.Yield.GetValueOrDefault() * latestInputDocument.Area.GetValueOrDefault()) / item.LoadSize) * item.RoundtripDistance;
                    
                    if(additionalDeliveryInfo == null) {
                        additionalDeliveryInfo = new FtmDeliveryAdditionalData();
                        customData.AdditionalDeliveryInfo.Add(diIndex, additionalDeliveryInfo);
                    }

                    additionalDeliveryInfo.LoadSize = item.LoadSize;
                    additionalDeliveryInfo.LoadUnit = item.LoadUnit;
                    additionalDeliveryInfo.RoundtripDistance = item.RoundtripDistance;

                    await sustainabilityClient.SetDeliveries(latestFtmId.ClientId, latestFtmId.Crop, latestFtmId.FieldId, DeliveryItems);
                    await sustainabilityClient.SetCustomData(latestFtmId.ClientId, latestFtmId.Crop, latestFtmId.FieldId, JsonConvert.SerializeObject(customData));

                    await dispatcher.InvokeAsync(() => RaisePropertyChanged(() => DeliveryItems));
                    await dispatcher.InvokeAsync(() => RaisePropertyChanged(() => CanAddDelivery));
                }
            });

            vm.AverageMilesPerGallon = SelectedDeliveryItem.AverageMilesPerGallon;
            vm.SelectedFuelType = vm.AvailableFuelTypes.Where(x => x.FuelName == SelectedDeliveryItem.FuelType).FirstOrDefault();

            if (additionalDeliveryInfo != null) {
                var di = customData.AdditionalDeliveryInfo[diIndex];
                vm.LoadSize = di.LoadSize;
                vm.LoadUnit = di.LoadUnit;
                vm.RoundtripDistance = di.RoundtripDistance;
            }

            ScreenDescriptor sd = new ScreenDescriptor("Landdb.Views.Secondary.Analysis.FieldToMarket.FtmAddDeliveryItem", "editDelivery", vm);
            Messenger.Default.Send<ShowPopupMessage>(new ShowPopupMessage() { ScreenDescriptor = sd });
        }

        async Task RemoveDeliveryItem() {
            if (SelectedDeliveryItem == null) { return; }

            if (customData == null) { customData = new FtmCustomDataItem(); }
            if (customData.AdditionalDeliveryInfo == null) { customData.AdditionalDeliveryInfo = new Dictionary<int, FtmDeliveryAdditionalData>(); }

            var diIndex = DeliveryItems.IndexOf(SelectedDeliveryItem);

            DeliveryItems.Remove(SelectedDeliveryItem);
            if(customData.AdditionalDeliveryInfo.ContainsKey(diIndex)) {
                customData.AdditionalDeliveryInfo.Remove(diIndex);
            }

            await dispatcher.InvokeAsync(() => RaisePropertyChanged(() => CanAddDelivery));
            await sustainabilityClient.SetDeliveries(latestFtmId.ClientId, latestFtmId.Crop, latestFtmId.FieldId, DeliveryItems);
            await sustainabilityClient.SetCustomData(latestFtmId.ClientId, latestFtmId.Crop, latestFtmId.FieldId, JsonConvert.SerializeObject(customData));
        }
    }

    public class FtmMessageViewModel : ViewModelBase {
        string title;
        string message;
        FtmId ftmId;

        public FtmMessageViewModel(FtmId ftmId, string title, string message, Action action, bool isError = false) {
            this.title = title;
            this.message = message;
            this.ftmId = ftmId;
            this.IsError = isError;

            if (action != null) {
                this.PerformActionCommand = new RelayCommand(action);
            }
        }

        public string Title {
            get { return title; }
            set {
                title = value;
                RaisePropertyChanged(() => Title);
            }
        }

        public string Message {
            get { return message; }
            set {
                message = value;
                RaisePropertyChanged(() => Message);
            }
        }

        public bool HasCommand {
            get { return PerformActionCommand != null; }
        }

        public bool IsError { get; set; }

        public ICommand PerformActionCommand {
            get;
            private set;
        }
    }
}
