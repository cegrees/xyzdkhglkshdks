﻿using FieldToMarket.Contracts.Partial;
using FieldToMarket.Contracts.Reference;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using Landdb.Client.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;

namespace Landdb.ViewModel.Analysis.FieldToMarket {
    public class FtmSubmitDryingViewModel : ViewModelBase {
        IClientEndpoint clientEndpoint;
        Dispatcher dispatcher;
        SustainabilityServices sustainabilityServices;
        SustainabilityServiceClient sustainabilityClient;
        ObservableCollection<DryingSystemItemViewModel> dryingSystems;
        ObservableCollection<FuelType> fuelTypes;
        FtmId ftmId;
        DryingInformation currentDryingInformation;
        Action onViewClose;

        DryingSystemItemViewModel selectedDryingSystem;
        double percentMoistureToRemove;
        double percentYieldToDry;

        private FtmSubmitDryingViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, SustainabilityServices sustainabilityServices, SustainabilityServiceClient sustainabilityClient, IEnumerable<DryingSystem> dryingSystems, IEnumerable<FuelType> fuels, FtmId ftmId, DryingInformation currentDryingInformation, Action handleViewClose) {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            this.sustainabilityServices = sustainabilityServices;
            this.sustainabilityClient = sustainabilityClient;
            this.ftmId = ftmId;
            this.currentDryingInformation = currentDryingInformation;
            this.onViewClose = handleViewClose;

            this.dryingSystems = new ObservableCollection<DryingSystemItemViewModel>(dryingSystems.Select(x => new DryingSystemItemViewModel(x)));
            this.dryingSystems.Insert(0, new DryingSystemItemViewModel(null));
            //this.dryingSystems.Add(new DryingSystemItemViewModel(null, true));

            this.fuelTypes = new ObservableCollection<FuelType>(fuels);
            
            if (currentDryingInformation != null) {
                percentMoistureToRemove = this.currentDryingInformation.PointsOfMoistureToRemove.GetValueOrDefault();
                percentYieldToDry = this.currentDryingInformation.PercentOfYieldToDry.GetValueOrDefault();

                if (this.currentDryingInformation.ActualFuelUsage != null && this.currentDryingInformation.ActualFuelUsage.Any()) {
                    selectedDryingSystem = this.dryingSystems.Where(x => x.IsCustom).FirstOrDefault();
                } else {
                    selectedDryingSystem = this.dryingSystems.Where(x => x.System != null && x.System.Name == currentDryingInformation.DryerType).FirstOrDefault();
                }
            } else {
                selectedDryingSystem = this.dryingSystems.Where(x => x.System == null && !x.IsCustom).FirstOrDefault();
            }

            SubmitCommand = new AwaitableRelayCommand(SubmitDrying);
            CancelCommand = new RelayCommand(handleViewClose);
        }

        public static async Task<FtmSubmitDryingViewModel> Create(IClientEndpoint clientEndpoint, Dispatcher dispatcher, SustainabilityServices sustainabilityServices, SustainabilityServiceClient sustainabilityClient, FtmId ftmId, DryingInformation currentDryingInformation, Action handleViewClose) {
            var dryingSystems = await sustainabilityServices.GetDryingSystems();
            var fuels = await sustainabilityServices.GetFuelTypes();
            return new FtmSubmitDryingViewModel(clientEndpoint, dispatcher, sustainabilityServices, sustainabilityClient, dryingSystems, fuels, ftmId, currentDryingInformation, handleViewClose);
        }

        public ICommand SubmitCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }

        public ObservableCollection<DryingSystemItemViewModel> DryingSystems {
            get { return dryingSystems; }
        }

        public ObservableCollection<FuelType> FuelTypes {
            get { return fuelTypes; }
        }

        public DryingSystemItemViewModel SelectedDryingSystem {
            get { return selectedDryingSystem; }
            set {
                selectedDryingSystem = value;
                RaisePropertyChanged(() => SelectedDryingSystem);
            }
        }

        public double PercentMoistureToRemove {
            get { return percentMoistureToRemove; }
            set {
                percentMoistureToRemove = value;
                RaisePropertyChanged(() => PercentMoistureToRemove);
            }
        }

        public double PercentYieldToDry {
            get { return percentYieldToDry; }
            set {
                percentYieldToDry = value;
                RaisePropertyChanged(() => PercentYieldToDry);
            }
        }

        async Task SubmitDrying() {
            if (currentDryingInformation == null) { currentDryingInformation = new DryingInformation(); }

            if (SelectedDryingSystem.System != null && !SelectedDryingSystem.IsCustom) {
                currentDryingInformation.DryerType = SelectedDryingSystem.System.Name;
            }

            currentDryingInformation.PercentOfYieldToDry = PercentYieldToDry;
            currentDryingInformation.PointsOfMoistureToRemove = PercentMoistureToRemove;

            if (SelectedDryingSystem.System == null && !SelectedDryingSystem.IsCustom)
            {
                currentDryingInformation = new DryingInformation();
            }

            var synced = await sustainabilityClient.SetDrying(ftmId.ClientId, ftmId.Crop, ftmId.FieldId, currentDryingInformation);

            onViewClose();
        }
    }

    public class DryingSystemItemViewModel : ViewModelBase {
        DryingSystem system;
        bool isCustom = false;

        public DryingSystemItemViewModel(DryingSystem system, bool isCustom = false) {
            this.system = system;
            this.isCustom = isCustom;
        }

        public DryingSystem System {
            get { return system; }
        }

        public bool IsCustom {
            get { return isCustom; }
        }

        public override string ToString() {
            if (isCustom) { return "-custom-"; }
            return system != null ? system.Name : "-none-";
        }
    }
}
