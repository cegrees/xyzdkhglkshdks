﻿using FieldToMarket.Contracts;
using FieldToMarket.Contracts.Partial;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Client.Services;
using Landdb.Infrastructure.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;

namespace Landdb.ViewModel.Analysis.FieldToMarket {
    public class FtmSubmitIrrigationViewModel : ViewModelBase {
        IClientEndpoint clientEndpoint;
        Dispatcher dispatcher;
        SustainabilityServices sustainabilityServices;
        SustainabilityServiceClient sustainabilityClient;
        FtmId ftmId;
        IrrigationInformation currentIrrigationInformation;
        Action onViewClose;

        bool isIrrigated;
        double irrigationAmount;

        public FtmSubmitIrrigationViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, SustainabilityServices sustainabilityServices, SustainabilityServiceClient sustainabilityClient, FtmId ftmId, IrrigationInformation currentIrrigationInformation, Action handleViewClose) {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            this.sustainabilityServices = sustainabilityServices;
            this.sustainabilityClient = sustainabilityClient;
            this.ftmId = ftmId;
            this.currentIrrigationInformation = currentIrrigationInformation;
            this.onViewClose = handleViewClose;

            this.irrigationAmount = currentIrrigationInformation != null ? currentIrrigationInformation.IrrigationAmount.GetValueOrDefault() : 0;
            this.isIrrigated = currentIrrigationInformation != null ? currentIrrigationInformation.IsIrrigated : false;

            SubmitCommand = new AwaitableRelayCommand(SubmitIrrigation);
            CancelCommand = new RelayCommand(handleViewClose);
        }

        public ICommand SubmitCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }

        public bool IsIrrigated {
            get { return isIrrigated; }
        }

        public double IrrigationAmount {
            get { return irrigationAmount; }
            set {
                irrigationAmount = value;
                RaisePropertyChanged(() => IrrigationAmount);
            }
        }

        async Task SubmitIrrigation() {
            currentIrrigationInformation.IrrigationAmount = IrrigationAmount;

            var irrigationSynced = await sustainabilityClient.SetIrrigation(ftmId.ClientId, ftmId.Crop, ftmId.FieldId, currentIrrigationInformation);

            onViewClose();
        }
    }
}
