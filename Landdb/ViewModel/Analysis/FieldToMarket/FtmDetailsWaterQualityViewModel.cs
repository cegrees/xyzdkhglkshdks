﻿using AgC.UnitConversion;
using FieldToMarket.Contracts;
using GalaSoft.MvvmLight;
using Landdb.Client.Infrastructure;
using Landdb.Client.Services;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Yield;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Client.Infrastructure.DisplayItems;
using Landdb.Infrastructure;
using Landdb.Resources;

namespace Landdb.ViewModel.Analysis.FieldToMarket {
    public class FtmDetailsWaterQualityViewModel : ViewModelBase {
        IClientEndpoint clientEndpoint;
        Dispatcher dispatcher;
        SustainabilityServices sustainabilityServices;
        SustainabilityServiceClient sustainabilityClient;

        FieldDetails latestDetailsDocument;
        FtmId latestFtmId;
        bool optedInToWaterDataSubmission;
        bool hasLocalChanges;

        public FtmDetailsWaterQualityViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, SustainabilityServices sustainabilityServices, SustainabilityServiceClient sustainabilityClient, Action notifyOfChanges = null) {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            this.sustainabilityServices = sustainabilityServices;
            this.sustainabilityClient = sustainabilityClient;

            SubmitWaterSurveyCommand = new AwaitableRelayCommand(SubmitWaterSurvey, CanSubmitSurvey);

            BuildConservationPracticesModel();
            BuildVegetativeCoverLevels();

            AvailableCrops = clientEndpoint.GetMasterlistService().GetCropList().OrderBy(x => x.Name).ToList();
        }

        public AwaitableRelayCommand SubmitWaterSurveyCommand { get; private set; }

        internal async Task RefreshFromDetails(FieldDetails details, CropZoneId cropZoneId, int cropYear) {
            latestDetailsDocument = details ?? new FieldDetails();
            var latestWaterQualityDoc = details.WaterQuality ?? new FieldWaterQualityDocument();
            latestFtmId = new FtmId() { Crop = details.Registration.Crop, ClientId = details.Registration.Id.ClientId, FieldId = details.Registration.Id.FieldId };

            optedInToWaterDataSubmission = details.WaterQuality != null;
            RaisePropertyChanged(() => OptedInToWaterDataSubmission);

            SetRemoteValues(latestWaterQualityDoc);

            RefreshVarietyDetails(details, cropZoneId, cropYear);
            RefreshYieldDetails(details, cropZoneId, cropYear);
            if (!string.IsNullOrWhiteSpace(details.Registration.HistoricId)) {
                RefreshHydrologyDetails(details, new FieldId(cropZoneId.DataSourceId, Guid.Parse(details.Registration.HistoricId)));
            }
        }

        void SetRemoteValues(FieldWaterQualityDocument wd) {
            variety = wd.VarietyName;
            rate = wd.SeedingRateValue;
            rateUnit = wd.SeedingRateUnit;
            plantingDate = wd.PlantingDate;
            harvestDate = wd.HarvestDate;
            previousCrop = AvailableCrops.Where(x => x.Id == wd.PreviousCropId).FirstOrDefault();

            insectPressure = wd.InsectPressure;
            weedPressure = wd.WeedPressure;
            diseasePressure = wd.DiseasePressure;

            ipmStrategy = wd.IpmStrategy;
            universityNutrientAppRateComparison = wd.UniversityNutrientAppRateComparison;
            soilConditionAtNApplication = wd.SoilConditionAtNApplication;

            hydrologicSoilGroup = wd.HydrologicSoilGroup;
            nCreditFromCoverCrop = wd.NitrogenCreditFromCoverCrop;
            residualNitrogenLevelSampleDepth = wd.ResidualNitrogenLevelSamplingDepth;
            residualNitrogenSampleValue = wd.ResidualNitrogenLevelValue;
            residualNitrogenSampleUnit = wd.ResidualNitrogenLevelUnit;
            isSoilTested = wd.AreSoilTestsPerformed;

            RaisePropertyChanged(() => Variety);
            RaisePropertyChanged(() => Rate);
            RaisePropertyChanged(() => RateUnit);
            RaisePropertyChanged(() => RateDisplay);
            RaisePropertyChanged(() => PlantingDate);
            RaisePropertyChanged(() => HarvestDate);
            RaisePropertyChanged(() => PreviousCrop);
            RaisePropertyChanged(() => InsectPressure);
            RaisePropertyChanged(() => WeedPressure);
            RaisePropertyChanged(() => DiseasePressure);
            RaisePropertyChanged(() => IpmStrategy);
            RaisePropertyChanged(() => UniversityNutrientAppRateComparison);
            RaisePropertyChanged(() => SoilConditionAtNApplication);
            RaisePropertyChanged(() => HydrologicSoilGroup);
            RaisePropertyChanged(() => NCreditFromCoverCrop);
            RaisePropertyChanged(() => ResidualNitrogenLevelSampleDepth);
            RaisePropertyChanged(() => ResidualNitrogenSampleValue);
            RaisePropertyChanged(() => ResidualNitrogenSampleUnit);
            RaisePropertyChanged(() => IsSoilTested);

            SetConservationPracticesFlags(wd.ConservationPractices);
            SetVegetativeCoverLevels(wd.MonthlyVegetativeCoverLevels);
            HasLocalChanges = false;
        }

        void RefreshVarietyDetails(FieldDetails details, CropZoneId cropZoneId, int cropYear) {
            CropYearId cropYearId = new CropYearId(cropZoneId.DataSourceId, cropYear);
            var data = clientEndpoint.GetView<CropZoneApplicationDataView>(cropYearId).GetValue(new CropZoneApplicationDataView());

            var q = from r in data.Items
                    let p = clientEndpoint.GetMasterlistService().GetProduct(r.ProductId)
                    where r.CropZoneId == cropZoneId && p.ProductType == GlobalStrings.ProductType_Seed
                    orderby r.StartDate
                    select new { ProductId = r.ProductId, ProductName = p.Name, Rate = (double)r.RateValue, RateUnit = r.RateUnit, Date = r.StartDate, ProductDensity = p.Density };

            var variety = q.FirstOrDefault();
            if (variety != null) {
                var rate = clientEndpoint.GetProductUnitResolver().GetPackageSafeUnit(variety.ProductId, variety.RateUnit).GetMeasure(variety.Rate, variety.ProductDensity);
                var packageUnit = clientEndpoint.GetProductUnitResolver().GetProductPackageUnit(variety.ProductId);
                if (packageUnit is AgC.UnitConversion.Package.PackageUnit && packageUnit.Name == rate.Unit.Name) {
                    rate = rate.CreateAs(((AgC.UnitConversion.Package.PackageUnit)packageUnit).PartUnit);
                }

                this.Variety = variety.ProductName;
                this.Rate = rate.Value;
                this.RateUnit = rate.Unit.Name;
                this.PlantingDate = variety.Date;

            } else {
                // TODO: Update for empty seed
            }
        }

        void RefreshYieldDetails(FieldDetails details, CropZoneId cropZoneId, int cropYear) {
            CropYearId cropYearId = new CropYearId(cropZoneId.DataSourceId, cropYear);
            var data = clientEndpoint.GetView<CropZoneLoadDataView>(cropYearId).GetValue(new CropZoneLoadDataView());

            var q = from y in data.Items
                    where y.CropZoneId == cropZoneId
                    orderby y.StartDateTime
                    select y.StartDateTime;

            var harvest = q.FirstOrDefault();

            if (harvest != default(DateTime)) {
                HarvestDate = harvest.ToUniversalTime();
            } else {
                // TODO: Update for empty harvest
            }
        }

        void RefreshHydrologyDetails(FieldDetails details, FieldId fieldId) {
            var data = clientEndpoint.GetView<Landdb.Domain.ReadModels.Tree.FieldDetailsView>(fieldId).GetValue(new Landdb.Domain.ReadModels.Tree.FieldDetailsView());

            if (!string.IsNullOrWhiteSpace(data.HydrologyGroup)) {
                HydrologicSoilGroups h;
                bool parseSuccess = Enum.TryParse(data.HydrologyGroup, out h);
                if (parseSuccess) {
                    HydrologicSoilGroup = h;
                } else {
                    if (data.HydrologyGroup == @"A/D" || data.HydrologyGroup == @"A / D") { HydrologicSoilGroup = HydrologicSoilGroups.AD; }
                    if (data.HydrologyGroup == @"B/D" || data.HydrologyGroup == @"B / D") { HydrologicSoilGroup = HydrologicSoilGroups.BD; }
                    if (data.HydrologyGroup == @"C/D" || data.HydrologyGroup == @"C / D") { HydrologicSoilGroup = HydrologicSoilGroups.CD; }
                }
            }
        }

        public bool HasLocalChanges {
            get { return hasLocalChanges; }
            set { 
                hasLocalChanges = value; 
                RaisePropertyChanged(() => HasLocalChanges);
                RaisePropertyChanged(() => IsComplete);
                RaisePropertyChanged(() => IsCompleteAndSubmitted);
                SubmitWaterSurveyCommand.RaiseCanExecuteChanged();
            }
        }

        public bool IsComplete {
            get {
                if (string.IsNullOrWhiteSpace(variety)) { return false; }
                if (plantingDate.Year < 1980) { return false; }
                if (harvestDate.Year < 1980) { return false; }
                if (previousCrop == null || previousCrop.Id == Guid.Empty) { return false; }
                if (weedPressure == BioticPressureValues.Unknown) { return false; }
                if (insectPressure == BioticPressureValues.Unknown) { return false; }
                if (diseasePressure == BioticPressureValues.Unknown) { return false; }
                if (hydrologicSoilGroup == HydrologicSoilGroups.Unknown) { return false; }
                foreach (var vc in MonthlyVegetativeCoverLevels.Values) {
                    if (vc == VegetativeCoverLevels.Unknown) { return false; }
                }
                if (ipmStrategy == IpmStrategies.Unknown) { return false; }
                if (universityNutrientAppRateComparison == UniversityNutrientAppRateComparisons.Unknown) { return false; }
                if (soilConditionAtNApplication == WaterQualitySoilConditionAtNApplication.Unknown) { return false; }

                return true;
            }
        }

        public bool IsCompleteAndSubmitted {
            get { return IsComplete && !HasLocalChanges; }
        }

        public bool OptedInToWaterDataSubmission {
            get { return optedInToWaterDataSubmission; }
            set {
                optedInToWaterDataSubmission = value;
                RaisePropertyChanged(() => OptedInToWaterDataSubmission);
                SubmitWaterSurveyCommand.RaiseCanExecuteChanged();
            }
        }

        public bool CanSubmitSurvey() {
            return HasLocalChanges && OptedInToWaterDataSubmission;
        }

        #region Collection Props
        public List<FtmWaterQualityConservationPracticesViewModel> AvailableConservationPractices { get; set; }

        void BuildConservationPracticesModel() {
            AvailableConservationPractices = new List<FtmWaterQualityConservationPracticesViewModel>() {
                new FtmWaterQualityConservationPracticesViewModel(ConservationPractices.GrassWaterway, OnConservationPracticeToggled, CanSelectAdditionalPractice),
                new FtmWaterQualityConservationPracticesViewModel(ConservationPractices.ConservationCover, OnConservationPracticeToggled, CanSelectAdditionalPractice),
                new FtmWaterQualityConservationPracticesViewModel(ConservationPractices.ContourStripCropping, OnConservationPracticeToggled, CanSelectAdditionalPractice),
                new FtmWaterQualityConservationPracticesViewModel(ConservationPractices.FilterStrip, OnConservationPracticeToggled, CanSelectAdditionalPractice),
                new FtmWaterQualityConservationPracticesViewModel(ConservationPractices.FieldBorders, OnConservationPracticeToggled, CanSelectAdditionalPractice),
                new FtmWaterQualityConservationPracticesViewModel(ConservationPractices.VegetativeBarrier, OnConservationPracticeToggled, CanSelectAdditionalPractice),
                new FtmWaterQualityConservationPracticesViewModel(ConservationPractices.SedimentBasins, OnConservationPracticeToggled, CanSelectAdditionalPractice),
                new FtmWaterQualityConservationPracticesViewModel(ConservationPractices.WaterAndSedimentControlBasin, OnConservationPracticeToggled, CanSelectAdditionalPractice),
                new FtmWaterQualityConservationPracticesViewModel(ConservationPractices.StreamHabitatImprovementAndManagement, OnConservationPracticeToggled, CanSelectAdditionalPractice),
                new FtmWaterQualityConservationPracticesViewModel(ConservationPractices.RiparianHerbaceousCover, OnConservationPracticeToggled, CanSelectAdditionalPractice),
                new FtmWaterQualityConservationPracticesViewModel(ConservationPractices.RiparianForestBuffer, OnConservationPracticeToggled, CanSelectAdditionalPractice),
                new FtmWaterQualityConservationPracticesViewModel(ConservationPractices.TailwaterRecoverySystem, OnConservationPracticeToggled, CanSelectAdditionalPractice),
            };
        }

        bool CanSelectAdditionalPractice() {
            var q = from acp in AvailableConservationPractices
                    where acp.IsChecked
                    select acp;
            return q.Count() < 3;
        }

        void OnConservationPracticeToggled() {
            HasLocalChanges = true;
        }

        void SetConservationPracticesFlags(ConservationPractices cp) {
            foreach (var acp in AvailableConservationPractices) {
                acp.SetIsCheckedSilently(cp.HasFlag(acp.Practice));
            }
            RaisePropertyChanged(() => AvailableConservationPractices);
        }

        public Dictionary<int, VegetativeCoverLevels> MonthlyVegetativeCoverLevels { get; set; }

        void BuildVegetativeCoverLevels() {
            MonthlyVegetativeCoverLevels = new Dictionary<int, VegetativeCoverLevels>() {
                {1, VegetativeCoverLevels.Unknown},
                {2, VegetativeCoverLevels.Unknown},
                {3, VegetativeCoverLevels.Unknown},
                {4, VegetativeCoverLevels.Unknown},
                {5, VegetativeCoverLevels.Unknown},
                {6, VegetativeCoverLevels.Unknown},
                {7, VegetativeCoverLevels.Unknown},
                {8, VegetativeCoverLevels.Unknown},
                {9, VegetativeCoverLevels.Unknown},
                {10, VegetativeCoverLevels.Unknown},
                {11, VegetativeCoverLevels.Unknown},
                {12, VegetativeCoverLevels.Unknown},
            };
        }

        void SetVegetativeCoverLevels(Dictionary<int, VegetativeCoverLevels> vc) {
            if (vc != null && vc.Count == 12) {
                MonthlyVegetativeCoverLevels = vc;
            } else {
                BuildVegetativeCoverLevels();
            }
            
            RaisePropertyChanged(() => MonthlyVegetativeCoverLevels);
        }

        public IList<MiniCrop> AvailableCrops { get; set; }
        #endregion

        #region Water Doc Props
        private string variety;
        public string Variety {
            get { return variety; }
            set {
                if (variety == value) { return; }
                variety = value; 
                RaisePropertyChanged(() => Variety);
                HasLocalChanges = true;
            }
        }

        private double rate;
        public double Rate {
            get { return rate; }
            set {
                if (rate == value) { return; }
                rate = value; 
                RaisePropertyChanged(() => Rate);
                RaisePropertyChanged(() => RateDisplay);
                HasLocalChanges = true;
            }
        }

        private string rateUnit;
        public string RateUnit {
            get { return rateUnit; }
            set {
                if (rateUnit == value) { return; }
                rateUnit = value; 
                RaisePropertyChanged(() => RateUnit);
                RaisePropertyChanged(() => RateDisplay);
                HasLocalChanges = true;
            }
        }

        public string RateDisplay => $"{Rate:n2} {RateUnit} / ac"; //TODO: Localize if we ever use FTM in other countries

        private DateTime plantingDate;
        public DateTime PlantingDate {
            get { return plantingDate; }
            set {
                if (plantingDate == value) { return; }
                plantingDate = value; 
                RaisePropertyChanged(() => PlantingDate);
                HasLocalChanges = true;
            }
        }

        private DateTime harvestDate;
        public DateTime HarvestDate {
            get { return harvestDate; }
            set {
                if (harvestDate == value) { return; }
                harvestDate = value;
                RaisePropertyChanged(() => HarvestDate);
                HasLocalChanges = true;
            }
        }


        private MiniCrop previousCrop;
        public MiniCrop PreviousCrop {
            get { return previousCrop; }
            set {
                if (previousCrop == value) { return; }
                previousCrop = value;
                RaisePropertyChanged(() => PreviousCrop);
                HasLocalChanges = true;
            }
        }


        private BioticPressureValues weedPressure;
        public BioticPressureValues WeedPressure {
            get { return weedPressure; }
            set {
                if (weedPressure == value) { return; }
                weedPressure = value;
                RaisePropertyChanged(() => WeedPressure);
                HasLocalChanges = true;
            }
        }

        private BioticPressureValues insectPressure;
        public BioticPressureValues InsectPressure {
            get { return insectPressure; }
            set {
                if (insectPressure == value) { return; }
                insectPressure = value;
                RaisePropertyChanged(() => InsectPressure);
                HasLocalChanges = true;
            }
        }

        private BioticPressureValues diseasePressure;
        public BioticPressureValues DiseasePressure {
            get { return diseasePressure; }
            set {
                if (diseasePressure == value) { return; }
                diseasePressure = value;
                RaisePropertyChanged(() => DiseasePressure);
                HasLocalChanges = true;
            }
        }

        private IpmStrategies ipmStrategy;
        public IpmStrategies IpmStrategy {
            get { return ipmStrategy; }
            set {
                if (ipmStrategy == value) { return; }
                ipmStrategy = value;
                RaisePropertyChanged(() => IpmStrategy);
                HasLocalChanges = true;
            }
        }

        private UniversityNutrientAppRateComparisons universityNutrientAppRateComparison;
        public UniversityNutrientAppRateComparisons UniversityNutrientAppRateComparison {
            get { return universityNutrientAppRateComparison; }
            set {
                if (universityNutrientAppRateComparison == value) { return; }
                universityNutrientAppRateComparison = value;
                RaisePropertyChanged(() => UniversityNutrientAppRateComparison);
                HasLocalChanges = true;
            }
        }

        private WaterQualitySoilConditionAtNApplication soilConditionAtNApplication;
        public WaterQualitySoilConditionAtNApplication SoilConditionAtNApplication {
            get { return soilConditionAtNApplication; }
            set {
                if (soilConditionAtNApplication == value) { return; }
                soilConditionAtNApplication = value;
                RaisePropertyChanged(() => SoilConditionAtNApplication);
                HasLocalChanges = true;
            }
        }

        private HydrologicSoilGroups hydrologicSoilGroup;
        public HydrologicSoilGroups HydrologicSoilGroup {
            get { return hydrologicSoilGroup; }
            set {
                if (hydrologicSoilGroup == value) { return; }
                hydrologicSoilGroup = value;
                RaisePropertyChanged(() => HydrologicSoilGroup);
                HasLocalChanges = true;
            }
        }

        private double nCreditFromCoverCrop;
        public double NCreditFromCoverCrop {
            get { return nCreditFromCoverCrop; }
            set {
                if (nCreditFromCoverCrop == value) { return; }
                nCreditFromCoverCrop = value;
                RaisePropertyChanged(() => NCreditFromCoverCrop);
                HasLocalChanges = true;
            }
        }

        private ResidualNitrogenLevelSamplingDepths residualNitrogenLevelSampleDepth;
        public ResidualNitrogenLevelSamplingDepths ResidualNitrogenLevelSampleDepth {
            get { return residualNitrogenLevelSampleDepth; }
            set {
                if (residualNitrogenLevelSampleDepth == value) { return; }
                residualNitrogenLevelSampleDepth = value;
                RaisePropertyChanged(() => ResidualNitrogenLevelSampleDepth);
                HasLocalChanges = true;
            }
        }

        private double residualNitrogenSampleValue;
        public double ResidualNitrogenSampleValue {
            get { return residualNitrogenSampleValue; }
            set {
                if (residualNitrogenSampleValue == value) { return; }
                residualNitrogenSampleValue = value;
                RaisePropertyChanged(() => ResidualNitrogenSampleValue);
                HasLocalChanges = true;
            }
        }

        private string residualNitrogenSampleUnit;
        public string ResidualNitrogenSampleUnit {
            get { return residualNitrogenSampleUnit; }
            set {
                if (residualNitrogenSampleUnit == value) { return; }
                residualNitrogenSampleUnit = value;
                RaisePropertyChanged(() => ResidualNitrogenSampleUnit);
                HasLocalChanges = true;
            }
        }

        private static string WeightText =>
            ApplicationEnvironment.CurrentDataSource.DatasourceCulture.ToLower().Equals("en-us") ?
                WeightUnitDisplayItem.GetWeightUnitDisplayTextFor(WeightUnitDisplayType.Pound) :
                WeightUnitDisplayItem.GetWeightUnitDisplayTextFor(WeightUnitDisplayType.Kilogram);
        public List<string> AvailableResidualNitrogenSampleUnits {
            get { return new List<string>() { "ppm", String.Format(Strings.Format_WeightPerArea, WeightText, AreaUnitDisplayItem.GetAreaUnitDisplayTextFor(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit)) }; }
        }

        private bool isSoilTested;
        public bool IsSoilTested {
            get { return isSoilTested; }
            set {
                if (isSoilTested == value) { return; }
                isSoilTested = value;
                RaisePropertyChanged(() => IsSoilTested);
                HasLocalChanges = true;
            }
        }

        #endregion

        public async Task SubmitWaterSurvey() {
            if(latestFtmId == null) { return; }

            ConservationPractices localCp = ConservationPractices.None;
            foreach (var acp in AvailableConservationPractices) {
                if (acp.IsChecked) {
                    localCp |= acp.Practice;
                }
            }

            FieldWaterQualityDocument waterDoc = new FieldWaterQualityDocument() {
               VarietyName = Variety,
               SeedingRateValue = Rate,
               SeedingRateUnit = RateUnit,
               PlantingDate = PlantingDate,
               HarvestDate = HarvestDate,
               PreviousCropId = PreviousCrop != null ? PreviousCrop.Id : Guid.Empty,
               PreviousCropName = PreviousCrop != null ? PreviousCrop.Name : string.Empty,
               WeedPressure = WeedPressure,
               InsectPressure = InsectPressure,
               DiseasePressure = DiseasePressure,
               IpmStrategy = IpmStrategy,
               UniversityNutrientAppRateComparison = UniversityNutrientAppRateComparison,
               SoilConditionAtNApplication = SoilConditionAtNApplication,
               HydrologicSoilGroup = HydrologicSoilGroup,
               NitrogenCreditFromCoverCrop = NCreditFromCoverCrop,
               AreSoilTestsPerformed = IsSoilTested,
               ResidualNitrogenLevelSamplingDepth = ResidualNitrogenLevelSampleDepth,
               ResidualNitrogenLevelValue = ResidualNitrogenSampleValue,
               ResidualNitrogenLevelUnit = ResidualNitrogenSampleUnit,
               ConservationPractices = localCp,
               MonthlyVegetativeCoverLevels = MonthlyVegetativeCoverLevels
            };

            await sustainabilityClient.SetWaterQualityDoc(latestFtmId.ClientId, latestFtmId.Crop, latestFtmId.FieldId, waterDoc);
            HasLocalChanges = false;
        }
    }

    public class FtmWaterQualityConservationPracticesViewModel : ViewModelBase {
        ConservationPractices practice;
        bool isChecked;
        Action onToggled;
        Func<bool> canSelectAdditionalPractice;

        public FtmWaterQualityConservationPracticesViewModel(ConservationPractices practice, Action onToggled = null, Func<bool> canSelectAdditionalPractice = null) {
            this.practice = practice;
            this.onToggled = onToggled;
            this.canSelectAdditionalPractice = canSelectAdditionalPractice;
        }

        public ConservationPractices Practice {
            get { return practice; }
            set {
                practice = value;
                RaisePropertyChanged(() => Practice);
            }
        }

        internal void SetIsCheckedSilently(bool isChecked) {
            this.isChecked = isChecked;
            RaisePropertyChanged(() => IsChecked);
        }

        public bool IsChecked {
            get { return isChecked; }
            set {
                if (value && canSelectAdditionalPractice != null && !canSelectAdditionalPractice()) { return; }

                isChecked = value;
                RaisePropertyChanged(() => IsChecked);
                if (onToggled != null) { onToggled(); }
            }
        }

    }
}
