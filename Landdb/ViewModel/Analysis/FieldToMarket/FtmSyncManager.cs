﻿using AgC.UnitConversion;
using ftmc = FieldToMarket.Contracts;
using FieldToMarket.Contracts.Partial;
using Landdb.Client.Infrastructure;
using Landdb.Client.Services;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Tree;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FieldToMarket.Contracts;
using FieldToMarket.Contracts.Reference;
using NLog;
using Landdb.Client.Models;
using Landdb.Domain.ReadModels.Yield;
using Newtonsoft.Json;
using Landdb.Domain.ReadModels.Map;

namespace Landdb.ViewModel.Analysis.FieldToMarket {
    public class FtmSyncManager {
        IClientEndpoint clientEndpoint;
        SustainabilityServices sustainabilityServices;
        SustainabilityServiceClient sustainabilityClient;
        Logger log = LogManager.GetCurrentClassLogger();

        public FtmSyncManager(IClientEndpoint clientEndpoint, SustainabilityServices sustainabilityServices, SustainabilityServiceClient sustainabilityClient) {
            this.clientEndpoint = clientEndpoint;
            this.sustainabilityServices = sustainabilityServices;
            this.sustainabilityClient = sustainabilityClient;
        }

        bool isYieldEstimated;
        public bool IsYieldEstimated {
            get { return isYieldEstimated; }
            private set { isYieldEstimated = value; }
        }

        public async Task<bool> CompareAndSubmitChanges(string ftmCrop, FieldDetails details, CropZoneDetailsView localCzData, FieldDetailsView localFieldData, IList<CropZoneApplicationDataItem> applications, IDictionary<CompositeUnit, YieldQuantity> yield, CropYearId cropYearId) {
            bool needsRefresh = false;
            if (details == null) { return false; }

            FtmId ftmId = new FtmId() { Crop = ftmCrop, ClientId = localCzData.Id.DataSourceId.ToString(), FieldId = localCzData.Id.Id.ToString() };
            double? area = localCzData.ReportedArea ?? localCzData.BoundaryArea ?? localFieldData.ReportedArea ?? localFieldData.BoundaryArea;

            // Yield
            bool yieldAutoSet = false;
            if (yield.Any() && area.HasValue) {
                var crop = (await sustainabilityServices.GetCrops()).Where(x => x.CropName == details.Registration.Crop).FirstOrDefault();
                if (crop != null && yield.Any(x => x.Key.PackageUnitName == crop.UnitOfMeasure)) {
                    var yieldQuantity = yield.FirstOrDefault(x => x.Key.PackageUnitName == crop.UnitOfMeasure).Value;
                    var yieldPerAcre = yieldQuantity.TotalQuantity / area.Value;
                    yieldAutoSet = true;
                    if (details != null && yieldPerAcre != details.Inputs.Yield.GetValueOrDefault()) {
                        var refreshedYield = await sustainabilityClient.SetYield(ftmId.ClientId, ftmId.Crop, ftmId.FieldId, yieldPerAcre);
                        needsRefresh = needsRefresh || refreshedYield;
                    }
                }
            }

            if (!yieldAutoSet) {
                isYieldEstimated = true;
            }

            // State
            var state = clientEndpoint.GetMasterlistService().GetStateByName(localFieldData.State);
            string stateAbbreviation = state != null ? state.Abbreviation : null;

            if (details.Inputs.State != stateAbbreviation) {
                var refreshedState = await sustainabilityClient.SetState(ftmId.ClientId, ftmId.Crop, ftmId.FieldId, stateAbbreviation);
                needsRefresh = needsRefresh || refreshedState;
            }

            // Slope
            if (localFieldData.SlopeLength != null && localFieldData.SlopePercent != null) {
                if ((double)localFieldData.SlopeLength.Value != details.Inputs.SlopeLength || (double)localFieldData.SlopePercent.Value != details.Inputs.SlopeGrade) {
                    var refreshedSlope = await sustainabilityClient.SetSlope(ftmId.ClientId, ftmId.Crop, ftmId.FieldId, (double)localFieldData.SlopePercent.Value, (double)localFieldData.SlopeLength.Value);
                    needsRefresh = needsRefresh || refreshedSlope;
                }
            }

            // Area
            if (area.HasValue && area != details.Inputs.Area) {
                var refreshedArea = await sustainabilityClient.SetArea(ftmId.ClientId, ftmId.Crop, ftmId.FieldId, area.Value);
                needsRefresh = needsRefresh || refreshedArea;
            }

            // Boundary
            var mapItem = clientEndpoint.GetView<ItemMap>(localCzData.Id);
            if (mapItem.HasValue && mapItem.Value.MostRecentMapItem != null) {
                var mruMap = mapItem.Value.MostRecentMapItem;
                if (mruMap.MapData != details.Inputs.BoundaryWkt) {
                    var refreshedBoundary = await sustainabilityClient.SetBoundary(ftmId.ClientId, ftmId.Crop, ftmId.FieldId, mruMap.MapData);
                    needsRefresh = needsRefresh || refreshedBoundary;
                }
            }

            var refreshedIrrigation = await SyncIrrigation(ftmId, details, localCzData, localFieldData);
            needsRefresh = needsRefresh || refreshedIrrigation;

            var refreshedCropProtection = await SyncCropProtection(ftmId, details, applications);
            needsRefresh = needsRefresh || refreshedCropProtection;

            var refreshedFertilizer = await SyncFertilizers(ftmId, details, applications);
            needsRefresh = needsRefresh || refreshedFertilizer;

            var refreshedCustomData = await SyncCustomData(ftmId, details, cropYearId, localCzData.CropId);
            needsRefresh = needsRefresh || refreshedCustomData;

            return needsRefresh;
        }

        async Task<bool> SyncIrrigation(FtmId ftmId, FieldDetails details, CropZoneDetailsView localCzData, FieldDetailsView localFieldData) {
            bool irrigationSynced = false;
            if (details == null) { return false; }

            if (ftmId == null || details == null || localCzData == null || localFieldData == null) { return false; }

            // If no irrigation information has been specified in either location, then there's no need to specify anything else, and no reason to sync anything.
            if (details != null && details.Inputs.IsIrrigated == null && localFieldData.Irrigated == null && localCzData.IrrigationInformation == null) { return false; }

            IrrigationInformation localIrrigation = new IrrigationInformation();

            IrrigationInformation remoteIrrigation = new IrrigationInformation() {
                IsIrrigated = details.Inputs.IsIrrigated.GetValueOrDefault(),
                PumpPressure = details.Inputs.PumpPressure,
                PumpDepth = details.Inputs.PumpDepth,
                IrrigationAmount = details.Inputs.IrrigationAmount,

                IrrigationSystem = details.Inputs.IrrigationSystem,
                PumpEnergySource = details.Inputs.PumpEnergySource,
                WaterOrigin = details.Inputs.WaterOrigin,
                WaterSource = details.Inputs.WaterSource
            };

            if (localCzData.IrrigationInformation != null) {
                localIrrigation.IsIrrigated = localCzData.IrrigationInformation.Irrigated.GetValueOrDefault();
                localIrrigation.PumpDepth = localCzData.IrrigationInformation.PumpLift;
                localIrrigation.PumpPressure = localCzData.IrrigationInformation.PumpPressure;

                localIrrigation.IrrigationSystem = localCzData.IrrigationInformation.IrrigationSystem;
                localIrrigation.PumpEnergySource = localCzData.IrrigationInformation.EnergySource;
                localIrrigation.WaterOrigin = localCzData.IrrigationInformation.WaterOrigin;
                localIrrigation.WaterSource = localCzData.IrrigationInformation.WaterSource;
            } else { // use field data
                localIrrigation.IsIrrigated = localFieldData.Irrigated.GetValueOrDefault();
                localIrrigation.PumpDepth = localFieldData.PumpLift;
                localIrrigation.PumpPressure = localFieldData.PumpPressure;

                localIrrigation.IrrigationSystem = localFieldData.IrrigationSystem;
                localIrrigation.PumpEnergySource = localFieldData.EnergySource;
                localIrrigation.WaterOrigin = localFieldData.WaterOrigin;
                localIrrigation.WaterSource = localFieldData.WaterSource;
            }

            localIrrigation.IrrigationAmount = remoteIrrigation.IrrigationAmount;

            bool irrigationDiffers = remoteIrrigation.IsIrrigated != localIrrigation.IsIrrigated ||
                                        remoteIrrigation.PumpPressure != localIrrigation.PumpPressure ||
                                        remoteIrrigation.PumpDepth != localIrrigation.PumpDepth ||
                                        remoteIrrigation.IrrigationSystem != localIrrigation.IrrigationSystem ||
                                        remoteIrrigation.PumpEnergySource != localIrrigation.PumpEnergySource ||
                                        remoteIrrigation.WaterOrigin != localIrrigation.WaterOrigin ||
                                        remoteIrrigation.WaterSource != localIrrigation.WaterSource /*||
                                        remoteIrrigation.IrrigationAmount != localIrrigation.IrrigationAmount */ ||
                                        details.Inputs.IsIrrigated == null;

            if (irrigationDiffers) {
                log.Debug("Irrigation difference detected. Syncing irrigation with FTM");
                irrigationSynced = await sustainabilityClient.SetIrrigation(ftmId.ClientId, ftmId.Crop, ftmId.FieldId, localIrrigation);
            }

            return irrigationSynced;
        }

        async Task<bool> SyncCropProtection(FtmId ftmId, FieldDetails details, IList<CropZoneApplicationDataItem> applications) {
            bool cropProtectionSynced = false;
            if (details == null) { return false; }

            var ml = clientEndpoint.GetMasterlistService();

            var q = from app in applications
                    let mlProduct = ml.GetProduct(app.ProductId)
                    where mlProduct != null && mlProduct.ProductType == GlobalStrings.ProductType_CropProtection && mlProduct.Category != null
                    let categories = mlProduct.Category.Split(',')
                    from cat in categories
                    select cat;


            var remoteCropProtections = details.Inputs.CropProtectionApplications ?? new List<Client.Services.CropProtectionApplication>();
            var remoteCropProtection = remoteCropProtections.FirstOrDefault() ?? new Client.Services.CropProtectionApplication();

            Client.Services.CropProtectionApplication localCropProtectionApplication = new Client.Services.CropProtectionApplication {
                FumigantCount = q.Count(x => x == "Fumigant"),
                FungicideCount = q.Count(x => x == "Fungicide"),
                GrowthRegulatorCount = q.Count(x => x == "GrowthRegulator"),
                HerbicideCount = q.Count(x => x == "Herbicide"),
                InsecticideCount = q.Count(x => x == "Insecticide"),
                SeedTreatmentCount = q.Count(x => x == "SeedTreatment")
            };

            if (remoteCropProtections.Count != 1 || remoteCropProtection.FumigantCount != localCropProtectionApplication.FumigantCount || remoteCropProtection.FungicideCount != localCropProtectionApplication.FungicideCount || remoteCropProtection.GrowthRegulatorCount != localCropProtectionApplication.GrowthRegulatorCount || remoteCropProtection.HerbicideCount != localCropProtectionApplication.HerbicideCount || remoteCropProtection.InsecticideCount != localCropProtectionApplication.InsecticideCount || remoteCropProtection.SeedTreatmentCount != localCropProtectionApplication.SeedTreatmentCount) {
                cropProtectionSynced = await sustainabilityClient.SetCropProtection(ftmId.ClientId, ftmId.Crop, ftmId.FieldId, new List<Client.Services.CropProtectionApplication>() { localCropProtectionApplication });
            }


            return cropProtectionSynced;
        }

        async Task<bool> SyncFertilizers(FtmId ftmId, FieldDetails details, IList<CropZoneApplicationDataItem> applications) {
            bool fertilizersSynced = false;
            if (details == null) { return false; }

            var ml = clientEndpoint.GetMasterlistService();
            var remoteFertilizers = details.Inputs.FertilizerApplications ?? new List<Client.Services.FertilizerApplication>();

            var q = from app in applications
                    let mlProduct = ml.GetProduct(app.ProductId)
                    where mlProduct != null && mlProduct.ProductType == GlobalStrings.ProductType_Fertilizer
                    select app;

            HashSet<Client.Services.FertilizerApplication> remoteFertilizerSet = new HashSet<Client.Services.FertilizerApplication>(remoteFertilizers);
            HashSet<Client.Services.FertilizerApplication> localFertilizerSet = new HashSet<Client.Services.FertilizerApplication>();

            foreach (var app in q) {
                var product = ml.GetProduct(app.ProductId);
                try {
                    var totalMeasure = UnitFactory.GetPackageSafeUnit(app.TotalProductUnit, product.StdUnit, product.StdFactor, product.StdPackageUnit).GetMeasure(app.TotalProductValue, product.Density);
                    var convertedValue = totalMeasure.GetValueAs(UnitFactory.GetPackageSafeUnit(app.RateUnit, product.StdUnit, product.StdFactor, product.StdPackageUnit));
                    var czDetails = clientEndpoint.GetView<CropZoneDetailsView>(app.CropZoneId).Value;
                    var area = czDetails.ReportedArea.HasValue ? czDetails.ReportedArea.Value : czDetails.BoundaryArea.HasValue ? czDetails.BoundaryArea.Value : 0;
                    var ratePerArea = UnitFactory.GetPackageSafeUnit(app.RateUnit, product.StdUnit, product.StdFactor, product.StdPackageUnit).GetMeasure((convertedValue / area), product.Density).CreateAs(AgC.UnitConversion.MassAndWeight.Pound.Self); // (double)app.RateValue
                    var formulation = product.Formulation ?? new FertilizerFormulation();

                    Client.Services.FertilizerApplication fertApp = new Client.Services.FertilizerApplication() {
                        CustomIdentifier = app.ProductTrackingId.ToString(),
                        FertilizerId = app.ProductId.Id.ToString(),
                        RateInPoundsPerAcre = ratePerArea.Value,
                        Nitrogen = (double)formulation.N.GetValueOrDefault() / 100,
                        Phosphorus = (double)formulation.P.GetValueOrDefault() / 100,
                        Potassium = (double)formulation.K.GetValueOrDefault() / 100
                        // TODO: figure something out for manure....
                    };
                    localFertilizerSet.Add(fertApp);
                } catch (IncompatibleConversionTypeException ex) {
                    var exMsg = string.Format("Encountered a unit conversion problem with product '{0}' ({1}). Rate unit: {2}  Product unit: {3} {4} {5}  Density: {6}", product.Name, product.Id, app.RateUnit, product.StdFactor, product.StdUnit, product.StdPackageUnit, product.Density.HasValue ? product.Density.Value.ToString() : "(null)");
                    log.ErrorException(exMsg, ex);
                } catch (Exception ex) {
                    var exMsg = string.Format("Encountered a problem with product '{0}' ({1}). Rate unit: {2}  Product unit: {3} {4} {5}  Density: {6}", product.Name, product.Id, app.RateUnit, product.StdFactor, product.StdUnit, product.StdPackageUnit, product.Density.HasValue ? product.Density.Value.ToString() : "(null)" );
                    log.ErrorException(exMsg, ex);
                    throw;
                }
            }

            if (!remoteFertilizerSet.SetEquals(localFertilizerSet)) {
                // TODO: This set comparison is *supposed* to return false if the items contained within are equivalent, according to their Equals() method,
                // but it doesn't seem to be operating as expected, and is pushing the list each time. Need to figure out a way around this, or at least, what I'm doing wrong.
                fertilizersSynced = await sustainabilityClient.SetFertilizer(ftmId.ClientId, ftmId.Crop, ftmId.FieldId, localFertilizerSet.ToList());
            }

            return fertilizersSynced;
        }

        async Task<bool> SyncCustomData(FtmId ftmId, FieldDetails details, CropYearId cropYearId, CropId cropzoneCropId) {
            bool synced = false;
            if (details == null) { return false; }
            if (ftmId == null || details == null || cropYearId == null) { return false; }

            var czId = Guid.Parse(ftmId.FieldId);
            
            FtmCustomDataItem customData = !string.IsNullOrWhiteSpace(details.Inputs.CustomData) ? JsonConvert.DeserializeObject<FtmCustomDataItem>(details.Inputs.CustomData) : new FtmCustomDataItem();

            if (details.Registration.Crop == "Sugarbeets") {
                // TODO: Account for multiple fields in a yield ticket...

                Guid sugarPropertyId = Guid.Parse("42365775-b399-44e9-9a8b-09089d2281f1");
                var yieldData = clientEndpoint.GetView<CropZoneLoadDataView>(cropYearId).GetValue(new CropZoneLoadDataView()).Items.Where(x => x.CropZoneId != null && x.CropZoneId.Id == czId);
                List<Tuple<double, double>> sugarProps = new List<Tuple<double, double>>(); // Tuple<area,sugar>

                foreach(var load in yieldData) {
                    var loadDetails = clientEndpoint.GetView<LoadDetailsView>(load.LoadId).GetValue(() => null);
                    if (loadDetails != null) {
                        var sugarSet = loadDetails.ReportedPropertySets.Where(x => x.LoadPropertySetId == sugarPropertyId).FirstOrDefault();
                        var sugarPropValue = sugarSet != null ? sugarSet.ReportedPropertyValues.FirstOrDefault() : null;
                        if (sugarPropValue != null) {
                            double sugarPropDouble = 0;
                            if(double.TryParse(sugarPropValue.ReportedValue, out sugarPropDouble)) {
                                sugarProps.Add(new Tuple<double,double>(Convert.ToDouble(load.AreaWeightedFinalQuantity), sugarPropDouble));
                            }
                        }
                    }
                }

                if (sugarProps.Any()) {
                    customData.SugarPercentage = WeightedAverage(sugarProps);
                } else { customData.SugarPercentage = null; }
            }

            if (details.Registration.Crop.StartsWith("Wheat")) {
                // TODO: Reduce duplication with above crops...
                Guid proteinPropertyId = Guid.Parse("06f43787-4ea8-4a2b-af2a-55753c73f475");

                var yieldData = clientEndpoint.GetView<CropZoneLoadDataView>(cropYearId).GetValue(new CropZoneLoadDataView()).Items.Where(x => x.CropZoneId != null && x.CropZoneId.Id == czId);
                List<Tuple<double, double>> proteinProps = new List<Tuple<double, double>>(); // Tuple<area,sugar>

                foreach (var load in yieldData) {
                    var loadDetails = clientEndpoint.GetView<LoadDetailsView>(load.LoadId).GetValue(() => null);
                    if (loadDetails != null) {
                        var proteinSet = loadDetails.ReportedPropertySets.Where(x => x.LoadPropertySetId == proteinPropertyId).FirstOrDefault();
                        var proteinPropValue = proteinSet != null ? proteinSet.ReportedPropertyValues.FirstOrDefault() : null;
                        if (proteinPropValue != null) {
                            double proteinPropDouble = 0;
                            if (double.TryParse(proteinPropValue.ReportedValue, out proteinPropDouble)) {
                                proteinProps.Add(new Tuple<double, double>(Convert.ToDouble(load.AreaWeightedFinalQuantity), proteinPropDouble));
                            }
                        }
                    }
                }

                if (proteinProps.Any()) {
                    customData.ProteinPercentage = WeightedAverage(proteinProps);
                } else { customData.SugarPercentage = null; }
            }

            if (cropzoneCropId != null) {
                customData.CropId = cropzoneCropId.Id;
            }

            JsonSerializerSettings serializerSettings = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };
            var localCustomData = JsonConvert.SerializeObject(customData, serializerSettings);

            if (localCustomData == "{}") { localCustomData = null; }

            if (localCustomData != details.Inputs.CustomData) {
                log.Info("Found differences between local and remote custom data. Syncing...");
                synced = await sustainabilityClient.SetCustomData(ftmId.ClientId, ftmId.Crop, ftmId.FieldId, localCustomData);
            } else {
                log.Info($"Local custom data matches remote. {localCustomData}");
            }

            return synced;
        }

        double WeightedAverage(List<Tuple<double, double>> records) {
            double weightedValueSum = records.Sum(x => x.Item1 * x.Item2);
            double weightSum = records.Sum(x => x.Item1);

            if (weightSum != 0) { return weightedValueSum / weightSum; } else { return 0; }
        }
    }

    public class FtmId {
        public string Crop { get; set; }
        public string ClientId { get; set; }
        public string FieldId { get; set; }
    }

}
