﻿using AgC.UnitConversion;
using FieldToMarket.Contracts;
using FieldToMarket.Contracts.Reference;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Client.Models;
using Landdb.Client.Services;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Map;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Domain.ReadModels.Yield;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ReportModels.Analysis;
using Landdb.ViewModel.Secondary.Map;
using Landdb.ViewModel.Secondary.Reports.Analysis;
using NLog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using Landdb.Resources;
using Telerik.Reporting;

namespace Landdb.ViewModel.Analysis.FieldToMarket {
    public class FtmDetailsViewModel : ViewModelBase {
        IClientEndpoint clientEndpoint;
        Dispatcher dispatcher;
        FtmListItemViewModel listItemModel;
        SustainabilityServices sustainabilityServices;
        SustainabilityServiceClient sustainabilityClient;
        FtmSyncManager syncManager;
        FtmDetailsInputsViewModel inputModel;
        FtmDetailsWaterQualityViewModel waterModel;

        CropZoneId cropZoneId;
        string ftmCrop;
        FieldDetails latestDetails;
        ValidationSummary validationSummary;

        bool isLoading = true;
        bool isIrrigated;
        bool isShared;
        bool showMissingStateValueWarning = false;
        bool isBusy = false;
        Logger log = LogManager.GetCurrentClassLogger();

        public FtmDetailsViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, SustainabilityServices sustainabilityServices, SustainabilityServiceClient sustainabilityClient, FtmListItemViewModel listItemModel) {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            this.sustainabilityServices = sustainabilityServices;
            this.sustainabilityClient = sustainabilityClient;

            this.listItemModel = listItemModel;
            this.cropZoneId = listItemModel.CropZoneId;
            this.ftmCrop = listItemModel.FtmCrop;

            this.FieldName = listItemModel.Name;
            this.ContextString = listItemModel.ContextString;

            RefreshCommand = new RelayCommand(() => Initialize());
            RefreshNrcsCommand = new RelayCommand(() => RefreshNrcs());

            syncManager = new FtmSyncManager(clientEndpoint, sustainabilityServices, sustainabilityClient);
            inputModel = new FtmDetailsInputsViewModel(clientEndpoint, dispatcher, sustainabilityServices, sustainabilityClient, () => {
                Initialize(); // TODO: Could probably go with a less analyze-the-world method here, but for now, it'll do.
            });
            waterModel = new FtmDetailsWaterQualityViewModel(clientEndpoint, dispatcher, sustainabilityServices, sustainabilityClient, () => {
                // nothing (for now)
            });

            ViewDetailedCalculationsCommand = new RelayCommand(ViewDetailedCalculations);

            ShareCommand = new AwaitableRelayCommand(ShareField);
            UnshareCommand = new AwaitableRelayCommand(UnshareField);
            GenerateReportCommand = new AwaitableRelayCommand<Visifire.Charts.Chart>(GenerateReport);
        }

        public ICommand RefreshCommand { get; private set; }
        public ICommand RefreshNrcsCommand { get; private set; }
        public ICommand ViewDetailedCalculationsCommand { get; private set; }
        public ICommand GenerateReportCommand { get; private set; }

        public ICommand ShareCommand { get; private set; }
        public ICommand UnshareCommand { get; private set; }

        public bool IsLoading {
            get { return isLoading; }
            private set {
                isLoading = value;
                RaisePropertyChanged(() => IsLoading);
            }
        }

        public bool IsIrrigated {
            get { return isIrrigated; }
            private set {
                isIrrigated = value;
                RaisePropertyChanged(() => IsIrrigated);
            }
        }

        public bool IsShared {
            get { return isShared; }
            private set {
                isShared = value;
                RaisePropertyChanged(() => IsShared);
            }
        }

        public bool IsStateDataMissing {
            get { return showMissingStateValueWarning; }
            set {
                showMissingStateValueWarning = value;
                RaisePropertyChanged(() => IsStateDataMissing);
            }
        }

        string nrcsStatus;
        public string NrcsStatus {
            get { return nrcsStatus; }
            set {
                nrcsStatus = value;
                RaisePropertyChanged(() => NrcsStatus);
            }
        }

        bool isNrcsSuccessful;
        public bool IsNrcsSuccessful {
            get { return isNrcsSuccessful || isRefreshingNrcs; }
            set {
                isNrcsSuccessful = value;
                RaisePropertyChanged(nameof(IsNrcsSuccessful));
            }
        }

        bool isRefreshingNrcs;
        public bool IsRefreshingNrcs {
            get { return isRefreshingNrcs; }
            set {
                isRefreshingNrcs = value;
                RaisePropertyChanged(nameof(IsRefreshingNrcs));
                RaisePropertyChanged(nameof(IsNrcsSuccessful));
            }
        }


        bool isRusle2;
        public bool IsRusle2 {
            get { return isRusle2; }
            private set {
                isRusle2 = value;
                RaisePropertyChanged(() => IsRusle2);
            }
        }

        public async Task Initialize() {
            IsLoading = true;
            var dataSourceId = ApplicationEnvironment.CurrentDataSourceId;
            log.Info("Retrieving/Syncing FTM data for {0} {1} {2}", dataSourceId.ToString(), ftmCrop, cropZoneId.Id.ToString());

            try {
                latestDetails = await sustainabilityClient.GetFieldDetails(dataSourceId.ToString(), ftmCrop, cropZoneId.Id.ToString());
                var crop = await sustainabilityServices.GetCrop(ftmCrop);

                var localCzData = clientEndpoint.GetView<CropZoneDetailsView>(cropZoneId).GetValue(() => null);
                if (localCzData == null) { return; }
                var localFieldData = clientEndpoint.GetView<FieldDetailsView>(localCzData.FieldId).GetValue(() => null);
                if (localFieldData == null) { return; }

                IsIrrigated = localCzData.IrrigationInformation == null ? localFieldData.Irrigated.GetValueOrDefault(false) : localCzData.IrrigationInformation.Irrigated.GetValueOrDefault(false);

                var cropYearId = new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear);
                var applicationData = clientEndpoint
                    .GetView<CropZoneApplicationDataView>(cropYearId)
                    .GetValue(new CropZoneApplicationDataView())
                    .Items.Where(x => x.CropZoneId == cropZoneId).ToList();

                var yieldSummaryItems = SummaryServices.GetYieldData(clientEndpoint, cropYearId, this.cropZoneId);

                var needsRefresh = await syncManager.CompareAndSubmitChanges(ftmCrop, latestDetails, localCzData, localFieldData, applicationData, yieldSummaryItems, new CropYearId(dataSourceId, ApplicationEnvironment.CurrentCropYear) );
                inputModel.IsYieldEstimated = syncManager.IsYieldEstimated;

                if (needsRefresh) {
                    latestDetails = await sustainabilityClient.GetFieldDetails(dataSourceId.ToString(), ftmCrop, cropZoneId.Id.ToString());
                }
                validationSummary = await sustainabilityClient.GetFieldValidationSummary(dataSourceId.ToString(), ftmCrop, cropZoneId.Id.ToString());

                var state = clientEndpoint.GetMasterlistService().GetStateByName(localFieldData.State);
                string stateAbbreviation = state != null ? state.Abbreviation : null;

                if (!string.IsNullOrWhiteSpace(stateAbbreviation)) {
                    var stateValues = await sustainabilityServices.GetStateValuesForCrop(ftmCrop, stateAbbreviation);
                    UpdateStateValues(stateValues);
                }

                UpdateLocalValues(latestDetails, crop);
                await inputModel.RefreshFromDetails(latestDetails, this.cropZoneId, crop, validationSummary);
                await waterModel.RefreshFromDetails(latestDetails, cropZoneId, ApplicationEnvironment.CurrentCropYear);
                RaisePropertyChanged("InputModel");
                RaisePropertyChanged("WaterModel");

                BuildDataSeriesList();
            } catch (Exception ex) {
                log.ErrorException("Caught an exception while refreshing FTM data", ex);
            } finally {
                IsLoading = false;
            }
        }

        public async Task RefreshNrcs() {
            var dataSourceId = ApplicationEnvironment.CurrentDataSourceId;
            var success = await sustainabilityClient.RefreshNrcsData(dataSourceId.ToString(), cropZoneId.Id.ToString());
            log.Info($"Attempted to refresh NRCS data for {dataSourceId.ToString()} : {cropZoneId.Id.ToString()}. Success: {success.ToString()}");

            IsRefreshingNrcs = success;
            await Task.Run(async delegate {
                await Task.Delay(TimeSpan.FromSeconds(30));
                IsRefreshingNrcs = false;
                await Initialize();
            });
        }

        void UpdateLocalValues(FieldDetails details, Crop crop) {
            if (details == null || details.Results == null) { return; }

            LandUseValue = details.Results.LandUseValue.GetValueOrDefault();
            SoilConservationValue = details.Results.SoilConservationValue.GetValueOrDefault();
            SoilCarbonValue = details.Results.SoilCarbonValue.GetValueOrDefault();
            WaterUseValue = details.Results.WaterUseValue.GetValueOrDefault();
            EnergyUseValue = details.Results.EnergyUseValue.GetValueOrDefault();
            GreenhouseGasValue = details.Results.GreenhouseGasEmissionsValue.GetValueOrDefault();

            if (details.NRCSResponseValues != null) {
                NrcsStatus = details.NRCSResponseValues.IsSuccessful ? "Success" : "Failed";
                IsNrcsSuccessful = details.NRCSResponseValues.IsSuccessful;
            }

            //IsRusle2 = string.IsNullOrWhiteSpace(details.CalculationMethod) ? true : details.CalculationMethod == CalculationMethod.RUSLE2.ToString();
            IsRusle2 = crop != null && crop.CalculationMethod == CalculationMethod.RUSLE2;

            IsShared = details.IsShared;
        }

        void UpdateStateValues(CropStateValue stateValues) {
            if (IsIrrigated) {
                if (!stateValues.IrrigatedLandUseValue.HasValue || !stateValues.IrrigatedSoilConservation.HasValue || !stateValues.IrrigatedWaterUse.HasValue
                    || !stateValues.IrrigatedEnergyUse.HasValue || !stateValues.IrrigatedGreenhouseGasEmissions.HasValue) {
                        IsStateDataMissing = true;
                } else { IsStateDataMissing = false; }

                StateLandUseValue = stateValues.IrrigatedLandUseValue;
                StateSoilConservationValue = stateValues.IrrigatedSoilConservation;
                StateSoilCarbonValue = 0;
                StateWaterUseValue = stateValues.IrrigatedWaterUse;
                StateEnergyUseValue = stateValues.IrrigatedEnergyUse;
                StateGreenhouseGasValue = stateValues.IrrigatedGreenhouseGasEmissions;
            } else {
                if (!stateValues.NonIrrigatedLandUseValue.HasValue || !stateValues.NonIrrigatedSoilConservation.HasValue /* || !stateValues.NonIrrigatedWaterUse.HasValue */ //Don't care for non-irrigated that water doesn't have a value.
                    || !stateValues.NonIrrigatedEnergyUse.HasValue || !stateValues.NonIrrigatedGreenhouseGasEmissions.HasValue) {
                        IsStateDataMissing = true;
                } else { IsStateDataMissing = false; }

                StateLandUseValue = stateValues.NonIrrigatedLandUseValue;
                StateSoilConservationValue = stateValues.NonIrrigatedSoilConservation;
                StateSoilCarbonValue = 0;
                StateWaterUseValue = stateValues.NonIrrigatedWaterUse;
                StateEnergyUseValue = stateValues.NonIrrigatedEnergyUse;
                StateGreenhouseGasValue = stateValues.NonIrrigatedGreenhouseGasEmissions;
            }
        }

        string fieldName;
        string contextString;

        public string FieldName {
            get { return fieldName; }
            set {
                fieldName = value;
                RaisePropertyChanged(() => FieldName);
            }
        }

        public string ContextString {
            get { return contextString; }
            set {
                contextString = value;
                RaisePropertyChanged(() => ContextString);
            }
        }

        public FtmDetailsInputsViewModel InputModel {
            get { return inputModel; }
        }

        public FtmDetailsWaterQualityViewModel WaterModel {
            get { return waterModel; }
        }

        #region Fieldprint Values
        double landUseValue;
        double soilConservationValue;
        double soilCarbonValue;
        double waterUseValue;
        double energyUseValue;
        double greenhouseGasValue;

        double? stateLandUseValue;
        double? stateSoilConservationValue;
        double? stateSoilCarbonValue;
        double? stateWaterUseValue;
        double? stateEnergyUseValue;
        double? stateGreenhouseGasValue;

        public double LandUseValue {
            get { return landUseValue; }
            set {
                landUseValue = value;
                RaisePropertyChanged(() => LandUseValue);
                RaisePropertyChanged(() => LandUseValueDisplay);
                RaisePropertyChanged(() => NormalizedLandUseValue);
                RaisePropertyChanged(() => DeviationLandUseDisplay);
            }
        }
        public double SoilConservationValue {
            get { return soilConservationValue; }
            set {
                soilConservationValue = value;
                RaisePropertyChanged(() => SoilConservationValue);
                RaisePropertyChanged(() => SoilConservationValueDisplay);
                RaisePropertyChanged(() => NormalizedSoilConservationValue);
                RaisePropertyChanged(() => DeviationSoilConservationDisplay);
            }
        }
        public double SoilCarbonValue {
            get { return soilCarbonValue; }
            set {
                soilCarbonValue = value;
                RaisePropertyChanged(() => SoilCarbonValue);
                RaisePropertyChanged(() => SoilCarbonValueDisplay);
                RaisePropertyChanged(() => NormalizedSoilCarbonValue);
                RaisePropertyChanged(() => DeviationSoilCarbonDisplay);
            }
        }
        public double WaterUseValue {
            get { return waterUseValue; }
            set {
                waterUseValue = value;
                RaisePropertyChanged(() => WaterUseValue);
                RaisePropertyChanged(() => WaterUseValueDisplay);
                RaisePropertyChanged(() => NormalizedWaterUseValue);
                RaisePropertyChanged(() => DeviationWaterUseDisplay);
            }
        }
        public double EnergyUseValue {
            get { return energyUseValue; }
            set {
                energyUseValue = value;
                RaisePropertyChanged(() => EnergyUseValue);
                RaisePropertyChanged(() => EnergyUseValueDisplay);
                RaisePropertyChanged(() => NormalizedEnergyUseValue);
                RaisePropertyChanged(() => DeviationEnergyUseDisplay);
            }
        }
        public double GreenhouseGasValue {
            get { return greenhouseGasValue; }
            set {
                greenhouseGasValue = value;
                RaisePropertyChanged(() => GreenhouseGasValue);
                RaisePropertyChanged(() => GreenhouseGasValueDisplay);
                RaisePropertyChanged(() => NormalizedGreenhouseGasValue);
                RaisePropertyChanged(() => DeviationGreenhouseGasDisplay);
            }
        }

        public string LandUseValueDisplay {
            get { return LandUseValue.ToString("n4"); }
        }
        public string SoilConservationValueDisplay {
            get { return SoilConservationValue.ToString("n4"); }
        }
        public string SoilCarbonValueDisplay {
            get { return SoilCarbonValue.ToString("n4"); }
        }
        public string WaterUseValueDisplay {
            get { return WaterUseValue.ToString("n4"); }
        }
        public string EnergyUseValueDisplay {
            get { return EnergyUseValue.KiloFormat(); }
        }
        public string GreenhouseGasValueDisplay {
            get { return GreenhouseGasValue.KiloFormat(); }
        }

        public double? StateLandUseValue {
            get { return stateLandUseValue; }
            set {
                stateLandUseValue = value;
                RaisePropertyChanged(() => StateLandUseValue);
                RaisePropertyChanged(() => StateLandUseValueDisplay);
                RaisePropertyChanged(() => NormalizedLandUseValue);
                RaisePropertyChanged(() => DeviationLandUseDisplay);
            }
        }
        public double? StateSoilConservationValue {
            get { return stateSoilConservationValue; }
            set {
                stateSoilConservationValue = value;
                RaisePropertyChanged(() => StateSoilConservationValue);
                RaisePropertyChanged(() => StateSoilConservationValueDisplay);
                RaisePropertyChanged(() => NormalizedSoilConservationValue);
                RaisePropertyChanged(() => DeviationSoilConservationDisplay);
            }
        }
        public double? StateSoilCarbonValue {
            get { return stateSoilCarbonValue; }
            set {
                stateSoilCarbonValue = value;
                RaisePropertyChanged(() => StateSoilCarbonValue);
                RaisePropertyChanged(() => StateSoilCarbonValueDisplay);
                RaisePropertyChanged(() => NormalizedSoilCarbonValue);
                RaisePropertyChanged(() => DeviationSoilCarbonDisplay);
            }
        }
        public double? StateWaterUseValue {
            get { return stateWaterUseValue; }
            set {
                stateWaterUseValue = value;
                RaisePropertyChanged(() => StateWaterUseValue);
                RaisePropertyChanged(() => StateWaterUseValueDisplay);
                RaisePropertyChanged(() => NormalizedWaterUseValue);
                RaisePropertyChanged(() => DeviationWaterUseDisplay);
            }
        }
        public double? StateEnergyUseValue {
            get { return stateEnergyUseValue; }
            set {
                stateEnergyUseValue = value;
                RaisePropertyChanged(() => StateEnergyUseValue);
                RaisePropertyChanged(() => StateEnergyUseValueDisplay);
                RaisePropertyChanged(() => NormalizedEnergyUseValue);
                RaisePropertyChanged(() => DeviationEnergyUseDisplay);
            }
        }
        public double? StateGreenhouseGasValue {
            get { return stateGreenhouseGasValue; }
            set {
                stateGreenhouseGasValue = value;
                RaisePropertyChanged(() => StateGreenhouseGasValue);
                RaisePropertyChanged(() => StateGreenhouseGasValueDisplay);
                RaisePropertyChanged(() => NormalizedGreenhouseGasValue);
                RaisePropertyChanged(() => DeviationGreenhouseGasDisplay);
            }
        }

        public string StateLandUseValueDisplay {
            get { return StateLandUseValue.HasValue ? StateLandUseValue.Value.ToString("n4") : "---"; }
        }
        public string StateSoilConservationValueDisplay {
            get { return StateSoilConservationValue.HasValue ? StateSoilConservationValue.Value.ToString("n4") : "---"; }
        }
        public string StateSoilCarbonValueDisplay {
            get { return StateSoilCarbonValue.HasValue ? StateSoilCarbonValue.Value.ToString("n4") : "---"; }
        }
        public string StateWaterUseValueDisplay {
            get { return StateWaterUseValue.HasValue ? StateWaterUseValue.Value.ToString("n4") : "---"; }
        }
        public string StateEnergyUseValueDisplay {
            get { return StateEnergyUseValue.HasValue ? StateEnergyUseValue.Value.KiloFormat() : "---"; }
        }
        public string StateGreenhouseGasValueDisplay {
            get { return StateGreenhouseGasValue.HasValue ? StateGreenhouseGasValue.Value.KiloFormat() : "---"; }
        }

        public double NormalizedLandUseValue {
            get {
                if (StateLandUseValue.GetValueOrDefault() == 0) { return 0; }
                var val = (LandUseValue / (2 * StateLandUseValue.GetValueOrDefault())) * 100;
                if (val < 0) { return 0; } if (val > 100) { return 100; }
                return val;
            }
        }
        public double NormalizedSoilConservationValue {
            get {
                if (StateSoilConservationValue.GetValueOrDefault() == 0) { return 0; }
                var val = (SoilConservationValue / (2 * StateSoilConservationValue.GetValueOrDefault())) * 100;
                if (val < 0) { return 0; } if (val > 100) { return 100; }
                return val;
            }
        }
        public double NormalizedSoilCarbonValue {
            get {
                var val = (1 - SoilCarbonValue) * 50;  /*(SoilCarbonValue / (2 * StateSoilCarbonValue)) * 100;*/
                if (val < 0) { return 0; } if (val > 100) { return 100; }
                return val;
            }
        }
        public double NormalizedWaterUseValue {
            get {
                if (StateWaterUseValue.GetValueOrDefault() == 0) { return 0; }
                var val = (WaterUseValue / (2 * StateWaterUseValue.GetValueOrDefault())) * 100;
                if (val < 0) { return 0; } if (val > 100) { return 100; }
                return val;
            }
        }
        public double NormalizedEnergyUseValue {
            get {
                if (StateEnergyUseValue.GetValueOrDefault() == 0) { return 0; }
                var val = (EnergyUseValue / (2 * StateEnergyUseValue.GetValueOrDefault())) * 100;
                if (val < 0) { return 0; } if (val > 100) { return 100; }
                return val;
            }
        }
        public double NormalizedGreenhouseGasValue {
            get {
                if (StateGreenhouseGasValue.GetValueOrDefault() == 0) { return 0; }
                var val = (GreenhouseGasValue / (2 * StateGreenhouseGasValue.GetValueOrDefault())) * 100;
                if (val < 0) { return 0; } if (val > 100) { return 100; }
                return val;
            }
        }

        public string DeviationLandUseDisplay {
            get {
                if (!StateLandUseValue.HasValue) { return "---"; }
                return (-0.02 * (NormalizedLandUseValue - 50)).ToString("+#0%;-#0%;---"); 
            }
        }
        public string DeviationSoilConservationDisplay {
            get {
                if (!StateSoilConservationValue.HasValue) { return "---"; }
                return (-0.02 * (NormalizedSoilConservationValue - 50)).ToString("+#0%;-#0%;---"); 
            }
        }
        public string DeviationSoilCarbonDisplay {
            get {
                if (!StateSoilCarbonValue.HasValue) { return "---"; }
                return (-0.02 * (NormalizedSoilCarbonValue - 50)).ToString("+#0%;-#0%;---"); 
            }
        }
        public string DeviationWaterUseDisplay {
            get {
                if (!StateWaterUseValue.HasValue) { return "---"; }
                return (-0.02 * (NormalizedWaterUseValue - 50)).ToString("+#0%;-#0%;---"); 
            }
        }
        public string DeviationEnergyUseDisplay {
            get {
                if (!StateEnergyUseValue.HasValue) { return "---"; }
                return (-0.02 * (NormalizedEnergyUseValue - 50)).ToString("+#0%;-#0%;---"); 
            }
        }
        public string DeviationGreenhouseGasDisplay {
            get {
                if (!StateGreenhouseGasValue.HasValue) { return "---"; }
                return (-0.02 * (NormalizedGreenhouseGasValue - 50)).ToString("+#0%;-#0%;---"); 
            }
        }


        void BuildDataSeriesList() {
            ObservableCollection<FieldPrintDataItem> fieldData = new ObservableCollection<FieldPrintDataItem>();
            fieldData.Add(new FieldPrintDataItem("Land", NormalizedLandUseValue));
            fieldData.Add(new FieldPrintDataItem("Conservation", NormalizedSoilConservationValue));
            if (IsRusle2) {
                fieldData.Add(new FieldPrintDataItem("Soil Carbon", NormalizedSoilCarbonValue));
            }
            if (IsIrrigated) {
                fieldData.Add(new FieldPrintDataItem("Water", NormalizedWaterUseValue));
            }
            fieldData.Add(new FieldPrintDataItem("Energy", NormalizedEnergyUseValue));
            fieldData.Add(new FieldPrintDataItem("GHG", NormalizedGreenhouseGasValue));
            FieldDataSeries = fieldData;

            ObservableCollection<FieldPrintDataItem> stateData = new ObservableCollection<FieldPrintDataItem>();
            stateData.Add(new FieldPrintDataItem("Land", 50));
            stateData.Add(new FieldPrintDataItem("Conservation", 50));
            if (IsRusle2) {
                stateData.Add(new FieldPrintDataItem("Soil Carbon", 50));
            }
            if (IsIrrigated) {
                stateData.Add(new FieldPrintDataItem("Water", 50));
            }
            stateData.Add(new FieldPrintDataItem("Energy", 50));
            stateData.Add(new FieldPrintDataItem("GHG", 50));
            StateDataSeries = stateData;

            ObservableCollection<FieldPrintDataItem> deviationData = new ObservableCollection<FieldPrintDataItem>();
            deviationData.Add(new FieldPrintDataItem("GHG", -2 * (NormalizedGreenhouseGasValue - 50)));
            deviationData.Add(new FieldPrintDataItem("Energy", -2 * (NormalizedEnergyUseValue - 50)));
            if (IsIrrigated) {
                deviationData.Add(new FieldPrintDataItem("Water", -2 * (NormalizedWaterUseValue - 50)));
            }
            if (IsRusle2) {
                deviationData.Add(new FieldPrintDataItem("Soil Carbon", -2 * (NormalizedSoilCarbonValue - 50)));
            }
            deviationData.Add(new FieldPrintDataItem("Conservation", -2 * (NormalizedSoilConservationValue - 50)));
            deviationData.Add(new FieldPrintDataItem("Land", -2 * (NormalizedLandUseValue - 50)));
            DeviationDataSeries = deviationData;
        }

        ObservableCollection<FieldPrintDataItem> fieldDataSeries;
        public ObservableCollection<FieldPrintDataItem> FieldDataSeries {
            get { return fieldDataSeries; }
            set {
                fieldDataSeries = value;
                RaisePropertyChanged(() => FieldDataSeries);
            }
        }

        ObservableCollection<FieldPrintDataItem> stateDataSeries;
        public ObservableCollection<FieldPrintDataItem> StateDataSeries {
            get { return stateDataSeries; }
            set {
                stateDataSeries = value;
                RaisePropertyChanged(() => StateDataSeries);
            }
        }

        ObservableCollection<FieldPrintDataItem> deviationDataSeries;
        public ObservableCollection<FieldPrintDataItem> DeviationDataSeries {
            get { return deviationDataSeries; }
            set {
                deviationDataSeries = value;
                RaisePropertyChanged(() => DeviationDataSeries);
            }
        }
        #endregion


        void ViewDetailedCalculations() {
            ScreenDescriptor sd = new ScreenDescriptor("Landdb.Views.Analysis.FieldToMarket.FtmDetailedCalculationsView", "detailedCalculations", latestDetails);
            Messenger.Default.Send<ShowOverlayMessage>(new ShowOverlayMessage() { ScreenDescriptor = sd });
        }

        async Task ShareField() {
            if (latestDetails == null) { return; }
            if (isBusy) { return; }

            if (!waterModel.IsCompleteAndSubmitted) {
                DialogFactory.ShowMessage(Strings.ConservationSurveyRequired_Text, Strings.CompletedConservationSurveyIsRequired_Text);
                return;
            }

            if (inputModel.HasBlockingMessages) {
                DialogFactory.ShowMessage(Strings.MissingOrInvalidInputs_Text, Strings.ThereAppearToBeMissingOrInvalidData_Text);
                return;
            }

            isBusy = true;
            await DialogFactory.ShowYesNoDialogAsync(Strings.ShareThisField_Text, Strings.DoYouWantToShareThisFieldsInformation_Text, 
                async () => {
                    try {
                        var shareResult = await sustainabilityClient.ShareField(latestDetails.Registration.Id.ClientId, latestDetails.Registration.Crop, latestDetails.Registration.Id.FieldId);
                        if (shareResult) {
                            IsShared = true;
                            listItemModel.IsShared = true;
                            Messenger.Default.Send(new FtmItemChangedMessage() { LastFtmItemEdited = listItemModel.CropZoneId });
                        }
                        
                    } finally {
                        isBusy = false;
                        Messenger.Default.Send(new FtmItemChangedMessage() { LastFtmItemEdited = listItemModel.CropZoneId });
                    }

                }, async () => { isBusy = false; });

            
        }

        async Task UnshareField() {
            if (latestDetails == null) { return; }
            if (isBusy) { return; }
            isBusy = true;
            await DialogFactory.ShowYesNoDialogAsync(Strings.UnshareThisField_Text, Strings.DoYouWantToRemoveThisFieldsInformation_Text,
                async () => {
                    try {
                        var shareResult = await sustainabilityClient.UnshareField(latestDetails.Registration.Id.ClientId, latestDetails.Registration.Crop, latestDetails.Registration.Id.FieldId);
                        if (shareResult) {
                            IsShared = false;
                            listItemModel.IsShared = false;
                            Messenger.Default.Send(new FtmItemChangedMessage() { LastFtmItemEdited = listItemModel.CropZoneId });
                        }
                    } finally {
                        isBusy = false;
                        Messenger.Default.Send(new FtmItemChangedMessage() { LastFtmItemEdited = listItemModel.CropZoneId });
                    }
                }, async () => { isBusy = false; });

            
        }

        async Task GenerateReport(Visifire.Charts.Chart currentChart)
        {

            var vm = new FtmReportsViewModel(clientEndpoint, this, currentChart, cropZoneId);
            Messenger.Default.Send<ShowOverlayMessage>(new ShowOverlayMessage()
            {
                ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Secondary.Reports.Work_Order.SingleWorkOrderView", "viewReport", vm)
            });

        }
    }

    public class FieldPrintSeries {
        public string Name { get; set; }
        public List<FieldPrintDataItem> Values { get; set; }
    }

    public class FieldPrintDataItem {
        public FieldPrintDataItem(string axisLabel, double normalizedValue) {
            this.AxisLabel = axisLabel;
            this.NormalizedValue = normalizedValue;
        }
        public string AxisLabel { get; set; }
        public double NormalizedValue { get; set; }
        public Brush Color {
            get {
                if (NormalizedValue < 0) { return Brushes.Goldenrod; } else { return Brushes.Green; }
            }
        }
    }
}
