﻿using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Client.Services;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Secondary.Analysis.FieldToMarket;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Analysis.FieldToMarket {
    public class FtmPageViewModel : AbstractListPage<FtmListItemViewModel, FtmDetailsViewModel> {
        SustainabilityServices sustainabilityServices = new SustainabilityServices(ApplicationEnvironment.FieldToMarketRemoteUri);
        SustainabilityServiceClient sustainabilityClient = new SustainabilityServiceClient();
        Logger log = LogManager.GetCurrentClassLogger();

        public FtmPageViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher) : base(clientEndpoint, dispatcher) {
            this.EnableAutoRefresh = true;

            // This will force a full rebuild of the details item and prevent an "infinite-load" bug that persists until a new item is selected.
            this.DefaultPostRefreshAction = (() => { SelectedListItem = null; });

            Messenger.Default.Register<FtmItemChangedMessage>(this, msg =>
            {
                RefreshItemsList(() =>
                {
                    var q = from li in ListItems
                            where li.CropZoneId == msg.LastFtmItemEdited
                            select li;
                    SelectedListItem = q.FirstOrDefault();
                    RaisePropertyChanged(() => SelectedListItem);
                });
            });

            RemoveFtmItemCommand = new AwaitableRelayCommand(RemoveSelectedFtmItem);
        }

        public ICommand RemoveFtmItemCommand { get; private set; }

        private bool isLoadingItems;

        public bool IsLoadingItems {
            get { return isLoadingItems; }
            set { 
                isLoadingItems = value;
                RaisePropertyChanged(() => IsLoadingItems);
            }
        }


        protected override IEnumerable<FtmListItemViewModel> GetListItemModels() {
            try {
                IsLoadingItems = true;
                log.Info("FTM: Getting field list for {0} - {1}", currentDataSourceId.Id.ToString(), ApplicationEnvironment.CurrentCropYear);
                //var remoteListTask = sustainabilityServices.GetFieldList(currentDataSourceId.Id.ToString(), ApplicationEnvironment.CurrentCropYear);
                //remoteListTask.Wait();

                var remoteListTask = sustainabilityClient.GetFieldList(currentDataSourceId.Id.ToString(), ApplicationEnvironment.CurrentCropYear);
                remoteListTask.Wait();

                var remoteList = remoteListTask.Result;
                log.Info("FTM: Field list for {0} - {1} contains {2} items", currentDataSourceId.Id.ToString(), ApplicationEnvironment.CurrentCropYear, remoteList.Count());

                //var settings = clientEndpoint.GetPersistentView<FtmSettingsModel>(new CropYearId(currentDataSourceId.Id, currentCropYear)).GetValue(new FtmSettingsModel());
                var flattenedHierarchy = clientEndpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(currentDataSourceId.Id, currentCropYear)).GetValue(new FlattenedTreeHierarchyView());

                var items = from cz in remoteList
                            let fth = flattenedHierarchy.Items.Where(x => x.CropZoneId != null && x.CropZoneId.Id.ToString() == cz.FieldId).FirstOrDefault()
                            where fth != null
                            select new FtmListItemViewModel() {
                                CropZoneId = new CropZoneId(currentDataSourceId.Id, Guid.Parse(cz.FieldId)),
                                Name = string.Format("{0} \\ {1} \\ {2}", fth.FarmName, fth.FieldName, fth.CropZoneName),
                                ContextString = cz.CropName,
                                FtmCrop = cz.CropName,
                                IsShared = cz.IsShared
                            };
                return items;
            } catch (Exception ex) {
                log.ErrorException("FTM: Had a problem getting the field list.", ex);
                return new List<FtmListItemViewModel>();
            } finally {
                IsLoadingItems = false;
            }
        }

        protected override async Task<ScreenDescriptor> CreateScreenDescriptorForNewItemCreation() {
            var vm = await FtmAddFieldsViewModel.Create(clientEndpoint, dispatcher, ApplicationEnvironment.CurrentCropYear);
            ScreenDescriptor descriptor = new ScreenDescriptor("Landdb.Views.Secondary.Analysis.FieldToMarket.FtmAddFieldsView", "add ftm fields", vm);
            return descriptor;
        }

        public override bool CanDelete(FtmListItemViewModel toDelete) {
            return false;
        }

        protected override bool PerformItemRemoval(FtmListItemViewModel selectedItem) {
            throw new NotImplementedException();
        }

        protected override IDomainCommand CreateSetCharmCommand(FtmListItemViewModel selectedItem, string charmName) {
            throw new NotImplementedException();
        }

        protected override FtmDetailsViewModel CreateDetailsModel(FtmListItemViewModel selectedItem) {
            if (selectedItem == null) { return null; }

            var vm = new FtmDetailsViewModel(clientEndpoint, dispatcher, sustainabilityServices, sustainabilityClient, selectedItem);
            vm.Initialize();
            return vm;
        }

        public override string GetEntityName() { return Strings.Fieldprint_Text.ToLower(); }

        public override string GetPluralEntityName() {
            return Strings.Fieldprints_Text.ToLower();
        }

        private async Task RemoveSelectedFtmItem() {
            if (SelectedListItem == null) { return; }
            if (!CanDelete(SelectedListItem)) { return; } // TODO: Show popup indicating that the item cannot be deleted

            await DialogFactory.ShowYesNoDialogAsync(string.Format(Strings.Remove_Format_Text, GetEntityName()), string.Format(Strings.AreYouSureYouWantToRemoveThis_Text, GetEntityName()), async () => {
                await RemoveFtmItem(SelectedListItem);
                
                int index = ListItems.IndexOf(SelectedListItem) - 1;
                if (index < 0) { index = 0; }

                await dispatcher.BeginInvoke(new Action(() => {
                    ListItems.Remove(SelectedListItem);
                    if (ListItems.Any()) {
                        SelectedListItem = ListItems[index];
                    } else {
                        SelectedListItem = default(FtmListItemViewModel);
                    }
                }));
            }, async () => { await Task.FromResult<object>(null); });

        }

        private async Task RemoveFtmItem(FtmListItemViewModel selectedItem) {
            var cropYearId = new CropYearId(currentDataSourceId.Id, currentCropYear);
            var settings = clientEndpoint.GetPersistentView<FtmSettingsModel>(cropYearId).GetValue(new FtmSettingsModel());
            var deletedFromServer = await sustainabilityClient.DeleteField(currentDataSourceId.Id.ToString(), selectedItem.FtmCrop, selectedItem.CropZoneId.Id.ToString());

            if (deletedFromServer) {
                settings.CropZones.RemoveAll(x => x.CropZoneId == selectedItem.CropZoneId && x.Crop == selectedItem.FtmCrop);
                clientEndpoint.SavePersistentEntity<FtmSettingsModel>(cropYearId, settings);
            } else {
                // TODO: Message the user
            }
        }
    }
}
