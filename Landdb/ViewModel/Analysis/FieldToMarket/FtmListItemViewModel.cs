﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Analysis.FieldToMarket {
    public class FtmListItemViewModel : ViewModelBase {

        private CropZoneId cropZoneId;
        private string name;
        private string contextString;
        private string ftmCrop;
        private bool isShared;

        public CropZoneId CropZoneId {
            get { return cropZoneId; }
            set { 
                cropZoneId = value;
                RaisePropertyChanged(() => CropZoneId);
            }
        }

        public string Name {
            get { return name; }
            set { 
                name = value;
                RaisePropertyChanged(() => Name);
            }
        }

        public string ContextString {
            get { return contextString; }
            set { 
                contextString = value;
                RaisePropertyChanged(() => ContextString);
            }
        }

        public string FtmCrop {
            get { return ftmCrop; }
            set {
                ftmCrop = value;
                RaisePropertyChanged(() => FtmCrop);
            }
        }

        public bool IsShared {
            get { return isShared; }
            set {
                isShared = value;
                RaisePropertyChanged(() => IsShared);
            }
        }

        public void ItemUpdated()
        {
            RaisePropertyChanged(() => IsShared);
        }

    }
}
