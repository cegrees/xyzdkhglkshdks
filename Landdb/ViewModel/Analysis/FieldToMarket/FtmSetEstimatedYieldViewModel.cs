﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using Landdb.Client.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;

namespace Landdb.ViewModel.Analysis.FieldToMarket {
    public class FtmSetEstimatedYieldViewModel : ViewModelBase {
        IClientEndpoint clientEndpoint;
        Dispatcher dispatcher;
        SustainabilityServices sustainabilityServices;
        SustainabilityServiceClient sustainabilityClient;
        FtmId ftmId;
        double estimatedYield;
        string yieldUnit;
        Action onViewClose;

        public FtmSetEstimatedYieldViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, SustainabilityServices sustainabilityServices, SustainabilityServiceClient sustainabilityClient, FtmId ftmId, double estimatedYield, Action onViewClose) {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            this.sustainabilityServices = sustainabilityServices;
            this.sustainabilityClient = sustainabilityClient;
            this.ftmId = ftmId;
            this.estimatedYield = estimatedYield;
            this.onViewClose = onViewClose;

            SubmitCommand = new AwaitableRelayCommand(Submit);
            CancelCommand = new RelayCommand(onViewClose);
        }

        public static async Task<FtmSetEstimatedYieldViewModel> Create(IClientEndpoint clientEndpoint, Dispatcher dispatcher, SustainabilityServices sustainabilityServices, SustainabilityServiceClient sustainabilityClient, FtmId ftmId, double estimatedYield, Action onViewClose) {
            var vm = new FtmSetEstimatedYieldViewModel(clientEndpoint, dispatcher, sustainabilityServices, sustainabilityClient, ftmId, estimatedYield, onViewClose);
            await vm.LoadCropUnit();
            return vm;
        }

        async Task LoadCropUnit() {
            var crops = await sustainabilityServices.GetCrops();
            var crop = crops.Where(x => x.CropName == ftmId.Crop).FirstOrDefault();
            if (crop != null) {
                yieldUnit = crop.UnitOfMeasure;
                RaisePropertyChanged(() => YieldUnit);
            }
        }

        public ICommand SubmitCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }

        public double EstimatedYield {
            get { return estimatedYield; }
            set {
                estimatedYield = value;
                RaisePropertyChanged(() => EstimatedYield);
            }
        }

        public string YieldUnit {
            get { return yieldUnit; }
            set {
                yieldUnit = value;
                RaisePropertyChanged(() => YieldUnit);
            }
        }

        async Task Submit() {
            var synced = await sustainabilityClient.SetYield(ftmId.ClientId, ftmId.Crop, ftmId.FieldId, EstimatedYield);
            onViewClose();
        }

    }
}
