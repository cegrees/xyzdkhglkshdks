﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using Landdb.Client.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using Ftm = FieldToMarket;

namespace Landdb.ViewModel.Analysis.FieldToMarket {
    public class FtmSubmitUsleInputsViewModel : ViewModelBase {
        IClientEndpoint clientEndpoint;
        Dispatcher dispatcher;
        SustainabilityServices sustainabilityServices;
        SustainabilityServiceClient sustainabilityClient;
        FieldInputs currentInputs;
        FtmId ftmId;
        Action onViewClose;

        private FtmSubmitUsleInputsViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, SustainabilityServices sustainabilityServices, SustainabilityServiceClient sustainabilityClient, FtmId ftmId, FieldInputs currentInputs, Action onViewClose) {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            this.sustainabilityServices = sustainabilityServices;
            this.sustainabilityClient = sustainabilityClient;
            this.ftmId = ftmId;
            this.currentInputs = currentInputs;
            this.onViewClose = onViewClose;

            SubmitCommand = new AwaitableRelayCommand(Submit);
            CancelCommand = new RelayCommand(onViewClose);
        }

        public static async Task<FtmSubmitUsleInputsViewModel> Create(IClientEndpoint clientEndpoint, Dispatcher dispatcher, SustainabilityServices sustainabilityServices, SustainabilityServiceClient sustainabilityClient, FtmId ftmId, FieldInputs currentInputs, Action onViewClose) {
            var vm = new FtmSubmitUsleInputsViewModel(clientEndpoint, dispatcher, sustainabilityServices, sustainabilityClient, ftmId, currentInputs, onViewClose);
            await vm.EnsureUsleCollectionsAreLoaded();
            if (currentInputs != null) {
                vm.SelectedTillageSystem = currentInputs.USLE_TillageSystem;
                vm.SelectedSoilTexture = currentInputs.USLE_SoilTexture;
                vm.UsleOperationalDieselUse = currentInputs.USLE_DieselUsePerArea.GetValueOrDefault();
                vm.SelectedUslePreviousCrop = currentInputs.USLE_PreviousCrop;
                vm.SelectedUsleWindSeverity = currentInputs.USLE_WindErosionSeverity;

                foreach (var cp in vm.ConservationPractices ?? new ObservableCollection<Landdb.ViewModel.Secondary.Analysis.FieldToMarket.ConservationPracticeViewModel>()) {
                    cp.IsSelected = currentInputs.USLE_ConservationPractices.Contains(cp.Name);
                }
            }
            return vm;
        }

        public ICommand SubmitCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }

        #region USLE
        public ObservableCollection<string> TillageClasses { get; private set; }
        public ObservableCollection<Landdb.ViewModel.Secondary.Analysis.FieldToMarket.ConservationPracticeViewModel> ConservationPractices { get; private set; }
        public ObservableCollection<string> SoilTextures { get; private set; }
        public ObservableCollection<string> UslePreviousCrops { get; private set; }
        public ObservableCollection<string> UsleWindSeverities { get; private set; }


        string selectedTillageSystem;
        public string SelectedTillageSystem {
            get { return selectedTillageSystem; }
            set {
                selectedTillageSystem = value;
                RaisePropertyChanged(() => SelectedTillageSystem);
            }
        }

        string selectedSoilTexture;
        public string SelectedSoilTexture {
            get { return selectedSoilTexture; }
            set {
                selectedSoilTexture = value;
                RaisePropertyChanged(() => SelectedSoilTexture);
            }
        }

        double usleOperationalDieselUse;
        public double UsleOperationalDieselUse {
            get { return usleOperationalDieselUse; }
            set {
                usleOperationalDieselUse = value;
                RaisePropertyChanged(() => UsleOperationalDieselUse);
            }
        }

        private string selectedUslePreviousCrop;
        public string SelectedUslePreviousCrop {
            get { return selectedUslePreviousCrop; }
            set {
                selectedUslePreviousCrop = value;
                RaisePropertyChanged(() => SelectedUslePreviousCrop);
            }
        }

        private string selectedUsleWindSeverity;
        public string SelectedUsleWindSeverity {
            get { return selectedUsleWindSeverity; }
            set {
                selectedUsleWindSeverity = value;
                RaisePropertyChanged(() => SelectedUsleWindSeverity);
            }
        }

        async Task EnsureUsleCollectionsAreLoaded() {
            if (TillageClasses == null || !TillageClasses.Any()) {
                var tillageClasses = await sustainabilityServices.GetTillageClasses();
                TillageClasses = new ObservableCollection<string>(tillageClasses);
                RaisePropertyChanged(() => TillageClasses);
            }

            if (SoilTextures == null || !SoilTextures.Any()) {
                var soilTextures = await sustainabilityServices.GetSoilTextures();
                SoilTextures = new ObservableCollection<string>(soilTextures);
                RaisePropertyChanged(() => SoilTextures);
            }
            if (ConservationPractices == null || !ConservationPractices.Any()) {
                var conservationPractices = await sustainabilityServices.GetConservationPractices();
                ConservationPractices = new ObservableCollection<Landdb.ViewModel.Secondary.Analysis.FieldToMarket.ConservationPracticeViewModel>();
                foreach (var cp in conservationPractices) {
                    ConservationPractices.Add(new Landdb.ViewModel.Secondary.Analysis.FieldToMarket.ConservationPracticeViewModel(cp));
                }
                RaisePropertyChanged(() => ConservationPractices);
            }
            if (UslePreviousCrops == null || !UslePreviousCrops.Any()) {
                var previousCrops = await sustainabilityServices.GetPreviousCrops();
                UslePreviousCrops = new ObservableCollection<string>(previousCrops);
                RaisePropertyChanged(() => UslePreviousCrops);
            }
            if (UsleWindSeverities == null || !UsleWindSeverities.Any()) {
                var windSeverities = await sustainabilityServices.GetWindSeverities();
                UsleWindSeverities = new ObservableCollection<string>(windSeverities);
                RaisePropertyChanged(() => UsleWindSeverities);
            }
        }


        #endregion

        async Task Submit() {
            List<string> selectedCp = null;
            if (ConservationPractices != null) {
                selectedCp = ConservationPractices.Where(x => x.IsSelected).Select(x => x.Name).ToList();
            } else { selectedCp = new List<string>(); }

            var usleInputs = new UsleInputs() {
                OperationalDieselUsePerArea = this.UsleOperationalDieselUse,
                PreviousCrop = this.SelectedUslePreviousCrop,
                SoilTexture = this.SelectedSoilTexture,
                TillageSystem = this.SelectedTillageSystem,
                WindErosionSeverity = this.SelectedUsleWindSeverity,
                ConservationPractices = selectedCp
            };

            var synced = await sustainabilityClient.SetUSLEInputs(ftmId.ClientId, ftmId.Crop, ftmId.FieldId, usleInputs);

            onViewClose();
        }
    }
}
