﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Analysis.FieldToMarket {
    [DataContract]
    public class FtmSettingsModel {
        public FtmSettingsModel() {
            CropZones = new List<FtmSettingsItem>();
        }

        [DataMember(Order=1)]
        public List<FtmSettingsItem> CropZones { get; set; }
    }

    [DataContract]
    public class FtmSettingsItem {
        [DataMember(Order=1)]
        public CropZoneId CropZoneId { get; set; }
        [DataMember(Order=2)]
        public string Crop { get; set; }
    }
}
