﻿using FieldToMarket.Contracts.Reference;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using Landdb.Client.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;

namespace Landdb.ViewModel.Analysis.FieldToMarket {
    public class FtmSubmitNrcsTillageManagementViewModel : ViewModelBase {
        IClientEndpoint clientEndpoint;
        Dispatcher dispatcher;
        SustainabilityServices sustainabilityServices;
        SustainabilityServiceClient sustainabilityClient;
        System.Windows.Point? centerPoint = null;
        string currentTillagePointer;
        FtmId ftmId;
        Action onViewClose;

        public FtmSubmitNrcsTillageManagementViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, SustainabilityServices sustainabilityServices, SustainabilityServiceClient sustainabilityClient, FtmId ftmId, System.Windows.Point? centerPoint, string currentTillagePointer, Action onViewClose) {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            this.sustainabilityServices = sustainabilityServices;
            this.sustainabilityClient = sustainabilityClient;
            this.ftmId = ftmId;
            this.currentTillagePointer = currentTillagePointer;
            this.centerPoint = centerPoint;
            this.onViewClose = onViewClose;

            SubmitCommand = new AwaitableRelayCommand(Submit);
            CancelCommand = new RelayCommand(onViewClose);
        }

        public static async Task<FtmSubmitNrcsTillageManagementViewModel> Create(IClientEndpoint clientEndpoint, Dispatcher dispatcher, SustainabilityServices sustainabilityServices, SustainabilityServiceClient sustainabilityClient, FtmId ftmId, System.Windows.Point? centerPoint, string currentTillagePointer, Action onViewClose) {
            var vm = new FtmSubmitNrcsTillageManagementViewModel(clientEndpoint, dispatcher, sustainabilityServices, sustainabilityClient, ftmId, centerPoint, currentTillagePointer, onViewClose);
            await vm.LoadTillageTemplates();
            return vm;
        }

        async Task LoadTillageTemplates() {
            var practices = await sustainabilityServices.GetTillageManagementPractices(ftmId.Crop, centerPoint.Value.Y, centerPoint.Value.X);
            TillageList = practices.ToList();

            SelectedTillage = TillageList.Where(x => x.Pointer == currentTillagePointer).FirstOrDefault();
        }

        public ICommand SubmitCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }

        List<TillageManagementPractice> tillageList;
        public List<TillageManagementPractice> TillageList {
            get { return tillageList; }
            set {
                tillageList = value;
                RaisePropertyChanged(() => TillageList);
            }
        }

        TillageManagementPractice selectedTillage;
        public TillageManagementPractice SelectedTillage {
            get { return selectedTillage; }
            set {
                selectedTillage = value;
                RaisePropertyChanged(() => SelectedTillage);
            }
        }
        

        async Task Submit() {
            var synced = await sustainabilityClient.SetTillageManagementTemplate(ftmId.ClientId, ftmId.Crop, ftmId.FieldId, SelectedTillage.Pointer);

            onViewClose();
        }
    }
}
