﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Analysis.FieldToMarket {
    public class FtmCustomDataItem {
        public double? SugarPercentage { get; set; }
        public double? ProteinPercentage { get; set; }
        public Guid CropId { get; set; }

        public Dictionary<int, FtmDeliveryAdditionalData> AdditionalDeliveryInfo { get; set; }
    }

    public class FtmDeliveryAdditionalData {
        public double LoadSize { get; set; }
        public string LoadUnit { get; set; }
        public double RoundtripDistance { get; set; }
    }
}
