﻿using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Plan;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Landdb.ViewModel.Analysis
{
    public class BudgetVarianceViewModel : AbstractListPage<PlanListItemViewModel, BudgetVarianceDetailViewModel>
    {
        public BudgetVarianceViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher)
            : base(clientEndpoint, dispatcher)
        {
            this.FilterModel = new PlansPageFilterViewModel(clientEndpoint);

            this.currentDataSourceId = new DataSourceId(ApplicationEnvironment.CurrentDataSourceId);
            this.currentCropYear = ApplicationEnvironment.CurrentCropYear;


            Messenger.Default.Register<PlansAddedMessage>(this, msg =>
            {
                RefreshItemsList(() =>
                {
                    var q = from li in ListItems
                            where li.Id == msg.LastPlanAdded
                            select li;
                    SelectedListItem = q.FirstOrDefault();
                });
            });
        }

        protected override IEnumerable<PlanListItemViewModel> GetListItemModels()
        {
            var cropYearId = new CropYearId(currentDataSourceId.Id, currentCropYear);
            var plansMaybe = clientEndpoint.GetView<PlanList>(cropYearId);

            if (plansMaybe.HasValue)
            {
                var sortedProjectedPlans = plansMaybe.Value.Plans.OrderBy(x => x.Name).Select(x => new PlanListItemViewModel(x));
                return sortedProjectedPlans.ToArray();
            }
            else
            {
                return null;
            }
        }

        protected override async Task<ScreenDescriptor> CreateScreenDescriptorForNewItemCreation()
        {
            throw new NotImplementedException();
        }

        public override bool CanDelete(PlanListItemViewModel toDelete) {
            return false;
        }

        protected override bool PerformItemRemoval(PlanListItemViewModel selectedItem)
        {
            throw new NotImplementedException();
        }

        protected override IDomainCommand CreateSetCharmCommand(PlanListItemViewModel selectedItem, string charmName)
        {
            throw new NotImplementedException();
        }

        protected override BudgetVarianceDetailViewModel CreateDetailsModel(PlanListItemViewModel selectedItem)
        {
            if (selectedItem == null) { return null; }

            Dictionary<string, List<string>> availableTimingEventTags = new Dictionary<string, List<string>>();

            var pMaybe = clientEndpoint.GetView<PlanView>(selectedItem.Id);

            if (pMaybe.HasValue)
            {
                BudgetVarianceDetailViewModel detailsViewModel = new BudgetVarianceDetailViewModel(clientEndpoint, dispatcher, pMaybe.Value);
                return detailsViewModel;
                //PlanDetailsViewModel plandetview = new PlanDetailsViewModel(clientEndpoint, dispatcher, pMaybe.Value, availableTimingEventTags);
                //plandetview.AvailableTimingEventTags = availableTimingEventTags;
                //return plandetview;
            }
            else
            {
                return null;
            }
        }
    }
}
