﻿using AgC.UnitConversion;
using GalaSoft.MvvmLight;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Domain.ReadModels.Plan;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Domain.ReadModels.Yield;
using Landdb.Domain.ReadModels.YieldLocation;
using Landdb.Infrastructure;
using Landdb.ViewModel.Fields.FieldDetails;
using Landdb.ViewModel.Yield;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Landdb.ViewModel.Analysis
{
    public class BudgetVarianceDetailViewModel : ViewModelBase
    {
        IClientEndpoint endpoint;
        PlanView view;
        int cropYear;
        List<string> CategoryList;
        List<string> TimingEventList;
        Dictionary<string, double> percentBudgetByCategory;
        Dictionary<string, double> percentBudgetByTimingEvent;
        Dispatcher dispatcher;

        public BudgetVarianceDetailViewModel(IClientEndpoint endPoint, Dispatcher dispatcher, PlanView view)
        {
            this.endpoint = endPoint;
            this.view = view;
            this.cropYear = ApplicationEnvironment.CurrentCropYear;
            this.dispatcher = dispatcher;
            percentBudgetByCategory = new Dictionary<string, double>();
            percentBudgetByTimingEvent = new Dictionary<string, double>();
            CategoryList = new List<string>();
            TimingEventList = new List<string>();

            var masterlistService = endpoint.GetMasterlistService();
            var categoryList = (from list in masterlistService.GetProductList()
                                where list.ProductType != null && list.ProductType.ToLower() != "surfactant"
                                orderby list.ProductType
                               select list.ProductType).Distinct();
            CategoryList.AddRange(categoryList);
            var timingList = endpoint.GetMasterlistService().GetTimingEventList().OrderBy(x => x.Order);
            TimingEventList = (from item in timingList
                               where item.Name.ToLower() != "annual"
                              select item.Name).ToList();
            GetPlannedProductByCategory();
            GetAppliedProductByCategory();
            GetRevenue();
        }

        public double TotalPlan { get; set; }
        public double TotalActual { get; set; }
        public double PercentOfPlan { get { return (TotalActual / TotalPlan); } }

        public double PlanRevenue { get; set; }
        public double ActualRevenue { get; set; }
        public double PercentPlanRevenue { get { return (ActualRevenue / PlanRevenue); } }

        public string CurrencyFormatString => $"'{CultureInfo.CurrentCulture.NumberFormat.CurrencySymbol}'###,###.##";

        public Dictionary<string, double> PlannedProductsByCategory { get; set; }
        public Dictionary<string, double> ActualProductsByCategory { get; set; }

        public Dictionary<string, double> PlannedProductsByTimingEvent { get; set; }
        public Dictionary<string, double> ActualProductsByTimingEvent { get; set; }

        public Dictionary<string, double> PercentBudgetByCategory
        {
            get
            {
                foreach (var cat in CategoryList)
                {
                    if (PlannedProductsByCategory[cat] != 0)
                    {
                        var value = (ActualProductsByCategory[cat] / PlannedProductsByCategory[cat]) * 100;
                        percentBudgetByCategory.Add(cat, value);
                    }
                    else
                    {
                        percentBudgetByCategory.Add(cat, 0.0);
                    }
                }

                return percentBudgetByCategory;
                
            }
        }


        public Dictionary<string, double> PercentBudgetByTimingEvent
        {
            get
            {
                foreach (var timing in TimingEventList)
                {
                    if (PlannedProductsByTimingEvent[timing] != 0)
                    {
                        var value = (ActualProductsByTimingEvent[timing] / PlannedProductsByTimingEvent[timing]) * 100;
                        percentBudgetByTimingEvent.Add(timing, value);
                    }
                    else
                    {
                        percentBudgetByTimingEvent.Add(timing, 0.0);
                    }
                }

                return percentBudgetByTimingEvent;
                
            }
        }
        void GetPlannedProductByCategory()
        {
            //create list object to hold all these categories and then sum them up and add them to a dictionary
            List<ProductItemDetail> products = new List<ProductItemDetail>();
            var masterlistServ = endpoint.GetMasterlistService();
            foreach (var p in view.Products)
            {
                ProductItemDetail prodDetail = new ProductItemDetail();
                var mlp = masterlistServ.GetProduct(p.Id);
                if(mlp == null) { continue; }
                prodDetail.Id = p.Id;
                prodDetail.ProductCategory = mlp.ProductType;

                // calculate the total from the rate
                var totalProduct = p.RateValue * (decimal)view.EstimatedArea.Value * p.PercentApplied * p.ApplicationCount;
                var totalProductMeasure = UnitFactory.GetPackageSafeUnit(p.RateUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit).GetMeasure((double)totalProduct, mlp.Density);
                var totalProductUnit = UnitFactory.GetPackageSafeUnit(p.TotalProductUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                var convertedTotalProduct = totalProductMeasure.GetValueAs(totalProductUnit);
                var displayTotal = DisplayMeasureConverter.ConvertToTotalDisplayMeasure(p.Id, (decimal)convertedTotalProduct, p.TotalProductUnit, mlp.Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);

                var unit = UnitFactory.GetPackageSafeUnit(p.SpecificCostUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                try
                {
                    if (displayTotal.CanConvertTo(unit))
                    {
                        var convertedTotal = unit != null && displayTotal.CanConvertTo(unit) ? displayTotal.GetValueAs(unit) : displayTotal.Value;
                        prodDetail.TotalCost = (double)(p.SpecificCostPerUnit.HasValue ? (decimal)convertedTotal * p.SpecificCostPerUnit.Value : p.ProductTotalCost);
                    }
                    else { prodDetail.TotalCost = 0.0; }
                }
                catch (Exception ex) { prodDetail.TotalCost = 0.0; }

                prodDetail.TimingEvent = p.TimingEvent;
                products.Add(prodDetail);
            }


            //var plannedProductCatList = (from p in view.Products
            //                            where endpoint.GetMasterlistService().GetProduct(p.Id) != null
            //                            select new { endpoint.GetMasterlistService().GetProduct(p.Id).ProductType, p.ProductTotalCost, p.TimingEvent }).ToList();
            
            PlannedProductsByCategory = new Dictionary<string, double>();
            var annualCost = view.EstimatedArea.Value * (view.Taxes + view.ReturnToMgt + view.Repairs + view.LandRent + view.Labor + view.Insurance + view.Equipment);

            foreach (var item in CategoryList)
            {
                var sumOfTotalCost = products.Where(x => x.ProductCategory == item).Sum(v => v.TotalCost);
                PlannedProductsByCategory.Add(item, (double)sumOfTotalCost);

                //if (item == "Service")
                //{  
                //    PlannedProductsByCategory[item] = PlannedProductsByCategory[item] + annualCost;
                //}
            }

            RaisePropertyChanged(() => PlannedProductsByCategory);
            PlannedProductsByTimingEvent = new Dictionary<string, double>();
            foreach (var timingEvent in TimingEventList)
            {
                var sumOfTotalCost = products.Where(x => x.TimingEvent == timingEvent).Sum(v => v.TotalCost);
                PlannedProductsByTimingEvent.Add(timingEvent, (double)sumOfTotalCost);

                //if (timingEvent == "Annual")
                //{
                //    PlannedProductsByTimingEvent[timingEvent] = PlannedProductsByTimingEvent[timingEvent] + annualCost;
                //}
            }

            RaisePropertyChanged(() => PlannedProductsByTimingEvent);
            RaisePropertyChanged(() => PercentBudgetByCategory);
            RaisePropertyChanged(() => PercentBudgetByTimingEvent);

            TotalPlan = PlannedProductsByCategory.Sum(x => x.Value);
            
        }

        void GetAppliedProductByCategory()
        {
            ActualProductsByCategory = new Dictionary<string, double>();
            //get all the associated crop zones
            var czs = view.CropZones.ToList();
            List<CropZoneId> czIds = new List<CropZoneId>();
            if (czs.Any())
            {
                var applicationData = endpoint.GetView<CropZoneApplicationDataView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new CropZoneApplicationDataView());
                var inventory = endpoint.GetView<InventoryListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new InventoryListView());

                var appList = new List<CropZoneApplicationDataItem>();

                foreach (var cz in czs)
                {
                    appList.AddRange(applicationData.Items.Where(app => app.CropZoneId == cz.Id));
                }

                List<ApplicationView> appDetails = new List<ApplicationView>();
                List<ProductItemDetail> prodDetails = new List<ProductItemDetail>();
                foreach (var item in appList)
                {
                    var mlp = endpoint.GetMasterlistService().GetProduct(item.ProductId);
                    ApplicationView detail = new ApplicationView();
                    if (appDetails.Any(x => x.Id == item.ApplicationId))
                    {
                        detail = appDetails.SingleOrDefault(id => id.Id == item.ApplicationId);
                    }
                    else
                    {
                        detail = endpoint.GetView<ApplicationView>(item.ApplicationId).Value;
                    }

                    var includedProduct = detail.Products.SingleOrDefault(prod => prod.TrackingId == item.ProductTrackingId);

                    if (includedProduct.SpecificCostPerUnit.HasValue && includedProduct.SpecificCostPerUnit.Value > 0)
                    {
                        var unit = !string.IsNullOrEmpty(includedProduct.SpecificCostUnit) ? UnitFactory.GetPackageSafeUnit(includedProduct.SpecificCostUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit) : UnitFactory.GetPackageSafeUnit(inventory.Products[includedProduct.Id].AveragePriceUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                        var totalProduct = UnitFactory.GetPackageSafeUnit(item.TotalProductUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit).GetMeasure((double)item.TotalProductValue, mlp.Density);
                        var totalProdConverted = totalProduct.CanConvertTo(unit) ? totalProduct.CreateAs(unit) : totalProduct;
                        var totalSpecificCost = (decimal)totalProdConverted.Value * includedProduct.SpecificCostPerUnit;

                        prodDetails.Add(new ProductItemDetail() { Id = item.ProductId, ProductCategory = mlp.ProductType, TotalCost = (double)totalSpecificCost, TimingEvent = detail.TimingEvent });
                    }
                    else
                    {
                        var masterlist = endpoint.GetMasterlistService();
                        if (inventory.Products.ContainsKey(item.ProductId))
                        {
                            try
                            {
                                var totalMeasure = UnitFactory.GetPackageSafeUnit(item.TotalProductUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit).GetMeasure(item.TotalProductValue, mlp.Density);
                                var avgPriceUnit = string.IsNullOrEmpty(inventory.Products[item.ProductId].AveragePriceUnit) ? null : UnitFactory.GetPackageSafeUnit(inventory.Products[item.ProductId].AveragePriceUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                                var convertedTotalMeasureValue = totalMeasure.CanConvertTo(avgPriceUnit) ? totalMeasure.GetValueAs(avgPriceUnit) : totalMeasure.Value;

                                var totalCost = inventory.Products[item.ProductId].AveragePriceValue * convertedTotalMeasureValue; // TODO: This will only pan out if the units of these measures is the same. Need to take this into account.
                                prodDetails.Add(new ProductItemDetail() { Id = item.ProductId, ProductCategory = mlp.ProductType, TotalCost = totalCost, TimingEvent = detail.TimingEvent });
                            }
                            catch (Exception ex) { return; }
                        }
                        else
                        {
                            return;
                        }
                    }
                }

                foreach (var cat in CategoryList)
                {
                    ActualProductsByCategory.Add(cat, prodDetails.Where(item => item.ProductCategory == cat).Sum(product => product.TotalCost));
                }

                RaisePropertyChanged(() => ActualProductsByCategory);

                ActualProductsByTimingEvent = new Dictionary<string, double>();

                foreach (var timingEvent in TimingEventList)
                {
                    ActualProductsByTimingEvent.Add(timingEvent, prodDetails.Where(item => item.TimingEvent == timingEvent).Sum(product => product.TotalCost));
                }

                RaisePropertyChanged(() => ActualProductsByTimingEvent);
                RaisePropertyChanged(() => PercentBudgetByCategory);
                RaisePropertyChanged(() => PercentBudgetByTimingEvent);

                TotalActual = ActualProductsByCategory.Sum(x => x.Value);
            }
        }

		void GetRevenue() {
			var revenue = ((view.YieldPerArea * view.EstimatedArea.Value * (double)view.PricePerUnit) * view.CropSharePercent) + (view.FSAPayment * view.EstimatedArea.Value);
			PlanRevenue = revenue;

			var currentCropYearId = new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear);

			//Get Associated Fields Yield Revenue
			var loadList = endpoint.GetView<CropZoneLoadDataView>(currentCropYearId).GetValue(new CropZoneLoadDataView()).Items;
			var flattenedHierarchy = endpoint.GetView<FlattenedTreeHierarchyView>(currentCropYearId).GetValue(new FlattenedTreeHierarchyView());
			var commodityList = endpoint.GetView<CommodityListView>(currentCropYearId).GetValue(new CommodityListView()).Commodities;
			var yieldLocations = endpoint.GetView<YieldLocationListView>(new DataSourceId(currentCropYearId.DataSourceId)).GetValue(() => new YieldLocationListView()).YieldLocations;

			List<CropPrice> priceContainer = new List<CropPrice>();

			foreach (var commodity in commodityList) {
				CommoditySummaryViewModel csViewModel = new CommoditySummaryViewModel(endpoint, dispatcher, currentCropYearId, commodity.CropId, commodity.CommodityDescription);

				foreach (var summary in csViewModel.UnitSummaries) {
					priceContainer.Add(new CropPrice() {
						CropID = commodity.CropId,
						CommodityDescription = commodity.CommodityDescription,
						AvgPrice = summary.Value.GrowerAverageRevenue,
						AvgPriceUnit = summary.Value.QuantityUnit
					});
				}
			}

			var locationsExcludedFromInventory = from loc in yieldLocations
												 where loc.IsActiveInCropYear(currentCropYearId.Id) && loc.IsQuantityExcludedFromInventory
												 select loc.Id.Id;

			var czLoads = from load in loadList
						  from loc in load.DestinationLocations
						  from cz in view.CropZones
						  where cz.Id == load.CropZoneId
							&& !locationsExcludedFromInventory.Contains(loc.Id.Id)
						  select load;

			foreach (var load in czLoads) {
				var cropId = flattenedHierarchy.Items.FirstOrDefault(x => x.CropZoneId == load.CropZoneId).CropId;
				var totalLoadQty = load.AreaWeightedFinalQuantity;
				var loadUnit = load.FinalQuantityUnit;
				CompositeUnit priceUnit = null;
				var priceValue = 0m;

				if (priceContainer.Any(x => x.CropID == cropId)) {
					var holder = (from u in priceContainer
								  where u.CropID == cropId && u.AvgPriceUnit == loadUnit
								  select u).ToList();
					if (holder.Any()) {
						priceUnit = holder.First().AvgPriceUnit;
						priceValue = holder.First().AvgPrice;
					} else {
						priceUnit = priceContainer.FirstOrDefault(u => u.CropID == cropId).AvgPriceUnit;
						priceValue = priceContainer.FirstOrDefault(u => u.CropID == cropId).AvgPrice;
					}

				} else {
					priceUnit = null;
					priceValue = 0m;
				}
				var loadRevenue = 0m;

				if (loadUnit == priceUnit) {
					loadRevenue = totalLoadQty * priceValue;
				} else {
					if (priceUnit != null && CompositeUnitConverter.CanConvert(loadUnit, priceUnit)) {
						var newValue = CompositeUnitConverter.ConvertValue(totalLoadQty, loadUnit, priceUnit);
						loadRevenue = newValue * priceValue;
					}
				}

				ActualRevenue += (double)loadRevenue;
			}

			//foreach (var unit in sortedUnitFrequencyList)
			//{
			//    var itemsWithThisUnit = YieldSummary.SummaryItems.Where(x => x.BaseItem.FinalQuantityUnit == unit);
			//    var growerQuantity = itemsWithThisUnit.Sum(x => x.GrowerShareQuantity);
			//    var totalQuantity = itemsWithThisUnit.Sum(x => x.BaseItem.WeightedFinalQuantity);

			//    yieldSummaryItems.Add(unit, new YieldSummaryItem(CostSummary.Area)
			//    {
			//        GrowerQuantity = growerQuantity,
			//        TotalYieldQuantity = totalQuantity,
			//        YieldQuantityUnit = unit,
			//    });
			//}

			//if (treeModel is CropZoneTreeItemViewModel && yieldSummaryItems.Sum(x => x.Value.TotalYieldQuantity) > 0)
			//{
			//    var czTreeModel = treeModel as CropZoneTreeItemViewModel;

			//    var currentCropYearId = new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear);

			//    // constructing this vm is kind of expensive just to get the average sale price. consider alternatives.
			//    var commoditySummary = new CommoditySummaryViewModel(clientEndpoint, dispatcher, currentCropYearId, czTreeModel.Crop);

			//    foreach (var yieldSummary in yieldSummaryItems)
			//    {
			//        if (commoditySummary.UnitSummaries.ContainsKey(yieldSummary.Key))
			//        {
			//            yieldSummary.Value.AverageSalePrice = commoditySummary.UnitSummaries[yieldSummary.Key].GrowerAverageRevenue;
			//        }

			//        yieldSummary.Value.GrowerRevenue = yieldSummary.Value.GrowerQuantity * yieldSummary.Value.AverageSalePrice;
			//    }
			//}

			//CostSummary.YieldSummaryItems.Clear();
			//yieldSummaryItems.ForEach(x => CostSummary.YieldSummaryItems.Add(x.Value));

			//if (!CostSummary.YieldSummaryItems.Any())
			//{
			//    CostSummary.YieldSummaryItems.Add(new YieldSummaryItem(CostSummary.Area));
			//}
		}
    }

    public class ProductItemDetail
    {
        public ProductId Id { get; set; }
        public string ProductCategory { get; set; }
        public double TotalCost { get; set; }
        public string TimingEvent { get; set; }
    }

    public class CropPrice {
		public CropId CropID { get; set; }
		public string CommodityDescription { get; set; }
		public decimal AvgPrice { get; set; }
        public CompositeUnit AvgPriceUnit { get; set; }
    }
}
