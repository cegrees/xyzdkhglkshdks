﻿using GalaSoft.MvvmLight;
using Landdb.Domain.ReadModels.Plan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Analysis
{
    public class BudgetVarianceListItemViewModel : ViewModelBase
    {
        PlanListItem li;

        public BudgetVarianceListItemViewModel(PlanListItem item)
        {
            this.li = item;
        }

        public CropPlanId Id { get { return li.Id; } }
        public string Name { get { return li.Name; } }
        public int FieldCount { get { return li.FieldCount; } }
        public int ProductCount { get { return li.ProductCount; } }

    }
}
