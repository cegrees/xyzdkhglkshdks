﻿using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Yield;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Threading;

namespace Landdb.ViewModel {
	public static class CompositeUnitHelper {
		public static IEnumerable<CompositeUnit> GetWeightUnitsForCrop(IClientEndpoint clientEndpoint, Dispatcher dispatcher, CropYearId cropYearId, CropId cropId) {
			var unitList = GetWeightPartUnits().ToList();

			if (cropId != null) {
				var appliedLoadValues = clientEndpoint.GetView<AppliedLoadValuesView>(cropYearId);
				appliedLoadValues.IfValue(values => {
					if (values.AppliedCropValues.ContainsKey(cropId.Id)) {
						var cropValues = values.AppliedCropValues[cropId.Id];
						cropValues.AppliedUnits.ForEach(x => unitList.Add(x));
					}
				});

				var cropDefaults = clientEndpoint.GetMasterlistService().GetCropDefaultsForCrop(cropId);
				if (cropDefaults != null) {
					if (cropDefaults.PackageUnits != null) {
						cropDefaults.PackageUnits.ForEach(pu => {
							unitList.Add(new CompositeUnit(pu.PackageUnitName, pu.ConversionFactor, pu.PartUnitName));
						});
					}
				}

				unitList.Select(x => x.PartUnitName).Distinct().ToList().ForEach(u => {
					if (!unitList.Any(x => x.PackageUnitName == u)) {
						unitList.Add(new CompositeUnit(u, 1, u));
					}
				});
			}

			return unitList.Distinct();
		}

		public static IEnumerable<CompositeUnit> GetWeightPartUnits() {
			var unitList = new List<CompositeUnit>();

		    if (Landdb.Client.Localization.DataSourceCultureInformation.UsesImperialUnits) {
		        unitList.Add(new CompositeUnit(AgC.UnitConversion.MassAndWeight.CentrumPound.Self));
		        unitList.Add(new CompositeUnit(AgC.UnitConversion.MassAndWeight.Ounce.Self));
		        unitList.Add(new CompositeUnit(AgC.UnitConversion.MassAndWeight.Pound.Self));
		        unitList.Add(new CompositeUnit(AgC.UnitConversion.MassAndWeight.ShortTon.Self));
		    }
            unitList.Add(new CompositeUnit(AgC.UnitConversion.MassAndWeight.Gram.Self));
			unitList.Add(new CompositeUnit(AgC.UnitConversion.MassAndWeight.Kilogram.Self));
            unitList.Add(new CompositeUnit(AgC.UnitConversion.MassAndWeight.MetricTon.Self));

			return unitList;
		}

		public static IEnumerable<CompositeUnit> GetLengthPartUnits() {
			var unitList = new List<CompositeUnit>();
		    if (Landdb.Client.Localization.DataSourceCultureInformation.UsesImperialUnits) {
		        unitList.Add(new CompositeUnit(AgC.UnitConversion.Length.Foot.Self));
		        unitList.Add(new CompositeUnit(AgC.UnitConversion.Length.Inch.Self));
		    }

		    unitList.Add(new CompositeUnit(AgC.UnitConversion.Length.Centimeter.Self));
			unitList.Add(new CompositeUnit(AgC.UnitConversion.Length.Meter.Self));

			return unitList;
		}

		public static IEnumerable<CompositeUnit> GetVolumePartUnits() {
			var unitList = new List<CompositeUnit>();

		    if (Landdb.Client.Localization.DataSourceCultureInformation.UsesImperialUnits) {
		        unitList.Add(new CompositeUnit(AgC.UnitConversion.Volume.CubicFoot.Self));
		        unitList.Add(new CompositeUnit(AgC.UnitConversion.Volume.CubicInch.Self));
		        unitList.Add(new CompositeUnit(AgC.UnitConversion.Volume.CubicYard.Self));
		        unitList.Add(new CompositeUnit(AgC.UnitConversion.Volume.FluidOunce.Self));
		        unitList.Add(new CompositeUnit(AgC.UnitConversion.Volume.Gallon.Self));
		        unitList.Add(new CompositeUnit(AgC.UnitConversion.Volume.Pint.Self));
		        unitList.Add(new CompositeUnit(AgC.UnitConversion.Volume.Quart.Self));
            }

            unitList.Add(new CompositeUnit(AgC.UnitConversion.Volume.Centiliter.Self));
			unitList.Add(new CompositeUnit(AgC.UnitConversion.Volume.CubicMeter.Self));
			unitList.Add(new CompositeUnit(AgC.UnitConversion.Volume.Liter.Self));
			unitList.Add(new CompositeUnit(AgC.UnitConversion.Volume.Milliliter.Self));

			return unitList;
		}
	}
}