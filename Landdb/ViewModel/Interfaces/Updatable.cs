﻿namespace Landdb.ViewModel
{
    public interface Updatable
    {
        void Update();
    }
}