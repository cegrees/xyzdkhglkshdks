﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Landdb.ViewModel.Secondary.Application;

namespace Landdb.ViewModel.Interfaces
{
    public interface IEnvironmentConditionsProperty
    {
        EnvironmentConditions EnvironmentConditions { get; set; }
    }
}
