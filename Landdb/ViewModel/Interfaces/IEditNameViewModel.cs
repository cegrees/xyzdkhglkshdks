﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Landdb.ViewModel {
    public interface IEditNameViewModel {
        ICommand UpdateCommand { get; }
        ICommand CancelCommand { get; }

        string Name { get; set; }
    }
}
