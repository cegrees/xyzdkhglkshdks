/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocatorTemplate xmlns:vm="clr-namespace:Landdb"
                                   x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"

  You can also use Blend to do all this with the tool's support.
  See http://www.galasoft.ch/mvvm
*/

using GalaSoft.MvvmLight;
using Landdb.ViewModel.Production;
using Landdb.Infrastructure;
using Landdb.Client.Infrastructure;
using Landdb.ViewModel.Fields;
using Landdb.ViewModel.Resources;
using Landdb.ViewModel.Secondary.Map;
using Landdb.ViewModel.Reports;
using Landdb.ViewModel.Maps;
using Landdb.ViewModel.Yield;
using Landdb.ViewModel.Maps.ImageryOverlay;
using Landdb.ViewModel.Analysis;
using Landdb.ViewModel.Analysis.FieldToMarket;
using Landdb.ViewModel.Analysis.Planet;
using Landdb.ViewModel.Production.Applications;
using Landdb.ViewModel.Production.Invoices;
using Landdb.ViewModel.Resources.Equipment;

namespace Landdb.ViewModel {
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// </summary>
    public class ViewModelLocator {
        static MainViewModel main;
        static StartupViewModel startup;
        static Account.AccountStatusWidgetViewModel accountStatusWidget;
        static Overlays.ConstructPivotOverlayViewModel constructpivotoverlay;
        static RecommendationsPageViewModel recommendationsPage;
        static SiteDataPageViewModel siteDataPage;
        static WorkOrdersPageViewModel workOrdersPage;
        static ApplicationsPageViewModel applicationsPage;
        static InvoicesPageViewModel invoicesPage;
        static FieldDetailsPageViewModel fieldDetailsPage;
        static CropDetailsPageViewModel cropDetailsPage;
        static MapsPageViewModel coloredMapPage;
        static MainPageViewModel mainPage;
        static InventoryPageViewModel inventoryPage;
        static ActiveIngredientPageViewModel activeIngredientPage;
        static PersonPageViewModel personPage;
        static CompanyPageViewModel companyPage;
        static ContractsPageViewModel contractPage;
        static PlansPageViewModel plansPage;
        static CreateCropzoneViewModel createcropzonevm;
        static ShapeImportViewModel shapeimportvm;
        static ScalePolygonViewModel scalepolygonvm;
        static SSurgoNrcsWfsViewModel ssurgonrcswfsvm;
        //static ThinkGeoMapsUtilityViewModel thinkgeoutilityvm;
        static CustomLabelViewModel customlabelvm;
        static ReportSelectionPageViewModel reportSelectionPage;
		static HarvestLoadsPageViewModel harvestLoadsPage;
		static StorageLoadsPageViewModel storageLoadsPage;
		static YieldLocationsPageViewModel yieldLocationsPage;
		static CommoditiesPageViewModel commoditiesPage;
		static EquipmentPageViewModel equipmentPage;
		static CropSettingsPageViewModel cropSettingsPage;

        /// <summary>
        /// Initializes a new instance of the ViewModelLocator class.
        /// </summary>
        public ViewModelLocator() {
            if (ViewModelBase.IsInDesignModeStatic) {
            } else {
                main = new MainViewModel();
                accountStatusWidget = new Account.AccountStatusWidgetViewModel();
            }
        }

        /// <summary>
        /// Gets the Main property which defines the main viewmodel.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "This non-static member is needed for data binding purposes.")]
        public MainViewModel Main {
            get {
                return main;
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "This non-static member is needed for data binding purposes.")]
        public StartupViewModel Startup {
            get {
                if (startup == null) {
                    startup = new StartupViewModel();
                }
                return startup;
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "This non-static member is needed for data binding purposes.")]
        public Account.AccountStatusWidgetViewModel AccountStatusWidget {
            get {
                return accountStatusWidget;
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "This non-static member is needed for data binding purposes.")]
        public Overlays.ConstructPivotOverlayViewModel ConstructPivotOverlay {
            get {
                if (constructpivotoverlay == null) {
                    constructpivotoverlay = new Overlays.ConstructPivotOverlayViewModel();
                }
                return constructpivotoverlay;
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "This non-static member is needed for data binding purposes.")]
        public MainPageViewModel MainPage {
            get {
                if (mainPage == null) {
                    Landdb.Client.Account.IAccountStatus accountStatus = ServiceLocator.Get<Landdb.Client.Account.IAccountStatus>();
                    Landdb.Client.Services.ISynchronizationManager syncManager = ServiceLocator.Get<Landdb.Client.Services.ISynchronizationManager>();

                    mainPage = new MainPageViewModel(accountStatus, syncManager, App.CloudClientFactory, App.Current.Dispatcher);
                }
                return mainPage;
            }
        }

        public RemoteSubmissionErrorNotifier RemoteErrorNotifier {
            get {
                return ServiceLocator.Get<IRemoteSubmissionErrorNotifier>() as RemoteSubmissionErrorNotifier;
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "This non-static member is needed for data binding purposes.")]
        public FieldDetailsPageViewModel FieldDetailsPage {
            get {
                if (fieldDetailsPage == null) {
                    fieldDetailsPage = new FieldDetailsPageViewModel(ServiceLocator.Get<IClientEndpoint>());
                }
                return fieldDetailsPage;
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "This non-static member is needed for data binding purposes.")]
        public CropDetailsPageViewModel CropDetailsPage {
            get {
                if (cropDetailsPage == null) {
                    var clientEndpoint = ServiceLocator.Get<IClientEndpoint>();
                    cropDetailsPage = new CropDetailsPageViewModel(clientEndpoint, new Landdb.ViewModel.Fields.FieldDetails.DetailsViewModelFactory(clientEndpoint), App.Current.Dispatcher);
                }
                return cropDetailsPage;
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "This non-static member is needed for data binding purposes.")]
        public MapsPageViewModel ColoredMapPage {
            get {
                if (coloredMapPage == null) {
                    var clientEndpoint = ServiceLocator.Get<IClientEndpoint>();
                    coloredMapPage = new MapsPageViewModel(clientEndpoint, new Landdb.ViewModel.Fields.FieldDetails.DetailsViewModelFactory(clientEndpoint), App.Current.Dispatcher);
                }
                return coloredMapPage;
            }
        }

        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "This non-static member is needed for data binding purposes.")]
        //public BudgetVarianceViewModel BudgetVariancePage
        //{
        //    get
        //    {
        //        if (budgetVariancePage == null)
        //        {
        //            budgetVariancePage = new BudgetVarianceViewModel(ServiceLocator.Get<IClientEndpoint>(), App.Current.Dispatcher);
        //        }
        //        return budgetVariancePage;
        //    }
        //}

        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "This non-static member is needed for data binding purposes.")]
        //public FtmPageViewModel FieldToMarketPage {
        //    get {
        //        if (fieldToMarketPage == null) {
        //            fieldToMarketPage = new FtmPageViewModel(ServiceLocator.Get<IClientEndpoint>(), App.Current.Dispatcher);
        //        }
        //        return fieldToMarketPage;
        //    }
        //}

        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "This non-static member is needed for data binding purposes.")]
        //public PlanetMapPageViewModel PlanetMapPage
        //{
        //    get
        //    {
        //        if (planetMapPage == null) {
        //            planetMapPage = new PlanetMapPageViewModel(ServiceLocator.Get<IClientEndpoint>());
        //        }
        //        return planetMapPage;
        //    }
        //}

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "This non-static member is needed for data binding purposes.")]
        public PlansPageViewModel PlansPage {
            get {
                if (plansPage == null) {
                    plansPage = new PlansPageViewModel(ServiceLocator.Get<IClientEndpoint>(), App.Current.Dispatcher);
                }
                return plansPage;
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "This non-static member is needed for data binding purposes.")]
        public ApplicationsPageViewModel ApplicationsPage {
            get {
                if (applicationsPage == null) {
                    applicationsPage = new ApplicationsPageViewModel(ServiceLocator.Get<IClientEndpoint>(), App.Current.Dispatcher);
                }
                return applicationsPage;
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "This non-static member is needed for data binding purposes.")]
        public InvoicesPageViewModel InvoicesPage {
            get {
                if (invoicesPage == null) {
                    invoicesPage = new InvoicesPageViewModel(ServiceLocator.Get<IClientEndpoint>(), App.Current.Dispatcher);
                }
                return invoicesPage;
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "This non-static member is needed for data binding purposes.")]
        public PersonPageViewModel PersonPage
        {
            get
            {
                if (personPage == null)
                {
                    personPage = new PersonPageViewModel(ServiceLocator.Get<IClientEndpoint>(), App.Current.Dispatcher);
                }
                return personPage;
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "This non-static member is needed for data binding purposes.")]
        public CompanyPageViewModel CompanyPage
        {
            get
            {
                if (companyPage == null)
                {
                    companyPage = new CompanyPageViewModel(ServiceLocator.Get<IClientEndpoint>(), App.Current.Dispatcher);
                }
                return companyPage;
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "This non-static member is needed for data binding purposes.")]
        public ContractsPageViewModel ContractPage
        {
            get
            {
                if (contractPage == null)
                {
                    contractPage = new ContractsPageViewModel(ServiceLocator.Get<IClientEndpoint>(), App.Current.Dispatcher);
                }
                return contractPage;
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "This non-static member is needed for data binding purposes.")]
        public WorkOrdersPageViewModel WorkOrdersPage {
            get {
                if (workOrdersPage == null) {
                    workOrdersPage = new WorkOrdersPageViewModel(ServiceLocator.Get<IClientEndpoint>(), App.Current.Dispatcher);
                }
                return workOrdersPage;
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "This non-static member is needed for data binding purposes.")]
        public RecommendationsPageViewModel RecommendationsPage
        {
            get
            {
                if (recommendationsPage == null)
                {
                    recommendationsPage = new RecommendationsPageViewModel(ServiceLocator.Get<IClientEndpoint>(), App.Current.Dispatcher);
                }
                return recommendationsPage;
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "This non-static member is needed for data binding purposes.")]
        public SiteDataPageViewModel SiteDataPage
        {
            get
            {
                if (siteDataPage == null)
                {
                    siteDataPage = new SiteDataPageViewModel(ServiceLocator.Get<IClientEndpoint>(), App.Current.Dispatcher);
                }
                return siteDataPage;
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "This non-static member is needed for data binding purposes.")]
        public InventoryPageViewModel InventoryPage {
            get {
                if (inventoryPage == null) {
                    inventoryPage = new InventoryPageViewModel(ServiceLocator.Get<IClientEndpoint>(), App.Current.Dispatcher);
                }
                return inventoryPage;
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "This non-static member is needed for data binding purposes.")]
        public ActiveIngredientPageViewModel ActiveIngredientPage {
            get {
                if (activeIngredientPage == null) {
                    activeIngredientPage = new ActiveIngredientPageViewModel(ServiceLocator.Get<IClientEndpoint>(), App.Current.Dispatcher);
                }
                return activeIngredientPage;
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "This non-static member is needed for data binding purposes.")]
        public CreateCropzoneViewModel CreateCropzoneVM {
            get {
                if (createcropzonevm == null) {
                    createcropzonevm = new CreateCropzoneViewModel(ServiceLocator.Get<IClientEndpoint>(), App.Current.Dispatcher);
                }
                return createcropzonevm;
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "This non-static member is needed for data binding purposes.")]
        public ShapeImportViewModel ShapeImportVM {
            get {
                if (shapeimportvm == null) {
                    shapeimportvm = new ShapeImportViewModel(ServiceLocator.Get<IClientEndpoint>(), App.Current.Dispatcher);
                }
                return shapeimportvm;
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "This non-static member is needed for data binding purposes.")]
        public ScalePolygonViewModel ScalePolygonVM {
            get {
                if (scalepolygonvm == null) {
                    var clientEndpoint = ServiceLocator.Get<IClientEndpoint>();
                    scalepolygonvm = new ScalePolygonViewModel(clientEndpoint);
                }
                return scalepolygonvm;
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "This non-static member is needed for data binding purposes.")]
        public SSurgoNrcsWfsViewModel SSurgoNrcsWfsVM {
            get {
                if (ssurgonrcswfsvm == null) {
                    var clientEndpoint = ServiceLocator.Get<IClientEndpoint>();
                    ssurgonrcswfsvm = new SSurgoNrcsWfsViewModel(clientEndpoint);
                }
                return ssurgonrcswfsvm;
            }
        }

        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "This non-static member is needed for data binding purposes.")]
        //public ThinkGeoMapsUtilityViewModel ThinkGeoUtitlityVM {
        //    get {
        //        if (thinkgeoutilityvm == null) {
        //            thinkgeoutilityvm = new ThinkGeoMapsUtilityViewModel();
        //        }
        //        return thinkgeoutilityvm;
        //    }
        //}

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "This non-static member is needed for data binding purposes.")]
        public CustomLabelViewModel CustomLabelVM {
            get {
                if (customlabelvm == null) {
                    customlabelvm = new CustomLabelViewModel();
                }
                return customlabelvm;
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "This non-static member is needed for data binding purposes.")]
        public ReportSelectionPageViewModel ReportSelection {
            get {
                if (reportSelectionPage == null) {
                    reportSelectionPage = new ReportSelectionPageViewModel(ServiceLocator.Get<IClientEndpoint>(), App.Current.Dispatcher);
                }
                return reportSelectionPage;
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "This non-static member is needed for data binding purposes.")]
        public EquipmentPageViewModel EquipmentPage
        {
            get
            {
                if (equipmentPage == null)
                {
                    equipmentPage = new EquipmentPageViewModel(ServiceLocator.Get<IClientEndpoint>(), App.Current.Dispatcher);
                }
                return equipmentPage;
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "This non-static member is needed for data binding purposes.")]
		public HarvestLoadsPageViewModel HarvestLoadsPage {
			get {
				if (harvestLoadsPage == null) {
					harvestLoadsPage = new HarvestLoadsPageViewModel(ServiceLocator.Get<IClientEndpoint>(), App.Current.Dispatcher, EquipmentPage);
				}
				return harvestLoadsPage;
			}
		}

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "This non-static member is needed for data binding purposes.")]
		public StorageLoadsPageViewModel StorageLoadsPage {
			get {
				if (storageLoadsPage == null) {
					storageLoadsPage = new StorageLoadsPageViewModel(ServiceLocator.Get<IClientEndpoint>(), App.Current.Dispatcher, EquipmentPage);
				}
				return storageLoadsPage;
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "This non-static member is needed for data binding purposes.")]
		public YieldLocationsPageViewModel YieldLocationsPage {
			get {
				if (yieldLocationsPage == null) {
					yieldLocationsPage = new YieldLocationsPageViewModel(ServiceLocator.Get<IClientEndpoint>(), App.Current.Dispatcher);
				}
				return yieldLocationsPage;
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "This non-static member is needed for data binding purposes.")]
		public CommoditiesPageViewModel CommoditiesPage {
			get {
				if (commoditiesPage == null) {
					commoditiesPage = new CommoditiesPageViewModel(ServiceLocator.Get<IClientEndpoint>(), App.Current.Dispatcher);
				}
				return commoditiesPage;
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "This non-static member is needed for data binding purposes.")]
		public CropSettingsPageViewModel CropSettingsPage {
			get {
				if (cropSettingsPage == null) {
					cropSettingsPage = new CropSettingsPageViewModel(ServiceLocator.Get<IClientEndpoint>(), App.Current.Dispatcher);
				}
				return cropSettingsPage;
			}
		}
	}
}