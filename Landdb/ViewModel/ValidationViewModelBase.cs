﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using Landdb.ViewModel.Secondary;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;

namespace Landdb.ViewModel {
	public abstract class ValidationViewModelBase : ViewModelBase, INotifyDataErrorInfo {

		//http://mark.mymonster.nl/2011/02/22/validating-our-viewmodel

		private readonly IDictionary<string, IList<PropertyValidationResult>> _propertyValidationResults = new Dictionary<string, IList<PropertyValidationResult>>();

		protected ValidationViewModelBase() { }

		public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

		public bool HasErrors => ErrorDescriptions.Any();
		public bool HasWarnings => WarningDescriptions.Any();

		public IEnumerable GetErrors(string propertyName) {
			var isEntityLevelError = string.IsNullOrWhiteSpace(propertyName);

			if (isEntityLevelError) {
				//yield return _errors.Values;
			} else if (_propertyValidationResults.ContainsKey(propertyName)) {
				foreach (var propertyError in _propertyValidationResults[propertyName]) {
					yield return propertyError;
				}
			}

			yield break;
		}

		public IEnumerable<PropertyValidationResult> ErrorDescriptions => from p in _propertyValidationResults
																		  from err in p.Value
																		  where err.Severity == PropertyValidationResult.ValidationResultSeverityTypes.Error
																		  select err;

		public IEnumerable<PropertyValidationResult> WarningDescriptions => from p in _propertyValidationResults
																			from war in p.Value
																			where war.Severity == PropertyValidationResult.ValidationResultSeverityTypes.Warning
																			select war;

		public bool ValidateViewModel() {
			ViewModelBase objectToValidate = this;
			_propertyValidationResults.Clear();
			Type objectType = objectToValidate.GetType();
			PropertyInfo[] properties = objectType.GetProperties();

			foreach (PropertyInfo property in properties) {
				if (property.GetCustomAttributes(typeof(ValidationAttribute), true).Any()) {
					object value = property.GetValue(objectToValidate, null);
					ValidateProperty(property.Name, value);
				}
			}

			return !HasErrors;
		}

		protected void ValidateAndRaisePropertyChanged(string propertyName, object value) {
			RaisePropertyChanged(propertyName);
			ValidateProperty(propertyName, value);
		}

		protected void ValidateAndRaisePropertyChanged<T>(Expression<Func<T>> propertyExpression) {
			RaisePropertyChanged(propertyExpression);

			ValidateProperty(propertyExpression);
		}

		protected void NotifyErrorsChanged(string propertyName) {
			ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs(propertyName));

			RaisePropertyChanged(() => ErrorDescriptions);
			RaisePropertyChanged(() => WarningDescriptions);

			RaisePropertyChanged(() => HasErrors);
			RaisePropertyChanged(() => HasWarnings);
		}

		protected bool ValidateProperty<T>(Expression<Func<T>> propertyExpression) {
			var name = ((MemberExpression)propertyExpression.Body).Member.Name;
			var value = propertyExpression.Compile()();

			return ValidateProperty(name, value);
		}

		protected bool ValidateProperty(string propertyName, object value) {
			ViewModelBase objectToValidate = this;
			var results = new List<ValidationResult>();
			var validationContext = new ValidationContext(objectToValidate, null, null) { MemberName = propertyName };
			bool isValid = Validator.TryValidateProperty(value, validationContext, results);

			if (!_propertyValidationResults.ContainsKey(propertyName)) {
				_propertyValidationResults.Add(propertyName, new List<PropertyValidationResult>());
			}

			if (isValid) {
				_propertyValidationResults[propertyName].Clear();
			} else {
				_propertyValidationResults[propertyName] = results.Select(x => new PropertyValidationResult(propertyName, x.ErrorMessage)).ToList();
			}

			NotifyErrorsChanged(propertyName);

			return isValid;		
		}
	}

	public class PropertyValidationResult {
		public PropertyValidationResult(string propertyName, string errorDescription) {
			PropertyName = propertyName;
			ValidationMessage = errorDescription;
			Severity = getSeverityFromMessage(errorDescription);
		}

		public string PropertyName { get; }
		public string ValidationMessage { get; }
		public ValidationResultSeverityTypes Severity { get; }

		public enum ValidationResultSeverityTypes { Error, Warning }

		private static ValidationResultSeverityTypes getSeverityFromMessage(string message) {
			var severity = ValidationResultSeverityTypes.Error;

			if (!string.IsNullOrWhiteSpace(message)) {
				var firstToken = message.Split(new char[] { ':' }).FirstOrDefault();

				if (!string.IsNullOrWhiteSpace(firstToken)) {
					Enum.TryParse(firstToken, out severity);
				}
			}

			return severity;
		}

		public override string ToString() => ValidationMessage;
	}
}