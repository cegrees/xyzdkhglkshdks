﻿using Landdb.Client.Infrastructure;
using Landdb.ViewModel.Fields;
using Landdb.ViewModel.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Landdb.ViewModel.Secondary.Analysis.FieldToMarket {
    public class FtmFieldSelectionViewModel : BaseFieldSelectionViewModel {
        Func<CropZoneTreeItemViewModel, Task> onFieldCheckedChanged;

        public FtmFieldSelectionViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, int cropYear, Func<CropZoneTreeItemViewModel, Task> onFieldCheckedChanged) : base(clientEndpoint, dispatcher, cropYear) {
            this.SelectedTreeDisplayMode = new TreeDisplayModeDisplayItem("Crops");
            this.onFieldCheckedChanged = onFieldCheckedChanged;
        }

        protected override void OnFieldCheckedChanged(Fields.CropZoneTreeItemViewModel treeItem) {
            if (onFieldCheckedChanged != null) {
                onFieldCheckedChanged(treeItem);
            }
            //if (treeItem is CropZoneTreeItemViewModel) {
            //    if (treeItem.IsChecked.HasValue && treeItem.IsChecked.Value) {
            //    }
            //}
        }
    }
}
