﻿using FieldToMarket.Contracts.Partial;
using FieldToMarket.Contracts.Reference;
using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Secondary.Analysis.FieldToMarket {
    public class FtmSetupItem : ViewModelBase {
        TillageManagementPractice selectedTillageManagementTemplate;
        List<TillageManagementPractice> tillageManagementTemplates;
        double projectedYield;
        Crop crop;
        double area;
        decimal? slopeGrade;
        decimal? slopeLength;
        bool isActualYield;
        bool isDryingUsed;

        public CropZoneId CropZoneId { get; set; }
        public FieldId ParentFieldId { get; set; }
        public int CropYear { get; set; }
        
        public string DisplayText { get; set; }
        public string AreaText { get; set; }
        public string TillageClass { get; set; }
        public string State { get; set; }

        private bool isRusle2;

        public bool IsRusle2 {
            get { return isRusle2; }
            set { 
                isRusle2 = value;
                RaisePropertyChanged(() => IsRusle2);
            }
        }

        public Crop Crop {
            get { return crop; }
            set {
                crop = value;
                RaisePropertyChanged(() => Crop);
                RaisePropertyChanged(() => IsValid);
            }
        }

        public TillageManagementPractice SelectedTillageManagementTemplate {
            get { return selectedTillageManagementTemplate; }
            set {
                selectedTillageManagementTemplate = value;
                RaisePropertyChanged(() => SelectedTillageManagementTemplate);
                RaisePropertyChanged(() => IsValid);
            }
        }

        public List<TillageManagementPractice> TillageManagementTemplates {
            get { return tillageManagementTemplates; }
            set {
                tillageManagementTemplates = value;
                RaisePropertyChanged(() => TillageManagementTemplates);
            }
        }

        public double ProjectedYield {
            get { return projectedYield; }
            set {
                if (!isActualYield || projectedYield <= 0) {
                    projectedYield = value;
                    RaisePropertyChanged(() => ProjectedYield);
                    RaisePropertyChanged(() => IsValid);
                }
            }
        }

        public double Area {
            get { return area; }
            set {
                area = value;
                RaisePropertyChanged(() => Area);
                RaisePropertyChanged(() => IsValid);
                RaisePropertyChanged(() => DeliveryDistance);
            }
        }

        public decimal? SlopeGrade {
            get { return slopeGrade; }
            set {
                slopeGrade = value;
                RaisePropertyChanged(() => SlopeGrade);
            }
        }

        public decimal? SlopeLength {
            get { return slopeLength; }
            set {
                slopeLength = value;
                RaisePropertyChanged(() => SlopeLength);
            }
        }

        public bool IsDryingUsed {
            get { return isDryingUsed; }
            set {
                isDryingUsed = value;
                RaisePropertyChanged(() => IsDryingUsed);
            }
        }

        double deliveryLoadSize;
        public double DeliveryLoadSize {
            get { return deliveryLoadSize; }
            set {
                deliveryLoadSize = value;
                RaisePropertyChanged(() => DeliveryLoadSize);
                RaisePropertyChanged(() => DeliveryDistance);
            }
        }

        double singleTripDeliveryDistance;
        public double SingleTripDeliveryDistance {
            get { return singleTripDeliveryDistance; }
            set {
                singleTripDeliveryDistance = value;
                RaisePropertyChanged(() => SingleTripDeliveryDistance);
                RaisePropertyChanged(() => DeliveryDistance);
            }
        }

        public double DeliveryDistance {
            get {
                if (deliveryLoadSize > 0) {
                    return Math.Ceiling((this.projectedYield * this.area) / this.deliveryLoadSize) * this.singleTripDeliveryDistance;
                } else { return 0; }
            }
            //get { return deliveryDistance; }
            //set {
            //    deliveryDistance = value;
            //    RaisePropertyChanged(() => DeliveryDistance);
            //}
        }

        // USLE props
        List<string> selectedUsleConservationPractices;
        public List<string> SelectedUsleConservationPractices {
            get { return selectedUsleConservationPractices; }
            set {
                selectedUsleConservationPractices = value;
                RaisePropertyChanged(() => SelectedUsleConservationPractices);
            }
        }

        string uslePreviousCrop;
        public string UslePreviousCrop {
            get { return uslePreviousCrop; }
            set {
                uslePreviousCrop = value;
                RaisePropertyChanged(() => UslePreviousCrop);
            }
        }

        string usleWindErosionSeverity;
        public string UsleWindErosionSeverity {
            get { return usleWindErosionSeverity; }
            set {
                usleWindErosionSeverity = value;
                RaisePropertyChanged(() => UsleWindErosionSeverity);
            }
        }

        double usleOperationalDieselUsePerArea;
        public double UsleOperationalDieselUsePerArea {
            get { return usleOperationalDieselUsePerArea; }
            set {
                usleOperationalDieselUsePerArea = value;
                RaisePropertyChanged(() => UsleOperationalDieselUsePerArea);
            }
        }

        string usleSoilTexture;
        public string UsleSoilTexture {
            get { return usleSoilTexture; }
            set {
                usleSoilTexture = value;
                RaisePropertyChanged(() => UsleSoilTexture);
            }
        }

        string usleTillageSystem;
        public string UsleTillageSystem {
            get { return usleTillageSystem; }
            set {
                usleTillageSystem = value;
                RaisePropertyChanged(() => UsleTillageSystem);
            }
        }

        public bool IsActualYield {
            get { return isActualYield; }
        }

        public string Boundary { get; set; }

        public void SetActualYield(double actualYield) {
            isActualYield = true;
            projectedYield = actualYield;
            RaisePropertyChanged(() => ProjectedYield);
            RaisePropertyChanged(() => IsValid);
            RaisePropertyChanged(() => IsActualYield);
        }

        public bool IsValid {
            get {
                if (Crop == null) { return false; }
                if (CropZoneId == null || Area <= 0 || ParentFieldId == null || CropYear <= 1980) { return false; }

                if (Crop.CalculationMethod == CalculationMethod.RUSLE2) {
                    return SelectedTillageManagementTemplate != null;
                } else if(Crop.CalculationMethod == CalculationMethod.UniversalSoilLoss) {
                    return !string.IsNullOrWhiteSpace(UsleTillageSystem) && !string.IsNullOrWhiteSpace(UslePreviousCrop) && !string.IsNullOrWhiteSpace(UsleSoilTexture) && !string.IsNullOrWhiteSpace(UsleTillageSystem) && UsleOperationalDieselUsePerArea > 0 && !string.IsNullOrWhiteSpace(State);
                } else { return false; }
            }
        }
    }
}
