﻿using FieldToMarket.Contracts.Reference;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure.Messages;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Landdb.ViewModel.Secondary.Analysis.FieldToMarket {
    public class DeliveryItemViewModel : ViewModelBase {
        double averageMilesPerGallon;
        double loadSize;
        string loadUnit;
        double roundtripDistance;
        FuelType selectedFuelType;
        Func<DeliveryItemViewModel, bool, Task> onCompleteAsync;

        public DeliveryItemViewModel(ObservableCollection<FuelType> availableFuelTypes, string loadUnit, Func<DeliveryItemViewModel, bool, Task> onCompleteAsync = null) {
            this.AvailableFuelTypes = availableFuelTypes;
            this.onCompleteAsync = onCompleteAsync;
            this.loadUnit = loadUnit;

            CompleteCommand = new AwaitableRelayCommand(() => OnComplete(true));
            CancelCommand = new AwaitableRelayCommand(() => OnComplete(false));
        }

        async Task OnComplete(bool complete) {
            if (onCompleteAsync != null) {
                await onCompleteAsync(this, complete);
            }
            IsComplete = complete;
            RaisePropertyChanged(() => IsComplete);
            RaisePropertyChanged(() => IsValid);
            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
        }

        public ICommand CompleteCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }

        public bool IsComplete { get; private set; }
        public bool IsValid {
            //get { return selectedFuelType != null && TotalDistance > 0 && AverageMilesPerGallon > 0; }
            get { return selectedFuelType != null && RoundtripDistance > 0 && LoadSize > 0 && AverageMilesPerGallon > 0; }
        }

        public ObservableCollection<FuelType> AvailableFuelTypes { get; private set; }

        public double AverageMilesPerGallon {
            get { return averageMilesPerGallon; }
            set {
                averageMilesPerGallon = value;
                RaisePropertyChanged(() => AverageMilesPerGallon);
            }
        }

        //public double TotalDistance {
        //    get { return totalDistance; }
        //    set {
        //        totalDistance = value;
        //        RaisePropertyChanged(() => TotalDistance);
        //    }
        //}

        public double LoadSize {
            get { return loadSize; }
            set {
                loadSize = value;
                RaisePropertyChanged(() => LoadSize);
            }
        }

        public string LoadUnit {
            get { return loadUnit; }
            set {
                loadUnit = value;
                RaisePropertyChanged(() => LoadUnit);
            }
        }

        public double RoundtripDistance {
            get { return roundtripDistance; }
            set {
                roundtripDistance = value;
                RaisePropertyChanged(() => RoundtripDistance);
            }
        }

        public FuelType SelectedFuelType {
            get { return selectedFuelType; }
            set {
                selectedFuelType = value;
                RaisePropertyChanged(() => SelectedFuelType);
            }
        }

    }
}
