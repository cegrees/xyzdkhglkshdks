﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Secondary.Analysis.FieldToMarket {
    public class ConservationPracticeViewModel : ViewModelBase {
        public ConservationPracticeViewModel(string name) {
            this.name = name;
        }

        private string name;
        public string Name {
            get { return name; }
            set { 
                name = value;
                RaisePropertyChanged(() => Name);
            }
        }

        private bool isSelected;
        public bool IsSelected {
            get { return isSelected; }
            set { 
                isSelected = value;
                RaisePropertyChanged(() => IsSelected);
            }
        }

    }
}
