﻿using FieldToMarket.Contracts.Reference;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Client.Services;
using Landdb.Client.Spatial;
using Landdb.Domain.ReadModels.Map;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Analysis.FieldToMarket;
using Landdb.ViewModel.Overlays;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using AgC.UnitConversion;
using Landdb.Resources;
using FtmPartial = FieldToMarket.Contracts.Partial;
using FtmC = FieldToMarket.Contracts;
using Landdb.ViewModel.Shared.Popups;
using Landdb.Views.Shared.Popups;

namespace Landdb.ViewModel.Secondary.Analysis.FieldToMarket {
    public class FtmAddFieldsViewModel : ViewModelBase {
        // TODO: Make this an interface, and ctor-injected
        SustainabilityServices sustainabilityServices = new SustainabilityServices(ApplicationEnvironment.FieldToMarketRemoteUri);
        SustainabilityServiceClient sustainabilityClient = new SustainabilityServiceClient();

        IClientEndpoint clientEndpoint;
        Dispatcher dispatcher;
        int currentCropYear;

        DryingSystem selectedDryingSystem;
        TillageManagementPractice selectedTillageManagementTemplate;
        ObservableCollection<DeliveryItemViewModel> deliveries = new ObservableCollection<DeliveryItemViewModel>();
        DeliveryItemViewModel selectedDelivery;
        double projectedYield;
        double percentMoistureToRemove = 0;
        double percentYieldToDry = 1;
        Point? mostRecentPoint = null;
        int currentUiPage = 0;

        private FtmAddFieldsViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, int currentCropYear) {
            FieldsPageModel = new FtmFieldSelectionViewModel(clientEndpoint, dispatcher, currentCropYear, OnFieldChanged);

            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            this.currentCropYear = currentCropYear;

            SetupItems = new ObservableCollection<FtmSetupItem>();
            TillageManagementTemplateCache = new Dictionary<Guid, List<TillageManagementPractice>>();

            CancelCommand = new RelayCommand(() => Messenger.Default.Send<ShowMainViewMessage>(new ShowMainViewMessage()));
            CompleteCommand = new AwaitableRelayCommand(OnComplete, CanComplete);

            AddDeliveryItemCommand = new RelayCommand(AddNewDeliveryItem);
            RemoveSelectedDeliveryItemCommand = new RelayCommand(() => RemoveDeliveryItem(Deliveries.FirstOrDefault()));
        }

        public static async Task<FtmAddFieldsViewModel> Create(IClientEndpoint clientEndpoint, Dispatcher dispatcher, int currentCropYear) {
            var instance = new FtmAddFieldsViewModel(clientEndpoint, dispatcher, currentCropYear);
            await instance.Initialize();
            return instance;
        }

        public ICommand CancelCommand { get; private set; }
        public AwaitableRelayCommand CompleteCommand { get; private set; }
        public ICommand AddDeliveryItemCommand { get; private set; }
        public ICommand RemoveSelectedDeliveryItemCommand { get; private set; }

        #region Page Models
        public FtmFieldSelectionViewModel FieldsPageModel { get; private set; }

        public int CurrentUiPage {
            get { return currentUiPage; }
            set { 
                currentUiPage = value;
                CompleteCommand.RaiseCanExecuteChanged();
            }
        }
        #endregion

        public ObservableCollection<FtmSetupItem> SetupItems { get; private set; }

        #region Collections
        public ObservableCollection<string> TillageClasses { get; private set; }
        public ObservableCollection<ConservationPracticeViewModel> ConservationPractices { get; private set; }
        public ObservableCollection<string> SoilTextures { get; private set; }
        public ObservableCollection<string> UslePreviousCrops { get; private set; }
        public ObservableCollection<string> UsleWindSeverities { get; private set; }
        public ObservableCollection<FuelType> FuelTypes { get; private set; }
        public ObservableCollection<DryingSystem> DryingSystems { get; private set; }
        public List<Crop> Crops { get; private set; }

        List<TillageManagementPractice> tillageManagementTemplates;
        public List<TillageManagementPractice> TillageManagementTemplates {
            get { return tillageManagementTemplates; }
            private set {
                tillageManagementTemplates = value;
                RaisePropertyChanged(() => TillageManagementTemplates);
            } 
        }

        public Dictionary<Guid, List<TillageManagementPractice>> TillageManagementTemplateCache { get; private set; }

        #endregion

        #region Global Template Properties
        public TillageManagementPractice SelectedTillageManagementTemplate {
            get { return selectedTillageManagementTemplate; }
            set {
                selectedTillageManagementTemplate = value;
                RaisePropertyChanged(() => SelectedTillageManagementTemplate);

                if (value != null) {
                    foreach (var item in SetupItems) {
                        item.SelectedTillageManagementTemplate = item.TillageManagementTemplates.Where(x => x.Pointer == value.Pointer).FirstOrDefault();
                    }
                }
            }
        }

        public DryingSystem SelectedDryingSystem {
            get { return selectedDryingSystem; }
            set {
                selectedDryingSystem = value;
                RaisePropertyChanged(() => SelectedDryingSystem);

                foreach (var item in SetupItems) {
                    item.IsDryingUsed = true;
                }
            }
        }

        public ObservableCollection<DeliveryItemViewModel> Deliveries {
            get { return deliveries; }
        }

        // Only used for managing selection for removal atm.
        public DeliveryItemViewModel SelectedDelivery {
            get { return selectedDelivery; }
            set {
                selectedDelivery = value;
                RaisePropertyChanged(() => SelectedDelivery);
            }
        }

        public double ProjectedYield {
            get { return projectedYield; }
            set {
                projectedYield = value;
                RaisePropertyChanged(() => ProjectedYield);

                foreach (var item in SetupItems) {
                    item.ProjectedYield = value;
                }
            }
        }

        public double PercentMoistureToRemove {
            get { return percentMoistureToRemove; }
            set {
                percentMoistureToRemove = value;
                RaisePropertyChanged(() => PercentMoistureToRemove);
            }
        }

        public double PercentYieldToDry {
            get { return percentYieldToDry; }
            set {
                percentYieldToDry = value;
                RaisePropertyChanged(() => PercentYieldToDry);
            }
        }
        #endregion

        #region USLE
        string selectedTillageSystem;
        public string SelectedTillageSystem {
            get { return selectedTillageSystem; }
            set {
                selectedTillageSystem = value;
                RaisePropertyChanged(() => SelectedTillageSystem);

                foreach (var item in SetupItems) {
                    item.UsleTillageSystem = value;
                }
            }
        }

        string selectedSoilTexture;
        public string SelectedSoilTexture {
            get { return selectedSoilTexture; }
            set {
                selectedSoilTexture = value;
                RaisePropertyChanged(() => SelectedSoilTexture);

                foreach (var item in SetupItems) {
                    item.UsleSoilTexture = value;
                }
            }
        }

        double usleOperationalDieselUse;
        public double UsleOperationalDieselUse {
            get { return usleOperationalDieselUse; }
            set {
                usleOperationalDieselUse = value;
                RaisePropertyChanged(() => UsleOperationalDieselUse);

                foreach (var item in SetupItems) {
                    item.UsleOperationalDieselUsePerArea = value;
                }
            }
        }

        private string selectedUslePreviousCrop;
        public string SelectedUslePreviousCrop {
            get { return selectedUslePreviousCrop; }
            set { 
                selectedUslePreviousCrop = value;
                RaisePropertyChanged(() => SelectedUslePreviousCrop);

                foreach (var item in SetupItems) {
                    item.UslePreviousCrop = value;
                }
            }
        }

        private string selectedUsleWindSeverity;
        public string SelectedUsleWindSeverity {
            get { return selectedUsleWindSeverity; }
            set { 
                selectedUsleWindSeverity = value;
                RaisePropertyChanged(() => SelectedUsleWindSeverity);

                foreach (var item in SetupItems) {
                    item.UsleWindErosionSeverity = value;
                }
            }
        }


        #endregion

        async Task Initialize() {
            var tillageClasses = await sustainabilityServices.GetTillageClasses();
            TillageClasses = new ObservableCollection<string>(tillageClasses);

            var fuelTypes = await sustainabilityServices.GetFuelTypes();
            FuelTypes = new ObservableCollection<FuelType>(fuelTypes);

            var crops = await sustainabilityServices.GetCrops();
            Crops = new List<Crop>(crops);

            var dryingSystems = await sustainabilityServices.GetDryingSystems();
            DryingSystems = new ObservableCollection<DryingSystem>(dryingSystems);
        }

        bool isLoadingTillageTemplates = false;

        public bool IsLoadingTillageTemplate {
            get { return isLoadingTillageTemplates; }
            set {
                isLoadingTillageTemplates = value;
                RaisePropertyChanged(() => IsLoadingTillageTemplate);
            }
        }

        public bool IsRusle2Calculation {
            get { return CurrentCalculationMethod == CalculationMethod.RUSLE2; }
        }

        public CalculationMethod CurrentCalculationMethod {
            get {
                if (currentCrop == null) { return CalculationMethod.Undefined; }
                return currentCrop.CalculationMethod;
            }
        }

        public string CropUnitOfMeasure {
            get {
                if (currentCrop == null) { return string.Empty; }
                return currentCrop.UnitOfMeasure;
            }
        }

        //DEBUG: If there's an issue with the View displaying the wrong area, then this is most likely the problem. I just used the default area unit. 
        public string PerAreaDisplay => $"{Strings.Per_Text.ToLower()} {UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).AbbreviatedDisplay}";

        Crop currentCrop = null;
        public Crop CurrentCrop {
            get { return currentCrop; }
            set {
                currentCrop = value;
                RaisePropertyChanged(() => CurrentCrop);
                RaisePropertyChanged(() => CurrentCalculationMethod);
                RaisePropertyChanged(() => IsRusle2Calculation);
                RaisePropertyChanged(() => CropUnitOfMeasure);
            }
        }

        async Task EnsureUsleCollectionsAreLoaded() {
            if (SoilTextures == null || !SoilTextures.Any()) {
                var soilTextures = await sustainabilityServices.GetSoilTextures();
                SoilTextures = new ObservableCollection<string>(soilTextures);
                RaisePropertyChanged(() => SoilTextures);
            }
            if (ConservationPractices == null || !ConservationPractices.Any()) {
                var conservationPractices = await sustainabilityServices.GetConservationPractices();
                ConservationPractices = new ObservableCollection<ConservationPracticeViewModel>();
                foreach (var cp in conservationPractices) {
                    ConservationPractices.Add(new ConservationPracticeViewModel(cp));
                }
                RaisePropertyChanged(() => ConservationPractices);
            }
            if (UslePreviousCrops == null || !UslePreviousCrops.Any()) {
                var previousCrops = await sustainabilityServices.GetPreviousCrops();
                UslePreviousCrops = new ObservableCollection<string>(previousCrops);
                RaisePropertyChanged(() => UslePreviousCrops);
            }
            if (UsleWindSeverities == null || !UsleWindSeverities.Any()) {
                var windSeverities = await sustainabilityServices.GetWindSeverities();
                UsleWindSeverities = new ObservableCollection<string>(windSeverities);
                RaisePropertyChanged(() => UsleWindSeverities);
            }
        }

        public async Task OnFieldChanged(Fields.CropZoneTreeItemViewModel treeItem) {
            if (treeItem.IsChecked.HasValue) {
                if (treeItem.IsChecked.Value) {
                    if (!SetupItems.Any(x => x.CropZoneId == treeItem.CropZoneId)) {
                        var parentTreeItem = treeItem.Parent as Landdb.ViewModel.Fields.FieldTreeItemViewModel;

                        var cz = clientEndpoint.GetView<CropZoneDetailsView>(treeItem.CropZoneId).GetValue(new CropZoneDetailsView());
                        var field = clientEndpoint.GetView<FieldDetailsView>(parentTreeItem.FieldId).GetValue(new FieldDetailsView());
                        
                        var newItem = new FtmSetupItem() { CropZoneId = treeItem.CropZoneId, ParentFieldId = ((Fields.FieldTreeItemViewModel)treeItem.Parent).FieldId, CropYear = currentCropYear, Area = treeItem.Area.GetValueAs(AgC.UnitConversion.Area.Acre.Self), AreaText = treeItem.Area.ToString(), 
                            SlopeLength = field.SlopeLength, SlopeGrade = field.SlopePercent };

                        newItem.DisplayText = string.Format("{0} : {1} : {2}", parentTreeItem != null && parentTreeItem.Parent != null ? parentTreeItem.Parent.ToString() : $"<{Strings.Unknown_Text.ToLower()}>", parentTreeItem != null ? parentTreeItem.Name : $"<{Strings.Unknown_Text.ToLower()}>", treeItem.Name);

                        var state = clientEndpoint.GetMasterlistService().GetStateByName(field.State);
                        string stateAbbreviation = state != null ? state.Abbreviation : null;

                        newItem.State = stateAbbreviation;
                        if (string.IsNullOrWhiteSpace(stateAbbreviation)) {
                            stateAbbreviation = "--";
                            //treeItem.IsChecked = false;
                            //return;
                        }

                        newItem.Crop = await LookupFtmCrop(stateAbbreviation, treeItem.CropId);
                        

                        if (newItem.Crop != null) {
                            if (CurrentCrop == null) {
                                CurrentCrop = newItem.Crop;
                                if (CurrentCalculationMethod == CalculationMethod.UniversalSoilLoss) { await EnsureUsleCollectionsAreLoaded(); }
                            }

                            if (newItem.Crop == CurrentCrop) {
                                var yield = SummaryServices.GetYieldData(clientEndpoint, new CropYearId(treeItem.CropZoneId.DataSourceId, currentCropYear), treeItem.CropZoneId);
                                if (yield.Any() && newItem.Area > 0) {
                                    if (yield.Any(x => x.Key.PackageUnitName == CurrentCrop.UnitOfMeasure)) {
                                        var yieldQuantity = yield.FirstOrDefault(x => x.Key.PackageUnitName == CurrentCrop.UnitOfMeasure).Value;
                                        newItem.SetActualYield(yieldQuantity.TotalQuantity / newItem.Area);
                                    }
                                }

                                newItem.IsRusle2 = CurrentCalculationMethod == CalculationMethod.RUSLE2;
                                SetupItems.Add(newItem);

                                #region Map gen & tillage profile merging
                                var mapItem = clientEndpoint.GetView<ItemMap>(treeItem.Id);
                                if (mapItem.HasValue && mapItem.Value.MostRecentMapItem != null) {
                                    newItem.Boundary = mapItem.Value.MostRecentMapItem.MapData;
                                    var centerPoint = GeometryFromWKT.GetCenterPoint(newItem.Boundary);
                                    mostRecentPoint = centerPoint;

                                    IsLoadingTillageTemplate = true;
                                    newItem.TillageManagementTemplates = await GetTillageManagementTemplates(treeItem.CropZoneId.Id, newItem.Crop, centerPoint);
                                    await MergeTillageProfiles(newItem.TillageManagementTemplates);
                                    IsLoadingTillageTemplate = false;
                                }
                                #endregion
                            } else {
                                treeItem.IsChecked = false; // Unselect the field because of incompatibility
                                // TODO: Display a message showing incompatibility.
                            }
                        } else {
                            treeItem.IsChecked = false; // Unselect the field because of incompatibility
                                                        // TODO: Display a message showing crop incompatibility.

                            MessageDialogViewModel vm = new MessageDialogViewModel(Strings.FieldSelectionIssue_Text.ToUpper(), Strings.PleaseCheckThatTheRequiredItemsAreCompletedOnFieldInfo_Text, Strings.ButtonText_OK, null);
                            Messenger.Default.Send(new ShowPopupMessage()
                            {
                                ScreenDescriptor = new ScreenDescriptor(typeof(MessageDialog), "messageDialog", vm)
                            });
                        }
                    }
                } else {
                    var czs = SetupItems.Where(x => x.CropZoneId == treeItem.CropZoneId).ToList();
                    foreach (var cz in czs) {
                        SetupItems.Remove(cz);
                    }
                    RebuildTillageManagementProfiles();

                    if (!SetupItems.Any()) { CurrentCrop = null; } // Reset this if they uncheck everything.
                }
                RaisePropertyChanged("SetupItems");
            }
        }

        #region Manage Tillage profiles
        async Task RebuildTillageManagementProfiles() {
            var previouslySelectedTemplate = SelectedTillageManagementTemplate == null ? string.Empty : SelectedTillageManagementTemplate.Pointer;

            List<TillageManagementPractice> practices = null;

            foreach (var item in SetupItems) {
                if (practices == null) {
                    practices = new List<TillageManagementPractice>(item.TillageManagementTemplates);
                } else {
                    practices.Intersect(item.TillageManagementTemplates);
                }
            }

            TillageManagementTemplates = practices;

            SelectedTillageManagementTemplate = (from tmt in TillageManagementTemplates ?? new List<TillageManagementPractice>()
                                                 where tmt.Pointer == previouslySelectedTemplate
                                                 select tmt).FirstOrDefault();
        }

        async Task MergeTillageProfiles(IList<TillageManagementPractice> newPractices) {
            var previouslySelectedTemplate = SelectedTillageManagementTemplate == null ? string.Empty : SelectedTillageManagementTemplate.Pointer;

            if (TillageManagementTemplates == null) {
                TillageManagementTemplates = new List<TillageManagementPractice>(newPractices);
            } else {
                TillageManagementTemplates = TillageManagementTemplates.Intersect(newPractices, new TillageManagementEqualityComparer()).ToList();
            }

            SelectedTillageManagementTemplate = (from tmt in TillageManagementTemplates
                                                 where tmt.Pointer == previouslySelectedTemplate
                                                 select tmt).FirstOrDefault();
        }

        async Task<List<TillageManagementPractice>> GetTillageManagementTemplates(Guid cropzoneId, Crop crop, Point? centerPoint) {
            if (crop == null) { return new List<TillageManagementPractice>(); }

            if (TillageManagementTemplateCache.ContainsKey(cropzoneId)) {
                return TillageManagementTemplateCache[cropzoneId];
            } else {
                var practices = await sustainabilityServices.GetTillageManagementPractices(crop.CropName, mostRecentPoint.Value.Y, mostRecentPoint.Value.X);
                var tillageList = new List<TillageManagementPractice>(practices);
                TillageManagementTemplateCache.Add(cropzoneId, tillageList);
                return tillageList;
            }
        }
        #endregion

        public async Task OnComplete() {
            var currentDataSourceId = ApplicationEnvironment.CurrentDataSource.DataSourceId;
            var settings = clientEndpoint.GetPersistentView<FtmSettingsModel>(new CropYearId(currentDataSourceId, currentCropYear)).GetValue(new FtmSettingsModel());

            ProgressOverlayViewModel vm = new ProgressOverlayViewModel() { TotalRecords = SetupItems.Count };
            ScreenDescriptor progressDescriptor = new ScreenDescriptor("Landdb.Views.Overlays.FtmRegistrationProgressOverlayView", "registerprocess", vm);
            Messenger.Default.Send<ShowOverlayMessage>(new ShowOverlayMessage() { ScreenDescriptor = progressDescriptor });

            try {
                foreach (var item in SetupItems) {
                    vm.StatusMessage = string.Format(Strings.Registering_Format_Text, item.DisplayText);
                    
                    if (item.IsValid) {
                        FieldInputs inputs = new FieldInputs() {
                            Area = item.Area,
                            BoundaryWkt = item.Boundary,
                            Yield = item.ProjectedYield,
                        };

                        if (!string.IsNullOrWhiteSpace(item.State)) {
                            var state = clientEndpoint.GetMasterlistService().GetStateByName(item.State);
                            string stateAbbreviation = state != null ? state.Abbreviation : null;
                            inputs.State = stateAbbreviation;
                        }

                        if (item.SlopeLength != null && item.SlopeGrade != null) {
                            inputs.SlopeGrade = (double)item.SlopeGrade.Value;
                            inputs.SlopeLength = (double)item.SlopeLength.Value;
                        }

                        if (SelectedDryingSystem != null && item.IsDryingUsed) {
                            inputs.DryerType = SelectedDryingSystem.Name;
                            inputs.PercentOfYieldToDry = PercentYieldToDry;
                            inputs.PointsOfMoistureToRemove = PercentMoistureToRemove;
                        }

                        if (Deliveries.Any()) {
                            var deliveryItems = from d in Deliveries
                                                select new DeliveryItem() { AverageMilesPerGallon = d.AverageMilesPerGallon, FuelType = d.SelectedFuelType.FuelName, TotalTransportDistance = Math.Ceiling((item.ProjectedYield * item.Area) / d.LoadSize) * d.RoundtripDistance};

                            inputs.DeliveryItems = deliveryItems.ToList();

                            if (inputs.DeliveryItems.Any() && item.DeliveryDistance > 0) {
                                inputs.DeliveryItems[0].TotalTransportDistance = item.DeliveryDistance;
                            }

                            
                        }

                        if (CurrentCalculationMethod == CalculationMethod.RUSLE2) {
                            inputs.NRCS_TillageManagementProfile = item.SelectedTillageManagementTemplate.Pointer;
                            inputs.TillagePractice = item.SelectedTillageManagementTemplate.TillageClass;
                        } else if (CurrentCalculationMethod == CalculationMethod.UniversalSoilLoss) {
                            List<string> selectedCp = ConservationPractices.Where(x => x.IsSelected).Select(x => x.Name).ToList();

                            inputs.USLE_DieselUsePerArea = this.UsleOperationalDieselUse;
                            inputs.USLE_PreviousCrop = this.SelectedUslePreviousCrop;
                            inputs.USLE_SoilTexture = this.SelectedSoilTexture;
                            inputs.USLE_TillageSystem = this.SelectedTillageSystem;
                            inputs.USLE_WindErosionSeverity = this.SelectedUsleWindSeverity;
                            inputs.USLE_ConservationPractices = selectedCp;
                        }

                        var status = await sustainabilityClient.RegisterField(currentDataSourceId.ToString(), item.Crop.CropName, item.CropZoneId.Id.ToString(), item.CropYear, item.ParentFieldId.Id.ToString(), inputs);

                        if (status == FtmFieldCreationStatus.AlreadyExists) {
                            var replaceStatus = await sustainabilityClient.ReplaceInputs(currentDataSourceId.ToString(), item.Crop.CropName, item.CropZoneId.Id.ToString(), inputs);
                        }
                        vm.CurrentRecord += 1;

                        if (status == FtmFieldCreationStatus.Created || status == FtmFieldCreationStatus.AlreadyExists) {
                            if (!settings.CropZones.Any(x => x.CropZoneId == item.CropZoneId && x.Crop == item.Crop.CropName)) {
                                settings.CropZones.Add(new FtmSettingsItem() { CropZoneId = item.CropZoneId, Crop = item.Crop.CropName });
                                clientEndpoint.SavePersistentEntity<FtmSettingsModel>(new CropYearId(currentDataSourceId, currentCropYear), settings);
                            }

                        }
                    } else { vm.CurrentRecord += 1; }
                }
            } finally {
                Messenger.Default.Send<HideOverlayMessage>(new HideOverlayMessage());
            }

            Messenger.Default.Send<ShowMainViewMessage>(new ShowMainViewMessage());
        }

        public bool CanComplete() {
            return SetupItems.Any(x => x.IsValid) && CurrentUiPage == 2; // Best way I know of at present to make sure Complete is only enabled when on the final page.
        }

        public bool CanAddDelivery {
            get { return !Deliveries.Any(); }
        }

        public void AddNewDeliveryItem() {
            DeliveryItemViewModel vm = new DeliveryItemViewModel(FuelTypes, currentCrop.UnitOfMeasure, async (item, complete) => {
                if (complete && item.IsValid) {
                    Deliveries.Add(item);
                    RaisePropertyChanged(() => Deliveries);
                    RaisePropertyChanged(() => CanAddDelivery);

                    if (Deliveries.Count == 1) {
                        foreach (var si in SetupItems) {
                            var d = Deliveries.First();
                            si.DeliveryLoadSize = d.LoadSize;
                            si.SingleTripDeliveryDistance = d.RoundtripDistance;
                            //si.DeliveryDistance = ((si.ProjectedYield * si.Area) / d.LoadSize) * d.RoundtripDistance;
                        }
                    }
                }
            });
            ScreenDescriptor sd = new ScreenDescriptor("Landdb.Views.Secondary.Analysis.FieldToMarket.FtmAddDeliveryItem", "addDelivery", vm);
            Messenger.Default.Send<ShowPopupMessage>(new ShowPopupMessage() { ScreenDescriptor = sd });
        }

        public void RemoveDeliveryItem(DeliveryItemViewModel delivery) {
            if (delivery != null) {
                Deliveries.Remove(delivery);
                RaisePropertyChanged(() => CanAddDelivery);
                foreach (var si in SetupItems) {
                    si.SingleTripDeliveryDistance = 0;
                    si.DeliveryLoadSize = 0;
                }
            }
        }

        Dictionary<string, Crop> cropLookupCache = new Dictionary<string, Crop>();
        readonly AsyncLock cropLookupLock = new AsyncLock();

        async Task<Crop> LookupFtmCrop(string state, CropId cropId) {
            using (var releaser = await cropLookupLock.LockAsync()) {
                string lookupKey = string.Format("{0}!{1}", state, cropId.Id.ToString());

                if (cropLookupCache.ContainsKey(lookupKey)) {
                    return cropLookupCache[lookupKey];
                }

                Guid cropGuid = cropId.Id;
                Guid subcropGuid = Guid.Empty;

                var crop = clientEndpoint.GetMasterlistService().GetCrop(cropId);

                if (crop.ParentId.HasValue) {
                    subcropGuid = cropGuid; // If there's a parent ID, it means what we thought *was* the crop ID is actually the subcrop ID

                    while (crop.ParentId.HasValue) { // get the top-level crop
                        crop = clientEndpoint.GetMasterlistService().GetCrop(new CropId(crop.ParentId.Value));
                        cropGuid = crop.Id;
                    }
                }

                var searchName = await sustainabilityServices.LookupFtmCropByAgCId(state, cropGuid, subcropGuid);

                var q = from c in Crops
                        where c.CropName == searchName
                        select c;

                var ftmCrop = q.FirstOrDefault();

                cropLookupCache.Add(lookupKey, ftmCrop);

                if (ftmCrop != null) {
                    return ftmCrop;
                } else {
                    return null;
                    //throw new ApplicationException("Couldn't find a matching crop");
                }
            }
        }

    }

    public class TillageManagementEqualityComparer : IEqualityComparer<TillageManagementPractice> {

        public bool Equals(TillageManagementPractice x, TillageManagementPractice y) {
            if (x == null && y == null) { return true; }
            if (x == null || y == null) { return false; }

            return x.Pointer == y.Pointer;
        }

        public int GetHashCode(TillageManagementPractice obj) {
            if (obj == null) { return -1; }
            return obj.Pointer.GetHashCode();
        }
    }

    
}
