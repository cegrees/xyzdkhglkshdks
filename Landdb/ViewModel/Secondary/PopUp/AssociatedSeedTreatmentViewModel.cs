﻿using AgC.UnitConversion;
using AgC.UnitConversion.MassAndWeight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Domain.ReadModels.Product;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Secondary.Application;
using Landdb.ViewModel.Shared;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.ComponentModel.DataAnnotations;
using Landdb.Resources;

namespace Landdb.ViewModel.Secondary.PopUp
{

    public class AssociatedSeedTreatmentViewModel : ValidationViewModelBase
    {
        readonly IClientEndpoint clientEndpoint;
        Action<AssociatedSeedTreatmentViewModel> _addAssociatedProduct;
        private Measure _ratePerArea;
        private Measure totalArea;
        private MiniProduct product;

        Task productLoadTask;
        private InventoryListView inventory;

        TabItem item;
        int tabIndex = 0;

        public AssociatedSeedTreatmentViewModel(IClientEndpoint clientEndpoint, Measure RatePerArea, Measure TotalArea, MiniProduct product, Action<AssociatedSeedTreatmentViewModel> addassociatedProduct)
        {
            this.clientEndpoint = clientEndpoint;
            this._ratePerArea = RatePerArea;
            this.totalArea = TotalArea;
            this.product = product;

            SelectedTabIndex = 0;
            RaisePropertyChanged(() => SelectedTabIndex);
            Products = new ObservableCollection<ProductListItemDetails>();

            Task.Run(() =>
            {
                BuildProductList();
            });

            SelectedProduct = null;
            RaisePropertyChanged(() => SelectedProduct);
            ProductSearchText = string.Empty;
            RaisePropertyChanged(() => ProductSearchText);

            IsSeedVisible = true;
            IsBagVisible = true;
            IsCwtVisible = true;
            IsRowVisible = true;

            TabVisiblity();

            inventory = clientEndpoint.GetView<InventoryListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear)).GetValue(new InventoryListView());

            this._addAssociatedProduct += addassociatedProduct;

            Close = new RelayCommand(ClosePopUp);
            Update = new RelayCommand(UpdateProducts);
        }

        public RelayCommand Close { get; set; }
        public RelayCommand Update { get; set; }

        public int SelectedTabIndex
        {
            get
            {
                return tabIndex;
            }
            set
            {
                tabIndex = value == -1 ? 0 : value;
            }
        }

        public TabItem TabItem
        {
            get { return item; }
            set
            {
                item = value;
                TabSelectionChanged();
            }
        }

        public bool IsSeedVisible { get; set; }
        public bool IsBagVisible { get; set; }
        public bool IsCwtVisible { get; set; }
        public bool IsRowVisible { get; set; }

        #region Products Items
        private bool _isProductListVisible;
        public bool IsProductListVisible
        {
            get { return _isProductListVisible; }
            set
            {
                _isProductListVisible = value;
                RaisePropertyChanged(() => IsProductListVisible);
            }
        }

        public string ProductSearchText { get; set; }

        public ObservableCollection<ProductListItemDetails> Products { get; private set; }
        public ObservableCollection<ProductListItemDetails> FilteredProducts { get; private set; }

        private ProductListItemDetails _selectedProduct;
        public ProductListItemDetails SelectedProduct
        {
            get { return _selectedProduct; }
            set
            {
                _selectedProduct = value;
                RaisePropertyChanged(() => SelectedProduct);
                PopulateUnits();
                TabVisiblity();
            }
        }

        
        #endregion

        #region PER SEED ITEMS

        private decimal _mgAIPerSeedValue { get; set; }
        public decimal MGActivePerSeedValue { get { return _mgAIPerSeedValue; } set { _mgAIPerSeedValue = value; ReCalculate(); } }

        public List<IUnit> AvailableRatePerSeedUnits { get; set; }

        private IUnit _selectedRatePerSeedUnit { get; set; }
        public IUnit SelectedRatePerSeedUnit { get { return _selectedRatePerSeedUnit; } set { _selectedRatePerSeedUnit = value; ReCalculate(); } }

        #endregion

        #region PER BAG ITEMS
        private decimal _ratePerBagValue { get; set; }
        public decimal RatePerBagValue { get { return _ratePerBagValue; } set { _ratePerBagValue = value; ReCalculate(); } }

        public List<IUnit> AvailableRatePerBagUnits { get; set; }

        private IUnit _selectedRatePerBagUnit { get; set; }
        public IUnit SelectedRatePerBagUnit { get { return _selectedRatePerBagUnit; } set { _selectedRatePerBagUnit = value; ReCalculate(); } }
        #endregion

        #region PER 1000 FT ROW ITEMS
        public decimal RowWidth { get; set; }
        private decimal _ratePer1000Value { get; set; }
        public decimal RatePer1000Value { get { return _ratePer1000Value; } set { _ratePer1000Value = value; ReCalculate(); } }

        public List<IUnit> Available1000FtRowUnits { get; set; }
        private IUnit _selectedRatePer1000FtRowHelper { get; set; }
        public IUnit SelectedRatePer1000FtRowHelper { get { return _selectedRatePer1000FtRowHelper; } set { _selectedRatePer1000FtRowHelper = value; ReCalculate(); } }

        #endregion

        #region PER CWT ITEMS
        public List<IUnit> AvailableCWTUnits { get; set; }
        private IUnit _selectedCWTUnit { get; set; }
        public IUnit SelectedCWTUnit
        {
            get
            {
                return _selectedCWTUnit;
            }
            set
            {
                _selectedCWTUnit = value;
                ReCalculate();
            }

        }

        private decimal _ratePerCWTValue { get; set; }
        public decimal RatePerCWTValue
        {
            get
            {
                return _ratePerCWTValue;
            }
            set
            {
                _ratePerCWTValue = value;
                ReCalculate();
            }
        }

        #endregion

        #region CALCULATED VALUE ITEMS
        private decimal _ratePerAreaValue { get; set; }
        [System.ComponentModel.DataAnnotations.CustomValidation(typeof(AssociatedSeedTreatmentViewModel), nameof(ValidateRate))]
        public decimal RatePerAreaValue
        {
            get { return _ratePerAreaValue; }
            set
            {
                _ratePerAreaValue = value;
            }
        }

        public List<UnitPerAreaHelper> AllowedRateUnits { get; set; }
        private UnitPerAreaHelper testUnit { get; set; }
        private UnitPerAreaHelper _selectedRatePerAreaUnitHelper
        {
            get { return testUnit; }
            set
            {
                testUnit = value;
            }
        }
        public UnitPerAreaHelper SelectedRatePerAreaUnitHelper
        {
            get
            {
                return _selectedRatePerAreaUnitHelper;
            }
            set
            {

                _selectedRatePerAreaUnitHelper = value;
                ReCalculate();
            }
        }


        private decimal _totalProductValue { get; set; }
        [System.ComponentModel.DataAnnotations.CustomValidation(typeof(AssociatedSeedTreatmentViewModel), nameof(ValidateRate))]
        public decimal TotalProductValue
        {
            get { return _totalProductValue; }
            set
            {
                _totalProductValue = value;
                
            }
        }

        public List<IUnit> AllowedTotalUnits { get; set; }

        private IUnit _selectedTotalProductUnit { get; set; }
        public IUnit SelectedTotalProductUnit
        {
            get
            {
                return _selectedTotalProductUnit;
            }
            set
            {
                _selectedTotalProductUnit = value;
                ReCalculate();
            }
        }

        #endregion

        void PopulateUnits()
        {
            if (SelectedProduct == null) { return; }

            var psu = UnitFactory.GetPackageSafeUnit(SelectedProduct.Product.StdPackageUnit, SelectedProduct.Product.StdUnit, SelectedProduct.Product.StdFactor, SelectedProduct.Product.StdPackageUnit);
            var compatibleUnits = AgC.UnitConversion.CompatibilityFactory.GetCompatibleUnits(psu, SelectedProduct.Product.Density);

            AllowedRateUnits = new List<UnitPerAreaHelper>();
            foreach (var u in compatibleUnits)
            {
                var uh = new UnitPerAreaHelper(u, UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit));
                AllowedRateUnits.Add(uh);
            }

            RaisePropertyChanged(() => AllowedRateUnits);
            AllowedTotalUnits = new List<IUnit>(compatibleUnits);
            _selectedTotalProductUnit = AllowedTotalUnits.SingleOrDefault(x => x.Name == SelectedProduct.Product.StdPackageUnit);


            AvailableRatePerSeedUnits = new List<IUnit>();
            AvailableRatePerSeedUnits.Add(Milligram.Self);
            //AvailableRatePerSeedUnits.Add(Gram.Self);
            SelectedRatePerSeedUnit = AvailableRatePerSeedUnits[0];
            RaisePropertyChanged(() => AvailableRatePerSeedUnits);
            RaisePropertyChanged(() => SelectedRatePerSeedUnit);


            AvailableRatePerBagUnits = new List<IUnit>();
            AvailableRatePerBagUnits.AddRange(AllowedRateUnits.Select(x => x.Unit));
            _selectedRatePerBagUnit = AvailableRatePerBagUnits.SingleOrDefault(x => x.Name == SelectedProduct.Product.StdUnit);
            RaisePropertyChanged(() => AvailableRatePerBagUnits);
            RaisePropertyChanged(() => SelectedRatePerBagUnit);


            AvailableCWTUnits = new List<IUnit>();
            AvailableCWTUnits.AddRange(AgC.UnitConversion.CompatibilityFactory.GetCompatibleUnits(psu, SelectedProduct.Product.Density));
            _selectedCWTUnit = AvailableCWTUnits.SingleOrDefault(x => x.Name == SelectedProduct.Product.StdUnit);
            RaisePropertyChanged(() => AvailableCWTUnits);
            RaisePropertyChanged(() => SelectedCWTUnit);


            Available1000FtRowUnits = new List<IUnit>();
            Available1000FtRowUnits.AddRange(AllowedRateUnits.Select(x => x.Unit));
            _selectedRatePer1000FtRowHelper = AllowedRateUnits.SingleOrDefault(x => x.Unit.Name == SelectedProduct.Product.StdUnit).Unit;
            RaisePropertyChanged(() => Available1000FtRowUnits);
            RaisePropertyChanged(() => SelectedRatePer1000FtRowHelper);

            _selectedRatePerAreaUnitHelper = AllowedRateUnits.FirstOrDefault(x => x.Unit.Name == SelectedProduct.Product.StdUnit);
            if(_selectedRatePerAreaUnitHelper == null) { _selectedRatePerAreaUnitHelper = AllowedRateUnits[0]; }

            
            RaisePropertyChanged(() => SelectedRatePerAreaUnitHelper);
            RaisePropertyChanged(() => AllowedTotalUnits);
            RaisePropertyChanged(() => SelectedTotalProductUnit);
        }
        

        async void BuildProductList()
        {
            var currentCropYearId = new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear);

            productLoadTask = Task.Run(() => {
                var mpl = clientEndpoint.GetMasterlistService().GetProductList();
                var mur = clientEndpoint.GetView<ProductUsageView>(currentCropYearId).GetValue(new ProductUsageView());
                var ucp = clientEndpoint.GetView<UserCreatedProductList>(new DataSourceId(currentCropYearId.DataSourceId)).GetValue(new UserCreatedProductList());

                Guid mlDsId = GlobalIdentifiers.GetMasterlistDataSourceId();
                var productGuids = mur.GetProductGuids();

                var q = from p in mpl
                        let cont = productGuids.Contains(p.Id)
                        select new ProductListItemDetails(p, mlDsId, cont ? Strings.UsedThisYear_Text : Strings.Unused_Text, cont ? (short)0 : (short)1);

                //TO DO :: PARSE OUT THE VARIOUS TYPES OF SEED TREATEMENTS I NEED
                Products = new ObservableCollection<ProductListItemDetails>((q.OrderBy(x => x.GroupSortOrder).ToList()).Where(x => x.Product.ProductType == GlobalStrings.ProductType_CropProtection && x.Product.StdPackageUnit.ToLower() != "bag" && x.Product.StdPackageUnit.ToLower() != "package"));
                RaisePropertyChanged(() => Products);

                //PerSeedProducts = new ObservableCollection<ProductListItemDetails>((q.Union(ucpQ).OrderBy(x => x.GroupSortOrder).ToList()).Where(x => x.Product.ProductType == GlobalStrings.ProductType_CropProtection && x.Product.StdPackageUnit == parent.SelectedProduct.Product.StdPackageUnit && x.Product.StdUnit == parent.SelectedProduct.Product.StdUnit && x.Product.StdFactor == parent.SelectedProduct.Product.StdFactor));
                //RaisePropertyChanged(() => PerSeedProducts);
                //Products = new ObservableCollection<ProductListItemDetails>(q.Union(ucpQ).OrderBy(x => x.GroupSortOrder).ToList());
            });
            await productLoadTask;
        }

        public void TabSelectionChanged()
        {
            //TO DO :: FILTER DOWN THE PRODUCTS LIST

            //TO DO :: CLEAR OUT VALUES
            RatePerAreaValue = 0m;
            TotalProductValue = 0m;

            RaisePropertyChanged(() => RatePerAreaValue);
            RaisePropertyChanged(() => TotalProductValue);

            //SelectedProduct = null;
            //Products = Products;
            //RaisePropertyChanged(() => Products);
        }

        void ReCalculate()
        {
            switch(SelectedTabIndex.ToString())
            {
                case "0":
                    CalculatePerSeed();
                    ValidateAndRaisePropertyChanged(() => RatePerAreaValue);
                    ValidateAndRaisePropertyChanged(() => TotalProductValue);
                    break;
                case "1":
                    CalculatePerBag();
                    ValidateAndRaisePropertyChanged(() => RatePerAreaValue);
                    ValidateAndRaisePropertyChanged(() => TotalProductValue);
                    break;
                case "2":
                    CalculateCWT();
                    ValidateAndRaisePropertyChanged(() => RatePerAreaValue);
                    ValidateAndRaisePropertyChanged(() => TotalProductValue);
                    break;
                case "3":
                    Calculate1000FtRow();
                    ValidateAndRaisePropertyChanged(() => RatePerAreaValue);
                    ValidateAndRaisePropertyChanged(() => TotalProductValue);
                    break;
                default:
                    break;
            }
        }

        void CalculateCWT()
        {
            if (product != null && SelectedProduct != null && SelectedCWTUnit != null && RatePerCWTValue != 0 && SelectedRatePerAreaUnitHelper != null)
            {
                var rateVal =(decimal) _ratePerArea.Value;
                var parentProd = product;
                if (parentProd.StdPackageUnit == "bag" && parentProd.StdFactor == 100)
                {
                    if (_ratePerArea.Unit.Name == parentProd.StdPackageUnit) { }
                    else
                    {
                        var unit = UnitFactory.GetPackageSafeUnit(parentProd.StdPackageUnit, parentProd.StdUnit, parentProd.StdFactor, parentProd.StdPackageUnit);
                        var parentRatePerArea = _ratePerArea.Unit.GetMeasure((double)rateVal, product.Density);
                        rateVal = parentRatePerArea.CanConvertTo(unit) ? (decimal)parentRatePerArea.GetValueAs(unit) : (decimal)parentRatePerArea.Value;
                    }
                }else if(parentProd.StdUnit == "pound")
                {
                    var multiplier = parentProd.StdFactor / 100;
                    var unit = UnitFactory.GetPackageSafeUnit(parentProd.StdPackageUnit, parentProd.StdUnit, parentProd.StdFactor, parentProd.StdPackageUnit);
                    var parentRatePerArea = _ratePerArea.Unit.GetMeasure((double)rateVal, product.Density);
                    rateVal = parentRatePerArea.CanConvertTo(unit) ? (decimal)parentRatePerArea.GetValueAs(unit) : (decimal)parentRatePerArea.Value;
                    rateVal = rateVal * (decimal)multiplier;
                }
                else { return; }

                var stdUnit = SelectedRatePerAreaUnitHelper.Unit;
                var packageUnit = SelectedTotalProductUnit;

                var ratePerAreaValue = rateVal * RatePerCWTValue;
                var ratePerArea = SelectedCWTUnit.GetMeasure((double)ratePerAreaValue, SelectedProduct.Product.Density);
                RatePerAreaValue = ratePerArea.Unit != SelectedRatePerAreaUnitHelper.Unit && ratePerArea.CanConvertTo(SelectedRatePerAreaUnitHelper.Unit) ? (decimal)ratePerArea.GetValueAs(SelectedRatePerAreaUnitHelper.Unit) : (decimal)ratePerArea.Value;

                var totalValue = RatePerAreaValue * (decimal)totalArea.Value;
                var totalMeasure = stdUnit.GetMeasure((double)totalValue, SelectedProduct.Product.Density);
                var convertedTotalProductValue = totalMeasure.Unit != packageUnit && totalMeasure.CanConvertTo(packageUnit) ? totalMeasure.GetValueAs(packageUnit) : totalMeasure.Value;
                TotalProductValue = (decimal)convertedTotalProductValue;

                RaisePropertyChanged(() => RatePerAreaValue);
                RaisePropertyChanged(() => TotalProductValue);
            }
        }

        void Calculate1000FtRow()
        {
            if (product != null && SelectedProduct != null && SelectedProduct.Product.Density != null && SelectedRatePer1000FtRowHelper != null && RowWidth != 0 && SelectedRatePerAreaUnitHelper != null)
            {
                var areaPer1000Row = ((RowWidth / 12) * 1000) / 43560;
                var area = totalArea;
                var acreUnit = UnitFactory.GetUnitByName("acre");
                var acres = !Equals(area.Unit, acreUnit) && area.CanConvertTo(acreUnit) ? area.GetValueAs(acreUnit) : area.Value;

                var numberOf1000Rows = (decimal)acres / areaPer1000Row;
                var totalProductValue = RatePer1000Value * numberOf1000Rows;

                var ratePerAreaValue = totalProductValue / (decimal)totalArea.Value;
                var ratePerArea = SelectedRatePer1000FtRowHelper.GetMeasure((double)ratePerAreaValue, SelectedProduct.Product.Density);

                var stdUnit = SelectedRatePerAreaUnitHelper.Unit;
                var packageUnit = SelectedTotalProductUnit;

                RatePerAreaValue = ratePerArea.Unit != SelectedRatePerAreaUnitHelper.Unit && ratePerArea.CanConvertTo(SelectedRatePerAreaUnitHelper.Unit) ? (decimal)ratePerArea.GetValueAs(SelectedRatePerAreaUnitHelper.Unit) : (decimal)ratePerArea.Value;
                var totalValue = RatePerAreaValue * (decimal)totalArea.Value;
                var totalMeasure = stdUnit.GetMeasure((double)totalValue, SelectedProduct.Product.Density);
                var convertedTotalProductValue = totalMeasure.Unit != packageUnit && totalMeasure.CanConvertTo(packageUnit) ? totalMeasure.GetValueAs(packageUnit) : totalMeasure.Value;
                TotalProductValue = (decimal)convertedTotalProductValue;

                RaisePropertyChanged(() => RatePerAreaValue);
                RaisePropertyChanged(() => TotalProductValue);
            }
        }

        void CalculatePerBag()
        {
            if (product != null && SelectedProduct != null && SelectedProduct.Product.Density != null && SelectedRatePerBagUnit != null && RatePerBagValue != 0 && SelectedRatePerAreaUnitHelper != null)
            {
                var rateVal = (decimal)_ratePerArea.Value;
                var parentProd = product;

                if (_ratePerArea.Unit.Name == parentProd.StdPackageUnit) { }
                else
                {
                    var unit = UnitFactory.GetPackageSafeUnit(parentProd.StdPackageUnit, parentProd.StdUnit, parentProd.StdFactor, parentProd.StdPackageUnit);
                    var parentRatePerArea = _ratePerArea.Unit.GetMeasure((double)rateVal, product.Density);
                    rateVal = parentRatePerArea.CanConvertTo(unit) ? (decimal)parentRatePerArea.GetValueAs(unit) : (decimal)parentRatePerArea.Value;
                }

                var stdUnit = SelectedRatePerAreaUnitHelper.Unit;
                var packageUnit = SelectedTotalProductUnit;

                var ratePerAreaValue = rateVal * RatePerBagValue;
                var ratePerArea = SelectedRatePerBagUnit.GetMeasure((double)ratePerAreaValue, SelectedProduct.Product.Density);
                RatePerAreaValue = ratePerArea.Unit != SelectedRatePerAreaUnitHelper.Unit && ratePerArea.CanConvertTo(SelectedRatePerAreaUnitHelper.Unit) ? (decimal)ratePerArea.GetValueAs(SelectedRatePerAreaUnitHelper.Unit) : (decimal)ratePerArea.Value;

                var totalValue = RatePerAreaValue * (decimal)totalArea.Value;
                var totalMeasure = stdUnit.GetMeasure((double)totalValue, SelectedProduct.Product.Density);
                var convertedTotalProductValue = totalMeasure.Unit != packageUnit && totalMeasure.CanConvertTo(packageUnit) ? totalMeasure.GetValueAs(packageUnit) : totalMeasure.Value;
                TotalProductValue = (decimal)convertedTotalProductValue;

                RaisePropertyChanged(() => RatePerAreaValue);
                RaisePropertyChanged(() => TotalProductValue);
            }
        }

        void CalculatePerSeed()
        {
                if (product != null && SelectedProduct != null && SelectedProduct.Product.Density != null && SelectedRatePerSeedUnit != null && MGActivePerSeedValue != 0 && SelectedRatePerAreaUnitHelper != null)
            {
                if (SelectedProduct.Product.ActiveIngredients.Sum(x => x.Percent) <= 0) { return; }
                //CALC BASED OFF OF ACTIVE
                var totalPercentAI = SelectedProduct.Product.ActiveIngredients.Sum(x => x.Percent);
                var totalProdPerSeed = MGActivePerSeedValue / totalPercentAI;

                //TO DO :: MAKE SURE RATE IS IN SEED OR KERNEL OTHERWISE TRY TO CONVERT
                var rateVal = (decimal)_ratePerArea.Value;
                var parentProd = product;
                var parentStdUnit = parentProd.StdUnit;
                if (parentStdUnit == "seed" || parentStdUnit == "kernel")
                {
                    if(_ratePerArea.Unit.Name == parentStdUnit) { }
                    else
                    {
                        var unit = UnitFactory.GetPackageSafeUnit(parentStdUnit, parentStdUnit, parentProd.StdFactor, parentProd.StdPackageUnit);
                        var parentRatePerArea = _ratePerArea.Unit.GetMeasure((double)rateVal, product.Density);
                        rateVal = parentRatePerArea.CanConvertTo(unit) ? (decimal)parentRatePerArea.GetValueAs(unit) : (decimal)parentRatePerArea.Value;
                    }
                }
                else { return; }


                var stdUnit = UnitFactory.GetPackageSafeUnit(SelectedRatePerAreaUnitHelper.Unit.Name, SelectedProduct.Product.StdUnit, SelectedProduct.Product.StdFactor, SelectedProduct.Product.StdPackageUnit);
                var packageUnit = SelectedTotalProductUnit;
                var totalSeedTreatmentPerAreaValue = totalProdPerSeed * rateVal;
                var totalSeedTreatmentPerArea = SelectedRatePerSeedUnit.GetMeasure((double)totalSeedTreatmentPerAreaValue, SelectedProduct.Product.Density);

                var convertedSeedTreatmentPerArea = totalSeedTreatmentPerArea.Unit != stdUnit && totalSeedTreatmentPerArea.CanConvertTo(stdUnit) ? totalSeedTreatmentPerArea.CreateAs(stdUnit) : totalSeedTreatmentPerArea;
                RatePerAreaValue = (decimal)convertedSeedTreatmentPerArea.Value;

                var totalValue = RatePerAreaValue * (decimal)totalArea.Value;
                var totalMeasure = stdUnit.GetMeasure((double)totalValue, SelectedProduct.Product.Density);
                var convertedTotalProductValue = totalMeasure.Unit != packageUnit && totalMeasure.CanConvertTo(packageUnit) ? totalMeasure.GetValueAs(packageUnit) : totalMeasure.Value;
                TotalProductValue = (decimal)convertedTotalProductValue;

                RaisePropertyChanged(() => RatePerAreaValue);
                RaisePropertyChanged(() => TotalProductValue);
            }
        }

        void TabVisiblity()
        {
            //TO DO :: HIDE PERSEED IF THE PRODUCTS STDUNIT IS NOT SEED OR KERNEL
            var parentProd = product;
            var parentStdUnit = parentProd.StdUnit;
            if (parentStdUnit == "seed" || parentStdUnit == "kernel")
            {
                IsSeedVisible = true;
                IsBagVisible = true;
                IsCwtVisible = false;
                IsRowVisible = true;
            }
            else if (parentProd.StdUnit == "pound")
            {
                IsSeedVisible = false;
                IsBagVisible = true;
                IsCwtVisible = true;
                IsRowVisible = true;
                SelectedTabIndex = 1;
                RaisePropertyChanged(() => SelectedTabIndex);
            }
            else
            {
                IsSeedVisible = true;
                IsBagVisible = true;
                IsCwtVisible = false;
                IsRowVisible = true;
            }

            RaisePropertyChanged(() => IsSeedVisible);
            RaisePropertyChanged(() => IsBagVisible);
            RaisePropertyChanged(() => IsCwtVisible);
            RaisePropertyChanged(() => IsRowVisible);
        }

        void ClosePopUp()
        {
            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
        }

        void UpdateProducts()
        {
            if (RatePerAreaValue != 0)
            {
                _addAssociatedProduct(this);
                Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
            }
        }

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateRate(decimal rate, ValidationContext context)
        {
            System.ComponentModel.DataAnnotations.ValidationResult result = null;

            var vm = (AssociatedSeedTreatmentViewModel)context.ObjectInstance;

            var shouldValidate = true;

            if (shouldValidate && rate == 0)
            {
                result = new System.ComponentModel.DataAnnotations.ValidationResult(Strings.RateCannotBeZero_Text);
            }

            return result;
        }
    }
}
