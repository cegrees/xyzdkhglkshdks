﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Company;
using Landdb.Domain.ReadModels.Person;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Secondary.Application;
using Landdb.ViewModel.Secondary.WorkOrder;
using Landdb.ViewModel.Secondary.Recommendation;

namespace Landdb.ViewModel.Secondary.PopUp
{
    public class IncludeApplicationCompany : ViewModelBase
    {
        IClientEndpoint clientEndpoint;
        Dispatcher dispatcher;

        CompanyListItem company;
        List<CompanyListItem> companyList;
        List<CompanyListItem> addedCompanies;

        InfoViewModel appView;
        WorkOrderEditorViewModel woView;
        RecommendationInfoViewModel recView;

        public IncludeApplicationCompany(IClientEndpoint clientEndpoint, Dispatcher dispatcher, InfoViewModel appView)
        {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            this.appView = appView;

            addedCompanies = new List<CompanyListItem>();
            SelectedCompanies = new ObservableCollection<CompanyListItem>();

            CancelCommand = new RelayCommand(Cancel);
            UpdateCommand = new RelayCommand(Update);
        }

        public IncludeApplicationCompany(IClientEndpoint clientEndpoint, Dispatcher dispatcher, WorkOrderEditorViewModel appView)
        {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            this.woView = appView;

            addedCompanies = new List<CompanyListItem>();
            SelectedCompanies = new ObservableCollection<CompanyListItem>();

            CancelCommand = new RelayCommand(Cancel);
            UpdateCommand = new RelayCommand(Update);
        }

        public IncludeApplicationCompany(IClientEndpoint clientEndpoint, Dispatcher dispatcher, RecommendationInfoViewModel appView)
        {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            this.recView = appView;

            addedCompanies = new List<CompanyListItem>();
            SelectedCompanies = new ObservableCollection<CompanyListItem>();

            CancelCommand = new RelayCommand(Cancel);
            UpdateCommand = new RelayCommand(Update);
        }

        public ICommand UpdateCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }

        public List<CompanyListItem> Companies
        {
            get
            {
                DataSourceId dsId = new DataSourceId(ApplicationEnvironment.CurrentDataSourceId);
				var cropYear = ApplicationEnvironment.CurrentCropYear;

				var companyListView = clientEndpoint.GetView<CompanyListView>(dsId).GetValue(() => null);
				if (companyListView != null) {
					var sortedFilteredCompanies = from p in companyListView.Companies
												 where p.IsActiveInCropYear(cropYear)
												 orderby p.Name
												 select p;

					companyList = sortedFilteredCompanies.ToList();
				}

                return companyList;
            }
            set
            {
                companyList = value;
            }
        }

        public CompanyListItem Company
        {
            get
            {
                return company;
            }
            set
            {
                company = value;

                if (SelectedCompanies == null || !SelectedCompanies.Contains(Company))
                {
                    SelectedCompanies.Add(Company);
                    addedCompanies.Add(Company);
                }

                RaisePropertyChanged("Company");
            }
        }

        public ObservableCollection<CompanyListItem> SelectedCompanies { get; set; }

        void Cancel()
        {
            foreach (CompanyListItem co in addedCompanies)
            {
                SelectedCompanies.Remove(co);
            }

            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
        }

        void Update()
        {

            //TO DO :: SEND LIST OF SELECTED APPLICATORS TO THE APPLICATORVIEWMODEL
            if (appView != null)
            {
                appView.SelectedCompanies = SelectedCompanies;
            }
            else if (woView != null)
            {
                woView.SelectedCompanies = SelectedCompanies;
            }
            else if (recView != null)
            {
                recView.SelectedCompanies = SelectedCompanies;
            }

            addedCompanies.Clear();


            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
        }
    }
}
