﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Company;
using Landdb.Domain.ReadModels.Person;
using Landdb.Domain.ReadModels.Application;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Secondary.Application;
using Landdb.ViewModel.Secondary.WorkOrder;
using AgC.UnitConversion;
using Landdb.Resources;

namespace Landdb.ViewModel.Secondary.PopUp {
    public class IncludeApplicator : ViewModelBase {
        IClientEndpoint clientEndpoint;
        Dispatcher dispatcher;

        List<CompanyListItem> companyList;

        ObservableCollection<PotentialApplicatorViewModel> potentialApplicators = new ObservableCollection<PotentialApplicatorViewModel>();
        PotentialApplicatorViewModel selectedPotentialApplicator = null;

        public IncludeApplicator(IClientEndpoint clientEndpoint, Dispatcher dispatcher, ObservableCollection<ApplicationApplicator> currentApplicatorSelections) {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;

            UpdateCommand = new RelayCommand(Update);
            AddCommand = new RelayCommand(Add);
            RemoveCommand = new RelayCommand<ApplicationApplicator>(Remove);
            IsEdit = true;
            PopulatePotentialApplicators();

            Applicators = currentApplicatorSelections;
        }

        public ICommand UpdateCommand { get; private set; }
        public ICommand AddCommand { get; private set; }
        public ICommand RemoveCommand { get; private set; }

        private bool _isEdit { get; set; }
        public bool IsEdit { get { return _isEdit; } set { _isEdit = value; RaisePropertyChanged(() => IsEdit); } }
        public decimal? BufferWidth { get; set; }
        public IUnit BufferWidthUnit
        {
            get
            {
                var areaUnit = ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit.ToLower();
                if(string.Equals(areaUnit, "acre"))
                {
                    return UnitFactory.GetUnitByName("foot");
                }
                else
                {
                    return UnitFactory.GetUnitByName("meter");
                }
            }
        }
        public string SelectedBufferTolerance { get; set; }
        public List<string> BufferZoneList
        {
            get
            {
                List<string> zones = new List<string>();
                zones.Add(Strings.BufferZone_SprayDrift_Text);
                zones.Add(Strings.BufferZone_PollenDrift_Text);
                zones.Add(Strings.BufferZone_InterimAquatic_Text);
                zones.Add(Strings.BufferZone_BeeProtection_Text);
                return zones;
            }
        }

        public ObservableCollection<PotentialApplicatorViewModel> PotentialApplicators {
            get { return potentialApplicators; }
            private set {
                potentialApplicators = value;
                RaisePropertyChanged("PotentialApplicators");
            }
        }

        public PotentialApplicatorViewModel SelectedPotentialApplicator {
            get { return selectedPotentialApplicator; }
            set {
                selectedPotentialApplicator = value;
                RaisePropertyChanged("SelectedPotentialApplicator");
            }
        }

        void PopulatePotentialApplicators() {
            PotentialApplicators.Clear();
            DataSourceId dsId = new DataSourceId(ApplicationEnvironment.CurrentDataSourceId);
			var currentCropYear = ApplicationEnvironment.CurrentCropYear;

            // Get companies
			var companyListView = clientEndpoint.GetView<CompanyListView>(dsId).GetValue(new CompanyListView());
			var sortedFilteredCompanies = from c in companyListView.Companies
										  where c.IsActiveInCropYear(currentCropYear)
										  orderby c.Name
										  select c;

			companyList = sortedFilteredCompanies.ToList();

            // Populate people
			var personListView = clientEndpoint.GetView<PersonListView>(dsId).GetValue(new PersonListView());
			var sortedFilteredPersons = from p in personListView.Persons
										where p.IsActiveInCropYear(currentCropYear)
										orderby p.Name
										select p;

			sortedFilteredPersons.ForEach(x => PotentialApplicators.Add(new PotentialApplicatorViewModel(clientEndpoint, x, companyList, OnItemChanged)));

            // Populate companies
            //foreach (var c in companyList) {
            //    PotentialApplicators.Add(new PotentialApplicatorViewModel(clientEndpoint, c));
            //}
        }

        void OnItemChanged(PotentialApplicatorViewModel changedItem) {

        }


        public ObservableCollection<ApplicationApplicator> Applicators { get; set; }

        void Add() {
            if (SelectedPotentialApplicator != null && !Applicators.Where(x => x.PersonId == SelectedPotentialApplicator.PersonId).Any()) {
                string compName = null;
                CompanyId companyId = null;
                if (SelectedPotentialApplicator.Company != null) {
                    compName = SelectedPotentialApplicator.Company.Name;
                    companyId = SelectedPotentialApplicator.Company.Id;
                }
                Applicators.Add(new ApplicationApplicator(SelectedPotentialApplicator.PersonId, SelectedPotentialApplicator.Name, companyId, compName, SelectedPotentialApplicator.LicenseNumber, SelectedPotentialApplicator.Expires));
            } else {
                //Applicators.Where(x => x.PersonId == SelectedPerson.Id).SingleOrDefault().LicenseNumber = LicenseNumber;
                //Applicators.Where(x => x.PersonId == SelectedPerson.Id).SingleOrDefault().Expires = Expires;
                //Applicators.Where(x => x.PersonId == SelectedPerson.Id).SingleOrDefault().CompanyName = Company.Name;
            }
        }

        void Remove(ApplicationApplicator itemToRemove) {
            if (itemToRemove == null) { return; }

            ApplicationApplicator appApplicator = Applicators.Where(x => x.PersonId == itemToRemove.PersonId).SingleOrDefault();
            Applicators.Remove(appApplicator);
        }

        void Update() {
            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
        }
    }

    public class PotentialApplicatorViewModel : ViewModelBase {
        IClientEndpoint clientEndpoint;
        bool isPerson;
        bool isDetailLoaded = false;
        string licenseNumber;
        DateTime expires = DateTime.Today;
        CompanyListItem companyListItem;
        PersonListItem personListItem;
        List<CompanyListItem> companyList;
        Action<PotentialApplicatorViewModel> onChanged;

        public PotentialApplicatorViewModel(IClientEndpoint clientEndpoint, PersonListItem person, List<CompanyListItem> companyList, Action<PotentialApplicatorViewModel> onChanged) {
            this.clientEndpoint = clientEndpoint;
            this.personListItem = person;
            this.companyList = companyList;
            this.onChanged = onChanged;

            IsPerson = true;
        }

        public PotentialApplicatorViewModel(IClientEndpoint clientEndpoint, CompanyListItem company, Action<PotentialApplicatorViewModel> onChanged) {
            this.clientEndpoint = clientEndpoint;
            this.companyListItem = company;
            this.companyList = new List<CompanyListItem>();
            this.onChanged = onChanged;

            IsPerson = false;
        }

        public List<CompanyListItem> Companies {
            get { return companyList; }
        }

        public PersonId PersonId {
            get {
                if (IsPerson) {
                    return personListItem.Id;
                } else {
                    return null;
                }
            }
        }

        public string Name {
            get {
                EnsureDetailsAreLoaded();
                if (IsPerson) {
                    return personListItem.Name;
                } else {
                    return companyListItem.Name;
                }
            }
        }

        public CompanyListItem Company {
            get {
                EnsureDetailsAreLoaded();
                return companyListItem; 
            }
            set {
                companyListItem = value;
                RaiseOnChanged();
                RaisePropertyChanged("Company");
            }
        }

        public bool IsPerson {
            get { return isPerson; }
            set {
                isPerson = value;
                RaisePropertyChanged("IsPerson");
            }
        }

        public DateTime Expires {
            get {
                EnsureDetailsAreLoaded();
                return expires; 
            }
            set {
                if (value == DateTime.MinValue) {
                    expires = DateTime.Today + TimeSpan.FromDays(1); // expires tomorrow
                } else {
                    expires = value;
                }
                RaiseOnChanged();
                RaisePropertyChanged("Expires");
            }
        }

        public string LicenseNumber {
            get {
                EnsureDetailsAreLoaded();
                return licenseNumber; 
            }
            set {
                licenseNumber = value;
                RaiseOnChanged();
                RaisePropertyChanged("LicenseNumber");
            }
        }

        void RaiseOnChanged() {
            if (onChanged != null) {
                onChanged(this);
            }
        }

        void EnsureDetailsAreLoaded() {
            if (!isDetailLoaded) {
                if (IsPerson) {
                    var personDetails = clientEndpoint.GetView<PersonDetailsView>(personListItem.Id);
                    if (personDetails.HasValue) {
                        Expires = personDetails.Value.ApplicatorLicenseExpiration;
                        LicenseNumber = personDetails.Value.ApplicatorLicenseNumber;
                        companyListItem = companyList.FirstOrDefault(x => x.Id == personDetails.Value.CompanyId);
                        if (companyListItem == null) { // try to find on name if ID can't be matched
                            companyListItem = companyList.FirstOrDefault(x => x.Name == personDetails.Value.CompanyName);
                        }
                    }
                }

                isDetailLoaded = true;
            }
        }

    }
}