﻿using System;
using Landdb.Resources;

namespace Landdb.ViewModel.Secondary {
	public class DurationViewModel {
		public DurationViewModel(TimeSpan? duration) {
			Duration = duration;
            _isCustom = false;
		}

		public TimeSpan? Duration { get; }
        private bool _isCustom { get; set; }
		public bool IsCustom
        {
            get
            {
                return _isCustom || Duration == null ? true : false;
            }
            set
            {
                _isCustom = value;
            }
        }

		public override string ToString() {
			if (Duration.HasValue) {
				return $"{Duration.Value.TotalHours:G} {Strings.Hours_Text}";
			} else {
				return Strings.Custom_Text;
			}
		}
	}
}
