﻿using GalaSoft.MvvmLight;
using Landdb.Client.Infrastructure;
using Landdb.ViewModel.Resources.Contract;
using Landdb.ViewModel.Resources.Contract.Rent;
using System.Collections.Generic;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Secondary.Contract {
	public class ContractShareViewModel : ViewModelBase {

		private readonly IClientEndpoint clientEndpoint;
		private readonly Dispatcher dispatcher;

		string INV_GROWER = Strings.FromGrowerInventory_Text;
		string INV_BILLED = Strings.DirectBilledToShareOwner_Text;

		public ContractShareViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			CropProtectionShare = 1;
			FertilizerShare = 1;
			SeedShare = 1;
			ServiceShare = 1;

			ShareOptions = new List<string>() {
				INV_GROWER,
				INV_BILLED,
			};

			CropProtectionFromInventory = INV_GROWER;
			FertilizerFromInventory = INV_GROWER;
			SeedFromInventory = INV_GROWER;
			ServiceFromInventory = INV_GROWER;
		}

        public ContractShareViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, ContractDetailsBaseViewModel copydetails) {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;

            CropProtectionShare = 1;
            FertilizerShare = 1;
            SeedShare = 1;
            ServiceShare = 1;

            ShareOptions = new List<string>() {
                INV_GROWER,
                INV_BILLED,
            };

            CropProtectionFromInventory = INV_GROWER;
            FertilizerFromInventory = INV_GROWER;
            SeedFromInventory = INV_GROWER;
            ServiceFromInventory = INV_GROWER;

            if (copydetails != null) {
                RentContractDetailsViewModel rentcontract = (RentContractDetailsViewModel)copydetails;
                IncludedShareInfoViewModel shareAndInfoViewModel = rentcontract.ShareInfo;
                IncludedInventoryInfoViewModel inventoryInfo = rentcontract.InventoryInfo;

                CropProtectionShare = shareAndInfoViewModel.CropProtectionGrowerShare;
                CropProtectionMargin = inventoryInfo.CropProtectionMargin;
                CropProtectionFromInventory = inventoryInfo.SelectedCropProtectionInventory;

                FertilizerShare = shareAndInfoViewModel.FertilizerGrowerShare;
                FertilizerMargin = inventoryInfo.FertilizerMargin;
                FertilizerFromInventory = inventoryInfo.SelectedFertilizerInventory;

                SeedShare = shareAndInfoViewModel.SeedGrowerShare;
                SeedMargin = inventoryInfo.SeedMargin;
                SeedFromInventory = inventoryInfo.SelectedSeedInventory;

                ServiceShare = shareAndInfoViewModel.ServiceGrowerShare;
                ServiceMargin = inventoryInfo.ServiceMargin;
                ServiceFromInventory = inventoryInfo.SelectedServiceInventory;
            }
        }

        public List<string> ShareOptions { get; }

		public bool ServiceInventory { get; private set; }
		public bool SeedInventory { get; private set; }
		public bool FertInventory { get; private set; }
		public bool CpInventory { get; private set; }

		private double _cropProtectionShare;
		public double CropProtectionShare {
			get { return _cropProtectionShare; }
			set {
				_cropProtectionShare = getValueInRange(value);
				_ownerCropProtectionShare = 1.0 - _cropProtectionShare;

				RaisePropertyChanged(() => CropProtectionShare);
				RaisePropertyChanged(() => OwnerCropProtectionShare);
			}
		}

		private double _cropProtectionMargin;
		public double CropProtectionMargin {
			get { return _cropProtectionMargin; }
			set {
				_cropProtectionMargin = value;
				RaisePropertyChanged(() => CropProtectionMargin);
			}
		}

		private string _cpOption;
		public string CropProtectionFromInventory {
			get { return _cpOption; }
			set {
				_cpOption = value;
				CpInventory = value.ToString() == INV_GROWER;

				RaisePropertyChanged(() => CropProtectionFromInventory);
			}
		}

		private double _fertilizerShare;
		public double FertilizerShare {
			get { return _fertilizerShare; }
			set {
				_fertilizerShare = getValueInRange(value);
				_ownerFertilizerShare = 1.0 - _fertilizerShare;

				RaisePropertyChanged(() => FertilizerShare);
				RaisePropertyChanged(() => OwnerFertilizerShare);
			}
		}

		private double _fertilizerMargin;
		public double FertilizerMargin {
			get { return _fertilizerMargin; }
			set {
				_fertilizerMargin = value;
				RaisePropertyChanged(() => FertilizerMargin);
			}
		}

		private string _fertOption;
		public string FertilizerFromInventory {
			get { return _fertOption; }
			set {
				_fertOption = value;
				FertInventory = value.ToString() == INV_GROWER;

				RaisePropertyChanged(() => FertilizerFromInventory);
			}
		}

		private double _seedShare;
		public double SeedShare {
			get { return _seedShare; }
			set {
				_seedShare = getValueInRange(value);
				_ownerSeedShare = 1.0 - _seedShare;

				RaisePropertyChanged(() => SeedShare);
				RaisePropertyChanged(() => OwnerSeedShare);
			}
		}

		private double _seedMargin;
		public double SeedMargin {
			get { return _seedMargin; }
			set {
				_seedMargin = value;
				RaisePropertyChanged(() => SeedMargin);
			}
		}

		private string _seedOption;
		public string SeedFromInventory {
			get { return _seedOption; }
			set {
				_seedOption = value;
				SeedInventory = value.ToString() == INV_GROWER;

				RaisePropertyChanged(() => SeedFromInventory);
			}
		}

		private double _serviceShare;
		public double ServiceShare {
			get { return _serviceShare; }
			set {
				_serviceShare = getValueInRange(value);
				_ownerServiceShare = 1.0 - _serviceShare;

				RaisePropertyChanged(() => ServiceShare);
				RaisePropertyChanged(() => OwnerServiceShare);
			}
		}

		private double _serviceMargin;
		public double ServiceMargin {
			get { return _serviceMargin; }
			set {
				_serviceMargin = value;
				RaisePropertyChanged(() => ServiceMargin);
			}
		}

		private string _serviceOption;
		public string ServiceFromInventory {
			get { return _serviceOption; }
			set {
				_serviceOption = value;
				ServiceInventory = value.ToString() == INV_GROWER;

				RaisePropertyChanged(() => ServiceFromInventory);
			}
		}

		private double _ownerCropProtectionShare;
		public double OwnerCropProtectionShare {
			get { return _ownerCropProtectionShare; }
			set {
				_ownerCropProtectionShare = getValueInRange(value);
				_cropProtectionShare = 1.0 - _ownerCropProtectionShare;

				RaisePropertyChanged(() => OwnerCropProtectionShare);
				RaisePropertyChanged(() => CropProtectionShare);
			}
		}

		private double _ownerFertilizerShare;
		public double OwnerFertilizerShare {
			get { return _ownerFertilizerShare; }
			set {
				_ownerFertilizerShare = getValueInRange(value);
				_fertilizerShare = 1.0 - _ownerFertilizerShare;

				RaisePropertyChanged(() => OwnerFertilizerShare);
				RaisePropertyChanged(() => FertilizerShare);
			}
		}

		private double _ownerSeedShare;
		public double OwnerSeedShare {
			get { return _ownerSeedShare; }
			set {
				_ownerSeedShare = getValueInRange(value);
				_seedShare = 1.0 - _ownerSeedShare;

				RaisePropertyChanged(() => OwnerSeedShare);
				RaisePropertyChanged(() => SeedShare);
			}
		}

		private double _ownerServiceShare;
		public double OwnerServiceShare {
			get { return _ownerServiceShare; }
			set {
				_ownerServiceShare = getValueInRange(value);
				_serviceShare = 1.0 - _ownerServiceShare;

				RaisePropertyChanged(() => OwnerServiceShare);
				RaisePropertyChanged(() => ServiceShare);
			}
		}

		public void ResetData() {
			CropProtectionShare = 1;
			FertilizerShare = 1;
			SeedShare = 1;
			ServiceShare = 1;

			CropProtectionMargin = 0;
			FertilizerMargin = 0;
			SeedMargin = 0;
			ServiceMargin = 0;

			CropProtectionFromInventory = INV_GROWER;
			FertilizerFromInventory = INV_GROWER;
			SeedFromInventory = INV_GROWER;
			ServiceFromInventory = INV_GROWER;
		}

		private double getValueInRange(double value) {
			if (value > 1) {
				return 1;
			} else if (value < 0) {
				return 0;
			} else {
				return value;
			}
		}
	}
}