﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Secondary.Contract;
using System;
using System.Linq;
using System.Windows.Input;
using System.Windows.Threading;

namespace Landdb.ViewModel.Secondary.ProductionContract {
	public class AddProductionContractViewModel : ViewModelBase {

		private readonly IClientEndpoint clientEndpoint;
		private readonly Dispatcher dispatcher;

		private readonly CropYearId currentCropYearId;

		public AddProductionContractViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, ContractId contractId, int currentCropYear) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			currentCropYearId = new CropYearId(contractId.DataSourceId, currentCropYear);

			ProductionContractId = contractId;

			FieldsPageModel = new ContractFieldSelectionPageViewModel(clientEndpoint, dispatcher, currentCropYearId, onCropZoneCropChanged, this);
			InfoPageModel = new ProductionContractInfoViewModel(clientEndpoint, dispatcher, currentCropYearId);
			SharePageModel = new ContractShareViewModel(clientEndpoint, dispatcher);

			ReturnToMainViewCommand = new RelayCommand(onReturnToMainView);
			CreateAnotherContractCommand = new RelayCommand(onCreateAnotherContract);

			CreateNewCommand = new RelayCommand(onComplete);
			CancelCommand = new RelayCommand(onCancel);
		}

		public ICommand CreateNewCommand { get; }
		public ICommand CancelCommand { get; }

		public ICommand ReturnToMainViewCommand { get; }
		public ICommand CreateAnotherContractCommand { get; }

		public ContractFieldSelectionPageViewModel FieldsPageModel { get; }
		public ProductionContractInfoViewModel InfoPageModel { get; }
		public ContractShareViewModel SharePageModel { get; }

		public ContractId ProductionContractId { get; private set; }

		// for display
		public int CropYear => currentCropYearId.Id;

		private int _selectedPageIndex;
		public int SelectedPageIndex {
			get { return _selectedPageIndex; }
			set {
				_selectedPageIndex = value;
				RaisePropertyChanged(() => SelectedPageIndex);
			}
		}

		private void ResetData() {
			ProductionContractId = new ContractId(currentCropYearId.DataSourceId, Guid.NewGuid());
			SelectedPageIndex = 0;

			FieldsPageModel.ResetData();
			InfoPageModel.ResetData();
			SharePageModel.ResetData();
		}

		private void onComplete() {
			if (InfoPageModel.SelectedCrop == null) { return; } // this will bail if multiple crops are selected on the fields page
			if (string.IsNullOrWhiteSpace(InfoPageModel.Name)) { return; }

			CreateProductionContract cmd = new CreateProductionContract(
				ProductionContractId,
				clientEndpoint.GenerateNewMetadata(),
				InfoPageModel.Name,
				InfoPageModel.StartDate.ToUniversalTime(),
				InfoPageModel.DeliveryDate,
				InfoPageModel.ContractPrice,
				InfoPageModel.FuturesPrice,
				InfoPageModel.ContractedAmount,
				InfoPageModel.SelectedContractAmountUnit,
				InfoPageModel.SelectedCrop.Id,
				InfoPageModel.ContractNumber,
				InfoPageModel.GrowerID,
				SharePageModel.CropProtectionShare,
				SharePageModel.CropProtectionMargin,
				SharePageModel.CpInventory,
				SharePageModel.FertilizerShare,
				SharePageModel.FertilizerMargin,
				SharePageModel.FertInventory,
				SharePageModel.SeedShare,
				SharePageModel.SeedMargin,
				SharePageModel.SeedInventory,
				SharePageModel.ServiceShare,
				SharePageModel.ServiceMargin,
				SharePageModel.ServiceInventory,
				FieldsPageModel.GetCropZoneAreas(),
				InfoPageModel.GetBuyerValueObject(),
				currentCropYearId.Id,
				InfoPageModel.Notes
			);

			clientEndpoint.SendOne(cmd);

			showPostContractOverlay();
		}

		private void onCancel() {
			Messenger.Default.Send(new ShowMainViewMessage());
		}

		private void onCropZoneCropChanged(CropId newCropId) {
			if (newCropId != null) { // crop is selected on field selection page
				InfoPageModel.SelectedCrop = InfoPageModel.Crops.Where(x => x.Id == newCropId.Id).FirstOrDefault();
				InfoPageModel.IsCropSetFromFieldSelectionPage = true;
			} else if (FieldsPageModel.FieldCount == 0) { // no crop is set on field selection page
				InfoPageModel.SelectedCrop = null;
				InfoPageModel.IsCropSetFromFieldSelectionPage = false;
			} else { // field page selections are invalid
				InfoPageModel.SelectedCrop = null;
			}
		}

		#region Post Contract Overlay
		private void showPostContractOverlay() {
			Messenger.Default.Send(new ShowOverlayMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Secondary.Contract.PostContractOverlay), "postContract", this)
			});
		}

		private void onReturnToMainView() {
			Messenger.Default.Send(new ContractAddedMessage() { LastContractAdded = ProductionContractId });
			Messenger.Default.Send(new ShowMainViewMessage());
		}

		private void onCreateAnotherContract() {
			ResetData();
			Messenger.Default.Send(new HideOverlayMessage());
		}
		#endregion
	}
}