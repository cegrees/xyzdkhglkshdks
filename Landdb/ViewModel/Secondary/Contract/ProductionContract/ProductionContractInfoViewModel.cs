﻿using GalaSoft.MvvmLight;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Company;
using Landdb.Domain.ReadModels.Person;
using Landdb.ViewModel.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Data;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Secondary.ProductionContract {
	public class ProductionContractInfoViewModel : ViewModelBase {

		private readonly IClientEndpoint clientEndpoint;
		private readonly Dispatcher dispatcher;

		private readonly CropYearId currentCropYearId;

		public ProductionContractInfoViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, CropYearId currentCropYearId) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			this.currentCropYearId = currentCropYearId;

			StartDate = DateTime.Now;
			DeliveryDate = DateTime.Now;

			BuyerList = getBuyerList();
		}

		public ICollectionView BuyerList { get; }

		public List<CompositeUnit> AllowedUnitList { get; private set; }

		public IEnumerable<MiniCrop> Crops => clientEndpoint.GetMasterlistService().GetCropList().OrderBy(x => x.Name);
		public bool IsCropSelectionComboBoxEnabled => !IsCropSetFromFieldSelectionPage;
		public double Basis => ContractPrice - FuturesPrice;

		public IEnumerable<CompositeUnit> ContractedAmountUnitList {
			get {
				if (SelectedCrop != null) {
					return CompositeUnitHelper.GetWeightUnitsForCrop(clientEndpoint, dispatcher, currentCropYearId, new CropId(SelectedCrop.Id)).OrderBy(x => x.FullDisplay);
				} else {
					return new List<CompositeUnit>();
				}
			}
		}

		private ContractorListItemViewModel _buyer;
		public ContractorListItemViewModel SelectedBuyer {
			get { return _buyer; }
			set {
				_buyer = value;
				GenerateNameFromSelections();
				RaisePropertyChanged(() => SelectedBuyer);
			}
		}

		private MiniCrop _selectedCrop;
		public MiniCrop SelectedCrop {
			get { return _selectedCrop; }
			set {
				_selectedCrop = value;
				GenerateNameFromSelections();

				if (_selectedCrop != null) {
					var cid = new CropId(_selectedCrop.Id);
					AllowedUnitList = CompositeUnitHelper.GetWeightUnitsForCrop(clientEndpoint, dispatcher, currentCropYearId, cid).ToList();
					RaisePropertyChanged(() => AllowedUnitList);
				}

				RaisePropertyChanged(() => SelectedCrop);
				RaisePropertyChanged(() => ContractedAmountUnitList);
			}
		}

		private bool _isCropSetFromFieldSelectionPage;
		public bool IsCropSetFromFieldSelectionPage {
			get { return _isCropSetFromFieldSelectionPage; }
			set {
				_isCropSetFromFieldSelectionPage = value;
				RaisePropertyChanged(() => IsCropSelectionComboBoxEnabled);
			}
		}

		private string _contractNumber;
		public string ContractNumber {
			get { return _contractNumber; }
			set {
				_contractNumber = value;
				GenerateNameFromSelections();
				RaisePropertyChanged(() => ContractNumber);
			}
		}

		private string _growerId;
		public string GrowerID {
			get { return _growerId; }
			set {
				_growerId = value;
				RaisePropertyChanged(() => GrowerID);
			}
		}

		private string _name;
		public string Name {
			get { return _name; }
			set {
				_name = value;
				RaisePropertyChanged(() => Name);
			}
		}

		private DateTime _startDate;
		public DateTime StartDate {
			get { return _startDate; }
			set {
				_startDate = value;
				RaisePropertyChanged(() => StartDate);
			}
		}

		private DateTime _deliveryDate;
		public DateTime DeliveryDate {
			get { return _deliveryDate; }
			set {
				_deliveryDate = value;
				RaisePropertyChanged(() => DeliveryDate);
			}
		}

		private double _contractPrice;
		public double ContractPrice {
			get { return _contractPrice; }
			set {
				_contractPrice = value;
				RaisePropertyChanged(() => ContractPrice);
				RaisePropertyChanged(() => Basis);
			}
		}

		private double _futuresPrice;
		public double FuturesPrice {
			get { return _futuresPrice; }
			set {
				_futuresPrice = value;
				RaisePropertyChanged(() => FuturesPrice);
				RaisePropertyChanged(() => Basis);
			}
		}

		private double _contractedAmount;
		public double ContractedAmount {
			get { return _contractedAmount; }
			set {
				_contractedAmount = value;
				RaisePropertyChanged(() => ContractedAmount);
			}
		}

		private CompositeUnit _contractedUnit;
		public CompositeUnit SelectedContractAmountUnit {
			get { return _contractedUnit; }
			set {
				_contractedUnit = value;
				RaisePropertyChanged(() => SelectedContractAmountUnit);
			}
		}

		private string _notes;
		public string Notes {
			get { return _notes; }
			set {
				_notes = value;
				RaisePropertyChanged(() => Notes);
			}
		}


		public Buyer GetBuyerValueObject() {
			if (SelectedBuyer == null) { return null; }

			if (SelectedBuyer.Group == Strings.People_Text) {
				return new Buyer(SelectedBuyer.PersonId, SelectedBuyer.Name, SelectedBuyer.PhoneNumber);
			} else if (SelectedBuyer.Group == Strings.Companies_Text) {
				return new Buyer(SelectedBuyer.CompanyId, SelectedBuyer.Name, SelectedBuyer.PhoneNumber);
			} else {
				return null;
			}
		}

		public void ResetData() {
			ContractNumber = string.Empty;
			GrowerID = string.Empty;

			StartDate = DateTime.Now;
			DeliveryDate = DateTime.Now;

			SelectedBuyer = null;
			SelectedContractAmountUnit = null;
			SelectedCrop = null;

			IsCropSetFromFieldSelectionPage = false;

			ContractPrice = 0;
			FuturesPrice = 0;
			ContractedAmount = 0;
		}

		public void GenerateNameFromSelections() {
			if (SelectedCrop != null) {
				var cropName = SelectedCrop.Name.Split(':').First().Trim();
				Name = $"{cropName} {Strings.Contract_Text} {ContractNumber}";
			} else {
				Name = ContractNumber;
			}

			if (SelectedBuyer != null) {
				Name = $"{SelectedBuyer.Name} {Name}";
			}
		}

		private ICollectionView getBuyerList() {
			var buyerList = ContractorListItemViewModel.GetListItemsForCropYear(clientEndpoint, currentCropYearId, false);

			var retList = CollectionViewSource.GetDefaultView(buyerList.OrderBy(x => x.Group).ThenBy(x => x.Name));
			retList.GroupDescriptions.Add(new PropertyGroupDescription("Group"));

			return retList;
		}
	}
}