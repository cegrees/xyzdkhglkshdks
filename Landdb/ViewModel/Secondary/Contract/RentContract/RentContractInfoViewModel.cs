﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Company;
using Landdb.Domain.ReadModels.Person;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Resources;
using Landdb.ViewModel.Resources.Contract;
using Landdb.ViewModel.Resources.Contract.Rent;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Secondary.RentContract {
	public class RentContractInfoViewModel : ViewModelBase {

		private readonly IClientEndpoint clientEndpoint;
		private readonly Dispatcher dispatcher;
		private readonly CropYearId currentCropYearId;
		private readonly AddRentContractViewModel parentVm;

		public RentContractInfoViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, CropYearId currentCropYearId, AddRentContractViewModel parentVm) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;
			this.currentCropYearId = currentCropYearId;
			this.parentVm = parentVm;

			StartDate = DateTime.Now;
			RentDue = DateTime.Now;

			GrowerCropShare = 1;

			LandOwnerList = getLandOwnerList();

			FlexLevels = new ObservableCollection<FlexLevelListItemViewModel>();

			FlexPercentageSources = new Dictionary<RentContractFlexPercentageSources, string>() {
				{ RentContractFlexPercentageSources.None, string.Empty },
				{ RentContractFlexPercentageSources.TotalRevenue, Strings.OfTotalRevenue_Text },
				{ RentContractFlexPercentageSources.ProductionRevenue, Strings.OfProductionRevenue_Text },
				{ RentContractFlexPercentageSources.TotalQuantityAtFlatPrice, Strings.OfTotalQuantityAtFlatCropPrice_Text },
			};

			SelectedFlexPercentageSource = FlexPercentageSources.First().Key;

			AddFlexCommand = new RelayCommand(onAddFlexItem);
			RemoveFlexCommand = new RelayCommand(onRemoveFlexItem);
		}

        public RentContractInfoViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, CropYearId currentCropYearId, AddRentContractViewModel parentVm, ContractDetailsBaseViewModel copydetails) {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            this.currentCropYearId = currentCropYearId;
            this.parentVm = parentVm;

            StartDate = DateTime.Now;
            RentDue = DateTime.Now;

            GrowerCropShare = 1;

            LandOwnerList = getLandOwnerList();

            FlexLevels = new ObservableCollection<FlexLevelListItemViewModel>();

            FlexPercentageSources = new Dictionary<RentContractFlexPercentageSources, string>() {
                { RentContractFlexPercentageSources.None, string.Empty },
                { RentContractFlexPercentageSources.TotalRevenue, Strings.OfTotalRevenue_Text },
                { RentContractFlexPercentageSources.ProductionRevenue, Strings.OfProductionRevenue_Text },
                { RentContractFlexPercentageSources.TotalQuantityAtFlatPrice, Strings.OfTotalQuantityAtFlatCropPrice_Text },
            };

            SelectedFlexPercentageSource = FlexPercentageSources.First().Key;

            if (copydetails != null) {
                RentContractDetailsViewModel rentcontract = (RentContractDetailsViewModel)copydetails;
                Name = rentcontract.Name;
                StartDate = rentcontract.StartDate;
                Duration = rentcontract.Duration;
                RentDue = rentcontract.RentDue;
                PerAcre = rentcontract.PerAcre;
                FlatCropPrice = rentcontract.FlatCropPrice;
                GrowerCropShare = rentcontract.ShareInfo.CropGrowerShare;
                foreach (ContractorListItemViewModel sellandowner in LandOwnerList) {
                    LandOwner peoplelandowner = new LandOwner(sellandowner.PersonId, sellandowner.Name, sellandowner.PhoneNumber);
                    LandOwner companylandowner = new LandOwner(sellandowner.CompanyId, sellandowner.Name, sellandowner.PhoneNumber);
                    if (rentcontract.LandOwner == peoplelandowner) {
                        SelectedLandOwner = sellandowner;
                        break;
                    }
                    else if (rentcontract.LandOwner == companylandowner) {
                        SelectedLandOwner = sellandowner;
                        break;
                    }
                }

                FlexLevels = new ObservableCollection<FlexLevelListItemViewModel>();
                foreach (FlexLevelListItemViewModel copyFlexLevel in rentcontract.FlexLevelList) {
                    if (copyFlexLevel != null) {
                        FlexLevel newFlexLevel = new FlexLevel(copyFlexLevel.Id, copyFlexLevel.Level, copyFlexLevel.Unit, copyFlexLevel.Bonus);
                        FlexLevelListItemViewModel flexItem = new FlexLevelListItemViewModel(newFlexLevel);
                        FlexLevels.Add(flexItem);
                    }
                }

                FlexPercentage = rentcontract.FlexPercentage * 100;
                SelectedFlexPercentageSource = rentcontract.FlexPercentageSource;
            }

            AddFlexCommand = new RelayCommand(onAddFlexItem);
            RemoveFlexCommand = new RelayCommand(onRemoveFlexItem);
        }

        public ICommand AddFlexCommand { get; }
		public ICommand RemoveFlexCommand { get; }

		public ObservableCollection<FlexLevelListItemViewModel> FlexLevels { get; }
		public Dictionary<RentContractFlexPercentageSources, string> FlexPercentageSources { get; }
		public ICollectionView LandOwnerList { get; }

		public FlexLevelListItemViewModel SelectedFlexLevel { get; set; }

		private ContractorListItemViewModel _landOwner;
		public ContractorListItemViewModel SelectedLandOwner {
			get { return _landOwner; }
			set {
				_landOwner = value;
				RaisePropertyChanged(() => SelectedLandOwner);
				GenerateShortDescription();
			}
		}

		private string _name;
		public string Name {
			get { return _name; }
			set {
				_name = value;
				RaisePropertyChanged(() => Name);
			}
		}

		private DateTime _startDate;
		public DateTime StartDate {
			get { return _startDate; }
			set {
				_startDate = value;
				RaisePropertyChanged(() => StartDate);
			}
		}

		private TimeSpan? _duration;
		public TimeSpan? Duration {
			get { return _duration; }
			set {
				_duration = value;
				RaisePropertyChanged(() => Duration);
			}
		}

		private double _perAcre;
		public double PerAcre {
			get { return _perAcre; }
			set {
				_perAcre = value;
				RaisePropertyChanged(() => PerAcre);
			}
		}

		private double _actualYield;
		public double ActualYield {
			get { return _actualYield; }
			set {
				_actualYield = value;
				RaisePropertyChanged(() => ActualYield);
			}
		}

		private DateTime _rentDue;
		public DateTime RentDue {
			get { return _rentDue; }
			set {
				_rentDue = value;
				RaisePropertyChanged(() => RentDue);
			}
		}

		private double _flatCropPrice;
		public double FlatCropPrice {
			get { return _flatCropPrice; }
			set {
				_flatCropPrice = value;
				RaisePropertyChanged(() => FlatCropPrice);
			}
		}

		private double _growerCropShare;
		public double GrowerCropShare {
			get { return _growerCropShare; }
			set {
				if (value > 1.0) {
					_growerCropShare = value / 100;
				} else {
					_growerCropShare = value;
				}

				_ownerCropShare = 1.0 - _growerCropShare;
				RaisePropertyChanged(() => GrowerCropShare);
				RaisePropertyChanged(() => OwnerCropShare);
			}
		}

		private double _ownerCropShare;
		public double OwnerCropShare {
			get { return _ownerCropShare; }
			set {
				if (value > 1.0) {
					_ownerCropShare = value / 100;
				} else {
					_ownerCropShare = value;
				}

				_growerCropShare = 1.0 - _ownerCropShare;
				RaisePropertyChanged(() => GrowerCropShare);
				RaisePropertyChanged(() => OwnerCropShare);
			}
		}

		private double _flexPercentage;
		public double FlexPercentage {
			get { return _flexPercentage; }
			set {
				if (value != 0) {
					_flexPercentage = value / 100;
				} else {
					_flexPercentage = 0;
				}

				RaisePropertyChanged(() => FlexPercentage);
			}
		}

		private RentContractFlexPercentageSources _selectedFlexPercentageSource;
		public RentContractFlexPercentageSources SelectedFlexPercentageSource {
			get { return _selectedFlexPercentageSource; }
			set {
				_selectedFlexPercentageSource = value;
				RaisePropertyChanged(() => SelectedFlexPercentageSource);
			}
		}

		public FlexLevel[] GetFlexLevelValueObjects() => FlexLevels.Select(x => new FlexLevel(x.Id, x.Level, x.Unit, x.Bonus)).ToArray();

		public void ResetData() {
			SelectedLandOwner = null;
			Name = string.Empty;

			StartDate = DateTime.UtcNow;
			RentDue = DateTime.UtcNow;
			Duration = null;

			PerAcre = 0;
			ActualYield = 0;
			FlatCropPrice = 0;

			GrowerCropShare = 1;

			FlexLevels.Clear();

			FlexPercentage = 0;
			SelectedFlexPercentageSource = RentContractFlexPercentageSources.None;
		}

		public LandOwner GetLandOwnerValueObject() {
			if (SelectedLandOwner == null) { return null; }

			if (SelectedLandOwner.Group == Strings.People_Text) {
				return new LandOwner(SelectedLandOwner.PersonId, SelectedLandOwner.Name, SelectedLandOwner.PhoneNumber);
			} else if (SelectedLandOwner.Group == Strings.Companies_Text) {
				return new LandOwner(SelectedLandOwner.CompanyId, SelectedLandOwner.Name, SelectedLandOwner.PhoneNumber);
			} else {
				return null;
			}
		}

		public void GenerateShortDescription() {
			var cropName = string.Empty;
			if (parentVm.SelectedCrop != null) {
				cropName = clientEndpoint.GetMasterlistService().GetCropDisplay(parentVm.SelectedCrop).Split(':').First();
			}

			var landOwnerName = string.Empty;
			if (SelectedLandOwner != null) {
				landOwnerName = SelectedLandOwner.Name;
			}

			Name = $"{landOwnerName} {cropName} {Strings.Rent_Text}".Replace("  ", " ").Trim();
		}

		private void onAddFlexItem() {
			var vm = new AddFlexInfoViewModel(clientEndpoint, dispatcher, parentVm.RentContractId, parentVm.SelectedCrop, newFlexLevel => {
				if (newFlexLevel != null) {
					FlexLevelListItemViewModel flexItem = new FlexLevelListItemViewModel(newFlexLevel);
					FlexLevels.Add(flexItem);
				}

				Messenger.Default.Send(new HidePopupMessage());
			});

			Messenger.Default.Send(new ShowPopupMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Resources.Contract.Rent.AddFlexInfoView), "flexinfo", vm)
			});
		}

		private void onRemoveFlexItem() {
			if (SelectedFlexLevel != null) {
				FlexLevels.Remove(SelectedFlexLevel);
			}
		}

		private ICollectionView getLandOwnerList() {
			var dsId = new DataSourceId(currentCropYearId.DataSourceId);
			var landOwnerList = new List<ContractorListItemViewModel>();

			var personListView = clientEndpoint.GetView<PersonListView>(dsId).GetValue(new PersonListView());
			var sortedFilteredPersons = from p in personListView.Persons
										where p.IsActiveInCropYear(currentCropYearId.Id)
										orderby p.Name
										select p;

			sortedFilteredPersons.ForEach(person => {
				landOwnerList.Add(new ContractorListItemViewModel(person));
			});

			var companyListView = clientEndpoint.GetView<CompanyListView>(dsId).GetValue(new CompanyListView());
			var sortedFilteredCompanies = from c in companyListView.Companies
										  where c.IsActiveInCropYear(currentCropYearId.Id)
										  orderby c.Name
										  select c;

			sortedFilteredCompanies.ForEach(company => {
				landOwnerList.Add(new ContractorListItemViewModel(company));
			});

			var retList = CollectionViewSource.GetDefaultView(landOwnerList.OrderBy(x => x.Group).ThenBy(x => x.Name));
			retList.GroupDescriptions.Add(new PropertyGroupDescription("Group"));

			return retList;
		}
	}
}