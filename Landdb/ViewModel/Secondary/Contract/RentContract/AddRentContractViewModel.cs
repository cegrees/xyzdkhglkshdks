﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Resources.Contract;
using Landdb.ViewModel.Secondary.Contract;
using System;
using System.Windows.Input;
using System.Windows.Threading;

namespace Landdb.ViewModel.Secondary.RentContract {
	public class AddRentContractViewModel : ViewModelBase {

		private readonly IClientEndpoint clientEndpoint;
		private readonly Dispatcher dispatcher;

		private readonly CropYearId currentCropYearId;
        private readonly bool iscopy = false;
        private readonly ContractDetailsBaseViewModel copydetails;
        private readonly ContractId copyid;

        public AddRentContractViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, ContractId contractId, int currentCropYear) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;
            copydetails = null;
            copyid = null;
            iscopy = false;

            currentCropYearId = new CropYearId(contractId.DataSourceId, currentCropYear);

			RentContractId = contractId;

			FieldsPageModel = new ContractFieldSelectionPageViewModel(clientEndpoint, dispatcher, currentCropYearId, onCropZoneCropChanged, this);
            FieldsPageModel.Enable = false;
			InfoPageModel = new RentContractInfoViewModel(clientEndpoint, dispatcher, currentCropYearId, this);
			SharePageModel = new ContractShareViewModel(clientEndpoint, dispatcher);

			ReturnToMainViewCommand = new RelayCommand(onReturnToMainView);
			CreateAnotherContractCommand = new RelayCommand(onCreateAnotherContract);
			CreateNewCommand = new RelayCommand(onCreateNew);
			CancelCommand = new RelayCommand(onCancel);
		}

        public AddRentContractViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, ContractId contractId, int currentCropYear, ContractDetailsBaseViewModel copydetails, ContractId copyid) {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            this.copydetails = copydetails;
            this.copyid = copyid;
            iscopy = true;

            currentCropYearId = new CropYearId(contractId.DataSourceId, currentCropYear);

            RentContractId = contractId;

            FieldsPageModel = new ContractFieldSelectionPageViewModel(clientEndpoint, dispatcher, currentCropYearId, onCropZoneCropChanged, this);
            FieldsPageModel.Enable = false;
            InfoPageModel = new RentContractInfoViewModel(clientEndpoint, dispatcher, currentCropYearId, this, copydetails);
            SharePageModel = new ContractShareViewModel(clientEndpoint, dispatcher, copydetails);

            ReturnToMainViewCommand = new RelayCommand(onReturnToMainView);
            CreateAnotherContractCommand = new RelayCommand(onCreateAnotherContract);
            CreateNewCommand = new RelayCommand(onCreateNew);
            CancelCommand = new RelayCommand(onCancel);

            if(copydetails != null) {
                InfoPageModel.Name = copydetails.Name;
            }
        }
        public ICommand CreateNewCommand { get; }
		public ICommand CancelCommand { get; }
		public ICommand ReturnToMainViewCommand { get; }
		public ICommand CreateAnotherContractCommand { get; }

		public ContractFieldSelectionPageViewModel FieldsPageModel { get; }
		public RentContractInfoViewModel InfoPageModel { get; }
		public ContractShareViewModel SharePageModel { get; }

		public CropId SelectedCrop { get; private set; }

		public ContractId RentContractId { get; private set; }

		// for display
		public int CropYear => currentCropYearId.Id;

		private int _selectedPageIndex = 0;
		public int SelectedPageIndex {
			get { return _selectedPageIndex; }
			set {
				_selectedPageIndex = value;
				RaisePropertyChanged(() => SelectedPageIndex);
			}
		}

		private void ResetData() {
			RentContractId = new ContractId(currentCropYearId.DataSourceId, Guid.NewGuid());
			SelectedPageIndex = 0;

			FieldsPageModel.ResetData();
			InfoPageModel.ResetData();
			SharePageModel.ResetData();
		}

		private void onCreateNew() {
			if (SelectedCrop == null) { return; } // this will bail if multiple crops are selected on the fields page
			if (string.IsNullOrWhiteSpace(InfoPageModel.Name)) { return; }

			CreateContract cmd = new CreateContract(
				RentContractId,
				clientEndpoint.GenerateNewMetadata(),
				InfoPageModel.Name,
				InfoPageModel.StartDate.ToUniversalTime(),
				InfoPageModel.Duration,
				InfoPageModel.RentDue.ToUniversalTime(),
				InfoPageModel.PerAcre,
				InfoPageModel.ActualYield,
				InfoPageModel.FlatCropPrice,
				SharePageModel.CropProtectionShare,
				SharePageModel.CropProtectionMargin,
				SharePageModel.CpInventory,
				SharePageModel.FertilizerShare,
				SharePageModel.FertilizerMargin,
				SharePageModel.FertInventory,
				SharePageModel.SeedShare,
				SharePageModel.SeedMargin,
				SharePageModel.SeedInventory,
				SharePageModel.ServiceShare,
				SharePageModel.ServiceMargin,
				SharePageModel.ServiceInventory,
				InfoPageModel.GrowerCropShare,
				FieldsPageModel.GetCropZoneAreas(),
				InfoPageModel.GetLandOwnerValueObject(),
				InfoPageModel.GetFlexLevelValueObjects(),
				currentCropYearId.Id,
				InfoPageModel.FlexPercentage,
				InfoPageModel.SelectedFlexPercentageSource
			);

			clientEndpoint.SendOne(cmd);

            if(copyid != null) {
                DocumentDescriptor sourceDoc = new DocumentDescriptor() {
                    DocumentType = DocumentDescriptor.ContractTypeString,
                    Identity = copyid,
                    Name = copydetails.Name
                };
                AddDocumentToContract cmd2 = new AddDocumentToContract(RentContractId, clientEndpoint.GenerateNewMetadata(), sourceDoc);
                clientEndpoint.SendOne(cmd2);
            }

            Messenger.Default.Send(new ShowOverlayMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Secondary.Contract.PostContractOverlay), "postContract", this)
			});
		}

		private void onCancel() {
			Messenger.Default.Send(new ShowMainViewMessage());
		}

		private void onCropZoneCropChanged(CropId newCropId) {
			SelectedCrop = newCropId;
			InfoPageModel.GenerateShortDescription();
		}

		private void onReturnToMainView() {
			Messenger.Default.Send(new ContractAddedMessage() { LastContractAdded = RentContractId });
			Messenger.Default.Send(new ShowMainViewMessage());
		}

		private void onCreateAnotherContract() {
			ResetData();
			Messenger.Default.Send(new HideOverlayMessage());
		}
    }
}