﻿using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Contract;
using Landdb.ViewModel.Fields;
using Landdb.ViewModel.Shared;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Secondary.Contract {
	public class ContractFieldSelectionPageViewModel : BaseFieldSelectionViewModel {

		private readonly CropId undecidedCropId = new CropId(new Guid("439b1e76-e226-4e74-94b2-90d010523183"));
		private readonly Dictionary<CropId, int> cropDictionary = new Dictionary<CropId, int>();

		private readonly CropZoneRentContractView czRentContractView;
		private readonly Action<CropId> onCropZoneCropChanged;

		private readonly bool shouldCheckForExistingRentContracts;

		private CropId selectedCrop;

		public ContractFieldSelectionPageViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, CropYearId cropYearId, Action<CropId> onCropZoneCropChanged, object parentVm)
			: base(clientEndpoint, dispatcher, cropYearId.Id, CropZoneSelectionEditMode.Contract) {

			SelectedTreeDisplayMode = new TreeDisplayModeDisplayItem("Crops");

			czRentContractView = clientEndpoint.GetView<CropZoneRentContractView>(cropYearId).GetValue(new CropZoneRentContractView());

			shouldCheckForExistingRentContracts = parentVm is RentContract.AddRentContractViewModel;

			this.onCropZoneCropChanged = onCropZoneCropChanged;
		}

        public bool Enable { get; set; }

        protected override void OnFieldCheckedChanged(CropZoneTreeItemViewModel treeItem) {
			// if the crop is null, use the id for "undecided"
			// otherwise, just use the valid crop id
			var thisCrop = treeItem.CropId != null ? treeItem.CropId : undecidedCropId;

			if (!cropDictionary.ContainsKey(thisCrop)) { cropDictionary.Add(thisCrop, 0); }

			if (treeItem.IsChecked.HasValue && treeItem.IsChecked.Value) { // cz is checked
				cropDictionary[thisCrop]++;
			} else { // cz is unchecked
				if (--cropDictionary[thisCrop] == 0) { cropDictionary.Remove(thisCrop); }
			}

			CropId candidateCrop = null;
			if (cropDictionary.Keys.Count == 1) {
				candidateCrop = cropDictionary.Keys.First();
			}

			if (candidateCrop != selectedCrop) {
				selectedCrop = candidateCrop;
				onCropZoneCropChanged?.Invoke(candidateCrop);
			}
		}

		internal override void ResetData() {
			selectedCrop = null;
			cropDictionary.Clear();
			onCropZoneCropChanged(selectedCrop);
			base.ResetData();
		}

		// overriding selectedcropzones for the sole purpose of adding the validation attribute
		[CustomValidation(typeof(ContractFieldSelectionPageViewModel), nameof(ValidateSelectedCropZones))]
		public override ObservableCollection<CropZoneSelectionViewModel> SelectedCropZones {
			get { return base.SelectedCropZones; }
			protected set { base.SelectedCropZones = value; }
		}

		#region Custom Validation
		public static ValidationResult ValidateSelectedCropZones(ObservableCollection<CropZoneSelectionViewModel> cropZones, ValidationContext context) {
			ValidationResult result = null;

			var vm = (ContractFieldSelectionPageViewModel)context.ObjectInstance;

			if (vm.selectedCrop == vm.undecidedCropId) {
				result = new ValidationResult(Strings.InvalidFieldSelections_Text + Strings.PleaseEnsureThatAllSelectedFieldsHaveValidCrop_Text);
			} else if (vm.cropDictionary.Keys.Count > 1) {
				result = new ValidationResult(Strings.InvalidFieldSelections_Text + Strings.PleaseEnsureThatAllSelectedFieldsShareSameCrop_Text);
			} else if (vm.shouldCheckForExistingRentContracts) {
				var associatedRentContracts = from scz in vm.SelectedCropZones
											  from czrc in vm.czRentContractView.CropZoneRentContracts.Values
											  where czrc.RentContractId != null
												&& scz.Id == czrc.CropZoneId
											  select czrc;

				if (associatedRentContracts.Any()) {
					result = new ValidationResult(Strings.InvalidFieldSelections_Text + Strings.AtLeastOneSelectedFieldHasAlreadyBeenAssignedContract_Text);
				}
			}

			return result;
		}
		#endregion
	}
}