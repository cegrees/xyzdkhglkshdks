﻿using GalaSoft.MvvmLight;
using Landdb.Client.Infrastructure;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Threading;

namespace Landdb.ViewModel.Secondary.YieldOperation {
	public class LoadDocumentsViewModel : ViewModelBase {

		private readonly IClientEndpoint clientEndpoint;
		private readonly Dispatcher dispatcher;

		private readonly CropYearId currentCropYearId;

		private ObservableCollection<DocumentDescriptor> sourceDocuments;
		private DocumentDescriptor selectedDocumentDescriptor;
		private object selectedDocumentDetails = null;

		public LoadDocumentsViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, CropYearId currentCropYearId, IEnumerable<DocumentDescriptor> initialSourceDocuments) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			this.currentCropYearId = currentCropYearId;

			sourceDocuments = new ObservableCollection<DocumentDescriptor>(initialSourceDocuments);

			if (sourceDocuments.Any()) {
				SelectedDocumentDescriptor = sourceDocuments.First();
			}
		}

		public int DocumentCount => Documents.Count;
		public ObservableCollection<DocumentDescriptor> Documents => sourceDocuments;
		public object SelectedDocumentDetails => selectedDocumentDetails;
		public DocumentDescriptor[] GetDocuments() => sourceDocuments.ToArray();

		public DocumentDescriptor SelectedDocumentDescriptor {
			get { return selectedDocumentDescriptor; }
			set {
				selectedDocumentDescriptor = value;
				UpdateDetails();
				RaisePropertyChanged(() => SelectedDocumentDescriptor);
			}
		}

		internal void ResetData() {
			sourceDocuments.Clear();
		}

		void UpdateDetails() {
			RaisePropertyChanged(() => SelectedDocumentDetails);
		}
	}
}