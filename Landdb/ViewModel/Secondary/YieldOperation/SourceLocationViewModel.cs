﻿using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Shared;
using Landdb.ViewModel.Yield;
using Landdb.ViewModel.Yield.Popups;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Secondary.YieldOperation {
	public class SourceLocationViewModel : ValidationViewModelBase {

		private readonly IClientEndpoint clientEndpoint;
		private readonly Dispatcher dispatcher;

		private readonly CropYearId currentCropYearId;

		private readonly Action<YieldLocationId> onSelectedYieldLocationChanged;
		private readonly Action<CropId> onSelectedCropChanged;

		public SourceLocationViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, CropYearId currentCropYearId, Action<YieldLocationId> onSelectedYieldLocationChanged, Action<CropId> onSelectedCropChanged) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			this.currentCropYearId = currentCropYearId;

			var segmentTypes = clientEndpoint.GetMasterlistService()
				.GetYieldLocationSegmentTypes()
				.SegmentTypeList
				.Select(x => x.Name);

			var locList = YieldLocationListItemViewModel.GetFlattenedListItemsForCropYear(clientEndpoint, currentCropYearId, false);

			YieldLocationList = (from loc in locList
								 let hasSublocs = loc.SubLocationList.Any()
								 where !segmentTypes.Contains(loc.Descriptor) || hasSublocs
								 select loc).ToList();

			SourceSubLocationInfo = new LoadSubLocationInfoViewModel(clientEndpoint);

			CropList = CropListItemViewModel.GetListItemsForCropYear(clientEndpoint, currentCropYearId);

			this.onSelectedYieldLocationChanged += onSelectedYieldLocationChanged;
			this.onSelectedCropChanged += onSelectedCropChanged;

			AddYieldLocationCommand = new RelayCommand(onAddYieldLocation);

			ValidateViewModel();
		}

		public ICommand AddYieldLocationCommand { get; }

		public LoadSubLocationInfoViewModel SourceSubLocationInfo { get; }

		public List<CropListItemViewModel> CropList { get; }
		public List<YieldLocationListItemViewModel> YieldLocationList { get; }

		// used for the custom combo box for yield locations
		private bool _isLocationListVisible = false;
		public bool IsLocationListVisible {
			get { return _isLocationListVisible; }
			set {
				_isLocationListVisible = value;
				RaisePropertyChanged(() => IsLocationListVisible);
			}
		}

		private YieldLocationListItemViewModel _selectedSourceLocation = null;
        [Required(ErrorMessageResourceName = "ErrorMessage_SourceLocationIsRequired_Text", ErrorMessageResourceType = typeof(Strings))]
        public YieldLocationListItemViewModel SelectedSourceLocation {
			get { return _selectedSourceLocation; }
			set {
				_selectedSourceLocation = value;

				if (_selectedSourceLocation != null) {
					onSelectedYieldLocationChanged(_selectedSourceLocation.Id);
				}

				ValidateAndRaisePropertyChanged(() => SelectedSourceLocation);

				SourceSubLocationInfo.SelectedTopLevelLocation = _selectedSourceLocation;
			}
		}

		private CropListItemViewModel _selectedCrop = null;
        [Required(ErrorMessageResourceName = "ErrorMessage_CropIsRequired_Text", ErrorMessageResourceType = typeof(Strings))]
        public CropListItemViewModel SelectedCrop {
			get { return _selectedCrop; }
			set {
				_selectedCrop = value;
				var cropId = _selectedCrop != null ? _selectedCrop.CropId : null;
				onSelectedCropChanged(cropId);
				ValidateAndRaisePropertyChanged(() => SelectedCrop);
			}
		}

		internal CropId GetSelectedCropId() => _selectedCrop != null ? _selectedCrop.CropId : null;
		internal YieldLocationId GetSelectedYieldLocationId() => _selectedSourceLocation != null ? _selectedSourceLocation.Id : null;

		internal void ResetData() {
			SelectedCrop = null;
			SelectedSourceLocation = null;
		}

		private void onAddYieldLocation() {
			IsLocationListVisible = false;

			var newLocId = new YieldLocationId(currentCropYearId.DataSourceId, Guid.NewGuid());

			var vm = new AddYieldLocationViewModel(clientEndpoint, dispatcher, newLocId, currentCropYearId.Id, li => {
				YieldLocationList.Add(li);
				YieldLocationList.OrderBy(x => x.Name);
				SelectedSourceLocation = YieldLocationList.Where(x => x.Id == li.Id).SingleOrDefault();
			});

			Messenger.Default.Send(new ShowOverlayMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Yield.Popups.AddYieldLocationView), Strings.ScreenDescriptor_AddYieldLocation_Text.ToLower(), vm)
			});
		}
	}
}