﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Resources;
using Landdb.ViewModel.Yield;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Input;
using Landdb.Resources;
using Landdb.ViewModel.Resources.Equipment;

namespace Landdb.ViewModel.Secondary.YieldOperation
{
    public class ImportLoadsViewModel : ViewModelBase
    {
        //List<ImportLoadItem> _loads;
        string _fileName;
        LoadEntryViewModel _parent;
        Action<List<ImportLoadItem>> _loadsReadyToImport;

        public ImportLoadsViewModel( string fileLocation, LoadEntryViewModel parent, Action<List<ImportLoadItem>> onImportConfirmed)
        {
            //loads = new List<ImportLoadItem>(loads.AsEnumerable());
            _fileName = fileLocation;
            _parent = parent;

            _defaultedDriverLoads = new List<ImportLoadItem>();
            _defaultedLocationLoads = new List<ImportLoadItem>();
            _defaultedTruckLoads = new List<ImportLoadItem>();

            LoadsToImport = new ObservableCollection<ImportLoadItem>();
            YieldLocations = _parent.YieldLocationList.ToList();
            TruckList = _parent.TruckList.ToList();
            PersonList = _parent.PersonList.ToList();
            RaisePropertyChanged(() => PersonList);
            _loadsReadyToImport = onImportConfirmed;
            ImportCommand = new RelayCommand(Complete);
            CancelCommand = new RelayCommand(Cancel);
            ShowTemplateCommand = new RelayCommand(CreateAndOpenTemplate);
            UnselectAllCommand = new RelayCommand(UnselectAll);

            Initialize();
        }

        public RelayCommand ImportCommand { get; set; }
        public RelayCommand CancelCommand { get; set; }
        public RelayCommand ShowTemplateCommand { get; set; }
        public RelayCommand UnselectAllCommand { get; set; }

        private List<ImportLoadItem> _defaultedLocationLoads { get; set; }
        public List<YieldLocationListItemViewModel> YieldLocations { get; set; }
        private YieldLocationListItemViewModel _defaultLocation {get;set;}
        public YieldLocationListItemViewModel SelectedDefaultLocation
        {
            get
            {
                return _defaultLocation;
            }
            set
            {
                _defaultLocation = value;

                LoadsToImport.ForEach(x => 
                { 
                    if(_defaultedLocationLoads.Any(y => y.ID == x.ID))
                    {
                        x.Destination = _defaultLocation != null ? _defaultLocation.Name : x.Destination;
                    }
                });

                RaisePropertyChanged(() => LoadsToImport);
            }
        }

        private List<ImportLoadItem> _defaultedTruckLoads { get; set; }
        public List<EquipmentListItemViewModel> TruckList { get; set; }
        private EquipmentListItemViewModel _defaultTruck { get; set; }
        public EquipmentListItemViewModel SelectedDefaultTruck
        {
            get
            {
                return _defaultTruck;
            }
            set
            {
                _defaultTruck = value;

                LoadsToImport.ForEach(x =>
                {
                    if (_defaultedTruckLoads.Any(y => y.ID == x.ID))
                    {
                        x.Truck = _defaultTruck != null ? _defaultTruck.BasicInfoViewModel.Name : x.Truck;
                    }
                });

                //LoadsToImport.ForEach(x => { x.Truck = string.IsNullOrEmpty(x.Truck) ? _defaultTruck.Name : x.Truck; });
                RaisePropertyChanged(() => LoadsToImport);
            }
        }

        private List<ImportLoadItem> _defaultedDriverLoads { get; set; }
        public List<PersonListItemViewModel> PersonList { get; set; }
        private PersonListItemViewModel _selectedPerson { get; set; }
        public PersonListItemViewModel SelectedDefaultDriver
        {
            get
            {
                return _selectedPerson;
            }
            set
            {
                _selectedPerson = value;

                LoadsToImport.ForEach(x =>
                {
                    if (_defaultedDriverLoads.Any(y => y.ID == x.ID))
                    {
                        x.Driver = _selectedPerson != null ? _selectedPerson.Name : x.Driver;
                    }
                });

                //LoadsToImport.ForEach(x => { x.Driver = string.IsNullOrEmpty(x.Driver) ? _selectedPerson.Name : x.Driver; });
                RaisePropertyChanged(() => LoadsToImport);
            }
        }

        public ObservableCollection<ImportLoadItem> LoadsToImport { get; set; }
        private ImportLoadItem _selectedLoad { get; set; }
        public ImportLoadItem SelectedLoad
        {
            get { return _selectedLoad; }
            set {
                _selectedLoad = value;
                RaisePropertyChanged(() => SelectedLoad);
            }
        }

        void Cancel()
        {
            //close the 
            Messenger.Default.Send(new HideOverlayMessage());
        }

        void Complete()
        {
            var sendEmOnOver = LoadsToImport.Where(x => x.IncludedLoad == true).ToList();
            _loadsReadyToImport(sendEmOnOver);
            Messenger.Default.Send(new HideOverlayMessage());
        }

        void UnselectAll()
        {
            LoadsToImport.ForEach((x) =>
            {
                x.IncludedLoad = false;
            });

            RaisePropertyChanged(() => LoadsToImport);
        }

        void CreateAndOpenTemplate()
        {
            //create closed xml version of the expected template and open it.
            var wb = new ClosedXML.Excel.XLWorkbook();
            var ws = wb.AddWorksheet(Strings.ExcelWorksheet_Title_GrainScaleImportTemplate);
            /////////////////////////////////////////////////////////////////////////////
            //HEADERS
            ////////////////////////////////////////////////////////////////////////////
            ws.Cell("A1").Value = Strings.ExcelWorksheet_Header_DateAndTime_Text;
            ws.Cell("B1").Value = Strings.ExcelWorksheet_Header_FieldName_Text;
            ws.Cell("C1").Value = Strings.ExcelWorksheet_Header_LoadNumber_Text;
            ws.Cell("D1").Value = Strings.ExcelWorksheet_Header_Destination_Text;
            ws.Cell("E1").Value = Strings.ExcelWorksheet_Header_Truck_Text;
            ws.Cell("F1").Value = Strings.ExcelWorksheet_Header_Driver_Text;
            ws.Cell("G1").Value = Strings.ExcelWorksheet_Header_Gross_Text;
            ws.Cell("H1").Value = Strings.ExcelWorksheet_Header_Tare_Text;
            ws.Cell("I1").Value = Strings.ExcelWorksheet_Header_Net_Text;
            ws.Cell("J1").Value = Strings.ExcelWorksheet_Header_LoadMoisture_Text;
            ws.Cell("K1").Value = Strings.ExcelWorksheet_Header_TestWeight_Text;
            ws.Cell("L1").Value = Strings.ExcelWorksheet_Header_ForeignMaterialPercent_Text;
            ws.Cell("M1").Value = Strings.ExcelWorksheet_Notes_Text;

            string fileName = System.IO.Path.GetTempPath() + Guid.NewGuid().ToString() + ".xlsx";
            wb.SaveAs(fileName);

            System.Diagnostics.Process.Start(fileName);
        }

        void Initialize()
        {
            try
            {
                var workbook1 = new ClosedXML.Excel.XLWorkbook(_fileName);
                var ws1 = workbook1.Worksheet(1);

                var rowCnt = ws1.Rows().Count();

                for (int i = 2; i <= rowCnt; i++)
                {
                    var row = ws1.Row(i);
                    bool empty = row.IsEmpty();
                    if (!empty)
                    {
                        var newLoad = new ImportLoadItem();
                        newLoad.ID = Guid.NewGuid();
                        var valueToConvert = ws1.Cell(i, 1).GetValue<string>();
                        newLoad.IncludedLoad = true;
                        newLoad.LoadDate = Convert.ToDateTime(valueToConvert);
                        newLoad.Field = ws1.Cell(i, 2).GetValue<string>();
                        newLoad.LoadId = ws1.Cell(i, 3).GetValue<string>();
                        newLoad.Gross = StringSanatizedAndConvertedToDecimal(ws1.Cell(i, 7).GetValue<string>());
                        newLoad.Tare = StringSanatizedAndConvertedToDecimal(ws1.Cell(i, 8).GetValue<string>());
                        newLoad.Net = StringSanatizedAndConvertedToDecimal(ws1.Cell(i, 9).GetValue<string>());
                        newLoad.LoadMoisture = StringSanatizedAndConvertedToDecimal(ws1.Cell(i, 10).GetValue<string>()) / 100;
                        newLoad.TestWeight = StringSanatizedAndConvertedToDecimal(ws1.Cell(i, 11).GetValue<string>());
                    
                        var foriegnMaterial = ws1.Cell(i, 12).GetValue<string>();
                        Regex r = new Regex("(?:[^0-9.]|(?<=['\"])s)", RegexOptions.IgnoreCase | RegexOptions.CultureInvariant | RegexOptions.Compiled);
                        var returnedValue = r.Replace(foriegnMaterial, String.Empty);
                        newLoad.FMPercent = StringSanatizedAndConvertedToDecimal(ws1.Cell(i, 12).GetValue<string>());
                        newLoad.Notes = ws1.Cell(i, 13).GetValue<string>();

                        //TRY TO MATCH THE DESTINATION
                        var locationName = ws1.Cell(i, 4).GetValue<string>().Trim();
                        if (!string.IsNullOrEmpty(locationName))
                        {
                            //return the greatest match over 80% or 78%
                            double lastPercentage = 0.0;
                            YieldLocationListItemViewModel matchedLocal = null;
                            foreach (var t in _parent.YieldLocationList)
                            {
                                var howSimilar = proximity(locationName, t.Name);

                                if (howSimilar > .78 && howSimilar > lastPercentage)
                                {
                                    lastPercentage = howSimilar;
                                    matchedLocal = t;
                                }
                            }
                            //var yieldLocation = !string.IsNullOrEmpty(locationName) && _parent.YieldLocationList.Any(x => x.Name.ToLower() == locationName.ToLower()) ? _parent.YieldLocationList.FirstOrDefault(x => x.Name.ToLower() == locationName.ToLower()) : null;
                            newLoad.Destination = matchedLocal != null ? matchedLocal.Name : string.Empty;
                        }

                        var truckName = ws1.Cell(i, 5).GetValue<string>().Trim();
                        if (!string.IsNullOrEmpty(truckName))
                        {
                            //return the greatest match over 80% or 78%
                            double lastPercentage = 0.0;
                            EquipmentListItemViewModel matchedTruck = null;
                            foreach (var t in _parent.TruckList)
                            {
                                var howSimilar = proximity(truckName, t.BasicInfoViewModel.Name);

                                if (howSimilar > .78 && howSimilar > lastPercentage)
                                {
                                    lastPercentage = howSimilar;
                                    matchedTruck = t;
                                }
                            }

                            //var truck = !string.IsNullOrEmpty(truckName) && _parent.TruckList.Any(x => x.Name.ToLower() == truckName.ToLower()) ? _parent.TruckList.FirstOrDefault(x => x.Name.ToLower() == truckName.ToLower()) : null;
                            newLoad.Truck = matchedTruck != null ? matchedTruck.BasicInfoViewModel.Name : string.Empty;
                        }

                        var driverName = ws1.Cell(i, 6).GetValue<string>().Trim();
                        if (!string.IsNullOrEmpty(driverName))
                        {
                            double lastPercentage = 0.0;
                            PersonListItemViewModel matchedPerson = null;
                            foreach (var t in _parent.PersonList)
                            {
                                var howSimilar = proximity(driverName, t.Name);

                                if (howSimilar > .78 && howSimilar > lastPercentage)
                                {
                                    lastPercentage = howSimilar;
                                    matchedPerson = t;
                                }
                            }

                            //var driver = !string.IsNullOrEmpty(driverName) && _parent.PersonList.Any(x => x.Name.ToLower() == driverName.ToLower()) ? _parent.PersonList.FirstOrDefault(x => x.Name.ToLower() == driverName.ToLower()) : null;
                            newLoad.Driver = matchedPerson != null ? matchedPerson.Name : string.Empty;
                        }

                        LoadsToImport.Add(newLoad);
                    }
                }

                _defaultedDriverLoads = (from l in LoadsToImport where string.IsNullOrEmpty(l.Driver) select l).ToList();
                //_defaultedDriverLoads.AddRange(LoadsToImport.Where(x => string.IsNullOrEmpty(x.Driver)));
                _defaultedLocationLoads.AddRange(LoadsToImport.Where(x => x.Destination == null || x.Destination == string.Empty));
                _defaultedTruckLoads.AddRange(LoadsToImport.Where(x => x.Truck == null || x.Truck == string.Empty));
            }
            catch (Exception ex)
            {

            }
        }

        private decimal StringSanatizedAndConvertedToDecimal(string intialString)
        {
            Regex r = new Regex("(?:[^0-9.]|(?<=['\"])s)", RegexOptions.IgnoreCase | RegexOptions.CultureInvariant | RegexOptions.Compiled);
            var returnedValue = r.Replace(intialString, String.Empty);

            try
            {
                var decimalValue = Convert.ToDecimal(returnedValue);
                return decimalValue;
            }
            catch (Exception ex)
            {
                return 0m;
            }
        }

        private string StringSanatized(string intialString)
        {
            Regex r = new Regex("(?:[^a-z0-9]|(?<=['\"])s)", RegexOptions.IgnoreCase | RegexOptions.CultureInvariant | RegexOptions.Compiled);
            var returnedValue = r.Replace(intialString, String.Empty);

            return returnedValue;
        }

        private static readonly double mWeightThreshold = 0.7;
        private static readonly int mNumChars = 4;

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// Jaro-Winkler Similarity Algorithm
        /// Code found @ http://stackoverflow.com/questions/19123506/jaro-winkler-distance-algorithm-in-c-sharp
        /// answered by leebickmtu
        /// </summary>
        /// <param name="aString1"></param>
        /// <param name="aString2"></param>
        /// <returns></returns>
        public static double proximity(string aString1, string aString2)
        {
            int lLen1 = aString1.Length;
            int lLen2 = aString2.Length;
            if (lLen1 == 0)
                return lLen2 == 0 ? 1.0 : 0.0;

            int lSearchRange = Math.Max(0, Math.Max(lLen1, lLen2) / 2 - 1);

            // default initialized to false
            bool[] lMatched1 = new bool[lLen1];
            bool[] lMatched2 = new bool[lLen2];

            int lNumCommon = 0;
            for (int i = 0; i < lLen1; ++i)
            {
                int lStart = Math.Max(0, i - lSearchRange);
                int lEnd = Math.Min(i + lSearchRange + 1, lLen2);
                for (int j = lStart; j < lEnd; ++j)
                {
                    if (lMatched2[j]) continue;
                    if (aString1[i] != aString2[j])
                        continue;
                    lMatched1[i] = true;
                    lMatched2[j] = true;
                    ++lNumCommon;
                    break;
                }
            }
            if (lNumCommon == 0) return 0.0;

            int lNumHalfTransposed = 0;
            int k = 0;
            for (int i = 0; i < lLen1; ++i)
            {
                if (!lMatched1[i]) continue;
                while (!lMatched2[k]) ++k;
                if (aString1[i] != aString2[k])
                    ++lNumHalfTransposed;
                ++k;
            }
            // System.Diagnostics.Debug.WriteLine("numHalfTransposed=" + numHalfTransposed);
            int lNumTransposed = lNumHalfTransposed / 2;

            // System.Diagnostics.Debug.WriteLine("numCommon=" + numCommon + " numTransposed=" + numTransposed);
            double lNumCommonD = lNumCommon;
            double lWeight = (lNumCommonD / lLen1
                             + lNumCommonD / lLen2
                             + (lNumCommon - lNumTransposed) / lNumCommonD) / 3.0;

            if (lWeight <= mWeightThreshold) return lWeight;
            int lMax = Math.Min(mNumChars, Math.Min(aString1.Length, aString2.Length));
            int lPos = 0;
            while (lPos < lMax && aString1[lPos] == aString2[lPos])
                ++lPos;
            if (lPos == 0) return lWeight;
            return lWeight + 0.1 * lPos * (1.0 - lWeight);

        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    }
}
