﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure.Messages;
using System.Windows.Input;

namespace Landdb.ViewModel.Secondary.YieldOperation {
	public class MoistureEstimationOverlayViewModel : ViewModelBase {
		public MoistureEstimationOverlayViewModel() {
			InitialWeight = 1000;
			InitialMoisture = 23;
			TargetMoisture = 15.5M;

			recalculate();

			CloseCommand = new RelayCommand(onClose);
		}

		public ICommand CloseCommand { get; }

		private decimal _initalWeight;
		public decimal InitialWeight {
			get { return _initalWeight; }
			set {
				_initalWeight = value;
				recalculate();
				RaisePropertyChanged(() => InitialWeight);
			}
		}

		private decimal _initialMoisture;
		public decimal InitialMoisture {
			get { return _initialMoisture; }
			set {
				_initialMoisture = value / 100;
				recalculate();
				RaisePropertyChanged(() => InitialMoisture);
			}
		}

		private decimal _targetMoisture;
		public decimal TargetMoisture {
			get { return _targetMoisture; }
			set {
				_targetMoisture = value / 100;
				recalculate();
				RaisePropertyChanged(() => TargetMoisture);
			}
		}

		private decimal _finalWeight;
		public decimal FinalWeight {
			get { return _finalWeight; }
			set { 
				_finalWeight = value;
				RaisePropertyChanged(() => FinalWeight);
			}
		}


		private void recalculate() {
			if (TargetMoisture != 0) {
				var numerator = 1 - InitialMoisture;
				var denominator = 1 - TargetMoisture;
				FinalWeight = InitialWeight * (numerator / denominator);
			} else {
				FinalWeight = 0;
			}
		}

		private void onClose() {
			Messenger.Default.Send(new HideOverlayMessage());
		}
	}
}