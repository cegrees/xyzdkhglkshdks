﻿using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Client.Services;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Resources;
using Landdb.ViewModel.Resources.Popups;
using Landdb.ViewModel.Yield;
using Landdb.ViewModel.Yield.Popups;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Resources;
using Landdb.ViewModel.Resources.Equipment;
using Landdb.ViewModel.Resources.Popups.Equipment;
using Landdb.Views.Resources.Popups.Equipment;

namespace Landdb.ViewModel.Secondary.YieldOperation {
	public class LoadViewModel : ValidationViewModelBase {

		private readonly IClientEndpoint clientEndpoint;
		private readonly Dispatcher dispatcher;

		private readonly LoadEntryViewModel parent;

		public LoadViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, LoadEntryViewModel parent) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			this.parent = parent;

			StartDate = parent.DefaultStartDate;
			StartTime = parent.DefaultStartTime;

			var nameGenerationService = ServiceLocator.Get<INameAutoGenerationService>();
			LoadNumber = nameGenerationService.DisplayName;

			LoadPropertySets = new List<LoadPropertySetViewModel>();
			refreshLoadPropertySets();

			SplitLoadPriceInfo = new LoadPriceSplitInfoViewModel(clientEndpoint, dispatcher, parent.ProductionContractList);
			QuantityInfo = new LoadQuantityInfoViewModel(clientEndpoint, dispatcher, onFinalQuantityChanged, onTruckUnitChanged);
			DestinationSubLocationInfo = new LoadSubLocationInfoViewModel(clientEndpoint);

			var defaultSelectedUnit = parent.CommodityUnitList.FirstOrDefault(x => x == parent.DefaultFinalQuantityUnit);
			if (defaultSelectedUnit != null) { QuantityInfo.SelectedFinalQuantityUnit = defaultSelectedUnit; }

			QuantityInfo.SetShrinkDefaults(
				parent.StandardDryMoisturePercentage ?? 0,
				parent.UserDefinedShrinkPercentage ?? 0,
				parent.UserDefinedTargetMoisturePercentage ?? 0
			);

			parent.PropertyChanged += parent_PropertyChanged;

			CreateCompositeUnitCommand = new RelayCommand(onCreateCompositeUnit);
			AddYieldLocationCommand = new RelayCommand(onCreateYieldLocation);
			AddEquipmentCommand = new RelayCommand(onCreateEquipment);
			AddPersonCommand = new RelayCommand(onCreatePerson);

			parent.RentContractList.CollectionChanged += (sender, args) => {
				SplitLoadPriceInfo.SelectedRentContract = parent.RentContractList.FirstOrDefault();
			};

			ErrorsChanged += (sender, args) => {
				parent.ValidateViewModel();
			};

			ValidateViewModel();
		}

		public ICommand CreateCompositeUnitCommand { get; }
		public ICommand AddYieldLocationCommand { get; }
		public ICommand AddEquipmentCommand { get; }
		public ICommand AddPersonCommand { get; }

		#region Properties
		public LoadQuantityInfoViewModel QuantityInfo { get; }
		public LoadPriceSplitInfoViewModel SplitLoadPriceInfo { get; }
		public LoadSubLocationInfoViewModel DestinationSubLocationInfo { get; }

		public List<LoadPropertySetViewModel> LoadPropertySets { get; private set; }

		public DateTime StartDateTime => StartDate.Date.Add(StartTime.TimeOfDay);
		public bool IsInitialMoistureUIVisible => IsStandardMoistureShrinkUIVisible || IsHandlingShrinkUIVisible;
		public bool IsStandardMoistureShrinkUIVisible => parent.StandardDryMoisturePercentage.HasValue;

		public bool IsHandlingShrinkUIVisible =>
			parent.UserDefinedTargetMoisturePercentage.HasValue
				&& parent.UserDefinedTargetMoisturePercentage > 0
				&& parent.UserDefinedShrinkPercentage.HasValue
				&& parent.UserDefinedShrinkPercentage > 0;

		public bool IsValid =>
			!HasErrors &&
				!QuantityInfo.HasErrors
				&& !string.IsNullOrWhiteSpace(LoadNumber)
				&& QuantityInfo.SelectedFinalQuantityUnit != null
				&& SelectedDestinationLocation != null;


		private bool _isSaleLoad = false;
		public bool IsSaleLoad {
			get { return _isSaleLoad; }
			set {
				_isSaleLoad = value;

				if (!_isSaleLoad) {
					SplitLoadPriceInfo.IsSplitLoad = false;
				}

				RaisePropertyChanged(() => IsSaleLoad);
			}
		}

		private string _loadNumber = string.Empty;
        [Required(AllowEmptyStrings = false, ErrorMessageResourceName = "ErrorMessage_LoadNumberIsRequired_Text", ErrorMessageResourceType = typeof(Strings))]
        public string LoadNumber {
			get { return _loadNumber; }
			set {
				_loadNumber = value;
				parent.RefreshTotalsAndCounts();
				ValidateAndRaisePropertyChanged(() => LoadNumber);
				RaisePropertyChanged(() => IsValid);
			}
		}

		private YieldLocationListItemViewModel _destinationLocation = null;
		[Required]
		public YieldLocationListItemViewModel SelectedDestinationLocation {
			get { return _destinationLocation; }
			set {
				_destinationLocation = value;

				if (_destinationLocation != null) {
					IsSaleLoad = _destinationLocation.YieldLocationType == YieldLocationTypes.Sale;
					DestinationSubLocationInfo.SelectedTopLevelLocation = _destinationLocation;
				}

				parent.RefreshTotalsAndCounts();

				ValidateAndRaisePropertyChanged(() => SelectedDestinationLocation);
				RaisePropertyChanged(() => IsValid);
			}
		}

		private DateTime _startDate = DateTime.MinValue;
		public DateTime StartDate {
			get { return _startDate; }
			set {
				_startDate = value;
				parent.DefaultStartDate = _startDate;
				RaisePropertyChanged(() => StartDate);
			}
		}

		private DateTime _startTime = DateTime.MinValue;
		public DateTime StartTime {
			get { return _startTime; }
			set {
				_startTime = value;
				RaisePropertyChanged(() => StartTime);
			}
		}

		private string _commodityDescription;
		public string CommodityDescription {
			get { return _commodityDescription; }
			set {
				_commodityDescription = value;
				RaisePropertyChanged(() => CommodityDescription);
			}
		}

		private EquipmentListItemViewModel _truck = null;
		public EquipmentListItemViewModel SelectedTruck {
			get { return _truck; }
			set {
				_truck = value;
				RaisePropertyChanged(() => SelectedTruck);

				setDefaultTare();
			}
		}

		private PersonListItemViewModel _driver = null;
		public PersonListItemViewModel SelectedDriver {
			get { return _driver; }
			set {
				_driver = value;
				RaisePropertyChanged(() => SelectedDriver);
			}
		}

		private string _notes = string.Empty;
		public string Notes {
			get { return _notes; }
			set {
				_notes = value;
				RaisePropertyChanged(() => Notes);
			}
		}

		// used for the custom combo box for yield locations
		private bool _isLocationListVisible = false;
		public bool IsLocationListVisible {
			get { return _isLocationListVisible; }
			set {
				_isLocationListVisible = value;
				RaisePropertyChanged(() => IsLocationListVisible);
			}
		}

		// used for the custom combo box for people
		private bool _isPersonListVisible = false;
		public bool IsPersonListVisible {
			get { return _isPersonListVisible; }
			set {
				_isPersonListVisible = value;
				RaisePropertyChanged(() => IsPersonListVisible);
			}
		}

		// used for the custom combo box for equipment
		private bool _isEquipmentListVisible = false;
		public bool IsEquipmentListVisible {
			get { return _isEquipmentListVisible; }
			set {
				_isEquipmentListVisible = value;
				RaisePropertyChanged(() => IsEquipmentListVisible);
			}
		}
		#endregion

		public ReportedLoadPropertySet[] GetReportedLoadPropertySets() {
			var rlpList = new List<ReportedLoadPropertySet>();

			foreach (var set in LoadPropertySets) {
				var rlpValueList = new List<ReportedLoadPropertyValue>();

				foreach (var prop in set.LoadProperties) {
					rlpValueList.Add(new ReportedLoadPropertyValue() {
						LoadPropertyId = prop.Id,
						ReportedValue = prop.Value,
						ReportedUnit = prop.SelectedUnit,
					});
				}

				rlpList.Add(new ReportedLoadPropertySet() {
					LoadPropertySetId = set.Id,
					ReportedPropertyValues = rlpValueList,
				});
			}

			return rlpList.ToArray();
		}

		#region Command Handlers
		private void onCreateCompositeUnit() {
			QuantityInfo.IsFinalQuantityUnitListVisible = false;

			var vm = new AddCompositeUnitViewModel(clientEndpoint, dispatcher, parent.CurrentCropYearId, parent.SelectedCropId, parent.UnsavedCustomUnitList, changeVm => {
				if (changeVm != null && changeVm.IsValid) {
					var newUnit = changeVm.GetNewUnit();

					// don't save the new unit straight to parent.AllowedUnits. otherwise it will
					// be lost on the next unit refresh.
					parent.UnsavedCustomUnitList.Add(newUnit);
					parent.RefreshUnitList();

					// go ahead and select the new unit for them
					QuantityInfo.SelectedFinalQuantityUnit = parent.CommodityUnitList.SingleOrDefault(x => x.FullDisplay == newUnit.FullDisplay);
				}

				Messenger.Default.Send(new HideOverlayMessage());
			});

			Messenger.Default.Send(new ShowOverlayMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Yield.Popups.AddCompositeUnitView), Strings.ScreenDescriptor_AddYieldUnit_Text.ToLower(), vm)
			});
		}

		private void onCreateYieldLocation() {
			IsLocationListVisible = false;

			var newLocId = new YieldLocationId(parent.CurrentCropYearId.DataSourceId, Guid.NewGuid());

			var vm = new AddYieldLocationViewModel(clientEndpoint, dispatcher, newLocId, parent.CurrentCropYearId.Id, li => {
				parent.YieldLocationList.Add(li);
				SelectedDestinationLocation = parent.YieldLocationList.SingleOrDefault(x => x.Id == li.Id);
			});

			Messenger.Default.Send(new ShowOverlayMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Yield.Popups.AddYieldLocationView), Strings.ScreenDescriptor_AddYieldLocation_Text.ToLower(), vm)
			});
		}

		private void onCreateEquipment() {
			AddEquipmentViewModel addEquipmentViewModel = new AddEquipmentViewModel(clientEndpoint, 
				new EquipmentId(parent.CurrentCropYearId.DataSourceId, Guid.NewGuid()), parent.CurrentCropYearId, (equipmentListItem, equipmentdetailsView) => {
					parent.TruckList.Add(equipmentListItem);
					SelectedTruck = parent.TruckList.SingleOrDefault(x => x.EquipmentId == equipmentListItem.EquipmentId);
				}, EquipmentPageViewModel.LastUsedStatusInfoSelectorItem,
				EquipmentPageViewModel.LastUsedEquipmentType);
			EquipmentPageViewModel.LastUsedStatusInfoSelectorItem = StatusInfoSelectorItem.CreateStatusInfoSelectorItem(
				addEquipmentViewModel.BasicInfoViewModel.GetCurrentStatusInfoViewModel.EquipmentStatusType,
				addEquipmentViewModel.BasicInfoViewModel.GetCurrentStatusInfoViewModel.IsActive,
				addEquipmentViewModel.BasicInfoViewModel.GetCurrentStatusInfoViewModel.EquipmentStatusDisplayText);
			EquipmentPageViewModel.LastUsedEquipmentType =
				addEquipmentViewModel.BasicInfoViewModel.SelectedEquipmentType.GetEquipmentType;
			Messenger.Default.Send(new ShowOverlayMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(AddEquipmentBasicInfoOnlyView).ToString(), Strings.ScreenDescriptor_AddEquipment_Text.ToLower(), addEquipmentViewModel)
			});
		}

		private void onCreatePerson() {
			IsPersonListVisible = false;

			var newPersonId = new PersonId(parent.CurrentCropYearId.DataSourceId, Guid.NewGuid());

			var vm = new AddPersonViewModel(clientEndpoint, dispatcher, newPersonId, parent.CurrentCropYearId.Id, li => {
				parent.PersonList.Add(li);
				SelectedDriver = parent.PersonList.SingleOrDefault(x => x.Id == li.Id);
			});

			Messenger.Default.Send(new ShowOverlayMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Resources.Popups.AddPersonView), Strings.ScreenDescriptor_AddPerson_Text.ToLower(), vm)
			});
		}
		#endregion

		private void parent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e) {
			if (e.PropertyName == nameof(parent.SelectedCropId)) {
				refreshLoadPropertySets();

				var defaultUnitSelection = parent.CommodityUnitList.FirstOrDefault(x => x == parent.DefaultFinalQuantityUnit);
				if (defaultUnitSelection != null) { QuantityInfo.SelectedFinalQuantityUnit = defaultUnitSelection; }

				QuantityInfo.SetShrinkDefaults(
					parent.StandardDryMoisturePercentage ?? 0,
					parent.UserDefinedShrinkPercentage ?? 0,
					parent.UserDefinedTargetMoisturePercentage ?? 0
				);

				RaisePropertyChanged(() => IsInitialMoistureUIVisible);
				RaisePropertyChanged(() => IsStandardMoistureShrinkUIVisible);
				RaisePropertyChanged(() => IsHandlingShrinkUIVisible);
			} else if (e.PropertyName == nameof(parent.LoadPropertySetSelections)) {
				refreshLoadPropertySets();
			}
		}

		private void refreshLoadPropertySets() {
			var lpDefinitions = clientEndpoint.GetMasterlistService().GetLoadPropertyDefinitions();
			var activeSets = lpDefinitions.LoadPropertySets.Where(x => x.IsActive);
			var selectedSetIds = parent.LoadPropertySetSelections.Where(x => x.IsChecked).Select(x => x.Id);
			var existingSetIds = LoadPropertySets.Select(x => x.Id);

			// remove unselected sets from the list
			LoadPropertySets.RemoveAll(x => !selectedSetIds.Contains(x.Id));

			// get the list of sets to add
			var propertySetsToAdd = from set in activeSets
									from selectedSetId in selectedSetIds
									where set.Id == selectedSetId
										&& !existingSetIds.Contains(selectedSetId)
									select set;

			// construct the viewmodels. this is a horrible, horrible abuse of the language.
			var propertySetViewModelsToAdd = propertySetsToAdd.Select(set => new LoadPropertySetViewModel(set) {
				LoadProperties = lpDefinitions.LoadProperties.Where(prop => set.LoadPropertyIds.Contains(prop.Id)).Select(prop => new LoadPropertyViewModel(prop) {
					ListItems = lpDefinitions.LoadPropertyListItems
									.Where(li => prop.ListItemIds != null && prop.ListItemIds.Contains(li.Id))
									.Select(li => new LoadPropertyListItemViewModel(li))
									.ToList()
				}).ToList()
			});

			LoadPropertySets = LoadPropertySets.Union(propertySetViewModelsToAdd).OrderBy(x => x.Name).ToList();
			RaisePropertyChanged(() => LoadPropertySets);
		}

		private void onFinalQuantityChanged() {
			parent.RefreshTotalsAndCounts();

			parent.DefaultFinalQuantityUnit = QuantityInfo.SelectedFinalQuantityUnit;

			SplitLoadPriceInfo.FinalQuantityValue = QuantityInfo.FinalQuantity;
			SplitLoadPriceInfo.FinalQuantityUnit = QuantityInfo.SelectedFinalQuantityUnit;

			DestinationSubLocationInfo.FinalQuantityValue = QuantityInfo.FinalQuantity;
			DestinationSubLocationInfo.FinalQuantityUnit = QuantityInfo.SelectedFinalQuantityUnit;
		}

		private void onTruckUnitChanged() {
			setDefaultTare();
		}

		private void setDefaultTare() {
			var isTruckSelectionValid = SelectedTruck != null && SelectedTruck.EquipmentId != null && SelectedTruck.EquipmentId.Id != Guid.Empty;

			if (isTruckSelectionValid && QuantityInfo.SelectedTruckWeightUnit != null) {
				var truckDetails = clientEndpoint.GetView<Domain.ReadModels.Equipment.EquipmentDetailsView>(SelectedTruck.EquipmentId).GetValue(() => null);

				if (truckDetails != null) {
					// from the list of the select truck's associated loads, pick the most recent one that has a 
					// weight unit that matches the one currently selected. defaults to zero if none found.
					// basically, we're setting the default tare by looking at the one most recently used.
					QuantityInfo.TruckTareWeight = (from l in truckDetails.AssociatedLoads
													orderby l.StartDateTime descending
													where l.TareUnit == QuantityInfo.SelectedTruckWeightUnit
													select l.TareValue).FirstOrDefault();
				}
			}
		}
	}
}