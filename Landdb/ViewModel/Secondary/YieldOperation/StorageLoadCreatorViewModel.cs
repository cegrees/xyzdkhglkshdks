﻿using Landdb.Client.Infrastructure;
using System;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Secondary.YieldOperation {
	public class StorageLoadCreatorViewModel : LoadCreatorBaseViewModel {

		public StorageLoadCreatorViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, YieldOperationId yieldOperationId, CropYearId currentCropYearId, params DocumentDescriptor[] sourceDocuments) :
			base(clientEndpoint, dispatcher, currentCropYearId, yieldOperationId, sourceDocuments) {

			SourceLocationPageModel = new SourceLocationViewModel(clientEndpoint, dispatcher, currentCropYearId, OnSelectedSourceLocationChanged, OnSelectedCropChanged);
		}

		public SourceLocationViewModel SourceLocationPageModel { get; }

		public override string TitleDisplay => Strings.CreateStorageLoads_Text.ToLower();

		protected override CropId GetSelectedCropId() => SourceLocationPageModel.GetSelectedCropId();

		public override bool IsCompletionAllowed => !(SourceLocationPageModel.HasErrors || LoadEntryPageModel.HasErrors);

		protected override void SaveValidLoads() {
			var cropId = GetSelectedCropId();
			var sourceLocationId = SourceLocationPageModel.GetSelectedYieldLocationId();

			foreach (var load in LoadEntryPageModel.ValidLoads) {
				var loadId = new LoadId(OperationId.DataSourceId, Guid.NewGuid());
				var truckId = load.SelectedTruck != null ? load.SelectedTruck.EquipmentId : null;
				var driverId = load.SelectedDriver != null ? load.SelectedDriver.Id : null;

				var quantityInfoVm = load.QuantityInfo;

				// zero out the target moisture percentage if it isn't used
				var targetMoisturePercentage = quantityInfoVm.StandardMoistureShrinkQuantity != 0 ? quantityInfoVm.StandardDryMoisturePercentage : 0;

				IDomainCommand cmd = null;

				if (load.SelectedDestinationLocation.YieldLocationType.Equals(YieldLocationTypes.Sale)) {
					cmd = new CreateStorageToSaleLoad(
						loadId,
						clientEndpoint.GenerateNewMetadata(),
						currentCropYearId.Id,
						OperationId,
						load.LoadNumber,
						load.StartDateTime.ToUniversalTime(),
						load.Notes,
						SourceLocationPageModel.SourceSubLocationInfo.GetSelectedLocationIds(),
						load.SelectedDestinationLocation.Id,
						cropId,
						load.CommodityDescription,
						load.SplitLoadPriceInfo.GetRentContractId(),
						truckId,
						driverId,
						quantityInfoVm.GetQuantityInfoV2ValueObject(),
						load.SplitLoadPriceInfo.GetGrowerPriceInfoValueObject(),
						load.SplitLoadPriceInfo.GetLandOwnerPriceInfoValueObject(),
						load.GetReportedLoadPropertySets(),
						DocumentsModel.GetDocuments()
					);
				} else if (load.SelectedDestinationLocation.YieldLocationType.Equals(YieldLocationTypes.Storage)) {
					cmd = new CreateStorageToStorageLoad(
						loadId,
						clientEndpoint.GenerateNewMetadata(),
						currentCropYearId.Id,
						OperationId,
						load.LoadNumber,
						load.StartDateTime.ToUniversalTime(),
						load.Notes,
						SourceLocationPageModel.SourceSubLocationInfo.GetSelectedLocationIds(),
						load.DestinationSubLocationInfo.GetSelectedLocationIds(),
						cropId,
						load.CommodityDescription,
						truckId,
						driverId,
						quantityInfoVm.GetQuantityInfoV2ValueObject(),
						load.GetReportedLoadPropertySets(),
						DocumentsModel.GetDocuments()
					);
				}

				if (cmd != null) {
					clientEndpoint.SendOne(cmd);
				} else {
					throw new Exception($"Failed to generate a create command for yield location type: {load.SelectedDestinationLocation.YieldLocationType}");
				}
			}

			ShowPostYieldOperationOverlay();
		}

		protected override void ResetData() {
			SourceLocationPageModel.ResetData();
			ResetDataInternal();
		}

		protected void OnSelectedSourceLocationChanged(YieldLocationId newLocationId) {
			if (newLocationId != null) {
				LoadEntryPageModel.SelectedSourceLocationId = newLocationId;
				LoadEntryPageModel.RefreshDestinationLocationList();
			}
		}
	}
}