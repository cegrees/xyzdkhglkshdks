﻿using Landdb.Client.Infrastructure;
using Landdb.ViewModel.Fields;
using Landdb.ViewModel.Shared;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Secondary.YieldOperation {
	public class FieldSelectionViewModel : BaseFieldSelectionViewModel {

		private readonly Action<CropId> onCropZoneCropChanged;

		private readonly CropId undecidedCropId = new CropId(new Guid("439b1e76-e226-4e74-94b2-90d010523183"));

		private Dictionary<CropId, int> cropDictionary = new Dictionary<CropId, int>();

		public FieldSelectionViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, int cropYear, Action<CropId> onCropZoneCropChanged)
			: base(clientEndpoint, dispatcher, cropYear, CropZoneSelectionEditMode.Yield) {

			SelectedTreeDisplayMode = new TreeDisplayModeDisplayItem("Crops");

			this.onCropZoneCropChanged = onCropZoneCropChanged;

			ValidateViewModel();
		}

		protected override void OnFieldCheckedChanged(CropZoneTreeItemViewModel treeItem) {
			// if the crop is null, use the id for "undecided"
			// otherwise, just use the valid crop id
			var thisCrop = treeItem.CropId != null ? treeItem.CropId : undecidedCropId;

			if (!cropDictionary.ContainsKey(thisCrop)) { cropDictionary.Add(thisCrop, 0); }

			if (treeItem.IsChecked.HasValue && treeItem.IsChecked.Value) { // cz is checked
				cropDictionary[thisCrop]++;
			} else { // cz is unchecked
				if (--cropDictionary[thisCrop] == 0) { cropDictionary.Remove(thisCrop); }
			}

			CropId candidateCrop = null;
			if (cropDictionary.Keys.Count == 1) {
				candidateCrop = cropDictionary.Keys.First();
			}

			if (candidateCrop != SelectedCrop) {
				SelectedCrop = candidateCrop;
				onCropZoneCropChanged?.Invoke(candidateCrop);
			}
		}

		private CropId selectedCrop;
		public CropId SelectedCrop {
			get { return selectedCrop; }
			set {
				if (selectedCrop != value) {
					selectedCrop = value;
					RaisePropertyChanged(() => SelectedCrop);
				}
			}
		}

		internal override void ResetData() {
			SelectedCrop = null;
			cropDictionary.Clear();
			onCropZoneCropChanged?.Invoke(selectedCrop);
			base.ResetData();
		}

		[CustomValidation(typeof(FieldSelectionViewModel), nameof(ValidateSelectedCropZones))]
		public override ObservableCollection<CropZoneSelectionViewModel> SelectedCropZones {
			get { return base.SelectedCropZones; }
			protected set { base.SelectedCropZones = value; }
		}

		#region Custom Validation
		public static ValidationResult ValidateSelectedCropZones(ObservableCollection<CropZoneSelectionViewModel> selectedCropZones, ValidationContext context) {
			ValidationResult result = null;

			var vm = (FieldSelectionViewModel)context.ObjectInstance;

			if (selectedCropZones == null || !selectedCropZones.Any()) {
				result = new ValidationResult(Strings.AtLeastOneCropZoneIsRequired_Text);
			} else if (selectedCropZones.Any(x => x.SelectedArea.Value == 0)) {
				result = new ValidationResult(Strings.AtLeastOneSelectedCropZoneHasNoArea_Text);
			} else if (vm.SelectedCrop == vm.undecidedCropId) {
				result = new ValidationResult(Strings.ValidationResult_AtLeastOneSelectedCropZoneHasCropOf_Text);
			} else if (vm.cropDictionary.Keys.Count > 1) {
				result = new ValidationResult(Strings.ValidationResult_AllSelectedCropZonesMustBeAssignedSameCrop_Text);
			}

			return result;
		}
		#endregion
	}
}