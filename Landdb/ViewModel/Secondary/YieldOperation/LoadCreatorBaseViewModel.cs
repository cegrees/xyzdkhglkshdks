﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure.Messages;
using System;
using System.Windows.Input;
using System.Windows.Threading;
using System.Linq;
using Landdb.Infrastructure;

namespace Landdb.ViewModel.Secondary.YieldOperation {
	public abstract class LoadCreatorBaseViewModel : ValidationViewModelBase {

		protected readonly IClientEndpoint clientEndpoint;
		protected readonly Dispatcher dispatcher;

		protected readonly CropYearId currentCropYearId;

		public LoadCreatorBaseViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, CropYearId currentCropYearId, YieldOperationId yieldOperationId, params DocumentDescriptor[] sourceDocuments) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			this.currentCropYearId = currentCropYearId;

			OperationId = yieldOperationId;

			LoadEntryPageModel = new LoadEntryViewModel(clientEndpoint, dispatcher, currentCropYearId);
			DocumentsModel = new LoadDocumentsViewModel(clientEndpoint, dispatcher, currentCropYearId, sourceDocuments);

			LoadEntryPageModel.ErrorsChanged += (sender, args) => { RaisePropertyChanged(() => IsCompletionAllowed); };

			CompleteCommand = new RelayCommand(SaveValidLoads);
			CancelCommand = new RelayCommand(onCancelYieldOperation);

			ReturnToMainViewCommand = new RelayCommand(onReturnToMainView);
			CreateNewYieldOperationCommand = new RelayCommand(onCreateNewYieldOperation);
		}

		public ICommand CompleteCommand { get; }
		public ICommand CancelCommand { get; }

		public ICommand ReturnToMainViewCommand { get; }
		public ICommand CreateNewYieldOperationCommand { get; }

		public YieldOperationId OperationId { get; private set; }

		public LoadEntryViewModel LoadEntryPageModel { get; }
		public LoadDocumentsViewModel DocumentsModel { get; }

		public abstract string TitleDisplay { get; }
		public abstract bool IsCompletionAllowed { get; }

		public int CropYear => currentCropYearId.Id;

		private int _selectedPageIndex = 0;
		public int SelectedPageIndex {
			get { return _selectedPageIndex; }
			set {
				_selectedPageIndex = value;
				RaisePropertyChanged(() => SelectedPageIndex);
			}
		}

		protected abstract CropId GetSelectedCropId();

		protected abstract void SaveValidLoads();
		protected abstract void ResetData();
		
		protected void ResetDataInternal() {
			OperationId = new YieldOperationId(currentCropYearId.DataSourceId, Guid.NewGuid());
			SelectedPageIndex = 0;

			LoadEntryPageModel.ResetData();
			DocumentsModel.ResetData();
		}

		protected void OnSelectedCropChanged(CropId newCropId) {
			LoadEntryPageModel.SelectedCropId = newCropId;
		}

		protected void ShowPostYieldOperationOverlay() {
			Messenger.Default.Send(new ShowOverlayMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Secondary.YieldOperation.PostYieldOperationOverlay), "post yield operation", this)
			});
		}

		private void onReturnToMainView() {
			Messenger.Default.Send(new YieldOperationAddedMessage() { LastYieldOperationAdded = OperationId });
			Messenger.Default.Send(new ShowMainViewMessage());
		}

		private void onCreateNewYieldOperation() {
			ResetData();
			Messenger.Default.Send(new HideOverlayMessage());
		}

		private void onCancelYieldOperation() {
			Messenger.Default.Send(new ShowMainViewMessage());
		}
	}
}