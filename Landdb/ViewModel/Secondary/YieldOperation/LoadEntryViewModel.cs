﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.CommoditySetting;
using Landdb.Domain.ReadModels.Contract;
using Landdb.Domain.ReadModels.Yield;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Resources;
using Landdb.ViewModel.Yield;
using NLog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Resources;
using Landdb.ViewModel.Resources.Equipment;

namespace Landdb.ViewModel.Secondary.YieldOperation {
	public class LoadEntryViewModel : ValidationViewModelBase {

		private readonly IClientEndpoint clientEndpoint;
		private readonly Dispatcher dispatcher;

		private readonly Logger log;

		private readonly AppliedLoadValuesView appliedLoadValuesView;
		private readonly ProductionContractListView productionContractListView;
		private readonly RentContractListView rentContractListView;

		public LoadEntryViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, CropYearId currentCropYearId) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			log = LogManager.GetCurrentClassLogger();

			CurrentCropYearId = currentCropYearId;

			DefaultFinalQuantityUnit = new CompositeUnit(AgC.UnitConversion.MassAndWeight.Pound.Self);

			DefaultStartDate = DateTime.Today + TimeSpan.FromHours(8);
			DefaultStartTime = DateTime.Today + TimeSpan.FromHours(8);

			// initialize lists
			Loads = new ObservableCollection<LoadViewModel>();
			YieldLocationList = new ObservableCollection<YieldLocationListItemViewModel>();
			TruckList = new ObservableCollection<EquipmentListItemViewModel>();
			PersonList = new ObservableCollection<PersonListItemViewModel>();
			ProductionContractList = new ObservableCollection<ProductionContractListItem>();
			RentContractList = new ObservableCollection<RentContractListItem>();
			CommodityUnitList = new ObservableCollection<CompositeUnit>();
			WeightUnitList = new ObservableCollection<CompositeUnit>();
			UnsavedCustomUnitList = new ObservableCollection<CompositeUnit>();
			CommodityDescriptionList = new ObservableCollection<string>();
			LoadPropertySetSelections = new ObservableCollection<LoadPropertySetSelectionViewModel>();

			CompositeUnitHelper.GetWeightPartUnits().OrderBy(x => x.AbbreviatedDisplay).ForEach(x => WeightUnitList.Add(x));

			clientEndpoint.GetMasterlistService()
				.GetLoadPropertyDefinitions()
				.LoadPropertySets
				.Where(x => x.IsActive)
				.OrderBy(x => x.Name)
				.ForEach(x => LoadPropertySetSelections.Add(new LoadPropertySetSelectionViewModel(x)));

			// set up projections
			appliedLoadValuesView = clientEndpoint.GetView<AppliedLoadValuesView>(currentCropYearId).GetValue(new AppliedLoadValuesView());
			productionContractListView = clientEndpoint.GetView<ProductionContractListView>(currentCropYearId).GetValue(new ProductionContractListView());
			rentContractListView = clientEndpoint.GetView<RentContractListView>(currentCropYearId).GetValue(new RentContractListView());

			// must be done after projections are initiated
			RefreshAllLists();

			AddLoadCommand = new RelayCommand(onAddLoad);
			DeleteLoadCommand = new RelayCommand(onRemoveLoad);
            ImportLoadCommand = new RelayCommand(onImportLoads);
			ShowMoistureEstimationOverlay = new RelayCommand(onShowMoistureEstimationGuide);
			ManageLoadPropertiesCommand = new RelayCommand(onManageLoadProperties);
            Print = new RelayCommand(onPrintLoadTicketReport);

			// set up validation
			Loads.CollectionChanged += (sender, args) => { ValidateProperty(() => Loads); };

			// add the first load
			AddLoadCommand.Execute(null);
		}

		public ICommand AddLoadCommand { get; }
		public ICommand DeleteLoadCommand { get; }
        public ICommand ImportLoadCommand { get; }
		public ICommand ShowMoistureEstimationOverlay { get; }
		public ICommand ManageLoadPropertiesCommand { get; }
        public ICommand Print { get; }

		#region Properties
		public CropYearId CurrentCropYearId { get; }

		[CustomValidation(typeof(LoadEntryViewModel), nameof(ValidateLoads))]
		public ObservableCollection<LoadViewModel> Loads { get; }

		public ObservableCollection<YieldLocationListItemViewModel> YieldLocationList { get; }
		public ObservableCollection<PersonListItemViewModel> PersonList { get; }
		public ObservableCollection<EquipmentListItemViewModel> TruckList { get; }
		public ObservableCollection<ProductionContractListItem> ProductionContractList { get; }
		public ObservableCollection<RentContractListItem> RentContractList { get; }
		public ObservableCollection<CompositeUnit> WeightUnitList { get; }
		public ObservableCollection<CompositeUnit> UnsavedCustomUnitList { get; }
		public ObservableCollection<LoadPropertySetSelectionViewModel> LoadPropertySetSelections { get; }

		// wtf is this voodoo?
		// answer: pull cropzoneids passively!
		public Func<List<CropZoneId>> GetSelectedCropZoneIds { get; set; } = () => new List<CropZoneId>();

		public ObservableCollection<CompositeUnit> CommodityUnitList { get; private set; }
		public ObservableCollection<string> CommodityDescriptionList { get; private set; }

		public decimal? StandardDryMoisturePercentage { get; private set; }
		public decimal? UserDefinedTargetMoisturePercentage { get; private set; }
		public decimal? UserDefinedShrinkPercentage { get; private set; }

		public YieldLocationId SelectedSourceLocationId { get; set; }

		public DateTime DefaultStartDate { get; set; }
		public DateTime DefaultStartTime { get; set; }
		public CompositeUnit DefaultFinalQuantityUnit { get; set; }

		public int LoadCount => ValidLoads.Count();
		public IEnumerable<LoadViewModel> ValidLoads => Loads.Where(x => x.IsValid);
		public decimal TotalQuantityValue => ValidLoads.Sum(x => CompositeUnitConverter.ConvertValue(x.QuantityInfo.FinalQuantity, x.QuantityInfo.SelectedFinalQuantityUnit, TotalQuantityUnit));
		public string TotalQuantityDisplayText => $"{TotalQuantityValue:N2} {TotalQuantityUnit.FullDisplay}";

		public CompositeUnit TotalQuantityUnit {
			get {
				if (ValidLoads.Any()) {
					return ValidLoads.First().QuantityInfo.SelectedFinalQuantityUnit;
				} else {
					return new CompositeUnit(AgC.UnitConversion.MassAndWeight.Pound.Self);
				}
			}
		}

		private CropId _selectedCropId;
		public CropId SelectedCropId {
			get { return _selectedCropId; }
			set {
				_selectedCropId = value;

				RefreshUnitList();
				RefreshPropertySetSelectionList();
				RefreshProductionContractList();
				RefreshRentContractList();
				RefreshCommoditySettingsAndDefaults();

				RaisePropertyChanged(() => SelectedCropId);
			}
		}

		private LoadViewModel _selectedLoad = null;
		public LoadViewModel SelectedLoad {
			get { return _selectedLoad; }
			set {
				// validate the current load before switching to the next
				if (_selectedLoad != null) {
					_selectedLoad.ValidateViewModel();
				}

				_selectedLoad = value;
				RaisePropertyChanged(() => SelectedLoad);
				RefreshTotalsAndCounts();
			}
		}
		#endregion

		internal void ResetData() {
			Loads.Clear();
			RefreshTotalsAndCounts();
			AddLoadCommand.Execute(null); // Add the first load
		}

		#region Refresh Methods
		internal void RefreshTotalsAndCounts() {
			RaisePropertyChanged(() => LoadCount);
			RaisePropertyChanged(() => TotalQuantityDisplayText);
		}

		internal void RefreshAllLists() {
			RefreshUnitList();
			RefreshPropertySetSelectionList();
			RefreshProductionContractList();
			RefreshRentContractList();
			RefreshDestinationLocationList();
			RefreshTruckList();
			RefreshDriverList();
			RefreshCommoditySettingsAndDefaults();
		}

		internal void RefreshCommoditySettingsAndDefaults() {
			if (SelectedCropId == null) { return; }

			var commoditySettingsKey = new Tuple<CropYearId, CropId>(CurrentCropYearId, SelectedCropId);
			var commoditySettingsView = clientEndpoint.GetView<CommoditySettingsDetailsView>(commoditySettingsKey).GetValue(() => null);
			if (commoditySettingsView != null) {
				UserDefinedShrinkPercentage = commoditySettingsView.EstimatedShrinkRatePercentage;
				UserDefinedTargetMoisturePercentage = commoditySettingsView.HandlingShrinkTargetMoisturePercentage;
			}

			var cropDefaults = clientEndpoint.GetMasterlistService().GetCropDefaultsForCrop(SelectedCropId);

			if (cropDefaults != null) {
				StandardDryMoisturePercentage = cropDefaults.DryMoisturePercentage;
			}
		}

		internal void RefreshUnitList() {
			if (SelectedCropId == null) { return; }

			var newUnitList = CompositeUnitHelper.GetWeightUnitsForCrop(clientEndpoint, dispatcher, CurrentCropYearId, SelectedCropId)
				.Union(UnsavedCustomUnitList)
				.Union(UnsavedCustomUnitList.Select(x => new CompositeUnit(x.PartUnitName)));

			CompositeUnit defaultUnit = null;

			var commoditySettingsKey = new Tuple<CropYearId, CropId>(CurrentCropYearId, SelectedCropId);
			var commoditySettingsView = clientEndpoint.GetView<CommoditySettingsDetailsView>(commoditySettingsKey).GetValue(() => null);
			if (commoditySettingsView != null) {
				defaultUnit = commoditySettingsView.LastUsedUnit;
			}

			if (appliedLoadValuesView.AppliedCropValues.ContainsKey(SelectedCropId.Id)) {
				var cropProperties = appliedLoadValuesView.AppliedCropValues[SelectedCropId.Id];

				// add the package units
				newUnitList = newUnitList.Union(cropProperties.AppliedUnits);

				// add the part units
				newUnitList = newUnitList.Union(cropProperties.AppliedUnits.Select(u => new CompositeUnit(u.PartUnitName)));

				// if there was no last used unit set from the commodity settings view, check the last applied
				if (defaultUnit == null) {
					defaultUnit = cropProperties.MostRecentlyAppliedFinalQuantityUnit;
				}

				// if there's still no default unit, this is the first time this crop has been used.
				// check for a default in the crop defaults.
				if (defaultUnit == null) {
					var cropDefaults = clientEndpoint.GetMasterlistService().GetCropDefaultsForCrop(SelectedCropId);
					if (cropDefaults?.PackageUnits != null && cropDefaults.PackageUnits.Any()) {
						var firstUnit = cropDefaults.PackageUnits.FirstOrDefault();
						defaultUnit = new CompositeUnit(firstUnit.PackageUnitName, firstUnit.ConversionFactor, firstUnit.PartUnitName);
					}
				}

				CommodityDescriptionList.Clear();

				if (cropProperties.AppliedCommodityDescriptions.Any()) {
					CommodityDescriptionList = new ObservableCollection<string>(cropProperties.AppliedCommodityDescriptions);
					RaisePropertyChanged(() => CommodityDescriptionList);
				}
			}

			CommodityUnitList = new ObservableCollection<CompositeUnit>(newUnitList.OrderBy(x => x.FullDisplay));
			RaisePropertyChanged(() => CommodityUnitList);

			// this has to be done after CommodityUnitList has been populated
			if (defaultUnit != null) {
				DefaultFinalQuantityUnit = defaultUnit;
				RaisePropertyChanged(() => DefaultFinalQuantityUnit);
			}

			RefreshTotalsAndCounts();
		}

		internal void RefreshPropertySetSelectionList() {
			if (SelectedCropId == null) { return; }

			if (appliedLoadValuesView.AppliedCropValues.ContainsKey(SelectedCropId.Id)) {
				var cropProperties = appliedLoadValuesView.AppliedCropValues[SelectedCropId.Id];
				var previouslyUsedSetIds = cropProperties.MostRecentlyAppliedPropertySetIds;

				LoadPropertySetSelections.Where(x => previouslyUsedSetIds.Contains(x.Id)).ForEach(propSet => {
					propSet.IsChecked = true;
				});

				RaisePropertyChanged(() => LoadPropertySetSelections);
			}
		}

		internal void RefreshDestinationLocationList() {
			YieldLocationList.Clear();

			var segmentTypes = clientEndpoint.GetMasterlistService()
				.GetYieldLocationSegmentTypes()
				.SegmentTypeList
				.Select(x => x.Name);

			var locList = YieldLocationListItemViewModel.GetFlattenedListItemsForCropYear(clientEndpoint, CurrentCropYearId, false);

			(from loc in locList
			 let hasSublocs = loc.SubLocationList.Any()
			 where !segmentTypes.Contains(loc.Descriptor) || hasSublocs
			 select loc).ForEach(x => YieldLocationList.Add(x));
		}

		internal void RefreshTruckList()
		{
			TruckList.Clear();
			EquipmentListItemViewModel.GetActiveListItemsForCropYearSortByType(clientEndpoint, CurrentCropYearId)
				.ForEach(model => TruckList.Add(model));
		}

		internal void RefreshDriverList() {
			PersonList.Clear();
			PersonListItemViewModel.GetListItemsForCropYear(clientEndpoint, CurrentCropYearId, true)
				.ForEach(x => PersonList.Add(x));
		}

		public void RefreshProductionContractList() {
			if (SelectedCropId == null) { return; }

			var newContractList = productionContractListView.ProductionContracts.Where(x => x.Crop == SelectedCropId);

			ProductionContractList.Clear();
			newContractList.OrderBy(x => x.Name).ForEach(x => ProductionContractList.Add(x));

			if (ProductionContractList.Any()) {
				ProductionContractList.Insert(0, new ProductionContractListItem() { Name = string.Empty });
			}

			RaisePropertyChanged(() => ProductionContractList);
		}

		public void RefreshRentContractList() {
			if (SelectedCropId == null) { return; }

			var newRentContractList = rentContractListView.RentContracts.Where(x => x.Crop == SelectedCropId);

			RentContractList.Clear();
			newRentContractList.OrderBy(x => x.Name).ForEach(x => RentContractList.Add(x));

			RentContractList.Insert(0, new RentContractListItem() { Name = Strings.GrowerShares_Text });
		}
		#endregion

		private void onAddLoad() {
			string commodityDescription = null;

			if (SelectedLoad != null) {
				DefaultFinalQuantityUnit = SelectedLoad.QuantityInfo.SelectedFinalQuantityUnit;
				commodityDescription = SelectedLoad.CommodityDescription;
			}

			var newLoad = new LoadViewModel(clientEndpoint, dispatcher, this) { CommodityDescription = commodityDescription };
			Loads.Add(newLoad);
			SelectedLoad = newLoad;

			RefreshTotalsAndCounts();
		}

		private void onRemoveLoad() {
			if (SelectedLoad != null) {
				var removedLoadIndex = Loads.IndexOf(SelectedLoad);
				Loads.Remove(SelectedLoad);

				var loadCount = Loads.Count();

				if (loadCount != 0) {
					if (removedLoadIndex == loadCount) {
						SelectedLoad = Loads.Last();
					} else if (removedLoadIndex == 0) {
						SelectedLoad = Loads.First();
					} else {
						SelectedLoad = Loads[removedLoadIndex];
					}
				}
			}
		}

        private void onImportLoads()
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.Filter = "Excel 2010|*.xlsx";
            openFileDialog1.FilterIndex = 1;

            openFileDialog1.Multiselect = false;
            
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string fileName = openFileDialog1.FileName;
                
                if(Loads.Count == 1 && Loads[0].QuantityInfo.DeliveredQuantity == 0)
                {
                    Loads.RemoveAt(0);
                }

                var vm = new ImportLoadsViewModel(fileName, this, onImportsReady);

                Messenger.Default.Send(new ShowOverlayMessage()
                {
                    ScreenDescriptor = new ScreenDescriptor(typeof(Views.Secondary.YieldOperation.ImportLoadsView), "ImportLoads", vm)
                });
                
            }
        }

        private void onImportsReady(List<ImportLoadItem> loads)
        {
            int i = 1;
            foreach (var load in loads)
            {
                var newLoad = new LoadViewModel(clientEndpoint, dispatcher, this);
                //var valueToConvert = ws1.Cell(i, 1).GetValue<string>();
                newLoad.StartDate = load.LoadDate;
                var loadName = load.LoadId;
                newLoad.LoadNumber = string.IsNullOrEmpty(loadName) ? newLoad.LoadNumber + "-" + (i).ToString() : loadName;
                //TO DO :: TRY TO MATCH THE DESTINATION
                var locationName = load.Destination;
                var yieldLocation = !string.IsNullOrEmpty(locationName) && YieldLocationList.Any(x => x.Name == locationName) ? YieldLocationList.FirstOrDefault(x => x.Name == locationName) : null;
                //truck and driver needs to be brought in....
                var truckName = load.Truck;
                var truck = !string.IsNullOrEmpty(truckName) && TruckList.Any(x => x.BasicInfoViewModel.Name == truckName) ? TruckList.FirstOrDefault(x => x.BasicInfoViewModel.Name == truckName) : null;
                newLoad.SelectedTruck = truck;
                var driverName = load.Driver;
                var driver = !string.IsNullOrEmpty(driverName) && PersonList.Any(x => x.Name == driverName) ? PersonList.FirstOrDefault(x => x.Name == driverName) : null;
                newLoad.SelectedDriver = driver;

                newLoad.SelectedDestinationLocation = yieldLocation;
                newLoad.QuantityInfo.TruckGrossWeight = load.Gross;
                newLoad.QuantityInfo.TruckTareWeight = load.Tare;
                newLoad.QuantityInfo.TruckNetWeight = load.Net;


                newLoad.QuantityInfo.InitialMoisturePercentage = load.LoadMoisture;

                newLoad.Notes = load.Notes;
                //TO DO :: GET LOAD PROPERTIES ADDED
                var holder = newLoad.LoadPropertySets;
                var lpDefinitions = clientEndpoint.GetMasterlistService().GetLoadPropertyDefinitions();
                var props = lpDefinitions;
                Loads.Add(newLoad);
                //newLoad.LoadPropertySets.

                if(newLoad.LoadPropertySets.Any(x => x.Id == new Guid("C9931DD8-F654-44F7-9F6C-E702FD9BDF1C")))
                {
                    var inHere = 0;
                    //Test Weight
                    var firstProperty = newLoad.LoadPropertySets.FirstOrDefault(x => x.Id == new Guid("C9931DD8-F654-44F7-9F6C-E702FD9BDF1C")).LoadProperties.Any() ? newLoad.LoadPropertySets.FirstOrDefault(x => x.Id == new Guid("C9931DD8-F654-44F7-9F6C-E702FD9BDF1C")).LoadProperties[0] : null;
                    if(firstProperty != null) { firstProperty.Value = load.TestWeight.ToString("N2"); }
                }
                if(newLoad.LoadPropertySets.Any(y => y.Id == new Guid("71200E91-1717-479A-BF20-F8E2C4901969")))
                {
                    //Foriegn Material
                    var firstProperty = newLoad.LoadPropertySets.FirstOrDefault(x => x.Id == new Guid("71200E91-1717-479A-BF20-F8E2C4901969")).LoadProperties.Any() ? newLoad.LoadPropertySets.FirstOrDefault(x => x.Id == new Guid("71200E91-1717-479A-BF20-F8E2C4901969")).LoadProperties[0] : null;
                    if (firstProperty != null) { firstProperty.Value = load.FMPercent.HasValue ? load.FMPercent.Value.ToString("N2") : string.Empty; }
                }

                i++;
            }
        }

        private void onManageLoadProperties() {
			var vm = new Yield.Popups.ManageAvailableLoadPropertiesViewModel(clientEndpoint, dispatcher, SelectedCropId, LoadPropertySetSelections, changeVm => {
				if (changeVm != null) {
					LoadPropertySetSelections.Clear();
					var newSelections = changeVm.AvailablePropertySets.Union(changeVm.AdditionalPropertySets);
					newSelections.OrderBy(x => x.Name).ForEach(x => LoadPropertySetSelections.Add(x));

					// not exactly sure why i'm having to raise a property changed on an observable, but the properychanged listener
					// method on the loadviewmodel doesn't pick it up otherwise.
					RaisePropertyChanged(() => LoadPropertySetSelections);
				}

				Messenger.Default.Send(new HideOverlayMessage());
			});

			Messenger.Default.Send(new ShowOverlayMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Yield.Popups.ManageAvailableLoadPropertiesView), Strings.ScreenDescriptor_ManageLoadProperties_Text.ToLower(), vm)
			});
		}

		private void onShowMoistureEstimationGuide() {
			var vm = new MoistureEstimationOverlayViewModel();
			Messenger.Default.Send(new ShowOverlayMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Secondary.YieldOperation.MoistureEstimationOverlay), Strings.ScreenDescriptor_MoistureCalculation_Text.ToLower(), vm)
			});
		}

        private void onPrintLoadTicketReport()
        {
            if (SelectedLoad == null) { return; }
            var reportGenerator = new Reports.Yield.Generators.LoadTicketReportGenerator(SelectedLoad, GetSelectedCropZoneIds(), SelectedCropId, clientEndpoint);
            var vm = new Reports.Yield.SingleYieldReportViewModel(clientEndpoint, dispatcher, reportGenerator);
            Messenger.Default.Send(new ShowOverlayMessage()
            {
                ScreenDescriptor = new ScreenDescriptor(typeof(Views.Secondary.Reports.Work_Order.SingleWorkOrderView), Strings.ScreenDescriptor_ViewReport_Text.ToLower(), vm)
            });
        }

        public static ValidationResult ValidateLoads(ObservableCollection<LoadViewModel> loads, ValidationContext context) {
			ValidationResult result = null;

			if (loads == null || !loads.Any()) {
				result = new ValidationResult(Strings.ValidationResult_AtLeastOneLoadRequired_Text);
			} else if (loads.Any(x => !x.IsValid)) {
				result = new ValidationResult(Strings.ValidationResult_OneOrMoreLoadsHaveErrors_Text);
			}

			return result;
		}
	}

    public class ImportLoadItem : ViewModelBase
    {
        public Guid ID { get; set; }
        private bool _included { get; set; }
        public bool IncludedLoad
        {
            get { return _included; }
            set
            {
                _included = value;
                RaisePropertyChanged(() => IncludedLoad); }
        }
        public DateTime LoadDate { get; set; }
        public string Field { get; set; }
        public string LoadId { get; set; }

        private string _destination { get; set; }
        public string Destination { get { return _destination; } set { _destination = value; RaisePropertyChanged(() => Destination); } }

        private string _truck { get; set; }
        public string Truck { get { return _truck; } set { _truck = value; RaisePropertyChanged(() => Truck); } }

        private string _driver { get; set; }
        public string Driver { get { return _driver; } set { _driver = value; RaisePropertyChanged(() => Driver); } }

        public decimal Gross { get; set; }
        public decimal Tare { get; set; }
        public decimal Net { get; set; }
        public decimal LoadMoisture { get; set; }
        public decimal TestWeight { get; set; }
        public decimal? FMPercent { get; set; }
        public string Notes { get; set; }
    }
}