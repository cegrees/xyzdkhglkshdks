﻿using Landdb.Client.Infrastructure;
using System;
using System.Windows.Threading;
using System.Linq;
using Landdb.Resources;

namespace Landdb.ViewModel.Secondary.YieldOperation {
	public class HarvestLoadCreatorViewModel : LoadCreatorBaseViewModel {

		public HarvestLoadCreatorViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, CropYearId currentCropYearId, YieldOperationId yieldOperationId, params DocumentDescriptor[] sourceDocuments)
			: base(clientEndpoint, dispatcher, currentCropYearId, yieldOperationId, sourceDocuments) {

			FieldsPageModel = new FieldSelectionViewModel(clientEndpoint, dispatcher, currentCropYearId.Id, OnSelectedCropChanged);
            
			FieldsPageModel.ErrorsChanged += (sender, args) => { RaisePropertyChanged(() => IsCompletionAllowed); };

			LoadEntryPageModel.GetSelectedCropZoneIds += () => {
				return FieldsPageModel.SelectedCropZones.Select(x => x.Id).ToList();
			};
		}

        public FieldSelectionViewModel FieldsPageModel { get; }

		public override string TitleDisplay => Strings.CreateHarvestLoads_Text.ToLower();

		protected override CropId GetSelectedCropId() => FieldsPageModel.SelectedCrop;

		public override bool IsCompletionAllowed => !(FieldsPageModel.HasErrors || LoadEntryPageModel.HasErrors);

		protected override void SaveValidLoads() {
			foreach (var load in LoadEntryPageModel.ValidLoads) {
				var loadId = new LoadId(OperationId.DataSourceId, Guid.NewGuid());
				var truckId = load.SelectedTruck?.EquipmentId;
				var driverId = load.SelectedDriver?.Id;

				var quantityInfoVm = load.QuantityInfo;

				// zero out the target moisture percentage if it isn't used
				var targetMoisturePercentage = quantityInfoVm.StandardMoistureShrinkQuantity != 0 ? quantityInfoVm.StandardDryMoisturePercentage : 0;

				IDomainCommand cmd = null;

				if (load.SelectedDestinationLocation.YieldLocationType.Equals(YieldLocationTypes.Storage)) {
					cmd = new CreateFieldToStorageLoad(
						loadId,
						clientEndpoint.GenerateNewMetadata(),
						currentCropYearId.Id,
						OperationId,
						load.LoadNumber,
						load.StartDateTime.ToUniversalTime(),
						load.Notes,
						FieldsPageModel.GetCropZoneAreas(),
						load.DestinationSubLocationInfo.GetSelectedLocationIds(),
						FieldsPageModel.SelectedCrop,
						load.CommodityDescription,
						truckId,
						driverId,
						quantityInfoVm.GetQuantityInfoV2ValueObject(),
						load.GetReportedLoadPropertySets(),
						DocumentsModel.GetDocuments()
					);
				} else if (load.SelectedDestinationLocation.YieldLocationType.Equals(YieldLocationTypes.Sale)) {
					cmd = new CreateFieldToSaleLoad(
						loadId,
						clientEndpoint.GenerateNewMetadata(),
						currentCropYearId.Id,
						OperationId,
						load.LoadNumber,
						load.StartDateTime.ToUniversalTime(),
						load.Notes,
						FieldsPageModel.GetCropZoneAreas(),
						load.SelectedDestinationLocation.Id,
						FieldsPageModel.SelectedCrop,
						load.CommodityDescription,
						load.SplitLoadPriceInfo.GetRentContractId(),
						truckId,
						driverId,
						quantityInfoVm.GetQuantityInfoV2ValueObject(),
						load.SplitLoadPriceInfo.GetGrowerPriceInfoValueObject(),
						load.SplitLoadPriceInfo.GetLandOwnerPriceInfoValueObject(),
						load.GetReportedLoadPropertySets(),
						DocumentsModel.GetDocuments()
					);
				}

				if (cmd != null) {
					clientEndpoint.SendOne(cmd);
				} else {
					throw new Exception($"Failed to generate a create command for yield location type: {load.SelectedDestinationLocation.YieldLocationType}");
				}
			}

			ShowPostYieldOperationOverlay();
		}

		protected override void ResetData() {
			FieldsPageModel.ResetData();
			ResetDataInternal();
		}
	}
}