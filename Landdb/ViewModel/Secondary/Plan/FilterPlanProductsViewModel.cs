﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels;
using Landdb.Domain.ReadModels.Inventory;
using NLog;
using Landdb.Client.Models;
using Landdb.Domain.ReadModels.Product;
using Landdb.Infrastructure;
using Landdb.Domain.ReadModels.Tags;
using Landdb.Domain.ReadModels.Plan;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure.Messages;
using System.Windows.Input;
using Landdb.ViewModel.Production;

namespace Landdb.ViewModel.Secondary.Plan {
    public class FilterPlanProductsViewModel : ViewModelBase
    {
        readonly Logger log = LogManager.GetCurrentClassLogger();
        Dictionary<string, List<string>> tempTimingEventTags = new Dictionary<string, List<string>>();
        Dictionary<string, List<string>> availableTimingEventTags = new Dictionary<string, List<string>>();
        Dictionary<string, List<string>> mergedTimingEventTags;
        private ObservableCollection<FilterPlanProductDetailsViewModel> copyProducts;
        private ObservableCollection<FilterPlanProductDetailsViewModel> plannedProducts;
        private bool selectAllProducts = false;
        TimingEvent timingEvent;
        List<TimingEvent> timingEvents;
        string timingEventTag;
        string selectedTimingEventTag;
        List<string> timingEventTags;
        int cropYear = 0;
        Action<ObservableCollection<ProductDetails>> onNodeCreated;
        Action onNodeIgnored;

        public FilterPlanProductsViewModel(ObservableCollection<FilterPlanProductDetailsViewModel> copyProducts, List<TimingEvent> timingEvents, Dictionary<string, List<string>> availableTimingEventTags, Action<ObservableCollection<ProductDetails>> onNodeCreated, Action onNodeIgnored)
        {
            this.plannedProducts = copyProducts;
            this.copyProducts = copyProducts;
            this.timingEvents = timingEvents;
            this.availableTimingEventTags = availableTimingEventTags;
            this.onNodeCreated = onNodeCreated;
            this.onNodeIgnored = onNodeIgnored;
            this.cropYear = ApplicationEnvironment.CurrentCropYear;

            ContinueCommand = new RelayCommand(Continue);
            CancelCommand = new RelayCommand(Cancel);

        }

        public ICommand CancelCommand { get; private set; }
        public ICommand ContinueCommand { get; private set; }

        public FilterPlanProductDetailsViewModel SelectedProduct { get; set; }
        public ObservableCollection<FilterPlanProductDetailsViewModel> CopyProducts {
            get { return copyProducts; }
            set {
                copyProducts = value;
                RaisePropertyChanged("CopyProducts");
            }
        }

        public ObservableCollection<FilterPlanProductDetailsViewModel> PlannedProducts {
            get { return plannedProducts; }
            set {
                plannedProducts = value;
                RaisePropertyChanged("PlannedProducts");
            }
        }

        public int CropYear {
            get { return cropYear; }
            set {
                cropYear = value;
                RaisePropertyChanged("CropYear");
            }
        }

        public bool SelectAllProducts {
            get {
                return selectAllProducts;
            }
            set {
                selectAllProducts = value;
                if (CopyProducts != null && CopyProducts.Count > 0) {
                    for (int i = 0; i < CopyProducts.Count; i++) {
                        CopyProducts[i].IsProductSelectedForCopy = selectAllProducts;
                    }
                }
                RaisePropertyChanged("CopyProducts");
                RaisePropertyChanged("SelectAllProducts");
            }
        }


        //private Dictionary<string, List<string>> ProcessTemporaryTimingEventTags() {
        //    if (tempTimingEventTags == null) { tempTimingEventTags = new Dictionary<string, List<string>>(); }
        //    foreach (var pp in copyProducts) {
        //        if (!string.IsNullOrEmpty(pp.TimingEventTag) && pp.TimingEvent != null) {
        //            if (!tempTimingEventTags.ContainsKey(pp.ProductDetails.TimingEvent)) {
        //                tempTimingEventTags.Add(pp.ProductDetails.TimingEvent, new List<string>());
        //            }
        //            if (!tempTimingEventTags[pp.ProductDetails.TimingEvent].Contains(pp.TimingEventTag)) {
        //                tempTimingEventTags[pp.ProductDetails.TimingEvent].Add(pp.TimingEventTag);
        //            }
        //        }
                
        //    }
        //    foreach (var pp in copyProducts) {
        //        if (!string.IsNullOrEmpty(pp.TimingEventTag) && pp.TimingEvent != null) {
        //            if (!availableTimingEventTags.ContainsKey(pp.ProductDetails.TimingEvent)) {
        //                availableTimingEventTags.Add(pp.ProductDetails.TimingEvent, new List<string>());
        //            }
        //            if (!availableTimingEventTags[pp.ProductDetails.TimingEvent].Contains(pp.TimingEventTag)) {
        //                availableTimingEventTags[pp.ProductDetails.TimingEvent].Add(pp.TimingEventTag);
        //            }
        //        }

        //    }
        //    for (int i = 0; i < copyProducts.Count; i++) {
        //        //copyProducts[i].AvailableTimingEventTags = availableTimingEventTags;
        //    }

        //    return tempTimingEventTags;
        //}


        //async void BuildProductList()
        //{
        //    var watch = Stopwatch.StartNew();

        //    await Task.Run(() =>
        //    {
        //        Dictionary<string, List<string>> availableTimingEventTags = new Dictionary<string, List<string>>();
        //        var plantagviewMaybe = clientEndpoint.GetView<PlanProductTagProjectionView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear));
        //        if (plantagviewMaybe.HasValue && plantagviewMaybe.Value.TagInfo != null) {
        //            foreach (var v in plantagviewMaybe.Value.TagInfo) {
        //                if(string.IsNullOrEmpty(v.TimingEvent)) { continue; }
        //                if (!availableTimingEventTags.ContainsKey(v.TimingEvent)) {
        //                    availableTimingEventTags.Add(v.TimingEvent, new List<string>());
        //                }
        //                foreach (var x in v.Tags) {
        //                    if (availableTimingEventTags.ContainsKey(v.TimingEvent)) {
        //                        if (!string.IsNullOrEmpty(x.Key) && !availableTimingEventTags[v.TimingEvent].Contains(x.Key)) {
        //                            availableTimingEventTags[v.TimingEvent].Add(x.Key);
        //                        }
        //                    }
        //                }
        //            }
        //        }

        //        foreach (var v in copyProducts) {
        //            if (v.TimingEvent == null) { continue; }
        //            if (!availableTimingEventTags.ContainsKey(v.ProductDetails.TimingEvent)) {
        //                availableTimingEventTags.Add(v.ProductDetails.TimingEvent, new List<string>());
        //            }
        //            if (availableTimingEventTags.ContainsKey(v.ProductDetails.TimingEvent)) {
        //                if (!string.IsNullOrEmpty(v.TimingEventTag) && !availableTimingEventTags[v.TimingEvent].Contains(v.TimingEventTag)) {
        //                    availableTimingEventTags[v.ProductDetails.TimingEvent].Add(v.TimingEventTag);
        //                }
        //            }
        //        }
        //        //for (int i = 0; i < copyProducts.Count; i++) {
        //        //    copyProducts[i].AvailableTimingEventTags = availableTimingEventTags;
        //        //}
        //    });
        //    watch.Stop();

        //}

        public List<TimingEvent> TimingEvents {
            get {
                return timingEvents;
            }
            set {
                timingEvents = value;
                RaisePropertyChanged("TimingEvents");
            }
        }

        public TimingEvent TimingEvent {
            get { return timingEvent; }
            set {
                timingEvent = value;
                SelectAppropriateTags();
                RaisePropertyChanged("TimingEvent");
                FilterPlannedProductList();
            }
        }

        public Dictionary<string, List<string>> AvailableTimingEventTags {
            get {
                return availableTimingEventTags;
            }
            set {
                availableTimingEventTags = value;
                SelectAppropriateTags();
                
            }
        }

        public Dictionary<string, List<string>> TempTimingEventTags {
            get {
                return tempTimingEventTags;
            }
            set {
                tempTimingEventTags = value;
                SelectAppropriateTags();
                RaisePropertyChanged("TempTimingEventTags");
                FilterPlannedProductList();
            }
        }

        public List<string> TimingEventTags {
            get {
                if (timingEventTags == null) {
                    return new List<string>();
                }
                return timingEventTags.OrderBy(q => q).ToList();
            }
            set {
                timingEventTags = value;
                RaisePropertyChanged("TimingEventTags");
            }
        }

        public string TimingEventTag {
            get { return timingEventTag; }
            set {
                if (timingEventTag == value) { return; }
                timingEventTag = value;
                RaisePropertyChanged("TimingEventTag");
                FilterPlannedProductList();
            }
        }

        public string SelectedTimingEventTag {
            get { return selectedTimingEventTag; }
            set {
                if (selectedTimingEventTag == value) { return; }
                selectedTimingEventTag = value;
                if (!string.IsNullOrEmpty(selectedTimingEventTag)) {
                    TimingEventTag = selectedTimingEventTag;
                }
                RaisePropertyChanged("SelectedTimingEventTag");
            }
        }

        private void SelectAppropriateTags() {
            if (timingEvent != null && !string.IsNullOrEmpty(timingEvent.Name)) {
                mergedTimingEventTags = availableTimingEventTags;
                if (tempTimingEventTags != null && tempTimingEventTags.Count > 0) {
                    mergedTimingEventTags = availableTimingEventTags.Concat(tempTimingEventTags).GroupBy(d => d.Key).ToDictionary(d => d.Key, d => d.First().Value);
                }

                if (mergedTimingEventTags != null && mergedTimingEventTags.ContainsKey(timingEvent.Name)) {
                    List<string> listTimingEventTags = new List<string>();
                    listTimingEventTags.Add(string.Empty);
                    foreach (var tet in mergedTimingEventTags[timingEvent.Name]) {
                        listTimingEventTags.Add(tet);
                    }
                    TimingEventTags = listTimingEventTags;
                }
                else {
                    TimingEventTags = new List<string>();
                }
            }
        }

        private void FilterPlannedProductList() {
            if (timingEvent == null) {
                selectAllProducts = false;
                CopyProducts = plannedProducts;
                RaisePropertyChanged("CopyProducts");
                RaisePropertyChanged("SelectAllProducts");
                return;
            }
            if (timingEvent != null && string.IsNullOrEmpty(timingEvent.Name)) {
                selectAllProducts = false;
                CopyProducts = plannedProducts;
                RaisePropertyChanged("CopyProducts");
                RaisePropertyChanged("SelectAllProducts");
                return;
            }
            ObservableCollection<FilterPlanProductDetailsViewModel> filteredProducts = new ObservableCollection<FilterPlanProductDetailsViewModel>();
            foreach (var p in plannedProducts) {
                if (p.TimingEvent != null && timingEvent != null && p.TimingEvent == timingEvent.Name) {
                    if (timingEventTag == null || string.IsNullOrEmpty(timingEventTag)) {
                        filteredProducts.Add(p);
                        continue;
                    }
                    else if (p.TimingEventTag != null && timingEventTag != null && p.TimingEventTag == timingEventTag) {
                        filteredProducts.Add(p);
                    }
                }
            }
            selectAllProducts = false;
            CopyProducts = filteredProducts;
            RaisePropertyChanged("CopyProducts");
            RaisePropertyChanged("SelectAllProducts");
            return;
        }

        void Continue() {
            ObservableCollection<ProductDetails> filteredProducts = new ObservableCollection<ProductDetails>();
            foreach (var pl in copyProducts) {
                if (pl.IsProductSelectedForCopy) {
                    filteredProducts.Add(pl.ProductDetails);
                }
            }
            if (filteredProducts.Count <= 0) { return; }
            onNodeCreated(filteredProducts); // notify the parent
            Messenger.Default.Send<HideOverlayMessage>(new HideOverlayMessage());
        }
        void Cancel() {
            copyProducts.Clear();
            Messenger.Default.Send<HideOverlayMessage>(new HideOverlayMessage());
        }
    }
}
