﻿using AgC.UnitConversion;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Converters.Reports;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Reports;
using Landdb.ViewModel.Secondary.Reports.Document.Generators;
using Microsoft.Win32;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using Telerik.Reporting;
using Telerik.Reporting.Processing;

namespace Landdb.ViewModel.Secondary.Plan {
    public class PlanMergeViewModel : ViewModelBase
    {
        //ReportTemplateViewModel template;
        //ReportTemplateViewModelToFile templateService = new ReportTemplateViewModelToFile();
        IDocumentReportGenerator selectedReportGenerator;
        //PlanMergeQueryCriteria criteria;
        ReportSource source;
        bool viewerSelected = false;
        PlanMergeTargetSelectionViewModel infoPageModel;
        PlanMergeSelectionViewModel selectionPageModel;
        FilterPlanMergeProductsViewModel productPageModel;
        IClientEndpoint endpoint;
        int cropYear;
        bool isallowedtosave = true;
        bool showsavebutton = false;


        public PlanMergeViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, int cropYear)
        {
            this.endpoint = clientEndpoint;
            this.cropYear = cropYear;
            CancelCommand = new RelayCommand(OnCancelReport);
            CompleteCommand = new RelayCommand(SaveProducts);
            //PDFExport = new RelayCommand(PDF);

            InfoPageModel = new PlanMergeTargetSelectionViewModel(clientEndpoint, cropYear);
            SelectionPageModel = new PlanMergeSelectionViewModel(clientEndpoint, cropYear);
            ProductPageModel = new FilterPlanMergeProductsViewModel(this, clientEndpoint, cropYear);

            //ObservableCollection<FilterPlanProductDetailsViewModel> copyProducts = new ObservableCollection<FilterPlanProductDetailsViewModel>();
            //List<TimingEvent> timingEvents2 = clientEndpoint.GetMasterlistService().GetTimingEventList().OrderBy(x => x.Order).ToList();
            //List<TimingEvent> timingEvents = new List<TimingEvent>();
            //timingEvents.Add(new TimingEvent());
            //timingEvents.AddRange(timingEvents2);

            //Dictionary<string, List<string>> availableTimingEventTags = new Dictionary<string, List<string>>();

            //var vm = new FilterPlanProductsViewModel(copyProducts, timingEvents, availableTimingEventTags, OnNewItemCreated, OnNewItemIgnored);
            //Messenger.Default.Send<ShowOverlayMessage>(new ShowOverlayMessage() {
            //    ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Production.Popups.FilterPlanProductsView", "filter plans products", vm)
            //});

            //ReportGenerators = new ObservableCollection<IDocumentReportGenerator>();
            //AddDocumentReportGenerators(clientEndpoint, cropYear);
            SelectedPageIndex = 0;
        }

        public ICommand CancelCommand { get; private set; }
        //public RelayCommand PDFExport { get; set; }
        public ICommand CompleteCommand { get; set; }
        public string TemplateName { get; set; }
        public int SelectedPageIndex { get; set; }
        public int CropYear { get { return ApplicationEnvironment.CurrentCropYear; } }

        public ReportSource ReportSource
        {
            get
            {
                return source;
            }
            set
            {
                source = value;
                RaisePropertyChanged("ReportSource");
            }
        }

        public ObservableCollection<IDocumentReportGenerator> ReportGenerators { get; private set; }
        public IDocumentReportGenerator SelectedReportGenerator
        {
            get { return selectedReportGenerator; }
            set
            {
                selectedReportGenerator = value;
                UpdateReport();
                RaisePropertyChanged("SelectedReportGenerator");
            }
        }

        public bool OnViewerSelected 
        {
            get { return viewerSelected; }
            set { viewerSelected = value; if (viewerSelected) { SelectedReportGenerator = null; } }
        }

        public bool IsAllowedToSave {
            get { return isallowedtosave; }
            set {
                isallowedtosave = value;
                RaisePropertyChanged("IsAllowedToSave");
            }
        }

        public bool ShowSaveButton {
            get { return showsavebutton; }
            set {
                showsavebutton = value;
                if(!isallowedtosave) {
                    showsavebutton = false;
                }
                RaisePropertyChanged("ShowSaveButton");
            }
        }

        //PlanMergeQueryCriteria BuildCriteria() {
        //    PlanMergeQueryCriteria criteria = new PlanMergeQueryCriteria();

        //    criteria.StartDatetime = InfoPageModel.ReportStartDate.AddHours(InfoPageModel.ReportStartDate.Hour * -1);
        //    criteria.EndDateTime = InfoPageModel.ReportEndDate.AddDays(1);
        //    criteria.IncludedDocuments = (from d in SelectionPageModel.ConcurrentDocuments
        //                                  where d.Checked == true
        //                                  select d.Descriptor).ToList();
        //    criteria.SpanishTranslation = InfoPageModel.SpanishTranslated;
        //    return criteria;
        //}

        public PlanMergeTargetSelectionViewModel InfoPageModel { get { return infoPageModel; } set { infoPageModel = value; SelectedReportGenerator = null; } }
        public PlanMergeSelectionViewModel SelectionPageModel { get { return selectionPageModel; } set { selectionPageModel = value; SelectedReportGenerator = null; } }
        public FilterPlanMergeProductsViewModel ProductPageModel { get { return productPageModel; } set { productPageModel = value; SelectedReportGenerator = null; } }

        public void UpdateReport()
        {
            if (SelectedReportGenerator != null)
            {
                //criteria = BuildCriteria();

                //ReportSource = SelectedReportGenerator.Generate(criteria, this);
                RaisePropertyChanged("ReportSource");
            }
        }

        void AddDocumentReportGenerators(IClientEndpoint clientEndpoint, int cropYear)
        {
            //ReportGenerators.Add(new MultiPrintDocumentGenerator(clientEndpoint, cropYear));
            //ReportGenerators.Add(new ByFieldDocumentGenerator(clientEndpoint, cropYear));
            //ReportGenerators.Add(new BreakOutDocumentGenerator(clientEndpoint, cropYear));
            //ReportGenerators.Add(new ProductSummaryReportGenerator(clientEndpoint, cropYear));
        }

        void OnCancelReport()
        {
            Messenger.Default.Send<ShowMainViewMessage>(new ShowMainViewMessage());
        }

        void SaveProducts()
        {
            if (IsAllowedToSave && ShowSaveButton) {
                ShowSaveButton = false;
                IsAllowedToSave = false;

                var plist = (from p in ProductPageModel.CopyProducts
                             where p.IsProductSelectedForCopy == true
                             select p).ToList();

                foreach (FilterPlanProductDetailsViewModel planproduct in plist) {
                    AddProductToPlan addCommand = new AddProductToPlan(InfoPageModel.SelectedTarget.Id, endpoint.GenerateNewMetadata(), Guid.NewGuid(), ToPlanProductEntry(planproduct));
                    endpoint.SendOne(addCommand);
                }
                OnCancelReport();
            }
        }

        internal PlanProductEntry ToPlanProductEntry(FilterPlanProductDetailsViewModel planproduct) {
            // TODO: This needs to represent the actual rate unit the user has chosen.
            //var pMaybe = endpoint.GetView<MiniProduct>(planproduct.ProductDetails.ID);
            //if (pMaybe.HasValue) {
            //    var psu = UnitFactory.GetPackageSafeUnit(pMaybe.Value.StdPackageUnit, pMaybe.Value.StdUnit, pMaybe.Value.StdFactor, pMaybe.Value.StdPackageUnit);

            //    PriceStrategy strategy1 = PriceStrategy.Specific;
            //    decimal price = SelectedPriceItem.Price;
            //    string priceUnit = SelectedPriceItem.PriceUnit ?? SelectedProduct.Product.StdPackageUnit;
            //    var areaUnit = ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit;
            //    return new PlanProductEntry(planproduct.ProductDetails.ID, Guid.NewGuid(), RateValue, SelectedRateUnitHelper.Unit.Name, (decimal)TotalProduct.Value, TotalProduct.Unit.Name, areaUnit, strategy1, price, priceUnit, (decimal)ProductAreaPercent, ApplicationCount, TimingEvent != null ? TimingEvent.Name : string.Empty, TargetDate, TimingEventTag);

            //}
            PriceStrategy strategy = PriceStrategy.Specific;
            return new PlanProductEntry(planproduct.ProductDetails.ID, Guid.NewGuid(), (decimal)planproduct.ProductDetails.RateMeasure.Value, planproduct.ProductDetails.RateMeasure.Unit.Name, (decimal)planproduct.ProductDetails.TotalProduct.Value, planproduct.ProductDetails.TotalProduct.Unit.Name, planproduct.ProductDetails.AreaUnit.Name, strategy, planproduct.ProductDetails.CostPerUnitValue, planproduct.ProductDetails.CostUnit.Name, (decimal)planproduct.ProductDetails.ProductAreaPercent, planproduct.ProductDetails.ApplicationCount, planproduct.ProductDetails.TimingEvent, planproduct.ProductDetails.TargetDate, planproduct.ProductDetails.TimingEventTag);
        }
    }
}
