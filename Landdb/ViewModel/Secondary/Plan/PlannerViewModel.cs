﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Domain.ReadModels.Plan;
using Landdb.Domain.ReadModels.Product;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ValueObjects;
using Landdb.ViewModel.Production;
using Landdb.ViewModel.Production.Popups;
using Landdb.ViewModel.Shared;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Secondary.Plan {
	public class PlannerViewModel : ViewModelBase {

		private readonly IClientEndpoint clientEndpoint;
		private readonly Dispatcher dispatcher;

		private readonly CropYearId currentCropYearId;

		private readonly Messenger localMessenger;
		private readonly PlansPageViewModel viewModel;

		private CropPlanId planId;
		private int selectedPageIndex;

		public PlannerViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, CropPlanId planId, int currentCropYear, PlansPageViewModel viewModel) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			this.planId = planId;
			this.viewModel = viewModel;

			currentCropYearId = new CropYearId(planId.DataSourceId, currentCropYear);

			CancelCommand = new RelayCommand(OnCancelPlan);
			CompleteCommand = new RelayCommand(OnCompletePlan);

			localMessenger = new Messenger();
			FieldsPageModel = new Application.FieldSelectionViewModel(clientEndpoint, dispatcher, currentCropYearId, null, false);
			InfoPageModel = new InfoViewModel(clientEndpoint, this, localMessenger);
			ProductsPageModel = new ProductSelectionViewModel(clientEndpoint, dispatcher, currentCropYearId, InfoPageModel, FieldsPageModel, localMessenger);

			FieldsPageModel.ErrorsChanged += (sender, args) => { RaisePropertyChanged(() => IsCompletionAllowed); };
			InfoPageModel.ErrorsChanged += (sender, args) => { RaisePropertyChanged(() => IsCompletionAllowed); };
			ProductsPageModel.ErrorsChanged += (sender, args) => { RaisePropertyChanged(() => IsCompletionAllowed); };

			ReturnToMainViewCommand = new RelayCommand(ReturnToMainView);
			CreateNewPlanCommand = new RelayCommand(CreateNewPlan);
			CopyPlan = new RelayCommand(CreateNewPlanFromThisPlan);
		}

		public ICommand CancelCommand { get; }
		public ICommand CompleteCommand { get; }

		public ICommand ReturnToMainViewCommand { get; }
		public ICommand CreateNewPlanCommand { get; }
		public ICommand CopyPlan { get; }

		public InfoViewModel InfoPageModel { get; }
		public Application.FieldSelectionViewModel FieldsPageModel { get; }
		public ProductSelectionViewModel ProductsPageModel { get; }

		public string TitleDisplay {
			get { return Strings.CreatePlan_Text.ToLower(); }
		}

		public bool IsCompletionAllowed {
			get { return !(FieldsPageModel.HasErrors || InfoPageModel.HasErrors || ProductsPageModel.HasErrors); }
		}

		public int SelectedPageIndex {
			get { return selectedPageIndex; }
			set {
				selectedPageIndex = value;
				RaisePropertyChanged(() => SelectedPageIndex);
			}
		}

		private void ResetData() {
			planId = new CropPlanId(planId.DataSourceId, Guid.NewGuid());
			SelectedPageIndex = 0;
			InfoPageModel.ResetData();
			FieldsPageModel.ResetData();
			ProductsPageModel.ResetData();
		}

		public int CropYear {
			get { return currentCropYearId.Id; }
		}

		void OnCompletePlan() {
			var cropId = InfoPageModel.Crop != null ? InfoPageModel.Crop.Id : new Guid();
			var planName = InfoPageModel.Name != null ? InfoPageModel.Name : string.Empty;
			var notes = InfoPageModel.Notes != null ? InfoPageModel.Notes : string.Empty;

			CreatePlan infoCommand = new CreatePlan(planId, clientEndpoint.GenerateNewMetadata(), currentCropYearId.Id, planName, cropId, InfoPageModel.EstimatedArea, FieldsPageModel.GetCropZoneAreas(), ProductsPageModel.GetPlannedProducts(), InfoPageModel.Rent, InfoPageModel.Equipment, InfoPageModel.Insurance, InfoPageModel.Labor, InfoPageModel.Repairs, InfoPageModel.Taxes, InfoPageModel.Management, InfoPageModel.YieldPerAcre, InfoPageModel.PricePerUnit, InfoPageModel.FsaPayment, InfoPageModel.Share, notes, ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit);
			clientEndpoint.SendOne(infoCommand);

			Messenger.Default.Send(new ShowOverlayMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Secondary.Plan.PostPlanOverlay), "postPlan", this)
			});
		}

		void OnCancelPlan() {
			// TODO: Show UI to confirm cancel. Be nice to only do this if anything's actually been entered. Dirty tracking?
			Messenger.Default.Send(new ShowMainViewMessage());
		}

		void ReturnToMainView() {
			Messenger.Default.Send(new PlansAddedMessage() { LastPlanAdded = planId });
			Messenger.Default.Send(new ShowMainViewMessage());
		}

		void CreateNewPlan() {
			ResetData();
			Messenger.Default.Send(new HideOverlayMessage());
		}

		void CreateNewPlanFromThisPlan() {
			SetData();
			Messenger.Default.Send(new HideOverlayMessage());
		}

		void SetData() {
			planId = new CropPlanId(planId.DataSourceId, Guid.NewGuid());
			InfoPageModel.Name = string.Empty;
			SelectedPageIndex = 0;
		}

		void OnAreaChanged() {
			InfoPageModel.SelectedArea = FieldsPageModel.TotalArea;
		}

		public void SetNewPlanWithThisData(PlanView view) {
			planId = new CropPlanId(planId.DataSourceId, Guid.NewGuid());
			InfoPageModel.Name = string.Empty;
			InfoPageModel.AssociatedPlanView = view;
			FieldsPageModel.AssociatedPlanView = view;
			ProductsPageModel.AssociatedPlanView = view;

			SelectedPageIndex = 0;
		}

		public void SetNewPlanWithAppliedProducts() {
			planId = new CropPlanId(planId.DataSourceId, Guid.NewGuid());
			SelectedPageIndex = 0;
			InfoPageModel.ResetData();
			InfoPageModel.Share = 1.0;
			FieldsPageModel.ResetData();
			ProductsPageModel.ResetData();
			ChooseCropZone();
		}

		ChoosePlanAppliedCropzonesViewModel selectAppliedCropZones;
		void ChooseCropZone() {
			IsOptionsMapPopupOpen = false;
			selectAppliedCropZones = new ChoosePlanAppliedCropzonesViewModel(clientEndpoint, dispatcher, ApplicationEnvironment.CurrentCropYear, planId, this);
			Messenger.Default.Send(new ShowOverlayMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Production.Popups.ChoosePlanAppliedCropzonesView), "selectCropzones", selectAppliedCropZones)
			});
		}

		public void SetSumarryItems(ObservableCollection<Landdb.ViewModel.Fields.FieldDetails.ApplicationLineItemViewModel> selectedPlanProducts) {
			if (selectedPlanProducts == null) { return; }
			if (selectedPlanProducts.Count == 0) { return; }
			Dictionary<string, List<string>> availableTimingEventTagz = GetAvailableTimingEventTags(clientEndpoint);
			var i = clientEndpoint.GetView<InventoryListView>(currentCropYearId);
			InventoryListView inventory = i.HasValue ? i.Value : new InventoryListView();
			ObservableCollection<PlannedProductViewModel> plannedProducts = new ObservableCollection<PlannedProductViewModel>();
			foreach (var prod in selectedPlanProducts) {
				var mp = clientEndpoint.GetMasterlistService().GetProduct(prod.ProductId);
				var mur = clientEndpoint.GetView<ProductUsageView>(currentCropYearId).GetValue(new ProductUsageView());
				var cont = mur.GetProductGuids().Contains(prod.ProductId.Id);
				ProductListItemDetails prodVM;
				prodVM = new ProductListItemDetails(mp, prod.ProductId.DataSourceId, cont ? Strings.UsedThisYear_Text : Strings.Unused_Text, cont ? (short)0 : (short)1);
				var planProd = new PlannedProductViewModel(clientEndpoint, ProductsPageModel, FieldsPageModel, localMessenger, inventory, InfoPageModel, availableTimingEventTagz);
				planProd.SelectedProduct = prodVM;
				planProd.RateValue = prod.RateValue;
				planProd.SelectedRateUnitHelper = planProd.AllowedUnits.FirstOrDefault(x => x.Unit.Name == prod.RateUnit);
				var appDetails = clientEndpoint.GetView<ApplicationView>(prod.ApplicationId).Value;
				var appTimingEvent = string.IsNullOrEmpty(appDetails.TimingEvent) ? string.Empty : appDetails.TimingEvent;
				var selectedTimingEvent = planProd.TimingEvents.FirstOrDefault(x => x.Name == appTimingEvent);
				planProd.TimingEvent = selectedTimingEvent;
				var value = prod.CostPerUnitValue != null && prod.CostPerUnitValue > 0 ? prod.CostPerUnitValue : 0.0;
				if (value > 0) {
					var priceItem = new PriceItem((decimal)value, prod.CostPerUnit, true);
					planProd.PriceItems.Add(priceItem);
					planProd.SelectedPriceItem = priceItem;
				}
				planProd.ProductAreaPercent = 1;
				plannedProducts.Add(planProd);
			}
			ProductsPageModel.SetPlannedProducts(plannedProducts);
			SelectedPageIndex = 0;
			Messenger.Default.Send(new ChangeScreenDescriptorMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Secondary.Plan.PlannerView), Strings.CreateCropPlan_Text.ToLower(), this)
			});
		}


		bool isOptionsMapPopupOpen = false;
		public bool IsOptionsMapPopupOpen {
			get { return isOptionsMapPopupOpen; }
			set {
				isOptionsMapPopupOpen = value;
				RaisePropertyChanged(() => IsOptionsMapPopupOpen);
			}
		}

		#region Commented out?
		//IList<MapItem> cropzoneLayerItems;
		//public IList<MapItem> CropZoneLayer {
		//    get { return cropzoneLayerItems; }
		//    set {
		//        cropzoneLayerItems = value;
		//        RaisePropertyChanged(() => CropZoneLayer);
		//    }
		//}

		//public ObservableCollection<PlannedProductViewModel> SummaryItems { get; private set; }

		//void RefreshSummaryItems(IList<MapItem> czmaps) {
		//    //IList<MapItem> czmaps = new List<MapItem>();
		//    var data = clientEndpoint.GetView<Landdb.Domain.ReadModels.Application.CropZoneApplicationDataView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear)).GetValue(new Landdb.Domain.ReadModels.Application.CropZoneApplicationDataView());
		//    var inventory = clientEndpoint.GetView<Landdb.Domain.ReadModels.Inventory.InventoryListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear)).GetValue(new Landdb.Domain.ReadModels.Inventory.InventoryListView());

		//    ApplicationConditionsViewModel ConditionsModel = new ApplicationConditionsViewModel(clientEndpoint, dispatcher, ApplicationEnvironment.CurrentCropYear);
		//    //InfoPageModel = new InfoViewModel(clientEndpoint, dispatcher, ConditionsModel, currentCropYear, applicationId.DataSourceId);
		//    //FieldsPageModel = new FieldSelectionViewModel(clientEndpoint, dispatcher, ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear, ConditionsModel);
		//    //ProductsPageModel = new ProductSelectionViewModel(clientEndpoint, dispatcher, applicationId.DataSourceId, currentCropYear, FieldsPageModel);
		//    //DocumentsModel = new DocumentListPageViewModel(clientEndpoint, dispatcher, currentCropYear, sourceDocuments);

		//    IEnumerable<Landdb.Domain.ReadModels.Application.CropZoneApplicationDataItem> filtered = null;
		//    CropZoneId czid = new CropZoneId(ApplicationEnvironment.CurrentDataSourceId, czmaps[0].CropZoneId.Value);
		//    filtered = data.Items.Where(x => x.CropZoneId == czid).OrderByDescending(x => x.StartDate);

		//    if (filtered != null) {
		//        filtered.ForEach(x => {
		//            var mp = clientEndpoint.GetMasterlistService().GetProduct(x.ProductId);
		//            var mur = clientEndpoint.GetView<Landdb.Domain.ReadModels.Product.ProductUsageView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new Landdb.Domain.ReadModels.Product.ProductUsageView());
		//            var cont = productGuids.Contains(x.ProductId.Id);
		//            Landdb.Client.Models.ProductListItemDetails prodVM;
		//            prodVM = new Landdb.Client.Models.ProductListItemDetails(mp, x.ProductId.DataSourceId, cont ? "Used this year" : "Unused", cont ? (short)0 : (short)1);

		//            var planProd = new PlannedProductViewModel(clientEndpoint, this, FieldsPageModel, dispatcher, inventory, InfoPageModel, AvailableTimingEventTags);
		//            planProd.SelectedProduct = prodVM;
		//            planProd.RateValue = prod.RateValue;
		//            planProd.SelectedRateUnitHelper = planProd.AllowedUnits.FirstOrDefault(x => x.Unit.Name == prod.RateUnit);
		//            planProd.SelectedPriceText = prod.SpecificCostPerUnit.HasValue ? prod.SpecificCostPerUnit.Value.ToString() : string.Empty;
		//            planProd.ApplicationCount = prod.ApplicationCount.HasValue ? prod.ApplicationCount.Value : 1;
		//            planProd.ProductAreaPercent = prod.PercentApplied;
		//            planProd.TimingEvent = planProd.TimingEvents.FirstOrDefault(t => t.Name == prod.TimingEvent);
		//            planProd.TimingEventTag = !string.IsNullOrEmpty(prod.TimingEventTag) ? prod.TimingEventTag : null;
		//            PlannedProducts.Add(planProd);






		//            var appSummaryItem = new Landdb.ViewModel.Fields.FieldDetails.ApplicationSummaryItem(clientEndpoint, null, inventory, x);
		//            //SummaryItems.Add(appSummaryItem);
		//            var p = clientEndpoint.GetMasterlistService().GetProduct(x.ProductId);
		//            if (p != null) {
		//            }
		//        });
		//    }











		//    ObservableCollection<PlannedProductViewModel> PlannedProducts = new ObservableCollection<PlannedProductViewModel>();
		//    PlannedProducts.Clear();
		//    foreach (var prod in associatedPlanView.Products) {

		//        var mp = clientEndpoint.GetMasterlistService().GetProduct(prod.Id);
		//        var mur = clientEndpoint.GetView<ProductUsageView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new ProductUsageView());
		//        var cont = productGuids.Contains(prod.Id.Id);
		//        ProductListItemDetails prodVM;
		//        prodVM = new ProductListItemDetails(mp, prod.Id.DataSourceId, cont ? "Used this year" : "Unused", cont ? (short)0 : (short)1);

		//        var planProd = new PlannedProductViewModel(clientEndpoint, this, fieldSelection, localMessenger, inventory, infoVM, AvailableTimingEventTags);
		//        planProd.SelectedProduct = prodVM;
		//        planProd.RateValue = prod.RateValue;
		//        planProd.SelectedRateUnitHelper = planProd.AllowedUnits.FirstOrDefault(x => x.Unit.Name == prod.RateUnit);
		//        planProd.SelectedPriceText = prod.SpecificCostPerUnit.HasValue ? prod.SpecificCostPerUnit.Value.ToString() : string.Empty;
		//        planProd.ApplicationCount = prod.ApplicationCount.HasValue ? prod.ApplicationCount.Value : 1;
		//        planProd.ProductAreaPercent = prod.PercentApplied;
		//        planProd.TimingEvent = planProd.TimingEvents.FirstOrDefault(t => t.Name == prod.TimingEvent);
		//        planProd.TimingEventTag = !string.IsNullOrEmpty(prod.TimingEventTag) ? prod.TimingEventTag : null;
		//        PlannedProducts.Add(planProd);
		//    }





		//    Varieties.Clear();
		//    FertilizerInformation = null;

		//    var data = clientEndpoint.GetView<CropZoneApplicationDataView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear)).GetValue(new CropZoneApplicationDataView());
		//    var inventory = clientEndpoint.GetView<InventoryListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear)).GetValue(new InventoryListView());

		//    IEnumerable<CropZoneApplicationDataItem> filtered = null;
		//    double area = 0;

		//    if (treeModel is CropZoneTreeItemViewModel) {
		//        filtered = data.Items.Where(x => x.CropZoneId == (CropZoneId)((CropZoneTreeItemViewModel)treeModel).Id).OrderByDescending(x => x.StartDate);
		//        area = ((CropZoneTreeItemViewModel)treeModel).Area.Value;
		//    }

		//    if (treeModel is FieldTreeItemViewModel) {
		//        filtered = data.Items.Where(x => x.FieldId == (FieldId)((FieldTreeItemViewModel)treeModel).Id).OrderByDescending(x => x.StartDate);
		//    }

		//    CostSummary = new CostSummaryViewModel(area);
		//    IInventoryCalculationService inventoryService = new InventoryCalculationService();

		//    if (filtered != null) {
		//        filtered.ForEach(x => {
		//            var appSummaryItem = new ApplicationSummaryItem(clientEndpoint, inventoryService, inventory, x);
		//            SummaryItems.Add(appSummaryItem);
		//            var p = clientEndpoint.GetMasterlistService().GetProduct(x.ProductId);
		//            if (p != null) {
		//                if (p.ProductType == GlobalStrings.ProductType_Seed) {
		//                    Varieties.Add(new VarietyItemViewModel(string.Format("{0} ({1})", p.Name, p.Manufacturer), x.StartDate));
		//                    CostSummary.SeedTotalCost += appSummaryItem.TotalCost.ToString() == "NaN" ? 0 : (decimal)appSummaryItem.TotalCost;
		//                }
		//                if (p.ProductType == GlobalStrings.ProductType_Fertilizer) {
		//                    CostSummary.FertilizerTotalCost += appSummaryItem.TotalCost.ToString() == "NaN" ? 0 : (decimal)appSummaryItem.TotalCost;
		//                }
		//                if (p.ProductType == GlobalStrings.ProductType_CropProtection) {
		//                    CostSummary.CropProtectionTotalCost += appSummaryItem.TotalCost.ToString() == "NaN" ? 0 : (decimal)appSummaryItem.TotalCost;
		//                }
		//                if (p.ProductType == GlobalStrings.ProductType_Service) {
		//                    CostSummary.ServicesTotalCost += appSummaryItem.TotalCost.ToString() == "NaN" ? 0 : (decimal)appSummaryItem.TotalCost;
		//                }
		//            }
		//        });
		//        totalProductionCostByAverage = CostSummary.TotalCost;
		//        productionCostPerAreaByAverage = CostSummary.CostPerArea;

		//        if (SelectedPricingViewStrategy == PricingViewStrategy.InvoiceMatching) {
		//            // yes, this is a bit silly. Potential refactor idea: calculate both cost summaries up front, and switch them out based on selected view strategy.
		//            RecalculateCostSummaryByInvoice();
		//        }
		//        else if (SelectedPricingViewStrategy == PricingViewStrategy.SpecificCost) {
		//            RecalculateCostSummaryBySpecific();
		//        }

		//        HasInvoices = SummaryItems.Any(x => x.HasInvoiceCost);
		//        HasSpecificCost = SummaryItems.Any(x => x.HasSpecificCost);
		//        RaisePropertyChanged(() => HasInvoices);
		//        RaisePropertyChanged(() => HasSpecificCost);
		//        RaisePropertyChanged(() => ShowDeviationLine);
		//    }
		//    RaisePropertyChanged(() => CostSummary);

		//    if (treeModel is CropZoneTreeItemViewModel) {
		//        var czDetails = clientEndpoint.GetView<Landdb.Domain.ReadModels.Tree.CropZoneDetailsView>(((CropZoneTreeItemViewModel)treeModel).Id).GetValue(() => null);
		//        CropId cropId = null;
		//        if (czDetails != null) {
		//            cropId = czDetails.CropId;
		//        }

		//        var czModel = (CropZoneTreeItemViewModel)treeModel;

		//        // TODO: need cancellability
		//        var analysis = await CalculateFertilizerUsage(filtered, ((CropZoneTreeItemViewModel)treeModel).Area, cropId);
		//        FertilizerInformation = analysis.FertilizerFormulation;
		//        analysis.ActiveIngredients.Sort(delegate(ActiveIngredientLineItemViewModel g1, ActiveIngredientLineItemViewModel g2) {
		//            return g1.PercentRemaining.CompareTo(g2.PercentRemaining);
		//        });

		//        ActiveIngredients = analysis.ActiveIngredients;
		//    }

		//    RefreshYieldSummary(treeModel);

		//    CostSummary.ProductionRevenue = CostSummary.YieldSummaryItems.Sum(x => x.GrowerRevenue) - CostSummary.TotalCost;
		//} 
		#endregion

		private Dictionary<string, List<string>> GetAvailableTimingEventTags(IClientEndpoint clientEndpoint) {
			Dictionary<string, List<string>> availableTimingEventTagz = new Dictionary<string, List<string>>();
			var plantagviewMaybe = clientEndpoint.GetView<ApplicationTagProjectionView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear));
			if (plantagviewMaybe.HasValue && plantagviewMaybe.Value.TagInfo != null) {
				foreach (var v in plantagviewMaybe.Value.TagInfo) {
					if (string.IsNullOrEmpty(v.TimingEvent)) { continue; }
					if (!availableTimingEventTagz.ContainsKey(v.TimingEvent)) {
						availableTimingEventTagz.Add(v.TimingEvent, new List<string>());
					}

					foreach (var x in v.Tags) {
						if (availableTimingEventTagz.ContainsKey(v.TimingEvent)) {
							if (!string.IsNullOrEmpty(x.Key) && !availableTimingEventTagz[v.TimingEvent].Contains(x.Key)) {
								availableTimingEventTagz[v.TimingEvent].Add(x.Key);
							}
						}
					}
				}
			}

			return availableTimingEventTagz;
		}
	}
}