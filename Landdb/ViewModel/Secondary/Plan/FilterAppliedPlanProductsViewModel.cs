﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels;
using Landdb.Domain.ReadModels.Inventory;
using NLog;
using Landdb.Client.Models;
using Landdb.Domain.ReadModels.Product;
using Landdb.Infrastructure;
using Landdb.Domain.ReadModels.Tags;
using Landdb.Domain.ReadModels.Plan;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure.Messages;
using System.Windows.Input;
using Landdb.ViewModel.Production;
using Landdb.Domain.ReadModels.Tree;
using AgC.UnitConversion;
using Landdb.Resources;

namespace Landdb.ViewModel.Secondary.Plan {
    public class FilterAppliedPlanProductsViewModel : ViewModelBase
    {
        readonly Logger log = LogManager.GetCurrentClassLogger();
        private ObservableCollection<FilterPlanProductDetailsViewModel> copyProducts;
        private ObservableCollection<FilterPlanProductDetailsViewModel> plannedProducts;
        private bool selectAllProducts = false;
        List<int> cropyearlist;
        int selectedcropyear = 0;
        int cropYear = 0;
        string planname = string.Empty;
        string cropzoneName = string.Empty;
        CropId cropId;
        string cropName = string.Empty;
        Action<ObservableCollection<ProductDetails>> onNodeCreated;
        Action onNodeIgnored;

        public FilterAppliedPlanProductsViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, ObservableCollection<FilterPlanProductDetailsViewModel> copyProducts, CropId cropId, string cropzoneName, Action<ObservableCollection<ProductDetails>> onNodeCreated, Action onNodeIgnored)
        {
            this.plannedProducts = copyProducts;
            this.copyProducts = copyProducts;
            this.cropzoneName = cropzoneName;
            this.cropId = cropId;
            this.onNodeCreated = onNodeCreated;
            this.onNodeIgnored = onNodeIgnored;
            this.selectedcropyear = ApplicationEnvironment.CurrentCropYear;
            this.cropYear = ApplicationEnvironment.CurrentCropYear;
            if (clientEndpoint != null) {
                this.cropName = clientEndpoint.GetMasterlistService().GetCropDisplay(cropId);
            }
            else {
                this.cropName = Strings.UnknownCrop_Text;
            }

            this.planname = string.Format(Strings.Format_ProductsAppliedOn_Text, ApplicationEnvironment.CurrentCropYear, cropName, cropzoneName);

            var years = clientEndpoint.GetView<FieldTreeView>(new DataSourceId(ApplicationEnvironment.CurrentDataSourceId));
            cropyearlist = years.Value.CropYearList.Keys.Where(x => x >= ApplicationEnvironment.CurrentCropYear).ToList();

            ContinueCommand = new RelayCommand(Continue);
            CancelCommand = new RelayCommand(Cancel);

        }

        public ICommand CancelCommand { get; private set; }
        public ICommand ContinueCommand { get; private set; }

        public FilterPlanProductDetailsViewModel SelectedProduct { get; set; }
        public ObservableCollection<FilterPlanProductDetailsViewModel> CopyProducts {
            get { return copyProducts; }
            set {
                copyProducts = value;
                RaisePropertyChanged("CopyProducts");
            }
        }

        public ObservableCollection<FilterPlanProductDetailsViewModel> PlannedProducts {
            get { return plannedProducts; }
            set {
                plannedProducts = value;
                RaisePropertyChanged("PlannedProducts");
            }
        }

        public bool SelectAllProducts {
            get {
                return selectAllProducts;
            }
            set {
                selectAllProducts = value;
                if (CopyProducts != null && CopyProducts.Count > 0) {
                    for (int i = 0; i < CopyProducts.Count; i++) {
                        CopyProducts[i].IsProductSelectedForCopy = selectAllProducts;
                    }
                }
                RaisePropertyChanged("CopyProducts");
                RaisePropertyChanged("SelectAllProducts");
            }
        }

        public int CropYear {
            get { return cropYear; }
            set {
                cropYear = value;
                RaisePropertyChanged("CropYear");
            }
        }

        public List<int> CropYearList {
            get {
                if (cropyearlist == null) {
                    return new List<int>();
                }
                return cropyearlist.OrderByDescending(q => q).ToList();
            }
            set {
                cropyearlist = value;
                RaisePropertyChanged("CropYearList");
            }
        }

        public int SelectedCropYear {
            get { return selectedcropyear; }
            set {
                if (selectedcropyear == value) { return; }
                selectedcropyear = value;
                RaisePropertyChanged("SelectedCropYear");
            }
        }

        public string PlanName {
            get { return planname; }
            set {
                if (planname == value) { return; }
                planname = value;
                RaisePropertyChanged("PlanName");
            }
        }

        public PlanProductEntry[] GetAppliedProducts() {
            List<PlanProductEntry> appliedProducts = new List<PlanProductEntry>();
            foreach (var ap in CopyProducts) {
                var areaUnit = ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit;
                PlanProductEntry ppe = new PlanProductEntry(ap.ProductDetails.ID, Guid.NewGuid(), (decimal)ap.ProductDetails.RateMeasure.Value, ap.ProductDetails.RateMeasure.Unit.Name, (decimal)ap.TotalProduct.Value, ap.TotalProduct.Unit.Name, ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit, PriceStrategy.Specific, 0m, string.Empty, 0m, 0, string.Empty, DateTime.Now, string.Empty);
                appliedProducts.Add(ppe);
            }
            return appliedProducts.ToArray();
        }

        void Continue() {
            ObservableCollection<ProductDetails> filteredProducts = new ObservableCollection<ProductDetails>();
            foreach (var pl in copyProducts) {
                if (pl.IsProductSelectedForCopy) {
                    filteredProducts.Add(pl.ProductDetails);
                }
            }
            if (filteredProducts.Count <= 0) { return; }
            onNodeCreated(filteredProducts); // notify the parent
            Messenger.Default.Send<HideOverlayMessage>(new HideOverlayMessage());

            //var cropId = InfoPageModel.Crop != null ? InfoPageModel.Crop.Id : new Guid();
            //var planName = InfoPageModel.Name != null ? InfoPageModel.Name : string.Empty;
            //var notes = InfoPageModel.Notes != null ? InfoPageModel.Notes : string.Empty;

            ////CreatePlan infoCommand = new CreatePlan(new CropPlanId(ApplicationEnvironment.CurrentDataSourceId, new Guid()), new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), cropYear, planname, cropId, 0, new List<CropZoneArea>().ToArray(), GetAppliedProducts(), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, notes, ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit);
            ////clientEndpoint.SendOne(infoCommand);

            //ShowOverlayMessage msg = new ShowOverlayMessage() { ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Secondary.Plan.PostPlanOverlay", "postPlan", this) };
            //Messenger.Default.Send<ShowOverlayMessage>(msg);

        }
        void Cancel() {
            copyProducts.Clear();
            Messenger.Default.Send<HideOverlayMessage>(new HideOverlayMessage());
        }
    }
}
