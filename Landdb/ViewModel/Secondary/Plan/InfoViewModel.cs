﻿using AgC.UnitConversion;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Plan;
using Landdb.ViewModel.Secondary.Application;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Landdb.Infrastructure;
using Landdb.Resources;

namespace Landdb.ViewModel.Secondary.Plan {
	public class InfoViewModel : ValidationViewModelBase {
		readonly IClientEndpoint clientEndpoint;

		double yieldperacre;
		double rent;
		double equipment;
		double insurance;
		double labor;
		double repairs;
		double taxes;
		double management;
		double priceperunit;
		double fsapayment;
		double share;
		double estArea;
		double revenue;
		double grossRevenue;
		Measure selectedArea;
		PlanView associatedPlanView;
		Messenger localMessenger;

		public InfoViewModel(IClientEndpoint endPoint, PlannerViewModel vm, Messenger localMessenger) {
			this.clientEndpoint = endPoint;
			this.localMessenger = localMessenger;
			this.FieldsPageModel = vm.FieldsPageModel;
			this.EstimatedArea = 1;
			this.Share = 100;

			FieldsPageModel.PropertyChanged += fieldVM_PropertyChanged;

			localMessenger.Register<PropertyChangedMessage<decimal>>(this, GetInputCost);

			ValidateViewModel();
		}

		public PlanView AssociatedPlanView {
			get { return associatedPlanView; }
			set {
				associatedPlanView = value;

				if (associatedPlanView != null) {
					this.EstimatedArea = associatedPlanView.CropZones.Sum(x => x.Area.Value);
					this.YieldPerAcre = associatedPlanView.YieldPerArea;
					Rent = associatedPlanView.LandRent;
					Equipment = associatedPlanView.Equipment;
					Insurance = associatedPlanView.Insurance;
					Labor = associatedPlanView.Labor;
					Repairs = associatedPlanView.Repairs;
					Taxes = associatedPlanView.Taxes;
					Management = associatedPlanView.ReturnToMgt;
					PricePerUnit = associatedPlanView.PricePerUnit;
					FsaPayment = associatedPlanView.FSAPayment;
					Share = associatedPlanView.CropSharePercent;
					Notes = string.Empty;
					Crop = Crops.FirstOrDefault(i => i.Id == associatedPlanView.CropId);
					YieldPerAcre = associatedPlanView.YieldPerArea;
				}

				RaisePropertyChanged(() => AssociatedPlanView);
			}
		}

		public FieldSelectionViewModel FieldsPageModel { get; private set; }

		public double YieldPerAcre { get { return yieldperacre; } set { yieldperacre = value; SetRevenue(); RaisePropertyChanged(() => YieldPerAcre); } }
		public double Rent { get { return rent; } set { rent = value; SetRevenue(); RaisePropertyChanged(() => Rent); } }
		public double Equipment { get { return equipment; } set { equipment = value; SetRevenue(); RaisePropertyChanged(() => Equipment); } }
		public double Insurance { get { return insurance; } set { insurance = value; SetRevenue(); RaisePropertyChanged(() => Insurance); } }
		public double Labor { get { return labor; } set { labor = value; SetRevenue(); RaisePropertyChanged(() => Labor); } }
		public double Repairs { get { return repairs; } set { repairs = value; SetRevenue(); RaisePropertyChanged(() => Repairs); } }
		public double Taxes { get { return taxes; } set { taxes = value; SetRevenue(); RaisePropertyChanged(() => Taxes); } }
		public double Management { get { return management; } set { management = value; SetRevenue(); RaisePropertyChanged(() => Management); } }
		public double PricePerUnit { get { return priceperunit; } set { priceperunit = value; SetRevenue(); RaisePropertyChanged(() => PricePerUnit); } }
		public double FsaPayment { get { return fsapayment; } set { fsapayment = value; SetRevenue(); RaisePropertyChanged(() => FsaPayment); } }

		public string Notes { get; set; }
		public MiniCrop Crop { get; set; }

		public double GrossRevenueArea { get; set; }
		public double Budget { get; set; }
		public double BudgetArea { get; set; }
		public double RevenueArea { get; set; }
		public double InputCost { get; set; }
		public double InputCostArea { get; set; }

		private string name;

        //http://www.devcurry.com/2013/02/aspnet-mvc-using-resource-files-to.html
        [Required(AllowEmptyStrings = false, ErrorMessageResourceName = "NameIsRequired_Text", ErrorMessageResourceType = typeof(Strings))]
        public string Name {
			get { return name; }
			set {
				name = value;
				ValidateAndRaisePropertyChanged(() => Name);
			}
		}

		public double Share {
			get { return share; }
			set {
				if (value > 1.0) {
					share = value / 100;
				} else {
					share = value;
				}

				SetRevenue();
			}
		}

		public double EstimatedArea {
			get { return estArea; }
			set {
				var oldValue = estArea;
				estArea = value;
				estArea = Math.Round(estArea, 2);
				SetRevenue();
				RaisePropertyChanged(() => EstimatedArea);
				localMessenger.Send<PropertyChangedMessage<double>>(new PropertyChangedMessage<double>(oldValue, estArea, "EstimatedArea"));
			}
		}

		public double NetRevenue {
			get { return revenue; }
			set {
				revenue = value;
				RaisePropertyChanged(() => NetRevenue);
				RaisePropertyChanged(() => NetRevenuePerAreaDisplay);
			}
		}

		public double GrossRevenue {
			get { return grossRevenue; }
			set {
				grossRevenue = value;
				RaisePropertyChanged(() => GrossRevenue);
			}
		}

		public IEnumerable<MiniCrop> Crops {
			get { return clientEndpoint.GetMasterlistService().GetCropList(); }
		}

		public System.Windows.Media.SolidColorBrush CostPerAreaBrush {
			get { return new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(0xff, 0x00, 0x6f, 0xca)); }
		}

		public string NetRevenuePerAreaDisplay {
			get { return string.Format(Strings.Per_Format_Text, (NetRevenue / EstimatedArea).ToString("C"), UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).AbbreviatedDisplay); }
		}

		public Measure SelectedArea {
			get { return selectedArea; }
			set {
				selectedArea = value;
				RaisePropertyChanged(() => SelectedArea);
			}
		}

		internal void ResetData() {
			YieldPerAcre = 0.0;
			Rent = 0.0;
			Management = 0.0;
			Notes = null;
			Name = null;
			Insurance = 0.0;
			Equipment = 0.0;
			Labor = 0.0;
			Repairs = 0.0;
			Taxes = 0.0;
			PricePerUnit = 0.0;
			FsaPayment = 0.0;
			EstimatedArea = 0.0;
			Crop = null;
			Share = 0.0;
			Name = string.Empty;

			SetRevenue();
		}

		private void SetSelectedArea(Measure measure) {
			SelectedArea = measure;
			EstimatedArea = SelectedArea.Value;
		}

		void SetRevenue() {
			//calculate value based off of inputs.
			var debts = (Equipment + Insurance + Labor + Repairs + Taxes + Management + Rent) * EstimatedArea;
			var totalYield = YieldPerAcre * EstimatedArea;
			var totalProfitFromYield = totalYield * PricePerUnit;
			var totalAfterShare = totalProfitFromYield * Share;
			var profits = totalAfterShare + (EstimatedArea * FsaPayment);
			NetRevenue = profits - (debts + InputCost);
			GrossRevenue = profits;
			GrossRevenueArea = profits / EstimatedArea;
			Budget = debts;
			BudgetArea = (debts) / EstimatedArea;
			RevenueArea = NetRevenue / EstimatedArea;
			InputCostArea = InputCost / EstimatedArea;

			RaisePropertyChanged(() => GrossRevenue);
			RaisePropertyChanged(() => GrossRevenueArea);
			RaisePropertyChanged(() => Budget);
			RaisePropertyChanged(() => BudgetArea);
			RaisePropertyChanged(() => RevenueArea);
			RaisePropertyChanged(() => InputCost);
			RaisePropertyChanged(() => InputCostArea);
		}

		private void GetInputCost(PropertyChangedMessage<decimal> inputCost) {
			if (inputCost.PropertyName == "TotalCost") {
				InputCost = (double)inputCost.NewValue;
				SetRevenue();
			}
		}

		void fieldVM_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e) {
			if (e.PropertyName == "TotalArea") {
				if (FieldsPageModel.SelectedCropZones.Count() == 1) {
					var selectedCZCropId = FieldsPageModel.SelectedCropZones[0].Crop;
					Crop = Crops.FirstOrDefault(x => x.Id == selectedCZCropId.Id);
					RaisePropertyChanged(() => Crop);
				}

				SetSelectedArea(FieldsPageModel.TotalArea);
			}
		}
	}
}