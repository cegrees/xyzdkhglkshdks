﻿using AgC.UnitConversion;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Controls;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Domain.ReadModels.Plan;
using Landdb.Domain.ReadModels.Product;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ValueObjects;
using Landdb.ViewModel.Secondary.Application;
using Landdb.ViewModel.Secondary.UserCreatedProduct;
using Landdb.ViewModel.Shared;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Landdb.Resources;
using Landdb.Client.Localization;

namespace Landdb.ViewModel.Secondary.Plan {
	public class PlannedProductViewModel : ValidationViewModelBase {

		private readonly ProductSelectionViewModel parent;
		private readonly FieldSelectionViewModel fieldSelectionModel;
		private readonly InfoViewModel infoViewModel;

		Guid trackingId;
		ProductListItemDetails selectedProduct;
		decimal rateValue;
		UnitPerAreaHelper rateUnitHelper;
		decimal costValuePerUnit; // TODO: Make this a Currency class when we have a good one built.
		IUnit costUnit;
		decimal totalCost = 0; // TODO: Make this a Currency class when we have a good one built.
		Measure totalProduct;
		decimal productAreaPercent = 1;
		Measure productArea;
		InventoryListView inventory;
		PriceItem selectedPriceItem;
		string selectedPriceText;
		InventoryListItem currentInventoryItem;
		int appCnt;
		bool isProductListVisible = false;
		bool isCreateProductPopupOpen = false;
		Measure totalArea;
		DateTime? targetDate;
		TimingEvent timingEvent;
		List<TimingEvent> timingEvents;
		string timingEventTag;
		string selectedTimingEventTag;
		List<string> timingEventTags;
		Dictionary<string, List<string>> availableTimingEventTags;
		Dictionary<string, List<string>> tempTimingEventTags;
		Dictionary<string, List<string>> mergedTimingEventTags;
		IClientEndpoint clientEndpoint;
		Messenger localMessenger;

		public PlannedProductViewModel(IClientEndpoint clientEndpoint, ProductSelectionViewModel parent, Application.FieldSelectionViewModel fieldSelectionModel, Messenger localMessenger, InventoryListView inventory, InfoViewModel infoViewModel, Dictionary<string, List<string>> availableTimingEventTags) {
			this.parent = parent;
			this.localMessenger = localMessenger;
			this.clientEndpoint = clientEndpoint;
			this.fieldSelectionModel = fieldSelectionModel;
			this.inventory = inventory;
			this.infoViewModel = infoViewModel;

			timingEvent = null;
			selectedTimingEventTag = null;
			timingEventTag = null;

			AllowedUnits = new ObservableCollection<UnitPerAreaHelper>();
			PriceItems = new ObservableCollection<PriceItem>();

			timingEvents = clientEndpoint.GetMasterlistService().GetTimingEventList().OrderBy(x => x.Order).ToList();

			var areaUnit = ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit;
			totalArea = Measure.CreateMeasure(this.infoViewModel.EstimatedArea, areaUnit);
			ProductArea = totalArea;

			ApplicationCount = 1;
			ProductAreaPercent = 1.0m;

			trackingId = Guid.NewGuid();
			TargetDate = parent.PlannedProducts.Count() > 0 ? parent.PlannedProducts.Last().TargetDate : null;

			ErrorsChanged += (sender, args) => {
				parent.ValidateViewModel();
			};

			CreateUserProductCommand = new RelayCommand(CreateUserProduct);

			localMessenger.Register<PropertyChangedMessage<double>>(this, GetEstimatedArea);
		}

		#region Properties
		public RelayCommand CreateUserProductCommand { get; }

		public ObservableCollection<UnitPerAreaHelper> AllowedUnits { get; }
		public ObservableCollection<PriceItem> PriceItems { get; }

		public string ProductSearchText { get; set; }

		[CustomValidation(typeof(PlannedProductViewModel), nameof(ValidateSelectedProduct))]
		public ProductListItemDetails SelectedProduct {
			get { return selectedProduct; }
			set {
				selectedProduct = value;

				ValidateAndRaisePropertyChanged(() => SelectedProduct);
				RaisePropertyChanged(() => DisplayName);

				if (selectedProduct != null) {
                    UpdateInventoryListItem();
                    UpdatePriceItems();
                    UpdateAllowedUnits();
					ProductAreaPercent = 1.0m;
					RateValue = 0;  // TODO: In the future, this should be the most recently used rate. We should also populate a list of the MR rates for this product.
					RecalculateItem();
					RaisePropertyChanged(() => TimingEvents);
				}

				parent.RefreshProductCount();
			}
		}

		public bool IsProductListVisible {
			get { return isProductListVisible; }
			set {
				isProductListVisible = value;
				RaisePropertyChanged(() => IsProductListVisible);
			}
		}

		public bool IsCreateProductPopupOpen {
			get { return isCreateProductPopupOpen; }
			set {
				isCreateProductPopupOpen = value;
				RaisePropertyChanged(() => IsCreateProductPopupOpen);
			}
		}

		public bool IsProductSelectedForCopy {
			get { return isCreateProductPopupOpen; }
			set {
				isCreateProductPopupOpen = value;
				RaisePropertyChanged(() => IsCreateProductPopupOpen);
			}
		}

		public string DisplayName {
			get {
				if (SelectedProduct == null) { return string.Empty; }
				return $"{SelectedProduct.Product.Name} ({SelectedProduct.Product.Manufacturer})";
			}
		}

		public decimal RateValue {
			get { return rateValue; }
			set {
				rateValue = value;
				RaisePropertyChanged(() => RateValue);
				RecalculateItem();
			}
		}

		[CustomValidation(typeof(PlannedProductViewModel), nameof(ValidateUnit))]
		public UnitPerAreaHelper SelectedRateUnitHelper {
			get { return rateUnitHelper; }
			set {
				rateUnitHelper = value;
				ValidateAndRaisePropertyChanged(() => SelectedRateUnitHelper);
				RecalculateItem();
			}
		}

		public PriceItem SelectedPriceItem {
			get { return selectedPriceItem; }
			set {
				selectedPriceItem = value;
				RaisePropertyChanged(() => SelectedPriceItem);
				RecalculateItem();
			}
		}

		public string SelectedPriceText {
			get { return selectedPriceText; }
			set {
				if (string.IsNullOrWhiteSpace(value)) { return; }

				if (selectedPriceText != value && (SelectedPriceItem == null || value != SelectedPriceItem.ToString())) {
					selectedPriceText = value;
					decimal price = 0m;
					var seeThis = decimal.TryParse(SelectedPriceText, out price);
					if (decimal.TryParse(selectedPriceText, out price)) {
						var priceUnit = currentInventoryItem == null || currentInventoryItem.AveragePriceUnit == null ? selectedProduct.Product.StdPackageUnit : currentInventoryItem.AveragePriceUnit;
						PriceItem priceItem = new PriceItem(price, priceUnit);
						if (!PriceItems.Contains(priceItem)) { PriceItems.Add(priceItem); }
						SelectedPriceItem = priceItem;
						selectedPriceText = priceItem.ToString();
					} else {
						//INCASE OF PARSE FAILURE   
						string parsedString = string.Empty;
						for (int i = 0; i < selectedPriceText.Count(); i++) {
							if (Char.IsDigit(selectedPriceText[i]) || selectedPriceText[i] == '.') {
								parsedString += selectedPriceText[i];
							}
						}

						if (decimal.TryParse(parsedString, out price)) {
							var priceUnit = currentInventoryItem == null || currentInventoryItem.AveragePriceUnit == null ? selectedProduct.Product.StdPackageUnit : currentInventoryItem.AveragePriceUnit;
							PriceItem priceItem = new PriceItem(price, priceUnit);
							if (!PriceItems.Contains(priceItem)) { PriceItems.Add(priceItem); }
							SelectedPriceItem = priceItem;
							selectedPriceText = priceItem.ToString();
						}
					}
				} else {
					selectedPriceText = selectedPriceItem.ToString();
				}
			}
		}

		public decimal CostValuePerUnit {
			get { return costValuePerUnit; }
			set {
				costValuePerUnit = value;
				RaisePropertyChanged(() => CostValuePerUnit);
				RecalculateItem();
			}
		}

		public IUnit CostUnit {
			get { return costUnit; }
			set {
				costUnit = value;
				RaisePropertyChanged(() => CostUnit);
			}
		}

		public Measure TotalProduct {
			get { return totalProduct; }
			set {
				totalProduct = value;
				RaisePropertyChanged(() => TotalProduct);
			}
		}

		public decimal TotalCost {
			get { return totalCost; }
			set {
				totalCost = value;
				RaisePropertyChanged(() => TotalCost);

				parent.RefreshCost();
			}
		}

		public decimal ProductAreaPercent {
			get { return productAreaPercent; }
			set {
				productAreaPercent = value;
				RaisePropertyChanged(() => ProductAreaPercent);

				RecalculateItem();
			}
		}

		// readonly property for displaying product area. Setter is only for conveniently raising the prop-changed event for the view
		public Measure ProductArea {
			get { return productArea; }
			private set {
				productArea = value;
				RaisePropertyChanged(() => ProductArea);
				RaisePropertyChanged(() => ProductAreaDisplayText);
			}
		}

		public string ProductAreaDisplayText {
			get { return ProductArea.AbbreviatedDisplay; }
			set {
				if (string.IsNullOrWhiteSpace(value)) { return; }

				decimal parsedValue = 0;
				if (decimal.TryParse(value, out parsedValue)) {
					ProductArea = totalArea.Unit.GetMeasure(Convert.ToDouble(parsedValue));
				}

				productAreaPercent = Convert.ToDecimal(productArea.Value / totalArea.Value);
				RaisePropertyChanged(() => ProductAreaPercent);
				RaisePropertyChanged(() => ProductAreaDisplayText);

				RecalculateItem();
			}
		}

		public int ApplicationCount {
			get { return appCnt; }
			set {
				appCnt = value;
				RaisePropertyChanged(() => ApplicationCount);
				RecalculateItem();
			}
		}

		public List<TimingEvent> TimingEvents {
			get { return timingEvents; }
			set {
				timingEvents = value;
				RaisePropertyChanged(() => TimingEvents);
			}
		}

		public TimingEvent TimingEvent {
			get { return timingEvent; }
			set {
				timingEvent = value;
				SelectAppropriateTags();
				RaisePropertyChanged(() => TimingEvent);
			}
		}

		public Dictionary<string, List<string>> AvailableTimingEventTags {
			get { return availableTimingEventTags; }
			set {
				availableTimingEventTags = value;
				SelectAppropriateTags();
			}
		}

		public Dictionary<string, List<string>> TempTimingEventTags {
			get { return tempTimingEventTags; }
			set {
				tempTimingEventTags = value;
				SelectAppropriateTags();
				RaisePropertyChanged(() => TempTimingEventTags);
			}
		}

		public List<string> TimingEventTags {
			get {
				if (timingEventTags == null) {
					return new List<string>();
				}

				return timingEventTags.OrderBy(q => q).ToList();
			}
			set {
				timingEventTags = value;
				RaisePropertyChanged(() => TimingEventTags);
			}
		}

		public string TimingEventTag {
			get { return timingEventTag; }
			set {
				if (timingEventTag == value) { return; }
				timingEventTag = value;
				RaisePropertyChanged(() => TimingEventTag);
			}
		}

		public string SelectedTimingEventTag {
			get { return selectedTimingEventTag; }
			set {
				if (selectedTimingEventTag == value) { return; }
				selectedTimingEventTag = value;
				if (!string.IsNullOrEmpty(selectedTimingEventTag)) {
					timingEventTag = selectedTimingEventTag;
				}
				RaisePropertyChanged(() => SelectedTimingEventTag);
			}
		}

		private void SelectAppropriateTags() {
			if (timingEvent != null && !string.IsNullOrEmpty(timingEvent.Name)) {
				mergedTimingEventTags = availableTimingEventTags;
				if (tempTimingEventTags != null && tempTimingEventTags.Count > 0) {
					mergedTimingEventTags = availableTimingEventTags.Concat(tempTimingEventTags).GroupBy(d => d.Key).ToDictionary(d => d.Key, d => d.First().Value);
				}

				if (mergedTimingEventTags != null && mergedTimingEventTags.ContainsKey(timingEvent.Name)) {
					TimingEventTags = mergedTimingEventTags[timingEvent.Name];
				} else {
					TimingEventTags = new List<string>();
				}
			}
		}

		public DateTime? TargetDate {
			get { return targetDate; }
			set {
				targetDate = value;
				RaisePropertyChanged(() => TargetDate);
			}
		}
		#endregion

		void UpdateInventoryListItem() {
			//currentInventoryItem = inventory.Products.ContainsKey(new ProductId(selectedProduct.Product.Id)) ? inventory.Products[new ProductId(selectedProduct.Product.Id)] : null;
			currentInventoryItem = inventory.Products.ContainsKey(selectedProduct.ProductId) ? inventory.Products[selectedProduct.ProductId] : null;
		}

		void UpdatePriceItems() {
			PriceItems.Clear();

			if (currentInventoryItem != null) {
				var priceItem = new PriceItem((decimal)currentInventoryItem.AveragePriceValue, currentInventoryItem.AveragePriceUnit, true);
				PriceItems.Add(priceItem);
				SelectedPriceItem = priceItem;
			} else {
				var priceItem = new PriceItem(0m, SelectedProduct.Product.StdPackageUnit, false);
				PriceItems.Add(priceItem);
				SelectedPriceItem = priceItem;
			}

			// Try to get the view with past price items
			var cachedPrices = clientEndpoint.GetView<SpecificPriceCacheView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear));
			if (cachedPrices.HasValue && cachedPrices.Value.Prices.ContainsKey(SelectedProduct.ProductId.Id)) {
				var priceList = cachedPrices.Value.Prices[SelectedProduct.ProductId.Id];
				foreach (var p in priceList) {
					PriceItems.Add(new PriceItem(p.Price, p.PriceUnit, false));
				}
			}
		}

		void UpdateAllowedUnits() {
			AllowedUnits.Clear();
			SelectedRateUnitHelper = null;

			if (SelectedProduct == null) { return; }

			var psu = UnitFactory.GetPackageSafeUnit(SelectedProduct.Product.StdPackageUnit, SelectedProduct.Product.StdUnit, SelectedProduct.Product.StdFactor, SelectedProduct.Product.StdPackageUnit);
			var compatibleUnits = CompatibilityFactory.GetCompatibleUnits(psu, SelectedProduct.Product.Density);

			foreach (var u in compatibleUnits) {
                if (DetermineDatasourceUnits.ShouldThisUnitShowInThisCulture(u.Name))
                {
                    var uh = new UnitPerAreaHelper(u, UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit));
                    AllowedUnits.Add(uh);
                    if (u == psu)
                    {
                        SelectedRateUnitHelper = uh;
                    }
                }
			}

			// If we couldn't find it in the list while we were building it, pick the first one, if any.
			if (SelectedRateUnitHelper == null && AllowedUnits.Any()) { SelectedRateUnitHelper = AllowedUnits[0]; }

			// TODO: Make CostUnit configurable
			CostUnit = UnitFactory.GetPackageSafeUnit(SelectedProduct.Product.StdPackageUnit, SelectedProduct.Product.StdUnit, SelectedProduct.Product.StdFactor, SelectedProduct.Product.StdPackageUnit);
		}

		private void GetEstimatedArea(PropertyChangedMessage<double> estimatedArea) {
			if (estimatedArea.PropertyName == "EstimatedArea") {
				var areaUnit = ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit;
				Measure estArea = Measure.CreateMeasure(estimatedArea.NewValue, areaUnit);
				totalArea = estArea;
				RecalculateItem();
			}
		}

		void RecalculateItem() {
			RecalculateByArea();
		}

		void RecalculateByArea() {
			var areaUnit = ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit;
			totalArea = Measure.CreateMeasure(infoViewModel.EstimatedArea, areaUnit);

			if (SelectedProduct == null || SelectedRateUnitHelper == null) {
				TotalProduct = new NullMeasure();
				return;
			}

			var psu = UnitFactory.GetPackageSafeUnit(SelectedProduct.Product.StdPackageUnit, SelectedProduct.Product.StdUnit, SelectedProduct.Product.StdFactor, SelectedProduct.Product.StdPackageUnit);
			var rate = SelectedRateUnitHelper.Unit.GetMeasure((double)RateValue, SelectedProduct.Product.Density); // TODO: Account for density, if necessary.

			ProductArea = totalArea.Unit.GetMeasure(totalArea.Value * (double)productAreaPercent);
			var totalValue = rate.Value * ProductArea.Value * ApplicationCount;
			var totalPreConvertedMeasure = rate.Unit.GetMeasure(totalValue, SelectedProduct.Product.Density);
            if (!totalPreConvertedMeasure.CanConvertTo(psu)) { return; }
			TotalProduct = totalPreConvertedMeasure.CreateAs(psu);
			var mlp = clientEndpoint.GetMasterlistService().GetProduct(SelectedProduct.ProductId);

			if (selectedPriceItem != null && selectedPriceItem.PriceUnit != null && mlp != null) {

				var priceUnit = UnitFactory.GetPackageSafeUnit(SelectedPriceItem.PriceUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
				if (TotalProduct.CanConvertTo(priceUnit)) {
					var convertedTotalProduct = TotalProduct.CreateAs(priceUnit);
					TotalCost = selectedPriceItem.Price * (decimal)convertedTotalProduct.Value; // TODO: This only works because we're currently defaulting units of Total Product and Cost to be the same. This won't always be the case, and needs to be changed.
				} else {
					TotalCost = 0;
				}
			} else {
				TotalCost = 0;
			}
		}

		internal PlanProductEntry ToPlanProductEntry() {
			// TODO: This needs to represent the actual rate unit the user has chosen.
			if (SelectedProduct != null) {
				var psu = UnitFactory.GetPackageSafeUnit(SelectedProduct.Product.StdPackageUnit, SelectedProduct.Product.StdUnit, SelectedProduct.Product.StdFactor, SelectedProduct.Product.StdPackageUnit);

				PriceStrategy strategy = PriceStrategy.Specific;
				decimal price = SelectedPriceItem != null ? SelectedPriceItem.Price : 0m;
				string priceUnit = SelectedPriceItem == null || SelectedPriceItem.PriceUnit == null ? psu.Name : SelectedPriceItem.PriceUnit;
				var areaUnit = ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit;

				return new PlanProductEntry(SelectedProduct.ProductId, trackingId, rateValue, SelectedRateUnitHelper.Unit.Name, (decimal)TotalProduct.Value, TotalProduct.Unit.Name, areaUnit, strategy, price, priceUnit, ProductAreaPercent, ApplicationCount, TimingEvent != null ? TimingEvent.Name : string.Empty, TargetDate, TimingEventTag);
			} else {
				return null;
			}
		}

		void OnProductCreated(MiniProduct product) {
			var pli = parent.AddTemporaryMiniProduct(product, new ProductId(ApplicationEnvironment.CurrentDataSourceId, product.Id), true);
			SelectedProduct = pli;

		}

		internal string ToName() {
			if (SelectedProduct != null && SelectedProduct.Product != null) {
				return SelectedProduct.Product.Name;
			} else {
				return string.Empty;
			}
		}

		void CreateUserProduct() {
			IsProductListVisible = false;
			IsCreateProductPopupOpen = false;

			var vm = new UserProductCreatorViewModel(clientEndpoint, ProductSearchText, OnProductCreated);

			Messenger.Default.Send(new ShowOverlayMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Secondary.UserCreatedProduct.UserProductCreatorView), "createProduct", vm)
			});
		}

		public static ValidationResult ValidateSelectedProduct(ProductListItemDetails product, ValidationContext context) {
			ValidationResult result = null;

			if (product == null) {
				result = new ValidationResult(Strings.ProductIsRequired_Text);
			}

			return result;
		}

		public static ValidationResult ValidateUnit(UnitPerAreaHelper unit, ValidationContext context) {
			ValidationResult result = null;

			if (unit == null) {
				result = new ValidationResult(Strings.ValidationResult_InvalidUnit_Text);
			}

			return result;
		}
	}
}