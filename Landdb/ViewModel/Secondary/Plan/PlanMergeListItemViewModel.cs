﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Secondary.Plan {
    public class PlanMergeListItemViewModel : ViewModelBase {
        bool ischecked;
        public PlanMergeListItemViewModel() {

        }

        public CropPlanId Id { get; set; }
        public DateTime StartDateTime { get; set; }
        public string Name { get; set; }
        public int FieldCount { get; set; }
        public int ProductCount { get; set; }
        public decimal TotalCost { get; set; }
        public bool Checked {
            get {
                return ischecked;
            }
            set {
                ischecked = value;
                RaisePropertyChanged(() => Checked, false, ischecked, true);
                //RaisePropertyChanged("ConcurrentPlans", new List<PlanMergeListItemViewModel>(), concurrentPlans, true);
                //RaisePropertyChanged(() => Checked, false, ischecked, true);
            }
        }
    }
}
