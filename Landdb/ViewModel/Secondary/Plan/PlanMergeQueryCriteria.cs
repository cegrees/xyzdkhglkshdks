﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Secondary.Plan {
    public class PlanMergeQueryCriteria
    {
        public DateTime StartDatetime { get; set; }
        public DateTime EndDateTime { get; set; }
        public List<DocumentDescriptor> IncludedDocuments { get; set; }
        public bool SpanishTranslation { get; set; }
    }
}
