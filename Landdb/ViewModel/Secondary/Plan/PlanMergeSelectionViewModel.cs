﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Plan;
using Landdb.Domain.ReadModels.WorkOrder;
using Landdb.Infrastructure;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Landdb.ViewModel.Secondary.Plan {
    public class PlanMergeSelectionViewModel : ViewModelBase
    {
        IClientEndpoint endpoint;
        int cropYear;
        PlanMergeListItemViewModel selected;
        ConcurrentQueue<PlanMergeListItemViewModel> queue;
        bool wos = true;
        bool apps = true;
        bool plans = true;
        PlanMergeItemViewModel selectedtarget;
        List<PlanMergeListItemViewModel> concurrentPlans;
        List<PlanMergeListItemViewModel> savedPlans;

        public PlanMergeSelectionViewModel(IClientEndpoint endpoint, int cropyear)
        {
            this.endpoint = endpoint;
            this.cropYear = cropyear;
            queue = new ConcurrentQueue<PlanMergeListItemViewModel>();
            concurrentPlans = new List<PlanMergeListItemViewModel>();
            savedPlans = new List<PlanMergeListItemViewModel>();
            StartDate = new DateTime(cropyear, 01, 01);
            EndDate = new DateTime(cropyear, 12, 31);
            GetDocuments();
            Messenger.Default.Register<PropertyChangedMessage<PlanMergeItemViewModel>>(this, OnTargetChanged);
            Messenger.Default.Register<PropertyChangedMessage<bool>>(this, OnCheckedChanged);

            SelectAll = new RelayCommand(selectAll);
            UnselectAll = new RelayCommand(unselectAll);
        }

        public ICommand SelectAll { get; set; }
        public ICommand UnselectAll { get; set; }

        public List<PlanMergeListItemViewModel> ConcurrentPlans
        {
            get { return concurrentPlans; }
            set {
                concurrentPlans = value;
                //RaisePropertyChanged("ConcurrentPlans", new List<PlanMergeListItemViewModel>(), concurrentPlans, true);
            }
        }

        public List<PlanMergeListItemViewModel> SavedPlans {
            get { return savedPlans; }
            set {
                savedPlans = value;
                RaisePropertyChanged("SavedPlans", new List<PlanMergeListItemViewModel>(), savedPlans, true);
            }
        }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public PlanMergeListItemViewModel SelectedDocument
        {
            get { return selected; }
            set
            {
                selected = value;

                if (selected != null)
                {
                    if (selected.Checked)
                    {
                        selected.Checked = false;
                    }
                    else
                    {
                        selected.Checked = true;
                    }
                }
            }
        }

        public PlanMergeItemViewModel SelectedTarget {
            get { return selectedtarget; }
            set {
                selectedtarget = value; }
        }

        public void selectAll()
        {
            if (ConcurrentPlans.Any())
            {
                ConcurrentPlans.ForEach(x => { x.Checked = true; });
            }
        }

        public void unselectAll()
        {
            if (ConcurrentPlans.Any())
            {
                ConcurrentPlans.ForEach(x => { x.Checked = false; });
            }
        }

        void GetDocuments() {
            concurrentPlans = new List<PlanMergeListItemViewModel>();
            List<PlanMergeListItemViewModel> planList = new List<PlanMergeListItemViewModel>();
            if (SelectedTarget == null) { return; }
            if (SelectedTarget.Id == null) { return; }
            if (SelectedTarget.Id.Id == null) { return; }
            Task planListTask = Task.Factory.StartNew(() => {
                    var plansMaybe = endpoint.GetView<PlanList>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear));
                    if (plansMaybe.HasValue) {
                        planList = (from p in plansMaybe.Value.Plans
                                    where p.Id != SelectedTarget.Id
                                    select new PlanMergeListItemViewModel() { Id = p.Id, FieldCount = p.FieldCount, ProductCount = p.ProductCount, Name = p.Name, StartDateTime = p.CreateDate, Checked = false }).ToList();
                }
            });

            planListTask.Wait();
            List<PlanMergeListItemViewModel> list = new List<PlanMergeListItemViewModel>();
            if (planList.Any()) {
                list.AddRange(planList);
            }
            ConcurrentPlans = list.OrderByDescending(x => x.StartDateTime).ToList();
            RaisePropertyChanged("ConcurrentPlans");
        }

        public void OnTargetChanged(PropertyChangedMessage<PlanMergeItemViewModel> targetChanged) {
            if (targetChanged.PropertyName == "SelectedTarget") {
                SelectedTarget = targetChanged.NewValue;
                GetDocuments();
            }
        }

        public void OnCheckedChanged(PropertyChangedMessage<bool> checkedChanged) {
            if (selectedtarget == null) { return; }
            if (concurrentPlans == null) { return; }
            if (concurrentPlans.Count == 0) { return; }
            if (checkedChanged.PropertyName == "Checked") {
                bool checkedddd = checkedChanged.NewValue;
                var plist = (from p in concurrentPlans
                         where p.Checked == true
                         select p).ToList();

                if (plist != savedPlans) {
                    SavedPlans = plist;
                }
            }

            //if (plist != savedPlans) {
            //        SavedPlans = concurrentPlans;
            //        //RaisePropertyChanged("ConcurrentPlans", new List<PlanMergeListItemViewModel>(), concurrentPlans, true);
            //    }
            //    //GetDocuments();
            //}
        }
    }
}
