﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Plan;
using Landdb.Domain.ReadModels.WorkOrder;
using Landdb.Infrastructure;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Landdb.ViewModel.Secondary.Plan {
    public class PlanMergeTargetSelectionViewModel : ViewModelBase
    {
        IClientEndpoint endpoint;
        int cropYear;
        PlanMergeItemViewModel selectedtarget;
        ConcurrentQueue<PlanMergeItemViewModel> queue;
        bool wos = true;
        bool apps = true;
        bool plans = true;
        
        public PlanMergeTargetSelectionViewModel(IClientEndpoint endpoint, int cropyear)
        {
            this.endpoint = endpoint;
            this.cropYear = cropyear;
            queue = new ConcurrentQueue<PlanMergeItemViewModel>();
            ConcurrentDocuments = new List<PlanMergeItemViewModel>();
            StartDate = new DateTime(cropyear, 01, 01);
            EndDate = new DateTime(cropyear, 12, 31);
            GetDocuments();

            SelectAll = new RelayCommand(selectAll);
            UnselectAll = new RelayCommand(unselectAll);
        }

        public ICommand SelectAll { get; set; }
        public ICommand UnselectAll { get; set; }

        public List<PlanMergeItemViewModel> ConcurrentDocuments
        {
            get;
            set;
        }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public PlanMergeItemViewModel SelectedTarget {
            get {
                return selectedtarget;
            }
            set
            {
                selectedtarget = value;

                if (selectedtarget != null)
                {
                    unselectAll();
                    if (selectedtarget.Checked)
                    {
                        selectedtarget.Checked = false;
                    }
                    else
                    {
                        selectedtarget.Checked = true;
                        RaisePropertyChanged("SelectedTarget", new PlanMergeItemViewModel(), selectedtarget, true);
                    }
                }
            }
        }

        public void selectAll()
        {
            if (ConcurrentDocuments.Any())
            {
                ConcurrentDocuments.ForEach(x => { x.Checked = true; });
            }
        }

        public void unselectAll()
        {
            if (ConcurrentDocuments.Any())
            {
                ConcurrentDocuments.ForEach(x => { x.Checked = false; });
            }
        }

        void GetDocuments() {
            ConcurrentDocuments = new List<PlanMergeItemViewModel>();
            List<PlanMergeItemViewModel> planList = new List<PlanMergeItemViewModel>();
            Task planListTask = Task.Factory.StartNew(() => {
                    var plansMaybe = endpoint.GetView<PlanList>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear));
                    if (plansMaybe.HasValue) {
                        planList = (from p in plansMaybe.Value.Plans
                                    select new PlanMergeItemViewModel() { Id = p.Id, FieldCount = p.FieldCount, ProductCount = p.ProductCount, Name = p.Name, StartDateTime = p.CreateDate, Checked = false }).ToList();
                    }
            });

            planListTask.Wait();
            List<PlanMergeItemViewModel> list = new List<PlanMergeItemViewModel>();
            if (planList.Any()) {
                list.AddRange(planList);
            }
            ConcurrentDocuments = list.OrderByDescending(x => x.StartDateTime).ToList();
            RaisePropertyChanged("ConcurrentDocuments");
        }
    }
}
