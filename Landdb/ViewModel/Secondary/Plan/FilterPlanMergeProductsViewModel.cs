﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels;
using Landdb.Domain.ReadModels.Inventory;
using NLog;
using Landdb.Client.Models;
using Landdb.Domain.ReadModels.Product;
using Landdb.Infrastructure;
using Landdb.Domain.ReadModels.Tags;
using Landdb.Domain.ReadModels.Plan;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure.Messages;
using System.Windows.Input;
using Landdb.ViewModel.Production;
using AgC.UnitConversion;

namespace Landdb.ViewModel.Secondary.Plan {
    public class FilterPlanMergeProductsViewModel : ViewModelBase
    {
        readonly Logger log = LogManager.GetCurrentClassLogger();
        Dictionary<string, List<string>> tempTimingEventTags = new Dictionary<string, List<string>>();
        Dictionary<string, List<string>> availableTimingEventTags = new Dictionary<string, List<string>>();
        private List<FilterPlanProductDetailsViewModel> copyProducts;
        private bool selectAllProducts = false;
        readonly IClientEndpoint endpoint;
        int cropYear;
        readonly IMasterlistService masterlist;
        PlanMergeItemViewModel selectedtarget;
        PlanMergeViewModel parent;
        bool massivecheckchange = false;

        public FilterPlanMergeProductsViewModel(PlanMergeViewModel parent, IClientEndpoint endpoint, int cropyear)
        {
            this.parent = parent;
            this.endpoint = endpoint;
            this.cropYear = cropyear;
            this.masterlist = endpoint.GetMasterlistService();
            copyProducts = new List<FilterPlanProductDetailsViewModel>();

            Messenger.Default.Register<PropertyChangedMessage<PlanMergeItemViewModel>>(this, OnTargetChanged);
            Messenger.Default.Register<PropertyChangedMessage<List<PlanMergeListItemViewModel>>>(this, OnPlansChanged);
            Messenger.Default.Register<PropertyChangedMessage<bool>>(this, OnCheckChanged);

            SelectAll = new RelayCommand(selectAll);
            UnselectAll = new RelayCommand(unselectAll);
            parent.ShowSaveButton = false;

        }

        public ICommand SelectAll { get; set; }
        public ICommand UnselectAll { get; set; }

        public FilterPlanProductDetailsViewModel SelectedProduct { get; set; }
        public List<FilterPlanProductDetailsViewModel> CopyProducts {
            get { return copyProducts; }
            set {
                copyProducts = value;
                RaisePropertyChanged("CopyProducts");
            }
        }

        public int CropYear {
            get { return cropYear; }
            set {
                cropYear = value;
                RaisePropertyChanged("CropYear");
            }
        }

        public bool SelectAllProducts {
            get {
                return selectAllProducts;
            }
            set {
                selectAllProducts = value;
                if (CopyProducts != null && CopyProducts.Count > 0) {
                    for (int i = 0; i < CopyProducts.Count; i++) {
                        CopyProducts[i].IsProductSelectedForCopy = selectAllProducts;
                    }
                }
                RaisePropertyChanged("CopyProducts");
                RaisePropertyChanged("SelectAllProducts");
            }
        }

        public List<PlanMergeListItemViewModel> ConcurrentPlans {
            get;
            set;
        }

        public PlanMergeItemViewModel SelectedTarget {
            get { return selectedtarget; }
            set {
                selectedtarget = value;
            }
        }


        public void selectAll() {
            massivecheckchange = true;
            if (CopyProducts.Any()) {
                CopyProducts.ForEach(x => { x.IsProductSelectedForCopy = true; });
                parent.ShowSaveButton = true;
            }
            massivecheckchange = false;
        }

        public void unselectAll() {
            massivecheckchange = true;
            if (CopyProducts.Any()) {
                CopyProducts.ForEach(x => { x.IsProductSelectedForCopy = false; });
            }
            parent.ShowSaveButton = false;
            massivecheckchange = false;
        }

        private void buildDetailsFromProjection(List<PlanMergeListItemViewModel> planstomerge) {
            parent.ShowSaveButton = false;
            massivecheckchange = true;
            decimal totalArea = 0m;
            if (selectedtarget == null) { return; }
            if (planstomerge == null) { return; }
            var planDetails = endpoint.GetView<PlanView>(selectedtarget.Id);
            if (planDetails.HasValue) {
                totalArea = (decimal)planDetails.Value.EstimatedArea.Value;
            }
            else {
                return;
            }

            //ObservableCollection<ProductDetails> Products = new ObservableCollection<ProductDetails>();
            copyProducts = new List<FilterPlanProductDetailsViewModel>();

            var timingEvents = endpoint.GetMasterlistService().GetTimingEventList().ToList();
            foreach (PlanMergeListItemViewModel plan in planstomerge) {
                var pMaybe = endpoint.GetView<PlanView>(plan.Id);
                if (pMaybe.HasValue) {

                    foreach (var p in pMaybe.Value.Products) {
                        if (p == null || string.IsNullOrWhiteSpace(p.RateUnit) || string.IsNullOrWhiteSpace(p.AreaUnit) || string.IsNullOrWhiteSpace(p.RateUnit) || string.IsNullOrWhiteSpace(p.CostUnit)) {
                            continue; // Guard statement for weeding out badly formatted products
                        }

                        //HACK :: SOME USER CREATED PRODUCTS ARE GETTING THE INCORRECT PRODUCTID
                        var masterlistProdQuery = endpoint.GetMasterlistService().GetProduct(p.Id);
                        var masterlistProd = masterlistProdQuery == null ? masterlist.GetProduct(new ProductId(ApplicationEnvironment.CurrentDataSourceId, p.Id.Id)) : masterlistProdQuery;

                        // calculate the total from the rate
                        var totalProduct = p.RateValue * totalArea * p.PercentApplied * p.ApplicationCount;
                        var totalProductMeasure = UnitFactory.GetPackageSafeUnit(p.RateUnit, masterlistProd.StdUnit, masterlistProd.StdFactor, masterlistProd.StdPackageUnit).GetMeasure((double)totalProduct, masterlistProd.Density);
                        var totalProductUnit = UnitFactory.GetPackageSafeUnit(p.TotalProductUnit, masterlistProd.StdUnit, masterlistProd.StdFactor, masterlistProd.StdPackageUnit);
                        var convertedTotalProduct = totalProductMeasure.GetValueAs(totalProductUnit);
                        var displayTotal = DisplayMeasureConverter.ConvertToTotalDisplayMeasure(p.Id, (decimal)convertedTotalProduct, p.TotalProductUnit, masterlistProd.Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);

                        var displayRate = DisplayMeasureConverter.ConvertToRateDisplayMeasure(p.Id, p.RateValue, p.RateUnit, masterlistProd.Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);
                        var unit = string.IsNullOrEmpty(p.SpecificCostUnit) ? null : UnitFactory.GetPackageSafeUnit(p.SpecificCostUnit, masterlistProd.StdUnit, masterlistProd.StdFactor, masterlistProd.StdPackageUnit);
                        decimal totalCost = 0m;

                        try {
                            if (displayTotal.CanConvertTo(unit)) {
                                var convertedTotal = unit != null && displayTotal.CanConvertTo(unit) ? displayTotal.GetValueAs(unit) : displayTotal.Value;
                                totalCost = p.SpecificCostPerUnit.HasValue ? (decimal)convertedTotal * p.SpecificCostPerUnit.Value : p.ProductTotalCost;
                            }
                            else {
                                totalCost = 0m;
                            }
                        }
                        catch (Exception ex) {
                            totalCost = 0m;
                        }

                        var timing = !string.IsNullOrEmpty(p.TimingEvent) ? timingEvents.SingleOrDefault(x => x.Name == p.TimingEvent) : null;
                        ProductDetails pd = new ProductDetails() {
                            ProductName = masterlistProd.Name,
                            RateMeasure = displayRate,
                            AreaUnit = UnitFactory.GetUnitByName(p.AreaUnit),
                            TotalProduct = displayTotal,
                            TotalCost = totalCost,
                            CostPerUnitValue = p.SpecificCostPerUnit.HasValue ? p.SpecificCostPerUnit.Value : 0m,
                            CostUnit = !string.IsNullOrEmpty(p.SpecificCostUnit) ? UnitFactory.GetUnitByName(p.SpecificCostUnit) : UnitFactory.GetUnitByName(p.CostUnit),
                            ID = p.Id,
                            ProductAreaPercent = p.PercentApplied,
                            TrackingId = p.TrackingId,
                            TimingEvent = p.TimingEvent,
                            TargetDate = p.TargetDate,
                            TimingEventTag = p.TimingEventTag,
                            TimingEventOrder = timing != null ? timing.Order : 0,
                            ApplicationCount = p.ApplicationCount.HasValue ? p.ApplicationCount.Value : 1,
                        };
                        //Products.Add(pd);
                        FilterPlanProductDetailsViewModel newproduct = new FilterPlanProductDetailsViewModel(pd, true);
                        copyProducts.Add(newproduct);
                        parent.ShowSaveButton = true;
                    }
                }
            }

            //Products = Products.OrderBy(x => x.TimingEventOrder).ThenBy(x => x.TimingEventTag);
            //Products = new ObservableCollection<ProductDetails>(Products.OrderBy(x => x.TimingEventOrder).ThenBy(x => x.TimingEventTag).ThenBy(b => b.TargetDate));

            //CreateCropZoneDetails();
            //InitializeProperties(planDetails);
            //CalculateNetRevenue();
            massivecheckchange = false;
        }

        public void OnTargetChanged(PropertyChangedMessage<PlanMergeItemViewModel> targetChanged) {
            if (targetChanged.PropertyName == "SelectedTarget") {
                SelectedTarget = targetChanged.NewValue;
                buildDetailsFromProjection(ConcurrentPlans);
                RaisePropertyChanged("CopyProducts");
            }
        }

        public void OnPlansChanged(PropertyChangedMessage<List<PlanMergeListItemViewModel>> plansChanged) {
            if (plansChanged.PropertyName == "SavedPlans") {
                ConcurrentPlans = plansChanged.NewValue;
                buildDetailsFromProjection(ConcurrentPlans);
                RaisePropertyChanged("CopyProducts");
            }
        }

        public void OnCheckChanged(PropertyChangedMessage<bool> checkChanged) {
            if (checkChanged.PropertyName == "IsProductSelectedForCopy") {
                if(!massivecheckchange) {

                    var plist = (from p in CopyProducts
                                 where p.IsProductSelectedForCopy == true
                                 select p).ToList();

                    if (plist.Any()) {
                        parent.ShowSaveButton = true;
                    }
                    else {
                        parent.ShowSaveButton = false;
                    }
                }
            }
        }

    }
}
