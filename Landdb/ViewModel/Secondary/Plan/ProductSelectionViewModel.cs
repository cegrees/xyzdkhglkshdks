﻿using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Domain.ReadModels.Plan;
using Landdb.Domain.ReadModels.Product;
using Landdb.ViewModel.Shared;
using NLog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Secondary.Plan {
	public class ProductSelectionViewModel : ValidationViewModelBase {

		readonly IClientEndpoint clientEndpoint;
		readonly Dispatcher dispatcher;

		readonly CropYearId currentCropYearId;

		readonly Logger log = LogManager.GetCurrentClassLogger();

		Application.FieldSelectionViewModel fieldSelection;
		InfoViewModel infoVM;
		InventoryListView inventory;

		PlannedProductViewModel selectedPlannedProduct;
		Dictionary<string, List<string>> tempTimingEventTags = new Dictionary<string, List<string>>();
		Dictionary<string, List<string>> availableTimingEventTags = new Dictionary<string, List<string>>();
		private ObservableCollection<PlannedProductViewModel> copyProducts;
		PlanView associatedPlanView;
		decimal totalCost;
		Task prodListTask;
		Messenger localMessenger;

		private ObservableCollection<PlannedProductViewModel> SelectedPlanProducts = new ObservableCollection<PlannedProductViewModel>();

		public ProductSelectionViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, CropYearId currentCropYearId, InfoViewModel infoVM, Application.FieldSelectionViewModel fieldSelection, Messenger localMessenger) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			this.currentCropYearId = currentCropYearId;

			this.fieldSelection = fieldSelection;
			this.localMessenger = localMessenger;

			this.infoVM = infoVM;

			inventory = clientEndpoint.GetView<InventoryListView>(currentCropYearId).GetValue(new InventoryListView());

			prodListTask = Task.Factory.StartNew(() => { BuildProductList(); });

			PlannedProducts = new ObservableCollection<PlannedProductViewModel>();
			copyProducts = new ObservableCollection<PlannedProductViewModel>();

			PlannedProducts.CollectionChanged += (sender, args) => { ValidateAndRaisePropertyChanged(() => PlannedProducts); };

			AddPlannedProductCommand = new RelayCommand(AddPlannedProduct);
			AddPlannedProductCommand.Execute(null);
			DeletePlannedProductCommand = new RelayCommand(DeleteProduct);
		}

		public PlanView AssociatedPlanView {
			get { return associatedPlanView; }
			set {
				associatedPlanView = value;

				if (associatedPlanView != null) {
					PlannedProducts.Clear();
					foreach (var prod in associatedPlanView.Products) {
						var mp = clientEndpoint.GetMasterlistService().GetProduct(prod.Id);
						var mur = clientEndpoint.GetView<ProductUsageView>(currentCropYearId).GetValue(new ProductUsageView());
						var cont = mur.GetProductGuids().Contains(prod.Id.Id);
						ProductListItemDetails prodVM;
						prodVM = new ProductListItemDetails(mp, prod.Id.DataSourceId, cont ? Strings.UsedThisYear_Text : Strings.Unused_Text, cont ? (short)0 : (short)1);

						var planProd = new PlannedProductViewModel(clientEndpoint, this, fieldSelection, localMessenger, inventory, infoVM, AvailableTimingEventTags);
						planProd.SelectedProduct = prodVM;
						planProd.RateValue = prod.RateValue;
						planProd.SelectedRateUnitHelper = planProd.AllowedUnits.FirstOrDefault(x => x.Unit.Name == prod.RateUnit);
						planProd.SelectedPriceText = prod.SpecificCostPerUnit.HasValue ? prod.SpecificCostPerUnit.Value.ToString() : string.Empty;
						planProd.ApplicationCount = prod.ApplicationCount.HasValue ? prod.ApplicationCount.Value : 1;
						planProd.ProductAreaPercent = prod.PercentApplied;
						planProd.TimingEvent = planProd.TimingEvents.FirstOrDefault(t => t.Name == prod.TimingEvent);
						planProd.TimingEventTag = !string.IsNullOrEmpty(prod.TimingEventTag) ? prod.TimingEventTag : null;
						planProd.TargetDate = prod.TargetDate;
						PlannedProducts.Add(planProd);
					}
				}

				RaisePropertyChanged(() => AssociatedPlanView);
			}
		}

		public ICommand AddPlannedProductCommand { get; private set; }
		public ICommand DeletePlannedProductCommand { get; private set; }

		public ObservableCollection<ProductListItemDetails> Products { get; private set; }

		[CustomValidation(typeof(ProductSelectionViewModel), nameof(ValidatePlannedProducts))]
		public ObservableCollection<PlannedProductViewModel> PlannedProducts { get; private set; }

		public Dictionary<string, List<string>> AvailableTimingEventTags { get; private set; }

		public void SetPlannedProducts(ObservableCollection<PlannedProductViewModel> selectedPlanProducts) {
			PlannedProducts = selectedPlanProducts;
			RaisePropertyChanged(() => PlannedProducts);
		}

		public ObservableCollection<PlannedProductViewModel> CopyProducts {
			get { return copyProducts; }
			set {
				copyProducts = value;
				RaisePropertyChanged(() => CopyProducts);
			}
		}

		public void SetSelectedPlanProducts(ObservableCollection<PlannedProductViewModel> selectedPlanProducts) {
			SelectedPlanProducts = selectedPlanProducts;
		}

		private T GetSourceDocumentView<T, TId>(DocumentDescriptor[] sourceDocuments, string typeString) where TId : AbstractDataSourceIdentity<Guid, Guid> {
			var q = from d in sourceDocuments
					where d.DocumentType == typeString
					select d;
			return q.Any() ? clientEndpoint.GetView<T>((TId)q.First().Identity).GetValue(() => default(T)) : default(T);  // The lengths I'll go to for a one-line solution....

		}

		public decimal TotalCost {
			get { return totalCost; }
			set {
				var oldValue = totalCost;
				totalCost = value;
				RaisePropertyChanged(() => TotalCost);
				localMessenger.Send(new PropertyChangedMessage<decimal>(oldValue, value, "TotalCost"));
			}
		}

		public int ProductCount {
			get {
				var q = from p in PlannedProducts
						where p.SelectedProduct != null
						select p;  // don't include products that have no data
				return q.Count();
			}
		}

		public PlannedProductViewModel SelectedPlannedProduct {
			get { return selectedPlannedProduct; }
			set {
				selectedPlannedProduct = value;

				if (selectedPlannedProduct != null) {
					selectedPlannedProduct.TempTimingEventTags = ProcessTemporaryTimingEventTags();
					selectedPlannedProduct.AvailableTimingEventTags = availableTimingEventTags;
				}

				RaisePropertyChanged(() => SelectedPlannedProduct);
			}
		}

		private Dictionary<string, List<string>> ProcessTemporaryTimingEventTags() {
			if (tempTimingEventTags == null) { tempTimingEventTags = new Dictionary<string, List<string>>(); }

			foreach (var pp in PlannedProducts) {
				if (!string.IsNullOrEmpty(pp.TimingEventTag) && pp.TimingEvent != null) {
					if (!tempTimingEventTags.ContainsKey(pp.TimingEvent.Name)) {
						tempTimingEventTags.Add(pp.TimingEvent.Name, new List<string>());
					}

					if (!tempTimingEventTags[pp.TimingEvent.Name].Contains(pp.TimingEventTag)) {
						tempTimingEventTags[pp.TimingEvent.Name].Add(pp.TimingEventTag);
					}
				}

			}

			foreach (var pp in PlannedProducts) {
				if (!string.IsNullOrEmpty(pp.TimingEventTag) && pp.TimingEvent != null) {
					if (!availableTimingEventTags.ContainsKey(pp.TimingEvent.Name)) {
						availableTimingEventTags.Add(pp.TimingEvent.Name, new List<string>());
					}

					if (!availableTimingEventTags[pp.TimingEvent.Name].Contains(pp.TimingEventTag)) {
						availableTimingEventTags[pp.TimingEvent.Name].Add(pp.TimingEventTag);
					}
				}

			}

			for (int i = 0; i < PlannedProducts.Count; i++) {
				PlannedProducts[i].AvailableTimingEventTags = availableTimingEventTags;
			}

			return tempTimingEventTags;
		}

		internal void RefreshCost() {
			TotalCost = (from ap in PlannedProducts
						 where ap.SelectedProduct != null
						 select ap.TotalCost).Sum();
		}

		internal void RefreshProductCount() {
			RaisePropertyChanged(() => ProductCount);
		}

		void DeleteProduct() {
			PlannedProducts.Remove(SelectedPlannedProduct);
			RaisePropertyChanged(()=>ProductCount);
		}

		async void BuildProductList() {
			var watch = Stopwatch.StartNew();

			await Task.Run(() => {
				var mpl = clientEndpoint.GetMasterlistService().GetProductList();
				var mur = clientEndpoint.GetView<ProductUsageView>(currentCropYearId).GetValue(new ProductUsageView());
				var ucp = clientEndpoint.GetView<UserCreatedProductList>(new DataSourceId(currentCropYearId.DataSourceId)).GetValue(new UserCreatedProductList());

				Guid mlDsId = GlobalIdentifiers.GetMasterlistDataSourceId();
				var productGuids = mur.GetProductGuids();

				var q = from p in mpl
						let cont = productGuids.Contains(p.Id)
						select new ProductListItemDetails(p, mlDsId, cont ? Strings.UsedThisYear_Text : Strings.Unused_Text, cont ? (short)0 : (short)1);

				var ucpQ = from p in ucp.UserCreatedProducts
						   let cont = productGuids.Contains(p.Id)
						   select new ProductListItemDetails(p, currentCropYearId.DataSourceId, cont ? Strings.UsedThisYear_Text : Strings.Unused_Text, cont ? (short)0 : (short)1);

				Products = new ObservableCollection<ProductListItemDetails>(q.Union(ucpQ).OrderBy(x => x.GroupSortOrder).ToList());

				availableTimingEventTags = new Dictionary<string, List<string>>();
				GetAvailableTimingEvents();
			});

			watch.Stop();
		}

		private void GetAvailableTimingEvents() {
			var plantagviewMaybe = clientEndpoint.GetView<PlanProductTagProjectionView>(currentCropYearId);

			if (plantagviewMaybe.HasValue && plantagviewMaybe.Value.TagInfo != null) {
				foreach (var v in plantagviewMaybe.Value.TagInfo) {
					if (string.IsNullOrEmpty(v.TimingEvent)) { continue; }

					if (!availableTimingEventTags.ContainsKey(v.TimingEvent)) {
						availableTimingEventTags.Add(v.TimingEvent, new List<string>());
					}

					foreach (var x in v.Tags) {
						if (availableTimingEventTags.ContainsKey(v.TimingEvent)) {
							if (!string.IsNullOrEmpty(x.Key) && !availableTimingEventTags[v.TimingEvent].Contains(x.Key)) {
								availableTimingEventTags[v.TimingEvent].Add(x.Key);
							}
						}
					}
				}
			}

			foreach (var v in PlannedProducts) {
				if (v.TimingEvent == null) { continue; }

				if (!availableTimingEventTags.ContainsKey(v.TimingEvent.Name)) {
					availableTimingEventTags.Add(v.TimingEvent.Name, new List<string>());
				}

				if (availableTimingEventTags.ContainsKey(v.TimingEvent.Name)) {
					if (!string.IsNullOrEmpty(v.TimingEventTag) && !availableTimingEventTags[v.TimingEvent.Name].Contains(v.TimingEventTag)) {
						availableTimingEventTags[v.TimingEvent.Name].Add(v.TimingEventTag);
					}
				}
			}

			for (int i = 0; i < PlannedProducts.Count; i++) {
				PlannedProducts[i].AvailableTimingEventTags = availableTimingEventTags;
			}
		}

		internal ProductListItemDetails AddTemporaryMiniProduct(MiniProduct product, ProductId productId, bool isUsed) {
			var p = new ProductListItemDetails(product, productId.DataSourceId, isUsed ? Strings.UsedThisYear_Text : Strings.Unused_Text, isUsed ? (short)0 : (short)1);
			Products.Add(p);
			return p;

		}
		public PlanProductEntry[] GetPlannedProducts() {
			List<PlanProductEntry> plannedProducts = new List<PlanProductEntry>();

			foreach (var ap in PlannedProducts) {
				//Weed out any Null Line Items on Products
				if (ap != null && ap.SelectedProduct != null) {
					plannedProducts.Add(ap.ToPlanProductEntry());
				}
			}

			return plannedProducts.ToArray();
		}

		internal string CreateNameFromProducts() {
			var sb = new StringBuilder();

			foreach (var ap in PlannedProducts) {
				if (sb.Length > 80) { break; }
				if (sb.Length > 0) { sb.Append(", "); }
				sb.Append(ap.ToName());
			}

			return sb.ToString();
		}

		internal void ResetData() {
			PlannedProducts.Clear();
			RaisePropertyChanged(() => ProductCount);
			SelectedPlannedProduct = null;
			TotalCost = 0m;
		}

		void AddPlannedProduct() {
			GetAvailableTimingEvents();
			var newPlannedProduct = new PlannedProductViewModel(clientEndpoint, this, fieldSelection, localMessenger, inventory, infoVM, availableTimingEventTags);
			PlannedProducts.Add(newPlannedProduct);
			SelectedPlannedProduct = newPlannedProduct;
			RaisePropertyChanged(() => ProductCount);
			//RaisePropertyChanged(() => TimingEvents);
		}

		#region Custom Validation
		public static ValidationResult ValidatePlannedProducts(ObservableCollection<PlannedProductViewModel> plannedProducts, ValidationContext context) {
			ValidationResult result = null;

			if (plannedProducts.Any(x => x.HasErrors)) {
				result = new ValidationResult(Strings.AtLeastOneProductHasErrors_Text);
			}

			return result;
		}
		#endregion
	}
}
