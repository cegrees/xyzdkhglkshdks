﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AgC.UnitConversion;
using GalaSoft.MvvmLight;
using Landdb.Domain.ReadModels;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.ViewModel.Secondary.Application;
using Landdb.ValueObjects;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Models;
using GalaSoft.MvvmLight.Command;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Secondary.UserCreatedProduct;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure;
using Landdb.Domain.ReadModels.Tags;
using Landdb.Controls;
using Landdb.Domain.ReadModels.Plan;
using Landdb.ViewModel.Production;

namespace Landdb.ViewModel.Secondary.Plan {
    public class FilterPlanProductDetailsViewModel : ViewModelBase {
        ProductDetails productDetails;
        string productName;
        string timingEvent;
        string timingEventTag;
        decimal totalCost = 0;
        Measure totalProduct;
        string rate;
        string costPerUnitDisplay;
        decimal productAreaPercent = 0;
        bool isProductSelectedForCopy = false;
        bool broadcastchanges = false;

        public FilterPlanProductDetailsViewModel(ProductDetails productdetails) {
            this.productDetails = productdetails;
            this.productName = productDetails.ProductName;
            this.timingEvent = productDetails.TimingEvent;
            this.timingEventTag = productDetails.TimingEventTag;
            this.totalCost = productDetails.TotalCost;
            this.totalProduct = productDetails.TotalProduct;
            this.rate = productDetails.Rate;
            this.costPerUnitDisplay = productDetails.CostPerUnitDisplay;
            this.productAreaPercent = productDetails.ProductAreaPercent;
        }

        public FilterPlanProductDetailsViewModel(ProductDetails productdetails, bool isProductSelectedForCopy) {
            this.productDetails = productdetails;
            this.productName = productDetails.ProductName;
            this.timingEvent = productDetails.TimingEvent;
            this.timingEventTag = productDetails.TimingEventTag;
            this.totalCost = productDetails.TotalCost;
            this.totalProduct = productDetails.TotalProduct;
            this.rate = productDetails.Rate;
            this.costPerUnitDisplay = productDetails.CostPerUnitDisplay;
            this.productAreaPercent = productDetails.ProductAreaPercent;
            this.IsProductSelectedForCopy = isProductSelectedForCopy;
            broadcastchanges = true;
        }

        public ProductDetails ProductDetails {
            get { return productDetails; }
            set {
                productDetails = value;
                RaisePropertyChanged("ProductDetails");
            }
        }

        public string TimingEvent {
            get { return timingEvent; }
            set {
                timingEvent = value;
                RaisePropertyChanged("TimingEvent");
            }
        }

        public string TimingEventTag {
            get { return timingEventTag; }
            set {
                if (timingEventTag == value) { return; }
                timingEventTag = value;
                RaisePropertyChanged("TimingEventTag");
            }
        }

        public bool IsProductSelectedForCopy {
            get { return isProductSelectedForCopy; }
            set {
                isProductSelectedForCopy = value;
                if (broadcastchanges) {
                    RaisePropertyChanged(() => IsProductSelectedForCopy, false, isProductSelectedForCopy, true);
                }
                else {
                    RaisePropertyChanged("IsProductSelectedForCopy");
                }
            }
        }

        public string ProductName {
            get { return productName; }
            set {
                if (productName == value) { return; }
                productName = value;
                RaisePropertyChanged("ProductName");
            }
        }

        public string Rate {
            get { return rate; }
            set {
                rate = value;
                RaisePropertyChanged("Rate");
            }
        }

        public Measure TotalProduct {
            get { return totalProduct; }
            set {
                totalProduct = value;
                RaisePropertyChanged("TotalProduct");
            }
        }

        public decimal TotalCost {
            get { return totalCost; }
            set {
                totalCost = value;
                RaisePropertyChanged("TotalCost");
            }
        }

        public string CostPerUnitDisplay {
            get { return costPerUnitDisplay; }
            set {
                costPerUnitDisplay = value;
                RaisePropertyChanged("CostPerUnitDisplay");
            }
        }

        public decimal ProductAreaPercent {
            get { return productAreaPercent; }
            set {
                productAreaPercent = value;
                RaisePropertyChanged("ProductAreaPercent");
            }
        }
    }
}