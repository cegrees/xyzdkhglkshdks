﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GalaSoft.MvvmLight;
using ThinkGeo.MapSuite.WpfDesktopEdition;
using ThinkGeo.MapSuite.Core;
using Landdb.Infrastructure;
using System.IO;
using Microsoft.Win32;
using GalaSoft.MvvmLight.Command;
using System.Windows.Input;
using System.Collections.ObjectModel;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure.Messages;
using Landdb.Domain.ReadModels.Tree;
using Landdb.MapStyles;
using Landdb.Client.Infrastructure;
using System.Windows.Threading;
using Landdb.ViewModel.Maps.ImageryOverlay;
using NLog;
using Landdb.ViewModel.Fields.FieldDetails.MapModels;
using Landdb.Domain.ReadModels.MapAnnotation;
using Landdb.Domain.ReadModels.Map;
using AgC.UnitConversion.Area;
using AgC.UnitConversion;
using Landdb.Resources;

namespace Landdb.ViewModel.Secondary.Map {
    public class PLSSViewModel : ViewModelBase {
        bool maploaded = false;
        bool startedOutOnline = false;
        bool ismousedown = false;
        bool draglabel = true;
        bool hasunsavedchanges = false;
        bool isfieldselected = false;
        bool loadbingmap = true;
        string wmsmaptype = "PLSS";
        string fieldname = Strings.DragLabel_Text;
        double desiredarea = 100;
        double oldarea = 0;
        IList<MapItem> clientLayerItems;
        WfsFeatureLayer wfsFeatureLayer = new WfsFeatureLayer();
        InMemoryFeatureLayer fieldsLayer = new InMemoryFeatureLayer();
        LayerOverlay layerOverlay = new LayerOverlay();
        LayerOverlay annotationOverlay = new LayerOverlay();
        Proj4Projection projection;
        Proj4Projection projection2;
        Proj4Projection projection84;
        Proj4Projection positionProjection;
        //RectangleShape previousmapextent = new RectangleShape();
        Feature selectedfield = new Feature();
        Feature loadedfeature = new Feature();
        BaseShape fieldshape = new MultipolygonShape();
        ClientLayerAreaStyle fieldsLayerAreaStyle = null;
        Logger log = LogManager.GetCurrentClassLogger();
        //string activemap = string.Empty;
        //string activemap1 = "BingRadar";
        //string activemap2 = "WorldMapKitRadar";
        //ZoomLevelFactory zoomlevelfactory = new ZoomLevelFactory();
        string coordinateformat = string.Empty;

        readonly IClientEndpoint clientEndpoint;
        IStyleFactory styleFactory;
        Dictionary<string, MapAnnotationCategory> userstylenames = new Dictionary<string, MapAnnotationCategory>();
        MapStyles.AnnotationMultiStyle customMultiStyles = new MapStyles.AnnotationMultiStyle();
        AnnotationScalingTextStyle scalingTextStyle2 = new AnnotationScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, 24, 6);
        List<AnnotationStyleItem> changedstyles = new List<AnnotationStyleItem>();
        MapSettings mapsettings;
        InMemoryFeatureLayer annotationLayer = new InMemoryFeatureLayer();
        int annotationopacity = 125;
        bool annotationvisibility = false;
        bool mapannotationlabelsvisible = false;

        ScalingTextStyle scalingTextStyle = new ScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, 24, 2);
        bool displayareainfieldlabel = false;
        double fittingpolygonfactor = 2;
        bool allowlabeloverlapping = true;
        bool fittingpolygon = true;
        bool displayUnselectedFields = false;
        bool labelallpolygonparts = false;
        bool displayLabels = false;
        private Dictionary<string, double> rotationAngles = new Dictionary<string, double>();
        private Dictionary<string, PointShape> labelPositions = new Dictionary<string, PointShape>();
        int areadecimals = 2;
        int bordersize = 2;
        string areaUnitString = string.Empty;
        AgC.UnitConversion.Area.AreaUnit agcAreaUnit;
        ThinkGeo.MapSuite.Core.AreaUnit mapAreaUnit;
        ThinkGeo.MapSuite.Core.DistanceUnit mapDistanceUnit;


        public PLSSViewModel(IClientEndpoint clientEndpoint, string wmsmaptype) {
            this.clientEndpoint = clientEndpoint;
            this.mapsettings = clientEndpoint.GetMapSettings();
            this.loadbingmap = false;
            this.wmsmaptype = wmsmaptype;
            if (wmsmaptype == "PLSSBing") {
                this.loadbingmap = true;
            }

            styleFactory = new StyleFactory();
            changedstyles = new List<AnnotationStyleItem>();
            userstylenames = new Dictionary<string, MapAnnotationCategory>();
            coordinateformat = mapsettings.DisplayCoordinateFormat;
            bordersize = mapsettings.BorderSize;
            areadecimals = mapsettings.AreaDecimals;
            areaUnitString = mapsettings.MapAreaUnit;
            coordinateformat = mapsettings.DisplayCoordinateFormat;

            agcAreaUnit = UnitFactory.GetUnitByName(areaUnitString) as AgC.UnitConversion.Area.AreaUnit;
            mapAreaUnit = MapUnitFactory.AreaMeasureConversion(agcAreaUnit);
            mapDistanceUnit = MapUnitFactory.AreaDistanceMeasureConversion(agcAreaUnit);

            annotationopacity = mapsettings.MapAnnotationOpacity;
            annotationvisibility = mapsettings.MapAnnotationsVisible;
            displayLabels = mapsettings.ScalingTextVisible;
            displayUnselectedFields = mapsettings.FieldsVisible;
            annotationopacity = mapsettings.MapAnnotationOpacity;
            if (mapsettings.DisplayAreaInFieldLabel.HasValue) {
                displayareainfieldlabel = mapsettings.DisplayAreaInFieldLabel.Value;
            }
            //if (mapsettings.CMGMapAnnotationsVisible.HasValue) {
            //    annotationvisibility = mapsettings.CMGMapAnnotationsVisible.Value;
            //}
            if (mapsettings.MapAnnotationLabelsVisible.HasValue) {
                mapannotationlabelsvisible = mapsettings.MapAnnotationLabelsVisible.Value;
            }
            positionProjection = new Proj4Projection(Proj4Projection.GetGoogleMapParametersString(), Proj4Projection.GetWgs84ParametersString());
            scalingTextStyle = new ScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, mapsettings.ScalingTextMaximumSize, this.mapsettings.ScalingTextMinimumSize);
            scalingTextStyle.PositionProjection = positionProjection;
            if (displayareainfieldlabel) {
                scalingTextStyle.SetTexTColumnName(@"AreaLabel");
            }
            else {
                scalingTextStyle.SetTexTColumnName(@"LdbLabel");
            }
            fittingpolygonfactor = mapsettings.FittingPolygonFactor;
            if (mapsettings.AllowLabelOverlapping.HasValue) {
                allowlabeloverlapping = mapsettings.AllowLabelOverlapping.Value;
            }
            if (mapsettings.FittingPolygon.HasValue) {
                fittingpolygon = mapsettings.FittingPolygon.Value;
            }
            //StyleData.Self.UserStyleNames = this.mapsettings.MapAnnotationCategories;

            scalingTextStyle2 = new AnnotationScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, mapsettings.ScalingTextMaximumSize, this.mapsettings.ScalingTextMinimumSize);
            scalingTextStyle2.ColumnsList = new List<string>() { @"Id", @"LdbLabel", @"TypeName", @"ShapeType" };
            //scalingTextStyle2.YOffsetInPixel = 20;

            scalingTextStyle.UnselectedFieldVisibility = displayUnselectedFields;
            scalingTextStyle.LabelAllPolygonParts = labelallpolygonparts;
            scalingTextStyle.PolygonLabelingLocationMode = PolygonLabelingLocationMode.BoundingBoxCenter;
            scalingTextStyle.FittingPolygon = fittingpolygon;
            scalingTextStyle.FittingPolygonFactor = fittingpolygonfactor;
            if (allowlabeloverlapping) {
                scalingTextStyle.OverlappingRule = LabelOverlappingRule.AllowOverlapping;
            }
            else {
                scalingTextStyle.OverlappingRule = LabelOverlappingRule.NoOverlapping;
            }

            //CheckForProjectionDLL();
            //activemap = activemap2;
            InitializeMap();
            HasUnsavedChanges = false;

            CancelCommand = new RelayCommand(Cancel);
        }

        //private void CheckForProjectionDLL() {
        //    activemap = activemap2;
        //    string systemPath = Environment.SystemDirectory;
        //    TestAndSetMap(systemPath, @"Map Suite 8.0");
        //    TestAndSetMap(systemPath, @"Map Suite 8.5");
        //    TestAndSetMap(systemPath, @"Map Suite 9.0");
        //    TestAndSetMap(systemPath, @"Map Suite 9.5");
        //    TestAndSetMap(systemPath, @"Map Suite 10.0");
        //    systemPath = @"C:\WINDOWS\system32";
        //    TestAndSetMap(systemPath, @"Map Suite 8.0");
        //    TestAndSetMap(systemPath, @"Map Suite 8.5");
        //    TestAndSetMap(systemPath, @"Map Suite 9.0");
        //    TestAndSetMap(systemPath, @"Map Suite 9.5");
        //    TestAndSetMap(systemPath, @"Map Suite 10.0");
        //    systemPath = @"C:\WINDOWS\sysWOW64";
        //    TestAndSetMap(systemPath, @"Map Suite 8.0");
        //    TestAndSetMap(systemPath, @"Map Suite 8.5");
        //    TestAndSetMap(systemPath, @"Map Suite 9.0");
        //    TestAndSetMap(systemPath, @"Map Suite 9.5");
        //    TestAndSetMap(systemPath, @"Map Suite 10.0");
        //    systemPath = @"C:\WINDOWS\SysWOW64";
        //    TestAndSetMap(systemPath, @"Map Suite 8.0");
        //    TestAndSetMap(systemPath, @"Map Suite 8.5");
        //    TestAndSetMap(systemPath, @"Map Suite 9.0");
        //    TestAndSetMap(systemPath, @"Map Suite 9.5");
        //    TestAndSetMap(systemPath, @"Map Suite 10.0");
        //}

        //private void TestAndSetMap(string systemPath, string suitePath) {
        //    string path1 = Path.Combine(systemPath, suitePath);
        //    if (activemap != "BingRadar") {
        //        if (Directory.Exists(path1)) {
        //            activemap = "BingRadar";
        //        }
        //    }
        //}
        public ICommand CancelCommand { get; private set; }

        public WpfMap Map { get; set; }

        static object clientLayerLock = new object();
        public IList<MapItem> ClientLayer {
            get { return clientLayerItems; }
            set {
                lock (clientLayerLock) {
                    clientLayerItems = value;
                    LoadFieldLayerShapes();
                    RaisePropertyChanged("ClientLayer");
                }
            }
        }

        public Feature SelectedField {
            get { return selectedfield; }
            set {
                if (selectedfield == value) { return; }
                selectedfield = value;
                RaisePropertyChanged("SelectedField");
                if (selectedfield.Id != loadedfeature.Id) {
                    loadedfeature = selectedfield;
                    fieldshape = selectedfield.GetShape();
                    if (fieldshape is MultipolygonShape) {
                        MultipolygonShape ps = fieldshape as MultipolygonShape;
                    }

                    if (maploaded) {
                        OpenAndLoadaShapefile();
                    }
                }
            }
        }

        public string FieldName {
            get { return fieldname; }
            set {
                if (fieldname == value) { return; }
                fieldname = value;
                RaisePropertyChanged("FieldName");
            }
        }

        public bool HasUnsavedChanges {
            get { return hasunsavedchanges; }
            set {
                if (hasunsavedchanges == value) { return; }
                hasunsavedchanges = value;
                RaisePropertyChanged("HasUnsavedChanges");
            }
        }

        public Dictionary<string, double> RotationAngles {
            get { return rotationAngles; }
            set {
                if (rotationAngles == value) { return; }
                rotationAngles = value;
                scalingTextStyle.RotationAngles = rotationAngles;

            }
        }

        public Dictionary<string, PointShape> LabelPositions {
            get { return labelPositions; }
            set {
                if (labelPositions == value) { return; }
                labelPositions = value;
                scalingTextStyle.LabelPositions = labelPositions;
            }
        }

        //public RectangleShape PreviousMapExtent
        //{
        //    get { return previousmapextent; }
        //    set
        //    {
        //        if (previousmapextent == value) { return; }
        //        previousmapextent = value;
        //        RaisePropertyChanged("PreviousMapExtent");
        //    }
        //}

        internal void EnsureFieldsLayerIsOpen() {
            if (fieldsLayer == null) {
                fieldsLayer = new InMemoryFeatureLayer();
            }

            if (!fieldsLayer.IsOpen) {
                fieldsLayer.Open();
            }

            if (!projection.IsOpen) {
                projection.Open();
            }

            if (!projection2.IsOpen) {
                projection2.Open();
            }

            if (!projection84.IsOpen) {
                projection84.Open();
            }
        }

        void InitializeMap() {
            Map = new WpfMap();
            if (loadbingmap) {
                Map.MapUnit = GeographyUnit.Meter;
                Map.CurrentExtent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
            }
            else {
                Map.MapUnit = GeographyUnit.DecimalDegree;
                Map.CurrentExtent = new RectangleShape(-143.4, 109.3, 116.7, -76.3);
            }

            startedOutOnline = Landdb.Client.Infrastructure.NetworkStatus.IsInternetAvailableFast();

            Map.MapTools.PanZoomBar.Visibility = System.Windows.Visibility.Visible;
            Map.MapTools.PanZoomBar.GlobeButtonVisibility = System.Windows.Visibility.Hidden;
            Map.MapTools.PanZoomBar.VerticalAlignment = System.Windows.VerticalAlignment.Center;
            Map.MapTools.PanZoomBar.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            Map.Background = System.Windows.Media.Brushes.White;
            ZoomLevelFactory zoomlevelfactory = new ZoomLevelFactory();
            if (wmsmaptype == "TOPO" || wmsmaptype == "Hydrology") {
                Map.ZoomLevelSet = zoomlevelfactory.GetTopoZoomLevelSet();
            }
            else {
                Map.ZoomLevelSet = zoomlevelfactory.ZoomLevelSET;
            }
            HasUnsavedChanges = false;
            Map.MouseLeftButtonDown += (sender, e) => {
                ((MainWindow)System.Windows.Application.Current.MainWindow).Draggable = false;
            };

            //zoomlevelfactory = new ZoomLevelFactory();
            //Map.ZoomLevelSet = zoomlevelfactory.ZoomLevelSET;

            //Map.ExtentOverlay.PanMode = MapPanMode.Default;

            projection = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetGoogleMapParametersString());
            projection2 = new Proj4Projection(Proj4Projection.GetGoogleMapParametersString(), Proj4Projection.GetWgs84ParametersString());
            //projection84 = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetWgs84ParametersString());
            projection84 = new Proj4Projection(Proj4Projection.GetEpsgParametersString(4326), Proj4Projection.GetEpsgParametersString(4326));
            positionProjection = new Proj4Projection(Proj4Projection.GetGoogleMapParametersString(), Proj4Projection.GetWgs84ParametersString());

            scalingTextStyle2.RotationAngles = new Dictionary<string, double>();
            scalingTextStyle2.LabelPositions = new Dictionary<string, PointShape>();
            CreateAnnotationMemoryLayer();

            //NoaaRadarMonitor.RefreshInterval = new TimeSpan(0, 0, 30);
            //NoaaRadarMonitor.StartMonitoring();

            Map.Loaded += (sender, e) => {
                if (maploaded) { return; }
                this.mapsettings = clientEndpoint.GetMapSettings();
                try {
                    if (loadbingmap) {
                        LoadBingMapsOverlay(); ;
                    }
                    UseLayer();
                    OpenAndLoadaShapefile();
                    RebuildMapAnnotations();
                    if (!layerOverlay.Layers.Contains("annotationLayer")) {
                        annotationOverlay.Layers.Add("annotationLayer", annotationLayer);
                    }
                    if (!Map.Overlays.Contains("annotationOverlay")) {
                        Map.Overlays.Add("annotationOverlay", annotationOverlay);
                    }
                    EnsureAnnotationLayerIsOpen();
                    if (Map.Overlays.Contains("annotationOverlay")) {
                        Map.Overlays["annotationOverlay"].Refresh();
                    }
                    if (Map.Overlays.Contains("importOverlay")) {
                        if (((LayerOverlay)Map.Overlays["importOverlay"]).Layers.Contains("ImportLayer")) {
                            scalingTextStyle.MapScale = Map.CurrentScale;
                            scalingTextStyle2.MapScale = Map.CurrentScale;
                            Map.Refresh();
                        }
                    }
                    maploaded = true;
                }
                catch { }
            };

            Map.CurrentScaleChanged += (sender, e) => {
                //scalingTextStyle.MapScale = e.CurrentScale;
                scalingTextStyle.MapScale = e.CurrentScale;
                scalingTextStyle2.MapScale = e.CurrentScale;
                //scalingTextStyle4.MapScale = Map.CurrentScale;
                //lastmapscale = e.CurrentScale;
                //if (repaintthescreen) {
                //    repaintthescreen = false;
                //}
            };


            //Map.MapClick += (sender, e) => {
            //    SavePolygon();
            //};

        }

        private void UseLayer() {
            try {
                WmsRasterLayer wmsImageLayer = new WmsRasterLayer();
                System.Collections.ObjectModel.Collection<string> layernames = new System.Collections.ObjectModel.Collection<string>();
                if (wmsmaptype == "PLSSBing") {
                    wmsImageLayer = new WmsRasterLayer(new Uri("http://www.geocommunicator.gov/wmsconnector/com.esri.wms.Esrimap?ServiceName=BLM_MAP_PLSS&"));
                    wmsImageLayer.UpperThreshold = double.MaxValue;
                    wmsImageLayer.LowerThreshold = double.MinValue;
                    wmsImageLayer.Open();
                    Collection<string> lnames = wmsImageLayer.GetServerLayerNames();
                    wmsImageLayer.ImageSource.Projection = projection;
                    layernames.Add("Meridian Lines");
                    layernames.Add("Base Lines");
                    layernames.Add("Township Index Labels");
                    layernames.Add("Minor Subdivision");
                    layernames.Add("Special Surveys");
                    layernames.Add("Quarter-Quarters - Alternate Source");
                    layernames.Add("Quarter-Quarters");
                    layernames.Add("Sections - Alternate Source");
                    layernames.Add("Sections");
                    layernames.Add("Townships - Alternate Source");
                    layernames.Add("Township Labels");
                    layernames.Add("Townships");
                } else if (wmsmaptype == "PLSS") {
                    //http://www.geocommunicator.gov/wmsconnector/com.esri.wms.Esrimap?ServiceName=BLM_MAP_PLSS
                    //wmsImageLayer = new WmsRasterLayer(new Uri("http://www.geocommunicator.gov/wmsconnector/com.esri.wms.Esrimap?ServiceName=BLM_MAP_PLSS&"));
                    wmsImageLayer = new WmsRasterLayer(new Uri("https://gis.blm.gov/arcgis/services/Cadastral/BLM_Natl_PLSS_CadNSDI/MapServer/WMSServer?"));
                    //wmsImageLayer = new WmsRasterLayer(new Uri("http://www.geocommunicator.gov/wmsconnector/com.esri.wms.Esrimap?ServiceName=BLM_MAP_PLSS"));
                    wmsImageLayer.UpperThreshold = double.MaxValue;
                    wmsImageLayer.LowerThreshold = double.MinValue;
                    wmsImageLayer.Open();
                    Collection<string> lnames = wmsImageLayer.GetServerLayerNames();
                    layernames.Add("0");
                    layernames.Add("1");
                    layernames.Add("2");
                    layernames.Add("3");

                    //layernames.Add("Meridians");
                    //layernames.Add("States");
                    //layernames.Add("Meridian Lines");
                    //layernames.Add("Base Lines");
                    //layernames.Add("Township Index Labels");
                    ////layernames.Add("Township Labels");
                    //layernames.Add("Range Index Labels");
                    //layernames.Add("Minor Subdivision");
                    //layernames.Add("Special Surveys");
                    //layernames.Add("Quarter-Quarters - Alternate Source");
                    //layernames.Add("Quarter-Quarters");
                    //layernames.Add("Sections - Alternate Source");
                    //layernames.Add("Sections");
                    //layernames.Add("Townships - Alternate Source");
                    //layernames.Add("Township Labels");
                    //layernames.Add("Townships");
                } else if (wmsmaptype == "TOPO") {
                    wmsImageLayer = new WmsRasterLayer(new Uri("http://basemap.nationalmap.gov/arcgis/services/USGSTopo/MapServer/WmsServer?"));
                    wmsImageLayer.UpperThreshold = double.MaxValue;
                    wmsImageLayer.LowerThreshold = double.MinValue;
                    wmsImageLayer.Open();
                    layernames = wmsImageLayer.GetServerLayerNames();
                }
                else if (wmsmaptype == "Hydrology") {
                    wmsImageLayer = new WmsRasterLayer(new Uri("http://basemap.nationalmap.gov/arcgis/services/USGSHydroNHD/MapServer/WmsServer?"));
                    //wmsImageLayer = new WmsRasterLayer(new Uri("http://basemap.nationalmap.gov/arcgis/services/USGSTopo/MapServer/WmsServer?"));
                    ////wmsImageLayer = new WmsRasterLayer(new Uri("http://services.nationalmap.gov/arcgis/services/USGSTopoLarge/MapServer/WMSServer"));
                    //wmsImageLayer = new WmsRasterLayer(new Uri("http://basemap.nationalmap.gov/arcgis/services/USGSImageryTopo/MapServer/WmsServer?"));
                    ////wmsImageLayer = new WmsRasterLayer(new Uri("http://services.nationalmap.gov/arcgis/services/USGSImageryTopoLarge/MapServer/WMSServer"));
                    ////wmsImageLayer = new WmsRasterLayer(new Uri("http://raster.nationalmap.gov/arcgis/services/Orthoimagery/USGS_EROS_Ortho_SCALE/ImageServer/WMSServer"));
                    ////wmsImageLayer = new WmsRasterLayer(new Uri("http://basemap.nationalmap.gov/arcgis/services/USGSImageryOnly/MapServer/WmsServer?"));
                    ////wmsImageLayer = new WmsRasterLayer(new Uri("http://services.nationalmap.gov/arcgis/services/USGSHydroNHDLarge/MapServer/WmsServer?"));
                    //wmsImageLayer = new WmsRasterLayer(new Uri("http://basemap.nationalmap.gov/arcgis/services/USGSShadedReliefOnly/MapServer/WmsServer?"));
                    ////wmsImageLayer = new WmsRasterLayer(new Uri("http://services.nationalmap.gov/arcgis/services/USGSShadedReliefLarge/MapServer/WmsServer?));
                    ////wmsImageLayer = new WmsRasterLayer(new Uri("http://services.nationalmap.gov/arcgis/services/TNM_Blank_US/MapServer/WmsServer?"));
                    ////wmsImageLayer = new WmsRasterLayer(new Uri("http://raster.nationalmap.gov/arcgis/services/Orthoimagery/USGS_EROS_Ortho_1Foot/ImageServer/WMSServer"));
                    ////wmsImageLayer = new WmsRasterLayer(new Uri("http://raster.nationalmap.gov/arcgis/services/Orthoimagery/USGS_EROS_Ortho_NAIP/ImageServer/WMSServer"));
                    ////wmsImageLayer = new WmsRasterLayer(new Uri("http://services.nationalmap.gov/arcgis/services/nhd/MapServer/WmsServer?"));
                    //wmsImageLayer = new WmsRasterLayer(new Uri("http://services.nationalmap.gov/arcgis/services/selectable_polygons/MapServer/WmsServer?"));
                    //wmsImageLayer = new WmsRasterLayer(new Uri("http://igems.doi.gov/arcgis/services/igems_haz/MapServer/WmsServer?"));
                    //wmsImageLayer = new WmsRasterLayer(new Uri("http://igems.doi.gov/arcgis/services/igems_info/MapServer/WmsServer?"));
                    ////wmsImageLayer = new WmsRasterLayer(new Uri("http://basemap.nationalmap.gov/arcgis/services/USGSTopo/MapServer/WmsServer?"));
                    ////wmsImageLayer = new WmsRasterLayer(new Uri("http://basemap.nationalmap.gov/arcgis/services/USGSTopo/MapServer/WmsServer?"));
                    ////wmsImageLayer = new WmsRasterLayer(new Uri("http://basemap.nationalmap.gov/arcgis/services/USGSTopo/MapServer/WmsServer?"));
                    ////wmsImageLayer = new WmsRasterLayer(new Uri("http://basemap.nationalmap.gov/arcgis/services/USGSTopo/MapServer/WmsServer?"));
                    ////wmsImageLayer = new WmsRasterLayer(new Uri("http://basemap.nationalmap.gov/arcgis/services/USGSTopo/MapServer/WmsServer?"));
                    ////wmsImageLayer = new WmsRasterLayer(new Uri("http://basemap.nationalmap.gov/arcgis/services/USGSTopo/MapServer/WmsServer?"));
                    ////wmsImageLayer = new WmsRasterLayer(new Uri("http://basemap.nationalmap.gov/arcgis/services/USGSTopo/MapServer/WmsServer?"));
                    ////wmsImageLayer = new WmsRasterLayer(new Uri("http://basemap.nationalmap.gov/arcgis/services/USGSTopo/MapServer/WmsServer?"));
                    ////wmsImageLayer = new WmsRasterLayer(new Uri("http://basemap.nationalmap.gov/arcgis/services/USGSTopo/MapServer/WmsServer?"));
                    ////wmsImageLayer = new WmsRasterLayer(new Uri("http://basemap.nationalmap.gov/arcgis/services/USGSTopo/MapServer/WmsServer?"));
                    wmsImageLayer.UpperThreshold = double.MaxValue;
                    wmsImageLayer.LowerThreshold = double.MinValue;
                    wmsImageLayer.Open();
                    layernames = wmsImageLayer.GetServerLayerNames();
                }
                //layernames = wmsImageLayer.GetServerLayerNames();
                //string projtext = wmsImageLayer.GetProjectionText();

                foreach (string layerName in layernames) {
                    wmsImageLayer.ActiveLayerNames.Add(layerName);
                }
                wmsImageLayer.Exceptions = "application/vnd.ogc.se_xml";
                wmsImageLayer.KeyColors.Add(new GeoColor(255, 255, 255, 255));
                //System.Collections.ObjectModel.Collection<string> keycolors = wmsImageLayer.KeyColors.;
                wmsImageLayer.DrawingExceptionMode = DrawingExceptionMode.DrawException;
                LayerOverlay staticOverlay = new LayerOverlay();
                staticOverlay.Layers.Add("wmsImageLayer", wmsImageLayer);
                Map.Overlays.Add(staticOverlay);
                wmsImageLayer.Close();

                Map.Refresh();


                ////wfsFeatureLayer.SendingWebRequest += (sender, e) => {
                ////    Uri uri = e.WebRequest.RequestUri;
                ////    string uristring = uri.AbsoluteUri;
                ////    if (uristring.Contains("SDMWGS84Geographic") && uristring.Contains("GetFeature")) {
                ////        string[] uriarray = uristring.Split(new string[] { @"propertyname=" }, StringSplitOptions.None);
                ////        string array1 = uriarray[0];
                ////        string array2 = uriarray[1];
                ////        string uristring2 = string.Format("{0}propertyname=({1})", array1, array2);
                ////        Uri uricustom = new Uri(uristring2);
                ////        System.Net.WebRequest wr = System.Net.WebRequest.Create(uricustom);
                ////        e.WebRequest = wr;
                ////    }
                ////};

                ////WfsFeatureSource featureSource = wfsFeatureLayer.FeatureSource as WfsFeatureSource;
                ////featureSource.RequestingData += (sender, e) => {
                ////    string servicurl = e.ServiceUrl;
                ////    string xmlrespons = e.XmlResponse;
                ////};
                ////featureSource.RequestedData += (sender, e) => {
                ////    string servicurl = e.ServiceUrl;
                ////    string xmlrespons = e.XmlResponse;
                ////};
                ////featureSource.SendingWebRequest += (sender, e) => {
                ////    string absoluturi = e.WebRequest.RequestUri.AbsoluteUri;
                ////};
                ////featureSource.SentWebRequest += (sender, e) => {
                ////    string absoluturi = e.Response.ResponseUri.AbsoluteUri;
                ////};



            }
            catch (Exception ex) {
                int i = 0;
            }
        }

        //private void SavePolygon() {
        //    EnsureFieldsLayerIsOpen();
        //    MultipolygonShape multipoly = new MultipolygonShape();
        //    Collection<Feature> selectedFeatures = fieldsLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);
        //    if (selectedFeatures.Count > 0) {
        //        for (int i = 0; i < fieldsLayer.InternalFeatures.Count; i++) {
        //            Feature feat = fieldsLayer.InternalFeatures[i];
        //            multipoly = feat.GetShape() as MultipolygonShape;
        //            break;
        //        }
        //        var importmessage = new ScalePolygonMessage() { ShapeBase = multipoly };
        //        Messenger.Default.Send<ScalePolygonMessage>(importmessage);
        //    }
        //}

        private void LoadBingMapsOverlay() {
            if (!startedOutOnline) { return; }
            //IMapOverlay mapoverlay = new BingMapOverlay();
            //SelectedOverlay.NewOverlay();
            IMapOverlay mapoverlay = new BingMapOverlay();
            Overlay bingOverlay = mapoverlay.Overlay;
            if (bingOverlay == null) { return; }
            if (Map.Overlays.Contains("Bing")) {
                Map.Overlays.Remove("Bing");
            }
            if (Map.Overlays.Contains("Bing Online")) {
                Map.Overlays.Remove("Bing Online");
            }
            if (Map.Overlays.Contains("Roads Only")) {
                Map.Overlays.Remove("Roads Only");
            }
            if (Map.Overlays.Contains("Road Online")) {
                Map.Overlays.Remove("Road Online");
            }
            if (Map.Overlays.Contains("None")) {
                Map.Overlays.Remove("None");
            }
            //bingOverlay.IsVisible = mapoverlay.Visibility;
            //((TileOverlay)bingOverlay).TileType = TileType.SingleTile;
            Map.Overlays.Add(mapoverlay.Name, bingOverlay);
            Map.Overlays.MoveToBottom(mapoverlay.Name);
        
        }

        //private void LoadWMSOverlay() {
        //    if (!startedOutOnline) { return; }
        //    IMapOverlay mapoverlay = new WorldMapKitOverlay();
        //    Overlay bingOverlay = mapoverlay.Overlay;
        //    if (bingOverlay == null) { return; }
        //    Map.Overlays.Add("WorldMapKit", bingOverlay);
        //    //Map.CurrentExtent = new RectangleShape(-155.733, 95.60, 104.42, -81.9);
        //    //extent = new RectangleShape(-126.4, 48.8, -67.0, 19.0);

        //}

        private void LoadFieldLayerShapes() {
            try {
                EnsureFieldsLayerIsOpen();
                fieldsLayer.InternalFeatures.Clear();
                //ieldsLayer.InternalFeatures.Add(SelectedField);
                foreach (var m in clientLayerItems) {
                    if (m.MapData != null && !string.IsNullOrEmpty(m.MapData) && m.MapData != "MULTIPOLYGON EMPTY") {
                        try {
                            IDictionary<string, string> columnvalues = new Dictionary<string, string>();
                            if (string.IsNullOrEmpty(m.Name)) { continue; }
                            if (string.IsNullOrEmpty(m.MapData)) { continue; }
                            if (string.IsNullOrEmpty(m.FieldId.ToString())) { continue; }

                            var labels = clientEndpoint.GetView<ItemMap>(new FieldId(ApplicationEnvironment.CurrentDataSourceId, m.FieldId));
                            if (labels.HasValue && labels.Value.MostRecentLabelItem != null) {
                                ItemLabelItem mrli = labels.Value.MostRecentLabelItem;
                                if (mrli != null && !mrli.OptionalVisibility) {
                                    continue;
                                }
                            }

                            string customarealabel = GetCustomFieldName(m.FieldId, m.Name);
                            columnvalues.Add(@"LdbLabel", m.Name);
                            columnvalues.Add(@"AreaLabel", customarealabel);
                            //MapItem
                            var testfeat = new Feature(m.MapData);
                            BaseShape testbase = testfeat.GetShape();
                            var f = new Feature(m.MapData, m.FieldId.ToString(), columnvalues);

                            if (fieldsLayer.InternalFeatures.Contains(f)) { continue; }
                            fieldsLayer.InternalFeatures.Add(m.FieldId.ToString(), f);

                            if (labels.HasValue && labels.Value.MostRecentLabelItem != null) {
                                ItemLabelItem mrli = labels.Value.MostRecentLabelItem;
                                if (mrli != null && !mrli.OptionalVisibility) {
                                    continue;
                                }
                                if (!string.IsNullOrWhiteSpace(mrli.SpatialLabelPosition)) {
                                    PointShape ps = new PointShape(mrli.SpatialLabelPosition);
                                    if (labelPositions.ContainsKey(m.FieldId.ToString())) {
                                        labelPositions[m.FieldId.ToString()] = ps;
                                    }
                                    else {
                                        labelPositions.Add(m.FieldId.ToString(), ps);
                                    }
                                }
                                if (rotationAngles.ContainsKey(m.FieldId.ToString())) {
                                    rotationAngles[m.FieldId.ToString()] = (double)mrli.HorizontalLabelRotation;
                                }
                                else {
                                    rotationAngles.Add(m.FieldId.ToString(), (double)mrli.HorizontalLabelRotation);
                                }
                            }

                        }
                        catch { }
                    }
                }
                scalingTextStyle.RotationAngles = rotationAngles;
                scalingTextStyle.LabelPositions = labelPositions;
                scalingTextStyle.MapScale = Map.CurrentScale;
                scalingTextStyle.PositionProjection = positionProjection;

                if (Map.Overlays.Contains("fieldOverlay")) {
                    Map.Overlays["fieldOverlay"].Refresh();
                }
            }
            catch { }
        }

        private void OpenAndLoadaShapefile() {
            HasUnsavedChanges = false;
            try {
                fieldsLayer.InternalFeatures.Clear();
                CreateInMemoryLayerAndOverlay();
                LoadShapeFileOverlayOntoMap();
                //if (Map.Overlays.Contains("importOverlay")) {
                //    if (((LayerOverlay)Map.Overlays["importOverlay"]).Layers.Contains("ImportLayer")) {
                //        Map.Refresh();
                //    }
                //}
            }
            catch { }
        }

        private void CreateInMemoryLayerAndOverlay() {
            layerOverlay = new LayerOverlay();
            layerOverlay.DrawingExceptionMode = DrawingExceptionMode.DrawException;
            fieldsLayer = new InMemoryFeatureLayer();

            scalingTextStyle = new ScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, mapsettings.ScalingTextMaximumSize, this.mapsettings.ScalingTextMinimumSize);
            if (displayareainfieldlabel) {
                scalingTextStyle.SetTexTColumnName(@"AreaLabel");
            }
            else {
                scalingTextStyle.SetTexTColumnName(@"LdbLabel");
            }
            fittingpolygonfactor = mapsettings.FittingPolygonFactor;
            if (mapsettings.AllowLabelOverlapping.HasValue) {
                allowlabeloverlapping = mapsettings.AllowLabelOverlapping.Value;
            }
            if (mapsettings.FittingPolygon.HasValue) {
                fittingpolygon = mapsettings.FittingPolygon.Value;
            }
            scalingTextStyle.UnselectedFieldVisibility = displayUnselectedFields;
            scalingTextStyle.LabelAllPolygonParts = labelallpolygonparts;
            scalingTextStyle.PolygonLabelingLocationMode = PolygonLabelingLocationMode.BoundingBoxCenter;
            scalingTextStyle.FittingPolygon = fittingpolygon;
            scalingTextStyle.FittingPolygonFactor = fittingpolygonfactor;
            if (allowlabeloverlapping) {
                scalingTextStyle.OverlappingRule = LabelOverlappingRule.AllowOverlapping;
            }
            else {
                scalingTextStyle.OverlappingRule = LabelOverlappingRule.NoOverlapping;
            }
            scalingTextStyle.RotationAngles = new Dictionary<string, double>();
            scalingTextStyle.LabelPositions = new Dictionary<string, PointShape>();
            scalingTextStyle.CustomLabels = new Dictionary<string, string>();
            scalingTextStyle.PositionProjection = positionProjection;

            //fieldsLayer
            fieldsLayer.FeatureSource.Open();
            FeatureSourceColumn column = new FeatureSourceColumn(@"LdbLabel", DbfColumnType.Character.ToString(), 64);
            FeatureSourceColumn column1 = new FeatureSourceColumn(@"AreaLabel", DbfColumnType.Character.ToString(), 64);
            fieldsLayer.Columns.Add(column);
            fieldsLayer.Columns.Add(column1);
            fieldsLayer.FeatureSource.Close();
            fieldsLayerAreaStyle = new ClientLayerAreaStyle();
            fieldsLayerAreaStyle.SelectedAreaStyle = new AreaStyle(new GeoPen(GeoColor.SimpleColors.Blue, 2), new GeoSolidBrush(new GeoColor(125, GeoColor.SimpleColors.Blue)));
            fieldsLayerAreaStyle.UnselectedAreaStyle = new AreaStyle(new GeoPen(GeoColor.StandardColors.Red, 2), new GeoSolidBrush(new GeoColor(125, GeoColor.StandardColors.Red)));
            fieldsLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(fieldsLayerAreaStyle);
            if (displayLabels) {
                fieldsLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(scalingTextStyle);
            }
            fieldsLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            if (loadbingmap) {
                fieldsLayer.FeatureSource.Projection = projection;
            }
            else {
                fieldsLayer.FeatureSource.Projection = projection84;
            }

            //fieldsLayer.FeatureSource.Projection = projection84;
            //fieldsLayer.FeatureSource.Projection = projection;
            //fieldsLayer.FeatureSource.Projection = projection84;

            scalingTextStyle.IsActive = true;
            scalingTextStyle.SelectedFeatures = new List<Feature>();
            fieldsLayerAreaStyle.SelectedFeatures = new List<Feature>();
            fieldsLayerAreaStyle.HiddenFeatures = new List<Feature>();
            layerOverlay.Layers.Add("ImportLayer", fieldsLayer);
        }

        private void LoadShapeFileOverlayOntoMap() {
            if (Map.Overlays.Contains("importOverlay")) {
                Map.Overlays.Remove("importOverlay");
            }
            if (layerOverlay.Layers.Contains("ImportLayer")) {
                Map.Overlays.Add("importOverlay", layerOverlay);
            }

            EnsureFieldsLayerIsOpen();
            //var extent = new RectangleShape(-143.4, 109.3, 116.7, -76.3);
            //TODO
            RectangleShape extent = new RectangleShape();
            //extent = new RectangleShape(-126.4, 48.8, -67.0, 19.0);
            //var extent = new RectangleShape(-126.4, 48.8, -67.0, 19.0);
            extent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
            if (loadbingmap) {
                extent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
            }
            else {
                extent = new RectangleShape(-126.4, 48.8, -67.0, 19.0);
            }

            try {
                //fieldsLayer.InternalFeatures.Add(SelectedField);
                if (ClientLayer.Count > 0 && fieldsLayer.InternalFeatures.Count == 0) {
                    LoadFieldLayerShapes();
                }
                var shapelayerfeatures = fieldsLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);
                if (shapelayerfeatures.Count > 0) {
                    var shapelayercolumns = fieldsLayer.FeatureSource.GetColumns();
                    extent = ExtentHelper.GetBoundingBoxOfItems(shapelayerfeatures);
                    extent.ScaleUp(100);
                    Map.CurrentExtent = extent;
                    PointShape center = extent.GetCenterPoint();
                    //Map.CenterAt(center);
                }
                else {
                    Map.CurrentExtent = extent;
                    PointShape center = extent.GetCenterPoint();
                    //Map.CenterAt(center);
                }
            }
            catch (Exception ex) {
                log.ErrorException("Caught an exception while working with PLSS", ex);
                //System.Windows.MessageBox.Show("ImportTools - Caught an exception while working with a simple draw shape.");
            }
        }

        void Cancel() {
            Map.Cursor = Cursors.Arrow;
            HasUnsavedChanges = false;
            fieldsLayer.InternalFeatures.Clear();
            var importmessage = new ShapeImportMessage() { ShapeBase = new MultipolygonShape() };
            Messenger.Default.Send<ShapeImportMessage>(importmessage);
        }

        private void AddNoaaWeatherLayer<T>(Action<T> setLayerPropertiesAction = null) where T : Layer, new() {
            LayerOverlay layerOverlay = GetLayerOverlay();
            if (!layerOverlay.Layers.Any(l => l is T)) {
                T layer = new T();
                if (setLayerPropertiesAction != null) {
                    setLayerPropertiesAction(layer);
                }
                layerOverlay.Layers.Add(layer);
                    layerOverlay.Refresh();
            }
        }

        private LayerOverlay GetLayerOverlay() {
            LayerOverlay layerOverlay = new LayerOverlay();
                layerOverlay = Map.Overlays.OfType<LayerOverlay>().FirstOrDefault();
                if (layerOverlay == null) {
                    layerOverlay = new LayerOverlay();
                    layerOverlay.DrawingExceptionMode = DrawingExceptionMode.DrawException;
                    Map.Overlays.Add(layerOverlay);
                    Map.Refresh(layerOverlay);
                }
            return layerOverlay;
        }
        private void CreateAnnotationMemoryLayer() {
            annotationOverlay = new LayerOverlay();
            annotationLayer = new InMemoryFeatureLayer();
            annotationLayer.FeatureSource.Open();
            FeatureSourceColumn column1 = new FeatureSourceColumn(@"Id", "string", 64);
            FeatureSourceColumn column2 = new FeatureSourceColumn(@"LdbLabel", "string", 64);
            FeatureSourceColumn column3 = new FeatureSourceColumn(@"TypeName", "string", 64);
            FeatureSourceColumn column4 = new FeatureSourceColumn(@"ShapeType", "string", 64);
            annotationLayer.Columns.Add(column1);
            annotationLayer.Columns.Add(column2);
            annotationLayer.Columns.Add(column3);
            annotationLayer.Columns.Add(column4);
            annotationLayer.FeatureSource.Close();
            annotationLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = null;
            annotationLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = null;
            annotationLayer.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = null;
            annotationLayer.ZoomLevelSet.ZoomLevel01.DefaultTextStyle = null;
            annotationLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Clear();
            AreaStyle areastyle = new AreaStyle(new GeoPen(new GeoColor(100, GeoColor.StandardColors.Transparent), 2), new GeoSolidBrush(new GeoColor(60, GeoColor.StandardColors.Transparent)));
            LineStyle linestyle = new LineStyle(new GeoPen(new GeoColor(100, GeoColor.StandardColors.Transparent), 2));
            PointStyle pointstyle = new PointStyle(PointSymbolType.Circle, new GeoSolidBrush(new GeoColor(100, GeoColor.StandardColors.Transparent)), 10);
            customMultiStyles = new MapStyles.AnnotationMultiStyle(areastyle, linestyle, pointstyle, annotationopacity);
            customMultiStyles.ColumnsList = new List<string>() { @"Id", @"LdbLabel", @"TypeName", @"ShapeType" };
            customMultiStyles.Opacity = annotationopacity;
            customMultiStyles.MaximumLabelSize = this.mapsettings.ScalingTextMaximumSize;
            customMultiStyles.MinimumLabelSize = this.mapsettings.ScalingTextMinimumSize;
            customMultiStyles.MaximumScale = 19000;
            customMultiStyles.MinimumScale = 1200;
            annotationLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customMultiStyles);
            if (mapannotationlabelsvisible) {
                annotationLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(scalingTextStyle2);
            }
            annotationLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            if (loadbingmap) {
                annotationLayer.FeatureSource.Projection = projection;
            }
            else {
                annotationLayer.FeatureSource.Projection = projection84;
            }
        }
        private void RebuildMapAnnotations() {
            var begin = DateTime.UtcNow;
            userstylenames = this.mapsettings.MapAnnotationCategories;
            //annotationopacity = this.mapsettings.MapAnnotationOpacity;
            customMultiStyles.Opacity = annotationopacity;
            if (userstylenames.Count > 0) {
                StyleData.Self.UserStyleNames = userstylenames;
                changedstyles = new List<AnnotationStyleItem>();
                foreach (KeyValuePair<string, MapAnnotationCategory> asi in userstylenames) {
                    AnnotationStyleItem annotatonstyle = styleFactory.GetStyle(asi.Value.Name, asi.Value.ShapeType);
                    annotatonstyle.Visible = asi.Value.Visible;
                    changedstyles.Add(annotatonstyle);
                }
                StyleData.Self.CustomStyles = changedstyles;
            }
            EnsureAnnotationLayerIsOpen();
            annotationLayer.InternalFeatures.Clear();
            if (!annotationvisibility) { return; }

            annotationLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = null;
            annotationLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = null;
            annotationLayer.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = null;
            annotationLayer.ZoomLevelSet.ZoomLevel01.DefaultTextStyle = null;
            annotationLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Clear();
            AreaStyle areastyle = new AreaStyle(new GeoPen(new GeoColor(100, GeoColor.StandardColors.Transparent), 2), new GeoSolidBrush(new GeoColor(60, GeoColor.StandardColors.Transparent)));
            LineStyle linestyle = new LineStyle(new GeoPen(new GeoColor(100, GeoColor.StandardColors.Transparent), 2));
            PointStyle pointstyle = new PointStyle(PointSymbolType.Circle, new GeoSolidBrush(new GeoColor(100, GeoColor.StandardColors.Transparent)), 10);
            customMultiStyles = new MapStyles.AnnotationMultiStyle(areastyle, linestyle, pointstyle, annotationopacity);
            customMultiStyles.ColumnsList = new List<string>() { @"Id", @"LdbLabel", @"TypeName", @"ShapeType" };
            customMultiStyles.Opacity = annotationopacity;
            customMultiStyles.MaximumLabelSize = this.mapsettings.ScalingTextMaximumSize;
            customMultiStyles.MinimumLabelSize = this.mapsettings.ScalingTextMinimumSize;
            customMultiStyles.MaximumScale = 19000;
            customMultiStyles.MinimumScale = 1200;
            annotationLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customMultiStyles);
            scalingTextStyle2 = new AnnotationScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, mapsettings.ScalingTextMaximumSize, this.mapsettings.ScalingTextMinimumSize);
            scalingTextStyle2.ColumnsList = new List<string>() { @"Id", @"LdbLabel", @"TypeName", @"ShapeType" };
            //scalingTextStyle2.YOffsetInPixel = 20;
            if (mapannotationlabelsvisible) {
                annotationLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(scalingTextStyle2);
            }
            annotationLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;

            var annotations = clientEndpoint.GetView<MapAnnotationListView>(new DataSourceId(ApplicationEnvironment.CurrentDataSourceId));
            List<MapAnnotationListItem> anlist = annotations.HasValue && annotations.Value.Annotations != null ? annotations.Value.Annotations : new List<MapAnnotationListItem>();
            IList<MapAnnotationDetailView> detaillist = new List<MapAnnotationDetailView>();
            IList<AnnotationStyleItem> stylelist = new List<AnnotationStyleItem>();
            foreach (var m in anlist) {
                if (m.Name != null && !string.IsNullOrEmpty(m.Name)) {
                    try {
                        var shapeItemMaybe = clientEndpoint.GetView<MapAnnotationDetailView>(m.Id);
                        MapAnnotationId mapAnnotationId = new MapAnnotationId();
                        string annotationName = string.Empty;
                        string annotationWktData = string.Empty;
                        string annotationShapeType = string.Empty;
                        string annotationType = string.Empty;
                        int? annotationCropYear = null;
                        shapeItemMaybe.IfValue(annotation => {
                            detaillist.Add(annotation);
                            string anid = annotation.Id.Id.ToString();
                            mapAnnotationId = annotation.Id;
                            annotationName = annotation.Name;
                            annotationWktData = annotation.WktData;
                            annotationShapeType = annotation.ShapeType;
                            annotationType = annotation.AnnotationType;
                            annotationCropYear = annotation.CropYear;
                            IDictionary<string, string> columnvalues = new Dictionary<string, string>();
                            columnvalues.Add(@"Id", anid);
                            columnvalues.Add(@"LdbLabel", annotationName);
                            columnvalues.Add(@"TypeName", annotationType);
                            columnvalues.Add(@"ShapeType", annotationShapeType);
                            Feature newfeature = new Feature(annotationWktData, anid, columnvalues);
                            //annotationLayer.InternalFeatures.Add(anid, newfeature);
                            var f = new Feature(annotationWktData, mapAnnotationId.Id.ToString(), columnvalues);
                            if (annotationLayer.InternalFeatures.Contains(anid)) {
                                annotationLayer.InternalFeatures.Remove(anid);
                            }
                            if (!annotationLayer.InternalFeatures.Contains(anid)) {
                                annotationLayer.InternalFeatures.Add(anid, newfeature);
                            }
                        });

                    }
                    catch { }
                }
            }
            var end = DateTime.UtcNow;
            log.Debug("Annotation Shapes load took " + (end - begin).TotalMilliseconds + "ms");
        }

        internal void EnsureAnnotationLayerIsOpen() {
            if (annotationLayer == null) {
                annotationLayer = new InMemoryFeatureLayer();
            }

            if (!annotationLayer.IsOpen) {
                annotationLayer.Open();
            }

            if (!projection.IsOpen) {
                projection.Open();
            }
        }
        private string GetCustomFieldName(Guid Id, string name) {
            string stringid = Id.ToString();
            string labelname = name;
            var fieldMaybe = clientEndpoint.GetView<FieldDetailsView>(new FieldId(ApplicationEnvironment.CurrentDataSourceId, Id));
            fieldMaybe.IfValue(field => {
                if (field.ReportedArea != null && !string.IsNullOrWhiteSpace(field.ReportedAreaUnit)) {
                    labelname = DisplaySelectedAreaDecimals(name, field.ReportedArea.Value, field.ReportedAreaUnit);
                }
                else if (field.BoundaryArea != null && !string.IsNullOrWhiteSpace(field.BoundaryAreaUnit)) {
                    labelname = DisplaySelectedAreaDecimals(name, field.BoundaryArea.Value, field.BoundaryAreaUnit);
                }
                else {
                    labelname = name + " " + Acre.Self.GetMeasure(0);
                }

            });
            return labelname;
        }

        private string GetCustomCropZoneName(Guid Id, string name) {
            string stringid = Id.ToString();
            string labelname = name;
            var fieldMaybe = clientEndpoint.GetView<CropZoneDetailsView>(new CropZoneId(ApplicationEnvironment.CurrentDataSourceId, Id));
            fieldMaybe.IfValue(field => {
                if (field.ReportedArea != null && !string.IsNullOrWhiteSpace(field.ReportedAreaUnit)) {
                    labelname = DisplaySelectedAreaDecimals(name, field.ReportedArea.Value, field.ReportedAreaUnit);
                }
                else if (field.BoundaryArea != null && !string.IsNullOrWhiteSpace(field.BoundaryAreaUnit)) {
                    labelname = DisplaySelectedAreaDecimals(name, field.BoundaryArea.Value, field.BoundaryAreaUnit);
                }
                else {
                    labelname = name + " " + Acre.Self.GetMeasure(0);
                }

            });
            return labelname;
        }

        private string DisplaySelectedAreaDecimals(string name, double unitvalue, string unitname) {
            IUnit _unit = UnitFactory.GetUnitByName(unitname);
            string decimalstring = "N" + areadecimals.ToString("N0");
            //  + "\r\n" + 
            return name + "\r\n" + unitvalue.ToString(decimalstring);
            //return name + ": " + unitvalue.ToString(decimalstring) + " " + _unit.AbbreviatedDisplay;
        }

        private void GetSelectedFeature() {
        }

    }
}
