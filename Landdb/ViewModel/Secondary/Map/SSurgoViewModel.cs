﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System.Windows.Input;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure.Messages;
using ThinkGeo.MapSuite.Core;
using Landdb.ViewModel.Secondary.Map.Nrcs;
using System.Collections.ObjectModel;
using AgC.UnitConversion.Length;
using AgC.UnitConversion;
using Landdb.Infrastructure;
using System.Diagnostics;
using Landdb.Resources;

namespace Landdb.ViewModel.Secondary.Map {
    public class SSurgoViewModel : ViewModelBase {
        bool maploaded = false;
        Dictionary<string, string> horizondictionary = new Dictionary<string, string>();
        string mukey = string.Empty;

        public SSurgoViewModel() {
            horizondictionary = new Dictionary<string, string>();
            horizondictionary.Add(@"A", @"Topsoil");
            horizondictionary.Add(@"B", @"Subsoil");
            horizondictionary.Add(@"C", @"Parent rock");
            horizondictionary.Add(@"D", @"unlike A & B");
            horizondictionary.Add(@"E", @"Eluvium");
            horizondictionary.Add(@"O", @"Organic mater");
            horizondictionary.Add(@"P", @"Humus(Peat)");
            horizondictionary.Add(@"R", @"Bedrock");
            horizondictionary.Add(@"L", @"Limic");
            horizondictionary.Add(@"a", @"Organic matter, highly decomposed");
            horizondictionary.Add(@"b", @"Buried soil horizon");
            horizondictionary.Add(@"c", @"Concretions or nodules");
            horizondictionary.Add(@"d", @"Dense unconsolidated materials");
            horizondictionary.Add(@"e", @"Organic matter, intermediate decomposition");
            horizondictionary.Add(@"f", @"Frozen soil");
            horizondictionary.Add(@"g", @"Strong gleying (mottling)");
            horizondictionary.Add(@"h", @"Illuvial accumulation of organic matter");
            horizondictionary.Add(@"i", @"Organic matter, slightly decomposed");
            horizondictionary.Add(@"j", @"Jarosite (yellow sulfate mineral)");
            horizondictionary.Add(@"jj", @"Cryoturbation (frost churning)");
            horizondictionary.Add(@"k", @"Accumulation of carbonates");
            horizondictionary.Add(@"m", @"Cementation or Induration");
            horizondictionary.Add(@"n", @"Accumulation of sodium");
            horizondictionary.Add(@"o", @"Accumulation of Fe and Al oxides");
            horizondictionary.Add(@"p", @"Plowing or other disturbance");
            horizondictionary.Add(@"q", @"Accumulation of silica");
            horizondictionary.Add(@"r", @"Weathered or soft bedrock");
            horizondictionary.Add(@"s", @"Illuvial organic matter and Fe and Al oxides");
            horizondictionary.Add(@"ss", @"Slickensides (shiny clay wedges)");
            horizondictionary.Add(@"t", @"Accumulation of silicate clays");
            horizondictionary.Add(@"u", @"Presence of human-manufactured materials (artifacts)");
            horizondictionary.Add(@"v", @"Plinthite (high iron, red material)");
            horizondictionary.Add(@"w", @"Distinctive color or structure without clay accumulation");
            horizondictionary.Add(@"x", @"Fragipan (high bulk density, brittle)");
            horizondictionary.Add(@"y", @"Accumulation of gypsum");
            horizondictionary.Add(@"z", @"Accumulation of soluble salts");
            CancelCommand = new RelayCommand(() => {
                Messenger.Default.Send<ShowSSurgoMessage>(new ShowSSurgoMessage());
            });
            SearchWikipediaCommand = new RelayCommand(onSearchWikipedia);

        }

        public ICommand CancelCommand { get; private set; }
        public ICommand SearchWikipediaCommand { get; private set; }

        public void Execute(string cropname, Feature cookieCutFeature, Feature ssurgoFeature) {
            Execute(cropname, cookieCutFeature, ssurgoFeature, "");
        }

        public void Execute(string cropname, Feature cookieCutFeature, Feature ssurgoFeature, string mukey) {
            this.cropname = cropname;
            this.cookieCutFeature = cookieCutFeature;
            this.ssurgoFeature = ssurgoFeature;
            this.mukey = mukey;
            mapUnitComponentList = new ObservableCollection<MapUnitComponent>();
            cHORIZONList = new ObservableCollection<CHORIZON>();
            coCROPYLList = new ObservableCollection<COCROPYL>();
            coMONTHList = new ObservableCollection<COMONTH>();
            RaisePropertyChanged("CropName");
            RaisePropertyChanged("Feature");
            RaisePropertyChanged("SSurgoFeature");

            try {
                GeoCollection<Feature> cookieCutFeatures = new GeoCollection<Feature>();
                cookieCutFeatures.Add(cookieCutFeature);

                Landdb.ViewModel.Secondary.Map.Nrcs.Component comp = new Nrcs.Component(cropname, cookieCutFeatures);
                comp.DataList[0].Componentexploded = true;
                MapUnitComponentList = comp.DataList;
                MapUnitComponentItem = MapUnitComponentList[0];
                Symbol = cookieCutFeature.ColumnValues["Musym"].Trim();
                Name = cookieCutFeature.ColumnValues["Muname"].Trim();

                double areaacres = System.Convert.ToDouble(cookieCutFeature.ColumnValues["Muarea"].Trim());
                coveragearea = areaacres;
                double coverageacres = System.Convert.ToDouble(cookieCutFeature.ColumnValues["Percover"].Trim());
                coveragepercent = coverageacres;
                Area = areaacres.ToString("N2") + $" {UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).AbbreviatedDisplay.ToLower()}   " + coverageacres.ToString("N0") + "%";
                sandtotal_r = 0;
                silttotal_r = 0;
                claytotal_r = 0;

                if (cHORIZONList.Count > 1) {
                    sandtotal_r = System.Convert.ToDouble(CHORIZONItem.Sandtotal_r);
                    silttotal_r = System.Convert.ToDouble(CHORIZONItem.Silttotal_r);
                    claytotal_r = System.Convert.ToDouble(CHORIZONItem.Claytotal_r);
                }

            }
            catch (Exception ex) {
                //log.ErrorException("SSurgoViewModel - Couldn't load the field shape.", ex);
                System.Windows.MessageBox.Show($"{Strings.Problem_LoadingFieldShape_Text} {Strings.OperationAborted_Text}");

            }
            //AreaPercent = coverageacres;
            //LengthMeasure meters = Meter.Self.GetMeasure(System.Convert.ToDouble(MapUnitComponentItem.Slopelenusle_r)) as LengthMeasure;
            //Measure feet = meters.CreateAs(AgC.UnitConversion.Length.Foot.Self);
            //SlopeLengthMeters = System.Convert.ToDouble(MapUnitComponentItem.Slopelenusle_r);
            //SlopeLength = feet.Value;
            //SlopePercent = System.Convert.ToDouble(MapUnitComponentItem.Slope_r);
            //Hydgrp = MapUnitComponentItem.Hydgrp;

            //public double TotalArea
        }
        private void onSearchWikipedia() {
            var searchTerm = string.Empty;

            if (!string.IsNullOrWhiteSpace(mukey)) {
                searchTerm = mukey;
                string browserPath = ApplicationEnvironment.GetSystemDefaultBrowserPath();

                //if (!string.IsNullOrWhiteSpace(browserPath)) {
                //    Process.Start(browserPath, $"\"? {searchTerm}\"");
                //}
                //else {
                    //searchTerm = searchTerm.Replace(' ', '_');
                    var url = $@"http://maps.communitycommons.org/viewer/mureport.aspx?mukey={searchTerm}"; // TODO: Allow for culture to drive the URL as well
                    //http://maps.communitycommons.org/viewer/mureport.aspx?mukey=888492
                    Process.Start(url);
                //}
            }
        }

        string cropname = string.Empty;
        public string CropName {
            get { return cropname; }
            set {
                if (cropname == value) { return; }
                cropname = value;
                RaisePropertyChanged("CropName");
            }
        }

        Feature cookieCutFeature = null;
        public Feature CookieCutFeature {
            get { return cookieCutFeature; }
            set {
                if (cookieCutFeature == value) { return; }
                cookieCutFeature = value;
                RaisePropertyChanged("Feature");
            }
        }

        Feature ssurgoFeature = null;
        public Feature SSurgoFeature {
            get { return ssurgoFeature; }
            set {
                if (ssurgoFeature == value) { return; }
                ssurgoFeature = value;
                RaisePropertyChanged("SSurgoFeature");
            }
        }


        private ObservableCollection<MapUnitComponent> mapUnitComponentList = new ObservableCollection<MapUnitComponent>();
        public ObservableCollection<MapUnitComponent> MapUnitComponentList {
            get { return mapUnitComponentList; }
            set {
                if (mapUnitComponentList == value) { return; }
                mapUnitComponentList = value;
                RaisePropertyChanged("MapUnitComponentList");
            }
        }

        private MapUnitComponent mapUnitComponentItem = new MapUnitComponent();
        public MapUnitComponent MapUnitComponentItem {
            get { return mapUnitComponentItem; }
            set {
                if (mapUnitComponentItem == value) { return; }
                mapUnitComponentItem = value;
                RaisePropertyChanged("MapUnitComponentItem");
                if (mapUnitComponentItem == null) { return; }
                if (string.IsNullOrEmpty(mapUnitComponentItem.Cokey)) { return; }
                GeoCollection<Feature> cookieCutFeatures = new GeoCollection<Feature>();
                cookieCutFeatures.Add(this.cookieCutFeature);

                Landdb.ViewModel.Secondary.Map.Nrcs.ComponentCropYield compyld = new Nrcs.ComponentCropYield(this.cropname, cookieCutFeatures, mapUnitComponentItem.Cokey);
                COCROPYLList = compyld.DataList;
                if (COCROPYLList.Count > 0) {
                    COCROPYLItem = COCROPYLList[0];
                }

                //Landdb.ViewModel.Secondary.Map.Nrcs.ComponenetMonth compmonth = new Nrcs.ComponenetMonth(this.cropname, cookieCutFeatures, mapUnitComponentItem.Cokey);
                //COMONTHList = compmonth.DataList;
                //if (COMONTHList.Count > 0) {
                //    COMONTHItem = COMONTHList[0];
                //}

                Landdb.ViewModel.Secondary.Map.Nrcs.ComponentHorizon comphoriz = new Nrcs.ComponentHorizon(this.cropname, cookieCutFeatures, mapUnitComponentItem.Cokey);
                if (comphoriz.DataList.Count > 0) {
                    comphoriz.DataList[0].Horizonexploded = true;
                }
                if (comphoriz.DataList.Count > 1) {
                    comphoriz.DataList[1].Horizonexploded = true;
                }
                //if (comphoriz.DataList.Count > 2) {
                //    comphoriz.DataList[2].Horizonexploded = true;
                //}
                for (int j = 0; j < comphoriz.DataList.Count; j++) {
                    StringBuilder sb = new StringBuilder();
                    CHORIZON ch = comphoriz.DataList[j];
                    string matchingdescription = string.Empty;
                    for (int i = 0; i < ch.Hzname.Length; i++) {
                        matchingdescription = string.Empty;
                        string shortstring = ch.Hzname.Substring(i, 1);
                        if (horizondictionary.ContainsKey(shortstring)) {
                            matchingdescription = horizondictionary[shortstring];
                        }
                        int k = i + 2;
                        int m = ch.Hzname.Length;
                        if (k <= m) {
                            string shortstring2 = ch.Hzname.Substring(i, 2);
                            if (horizondictionary.ContainsKey(shortstring2)) {
                                matchingdescription = horizondictionary[shortstring2];
                            }
                        }
                        if (!string.IsNullOrEmpty(matchingdescription)) {
                            if (sb.Length > 0) {
                                sb.Append("\n");
                            }
                            sb.Append(matchingdescription);
                        }
                    }
                    //sb.Append("\n");
                    comphoriz.DataList[j].Description = sb.ToString();
                }

                CHORIZONList = comphoriz.DataList;
                if (CHORIZONList.Count > 0) {
                    CHORIZONItem = CHORIZONList[0];
                }
            }
        }
        

        private ObservableCollection<CHORIZON> cHORIZONList = new ObservableCollection<CHORIZON>();
        public ObservableCollection<CHORIZON> CHORIZONList {
            get { return cHORIZONList; }
            set {
                if (cHORIZONList == value) { return; }
                cHORIZONList = value;
                RaisePropertyChanged("CHORIZONList");
                RaisePropertyChanged("CHORIZONListRev");
            }
        }

        public ObservableCollection<CHORIZON> CHORIZONListRev {
            get { return new ObservableCollection<CHORIZON>(cHORIZONList.OrderByDescending(x => x.Hzdept_r_int)); }
        }

        private CHORIZON cHORIZONItem = new CHORIZON();
        public CHORIZON CHORIZONItem {
            get { return cHORIZONItem; }
            set {
                if (cHORIZONItem == value) { return; }
                cHORIZONItem = value;
                RaisePropertyChanged("CHORIZONItem");
            }
        }

        private ObservableCollection<COCROPYL> coCROPYLList = new ObservableCollection<COCROPYL>();
        public ObservableCollection<COCROPYL> COCROPYLList {
            get { return coCROPYLList; }
            set {
                if (coCROPYLList == value) { return; }
                coCROPYLList = value;
                RaisePropertyChanged("COCROPYLList");
            }
        }

        private COCROPYL coCROPYLItem = new COCROPYL();
        public COCROPYL COCROPYLItem {
            get { return coCROPYLItem; }
            set {
                if (coCROPYLItem == value) { return; }
                coCROPYLItem = value;
                RaisePropertyChanged("COCROPYLItem");
            }
        }

        private ObservableCollection<COMONTH> coMONTHList = new ObservableCollection<COMONTH>();
        public ObservableCollection<COMONTH> COMONTHList {
            get { return coMONTHList; }
            set {
                if (coMONTHList == value) { return; }
                coMONTHList = value;
                RaisePropertyChanged("COMONTHList");
            }
        }

        private COMONTH coMONTHItem = new COMONTH();
        public COMONTH COMONTHItem {
            get { return coMONTHItem; }
            set {
                if (coMONTHItem == value) { return; }
                coMONTHItem = value;
                RaisePropertyChanged("COMONTHItem");
            }
        }



        string symbol = string.Empty;
        public string Symbol {
            get { return symbol; }
            set {
                if (symbol == value) { return; }
                symbol = value;
                RaisePropertyChanged("Symbol");
            }
        }

        string name = string.Empty;
        public string Name {
            get { return name; }
            set {
                if (name == value) { return; }
                name = value;
                RaisePropertyChanged("Name");
            }
        }

        string area = string.Empty;
        public string Area {
            get { return area; }
            set {
                if (area == value) { return; }
                area = value;
                RaisePropertyChanged("Area");
            }
        }

        double areapercent = 0;
        public double AreaPercent {
            get { return areapercent; }
            set {
                if (areapercent == value) { return; }
                areapercent = value;
                RaisePropertyChanged("AreaPercent");
            }
        }

        public string CoverageAreaString {
            get { return CoverageArea.ToString("N2"); }
        }

        public string CoveragePercentString {
            get { return CoveragePercent.ToString("N0"); }
        }

        double coveragearea = 0;
        public double CoverageArea {
            get { return coveragearea; }
            set {
                if (coveragearea == value) { return; }
                coveragearea = value;
                RaisePropertyChanged("CoverageArea");
                RaisePropertyChanged("CoverageAreaString");
            }
        }

        double coveragepercent = 0;
        public double CoveragePercent {
            get { return coveragepercent; }
            set {
                if (coveragepercent == value) { return; }
                coveragepercent = value;
                RaisePropertyChanged("CoveragePercent");
                RaisePropertyChanged("CoveragePercentString");
            }
        }

        double totalarea = 0;
        public double TotalArea {
            get { return totalarea; }
            set {
                if (totalarea == value) { return; }
                totalarea = value;
                RaisePropertyChanged("TotalArea");
            }
        }

        double slopelengthmeters = 0;
        public double SlopeLengthMeters {
            get { return slopelengthmeters; }
            set {
                if (slopelengthmeters == value) { return; }
                slopelengthmeters = value;
                RaisePropertyChanged("SlopeLengthMeters");
            }
        }

        public string SlopeLengthString {
            get { return SlopeLength.ToString("N0"); }
        }

        double slopelength = 0;
        public double SlopeLength {
            get { return slopelength; }
            set {
                if (slopelength == value) { return; }
                slopelength = value;
                RaisePropertyChanged("SlopeLength");
                RaisePropertyChanged("SlopeLengthString");
            }
        }

        double slopepercent = 0;
        public double SlopePercent {
            get { return slopepercent; }
            set {
                if (slopepercent == value) { return; }
                slopepercent = value;
                RaisePropertyChanged("SlopePercent");
            }
        }

        string soiltype = string.Empty;
        public string SoilType {
            get { return soiltype; }
            set {
                if (soiltype == value) { return; }
                soiltype = value;
                RaisePropertyChanged("SoilType");
            }
        }
 
        double sandtotal_r = 0;
        public double SandTotal_r {
            get { return sandtotal_r; }
            set {
                if (sandtotal_r == value) { return; }
                sandtotal_r = value;
                RaisePropertyChanged("SandTotal_r");
            }
        }

        double silttotal_r = 0;
        public double SiltTotal_r {
            get { return silttotal_r; }
            set {
                if (silttotal_r == value) { return; }
                silttotal_r = value;
                RaisePropertyChanged("SiltTotal_r");
            }
        }

        double claytotal_r = 0;
        public double ClayTotal_r {
            get { return claytotal_r; }
            set {
                if (claytotal_r == value) { return; }
                claytotal_r = value;
                RaisePropertyChanged("ClayTotal_r");
            }
        }

        string hydgrp = string.Empty;
        public string Hydgrp {
            get { return hydgrp; }
            set {
                if (hydgrp == value) { return; }
                hydgrp = value;
                RaisePropertyChanged("Hydgrp");
            }
        }
    }
}
