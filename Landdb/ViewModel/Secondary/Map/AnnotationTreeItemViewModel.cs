﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GalaSoft.MvvmLight;
using Landdb.Domain.ReadModels.Tree;
using System.Collections.ObjectModel;
using Landdb.Client.Infrastructure;
using Landdb.ViewModel.Secondary.Map;
using Landdb.Domain.ReadModels.MapAnnotation;

namespace Landdb.ViewModel.Secondary.Map {
    public class AnnotationTreeItemViewModel : AbstractTreeItemViewModel {
        public string Name { get; }
        public override IIdentity Id => null;
        public AnnotationTreeItemViewModel(string name, Dictionary<string, MapAnnotationCategory> categories, List<MapAnnotationListItem> annotations, bool sortChildrenAlphabetically, Action<AbstractTreeItemViewModel> onCheckedChanged = null, Predicate<object> filter = null)
            : base(null, onCheckedChanged, null, filter) {
            Name = name;

            var annotationCategoryList = from eachCategory in categories.ToList()
                    select eachCategory;

            Children = new ObservableCollection<AbstractTreeItemViewModel>();
            foreach (KeyValuePair<string, MapAnnotationCategory> annotationCategory in annotationCategoryList) {
                var annotationsCreatedByUser = from eachAnnotation in annotations
                        where eachAnnotation.AnnotationType == annotationCategory.Value.Name
                        select eachAnnotation;

                Children.Add(new AnnotationStyleTreeItemViewModel(annotationCategory.Value, annotationsCreatedByUser.ToList(), this, sortChildrenAlphabetically, onCheckedChanged, filter));
            }
        }

        public override string ToString() {
            return Name;
        }
    }
}
