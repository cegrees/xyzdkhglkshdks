﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GalaSoft.MvvmLight;
using ThinkGeo.MapSuite.WpfDesktopEdition;
using ThinkGeo.MapSuite.Core;
using Landdb.Infrastructure;
using System.IO;
using Microsoft.Win32;
using GalaSoft.MvvmLight.Command;
using System.Windows.Input;
using System.Collections.ObjectModel;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure.Messages;
using Landdb.Domain.ReadModels.Tree;
using Landdb.MapStyles;
using Landdb.Resources;
using Landdb.ViewModel.Maps.ImageryOverlay;
using Landdb.ViewModel.Fields.FieldDetails.MapModels;
using NLog;

namespace Landdb.ViewModel.Secondary.Map {
    public class CustomLabelViewModel : ViewModelBase {
        bool maploaded = false;
        bool startedOutOnline = false;
        Logger log = LogManager.GetCurrentClassLogger();
        //IMapOverlay selectedoverlay;
        bool ismousedown = false;
        bool draglabel = true;
        bool hasunsavedchanges = false;
        bool cansavechanges = false;
        bool isfieldselected = false;
        bool isCropZone = false;
        string fieldname = Strings.DragLabel_Text;
        double labelangle = 0;
        double slidervalue = 0;
        PointShape worldlocation = new PointShape();
        IList<MapItem> clientLayerItems;
        InMemoryFeatureLayer fieldsLayer = new InMemoryFeatureLayer();
        LayerOverlay layerOverlay = new LayerOverlay();
        Proj4Projection projection;
        Feature selectedfield = new Feature();
        Feature selectedfeature = new Feature();
        Cursor previousCursor;
        private Dictionary<string, double> rotationAngles = new Dictionary<string, double>();
        private Dictionary<string, PointShape> labelPositions = new Dictionary<string, PointShape>();
        ScalingTextStyle scalingTextStyle = new ScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, 24, 2);
        ClientLayerAreaStyle fieldsLayerAreaStyle = null;
        ZoomLevelFactory zoomlevelfactory = new ZoomLevelFactory();

        public CustomLabelViewModel() {
            //var locator = System.Windows.Application.Current.TryFindResource("Locator") as ViewModelLocator;
            //selectedoverlay = locator.ThinkGeoUtitlityVM.GetSelectedMapFromFactory("Bing");
            //selectedoverlay = new BingMapOverlay();

            scalingTextStyle.MandateOverlapping = true;
            InitializeMap();

            HasUnsavedChanges = false;

            MoveLabelCommand = new RelayCommand(MoveLabel);
            CancelCommand = new RelayCommand(OnCancelCropzone);
            CompleteCommand = new RelayCommand(OnCompleteCropzone);

        }

        public ICommand MoveLabelCommand { get; private set; }
        public ICommand RotateLabelCommand { get; private set; }
        public ICommand CompleteCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }

        public WpfMap Map { get; set; }

        public IList<MapItem> ClientLayer {
            get { return clientLayerItems; }
            set {
                clientLayerItems = value;
                LoadFieldLayerShapes();
                cansavechanges = true;
                RaisePropertyChanged("ClientLayer");
            }
        }

        public Feature SelectedField {
            get { return selectedfield; }
            set {
                if (selectedfield == value) { return; }
                selectedfield = value;
                if (selectedfield == null) { return; }
                if (!maploaded) { return; }

                if (string.IsNullOrEmpty(selectedfield.Id)) { return; }
                if (fieldsLayerAreaStyle == null) {
                    CreateInMemoryLayerAndOverlay();
                }
                fieldsLayerAreaStyle.SelectedFeatures.Clear();
                fieldsLayerAreaStyle.SelectedFeatures.Add(selectedfield);
                isfieldselected = true;
                if (RotationAngles.ContainsKey(selectedfield.Id)) {
                    slidervalue = GetSlideValueFromLabelAngle(RotationAngles[selectedfield.Id]);
                    RaisePropertyChanged("SliderValue");
                }
                if (LabelPositions.ContainsKey(selectedfield.Id)) {
                    WorldLocation = LabelPositions[selectedfield.Id];
                }
                //else {
                //    WorldLocation = new PointShape();
                //}

                scalingTextStyle.LabelPositions = labelPositions;
                scalingTextStyle.RotationAngles = rotationAngles;
                scalingTextStyle.MapScale = Map.CurrentScale;

                if (Map.Overlays.Contains("importOverlay")) {
                    scalingTextStyle.MapScale = Map.CurrentScale;
                    Map.Overlays["importOverlay"].Refresh();
                }
                RaisePropertyChanged("SelectedField");
                RaisePropertyChanged("SliderValue");
                RaisePropertyChanged("WorldLocation");
                RaisePropertyChanged("DisplayLabelPosition");
            }
        }

        public Feature SelectedFeature {
            get { return selectedfeature; }
            set {
                selectedfeature = value;
                RetrieveSelectedFieldBoundingBox();
                RaisePropertyChanged("SelectedFeature");
            }
        }

        public string FieldName {
            get { return fieldname; }
            set {
                if (fieldname == value) { return; }
                fieldname = value;
                RaisePropertyChanged("FieldName");
            }
        }

        public bool HasUnsavedChanges {
            get { return hasunsavedchanges; }
            set {
                if (hasunsavedchanges == value) { return; }
                hasunsavedchanges = value;
                RaisePropertyChanged("HasUnsavedChanges");
            }
        }

        public bool CanSaveChanges {
            get { return cansavechanges; }
            set {
                if (cansavechanges == value) { return; }
                cansavechanges = value;
                RaisePropertyChanged("CanSaveChanges");
            }
        }

        public bool IsCropZone {
            get { return isCropZone; }
            set {
                if (isCropZone == value) { return; }
                isCropZone = value;
                RaisePropertyChanged("IsCropZone");
            }
        }

        public double SliderValue {
            get { return slidervalue; }
            set {
                if (slidervalue == value) { return; }
                slidervalue = value;
                LabelAngle = GetLabelAngleFromSliderValue(slidervalue);
                RaisePropertyChanged("SliderValue");
                RaisePropertyChanged("LabelAngle");
            }
        }

        public double LabelAngle {
            get { return labelangle; }
            set {
                if (labelangle == value) { return; }
                labelangle = value;
                if (SelectedField == null) { return; }
                if (string.IsNullOrEmpty(SelectedField.Id)) { return; }
                if (RotationAngles.ContainsKey(SelectedField.Id)) {
                    RotationAngles[SelectedField.Id] = labelangle;
                    HasUnsavedChanges = true;
                }
                else {
                    RotationAngles.Add(SelectedField.Id, labelangle);
                    HasUnsavedChanges = true;
                }
                scalingTextStyle.RotationAngles = RotationAngles;
                scalingTextStyle.MapScale = Map.CurrentScale;
                if (Map.Overlays.Contains("importOverlay")) {
                    Map.Overlays["importOverlay"].Refresh();
                }
                RaisePropertyChanged("LabelAngle");
            }
        }

        public PointShape WorldLocation {
            get { return worldlocation; }
            set {
                if (worldlocation == value) { return; }
                worldlocation = value;
                if (SelectedField == null) { return; }
                if (string.IsNullOrEmpty(SelectedField.Id)) { return; }
                if (!LabelPositions.ContainsKey(SelectedField.Id)) {
                    LabelPositions.Add(SelectedField.Id, worldlocation);
                    HasUnsavedChanges = true;
                    scalingTextStyle.LabelPositions = labelPositions;
                }
                else {
                    LabelPositions[SelectedField.Id] = worldlocation;
                    HasUnsavedChanges = true;
                }
                scalingTextStyle.LabelPositions = labelPositions;
                scalingTextStyle.MapScale = Map.CurrentScale;
                if (Map.Overlays.Contains("importOverlay")) {
                    Map.Overlays["importOverlay"].Refresh();
                }
                RaisePropertyChanged("WorldLocation");
                RaisePropertyChanged("DisplayLabelPosition");
            }
        }

        public string DisplayLabelPosition {
            get {
                if (worldlocation == null) { return string.Empty; }
                if (worldlocation.X == 0 || worldlocation.Y == 0) { return string.Empty; }
                PointShape latlonlocation = projection.ConvertToInternalProjection(worldlocation) as PointShape;
                Vertex geoVertex1 = new Vertex(latlonlocation.X, latlonlocation.Y);
                string _DecimalDegreeString = Math.Round(geoVertex1.Y, 5) + ", " + Math.Round(geoVertex1.X, 5);
                return _DecimalDegreeString;
            }
        }


        public Dictionary<string, double> RotationAngles {
            get { return rotationAngles; }
            set { rotationAngles = value; }
        }

        public Dictionary<string, PointShape> LabelPositions {
            get { return labelPositions; }
            set { labelPositions = value; }
        }

        public void ResetLabels() {
            worldlocation = new PointShape();
            LabelPositions = new Dictionary<string, PointShape>();
            RotationAngles = new Dictionary<string, double>();
            labelPositions = new Dictionary<string, PointShape>();
            rotationAngles = new Dictionary<string, double>();
            fieldsLayer.InternalFeatures.Clear();
            scalingTextStyle.LabelPositions = new Dictionary<string, PointShape>();
            scalingTextStyle.RotationAngles = new Dictionary<string, double>();
            if (fieldsLayerAreaStyle != null) {
                fieldsLayerAreaStyle.SelectedFeatures.Clear();
            }
            Map.Refresh();
        }
        //public IMapOverlay SelectedOverlay {
        //    get { return selectedoverlay; }
        //    set {
        //        if (selectedoverlay == value) { return; }
        //        if (selectedoverlay == null) { return; }
        //        if (string.IsNullOrWhiteSpace(value.Name)) { return; }
        //        selectedoverlay = value;
        //        //LoadBingMapsOverlay();
        //        //Map.Refresh();
        //        RaisePropertyChanged("SelectedOverlay");
        //    }
        //}

        private double GetLabelAngleFromSliderValue(double slidval) {
            double labangl = 0;
            if (slidval >= 0 && slidval <= 90) {
                labangl = slidervalue;
            }
            else {
                labangl = 360 + slidval;
            }
            //if (slidval >= 0 && slidval <= 90) {
            //    labangl = slidval;
            //}
            //else if (slidval >= -90 && slidval < 0) {
            //    labangl = 360 - slidval;
            //}
            //else if (slidval > 90) {
            //    labangl = 90;
            //}
            //else if (slidval < -90) {
            //    labangl = 270;
            //}
            return labangl;
        }

        private double GetSlideValueFromLabelAngle(double labangl) {
            double slidval = 0;
            if (labangl >= 0 && labangl <= 90) {
                slidval = labangl;
            }
            else if (labangl >= 270 && labangl <= 360) {
                //slidval = 360 + labangl;
                slidval = labangl - 360;
            }
            else if (labangl < 0) {
                slidval = 0;
            }
            else if (labangl > 360) {
                slidval = 90;
            }
            else if (labangl > 90 && labangl < 270) {
                slidval = 0;
            }
            return slidval;
        }

        internal void EnsureFieldsLayerIsOpen() {
            if (fieldsLayer == null) {
                fieldsLayer = new InMemoryFeatureLayer();
            }

            if (!fieldsLayer.IsOpen) {
                fieldsLayer.Open();
            }

            if (!projection.IsOpen) {
                projection.Open();
            }
        }

        void InitializeMap() {
            Map = new WpfMap();
            startedOutOnline = Landdb.Client.Infrastructure.NetworkStatus.IsInternetAvailableFast();
            Map.MapUnit = GeographyUnit.Meter;
            Map.MapTools.PanZoomBar.Visibility = System.Windows.Visibility.Visible;
            Map.MapTools.PanZoomBar.GlobeButtonVisibility = System.Windows.Visibility.Hidden;
            Map.MapTools.PanZoomBar.VerticalAlignment = System.Windows.VerticalAlignment.Center;
            Map.MapTools.PanZoomBar.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            //Map.Background = System.Windows.Media.Brushes.Aquamarine;
            Map.Background = System.Windows.Media.Brushes.White;
            zoomlevelfactory = new ZoomLevelFactory();
            Map.ZoomLevelSet = zoomlevelfactory.ZoomLevelSET;

            Map.MouseLeftButtonDown += (sender, e) => {
                ((MainWindow)System.Windows.Application.Current.MainWindow).Draggable = false;
            };

            projection = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetGoogleMapParametersString());

            Map.Loaded += (sender, e) => {
                if (maploaded) { return; }
                LoadBingMapsOverlay();
                OpenAndLoadaShapefile();
                maploaded = true;
                RetrieveSelectedFieldBoundingBox();

                if (fieldsLayer.InternalFeatures.Count > 0 && isCropZone) {
                    SelectedField = fieldsLayer.InternalFeatures[0];
                }
                HighlightSelectedField();
                log.Info("LabelDisplay - Map Loaded.");
            };
            Map.CurrentScaleChanged += (sender, e) => {
                scalingTextStyle.MapScale = e.CurrentScale;
            };

            Map.MouseDown += (sender, e) => {
                ismousedown = true;
                if (isfieldselected) {
                    Map.ExtentOverlay.PanMode = MapPanMode.Disabled;
                }
                else {
                    Map.ExtentOverlay.PanMode = MapPanMode.Default;
                }
            };

            Map.MouseUp += (sender, e) => {
                ismousedown = false;
                Map.ExtentOverlay.PanMode = MapPanMode.Default;
                PointShape worldlocation2 = Map.ToWorldCoordinate(e.GetPosition(Map));
                EnsureFieldsLayerIsOpen();
                try {
                    Collection<Feature> selectedFeatures = fieldsLayer.QueryTools.GetFeaturesContaining(worldlocation2, ReturningColumnsType.AllColumns);
                    if (selectedFeatures.Count > 0) {
                        PointShape latlonlocation = projection.ConvertToInternalProjection(worldlocation2) as PointShape;
                        for (int i = 0; i < fieldsLayer.InternalFeatures.Count; i++) {
                            Feature feat = fieldsLayer.InternalFeatures[i];
                            MultipolygonShape multipoly = feat.GetShape() as MultipolygonShape;
                            if (multipoly.Polygons.Count > 0) {
                                if (multipoly.Contains(latlonlocation)) {
                                    SelectedField = feat;
                                    WorldLocation = worldlocation2;
                                    break;
                                }
                            }
                        }
                    }
                }
                catch (Exception ex) {
                    log.ErrorException("Caught an exception while working with a custom label shape", ex);
                    System.Windows.MessageBox.Show($"{Strings.ImportTools_Text} {Strings.CaughtAnExceptionWhileWorkingWithCustomLabelShape_Text}");
                }
            };

            Map.MouseMove += (sender, e) => {
                PointShape worldlocation2 = Map.ToWorldCoordinate(e.GetPosition(Map));
                EnsureFieldsLayerIsOpen();
                try {
                    if (ismousedown && isfieldselected) {
                        Collection<Feature> selectedFeatures = fieldsLayer.QueryTools.GetFeaturesContaining(worldlocation2, ReturningColumnsType.AllColumns);
                        PointShape latlonlocation = projection.ConvertToInternalProjection(worldlocation2) as PointShape;
                        //PointShape centerpoint = new PointShape();
                        //labelangle = 0;
                        HasUnsavedChanges = true;
                        if (draglabel) {
                            WorldLocation = worldlocation2;
                            //if (!LabelPositions.ContainsKey(SelectedField.Id)) {
                            //    LabelPositions.Add(SelectedField.Id, worldlocation);
                            //    scalingTextStyle.LabelPositions = labelPositions;
                            //}
                            //else {
                            //    LabelPositions[SelectedField.Id] = worldlocation;
                            //}
                            //scalingTextStyle.LabelPositions = labelPositions;
                        }
                        //else {
                        //    labelangle = RetrieveLabelAngle(worldlocation);
                        //    if (RotationAngles.ContainsKey(SelectedField.Id)) {
                        //        RotationAngles[SelectedField.Id] = labelangle;
                        //    }
                        //    else {
                        //        RotationAngles.Add(SelectedField.Id, labelangle);
                        //    }
                        //    scalingTextStyle.RotationAngles = rotationAngles;
                        //}
                        //scalingTextStyle.MapScale = Map.CurrentScale;
                        //if (Map.Overlays.Contains("importOverlay")) {
                        //    Map.Overlays["importOverlay"].Refresh();
                        //}

                    }
                    //else {
                    //    isfieldselected = false;
                    //    if (selectedFeatures.Count > 0) {
                    //        for (int i = 0; i < fieldsLayer.InternalFeatures.Count; i++) {
                    //            Feature feat = fieldsLayer.InternalFeatures[i];
                    //            MultipolygonShape multipoly = feat.GetShape() as MultipolygonShape;
                    //            if (multipoly.Polygons.Count > 0) {
                    //                if (multipoly.Contains(latlonlocation)) {
                    //                    SelectedField = feat;
                    //                    fieldsLayerAreaStyle.SelectedFeatures.Clear();
                    //                    fieldsLayerAreaStyle.SelectedFeatures.Add(feat);
                    //                    isfieldselected = true;
                    //                    break;
                    //                }
                    //            }
                    //        }
                    //        if (Map.Overlays.Contains("importOverlay")) {
                    //            scalingTextStyle.MapScale = Map.CurrentScale; 
                    //            Map.Overlays["importOverlay"].Refresh();
                    //        }
                    //    }
                    //}
                }
                catch (Exception ex) {
                    log.ErrorException("Caught an exception while working with a custom label shape", ex);
                    //System.Windows.MessageBox.Show("ImportTools - Caught an exception while working with a custom label shape.");
                }

            };

            Map.CurrentScaleChanged += (sender, e) => {
                scalingTextStyle.MapScale = e.CurrentScale;
            };

        }

        private void HighlightSelectedField() {
            if (maploaded) {
                //if (!isCropZone) {
                    BaseShape baseshape = SelectedFeature.GetShape();
                    EnsureFieldsLayerIsOpen();
                    try {
                        Feature selectedFeat = fieldsLayer.QueryTools.GetFeatureById(selectedfeature.Id, ReturningColumnsType.AllColumns);
                        SelectedField = selectedFeat;
                    }
                    catch (Exception ex) {
                        log.ErrorException(Strings.CaughtAnExceptionWhileWorkingWithCustomLabelShape_Text, ex);
                    }
                //}
            }
        }

        void MoveLabel()
        {
            log.Info("LabelTools - Move Label.");
            //draglabel = !draglabel;
            //if (draglabel)
            //{
            //    FieldName = "Drag Label";
            //}
            //else
            //{
            //    FieldName = "Rotate Label";
            //}
        }

        private double RetrieveLabelAngle(PointShape worldlocation) {
            double labelangle = 0;
            PointShape centerpoint;
            if (LabelPositions.ContainsKey(SelectedField.Id)) {
                centerpoint = (PointShape)LabelPositions[SelectedField.Id];
            }
            else {
                BaseShape abs = SelectedField.GetShape();
                abs = projection.ConvertToExternalProjection(abs);
                centerpoint = abs.GetCenterPoint() as PointShape;
            }
            labelangle = GetAngleFromTwoVertices(new Vertex(centerpoint), new Vertex(worldlocation));
            labelangle = Math.Round(labelangle, 0);
            labelangle = 360 - labelangle;
            //if (labelangle < 0) {
            //    labelangle = 360 - labelangle;
            //}
            return labelangle;
        }

        private void LoadBingMapsOverlay() {
            log.Info("LabelDisplay - Load Imagery.");
            if (!startedOutOnline) { return; }
            IMapOverlay mapoverlay = new BingMapOverlay();
            Overlay bingOverlay = mapoverlay.Overlay;
            if (bingOverlay == null) { return; }
            if (Map.Overlays.Contains("Bing")) {
                Map.Overlays.Remove("Bing");
            }
            if (Map.Overlays.Contains("Bing Online")) {
                Map.Overlays.Remove("Bing Online");
            }
            if (Map.Overlays.Contains("Roads Only")) {
                Map.Overlays.Remove("Roads Only");
            }
            if (Map.Overlays.Contains("Road Online")) {
                Map.Overlays.Remove("Road Online");
            }
            if (Map.Overlays.Contains("None")) {
                Map.Overlays.Remove("None");
            }
            //bingOverlay.IsVisible = mapoverlay.Visibility;
            //((TileOverlay)bingOverlay).TileType = TileType.SingleTile;
            Map.Overlays.Add(mapoverlay.Name, bingOverlay);
            Map.Overlays.MoveToBottom(mapoverlay.Name);
        }

        private void LoadFieldLayerShapes() {
            EnsureFieldsLayerIsOpen();
            fieldsLayer.InternalFeatures.Clear();

            foreach (var m in clientLayerItems) {
                if (m.MapData != null && !string.IsNullOrEmpty(m.MapData) && m.MapData != "MULTIPOLYGON EMPTY") {
                    IDictionary<string, string> columnvalues = new Dictionary<string, string>();
                    columnvalues.Add(@"LdbLabel", m.Name);
                    if (isCropZone) {
                        var f = new Feature(m.MapData, m.CropZoneId.ToString(), columnvalues);
                        fieldsLayer.InternalFeatures.Add(m.CropZoneId.ToString(), f);
                    }
                    else {
                        var f = new Feature(m.MapData, m.FieldId.ToString(), columnvalues);
                        fieldsLayer.InternalFeatures.Add(m.FieldId.ToString(), f);
                    }
                }
            }

            //if (fieldsLayer.InternalFeatures.Count > 0) {
            //    SelectedField = fieldsLayer.InternalFeatures[0];
            //}

            if (Map.Overlays.Contains("importOverlay")) {
                scalingTextStyle.MapScale = Map.CurrentScale;
                Map.Overlays["importOverlay"].Refresh();
            }
        }

        private void OpenAndLoadaShapefile() {
            log.Info("LabelDisplay - Open and load shapefiles.");
            CreateInMemoryLayerAndOverlay();
            log.Info("LabelDisplay - Set current extent.");
            LoadShapeFileOverlayOntoMap();
            scalingTextStyle.MapScale = Map.CurrentScale;
            if (Map.Overlays.Contains("importOverlay")) {
                if (((LayerOverlay)Map.Overlays["importOverlay"]).Layers.Contains("ImportLayer")) {
                    Map.Refresh();
                }
            }
        }

        private void CreateInMemoryLayerAndOverlay() {
            layerOverlay = new LayerOverlay();
            layerOverlay.DrawingExceptionMode = DrawingExceptionMode.DrawException;
            //currentLayer = new InMemoryFeatureLayer();

            //fieldsLayer
            fieldsLayer.FeatureSource.Open();
            FeatureSourceColumn column = new FeatureSourceColumn(@"LdbLabel", DbfColumnType.Character.ToString(), 64);
            fieldsLayer.Columns.Add(column);
            fieldsLayer.FeatureSource.Close();
            fieldsLayerAreaStyle = new ClientLayerAreaStyle();
            fieldsLayerAreaStyle.SelectedAreaStyle = new AreaStyle(new GeoPen(GeoColor.SimpleColors.Red, 2), new GeoSolidBrush(new GeoColor(75, GeoColor.SimpleColors.PastelYellow)));
            fieldsLayerAreaStyle.UnselectedAreaStyle = new AreaStyle(new GeoPen(GeoColor.StandardColors.LightGray, 2), new GeoSolidBrush(new GeoColor(75, GeoColor.StandardColors.LightGray)));
            fieldsLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(fieldsLayerAreaStyle);
            fieldsLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(scalingTextStyle);
            fieldsLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            fieldsLayer.FeatureSource.Projection = projection;
            fieldsLayerAreaStyle.SelectedFeatures = new List<Feature>();
            fieldsLayerAreaStyle.HiddenFeatures = new List<Feature>();
            layerOverlay.Layers.Add("ImportLayer", fieldsLayer);
        }

        private void LoadShapeFileOverlayOntoMap() {
            if (Map.Overlays.Contains("importOverlay")) {
                Map.Overlays.Remove("importOverlay");
            }
            if (layerOverlay.Layers.Contains("ImportLayer")) {
                Map.Overlays.Add("importOverlay", layerOverlay);
            }

            EnsureFieldsLayerIsOpen();
            var extent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
            try {
                if (ClientLayer.Count > 0 && fieldsLayer.InternalFeatures.Count == 0) {
                    LoadFieldLayerShapes();
                }
                var shapelayerfeatures = fieldsLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);
                if (shapelayerfeatures.Count > 0) {
                    var shapelayercolumns = fieldsLayer.FeatureSource.GetColumns();
                    extent = ExtentHelper.GetBoundingBoxOfItems(shapelayerfeatures);
                    Map.CurrentExtent = extent;
                    PointShape center = extent.GetCenterPoint();
                    Map.CenterAt(center);
                }
                else {
                    Map.CurrentExtent = extent;
                    PointShape center = extent.GetCenterPoint();
                    Map.CenterAt(center);
                }
            }
            catch (Exception ex) {
                log.Info("Exception while setting custom label extent", ex);
                Map.CurrentExtent = extent;
                //PointShape center = extent.GetCenterPoint();
                //Map.CenterAt(center);
            }
        }

        private void RetrieveSelectedFieldBoundingBox() {
            log.Info("LabelDisplay - Retrieve bounding box.");
            try {
                HighlightSelectedField();
                InMemoryFeatureLayer currentLayer3 = new InMemoryFeatureLayer();
                Proj4Projection projection3 = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetGoogleMapParametersString());
                currentLayer3.FeatureSource.Projection = projection3;
                currentLayer3.InternalFeatures.Add(SelectedFeature);
                if (currentLayer3.InternalFeatures.Count > 0) {
                    BaseShape fieldshape3 = SelectedFeature.GetShape();
                    if (fieldshape3 is MultipolygonShape) {
                        currentLayer3.Open();
                        RectangleShape rs = currentLayer3.GetBoundingBox();
                        currentLayer3.Close();
                        if (maploaded) {
                            scalingTextStyle.MapScale = Map.CurrentScale;
                            Map.CurrentExtent = rs;
                            //Map.Refresh();
                        }
                    }
                }
            }
            catch (Exception ex) {
                log.Info("Exception while select custom label shape", ex);
            }
            Map.Refresh();
        }

        private void CutLine_TrackEnded(object sender, TrackEndedTrackInteractiveOverlayEventArgs e) {
            Map.TrackOverlay.TrackEnded -= new EventHandler<TrackEndedTrackInteractiveOverlayEventArgs>(CutLine_TrackEnded);
            Map.TrackOverlay.TrackMode = TrackMode.None;
            Map.Cursor = previousCursor;
            scalingTextStyle.MapScale = Map.CurrentScale; 

            Map.Refresh();
        }

        private double GetAngleFromTwoVertices(Vertex b, Vertex c) {
            double alpha = 0;
            double tangentAlpha = (c.Y - b.Y) / (c.X - b.X);
            double Peta = Math.Atan(tangentAlpha);

            if (c.X > b.X) {
                alpha = 90 - (Peta * (180 / Math.PI));
            }
            else if (c.X < b.X) {
                alpha = 270 - (Peta * (180 / Math.PI));
            }
            else {
                if (c.Y > b.Y) alpha = 0;
                if (c.Y < b.Y) alpha = 180;
            }
            return alpha;
        }

        void OnCompleteCropzone() {
            log.Info("LabelTools - Save Labels.");
            Map.Cursor = Cursors.Arrow;
            var importmessage = new CustomLabelMessage() { RotationAngles = this.rotationAngles, LabelPositions = this.labelPositions };
            Messenger.Default.Send<CustomLabelMessage>(importmessage);
        }

        void OnCancelCropzone() {
            log.Info("LabelTools - Cancel.");
            Map.Cursor = Cursors.Arrow;
            selectedfield = new Feature();
            selectedfeature = new Feature();
            worldlocation = new PointShape();
            fieldsLayer.InternalFeatures.Clear();
            rotationAngles = new Dictionary<string, double>();
            labelPositions = new Dictionary<string, PointShape>();
            scalingTextStyle.LabelPositions = new Dictionary<string, PointShape>();
            scalingTextStyle.RotationAngles = new Dictionary<string, double>(); 
            fieldsLayerAreaStyle.SelectedFeatures.Clear();
            Messenger.Default.Send<ShowMainViewMessage>(new ShowMainViewMessage());
        }
    }
}
