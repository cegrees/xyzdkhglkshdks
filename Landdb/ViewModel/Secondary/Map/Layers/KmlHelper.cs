﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ICSharpCode.SharpZipLib.Zip;
using System.IO;

namespace Landdb.ViewModel.Secondary.Map.Layers
{
    public class KmlHelper
    {

        // Copy this function is from FormIndexBuilder.
        public string ProcessUnZip(string fileToUpZip)
        {
            ZipInputStream s = null;
            ZipEntry theEntry = null;

            string fileName = null;
            FileStream streamWriter = null;
            try
            {
                s = new ZipInputStream(File.OpenRead(fileToUpZip));
                //s.Password = Password;
                while ((theEntry = s.GetNextEntry()) != null)
                {
                    if (theEntry.Name != String.Empty)
                    {
                        if (theEntry.Name.EndsWith(".kml", StringComparison.InvariantCultureIgnoreCase))
                        {
                            string folder = Path.GetDirectoryName(fileToUpZip);
                            fileName = Path.Combine(folder, theEntry.Name);

                            fileName = fileName.Replace("/", "\\");
                            string directoryName = Path.GetDirectoryName(fileName);
                            if (!Directory.Exists(directoryName))
                            {
                                Directory.CreateDirectory(directoryName);
                            }

                            streamWriter = File.Create(fileName);
                            int size = 2048;
                            byte[] data = new byte[2048];
                            while (true)
                            {
                                size = s.Read(data, 0, data.Length);
                                if (size > 0)
                                {
                                    streamWriter.Write(data, 0, size);
                                }
                                else
                                {
                                    break;
                                }
                            }
                            break;
                        }
                    }
                }
                return fileName;
            }
            finally
            {
                if (streamWriter != null)
                {
                    streamWriter.Close();
                    streamWriter = null;
                }
                if (theEntry != null)
                {
                    theEntry = null;
                }
                if (s != null)
                {
                    s.Close();
                    s = null;
                }
                GC.Collect();
                GC.Collect(1);
            }
        }
    }
}
