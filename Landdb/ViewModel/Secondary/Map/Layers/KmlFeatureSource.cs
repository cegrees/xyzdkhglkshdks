﻿//KML reference
//<styleUrl> 
//URL of a <Style> or <StyleMap> defined in a Document. If the style is in the same file, use a # reference. 
//If the style is defined in an external file, use a full URL along with # referencing. Examples are 
//<styleUrl>#myIconStyleID</styleUrl>
//<styleUrl>http://someserver.com/somestylefile.xml#restaurant</styleUrl>
//<styleUrl>eateries.kml#my-lunch-spot</styleUrl>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Xml;
using System.Net;
using System.Web;
using ThinkGeo.MapSuite.Core;

namespace Landdb.ViewModel.Secondary.Map.Layers
{
    public class KmlFeatureSource : FeatureSource
    {
        private const string presetXml = "<?xml version='1.0' encoding='UTF-8'?><kml xmlns='http://earth.google.com/kml/2.1'></kml>";
        private XmlDocument kmlDocument;
        private InMemoryFeatureLayer inMemoryFeatureLayer;
        private Collection<Feature> tempFeatures;
        private Dictionary<string, Dictionary<string, string>> globalStyles;

        protected KmlFeatureSource()
            : this(string.Empty)
        {

        }

        public KmlFeatureSource(string kmlPathFileName)
            : this(kmlPathFileName, KmlStringType.File)
        {
        }

        public KmlFeatureSource(string kmlString, KmlStringType stringType)
            : base()
        {
            switch (stringType)
            {
                case KmlStringType.String:
                    kmlDocument = new XmlDocument();
                    if (string.IsNullOrEmpty(kmlString))
                    {
                        //kmlDocument.LoadXml(R);
                    }
                    else
                    {
                        kmlDocument.LoadXml(kmlString);
                    }
                    break;
                case KmlStringType.File:
                default:
                    kmlDocument = new XmlDocument();
                    kmlDocument.Load(kmlString);
                    break;
            }
            this.inMemoryFeatureLayer = new InMemoryFeatureLayer();
            inMemoryFeatureLayer.Open();
            inMemoryFeatureLayer.Columns.Add(new FeatureSourceColumn("linecolor"));
            inMemoryFeatureLayer.Columns.Add(new FeatureSourceColumn("linewidth"));

            inMemoryFeatureLayer.Close();

            globalStyles = new Dictionary<string, Dictionary<string, string>>();
        }

        protected override void OpenCore()
        {
            if (inMemoryFeatureLayer.InternalFeatures.Count == 0)
            {
                tempFeatures = new Collection<Feature>();
                XmlNamespaceManager manager = new XmlNamespaceManager(kmlDocument.NameTable);
                manager.AddNamespace("kk", kmlDocument.DocumentElement.NamespaceURI);

                if (kmlDocument.DocumentElement.HasChildNodes)
                {
                    XmlNodeList styleList = kmlDocument.DocumentElement.ChildNodes[0].SelectNodes("kk:Style", manager);
                    foreach (XmlNode item in styleList)
                    {
                        string styleId = item.Attributes["id"].Value;
                        Dictionary<string, string> columnValues = GetColumnValues(item, manager);
                        globalStyles.Add(styleId, columnValues);
                    }

                    XmlNodeList placemarks = kmlDocument.SelectNodes("//kk:Placemark", manager);
                    foreach (XmlNode placemark in placemarks)
                    {
                        ProcessPlacemark(placemark, manager);
                    }

                    // There is no Placemark tag, so it may use NetworkLink
                    if (placemarks.Count == 0)
                    {
                        XmlNodeList networkLinks = kmlDocument.SelectNodes("//kk:NetworkLink", manager);
                        foreach (XmlNode networkLink in networkLinks)
                        {
                            ProcessNetworkLink(networkLink, manager);
                        }
                    }

                    foreach (Feature item in tempFeatures)
                    {
                        WellKnownType wellKnownType = item.GetWellKnownType();
                        if (wellKnownType == WellKnownType.Polygon || wellKnownType == WellKnownType.Multipolygon)
                        {
                            inMemoryFeatureLayer.InternalFeatures.Add(item);
                        }
                    }

                    foreach (Feature item in tempFeatures)
                    {
                        WellKnownType wellKnownType = item.GetWellKnownType();
                        if (wellKnownType == WellKnownType.Line || wellKnownType == WellKnownType.Multiline)
                        {
                            inMemoryFeatureLayer.InternalFeatures.Add(item);
                        }
                    }

                    foreach (Feature item in tempFeatures)
                    {
                        WellKnownType wellKnownType = item.GetWellKnownType();
                        if (wellKnownType == WellKnownType.Point || wellKnownType == WellKnownType.Multipoint)
                        {
                            inMemoryFeatureLayer.InternalFeatures.Add(item);
                        }
                    }
                }
            }
        }

        private void ProcessNetworkLink(XmlNode networkLink, XmlNamespaceManager manager)
        {
            XmlNode hrefNode = networkLink.SelectSingleNode("kk:Url/kk:href", manager);
            string fileUrl = hrefNode.InnerText;
            WebRequest request = WebRequest.Create(fileUrl);
            request.Timeout = 10000;
            WebResponse response = null;
            try
            {
                response = request.GetResponse();
                string disposition = response.Headers["Content-Disposition"];
                if (!string.IsNullOrEmpty(disposition))
                {
                    string filename = @"C:\temp\" + disposition.Split('=')[1].Trim('\"');
                    WebClient webClient = new WebClient();
                    webClient.DownloadFile(fileUrl, filename);
                    if (filename.EndsWith(".kml", StringComparison.InvariantCultureIgnoreCase))
                    {

                    }
                    else if (filename.EndsWith(".kmz", StringComparison.InvariantCultureIgnoreCase))
                    {
                        filename = new KmlHelper().ProcessUnZip(filename);
                    }
                    else
                    {
                        return;
                    }

                    KmlFeatureSource source = new KmlFeatureSource(filename);
                    source.Open();
                    foreach (Feature item in source.inMemoryFeatureLayer.InternalFeatures)
                    {
                        tempFeatures.Add(item);
                    }
                    source.Close();

                }
            }
            catch
            {

            }
            finally
            {
                if (response != null)
                {
                    response.Close();
                }
            }
        }

        private void ProcessPlacemark(XmlNode placemark, XmlNamespaceManager manager)
        {
            XmlNode stylesNode = placemark.SelectSingleNode("kk:Style", manager);
            Dictionary<string, string> columnValues = GetColumnValues(stylesNode, manager);

            XmlNode descriptionNode = placemark.SelectSingleNode("kk:description", manager);
            if (descriptionNode != null)
            {
                columnValues.Add("popupHTML", descriptionNode.InnerXml);
            }

            XmlNodeList linearRingList = placemark.SelectNodes(".//kk:LinearRing", manager);
            foreach (XmlNode node in linearRingList)
            {
                string value = node.SelectSingleNode("kk:coordinates", manager).InnerText.Trim();
                //TODO: the document says use " ", not "\r\n"
                string[] values = value.Split(new string[] { " ", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
                Collection<Vertex> Vertices = new Collection<Vertex>();
                foreach (string item in values)
                {
                    string[] items = item.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    Vertex vertex = new Vertex(double.Parse(items[0]), double.Parse(items[1]));
                    Vertices.Add(vertex);
                }
                RingShape ringShape = new RingShape(Vertices);
                tempFeatures.Add(new Feature(ringShape, columnValues));
            }

            XmlNodeList lineStringList = placemark.SelectNodes(".//kk:LineString", manager);
            foreach (XmlNode node in lineStringList)
            {
                string value = node.SelectSingleNode("kk:coordinates", manager).InnerText.Trim();
                //TODO: the document says use " ", not "\r\n"
                string[] values = value.Split(new string[] { " ", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
                Collection<Vertex> Vertices = new Collection<Vertex>();
                foreach (string item in values)
                {
                    string[] items = item.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    Vertex vertex = new Vertex(double.Parse(items[0]), double.Parse(items[1]));
                    Vertices.Add(vertex);
                }
                LineShape lineShape = new LineShape(Vertices);

                if (columnValues.Count == 0 || (columnValues.Count == 1 && columnValues.ContainsKey("popupHTML")))
                {
                    columnValues.Add("linecolor", "ffffffff");
                    columnValues.Add("linewidth", "1");
                    if (placemark.SelectSingleNode("kk:styleUrl", manager) != null)
                    {
                        string styleUrl = placemark.SelectSingleNode("kk:styleUrl", manager).InnerText;
                        if (styleUrl.StartsWith("#"))
                        {
                            columnValues = globalStyles[styleUrl.TrimStart('#')];
                        }
                    }
                }

                tempFeatures.Add(new Feature(lineShape, columnValues));
            }

            XmlNodeList pointList = placemark.SelectNodes(".//kk:Point", manager);
            foreach (XmlNode item in pointList)
            {
                string value = item.SelectSingleNode("kk:coordinates", manager).InnerText.Trim();
                string[] values = value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                PointShape point = new PointShape(double.Parse(values[0]), double.Parse(values[1]));
                tempFeatures.Add(new Feature(point, columnValues));
            }
        }

        private Dictionary<string, string> GetColumnValues(XmlNode stylesNode, XmlNamespaceManager manager)
        {
            Dictionary<string, string> columnValues = new Dictionary<string, string>();
            if (stylesNode != null)
            {
                foreach (XmlNode styleNode in stylesNode.ChildNodes)
                {
                    if (styleNode.Name == "LineStyle")
                    {
                        string linecolor = "ffffffff";
                        if (styleNode.SelectSingleNode("kk:color", manager) != null)
                        {
                            linecolor = styleNode.SelectSingleNode("kk:color", manager).InnerText;
                        }
                        string linewidth = "1";
                        if (styleNode.SelectSingleNode("kk:width", manager) != null)
                        {
                            linewidth = styleNode.SelectSingleNode("kk:width", manager).InnerText;
                        }
                        columnValues.Add("linecolor", linecolor);
                        columnValues.Add("linewidth", linewidth);
                        break;
                    }
                }
            }

            return columnValues;
        }

        protected override void CloseCore()
        {
            //inMemoryFeatureLayer.InternalFeatures.Clear();
            //globalStyles.Clear();
        }

        protected override Collection<Feature> GetAllFeaturesCore(IEnumerable<string> returningColumnNames)
        {
            return inMemoryFeatureLayer.InternalFeatures;
        }
    }
}
