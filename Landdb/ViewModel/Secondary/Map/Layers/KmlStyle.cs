﻿//KML reference:
//<color> 
//Color and opacity (alpha) values are expressed in hexadecimal notation. The range of values for any one color is 0 
//to 255 (00 to ff). For alpha, 00 is fully transparent and ff is fully opaque. The order of expression is aabbggrr, 
//where aa=alpha (00 to ff); bb=blue (00 to ff); gg=green (00 to ff); rr=red (00 to ff). For example, if you want to 
//apply a blue color with 50 percent opacity to an overlay, you would specify the following: <color>7fff0000</color>, 
//where alpha=0x7f, blue=0xff, green=0x00, and red=0x00. 

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using ThinkGeo.MapSuite.Core;

namespace Landdb.ViewModel.Secondary.Map.Layers
{
    public class KmlStyle : Style
    {
        private GeoImage defaultIcon;

        public KmlStyle()
            : base()
        {
            Stream stream = new MemoryStream();
            //Properties.Resources.GoogleEarthPushpin.Save(stream, ImageFormat.Png);
            defaultIcon = new GeoImage(stream);
        }

        public GeoImage DefaultIcon
        {
            get { return defaultIcon; }
            set { defaultIcon = value; }
        }

        protected override void DrawCore(IEnumerable<Feature> features, GeoCanvas canvas, Collection<SimpleCandidate> labelsInThisLayer, System.Collections.ObjectModel.Collection<SimpleCandidate> labelsInAllLayers)
        {
            foreach (Feature feature in features)
            {
                switch (feature.GetWellKnownType())
                {
                    case WellKnownType.Line:
                    case WellKnownType.Multiline:
                        {
                            GeoPen geoPen = GetGeoPen(feature);
                            canvas.DrawLine(feature, geoPen, DrawingLevel.LevelOne);
                        }
                        break;
                    case WellKnownType.Point:
                    case WellKnownType.Multipoint:
                        if (defaultIcon == null)
                        {
                            GeoBrush geoBrush = GetGeoBrush(feature);
                            canvas.DrawEllipse(feature, 8, 8, geoBrush, DrawingLevel.LevelOne);
                        }
                        else
                        {
                            PointShape pointShape = feature.GetShape() as PointShape;
                            canvas.DrawWorldImageWithoutScaling(defaultIcon, pointShape.X, pointShape.Y, DrawingLevel.LevelOne);
                        }
                        break;
                    case WellKnownType.Polygon:
                    case WellKnownType.Multipolygon:
                        {
                            GeoPen geoPen = GetGeoPen(feature);
                            canvas.DrawArea(feature, geoPen, DrawingLevel.LevelOne);
                        }
                        break;
                }
            }
        }

        private GeoBrush GetGeoBrush(Feature feature)
        {
            return new GeoSolidBrush(GeoColor.SimpleColors.Black);
        }

        private GeoPen GetGeoPen(Feature feature)
        {
            GeoPen geoPen = new GeoPen(GeoColor.SimpleColors.Black);

            if (feature.ColumnValues.Count >= 2)
            {
                string linecolor = feature.ColumnValues["linecolor"];
                string linewidth = feature.ColumnValues["linewidth"];

                GeoColor geoColor = GetGeoColor(linecolor);
                int width = int.Parse(linewidth);

                geoPen.Color = geoColor;
                geoPen.Width = width;
            }

            return geoPen;
        }

        private GeoColor GetGeoColor(string linecolor)
        {
            GeoColor geoColor = new GeoColor();

            if (linecolor.Length == 8)
            {
                int alpha = int.Parse(linecolor.Substring(0, 2), NumberStyles.AllowHexSpecifier);
                int blue = int.Parse(linecolor.Substring(2, 2), NumberStyles.AllowHexSpecifier);
                int green = int.Parse(linecolor.Substring(4, 2), NumberStyles.AllowHexSpecifier);
                int red = int.Parse(linecolor.Substring(6, 2), NumberStyles.AllowHexSpecifier);

                geoColor = new GeoColor(alpha, red, green, blue);
            }

            return geoColor;
        }
    }
}
