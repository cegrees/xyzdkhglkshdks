﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Landdb.ViewModel.Secondary.Map.Layers
{
    public enum KmlStringType
    {
        File = 0,
        String = 1,
    }
}