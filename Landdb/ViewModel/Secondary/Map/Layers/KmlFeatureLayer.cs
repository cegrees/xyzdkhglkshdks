﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ThinkGeo.MapSuite.Core;

namespace Landdb.ViewModel.Secondary.Map.Layers
{
    public class KmlFeatureLayer : FeatureLayer
    {

        protected KmlFeatureLayer()
            : this(string.Empty)
        {

        }

        public KmlFeatureLayer(string kmlPathFileName)
            : this(kmlPathFileName, KmlStringType.File)
        {
        }

        public KmlFeatureLayer(string kmlString, KmlStringType stringType)
            : base()
        {
            FeatureSource = new KmlFeatureSource(kmlString, stringType);
        }

        public override bool HasBoundingBox
        {
            get
            {
                return true;
            }
        }
    }
}

