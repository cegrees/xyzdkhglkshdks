﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GalaSoft.MvvmLight;
using ThinkGeo.MapSuite.WpfDesktopEdition;
using ThinkGeo.MapSuite.Core;
using Landdb.Infrastructure;
using System.IO;
using Microsoft.Win32;
using GalaSoft.MvvmLight.Command;
using System.Windows.Input;
using System.Collections.ObjectModel;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure.Messages;
using Landdb.Domain.ReadModels.Tree;
using Landdb.MapStyles;
using Landdb.Client.Infrastructure;
using System.Windows.Threading;
using Landdb.ViewModel.Maps.ImageryOverlay;
using System.Drawing;
using Landdb.ViewModel.Maps;
using NLog;
using System.Net;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using AgC.UnitConversion;
using Landdb.ViewModel.Fields.FieldDetails.MapModels;
using AgC.UnitConversion.Length;
using Landdb.Resources;

namespace Landdb.ViewModel.Secondary.Map {
    public class SSurgoNrcsInfoWfsViewModel : ViewModelBase
    {
        Logger log = LogManager.GetCurrentClassLogger();
        bool maploaded = false;
        bool startedOutOnline = false;
        IMapFilter selectedfilter = null;
        bool ismousedown = false;
        bool draglabel = true;
        //bool hasunsavedchanges = false;
        bool isfieldselected = false;
        string fieldname = string.Empty;
        double totalarea = 0;
        double desiredarea = 100;
        double oldarea = 0;
        double scalepercent = 0;
        double pointpercentage = 100;
        InMemoryFeatureLayer fieldsLayer = new InMemoryFeatureLayer();
        InMemoryFeatureLayer ssurgoLayer = new InMemoryFeatureLayer();
        InMemoryFeatureLayer cookiecutLayer = new InMemoryFeatureLayer();
        WfsFeatureLayer wfsFeatureLayer = new WfsFeatureLayer();
        Proj4Projection projection;
        RectangleShape previousmapextent = new RectangleShape();
        Feature selectedfield = new Feature();
        GeoCollection<Feature> selectedFeatures = new GeoCollection<Feature>();
        Feature loadedfeature = new Feature();
        BaseShape fieldshape = new MultipolygonShape();
        Dictionary<string, double> dictarea = new Dictionary<string, double>();
        Dictionary<string, double> dictpercent = new Dictionary<string, double>();
        Dictionary<string, double> surgoareas = new Dictionary<string, double>();
        Dictionary<string, SSurgoViewModel> dictnrcs = new Dictionary<string, SSurgoViewModel>();
        private Dictionary<string, AreaStyle> colorstyles = new Dictionary<string, AreaStyle>();
        ObservableCollection<ColorItem> coloritems = new ObservableCollection<ColorItem>();
        ObservableCollection<ColorItem> cookiecutcoloritems = new ObservableCollection<ColorItem>();
        Dictionary<string, System.Drawing.Color> cropzonecolors = new Dictionary<string, System.Drawing.Color>();
        IList<System.Drawing.Color> usedcolors = new List<System.Drawing.Color>();
        bool showthetoggle = true;
        ColorItem selectedcoloritem = null;
        int colormultiplier = 50;
        int randomchoice = 5;
        readonly IClientEndpoint clientEndpoint;
        Random rand = new Random();
        //MapSettings mapsettings;
        int cmgopacity = 255;
        IList<SSurgoViewModel> soilInfoList = new List<SSurgoViewModel>();
        MapSettings mapsettings;
        string areaUnitString = string.Empty;
        string coordinateformat = string.Empty;
        AgC.UnitConversion.Area.AreaUnit agcAreaUnit;
        ThinkGeo.MapSuite.Core.AreaUnit mapAreaUnit;
        ThinkGeo.MapSuite.Core.DistanceUnit mapDistanceUnit;


        public SSurgoNrcsInfoWfsViewModel(IClientEndpoint clientEndpoint, GeoCollection<Feature> selectedFeatures)
        {
            this.clientEndpoint = clientEndpoint;
            this.soilInfoList = new List<SSurgoViewModel>();
            this.selectedFeatures = new GeoCollection<Feature>();
            this.selectedFeatures.Add(selectedFeatures[0]);
            PointPercentage = 100;
            projection = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetGoogleMapParametersString());

            InitializeSettings();
            OpenAndLoadaShapefile();
            RetrieveSSurgoData();

            BaseShape baseshape = this.selectedFeatures[0].GetShape();
            AreaBaseShape areabaseshape = baseshape as AreaBaseShape;
            double selectedarea = areabaseshape.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);

            Collection<Feature> clickedFeatures = cookiecutLayer.FeatureSource.GetAllFeatures(ReturningColumnsType.AllColumns);

            SlopeLength = 0;
            SlopePercent = 0;
            SandTotal_r = 0;
            SiltTotal_r = 0;
            ClayTotal_r = 0;
            Infiltration = 0;

            bool hadDgroup = false;

            foreach (Feature clicked in clickedFeatures) {
                try {
                    BaseShape clickedbaseshape = clicked.GetShape();
                    AreaBaseShape clickedareabaseshape = clickedbaseshape as AreaBaseShape;
                    double clickedarea = clickedareabaseshape.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);

                    SSurgoViewModel ssurgonrcswfsvm = new SSurgoViewModel();
                    ssurgonrcswfsvm.Execute("", clicked, this.selectedFeatures[0]);
                    string symbol = ssurgonrcswfsvm.Symbol;
                    string name = ssurgonrcswfsvm.Name;
                    double areapercent = dictpercent[ssurgonrcswfsvm.Symbol];
                    double totalarea = dictarea[ssurgonrcswfsvm.Symbol];
                    //string soiltype = SoilTypeFactory(ssurgonrcswfsvm.Name);
                    string soiltype1 = SoilTextureFactory(ssurgonrcswfsvm.SandTotal_r, ssurgonrcswfsvm.SiltTotal_r, ssurgonrcswfsvm.ClayTotal_r);
                    ssurgonrcswfsvm.SoilType = soiltype1;
                    ssurgonrcswfsvm.Hydgrp = ssurgonrcswfsvm.MapUnitComponentItem.Hydgrp;
                    if (ssurgonrcswfsvm.Hydgrp == "A/D" || ssurgonrcswfsvm.Hydgrp == "B/D" || ssurgonrcswfsvm.Hydgrp == "C/D" || ssurgonrcswfsvm.Hydgrp == "D") { hadDgroup = true; }
                    double hydrology1 = HydrologicalFactory(ssurgonrcswfsvm.Hydgrp);
                    ssurgonrcswfsvm.TotalArea = totalarea;
                    if (dictpercent.ContainsKey(ssurgonrcswfsvm.Symbol)) {
                        ssurgonrcswfsvm.AreaPercent = dictpercent[ssurgonrcswfsvm.Symbol];
                    }
                    if (!string.IsNullOrEmpty(ssurgonrcswfsvm.MapUnitComponentItem.Slopelenusle_r)) {
                        LengthMeasure meters = Meter.Self.GetMeasure(System.Convert.ToDouble(ssurgonrcswfsvm.MapUnitComponentItem.Slopelenusle_r)) as LengthMeasure;
                        Measure feet = meters.CreateAs(AgC.UnitConversion.Length.Foot.Self);
                        ssurgonrcswfsvm.SlopeLengthMeters = System.Convert.ToDouble(ssurgonrcswfsvm.MapUnitComponentItem.Slopelenusle_r);
                        ssurgonrcswfsvm.SlopeLength = feet.Value;
                    }

                    SlopeLength += ssurgonrcswfsvm.SlopeLength * (ssurgonrcswfsvm.CoveragePercent / 100);
                    ssurgonrcswfsvm.SlopePercent = System.Convert.ToDouble(ssurgonrcswfsvm.MapUnitComponentItem.Slope_r);
                    SlopePercent += ssurgonrcswfsvm.SlopePercent * (ssurgonrcswfsvm.CoveragePercent / 100);
                    SandTotal_r += ssurgonrcswfsvm.SandTotal_r * (ssurgonrcswfsvm.CoveragePercent / 100);
                    SiltTotal_r += ssurgonrcswfsvm.SiltTotal_r * (ssurgonrcswfsvm.CoveragePercent / 100);
                    ClayTotal_r += ssurgonrcswfsvm.ClayTotal_r * (ssurgonrcswfsvm.CoveragePercent / 100);
                    Infiltration += hydrology1 * (ssurgonrcswfsvm.CoveragePercent / 100);

                    if (!dictnrcs.ContainsKey(ssurgonrcswfsvm.Symbol)) {
                        dictnrcs.Add(ssurgonrcswfsvm.Symbol, ssurgonrcswfsvm);
                    }
                    else {
                        dictnrcs[ssurgonrcswfsvm.Symbol].CoverageArea += ssurgonrcswfsvm.CoverageArea;
                        dictnrcs[ssurgonrcswfsvm.Symbol].CoveragePercent += ssurgonrcswfsvm.CoveragePercent;
                        dictnrcs[ssurgonrcswfsvm.Symbol].TotalArea += ssurgonrcswfsvm.TotalArea;
                    }
                    

                }
                catch (Exception ex) {
                    //log.ErrorException("SSurgoViewModel - Couldn't load the field shape.", ex);
                    System.Windows.MessageBox.Show($"{Strings.Problem_LoadingFieldShape_Text} {Strings.OperationAborted_Text}");

                }
            }
            soilInfoList = new List<SSurgoViewModel>();
            foreach (var dict in dictnrcs) {
                soilInfoList.Add(dict.Value);
            }
            soiltype = SoilTextureFactory(SandTotal_r, SiltTotal_r, ClayTotal_r);
            hydrologygroup = InfiltrationFactory(Infiltration, hadDgroup);

            var newList = soilInfoList.OrderByDescending(x => x.CoveragePercent).ToList();
            soilInfoList = newList;
        }

        public GeoCollection<Feature> SelectedFeatures {
            get { return selectedFeatures; }
            set {
                selectedFeatures = value;
                RaisePropertyChanged("SelectedFeatures");
            }
        }

        public IList<SSurgoViewModel> SoilInfoList {
            get { return soilInfoList; }
            set {
                soilInfoList = value;
                RaisePropertyChanged("SoilInfoList");
            }
        }

        public string SlopeLengthString {
            get { return SlopeLength.ToString("N0"); }
        }

        double slopelength = 0;
        public double SlopeLength {
            get { return slopelength; }
            set {
                if (slopelength == value) { return; }
                slopelength = value;
                RaisePropertyChanged("SlopeLength");
                RaisePropertyChanged("SlopeLengthString");
            }
        }

        public string SlopePercentString {
            get { return SlopePercent.ToString("N0"); }
        }

        double slopepercent = 0;
        public double SlopePercent {
            get { return slopepercent; }
            set {
                if (slopepercent == value) { return; }
                slopepercent = value;
                RaisePropertyChanged("SlopePercent");
                RaisePropertyChanged("SlopePercentString");
            }
        }

        public string SandTotal_rString {
            get { return SandTotal_r.ToString("N0"); }
        }

        double sandtotal_r = 0;
        public double SandTotal_r {
            get { return sandtotal_r; }
            set {
                if (sandtotal_r == value) { return; }
                sandtotal_r = value;
                RaisePropertyChanged("SandTotal_r");
                RaisePropertyChanged("SandTotal_rString");
            }
        }

        public string SiltTotal_rString {
            get { return SiltTotal_r.ToString("N0"); }
        }

        double silttotal_r = 0;
        public double SiltTotal_r {
            get { return silttotal_r; }
            set {
                if (silttotal_r == value) { return; }
                silttotal_r = value;
                RaisePropertyChanged("SiltTotal_r");
                RaisePropertyChanged("SiltTotal_rString");
            }
        }

        public string ClayTotal_rString {
            get { return ClayTotal_r.ToString("N0"); }
        }

        double claytotal_r = 0;
        public double ClayTotal_r {
            get { return claytotal_r; }
            set {
                if (claytotal_r == value) { return; }
                claytotal_r = value;
                RaisePropertyChanged("ClayTotal_r");
                RaisePropertyChanged("ClayTotal_r");
            }
        }

        string soiltype = string.Empty;
        public string SoilType {
            get { return soiltype; }
            set {
                if (soiltype == value) { return; }
                soiltype = value;
                RaisePropertyChanged("SoilType");
            }
        }

        double infiltration = 0;
        public double Infiltration {
            get { return infiltration; }
            set {
                if (infiltration == value) { return; }
                infiltration = value;
                RaisePropertyChanged("Infiltration");
            }
        }

        string hydrologygroup = string.Empty;
        public string HydrologyGroup {
            get { return hydrologygroup; }
            set {
                if (hydrologygroup == value) { return; }
                hydrologygroup = value;
                RaisePropertyChanged("HydrologyGroup");
            }
        }

        public string FieldName
        {
            get { return fieldname; }
            set
            {
                if (fieldname == value) { return; }
                fieldname = value;
                RaisePropertyChanged("FieldName");
            }
        }

        public double ScalePercent
        {
            get { return scalepercent; }
            set
            {
                if (scalepercent == value) { return; }
                scalepercent = value;
                RaisePropertyChanged("ScalePercent");
            }
        }

        public double PointPercentage
        {
            get { return pointpercentage; }
            set
            {
                if (pointpercentage == value) { return; }
                pointpercentage = value;
                RaisePropertyChanged("PointPercentage");
            }
        }

        private void InitializeSettings()
        {
            this.mapsettings = clientEndpoint.GetMapSettings();
            areaUnitString = mapsettings.MapAreaUnit;
            coordinateformat = mapsettings.DisplayCoordinateFormat;
            agcAreaUnit = AgC.UnitConversion.UnitFactory.GetUnitByName(areaUnitString) as AgC.UnitConversion.Area.AreaUnit;
            mapAreaUnit = MapUnitFactory.AreaMeasureConversion(agcAreaUnit);
            mapDistanceUnit = MapUnitFactory.AreaDistanceMeasureConversion(agcAreaUnit);
        }

        internal void EnsureFieldsLayerIsOpen()
        {
            if (fieldsLayer == null)
            {
                fieldsLayer = new InMemoryFeatureLayer();
            }

            if (!fieldsLayer.IsOpen)
            {
                fieldsLayer.Open();
            }

            if (!projection.IsOpen)
            {
                projection.Open();
            }
        }

        internal void EnsureSSurgoLayerIsOpen() {
            if (ssurgoLayer == null) {
                ssurgoLayer = new InMemoryFeatureLayer();
            }

            if (!ssurgoLayer.IsOpen) {
                ssurgoLayer.Open();
            }

            if (!projection.IsOpen) {
                projection.Open();
            }
        }

        internal void EnsureCookieCutLayerIsOpen() {
            if (cookiecutLayer == null) {
                cookiecutLayer = new InMemoryFeatureLayer();
            }

            if (!cookiecutLayer.IsOpen) {
                cookiecutLayer.Open();
            }

            if (!projection.IsOpen) {
                projection.Open();
            }
        }

        private static SizeF GetStringPixelLength(string text, string fontname, float fontsize) {
            SizeF size = new SizeF();
            using (System.Drawing.Graphics graphics = System.Drawing.Graphics.FromImage(new Bitmap(1, 1))) {
                size = graphics.MeasureString(text, new Font(fontname, fontsize, FontStyle.Regular, GraphicsUnit.Point));
            }
            return size;
        }


        private void DetermineLegendWitdh(LegendAdornmentLayer legendLayer, CustomLegendItem legendItem) {
            SetLegendItemWidth(legendItem);

            if (legendItem.Width > legendLayer.Width) {
                legendLayer.Width = legendItem.Width;
            }
        }

        private static void SetLegendItemWidth(CustomLegendItem legendItem) {
            var fudgeFactor = 10;
            var lineWidth = (int)new GdiPlusGeoCanvas().MeasureText(legendItem.TextStyle.TextColumnName, legendItem.TextStyle.Font).Width;
            var totalImageLength = legendItem.ImageWidth + legendItem.ImageLeftPadding + legendItem.ImageRightPadding;
            var totalTextLength = legendItem.TextLeftPadding + legendItem.TextRightPadding + lineWidth;
            legendItem.Width = totalImageLength + totalTextLength + fudgeFactor;
        }

        //private void LoadFieldLayerShapes() {
        //    EnsureFieldsLayerIsOpen();
        //    fieldsLayer.InternalFeatures.Clear();
        //    foreach (Feature featur in SelectedFeatures) {
        //        fieldsLayer.InternalFeatures.Add(featur);
        //    }
        //}

        private void OpenAndLoadaShapefile() {
            PointPercentage = 100;
            fieldsLayer.InternalFeatures.Clear();
            CreateInMemoryLayerAndOverlay();
            LoadShapeFileOverlayOntoMap();
        }

        private void CreateInMemoryLayerAndOverlay() {
            fieldsLayer = new InMemoryFeatureLayer();

            fieldsLayer.FeatureSource.Open();
            FeatureSourceColumn column = new FeatureSourceColumn(@"LdbLabel", DbfColumnType.Character.ToString(), 64);
            fieldsLayer.Columns.Add(column);
            fieldsLayer.FeatureSource.Close();
            fieldsLayer.FeatureSource.Projection = projection;
        }

        private void LoadShapeFileOverlayOntoMap() {

            EnsureFieldsLayerIsOpen();
            var extent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
            try {
                foreach (Feature featur in SelectedFeatures) {
                    fieldsLayer.InternalFeatures.Add(featur);
                }
                var shapelayerfeatures = fieldsLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);
                if (shapelayerfeatures.Count > 0) {
                    var shapelayercolumns = fieldsLayer.FeatureSource.GetColumns();
                    extent = ExtentHelper.GetBoundingBoxOfItems(shapelayerfeatures);
                    extent.ScaleUp(20);
                }
                else {
                }
            }
            catch (Exception ex) {
                log.ErrorException("Caught an exception while loading shapes", ex);
                //System.Windows.MessageBox.Show("ImportTools - Caught an exception while working with a simple draw shape.");
            }
        }

        private void RetrieveSSurgoData() {
            PointPercentage = 100;
            ssurgoLayer.InternalFeatures.Clear();
            HandleWFSLayer();
            CreateSSurgoInMemoryLayerAndOverlay();
            CreateCookieCutInMemoryLayerAndOverlay();
            cookiecutcoloritems = new ObservableCollection<ColorItem>();
            LoadSSurgpOverlayOntoMap();
            CutOutLayer();
        }

        private void HandleWFSLayer() {
            wfsFeatureLayer = new WfsFeatureLayer(@"http://SDMDataAccess.nrcs.usda.gov/Spatial/SDMWGS84Geographic.wfs", "MapunitPolyExtended");
            wfsFeatureLayer.Open();
            wfsFeatureLayer.FeatureSource.Open();
            System.Collections.ObjectModel.Collection<FeatureSourceColumn> columns = wfsFeatureLayer.FeatureSource.GetColumns();
            wfsFeatureLayer.SendingWebRequest += (sender, e) => {
                Uri uri = e.WebRequest.RequestUri;
                string uristring = uri.AbsoluteUri;
                if (uristring.Contains("SDMWGS84Geographic") && uristring.Contains("GetFeature")) {
                    string[] uriarray = uristring.Split(new string[] { @"propertyname=" }, StringSplitOptions.None);
                    string array1 = uriarray[0];
                    string array2 = uriarray[1];
                    string uristring2 = string.Format("{0}propertyname=({1})", array1, array2);
                    Uri uricustom = new Uri(uristring2);
                    System.Net.WebRequest wr = System.Net.WebRequest.Create(uricustom);
                    e.WebRequest = wr;
                }
            };

            WfsFeatureSource featureSource = wfsFeatureLayer.FeatureSource as WfsFeatureSource;
            featureSource.RequestingData += (sender, e) => {
                string servicurl = e.ServiceUrl;
                string xmlrespons = e.XmlResponse;
            };
            featureSource.RequestedData += (sender, e) => {
                string servicurl = e.ServiceUrl;
                string xmlrespons = e.XmlResponse;
            };
            featureSource.SendingWebRequest += (sender, e) => {
                string absoluturi = e.WebRequest.RequestUri.AbsoluteUri;
            };
            featureSource.SentWebRequest += (sender, e) => {
                string absoluturi = e.Response.ResponseUri.AbsoluteUri;
            };
        }

        private void CreateSSurgoInMemoryLayerAndOverlay() {
            ssurgoLayer = new InMemoryFeatureLayer();
            ssurgoLayer.FeatureSource.Open();
            FeatureSourceColumn column1 = new FeatureSourceColumn(@"Areasym", DbfColumnType.Character.ToString(), 64);
            FeatureSourceColumn column2 = new FeatureSourceColumn(@"Spatver", DbfColumnType.Character.ToString(), 64);
            FeatureSourceColumn column3 = new FeatureSourceColumn(@"Musym", DbfColumnType.Character.ToString(), 64);
            FeatureSourceColumn column4 = new FeatureSourceColumn(@"Natmusym", DbfColumnType.Character.ToString(), 64);
            FeatureSourceColumn column5 = new FeatureSourceColumn(@"Mukey", DbfColumnType.Character.ToString(), 64);
            FeatureSourceColumn column6 = new FeatureSourceColumn(@"Muname", DbfColumnType.Character.ToString(), 64);
            FeatureSourceColumn column7 = new FeatureSourceColumn(@"Mupoly", DbfColumnType.Character.ToString(), 64);
            FeatureSourceColumn column8 = new FeatureSourceColumn(@"Muarea", DbfColumnType.Character.ToString(), 64);
            FeatureSourceColumn column9 = new FeatureSourceColumn(@"Descrip", DbfColumnType.Character.ToString(), 128);
            FeatureSourceColumn column10 = new FeatureSourceColumn(@"Descarea", DbfColumnType.Character.ToString(), 128);
            FeatureSourceColumn column11 = new FeatureSourceColumn(@"Percover", DbfColumnType.Character.ToString(), 64);
            FeatureSourceColumn column12 = new FeatureSourceColumn(@"TreeID", DbfColumnType.Character.ToString(), 64);
            FeatureSourceColumn column13 = new FeatureSourceColumn(@"RecID", DbfColumnType.Character.ToString(), 64);
            ssurgoLayer.Columns.Add(column1);
            ssurgoLayer.Columns.Add(column2);
            ssurgoLayer.Columns.Add(column3);
            ssurgoLayer.Columns.Add(column4);
            ssurgoLayer.Columns.Add(column5);
            ssurgoLayer.Columns.Add(column6);
            ssurgoLayer.Columns.Add(column7);
            ssurgoLayer.Columns.Add(column8);
            ssurgoLayer.Columns.Add(column9);
            ssurgoLayer.Columns.Add(column10);
            ssurgoLayer.Columns.Add(column11);
            ssurgoLayer.Columns.Add(column12);
            ssurgoLayer.Columns.Add(column13);
            ssurgoLayer.FeatureSource.Close();
            ssurgoLayer.FeatureSource.Projection = projection;
        }

        private void LoadSSurgpOverlayOntoMap() {
            EnsureSSurgoLayerIsOpen();
            EnsureCookieCutLayerIsOpen();
            try {
                surgoareas = new Dictionary<string, double>(); 
                var shapelayerfeatures = fieldsLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);
                if (shapelayerfeatures.Count > 0) {
                    var shapelayercolumns = fieldsLayer.FeatureSource.GetColumns();
                    RectangleShape wfsextent = ExtentHelper.GetBoundingBoxOfItems(shapelayerfeatures);
                    double extentarea = wfsextent.GetArea(GeographyUnit.Meter, mapAreaUnit);
                    RectangleShape wfsextentinternal = projection.ConvertToInternalProjection(wfsextent) as RectangleShape;
                    System.Collections.ObjectModel.Collection<Feature> featuressss = wfsFeatureLayer.FeatureSource.GetFeaturesInsideBoundingBox(wfsextentinternal, ReturningColumnsType.AllColumns);
                    //System.Collections.ObjectModel.Collection<Feature> featuress = wfsFeatureLayer.QueryTools.GetFeaturesWithinDistanceOf(wfsextentinternal, GeographyUnit.DecimalDegree, DistanceUnit.Feet, 100, ReturningColumnsType.AllColumns);
                    //System.Collections.ObjectModel.Collection<Feature> featurestouching = wfsFeatureLayer.QueryTools.GetFeaturesTouching(wfsextentinternal, ReturningColumnsType.AllColumns);
                    //System.Collections.ObjectModel.Collection<Feature> featuresss = wfsFeatureLayer.FeatureSource.GetAllFeatures(ReturningColumnsType.NoColumns);
                    //System.Collections.ObjectModel.Collection<Feature> featuresoverlapping = wfsFeatureLayer.QueryTools.GetFeaturesOverlapping(extentpoly, new string[] { "musym" });
                    //System.Collections.ObjectModel.Collection<Feature> featurescrossing = wfsFeatureLayer.QueryTools.GetFeaturesCrossing(extentpoly, new string[] { "musym" });
                    //System.Collections.ObjectModel.Collection<Feature> featuresintersecting = wfsFeatureLayer.QueryTools.GetFeaturesIntersecting(extentpoly, new string[] { "musym" });
                    //System.Collections.ObjectModel.Collection<Feature> featuresss = wfsFeatureLayer.FeatureSource.GetAllFeatures(ReturningColumnsType.NoColumns);
                    //System.Collections.ObjectModel.Collection<Feature> featurescontains = wfsFeatureLayer.FeatureSource.SpatialQuery(extentpoly, QueryType.Contains, ReturningColumnsType.AllColumns);
                    //System.Collections.ObjectModel.Collection<Feature> featurescrosses = wfsFeatureLayer.FeatureSource.SpatialQuery(extentpoly, QueryType.Crosses, ReturningColumnsType.AllColumns);
                    //System.Collections.ObjectModel.Collection<Feature> featuresdisjont = wfsFeatureLayer.FeatureSource.SpatialQuery(extentpoly, QueryType.Disjoint, ReturningColumnsType.AllColumns);
                    //System.Collections.ObjectModel.Collection<Feature> featuresintersects = wfsFeatureLayer.FeatureSource.SpatialQuery(extentpoly, QueryType.Intersects, new string[] { "musym" });
                    //System.Collections.ObjectModel.Collection<Feature> featuresoverlaps = wfsFeatureLayer.FeatureSource.SpatialQuery(extentpoly, QueryType.Overlaps, new string[] { "musym" });
                    //System.Collections.ObjectModel.Collection<Feature> featurestopo = wfsFeatureLayer.FeatureSource.SpatialQuery(extentpoly, QueryType.TopologicalEqual, ReturningColumnsType.AllColumns);
                    //System.Collections.ObjectModel.Collection<Feature> featurestouches = wfsFeatureLayer.FeatureSource.SpatialQuery(extentpoly, QueryType.Touches, new string[] { "musym" });
                    //System.Collections.ObjectModel.Collection<Feature> featureswithin = wfsFeatureLayer.FeatureSource.SpatialQuery(extentpoly, QueryType.Within, ReturningColumnsType.AllColumns);

                    foreach (Feature feat in featuressss) {
                        string featkey = feat.ColumnValues["musym"];
                        string featname = feat.ColumnValues["muname"];
                        string featdesc = string.Format("{0}: {1}", featkey, featname);
                        feat.ColumnValues.Add("description", featdesc);
                        BaseShape featureshape = feat.GetShape();
                        MultipolygonShape multipolyshape = new MultipolygonShape();
                        if (featureshape is MultipolygonShape) {
                            multipolyshape = featureshape as MultipolygonShape;
                        }
                        else {
                            PolygonShape polyshape = featureshape as PolygonShape;
                            multipolyshape.Polygons.Add(polyshape);
                        }
                        double featarea = multipolyshape.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
                        feat.ColumnValues.Add("muareaacres", featarea.ToString("N5"));
                        string featdesc2 = string.Format("{0}: {1}: {2}", featkey, featname, featarea.ToString("N2"));
                        feat.ColumnValues.Add("descriptionarea", featdesc);
                        Feature newfeature = new Feature(featureshape);
                        newfeature.ColumnValues.Add(@"Areasym", feat.ColumnValues[@"areasymbol"]);
                        newfeature.ColumnValues.Add(@"Spatver", feat.ColumnValues[@"spatialversion"]);
                        newfeature.ColumnValues.Add(@"Musym", feat.ColumnValues[@"musym"]);
                        newfeature.ColumnValues.Add(@"Natmusym", feat.ColumnValues[@"nationalmusym"]);
                        newfeature.ColumnValues.Add(@"Mukey", feat.ColumnValues[@"mukey"]);
                        newfeature.ColumnValues.Add(@"Muname", feat.ColumnValues[@"muname"]);
                        newfeature.ColumnValues.Add(@"Mupoly", feat.ColumnValues[@"mupolygonkey"]);
                        newfeature.ColumnValues.Add(@"Muarea", feat.ColumnValues[@"muareaacres"]);
                        newfeature.ColumnValues.Add(@"Descrip", featdesc);
                        newfeature.ColumnValues.Add(@"Descarea", feat.ColumnValues[@"descriptionarea"]);
                        ssurgoLayer.InternalFeatures.Add(newfeature);

                        if (surgoareas.ContainsKey(featkey)) {
                            surgoareas[featkey] += featarea;
                        }
                        else {
                            surgoareas.Add(featkey, featarea);
                        }
                    }
                }
            }
            catch (Exception ex) {
                log.ErrorException("Caught an exception while working with nrcs info", ex);
                //System.Windows.MessageBox.Show("ImportTools - Caught an exception while working with a simple draw shape.");
            }
        }

        private void CreateCookieCutInMemoryLayerAndOverlay() {
            cookiecutLayer = new InMemoryFeatureLayer();
            cookiecutLayer.FeatureSource.Open();
            FeatureSourceColumn column1 = new FeatureSourceColumn(@"Areasym", DbfColumnType.Character.ToString(), 64);
            FeatureSourceColumn column2 = new FeatureSourceColumn(@"Spatver", DbfColumnType.Character.ToString(), 64);
            FeatureSourceColumn column3 = new FeatureSourceColumn(@"Musym", DbfColumnType.Character.ToString(), 64);
            FeatureSourceColumn column4 = new FeatureSourceColumn(@"Natmusym", DbfColumnType.Character.ToString(), 64);
            FeatureSourceColumn column5 = new FeatureSourceColumn(@"Mukey", DbfColumnType.Character.ToString(), 64);
            FeatureSourceColumn column6 = new FeatureSourceColumn(@"Muname", DbfColumnType.Character.ToString(), 64);
            FeatureSourceColumn column7 = new FeatureSourceColumn(@"Mupoly", DbfColumnType.Character.ToString(), 64);
            FeatureSourceColumn column8 = new FeatureSourceColumn(@"Muarea", DbfColumnType.Character.ToString(), 64);
            FeatureSourceColumn column9 = new FeatureSourceColumn(@"Descrip", DbfColumnType.Character.ToString(), 128);
            FeatureSourceColumn column10 = new FeatureSourceColumn(@"Descarea", DbfColumnType.Character.ToString(), 128);
            FeatureSourceColumn column11 = new FeatureSourceColumn(@"Percover", DbfColumnType.Character.ToString(), 64);
            FeatureSourceColumn column12 = new FeatureSourceColumn(@"TreeID", DbfColumnType.Character.ToString(), 64);
            FeatureSourceColumn column13 = new FeatureSourceColumn(@"RecID", DbfColumnType.IntegerInBinary.ToString(), 8);
            cookiecutLayer.Columns.Add(column1);
            cookiecutLayer.Columns.Add(column2);
            cookiecutLayer.Columns.Add(column3);
            cookiecutLayer.Columns.Add(column4);
            cookiecutLayer.Columns.Add(column5);
            cookiecutLayer.Columns.Add(column6);
            cookiecutLayer.Columns.Add(column7);
            cookiecutLayer.Columns.Add(column8);
            cookiecutLayer.Columns.Add(column9);
            cookiecutLayer.Columns.Add(column10);
            cookiecutLayer.Columns.Add(column11);
            cookiecutLayer.Columns.Add(column12);
            cookiecutLayer.Columns.Add(column13);
            cookiecutLayer.FeatureSource.Close();
            cookiecutLayer.FeatureSource.Projection = projection;
        }

        public void OnColorUpdated() {
            var mapsettings = clientEndpoint.GetMapSettings();
            foreach (var ci in coloritems) {
                MapColorItem mci = new MapColorItem() { Key = ci.Key, Label = ci.Label, CropName = ci.CropName, MapColor = ci.MapColorInt, Visible = ci.Visible };
                if (mapsettings.SSurgoColors.ContainsKey(ci.Key)) {
                    mapsettings.SSurgoColors[ci.Key].MapColor = ci.MapColorInt;
                    mapsettings.SSurgoColors[ci.Key].Visible = ci.Visible;
                }
                else {
                    mapsettings.SSurgoColors.Add(ci.Key, mci);
                }
            }
            clientEndpoint.SaveMapSettings(mapsettings);
            RaisePropertyChanged("ColorItem");
            RaisePropertyChanged("ColorItems");
            RaisePropertyChanged("CropzoneColors");

        }

        public void CutOutLayer() {
            totalarea = 0;
            dictarea = new Dictionary<string,double>();
            dictpercent = new Dictionary<string,double>();
            if (fieldsLayer.InternalFeatures.Count == 0) { return; }
            double progresspercent = 0;
            int intpercent = 0;
            int nrecords = 0;
            cookiecutLayer.InternalFeatures.Clear();
            try {
                foreach (Feature feature in fieldsLayer.InternalFeatures) {
                    BaseShape baseshape = feature.GetShape();
                    MultipolygonShape fieldShape = new MultipolygonShape();
                    if (baseshape is MultipolygonShape) {
                        fieldShape = baseshape as MultipolygonShape;
                    }
                    else {
                        PolygonShape ps = baseshape as PolygonShape;
                        fieldShape.Polygons.Add(ps);
                    }
                    if (fieldShape == null) { continue; }
                    double fieldshapearea = fieldShape.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
                    totalarea += fieldshapearea;
                }
                
                foreach (Feature feature in fieldsLayer.InternalFeatures) {
                    BaseShape baseshape = feature.GetShape();
                    MultipolygonShape fieldShape = new MultipolygonShape();
                    if (baseshape is MultipolygonShape) {
                        fieldShape = baseshape as MultipolygonShape;
                    }
                    else {
                        PolygonShape ps = baseshape as PolygonShape;
                        fieldShape.Polygons.Add(ps);
                    }
                    if (fieldShape == null) { continue; }
                    double fieldshapearea = fieldShape.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
                    foreach (Feature ssurgofeature in ssurgoLayer.InternalFeatures) {
                        try {
                            BaseShape basessurgo = ssurgofeature.GetShape();
                            MultipolygonShape ssurgoShape = new MultipolygonShape();
                            if (basessurgo is MultipolygonShape) {
                                ssurgoShape = basessurgo as MultipolygonShape;
                            }
                            else {
                                PolygonShape ps = basessurgo as PolygonShape;
                                ssurgoShape.Polygons.Add(ps);
                            }
                            if (ssurgoShape == null) { continue; }
                            string featkey = ssurgofeature.ColumnValues[@"Musym"];
                            string featname = ssurgofeature.ColumnValues[@"Muname"];
                            string featdesc = string.Format("{0}: {1}", featkey, featname);
                            if (!ssurgofeature.ColumnValues.ContainsKey(@"TreeID")) {
                                ssurgofeature.ColumnValues.Add(@"TreeID", feature.Id);
                            }
                            ssurgofeature.ColumnValues[@"TreeID"] = feature.Id;
                            if (fieldShape.Contains(ssurgoShape)) {
                                double featarea = ssurgoShape.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
                                double percentcoverage = 0;
                                if (fieldshapearea > 0) {
                                    percentcoverage = (featarea / fieldshapearea) * 100;
                                }
                                string featdesc2 = string.Format("{0}: {1}: {2}", featkey, featname, featarea.ToString("N2"));
                                string featdesc3 = string.Format("{0}: {1}: {2}%", featkey, featarea.ToString("N2"), percentcoverage.ToString("N2"));
                                ssurgofeature.ColumnValues[@"Muarea"] = featarea.ToString("N5");
                                ssurgofeature.ColumnValues[@"Descrip"] = featdesc;
                                ssurgofeature.ColumnValues[@"Descarea"] = featdesc2;
                                ssurgofeature.ColumnValues[@"Percover"] = percentcoverage.ToString("N2"); ;
                                nrecords++;
                                ssurgofeature.ColumnValues[@"RecID"] = nrecords.ToString("N0");
                                Feature cookiecurfeature = new Feature(basessurgo, ssurgofeature.ColumnValues);
                                cookiecutLayer.InternalFeatures.Add(ssurgofeature);
                                if (dictarea.ContainsKey(featkey)) {
                                    dictarea[featkey] += featarea;
                                }
                                else {
                                    dictarea.Add(featkey, featarea);
                                }
                                double percentagearea = (dictarea[featkey] / totalarea) * 100;
                                if (dictpercent.ContainsKey(featkey)) {
                                    dictpercent[featkey] = percentagearea;
                                }
                                else {
                                    dictpercent.Add(featkey, percentagearea);
                                }
                            }
                            else if (ssurgoShape.Contains(fieldShape)) {
                                double featarea = fieldShape.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
                                double percentcoverage = 0;
                                if (fieldshapearea > 0) {
                                    percentcoverage = (featarea / fieldshapearea) * 100;
                                }
                                string featdesc2 = string.Format("{0}: {1}: {2}", featkey, featname, featarea.ToString("N2"));
                                string featdesc3 = string.Format("{0}: {1}: {2}%", featkey, featarea.ToString("N2"), percentcoverage.ToString("N2"));
                                ssurgofeature.ColumnValues[@"Muarea"] = featarea.ToString("N5");
                                ssurgofeature.ColumnValues[@"Descrip"] = featdesc;
                                ssurgofeature.ColumnValues[@"Descarea"] = featdesc2;
                                ssurgofeature.ColumnValues[@"Percover"] = percentcoverage.ToString("N2");
                                nrecords++;
                                ssurgofeature.ColumnValues[@"RecID"] = nrecords.ToString("N0");
                                Feature cookiecurfeature = new Feature(baseshape, ssurgofeature.ColumnValues);
                                cookiecutLayer.InternalFeatures.Add(cookiecurfeature);
                                //IdentifySSurgoColors(featkey, featdesc);
                                if (dictarea.ContainsKey(featkey)) {
                                    dictarea[featkey] += featarea;
                                }
                                else {
                                    dictarea.Add(featkey, featarea);
                                }
                                double percentagearea = (dictarea[featkey] / totalarea) * 100;
                                if (dictpercent.ContainsKey(featkey)) {
                                    dictpercent[featkey] = percentagearea;
                                }
                                else {
                                    dictpercent.Add(featkey, percentagearea);
                                }
                            }
                            else if (fieldShape.Intersects(ssurgoShape)) {
                                MultipolygonShape overlapMultiPolygonShape = ssurgoShape.GetIntersection(fieldShape);
                                double featarea = overlapMultiPolygonShape.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
                                double percentcoverage = 0;
                                if (fieldshapearea > 0) {
                                    percentcoverage = (featarea / fieldshapearea) * 100;
                                }
                                string featdesc2 = string.Format("{0}: {1}: {2}", featkey, featname, featarea.ToString("N2"));
                                string featdesc3 = string.Format("{0}: {1}: {2}%", featkey, featarea.ToString("N2"), percentcoverage.ToString("N2"));
                                ssurgofeature.ColumnValues[@"Muarea"] = featarea.ToString("N5");
                                ssurgofeature.ColumnValues[@"Descrip"] = featdesc;
                                ssurgofeature.ColumnValues[@"Descarea"] = featdesc2;
                                ssurgofeature.ColumnValues[@"Percover"] = percentcoverage.ToString("N2"); ;
                                nrecords++;
                                ssurgofeature.ColumnValues[@"RecID"] = nrecords.ToString("N0");
                                Feature cookiecurfeature = new Feature(overlapMultiPolygonShape, ssurgofeature.ColumnValues);
                                cookiecutLayer.InternalFeatures.Add(cookiecurfeature);
                                if (dictarea.ContainsKey(featkey)) {
                                    dictarea[featkey] += featarea;
                                }
                                else {
                                    dictarea.Add(featkey, featarea);
                                }
                                double percentagearea = (dictarea[featkey] / totalarea) * 100;
                                if (dictpercent.ContainsKey(featkey)) {
                                    dictpercent[featkey] = percentagearea;
                                }
                                else {
                                    dictpercent.Add(featkey, percentagearea);
                                }
                            }
                        }
                        catch (Exception ex) {
                            string field = feature.ColumnValues[@"LdbLabel"];
                            string ssurgokey = ssurgofeature.ColumnValues[@"Musym"];
                            string ssurgoname = ssurgofeature.ColumnValues[@"Muname"];
                            log.Error("Exception while clipping ssurgo shapes", ex);
                        }
                    }
                }
            }
            catch (Exception ex) {
                log.Error("Exception while processing cookie cutter", ex);
            }
        }


        void ExportSelectedShapeFiles() {
            SelectedShapeExport sse = new SelectedShapeExport(clientEndpoint, projection, "SSurgo", FieldName, false);
            sse.ExportShapeSSurgoFile(cookiecutLayer);
        }

        void SaveSelectedLayerState() {
            SelectedShapeExport sse = new SelectedShapeExport(clientEndpoint, projection, "SSurgo", FieldName, false);
            sse.ExportSSurgoXml(cookiecutLayer);
        }

        private void SSurgoNrcsWfsTool(string cropname, Feature cookieCutFeature, Feature ssurgoFeature) {
            SSurgoViewModel ssurgonrcswfsvm = new SSurgoViewModel();
            ssurgonrcswfsvm.Execute(cropname, cookieCutFeature, ssurgoFeature);
            //ScreenDescriptor descriptor = null;
            //descriptor = new ScreenDescriptor("Landdb.Views.Secondary.Map.SSurgoView", "SSurgoVM", ssurgonrcswfsvm);
            //Messenger.Default.Send<ShowPopupMessage>(new ShowPopupMessage() { ScreenDescriptor = descriptor });
        }

        void OnShowSSurgoNrcsView(ShowSSurgoMessage message) {
            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
        }

        private string SoilTypeFactory(string descripiton) {
            string lowerdescription = descripiton.ToLower();
            string soiltype = string.Empty;
            if (lowerdescription.Contains("sandy clay loam")) {
                soiltype = Strings.SoilType_SandyClayLoam_Text;
                return soiltype;
            }
            if (lowerdescription.Contains("silty clay loam")) {
                soiltype = Strings.SoilType_SiltyClayLoam_Text;
                return soiltype;
            }
            if (lowerdescription.Contains("loamy sand")) {
                soiltype = Strings.SoilType_LoamySand_Text;
                return soiltype;
            }
            if (lowerdescription.Contains("sandy loam")) {
                soiltype = Strings.SoilType_SandyLoam_Text;
                return soiltype;
            }
            if (lowerdescription.Contains("silt loam")) {
                soiltype = Strings.SoilType_SiltLoam_Text;
                return soiltype;
            }
            if (lowerdescription.Contains("clay loam")) {
                soiltype = Strings.SoilType_ClayLoam_Text;
                return soiltype;
            }
            if (lowerdescription.Contains("silty clay")) {
                soiltype = Strings.SoilType_SiltyClay_Text;
                return soiltype;
            }
            if (lowerdescription.Contains("sandy clay")) {
                soiltype = Strings.SoilType_SandyClay_Text;
                return soiltype;
            }
            if (lowerdescription.Contains("clay")) {
                soiltype = Strings.SoilType_Clay_Text;
                if (lowerdescription.Contains("sand")) {
                    soiltype = Strings.SoilType_SandyClay_Text;
                    if (lowerdescription.Contains("loam")) {
                        soiltype = Strings.SoilType_SandyClayLoam_Text;
                    }
                }
                if (lowerdescription.Contains("silt")) {
                    soiltype = Strings.SoilType_SiltyClay_Text;
                    if (lowerdescription.Contains("loam")) {
                        soiltype = Strings.SoilType_SiltyClayLoam_Text;
                    }
                }
                if (lowerdescription.Contains("loam")) {
                    soiltype = Strings.SoilType_ClayLoam_Text;
                }
                return soiltype;
            }
            if (lowerdescription.Contains("silt")) {
                soiltype = Strings.SoilType_Silt_Text;
                if (lowerdescription.Contains("clay")) {
                    soiltype = Strings.SoilType_SiltyClay_Text;
                    if (lowerdescription.Contains("loam")) {
                        soiltype = Strings.SoilType_SiltyClayLoam_Text;
                    }
                }
                if (lowerdescription.Contains("loam")) {
                    soiltype = Strings.SoilType_SiltLoam_Text;
                }
                return soiltype;
            }
            if (lowerdescription.Contains("sand")) {
                soiltype = Strings.SoilType_Sand_Text;
                if (lowerdescription.Contains("clay")) {
                    soiltype = Strings.SoilType_SandyClay_Text;
                    if (lowerdescription.Contains("loam")) {
                        soiltype = Strings.SoilType_SandyClayLoam_Text;
                    }
                }
                if (lowerdescription.Contains("loam")) {
                    soiltype = Strings.SoilType_SandyLoam_Text;
                }
                return soiltype;
            }
            if (lowerdescription.Contains("loam")) {
                soiltype = Strings.SoilType_Loam_Text;
                if (lowerdescription.Contains("clay")) {
                    soiltype = Strings.SoilType_ClayLoam_Text;
                }
                if (lowerdescription.Contains("silt")) {
                    soiltype = Strings.SoilType_SiltLoam_Text;
                    if (lowerdescription.Contains("clay")) {
                        soiltype = Strings.SoilType_SiltyClayLoam_Text;
                    }
                }
                if (lowerdescription.Contains("sand")) {
                    soiltype = Strings.SoilType_LoamySand_Text;
                    if (lowerdescription.Contains("clay")) {
                        soiltype = Strings.SoilType_SiltyClayLoam_Text;
                    }
                }
                return soiltype;
            }
            return soiltype;
        }

        private string SoilTextureFactory(double sandtotal_r, double silttotal_r, double claytotal_r) {
            string soiltype = string.Empty;
            if (sandtotal_r == 0 && silttotal_r == 0 && claytotal_r == 0) {
                return soiltype;
            }
            if (sandtotal_r >= 80 && silttotal_r < 10 && claytotal_r < 10) {
                soiltype = Strings.SoilType_Sand_Text;
                return soiltype;
            }
            if (sandtotal_r < 20 && silttotal_r >= 80 && claytotal_r < 10) {
                soiltype = Strings.SoilType_Silt_Text;
                return soiltype;
            }
            if (sandtotal_r < 40 && silttotal_r < 40 && claytotal_r >= 60) {
                soiltype = Strings.SoilType_Clay_Text;
                return soiltype;
            }
            if (sandtotal_r >= 70 && sandtotal_r < 90 && silttotal_r < 30 && claytotal_r < 20) {
                soiltype = Strings.SoilType_LoamySand_Text;
                return soiltype;
            }
            if (sandtotal_r < 50 && silttotal_r >= 50 && silttotal_r < 90 && claytotal_r < 30) {
                soiltype = Strings.SoilType_SiltLoam_Text;
                return soiltype;
            }
            if (sandtotal_r >= 50 && sandtotal_r < 70 && silttotal_r >= 10 && silttotal_r < 50 && claytotal_r < 20) {
                soiltype = Strings.SoilType_SandyLoam_Text;
                return soiltype;
            }
            if (sandtotal_r >= 40 && sandtotal_r < 50 && silttotal_r >= 40 && silttotal_r < 50 && claytotal_r < 10) {
                soiltype = Strings.SoilType_SandyLoam_Text;
                return soiltype;
            }
            if (sandtotal_r >= 40 && sandtotal_r < 60 && silttotal_r < 20 && claytotal_r >= 40 && claytotal_r < 60) {
                soiltype = Strings.SoilType_SandyClay_Text;
                return soiltype;
            }
            if (sandtotal_r < 20 && silttotal_r >= 40 && silttotal_r < 60 && claytotal_r >= 40 && claytotal_r < 60) {
                soiltype = Strings.SoilType_SiltyClay_Text;
                return soiltype;
            }
            if (sandtotal_r >= 40 && sandtotal_r < 80 && silttotal_r < 30 && claytotal_r >= 20 && claytotal_r < 40) {
                soiltype = Strings.SoilType_SandyClayLoam_Text;
                return soiltype;
            }
            if (sandtotal_r < 20 && silttotal_r >= 40 && silttotal_r < 70 && claytotal_r >= 30 && claytotal_r < 40) {
                soiltype = Strings.SoilType_SiltyClayLoam_Text;
                return soiltype;
            }
            if (sandtotal_r >= 20 && sandtotal_r < 40 && silttotal_r >= 20 && silttotal_r < 50 && claytotal_r >= 30 && claytotal_r < 40) {
                soiltype = Strings.SoilType_ClayLoam_Text;
                return soiltype;
            }
            if (sandtotal_r >= 20 && sandtotal_r < 50 && silttotal_r >= 30 && silttotal_r < 50 && claytotal_r >= 10 && claytotal_r < 30) {
                soiltype = Strings.SoilType_Loam_Text;
                return soiltype;
            }
            return soiltype;
        }

        private double HydrologicalFactory(string description) {
            string lowerdescription = description.ToLower();
            double soiltype = 0;
            if (lowerdescription.Contains("a")) {
                soiltype = .325;
                return soiltype;
            }
            if (lowerdescription.Contains("b")) {
                soiltype = .225;
                return soiltype;
            }
            if (lowerdescription.Contains("c")) {
                soiltype = .10;
                return soiltype;
            }
            if (lowerdescription.Contains("d")) {
                soiltype = .025;
                return soiltype;
            }
            if (lowerdescription.Contains("a/d")) {
                soiltype = .31;
                return soiltype;
            }
            if (lowerdescription.Contains("b/d")) {
                soiltype = .225;
                return soiltype;
            }
            if (lowerdescription.Contains("c/d")) {
                soiltype = .10;
                return soiltype;
            }
            return soiltype;
        }

        private string InfiltrationFactory(double inchesperhour, bool hadDgroup) {
            string hgroup = string.Empty;
            if (inchesperhour < .05) {
                hgroup = "D";
            }
            if (inchesperhour >= .05 && inchesperhour < .15 ) {
                hgroup = "C";
                if (hadDgroup) {
                    hgroup = string.Format("{0}/D", "C");
                }
            }
            if (inchesperhour >= .15 && inchesperhour < .30) {
                hgroup = "B";
                if (hadDgroup) {
                    hgroup = string.Format("{0}/D", "B");
                }
            }
            if (inchesperhour >= .30) {
                hgroup = "A";
                if (hadDgroup) {
                    hgroup = string.Format("{0}/D", "A");
                }
            }
            return hgroup;
        }
    }
}
