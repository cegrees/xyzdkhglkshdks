﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GalaSoft.MvvmLight;
using System.Collections.ObjectModel;
using Landdb.Domain.ReadModels.MapAnnotation;
using Landdb.ViewModel.Fields;
using Landdb.Client.Infrastructure;

namespace Landdb.ViewModel.Secondary.Map {
    public class AnnotationStyleTreeItemViewModel : AbstractTreeItemViewModel {
        public MapAnnotationCategory Style { get; set; }
        public string Name { get; }
        public override IIdentity Id => null;
        public AnnotationsDisplayItem AnnotationsDisplayItem => new AnnotationsDisplayItem(Name);
        public AnnotationStyleTreeItemViewModel(MapAnnotationCategory annotationCategoryStyle, List<MapAnnotationListItem> annotationsCreatedByUser, AbstractTreeItemViewModel parent, bool sortChildrenAlphabetically, Action<AbstractTreeItemViewModel> onCheckedChanged = null, Predicate<object> filter = null)
            : base(null, onCheckedChanged, null, filter) {
            Name = annotationCategoryStyle.Name;
            Style = annotationCategoryStyle;

            var annotationNames = from eachAnnotation in annotationsCreatedByUser
                    select new AnnotationShapeTreeItemViewModel(eachAnnotation, this, sortChildrenAlphabetically, onCheckedChanged, filter); // and the horse it rode in on...

            if (sortChildrenAlphabetically) 
                annotationNames = annotationNames.OrderBy(x => x.Name);

            Children = new ObservableCollection<AbstractTreeItemViewModel>(annotationNames);
        }
        public override string ToString() {
            return Name;
        }
    }
}