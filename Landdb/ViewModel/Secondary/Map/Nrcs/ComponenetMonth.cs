﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using ThinkGeo.MapSuite.Core;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace Landdb.ViewModel.Secondary.Map.Nrcs {
    public class ComponenetMonth : INrcsFilter {

        public ComponenetMonth(string crop, GeoCollection<Feature> features) {

            StringBuilder sb = new StringBuilder(@"SELECT saversion, saverest, l.areasymbol, l.areaname, l.lkey, musym, muname, museq, mu.mukey, comppct_r, compname, localphase, slope_r, c.cokey, cy.monthseq, cy.month, cy.flodfreqcl, cy.floddurcl, cy.pondfreqcl, cy.ponddep_l, cy.ponddep_r, cy.ponddep_h, cy.dlyavgprecip_l, cy.dlyavgprecip_r, cy.dlyavgprecip_h, cy.dlyavgpotet_l, cy.dlyavgpotet_r, cy.dlyavgpotet_h, cy.comonthkey, st.soitempmm, st.soitempdept_l, st.soitempdept_r, st.soitempdept_h, st.soitempdepb_l, st.soitempdepb_r, st.soitempdepb_h, st.cosoiltempkey, sm.soimoistdept_l, sm.soimoistdept_r, sm.soimoistdept_h, sm.soimoistdepb_l, sm.soimoistdepb_r, sm.soimoistdepb_h, sm.soimoiststat, sm.cosoilmoistkey FROM sacatalog sac INNER JOIN legend l ON l.areasymbol = sac.areasymbol INNER JOIN mapunit mu ON mu.lkey = l.lkey LEFT OUTER JOIN component c ON c.mukey = mu.mukey LEFT OUTER JOIN comonth cy ON cy.cokey = c.cokey LEFT OUTER JOIN cosoiltemp st ON st.comonthkey = cy.comonthkey LEFT OUTER JOIN cosoilmoist sm ON sm.comonthkey = cy.comonthkey WHERE (");
            bool notfirstrecord = false;
            foreach (Feature feature in features) {
                string musym = feature.ColumnValues["musym"];
                string mukey = feature.ColumnValues["mukey"];

                if (notfirstrecord) {
                    sb.Append(string.Format(" or (mu.mukey = '{0}' and mu.musym = '{1}') ", mukey, musym));
                }
                else {
                    sb.Append(string.Format("(mu.mukey = '{0}' and mu.musym = '{1}') ", mukey, musym));
                }
                notfirstrecord = true;
            }
            sb.Append(")");

            string expandedstring = sb.ToString();
            NrcsWebService webservice = new NrcsWebService();
            XDocument doc1 = webservice.CreateWebRequest(expandedstring);

            QueryTheData(doc1);
        }

        public ComponenetMonth(string crop, GeoCollection<Feature> features, string cokey) {

            StringBuilder sb = new StringBuilder(@"SELECT saversion, saverest, l.areasymbol, l.areaname, l.lkey, musym, muname, museq, mu.mukey, comppct_r, compname, localphase, slope_r, c.cokey, cy.monthseq, cy.month, cy.flodfreqcl, cy.floddurcl, cy.pondfreqcl, cy.ponddep_l, cy.ponddep_r, cy.ponddep_h, cy.dlyavgprecip_l, cy.dlyavgprecip_r, cy.dlyavgprecip_h, cy.dlyavgpotet_l, cy.dlyavgpotet_r, cy.dlyavgpotet_h, cy.comonthkey, st.soitempmm, st.soitempdept_l, st.soitempdept_r, st.soitempdept_h, st.soitempdepb_l, st.soitempdepb_r, st.soitempdepb_h, st.cosoiltempkey, sm.soimoistdept_l, sm.soimoistdept_r, sm.soimoistdept_h, sm.soimoistdepb_l, sm.soimoistdepb_r, sm.soimoistdepb_h, sm.soimoiststat, sm.cosoilmoistkey FROM sacatalog sac INNER JOIN legend l ON l.areasymbol = sac.areasymbol INNER JOIN mapunit mu ON mu.lkey = l.lkey LEFT OUTER JOIN component c ON c.mukey = mu.mukey LEFT OUTER JOIN comonth cy ON cy.cokey = c.cokey LEFT OUTER JOIN cosoiltemp st ON st.comonthkey = cy.comonthkey LEFT OUTER JOIN cosoilmoist sm ON sm.comonthkey = cy.comonthkey WHERE (");
            bool notfirstrecord = false;
            foreach (Feature feature in features) {
                string musym = feature.ColumnValues["musym"];
                string mukey = feature.ColumnValues["mukey"];

                if (notfirstrecord) {
                    sb.Append(string.Format(" or (mu.mukey = '{0}' and mu.musym = '{1}' and cy.cokey = '{2}') ", mukey, musym, cokey));
                }
                else {
                    sb.Append(string.Format("(mu.mukey = '{0}' and mu.musym = '{1}' and cy.cokey = '{2}') ", mukey, musym, cokey));
                }
                notfirstrecord = true;
            }
            sb.Append(")");

            string expandedstring = sb.ToString();
            NrcsWebService webservice = new NrcsWebService();
            XDocument doc1 = webservice.CreateWebRequest(expandedstring);

            QueryTheData(doc1);
        }

        public string Name {
            get { return "Soil Moisture and Temperature"; }
        }

        private void QueryTheData(XDocument doc) {
            ObservableCollection<COMONTH> cropList = new ObservableCollection<COMONTH>();
            try {

                var results1 = from st in doc.Descendants()
                               select st;

                //where st.Name.LocalName == "NewDataSet"
                foreach (var r in results1) {
                    if (r.Name.LocalName == "NewDataSet") {
                        var results2 = from ele in r.Descendants()
                                       select ele;

                        foreach (var tab in results2) {
                            if (tab.Name.LocalName == "Table") {
                                
                                var results3 = from ele in tab.Elements()
                                               select ele;

                                List<XElement> results33 = results3.ToList();

                                COMONTH cropyld = new COMONTH(
                                    results33[0].Value, results33[1].Value, results33[2].Value, results33[3].Value, results33[4].Value, results33[5].Value, results33[6].Value, results33[7].Value, results33[8].Value, results33[9].Value, 
                                    results33[10].Value, results33[11].Value, results33[12].Value, results33[13].Value, results33[14].Value, results33[15].Value, results33[16].Value, results33[17].Value, results33[18].Value, results33[19].Value,
                                    results33[20].Value, results33[21].Value, results33[22].Value, results33[23].Value, results33[24].Value, results33[25].Value, results33[26].Value, results33[27].Value, results33[28].Value, results33[29].Value,
                                    results33[30].Value, results33[31].Value, results33[32].Value, results33[33].Value, results33[34].Value, results33[35].Value, results33[36].Value, results33[37].Value, results33[38].Value, results33[39].Value,
                                    results33[40].Value, results33[41].Value, results33[42].Value, results33[43].Value, results33[44].Value);

                                cropList.Add(cropyld);
                            }
                        }
                        break;
                    }
                }
            }
            catch (Exception ex1) {
                Console.WriteLine("Exception of type " + ex1.GetType().Name);
                throw;
            }

            //DataList = cropList;
            DataList = new ObservableCollection<COMONTH>(cropList.OrderBy(x => x.Monthint));
        }

        private ObservableCollection<COMONTH> dataList = new ObservableCollection<COMONTH>();
        public ObservableCollection<COMONTH> DataList {
            get { return dataList; }
            set { dataList = value; }
        }
    }

    public class COMONTH : ViewModelBase {

        public COMONTH() {
        }

        public COMONTH(string saversion, string saverest, string areasymbol, string areaname, string lkey, string musym, string muname, string museq, string mukey, string comppct_r, string compname, string localphase, string slope_r, string cokey, string monthseq, string month, string flodfreqcl, string floddurcl, string pondfreqcl, string ponddep_l, string ponddep_r, string ponddep_h, string dlyavgprecip_l, string dlyavgprecip_r, string dlyavgprecip_h, string dlyavgpotet_l, string dlyavgpotet_r, string dlyavgpotet_h, string comonthkey, string soitempmm, string soitempdept_l, string soitempdept_r, string soitempdept_h, string soitempdepb_l, string soitempdepb_r, string soitempdepb_h, string cosoiltempkey, string soimoistdept_l, string soimoistdept_r, string soimoistdept_h, string soimoistdepb_l, string soimoistdepb_r, string soimoistdepb_h, string soimoiststat, string cosoilmoistkey) {

            this.saversion = saversion;
            this.saverest = saverest;
            this.areasymbol = areasymbol;
            this.areaname = areaname;
            this.lkey = lkey;
            this.musym = musym;
            this.muname = muname;
            this.museq = museq;
            this.mukey = mukey;
            this.comppct_r = comppct_r;
            this.compname = compname;
            this.localphase = localphase;
            this.slope_r = slope_r;
            this.cokey = cokey;
            this.monthseq = monthseq;
            if (!string.IsNullOrEmpty(monthseq)) {
                monthint = System.Convert.ToInt32(monthseq);
            }
            this.month = month;
            this.flodfreqcl = flodfreqcl;
            this.floddurcl = floddurcl;
            this.pondfreqcl = pondfreqcl;
            this.ponddep_l = ponddep_l;
            this.ponddep_r = ponddep_r;
            this.ponddep_h = ponddep_h;
            this.dlyavgprecip_l = dlyavgprecip_l;
            this.dlyavgprecip_r = dlyavgprecip_r;
            this.dlyavgprecip_h = dlyavgprecip_h;
            this.dlyavgpotet_l = dlyavgpotet_l;
            this.dlyavgpotet_r = dlyavgpotet_r;
            this.dlyavgpotet_h = dlyavgpotet_h;
            this.comonthkey = comonthkey;
            this.soitempmm = soitempmm;
            this.soitempdept_l = soitempdept_l;
            this.soitempdept_r = soitempdept_r;
            this.soitempdept_h = soitempdept_h;
            this.soitempdepb_l = soitempdepb_l;
            this.soitempdepb_r = soitempdepb_r;
            this.soitempdepb_h = soitempdepb_h;
            this.cosoiltempkey = cosoiltempkey;
            this.soimoistdept_l = soimoistdept_l;
            this.soimoistdept_r = soimoistdept_r;
            this.soimoistdept_h = soimoistdept_h;
            this.soimoistdepb_l = soimoistdepb_l;
            this.soimoistdepb_r = soimoistdepb_r;
            this.soimoistdepb_h = soimoistdepb_h;
            this.soimoiststat = soimoiststat;
            this.cosoilmoistkey = cosoilmoistkey;
        }

        private string saversion;
        public string Saversion {
            get { return saversion; }
            set { saversion = value; }
        }

        private string saverest;
        public string Saverest {
            get { return saverest; }
            set { saverest = value; }
        }

        private string areasymbol;
        public string Areasymbol {
            get { return areasymbol; }
            set { areasymbol = value; }
        }

        private string areaname;
        public string Areaname {
            get { return areaname; }
            set { areaname = value; }
        }

        private string lkey;
        public string Ikey {
            get { return lkey; }
            set { lkey = value; }
        }

        private string musym;
        public string Musym {
            get { return musym; }
            set { musym = value; }
        }

        private string muname;
        public string Muname {
            get { return muname; }
            set { muname = value; }
        }

        private string museq;
        public string Museq {
            get { return museq; }
            set { museq = value; }
        }

        private string mukey;
        public string Mukey {
            get { return mukey; }
            set { mukey = value; }
        }

        private string comppct_r;
        public string Comppct_r {
            get { return comppct_r; }
            set { comppct_r = value; }
        }

        private string compname;
        public string Compname {
            get { return compname; }
            set { compname = value; }
        }

        private string localphase;
        public string Localphase {
            get { return localphase; }
            set { localphase = value; }
        }

        private string slope_r;
        public string Slope_r {
            get { return slope_r; }
            set { slope_r = value; }
        }

        private string cokey;
        public string Cokey {
            get { return cokey; }
            set { cokey = value; }
        }

        private string monthseq;
        public string Monthseq {
            get { return monthseq; }
            set { monthseq = value; }
        }

        private int monthint = 0;
        public int Monthint {
            get { return monthint; }
            set { monthint = value; }
        }

        private string month;
        public string Month {
            get { return month; }
            set { month = value; }
        }

        private string flodfreqcl;
        public string Flodfreqcl {
            get { return flodfreqcl; }
            set { flodfreqcl = value; }
        }

        private string floddurcl;
        public string Floddurcl {
            get { return floddurcl; }
            set { floddurcl = value; }
        }

        private string pondfreqcl;
        public string Pondfreqcl {
            get { return pondfreqcl; }
            set { pondfreqcl = value; }
        }

        private string ponddep_l;
        public string Ponddep_l {
            get { return ponddep_l; }
            set { ponddep_l = value; }
        }

        private string ponddep_r;
        public string Ponddep_r {
            get { return ponddep_r; }
            set { ponddep_r = value; }
        }

        private string ponddep_h;
        public string Ponddep_h {
            get { return ponddep_h; }
            set { ponddep_h = value; }
        }

        private string dlyavgprecip_l;
        public string Dlyavgprecip_l {
            get { return dlyavgprecip_l; }
            set { dlyavgprecip_l = value; }
        }

        private string dlyavgprecip_r;
        public string Dlyavgprecip_r {
            get { return dlyavgprecip_r; }
            set { dlyavgprecip_r = value; }
        }

        private string dlyavgprecip_h;
        public string Dlyavgprecip_h {
            get { return dlyavgprecip_h; }
            set { dlyavgprecip_h = value; }
        }

        private string dlyavgpotet_l;
        public string Dlyavgpotet_l {
            get { return dlyavgpotet_l; }
            set { dlyavgpotet_l = value; }
        }

        private string dlyavgpotet_r;
        public string Dlyavgpotet_r {
            get { return dlyavgpotet_r; }
            set { dlyavgpotet_r = value; }
        }

        private string dlyavgpotet_h;
        public string Dlyavgpotet_h {
            get { return dlyavgpotet_h; }
            set { dlyavgpotet_h = value; }
        }

        private string comonthkey;
        public string Comonthkey {
            get { return comonthkey; }
            set { comonthkey = value; }
        }

        private string soitempmm;
        public string Soitempmm {
            get { return soitempmm; }
            set { soitempmm = value; }
        }

        private string soitempdept_l;
        public string Soitempdept_l {
            get {
                if(string.IsNullOrEmpty(soitempdept_l)) { return @"0"; }
                return soitempdept_l; 
            }
            set { soitempdept_l = value; }
        }

        private string soitempdept_r;
        public string Soitempdept_r {
            get { return soitempdept_r; }
            set { soitempdept_r = value; }
        }

        private string soitempdept_h;
        public string Soitempdept_h {
            get { 
                if(string.IsNullOrEmpty(soitempdept_h)) { return @"0"; }
                return soitempdept_h; 
            }
            set { soitempdept_h = value; }
        }

        public int Soitempdept_diff {
            get { return System.Convert.ToInt32(Soitempdept_h) - System.Convert.ToInt32(Soitempdept_l); }
        }

        private string soitempdepb_l;
        public string Soitempdepb_l {
            get {
                if (string.IsNullOrEmpty(soitempdepb_l)) { return @"0"; }
                return soitempdepb_l; 
            }
            set { soitempdepb_l = value; }
        }

        private string soitempdepb_r;
        public string Soitempdepb_r {
            get { return soitempdepb_r; }
            set { soitempdepb_r = value; }
        }

        private string soitempdepb_h;
        public string Soitempdepb_h {
            get {
                if (string.IsNullOrEmpty(soitempdepb_h)) { return @"0"; }
                return soitempdepb_h; 
            }
            set { soitempdepb_h = value; }
        }

        public int Soitempdepb_diff {
            get { return System.Convert.ToInt32(Soitempdepb_h) - System.Convert.ToInt32(Soitempdepb_l); }
        }

        private string cosoiltempkey;
        public string Cosoiltempkey {
            get { return cosoiltempkey; }
            set { cosoiltempkey = value; }
        }

        private string soimoistdept_l;
        public string Soimoistdept_l {
            get {
                if (string.IsNullOrEmpty(soimoistdept_l)) { return @"0"; }
                return soimoistdept_l; 
            }
            set { soimoistdept_l = value; }
        }

        private string soimoistdept_r;
        public string Soimoistdept_r {
            get { return soimoistdept_r; }
            set { soimoistdept_r = value; }
        }

        private string soimoistdept_h;
        public string Soimoistdept_h {
            get {
                if (string.IsNullOrEmpty(soimoistdept_h)) { return @"0"; }
                return soimoistdept_h; 
            }
            set { soimoistdept_h = value; }
        }

        public int Soimoistdept_diff {
            get { return  System.Convert.ToInt32(Soimoistdept_h) - System.Convert.ToInt32(Soimoistdept_l); }
        }

        private string soimoistdepb_l;
        public string Soimoistdepb_l {
            get {
                if (string.IsNullOrEmpty(soimoistdepb_l)) { return @"0"; }
                return soimoistdepb_l; 
            }
            set { soimoistdepb_l = value; }
        }

        private string soimoistdepb_r;
        public string Soimoistdepb_r {
            get { return soimoistdepb_r; }
            set { soimoistdepb_r = value; }
        }

        private string soimoistdepb_h;
        public string Soimoistdepb_h {
            get {
                if (string.IsNullOrEmpty(soimoistdepb_h)) { return @"0"; }
                return soimoistdepb_h; 
            }
            set { soimoistdepb_h = value; }
        }

        public int Soimoistdepb_diff {
            get { return  System.Convert.ToInt32(Soimoistdepb_h) - System.Convert.ToInt32(Soimoistdepb_l); }
        }

        private string soimoiststat;
        public string Soimoiststat {
            get { return soimoiststat; }
            set { soimoiststat = value; }
        }

        private string cosoilmoistkey;
        public string Cosoilmoistkey {
            get { return cosoilmoistkey; }
            set { cosoilmoistkey = value; }
        }
    }
}
