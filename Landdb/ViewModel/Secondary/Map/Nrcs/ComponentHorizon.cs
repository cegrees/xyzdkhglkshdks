﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using ThinkGeo.MapSuite.Core;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace Landdb.ViewModel.Secondary.Map.Nrcs {
    public class ComponentHorizon : INrcsFilter {

        public ComponentHorizon(string crop, GeoCollection<Feature> features) {
            StringBuilder sb = new StringBuilder(@"SELECT saversion, saverest, l.areasymbol, l.areaname, l.lkey, musym, muname, museq, mu.mukey, comppct_r, compname, localphase, slope_r, c.cokey, ch.chkey, ch.hzname, ch.desgndisc, ch.desgnmaster, ch.desgnmasterprime, ch.desgnvert, ch.hzdept_l, ch.hzdept_r, ch.hzdept_h, ch.hzdepb_l, ch.hzdepb_r, ch.hzdepb_h, ch.hzthk_l, ch.hzthk_r, ch.hzthk_h, ch.fraggt10_l, ch.fraggt10_r, ch.fraggt10_h, ch.frag3to10_l, ch.frag3to10_r, ch.frag3to10_h, ch.sieveno4_l, ch.sieveno4_r, ch.sieveno4_h, ch.sieveno10_l, ch.sieveno10_r, ch.sieveno10_h, ch.sieveno40_l, ch.sieveno40_r, ch.sieveno40_h, ch.sieveno200_l, ch.sieveno200_r, ch.sieveno200_h, ch.sandtotal_l, ch.sandtotal_r, ch.sandtotal_h, ch.sandvc_l, ch.sandvc_r, ch.sandvc_h, ch.sandco_l, ch.sandco_r, ch.sandco_h, ch.sandmed_l, ch.sandmed_r, ch.sandmed_h, ch.sandfine_l, ch.sandfine_r, ch.sandfine_h, ch.sandvf_l, ch.sandvf_r, ch.sandvf_h, ch.silttotal_l, ch.silttotal_r, ch.silttotal_h, ch.siltco_l, ch.siltco_r, ch.siltco_h, ch.siltfine_l, ch.siltfine_r, ch.siltfine_h, ch.claytotal_l, ch.claytotal_r, ch.claytotal_h, ch.claysizedcarb_l, ch.claysizedcarb_r, ch.claysizedcarb_h, ch.om_l, ch.om_r, ch.om_h, ch.dbtenthbar_l, ch.dbtenthbar_r, ch.dbtenthbar_h, ch.dbthirdbar_l, ch.dbthirdbar_r, ch.dbthirdbar_h, ch.dbfifteenbar_l, ch.dbfifteenbar_r, ch.dbfifteenbar_h, ch.dbovendry_l, ch.dbovendry_r, ch.dbovendry_h, ch.partdensity, ch.ksat_l, ch.ksat_r, ch.ksat_h, ch.awc_l, ch.awc_r, ch.awc_h, ch.wtenthbar_l, ch.wtenthbar_r, ch.wtenthbar_h, ch.wthirdbar_l, ch.wthirdbar_r, ch.wthirdbar_h, ch.wfifteenbar_l, ch.wfifteenbar_r, ch.wfifteenbar_h, ch.wsatiated_l, ch.wsatiated_r, ch.wsatiated_h, ch.lep_l, ch.lep_r, ch.lep_h, ch.ll_l, ch.ll_r, ch.ll_h, ch.pi_l, ch.pi_r, ch.pi_h, ch.aashind_l, ch.aashind_r, ch.aashind_h, ch.kwfact, ch.kffact, ch.caco3_l, ch.caco3_r, ch.caco3_h, ch.gypsum_l, ch.gypsum_r, ch.gypsum_h, ch.sar_l, ch.sar_r, ch.sar_h, ch.ec_l, ch.ec_r, ch.ec_h, ch.cec7_l, ch.cec7_r, ch.cec7_h, ch.ecec_l, ch.ecec_r, ch.ecec_h, ch.sumbases_l, ch.sumbases_r, ch.sumbases_h, ch.ph1to1h2o_l, ch.ph1to1h2o_r, ch.ph1to1h2o_h, ch.ph01mcacl2_l, ch.ph01mcacl2_r, ch.ph01mcacl2_h, ch.freeiron_l, ch.freeiron_r, ch.freeiron_h, ch.feoxalate_l, ch.feoxalate_r, ch.feoxalate_h, ch.extracid_l, ch.extracid_r, ch.extracid_h, ch.extral_l, ch.extral_r, ch.extral_h, ch.aloxalate_l, ch.aloxalate_r, ch.aloxalate_h, ch.pbray1_l, ch.pbray1_r, ch.pbray1_h, ch.poxalate_l, ch.poxalate_r, ch.poxalate_h, ch.ph2osoluble_l, ch.ph2osoluble_r, ch.ph2osoluble_h, ch.ptotal_l, ch.ptotal_r, ch.ptotal_h, ch.excavdifcl, ch.excavdifms FROM sacatalog sac INNER JOIN legend l ON l.areasymbol = sac.areasymbol INNER JOIN mapunit mu ON mu.lkey = l.lkey LEFT OUTER JOIN component c ON c.mukey = mu.mukey LEFT OUTER JOIN chorizon ch ON ch.cokey = c.cokey  WHERE (");
            bool notfirstrecord = false;
            foreach (Feature feature in features) {
                string musym = feature.ColumnValues["musym"];
                string mukey = feature.ColumnValues["mukey"];

                if (notfirstrecord) {
                    sb.Append(string.Format(" or (mu.mukey = '{0}' and mu.musym = '{1}') ", mukey, musym));
                }
                else {
                    sb.Append(string.Format("(mu.mukey = '{0}' and mu.musym = '{1}') ", mukey, musym));
                }
                notfirstrecord = true;
            }
            sb.Append(")");

            string expandedstring = sb.ToString();
            NrcsWebService webservice = new NrcsWebService();
            XDocument doc1 = webservice.CreateWebRequest(expandedstring);

            QueryTheData(doc1);
        }

        public ComponentHorizon(string crop, GeoCollection<Feature> features, string cokey) {
            StringBuilder sb = new StringBuilder(@"SELECT saversion, saverest, l.areasymbol, l.areaname, l.lkey, musym, muname, museq, mu.mukey, comppct_r, compname, localphase, slope_r, c.cokey, ch.chkey, ch.hzname, ch.desgndisc, ch.desgnmaster, ch.desgnmasterprime, ch.desgnvert, ch.hzdept_l, ch.hzdept_r, ch.hzdept_h, ch.hzdepb_l, ch.hzdepb_r, ch.hzdepb_h, ch.hzthk_l, ch.hzthk_r, ch.hzthk_h, ch.fraggt10_l, ch.fraggt10_r, ch.fraggt10_h, ch.frag3to10_l, ch.frag3to10_r, ch.frag3to10_h, ch.sieveno4_l, ch.sieveno4_r, ch.sieveno4_h, ch.sieveno10_l, ch.sieveno10_r, ch.sieveno10_h, ch.sieveno40_l, ch.sieveno40_r, ch.sieveno40_h, ch.sieveno200_l, ch.sieveno200_r, ch.sieveno200_h, ch.sandtotal_l, ch.sandtotal_r, ch.sandtotal_h, ch.sandvc_l, ch.sandvc_r, ch.sandvc_h, ch.sandco_l, ch.sandco_r, ch.sandco_h, ch.sandmed_l, ch.sandmed_r, ch.sandmed_h, ch.sandfine_l, ch.sandfine_r, ch.sandfine_h, ch.sandvf_l, ch.sandvf_r, ch.sandvf_h, ch.silttotal_l, ch.silttotal_r, ch.silttotal_h, ch.siltco_l, ch.siltco_r, ch.siltco_h, ch.siltfine_l, ch.siltfine_r, ch.siltfine_h, ch.claytotal_l, ch.claytotal_r, ch.claytotal_h, ch.claysizedcarb_l, ch.claysizedcarb_r, ch.claysizedcarb_h, ch.om_l, ch.om_r, ch.om_h, ch.dbtenthbar_l, ch.dbtenthbar_r, ch.dbtenthbar_h, ch.dbthirdbar_l, ch.dbthirdbar_r, ch.dbthirdbar_h, ch.dbfifteenbar_l, ch.dbfifteenbar_r, ch.dbfifteenbar_h, ch.dbovendry_l, ch.dbovendry_r, ch.dbovendry_h, ch.partdensity, ch.ksat_l, ch.ksat_r, ch.ksat_h, ch.awc_l, ch.awc_r, ch.awc_h, ch.wtenthbar_l, ch.wtenthbar_r, ch.wtenthbar_h, ch.wthirdbar_l, ch.wthirdbar_r, ch.wthirdbar_h, ch.wfifteenbar_l, ch.wfifteenbar_r, ch.wfifteenbar_h, ch.wsatiated_l, ch.wsatiated_r, ch.wsatiated_h, ch.lep_l, ch.lep_r, ch.lep_h, ch.ll_l, ch.ll_r, ch.ll_h, ch.pi_l, ch.pi_r, ch.pi_h, ch.aashind_l, ch.aashind_r, ch.aashind_h, ch.kwfact, ch.kffact, ch.caco3_l, ch.caco3_r, ch.caco3_h, ch.gypsum_l, ch.gypsum_r, ch.gypsum_h, ch.sar_l, ch.sar_r, ch.sar_h, ch.ec_l, ch.ec_r, ch.ec_h, ch.cec7_l, ch.cec7_r, ch.cec7_h, ch.ecec_l, ch.ecec_r, ch.ecec_h, ch.sumbases_l, ch.sumbases_r, ch.sumbases_h, ch.ph1to1h2o_l, ch.ph1to1h2o_r, ch.ph1to1h2o_h, ch.ph01mcacl2_l, ch.ph01mcacl2_r, ch.ph01mcacl2_h, ch.freeiron_l, ch.freeiron_r, ch.freeiron_h, ch.feoxalate_l, ch.feoxalate_r, ch.feoxalate_h, ch.extracid_l, ch.extracid_r, ch.extracid_h, ch.extral_l, ch.extral_r, ch.extral_h, ch.aloxalate_l, ch.aloxalate_r, ch.aloxalate_h, ch.pbray1_l, ch.pbray1_r, ch.pbray1_h, ch.poxalate_l, ch.poxalate_r, ch.poxalate_h, ch.ph2osoluble_l, ch.ph2osoluble_r, ch.ph2osoluble_h, ch.ptotal_l, ch.ptotal_r, ch.ptotal_h, ch.excavdifcl, ch.excavdifms FROM sacatalog sac INNER JOIN legend l ON l.areasymbol = sac.areasymbol INNER JOIN mapunit mu ON mu.lkey = l.lkey LEFT OUTER JOIN component c ON c.mukey = mu.mukey LEFT OUTER JOIN chorizon ch ON ch.cokey = c.cokey  WHERE (");
            bool notfirstrecord = false;
            foreach (Feature feature in features) {
                string musym = feature.ColumnValues["musym"];
                string mukey = feature.ColumnValues["mukey"];

                if (notfirstrecord) {
                    sb.Append(string.Format(" or (mu.mukey = '{0}' and mu.musym = '{1}' and ch.cokey = '{2}') ", mukey, musym, cokey));
                }
                else {
                    sb.Append(string.Format("(mu.mukey = '{0}' and mu.musym = '{1}' and ch.cokey = '{2}') ", mukey, musym, cokey));
                }
                notfirstrecord = true;
            }
            sb.Append(")");

            string expandedstring = sb.ToString();
            NrcsWebService webservice = new NrcsWebService();
            XDocument doc1 = webservice.CreateWebRequest(expandedstring);

            QueryTheData(doc1);
        }

        public string Name {
            get { return "Component Horizon"; }
        }

        private void QueryTheData(XDocument doc) {
            ObservableCollection<CHORIZON> cropList = new ObservableCollection<CHORIZON>();
            try {

                var results1 = from st in doc.Descendants()
                               select st;

                //where st.Name.LocalName == "NewDataSet"
                foreach (var r in results1) {
                    if (r.Name.LocalName == "NewDataSet") {
                        var results2 = from ele in r.Descendants()
                                       select ele;

                        foreach (var tab in results2) {
                            if (tab.Name.LocalName == "Table") {
                                
                                var results3 = from ele in tab.Elements()
                                               select ele;

                                List<XElement> results33 = results3.ToList();
                                
                                CHORIZON cropyld = new CHORIZON(results33[0].Value, results33[1].Value, results33[2].Value, results33[3].Value, results33[4].Value, results33[5].Value, results33[6].Value, results33[7].Value, results33[8].Value, results33[9].Value, 
                                    results33[10].Value, results33[11].Value, results33[12].Value, results33[13].Value, results33[14].Value, results33[15].Value, results33[16].Value, results33[17].Value, results33[18].Value, results33[19].Value,
                                    results33[20].Value, results33[21].Value, results33[22].Value, results33[23].Value, results33[24].Value, results33[25].Value, results33[26].Value, results33[27].Value, results33[28].Value, results33[29].Value,
                                    results33[30].Value, results33[31].Value, results33[32].Value, results33[33].Value, results33[34].Value, results33[35].Value, results33[36].Value, results33[37].Value, results33[38].Value, results33[39].Value,
                                    results33[40].Value, results33[41].Value, results33[42].Value, results33[43].Value, results33[44].Value, results33[45].Value, results33[46].Value, results33[47].Value, results33[48].Value, results33[49].Value,
                                    results33[50].Value, results33[51].Value, results33[52].Value, results33[53].Value, results33[54].Value, results33[55].Value, results33[56].Value, results33[57].Value, results33[58].Value, results33[59].Value,
                                    results33[60].Value, results33[61].Value, results33[62].Value, results33[63].Value, results33[64].Value, results33[65].Value, results33[66].Value, results33[67].Value, results33[68].Value, results33[69].Value,
                                    results33[70].Value, results33[71].Value, results33[72].Value, results33[73].Value, results33[74].Value, results33[75].Value, results33[76].Value, results33[77].Value, results33[78].Value, results33[79].Value,
                                    results33[80].Value, results33[81].Value, results33[82].Value, results33[83].Value, results33[84].Value, results33[85].Value, results33[86].Value, results33[87].Value, results33[88].Value, results33[89].Value,
                                    results33[90].Value, results33[91].Value, results33[92].Value, results33[93].Value, results33[94].Value, results33[95].Value, results33[96].Value, results33[97].Value, results33[98].Value, results33[99].Value,
                                    results33[100].Value, results33[101].Value, results33[102].Value, results33[103].Value, results33[104].Value, results33[105].Value, results33[106].Value, results33[107].Value, results33[108].Value, results33[109].Value,
                                    results33[110].Value, results33[111].Value, results33[112].Value, results33[113].Value, results33[114].Value, results33[115].Value, results33[116].Value, results33[117].Value, results33[118].Value, results33[119].Value,
                                    results33[120].Value, results33[121].Value, results33[122].Value, results33[123].Value, results33[124].Value, results33[125].Value, results33[126].Value, results33[127].Value, results33[128].Value, results33[129].Value,
                                    results33[130].Value, results33[131].Value, results33[132].Value, results33[133].Value, results33[134].Value, results33[135].Value, results33[136].Value, results33[137].Value, results33[138].Value, results33[139].Value,
                                    results33[140].Value, results33[141].Value, results33[142].Value, results33[143].Value, results33[144].Value, results33[145].Value, results33[146].Value, results33[147].Value, results33[148].Value, results33[149].Value,
                                    results33[150].Value, results33[151].Value, results33[152].Value, results33[153].Value, results33[154].Value, results33[155].Value, results33[156].Value, results33[157].Value, results33[158].Value, results33[159].Value,
                                    results33[160].Value, results33[161].Value, results33[162].Value, results33[163].Value, results33[164].Value, results33[165].Value, results33[166].Value, results33[167].Value, results33[168].Value, results33[169].Value,
                                    results33[170].Value, results33[171].Value, results33[172].Value, results33[173].Value, results33[174].Value, results33[175].Value, results33[176].Value, results33[177].Value, results33[178].Value, results33[179].Value,
                                    results33[180].Value, results33[181].Value, results33[182].Value, results33[183].Value);

                                cropList.Add(cropyld);
                            }
                        }
                        break;
                    }
                }
            }
            catch (Exception ex1) {
                Console.WriteLine("Exception of type " + ex1.GetType().Name);
                throw;
            }

            DataList = new ObservableCollection<CHORIZON>(cropList.OrderBy(x => x.Hzdept_r_int).ThenBy(x => x.Hzdept_h).ThenBy(x => x.Hzdept_l));
            //DataList = cropList;
        }

        private ObservableCollection<CHORIZON> dataList = new ObservableCollection<CHORIZON>();
        public ObservableCollection<CHORIZON> DataList {
            get { return dataList; }
            set { dataList = value; }
        }
    }

    public class CHORIZON : ViewModelBase {

        public CHORIZON() {
        }

        public CHORIZON(string saversion, string saverest, string areasymbol, string areaname, string lkey, string musym, string muname, string museq, string mukey, string comppct_r, string compname, string localphase, string slope_r, string cokey, string chkey,
            string hzname, string desgndisc, string desgnmaster, string desgnmasterprime, string desgnvert, string hzdept_l, string hzdept_r, string hzdept_h,
            string hzdepb_l, string hzdepb_r, string hzdepb_h, string hzthk_l, string hzthk_r, string hzthk_h, string fraggt10_l, string fraggt10_r,
            string fraggt10_h, string frag3to10_l, string frag3to10_r, string frag3to10_h, string sieveno4_l, string sieveno4_r, string sieveno4_h, string sieveno10_l,
            string sieveno10_r, string sieveno10_h, string sieveno40_l, string sieveno40_r, string sieveno40_h, string sieveno200_l, string sieveno200_r, string sieveno200_h,
            string sandtotal_l, string sandtotal_r, string sandtotal_h, string sandvc_l, string sandvc_r, string sandvc_h, string sandco_l,
            string sandco_r, string sandco_h, string sandmed_l, string sandmed_r, string sandmed_h, string sandfine_l, string sandfine_r, string sandfine_h,
            string sandvf_l, string sandvf_r, string sandvf_h, string silttotal_l, string silttotal_r, string silttotal_h, string siltco_l, string siltco_r,
            string siltco_h, string siltfine_l, string siltfine_r, string siltfine_h, string claytotal_l, string claytotal_r, string claytotal_h,
            string claysizedcarb_l, string claysizedcarb_r, string claysizedcarb_h, string om_l, string om_r, string om_h, string dbtenthbar_l,
            string dbtenthbar_r, string dbtenthbar_h, string dbthirdbar_l, string dbthirdbar_r, string dbthirdbar_h, string dbfifteenbar_l, string dbfifteenbar_r, string dbfifteenbar_h,
            string dbovendry_l, string dbovendry_r, string dbovendry_h, string partdensity, string ksat_l, string ksat_r, string ksat_h, string awc_l,
            string awc_r, string awc_h, string wtenthbar_l, string wtenthbar_r, string wtenthbar_h, string wthirdbar_l, string wthirdbar_r,
            string wthirdbar_h, string wfifteenbar_l, string wfifteenbar_r, string wfifteenbar_h, string wsatiated_l, string wsatiated_r, string wsatiated_h,
            string lep_l, string lep_r, string lep_h, string ll_l, string ll_r, string ll_h, string pi_l,
            string pi_r, string pi_h, string aashind_l, string aashind_r, string aashind_h, string kwfact, string kffact,
            string caco3_l, string caco3_r, string caco3_h, string gypsum_l, string gypsum_r, string gypsum_h, string sar_l,
            string sar_r, string sar_h, string ec_l, string ec_r, string ec_h, string cec7_l, string cec7_r,
            string cec7_h, string ecec_l, string ecec_r, string ecec_h, string sumbases_l, string sumbases_r, string sumbases_h,
            string ph1to1h2o_l, string ph1to1h2o_r, string ph1to1h2o_h, string ph01mcacl2_l, string ph01mcacl2_r, string ph01mcacl2_h, string freeiron_l,
            string freeiron_r, string freeiron_h, string feoxalate_l, string feoxalate_r, string feoxalate_h, string extracid_l, string extracid_r,
            string extracid_h, string extral_l, string extral_r, string extral_h, string aloxalate_l, string aloxalate_r, string aloxalate_h,
            string pbray1_l, string pbray1_r, string pbray1_h, string poxalate_l, string poxalate_r, string poxalate_h, string ph2osoluble_l,
            string ph2osoluble_r, string ph2osoluble_h, string ptotal_l, string ptotal_r, string ptotal_h, string excavdifcl, string excavdifms) {
            this.saversion = saversion;
            this.saverest = saverest;
            this.areasymbol = areasymbol;
            this.areaname = areaname;
            this.lkey = lkey;
            this.musym = musym;
            this.muname = muname;
            this.museq = museq;
            this.mukey = mukey;
            this.comppct_r = comppct_r;
            this.compname = compname;
            this.localphase = localphase;
            this.slope_r = slope_r;
            this.cokey = cokey;
            this.chkey = chkey;
            this.hzname = hzname;
            this.desgndisc = desgndisc;
            this.desgnmaster = desgnmaster;
            this.desgnmasterprime = desgnmasterprime;
            this.desgnvert = desgnvert;
            this.hzdept_l = hzdept_l;
            this.hzdept_r = hzdept_r;
            if (!string.IsNullOrEmpty(hzdept_r)) {
                this.hzdept_r_int = System.Convert.ToInt32(hzdept_r);
            }
            this.hzdept_h = hzdept_h;
            this.hzdepb_l = hzdepb_l;
            this.hzdepb_r = hzdepb_r;
            this.hzdepb_h = hzdepb_h;
            this.hzthk_l = hzthk_l;
            this.hzthk_r = hzthk_r;
            if (!string.IsNullOrEmpty(hzthk_r)) {
                this.hzthk_r_int = System.Convert.ToInt32(hzthk_r);
            }
            if (this.hzthk_r_int == 0) {
                int hzdept_r1 = System.Convert.ToInt32(hzdept_r);
                int hzdepb_r1 = System.Convert.ToInt32(hzdepb_r);
                this.hzthk_r_int = hzdepb_r1 - hzdept_r1;
            }
            this.hzthk_h = hzthk_h;
            this.fraggt10_l = fraggt10_l;
            this.fraggt10_r = fraggt10_r;
            this.fraggt10_h = fraggt10_h;
            this.frag3to10_l = frag3to10_l;
            this.frag3to10_r = frag3to10_r;
            this.frag3to10_h = frag3to10_h;
            this.sieveno4_l = sieveno4_l;
            this.sieveno4_r = sieveno4_r;
            this.sieveno4_h = sieveno4_h;
            this.sieveno10_l = sieveno10_l;
            this.sieveno10_r = sieveno10_r;
            this.sieveno10_h = sieveno10_h;
            this.sieveno40_l = sieveno40_l;
            this.sieveno40_r = sieveno40_r;
            this.sieveno40_h = sieveno40_h;
            this.sieveno200_l = sieveno200_l;
            this.sieveno200_r = sieveno200_r;
            this.sieveno200_h = sieveno200_h;
            this.sandtotal_l = sandtotal_l;
            this.sandtotal_r = sandtotal_r;
            this.sandtotal_h = sandtotal_h;
            this.sandvc_l = sandvc_l;
            this.sandvc_r = sandvc_r;
            this.sandvc_h = sandvc_h;
            this.sandco_l = sandco_l;
            this.sandco_r = sandco_r;
            this.sandco_h = sandco_h;
            this.sandmed_l = sandmed_l;
            this.sandmed_r = sandmed_r;
            this.sandmed_h = sandmed_h;
            this.sandfine_l = sandfine_l;
            this.sandfine_r = sandfine_r;
            this.sandfine_h = sandfine_h;
            this.sandvf_l = sandvf_l;
            this.sandvf_r = sandvf_r;
            this.sandvf_h = sandvf_h;
            this.silttotal_l = silttotal_l;
            this.silttotal_r = silttotal_r;
            this.silttotal_h = silttotal_h;
            this.siltco_l = siltco_l;
            this.siltco_r = siltco_r;
            this.siltco_h = siltco_h;
            this.siltfine_l = siltfine_l;
            this.siltfine_r = siltfine_r;
            this.siltfine_h = siltfine_h;
            this.claytotal_l = claytotal_l;
            this.claytotal_r = claytotal_r;
            this.claytotal_h = claytotal_h;
            this.claysizedcarb_l = claysizedcarb_l;
            this.claysizedcarb_r = claysizedcarb_r;
            this.claysizedcarb_h = claysizedcarb_h;
            this.om_l = om_l;
            this.om_r = om_r;
            this.om_h = om_h;
            this.dbtenthbar_l = dbtenthbar_l;
            this.dbtenthbar_r = dbtenthbar_r;
            this.dbtenthbar_h = dbtenthbar_h;
            this.dbthirdbar_l = dbthirdbar_l;
            this.dbthirdbar_r = dbthirdbar_r;
            this.dbthirdbar_h = dbthirdbar_h;
            this.dbfifteenbar_l = dbfifteenbar_l;
            this.dbfifteenbar_r = dbfifteenbar_r;
            this.dbfifteenbar_h = dbfifteenbar_h;
            this.dbovendry_l = dbovendry_l;
            this.dbovendry_r = dbovendry_r;
            this.dbovendry_h = dbovendry_h;
            this.partdensity = partdensity;
            this.ksat_l = ksat_l;
            this.ksat_r = ksat_r;
            this.ksat_h = ksat_h;
            this.awc_l = awc_l;
            this.awc_r = awc_r;
            this.awc_h = awc_h;
            this.wtenthbar_l = wtenthbar_l;
            this.wtenthbar_r = wtenthbar_r;
            this.wtenthbar_h = wtenthbar_h;
            this.wthirdbar_l = wthirdbar_l;
            this.wthirdbar_r = wthirdbar_r;
            this.wthirdbar_h = wthirdbar_h;
            this.wfifteenbar_l = wfifteenbar_l;
            this.wfifteenbar_r = wfifteenbar_r;
            this.wfifteenbar_h = wfifteenbar_h;
            this.wsatiated_l = wsatiated_l;
            this.wsatiated_r = wsatiated_r;
            this.wsatiated_h = wsatiated_l;
            this.lep_l = lep_l;
            this.lep_r = lep_r;
            this.lep_h = lep_l;
            this.ll_l = ll_l;
            this.ll_r = ll_r;
            this.ll_h = ll_h;
            this.pi_l = pi_l;
            this.pi_r = pi_r;
            this.pi_h = pi_h;
            this.aashind_l = aashind_l;
            this.aashind_r = aashind_r;
            this.aashind_h = aashind_h;
            this.kwfact = kwfact;
            this.kffact = kffact;
            this.caco3_l = caco3_l;
            this.caco3_r = caco3_r;
            this.caco3_h = caco3_h;
            this.gypsum_l = gypsum_l;
            this.gypsum_r = gypsum_r;
            this.gypsum_h = gypsum_h;
            this.sar_l = sar_l;
            this.sar_r = sar_r;
            this.sar_h = sar_h;
            this.ec_l = ec_l;
            this.ec_r = ec_r;
            this.ec_h = ec_h;
            this.cec7_l = cec7_l;
            this.cec7_r = cec7_r;
            this.cec7_h = cec7_h;
            this.ecec_l = ecec_l;
            this.ecec_r = ecec_r;
            this.ecec_h = ecec_h;
            this.sumbases_l = sumbases_l;
            this.sumbases_r = sumbases_r;
            this.sumbases_h = sumbases_h;
            this.ph1to1h2o_l = ph1to1h2o_l;
            this.ph1to1h2o_r = ph1to1h2o_r;
            this.ph1to1h2o_h = ph1to1h2o_h;
            this.ph01mcacl2_l = ph01mcacl2_l;
            this.ph01mcacl2_r = ph01mcacl2_r;
            this.ph01mcacl2_h = ph01mcacl2_h;
            this.freeiron_l = freeiron_l;
            this.freeiron_r = freeiron_r;
            this.freeiron_h = freeiron_h;
            this.feoxalate_l = feoxalate_l;
            this.feoxalate_r = feoxalate_r;
            this.feoxalate_h = feoxalate_h;
            this.extracid_l = extracid_l;
            this.extracid_r = extracid_r;
            this.extracid_h = extracid_h;
            this.extral_l = extral_l;
            this.extral_r = extral_r;
            this.extral_h = extral_h;
            this.aloxalate_l = aloxalate_l;
            this.aloxalate_r = aloxalate_r;
            this.aloxalate_h = aloxalate_h;
            this.pbray1_l = pbray1_l;
            this.pbray1_r = pbray1_r;
            this.pbray1_h = pbray1_h;
            this.poxalate_l = poxalate_l;
            this.poxalate_r = poxalate_r;
            this.poxalate_h = poxalate_h;
            this.ph2osoluble_l = ph2osoluble_l;
            this.ph2osoluble_r = ph2osoluble_r;
            this.ph2osoluble_h = ph2osoluble_h;
            this.ptotal_l = ptotal_l;
            this.ptotal_r = ptotal_r;
            this.ptotal_h = ptotal_h;
            this.excavdifcl = excavdifcl;
            this.excavdifms = excavdifms;
        }

        private bool horizonexploded = false;
        public bool Horizonexploded {
            get { return horizonexploded; }
            set { horizonexploded = value; }
        }

        private string saversion;
        public string Saversion {
            get { return saversion; }
            set { saversion = value; }
        }

        private string saverest;
        public string Saverest {
            get { return saverest; }
            set { saverest = value; }
        }

        private string areasymbol;
        public string Areasymbol {
            get { return areasymbol; }
            set { areasymbol = value; }
        }

        private string areaname;
        public string Areaname {
            get { return areaname; }
            set { areaname = value; }
        }

        private string lkey;
        public string Ikey {
            get { return lkey; }
            set { lkey = value; }
        }

        private string musym;
        public string Musym {
            get { return musym; }
            set { musym = value; }
        }

        private string muname;
        public string Muname {
            get { return muname; }
            set { muname = value; }
        }

        private string museq;
        public string Museq {
            get { return museq; }
            set { museq = value; }
        }

        private string mukey;
        public string Mukey {
            get { return mukey; }
            set { mukey = value; }
        }

        private string comppct_r;
        public string Comppct_r {
            get { return comppct_r; }
            set { comppct_r = value; }
        }

        private string compname;
        public string Compname {
            get { return compname; }
            set { compname = value; }
        }

        private string localphase;
        public string Localphase {
            get { return localphase; }
            set { localphase = value; }
        }

        private string slope_r;
        public string Slope_r {
            get { return slope_r; }
            set { slope_r = value; }
        }

        private string cokey;
        public string Cokey {
            get { return cokey; }
            set { cokey = value; }
        }

        private string chkey;
        public string Chkey {
            get { return chkey; }
            set { chkey = value; }
        }

        private string hzname;
        public string Hzname {
            get { return hzname; }
            set { hzname = value; }
        }

        private string description;
        public string Description {
            get { return description; }
            set { description = value; }
        }

        private string desgndisc;
        public string Desgndisc {
            get { return desgndisc; }
            set { desgndisc = value; }
        }

        private string desgnmaster;
        public string Desgnmaster {
            get { return desgnmaster; }
            set { desgnmaster = value; }
        }

        private string desgnmasterprime;
        public string Desgnmasterprime {
            get { return desgnmasterprime; }
            set { desgnmasterprime = value; }
        }

        private string desgnvert;
        public string Desgnvert {
            get { return desgnvert; }
            set { desgnvert = value; }
        }

        private string hzdept_l;
        public string Hzdept_l {
            get { return hzdept_l; }
            set { hzdept_l = value; }
        }

        private string hzdept_r;
        public string Hzdept_r {
            get { return hzdept_r; }
            set { hzdept_r = value; }
        }

        private int hzdept_r_int;
        public int Hzdept_r_int {
            get { return hzdept_r_int; }
            set { hzdept_r_int = value; }
        }

        private string hzdept_h;
        public string Hzdept_h {
            get { return hzdept_h; }
            set { hzdept_h = value; }
        }

        private string hzdepb_l;
        public string Hzdepb_l {
            get { return hzdepb_l; }
            set { hzdepb_l = value; }
        }

        private string hzdepb_r;
        public string Hzdepb_r {
            get { return hzdepb_r; }
            set { hzdepb_r = value; }
        }

        private string hzdepb_h;
        public string Hzdepb_h {
            get { return hzdepb_h; }
            set { hzdepb_h = value; }
        }

        private string hzthk_l;
        public string Hzthk_l {
            get { return hzthk_l; }
            set { hzthk_l = value; }
        }

        private string hzthk_r;
        public string Hzthk_r {
            get { return hzthk_r; }
            set { hzthk_r = value; }
        }

        private int hzthk_r_int;
        public int Hzthk_r_int {
            get { return hzthk_r_int; }
            set { hzthk_r_int = value; }
        }

        private string hzthk_h;
        public string Hzthk_h {
            get { return hzthk_h; }
            set { hzthk_h = value; }
        }

        private string fraggt10_l;
        public string Fraggt10_l {
            get { return fraggt10_l; }
            set { fraggt10_l = value; }
        }

        private string fraggt10_r;
        public string Fraggt10_r {
            get { return fraggt10_r; }
            set { fraggt10_r = value; }
        }

        private string fraggt10_h;
        public string Fraggt10_h {
            get { return fraggt10_h; }
            set { fraggt10_h = value; }
        }

        private string frag3to10_l;
        public string Frag3to10_l {
            get { return frag3to10_l; }
            set { frag3to10_l = value; }
        }

        private string frag3to10_r;
        public string Frag3to10_r {
            get { return frag3to10_r; }
            set { frag3to10_r = value; }
        }

        private string frag3to10_h;
        public string Frag3to10_h {
            get { return frag3to10_h; }
            set { frag3to10_h = value; }
        }

        private string sieveno4_l;
        public string Sieveno4_l {
            get { return sieveno4_l; }
            set { sieveno4_l = value; }
        }

        private string sieveno4_r;
        public string Sieveno4_r {
            get { return sieveno4_r; }
            set { sieveno4_r = value; }
        }

        private string sieveno4_h;
        public string Sieveno4_h {
            get { return sieveno4_h; }
            set { sieveno4_h = value; }
        }

        private string sieveno10_l;
        public string Sieveno10_l {
            get { return sieveno10_l; }
            set { sieveno10_l = value; }
        }

        private string sieveno10_r;
        public string Sieveno10_r {
            get { return sieveno10_r; }
            set { sieveno10_r = value; }
        }

        private string sieveno10_h;
        public string Sieveno10_h {
            get { return sieveno10_h; }
            set { sieveno10_h = value; }
        }

        private string sieveno40_l;
        public string Sieveno40_l {
            get { return sieveno40_l; }
            set { sieveno40_l = value; }
        }

        private string sieveno40_r;
        public string Sieveno40_r {
            get { return sieveno40_r; }
            set { sieveno40_r = value; }
        }

        private string sieveno40_h;
        public string Sieveno40_h {
            get { return sieveno40_h; }
            set { sieveno40_h = value; }
        }

        private string sieveno200_l;
        public string Sieveno200_l {
            get { return sieveno200_l; }
            set { sieveno200_l = value; }
        }

        private string sieveno200_r;
        public string Sieveno200_r {
            get { return sieveno200_r; }
            set { sieveno200_r = value; }
        }

        private string sieveno200_h;
        public string Sieveno200_h {
            get { return sieveno200_h; }
            set { sieveno200_h = value; }
        }
 
        private string sandtotal_l;
        public string Sandtotal_l {
            get { return sandtotal_l; }
            set { sandtotal_l = value; }
        }

        private string sandtotal_r;
        public string Sandtotal_r {
            get { return sandtotal_r; }
            set { sandtotal_r = value; }
        }

        private string sandtotal_h;
        public string Sandtotal_h {
            get { return sandtotal_h; }
            set { sandtotal_h = value; }
        }

        private string sandvc_l;
        public string Sandvc_l {
            get { return sandvc_l; }
            set { sandvc_l = value; }
        }

        private string sandvc_r;
        public string Sandvc_r {
            get { return sandvc_r; }
            set { sandvc_r = value; }
        }

        private string sandvc_h;
        public string Sandvc_h {
            get { return sandvc_h; }
            set { sandvc_h = value; }
        }

        private string sandco_l;
        public string Sandco_l {
            get { return sandco_l; }
            set { sandco_l = value; }
        }

        private string sandco_r;
        public string Sandco_r {
            get { return sandco_r; }
            set { sandco_r = value; }
        }

        private string sandco_h;
        public string Sandco_h {
            get { return sandco_h; }
            set { sandco_h = value; }
        }

        private string sandmed_l;
        public string Sandmed_l {
            get { return sandmed_l; }
            set { sandmed_l = value; }
        }

        private string sandmed_r;
        public string Sandmed_r {
            get { return sandmed_r; }
            set { sandmed_r = value; }
        }

        private string sandmed_h;
        public string Sandmed_h {
            get { return sandmed_h; }
            set { sandmed_h = value; }
        }

        private string sandfine_l;
        public string Sandfine_l {
            get { return sandfine_l; }
            set { sandfine_l = value; }
        }

        private string sandfine_r;
        public string Sandfine_r {
            get { return sandfine_r; }
            set { sandfine_r = value; }
        }

        private string sandfine_h;
        public string Sandfine_h {
            get { return sandfine_h; }
            set { sandfine_h = value; }
        }

        private string sandvf_l;
        public string Sandvf_l {
            get { return sandvf_l; }
            set { sandvf_l = value; }
        }

        private string sandvf_r;
        public string Sandvf_r {
            get { return sandvf_r; }
            set { sandvf_r = value; }
        }

        private string sandvf_h;
        public string Sandvf_h {
            get { return sandvf_h; }
            set { sandvf_h = value; }
        }

        private string silttotal_l;
        public string Silttotal_l {
            get { return silttotal_l; }
            set { silttotal_l = value; }
        }

        private string silttotal_r;
        public string Silttotal_r {
            get { return silttotal_r; }
            set { silttotal_r = value; }
        }

        private string silttotal_h;
        public string Silttotal_h {
            get { return silttotal_h; }
            set { silttotal_h = value; }
        }

        private string siltco_l;
        public string Siltco_l {
            get { return siltco_l; }
            set { siltco_l = value; }
        }

        private string siltco_r;
        public string Siltco_r {
            get { return siltco_r; }
            set { siltco_r = value; }
        }

        private string siltco_h;
        public string Siltco_h {
            get { return siltco_h; }
            set { siltco_h = value; }
        }

        private string siltfine_l;
        public string Siltfine_l {
            get { return siltfine_l; }
            set { siltfine_l = value; }
        }

        private string siltfine_r;
        public string Siltfine_r {
            get { return siltfine_r; }
            set { siltfine_r = value; }
        }

        private string siltfine_h;
        public string Siltfine_h {
            get { return siltfine_h; }
            set { siltfine_h = value; }
        }

        private string claytotal_l;
        public string Claytotal_l {
            get { return claytotal_l; }
            set { claytotal_l = value; }
        }

        private string claytotal_r;
        public string Claytotal_r {
            get { return claytotal_r; }
            set { claytotal_r = value; }
        }

        private string claytotal_h;
        public string Claytotal_h {
            get { return claytotal_h; }
            set { claytotal_h = value; }
        }

        private string claysizedcarb_l;
        public string Claysizedcarb_l {
            get { return claysizedcarb_l; }
            set { claysizedcarb_l = value; }
        }

        private string claysizedcarb_r;
        public string Claysizedcarb_r {
            get { return claysizedcarb_r; }
            set { claysizedcarb_r = value; }
        }

        private string claysizedcarb_h;
        public string Claysizedcarb_h {
            get { return claysizedcarb_h; }
            set { claysizedcarb_h = value; }
        }

        private string om_l;
        public string Om_l {
            get { return om_l; }
            set { om_l = value; }
        }

        private string om_r;
        public string Om_r {
            get { return om_r; }
            set { om_r = value; }
        }

        private string om_h;
        public string Om_h {
            get { return om_h; }
            set { om_h = value; }
        }

        private string dbtenthbar_l;
        public string Dbtenthbar_l {
            get { return dbtenthbar_l; }
            set { dbtenthbar_l = value; }
        }

        private string dbtenthbar_r;
        public string Dbtenthbar_r {
            get { return dbtenthbar_r; }
            set { dbtenthbar_r = value; }
        }

        private string dbtenthbar_h;
        public string Dbtenthbar_h {
            get { return dbtenthbar_h; }
            set { dbtenthbar_h = value; }
        }

        private string dbthirdbar_l;
        public string Dbthirdbar_l {
            get { return dbthirdbar_l; }
            set { dbthirdbar_l = value; }
        }

        private string dbthirdbar_r;
        public string Dbthirdbar_r {
            get { return dbthirdbar_r; }
            set { dbthirdbar_r = value; }
        }

        private string dbthirdbar_h;
        public string Dbthirdbar_h {
            get { return dbthirdbar_h; }
            set { dbthirdbar_h = value; }
        }

        private string dbfifteenbar_l;
        public string Dbfifteenbar_l {
            get { return dbfifteenbar_l; }
            set { dbfifteenbar_l = value; }
        }

        private string dbfifteenbar_r;
        public string Dbfifteenbar_r {
            get { return dbfifteenbar_r; }
            set { dbfifteenbar_r = value; }
        }

        private string dbfifteenbar_h;
        public string Dbfifteenbar_h {
            get { return dbfifteenbar_h; }
            set { dbfifteenbar_h = value; }
        }

        private string dbovendry_l;
        public string Dbovendry_l {
            get { return dbovendry_l; }
            set { dbovendry_l = value; }
        }

        private string dbovendry_r;
        public string Dbovendry_r {
            get { return dbovendry_r; }
            set { dbovendry_r = value; }
        }

        private string dbovendry_h;
        public string Dbovendry_h {
            get { return dbovendry_h; }
            set { dbovendry_h = value; }
        }

        private string partdensity;
        public string Partdensity {
            get { return partdensity; }
            set { partdensity = value; }
        }

        private string ksat_l;
        public string Ksat_l {
            get { return ksat_l; }
            set { ksat_l = value; }
        }

        private string ksat_r;
        public string Ksat_r {
            get { return ksat_r; }
            set { ksat_r = value; }
        }

        private string ksat_h;
        public string Ksat_h {
            get { return ksat_h; }
            set { ksat_h = value; }
        }

        private string awc_l;
        public string Awc_l {
            get { return awc_l; }
            set { awc_l = value; }
        }

        private string awc_r;
        public string Awc_r {
            get { return awc_r; }
            set { awc_r = value; }
        }

        private string awc_h;
        public string Awc_h {
            get { return awc_h; }
            set { awc_h = value; }
        }

        private string wtenthbar_l;
        public string Wtenthbar_l {
            get { return wtenthbar_l; }
            set { wtenthbar_l = value; }
        }

        private string wtenthbar_r;
        public string Wtenthbar_r {
            get { return wtenthbar_r; }
            set { wtenthbar_r = value; }
        }

        private string wtenthbar_h;
        public string Wtenthbar_h {
            get { return wtenthbar_h; }
            set { wtenthbar_h = value; }
        }

        private string wthirdbar_l;
        public string Wthirdbar_l {
            get { return wthirdbar_l; }
            set { wthirdbar_l = value; }
        }

        private string wthirdbar_r;
        public string Wthirdbar_r {
            get { return wthirdbar_r; }
            set { wthirdbar_r = value; }
        }

        private string wthirdbar_h;
        public string Wthirdbar_h {
            get { return wthirdbar_h; }
            set { wthirdbar_h = value; }
        }

        private string wfifteenbar_l;
        public string Wfifteenbar_l {
            get { return wfifteenbar_l; }
            set { wfifteenbar_l = value; }
        }

        private string wfifteenbar_r;
        public string Wfifteenbar_r {
            get { return wfifteenbar_r; }
            set { wfifteenbar_r = value; }
        }

        private string wfifteenbar_h;
        public string Wfifteenbar_h {
            get { return wfifteenbar_h; }
            set { wfifteenbar_h = value; }
        }

        private string wsatiated_l;
        public string Wsatiated_l {
            get { return wsatiated_l; }
            set { wsatiated_l = value; }
        }

        private string wsatiated_r;
        public string Wsatiated_r {
            get { return wsatiated_r; }
            set { wsatiated_r = value; }
        }

        private string wsatiated_h;
        public string Wsatiated_h {
            get { return wsatiated_h; }
            set { wsatiated_h = value; }
        }

        private string lep_l;
        public string Lep_l {
            get { return lep_l; }
            set { lep_l = value; }
        }

        private string lep_r;
        public string Lep_r {
            get { return lep_r; }
            set { lep_r = value; }
        }

        private string lep_h;
        public string Lep_h {
            get { return lep_h; }
            set { lep_h = value; }
        }

        private string ll_l;
        public string Ll_l {
            get { return ll_l; }
            set { ll_l = value; }
        }

        private string ll_r;
        public string Ll_r {
            get { return ll_r; }
            set { ll_r = value; }
        }

        private string ll_h;
        public string Ll_h {
            get { return ll_h; }
            set { ll_h = value; }
        }

        private string pi_l;
        public string Pi_l {
            get { return pi_l; }
            set { pi_l = value; }
        }

        private string pi_r;
        public string Pi_r {
            get { return pi_r; }
            set { pi_r = value; }
        }

        private string pi_h;
        public string Pi_h {
            get { return pi_h; }
            set { pi_h = value; }
        }

        private string aashind_l;
        public string Aashind_l {
            get { return aashind_l; }
            set { aashind_l = value; }
        }

        private string aashind_r;
        public string Aashind_r {
            get { return aashind_r; }
            set { aashind_r = value; }
        }

        private string aashind_h;
        public string Aashind_h {
            get { return aashind_h; }
            set { aashind_h = value; }
        }

        private string kwfact;
        public string Kwfact {
            get { return kwfact; }
            set { kwfact = value; }
        }

        private string kffact;
        public string Kffact {
            get { return kffact; }
            set { kffact = value; }
        }

        private string caco3_l;
        public string Caco3_l {
            get { return caco3_l; }
            set { caco3_l = value; }
        }

        private string caco3_r;
        public string Caco3_r {
            get { return caco3_r; }
            set { caco3_r = value; }
        }

        private string caco3_h;
        public string Caco3_h {
            get { return caco3_h; }
            set { caco3_h = value; }
        }

        private string gypsum_l;
        public string Gypsum_l {
            get { return gypsum_l; }
            set { gypsum_l = value; }
        }

        private string gypsum_r;
        public string Gypsum_r {
            get { return gypsum_r; }
            set { gypsum_r = value; }
        }

        private string gypsum_h;
        public string Gypsum_h {
            get { return gypsum_h; }
            set { gypsum_h = value; }
        }

        private string sar_l;
        public string Sar_l {
            get { return sar_l; }
            set { sar_l = value; }
        }

        private string sar_r;
        public string Sar_r {
            get { return sar_r; }
            set { sar_r = value; }
        }

        private string sar_h;
        public string Sar_h {
            get { return sar_h; }
            set { sar_h = value; }
        }

        private string ec_l;
        public string Ec_l {
            get { return ec_l; }
            set { ec_l = value; }
        }

        private string ec_r;
        public string Ec_r {
            get { return ec_r; }
            set { ec_r = value; }
        }

        private string ec_h;
        public string Ec_h {
            get { return ec_h; }
            set { ec_h = value; }
        }

        private string cec7_l;
        public string Cec7_l {
            get { return cec7_l; }
            set { cec7_l = value; }
        }

        private string cec7_r;
        public string Cec7_r {
            get { return cec7_r; }
            set { cec7_r = value; }
        }

        private string cec7_h;
        public string Cec7_h {
            get { return cec7_h; }
            set { cec7_h = value; }
        }

        private string ecec_l;
        public string Ecec_l {
            get { return ecec_l; }
            set { ecec_l = value; }
        }

        private string ecec_r;
        public string Ecec_r {
            get { return ecec_r; }
            set { ecec_r = value; }
        }

        private string ecec_h;
        public string Ecec_h {
            get { return ecec_h; }
            set { ecec_h = value; }
        }

        private string sumbases_l;
        public string Sumbases_l {
            get { return sumbases_l; }
            set { sumbases_l = value; }
        }

        private string sumbases_r;
        public string Sumbases_r {
            get { return sumbases_r; }
            set { sumbases_r = value; }
        }

        private string sumbases_h;
        public string Sumbases_h {
            get { return sumbases_h; }
            set { sumbases_h = value; }
        }

        private string ph1to1h2o_l;
        public string Ph1to1h2o_l {
            get { return ph1to1h2o_l; }
            set { ph1to1h2o_l = value; }
        }

        private string ph1to1h2o_r;
        public string Ph1to1h2o_r {
            get { return ph1to1h2o_r; }
            set { ph1to1h2o_r = value; }
        }

        private string ph1to1h2o_h;
        public string Ph1to1h2o_h {
            get { return ph1to1h2o_h; }
            set { ph1to1h2o_h = value; }
        }

        private string ph01mcacl2_l;
        public string Ph01mcacl2_l {
            get { return ph01mcacl2_l; }
            set { ph01mcacl2_l = value; }
        }

        private string ph01mcacl2_r;
        public string Ph01mcacl2_r {
            get { return ph01mcacl2_r; }
            set { ph01mcacl2_r = value; }
        }

        private string ph01mcacl2_h;
        public string Ph01mcacl2_h {
            get { return ph01mcacl2_h; }
            set { ph01mcacl2_h = value; }
        }

        private string freeiron_l;
        public string Freeiron_l {
            get { return freeiron_l; }
            set { freeiron_l = value; }
        }

        private string freeiron_r;
        public string Freeiron_r {
            get { return freeiron_r; }
            set { freeiron_r = value; }
        }

        private string freeiron_h;
        public string Freeiron_h {
            get { return freeiron_h; }
            set { freeiron_h = value; }
        }

        private string feoxalate_l;
        public string Feoxalate_l {
            get { return feoxalate_l; }
            set { feoxalate_l = value; }
        }

        private string feoxalate_r;
        public string Feoxalate_r {
            get { return feoxalate_r; }
            set { feoxalate_r = value; }
        }

        private string feoxalate_h;
        public string Feoxalate_h {
            get { return feoxalate_h; }
            set { feoxalate_h = value; }
        }

        private string extracid_l;
        public string Extracid_l {
            get { return extracid_l; }
            set { extracid_l = value; }
        }

        private string extracid_r;
        public string Extracid_r {
            get { return extracid_r; }
            set { extracid_r = value; }
        }

        private string extracid_h;
        public string Extracid_h {
            get { return extracid_h; }
            set { extracid_h = value; }
        }

        private string extral_l;
        public string Extral_l {
            get { return extral_l; }
            set { extral_l = value; }
        }

        private string extral_r;
        public string Extral_r {
            get { return extral_r; }
            set { extral_r = value; }
        }

        private string extral_h;
        public string Extral_h {
            get { return extral_h; }
            set { extral_h = value; }
        }

        private string aloxalate_l;
        public string Aloxalate_l {
            get { return aloxalate_l; }
            set { aloxalate_l = value; }
        }

        private string aloxalate_r;
        public string Aloxalate_r {
            get { return aloxalate_r; }
            set { aloxalate_r = value; }
        }

        private string aloxalate_h;
        public string Aloxalate_h {
            get { return aloxalate_h; }
            set { aloxalate_h = value; }
        }

        private string pbray1_l;
        public string Pbray1_l {
            get { return pbray1_l; }
            set { pbray1_l = value; }
        }

        private string pbray1_r;
        public string Pbray1_r {
            get { return pbray1_r; }
            set { pbray1_r = value; }
        }

        private string pbray1_h;
        public string Pbray1_h {
            get { return pbray1_h; }
            set { pbray1_h = value; }
        }

        private string poxalate_l;
        public string Poxalate_l {
            get { return poxalate_l; }
            set { poxalate_l = value; }
        }

        private string poxalate_r;
        public string Poxalate_r {
            get { return poxalate_r; }
            set { poxalate_r = value; }
        }

        private string poxalate_h;
        public string Poxalate_h {
            get { return poxalate_h; }
            set { poxalate_h = value; }
        }

        private string ph2osoluble_l;
        public string Ph2osoluble_l {
            get { return ph2osoluble_l; }
            set { ph2osoluble_l = value; }
        }

        private string ph2osoluble_r;
        public string Ph2osoluble_r {
            get { return ph2osoluble_r; }
            set { ph2osoluble_r = value; }
        }

        private string ph2osoluble_h;
        public string Ph2osoluble_h {
            get { return ph2osoluble_h; }
            set { ph2osoluble_h = value; }
        }

        private string ptotal_l;
        public string Ptotal_l {
            get { return ptotal_l; }
            set { ptotal_l = value; }
        }

        private string ptotal_r;
        public string Ptotal_r {
            get { return ptotal_r; }
            set { ptotal_r = value; }
        }

        private string ptotal_h;
        public string Ptotal_h {
            get { return ptotal_h; }
            set { ptotal_h = value; }
        }

        private string excavdifcl;
        public string Excavdifcl {
            get { return excavdifcl; }
            set { excavdifcl = value; }
        }

        private string excavdifms;
        public string Excavdifms {
            get { return excavdifms; }
            set { excavdifms = value; }
        }
    }
}
