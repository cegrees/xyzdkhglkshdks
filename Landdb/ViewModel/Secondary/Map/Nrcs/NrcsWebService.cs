﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Web;
using System.Xml;
using System.Xml.Linq; // LINQ for XML
using System.Xml.XPath;
using System.Data; // XPath extensions


namespace Landdb.ViewModel.Secondary.Map.Nrcs {
    public class NrcsWebService {

        public NrcsWebService() {
        }

        public XDocument CreateWebRequest(string sqlstatements) {
            try {
                string pageName = "https://SDMDataAccess.nrcs.usda.gov/Tabular/SDMTabularService.asmx";
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(pageName);
                req.Method = "POST";
                req.ContentType = "application/soap+xml; charset=utf-8";
                //req.Headers.Add("SOAPAction", "\"http://SDMDataAccess.nrcs.usda.gov/Tabular/SDMTabularService.asmx/RunQuery\"");

                string xmlRequest = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                            "<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001//XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">" +
                                "<soap12:Body>" +
                                    "<RunQuery xmlns=\"http://SDMDataAccess.nrcs.usda.gov/Tabular/SDMTabularService.asmx\">" +
                                        $"<Query>{sqlstatements}</Query>" +
                                    "</RunQuery>" +
                                "</soap12:Body>" +
                            "</soap12:Envelope>";

                //string pageName = "http://SDMDataAccess.nrcs.usda.gov/Tabular/SDMTabularService.asmx";
                //req.Headers.Add("SOAPAction", "\"http://SDMDataAccess.nrcs.usda.gov/Tabular/SDMTabularService.asmx/RunQuery\"");
                //" <RunQuery xmlns=\"http://SDMDataAccess.nrcs.usda.gov/Tabular/SDMTabularService.asmx\">",

                //string pageName = "http://SDMDataAccess.cs.egov.usda.gov/Tabular/SDMTabularService.asmx";
                //req.Headers.Add("SOAPAction", "\"http://SDMDataAccess.cs.egov.usda.gov/Tabular/SDMTabularService.asmx/RunQuery\"");
                //" <RunQuery xmlns=\"http://SDMDataAccess.cs.egov.usda.gov/Tabular/SDMTabularService.asmx\">",


                byte[] reqBytes = new UTF8Encoding().GetBytes(xmlRequest);
                req.ContentLength = reqBytes.Length;
                try {
                    using (Stream reqStream = req.GetRequestStream()) {
                        reqStream.Write(reqBytes, 0, reqBytes.Length);
                    }
                }
                catch (Exception ex) {
                    Console.WriteLine("Exception of type " + ex.GetType().Name);
                    throw;
                }
                try {
                    XDocument doc2 = null;
                    using (HttpWebResponse response = req.GetResponse() as HttpWebResponse) {
                        using (var reader = new StreamReader(response.GetResponseStream())) {
                            var data = reader.ReadToEnd();
                            doc2 = XDocument.Load(new StringReader(data));
                        }
                    }
                    return doc2;
                }
                catch (Exception ex2) {
                    Console.WriteLine("Exception of type " + ex2.GetType().Name);
                    throw;
                }
            }
            catch (Exception ex1) {
                Console.WriteLine("Exception of type " + ex1.GetType().Name);
                throw;
            }
        }
    }
}
