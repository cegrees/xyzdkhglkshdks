﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using ThinkGeo.MapSuite.Core;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace Landdb.ViewModel.Secondary.Map.Nrcs {
    public class Component : INrcsFilter {

        public Component(string crop, GeoCollection<Feature> features) {
            StringBuilder sb = new StringBuilder(@"SELECT saversion, saverest, l.areasymbol, l.areaname, l.lkey, musym, muname, museq, mu.mukey, c.cokey, c.comppct_l, c.comppct_r, c.comppct_h, c.compname, c.compkind, c.majcompflag, c.otherph, c.localphase, c.slope_l, c.slope_r, c.slope_h, c.slopelenusle_l, c.slopelenusle_r, c.slopelenusle_h, c.runoff, c.tfact, c.wei, c.weg, c.erocl, c.earthcovkind1, c.earthcovkind2, c.hydricon, c.hydricrating, c.drainagecl, c.elev_l, c.elev_r, c.elev_h, c.aspectccwise, c.aspectrep, c.aspectcwise, c.geomdesc, c.albedodry_l, c.albedodry_r, c.albedodry_h, c.airtempa_l, c.airtempa_r, c.airtempa_h, c.map_l, c.map_r, c.map_h, c.reannualprecip_l, c.reannualprecip_r, c.reannualprecip_h, c.ffd_l, c.ffd_r, c.ffd_h, c.nirrcapcl, c.nirrcapscl, c.nirrcapunit, c.irrcapcl, c.irrcapscl, c.irrcapunit, c.cropprodindex, c.constreeshrubgrp, c.wndbrksuitgrp, c.rsprod_l, c.rsprod_r, c.rsprod_h, c.foragesuitgrpid, c.wlgrain, c.wlgrass, c.wlherbaceous, c.wlshrub, c.wlconiferous, c.wlhardwood, c.wlwetplant, c.wlshallowwat, c.wlrangeland, c.wlopenland, c.wlwoodland, c.wlwetland, c.soilslippot, c.frostact, c.initsub_l, c.initsub_r, c.initsub_h, c.totalsub_l, c.totalsub_r, c.totalsub_h, c.hydgrp, c.corcon, c.corsteel, c.taxclname, c.taxorder, c.taxsuborder, c.taxgrtgroup, c.taxsubgrp, c.taxpartsize, c.taxpartsizemod, c.taxceactcl, c.taxreaction, c.taxtempcl, c.taxmoistscl, c.taxtempregime, c.soiltaxedition, c.castorieindex, c.flecolcomnum, c.flhe, c.flphe, c.flsoilleachpot, c.flsoirunoffpot, c.fltemik2use, c.fltriumph2use, c.indraingrp, c.innitrateleachi, c.misoimgmtgrp, c.vasoimgtgrp FROM sacatalog sac INNER JOIN legend l ON l.areasymbol = sac.areasymbol INNER JOIN mapunit mu ON mu.lkey = l.lkey LEFT OUTER JOIN component c ON c.mukey = mu.mukey WHERE (");
            bool notfirstrecord = false;
            foreach (Feature feature in features) {
                string musym = feature.ColumnValues["musym"];
                string mukey = feature.ColumnValues["mukey"];

                if (notfirstrecord) {
                    sb.Append(string.Format(" or (mu.mukey = '{0}' and mu.musym = '{1}') ", mukey, musym));
                }
                else {
                    sb.Append(string.Format("(mu.mukey = '{0}' and mu.musym = '{1}') ", mukey, musym));
                }
                notfirstrecord = true;
            }
            sb.Append(")");

            string expandedstring = sb.ToString();
            NrcsWebService webservice = new NrcsWebService();
            XDocument doc1 = webservice.CreateWebRequest(expandedstring);

            QueryTheData(doc1);
        }

        public string Name {
            get { return "Component"; }
        }

        private void QueryTheData(XDocument doc) {
            ObservableCollection<MapUnitComponent> cropList = new ObservableCollection<MapUnitComponent>();
            try {

                var results1 = from st in doc.Descendants()
                               select st;

                //where st.Name.LocalName == "NewDataSet"
                foreach (var r in results1) {
                    if (r.Name.LocalName == "NewDataSet") {
                        var results2 = from ele in r.Descendants()
                                       select ele;

                        foreach (var tab in results2) {
                            if (tab.Name.LocalName == "Table") {
                                
                                var results3 = from ele in tab.Elements()
                                               select ele;

                                List<XElement> results33 = results3.ToList();

                                MapUnitComponent cropyld = new MapUnitComponent(results33[0].Value, results33[1].Value, results33[2].Value, results33[3].Value, results33[4].Value, results33[5].Value, results33[6].Value, results33[7].Value, results33[8].Value, results33[9].Value,
                                    results33[10].Value, results33[11].Value, results33[12].Value, results33[13].Value, results33[14].Value, results33[15].Value, results33[16].Value, results33[17].Value, results33[18].Value, results33[19].Value,
                                    results33[20].Value, results33[21].Value, results33[22].Value, results33[23].Value, results33[24].Value, results33[25].Value, results33[26].Value, results33[27].Value, results33[28].Value, results33[29].Value,
                                    results33[30].Value, results33[31].Value, results33[32].Value, results33[33].Value, results33[34].Value, results33[35].Value, results33[36].Value, results33[37].Value, results33[38].Value, results33[39].Value,
                                    results33[40].Value, results33[41].Value, results33[42].Value, results33[43].Value, results33[44].Value, results33[45].Value, results33[46].Value, results33[47].Value, results33[48].Value, results33[49].Value,
                                    results33[50].Value, results33[51].Value, results33[52].Value, results33[53].Value, results33[54].Value, results33[55].Value, results33[56].Value, results33[57].Value, results33[58].Value, results33[59].Value,
                                    results33[60].Value, results33[61].Value, results33[62].Value, results33[63].Value, results33[64].Value, results33[65].Value, results33[66].Value, results33[67].Value, results33[68].Value, results33[69].Value,
                                    results33[70].Value, results33[71].Value, results33[72].Value, results33[73].Value, results33[74].Value, results33[75].Value, results33[76].Value, results33[77].Value, results33[78].Value, results33[79].Value,
                                    results33[80].Value, results33[81].Value, results33[82].Value, results33[83].Value, results33[84].Value, results33[85].Value, results33[86].Value, results33[87].Value, results33[88].Value, results33[89].Value,
                                    results33[90].Value, results33[91].Value, results33[92].Value, results33[93].Value, results33[94].Value, results33[95].Value, results33[96].Value, results33[97].Value, results33[98].Value, results33[99].Value,
                                    results33[100].Value, results33[101].Value, results33[102].Value, results33[103].Value, results33[104].Value, results33[105].Value, results33[106].Value, results33[107].Value, results33[108].Value, results33[109].Value,
                                    results33[110].Value, results33[111].Value, results33[112].Value, results33[113].Value, results33[114].Value, results33[115].Value, results33[116].Value);

                                cropList.Add(cropyld);
                            }
                        }
                        break;
                    }
                }
            }
            catch (Exception ex1) {
                Console.WriteLine("Exception of type " + ex1.GetType().Name);
                throw;
            }

            DataList = new ObservableCollection<MapUnitComponent>(cropList.OrderByDescending(x => x.Comppct_r_int).ThenByDescending(x => x.Comppct_h).ThenByDescending(x => x.Comppct_l));
            //DataList = cropList;
        }

        private ObservableCollection<MapUnitComponent> dataList = new ObservableCollection<MapUnitComponent>();
        public ObservableCollection<MapUnitComponent> DataList {
            get { return dataList; }
            set { dataList = value; }
        }
    }

    public class MapUnitComponent : ViewModelBase {

        public MapUnitComponent() {
        }
        //c.fltriumph2use, c.indraingrp, c.innitrateleachi, c.misoimgmtgrp, c.vasoimgtgrp 

        public MapUnitComponent(string saversion, string saverest, string areasymbol, string areaname, string lkey, string musym, string muname, string museq, string mukey, string cokey, string comppct_l, string comppct_r, string comppct_h, string compname, string compkind, string majcompflag, string otherph, string localphase, string slope_l, string slope_r, string slope_h, string slopelenusle_l, string slopelenusle_r, string slopelenusle_h, string runoff, string tfact, string wei, string weg, string erocl, string earthcovkind1, string earthcovkind2, string hydricon, string hydricrating, string drainagecl, string elev_l, string elev_r, string elev_h, string aspectccwise, string aspectrep, string aspectcwise, string geomdesc, string albedodry_l, string albedodry_r, string albedodry_h, string airtempa_l, string airtempa_r, string airtempa_h, string map_l, string map_r, string map_h, string reannualprecip_l, string reannualprecip_r, string reannualprecip_h, string ffd_l, string ffd_r, string ffd_h, string nirrcapcl, string nirrcapscl, string nirrcapunit, string irrcapcl, string irrcapscl, string irrcapunit, string cropprodindex, string constreeshrubgrp, string wndbrksuitgrp, string rsprod_l, string rsprod_r, string rsprod_h, string foragesuitgrpid, string wlgrain, string wlgrass, string wlherbaceous, string wlshrub, string wlconiferous, string wlhardwood, string wlwetplant, string wlshallowwat, string wlrangeland, string wlopenland, string wlwoodland, string wlwetland, string soilslippot, string frostact, string initsub_l, string initsub_r, string initsub_h, string totalsub_l, string totalsub_r, string totalsub_h, string hydgrp, string corcon, string corsteel, string taxclname, string taxorder, string taxsuborder, string taxgrtgroup, string taxsubgrp, string taxpartsize, string taxpartsizemod, string taxceactcl, string taxreaction, string taxtempcl, string taxmoistscl, string taxtempregime, string soiltaxedition, string castorieindex, string flecolcomnum, string flhe, string flphe, string flsoilleachpot, string flsoirunoffpot, string fltemik2use, string fltriumph2use, string indraingrp, string innitrateleachi, string misoimgmtgrp, string vasoimgtgrp) {
            this.saversion = saversion;
            this.saverest = saverest;
            this.areasymbol = areasymbol;
            this.areaname = areaname;
            this.lkey = lkey;
            this.musym = musym;
            this.muname = muname;
            this.museq = museq;
            this.mukey = mukey;
            this.cokey = cokey;
            this.comppct_l = comppct_l;
            this.comppct_r = comppct_r;
            if (!string.IsNullOrEmpty(comppct_r)) {
                this.comppct_r_int = System.Convert.ToInt32(comppct_r);
            }
            this.comppct_h = comppct_h;
            this.compname = compname;
            this.compkind = compkind;
            this.majcompflag = majcompflag;
            this.otherph = otherph;
            this.localphase = localphase;
            this.slope_l = slope_l;
            this.slope_r = slope_r;
            this.slope_h = slope_h;
            this.slopelenusle_l = slopelenusle_l;
            this.slopelenusle_r = slopelenusle_r;
            this.slopelenusle_h = slopelenusle_h;
            this.runoff = runoff;
            this.tfact = tfact;
            this.wei = wei;
            this.weg = weg;
            this.erocl = erocl;
            this.earthcovkind1 = earthcovkind1;
            this.earthcovkind2 = earthcovkind2;
            this.hydricon = hydricon;
            this.hydricrating = hydricrating;
            this.drainagecl = drainagecl;
            this.elev_l = elev_l;
            this.elev_r = elev_r;
            this.elev_h = elev_h;
            this.aspectccwise = aspectccwise;
            this.aspectrep = aspectrep;
            this.aspectcwise = aspectcwise;
            this.geomdesc = geomdesc;
            this.albedodry_l = albedodry_l;
            this.albedodry_r = albedodry_r;
            this.albedodry_h = albedodry_h;
            this.airtempa_l = airtempa_l;
            this.airtempa_r = airtempa_r;
            this.airtempa_h = airtempa_h;
            this.map_l = map_l;
            this.map_r = map_r;
            this.map_h = map_h;
            this.reannualprecip_l = reannualprecip_l;
            this.reannualprecip_r = reannualprecip_r;
            this.reannualprecip_h = reannualprecip_h;
            this.ffd_l = ffd_l;
            this.ffd_r = ffd_r;
            this.ffd_h = ffd_h;
            this.nirrcapcl = nirrcapcl;
            this.nirrcapscl = nirrcapscl;
            this.nirrcapunit = nirrcapunit;
            this.irrcapcl = irrcapcl;
            this.irrcapscl = irrcapscl;
            this.irrcapunit = irrcapunit;
            this.cropprodindex = cropprodindex;
            this.constreeshrubgrp = constreeshrubgrp;
            this.wndbrksuitgrp = wndbrksuitgrp;
            this.rsprod_l = rsprod_l;
            this.rsprod_r = rsprod_r;
            this.rsprod_h = rsprod_h;
            this.foragesuitgrpid = foragesuitgrpid;
            this.wlgrain = wlgrain;
            this.wlgrass = wlgrass;
            this.wlherbaceous = wlherbaceous;
            this.wlshrub = wlshrub;
            this.wlconiferous = wlconiferous;
            this.wlhardwood = wlhardwood;
            this.wlwetplant = wlwetplant;
            this.wlshallowwat = wlshallowwat;
            this.wlrangeland = wlrangeland;
            this.wlopenland = wlopenland;
            this.wlwoodland = wlwoodland;
            this.wlwetland = wlwetland;
            this.soilslippot = soilslippot;
            this.frostact = frostact;
            this.initsub_l = initsub_l;
            this.initsub_r = initsub_r;
            this.initsub_h = initsub_h;
            this.totalsub_l = totalsub_l;
            this.totalsub_r = totalsub_r;
            this.totalsub_h = totalsub_h;
            this.hydgrp = hydgrp;
            this.corcon = corcon;
            this.corsteel = corsteel;
            this.taxclname = taxclname;
            this.taxorder = taxorder;
            this.taxsuborder = taxsuborder;
            this.taxgrtgroup = taxgrtgroup;
            this.taxsubgrp = taxsubgrp;
            this.taxpartsize = taxpartsize;
            this.taxpartsizemod = taxpartsizemod;
            this.taxceactcl = taxceactcl;
            this.taxreaction = taxreaction;
            this.taxtempcl = taxtempcl;
            this.taxmoistscl = taxmoistscl;
            this.taxtempregime = taxtempregime;
            this.soiltaxedition = soiltaxedition;
            this.castorieindex = castorieindex;
            this.flecolcomnum = flecolcomnum;
            this.flhe = flhe;
            this.flphe = flphe;
            this.flsoilleachpot = flsoilleachpot;
            this.flsoirunoffpot = flsoirunoffpot;
            this.fltemik2use = fltemik2use;
            this.fltriumph2use = fltriumph2use;
            this.indraingrp = indraingrp;
            this.innitrateleachi = innitrateleachi;
            this.misoimgmtgrp = misoimgmtgrp;
            this.vasoimgtgrp = vasoimgtgrp;
        }

        private string componentcolor = string.Empty;
        public string Componentcolor {
            get { return componentcolor; }
            set { componentcolor = value; }
        }

        private bool componentexploded = false;
        public bool Componentexploded {
            get { return componentexploded; }
            set { componentexploded = value; }
        }

        private string saversion;
        public string Saversion {
            get { return saversion; }
            set { saversion = value; }
        }

        private string saverest;
        public string Saverest {
            get { return saverest; }
            set { saverest = value; }
        }

        private string areasymbol;
        public string Areasymbol {
            get { return areasymbol; }
            set { areasymbol = value; }
        }

        private string areaname;
        public string Areaname {
            get { return areaname; }
            set { areaname = value; }
        }

        private string lkey;
        public string Ikey {
            get { return lkey; }
            set { lkey = value; }
        }

        private string musym;
        public string Musym {
            get { return musym; }
            set { musym = value; }
        }

        private string muname;
        public string Muname {
            get { return muname; }
            set { muname = value; }
        }

        private string museq;
        public string Museq {
            get { return museq; }
            set { museq = value; }
        }

        private string mukey;
        public string Mukey {
            get { return mukey; }
            set { mukey = value; }
        }

        private string cokey;
        public string Cokey {
            get { return cokey; }
            set { cokey = value; }
        }

        private string comppct_l;
        public string Comppct_l {
            get { return comppct_l; }
            set { comppct_l = value; }
        }

        private string comppct_r;
        public string Comppct_r {
            get { return comppct_r; }
            set { comppct_r = value; }
        }

        private int comppct_r_int;
        public int Comppct_r_int {
            get { return comppct_r_int; }
            set { comppct_r_int = value; }
        }

        private string comppct_h;
        public string Comppct_h {
            get { return comppct_h; }
            set { comppct_h = value; }
        }

        private string compname;
        public string Compname {
            get { return compname; }
            set { compname = value; }
        }

        private string compkind;
        public string Compkind {
            get { return compkind; }
            set { compkind = value; }
        }

        private string majcompflag;
        public string Majcompflag {
            get { return majcompflag; }
            set { majcompflag = value; }
        }

        private string otherph;
        public string Otherph {
            get { return otherph; }
            set { otherph = value; }
        }

        private string localphase;
        public string Localphase {
            get { return localphase; }
            set { localphase = value; }
        }

        private string slope_l;
        public string Slope_l {
            get { return slope_l; }
            set { slope_l = value; }
        }

        private string slope_r;
        public string Slope_r {
            get { return slope_r; }
            set { slope_r = value; }
        }

        private string slope_h;
        public string Slope_h {
            get { return slope_h; }
            set { slope_l = value; }
        }

        private string slopelenusle_l;
        public string Slopelenusle_l {
            get { return slopelenusle_l; }
            set { slopelenusle_l = value; }
        }

        private string slopelenusle_r;
        public string Slopelenusle_r {
            get { return slopelenusle_r; }
            set { slopelenusle_l = value; }
        }

        private string slopelenusle_h;
        public string Slopelenusle_h {
            get { return slopelenusle_h; }
            set { slopelenusle_h = value; }
        }

        private string runoff;
        public string Runoff {
            get { return runoff; }
            set { runoff = value; }
        }

        private string tfact;
        public string Tfact {
            get { return tfact; }
            set { tfact = value; }
        }

        private string wei;
        public string Wei {
            get { return wei; }
            set { wei = value; }
        }

        private string weg;
        public string Weg {
            get { return weg; }
            set { weg = value; }
        }

        private string erocl;
        public string Erocl {
            get { return erocl; }
            set { erocl = value; }
        }

        private string earthcovkind1;
        public string Earthcovkind1 {
            get { return earthcovkind1; }
            set { earthcovkind1 = value; }
        }

        private string earthcovkind2;
        public string Earthcovkind2 {
            get { return earthcovkind2; }
            set { earthcovkind2 = value; }
        }

        private string hydricon;
        public string Hydricon {
            get { return hydricon; }
            set { hydricon = value; }
        }

        private string hydricrating;
        public string Hydricrating {
            get { return hydricrating; }
            set { hydricrating = value; }
        }

        private string drainagecl;
        public string Drainagecl {
            get { return drainagecl; }
            set { drainagecl = value; }
        }

        private string elev_l;
        public string Elev_l {
            get { return elev_l; }
            set { elev_l = value; }
        }

        private string elev_r;
        public string Elev_r {
            get { return elev_r; }
            set { elev_r = value; }
        }

        private string elev_h;
        public string Elev_h {
            get { return elev_h; }
            set { elev_h = value; }
        }
        private string aspectccwise;
        public string Aspectccwise {
            get { return aspectccwise; }
            set { aspectccwise = value; }
        }

        private string aspectrep;
        public string Aspectrep {
            get { return aspectrep; }
            set { aspectrep = value; }
        }

        private string aspectcwise;
        public string Aspectcwise {
            get { return aspectcwise; }
            set { aspectcwise = value; }
        }

        private string geomdesc;
        public string Geomdesc {
            get { return geomdesc; }
            set { geomdesc = value; }
        }

        private string albedodry_l;
        public string Albedodry_l {
            get { return albedodry_l; }
            set { albedodry_l = value; }
        }

        private string albedodry_r;
        public string Albedodry_r {
            get { return albedodry_r; }
            set { albedodry_r = value; }
        }

        private string albedodry_h;
        public string Albedodry_h {
            get { return albedodry_h; }
            set { albedodry_h = value; }
        }

        private string airtempa_l;
        public string Airtempa_l {
            get { return airtempa_l; }
            set { airtempa_l = value; }
        }

        private string airtempa_r;
        public string Airtempa_r {
            get { return airtempa_r; }
            set { airtempa_r = value; }
        }

        private string airtempa_h;
        public string Airtempa_h {
            get { return airtempa_h; }
            set { airtempa_h = value; }
        }

        private string map_l;
        public string Map_l {
            get { return map_l; }
            set { map_l = value; }
        }

        private string map_r;
        public string Map_r {
            get { return map_r; }
            set { map_r = value; }
        }

        private string map_h;
        public string Map_h {
            get { return map_h; }
            set { map_h = value; }
        }

        private string reannualprecip_l;
        public string Reannualprecip_l {
            get { return reannualprecip_l; }
            set { reannualprecip_l = value; }
        }

        private string reannualprecip_r;
        public string Reannualprecip_r {
            get { return reannualprecip_r; }
            set { reannualprecip_r = value; }
        }

        private string reannualprecip_h;
        public string Reannualprecip_h {
            get { return reannualprecip_h; }
            set { reannualprecip_h = value; }
        }

        private string ffd_l;
        public string Ffd_l {
            get { return ffd_l; }
            set { ffd_l = value; }
        }

        private string ffd_r;
        public string Ffd_r {
            get { return ffd_r; }
            set { ffd_r = value; }
        }

        private string ffd_h;
        public string Ffd_h {
            get { return ffd_h; }
            set { ffd_h = value; }
        }

        private string nirrcapcl;
        public string Mirrcapcl {
            get { return nirrcapcl; }
            set { nirrcapcl = value; }
        }

        private string nirrcapscl;
        public string Mirrcapscl {
            get { return nirrcapscl; }
            set { nirrcapscl = value; }
        }

        private string nirrcapunit;
        public string Mirrcapunit {
            get { return nirrcapunit; }
            set { nirrcapunit = value; }
        }

        private string irrcapcl;
        public string Irrcapcl {
            get { return irrcapcl; }
            set { irrcapcl = value; }
        }

        private string irrcapscl;
        public string Irrcapscl {
            get { return irrcapscl; }
            set { irrcapscl = value; }
        }

        private string irrcapunit;
        public string Irrcapunit {
            get { return irrcapunit; }
            set { irrcapunit = value; }
        }

        private string cropprodindex;
        public string Cropprodindex {
            get { return cropprodindex; }
            set { cropprodindex = value; }
        }

        private string constreeshrubgrp;
        public string Constreeshrubgrp {
            get { return constreeshrubgrp; }
            set { constreeshrubgrp = value; }
        }

        private string wndbrksuitgrp;
        public string Wndbrksuitgrp {
            get { return wndbrksuitgrp; }
            set { wndbrksuitgrp = value; }
        }

        private string rsprod_l;
        public string Rsprod_l {
            get { return rsprod_l; }
            set { rsprod_l = value; }
        }

        private string rsprod_r;
        public string Rsprod_r {
            get { return rsprod_r; }
            set { rsprod_r = value; }
        }

        private string rsprod_h;
        public string Rsprod_h {
            get { return rsprod_h; }
            set { rsprod_h = value; }
        }

        private string foragesuitgrpid;
        public string Foragesuitgrpid {
            get { return foragesuitgrpid; }
            set { foragesuitgrpid = value; }
        }

        private string wlgrain;
        public string Wlgrain {
            get { return wlgrain; }
            set { wlgrain = value; }
        }

        private string wlgrass;
        public string Wlgrass {
            get { return wlgrass; }
            set { wlgrass = value; }
        }

        private string wlherbaceous;
        public string Wlherbaceous {
            get { return wlherbaceous; }
            set { wlherbaceous = value; }
        }

        private string wlshrub;
        public string Wlshrub {
            get { return wlshrub; }
            set { wlshrub = value; }
        }

        private string wlconiferous;
        public string Wlconiferous {
            get { return wlconiferous; }
            set { wlconiferous = value; }
        }

        private string wlhardwood;
        public string Wlhardwood {
            get { return wlhardwood; }
            set { wlhardwood = value; }
        }

        private string wlwetplant;
        public string Wlwetplant {
            get { return wlwetplant; }
            set { wlwetplant = value; }
        }

        private string wlshallowwat;
        public string Wlshallowwat {
            get { return wlshallowwat; }
            set { wlshallowwat = value; }
        }

        private string wlrangeland;
        public string Wlrangeland {
            get { return wlrangeland; }
            set { wlrangeland = value; }
        }

        private string wlopenland;
        public string Wlopenland {
            get { return wlopenland; }
            set { wlopenland = value; }
        }

        private string wlwoodland;
        public string Wlwoodland {
            get { return wlwoodland; }
            set { wlwoodland = value; }
        }

        private string wlwetland;
        public string Wlwetland {
            get { return wlwetland; }
            set { wlwetland = value; }
        }

        private string soilslippot;
        public string Soilslippot {
            get { return soilslippot; }
            set { soilslippot = value; }
        }

        private string frostact;
        public string Frostact {
            get { return frostact; }
            set { frostact = value; }
        }

        private string initsub_l;
        public string Initsub_l {
            get { return initsub_l; }
            set { initsub_l = value; }
        }

        private string initsub_r;
        public string Initsub_r {
            get { return initsub_r; }
            set { initsub_r = value; }
        }

        private string initsub_h;
        public string Initsub_h {
            get { return initsub_h; }
            set { initsub_h = value; }
        }

        private string totalsub_l;
        public string Totalsub_l {
            get { return totalsub_l; }
            set { totalsub_l = value; }
        }

        private string totalsub_r;
        public string Totalsub_r {
            get { return totalsub_r; }
            set { totalsub_r = value; }
        }

        private string totalsub_h;
        public string Totalsub_h {
            get { return totalsub_h; }
            set { totalsub_h = value; }
        }

        private string hydgrp;
        public string Hydgrp {
            get { return hydgrp; }
            set { hydgrp = value; }
        }

        private string corcon;
        public string Corcon {
            get { return corcon; }
            set { corcon = value; }
        }

        private string corsteel;
        public string Corsteel {
            get { return corsteel; }
            set { corsteel = value; }
        }

        private string taxclname;
        public string Taxclname {
            get { return taxclname; }
            set { taxclname = value; }
        }

        private string taxorder;
        public string Taxorder {
            get { return taxorder; }
            set { taxorder = value; }
        }

        private string taxsuborder;
        public string Taxsuborder {
            get { return taxsuborder; }
            set { taxsuborder = value; }
        }

        private string taxgrtgroup;
        public string Taxgrtgroup {
            get { return taxgrtgroup; }
            set { taxgrtgroup = value; }
        }

        private string taxsubgrp;
        public string Taxsubgrp {
            get { return taxsubgrp; }
            set { taxsubgrp = value; }
        }

        private string taxpartsize;
        public string Taxpartsize {
            get { return taxpartsize; }
            set { taxpartsize = value; }
        }

        private string taxpartsizemod;
        public string Taxpartsizemod {
            get { return taxpartsizemod; }
            set { taxpartsizemod = value; }
        }

        private string taxceactcl;
        public string Taxceactcl {
            get { return taxceactcl; }
            set { taxceactcl = value; }
        }

        private string taxreaction;
        public string Taxreaction {
            get { return taxreaction; }
            set { taxreaction = value; }
        }

        private string taxtempcl;
        public string Taxtempcl {
            get { return taxtempcl; }
            set { taxtempcl = value; }
        }

        private string taxmoistscl;
        public string Taxmoistscl {
            get { return taxmoistscl; }
            set { taxmoistscl = value; }
        }

        private string taxtempregime;
        public string Taxtempregime {
            get { return taxtempregime; }
            set { taxtempregime = value; }
        }

        private string soiltaxedition;
        public string Soiltaxedition {
            get { return soiltaxedition; }
            set { soiltaxedition = value; }
        }

        private string castorieindex;
        public string Castorieindex {
            get { return castorieindex; }
            set { castorieindex = value; }
        }

        private string flecolcomnum;
        public string Flecolcomnum {
            get { return flecolcomnum; }
            set { flecolcomnum = value; }
        }

        private string flhe;
        public string Flhe {
            get { return flhe; }
            set { flhe = value; }
        }

        private string flphe;
        public string Flphe {
            get { return flphe; }
            set { flphe = value; }
        }

        private string flsoilleachpot;
        public string Flsoilleachpot {
            get { return flsoilleachpot; }
            set { flsoilleachpot = value; }
        }

        private string flsoirunoffpot;
        public string Flsoirunoffpot {
            get { return flsoirunoffpot; }
            set { flsoirunoffpot = value; }
        }

        private string fltemik2use;
        public string Fltemik2use {
            get { return fltemik2use; }
            set { fltemik2use = value; }
        }

        private string fltriumph2use;
        public string Fltriumph2use {
            get { return fltriumph2use; }
            set { fltriumph2use = value; }
        }

        private string indraingrp;
        public string Indraingrp {
            get { return indraingrp; }
            set { indraingrp = value; }
        }

        private string innitrateleachi;
        public string Innitrateleachi {
            get { return innitrateleachi; }
            set { innitrateleachi = value; }
        }

        private string misoimgmtgrp;
        public string Misoimgmtgrp {
            get { return misoimgmtgrp; }
            set { misoimgmtgrp = value; }
        }

        private string vasoimgtgrp;
        public string Vasoimgtgrp {
            get { return vasoimgtgrp; }
            set { vasoimgtgrp = value; }
        }
    }
}
