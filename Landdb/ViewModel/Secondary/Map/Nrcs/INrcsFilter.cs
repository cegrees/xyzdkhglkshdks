﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using ThinkGeo.MapSuite.Core;

namespace Landdb.ViewModel.Secondary.Map.Nrcs {
    public interface INrcsFilter {
        string Name { get; }
    }
}
