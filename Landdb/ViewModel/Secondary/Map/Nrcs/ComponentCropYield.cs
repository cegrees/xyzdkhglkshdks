﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using ThinkGeo.MapSuite.Core;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace Landdb.ViewModel.Secondary.Map.Nrcs {
    public class ComponentCropYield : INrcsFilter {

        public ComponentCropYield(string crop, GeoCollection<Feature> features) {

            StringBuilder sb = new StringBuilder(@"SELECT saversion, saverest, l.areasymbol, l.areaname, l.lkey, musym, muname, museq, mu.mukey, comppct_r, compname, localphase, slope_r, c.cokey, cy.cropname, cy.yldunits, cy.nonirryield_l, cy.nonirryield_r, cy.nonirryield_h, cy.irryield_l, cy.irryield_r, cy.irryield_h, cy.cropprodindex, cy.vasoiprdgrp, cy.cocropyldkey FROM sacatalog sac INNER JOIN legend l ON l.areasymbol = sac.areasymbol INNER JOIN mapunit mu ON mu.lkey = l.lkey LEFT OUTER JOIN component c ON c.mukey = mu.mukey LEFT OUTER JOIN cocropyld cy ON cy.cokey = c.cokey WHERE (");
            bool notfirstrecord = false;
            foreach (Feature feature in features) {
                string musym = feature.ColumnValues["musym"];
                string mukey = feature.ColumnValues["mukey"];

                if (notfirstrecord) {
                    sb.Append(string.Format(" or (mu.mukey = '{0}' and mu.musym = '{1}') ", mukey, musym));
                }
                else {
                    sb.Append(string.Format("(mu.mukey = '{0}' and mu.musym = '{1}') ", mukey, musym));
                }
                notfirstrecord = true;
            }
            sb.Append(")");

            string expandedstring = sb.ToString();
            NrcsWebService webservice = new NrcsWebService();
            XDocument doc1 = webservice.CreateWebRequest(expandedstring);

            QueryTheData(doc1);
        }

        public ComponentCropYield(string crop, GeoCollection<Feature> features, string cokey) {

            StringBuilder sb = new StringBuilder(@"SELECT saversion, saverest, l.areasymbol, l.areaname, l.lkey, musym, muname, museq, mu.mukey, comppct_r, compname, localphase, slope_r, c.cokey, cy.cropname, cy.yldunits, cy.nonirryield_l, cy.nonirryield_r, cy.nonirryield_h, cy.irryield_l, cy.irryield_r, cy.irryield_h, cy.cropprodindex, cy.vasoiprdgrp, cy.cocropyldkey FROM sacatalog sac INNER JOIN legend l ON l.areasymbol = sac.areasymbol INNER JOIN mapunit mu ON mu.lkey = l.lkey LEFT OUTER JOIN component c ON c.mukey = mu.mukey LEFT OUTER JOIN cocropyld cy ON cy.cokey = c.cokey WHERE (");
            bool notfirstrecord = false;
            foreach (Feature feature in features) {
                string musym = feature.ColumnValues["musym"];
                string mukey = feature.ColumnValues["mukey"];

                if (notfirstrecord) {
                    sb.Append(string.Format(" or (mu.mukey = '{0}' and mu.musym = '{1}' and cy.cokey = '{2}') ", mukey, musym, cokey));
                }
                else {
                    sb.Append(string.Format("(mu.mukey = '{0}' and mu.musym = '{1}' and cy.cokey = '{2}') ", mukey, musym, cokey));
                }
                notfirstrecord = true;
            }
            sb.Append(")");

            string expandedstring = sb.ToString();
            NrcsWebService webservice = new NrcsWebService();
            XDocument doc1 = webservice.CreateWebRequest(expandedstring);

            QueryTheData(doc1);
        }

        public string Name {
            get { return "Crop Yield"; }
        }

        private void QueryTheData(XDocument doc) {
            ObservableCollection<COCROPYL> cropList = new ObservableCollection<COCROPYL>();
            try {

                var results1 = from st in doc.Descendants()
                               select st;

                //where st.Name.LocalName == "NewDataSet"
                foreach (var r in results1) {
                    if (r.Name.LocalName == "NewDataSet") {
                        var results2 = from ele in r.Descendants()
                                       select ele;

                        foreach (var tab in results2) {
                            if (tab.Name.LocalName == "Table") {
                                
                                var results3 = from ele in tab.Elements()
                                               select ele;

                                List<XElement> results33 = results3.ToList();

                                COCROPYL cropyld = new COCROPYL(results33[0].Value, results33[1].Value, results33[2].Value, results33[3].Value, results33[4].Value, results33[5].Value, 
                                    results33[6].Value, results33[7].Value, results33[8].Value, results33[9].Value, results33[10].Value, results33[11].Value, results33[12].Value, 
                                    results33[13].Value, results33[14].Value, results33[15].Value, results33[16].Value, results33[17].Value, results33[18].Value, results33[19].Value, 
                                    results33[20].Value, results33[21].Value, results33[22].Value, results33[23].Value, results33[24].Value);

                                cropList.Add(cropyld);
                            }
                        }
                        break;
                    }
                }
            }
            catch (Exception ex1) {
                Console.WriteLine("Exception of type " + ex1.GetType().Name);
                throw;
            }

            DataList = cropList;
        }

        private ObservableCollection<COCROPYL> dataList = new ObservableCollection<COCROPYL>();
        public ObservableCollection<COCROPYL> DataList {
            get { return dataList; }
            set { dataList = value; }
        }
    }

    public class COCROPYL : ViewModelBase {

        public COCROPYL() {
        }

        public COCROPYL(string saversion, string saverest, string areasymbol, string areaname, string lkey, string musym, string muname, string museq, string mukey, string comppct_r, string compname, string localphase, string slope_r, string cokey, string cropname, string yldunits, string nonirryield_l, string nonirryield_r, string nonirryield_h, string irryield_l, string irryield_r, string irryield_h, string cropprodindex, string vasoiprdgr, string cocropyldkey) {
            this.saversion = saversion;
            this.saverest = saverest;
            this.areasymbol = areasymbol;
            this.areaname = areaname;
            this.lkey = lkey;
            this.musym = musym;
            this.muname = muname;
            this.museq = museq;
            this.mukey = mukey;
            this.comppct_r = comppct_r;
            this.compname = compname;
            this.localphase = localphase;
            this.slope_r = slope_r;
            this.cokey = cokey;
            this.cropname = cropname;
            this.yldunits = yldunits;
            this.nonirryield_l = nonirryield_l;
            this.nonirryield_r = nonirryield_r;
            this.nonirryield_h = nonirryield_h;
            this.irryield_l = irryield_l;
            this.irryield_r = irryield_r;
            this.irryield_h = irryield_h;
            this.cropprodindex = cropprodindex;
            this.vasoiprdgr = vasoiprdgr;
            this.cocropyldkey = cocropyldkey;
        }

        private string saversion;
        public string Saversion {
            get { return saversion; }
            set { saversion = value; }
        }

        private string saverest;
        public string Saverest {
            get { return saverest; }
            set { saverest = value; }
        }

        private string areasymbol;
        public string Areasymbol {
            get { return areasymbol; }
            set { areasymbol = value; }
        }

        private string areaname;
        public string Areaname {
            get { return areaname; }
            set { areaname = value; }
        }

        private string lkey;
        public string Ikey {
            get { return lkey; }
            set { lkey = value; }
        }

        private string musym;
        public string Musym {
            get { return musym; }
            set { musym = value; }
        }

        private string muname;
        public string Muname {
            get { return muname; }
            set { muname = value; }
        }

        private string museq;
        public string Museq {
            get { return museq; }
            set { museq = value; }
        }

        private string mukey;
        public string Mukey {
            get { return mukey; }
            set { mukey = value; }
        }

        private string comppct_r;
        public string Comppct_r {
            get { return comppct_r; }
            set { comppct_r = value; }
        }

        private string compname;
        public string Compname {
            get { return compname; }
            set { compname = value; }
        }

        private string localphase;
        public string Localphase {
            get { return localphase; }
            set { localphase = value; }
        }

        private string slope_r;
        public string Slope_r {
            get { return slope_r; }
            set { slope_r = value; }
        }

        private string cokey;
        public string Cokey {
            get { return cokey; }
            set { cokey = value; }
        }

        private string cropname;
        public string Cropname {
            get { return cropname; }
            set { cropname = value; }
        }

        private string yldunits;
        public string Yldunits {
            get { return yldunits; }
            set { yldunits = value; }
        }

        private string nonirryield_l;
        public string Nonirryield_l {
            get { return nonirryield_l; }
            set { nonirryield_l = value; }
        }

        private string nonirryield_r;
        public string Nonirryield_r {
            get { return nonirryield_r; }
            set { nonirryield_r = value; }
        }

        private string nonirryield_h;
        public string Nonirryield_h {
            get { return nonirryield_h; }
            set { nonirryield_h = value; }
        }

        private string irryield_l;
        public string Irryield_l {
            get { return irryield_l; }
            set { irryield_l = value; }
        }

        private string irryield_r;
        public string Irryield_r {
            get { return irryield_r; }
            set { irryield_r = value; }
        }

        private string irryield_h;
        public string Irryield_h {
            get { return irryield_h; }
            set { irryield_h = value; }
        }

        private string cropprodindex;
        public string Cropprodindex {
            get { return cropprodindex; }
            set { cropprodindex = value; }
        }

        private string vasoiprdgr;
        public string Vasoiprdgr {
            get { return vasoiprdgr; }
            set { vasoiprdgr = value; }
        }

        private string cocropyldkey;
        public string Cocropyldkey {
            get { return cocropyldkey; }
            set { cocropyldkey = value; }
        }


    }
}
