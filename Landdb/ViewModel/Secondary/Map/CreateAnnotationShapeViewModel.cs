﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Shapes;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using AgC.UnitConversion;
using Landdb.Client.Spatial;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Secondary.Map;
using Landdb.Infrastructure;
using Landdb.Domain.ReadModels.MapAnnotation;
using Landdb.ViewModel.Fields;
using NLog;
using Landdb.ViewModel.Yield;
using System.Collections.ObjectModel;
using Landdb;
using Landdb.Resources;
using Landdb.ViewModel;

namespace Landdb.ViewModel.Secondary.Map {
    public class CreateAnnotationShapeViewModel : ViewModelBase {
        readonly IClientEndpoint clientEndpoint;

        Logger log = LogManager.GetCurrentClassLogger();
        AbstractTreeItemViewModel rootNode;
        AbstractTreeItemViewModel selectedParentNode;
        AbstractTreeItemViewModel selectedTreeItem;
        bool isAddItemPopupOpen;
        Action<AbstractTreeItemViewModel> onNodeCreated;
        Action onNodeIgnored;

        int? currentCropYear;
        Guid currentDataSourceId;

        public CreateAnnotationShapeViewModel(IClientEndpoint clientEndpoint, Action<AbstractTreeItemViewModel> onNodeCreated, Action onNodeIgnored) {
            this.clientEndpoint = clientEndpoint;
            this.onNodeCreated = onNodeCreated;
            this.onNodeIgnored = onNodeIgnored;

            commandlist = new List<IDomainCommand>();

            CreateNewItemCommand = new RelayCommand(CreateNewItem);
            CancelCreateCommand = new RelayCommand(CancelCreate);

            FilterModel = new TreeFilterModel(clientEndpoint);
        }

        public ICommand CreateNewItemCommand { get; private set; }
        public ICommand CancelCreateCommand { get; private set; }

        MapAnnotationId id;
        public MapAnnotationId Id {
            get { return id; }
            set {
                id = value;
            }
        }

        private IList<MapAnnotationCategory> stylenames = new List<MapAnnotationCategory>();
        public IList<MapAnnotationCategory> StyleNames {
            get { return stylenames; }
            set {
                //if (stylenames == value) { return; }
                stylenames = value;
                RaisePropertyChanged("StyleNames");
            }
        }

        private MapAnnotationCategory selectedstyle;
        public MapAnnotationCategory SelectedStyle {
            get { return selectedstyle; }
            set {
                if (selectedstyle == value) { return; }
                selectedstyle = value;
                RaisePropertyChanged("SelectedStyle");
                if (isNew && selectedstyle != null) {
                    NewItemName = String.Format(Strings.NewThing_Text, AnnotationsDisplayItem.GetAnnotationStyleDisplayTextFor(SelectedStyle.Name)); ;
                }
                else if (isNew && selectedstyle == null) {
                    log.Info("AnnotationTools - selected style is null");
                    NewItemName = Strings.NewNullStyle_Text;
                }
            }
        }
        public ObservableCollection<YieldLocationListItemViewModel> YieldLocationList { get; set; }

        public YieldLocationListItemViewModel InitialDestinationLocation { get; set; }

        private YieldLocationListItemViewModel _destinationLocation = null;
        public YieldLocationListItemViewModel SelectedDestinationLocation
        {
            get { return _destinationLocation; }
            set
            {
                _destinationLocation = value;

                //if (_destinationLocation != null) {
                //    IsSaleLoad = _destinationLocation.YieldLocationType == YieldLocationTypes.Sale;
                //    DestinationSubLocationInfo.SelectedTopLevelLocation = _destinationLocation;
                //}

                //parent.RefreshTotalsAndCounts();

                //ValidateAndRaisePropertyChanged(() => SelectedDestinationLocation);
                //RaisePropertyChanged(() => IsValid);
                RaisePropertyChanged("SelectedDestinationLocation");
            }
        }
        string newItemName;
        public string NewItemName {
            get { return newItemName; }
            set {
                newItemName = value;
                RaisePropertyChanged("NewItemName");
            }
        }

        bool applyCropYear = false;
        public bool ApplyCropYear {
            get { return applyCropYear; }
            set {
                if (applyCropYear == value) { return; }
                applyCropYear = value;
                if (applyCropYear) {
                    EnableCropYear = false;
                }
                RaisePropertyChanged("ApplyCropYear");
            }
        }

        bool enableCropYear = true;
        public bool EnableCropYear {
            get { return enableCropYear; }
            set {
                if (enableCropYear == value) { return; }
                enableCropYear = value;
                RaisePropertyChanged("EnableCropYear");
            }
        }

        bool useCoordinates = false;
        public bool UseCoordinates {
            get { return useCoordinates; }
            set {
                if (useCoordinates == value) { return; }
                useCoordinates = value;
                RaisePropertyChanged("UseCoordinates");
            }
        }

        bool isNew = true;
        public bool IsNew {
            get { return isNew; }
            set {
                isNew = value;
                RaisePropertyChanged("IsNew");
            }
        }

        bool isYieldLocation = false;
        public bool IsYieldLocation
        {
            get { return isYieldLocation; }
            set
            {
                isYieldLocation = value;
                RaisePropertyChanged("IsYieldLocation");
            }
        }

        public bool IsEditingMode
        {
            get { return !isNew; }
        }

        private IList<IDomainCommand> commandlist = new List<IDomainCommand>();
        public IList<IDomainCommand> CommandList {
            get { return commandlist; }
            set { commandlist = value; }
        }

        private IList<IDomainCommand> commandassociationlist = new List<IDomainCommand>();
        public IList<IDomainCommand> CommandAssociationList
        {
            get { return commandassociationlist; }
            set { commandassociationlist = value; }
        }

        public int? CurrentCropYear {
            get { return currentCropYear; }
            set {
                currentCropYear = value;
            }
        }
        public Guid CurrentDataSourceId {
            get { return currentDataSourceId; }
            set {
                currentDataSourceId = value;
            }
        }

        public IPageFilterViewModel FilterModel { get; protected set; }

        public AbstractTreeItemViewModel RootNode {
            get { return rootNode; }
            set {
                rootNode = value;
                RaisePropertyChanged("RootNode");
            }
        }

        public AbstractTreeItemViewModel SelectedParentNode {
            get { return selectedParentNode; }
            set {
                selectedParentNode = value;
                RaisePropertyChanged("SelectedParentNode");
            }
        }

        public AbstractTreeItemViewModel SelectedTreeItem {
            get { return selectedTreeItem; }
            set { selectedTreeItem = value; }
        }

        public bool IsAddItemPopupOpen {
            get { return isAddItemPopupOpen; }
            set {
                isAddItemPopupOpen = value;
                RaisePropertyChanged("IsAddItemPopupOpen");
            }
        }

        void CreateNewItem() {
            AbstractTreeItemViewModel newItem = null;
            string name = NewItemName;
            if (string.IsNullOrEmpty(name)) {
                name = selectedstyle.Name ;
            }
            log.Info("AnnotationTools - Create/update annotation item " + name + "  " + id.Id.ToString());
            this.currentDataSourceId = ApplicationEnvironment.CurrentDataSourceId;
            CurrentCropYear = null;
            if (applyCropYear) {
                this.currentCropYear = ApplicationEnvironment.CurrentCropYear;
            }
            if (isNew) {
                //id = new MapAnnotationId(this.currentDataSourceId, Guid.NewGuid());
                CreateMapAnnotation createMapAnnotationCommand = new CreateMapAnnotation(id, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), name, string.Empty, selectedstyle.Name, selectedstyle.ShapeType, this.CurrentCropYear, null);
                commandlist.Add(createMapAnnotationCommand);
            }
            else {
                RenameMapAnnotation renameMapAnnotationCommand = new RenameMapAnnotation(id, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), name);
                commandlist.Add(renameMapAnnotationCommand);
                RelocateMapAnnotation relocateMapAnnotationCommand = new RelocateMapAnnotation(id, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), string.Empty);
                commandlist.Add(relocateMapAnnotationCommand);
            }
            MapAnnotationListItem mapannotationlistitem = new MapAnnotationListItem() { Id = id, Name = name, AnnotationType = selectedstyle.Name, ShapeType = selectedstyle.ShapeType, CropYear = this.CurrentCropYear };
            List<MapAnnotationListItem> annotationlist = new List<MapAnnotationListItem>() { mapannotationlistitem };
            bool stylefound = false;
            foreach (AnnotationStyleTreeItemViewModel style in RootNode.Children) {
                if (selectedstyle.Name == style.Name) {
                    mapannotationlistitem.Name = name;
                    AnnotationShapeTreeItemViewModel annotationstyle = new AnnotationShapeTreeItemViewModel(mapannotationlistitem, style, true, filter: FilterModel.FilterItem);
                    newItem = annotationstyle;
                    selectedTreeItem = annotationstyle;
                    selectedParentNode = style;
                    stylefound = true;
                    break;
                }
            }
            if (!stylefound) {
                AnnotationStyleTreeItemViewModel annotationstyle = new AnnotationStyleTreeItemViewModel(selectedstyle, annotationlist, RootNode, true, filter: FilterModel.FilterItem);
                annotationstyle.IsExpanded = true;
                newItem = annotationstyle;
                selectedTreeItem = annotationstyle.Children.FirstOrDefault();
                selectedParentNode = annotationstyle;
            }
            if ((InitialDestinationLocation == null && SelectedDestinationLocation != null) || (InitialDestinationLocation != null && SelectedDestinationLocation != null && SelectedDestinationLocation.Id != InitialDestinationLocation.Id)) {
                AddRelatedBufferZone selectedyieldlocationcommand = new AddRelatedBufferZone(SelectedDestinationLocation.Id, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), id, selectedstyle.Name);
                commandlist.Add(selectedyieldlocationcommand);
                commandassociationlist.Add(selectedyieldlocationcommand);
                //clientEndpoint.SendOne(selectedyieldlocationcommand);

            }
            onNodeCreated(newItem); // notify the parent
            IsAddItemPopupOpen = false;
            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());

            // Clear item inputs
            NewItemName = string.Empty;
            CurrentCropYear = null;
        }

        void CancelCreate() {
            log.Info("AnnotationTools - Cancel create new item");
            IsAddItemPopupOpen = false;
            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());

            // Clear item inputs
            NewItemName = string.Empty;
            CurrentCropYear = null;
        }
    }
}
