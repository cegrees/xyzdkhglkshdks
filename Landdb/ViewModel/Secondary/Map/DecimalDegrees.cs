﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ThinkGeo.MapSuite.Core;

namespace Landdb.ViewModel.Secondary.Map
{
    static class DecimalDegrees
    {
        public enum Type { Longitude, Latitude };

        //From Decimal-degrees to Degrees Decimal-minutes.
        public static string d_To_Dm(double d, int precision,Type type)
        {
            string result;
            string hemisphere = GetHemisphere(d, type);
            d = Math.Abs(d);
            double D = Math.Truncate(d);
            double m = Math.Round(((d - D) * 60),precision);
            string zeroHolder = "";
            if (m < 10) { zeroHolder = "0"; }
            result = D + "° " + zeroHolder + m + "' " + hemisphere;
                
            return result;
        }

       //From Decimal-degrees to Degrees Minutes Decimal-seconds.
       public static string d_To_DMs(double d, int precision, Type type)
        {
            string result ;
            string hemisphere = GetHemisphere(d, type);
            d = Math.Abs(d);
            double D = Math.Truncate(d);
            double M = Math.Truncate((d - D)* 60);
            double s = Math.Round((d - D - (M / 60)) * 3600,precision);
            string minuteZeroHolder = "";
            string secondZeroHolder = "";
            if (M < 10) { minuteZeroHolder = "0"; }
            if (s < 10) { secondZeroHolder = "0"; }
            result = D + "° " + minuteZeroHolder + M + "' " + secondZeroHolder + s + "'' " + hemisphere;
           
            return result;
        }

        //From Decimal-minutes to Decimal-degrees.
        public static double Dm_To_d(double D, double m, int precision)
        {
            double d = Math.Abs(D) + (m / 60);
            if (D < 0) { d = -d; }
            d = Math.Round(d, precision);
            return d;
        }

        //From Degrees Minutes Decimal-seconds to Decimal-degrees.
        public static double DMs_To_d(double D, double M, double s, int precision)
        {
            double d = Math.Abs(D) + (Math.Abs(M) / 60) + (s / 3600);
            if (D < 0) { d = -d; }
            d = Math.Round(d, precision);
            return d;
        }

        //Gets the Hemisphere according to the positive (North, East) or negative value (South, West).
        private static string GetHemisphere(double d, Type type)
        {
            string result;
            if (type == Type.Longitude)
            {
                if (d > 0) { result = "E"; }
                else { result = "W"; }
            }
            else
            {
                if (d > 0) { result = "N"; }
                else { result = "S"; }
            }
            return result;
        }

        //From Decimal-degrees STRING to Degrees Minutes Decimal-seconds.
        public static string ddstring_To_DMs(string decimaldegreecoordinates, string coordinateformat) {
            if (coordinateformat == "Decimal Degree") { return decimaldegreecoordinates; }
            try {
                string coords = decimaldegreecoordinates;
                coords.Replace(" ", "");
                char[] delimiterChars = { ',' };
                string[] words = coords.Split(delimiterChars);
                if (words.Length == 2) {
                    double v1 = System.Convert.ToDouble(words[0]);
                    double v2 = System.Convert.ToDouble(words[1]);
                    Vertex geoVertex2 = new Vertex(v1, v2);
                    string _DegreesMinutesSecondsCoordinates = DecimalDegrees.d_To_DMs(geoVertex2.X, 2, DecimalDegrees.Type.Longitude) + "  " + DecimalDegrees.d_To_DMs(geoVertex2.Y, 2, DecimalDegrees.Type.Latitude);
                    return _DegreesMinutesSecondsCoordinates;
                }
                return decimaldegreecoordinates;
            }
            catch {
                return decimaldegreecoordinates;
            }
            return decimaldegreecoordinates;
        }
    }
}
