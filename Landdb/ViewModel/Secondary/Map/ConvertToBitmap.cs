﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GalaSoft.MvvmLight;
using ThinkGeo.MapSuite.Core;
using Landdb.Infrastructure;
using System.IO;
using Microsoft.Win32;
using GalaSoft.MvvmLight.Command;
using System.Windows.Input;
using System.Collections.ObjectModel;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure.Messages;
using Landdb.Domain.ReadModels.Tree;
using Landdb.MapStyles;
using Landdb.Client.Infrastructure;
using System.Windows.Threading;
using System.Drawing;
using Landdb.ViewModel.Maps.ImageryOverlay;
using ThinkGeo.MapSuite.WpfDesktopEdition;
using Landdb.Client.Spatial;
using System.Windows.Media;
using System.Threading.Tasks;
using AgC.UnitConversion;
using Landdb.ViewModel.Fields.FieldDetails.MapModels;
using Landdb.Domain.ReadModels.Map;
using Landdb.Domain.ReadModels.MapAnnotation;
using Landdb.ReportModels.Scouting;
using Landdb.ViewModel.Maps;

namespace Landdb.ViewModel.Secondary.Map {
    public class ConvertToBitmap : ViewModelBase {
        bool startedOutOnline = false;
        IClientEndpoint clientEndpoint;
        string maplayerimagery = "Bing";
        //IMapOverlay selectedoverlay;
        InMemoryFeatureLayer fieldsLayer = new InMemoryFeatureLayer();
        InMemoryFeatureLayer scoutingLayer = new InMemoryFeatureLayer();
        InMemoryFeatureLayer annotationLayer = new InMemoryFeatureLayer();
        InMemoryFeatureLayer bufferLayer = new InMemoryFeatureLayer();
        Proj4Projection projection;
        Feature selectedfield = new Feature();
        Feature loadedfeature = new Feature();
        BaseShape fieldshape = new MultipolygonShape();
        ClientLayerAreaStyle fieldsLayerAreaStyle = null;
        private Bitmap bitMap = null;
        int width;
        int height;
        bool includelabels = false;
        decimal zoneDistanceValue = 0;
        string zoneDistanceUnit = string.Empty;
        string zoneSeverity = string.Empty;

        ScalingTextStyle scalingTextStyle = new ScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, 24, 6);
        AnnotationScalingTextStyle scalingTextStyle2 = new AnnotationScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, 24, 2);
        MapStyles.AnnotationMultiStyle customMultiStyles = new MapStyles.AnnotationMultiStyle();
        string areaUnitString = string.Empty;
        AgC.UnitConversion.Area.AreaUnit agcAreaUnit;
        ThinkGeo.MapSuite.Core.AreaUnit mapAreaUnit;
        ThinkGeo.MapSuite.Core.DistanceUnit mapDistanceUnit;
        bool bingmapvisible = true;
        int fieldopacity = 125;
        int fieldopacityfill = 125;
        int annotationopacity = 125;
        int areadecimals = 2;
        int bordersize = 2;
        bool annotationvisibility = false;
        bool displayareainfieldlabel = false;
        bool displayareaonlylabel = false;
        bool mapannotationlabelsvisible = false;
        bool displayLabels = false;
        bool displayUnselectedFields = false;
        bool displayFillColor = false;
        bool labelallpolygonparts = false;
        bool allowlabeloverlapping = true;
        bool fittingpolygon = true;
        double fittingpolygonfactor = 2;

        MapSettings mapsettings;
        IStyleFactory styleFactory;
        string fieldsselectedfillcolor;
        string fieldsunselectedfillcolor;
        string fieldsselectedbordercolor;
        string fieldsunselectedbordercolor;
        string coordinateformat = string.Empty;
        GeoColor cropzoneGeoColor = GeoColor.StandardColors.Red;
        GeoColor fieldsselectedGeoColor = GeoColor.StandardColors.Yellow;
        GeoColor fieldsunselectedGeoColor = GeoColor.StandardColors.Gray;
        GeoColor fieldsselectedborderGeoColor = new GeoColor();
        GeoColor fieldsunselectedborderGeoColor = new GeoColor();
        List<AnnotationStyleItem> changedstyles = new List<AnnotationStyleItem>();
        Dictionary<string, MapAnnotationCategory> userstylenames = new Dictionary<string, MapAnnotationCategory>();

        private Dictionary<string, double> rotationAngles = new Dictionary<string, double>();
        private Dictionary<string, PointShape> labelPositions = new Dictionary<string, PointShape>();
        List<AnnotationStyleItem> listofstyles = new List<AnnotationStyleItem>();

        public double scale = 20.0;

        public ConvertToBitmap(string mapData, IClientEndpoint clientEndpoint)
        {
            this.clientEndpoint = clientEndpoint;
            this.includelabels = false;
            this.width = 400;
            this.height = 400;
            zoneDistanceValue = 0;
            zoneDistanceUnit = string.Empty;
            zoneSeverity = string.Empty;

            LoadMapSettings();
            InitializeMap();
            Candidates = new Collection<SimpleCandidate>();
            if (string.IsNullOrEmpty(mapData)) { return; }
            SelectedField = new Feature(mapData);

            var shape = SelectedField.GetShape();

            var simpleCandidate = new SimpleCandidate(mapData, SelectedField.GetShape() as PolygonShape);
            Candidates.Add(simpleCandidate);

            Fields = new List<Feature>();
            Buffers = new List<Feature>();
            DrawImage();
        }

        //public ConvertToBitmap(List<string> mapData, IClientEndpoint clientEndpoint) {
        //    var converter = new ConvertToBitmap(mapData, clientEndpoint, 0, "", "");
        //}

        public ConvertToBitmap(List<string> mapData, IClientEndpoint clientEndpoint, decimal EnvironmentalZoneDistanceValue, string EnvironmentalZoneDistanceUnit, string Severity)
        {
            this.clientEndpoint = clientEndpoint;
            this.includelabels = false;
            this.width = 400;
            this.height = 400;
            zoneDistanceValue = EnvironmentalZoneDistanceValue;
            zoneDistanceUnit = EnvironmentalZoneDistanceUnit;
            zoneSeverity = Severity;
            //zoneDistanceValue = 1320;
            //zoneDistanceUnit = "Feet";
            //zoneSeverity = "Spray Drift";
            //zoneSeverity = "Pollen Drift";
            //zoneSeverity = "BufferZoneLERAPB";
            //zoneSeverity = "Interim Aquatic";
            //zoneSeverity = "Spray Drift";
            //zoneSeverity = "Bee Protection";
            LoadMapSettings();
            InitializeMap();
            if (!string.IsNullOrEmpty(Severity)) {
                CreateBufferMemoryLayer(zoneSeverity);
            }
            Fields = new List<Feature>();
            Buffers = new List<Feature>();
            Candidates = new Collection<SimpleCandidate>();
            foreach (var data in mapData) {
                if (data != null && data != "MULTIPOLYGON EMPTY" && data != string.Empty) {
                    var feature = new Feature(data);
                    Fields.Add(feature);
                    var simpleCandidate = new SimpleCandidate(data, feature.GetShape() as PolygonShape);
                    Candidates.Add(simpleCandidate);
                    if (zoneDistanceValue > 0 && !string.IsNullOrEmpty(Severity)) {
                        MultipolygonShape mps1 = feature.GetShape().Buffer(0, GeographyUnit.DecimalDegree, mapDistanceUnit);
                        MultipolygonShape mps2 = feature.GetShape().Buffer((double)zoneDistanceValue, GeographyUnit.DecimalDegree, mapDistanceUnit);
                        MultipolygonShape mps3 = mps2.GetDifference(mps1);
                        if (mps3 != null && mps3.Polygons.Count > 0) {
                            var bufferfeature = new Feature(mps3);
                            Buffers.Add(bufferfeature);
                        }
                    }
                }
            }

            SelectedField = new Feature();
            OpenAndLoadaShapefile();
            DrawImage();
        }

        //public ConvertToBitmap(List<string> mapData, int width, int height, IClientEndpoint clientEndpoint) {
        //    var converter = new ConvertToBitmap(mapData, width, height, clientEndpoint, 0, "", "");
        //}

        public ConvertToBitmap(List<string> mapData, int width, int height, IClientEndpoint clientEndpoint, decimal EnvironmentalZoneDistanceValue, string EnvironmentalZoneDistanceUnit, string Severity) {
            this.clientEndpoint = clientEndpoint;
            this.includelabels = false;
            this.width = width;
            this.height = height;
            zoneDistanceValue = EnvironmentalZoneDistanceValue;
            zoneDistanceUnit = EnvironmentalZoneDistanceUnit;
            zoneSeverity = Severity;
            //zoneDistanceValue = 1320;
            //zoneDistanceUnit = "Feet";
            //zoneSeverity = "Spray Drift";
            //zoneSeverity = "Pollen Drift";
            //zoneSeverity = "BufferZoneLERAPB";
            //zoneSeverity = "Interim Aquatic";
            //zoneSeverity = "Spray Drift";
            //zoneSeverity = "Bee Protection";

            LoadMapSettings();
            InitializeMap();
            CreateBufferMemoryLayer(zoneSeverity);
            Fields = new List<Feature>();
            Buffers = new List<Feature>();
            Candidates = new Collection<SimpleCandidate>();
            foreach (var data in mapData)
            {
                if (data != null && data != "MULTIPOLYGON EMPTY" && data != string.Empty)
                {
                    var feature = new Feature(data);
                    Fields.Add(feature);
                    var simpleCandidate = new SimpleCandidate(data, feature.GetShape() as PolygonShape);
                    Candidates.Add(simpleCandidate);
                    if (zoneDistanceValue > 0) {
                        MultipolygonShape mps1 = feature.GetShape().Buffer(0, GeographyUnit.DecimalDegree, mapDistanceUnit);
                        MultipolygonShape mps2 = feature.GetShape().Buffer((double)zoneDistanceValue, GeographyUnit.DecimalDegree, mapDistanceUnit);
                        MultipolygonShape mps3 = mps2.GetDifference(mps1);
                        if (mps3 != null && mps3.Polygons.Count > 0) {
                            var bufferfeature = new Feature(mps3);
                            Buffers.Add(bufferfeature);
                        }
                    }
                }
            }

            SelectedField = new Feature();
            OpenAndLoadaShapefile();
            DrawImage();
        }

        public ConvertToBitmap(List<Feature> mapFeatures, bool includelabels, IClientEndpoint clientEndpoint, double scale, decimal? bufferWidth, string bufferUnit, string severity) {
            this.clientEndpoint = clientEndpoint;
            this.includelabels = includelabels;
            this.width = 400;
            this.height = 400;
            this.scale = scale;
            BuildMap(mapFeatures, this.width, this.height, this.includelabels, bufferWidth, bufferUnit, severity);
            DrawImage();
        }

        public ConvertToBitmap(List<Feature> mapFeatures, bool includelabels, IClientEndpoint clientEndpoint, double scale, List<Observation> scoutingobservations, double clusterradius) {
            this.clientEndpoint = clientEndpoint;
            this.includelabels = includelabels;
            this.width = 400;
            this.height = 400;
            this.scale = scale;
            BuildMap(mapFeatures, this.width, this.height, this.includelabels, null, string.Empty, string.Empty);
            OpenAndLoadScoutingObservations(scoutingobservations, clusterradius);
            DrawImage();
        }

        public ConvertToBitmap(List<Feature> mapFeatures, bool includelabels, IClientEndpoint clientEndpoint, int width, int height)
        {
            this.clientEndpoint = clientEndpoint;
            this.includelabels = includelabels;
            this.width = width;
            this.height = height;
            BuildMap(mapFeatures, this.width, this.height, this.includelabels, null, string.Empty, string.Empty);
            DrawImage();
        }

        public ConvertToBitmap(string mapData, int width, int height, IClientEndpoint clientEndpoint)
        {
            this.clientEndpoint = clientEndpoint;
            this.includelabels = false;
            this.width = width;
            this.height = height;
            zoneDistanceValue = 0;
            zoneDistanceUnit = string.Empty;
            zoneSeverity = string.Empty;

            LoadMapSettings();
            InitializeMap();
            Candidates = new Collection<SimpleCandidate>();
            //if (maps.Value.MostRecentMapItem == null || string.IsNullOrWhiteSpace(maps.Value.MostRecentMapItem.MapData) || maps.Value.MostRecentMapItem.MapData == "MULTIPOLYGON EMPTY") { continue; }
            //if (maps.Value.MostRecentMapItem != null && maps.Value.MostRecentMapItem.MapData != string.Empty && maps.Value.MostRecentMapItem.MapData != "MULTIPOLYGON EMPTY")
            //if (data != null && data != "MULTIPOLYGON EMPTY" && data != string.Empty)
            //if (maps.Value.MostRecentMapItem != null && maps.Value.MostRecentMapItem.MapData != "MULTIPOLYGON EMPTY")
            //if (string.IsNullOrWhiteSpace(mapData) || mapData == "MULTIPOLYGON EMPTY") { return; }
            if (string.IsNullOrWhiteSpace(mapData) || mapData == "MULTIPOLYGON EMPTY") { return; }
            SelectedField = new Feature(mapData);

            var shape = SelectedField.GetShape();

            var simpleCandidate = new SimpleCandidate(mapData, SelectedField.GetShape() as PolygonShape);
            Candidates.Add(simpleCandidate);

            Fields = new List<Feature>();
            Buffers = new List<Feature>();
            DrawImage();
        }

        public string CenterLatLong { get; set; }

        private void BuildMap(List<Feature> mapFeatures, int width, int height, bool includelabels, decimal? bufferWidth, string bufferUnit, string severity) {
            this.width = width;
            this.height = height;
            zoneDistanceValue = bufferWidth.HasValue ? bufferWidth.Value : 0;
            zoneDistanceUnit = bufferUnit;
            zoneSeverity = severity;

            rotationAngles = new Dictionary<string, double>();
            labelPositions = new Dictionary<string, PointShape>();

            LoadMapSettings();
            InitializeMap();
            CreateBufferMemoryLayer(zoneSeverity);
            Fields = new List<Feature>();
            Buffers = new List<Feature>();
            Candidates = new Collection<SimpleCandidate>();
            foreach (var feature in mapFeatures) {
                if (feature != null) {
                    Fields.Add(feature);
                    var simpleCandidate = new SimpleCandidate(feature.GetWellKnownText(), feature.GetShape() as PolygonShape);
                    Candidates.Add(simpleCandidate);

                    var labels = clientEndpoint.GetView<ItemMap>(new CropZoneId(ApplicationEnvironment.CurrentDataSourceId, new Guid(feature.Id)));

                    if (labels.HasValue) {
                        ItemLabelItem mrli = labels.Value.MostRecentLabelItem;
                        if (mrli != null && !mrli.OptionalVisibility) {
                            continue;
                        }
                        if (mrli != null && !string.IsNullOrWhiteSpace(mrli.SpatialLabelPosition)) {
                            PointShape ps = new PointShape(mrli.SpatialLabelPosition);
                            if (labelPositions.ContainsKey(feature.Id)) {
                                labelPositions[feature.Id] = ps;
                            }
                            else {
                                labelPositions.Add(feature.Id, ps);
                            }
                            if (rotationAngles.ContainsKey(feature.Id)) {
                                rotationAngles[feature.Id] = (double)mrli.HorizontalLabelRotation;
                            }
                            else {
                                rotationAngles.Add(feature.Id, (double)mrli.HorizontalLabelRotation);
                            }
                        }
                    }

                    if (zoneDistanceValue > 0)
                    {
                        MultipolygonShape mps1 = feature.GetShape().Buffer(0, GeographyUnit.DecimalDegree, mapDistanceUnit);
                        MultipolygonShape mps2 = feature.GetShape().Buffer((double)zoneDistanceValue, GeographyUnit.DecimalDegree, mapDistanceUnit);
                        MultipolygonShape mps3 = mps2.GetDifference(mps1);
                        if (mps3 != null && mps3.Polygons.Count > 0)
                        {
                            var bufferfeature = new Feature(mps3);
                            Buffers.Add(bufferfeature);
                        }
                    }
                }
            }
            scalingTextStyle.RotationAngles = rotationAngles;
            scalingTextStyle.LabelPositions = labelPositions;

            SelectedField = new Feature();
            OpenAndLoadaShapefile();

        }

        private void LoadMapSettings() {
            if (clientEndpoint == null) {
                clientEndpoint = ServiceLocator.Get<IClientEndpoint>();
            }
            this.mapsettings = clientEndpoint.GetMapSettings();
            maplayerimagery = mapsettings.MapInfoSelectorItem.DisplayText;
            var coordinateSystem = mapsettings.DisplayCoordinateFormat;
            bingmapvisible = mapsettings.BingMapVisible;
            fieldopacity = mapsettings.FieldsOpacity;
            fieldopacityfill = mapsettings.FieldsOpacity;
            annotationopacity = mapsettings.MapAnnotationOpacity;
            annotationvisibility = mapsettings.MapAnnotationsVisible;
            displayLabels = mapsettings.ScalingTextVisible;
            displayUnselectedFields = mapsettings.FieldsVisible;
            if (mapsettings.DisplayFillColor.HasValue) {
                displayFillColor = mapsettings.DisplayFillColor.Value;
                if (displayFillColor) {
                    fieldopacityfill = mapsettings.FieldsOpacity;
                }
                else {
                    fieldopacityfill = 0;
                }
            }
            if (mapsettings.DisplayAreaInFieldLabel.HasValue) {
                displayareainfieldlabel = mapsettings.DisplayAreaInFieldLabel.Value;
            }
            if (mapsettings.DisplayAreaOnlyLabel.HasValue) {
                displayareaonlylabel = mapsettings.DisplayAreaOnlyLabel.Value;
            }
            if (mapsettings.MapAnnotationLabelsVisible.HasValue) {
                mapannotationlabelsvisible = mapsettings.MapAnnotationLabelsVisible.Value;
            }

            styleFactory = new StyleFactory();
            changedstyles = new List<AnnotationStyleItem>();
            userstylenames = new Dictionary<string, MapAnnotationCategory>();

            bordersize = mapsettings.BorderSize;
            areadecimals = mapsettings.AreaDecimals;
            areaUnitString = mapsettings.MapAreaUnit;
            coordinateformat = mapsettings.DisplayCoordinateFormat;

            agcAreaUnit = UnitFactory.GetUnitByName(areaUnitString) as AgC.UnitConversion.Area.AreaUnit;
            mapAreaUnit = MapUnitFactory.AreaMeasureConversion(agcAreaUnit);
            mapDistanceUnit = MapUnitFactory.AreaDistanceMeasureConversion(agcAreaUnit);

            scalingTextStyle = new ScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, mapsettings.ScalingTextMaximumSize, this.mapsettings.ScalingTextMinimumSize);
            scalingTextStyle.WrapLabelText = true;
            //if (displayareainfieldlabel) {
            //    scalingTextStyle.SetTexTColumnName(@"AreaLabel");
            //}
            //else {
            //    scalingTextStyle.SetTexTColumnName(@"LdbLabel");
            //}
            if (displayareainfieldlabel) {
                if (displayareaonlylabel) {
                    scalingTextStyle.SetTexTColumnName(@"AreaOnlyLabel");
                }
                else {
                    scalingTextStyle.SetTexTColumnName(@"AreaLabel");
                }
            }
            else {
                scalingTextStyle.SetTexTColumnName(@"LdbLabel");
            }
            fittingpolygonfactor = mapsettings.FittingPolygonFactor;
            if (mapsettings.AllowLabelOverlapping.HasValue) {
                allowlabeloverlapping = mapsettings.AllowLabelOverlapping.Value;
            }
            if (mapsettings.FittingPolygon.HasValue) {
                fittingpolygon = mapsettings.FittingPolygon.Value;
            }
            scalingTextStyle.UnselectedFieldVisibility = displayUnselectedFields;
            scalingTextStyle.LabelAllPolygonParts = labelallpolygonparts;
            scalingTextStyle.PolygonLabelingLocationMode = PolygonLabelingLocationMode.BoundingBoxCenter;
            scalingTextStyle.FittingPolygon = fittingpolygon;
            scalingTextStyle.FittingPolygonFactor = fittingpolygonfactor;
            if (allowlabeloverlapping) {
                scalingTextStyle.OverlappingRule = LabelOverlappingRule.AllowOverlapping;
            }
            else {
                scalingTextStyle.OverlappingRule = LabelOverlappingRule.NoOverlapping;
            }

            fieldsselectedfillcolor = mapsettings.FieldsSelectedFillColor;
            fieldsunselectedfillcolor = mapsettings.FieldsUnSelectedFillColor;
            if (!string.IsNullOrEmpty(fieldsselectedfillcolor)) {
                fieldsselectedGeoColor = GeoColor.FromHtml(fieldsselectedfillcolor);
            }
            if (!string.IsNullOrEmpty(fieldsunselectedfillcolor)) {
                fieldsunselectedGeoColor = GeoColor.FromHtml(fieldsunselectedfillcolor);
            }

            fieldsselectedbordercolor = mapsettings.FieldsSelectedBorderColor;
            fieldsunselectedbordercolor = mapsettings.FieldsUnSelectedBorderColor;
            if (!string.IsNullOrEmpty(fieldsselectedbordercolor)) {
                fieldsselectedborderGeoColor = GeoColor.FromHtml(fieldsselectedbordercolor);
            }
            else {
                fieldsselectedborderGeoColor = fieldsselectedGeoColor;
            }
            if (!string.IsNullOrEmpty(fieldsunselectedbordercolor)) {
                fieldsunselectedborderGeoColor = GeoColor.FromHtml(fieldsunselectedbordercolor);
            }
            else {
                fieldsunselectedborderGeoColor = fieldsunselectedGeoColor;
            }

            //if (mapsettings.BingMapType == "Bing") {
            //    selectedoverlay = new BingMapOverlay();
            //}
            //else {
            //    selectedoverlay = new BingRoadMapsOverlay();
            //}

        }

        public MapEngine Map { get; set; }
        public Collection<SimpleCandidate> Candidates { get; set; }

        public Feature SelectedField {
            get { return selectedfield; }
            set {
                if (selectedfield == value) { return; }
                selectedfield = value;
                RaisePropertyChanged("SelectedField");
                if (selectedfield.Id != loadedfeature.Id) {
                    loadedfeature = selectedfield;
                    fieldshape = selectedfield.GetShape();
                    if (fieldshape is MultipolygonShape) {
                        MultipolygonShape ps = fieldshape as MultipolygonShape;
                        OpenAndLoadaShapefile();
                    }
                }
            }
        }

        public List<Feature> Fields { get; set; }

        public List<Feature> Buffers { get; set; }

        public Bitmap BitMap {
            get { return bitMap; }
            set {
                if (bitMap == value) { return; }
                bitMap = value;
                RaisePropertyChanged("BitMap");
            }
        }

        //public Bitmap second { get; set; }

        void InitializeMap() {
            startedOutOnline = Landdb.Client.Infrastructure.NetworkStatus.IsInternetAvailableFast();
            Map = new MapEngine();
            projection = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetGoogleMapParametersString());
            Map.CurrentExtent = ExtentHelper.GetDrawingExtent(new RectangleShape(-13900000.2, 9200000.4, 12000000.9, -9300000.2), width, height);

            BingMapsLayer binglayer = new BingMapsLayer("AgwFGldoH7RpgsCWgbL0pqvweosSbsuGvRinCPUPgJwGUlqwYBYsRY3O33dQHxAy");
            //binglayer.CacheDirectory = ApplicationEnvironment.MapCacheDirectory;
            //binglayer.CachePictureFormat = BingMapsPictureFormat.Default;
            binglayer.DrawingExceptionMode = DrawingExceptionMode.DrawException;
            binglayer.DrawingException += layer_DrawingException;
            binglayer.MapType = BingMapsMapType.AerialWithLabels;
            binglayer.IsVisible = true;
            if (maplayerimagery == "Bing") {
                binglayer.MapType = BingMapsMapType.AerialWithLabels;
                binglayer.IsVisible = true;
            }
            if (maplayerimagery == "Roads Only") {
                binglayer.MapType = BingMapsMapType.Road;
                binglayer.IsVisible = true;
            }
            if (maplayerimagery == "None") {
                binglayer.MapType = BingMapsMapType.Road;
                binglayer.IsVisible = false;
            }

            binglayer.SendingWebRequest += layer_SendingWebRequest;
            binglayer.TimeoutInSeconds = 20;
            Map.StaticLayers.Add("Bing", binglayer);

            if (annotationvisibility && includelabels) {
                CreateAnnotationMemoryLayer();
                RebuildMapAnnotations();
            }
        }

        void layer_SendingWebRequest(object sender, SendingWebRequestEventArgs e)
        {
            var holder = e;
        }

        private void layer_DrawingException(object sender, EventArgs e)
        {
            var holder = e;
        }

        private void OpenAndLoadaShapefile() {
            CreateInMemoryLayerAndOverlay();
            LoadShapeFileOverlayOntoMap();
        }

        private void CreateInMemoryLayerAndOverlay() {
            fieldsLayer = new InMemoryFeatureLayer();
            fieldsLayer.FeatureSource.Open();
            FeatureSourceColumn column = new FeatureSourceColumn(@"LdbLabel", DbfColumnType.Character.ToString(), 64);
            FeatureSourceColumn column1 = new FeatureSourceColumn(@"AreaLabel", DbfColumnType.Character.ToString(), 64);
            FeatureSourceColumn column2 = new FeatureSourceColumn(@"AreaOnlyLabel", DbfColumnType.Character.ToString(), 64);
            fieldsLayer.Columns.Add(column);
            fieldsLayer.Columns.Add(column1);
            fieldsLayer.Columns.Add(column2);
            fieldsLayer.FeatureSource.Close();
            fieldsLayerAreaStyle = new ClientLayerAreaStyle();
            //fieldsLayerAreaStyle.SelectedAreaStyle = new AreaStyle(new GeoPen(GeoColor.SimpleColors.Yellow, 2), new GeoSolidBrush(new GeoColor(75, GeoColor.SimpleColors.Yellow)));
            //fieldsLayerAreaStyle.UnselectedAreaStyle = new AreaStyle(new GeoPen(GeoColor.SimpleColors.Yellow, 2), new GeoSolidBrush(new GeoColor(75, GeoColor.SimpleColors.Yellow)));
            fieldsLayerAreaStyle.SelectedAreaStyle = AreaStyles.CreateSimpleAreaStyle(new GeoColor(fieldopacityfill, fieldsselectedGeoColor), new GeoColor(fieldopacity, fieldsselectedborderGeoColor), bordersize);
            fieldsLayerAreaStyle.UnselectedAreaStyle = new AreaStyle(new GeoPen(new GeoColor(fieldopacity, fieldsunselectedborderGeoColor), bordersize), new GeoSolidBrush(new GeoColor(fieldopacityfill, fieldsunselectedGeoColor)));
            fieldsLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(fieldsLayerAreaStyle);
            if (displayLabels && includelabels) {
                fieldsLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(scalingTextStyle);
            }
            fieldsLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            fieldsLayer.FeatureSource.Projection = projection;
            fieldsLayerAreaStyle.SelectedFeatures = new List<Feature>();
            fieldsLayerAreaStyle.HiddenFeatures = new List<Feature>();
        }

        private void LoadShapeFileOverlayOntoMap() {
            if (fieldsLayer == null) {
                fieldsLayer = new InMemoryFeatureLayer();
            }

            if (!fieldsLayer.IsOpen) {
                fieldsLayer.Open();
            }

            if (!projection.IsOpen) {
                projection.Open();
            }
            var extent = ExtentHelper.GetDrawingExtent(new RectangleShape(-126.51, 50.92, -66.08, 22.48), width, height);
            try {
                if (Fields == null)
                {
                    fieldsLayer.InternalFeatures.Add(SelectedField);
                }
                else
                {
                    foreach (var layer in Fields)
                    {
                        fieldsLayer.InternalFeatures.Add(layer);
                        
                    }
                }
                if (Buffers == null) {
                    //fieldsLayer.InternalFeatures.Add(SelectedField);
                }
                else {
                    foreach (var layer in Buffers) {
                        bufferLayer.InternalFeatures.Add(layer);

                    }
                }

                var shapelayerfeatures = fieldsLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);
                //var shapelayerbuffers = bufferLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);
                //if (shapelayerbuffers.Count > 0) {
                //    shapelayerfeatures = shapelayerbuffers;
                //}
                if (shapelayerfeatures.Count > 0) {
                    //var shapelayercolumns = fieldsLayer.FeatureSource.GetColumns();
                    extent = ExtentHelper.GetBoundingBoxOfItems(shapelayerfeatures);
                    //extent.ScaleUp(120);

                    var boxedSize = extent.Width > extent.Height ? extent.Width : extent.Height;
                    extent = ExtentHelper.GetDrawingExtent(extent.GetBoundingBox(), (int)boxedSize, (int)boxedSize);
                    extent.ScaleUp(scale);
                    Map.CurrentExtent = extent;

                    Map.StaticLayers.Add("fieldLayer", fieldsLayer);
                    //if (displayLabels && includelabels) {
                    //    fieldsLayerAreaStyle.SelectedFeatures = fieldsLayer.InternalFeatures.ToList();
                    //}
                    fieldsLayerAreaStyle.SelectedFeatures = fieldsLayer.InternalFeatures.ToList();
                }
                else {
                    Map.CurrentExtent = extent;
                }
            }
            catch {
                Map.CurrentExtent = extent;
            }
            if (includelabels) {
                scalingTextStyle.MapScale = Map.GetCurrentScale(this.width, GeographyUnit.Meter);
            }
        }

        private void OpenAndLoadScoutingObservations(List<Observation> scoutingobservations, double distance) {
            //CreateInMemoryScoutingLayerAndOverlay();
            LoadScoutingOverlayOntoMap(scoutingobservations, distance);
        }

        private void LoadScoutingOverlayOntoMap(List<Observation> scoutingobservations, double clusterradius) {
            scoutingLayer = new InMemoryFeatureLayer();
            scoutingLayer.FeatureSource.Open();
            FeatureSourceColumn column = new FeatureSourceColumn(@"ObservationNumber", "String", 64);
            scoutingLayer.Columns.Add(column);
            scoutingLayer.FeatureSource.Close();

            //MapStyles.ClusterPointStyle clusterPointStyle = new MapStyles.ClusterPointStyle(PointSymbolType.Triangle, new GeoSolidBrush(GeoColor.FromArgb(150, GeoColor.StandardColors.Orange)), new GeoPen(GeoColor.StandardColors.Red, 2), 27);
            MapStyles.ClusterPointStyle clusterPointStyle = new MapStyles.ClusterPointStyle(PointSymbolType.Circle, new GeoSolidBrush(GeoColor.FromArgb(150, GeoColor.StandardColors.Orange)), new GeoPen(GeoColor.StandardColors.Yellow, 3), 50);
            clusterPointStyle.TextStyle = new TextStyle("ObservationNumber", new GeoFont("Arial", 8), new GeoSolidBrush(GeoColor.StandardColors.Black));
            clusterPointStyle.TextStyle.DuplicateRule = LabelDuplicateRule.UnlimitedDuplicateLabels;
            clusterPointStyle.TextStyle.PointPlacement = PointPlacement.Center;
            clusterPointStyle.TextStyle.OverlappingRule = LabelOverlappingRule.AllowOverlapping;
            scoutingLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Clear();
            scoutingLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(clusterPointStyle);
            scoutingLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            scoutingLayer.FeatureSource.Projection = projection;

            if (scoutingLayer == null) {
                scoutingLayer = new InMemoryFeatureLayer();
            }

            if (!scoutingLayer.IsOpen) {
                scoutingLayer.Open();
            }

            if (!scoutingLayer.IsOpen) {
                scoutingLayer.Open();
            }

            Collection<Feature> featurelist = new Collection<Feature>();
            foreach (Observation obser in scoutingobservations) {
                if(obser == null) { continue; }
                if(obser.ObservationNumber == 0) { continue; }
                if (obser.GeoPosition.Longitude == 0) { continue; }
                if (obser.GeoPosition.Latitude == 0) { continue; }
                IDictionary<string, string> columnvalues = new Dictionary<string, string>();
                string obid = obser.ObservationNumber.ToString();
                columnvalues.Add(@"ObservationNumber", obid);
                PointShape pointshape = new PointShape(obser.GeoPosition.Longitude, obser.GeoPosition.Latitude);
                Feature newfeature = new Feature(pointshape.GetWellKnownText(), obid, columnvalues);
                scoutingLayer.InternalFeatures.Add(newfeature);
                featurelist.Add(newfeature);
            }

            Dictionary<string, IList<Feature>> clusterDictionary = clusterPointStyle.ClusterDictionaryMethod(scoutingLayer.InternalFeatures, clusterradius, GeographyUnit.DecimalDegree, mapDistanceUnit);

            clusterPointStyle.ClusterDictionary = clusterDictionary;
            Map.StaticLayers.Add("scoutingLayer", scoutingLayer);
        }

        private void DrawImage() {
            if (bitMap != null) { bitMap.Dispose(); }
            bitMap = new Bitmap(width, height);
            //bitMap = new Bitmap((int)(Map.CurrentExtent.Width * .2), (int)(Map.CurrentExtent.Height * .2));
            
            Map.OpenAllLayers();
            
            //make sure bing layers are loaded.....
            Map.LayerDrawing += Map_LayerDrawing;
            Map.LayerDrawn += Map_LayerDrawn;

            Task drawMapTask = Task.Factory.StartNew(() =>
            {
                Map.DrawStaticLayers(bitMap, GeographyUnit.Meter);

                Map.CloseAllLayers();
                bitMap.SetResolution(80.0F, 80.0F);
                BitMap = bitMap;
            });

            drawMapTask.Wait();
        }

        void Map_LayerDrawn(object sender, LayerDrawnEventArgs e)
        {
            var holder = e;
        }

        void Map_LayerDrawing(object sender, LayerDrawingEventArgs e)
        {
            var holder = e;
        }

        private void CreateAnnotationMemoryLayer() {
            annotationLayer = new InMemoryFeatureLayer();
            annotationLayer.FeatureSource.Open();
            FeatureSourceColumn column1 = new FeatureSourceColumn(@"Id", "string", 64);
            FeatureSourceColumn column2 = new FeatureSourceColumn(@"LdbLabel", "string", 64);
            FeatureSourceColumn column3 = new FeatureSourceColumn(@"TypeName", "string", 64);
            FeatureSourceColumn column4 = new FeatureSourceColumn(@"ShapeType", "string", 64);
            annotationLayer.Columns.Add(column1);
            annotationLayer.Columns.Add(column2);
            annotationLayer.Columns.Add(column3);
            annotationLayer.Columns.Add(column4);
            annotationLayer.FeatureSource.Close();
            annotationLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = null;
            annotationLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = null;
            annotationLayer.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = null;
            annotationLayer.ZoomLevelSet.ZoomLevel01.DefaultTextStyle = null;
            annotationLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Clear();
            AreaStyle areastyle = new AreaStyle(new GeoPen(new GeoColor(100, GeoColor.StandardColors.Transparent), 2), new GeoSolidBrush(new GeoColor(60, GeoColor.StandardColors.Transparent)));
            LineStyle linestyle = new LineStyle(new GeoPen(new GeoColor(100, GeoColor.StandardColors.Transparent), 2));
            PointStyle pointstyle = new PointStyle(PointSymbolType.Circle, new GeoSolidBrush(new GeoColor(100, GeoColor.StandardColors.Transparent)), 10);
            customMultiStyles = new MapStyles.AnnotationMultiStyle(areastyle, linestyle, pointstyle, annotationopacity);
            customMultiStyles.ColumnsList = new List<string>() { @"Id", @"LdbLabel", @"TypeName", @"ShapeType" };
            customMultiStyles.Opacity = annotationopacity;
            customMultiStyles.MaximumLabelSize = this.mapsettings.ScalingTextMaximumSize;
            customMultiStyles.MinimumLabelSize = this.mapsettings.ScalingTextMinimumSize;
            customMultiStyles.MaximumScale = 19000;
            customMultiStyles.MinimumScale = 1200;
            annotationLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customMultiStyles);
            scalingTextStyle2 = new AnnotationScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, mapsettings.ScalingTextMaximumSize, this.mapsettings.ScalingTextMinimumSize);
            scalingTextStyle2.ColumnsList = new List<string>() { @"Id", @"LdbLabel", @"TypeName", @"ShapeType" };
            //scalingTextStyle2.YOffsetInPixel = 20;
            if (mapannotationlabelsvisible) {
                annotationLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(scalingTextStyle2);
            }
            annotationLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            annotationLayer.FeatureSource.Projection = projection;

            Map.StaticLayers.Add("annotationLayer", annotationLayer);
        }

        private void RebuildMapAnnotations() {
            var begin = DateTime.UtcNow;
            userstylenames = this.mapsettings.MapAnnotationCategories;
            annotationopacity = this.mapsettings.MapAnnotationOpacity;
            customMultiStyles.Opacity = annotationopacity;
            if (userstylenames.Count > 0) {
                StyleData.Self.UserStyleNames = userstylenames;
                changedstyles = new List<AnnotationStyleItem>();
                foreach (KeyValuePair<string, MapAnnotationCategory> asi in userstylenames) {
                    AnnotationStyleItem annotatonstyle = styleFactory.GetStyle(asi.Value.Name, asi.Value.ShapeType);
                    annotatonstyle.Visible = asi.Value.Visible;
                    changedstyles.Add(annotatonstyle);
                }
                StyleData.Self.CustomStyles = changedstyles;
            }
            if (annotationLayer == null) {
                annotationLayer = new InMemoryFeatureLayer();
            }

            if (!annotationLayer.IsOpen) {
                annotationLayer.Open();
            }
            annotationLayer.InternalFeatures.Clear();
            if (!annotationvisibility) { return; }

            annotationLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = null;
            annotationLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = null;
            annotationLayer.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = null;
            annotationLayer.ZoomLevelSet.ZoomLevel01.DefaultTextStyle = null;
            annotationLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Clear();
            AreaStyle areastyle = new AreaStyle(new GeoPen(new GeoColor(100, GeoColor.StandardColors.Transparent), 2), new GeoSolidBrush(new GeoColor(60, GeoColor.StandardColors.Transparent)));
            LineStyle linestyle = new LineStyle(new GeoPen(new GeoColor(100, GeoColor.StandardColors.Transparent), 2));
            PointStyle pointstyle = new PointStyle(PointSymbolType.Circle, new GeoSolidBrush(new GeoColor(100, GeoColor.StandardColors.Transparent)), 10);
            customMultiStyles = new MapStyles.AnnotationMultiStyle(areastyle, linestyle, pointstyle, annotationopacity);
            customMultiStyles.ColumnsList = new List<string>() { @"Id", @"LdbLabel", @"TypeName", @"ShapeType" };
            customMultiStyles.Opacity = annotationopacity;
            customMultiStyles.MaximumLabelSize = this.mapsettings.ScalingTextMaximumSize;
            customMultiStyles.MinimumLabelSize = this.mapsettings.ScalingTextMinimumSize;
            customMultiStyles.MaximumScale = 19000;
            customMultiStyles.MinimumScale = 1200;
            annotationLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customMultiStyles);
            scalingTextStyle2 = new AnnotationScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, mapsettings.ScalingTextMaximumSize, this.mapsettings.ScalingTextMinimumSize);
            scalingTextStyle2.ColumnsList = new List<string>() { @"Id", @"LdbLabel", @"TypeName", @"ShapeType" };
            //scalingTextStyle2.YOffsetInPixel = 20;
            if (mapannotationlabelsvisible) {
                annotationLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(scalingTextStyle2);
            }
            annotationLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;

            var annotations = clientEndpoint.GetView<MapAnnotationListView>(new DataSourceId(ApplicationEnvironment.CurrentDataSourceId));
            List<MapAnnotationListItem> anlist = annotations.HasValue && annotations.Value.Annotations != null ? annotations.Value.Annotations : new List<MapAnnotationListItem>();
            IList<MapAnnotationDetailView> detaillist = new List<MapAnnotationDetailView>();
            IList<AnnotationStyleItem> stylelist = new List<AnnotationStyleItem>();
            foreach (var m in anlist) {
                if (m.Name != null && !string.IsNullOrEmpty(m.Name)) {
                    try {
                        var shapeItemMaybe = clientEndpoint.GetView<MapAnnotationDetailView>(m.Id);
                        MapAnnotationId mapAnnotationId = new MapAnnotationId();
                        string annotationName = string.Empty;
                        string annotationWktData = string.Empty;
                        string annotationShapeType = string.Empty;
                        string annotationType = string.Empty;
                        int? annotationCropYear = null;
                        shapeItemMaybe.IfValue(annotation => {
                            detaillist.Add(annotation);
                            string anid = annotation.Id.Id.ToString();
                            mapAnnotationId = annotation.Id;
                            annotationName = annotation.Name;
                            annotationWktData = annotation.WktData;
                            annotationShapeType = annotation.ShapeType;
                            annotationType = annotation.AnnotationType;
                            annotationCropYear = annotation.CropYear;
                            IDictionary<string, string> columnvalues = new Dictionary<string, string>();
                            columnvalues.Add(@"Id", anid);
                            columnvalues.Add(@"LdbLabel", annotationName);
                            columnvalues.Add(@"TypeName", annotationType);
                            columnvalues.Add(@"ShapeType", annotationShapeType);
                            Feature newfeature = new Feature(annotationWktData, anid, columnvalues);
                            annotationLayer.InternalFeatures.Add(anid, newfeature);
                            var f = new Feature(annotationWktData, mapAnnotationId.Id.ToString(), columnvalues);
                            if (annotationLayer.InternalFeatures.Contains(anid)) {
                                annotationLayer.InternalFeatures.Remove(anid);
                            }
                            annotationLayer.InternalFeatures.Add(anid, newfeature);
                        });

                    }
                    catch { }
                }
            }

            //var end = DateTime.UtcNow;
            //log.Debug("Annotation Shapes load took " + (end - begin).TotalMilliseconds + "ms");
        }

        private void CreateBufferMemoryLayer(string Severity) {
            Dictionary<string, string> listofhatchstrings = new Dictionary<string, string>();
            listofhatchstrings.Add("BufferZone", "ForwardDiagonal");
            listofhatchstrings.Add("Pollen Drift", "Weave");
            //listofhatchstrings.Add("BufferZoneLERAPB", "Trellis");
            listofhatchstrings.Add("Interim Aquatic", "Shingle");
            listofhatchstrings.Add("Spray Drift", "DottedGrid");
            listofhatchstrings.Add("Bee Protection", "LargeCheckerBoard");
            Dictionary<string, GeoColor> listofcolors = new Dictionary<string, GeoColor>();
            listofcolors.Add("BufferZone", GeoColor.StandardColors.SaddleBrown);
            listofcolors.Add("Pollen Drift", GeoColor.StandardColors.Red);
            //listofcolors.Add("BufferZoneLERAPB", GeoColor.StandardColors.Orange);
            listofcolors.Add("Interim Aquatic", GeoColor.StandardColors.Blue);
            listofcolors.Add("Spray Drift", GeoColor.StandardColors.Cyan);
            listofcolors.Add("Bee Protection", GeoColor.StandardColors.Gold);
            Dictionary<string, AnnotationStyleItem> listofstyles = new Dictionary<string, AnnotationStyleItem>();
            listofstyles.Add("BufferZone", new AnnotationStyleItem("BufferZone", "Polygon", AreaStyles.CreateHatchStyle(ViewModel.Maps.HatchStyleFactory.GetHatchStyle("ForwardDiagonal"), new GeoColor(125, GeoColor.StandardColors.SaddleBrown), new GeoColor(25, GeoColor.StandardColors.Brown))));
            listofstyles.Add("Pollen Drift", new AnnotationStyleItem("Pollen Drift", "Polygon", AreaStyles.CreateHatchStyle(ViewModel.Maps.HatchStyleFactory.GetHatchStyle("Weave"), new GeoColor(125, GeoColor.StandardColors.Red), new GeoColor(25, GeoColor.StandardColors.Red))));
            //listofstyles.Add("BufferZoneLERAPB", new AnnotationStyleItem("BufferZoneLERAPB", "Polygon", AreaStyles.CreateHatchStyle(ViewModel.Maps.HatchStyleFactory.GetHatchStyle("Trellis"), new GeoColor(125, GeoColor.StandardColors.Orange), new GeoColor(25, GeoColor.StandardColors.Orange))));
            listofstyles.Add("Interim Aquatic", new AnnotationStyleItem("Interim Aquatic", "Polygon", AreaStyles.CreateHatchStyle(ViewModel.Maps.HatchStyleFactory.GetHatchStyle("Shingle"), new GeoColor(125, GeoColor.StandardColors.Blue), new GeoColor(25, GeoColor.StandardColors.Blue))));
            listofstyles.Add("Spray Drift", new AnnotationStyleItem("Spray Drift", "Polygon", AreaStyles.CreateHatchStyle(ViewModel.Maps.HatchStyleFactory.GetHatchStyle("DottedGrid"), new GeoColor(125, GeoColor.StandardColors.Cyan), new GeoColor(25, GeoColor.StandardColors.Cyan))));
            listofstyles.Add("Bee Protection", new AnnotationStyleItem("Bee Protection", "Polygon", AreaStyles.CreateHatchStyle(ViewModel.Maps.HatchStyleFactory.GetHatchStyle("LargeCheckerBoard"), new GeoColor(125, GeoColor.StandardColors.Gold), new GeoColor(25, GeoColor.StandardColors.Gold))));

            bufferLayer = new InMemoryFeatureLayer();
            bufferLayer.FeatureSource.Open();
            string hatchstyle1 = "None";
            GeoColor geocolor1 = GeoColor.SimpleColors.Red;
            if (!string.IsNullOrEmpty(Severity) && listofcolors.ContainsKey(Severity) && listofhatchstrings.ContainsKey(Severity)) {
                hatchstyle1 = listofhatchstrings[Severity];
                geocolor1 = listofcolors[Severity];
            }
            bufferLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = AreaStyles.CreateHatchStyle(ViewModel.Maps.HatchStyleFactory.GetHatchStyle(hatchstyle1), new GeoColor(150, geocolor1), new GeoColor(25, geocolor1));
            bufferLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = LineStyles.CreateSimpleLineStyle(GeoColor.FromArgb(100, GeoColor.SimpleColors.Red), 2, true);
            bufferLayer.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = PointStyles.CreateSimplePointStyle(PointSymbolType.Circle, GeoColor.FromArgb(100, GeoColor.SimpleColors.Red), 8);
            bufferLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            bufferLayer.FeatureSource.Projection = projection;

            Map.StaticLayers.Add("bufferLayer", bufferLayer);

            if (bufferLayer == null) {
                bufferLayer = new InMemoryFeatureLayer();
            }

            if (!bufferLayer.IsOpen) {
                bufferLayer.Open();
            }

            if (!bufferLayer.IsOpen) {
                bufferLayer.Open();
            }

        }

        private string DisplaySelectedAreaDecimals(string name, double unitvalue, string unitname) {
            IUnit _unit = UnitFactory.GetUnitByName(unitname);
            string decimalstring = "N" + areadecimals.ToString("N0");
            //  + "\r\n" + 
            return name + "\r\n" + unitvalue.ToString(decimalstring);
            //return name + ": " + unitvalue.ToString(decimalstring) + " " + _unit.AbbreviatedDisplay;
        }

        private string DisplayLabelWithArea(string name, string customarealabel) {
            return name + "\r\n" + customarealabel;
        }

        private string GetCustomAreaLabel(Guid Id, bool isfieldlabel) {
            string stringid = Id.ToString();
            string labelname = string.Empty;
            if (isfieldlabel) {
                var fieldMaybe = clientEndpoint.GetView<FieldDetailsView>(new FieldId(ApplicationEnvironment.CurrentDataSourceId, Id));
                fieldMaybe.IfValue(field => {
                    labelname = string.Empty;
                    if (field.ReportedArea != null && !string.IsNullOrWhiteSpace(field.ReportedAreaUnit)) {
                        labelname = DisplaySelectedAreaOnlyDecimals(field.ReportedArea.Value, field.ReportedAreaUnit);
                    }
                    else if (field.BoundaryArea != null && !string.IsNullOrWhiteSpace(field.BoundaryAreaUnit)) {
                        labelname = DisplaySelectedAreaOnlyDecimals(field.BoundaryArea.Value, field.BoundaryAreaUnit);
                    }
                });
            }
            else {
                var fieldMaybe = clientEndpoint.GetView<CropZoneDetailsView>(new CropZoneId(ApplicationEnvironment.CurrentDataSourceId, Id));
                fieldMaybe.IfValue(field => {
                    labelname = string.Empty;
                    if (field.ReportedArea != null && !string.IsNullOrWhiteSpace(field.ReportedAreaUnit)) {
                        labelname = DisplaySelectedAreaOnlyDecimals(field.ReportedArea.Value, field.ReportedAreaUnit);
                    }
                    else if (field.BoundaryArea != null && !string.IsNullOrWhiteSpace(field.BoundaryAreaUnit)) {
                        labelname = DisplaySelectedAreaOnlyDecimals(field.BoundaryArea.Value, field.BoundaryAreaUnit);
                    }
                });
            }
            return labelname;
        }

        private string DisplaySelectedAreaOnlyDecimals(double unitvalue, string unitname) {
            IUnit _unit = UnitFactory.GetUnitByName(unitname);
            string decimalstring = "N" + areadecimals.ToString("N0");
            return unitvalue.ToString(decimalstring);
        }

    }
}
