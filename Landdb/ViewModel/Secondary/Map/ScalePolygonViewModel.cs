﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GalaSoft.MvvmLight;
using ThinkGeo.MapSuite.WpfDesktopEdition;
using ThinkGeo.MapSuite.Core;
using Landdb.Infrastructure;
using System.IO;
using Microsoft.Win32;
using GalaSoft.MvvmLight.Command;
using System.Windows.Input;
using System.Collections.ObjectModel;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure.Messages;
using Landdb.Domain.ReadModels.Tree;
using Landdb.MapStyles;
using Landdb.Client.Infrastructure;
using System.Windows.Threading;
using Landdb.ViewModel.Maps.ImageryOverlay;
using AgC.UnitConversion;
using Landdb.Resources;
using Landdb.ViewModel.Fields.FieldDetails.MapModels;
using NLog;

namespace Landdb.ViewModel.Secondary.Map {
    public class ScalePolygonViewModel : ViewModelBase
    {
        Logger log = LogManager.GetCurrentClassLogger();
        bool maploaded = false;
        bool startedOutOnline = false;
        //IMapOverlay selectedoverlay;
        bool ismousedown = false;
        bool draglabel = true;
        bool hasunsavedchanges = false;
        bool isfieldselected = false;
        string fieldname = Strings.DragLabel_Text;
        double desiredarea = 100;
        double oldarea = 0;
        double scalepercent = 0;
        double pointpercentage = 100;
        double desireddistance = 0;
        IList<MapItem> clientLayerItems;
        InMemoryFeatureLayer fieldsLayer = new InMemoryFeatureLayer();
        LayerOverlay layerOverlay = new LayerOverlay();
        Proj4Projection projection;
        RectangleShape previousmapextent = new RectangleShape();
        Feature selectedfield = new Feature();
        Feature loadedfeature = new Feature();
        BaseShape fieldshape = new MultipolygonShape();
        ClientLayerAreaStyle fieldsLayerAreaStyle = null;
        IClientEndpoint clientEndpoint;
        MapSettings mapsettings;
        string areaUnitString = string.Empty;
        string coordinateformat = string.Empty;
        AgC.UnitConversion.Area.AreaUnit agcAreaUnit;
        ThinkGeo.MapSuite.Core.AreaUnit mapAreaUnit;
        ThinkGeo.MapSuite.Core.DistanceUnit mapDistanceUnit;
        int fieldopacity = 125;
        int annotationopacity = 125;
        int areadecimals = 2;
        ZoomLevelFactory zoomlevelfactory = new ZoomLevelFactory();

        public ScalePolygonViewModel(IClientEndpoint clientEndpoint)
        {
            this.clientEndpoint = clientEndpoint;
            InitializeSettings();
            //var locator = System.Windows.Application.Current.TryFindResource("Locator") as ViewModelLocator;
            //selectedoverlay = locator.ThinkGeoUtitlityVM.GetSelectedMapFromFactory("Bing");
            //selectedoverlay = new BingMapOverlay();

            InitializeMap();
            PointPercentage = 100;
            HasUnsavedChanges = false;

            CompleteCommand = new RelayCommand(Complete);
            CalculateAreaCommand = new RelayCommand(CalculateArea);
            CalculateDistanceCommand = new RelayCommand(CalculateDistance);
            CalculateWeedCommand = new RelayCommand(CalculateWeed);
            CancelCommand = new RelayCommand(Cancel);
        }

        public ICommand CompleteCommand { get; private set; }
        public ICommand CalculateAreaCommand { get; private set; }
        public ICommand CalculateDistanceCommand { get; private set; }
        public ICommand CalculateWeedCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }

        public WpfMap Map { get; set; }

        public IList<MapItem> ClientLayer
        {
            get { return clientLayerItems; }
            set
            {
                clientLayerItems = value;
                LoadFieldLayerShapes();
                RaisePropertyChanged("ClientLayer");
            }
        }

        public Feature SelectedField
        {
            get { return selectedfield; }
            set
            {
                if (selectedfield == value) { return; }
                selectedfield = value;
                RaisePropertyChanged("SelectedField");
                InitializeSettings();
                if (selectedfield.Id != loadedfeature.Id)
                {
                    loadedfeature = selectedfield;
                    fieldshape = selectedfield.GetShape();
                    if (fieldshape is MultipolygonShape)
                    {
                        MultipolygonShape ps = fieldshape as MultipolygonShape;
                        DesiredArea = System.Convert.ToDouble(Math.Round(ps.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit), 4));
                    }

                    if (maploaded)
                    {
                        OpenAndLoadaShapefile();
                    }
                }
            }
        }

        public string FieldName
        {
            get { return fieldname; }
            set
            {
                if (fieldname == value) { return; }
                fieldname = value;
                RaisePropertyChanged("FieldName");
            }
        }

        public bool HasUnsavedChanges
        {
            get { return hasunsavedchanges; }
            set
            {
                if (hasunsavedchanges == value) { return; }
                hasunsavedchanges = value;
                RaisePropertyChanged("HasUnsavedChanges");
            }
        }

        public RectangleShape PreviousMapExtent
        {
            get { return previousmapextent; }
            set
            {
                if (previousmapextent == value) { return; }
                previousmapextent = value;
                RaisePropertyChanged("PreviousMapExtent");
            }
        }

        public double DesiredArea
        {
            get { return desiredarea; }
            set
            {
                if (desiredarea == value) { return; }
                desiredarea = value;
                RaisePropertyChanged("DesiredArea");
            }
        }

        public double OldArea
        {
            get { return oldarea; }
            set
            {
                if (oldarea == value) { return; }
                oldarea = value;
                RaisePropertyChanged("OldArea");
            }
        }

        public double ScalePercent
        {
            get { return scalepercent; }
            set
            {
                if (scalepercent == value) { return; }
                scalepercent = value;
                RaisePropertyChanged("ScalePercent");
            }
        }

        public double PointPercentage
        {
            get { return pointpercentage; }
            set
            {
                if (pointpercentage == value) { return; }
                pointpercentage = value;
                RaisePropertyChanged("PointPercentage");
            }
        }

        public double DesiredDistance {
            get { return desireddistance; }
            set {
                if (desireddistance == value) { return; }
                desireddistance = value;
                RaisePropertyChanged("DesiredDistance");
            }
        }
        //public IMapOverlay SelectedOverlay {
        //    get { return selectedoverlay; }
        //    set {
        //        if (selectedoverlay == value) { return; }
        //        if (selectedoverlay == null) { return; }
        //        if (string.IsNullOrWhiteSpace(value.Name)) { return; }
        //        selectedoverlay = value;
        //        //LoadBingMapsOverlay();
        //        //Map.Refresh();
        //        RaisePropertyChanged("SelectedOverlay");
        //    }
        //}

        internal void EnsureFieldsLayerIsOpen()
        {
            if (fieldsLayer == null)
            {
                fieldsLayer = new InMemoryFeatureLayer();
            }

            if (!fieldsLayer.IsOpen)
            {
                fieldsLayer.Open();
            }

            if (!projection.IsOpen)
            {
                projection.Open();
            }
        }

        private void InitializeSettings() {
            this.mapsettings = clientEndpoint.GetMapSettings();
            areadecimals = mapsettings.AreaDecimals;
            areaUnitString = mapsettings.MapAreaUnit;
            coordinateformat = mapsettings.DisplayCoordinateFormat;
            agcAreaUnit = UnitFactory.GetUnitByName(areaUnitString) as AgC.UnitConversion.Area.AreaUnit;
            mapAreaUnit = MapUnitFactory.AreaMeasureConversion(agcAreaUnit);
            mapDistanceUnit = MapUnitFactory.AreaDistanceMeasureConversion(agcAreaUnit);
        }

        void InitializeMap()
        {
            Map = new WpfMap();
            startedOutOnline = Landdb.Client.Infrastructure.NetworkStatus.IsInternetAvailableFast();
            Map.MapUnit = GeographyUnit.Meter;
            Map.MapTools.PanZoomBar.Visibility = System.Windows.Visibility.Visible;
            Map.MapTools.PanZoomBar.GlobeButtonVisibility = System.Windows.Visibility.Hidden;
            Map.MapTools.PanZoomBar.VerticalAlignment = System.Windows.VerticalAlignment.Center;
            Map.MapTools.PanZoomBar.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            Map.Background = System.Windows.Media.Brushes.White;
            zoomlevelfactory = new ZoomLevelFactory();
            Map.ZoomLevelSet = zoomlevelfactory.ZoomLevelSET;

            projection = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetGoogleMapParametersString());

            Map.Loaded += (sender, e) =>
            {
                if (maploaded) { return; }
                LoadBingMapsOverlay();
                OpenAndLoadaShapefile();
                maploaded = true;
            };

            Map.CurrentScaleChanged += (sender, e) =>
            {
                //scalingTextStyle.MapScale = e.CurrentScale;
            };

            //Map.MapClick += (sender, e) => {
            //    SavePolygon();
            //};

        }

        private void SavePolygon()
        {
            log.Info("ScaleTools - Save save polygon");
            EnsureFieldsLayerIsOpen();
            MultipolygonShape multipoly = new MultipolygonShape();
            try {
                Collection<Feature> selectedFeatures = fieldsLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);
                if (selectedFeatures.Count > 0) {
                    for (int i = 0; i < fieldsLayer.InternalFeatures.Count; i++) {
                        Feature feat = fieldsLayer.InternalFeatures[i];
                        multipoly = feat.GetShape() as MultipolygonShape;
                        break;
                    }
                    var importmessage = new ScalePolygonMessage() { ShapeBase = multipoly };
                    Messenger.Default.Send<ScalePolygonMessage>(importmessage);
                }
            }
            catch (Exception ex) {
                log.ErrorException("Caught an exception while saving polygon", ex);
                //System.Windows.MessageBox.Show("ImportTools - Caught an exception while working with a simple draw shape.");
            }

        }

        private void LoadBingMapsOverlay()
        {
            if (!startedOutOnline) { return; }
            IMapOverlay mapoverlay = new BingMapOverlay();
            Overlay bingOverlay = mapoverlay.Overlay;
            if (bingOverlay == null) { return; }

            if (Map.Overlays.Contains("Bing")) {
                Map.Overlays.Remove("Bing");
            }
            if (Map.Overlays.Contains("Bing Online")) {
                Map.Overlays.Remove("Bing Online");
            }
            if (Map.Overlays.Contains("Roads Only")) {
                Map.Overlays.Remove("Roads Only");
            }
            if (Map.Overlays.Contains("Road Online")) {
                Map.Overlays.Remove("Road Online");
            }
            if (Map.Overlays.Contains("None")) {
                Map.Overlays.Remove("None");
            }
            //bingOverlay.IsVisible = mapoverlay.Visibility;
            //((TileOverlay)bingOverlay).TileType = TileType.SingleTile;
            Map.Overlays.Add(mapoverlay.Name, bingOverlay);
            Map.Overlays.MoveToBottom(mapoverlay.Name);
        
        }

        private void LoadFieldLayerShapes()
        {
            EnsureFieldsLayerIsOpen();
            fieldsLayer.InternalFeatures.Clear();
            fieldsLayer.InternalFeatures.Add(SelectedField);

            //foreach (var m in clientLayerItems) {
            //    if (m.MapData != null && !string.IsNullOrEmpty(m.MapData)) {
            //        IDictionary<string, string> columnvalues = new Dictionary<string, string>();
            //        columnvalues.Add(@"LdbLabel", m.Name);
            //        var f = new Feature(m.MapData, m.FieldId.ToString(), columnvalues);
            //        fieldsLayer.InternalFeatures.Add(m.FieldId.ToString(), f);
            //    }
            //}

            if (Map.Overlays.Contains("fieldOverlay"))
            {
                Map.Overlays["fieldOverlay"].Refresh();
            }
        }

        private void OpenAndLoadaShapefile()
        {
            PointPercentage = 100;
            HasUnsavedChanges = false;
            fieldsLayer.InternalFeatures.Clear();
            CreateInMemoryLayerAndOverlay();
            LoadShapeFileOverlayOntoMap();
            if (Map.Overlays.Contains("importOverlay"))
            {
                if (((LayerOverlay)Map.Overlays["importOverlay"]).Layers.Contains("ImportLayer"))
                {
                    Map.Refresh();
                }
            }
        }

        private void CreateInMemoryLayerAndOverlay()
        {
            layerOverlay = new LayerOverlay();
            layerOverlay.DrawingExceptionMode = DrawingExceptionMode.DrawException;
            fieldsLayer = new InMemoryFeatureLayer();

            //fieldsLayer
            fieldsLayer.FeatureSource.Open();
            FeatureSourceColumn column = new FeatureSourceColumn(@"LdbLabel", DbfColumnType.Character.ToString(), 64);
            fieldsLayer.Columns.Add(column);
            fieldsLayer.FeatureSource.Close();
            fieldsLayerAreaStyle = new ClientLayerAreaStyle();
            fieldsLayerAreaStyle.SelectedAreaStyle = new AreaStyle(new GeoPen(GeoColor.SimpleColors.PastelYellow, 2), new GeoSolidBrush(new GeoColor(75, GeoColor.SimpleColors.PastelYellow)));
            fieldsLayerAreaStyle.UnselectedAreaStyle = new AreaStyle(new GeoPen(GeoColor.StandardColors.LightGray, 2), new GeoSolidBrush(new GeoColor(75, GeoColor.StandardColors.LightGray)));
            fieldsLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(fieldsLayerAreaStyle);
            //fieldsLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(scalingTextStyle);
            fieldsLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            fieldsLayer.FeatureSource.Projection = projection;
            fieldsLayerAreaStyle.SelectedFeatures = new List<Feature>();
            fieldsLayerAreaStyle.HiddenFeatures = new List<Feature>();
            layerOverlay.Layers.Add("ImportLayer", fieldsLayer);
        }

        private void LoadShapeFileOverlayOntoMap()
        {
            if (Map.Overlays.Contains("importOverlay"))
            {
                Map.Overlays.Remove("importOverlay");
            }
            if (layerOverlay.Layers.Contains("ImportLayer"))
            {
                Map.Overlays.Add("importOverlay", layerOverlay);
            }

            EnsureFieldsLayerIsOpen();
            var extent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
            try
            {
                fieldsLayer.InternalFeatures.Add(SelectedField);
                //if (ClientLayer.Count > 0 && fieldsLayer.InternalFeatures.Count == 0) {
                //    LoadFieldLayerShapes();
                //}
                var shapelayerfeatures = fieldsLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);
                if (shapelayerfeatures.Count > 0)
                {
                    var shapelayercolumns = fieldsLayer.FeatureSource.GetColumns();
                    extent = ExtentHelper.GetBoundingBoxOfItems(shapelayerfeatures);
                    extent.ScaleUp(20);
                    Map.CurrentExtent = extent;
                    PointShape center = extent.GetCenterPoint();
                    Map.CenterAt(center);
                }
                else
                {
                    Map.CurrentExtent = extent;
                    PointShape center = extent.GetCenterPoint();
                    Map.CenterAt(center);
                }
            }
            catch
            {
                Map.CurrentExtent = extent;
                PointShape center = extent.GetCenterPoint();
                Map.CenterAt(center);
            }
        }

        void map_ScaleShape()
        {
            log.Info("ScaleTools - Calculate polygon area");
            double shapearea101 = 0;
            MultipolygonShape shape = null;
            MultipolygonShape shape2 = new MultipolygonShape();
            shape = SelectedField.GetShape() as MultipolygonShape;
            shape2 = SelectedField.GetShape() as MultipolygonShape;
            MultipolygonShape shape3 = new MultipolygonShape();

            if (shape != null)
            {
                shapearea101 = shape2.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
                OldArea = shapearea101;

                double ndiff = -.0001;
                double pdiff = .0001;
                double counter = 0;
                double areadifference = DesiredArea - shapearea101;
                if (areadifference < 0)
                {
                    areadifference = areadifference * -1;
                }
                ScalePercent = (areadifference / OldArea) * 2;

                while (areadifference < ndiff || areadifference > pdiff)
                {
                    shapearea101 = shape.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);

                    if (DesiredArea > shapearea101)
                    {
                        shape.ScaleUp(System.Convert.ToSingle(ScalePercent));
                    }
                    else
                    {
                        shape.ScaleDown(System.Convert.ToSingle(ScalePercent));
                    }

                    shapearea101 = shape.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
                    areadifference = DesiredArea - shapearea101;
                    if (areadifference < 0)
                    {
                        areadifference = areadifference * -1;
                    }
                    ScalePercent = (areadifference / OldArea) * 2;

                    counter++;
                    if (counter > 1000)
                    {
                        break;
                    }
                }
                Feature newfeature = new Feature(shape, SelectedField.ColumnValues);
                fieldsLayer.InternalFeatures.Clear();
                fieldsLayer.InternalFeatures.Add(newfeature);
                selectedfield = newfeature;

                try {
                    var shapelayerfeatures = fieldsLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);
                    if (shapelayerfeatures.Count > 0) {
                        var shapelayercolumns = fieldsLayer.FeatureSource.GetColumns();
                        var extent = ExtentHelper.GetBoundingBoxOfItems(shapelayerfeatures);
                        extent.ScaleUp(20);
                        Map.CurrentExtent = extent;
                        PointShape center = extent.GetCenterPoint();
                        Map.CenterAt(center);
                    }
                    Map.Refresh();
                }
                catch (Exception ex) {
                    log.ErrorException("Caught an exception while scaling a shape", ex);
                    //System.Windows.MessageBox.Show("ImportTools - Caught an exception while working with a simple draw shape.");
                }

            }
        }

        void map_BufferShape() {
            log.Info("ScaleTools - buffer polygon");
            MultipolygonShape shape = null;
            shape = SelectedField.GetShape() as MultipolygonShape;
            MultipolygonShape shape3 = new MultipolygonShape();
            if (shape != null) {
                if (shape.Polygons.Count > 0) {
                    shape3 = shape.Buffer(desireddistance, Map.MapUnit, mapDistanceUnit);
                    Feature newfeature = new Feature(shape3, SelectedField.ColumnValues);
                    fieldsLayer.InternalFeatures.Clear();
                    fieldsLayer.InternalFeatures.Add(newfeature);
                    selectedfield = newfeature;
                    try {
                        var shapelayerfeatures = fieldsLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);
                        if (shapelayerfeatures.Count > 0) {
                            var shapelayercolumns = fieldsLayer.FeatureSource.GetColumns();
                            var extent = ExtentHelper.GetBoundingBoxOfItems(shapelayerfeatures);
                            extent.ScaleUp(20);
                            Map.CurrentExtent = extent;
                            PointShape center = extent.GetCenterPoint();
                            Map.CenterAt(center);
                        }
                        Map.Refresh();
                    }
                    catch (Exception ex) {
                        log.ErrorException("Caught an exception while buffering a shape", ex);
                    }
                }
            }
        }

        void map_WeedShape()
        {
            log.Info("ScaleTools - Weed polygon points");
            double shapearea101 = 0;
            MultipolygonShape shape = null;
            MultipolygonShape shape2 = new MultipolygonShape();
            shape = SelectedField.GetShape() as MultipolygonShape;
            shape2 = SelectedField.GetShape() as MultipolygonShape;
            MultipolygonShape shape3 = new MultipolygonShape();

            if (shape != null)
            {
                foreach (PolygonShape poly in shape.Polygons)
                {
                    PolygonShape newpoly = WeedPolygonPercent(poly, pointpercentage);
                    shape3.Polygons.Add(newpoly);
                }

                Feature newfeature = new Feature(shape3, SelectedField.ColumnValues);
                fieldsLayer.InternalFeatures.Clear();
                fieldsLayer.InternalFeatures.Add(newfeature);
                selectedfield = newfeature;

                try {
                    var shapelayerfeatures = fieldsLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);
                    if (shapelayerfeatures.Count > 0) {
                        var shapelayercolumns = fieldsLayer.FeatureSource.GetColumns();
                        var extent = ExtentHelper.GetBoundingBoxOfItems(shapelayerfeatures);
                        extent.ScaleUp(20);
                        Map.CurrentExtent = extent;
                        PointShape center = extent.GetCenterPoint();
                        Map.CenterAt(center);
                    }
                    Map.Refresh();
                }
                catch (Exception ex) {
                    log.ErrorException("Caught an exception while weeding a shape", ex);
                    //System.Windows.MessageBox.Show("ImportTools - Caught an exception while working with a simple draw shape.");
                }

            }
        }

        private PolygonShape WeedPolygonPercent(PolygonShape weedpolygon, double pivotverticespertent)
        {
            Collection<Vertex> vertexcol = weedpolygon.OuterRing.Vertices;
            Collection<Vertex> vertexnew = new Collection<Vertex>();
            double vertexcount = vertexcol.Count;
            double vertexcountnew2 = Math.Truncate(vertexcount * (pivotverticespertent / 100));
            double vertexcountnew = Math.Truncate(vertexcount / vertexcountnew2);

            double i = 0;
            foreach (Vertex vert in vertexcol)
            {
                i++;
                if (i == 1)
                {
                    vertexnew.Add(vert);
                }
                if (i >= vertexcountnew)
                {
                    i = 0;
                }
            }
            RingShape ringshape = new RingShape(vertexnew);
            PolygonShape newpoly = new PolygonShape(ringshape);
            return newpoly;
        }

        void Complete()
        {
            HasUnsavedChanges = false;
            SavePolygon();
        }

        void CalculateArea()
        {
            HasUnsavedChanges = true;
            map_ScaleShape();
        }

        void CalculateDistance() {
            HasUnsavedChanges = true;
            map_BufferShape();
        }

        void CalculateWeed()
        {
            HasUnsavedChanges = true;
            map_WeedShape();
        }

        void Cancel()
        {
            log.Info("ScaleTools - Cancel scale");
            Map.Cursor = Cursors.Arrow;
            HasUnsavedChanges = false;
            fieldsLayer.InternalFeatures.Clear();
            var importmessage = new ShapeImportMessage() { ShapeBase = new MultipolygonShape() };
            Messenger.Default.Send<ShapeImportMessage>(importmessage);
        }

    }
}
