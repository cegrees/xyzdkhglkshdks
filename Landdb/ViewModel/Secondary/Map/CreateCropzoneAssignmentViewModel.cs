﻿using AgC.UnitConversion;
using GalaSoft.MvvmLight;
using Landdb.Client.Infrastructure;
using Landdb.Client.Spatial;
using Landdb.Domain.ReadModels.Map;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.ViewModel.Fields;
using Landdb.ViewModel.Secondary.Application;
using Microsoft.Maps.MapControl.WPF;
using NLog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Landdb.ViewModel.Shared {
    public abstract class CreateCropzoneAssignmentViewModel : ViewModelBase {
        protected readonly Dispatcher dispatcher;
        protected readonly IClientEndpoint clientEndpoint;
        protected int cropYear;
        protected Logger log = LogManager.GetCurrentClassLogger();
        protected Measure totalArea = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(0);
        private TreeDisplayModeDisplayItem selectedTreeDisplayMode { get; set; }
        bool isEditPopupOpen = false;

        public CreateCropzoneAssignmentViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, int cropYear) {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            this.cropYear = cropYear;

            SelectedCropZones = new ObservableCollection<CropZoneSelectionViewModel>();

            BuildTreeList();
        }

        public ObservableCollection<AbstractTreeItemViewModel> RootTreeItemModels { get; private set; }
        public ObservableCollection<CropZoneSelectionViewModel> SelectedCropZones { get; private set; }

        public MapLayer BingMapsLayer { get; private set; }

        public string MapCulture
        {
            get { return Infrastructure.ApplicationEnvironment.BingWpfMapCulture; }
        }

        public bool IsEditPopupOpen {
            get { return isEditPopupOpen; }
            set {
                isEditPopupOpen = value;
                RaisePropertyChanged("IsEditPopupOpen");
            }
        }

        public Measure TotalArea {
            get { return totalArea; }
            set {
                totalArea = value;
                RaisePropertyChanged("TotalArea");
            }
        }

        public int FieldCount {
            get { return SelectedCropZones.Count; }
        }

        public TreeDisplayModeDisplayItem SelectedTreeDisplayMode {
            get { return selectedTreeDisplayMode; }
            set {
                if (value != null)
                    if (!Equals(selectedTreeDisplayMode, value)) {
                    selectedTreeDisplayMode = value;
                    if (RootTreeItemModels != null) {
                        RootTreeItemModels.Clear();
                    }
                    BuildTreeList();
                    RaisePropertyChanged("SelectedTreeDisplayMode");
                }
            }
        }

        void BuildTreeList() {
            
            //await Task.Run(new Action(() => {
                var begin = DateTime.UtcNow;
                AbstractTreeItemViewModel rootNode = null;
                var settings = clientEndpoint.GetUserSettings();
                bool sortAlphabetically = settings.AreFieldsSortedAlphabetically;

                // TODO: When maps are separate, move this back to the Farms case in the switch below.
                var years = clientEndpoint.GetView<FieldTreeView>(new DataSourceId(ApplicationEnvironment.CurrentDataSourceId));
                var yearItem = (years.HasValue && years.Value.CropYearList.ContainsKey(cropYear)) ? years.Value.CropYearList[cropYear] : new TreeViewYearItem();

            if (SelectedTreeDisplayMode.IsCrops) {
                var tree = clientEndpoint.GetView<CropTreeView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new CropTreeView());
                IList<TreeViewCropItem> crops = tree.Crops ?? new List<TreeViewCropItem>();
                rootNode = new GrowerCropTreeItemViewModel("Home", crops, sortAlphabetically, OnFieldCheckedChanged_internal);
            } else { //if it's anything else
                var tree = clientEndpoint.GetView<CropTreeView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new CropTreeView());
                IList<TreeViewCropItem> crops = tree.Crops ?? new List<TreeViewCropItem>();
                rootNode = new GrowerCropTreeItemViewModel("Home", crops, sortAlphabetically, OnFieldCheckedChanged_internal);
            }

            dispatcher.BeginInvoke(new Action(() => {
                    // initialize map
                    var mapQ = from m in yearItem.Maps
                               where m.IsVisible
                               select m.MapData;
                    BingMapsLayer = BingMapsUtility.GetLayerForMapData(mapQ);
                    RaisePropertyChanged("BingMapsLayer");
                }));

                if (rootNode != null) {
                    RootTreeItemModels = new ObservableCollection<AbstractTreeItemViewModel>() { rootNode };
                    RootTreeItemModels[0].IsExpanded = true;
                }
                RaisePropertyChanged("RootTreeItemModels");

                var end = DateTime.UtcNow;

                log.Debug("Applicator tree load took " + (end - begin).TotalMilliseconds + "ms");
            //}));

        }

        protected abstract void OnFieldCheckedChanged(CropZoneTreeItemViewModel treeItem);

        private void OnFieldCheckedChanged_internal(AbstractTreeItemViewModel treeItem) {
            if (treeItem is CropZoneTreeItemViewModel) {
                if (treeItem.IsChecked.HasValue && treeItem.IsChecked.Value && !SelectedCropZones.Any(x => x.Id == (treeItem.Id as CropZoneId))) {
                    var mapItem = clientEndpoint.GetView<ItemMap>(treeItem.Id);
                    string mapData = null;
                    if (mapItem.HasValue && mapItem.Value.MostRecentMapItem != null) {
                        mapData = mapItem.Value.MostRecentMapItem.MapData;
                    }

                    var cztivm = new CropZoneSelectionViewModel(treeItem as CropZoneTreeItemViewModel, mapData, clientEndpoint, () => { IsEditPopupOpen = false; }, (delta) => { 
                        TotalArea = TotalArea.Unit.GetMeasure(TotalArea.Value + delta); 
                        RaisePropertyChanged("TotalArea"); 
                    });

                    SelectedCropZones.Add(cztivm);

                    TotalArea += cztivm.SelectedArea;
                }

                if (treeItem.IsChecked.HasValue && !treeItem.IsChecked.Value) {
                    var found = SelectedCropZones.Where(x => x.Id == (CropZoneId)treeItem.Id).ToList();
                    found.ForEach(x => {
                        SelectedCropZones.Remove(x);
                        TotalArea -= x.SelectedArea;
                    });
                }

                RaisePropertyChanged("FieldCount");

                OnFieldCheckedChanged(treeItem as CropZoneTreeItemViewModel);
            }
        }

        internal CropZoneArea[] GetCropZoneAreas() {
            List<CropZoneArea> areas = new List<CropZoneArea>();

            foreach (var cz in SelectedCropZones) {
                areas.Add(new CropZoneArea(cz.Id, cz.SelectedArea.Value, cz.SelectedArea.Unit.Name));
            }

            return areas.ToArray();
        }

        internal void ResetData() {
            SelectedCropZones.Clear();
            TotalArea = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(0);
            RaisePropertyChanged("FieldCount");
            BuildTreeList();
        }
    }
}
