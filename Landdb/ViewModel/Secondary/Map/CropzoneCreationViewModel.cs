﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Shapes;
using AgC.UnitConversion;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Spatial;
using Landdb.ViewModel.Fields;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.Infrastructure;
using System.Windows.Input;

namespace Landdb.ViewModel.Secondary.Map {
    // TODO: Get into the Shared namespace instead of Secondary.Application
    public class CropzoneCreationViewModel : ViewModelBase {
        CropZoneId id;
        string fieldName;
        string name;
        Measure selectedArea;
        Path shapePreview;
        string mapData;
        double areapercent;
        string centerLatLong;
        MiniCrop crop = new MiniCrop();
        int cropYearChangeOccurred;
        bool? completed;
        IList<IDomainCommand> commandlist = new List<IDomainCommand>();
        IList<CropZoneTreeItemViewModel> nodelist = new List<CropZoneTreeItemViewModel>();

        public CropzoneCreationViewModel(CropZoneId id, string name, string fieldname, string mapData, string centerLatLong, Measure selectedArea) {
            this.id = id;
            this.fieldName = fieldname;
            this.name = name;
            this.mapData = mapData;
            this.centerLatLong = centerLatLong;
            this.selectedArea = selectedArea;
            this.cropYearChangeOccurred = 0;
            MakePreviewShape();
            commandlist = new List<IDomainCommand>();
            nodelist = new List<CropZoneTreeItemViewModel>();
        }

        public CropzoneCreationViewModel(string name, string mapData, Measure selectedArea) {
            this.name = name;
            this.mapData = mapData;
            this.selectedArea = selectedArea;
            if (!string.IsNullOrEmpty(this.mapData)) {
                this.shapePreview = WpfTransforms.CreatePathFromMapData(new[] { this.mapData }, 55);
            }
            commandlist = new List<IDomainCommand>();
            nodelist = new List<CropZoneTreeItemViewModel>();
        }

        public CropzoneCreationViewModel(string name, string mapData, Measure selectedArea, int cropYearChangeOccurred) {
            this.name = name;
            this.mapData = mapData;
            this.selectedArea = selectedArea;
            this.cropYearChangeOccurred = cropYearChangeOccurred;
            if (!string.IsNullOrEmpty(this.mapData)) {
                this.shapePreview = WpfTransforms.CreatePathFromMapData(new[] { this.mapData }, 55);
            }
            commandlist = new List<IDomainCommand>();
            nodelist = new List<CropZoneTreeItemViewModel>();
        }

        public ICommand CreateNewItemCommand { get; private set; }

        public CropZoneId Id {
            get { return id; }
            set {
                id = value;
                RaisePropertyChanged("Id");
            }
        }

        public string FieldName {
            get { return fieldName; }
            set {
                fieldName = value;
                RaisePropertyChanged("FieldName");
            }
        }

        public string Name {
            get { return name; }
            set {
                name = value;
                RaisePropertyChanged("Name");
            }
        }

        public bool? Completed {
            get { return completed; }
            set {
                completed = value;
                RaisePropertyChanged("Completed");
            }
        }

        public Measure SelectedArea {
            get { return selectedArea; }
            set {
                selectedArea = value;
                RaisePropertyChanged("SelectedArea");
            }
        }

        public double AreaPercent
        {
            get { return areapercent; }
            set
            {
                areapercent = value;
                RaisePropertyChanged("AreaPercent");
            }
        }

        public string DisplayAreaandPercent
        {
            get {
                double areapercentcalc = areapercent * 100;
                //return $"{selectedArea.Value.ToString("N2")}  :  {areapercentcalc.ToString("N1")}%";
                return $"{areapercentcalc.ToString("N1")}%";
            }
        }

        public Path ShapePreview {
            get { return shapePreview; }
        }

        public string MapData {
            get { return mapData; }
            set
            {
                mapData = value;
                RaisePropertyChanged("MapData");
            }
        }

        public string CenterLatLong {
            get { return centerLatLong; }
            set {
                centerLatLong = value;
                RaisePropertyChanged("CenterLatLong");
            }
        }

        public MiniCrop Crop {
            get { return crop; }
            set {
                crop = value;
                RaisePropertyChanged("Crop");
            }
        }

        public int CropYearChangeOccurred {
            get { return cropYearChangeOccurred; }
            set {
                cropYearChangeOccurred = value;
                RaisePropertyChanged("CropYearChangeOccurred");
            }
        }

        public IList<IDomainCommand> CommandList {
            get { return commandlist; }
            set { commandlist = value; }
        }

        public IList<CropZoneTreeItemViewModel> Nodelist {
            get { return nodelist; }
            set { nodelist = value; }
        }

        public void MakePreviewShape()
        {
            if (!string.IsNullOrEmpty(this.mapData))
            {
                this.shapePreview = WpfTransforms.CreatePathFromMapData(new[] { this.mapData }, 55);
            }
        }
    }
}
