﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GalaSoft.MvvmLight;
using ThinkGeo.MapSuite.WpfDesktopEdition;
using ThinkGeo.MapSuite.Core;
using Landdb.Infrastructure;
using System.IO;
using Microsoft.Win32;
using GalaSoft.MvvmLight.Command;
using System.Windows.Input;
using System.Collections.ObjectModel;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure.Messages;
using Landdb.Domain.ReadModels.Tree;
using Landdb.MapStyles;
using Landdb.Client.Infrastructure;
using System.Windows.Threading;
using Landdb.ViewModel.Maps.ImageryOverlay;
using System.Drawing;
using Landdb.ViewModel.Maps;
using NLog;
using System.Net;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using AgC.UnitConversion;
using Landdb.Resources;
using Landdb.ViewModel.Fields.FieldDetails.MapModels;

namespace Landdb.ViewModel.Secondary.Map {
    public class SSurgoNrcsWfsViewModel : ViewModelBase
    {
        Logger log = LogManager.GetCurrentClassLogger();
        bool maploaded = false;
        bool startedOutOnline = false;
        //IMapOverlay selectedoverlay;
        IMapFilter selectedfilter = null;
        bool ismousedown = false;
        bool draglabel = true;
        bool hasunsavedchanges = false;
        bool isfieldselected = false;
        string fieldname = string.Empty;
        double totalarea = 0;
        double desiredarea = 100;
        double oldarea = 0;
        double scalepercent = 0;
        double pointpercentage = 100;
        IList<MapItem> clientLayerItems;
        InMemoryFeatureLayer fieldsLayer = new InMemoryFeatureLayer();
        InMemoryFeatureLayer ssurgoLayer = new InMemoryFeatureLayer();
        InMemoryFeatureLayer cookiecutLayer = new InMemoryFeatureLayer();
        WfsFeatureLayer wfsFeatureLayer = new WfsFeatureLayer();
        LayerOverlay layerOverlay = new LayerOverlay();
        LayerOverlay ssurgoOverlay = new LayerOverlay();
        LayerOverlay cookiecutOverlay = new LayerOverlay();
        LegendAdornmentLayer legendLayer = new LegendAdornmentLayer();
        AdornmentOverlay adornmentOverlay = new AdornmentOverlay();
        Proj4Projection projection;
        RectangleShape previousmapextent = new RectangleShape();
        Feature selectedfield = new Feature();
        GeoCollection<Feature> selectedFeatures;
        Feature loadedfeature = new Feature();
        BaseShape fieldshape = new MultipolygonShape();
        ClientLayerAreaStyle fieldsLayerAreaStyle = null;
        SSurgoMultiAreaStyle ssurgoLayerAreaStyle = null;
        SSurgoMultiAreaStyle cookiecutLayerAreaStyle = null;
        Dictionary<string, double> dictarea = new Dictionary<string, double>();
        Dictionary<string, double> dictpercent = new Dictionary<string, double>();
        Dictionary<string, double> surgoareas = new Dictionary<string, double>();
        private Dictionary<string, AreaStyle> colorstyles = new Dictionary<string, AreaStyle>();
        ScalingTextStyle scalingTextStyle3 = new ScalingTextStyle(GeographyUnit.DecimalDegree, @"Musym", 19000, 1200, 12, 4);
        ScalingTextStyle scalingTextStyle4 = new ScalingTextStyle(GeographyUnit.DecimalDegree, @"Musym", 19000, 1200, 12, 4);
        ObservableCollection<ColorItem> coloritems = new ObservableCollection<ColorItem>();
        ObservableCollection<ColorItem> cookiecutcoloritems = new ObservableCollection<ColorItem>();
        Dictionary<string, System.Drawing.Color> cropzonecolors = new Dictionary<string, System.Drawing.Color>();
        IList<System.Drawing.Color> usedcolors = new List<System.Drawing.Color>();
        bool showthetoggle = true;
        bool isPopupScreen = false;
        ColorItem selectedcoloritem = null;
        int colormultiplier = 50;
        int randomchoice = 5;
        readonly IClientEndpoint clientEndpoint;
        Random rand = new Random();
        int cmgopacity = 255;
        IMapOverlay selectedoverlay;
        ZoomLevelFactory zoomlevelfactory = new ZoomLevelFactory();
        MapSettings mapsettings;
        string areaUnitString = string.Empty;
        string coordinateformat = string.Empty;
        AgC.UnitConversion.Area.AreaUnit agcAreaUnit;
        ThinkGeo.MapSuite.Core.AreaUnit mapAreaUnit;
        ThinkGeo.MapSuite.Core.DistanceUnit mapDistanceUnit;

        public SSurgoNrcsWfsViewModel(IClientEndpoint clientEndpoint)
        {
            Messenger.Default.Register<ShowSSurgoMessage>(this, OnShowSSurgoNrcsView);

            this.clientEndpoint = clientEndpoint;
            SelectedFilter = new SSurgoItem(this, clientEndpoint);
            selectedFeatures = new GeoCollection<Feature>();
            InitializeSettings();
            
            if (mapsettings.MapInfoSelectorItem.IsBing()) {
                selectedoverlay = new BingMapOverlay();
            }
            else {
                selectedoverlay = new BingRoadMapsOverlay();
            }
            InitializeMap();
            PointPercentage = 100;
            HasUnsavedChanges = false;
            isPopupScreen = false;

            CompleteCommand = new RelayCommand(Complete);
            CancelCommand = new RelayCommand(Cancel);
            ExportCommand = new RelayCommand(ExportSelectedShapeFiles);
            PrintMapCommand = new RelayCommand(Print);
            SaveStateCommand = new RelayCommand(SaveSelectedLayerState);
        }

        public ICommand CompleteCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }
        public ICommand ExportCommand { get; private set; }
        public ICommand SaveStateCommand { get; private set; }
        public ICommand PrintMapCommand { get; private set; }

        public WpfMap Map { get; set; }

        public IList<MapItem> ClientLayer
        {
            get { return clientLayerItems; }
            set
            {
                clientLayerItems = value;
                RaisePropertyChanged("ClientLayer");
            }
        }

        public Feature SelectedField
        {
            get { return selectedfield; }
            set
            {
                if (selectedfield == value) { return; }
                selectedfield = value;
                RaisePropertyChanged("SelectedField");
            }
        }

        public GeoCollection<Feature> SelectedFeatures {
            get { return selectedFeatures; }
            set {
                selectedFeatures = value;
                RaisePropertyChanged("SelectedFeatures");
                if (maploaded) {
                    this.mapsettings = clientEndpoint.GetMapSettings();
                    //cmgopacity = mapsettings.UnselectedOpacity;
                    cmgopacity = mapsettings.FieldsOpacity;
                    if (!mapsettings.MapInfoSelectorItem.Equals(selectedoverlay.MapInfoSelectorItem)) {
                        if (mapsettings.MapInfoSelectorItem.IsBing()) {
                            selectedoverlay = new BingMapOverlay();
                        }
                        else {
                            selectedoverlay = new BingRoadMapsOverlay();
                        }
                        LoadBingMapsOverlay();
                    }
                    OpenAndLoadaShapefile();
                    RetrieveSSurgoData();
                }
                if (Map.Overlays.Contains("cookiecutOverlay")) {
                    scalingTextStyle4.MapScale = Map.CurrentScale;
                    Map.Overlays["cookiecutOverlay"].Refresh();
                }
            }
        }

        public IMapFilter SelectedFilter {
            get { return selectedfilter; }
            set {
                if (selectedfilter == value) { return; }
                selectedfilter = value;
                RaisePropertyChanged("SelectedFilter");
            }
        }

        public ObservableCollection<ColorItem> ColorItems {
            get { return coloritems; }
            set {
                if (coloritems == value) { return; }
                coloritems = value;
                //OnColorUpdated();
                RaisePropertyChanged("ColorItem");
                RaisePropertyChanged("ColorItems");
                RaisePropertyChanged("CropzoneColors");
            }
        }

        public ColorItem SelectedColorItem {
            get { return selectedcoloritem; }
            set {
                if (selectedcoloritem == value) { return; }
                selectedcoloritem = value;
                RaisePropertyChanged("SelectedColorItem");
                RaisePropertyChanged("ColorItems");
                RaisePropertyChanged("CropzoneColors");
            }
        }

        public Dictionary<string, AreaStyle> ColorStyles {
            get { return colorstyles; }
            set {
                if (colorstyles == value) { return; }
                colorstyles = value;
                ssurgoLayerAreaStyle.ColorStyles = colorstyles;
                cookiecutLayerAreaStyle.ColorStyles = colorstyles;
                //scalingTextStyle.ColorStyles = colorstyles;
                RefreshFieldsLayer();
                RaisePropertyChanged("ColorStyles");
            }
        }

        public Dictionary<string, System.Drawing.Color> CropzoneColors {
            get { return cropzonecolors; }
            set {
                cropzonecolors = value;
                Dictionary<string, AreaStyle> cstyles = new Dictionary<string, AreaStyle>();
                foreach (var colr in cropzonecolors) {
                    GeoColor gc = new GeoColor(this.mapsettings.UnselectedOpacity, colr.Value.R, colr.Value.G, colr.Value.B);
                    GeoColor blackColor = new GeoColor(this.mapsettings.UnselectedOpacity, GeoColor.StandardColors.Black);
                    AreaStyle astyle = new AreaStyle(new GeoPen(blackColor, 2), new GeoSolidBrush(gc));
                    cstyles.Add(colr.Key, astyle);
                }
                ColorStyles = cstyles;
                RaisePropertyChanged("CropzoneColors");
            }
        }

        public string FieldName
        {
            get { return fieldname; }
            set
            {
                if (fieldname == value) { return; }
                fieldname = value;
                RaisePropertyChanged("FieldName");
            }
        }

        public bool HasUnsavedChanges
        {
            get { return hasunsavedchanges; }
            set
            {
                if (hasunsavedchanges == value) { return; }
                hasunsavedchanges = value;
                RaisePropertyChanged("HasUnsavedChanges");
            }
        }

        public RectangleShape PreviousMapExtent
        {
            get { return previousmapextent; }
            set
            {
                if (previousmapextent == value) { return; }
                previousmapextent = value;
                RaisePropertyChanged("PreviousMapExtent");
            }
        }

        public double DesiredArea
        {
            get { return desiredarea; }
            set
            {
                if (desiredarea == value) { return; }
                desiredarea = value;
                RaisePropertyChanged("DesiredArea");
            }
        }

        public double OldArea
        {
            get { return oldarea; }
            set
            {
                if (oldarea == value) { return; }
                oldarea = value;
                RaisePropertyChanged("OldArea");
            }
        }

        public double ScalePercent
        {
            get { return scalepercent; }
            set
            {
                if (scalepercent == value) { return; }
                scalepercent = value;
                RaisePropertyChanged("ScalePercent");
            }
        }

        public double PointPercentage
        {
            get { return pointpercentage; }
            set
            {
                if (pointpercentage == value) { return; }
                pointpercentage = value;
                RaisePropertyChanged("PointPercentage");
            }
        }

        public bool IsPopupScreen
        {
            get { return isPopupScreen; }
            set
            {
                if (isPopupScreen == value) { return; }
                isPopupScreen = value;
                RaisePropertyChanged("IsPopupScreen");
            }
        }

        private void InitializeSettings()
        {
            this.mapsettings = clientEndpoint.GetMapSettings();
            areaUnitString = mapsettings.MapAreaUnit;
            coordinateformat = mapsettings.DisplayCoordinateFormat;
            agcAreaUnit = AgC.UnitConversion.UnitFactory.GetUnitByName(areaUnitString) as AgC.UnitConversion.Area.AreaUnit;
            mapAreaUnit = MapUnitFactory.AreaMeasureConversion(agcAreaUnit);
            mapDistanceUnit = MapUnitFactory.AreaDistanceMeasureConversion(agcAreaUnit);
            cmgopacity = mapsettings.FieldsOpacity;

        }
        private void ProcessSelectedFilter() {
            if (fieldsLayer.InternalFeatures.Count > 0) {
                Map.CurrentExtent = fieldsLayer.GetBoundingBox();
                try {
                    scalingTextStyle4.MapScale = Map.CurrentScale;
                    scalingTextStyle4.MinimumLabelSize = mapsettings.ScalingTextMinimumSize;
                    scalingTextStyle4.MaximumLabelSize = mapsettings.ScalingTextMaximumSize;
                    Map.Refresh();
                }
                catch { }
            }
        }

        public IMapOverlay SelectedOverlay {
            get { return selectedoverlay; }
            set {
                if (selectedoverlay == value) { return; }
                if (selectedoverlay == null) { return; }
                if (string.IsNullOrWhiteSpace(value.Name)) { return; }
                selectedoverlay = value;
                RaisePropertyChanged("SelectedOverlay");
            }
        }

        void SetCMGMapType() {
            if (selectedoverlay.MapInfoSelectorItem.IsBing()) {
                selectedoverlay = new BingRoadMapsOverlay();
            }
            else {
                selectedoverlay = new BingMapOverlay();
            }
            LoadBingMapsOverlay();
            if (fieldsLayer.InternalFeatures.Count > 0) {
                scalingTextStyle4.MapScale = Map.CurrentScale;
                Map.CurrentExtent = fieldsLayer.GetBoundingBox();
                Map.Refresh();
            }
            var mapsettings = clientEndpoint.GetMapSettings();
            mapsettings.MapInfoSelectorItem = selectedoverlay.MapInfoSelectorItem;
            clientEndpoint.SaveMapSettings(mapsettings);
        }

        internal void EnsureFieldsLayerIsOpen()
        {
            if (fieldsLayer == null)
            {
                fieldsLayer = new InMemoryFeatureLayer();
            }

            if (!fieldsLayer.IsOpen)
            {
                fieldsLayer.Open();
            }

            if (!projection.IsOpen)
            {
                projection.Open();
            }
        }

        internal void EnsureSSurgoLayerIsOpen() {
            if (ssurgoLayer == null) {
                ssurgoLayer = new InMemoryFeatureLayer();
            }

            if (!ssurgoLayer.IsOpen) {
                ssurgoLayer.Open();
            }

            if (!projection.IsOpen) {
                projection.Open();
            }
        }

        internal void EnsureCookieCutLayerIsOpen() {
            if (cookiecutLayer == null) {
                cookiecutLayer = new InMemoryFeatureLayer();
            }

            if (!cookiecutLayer.IsOpen) {
                cookiecutLayer.Open();
            }

            if (!projection.IsOpen) {
                projection.Open();
            }
        }

        void InitializeMap()
        {
            Map = new WpfMap();
            startedOutOnline = Landdb.Client.Infrastructure.NetworkStatus.IsInternetAvailableFast();
            Map.MapUnit = GeographyUnit.Meter;
            Map.MapTools.PanZoomBar.Visibility = System.Windows.Visibility.Visible;
            Map.MapTools.PanZoomBar.GlobeButtonVisibility = System.Windows.Visibility.Hidden;
            Map.MapTools.PanZoomBar.VerticalAlignment = System.Windows.VerticalAlignment.Center;
            Map.MapTools.PanZoomBar.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            Map.Background = System.Windows.Media.Brushes.White;
            zoomlevelfactory = new ZoomLevelFactory();
            Map.ZoomLevelSet = zoomlevelfactory.ZoomLevelSET;

            projection = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetGoogleMapParametersString());

            Map.Loaded += (sender, e) =>
            {
                if (maploaded) { return; }
                LoadBingMapsOverlay();
                OpenAndLoadaShapefile();
                RetrieveSSurgoData();
                //BuildLegend();
                maploaded = true;
            };

            Map.CurrentScaleChanged += (sender, e) =>
            {
                scalingTextStyle3.MapScale = e.CurrentScale;
                scalingTextStyle4.MapScale = e.CurrentScale;
            };

            Map.MouseDown += (sender, e) => {
                System.Windows.Point ee = e.GetPosition(Map);
                PointShape worldpoint = Map.ToWorldCoordinate(e.GetPosition(Map));
                try {
                    Collection<Feature> clickedFeatures = cookiecutLayer.QueryTools.GetFeaturesContaining(worldpoint, ReturningColumnsType.AllColumns);
                    Collection<Feature> clickedssurgos = fieldsLayer.QueryTools.GetFeaturesContaining(worldpoint, ReturningColumnsType.AllColumns);
                    if (clickedFeatures.Count > 0) {
                        if (clickedssurgos.Count > 0) {
                            SSurgoNrcsWfsTool("", clickedFeatures[0], clickedssurgos[0]);
                        }
                        else {
                            SSurgoNrcsWfsTool("", clickedFeatures[0], new Feature());
                        }
                        //string text = "Symbol: " + clickedFeatures[0].ColumnValues["Musym"].Trim();
                        //text = text + "\r\n" + "Name: " + clickedFeatures[0].ColumnValues["Muname"].Trim();
                        //text = text + "\r\n" + "Area: " + clickedFeatures[0].ColumnValues["Muarea"].Trim() + " acres";
                        //text = text + "\r\n" + "Coverage: " + clickedFeatures[0].ColumnValues["Percover"].Trim() + "%";
                        //System.Windows.MessageBox.Show(text, "SSURGO", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Information, System.Windows.MessageBoxResult.OK, (System.Windows.MessageBoxOptions)0);
                    }
                }
                catch (Exception ex) {
                    log.ErrorException("Caught an exception while working with a ssurgo shape", ex);
                    //System.Windows.MessageBox.Show("ImportTools - Caught an exception while working with a ssurgo shape.");
                }

            };
        }

        private string DisplayLegendSelectedAreaDecimals(string label, double dictarea, double dictpercent, string unitname, int labelpad, int areapad) {
            string featdesc3 = string.Empty;
            IUnit _unit = UnitFactory.GetUnitByName(unitname);
            string decimalstring = "N" + mapsettings.AreaDecimals.ToString("N0");
            string string1 = string.Format("{0}:", label);
            string string2 = string.Format("{0} {1} ", dictarea.ToString(decimalstring), _unit.AbbreviatedDisplay);
            if (dictarea == 0) {
                string2 = string.Format("0 {0} ", _unit.AbbreviatedDisplay);
            }
            featdesc3 = string.Format("{0} {1} {2}%", string1.PadRight(labelpad), string2.PadLeft(areapad), dictpercent.ToString("N0").PadLeft(3));
            
            return featdesc3;
        }

        private static SizeF GetStringPixelLength(string text, string fontname, float fontsize) {
            SizeF size = new SizeF();
            using (System.Drawing.Graphics graphics = System.Drawing.Graphics.FromImage(new Bitmap(1, 1))) {
                size = graphics.MeasureString(text, new Font(fontname, fontsize, FontStyle.Regular, GraphicsUnit.Point));
            }
            return size;
        }

        private void BuildLegend() {
            log.Info("SSurgoDisplay - Build legend");
            for (int o = Map.Overlays.Count - 1; o > 0; o--) {
                if (Map.Overlays[o] is AdornmentOverlay) {
                    Map.Overlays.RemoveAt(o);
                }
            }

            float legendheight = 0;
            float baselength = 50;
            float baselegendwidth = 100;
            int padrightname = 30;
            int padrightarea = 10;
            int padrightnamemin = 5;
            int padrightnamecalc = 0;
            float baseheight = 2;
            float baselegendheight = 20;
            string _unitname = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).AbbreviatedDisplay;
            IUnit _unit = UnitFactory.GetUnitByName(_unitname);
            string decimalstring = "N" + mapsettings.AreaDecimals.ToString("N0");

            legendLayer = new LegendAdornmentLayer();
            legendLayer.BackgroundMask = AreaStyles.CreateLinearGradientStyle(new GeoColor(255, 255, 255, 255), new GeoColor(255, 230, 230, 230), 90, GeoColor.SimpleColors.Black);
            CustomLegendItem title = new CustomLegendItem();
            title.TextStyle = new TextStyle(Strings.MapLegend_Text, new GeoFont("Arial", 10, DrawingFontStyles.Bold), new GeoSolidBrush(GeoColor.SimpleColors.Black));
            SizeF TextStylelength = GetStringPixelLength(Strings.MapLegend_Text, "Arial", 10);
            title.Height = TextStylelength.Height + 5;
            legendheight += title.Height;
            title.Width = TextStylelength.Width;
            DetermineLegendWitdh(legendLayer, title);
            legendLayer.Title = title;
            int baselabelwidth = 0;
            int baseareawidth = 0;
            foreach (var ci in coloritems) {
                if (ci.Visible && dictarea[ci.Key] > 0) {
                    string string1 = string.Format("{0}: ", ci.Key);
                    int featlabellength = string1.Length;
                    if (featlabellength > baselabelwidth) {
                        baselabelwidth = featlabellength;
                    }

                    string string2 = string.Format(" {0} {1} ", dictarea[ci.Key].ToString(decimalstring), _unit.AbbreviatedDisplay);
                    int featarealength = string2.Length;
                    if (featarealength > baseareawidth) {
                        baseareawidth = featarealength;
                    }
                }
            }
            baselabelwidth += 1;
            baseareawidth += 1;
            foreach (var ci in coloritems) {
                if (ci.Visible && dictarea[ci.Key] > 0) {
                    string featdesc3 = DisplayLegendSelectedAreaDecimals(ci.Key, dictarea[ci.Key], dictpercent[ci.Key], _unitname, baselabelwidth, baseareawidth);
                    SizeF featdesclength = GetStringPixelLength(featdesc3, "Consolas", 8);
                    if ((featdesclength.Width + baselength) > baselegendwidth) {
                        baselegendwidth = featdesclength.Width + baselength;
                    }
                    if ((featdesclength.Height + baseheight) > baselegendheight) {
                        baselegendheight = featdesclength.Height + baseheight;
                    }
                }
            }

            foreach (ColorItem ci in ColorItems) {
                bool foundcookieshape = false;
                foreach (Feature feeture in cookiecutLayer.InternalFeatures) {
                    string musym = feeture.ColumnValues[@"Musym"];
                    if (ci.Key == musym) {
                        foundcookieshape = true;
                        break;
                    }
                }
                if (!foundcookieshape) { continue; }

                if (ci.Visible && dictarea[ci.Key] > 0) {
                    CustomLegendItem legendItem4 = new CustomLegendItem();
                    legendItem4.ImageStyle = AreaStyles.CreateSimpleAreaStyle(GeoColor.FromArgb(ci.MapColor.A, ci.MapColor.R, ci.MapColor.G, ci.MapColor.B));
                    string featdesc3 = string.Empty;
                    if (dictarea.ContainsKey(ci.Key)) {
                        featdesc3 = DisplayLegendSelectedAreaDecimals(ci.Key, dictarea[ci.Key], dictpercent[ci.Key], _unitname, baselabelwidth, baseareawidth);
                    }
                    else {
                        featdesc3 = ci.Key;
                    }
                    legendItem4.TextStyle = new TextStyle(featdesc3, new GeoFont("Consolas", 8), new GeoSolidBrush(GeoColor.SimpleColors.Black));
                    legendItem4.Height = baselegendheight;
                    legendheight += legendItem4.Height;
                    legendLayer.Width = baselegendwidth;
                    DetermineLegendWitdh(legendLayer, legendItem4);
                    legendLayer.LegendItems.Add(ci.Key, legendItem4);
                }
                //CustomLegendItem legendItem4 = new CustomLegendItem();
                //legendItem4.ImageStyle = AreaStyles.CreateSimpleAreaStyle(GeoColor.FromArgb(ci.MapColor.A, ci.MapColor.R, ci.MapColor.G, ci.MapColor.B));
                //string featdesc3 = string.Empty;
                //if (dictarea.ContainsKey(ci.Key)) {
                //    string string1 = string.Format("{0}:", ci.Key);
                //    string string2 = string.Format("{0}ac:", dictarea[ci.Key].ToString("N2"));
                //    string string3 = string.Format("{0}%", dictpercent[ci.Key].ToString("N0"));
                //    featdesc3 = DisplayLegendSelectedAreaDecimals(ci.Key, dictarea[ci.Key], dictpercent[ci.Key], _unitname, baselabelwidth, baseareawidth);
                //}
                //else {
                //    featdesc3 = ci.Key;
                //}
                //legendItem4.TextStyle = new TextStyle(featdesc3, new GeoFont("Consolas", 8), new GeoSolidBrush(GeoColor.SimpleColors.Black));
                //legendItem4.Height = 25;
                //legendheight += legendItem4.Height;
                //legendItem4.Width = 250;
                //DetermineLegendWitdh(legendLayer, legendItem4);
                //legendLayer.LegendItems.Add(ci.Key, legendItem4);
            }
            legendheight += 20;
            if (legendheight < 100) {
                legendheight = 100;
            }
            legendLayer.Height = legendheight;
            legendLayer.Width = baselegendwidth;
            legendLayer.ContentResizeMode = LegendContentResizeMode.Fixed;
            legendLayer.Location = AdornmentLocation.UpperLeft;
            legendLayer.YOffsetInPixel = 50;

            adornmentOverlay = new AdornmentOverlay();
            adornmentOverlay.Layers.Add("LegendLayer", legendLayer);
            Map.Overlays.Add(adornmentOverlay);
        }

        private void DetermineLegendWitdh(LegendAdornmentLayer legendLayer, CustomLegendItem legendItem) {
            SetLegendItemWidth(legendItem);

            if (legendItem.Width > legendLayer.Width) {
                legendLayer.Width = legendItem.Width;
            }
        }

        private static void SetLegendItemWidth(CustomLegendItem legendItem) {
            var fudgeFactor = 10;
            var lineWidth = (int)new GdiPlusGeoCanvas().MeasureText(legendItem.TextStyle.TextColumnName, legendItem.TextStyle.Font).Width;
            var totalImageLength = legendItem.ImageWidth + legendItem.ImageLeftPadding + legendItem.ImageRightPadding;
            var totalTextLength = legendItem.TextLeftPadding + legendItem.TextRightPadding + lineWidth;
            legendItem.Width = totalImageLength + totalTextLength + fudgeFactor;
        }

        //private void SavePolygon()
        //{
        //    EnsureFieldsLayerIsOpen();
        //    MultipolygonShape multipoly = new MultipolygonShape();
        //    Collection<Feature> selectedFeatures = fieldsLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);
        //    if (selectedFeatures.Count > 0)
        //    {
        //        for (int i = 0; i < fieldsLayer.InternalFeatures.Count; i++)
        //        {
        //            Feature feat = fieldsLayer.InternalFeatures[i];
        //            multipoly = feat.GetShape() as MultipolygonShape;
        //            break;
        //        }
        //        var importmessage = new ScalePolygonMessage() { ShapeBase = multipoly };
        //        Messenger.Default.Send<ScalePolygonMessage>(importmessage);
        //    }
        //}

        private void LoadBingMapsOverlay() {
            if (!startedOutOnline) { return; }
            IMapOverlay mapoverlay = SelectedOverlay;
            Overlay bingOverlay = mapoverlay.Overlay;
            if (bingOverlay == null) { return; }
            if (Map.Overlays.Contains("Bing")) {
                Map.Overlays.Remove("Bing");
            }
            if (Map.Overlays.Contains("Bing Online")) {
                Map.Overlays.Remove("Bing Online");
            }
            if (Map.Overlays.Contains("Roads Only")) {
                Map.Overlays.Remove("Roads Only");
            }
            if (Map.Overlays.Contains("Road Online")) {
                Map.Overlays.Remove("Road Online");
            }
            if (Map.Overlays.Contains("None")) {
                Map.Overlays.Remove("None");
            }
            //bingOverlay.IsVisible = mapoverlay.Visibility;
            //((TileOverlay)bingOverlay).TileType = TileType.SingleTile;
            Map.Overlays.Add(mapoverlay.Name, bingOverlay);
            Map.Overlays.MoveToBottom(mapoverlay.Name);
        }

        private void LoadFieldLayerShapes() {
            EnsureFieldsLayerIsOpen();
            fieldsLayer.InternalFeatures.Clear();
            foreach (Feature featur in SelectedFeatures) {
                fieldsLayer.InternalFeatures.Add(featur);
            }
            if (Map.Overlays.Contains("fieldOverlay")) {
                Map.Overlays["fieldOverlay"].Refresh();
            }
        }

        private void OpenAndLoadaShapefile() {
            PointPercentage = 100;
            HasUnsavedChanges = false;
            fieldsLayer.InternalFeatures.Clear();
            CreateInMemoryLayerAndOverlay();
            LoadShapeFileOverlayOntoMap();
            if (Map.Overlays.Contains("importOverlay")) {
                if (((LayerOverlay)Map.Overlays["importOverlay"]).Layers.Contains("ImportLayer")) {
                    Map.Refresh();
                }
            }
        }

        private void CreateInMemoryLayerAndOverlay() {
            layerOverlay = new LayerOverlay();
            layerOverlay.DrawingExceptionMode = DrawingExceptionMode.DrawException;
            fieldsLayer = new InMemoryFeatureLayer();

            //fieldsLayer
            fieldsLayer.FeatureSource.Open();
            FeatureSourceColumn column = new FeatureSourceColumn(@"LdbLabel", DbfColumnType.Character.ToString(), 64);
            fieldsLayer.Columns.Add(column);
            fieldsLayer.FeatureSource.Close();
            fieldsLayerAreaStyle = new ClientLayerAreaStyle();
            fieldsLayerAreaStyle.SelectedAreaStyle = new AreaStyle(new GeoPen(GeoColor.SimpleColors.PastelYellow, 2), new GeoSolidBrush(new GeoColor(75, GeoColor.SimpleColors.PastelYellow)));
            fieldsLayerAreaStyle.UnselectedAreaStyle = new AreaStyle(new GeoPen(GeoColor.StandardColors.LightGray, 2), new GeoSolidBrush(new GeoColor(75, GeoColor.StandardColors.LightGray)));
            fieldsLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(fieldsLayerAreaStyle);
            //fieldsLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(scalingTextStyle);
            fieldsLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            fieldsLayer.FeatureSource.Projection = projection;
            fieldsLayerAreaStyle.SelectedFeatures = new List<Feature>();
            fieldsLayerAreaStyle.HiddenFeatures = new List<Feature>();
            layerOverlay.Layers.Add("ImportLayer", fieldsLayer);
        }

        private void LoadShapeFileOverlayOntoMap() {
            if (Map.Overlays.Contains("importOverlay")) {
                Map.Overlays.Remove("importOverlay");
            }
            if (layerOverlay.Layers.Contains("ImportLayer")) {
                Map.Overlays.Add("importOverlay", layerOverlay);
            }

            EnsureFieldsLayerIsOpen();
            var extent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
            try {
                foreach (Feature featur in SelectedFeatures) {
                    //if (item.MapData != null && !string.IsNullOrEmpty(item.MapData) && item.MapData != "MULTIPOLYGON EMPTY")

                        fieldsLayer.InternalFeatures.Add(featur);
                }
                var shapelayerfeatures = fieldsLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);
                if (shapelayerfeatures.Count > 0) {
                    var shapelayercolumns = fieldsLayer.FeatureSource.GetColumns();
                    extent = ExtentHelper.GetBoundingBoxOfItems(shapelayerfeatures);
                    extent.ScaleUp(20);
                    Map.CurrentExtent = extent;
                    //PointShape center = extent.GetCenterPoint();
                    //Map.CenterAt(center);
                }
                else {
                    Map.CurrentExtent = extent;
                    //PointShape center = extent.GetCenterPoint();
                    //Map.CenterAt(center);
                }
            }
            catch {
                Map.CurrentExtent = extent;
                //PointShape center = extent.GetCenterPoint();
                //Map.CenterAt(center);
            }
        }

        private void RetrieveSSurgoData() {
            PointPercentage = 100;
            HasUnsavedChanges = false;
            ssurgoLayer.InternalFeatures.Clear();
            HandleWFSLayer();
            CreateSSurgoInMemoryLayerAndOverlay();
            CreateCookieCutInMemoryLayerAndOverlay();
            cookiecutcoloritems = new ObservableCollection<ColorItem>();
            LoadSSurgpOverlayOntoMap();
            CutOutLayer();
            log.Info("SSurgoDisplay - Order color items");
            SelectedFilter.ColorItems = new ObservableCollection<ColorItem>(cookiecutcoloritems.OrderBy(x => x.Key));
            BuildLegend();
            if (Map.Overlays.Contains("cookiecutOverlay")) {
                if (((LayerOverlay)Map.Overlays["cookiecutOverlay"]).Layers.Contains("cookiecutLayer")) {
                    log.Info("SSurgoDisplay - Refresh map");
                    Map.Refresh();
                }
            }
        }

        private void HandleWFSLayer() {
            log.Info("SSurgoDisplay - Set up wfs layer");
            //string wfscapabilityes = WfsFeatureLayer.GetCapabilities(@"http://SDMDataAccess.nrcs.usda.gov/Spatial/SDMWGS84Geographic.wfs");
            //Collection<string> wfslayers = WfsFeatureLayer.GetLayers(@"http://SDMDataAccess.nrcs.usda.gov/Spatial/SDMWGS84Geographic.wfs");
            //string wfscapabilityes = WfsFeatureLayer.GetCapabilities(@"http://SDMDataAccess.cs.egov.usda.gov/Spatial/SDMWGS84Geographic.wfs");
            //Collection<string> wfslayers = WfsFeatureLayer.GetLayers(@"http://SDMDataAccess.cs.egov.usda.gov/Spatial/SDMWGS84Geographic.wfs");

            //string wfscapabilityes = WfsFeatureLayer.GetCapabilities(@"http://sdmdataaccess.sc.egov.usda.gov/Spatial/SDMWGS84Geographic.wfs");
            //http://sdmdataaccess.sc.egov.usda.gov/Spatial/SDMWGS84Geographic.wfs?SERVICE=WFS&VERSION=1.1.0&REQUEST=GetCapabilities

            //wfsFeatureLayer = new WfsFeatureLayer(@"http://SDMDataAccess.nrcs.usda.gov/Spatial/SDMWGS84Geographic.wfs", "MapunitPoly");
            //wfsFeatureLayer = new WfsFeatureLayer(@"http://SDMDataAccess.nrcs.usda.gov/Spatial/SDMWGS84Geographic.wfs", "MapunitPolyExtended");
            //wfsFeatureLayer = new WfsFeatureLayer(@"http://SDMDataAccess.nrcs.usda.gov/Spatial/SDMWGS84Geographic.wfs", "MapunitPolyNoGeometry");
            //wfsFeatureLayer = new WfsFeatureLayer(@"http://SDMDataAccess.nrcs.usda.gov/Spatial/SDMWGS84Geographic.wfs", "SurveyAreaPoly");

            wfsFeatureLayer = new WfsFeatureLayer(@"http://SDMDataAccess.nrcs.usda.gov/Spatial/SDMWGS84Geographic.wfs", "MapunitPolyExtended");
            //wfsFeatureLayer = new WfsFeatureLayer(@"http://SDMDataAccess.cs.egov.usda.gov/Spatial/SDMWGS84Geographic.wfs", "MapunitPolyExtended");
            wfsFeatureLayer.Open();
            wfsFeatureLayer.FeatureSource.Open();
            System.Collections.ObjectModel.Collection<FeatureSourceColumn> columns = wfsFeatureLayer.FeatureSource.GetColumns();
            wfsFeatureLayer.SendingWebRequest += (sender, e) => {
                Uri uri = e.WebRequest.RequestUri;
                string uristring = uri.AbsoluteUri;
                if (uristring.Contains("SDMWGS84Geographic") && uristring.Contains("GetFeature")) {
                    string[] uriarray = uristring.Split(new string[] { @"propertyname=" }, StringSplitOptions.None);
                    string array1 = uriarray[0];
                    string array2 = uriarray[1];
                    string uristring2 = string.Format("{0}propertyname=({1})", array1, array2);
                    Uri uricustom = new Uri(uristring2);
                    System.Net.WebRequest wr = System.Net.WebRequest.Create(uricustom);
                    e.WebRequest = wr;
                }
            };

            WfsFeatureSource featureSource = wfsFeatureLayer.FeatureSource as WfsFeatureSource;
            featureSource.RequestingData += (sender, e) => {
                string servicurl = e.ServiceUrl;
                string xmlrespons = e.XmlResponse;
            };
            featureSource.RequestedData += (sender, e) => {
                string servicurl = e.ServiceUrl;
                string xmlrespons = e.XmlResponse;
            };
            featureSource.SendingWebRequest += (sender, e) => {
                string absoluturi = e.WebRequest.RequestUri.AbsoluteUri;
            };
            featureSource.SentWebRequest += (sender, e) => {
                string absoluturi = e.Response.ResponseUri.AbsoluteUri;
            };
        }

        private void CreateSSurgoInMemoryLayerAndOverlay() {
            ssurgoOverlay = new LayerOverlay();
            ssurgoOverlay.DrawingExceptionMode = DrawingExceptionMode.DrawException;
            ssurgoLayer = new InMemoryFeatureLayer();
            ssurgoLayer.FeatureSource.Open();
            FeatureSourceColumn column1 = new FeatureSourceColumn(@"Areasym", DbfColumnType.Character.ToString(), 64);
            FeatureSourceColumn column2 = new FeatureSourceColumn(@"Spatver", DbfColumnType.Character.ToString(), 64);
            FeatureSourceColumn column3 = new FeatureSourceColumn(@"Musym", DbfColumnType.Character.ToString(), 64);
            FeatureSourceColumn column4 = new FeatureSourceColumn(@"Natmusym", DbfColumnType.Character.ToString(), 64);
            FeatureSourceColumn column5 = new FeatureSourceColumn(@"Mukey", DbfColumnType.Character.ToString(), 64);
            FeatureSourceColumn column6 = new FeatureSourceColumn(@"Muname", DbfColumnType.Character.ToString(), 64);
            FeatureSourceColumn column7 = new FeatureSourceColumn(@"Mupoly", DbfColumnType.Character.ToString(), 64);
            FeatureSourceColumn column8 = new FeatureSourceColumn(@"Muarea", DbfColumnType.Character.ToString(), 64);
            FeatureSourceColumn column9 = new FeatureSourceColumn(@"Descrip", DbfColumnType.Character.ToString(), 128);
            FeatureSourceColumn column10 = new FeatureSourceColumn(@"Descarea", DbfColumnType.Character.ToString(), 128);
            FeatureSourceColumn column11 = new FeatureSourceColumn(@"Percover", DbfColumnType.Character.ToString(), 64);
            FeatureSourceColumn column12 = new FeatureSourceColumn(@"TreeID", DbfColumnType.Character.ToString(), 64);
            FeatureSourceColumn column13 = new FeatureSourceColumn(@"RecID", DbfColumnType.IntegerInBinary.ToString(), 64);
            ssurgoLayer.Columns.Add(column1);
            ssurgoLayer.Columns.Add(column2);
            ssurgoLayer.Columns.Add(column3);
            ssurgoLayer.Columns.Add(column4);
            ssurgoLayer.Columns.Add(column5);
            ssurgoLayer.Columns.Add(column6);
            ssurgoLayer.Columns.Add(column7);
            ssurgoLayer.Columns.Add(column8);
            ssurgoLayer.Columns.Add(column9);
            ssurgoLayer.Columns.Add(column10);
            ssurgoLayer.Columns.Add(column11);
            ssurgoLayer.Columns.Add(column12);
            ssurgoLayer.Columns.Add(column13);
            ssurgoLayer.FeatureSource.Close();
            ssurgoLayerAreaStyle = new SSurgoMultiAreaStyle();
            ssurgoLayerAreaStyle.ColumnsList = new List<string>() { @"Areasym", @"Spatver", @"Musym", @"Natmusym", @"Mukey", @"Muname", @"Mupoly", @"Descrip", @"Descarea", @"Percover", @"TreeID", @"RecID" };
            ssurgoLayerAreaStyle.SelectedAreaStyle = new AreaStyle(new GeoPen(GeoColor.StandardColors.Yellow, 4), new GeoSolidBrush(GeoColor.StandardColors.Blue));
            ssurgoLayerAreaStyle.UnselectedAreaStyle = new AreaStyle(new GeoPen(GeoColor.StandardColors.Red, 2), new GeoSolidBrush(new GeoColor(20, GeoColor.StandardColors.LightGray)));
            ssurgoLayerAreaStyle.ColorStyles = colorstyles;
            ssurgoLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(ssurgoLayerAreaStyle);
            scalingTextStyle3.FittingPolygon = true;
            scalingTextStyle3.LabelAllPolygonParts = true;
            scalingTextStyle3.PolygonLabelingLocationMode = PolygonLabelingLocationMode.Centroid;
            scalingTextStyle3.FittingPolygonLabel = true;
            ssurgoLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(scalingTextStyle3);
            ssurgoLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            ssurgoLayer.FeatureSource.Projection = projection;
        }

        private void LoadSSurgpOverlayOntoMap() {
            log.Info("SSurgoDisplay - Load SSurgo Layer");
            if (Map.Overlays.Contains("cookiecutOverlay")) {
                Map.Overlays.Remove("cookiecutOverlay");
            }
            if (cookiecutOverlay.Layers.Contains("cookiecutLayer")) {
                Map.Overlays.Add("cookiecutOverlay", cookiecutOverlay);
            }

            SelectedFilter.Execute(clientEndpoint, rand);
            EnsureSSurgoLayerIsOpen();
            EnsureCookieCutLayerIsOpen();
            try {
                surgoareas = new Dictionary<string, double>(); 
                var shapelayerfeatures = fieldsLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);
                if (shapelayerfeatures.Count > 0) {
                    var shapelayercolumns = fieldsLayer.FeatureSource.GetColumns();
                    RectangleShape wfsextent = ExtentHelper.GetBoundingBoxOfItems(shapelayerfeatures);
                    double extentarea = wfsextent.GetArea(GeographyUnit.Meter, mapAreaUnit);
                    if (extentarea > 100000) {
                        wfsextent = map_ScaleExtent(wfsextent);
                    }
                    RectangleShape wfsextentinternal = projection.ConvertToInternalProjection(wfsextent) as RectangleShape;
                    System.Collections.ObjectModel.Collection<Feature> featuressss = wfsFeatureLayer.FeatureSource.GetFeaturesInsideBoundingBox(wfsextentinternal, ReturningColumnsType.AllColumns);
                    //System.Collections.ObjectModel.Collection<Feature> featuress = wfsFeatureLayer.QueryTools.GetFeaturesWithinDistanceOf(wfsextentinternal, GeographyUnit.DecimalDegree, DistanceUnit.Feet, 100, ReturningColumnsType.AllColumns);
                    //System.Collections.ObjectModel.Collection<Feature> featurestouching = wfsFeatureLayer.QueryTools.GetFeaturesTouching(wfsextentinternal, ReturningColumnsType.AllColumns);
                    //System.Collections.ObjectModel.Collection<Feature> featuresss = wfsFeatureLayer.FeatureSource.GetAllFeatures(ReturningColumnsType.NoColumns);
                    //System.Collections.ObjectModel.Collection<Feature> featuresoverlapping = wfsFeatureLayer.QueryTools.GetFeaturesOverlapping(extentpoly, new string[] { "musym" });
                    //System.Collections.ObjectModel.Collection<Feature> featurescrossing = wfsFeatureLayer.QueryTools.GetFeaturesCrossing(extentpoly, new string[] { "musym" });
                    //System.Collections.ObjectModel.Collection<Feature> featuresintersecting = wfsFeatureLayer.QueryTools.GetFeaturesIntersecting(extentpoly, new string[] { "musym" });
                    //System.Collections.ObjectModel.Collection<Feature> featuresss = wfsFeatureLayer.FeatureSource.GetAllFeatures(ReturningColumnsType.NoColumns);
                    //System.Collections.ObjectModel.Collection<Feature> featurescontains = wfsFeatureLayer.FeatureSource.SpatialQuery(extentpoly, QueryType.Contains, ReturningColumnsType.AllColumns);
                    //System.Collections.ObjectModel.Collection<Feature> featurescrosses = wfsFeatureLayer.FeatureSource.SpatialQuery(extentpoly, QueryType.Crosses, ReturningColumnsType.AllColumns);
                    //System.Collections.ObjectModel.Collection<Feature> featuresdisjont = wfsFeatureLayer.FeatureSource.SpatialQuery(extentpoly, QueryType.Disjoint, ReturningColumnsType.AllColumns);
                    //System.Collections.ObjectModel.Collection<Feature> featuresintersects = wfsFeatureLayer.FeatureSource.SpatialQuery(extentpoly, QueryType.Intersects, new string[] { "musym" });
                    //System.Collections.ObjectModel.Collection<Feature> featuresoverlaps = wfsFeatureLayer.FeatureSource.SpatialQuery(extentpoly, QueryType.Overlaps, new string[] { "musym" });
                    //System.Collections.ObjectModel.Collection<Feature> featurestopo = wfsFeatureLayer.FeatureSource.SpatialQuery(extentpoly, QueryType.TopologicalEqual, ReturningColumnsType.AllColumns);
                    //System.Collections.ObjectModel.Collection<Feature> featurestouches = wfsFeatureLayer.FeatureSource.SpatialQuery(extentpoly, QueryType.Touches, new string[] { "musym" });
                    //System.Collections.ObjectModel.Collection<Feature> featureswithin = wfsFeatureLayer.FeatureSource.SpatialQuery(extentpoly, QueryType.Within, ReturningColumnsType.AllColumns);

                    foreach (Feature feat in featuressss) {
                        string featkey = feat.ColumnValues["musym"];
                        string featname = feat.ColumnValues["muname"];
                        string featdesc = string.Format("{0}: {1}", featkey, featname);
                        feat.ColumnValues.Add("description", featdesc);
                        BaseShape featureshape = feat.GetShape();
                        MultipolygonShape multipolyshape = new MultipolygonShape();
                        if (featureshape is MultipolygonShape) {
                            multipolyshape = featureshape as MultipolygonShape;
                        }
                        else {
                            PolygonShape polyshape = featureshape as PolygonShape;
                            multipolyshape.Polygons.Add(polyshape);
                        }
                        double featarea = multipolyshape.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
                        feat.ColumnValues.Add("muareaacres", featarea.ToString("N5"));
                        string featdesc2 = string.Format("{0}: {1}: {2}", featkey, featname, featarea.ToString("N2"));
                        feat.ColumnValues.Add("descriptionarea", featdesc);
                        Feature newfeature = new Feature(featureshape);
                        newfeature.ColumnValues.Add(@"Areasym", feat.ColumnValues[@"areasymbol"]);
                        newfeature.ColumnValues.Add(@"Spatver", feat.ColumnValues[@"spatialversion"]);
                        newfeature.ColumnValues.Add(@"Musym", feat.ColumnValues[@"musym"]);
                        newfeature.ColumnValues.Add(@"Natmusym", feat.ColumnValues[@"nationalmusym"]);
                        newfeature.ColumnValues.Add(@"Mukey", feat.ColumnValues[@"mukey"]);
                        newfeature.ColumnValues.Add(@"Muname", feat.ColumnValues[@"muname"]);
                        newfeature.ColumnValues.Add(@"Mupoly", feat.ColumnValues[@"mupolygonkey"]);
                        newfeature.ColumnValues.Add(@"Muarea", feat.ColumnValues[@"muareaacres"]);
                        newfeature.ColumnValues.Add(@"Descrip", featdesc);
                        newfeature.ColumnValues.Add(@"Descarea", feat.ColumnValues[@"descriptionarea"]);
                        //newfeature.ColumnValues.Add(@"Percentcoverage", feat.ColumnValues[@"percentcoverage"]);
                        //newfeature.ColumnValues.Add(@"TreeID", feat.ColumnValues[@"treeID"]);
                        ssurgoLayer.InternalFeatures.Add(newfeature);

                        if (surgoareas.ContainsKey(featkey)) {
                            surgoareas[featkey] += featarea;
                        }
                        else {
                            surgoareas.Add(featkey, featarea);
                        }

                        //IdentifySSurgoColors(featkey, featdesc);
                    }
                    //SelectedFilter.ColorItems = new ObservableCollection<ColorItem>(ColorItems.OrderBy(x => x.Key));
                    //Map.Refresh();
                }
            }
            catch (Exception ex) {
                log.ErrorException("Caught an exception while loading nrcs info", ex);
                //System.Windows.MessageBox.Show("ImportTools - Caught an exception while working with a simple draw shape.");
            }
        }

        private void IdentifySSurgoColors(string musym, string description) {
            System.Drawing.Color color = System.Drawing.Color.Red;
            bool colorused = true;
            while (colorused) {
                try {
                    if (usedcolors.Count > 60) {
                        colormultiplier = 25;
                        randomchoice = 10;
                    }
                    color = System.Drawing.Color.FromArgb(255, (rand.Next(0, randomchoice) * colormultiplier), (rand.Next(0, randomchoice) * colormultiplier), (rand.Next(0, randomchoice) * colormultiplier));

                    ColorItem colitem = new ColorItem(SelectedFilter, musym, description, musym, color);
                    colitem.Visible = true;
                    GeoColor gc = new GeoColor(cmgopacity, colitem.MapColor.R, colitem.MapColor.G, colitem.MapColor.B);
                    GeoColor blackColor = new GeoColor(cmgopacity, GeoColor.StandardColors.Black);
                    AreaStyle astyle = new AreaStyle(new GeoPen(blackColor, 2), new GeoSolidBrush(gc));
                    if (!colorstyles.ContainsKey(musym)) {
                        colorstyles.Add(musym, astyle);
                    }

                    if (!usedcolors.Contains(color)) {
                        usedcolors.Add(color);
                        colorused = false;
                    }
                    MapColorItem mci = new MapColorItem() { Key = colitem.Key, Label = colitem.Label, CropName = colitem.CropName, MapColor = colitem.MapColorInt, Visible = colitem.Visible };

                    for (int i = 0; i < coloritems.Count; i++) {
                        ColorItem ci = coloritems[i];
                        if (musym == ci.Key) {
                            colitem = ci;
                            colitem.Label = description;
                            break;
                        }
                    }
                    bool foundcoloritem = false;
                    for (int i = 0; i < cookiecutcoloritems.Count; i++) {
                        ColorItem ci = cookiecutcoloritems[i];
                        if (musym == ci.Key) {
                            cookiecutcoloritems[i].Label = description;
                            foundcoloritem = true;
                            break;
                        }
                    }
                    if (!foundcoloritem) {
                        cookiecutcoloritems.Add(colitem);
                    }
                }
                catch (Exception ex) {
                    log.Error("Exception while processing ssurgo colors", ex);

                }
            }
        }

        private void CreateCookieCutInMemoryLayerAndOverlay() {
            cookiecutOverlay = new LayerOverlay();
            cookiecutOverlay.DrawingExceptionMode = DrawingExceptionMode.DrawException;
            cookiecutLayer = new InMemoryFeatureLayer();
            cookiecutLayer.FeatureSource.Open();
            FeatureSourceColumn column1 = new FeatureSourceColumn(@"Areasym", DbfColumnType.Character.ToString(), 64);
            FeatureSourceColumn column2 = new FeatureSourceColumn(@"Spatver", DbfColumnType.Character.ToString(), 64);
            FeatureSourceColumn column3 = new FeatureSourceColumn(@"Musym", DbfColumnType.Character.ToString(), 64);
            FeatureSourceColumn column4 = new FeatureSourceColumn(@"Natmusym", DbfColumnType.Character.ToString(), 64);
            FeatureSourceColumn column5 = new FeatureSourceColumn(@"Mukey", DbfColumnType.Character.ToString(), 64);
            FeatureSourceColumn column6 = new FeatureSourceColumn(@"Muname", DbfColumnType.Character.ToString(), 64);
            FeatureSourceColumn column7 = new FeatureSourceColumn(@"Mupoly", DbfColumnType.Character.ToString(), 64);
            FeatureSourceColumn column8 = new FeatureSourceColumn(@"Muarea", DbfColumnType.Character.ToString(), 64);
            FeatureSourceColumn column9 = new FeatureSourceColumn(@"Descrip", DbfColumnType.Character.ToString(), 128);
            FeatureSourceColumn column10 = new FeatureSourceColumn(@"Descarea", DbfColumnType.Character.ToString(), 128);
            FeatureSourceColumn column11 = new FeatureSourceColumn(@"Percover", DbfColumnType.Character.ToString(), 64);
            FeatureSourceColumn column12 = new FeatureSourceColumn(@"TreeID", DbfColumnType.Character.ToString(), 64);
            FeatureSourceColumn column13 = new FeatureSourceColumn(@"RecID", DbfColumnType.IntegerInBinary.ToString(), 8);
            cookiecutLayer.Columns.Add(column1);
            cookiecutLayer.Columns.Add(column2);
            cookiecutLayer.Columns.Add(column3);
            cookiecutLayer.Columns.Add(column4);
            cookiecutLayer.Columns.Add(column5);
            cookiecutLayer.Columns.Add(column6);
            cookiecutLayer.Columns.Add(column7);
            cookiecutLayer.Columns.Add(column8);
            cookiecutLayer.Columns.Add(column9);
            cookiecutLayer.Columns.Add(column10);
            cookiecutLayer.Columns.Add(column11);
            cookiecutLayer.Columns.Add(column12);
            cookiecutLayer.Columns.Add(column13);
            cookiecutLayer.FeatureSource.Close();
            cookiecutLayerAreaStyle = new SSurgoMultiAreaStyle();
            cookiecutLayerAreaStyle.ColumnsList = new List<string>() { @"Areasym", @"Spatver", @"Musym", @"Natmusym", @"Mukey", @"Muname", @"Mupoly", @"Descrip", @"Descarea", @"Percover", @"TreeID", @"RecID" };
            cookiecutLayerAreaStyle.SelectedAreaStyle = new AreaStyle(new GeoPen(GeoColor.StandardColors.Yellow, 4), new GeoSolidBrush(GeoColor.StandardColors.Blue));
            cookiecutLayerAreaStyle.UnselectedAreaStyle = new AreaStyle(new GeoPen(GeoColor.StandardColors.Red, 2), new GeoSolidBrush(new GeoColor(20, GeoColor.StandardColors.LightGray)));
            cookiecutLayerAreaStyle.ColorStyles = colorstyles;

            cookiecutLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(cookiecutLayerAreaStyle);
            scalingTextStyle4.FittingPolygon = true;
            scalingTextStyle4.LabelAllPolygonParts = true;
            scalingTextStyle4.PolygonLabelingLocationMode = PolygonLabelingLocationMode.Centroid;
            scalingTextStyle4.FittingPolygonLabel = true;
            cookiecutLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(scalingTextStyle4);
            cookiecutLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            cookiecutLayer.FeatureSource.Projection = projection;
            cookiecutOverlay.Layers.Add("cookiecutLayer", cookiecutLayer);
        }

        private RectangleShape map_ScaleExtent(RectangleShape shape4) {
            double shapearea101 = 0;
            PolygonShape poly = shape4.ToPolygon();
            PolygonShape shape = new PolygonShape(poly.GetWellKnownText());
            PolygonShape shape2 = new PolygonShape(poly.GetWellKnownText());
            MultipolygonShape shape3 = new MultipolygonShape();
            if (shape != null) {
                shapearea101 = shape2.GetArea(GeographyUnit.Meter, mapAreaUnit);
                OldArea = shapearea101;

                double ndiff = -.0001;
                double pdiff = .0001;
                double counter = 0;
                double areadifference = 99900 - shapearea101;
                if (areadifference < 0) {
                    areadifference = areadifference * -1;
                }
                ScalePercent = (areadifference / OldArea) * 2;

                while (areadifference < ndiff || areadifference > pdiff) {
                    shapearea101 = shape.GetArea(GeographyUnit.Meter, mapAreaUnit);

                    if (DesiredArea > shapearea101) {
                        shape.ScaleUp(System.Convert.ToSingle(ScalePercent));
                    }
                    else {
                        shape.ScaleDown(System.Convert.ToSingle(ScalePercent));
                    }

                    shapearea101 = shape.GetArea(GeographyUnit.Meter, mapAreaUnit);
                    areadifference = DesiredArea - shapearea101;
                    if (areadifference < 0) {
                        areadifference = areadifference * -1;
                    }
                    ScalePercent = (areadifference / OldArea) * 2;

                    counter++;
                    if (counter > 1000) {
                        break;
                    }
                }
                return shape.GetBoundingBox();
            }
            return shape4;
        }

        void Complete()
        {
            HasUnsavedChanges = false;
            //SavePolygon();
        }

        void Cancel()
        {
            Map.Cursor = Cursors.Arrow;
            HasUnsavedChanges = false;
            fieldsLayer.InternalFeatures.Clear();

            if (isPopupScreen)
            {
                Close();
            } else
            {
                var importmessage = new ShapeImportMessage() { ShapeBase = new MultipolygonShape() };
                Messenger.Default.Send<ShapeImportMessage>(importmessage);
            }
        }

        public void OnColorUpdated() {
            var mapsettings = clientEndpoint.GetMapSettings();
            foreach (var ci in coloritems) {
                MapColorItem mci = new MapColorItem() { Key = ci.Key, Label = ci.Label, CropName = ci.CropName, MapColor = ci.MapColorInt, Visible = ci.Visible };
                if (mapsettings.SSurgoColors.ContainsKey(ci.Key)) {
                    mapsettings.SSurgoColors[ci.Key].MapColor = ci.MapColorInt;
                    mapsettings.SSurgoColors[ci.Key].Visible = ci.Visible;
                }
                else {
                    mapsettings.SSurgoColors.Add(ci.Key, mci);
                }
            }
            clientEndpoint.SaveMapSettings(mapsettings);
            RaisePropertyChanged("ColorItem");
            RaisePropertyChanged("ColorItems");
            RaisePropertyChanged("CropzoneColors");

        }

        public void RefreshFieldsLayer() {
            try {
                scalingTextStyle3.MapScale = Map.CurrentScale;
                scalingTextStyle4.MapScale = Map.CurrentScale;
                //scalingTextStyle.MinimumLabelSize = mapsettings.ScalingTextMinimumSize;
                //scalingTextStyle.MaximumLabelSize = mapsettings.ScalingTextMaximumSize;
                BuildLegend();

                Map.Refresh();
            }
            catch { }
        }


        public void CutOutLayer() {
            log.Info("SSurgoDisplay - Cut out layer");
            totalarea = 0;
            dictarea = new Dictionary<string,double>();
            dictpercent = new Dictionary<string,double>();
            if (fieldsLayer.InternalFeatures.Count == 0) { return; }
            double progresspercent = 0;
            int intpercent = 0;
            int nrecords = 0;
            cookiecutLayer.InternalFeatures.Clear();
            try {
                foreach (Feature feature in fieldsLayer.InternalFeatures) {
                    BaseShape baseshape = feature.GetShape();
                    MultipolygonShape fieldShape = new MultipolygonShape();
                    if (baseshape is MultipolygonShape) {
                        fieldShape = baseshape as MultipolygonShape;
                    }
                    else {
                        PolygonShape ps = baseshape as PolygonShape;
                        fieldShape.Polygons.Add(ps);
                    }
                    if (fieldShape == null) { continue; }
                    double fieldshapearea = fieldShape.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
                    totalarea += fieldshapearea;
                }
                
                foreach (Feature feature in fieldsLayer.InternalFeatures) {
                    BaseShape baseshape = feature.GetShape();
                    MultipolygonShape fieldShape = new MultipolygonShape();
                    if (baseshape is MultipolygonShape) {
                        fieldShape = baseshape as MultipolygonShape;
                    }
                    else {
                        PolygonShape ps = baseshape as PolygonShape;
                        fieldShape.Polygons.Add(ps);
                    }
                    if (fieldShape == null) { continue; }
                    double fieldshapearea = fieldShape.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
                    foreach (Feature ssurgofeature in ssurgoLayer.InternalFeatures) {
                        string shapeorientation = "SSurgo Feature";
                        try {
                            shapeorientation = "Getting SSurgo Feature";
                            BaseShape basessurgo = ssurgofeature.GetShape();
                            MultipolygonShape ssurgoShape = new MultipolygonShape();
                            if (basessurgo is MultipolygonShape) {
                                ssurgoShape = basessurgo as MultipolygonShape;
                            }
                            else {
                                PolygonShape ps = basessurgo as PolygonShape;
                                ssurgoShape.Polygons.Add(ps);
                            }
                            if (ssurgoShape == null) { continue; }
                            string featkey = ssurgofeature.ColumnValues[@"Musym"];
                            string featname = ssurgofeature.ColumnValues[@"Muname"];
                            string featdesc = string.Format("{0}: {1}", featkey, featname);
                            if (!ssurgofeature.ColumnValues.ContainsKey(@"TreeID")) {
                                ssurgofeature.ColumnValues.Add(@"TreeID", feature.Id);
                            }
                            ssurgofeature.ColumnValues[@"TreeID"] = feature.Id;
                            if (fieldShape.Contains(ssurgoShape)) {
                                shapeorientation = "Field Contains SSurgo";
                                double featarea = ssurgoShape.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
                                double percentcoverage = 0;
                                if (fieldshapearea > 0) {
                                    percentcoverage = (featarea / fieldshapearea) * 100;
                                }
                                string featdesc2 = string.Format("{0}: {1}: {2}", featkey, featname, featarea.ToString("N2"));
                                string featdesc3 = string.Format("{0}: {1}: {2}%", featkey, featarea.ToString("N2"), percentcoverage.ToString("N2"));
                                ssurgofeature.ColumnValues[@"Muarea"] = featarea.ToString("N5");
                                ssurgofeature.ColumnValues[@"Descrip"] = featdesc;
                                ssurgofeature.ColumnValues[@"Descarea"] = featdesc2;
                                ssurgofeature.ColumnValues[@"Percover"] = percentcoverage.ToString("N2"); ;
                                nrecords++;
                                ssurgofeature.ColumnValues[@"RecID"] = nrecords.ToString("N0");
                                Feature cookiecurfeature = new Feature(basessurgo, ssurgofeature.ColumnValues);
                                cookiecutLayer.InternalFeatures.Add(ssurgofeature);
                                IdentifySSurgoColors(featkey, featdesc);
                                if (dictarea.ContainsKey(featkey)) {
                                    dictarea[featkey] += featarea;
                                }
                                else {
                                    dictarea.Add(featkey, featarea);
                                }
                                double percentagearea = (dictarea[featkey] / totalarea) * 100;
                                if (dictpercent.ContainsKey(featkey)) {
                                    dictpercent[featkey] = percentagearea;
                                }
                                else {
                                    dictpercent.Add(featkey, percentagearea);
                                }
                            }
                            else if (ssurgoShape.Contains(fieldShape)) {
                                shapeorientation = "SSurgo Contains Field";
                                double featarea = fieldShape.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
                                double percentcoverage = 0;
                                if (fieldshapearea > 0) {
                                    percentcoverage = (featarea / fieldshapearea) * 100;
                                }
                                string featdesc2 = string.Format("{0}: {1}: {2}", featkey, featname, featarea.ToString("N2"));
                                string featdesc3 = string.Format("{0}: {1}: {2}%", featkey, featarea.ToString("N2"), percentcoverage.ToString("N2"));
                                ssurgofeature.ColumnValues[@"Muarea"] = featarea.ToString("N5");
                                ssurgofeature.ColumnValues[@"Descrip"] = featdesc;
                                ssurgofeature.ColumnValues[@"Descarea"] = featdesc2;
                                ssurgofeature.ColumnValues[@"Percover"] = percentcoverage.ToString("N2");
                                nrecords++;
                                ssurgofeature.ColumnValues[@"RecID"] = nrecords.ToString("N0");
                                Feature cookiecurfeature = new Feature(baseshape, ssurgofeature.ColumnValues);
                                cookiecutLayer.InternalFeatures.Add(cookiecurfeature);
                                IdentifySSurgoColors(featkey, featdesc);
                                if (dictarea.ContainsKey(featkey)) {
                                    dictarea[featkey] += featarea;
                                }
                                else {
                                    dictarea.Add(featkey, featarea);
                                }
                                double percentagearea = (dictarea[featkey] / totalarea) * 100;
                                if (dictpercent.ContainsKey(featkey)) {
                                    dictpercent[featkey] = percentagearea;
                                }
                                else {
                                    dictpercent.Add(featkey, percentagearea);
                                }
                            }
                            else if (fieldShape.Intersects(ssurgoShape)) {
                                shapeorientation = "Field and SSurgo Intersect";
                                MultipolygonShape overlapMultiPolygonShape = ssurgoShape.GetIntersection(fieldShape);
                                double featarea = overlapMultiPolygonShape.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
                                double percentcoverage = 0;
                                if (fieldshapearea > 0) {
                                    percentcoverage = (featarea / fieldshapearea) * 100;
                                }
                                string featdesc2 = string.Format("{0}: {1}: {2}", featkey, featname, featarea.ToString("N2"));
                                string featdesc3 = string.Format("{0}: {1}: {2}%", featkey, featarea.ToString("N2"), percentcoverage.ToString("N2"));
                                ssurgofeature.ColumnValues[@"Muarea"] = featarea.ToString("N5");
                                ssurgofeature.ColumnValues[@"Descrip"] = featdesc;
                                ssurgofeature.ColumnValues[@"Descarea"] = featdesc2;
                                ssurgofeature.ColumnValues[@"Percover"] = percentcoverage.ToString("N2"); ;
                                nrecords++;
                                ssurgofeature.ColumnValues[@"RecID"] = nrecords.ToString("N0");
                                Feature cookiecurfeature = new Feature(overlapMultiPolygonShape, ssurgofeature.ColumnValues);
                                cookiecutLayer.InternalFeatures.Add(cookiecurfeature);
                                IdentifySSurgoColors(featkey, featdesc);
                                if (dictarea.ContainsKey(featkey)) {
                                    dictarea[featkey] += featarea;
                                }
                                else {
                                    dictarea.Add(featkey, featarea);
                                }
                                double percentagearea = (dictarea[featkey] / totalarea) * 100;
                                if (dictpercent.ContainsKey(featkey)) {
                                    dictpercent[featkey] = percentagearea;
                                }
                                else {
                                    dictpercent.Add(featkey, percentagearea);
                                }
                            }
                        }
                        catch (Exception ex) {
                            string field = feature.ColumnValues[@"LdbLabel"];
                            string ssurgokey = ssurgofeature.ColumnValues[@"Musym"];
                            string ssurgoname = ssurgofeature.ColumnValues[@"Muname"];
                            string ssurgostring = string.Format("Exception while clipping ssurgo shapes {0}: {1}:", field, shapeorientation);
                            log.Error(ssurgoname, ex);
                        }
                    }
                }
            }
            catch (Exception ex) {
                log.Error("Exception while processing cookie cutter", ex);
            }
        }


        void ExportSelectedShapeFiles() {
            SelectedShapeExport sse = new SelectedShapeExport(clientEndpoint, projection, "SSurgo", FieldName, false);
            sse.ExportShapeSSurgoFile(cookiecutLayer);
        }

        void SaveSelectedLayerState() {
            //if (Map.Overlays.Contains("cookiecutOverlay")) {
            //    Overlay stateOverlay = Map.Overlays["cookiecutOverlay"];
            //    SelectedShapeExport sse = new SelectedShapeExport(clientEndpoint, projection, "SSurgo", FieldName, false);
            //    //sse.SaveOverlayState(stateOverlay);
            //    sse.SaveLayerState(cookiecutLayer);
            //}
            SelectedShapeExport sse = new SelectedShapeExport(clientEndpoint, projection, "SSurgo", FieldName, false);
            sse.ExportSSurgoXml(cookiecutLayer);
        }

        private void SSurgoNrcsWfsTool(string cropname, Feature cookieCutFeature, Feature ssurgoFeature) {
            SSurgoViewModel ssurgonrcswfsvm = new SSurgoViewModel();
            string mukey = cookieCutFeature.ColumnValues[@"Mukey"];
            ssurgonrcswfsvm.Execute(cropname, cookieCutFeature, ssurgoFeature, mukey);
            //ssurgonrcswfsvm.FieldName = Name;
            //ssurgonrcswfsvm.SelectedField = displayModel.SelectedFeature;
            //ssurgonrcswfsvm.SelectedFeatures = displayModel.SelectedFeatures;
            //ssurgonrcswfsvm.PreviousMapExtent = displayModel.MapExtent;
            //var scalemessage = new ChangeScreenDescriptorMessage() { ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Secondary.Map.SSurgoView", "SSurgoVM", ssurgonrcswfsvm) };
            //Messenger.Default.Send<ChangeScreenDescriptorMessage>(scalemessage);

            

            ScreenDescriptor descriptor = null;
            descriptor = new ScreenDescriptor("Landdb.Views.Secondary.Map.SSurgoView", "SSurgoVM", ssurgonrcswfsvm);
            Messenger.Default.Send<ShowPopupMessage>(new ShowPopupMessage() { ScreenDescriptor = descriptor });
        }

        void OnShowSSurgoNrcsView(ShowSSurgoMessage message) {
            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
        }
        public void Close()
        {
            //Messenger.Default.Send(new HidePopupMessage());
            Messenger.Default.Send<HideOverlayMessage>(new HideOverlayMessage());
        }

        public Bitmap MapImage { get; set; }

        void Print() {
            var name = FieldName;
            var holder = SelectedField;
            var width = Map.ActualWidth;
            var height = Map.ActualHeight;
            RenderTargetBitmap bmp = new RenderTargetBitmap((int)width, (int)height, 96, 96, PixelFormats.Pbgra32);
            bmp.Render(Map);
            PngBitmapEncoder png = new PngBitmapEncoder();
            png.Frames.Add(BitmapFrame.Create(bmp));

            try {

                MemoryStream fs = new MemoryStream();
                png.Save(fs);
                MapImage = new Bitmap(fs);
            }
            catch (Exception ex) {
                return;
            }

            //Now generate Report
            var vm = new Secondary.Reports.Map.PrintMapViewModel(clientEndpoint, App.Current.Dispatcher, MapImage, new List<Guid>(), FieldName, new FieldId());
            var sd = new ScreenDescriptor("Landdb.Views.Secondary.Reports.Work_Order.SingleWorkOrderView", "viewReport", vm);
            Messenger.Default.Send(new ShowOverlayMessage() { ScreenDescriptor = sd });
        }

    }
}
