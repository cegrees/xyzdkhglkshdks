﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GalaSoft.MvvmLight;
using ThinkGeo.MapSuite.WpfDesktopEdition;
using ThinkGeo.MapSuite.Core;
using Landdb.Infrastructure;
using System.IO;
using Microsoft.Win32;
using GalaSoft.MvvmLight.Command;
using System.Windows.Input;
using System.Collections.ObjectModel;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure.Messages;
using Landdb.Domain.ReadModels.Tree;
using Landdb.MapStyles;
using Landdb.Client.Infrastructure;
using System.Windows.Threading;
using Landdb.ViewModel.Maps.ImageryOverlay;
using System.Drawing.Printing;
using Landdb.ViewModel.Maps;
using System.Windows;
using Landdb.Views.Secondary.Map;
using System.Windows.Controls;
using System.Drawing;
using Landdb.Resources;
using PdfSharp;
using PdfSharp.Pdf;
using Landdb.ViewModel.Fields.FieldDetails.MapModels;
using NLog;

namespace Landdb.ViewModel.Secondary.Map {
    public class PrintMapToolViewModel : ViewModelBase
    {
        Logger log = LogManager.GetCurrentClassLogger();
        bool maploaded = false;
        bool startedOutOnline = false;
        bool ismousedown = false;
        bool draglabel = true;
        bool hasunsavedchanges = false;
        bool isfieldselected = false;
        string fieldname = string.Empty;
        LayerOverlay layerOverlay = new LayerOverlay();
        Proj4Projection projection;
        Feature selectedfield = new Feature();
        Feature loadedfeature = new Feature();
        BaseShape fieldshape = new MultipolygonShape();
        ClientLayerAreaStyle fieldsLayerAreaStyle = null;

        List<string> paperSizes = new List<string>();
        string selectedPaperSize = string.Empty;
        List<string> paperOrientations = new List<string>();
        string selectedPaperOrientation = string.Empty;
        List<string> paperPercentages = new List<string>();
        string selectedPaperPercentage = string.Empty;

        ScalingTextStyle scalingTextStyle1 = new ScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, 24, 6);
        AnnotationScalingTextStyle scalingTextStyle2 = new AnnotationScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, 24, 6);
        SimpleMarkerOverlay simpleMarkerOverlay = new SimpleMarkerOverlay();
        InMemoryFeatureLayer fieldsLayer = new InMemoryFeatureLayer();
        InMemoryFeatureLayer annotationLayer = new InMemoryFeatureLayer();
        RectangleShape previousmapextent = new RectangleShape();
        BingMapsMapType bingMapsMaptype = BingMapsMapType.Road;
        ObservableCollection<ColorItem> coloritems = new ObservableCollection<ColorItem>();
        ZoomLevelFactory zoomlevelfactory = new ZoomLevelFactory();

        public PrintMapToolViewModel() {

            InitializeMap();

            HasUnsavedChanges = false;

            CompleteCommand = new RelayCommand(Complete);
            CancelCommand = new RelayCommand(Cancel);
            ToBitmapCommand = new RelayCommand(ToBitmap_Click);
            ToPrintPreviewCommand = new RelayCommand(ToPrintPreview_Click);
            ToPdfCommand = new RelayCommand(ToPDF_Click);
            ToPrinterCommand = new RelayCommand(ToPrinter_Click);
            ZoomInCommand = new RelayCommand(ZoomIn_Click);
            ZoomOutCommand = new RelayCommand(ZoomOut_Click);
            PanNorthCommand = new RelayCommand(PanNorth_Click);
            PanWestCommand = new RelayCommand(PanWest_Click);
            PanSouthCommand = new RelayCommand(PanSouth_Click);
            PanEastCommand = new RelayCommand(PanEast_Click);

            AddTitleCommand = new RelayCommand(AddTitle_Click);
            AddLabelCommand = new RelayCommand(AddLabel_Click);
            AddImageCommand = new RelayCommand(AddImage_Click);
            AddScaleLineCommand = new RelayCommand(AddScaleLine_Click);
            AddScaleBarCommand = new RelayCommand(AddScaleBar_Click);
            AddDataGridCommand = new RelayCommand(AddDataGrid_Click);
        }

        public ICommand CompleteCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }
        public ICommand ToBitmapCommand { get; private set; }
        public ICommand ToPrintPreviewCommand { get; private set; }
        public ICommand ToPdfCommand { get; private set; }
        public ICommand ToPrinterCommand { get; private set; }
        public ICommand ZoomInCommand { get; private set; }
        public ICommand ZoomOutCommand { get; private set; }
        public ICommand PanNorthCommand { get; private set; }
        public ICommand PanWestCommand { get; private set; }
        public ICommand PanSouthCommand { get; private set; }
        public ICommand PanEastCommand { get; private set; }
        public ICommand AddTitleCommand { get; private set; }
        public ICommand AddLabelCommand { get; private set; }
        public ICommand AddImageCommand { get; private set; }
        public ICommand AddScaleLineCommand { get; private set; }
        public ICommand AddScaleBarCommand { get; private set; }
        public ICommand AddDataGridCommand { get; private set; }

        public WpfMap Map { get; set; }

        public ScalingTextStyle ScalingTextStyle1 {
            get { return scalingTextStyle1; }
            set { scalingTextStyle1 = value; }
        }

        public AnnotationScalingTextStyle ScalingTextStyle2 {
            get { return scalingTextStyle2; }
            set { scalingTextStyle2 = value; }
        }

        public SimpleMarkerOverlay SimpleMarkerOverlay {
            get { return simpleMarkerOverlay; }
            set { simpleMarkerOverlay = value; }
        }

        public InMemoryFeatureLayer FieldsLayer {
            get { return fieldsLayer; }
            set { fieldsLayer = value; }
        }

        public InMemoryFeatureLayer AnnotationLayer {
            get { return annotationLayer; }
            set { annotationLayer = value; }
        }

        public BingMapsMapType BingMapsMaptype {
            get { return bingMapsMaptype; }
            set { bingMapsMaptype = value; }
        }

        public RectangleShape PreviousMapExtent {
            get { return previousmapextent; }
            set { previousmapextent = value; }
        }

        public ObservableCollection<ColorItem> ColorItems {
            get { return coloritems; }
            set { coloritems = value; }
        }


        //public IList<MapItem> ClientLayer
        //{
        //    get { return clientLayerItems; }
        //    set
        //    {
        //        clientLayerItems = value;
        //        //LoadFieldLayerShapes();
        //        RaisePropertyChanged("ClientLayer");
        //    }
        //}

        //public Feature SelectedField
        //{
        //    get { return selectedfield; }
        //    set
        //    {
        //        if (selectedfield == value) { return; }
        //        selectedfield = value;
        //        RaisePropertyChanged("SelectedField");
        //        if (selectedfield.Id != loadedfeature.Id)
        //        {
        //            loadedfeature = selectedfield;
        //            fieldshape = selectedfield.GetShape();
        //            if (fieldshape is MultipolygonShape)
        //            {
        //                MultipolygonShape ps = fieldshape as MultipolygonShape;
        //            }

        //            if (maploaded)
        //            {
        //                //OpenAndLoadaShapefile();
        //            }
        //        }
        //    }
        //}

        public string FieldName
        {
            get { return fieldname; }
            set
            {
                if (fieldname == value) { return; }
                fieldname = value;
                RaisePropertyChanged("FieldName");
            }
        }

        public List<string> PaperSizes {
            get { return paperSizes; }
        }

        public string SelectedPaperSize {
            get {
                return selectedPaperSize;
            }
            set {
                if (selectedPaperSize == value) { return; }
                selectedPaperSize = value;
                if (maploaded) {
                    // When the page size combobox is changed then we update the PagePrinterLayer to reflect the changes
                    PrinterInteractiveOverlay printerInteractiveOverlay = (PrinterInteractiveOverlay)Map.InteractiveOverlays["PrintPreviewOverlay"];
                    PagePrinterLayer pagePrinterLayer = (PagePrinterLayer)printerInteractiveOverlay.PrinterLayers["PageLayer"];

                    if (selectedPaperSize.StartsWith("ANSI A"))
                        pagePrinterLayer.PageSize = PrinterPageSize.AnsiA;
                    else if (selectedPaperSize.StartsWith("ANSI B"))
                        pagePrinterLayer.PageSize = PrinterPageSize.AnsiB;
                    else if (selectedPaperSize.StartsWith("ANSI C"))
                        pagePrinterLayer.PageSize = PrinterPageSize.AnsiC;
                    else if (selectedPaperSize.StartsWith("ANSI D"))
                        pagePrinterLayer.PageSize = PrinterPageSize.AnsiD;
                    else if (selectedPaperSize.StartsWith("ANSI E"))
                        pagePrinterLayer.PageSize = PrinterPageSize.AnsiE;

                    Map.Refresh();
                }

                RaisePropertyChanged("SelectedPaperSize");
            }
        }

        public List<string> PaperOrientations {
            get { return paperOrientations; }
        }

        public string SelectedPaperOrientation {
            get {
                return selectedPaperOrientation;
            }
            set {
                if (selectedPaperOrientation == value) { return; }
                selectedPaperOrientation = value;

                if (this.maploaded) {
                    RectangleShape pageBoundingbox = GetPageBoundingBox(PrintingUnit.Inch);
                    RectangleShape mapBoundingbox = GetMapBoundingBox(PrintingUnit.Inch);
                    PrinterInteractiveOverlay printerInteractiveOverlay = (PrinterInteractiveOverlay)Map.InteractiveOverlays["PrintPreviewOverlay"];
                    PagePrinterLayer pagePrinterLayer = (PagePrinterLayer)printerInteractiveOverlay.PrinterLayers["PageLayer"];
                    MapPrinterLayer mapPrinterLayer = ((MapPrinterLayer)(printerInteractiveOverlay.PrinterLayers["MapLayer"]));
                    ImagePrinterLayer imagePrinterLayer = ((ImagePrinterLayer)(printerInteractiveOverlay.PrinterLayers["ImageLayer"]));
                    ScaleLinePrinterLayer scaleLinePrinterLayer = ((ScaleLinePrinterLayer)(printerInteractiveOverlay.PrinterLayers["ScaleLineLayer"]));
                    LabelPrinterLayer labelPrinterLayer = ((LabelPrinterLayer)(printerInteractiveOverlay.PrinterLayers["LabelLayer"]));
                    LegendPrinterLayer legendPrinterLayer = ((LegendPrinterLayer)(printerInteractiveOverlay.PrinterLayers["LegendPrinterLayer"]));

                    if (selectedPaperOrientation == "Landscape") {
                        pagePrinterLayer.Orientation = PrinterOrientation.Landscape;
                        pageBoundingbox = GetPageBoundingBox(PrintingUnit.Inch);
                        PointShape mapCenter = new PointShape();
                        mapCenter = pageBoundingbox.GetCenterPoint();
                        mapPrinterLayer.SetPosition(10.25, 6.75, mapCenter.X, mapCenter.Y - .5, PrintingUnit.Inch);
                        //mapPrinterLayer.SetPosition(10.5, 7, mapCenter.X, mapCenter.Y - .5, PrintingUnit.Inch);
                        //mapPrinterLayer.SetPosition(10, 8, mapCenter.X, mapCenter.Y, PrintingUnit.Inch);
                        //imagePrinterLayer.SetPosition(.75, .75, pageBoundingbox.GetCenterPoint().X + 5.25, pageBoundingbox.GetCenterPoint().Y - 3.50, PrintingUnit.Inch);
                        //scaleLinePrinterLayer.SetPosition(1.25, .25, pageBoundingbox.GetCenterPoint().X - 5.25, pageBoundingbox.GetCenterPoint().Y - 3.50, PrintingUnit.Inch);
                        //labelPrinterLayer.SetPosition(5, 1, labelCenter, PrintingUnit.Inch);
                    }
                    else if (selectedPaperOrientation == "Portrait") {
                        pagePrinterLayer.Orientation = PrinterOrientation.Portrait;
                        pageBoundingbox = GetPageBoundingBox(PrintingUnit.Inch);
                        PointShape mapCenter = new PointShape();
                        mapCenter = pageBoundingbox.GetCenterPoint();
                        mapPrinterLayer.SetPosition(7.75, 9.25, mapCenter.X, mapCenter.Y - .5, PrintingUnit.Inch);
                        //mapPrinterLayer.SetPosition(8, 9.5, mapCenter.X, mapCenter.Y - .5, PrintingUnit.Inch);
                        //mapPrinterLayer.SetPosition(8, 10, pageBoundingbox.GetCenterPoint().X, pageBoundingbox.GetCenterPoint().Y - 1.5, PrintingUnit.Inch);
                        //imagePrinterLayer.SetPosition(.75, .75, pageBoundingbox.GetCenterPoint().X + 3.50, pageBoundingbox.GetCenterPoint().Y - 5.25, PrintingUnit.Inch);
                        //scaleLinePrinterLayer.SetPosition(1.25, .25, pageBoundingbox.GetCenterPoint().X - 3.50, pageBoundingbox.GetCenterPoint().Y - 5.25, PrintingUnit.Inch);
                        //labelPrinterLayer.SetPosition(5, 1, labelCenter, PrintingUnit.Inch);
                    }
                    PointShape labelCenter = new PointShape();
                    labelCenter.X = pageBoundingbox.UpperRightPoint.X - pageBoundingbox.Width / 2;
                    //labelCenter.Y = pageBoundingbox.UpperLeftPoint.Y - .5;
                    labelCenter.Y = pageBoundingbox.UpperLeftPoint.Y - .75;
                    labelPrinterLayer.SetPosition(5, 1, labelCenter, PrintingUnit.Inch);

                    PointShape imagePosition = new PointShape();
                    //imagePosition.X = pageBoundingbox.LowerRightPoint.X - .65;
                    //imagePosition.Y = pageBoundingbox.LowerRightPoint.Y + .65;
                    imagePosition.X = pageBoundingbox.LowerRightPoint.X - 1.25;
                    imagePosition.Y = pageBoundingbox.LowerRightPoint.Y + 1.25;
                    imagePrinterLayer.SetPosition(.75, .75, imagePosition.X, imagePosition.Y, PrintingUnit.Inch);

                    PointShape scalePosition = new PointShape();
                    //scalePosition.X = pageBoundingbox.LowerLeftPoint.X + 1.25;
                    //scalePosition.Y = pageBoundingbox.LowerLeftPoint.Y + .75;
                    scalePosition.X = pageBoundingbox.LowerLeftPoint.X + 1.5;
                    scalePosition.Y = pageBoundingbox.LowerLeftPoint.Y + 1;
                    scaleLinePrinterLayer.SetPosition(1.25, .25, scalePosition.X, scalePosition.Y, PrintingUnit.Inch);

                    legendPrinterLayer.SetPosition(2, 1, -2.9, 3.9, PrintingUnit.Inch);

                    Map.Refresh();
                }

                RaisePropertyChanged("SelectedPaperOrientation");
            }
        }

        public List<string> PaperPercentages {
            get { return paperPercentages; }
        }

        public string SelectedPaperPercentage {
            get {
                return selectedPaperPercentage;
            }
            set {
                if (selectedPaperPercentage == value) { return; }
                selectedPaperPercentage = value;
                if (this.maploaded) {
                    String zoomString = (String)selectedPaperPercentage;
                    double currentZoom = Double.Parse(zoomString.Replace("%", ""));
                    Map.CurrentScale = PrinterHelper.GetPointsPerGeographyUnit(Map.MapUnit) / (currentZoom / 100);
                    Map.Refresh();
                }

                RaisePropertyChanged("SelectedPaperPercentage");
            }
        }

        public bool HasUnsavedChanges
        {
            get { return hasunsavedchanges; }
            set
            {
                if (hasunsavedchanges == value) { return; }
                hasunsavedchanges = value;
                RaisePropertyChanged("HasUnsavedChanges");
            }
        }

        internal void EnsureFieldsLayerIsOpen()
        {
            if (fieldsLayer == null)
            {
                fieldsLayer = new InMemoryFeatureLayer();
            }

            if (!fieldsLayer.IsOpen)
            {
                fieldsLayer.Open();
            }

            if (!projection.IsOpen)
            {
                projection.Open();
            }
        }

        void InitializeMap()
        {
            Map = new WpfMap();
            startedOutOnline = Landdb.Client.Infrastructure.NetworkStatus.IsInternetAvailableFast();
            Map.MapUnit = GeographyUnit.Meter;
            Map.MapTools.PanZoomBar.Visibility = System.Windows.Visibility.Hidden;
            Map.MapTools.PanZoomBar.GlobeButtonVisibility = System.Windows.Visibility.Hidden;
            Map.Background = System.Windows.Media.Brushes.White;
            zoomlevelfactory = new ZoomLevelFactory();
            Map.ZoomLevelSet = zoomlevelfactory.ZoomLevelSET;

            projection = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetGoogleMapParametersString());

            Map.Loaded += (sender, e) =>
            {
                if (maploaded) { return; }
                //LoadBingMapsOverlay();
                //OpenAndLoadaShapefile();

                SetupMapWithBlankPage();
                AddCenteredTitleLabel();
                AddMapLayer();
                ////AddDataGridLayer();
                AddLogoImage();
                AddScaleLineLayer();
                AddLegendLayer();
                //SetupContextMenu();
                SetupCombos();
                Map.Refresh();
                maploaded = true;
            };

            Map.CurrentScaleChanged += (sender, e) => {
                scalingTextStyle1.MapScale = e.CurrentScale;
                scalingTextStyle2.MapScale = e.CurrentScale;
                // Here we sync up the zoom combox to the map's zoom level.
                PrinterZoomLevelSet printerZoomLevelSet = (PrinterZoomLevelSet)Map.ZoomLevelSet;
                ZoomLevel currentZoomLevel = printerZoomLevelSet.GetZoomLevel(Map.CurrentExtent, Map.ActualWidth, Map.MapUnit);
                SelectedPaperPercentage = printerZoomLevelSet.GetZoomPercentage(currentZoomLevel) + "%";

                //scalingTextStyle.MapScale = e.CurrentScale;
            };

            Map.MapClick += (sender, e) => {
                 //Here we loop through all of the PrinterLayers to find the one the user right mouse clicked on.
                 //Then we show the right click menu to give the user some options

                //if (e.MouseButton == MapMouseButton.Right) {
                //    ((MenuItem)Map.ContextMenu.Items[0]).IsEnabled = false;
                //    ((MenuItem)Map.ContextMenu.Items[1]).IsEnabled = false;

                //    PrinterInteractiveOverlay printerOverlay = (PrinterInteractiveOverlay)Map.InteractiveOverlays["PrintPreviewOverlay"];
                //    for (int i = printerOverlay.PrinterLayers.Count - 1; i >= 0; i--) {
                //        if (printerOverlay.PrinterLayers[i].GetType() != typeof(PagePrinterLayer)) {
                //            RectangleShape boundingBox = printerOverlay.PrinterLayers[i].GetPosition();
                //            if (boundingBox.Contains(e.WorldLocation)) {
                //                ((MenuItem)Map.ContextMenu.Items[0]).Tag = printerOverlay.PrinterLayers[i];
                //                ((MenuItem)Map.ContextMenu.Items[0]).IsEnabled = true;

                //                ((MenuItem)Map.ContextMenu.Items[1]).Tag = printerOverlay.PrinterLayers[i];
                //                ((MenuItem)Map.ContextMenu.Items[1]).IsEnabled = true;

                //                break;
                //            }
                //        }
                //    }
                //}


            };
        }

        void Complete() {
            HasUnsavedChanges = false;
        }

        void Cancel() {
            Map.Cursor = Cursors.Arrow;
            HasUnsavedChanges = false;
            var importmessage = new ShapeImportMessage() { ShapeBase = new MultipolygonShape() };
            Messenger.Default.Send<ShapeImportMessage>(importmessage);
        }

        #region Add PrinterLayer Elements

        private void SetupMapWithBlankPage() {
            // Setup the map unit, you want to always use feet or meters for printer layout maps
            Map.MapUnit = GeographyUnit.Meter;

            // Here we create the default zoom levels for the sample.  We have pre-created a PrinterZoomLevelSet
            // That pre-defines commonly used zoom levels based on percentages of zoom
            Map.ZoomLevelSet = new PrinterZoomLevelSet(Map.MapUnit, PrinterHelper.GetPointsPerGeographyUnit(Map.MapUnit));

            // Here we set the background color to gray so there is contrast with the while page
            Map.BackgroundOverlay.BackgroundBrush = new GeoSolidBrush(GeoColor.StandardColors.LightGray);

            // Create the PrinterInteractiveOverlay to contain all of the PrinterLayers.
            // The interactive overlay allows the layers to be interacted with
            PrinterInteractiveOverlay printerOverlay = new PrinterInteractiveOverlay();

            Map.InteractiveOverlays.Add("PrintPreviewOverlay", printerOverlay);
            Map.InteractiveOverlays.MoveToBottom("PrintPreviewOverlay");

            // Create the PagePrinterLayer which shows the page boundary and is the area the user will
            // arrange all of the other layer on top of.
            PagePrinterLayer pagePrinterLayer = new PagePrinterLayer(PrinterPageSize.AnsiA, PrinterOrientation.Portrait);
            pagePrinterLayer.Open();
            printerOverlay.PrinterLayers.Add("PageLayer", pagePrinterLayer);

            // Get the pages extent, slightly scale it up, and then set that as the default current extent
            Map.CurrentExtent = RectangleShape.ScaleUp(pagePrinterLayer.GetPosition(), 10).GetBoundingBox();

            // Set the minimum sscale of the map to the last zoom level
            Map.MinimumScale = Map.ZoomLevelSet.ZoomLevel20.Scale;
        }

        private void AddCenteredTitleLabel() {
            LabelPrinterLayer labelPrinterLayer = new LabelPrinterLayer();

            //Setup the text and the font..
            labelPrinterLayer.Text = "Print Map";
            labelPrinterLayer.Font = new GeoFont("Arial", 10, DrawingFontStyles.Bold);
            labelPrinterLayer.TextBrush = new GeoSolidBrush(GeoColor.StandardColors.Black);

            // Set the labels position so that is it centered on the page one inch from the top                        
            labelPrinterLayer.PrinterWrapMode = PrinterWrapMode.AutoSizeText;
            RectangleShape pageBoundingbox = GetPageBoundingBox(PrintingUnit.Inch);
            PointShape labelCenter = new PointShape();
            labelCenter.X = pageBoundingbox.UpperRightPoint.X - pageBoundingbox.Width / 2;
            //labelCenter.Y = pageBoundingbox.UpperLeftPoint.Y - .5;
            labelCenter.Y = pageBoundingbox.UpperLeftPoint.Y - .75;
            labelPrinterLayer.SetPosition(5, 1, labelCenter, PrintingUnit.Inch);

            // Find the PrinterInteractiveOverlay so we can add the new LabelPrinterLayer
            PrinterInteractiveOverlay printerInteractiveOverlay = (PrinterInteractiveOverlay)Map.InteractiveOverlays["PrintPreviewOverlay"];
            printerInteractiveOverlay.PrinterLayers.Add("LabelLayer", labelPrinterLayer);
        }

        private void AddMapLayer()
        {
            //RectangleShape extent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
            // Create the MapPrinterLayer and set the position
            MapPrinterLayer mapPrinterLayer = new MapPrinterLayer();
            mapPrinterLayer.MapUnit = GeographyUnit.Meter;
            mapPrinterLayer.MapExtent = PreviousMapExtent;
            mapPrinterLayer.BackgroundMask = new AreaStyle(new GeoPen(GeoColor.StandardColors.Black, 1));
            mapPrinterLayer.Open();

            // Set the maps position slightly below the pages center and 8 inches wide and 7 inches tall
            RectangleShape pageBoundingbox = GetPageBoundingBox(PrintingUnit.Inch);
            //mapPrinterLayer.SetPosition(8, 10, pageBoundingbox.GetCenterPoint().X, pageBoundingbox.GetCenterPoint().Y - 1.5, PrintingUnit.Inch);
            pageBoundingbox = GetPageBoundingBox(PrintingUnit.Inch);
            PointShape mapCenter = new PointShape();
            mapCenter = pageBoundingbox.GetCenterPoint();
            //mapPrinterLayer.SetPosition(8, 9.5, mapCenter.X, mapCenter.Y - .5, PrintingUnit.Inch);
            mapPrinterLayer.SetPosition(7.75, 9.25, mapCenter.X, mapCenter.Y - .5, PrintingUnit.Inch);

            // Setup the intial extent and ensure they snap to the default ZoomLevel
            ZoomLevelSet zoomLevelSet = new ZoomLevelSet();
            mapPrinterLayer.MapExtent = ExtentHelper.ZoomToScale(zoomLevelSet.ZoomLevel03.Scale, PreviousMapExtent, mapPrinterLayer.MapUnit, (float)mapPrinterLayer.GetBoundingBox().Width, (float)mapPrinterLayer.GetBoundingBox().Height);

            // Add Bing Maps
            BingMapsLayer bingmapsLayer = new BingMapsLayer();
            string bingpassword = "AgwFGldoH7RpgsCWgbL0pqvweosSbsuGvRinCPUPgJwGUlqwYBYsRY3O33dQHxAy";
            bingmapsLayer.ApplicationId = bingpassword;
            bingmapsLayer.MapType = bingMapsMaptype;
            bingmapsLayer.DrawingExceptionMode = DrawingExceptionMode.DrawException;
            //bingmapsLayer.TileCache = new InMemoryBitmapTileCache();
            //string gDirectory = Landdb.Infrastructure.ApplicationEnvironment.MapCacheDirectory;
            //FileBitmapTileCache bitmapTileCache = new FileBitmapTileCache();
            //bitmapTileCache.CacheDirectory = gDirectory;
            //bitmapTileCache.CacheId = "BaseCachedTiles";
            //bitmapTileCache.TileAccessMode = TileAccessMode.Default;
            //bitmapTileCache.ImageFormat = TileImageFormat.Jpeg;
            //bingmapsLayer.TileCache = bitmapTileCache;
            mapPrinterLayer.Layers.Add(bingmapsLayer);
            mapPrinterLayer.Layers.Add(fieldsLayer);
            mapPrinterLayer.Layers.Add(annotationLayer);

            try {
                var shapelayerfeatures = fieldsLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);
                if (shapelayerfeatures.Count > 0) {
                    RectangleShape rs = ExtentHelper.GetBoundingBoxOfItems(shapelayerfeatures);
                    rs.ScaleUp(10);
                    mapPrinterLayer.MapExtent = rs;
                    Map.Refresh();
                }

                // Add the MapPrinterLayer to the PrinterInteractiveOverlay
                PrinterInteractiveOverlay printerInteractiveOverlay = (PrinterInteractiveOverlay)Map.InteractiveOverlays["PrintPreviewOverlay"];
                printerInteractiveOverlay.PrinterLayers.Add("MapLayer", mapPrinterLayer);
            }
            catch (Exception ex) {
                log.ErrorException("Caught an exception while working with the printing tool", ex);
                //System.Windows.MessageBox.Show("ImportTools - Caught an exception while working with a simple draw shape.");
            }

        }

        private void AddLegendLayer()
        {
            LegendItem title = new LegendItem();
            title.TextStyle = new TextStyle(Strings.MapLegend_Text, new GeoFont("Arial", 10, DrawingFontStyles.Bold), new GeoSolidBrush(GeoColor.SimpleColors.Black));

            LegendAdornmentLayer legendAdornmentLayer = new LegendAdornmentLayer();
            legendAdornmentLayer.Height = 100;

            legendAdornmentLayer.Title = title;
            //ColorItems
            foreach (ColorItem ci in coloritems) {
                LegendItem legendItem1 = new LegendItem();
                legendItem1.ImageStyle = new AreaStyle(new GeoSolidBrush(GeoColor.FromArgb(125, ci.MapColor.R, ci.MapColor.G, ci.MapColor.B)));
                legendItem1.TextStyle = new TextStyle(ci.Label, new GeoFont("Arial", 8), new GeoSolidBrush(GeoColor.SimpleColors.Black));
                legendAdornmentLayer.LegendItems.Add(legendItem1);
            }
            //legendAdornmentLayer.LegendItems.Add(legendItem1);
            //legendAdornmentLayer.LegendItems.Add(legendItem2);

            LegendPrinterLayer legendPrinterLayer = new LegendPrinterLayer(legendAdornmentLayer);
            //legendPrinterLayer.SetPosition(2, 1, -2.9, 3.9, PrintingUnit.Inch);
            legendPrinterLayer.SetPosition(2, 1, -2.9, 3.7, PrintingUnit.Inch);
            legendPrinterLayer.BackgroundMask = AreaStyles.CreateSimpleAreaStyle(new GeoColor(255, 230, 230, 230), GeoColor.SimpleColors.Black, 1);
            PrinterInteractiveOverlay printerInteractiveOverlay = (PrinterInteractiveOverlay)Map.InteractiveOverlays["PrintPreviewOverlay"];
            printerInteractiveOverlay.PrinterLayers.Add("LegendPrinterLayer", legendPrinterLayer);
        }

        //private void AddDataGridLayer()
        //{
        //    // Create the DataGridPrinterLayer
        //    DataGridPrinterLayer dataGridPrinterLayer = new DataGridPrinterLayer();
        //    dataGridPrinterLayer.TextFont = new GeoFont("Arial", 8);
        //    //dataGridPrinterLayer.CellTextJustification = LabelTextJustification.Left;
        //    dataGridPrinterLayer.TextHorizontalAlignment = TextHorizontalAlignment.Left;

        //    // Set the data grid position 4 inches below the page center and 8 inches wide and 2.5 inches tall
        //    RectangleShape pageBoundingbox = GetPageBoundingBox(PrintingUnit.Inch);
        //    dataGridPrinterLayer.SetPosition(8, 2.5, pageBoundingbox.GetCenterPoint().X, pageBoundingbox.GetCenterPoint().Y - 4, PrintingUnit.Inch);

        //    //Create the data table and columns
        //    dataGridPrinterLayer.DataTable = new DataTable();
        //    dataGridPrinterLayer.DataTable.Columns.Add("Country");
        //    dataGridPrinterLayer.DataTable.Columns.Add("Population");
        //    dataGridPrinterLayer.DataTable.Columns.Add("CurrencyCode");
        //    dataGridPrinterLayer.DataTable.Columns.Add("Area");

        //    // Find all of the countries with a population greater than 70 million
        //    // and add those items to the data table
        //    ShapeFileFeatureLayer shapefileFeatureLayer = new ShapeFileFeatureLayer(@"data/Countries02.shp", ShapeFileReadWriteMode.ReadOnly);
        //    shapefileFeatureLayer.Open();
        //    Collection<Feature> features = shapefileFeatureLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);
        //    shapefileFeatureLayer.Close();

        //    foreach (Feature feature in features)
        //    {
        //        double pop = Convert.ToDouble(feature.ColumnValues["Pop_cntry"].ToString());
        //        if (pop > 70000000)
        //        {
        //            dataGridPrinterLayer.DataTable.Rows.Add(new object[4] { feature.ColumnValues["Cntry_Name"], feature.ColumnValues["Pop_cntry"], feature.ColumnValues["curr_code"], feature.ColumnValues["sqkm"] });
        //        }
        //    }

        //    // Add the DataGridPrinterLayer to the PrinterInteractiveOverlay
        //    PrinterInteractiveOverlay printerInteractiveOverlay = (PrinterInteractiveOverlay)Map.InteractiveOverlays["PrintPreviewOverlay"];
        //    printerInteractiveOverlay.PrinterLayers.Add("DataGridLayer", dataGridPrinterLayer);
        //}

        private void AddLogoImage()
        {
            //Load the image to a GeoImage and then create the ImagePrinterLayer using that image
            string programDirectory = Landdb.Infrastructure.ApplicationEnvironment.ApplicationExecutableDirectory;
            string imageDirectory = Path.Combine(programDirectory, @"Resources\Images\");
            string compass = System.IO.Path.Combine(imageDirectory, "Compass.png");
            GeoImage compassGeoImage = new GeoImage(compass);
            ImagePrinterLayer imagePrinterLayer = new ImagePrinterLayer(compassGeoImage);

            // Set the imagePrinterLayer position offset from the page center and .75 inches wide and .75 inches tall
            RectangleShape pageBoundingbox = GetPageBoundingBox(PrintingUnit.Inch);
            PointShape imagePosition = new PointShape();
            //imagePosition.X = pageBoundingbox.LowerRightPoint.X - .65;
            //imagePosition.Y = pageBoundingbox.LowerRightPoint.Y + .65;
            imagePosition.X = pageBoundingbox.LowerRightPoint.X - 1.25;
            imagePosition.Y = pageBoundingbox.LowerRightPoint.Y + 1.25;
            imagePrinterLayer.SetPosition(.75, .75, imagePosition.X, imagePosition.Y, PrintingUnit.Inch);

            // Add the imagePrinterLayer to the PrinterInteractiveOverlay
            PrinterInteractiveOverlay printerInteractiveOverlay = (PrinterInteractiveOverlay)Map.InteractiveOverlays["PrintPreviewOverlay"];
            printerInteractiveOverlay.PrinterLayers.Add("ImageLayer", imagePrinterLayer);
        }

        private void AddScaleLineLayer()
        {
            // Get the PrinterInteractiveOverlay
            PrinterInteractiveOverlay printerInteractiveOverlay = (PrinterInteractiveOverlay)Map.InteractiveOverlays["PrintPreviewOverlay"];

            // Grab the MapPrinterLayer as we need to pass that into the scaleLinePrinterLayer so it
            // can be synced up to the map itself in real-time
            MapPrinterLayer mapPrinterLayer = (MapPrinterLayer)printerInteractiveOverlay.PrinterLayers["MapLayer"];

            //Create the scaleLinePrinterLayer and pass in the MapPrinterLayer
            ScaleLinePrinterLayer scaleLinePrinterLayer = new ScaleLinePrinterLayer(mapPrinterLayer);
            scaleLinePrinterLayer.MapUnit = GeographyUnit.DecimalDegree;

            // Set the scale line position offset from the page center and 1.25 inches wide and .25 inches tall
            RectangleShape pageBoundingbox = GetPageBoundingBox(PrintingUnit.Inch);

            PointShape scalePosition = new PointShape();
            scalePosition.X = pageBoundingbox.LowerLeftPoint.X + 1.25;
            scalePosition.Y = pageBoundingbox.LowerLeftPoint.Y + .75;
            scaleLinePrinterLayer.SetPosition(1.25, .25, scalePosition.X, scalePosition.Y, PrintingUnit.Inch);
            //scaleLinePrinterLayer.SetPosition(1.25, .25, pageBoundingbox.GetCenterPoint().X - 3.50, pageBoundingbox.GetCenterPoint().Y - 5.25, PrintingUnit.Inch);

            // Add the ScaleLinePrinterLayer to the PrinterInteractiveOverlay
            printerInteractiveOverlay.PrinterLayers.Add("ScaleLineLayer", scaleLinePrinterLayer);
        }

        #endregion

        #region Helper Methods

        private RectangleShape GetPageBoundingBox(PrintingUnit unit)
        {
            // This helper method gets the pages bounding box in the unit requested
            PrinterInteractiveOverlay printerInteractiveOverlay = (PrinterInteractiveOverlay)Map.InteractiveOverlays["PrintPreviewOverlay"];
            PagePrinterLayer pagePrinterLayer = (PagePrinterLayer)printerInteractiveOverlay.PrinterLayers["PageLayer"];
            return pagePrinterLayer.GetPosition(unit); ;
        }

        private RectangleShape GetMapBoundingBox(PrintingUnit unit) {
            // This helper method gets the pages bounding box in the unit requested
            PrinterInteractiveOverlay printerInteractiveOverlay = (PrinterInteractiveOverlay)Map.InteractiveOverlays["PrintPreviewOverlay"];
            //PagePrinterLayer pagePrinterLayer = (PagePrinterLayer)printerInteractiveOverlay.PrinterLayers["PageLayer"];
            MapPrinterLayer mapPrinterLayer = ((MapPrinterLayer)(printerInteractiveOverlay.PrinterLayers["MapLayer"]));
            return mapPrinterLayer.GetPosition(unit); ;
        }

        private PageSize GetPdfPageSize(PrinterPageSize pageSize) {
            PageSize pdfPageSize = PageSize.Letter;
            switch (pageSize) {
                case PrinterPageSize.AnsiA:
                    pdfPageSize = PageSize.Letter;
                    break;
                case PrinterPageSize.AnsiB:
                    pdfPageSize = PageSize.Ledger;
                    break;
                case PrinterPageSize.AnsiC:
                    pdfPageSize = PageSize.A2;
                    break;
                case PrinterPageSize.AnsiD:
                    pdfPageSize = PageSize.A1;
                    break;
                case PrinterPageSize.AnsiE:
                    pdfPageSize = PageSize.A0;
                    break;
                case PrinterPageSize.Custom:
                    MessageBox.Show(Strings.MessageBox_PrintMapToolViewModel_NotImplemented_Text);
                    break;
                default:
                    throw new NotSupportedException();
            }
            return pdfPageSize;
        }

        private PaperSize GetPrintPreviewPaperSize(PagePrinterLayer pagePrinterLayer)
        {
            PaperSize printPreviewPaperSize = new PaperSize("AnsiA", 850, 1100);
            switch (pagePrinterLayer.PageSize)
            {
                case PrinterPageSize.AnsiA:
                    printPreviewPaperSize = new PaperSize("AnsiA", 850, 1100);
                    break;
                case PrinterPageSize.AnsiB:
                    printPreviewPaperSize = new PaperSize("AnsiB", 1100, 1700);
                    break;
                case PrinterPageSize.AnsiC:
                    printPreviewPaperSize = new PaperSize("AnsiC", 1700, 2200);
                    break;
                case PrinterPageSize.AnsiD:
                    printPreviewPaperSize = new PaperSize("AnsiD", 2200, 3400);
                    break;
                case PrinterPageSize.AnsiE:
                    printPreviewPaperSize = new PaperSize("AnsiE", 3400, 4400);
                    break;
                case PrinterPageSize.Custom:
                    printPreviewPaperSize = new PaperSize("Custom Size", (int)pagePrinterLayer.CustomWidth, (int)pagePrinterLayer.CustomHeight);
                    break;
                default:
                    break;
            }

            return printPreviewPaperSize;
        }

        #endregion

        #region Export Buttons

        private void ToPrintPreview_Click() {
            PrinterInteractiveOverlay printerInteractiveOverlay = (PrinterInteractiveOverlay)Map.InteractiveOverlays["PrintPreviewOverlay"];
            PagePrinterLayer pagePrinterLayer = printerInteractiveOverlay.PrinterLayers["PageLayer"] as PagePrinterLayer;

            PrintDialog printDialog = new PrintDialog();
            PrintDocument printDocument = new PrintDocument();
            printDocument.DefaultPageSettings.Landscape = true;
            if (pagePrinterLayer.Orientation == PrinterOrientation.Portrait) {
                printDocument.DefaultPageSettings.Landscape = false;
            }

            printDocument.DefaultPageSettings.PaperSize = GetPrintPreviewPaperSize(pagePrinterLayer);

            PrinterGeoCanvas printerGeoCanvas = new PrinterGeoCanvas();
            printerGeoCanvas.DrawingArea = printDocument.DefaultPageSettings.Bounds;
            printerGeoCanvas.BeginDrawing(printDocument, pagePrinterLayer.GetBoundingBox(), Map.MapUnit);

            // Loop through all of the PrintingLayer in the PrinterInteractiveOverlay and print all of the
            // except for the PagePrinterLayer                
            Collection<SimpleCandidate> labelsInAllLayers = new Collection<SimpleCandidate>();
            foreach (PrinterLayer printerLayer in printerInteractiveOverlay.PrinterLayers) {
                printerLayer.IsDrawing = true;
                if (!(printerLayer is PagePrinterLayer)) {
                    printerLayer.Draw(printerGeoCanvas, labelsInAllLayers);
                }
                printerLayer.IsDrawing = false;
            }

            printerGeoCanvas.Flush();

            System.Windows.Forms.PrintPreviewDialog printPreviewDialog = new System.Windows.Forms.PrintPreviewDialog();
            printPreviewDialog.Document = printDocument;
            System.Windows.Forms.DialogResult dialogResult = printPreviewDialog.ShowDialog();
        }

        private void ToBitmap_Click() {
            PrinterInteractiveOverlay printerInteractiveOverlay = (PrinterInteractiveOverlay)Map.InteractiveOverlays["PrintPreviewOverlay"];
            PagePrinterLayer pagePrinterLayer = printerInteractiveOverlay.PrinterLayers["PageLayer"] as PagePrinterLayer;

            // Create a bitmap that is the size of the pages bounding box.  The page by default is in points unit
            // system which is very similar to pixels so that ampping is nice.
            Bitmap bitmap = null;
            try {
                bitmap = new Bitmap((int)pagePrinterLayer.GetBoundingBox().Width, (int)pagePrinterLayer.GetBoundingBox().Height);

                // Create a GdiPlusGeoCanvas and start the drawing
                GdiPlusGeoCanvas gdiPlusGeoCanvas = new GdiPlusGeoCanvas();
                gdiPlusGeoCanvas.BeginDrawing(bitmap, pagePrinterLayer.GetBoundingBox(), Map.MapUnit);

                // Loop through all of the PrintingLayer in the PrinterInteractiveOverlay and print all of the
                // except for the PagePrinterLayer                
                Collection<SimpleCandidate> labelsInAllLayers = new Collection<SimpleCandidate>();
                foreach (PrinterLayer printerLayer in printerInteractiveOverlay.PrinterLayers) {
                    printerLayer.IsDrawing = true;
                    if (!(printerLayer is PagePrinterLayer)) {
                        printerLayer.Draw(gdiPlusGeoCanvas, labelsInAllLayers);
                    }
                    printerLayer.IsDrawing = false;
                }

                // End the drawing
                gdiPlusGeoCanvas.EndDrawing();

                //Save the resulting bitmap to a file and open the file to show the user
                string filename = AddLayersBySaveFileDialog();
                if (string.IsNullOrEmpty(filename)) { return; }
                //string filename = "PrintingResults.bmp";
                bitmap.Save(filename);
                System.Diagnostics.Process.Start(filename);
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message + " " + ex.StackTrace, Strings.MessageBox_Exception_Text);
            }
            finally {
                if (bitmap != null) { bitmap.Dispose(); }
            }
        }

        internal string AddLayersBySaveFileDialog() {
            SaveFileDialog savefiledialog = new SaveFileDialog();
            //savefiledialog.CheckFileExists = true;
            //savefiledialog.CheckPathExists = true;
            savefiledialog.OverwritePrompt = true;
            savefiledialog.Title = Strings.SaveBitmapFile_Text;
            //savefiledialog.Filter = "shp";
            savefiledialog.DefaultExt = "bmp";
            savefiledialog.Filter = "bmp files (*.bmp)|*.bmp";
            bool? dialogresult = savefiledialog.ShowDialog();
            if (dialogresult.HasValue && dialogresult.Value) {
                return savefiledialog.FileName;
            }
            return string.Empty;
        }

        private void ToPDF_Click() {
            // Get the PrinterInteractiveOverlay and PagePrinterLayer as we are going to need them below
            PrinterInteractiveOverlay printerInteractiveOverlay = (PrinterInteractiveOverlay)Map.InteractiveOverlays["PrintPreviewOverlay"];
            PagePrinterLayer pagePrinterLayer = printerInteractiveOverlay.PrinterLayers["PageLayer"] as PagePrinterLayer;

            PdfDocument pdfDocument = null;
            PdfPage pdfPage = null;

            try {
                // Create the PDF documents, pages, and copnfigure them
                pdfDocument = new PdfDocument();
                pdfPage = pdfDocument.AddPage();
                pdfPage.Orientation = pagePrinterLayer.Orientation == PrinterOrientation.Portrait ? PageOrientation.Portrait : PageOrientation.Landscape;
                pdfPage.Size = GetPdfPageSize(pagePrinterLayer.PageSize);

                // Create the PdfGeoCanvas and pass tha PdfPage into the BeginDrawing 
                PdfGeoCanvas pdfGeoCanvas = new PdfGeoCanvas();
                pdfGeoCanvas.BeginDrawing(pdfPage, pagePrinterLayer.GetBoundingBox(), Map.MapUnit);

                // Loop through all of the PrinterLayers and draw them to the canvas except for the
                // PagePrinterLayer which is just used for the visual layout
                Collection<SimpleCandidate> labelsInAllLayers = new Collection<SimpleCandidate>();
                foreach (PrinterLayer printerLayer in printerInteractiveOverlay.PrinterLayers) {
                    printerLayer.IsDrawing = true;
                    if (!(printerLayer is PagePrinterLayer)) {
                        printerLayer.Draw(pdfGeoCanvas, labelsInAllLayers);
                    }
                    printerLayer.IsDrawing = false;
                }

                // Finish up the drawing
                pdfGeoCanvas.EndDrawing();

                // Dave the PDF document and launch it in the default viewer to show the user
                string filename = "PrintingResults.pdf";
                pdfDocument.Save(filename);
                System.Diagnostics.Process.Start(filename);
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message + " " + ex.StackTrace, Strings.MessageBox_Exception_Text);
            }
            finally {
                if (pdfDocument != null) { pdfDocument.Dispose(); }
            }
        }

        private void ToPrinter_Click() {
            PrinterInteractiveOverlay printerInteractiveOverlay = (PrinterInteractiveOverlay)Map.InteractiveOverlays["PrintPreviewOverlay"];
            PagePrinterLayer pagePrinterLayer = printerInteractiveOverlay.PrinterLayers["PageLayer"] as PagePrinterLayer;

            PrintDocument printDocument = new PrintDocument();
            printDocument.DefaultPageSettings.Landscape = true;
            if (pagePrinterLayer.Orientation == PrinterOrientation.Portrait) {
                printDocument.DefaultPageSettings.Landscape = false;
            }

            printDocument.DefaultPageSettings.PaperSize = GetPrintPreviewPaperSize(pagePrinterLayer);

            PrinterGeoCanvas printerGeoCanvas = new PrinterGeoCanvas();
            printerGeoCanvas.DrawingArea = new System.Drawing.Rectangle(0, 0, Convert.ToInt32(printDocument.DefaultPageSettings.PrintableArea.Width), Convert.ToInt32(printDocument.DefaultPageSettings.PrintableArea.Height));
            printerGeoCanvas.BeginDrawing(printDocument, pagePrinterLayer.GetBoundingBox(), Map.MapUnit);

            // Loop through all of the PrintingLayer in the PrinterInteractiveOverlay and print all of the
            // except for the PagePrinterLayer                
            Collection<SimpleCandidate> labelsInAllLayers = new Collection<SimpleCandidate>();
            foreach (PrinterLayer printerLayer in printerInteractiveOverlay.PrinterLayers) {
                printerLayer.IsDrawing = true;
                if (!(printerLayer is PagePrinterLayer)) {
                    printerLayer.Draw(printerGeoCanvas, labelsInAllLayers);
                }
                printerLayer.IsDrawing = false;
            }

            printerGeoCanvas.EndDrawing();
        }

        #endregion

        #region Pan & Zoom Buttons

        private void ZoomIn_Click() {
            // Grab the MapPrinterLayer and adjust the extent
            PrinterInteractiveOverlay printerOverlay = (PrinterInteractiveOverlay)Map.InteractiveOverlays["PrintPreviewOverlay"];
            MapPrinterLayer mapPrinterLayer = ((MapPrinterLayer)(printerOverlay.PrinterLayers["MapLayer"]));

            // Here we snap the current scale to the default zoomLevelSet when we zoom in
            ZoomLevelSet zoomLevelSet = new ZoomLevelSet();
            double newScale = ZoomLevelSet.GetLowerZoomLevelScale(ExtentHelper.GetScale(mapPrinterLayer.MapExtent, (float)mapPrinterLayer.GetBoundingBox().Width, mapPrinterLayer.MapUnit), zoomLevelSet);
            mapPrinterLayer.MapExtent = ExtentHelper.ZoomToScale(newScale, mapPrinterLayer.MapExtent, mapPrinterLayer.MapUnit, (float)mapPrinterLayer.GetBoundingBox().Width, (float)mapPrinterLayer.GetBoundingBox().Height);

            Map.Refresh();
        }

        private void ZoomOut_Click() {
            // Grab the MapPrinterLayer and adjust the extent
            PrinterInteractiveOverlay printerOverlay = (PrinterInteractiveOverlay)Map.InteractiveOverlays["PrintPreviewOverlay"];
            MapPrinterLayer mapPrinterLayer = ((MapPrinterLayer)(printerOverlay.PrinterLayers["MapLayer"]));

            // Here we snap the current scale to the default zoomLevelSet when we zoom in
            ZoomLevelSet zoomLevelSet = new ZoomLevelSet();
            double newScale = ZoomLevelSet.GetHigherZoomLevelScale(ExtentHelper.GetScale(mapPrinterLayer.MapExtent, (float)mapPrinterLayer.GetBoundingBox().Width, mapPrinterLayer.MapUnit), zoomLevelSet);
            mapPrinterLayer.MapExtent = ExtentHelper.ZoomToScale(newScale, mapPrinterLayer.MapExtent, mapPrinterLayer.MapUnit, (float)mapPrinterLayer.GetBoundingBox().Width, (float)mapPrinterLayer.GetBoundingBox().Height);

            Map.Refresh();
        }

        private void PanNorth_Click() {
            PrinterInteractiveOverlay printerOverlay = (PrinterInteractiveOverlay)Map.InteractiveOverlays["PrintPreviewOverlay"];
            MapPrinterLayer mapPrinterLayer = ((MapPrinterLayer)(printerOverlay.PrinterLayers["MapLayer"]));
            mapPrinterLayer.MapExtent = ExtentHelper.Pan(mapPrinterLayer.MapExtent, PanDirection.Up, 30);
            Map.Refresh();
        }

        private void PanWest_Click() {
            PrinterInteractiveOverlay printerOverlay = (PrinterInteractiveOverlay)Map.InteractiveOverlays["PrintPreviewOverlay"];
            MapPrinterLayer mapPrinterLayer = ((MapPrinterLayer)(printerOverlay.PrinterLayers["MapLayer"]));
            mapPrinterLayer.MapExtent = ExtentHelper.Pan(mapPrinterLayer.MapExtent, PanDirection.Left, 30); 
            Map.Refresh();
        }

        private void PanSouth_Click() {
            PrinterInteractiveOverlay printerOverlay = (PrinterInteractiveOverlay)Map.InteractiveOverlays["PrintPreviewOverlay"];
            MapPrinterLayer mapPrinterLayer = ((MapPrinterLayer)(printerOverlay.PrinterLayers["MapLayer"]));
            mapPrinterLayer.MapExtent = ExtentHelper.Pan(mapPrinterLayer.MapExtent, PanDirection.Down, 30);
            Map.Refresh();
        }

        private void PanEast_Click() {
            PrinterInteractiveOverlay printerOverlay = (PrinterInteractiveOverlay)Map.InteractiveOverlays["PrintPreviewOverlay"];
            MapPrinterLayer mapPrinterLayer = ((MapPrinterLayer)(printerOverlay.PrinterLayers["MapLayer"]));
            mapPrinterLayer.MapExtent = ExtentHelper.Pan(mapPrinterLayer.MapExtent, PanDirection.Right, 30);
            Map.Refresh();
        }

        #endregion

        #region Combo Boxes & ContextMenu

        private void SetupCombos() {

            paperSizes = new List<string>();
            paperSizes.Add("ANSI A - 8.5 x 11 (A4)");
            paperSizes.Add("ANSI B - 11 x 17  (A3)");
            paperSizes.Add("ANSI C - 17 x 22  (A2)");
            paperSizes.Add("ANSI D - 22 x 34  (A1)");
            paperSizes.Add("ANSI E - 34 x 44  (A0)");
            selectedPaperSize = paperSizes[0];
            RaisePropertyChanged("PaperSizes");
            RaisePropertyChanged("SelectedPaperSize");

            paperOrientations = new List<string>();
            paperOrientations.Add("Portrait");
            paperOrientations.Add("Landscape");
            selectedPaperOrientation = paperOrientations[0];
            RaisePropertyChanged("PaperOrientations");
            RaisePropertyChanged("SelectedPaperOrientation");


            //Collection<ZoomLevel> mapzoomlevels = Map.ZoomLevelSet.GetZoomLevels();
            //PrinterZoomLevelSet pz = new PrinterZoomLevelSet();
            //ZoomLevelSet zz = new ZoomLevelSet();
            //ZoomLevel zl = new ZoomLevel();
            //double pzper = pz.GetZoomPercentage(zl);

            paperPercentages = new List<string>();
            Collection<ZoomLevel> mapzoomlevels = Map.ZoomLevelSet.GetZoomLevels();
            ZoomLevelSet zls = Map.ZoomLevelSet;
            PrinterZoomLevelSet pzls = zls as PrinterZoomLevelSet;
            foreach (ZoomLevel level in mapzoomlevels) {
                //paperPercentages.Add(((PrinterZoomLevelSet)Map.ZoomLevelSet).GetZoomPercentage(level) + "%");
                paperPercentages.Add(pzls.GetZoomPercentage(level) + "%");
            }
            selectedPaperPercentage = paperOrientations[0];
            RaisePropertyChanged("PaperPercentages");
            RaisePropertyChanged("SelectedPaperPercentage");

        }

        private void SetupContextMenu() {
            //Map.ContextMenu = new System.Windows.Controls.ContextMenu();

            //MenuItem removeItem = new MenuItem();
            //removeItem.Header = "Remove";
            //removeItem.Click += new RoutedEventHandler(RemoveLayer_Click);
            //Map1.ContextMenu.Items.Add(removeItem);

            //MenuItem editItem = new MenuItem();
            //editItem.Header = "Property";
            //editItem.Click += new RoutedEventHandler(EditLayer_Click);
            //Map1.ContextMenu.Items.Add(editItem);
        }

        //private void cbxPaperSize_SelectionChanged(object sender, SelectionChangedEventArgs e) {
        //    if (IsLoaded) {
        //        // When the page size combobox is changed then we update the PagePrinterLayer to reflect the changes
        //        PrinterInteractiveOverlay printerInteractiveOverlay = (PrinterInteractiveOverlay)Map.InteractiveOverlays["PrintPreviewOverlay"];
        //        PagePrinterLayer pagePrinterLayer = (PagePrinterLayer)printerInteractiveOverlay.PrinterLayers["PageLayer"];

        //        string selectText = ((ComboBoxItem)e.AddedItems[0]).Content.ToString();
        //        if (selectText.StartsWith("ANSI A"))
        //            pagePrinterLayer.PageSize = PrinterPageSize.AnsiA;
        //        else if (selectText.StartsWith("ANSI B"))
        //            pagePrinterLayer.PageSize = PrinterPageSize.AnsiB;
        //        else if (selectText.StartsWith("ANSI C"))
        //            pagePrinterLayer.PageSize = PrinterPageSize.AnsiC;
        //        else if (selectText.StartsWith("ANSI D"))
        //            pagePrinterLayer.PageSize = PrinterPageSize.AnsiD;
        //        else if (selectText.StartsWith("ANSI E"))
        //            pagePrinterLayer.PageSize = PrinterPageSize.AnsiE;

        //        Map.Refresh();
        //    }
        //}

        //private void cbxOrientation_SelectionChanged(object sender, SelectionChangedEventArgs e) {
        //    // When the orientation combobox is changed then we update the PagePrinterLayer to reflect the changes
        //    if (this.IsLoaded) {
        //        PrinterInteractiveOverlay printerInteractiveOverlay = (PrinterInteractiveOverlay)Map.InteractiveOverlays["PrintPreviewOverlay"];
        //        PagePrinterLayer pagePrinterLayer = (PagePrinterLayer)printerInteractiveOverlay.PrinterLayers["PageLayer"];

        //        string selectText = ((ComboBoxItem)e.AddedItems[0]).Content.ToString();
        //        if (selectText.ToUpper() == "LANDSCAPE") {
        //            pagePrinterLayer.Orientation = PrinterOrientation.Landscape;
        //        }
        //        else if (selectText.ToUpper() == "PORTRAIT") {
        //            pagePrinterLayer.Orientation = PrinterOrientation.Portrait;
        //        }

        //        Map.Refresh();
        //    }
        //}

        #endregion

        #region Toolbox

        //private void toolBoxItem_Click(object sender, RoutedEventArgs e) {
        //    PrinterInteractiveOverlay printerInteractiveOverlay = (PrinterInteractiveOverlay)Map1.InteractiveOverlays["PrintPreviewOverlay"];
        //    PrinterLayer printerLayer = null;

        //    if (sender == btnAddLabel) {
        //        printerLayer = new LabelPrinterLayer();
        //        printerLayer.SetPosition(2, 1, 0, 0, PrintingUnit.Inch);
        //    }
        //    else if (sender == btnAddImage) {
        //        printerLayer = new ImagePrinterLayer();
        //    }
        //    else if (sender == btnAddScaleLine) {
        //        MessageBox.Show("NotImplemented");
        //    }
        //    else if (sender == btnAddScaleBar) {
        //        MessageBox.Show("NotImplemented");
        //    }
        //    else if (sender == btnAddDataGrid) {
        //        printerLayer = new DataGridPrinterLayer();
        //        printerLayer.SetPosition(1, 1, 0, 0, PrintingUnit.Inch);
        //    }

        //    //if (ShowPrinterLayerProperties(printerLayer) == DialogResult.OK)
        //    if (ShowPrinterLayerProperties(printerLayer) == true) {
        //        printerInteractiveOverlay.PrinterLayers.Add(printerLayer);
        //        Map1.Refresh();
        //    }
        //}

        private void AddTitle_Click() {
            PrinterInteractiveOverlay printerInteractiveOverlay = (PrinterInteractiveOverlay)Map.InteractiveOverlays["PrintPreviewOverlay"];
            LabelPrinterLayer labelPrinterLayer = ((LabelPrinterLayer)(printerInteractiveOverlay.PrinterLayers["LabelLayer"]));
            if (ShowPrinterLayerProperties(labelPrinterLayer) == true) {
                RectangleShape pageBoundingbox = GetPageBoundingBox(PrintingUnit.Inch);
                PointShape labelCenter = new PointShape();
                labelCenter.X = pageBoundingbox.UpperRightPoint.X - pageBoundingbox.Width / 2;
                labelCenter.Y = pageBoundingbox.UpperLeftPoint.Y - .5;
                labelPrinterLayer.SetPosition(5, 1, labelCenter, PrintingUnit.Inch);
                //printerInteractiveOverlay.PrinterLayers.Add(labelPrinterLayer);
                Map.Refresh();
            }
        }

        private void AddLabel_Click() {
            PrinterInteractiveOverlay printerInteractiveOverlay = (PrinterInteractiveOverlay)Map.InteractiveOverlays["PrintPreviewOverlay"];
            PrinterLayer printerLayer = null;
            if (ShowPrinterLayerProperties(printerLayer) == true) {
                printerInteractiveOverlay.PrinterLayers.Add(printerLayer);
                Map.Refresh();
            }
        }

        private void AddImage_Click() {
            MessageBox.Show(Strings.NotImplemented_Text);
        }

        private void AddScaleLine_Click() {
            MessageBox.Show(Strings.NotImplemented_Text);
        }

        private void AddScaleBar_Click() {
            MessageBox.Show(Strings.NotImplemented_Text);
        }

        private void AddDataGrid_Click() {
            MessageBox.Show(Strings.NotImplemented_Text);
        }

        #endregion

        #region Right Click and Dialogs

        //private void RemoveLayer_Click(object sender, RoutedEventArgs e) {
        //    PrinterInteractiveOverlay printerInteractiveOverlay = (PrinterInteractiveOverlay)Map1.InteractiveOverlays["PrintPreviewOverlay"];
        //    PrinterLayer printerLayer = (PrinterLayer)((MenuItem)sender).Tag;

        //    printerInteractiveOverlay.PrinterLayers.Remove(printerLayer);

        //    Map1.Refresh();
        //}

        //private void EditLayer_Click(object sender, RoutedEventArgs e) {
        //    PrinterInteractiveOverlay printerInteractiveOverlay = (PrinterInteractiveOverlay)Map1.InteractiveOverlays["PrintPreviewOverlay"];
        //    PrinterLayer printerLayer = (PrinterLayer)((MenuItem)sender).Tag;

        //    ShowPrinterLayerProperties(printerLayer);
        //    Map1.Refresh();
        //}

        private bool? ShowPrinterLayerProperties(PrinterLayer printerLayer) {
            bool? dialogResult = false;

            if (printerLayer != null) {
                if (printerLayer.GetType() == typeof(LabelPrinterLayer)) {
                    LabelPrinterLayerProperty dialog = new LabelPrinterLayerProperty((LabelPrinterLayer)printerLayer);
                    //dialog.Owner = Application.Current.MainWindow;
                    dialogResult = dialog.ShowDialog();
                }
                else if (printerLayer.GetType() == typeof(ScaleBarPrinterLayer)) {
                    MessageBox.Show($"{Strings.Scalebar_Text} ({Strings.ComingSoon_Text})");
                }
                else if (printerLayer.GetType() == typeof(ScaleLinePrinterLayer)) {
                    MessageBox.Show($"{Strings.Scaleline_Text} ({Strings.ComingSoon_Text})");
                }
                else if (printerLayer.GetType() == typeof(ImagePrinterLayer)) {
                    ImagePrinterLayerProperty dialog = new ImagePrinterLayerProperty((ImagePrinterLayer)printerLayer);
                    //dialog.Owner = Application.Current.MainWindow;
                    dialogResult = dialog.ShowDialog();
                }
                //else if (printerLayer.GetType() == typeof(DataGridPrinterLayer)) {
                //    DataGridPrinterLayerProperty dialog = new DataGridPrinterLayerProperty((DataGridPrinterLayer)printerLayer);
                //    dialog.Owner = Application.Current.MainWindow;
                //    dialogResult = dialog.ShowDialog();
                //}
            }

            return dialogResult;
        }

        #endregion

    }
}
