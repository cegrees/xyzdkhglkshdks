﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GalaSoft.MvvmLight;
using ThinkGeo.MapSuite.WpfDesktopEdition;
using ThinkGeo.MapSuite.Core;
using Landdb.Infrastructure;
using System.IO;
using Microsoft.Win32;
using GalaSoft.MvvmLight.Command;
using System.Windows.Input;
using System.Collections.ObjectModel;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure.Messages;
using Landdb.Client.Infrastructure;
using System.Windows.Threading;
using Landdb.Resources;
using Landdb.ViewModel.Maps.ImageryOverlay;
using Landdb.ViewModel.Fields.FieldDetails.MapModels;
using NLog;
using System.Windows;

namespace Landdb.ViewModel.Secondary.Map {
    public class ShapeImportViewModel : ViewModelBase {
        IClientEndpoint clientEndpoint;
        Dispatcher dispatcher;
        Logger log = LogManager.GetCurrentClassLogger();
        bool maploaded = false;
        bool startedOutOnline = false;
        bool hasunsavedchanges = false;
        //IMapOverlay selectedoverlay;
        ShapeFileFeatureLayer shapeLayer = new ShapeFileFeatureLayer();
        GpxFeatureLayer gpxLayer = new GpxFeatureLayer();
        KmlFeatureLayer kmlLayer = new KmlFeatureLayer();
        InMemoryFeatureLayer currentLayer = new InMemoryFeatureLayer();
        InMemoryFeatureLayer selectedlayer = new InMemoryFeatureLayer();
        LayerOverlay layerOverlay = new LayerOverlay();
        Proj4Projection projection;
        int projInt = 4326;
        string overrideprojection = string.Empty;
        string filename = string.Empty;
        string newfilenamewithpath = string.Empty;
        string newprojectionfilenamewithpath = string.Empty;
        MultipolygonShape importpoly = new MultipolygonShape();
        ZoomLevelFactory zoomlevelfactory = new ZoomLevelFactory();
        string coordinateformat = string.Empty;

        public ShapeImportViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher) {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            projection = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetGoogleMapParametersString());
            importpoly = new MultipolygonShape();

            InitializeMap();

            ResetLayers();
            CompleteCommand = new RelayCommand(Complete);
            OpenImportShapeFileCommand = new RelayCommand(OpenAndLoadaShapefile);
            ExportToGeojsonCommand = new RelayCommand(ExportShapeFile);
            CancelCommand = new RelayCommand(Cancel);
        }

        public ICommand CompleteCommand { get; private set; }
        public ICommand OpenImportShapeFileCommand { get; private set; }
        public ICommand ExportToGeojsonCommand { get; private set; }
        public ICommand ExportToKmlCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }

        public WpfMap Map { get; set; }

        public string FileName {
            get { return filename; }
            set {
                if (filename == value) { return; }
                filename = value;
                RaisePropertyChanged("FileName");
            }
        }

        public string OverrideProjection {
            get { return overrideprojection; }
            set {
                if (overrideprojection == value) { return; }
                overrideprojection = value;
                RaisePropertyChanged("OverrideProjection");
            }
        }

        public Proj4Projection Projection {
            get { return projection; }
            set {
                if (projection == value) { return; }
                Proj4Projection test = value;
                if (projection.InternalProjectionParametersString == test.InternalProjectionParametersString) { return; }
                projection = value;
                if (maploaded && !string.IsNullOrEmpty(filename)) {
                    OpenAndLoadaShapefile();
                }
                RaisePropertyChanged("Projection");
            }
        }

        public bool HasUnsavedChanges {
            get { return hasunsavedchanges; }
            set {
                if (hasunsavedchanges == value) { return; }
                hasunsavedchanges = value;
                RaisePropertyChanged("HasUnsavedChanges");
            }
        }

        public void ResetLayers() {
            HasUnsavedChanges = false;
            importpoly = new MultipolygonShape();
            overrideprojection = string.Empty;
            selectedlayer.InternalFeatures.Clear();
            Map.Refresh();
        }

        void OnCancelCropzone() {
            var importmessage = new ShapeImportMessage() { ShapeBase = new MultipolygonShape() };
            Messenger.Default.Send<ShapeImportMessage>(importmessage);
        }

        void InitializeMap() {
            Map = new WpfMap();
            startedOutOnline = Landdb.Client.Infrastructure.NetworkStatus.IsInternetAvailableFast();
            Map.MapUnit = GeographyUnit.Meter;
            Map.MapTools.PanZoomBar.Visibility = System.Windows.Visibility.Visible;
            Map.MapTools.PanZoomBar.GlobeButtonVisibility = System.Windows.Visibility.Hidden;
            Map.MapTools.PanZoomBar.VerticalAlignment = System.Windows.VerticalAlignment.Center;
            Map.MapTools.PanZoomBar.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            Map.Background = System.Windows.Media.Brushes.White;
            Map.Cursor = Cursors.Hand;
            zoomlevelfactory = new ZoomLevelFactory();
            Map.ZoomLevelSet = zoomlevelfactory.ZoomLevelSET;
            Map.MouseLeftButtonDown += (sender, e) => {
                ((MainWindow)System.Windows.Application.Current.MainWindow).Draggable = false;
            };

            Map.Loaded += (sender, e) => {
                if (maploaded) { return; }
                log.Info("ImportTools - Initializing shape import");
                if (string.IsNullOrEmpty(newfilenamewithpath)) {
                    LoadBingMapsOverlay();
                    OpenAndLoadaShapefile();
                }
                maploaded = true;
            };

            Map.MapClick += (sender, e) => {
                if (Map.TrackOverlay.TrackMode != TrackMode.None) { return; }
                EnsureCurrentLayerIsOpen();
                PointShape worldlocation = e.WorldLocation as PointShape;
                PointShape latlonlocation = projection.ConvertToInternalProjection(worldlocation) as PointShape;
                try {
                    Collection<Feature> selectedFeatures = currentLayer.QueryTools.GetFeaturesContaining(e.WorldLocation, ReturningColumnsType.AllColumns);
                    if (selectedFeatures.Count > 0) {
                        for (int i = 0; i < currentLayer.InternalFeatures.Count; i++) {
                            MultipolygonShape multipoly = new MultipolygonShape();
                            Feature feat = currentLayer.InternalFeatures[i];
                            var shape = feat.GetShape();
                            if (shape is AreaBaseShape)
                            {
                                multipoly = new MultipolygonShape();
                                if (shape is MultipolygonShape)
                                {
                                    multipoly = shape as MultipolygonShape;
                                }
                                PolygonShape poly = new PolygonShape();
                                if (shape is PolygonShape)
                                {
                                    poly = shape as PolygonShape;
                                    multipoly.Polygons.Add(poly);
                                }
                                if (shape is EllipseShape)
                                {
                                    EllipseShape ellipse = shape as EllipseShape;
                                    poly = ellipse.ToPolygon();
                                    multipoly.Polygons.Add(poly);
                                }
                            }
                            if (multipoly.Polygons.Count > 0) {
                                if (multipoly.Contains(latlonlocation)) {
                                    log.Info("ImportTools - Import shape selected.");
                                    selectedlayer.InternalFeatures.Add(new Feature(multipoly));
                                    currentLayer.InternalFeatures.RemoveAt(i);
                                    if (importpoly.Polygons.Count == 0) {
                                        importpoly = multipoly;
                                    }
                                    else {
                                        MultipolygonShape newpoly = importpoly.Union(multipoly);
                                        importpoly = newpoly;
                                    }
                                    HasUnsavedChanges = true;
                                    break;
                                }
                            }
                        }
                        Map.Refresh();
                    }
                }
                catch (Exception ex) {
                    //log.Error("Importtool - Error mouse click - {0}", ex);
                    //System.Windows.MessageBox.Show("ImportTools - Caught an exception while importing a shape.");
                    try {
                        for (int i = 0; i < currentLayer.InternalFeatures.Count; i++) {
                            Feature feat = currentLayer.InternalFeatures[i];
                            MultipolygonShape multipoly = feat.GetShape() as MultipolygonShape;
                            MultipolygonShape mps = multipoly.Buffer(0, Map.MapUnit, MapUnitFactory.AreaDistanceMeasureConversion(Map.MapUnit));
                            if (mps.Polygons.Count > 0) {
                                if (mps.Contains(latlonlocation)) {
                                    log.Info($"ImportTools - Import shape selected.");
                                    selectedlayer.InternalFeatures.Add(new Feature(mps));
                                    currentLayer.InternalFeatures.RemoveAt(i);
                                    if (importpoly.Polygons.Count == 0) {
                                        importpoly = mps;
                                    }
                                    else {
                                        MultipolygonShape newpoly = importpoly.Union(mps);
                                        importpoly = newpoly;
                                    }
                                    HasUnsavedChanges = true;
                                    break;
                                }
                            }
                        }
                        Map.Refresh();
                    }
                    catch (Exception ex11) {
                        log.Error("Importtool - Error mouse click - {0}", ex);
                        System.Windows.MessageBox.Show($"{Strings.ImportTools_Text} - {Strings.Exception_ImportingShape_Text}.");
                    }
                }
            };
        }

        private void LoadBingMapsOverlay() {
            if (!startedOutOnline) { return; }
            IMapOverlay mapoverlay = new BingMapOverlay();
            Overlay bingOverlay = mapoverlay.Overlay;
            if (bingOverlay == null) { return; }
            if (Map.Overlays.Contains("Bing")) {
                Map.Overlays.Remove("Bing");
            }
            if (Map.Overlays.Contains("Bing Online")) {
                Map.Overlays.Remove("Bing Online");
            }
            if (Map.Overlays.Contains("Roads Only")) {
                Map.Overlays.Remove("Roads Only");
            }
            if (Map.Overlays.Contains("Road Online")) {
                Map.Overlays.Remove("Road Online");
            }
            if (Map.Overlays.Contains("None")) {
                Map.Overlays.Remove("None");
            }
            Map.Overlays.Add(mapoverlay.Name, bingOverlay);
            Map.Overlays.MoveToBottom(mapoverlay.Name);
        }

        private void OpenAndLoadaShapefile() {
            bool goodfile = AddLayersByOpenFileDialog();
            if (goodfile) {
                bool fileloaded = CreateShapeFileLayerAndOverlay();
                if (fileloaded) {
                    LoadShapeFileOverlayOntoMap();
                    if (Map.Overlays.Contains("importOverlay")) {
                        if (((LayerOverlay)Map.Overlays["importOverlay"]).Layers.Contains("ImportLayer")) {
                            Map.Refresh();
                        }
                    }
                }
                else {
                    log.Info("ImportTools - failed to load file.");
                    shapeLayer = new ShapeFileFeatureLayer();
                    gpxLayer = new GpxFeatureLayer();
                    kmlLayer = new KmlFeatureLayer();
                    newfilenamewithpath = string.Empty;
                    OnCancelCropzone();
                }
            }
            else {
                log.Info("ImportTools - Not a good shape file to import.");
                shapeLayer = new ShapeFileFeatureLayer();
                gpxLayer = new GpxFeatureLayer();
                kmlLayer = new KmlFeatureLayer();
                newfilenamewithpath = string.Empty;
                OnCancelCropzone();
            }
        }

        private bool AddLayersByOpenFileDialog() {
            importpoly = new MultipolygonShape();
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Multiselect = false;
            openFileDialog.AddExtension = true;
            openFileDialog.CheckFileExists = false;
            //openFileDialog.Filter = "All supported layers|*.shp;*.sid;*.jp2;*.jpg;*.ecw;*.tif;*.png;*.bmp|Shape files (*.shp)|*.shp|MrSID Images (*.sid)|*.sid|JPEG 2000 Images (*.jp2)|*.jp2|JPEG Images (*.jpg)|*.jpg|ECW Images (*.ecw)|*.ecw|GeoTIFF Images (*.tif)|*.tif";
            //openFileDialog.Filter = "All supported layers|*.shp;*.gpx;*.kml;|Shape files (*.shp)|*.shp|GPX files (*.gpx)|*.gpx|KML files (*.kml)|*.kml";
            //openFileDialog.Filter = "All supported layers|*.shp;*.gpx;|Shape files (*.shp)|*.shp|GPX files (*.gpx)|*.gpx";
            //openFileDialog.Filter = "Shape files (*.shp)|*.shp|KML files (*.kml)|*.kml|geojson files (*.geojson)|*.geojson";
            openFileDialog.Filter = "Shape files (*.shp)|*.shp|KML files (*.kml)|*.kml";
            //openFileDialog.Filter = "Shape files (*.shp)|*.shp";
            bool? dialogresult = openFileDialog.ShowDialog();
            if (dialogresult.HasValue && dialogresult.Value) {
                newfilenamewithpath = openFileDialog.FileName;
                log.Info("ImportTools - Shape import file selected: {0}.", newfilenamewithpath);
                if (File.Exists(newfilenamewithpath)) {
                    string tempFileBase = Path.Combine(Path.GetDirectoryName(newfilenamewithpath), Path.GetFileNameWithoutExtension(newfilenamewithpath));
                    string result = Path.GetExtension(newfilenamewithpath).Replace(".", "");
                    if (result == "kml" && File.Exists(newfilenamewithpath))
                    {
                        FileName = openFileDialog.FileName;
                        return true;
                    }
                    if (result == "geojson" && File.Exists(newfilenamewithpath))
                    {
                        FileName = openFileDialog.FileName;
                        return true;
                    }
                    if (result == "gpx" && File.Exists(newfilenamewithpath)) {
                        FileName = openFileDialog.FileName;
                        return true;
                    }
                    newprojectionfilenamewithpath = string.Format("{0}.dbf", tempFileBase);
                    if (result == "shp" && !File.Exists(newprojectionfilenamewithpath)) {
                        return false;
                    }
                    newprojectionfilenamewithpath = string.Format("{0}.shx", tempFileBase);
                    if (result == "shp" && !File.Exists(newprojectionfilenamewithpath)) {
                        return false;
                    }
                    FileName = openFileDialog.FileName;
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                return false;
            }
        }

        private bool CreateShapeFileLayerAndOverlay() {
            try {
                layerOverlay = new LayerOverlay();
                layerOverlay.DrawingExceptionMode = DrawingExceptionMode.DrawException;
                shapeLayer = new ShapeFileFeatureLayer();
                gpxLayer = new GpxFeatureLayer();
                kmlLayer = new KmlFeatureLayer();
                selectedlayer = new InMemoryFeatureLayer();
                currentLayer = new InMemoryFeatureLayer();
                log.Info("ImportTools - Load import file: {0}.", newfilenamewithpath);
                if (File.Exists(newfilenamewithpath)) {
                    string result = Path.GetExtension(newfilenamewithpath).Replace(".", "");
                    if (result == "shp") {
                        shapeLayer = new ShapeFileFeatureLayer(newfilenamewithpath, ShapeFileReadWriteMode.ReadOnly);
                        ProjectToPRJFile();
                        shapeLayer.RequireIndex = false;
                        EnsureCurrentLayerIsOpen();
                        Collection<Feature> allfeatures = shapeLayer.FeatureSource.GetAllFeatures(ReturningColumnsType.AllColumns);
                        foreach (Feature feat in allfeatures) {
                            currentLayer.InternalFeatures.Add(feat);
                            BaseShape bs = feat.GetShape();
                            if (bs is PolygonShape) {
                                PolygonShape poly = bs as PolygonShape;
                            }
                            if (bs is MultipolygonShape) {
                                MultipolygonShape mpoly = bs as MultipolygonShape;
                            }
                        }
                    }
                    if (result == "gpx") {
                        gpxLayer = new GpxFeatureLayer(newfilenamewithpath);
                        ProjectToPRJFile();
                        EnsureCurrentLayerIsOpen();
                        Collection<Feature> allfeatures = gpxLayer.FeatureSource.GetAllFeatures(ReturningColumnsType.AllColumns);
                        foreach (Feature feat in allfeatures) {
                            BaseShape bs = feat.GetShape();
                            if (bs is MultilineShape) {
                                MultilineShape mline = bs as MultilineShape;
                                PolygonShape poly = new PolygonShape(new RingShape(mline.Lines[0].Vertices));
                                MultipolygonShape mpoly = new MultipolygonShape();
                                mpoly.Polygons.Add(poly);
                                currentLayer.InternalFeatures.Add(new Feature(mpoly, feat.ColumnValues));
                            }
                            if (bs is PolygonShape) {
                                PolygonShape poly = bs as PolygonShape;
                                currentLayer.InternalFeatures.Add(feat);
                            }
                            if (bs is MultipolygonShape) {
                                MultipolygonShape mpoly = bs as MultipolygonShape;
                                currentLayer.InternalFeatures.Add(feat);
                            }
                        }
                    }
                    if (result == "kml") {
                        kmlLayer = new KmlFeatureLayer(newfilenamewithpath);
                        kmlLayer.RequireIndex = false;
                        //KmlFeatureLayer.BuildIndexFile(newfilenamewithpath, BuildIndexMode.DoNotRebuild);
                        ProjectToPRJFile();
                        EnsureCurrentLayerIsOpen();
                        if (!kmlLayer.FeatureSource.IsOpen)
                        {
                            kmlLayer.FeatureSource.Open();
                        }
                        //if (!kmlLayer.IsOpen)
                        //{
                        //    kmlLayer.Open();
                        //}
                        Collection<Feature> allfeatures = kmlLayer.FeatureSource.GetAllFeatures(ReturningColumnsType.AllColumns);
                        foreach (Feature feat in allfeatures)
                        {
                            BaseShape bs = feat.GetShape();
                            if (bs is MultilineShape)
                            {
                                MultilineShape mline = bs as MultilineShape;
                                PolygonShape poly = new PolygonShape(new RingShape(mline.Lines[0].Vertices));
                                MultipolygonShape mpoly = new MultipolygonShape();
                                mpoly.Polygons.Add(poly);
                                currentLayer.InternalFeatures.Add(new Feature(mpoly, feat.ColumnValues));
                            }
                            if (bs is PolygonShape)
                            {
                                PolygonShape poly = bs as PolygonShape;
                                currentLayer.InternalFeatures.Add(feat);
                            }
                            if (bs is MultipolygonShape)
                            {
                                MultipolygonShape mpoly = bs as MultipolygonShape;
                                currentLayer.InternalFeatures.Add(feat);
                            }
                        }
                    }
                    if (result == "geojson")
                    {
                        Maps.JsonClass.FeatureCollection allfeatures = Newtonsoft.Json.JsonConvert.DeserializeObject<Maps.JsonClass.FeatureCollection>(File.ReadAllText(newfilenamewithpath));
                        foreach (Maps.JsonClass.Feature jsonfeat in allfeatures.features)
                        {
                            Feature feat = Feature.CreateFeatureFromGeoJson(Newtonsoft.Json.JsonConvert.SerializeObject(jsonfeat.ToString()));
                            BaseShape bs = feat.GetShape();
                            if (bs is PointShape)
                            {
                                PointShape poly = bs as PointShape;
                                currentLayer.InternalFeatures.Add(feat);
                            }
                            if (bs is MultilineShape)
                            {
                                MultilineShape mline = bs as MultilineShape;
                                PolygonShape poly = new PolygonShape(new RingShape(mline.Lines[0].Vertices));
                                MultipolygonShape mpoly = new MultipolygonShape();
                                mpoly.Polygons.Add(poly);
                                currentLayer.InternalFeatures.Add(new Feature(mpoly, feat.ColumnValues));
                            }
                            if (bs is LineShape)
                            {
                                LineShape poly = bs as LineShape;
                                currentLayer.InternalFeatures.Add(feat);
                            }
                            if (bs is PolygonShape)
                            {
                                PolygonShape poly = bs as PolygonShape;
                                currentLayer.InternalFeatures.Add(feat);
                            }
                            if (bs is MultipolygonShape)
                            {
                                MultipolygonShape mpoly = bs as MultipolygonShape;
                                currentLayer.InternalFeatures.Add(feat);
                            }
                        }
                    }
                    AreaStyle currentLayerStyle = AreaStyles.CreateSimpleAreaStyle(new GeoColor(80, GeoColor.SimpleColors.BrightYellow), new GeoColor(125, GeoColor.SimpleColors.Blue), 2);
                    currentLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = currentLayerStyle;
                    currentLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
                    currentLayer.FeatureSource.Projection = projection;
                    layerOverlay.Layers.Add("ImportLayer", currentLayer);
                    AreaStyle selectedLayerStyle = AreaStyles.CreateSimpleAreaStyle(new GeoColor(125, GeoColor.SimpleColors.LightRed), new GeoColor(255, GeoColor.SimpleColors.Red), 2);
                    selectedlayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = selectedLayerStyle;
                    selectedlayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
                    selectedlayer.FeatureSource.Projection = projection;
                    layerOverlay.Layers.Add("SelectedLayer", selectedlayer);
                    log.Info("ImportTools - Import shapefile loaded.");
                    return true;
                }
                else {
                    log.Info("ImportTools - File cannot be imported.");
                    OnCancelCropzone();
                    return false;
                }
            }
            catch (Exception ex) {
                log.ErrorException("ImportTools - failed to load.", ex);
                return false;
            }
        }
            //        KmlFeatureLayer layer = new KmlFeatureLayer("..\\..\\App_Data\\KML_Samples.kml");
            //layer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;

            //layer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = new AreaStyle(new GeoPen(GeoColor.SimpleColors.Black), new GeoSolidBrush(GeoColor.SimpleColors.Yellow));
            //layer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = new LineStyle(new GeoPen(GeoColor.SimpleColors.Blue, 5));
            //layer.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = new PointStyle(PointSymbolType.Circle, new GeoSolidBrush(GeoColor.SimpleColors.Green), 10);

            //LayerOverlay stateOverlay = new LayerOverlay();
            //stateOverlay.Layers.Add("KmlLayer", layer);
            //winformsMap1.Overlays.Add("StateOverlay", stateOverlay);


        private void ProjectToPRJFile() {
            string tempFileBase = Path.Combine(Path.GetDirectoryName(newfilenamewithpath), Path.GetFileNameWithoutExtension(newfilenamewithpath));
            if (string.IsNullOrWhiteSpace(overrideprojection)) {
                newprojectionfilenamewithpath = string.Format("{0}.prj", tempFileBase);
                if (File.Exists(newprojectionfilenamewithpath)) {
                    Proj4Projection proj4 = new Proj4Projection();
                    string prjFileText = System.IO.File.ReadAllText(newprojectionfilenamewithpath);
                    proj4.InternalProjectionParametersString = Proj4Projection.ConvertPrjToProj4(prjFileText);
                    if (string.IsNullOrEmpty(proj4.InternalProjectionParametersString)) { return; }
                    proj4.ExternalProjectionParametersString = Proj4Projection.GetGoogleMapParametersString();
                    projection = new Proj4Projection(proj4.InternalProjectionParametersString, proj4.ExternalProjectionParametersString);
                }
            }
            else {
                if (overrideprojection.Length < 7) {
                    Proj4Projection proj4 = new Proj4Projection();
                    proj4.InternalProjectionParametersString = Proj4Projection.GetEpsgParametersString(System.Convert.ToInt32(overrideprojection));
                    if (string.IsNullOrEmpty(proj4.InternalProjectionParametersString)) { return; }
                    proj4.ExternalProjectionParametersString = Proj4Projection.GetGoogleMapParametersString();
                    projection = new Proj4Projection(proj4.InternalProjectionParametersString, proj4.ExternalProjectionParametersString);
                }
                else {
                    Proj4Projection proj4 = new Proj4Projection();
                    proj4.InternalProjectionParametersString = overrideprojection;
                    if (string.IsNullOrEmpty(proj4.InternalProjectionParametersString)) { return; }
                    proj4.ExternalProjectionParametersString = Proj4Projection.GetGoogleMapParametersString();
                    projection = new Proj4Projection(proj4.InternalProjectionParametersString, proj4.ExternalProjectionParametersString);
                }
            }
        }

        private void LoadShapeFileOverlayOntoMap() {
            log.Info("ImportTools - Load the import overlay and set extents.");
            if (Map.Overlays.Contains("importOverlay")) {
                Map.Overlays.Remove("importOverlay");
            }
            if (layerOverlay.Layers.Contains("ImportLayer")) {
                if (!string.IsNullOrEmpty(FileName)) {
                    Map.Overlays.Add("importOverlay", layerOverlay);
                }
            }

            EnsureCurrentLayerIsOpen();
            var extent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
            try {
                var shapelayerfeatures = currentLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);
                if (shapelayerfeatures.Count > 0) {
                    Map.CurrentExtent = currentLayer.GetBoundingBox();
                    //var shapelayercolumns = currentLayer.FeatureSource.GetColumns();
                    //extent = ExtentHelper.GetBoundingBoxOfItems(shapelayerfeatures);
                    //Map.CurrentExtent = extent;
                    //PointShape center = extent.GetCenterPoint();
                    //Map.CenterAt(center);

                }
                else {
                    Map.CurrentExtent = extent;
                    //PointShape center = extent.GetCenterPoint();
                    //Map.CenterAt(center);
                }
            }
            catch {
                Map.CurrentExtent = extent;
                //PointShape center = extent.GetCenterPoint();
                //Map.CenterAt(center);
            }
        }

        private void EnsureCurrentLayerIsOpen() {
            if (currentLayer == null) {
                currentLayer = new InMemoryFeatureLayer();
            }

            if (!currentLayer.IsOpen) {
                currentLayer.Open();
            }

            if (selectedlayer == null) {
                selectedlayer = new InMemoryFeatureLayer();
            }

            if (!selectedlayer.IsOpen) {
                selectedlayer.Open();
            }

            if (shapeLayer == null) {
                shapeLayer = new ShapeFileFeatureLayer();
            }

            if (!string.IsNullOrWhiteSpace(shapeLayer.ShapePathFileName)) {
                if (!shapeLayer.IsOpen) {
                    shapeLayer.Open();
                }
            }

            if (gpxLayer == null) {
                gpxLayer = new GpxFeatureLayer();
            }

            if (!string.IsNullOrWhiteSpace(gpxLayer.GpxPathFilename)) {
                if (!gpxLayer.IsOpen) {
                    gpxLayer.Open();
                }
            }

            if (kmlLayer == null) {
                kmlLayer = new KmlFeatureLayer();
            }

            //if (!string.IsNullOrWhiteSpace(kmlLayer.KmlPathFilename)) {
            //    if (!kmlLayer.IsOpen) {
            //        kmlLayer.Open();
            //    }
            //}

            if (projection != null) {
                if (!projection.IsOpen) {
                    projection.Open();
                }
            }
        }

        void Cancel()
        {
            log.Info("ImportTools - Cancel import shape.");
            HasUnsavedChanges = false;
            Map.Cursor = Cursors.Arrow;
            var importmessage = new ShapeImportMessage() { ShapeBase = new MultipolygonShape() };
            Messenger.Default.Send<ShapeImportMessage>(importmessage);
        }

        void Complete() {
            HasUnsavedChanges = false;
            SavePolygon();
        }

        private void SavePolygon() {
            try {
                if (importpoly.Polygons.Count > 0) {
                    log.Info("ImportTools - Save import shape.");
                    Map.Overlays["importOverlay"].Refresh();
                    Map.Cursor = Cursors.Arrow;
                    ShapeValidationResult result = importpoly.Validate(ShapeValidationMode.Advanced);
                    if (result.IsValid) {
                        MultipolygonShape multipolygon1 = projection.ConvertToInternalProjection(importpoly) as MultipolygonShape;
                        MultipolygonShape multipolygon2 = projection.ConvertToExternalProjection(importpoly) as MultipolygonShape;

                        var importmessage = new ShapeImportMessage() { ShapeBase = multipolygon2 };
                        Messenger.Default.Send<ShapeImportMessage>(importmessage);
                    }
                    else {
                        log.Info($"ImportTools - Import shape is not valid: {result.ValidationErrors}");
                        System.Windows.MessageBox.Show($"{Strings.ImportTools_Text} - {Strings.ImportShapeIsNotValid_Text}: {result.ValidationErrors}");
                    }
                }
            }
            catch (Exception ex) {
                log.ErrorException("Caught an exception while importing a shape", ex);
                System.Windows.MessageBox.Show($"{Strings.ImportTools_Text} - {Strings.Exception_ImportingShape_Text}.  {Strings.ImportAborted_Text}");
                OnCancelCropzone();
            }
        }

        internal void ExportShapeFile()
        {
            string filename = AddLayersBySaveFileDialog();
            //string flatfilename = Path.GetFileNameWithoutExtension(filename);
            //string directoryname = Path.GetDirectoryName(filename);
            //string jsonfilename = $"{flatfilename}.geojson";
            //string geojsonfilename = Path.Combine(directoryname, jsonfilename);
            //filename = geojsonfilename;
            if (string.IsNullOrEmpty(filename)) { return; }
            if (filename.EndsWith(".shp"))
            {
                try
                {
                    Collection<FeatureSourceColumn> layercolumns = shapeLayer.FeatureSource.GetColumns();
                    List<DbfColumn> columns = new List<DbfColumn>();
                    log.Info("MapDisplay - Export: build columns");
                    foreach (FeatureSourceColumn fsc in layercolumns)
                    {
                        DbfColumnType coltype = DbfColumnType.Character;
                        if (fsc.TypeName == "Interger") { coltype = DbfColumnType.IntegerInBinary; }
                        if (fsc.TypeName == "Double") { coltype = DbfColumnType.DoubleInBinary; }
                        if (fsc.TypeName == "Date") { coltype = DbfColumnType.Date; }
                        if (fsc.TypeName == "Logical") { coltype = DbfColumnType.Logical; }
                        if (fsc.TypeName == "Memo") { coltype = DbfColumnType.Memo; }
                        columns.Add(new DbfColumn(fsc.ColumnName, coltype, fsc.MaxLength, 0));
                    }

                    var exportfeatures = shapeLayer.FeatureSource.GetAllFeatures(ReturningColumnsType.AllColumns) ;
                    string farmnamecolumn = "Farm Name";
                    foreach (FeatureSourceColumn fsc in layercolumns)
                    {
                        if (fsc.ColumnName == "Farm Name")
                        {
                            farmnamecolumn = "Farm Name";
                            break;
                        }
                        if (fsc.ColumnName == "Farm")
                        {
                            farmnamecolumn = "Farm";
                            break;
                        }
                        if (fsc.ColumnName == "Farmname")
                        {
                            farmnamecolumn = "Farmname";
                            break;
                        }
                        if (fsc.ColumnName == "FARM_NAME")
                        {
                            farmnamecolumn = "FARM_NAME";
                            break;
                        }
                    }
                    Dictionary<string, List<Feature>> farmList = new Dictionary<string, List<Feature>>();
                    Dictionary<string, List<Feature>> farmListtest = new Dictionary<string, List<Feature>>();
                    foreach (Feature featr in exportfeatures)
                    {
                        if (!farmList.ContainsKey(featr.ColumnValues[farmnamecolumn]))
                        {
                            var thisfarm = shapeLayer.QueryTools.GetFeaturesByColumnValue(farmnamecolumn, featr.ColumnValues[farmnamecolumn]);
                            farmList.Add(featr.ColumnValues[farmnamecolumn], thisfarm.ToList<Feature>());
                        }
                    }

                    string fulldirectoryname1 = Path.GetDirectoryName(filename);
                    var fulldirectoryname = Path.Combine(fulldirectoryname1, @"ExportShapeFiles");
                    if (!Directory.Exists(fulldirectoryname))
                    {
                        Directory.CreateDirectory(fulldirectoryname);
                    }
                    farmListtest = new Dictionary<string, List<Feature>>();
                    int farmcount = 0;
                    foreach (var farmfields in farmList)
                    {
                        try
                        {
                            farmcount++;
                            string key = farmfields.Key;
                            string newkey = string.Join("_", key.Split(Path.GetInvalidFileNameChars()));
                            newkey = $"{newkey}-{farmcount}";
                            //if (farmListtest.ContainsKey(newkey))
                            //{
                            //    newkey = $"{newkey}-{farmcount}";
                            //}
                            farmListtest.Add(newkey, new List<Feature>());
                            List<Feature> featurelist = farmfields.Value;
                            var fullfilename = Path.Combine(fulldirectoryname, newkey + @".shp");
                            ShapeFileFeatureLayer shapeFileFeatureLayer2 = CreateNewFeaturesShapeFile(fullfilename, columns, ShapeFileType.Polygon);
                            shapeFileFeatureLayer2.FeatureSource.Projection = shapeLayer.FeatureSource.Projection;
                            shapeFileFeatureLayer2.RequireIndex = false;

                            log.Info("MapDisplay - Export: load shapes");
                            shapeFileFeatureLayer2.Open();
                            shapeFileFeatureLayer2.EditTools.BeginTransaction();
                            foreach (var feat in featurelist)
                            {
                                shapeFileFeatureLayer2.FeatureSource.AddFeature(feat);
                            }
                            shapeFileFeatureLayer2.EditTools.CommitTransaction();
                            shapeFileFeatureLayer2.Close();
                        } catch(Exception ex2)
                        {
                            int uuuuu = 0;
                        }
                    }
                } catch (Exception ex1)
                {
                    int dddd = 0;
                }
                MessageBox.Show(Strings.ExportShapesComplete_Text);
                return;
            }
            if (filename.EndsWith(".kml"))
            {
                SharpKml.Dom.Document document = new SharpKml.Dom.Document();
                document.Name = filename;
                document.Open = false;

                GeoCollection<Feature> exportfeatures = new GeoCollection<Feature>();
                exportfeatures = currentLayer.InternalFeatures;

                foreach (Feature feature in exportfeatures)
                {
                    BaseShape baseshape = feature.GetShape();
                    if (baseshape == null) { continue; }
                    try
                    {
                        if (baseshape is PointShape)
                        {
                            PointShape centerpoint = baseshape.GetCenterPoint();
                            Feature newfeat = new Feature(centerpoint, feature.ColumnValues);
                            SharpKml.Dom.Placemark placemark = TranslatePointFeatureToKML(newfeat);
                            document.AddFeature(placemark);
                        }
                        else
                        {
                            Feature newfeat = new Feature(baseshape, feature.ColumnValues);
                            SharpKml.Dom.Placemark placemark = TranslateMultipolygonFeatureToKML(newfeat);
                            document.AddFeature(placemark);
                        }
                    }
                    catch { }
                }
                var kml = new SharpKml.Dom.Kml();
                kml.Feature = document;
                SharpKml.Engine.KmlFile kmlfile = SharpKml.Engine.KmlFile.Create(kml, false);
                using (var stream = System.IO.File.OpenWrite(filename))
                {
                    kmlfile.Save(stream);
                }
                MessageBox.Show(Strings.ExportShapesComplete_Text);
                return;
            }
            if (filename.EndsWith(".geojson"))
            {
                int nrecords = 0;
                GeoCollection<Feature> exportfeatures = new GeoCollection<Feature>();
                exportfeatures = currentLayer.InternalFeatures;

                StringBuilder geojsonfeaturelist = new StringBuilder();
                foreach (Feature feature in exportfeatures)
                {
                    nrecords++;
                    BaseShape baseshape = feature.GetShape();
                    if (baseshape == null) { continue; }
                    try
                    {
                        Dictionary<string, string> featcolumns = feature.ColumnValues;
                        Dictionary<string, string> newcolumns = new Dictionary<string, string>();

                        foreach (var col in featcolumns)
                        {
                            string fscname = col.Key;
                            string fscname2= col.Key;
                            if (fscname.Contains(" "))
                            {
                                StringBuilder sb = new StringBuilder(fscname);
                                sb.Replace(' ', '_');
                                fscname = sb.ToString();
                            }
                            newcolumns.Add(fscname, col.Value);
                        }
                        Feature newfeature = new Feature(baseshape, newcolumns);

                        string geojsonfeature = newfeature.GetGeoJson();
                        if (geojsonfeaturelist.Length == 0)
                        {
                            geojsonfeaturelist = new StringBuilder(geojsonfeature);
                        }
                        else
                        {
                            geojsonfeaturelist.Append(",");
                            geojsonfeaturelist.Append(geojsonfeature);

                        }
                    }
                    catch (Exception ex)
                    {
                        int xxxx = 0;
                    }
                }
                string ob = "{";
                string cb = "}";
                string jsoncontent = $"{ob}\"type\":\"FeatureCollection\",\"features\":[{geojsonfeaturelist}]{cb}";
                File.WriteAllText(filename, jsoncontent);
                MessageBox.Show(Strings.ExportShapesComplete_Text);
                return;
            }
        }

        internal string AddLayersBySaveFileDialog()
        {
            SaveFileDialog savefiledialog = new SaveFileDialog();
            savefiledialog.OverwritePrompt = true;
            savefiledialog.Title = Strings.ExportShapesFile_Text;
            savefiledialog.DefaultExt = "shp";
            savefiledialog.Filter = "Shape files (*.shp)|*.shp|kml files (*.kml)|*.kml|geojson files (*.geojson)|*.geojson";
            bool? dialogresult = savefiledialog.ShowDialog();
            if (dialogresult.HasValue && dialogresult.Value)
            {
                return savefiledialog.FileName;
            }
            return string.Empty;
        }

        public ShapeFileFeatureLayer CreateNewFeaturesShapeFile(string newfilenamewithpath, List<DbfColumn> columns, ShapeFileType shapefiletype)
        {
            ShapeFileFeatureSource.CreateShapeFile(shapefiletype, newfilenamewithpath, columns);
            ShapeFileFeatureLayer shapeFileFeatureLayer1 = new ShapeFileFeatureLayer(newfilenamewithpath, ShapeFileReadWriteMode.ReadWrite);
            return shapeFileFeatureLayer1;
        }

        public SharpKml.Dom.Placemark TranslateMultipolygonFeatureToKML(ThinkGeo.MapSuite.Core.Feature feature)
        {
            string name = feature.ColumnValues[@"LdbLabel"];
            MultipolygonShape mpoly = feature.GetShape() as MultipolygonShape;
            if (mpoly == null) { return null; }
            byte byte_Color_R = 150, byte_Color_G = 150, byte_Color_B = 150, byte_Color_A = 100; //you may get your own color by other method
            var style = new SharpKml.Dom.Style();
            style.Polygon = new SharpKml.Dom.PolygonStyle();
            style.Polygon.ColorMode = SharpKml.Dom.ColorMode.Normal;
            style.Polygon.Color = new SharpKml.Base.Color32(byte_Color_A, byte_Color_B, byte_Color_G, byte_Color_R);
            SharpKml.Dom.MultipleGeometry multiplegeometry = new SharpKml.Dom.MultipleGeometry();
            int i = 0;
            foreach (var poly in mpoly.Polygons)
            {
                i++;
                SharpKml.Dom.Polygon polygon = new SharpKml.Dom.Polygon();
                polygon.Extrude = true;
                polygon.AltitudeMode = SharpKml.Dom.AltitudeMode.ClampToGround;
                //Color Style Setting:
                SharpKml.Dom.OuterBoundary outerBoundary = new SharpKml.Dom.OuterBoundary();
                outerBoundary.LinearRing = new SharpKml.Dom.LinearRing();
                outerBoundary.LinearRing.Coordinates = new SharpKml.Dom.CoordinateCollection();
                foreach (Vertex vertex in poly.OuterRing.Vertices)
                {
                    outerBoundary.LinearRing.Coordinates.Add(new SharpKml.Base.Vector(vertex.Y, vertex.X));
                }
                polygon.OuterBoundary = outerBoundary;
                foreach (RingShape ringshape in poly.InnerRings)
                {
                    SharpKml.Dom.InnerBoundary innerboundary = new SharpKml.Dom.InnerBoundary();
                    innerboundary.LinearRing = new SharpKml.Dom.LinearRing();
                    innerboundary.LinearRing.Coordinates = new SharpKml.Dom.CoordinateCollection();
                    foreach (Vertex vertex in ringshape.Vertices)
                    {
                        innerboundary.LinearRing.Coordinates.Add(new SharpKml.Base.Vector(vertex.Y, vertex.X));
                    }
                    polygon.AddInnerBoundary(innerboundary);
                }
                multiplegeometry.AddGeometry(polygon);
            }
            SharpKml.Dom.Placemark placemark = new SharpKml.Dom.Placemark();
            placemark.Name = name;
            placemark.Geometry = multiplegeometry;
            placemark.AddStyle(style);
            return placemark;
        }

        public SharpKml.Dom.Placemark TranslatePointFeatureToKML(ThinkGeo.MapSuite.Core.Feature feature)
        {
            string name = feature.ColumnValues[@"LdbLabel"];
            PointShape thispoint = feature.GetShape() as PointShape;
            if (thispoint == null) { return null; }
            SharpKml.Dom.Point point = new SharpKml.Dom.Point();
            point.Coordinate = new SharpKml.Base.Vector(thispoint.Y, thispoint.X);
            point.AltitudeMode = SharpKml.Dom.AltitudeMode.RelativeToGround;

            var iconstyle = new SharpKml.Dom.Style();
            iconstyle.Id = "randomColorIcon";
            iconstyle.Icon = new SharpKml.Dom.IconStyle();
            iconstyle.Icon.Color = new SharpKml.Base.Color32(255, 0, 255, 0);
            iconstyle.Icon.ColorMode = SharpKml.Dom.ColorMode.Random;
            iconstyle.Icon.Icon = new SharpKml.Dom.IconStyle.IconLink(new Uri("http://maps.google.com/mapfiles/kml/pal3/icon21.png"));
            iconstyle.Icon.Scale = 1.1;

            SharpKml.Dom.Placemark placemark = new SharpKml.Dom.Placemark();
            placemark.Name = name;
            placemark.Geometry = point;
            placemark.AddStyle(iconstyle);
            return placemark;
        }
    }
}
