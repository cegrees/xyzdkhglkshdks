﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GalaSoft.MvvmLight;
using ThinkGeo.MapSuite.WpfDesktopEdition;
using ThinkGeo.MapSuite.Core;
using Landdb.Infrastructure;
using System.IO;
using Microsoft.Win32;
using GalaSoft.MvvmLight.Command;
using System.Windows.Input;
using System.Collections.ObjectModel;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure.Messages;
using Landdb.MapStyles;
using Landdb.ViewModel.Maps.ImageryOverlay;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Client.Infrastructure;
using Landdb.ViewModel.Fields.FieldDetails;
using NLog;
using Landdb.Domain.ReadModels.MapAnnotation;
using System.Windows;
using System.Windows.Data;
using System.Windows.Threading;
using AgC.UnitConversion;
using Landdb.ViewModel.Fields.FieldDetails.MapModels;
using Landdb.ViewModel.Fields;
using Landdb.ViewModel.Yield;
using Landdb.Domain.ReadModels.YieldLocation;
using Landdb.Resources;

namespace Landdb.ViewModel.Secondary.Map {
    public class AnnotationsViewModel : ViewModelBase
    {
        IClientEndpoint clientEndpoint;
        readonly Dispatcher dispatcher;
        MapViewModel parent;

        Guid currentDataSourceId;
        int currentCropYear;
        Logger log = LogManager.GetCurrentClassLogger();
        bool isLoadingTree;
        bool hasunsavedchanges = false;
        bool canLoadAnnotations = false;
        bool importshapes = false;
        //bool sortAlphabetically = userSettings.AreFieldsSortedAlphabetically;
        bool sortAlphabetically = true;
        string areaUnitString = string.Empty;
        string coordinateformat = string.Empty;
        AgC.UnitConversion.Area.AreaUnit agcAreaUnit;
        ThinkGeo.MapSuite.Core.AreaUnit mapAreaUnit;
        ThinkGeo.MapSuite.Core.DistanceUnit mapDistanceUnit;
        int fieldopacity = 125;
        int annotationopacity = 125;
        MapSettings mapsettings;
        string fieldsselectedfillcolor;
        string fieldsunselectedfillcolor;
        string fieldsselectedbordercolor;
        string fieldsunselectedbordercolor;
        GeoColor fieldsselectedGeoColor = GeoColor.StandardColors.Yellow;
        GeoColor fieldsunselectedGeoColor = GeoColor.StandardColors.Gray;
        GeoColor fieldsselectedborderGeoColor = new GeoColor();
        GeoColor fieldsunselectedborderGeoColor = new GeoColor();

        IList<MapItem> clientLayerItems;
        IList<MapItem> fieldtreeshapes;
        InMemoryFeatureLayer fieldsLayer = new InMemoryFeatureLayer();
        InMemoryFeatureLayer currentLayer = new InMemoryFeatureLayer();
        InMemoryFeatureLayer importLayer = new InMemoryFeatureLayer();
        InMemoryFeatureLayer measureLayer = new InMemoryFeatureLayer();
        bool maploaded = false;
        bool startedOutOnline = false;
        bool isOptionsMapPopupOpen = false;
        LayerOverlay layerOverlay = new LayerOverlay();
        LayerOverlay fieldOverlay = new LayerOverlay();
        PopupOverlay popupOverlay = new PopupOverlay();
        Proj4Projection projection;
        Feature selectedfield = new Feature();
        Feature loadedfeature = new Feature();
        RectangleShape currentmapextent = new RectangleShape();
        bool drawingpivot = true;
        double radius = 0;
        double pivotpoints = 36;
        double pointx = 0;
        double pointy = 0;
        BaseShape fieldshape = new MultipolygonShape();
        Cursor previousCursor;
        MapStyles.AnnotationMultiStyle customMultiStyles = new MapStyles.AnnotationMultiStyle();
        List<AnnotationStyleItem> changedstyles = new List<AnnotationStyleItem>();
        Dictionary<string, MapAnnotationCategory> userstylenames = new Dictionary<string, MapAnnotationCategory>();
        AreaStyle unselectedarea = new AreaStyle(new GeoSolidBrush(GeoColor.FromArgb(255, GeoColor.StandardColors.Transparent)));
        LineStyle unselectedline = new LineStyle(new GeoPen(GeoColor.FromArgb(255, GeoColor.StandardColors.Transparent), 2));
        PointStyle unselectedpoint = PointStyles.CreateSimplePointStyle(PointSymbolType.Circle, new GeoColor(255, GeoColor.StandardColors.Transparent), 10);
        LineStyle lineStylePen = new LineStyle(new GeoPen(GeoColor.FromArgb(150, GeoColor.StandardColors.Orange), 4));
        AreaStyle areaStyleBrush = new AreaStyle(new GeoSolidBrush(GeoColor.StandardColors.Blue));
        ClientLayerAreaStyle fieldsLayerAreaStyle = null;
        ScalingTextStyle scalingTextStyle = new ScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, 24, 2);
        AnnotationScalingTextStyle scalingTextStyle2 = new AnnotationScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, 24, 2);
        CreateAnnotationShapeViewModel createItemViewModel;
        EditAnnotationCategoryViewModel editCategoryViewModel;
        ImportAnnotationViewModel importAnnotationViewModel;
        IStyleFactory styleFactory;
        ZoomLevelFactory zoomlevelfactory = new ZoomLevelFactory();
        private IList<IDomainCommand> commandlist = new List<IDomainCommand>();

        public AnnotationsViewModel(IClientEndpoint clientEndpoint, MapViewModel mapViewModel, Dispatcher dispatcher)
        {
            this.clientEndpoint = clientEndpoint;
            this.parent = mapViewModel;
            this.dispatcher = dispatcher;
            this.currentDataSourceId = ApplicationEnvironment.CurrentDataSourceId;
            this.currentCropYear = ApplicationEnvironment.CurrentCropYear;

            styleFactory = new StyleFactory();
            changedstyles = new List<AnnotationStyleItem>();
            userstylenames = new Dictionary<string, MapAnnotationCategory>();
            commandlist = new List<IDomainCommand>();

            InitializeSettings();
            scalingTextStyle = new ScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, mapsettings.ScalingTextMaximumSize, this.mapsettings.ScalingTextMinimumSize);
            scalingTextStyle2 = new AnnotationScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, mapsettings.ScalingTextMaximumSize, this.mapsettings.ScalingTextMinimumSize);
            scalingTextStyle2.ColumnsList = new List<string>() { @"Id", @"LdbLabel", @"TypeName", @"ShapeType" };

            fieldsselectedGeoColor = GeoColor.StandardColors.Yellow;
            fieldsunselectedGeoColor = GeoColor.StandardColors.Gray;
            GeoColor fieldsunselectedGeoColor2 = GeoColor.StandardColors.Gray;

            fieldsselectedfillcolor = mapsettings.FieldsSelectedFillColor;
            fieldsunselectedfillcolor = mapsettings.FieldsUnSelectedFillColor;
            string fieldsunselectedfillcolor2 = GeoColor.ToHtml(fieldsunselectedGeoColor2);
            if (!string.IsNullOrEmpty(fieldsselectedfillcolor)) {
                fieldsselectedGeoColor = GeoColor.FromHtml(fieldsselectedfillcolor);
            }
            if (!string.IsNullOrEmpty(fieldsunselectedfillcolor)) {
                fieldsunselectedGeoColor = GeoColor.FromHtml(fieldsunselectedfillcolor);
                fieldsunselectedGeoColor2 = GeoColor.FromHtml(fieldsunselectedfillcolor2);
            }

            fieldsselectedbordercolor = mapsettings.FieldsSelectedBorderColor;
            fieldsunselectedbordercolor = mapsettings.FieldsUnSelectedBorderColor;
            if (!string.IsNullOrEmpty(fieldsselectedbordercolor)) {
                fieldsselectedborderGeoColor = GeoColor.FromHtml(fieldsselectedbordercolor);
            }
            else {
                fieldsselectedborderGeoColor = fieldsselectedGeoColor;
            }
            if (!string.IsNullOrEmpty(fieldsunselectedbordercolor)) {
                fieldsunselectedborderGeoColor = GeoColor.FromHtml(fieldsunselectedbordercolor);
            }
            else {
                fieldsunselectedborderGeoColor = fieldsunselectedGeoColor;
            }

            YieldLocationList = new ObservableCollection<YieldLocationListItemViewModel>();
            //RefreshDestinationLocationList();

            if (maploaded) { return; }
            //canLoadAnnotations = true;
            InitializeMap();
            areaStyleBrush = new AreaStyle(new GeoPen(new GeoSolidBrush(new GeoColor(50, GeoColor.StandardColors.Yellow)), 3), new GeoSolidBrush(GeoColor.StandardColors.Blue));

            Messenger.Default.Register<DataSourceChangedMessage>(this, OnDataSourceChanged);
            Messenger.Default.Register<RemoteEventReceived>(this, OnRemoteEventReceived);

            createItemViewModel = new CreateAnnotationShapeViewModel(this.clientEndpoint, OnNewItemCreated, OnNewItemIgnored) { CurrentCropYear = ApplicationEnvironment.CurrentCropYear, CurrentDataSourceId = ApplicationEnvironment.CurrentDataSourceId };
            editCategoryViewModel = new EditAnnotationCategoryViewModel(this.clientEndpoint, OnEditItemModified, OnNewItemIgnored) { CurrentCropYear = ApplicationEnvironment.CurrentCropYear, CurrentDataSourceId = ApplicationEnvironment.CurrentDataSourceId };
            importAnnotationViewModel = new ImportAnnotationViewModel(this.clientEndpoint, OnImportItemCompleted, OnNewItemIgnored) { CurrentCropYear = ApplicationEnvironment.CurrentCropYear, CurrentDataSourceId = ApplicationEnvironment.CurrentDataSourceId };
            ImportAnnotationFileCommand = new RelayCommand(ImportFile);
            CreateNewItemCommand = new RelayCommand(CreateNewItem);
            EditItemCommand = new RelayCommand(EditItem);
            RemoveItemCommand = new RelayCommand(RemoveItem);
            ExitAnnotationCommand = new RelayCommand(ExitAnnotation);
            SaveAnnotationCommand = new RelayCommand(SaveAnnotation);
            FinishCommand = new RelayCommand(Finish);
            RefreshAnnotationsMapCommand = new RelayCommand(RefreshImageryLayer);

            FilterModel = new TreeFilterModel(clientEndpoint);
            //if (currentDataSourceId != Guid.Empty && currentCropYear > 1980) {
            //    canLoadAnnotations = true;
            //    LoadAnnotations();
            //}
        }

        public ICommand ImportAnnotationFileCommand { get; private set; }
        public ICommand CreateNewItemCommand { get; private set; }
        public ICommand EditItemCommand { get; private set; }
        public ICommand RemoveItemCommand { get; private set; }
        public ICommand SaveAnnotationCommand { get; private set; }
        public ICommand ExitAnnotationCommand { get; private set; }
        public ICommand FinishCommand { get; private set; }
        public ICommand RefreshAnnotationsMapCommand { get; private set; }

        public bool IsLoadingTree {
            get { return isLoadingTree; }
            set {
                isLoadingTree = value;
                RaisePropertyChanged("IsLoadingTree");
            }
        }

        public bool MapLoaded {
            get { return maploaded; }
            set {
                maploaded = value;
                RaisePropertyChanged("MapLoaded");
            }
        }

        public WpfMap Map { get; set; }

        static object clientLayerLock = new object();
        public IList<MapItem> ClientLayer {
            get { return clientLayerItems; }
            set {
                lock (clientLayerLock) {
                    clientLayerItems = value;
                    EnsureFieldsLayerIsOpen();
                    fieldsLayer.InternalFeatures.Clear();
                    foreach (var m in clientLayerItems) {
                        if (m.MapData != null && !string.IsNullOrEmpty(m.MapData) && m.MapData != "MULTIPOLYGON EMPTY") {
                            try {
                                IDictionary<string, string> columnvalues = new Dictionary<string, string>();
                                columnvalues.Add(@"LdbLabel", m.Name);
                                var f = new Feature(m.MapData, m.FieldId.ToString(), columnvalues);
                                if (fieldsLayer.InternalFeatures.Contains(f)) { continue; }
                                fieldsLayer.InternalFeatures.Add(m.FieldId.ToString(), f);
                            }
                            catch { }
                        }
                    }
                    canLoadAnnotations = true;
                    RaisePropertyChanged("ClientLayer");
                }
            }
        }


        public ObservableCollection<AnnotationTreeItemViewModel> RootTreeItemModels { get; private set; }
        public CollectionView RootTreeItemModelsView { get; private set; }
        public IPageFilterViewModel FilterModel { get; protected set; }
        public ObservableCollection<YieldLocationListItemViewModel> YieldLocationList { get; }

        public IList<MapItem> FieldTreeShapes {
            get { return fieldtreeshapes; }
            set {
                if (fieldtreeshapes == value) { return; }
                fieldtreeshapes = value;
            }
        }

        public Feature SelectedField {
            get { return selectedfield; }
            set {
                if (selectedfield == value) { return; }
                selectedfield = value;
                InitializeSettings();
                OpenAndLoadaShapefile();
                RaisePropertyChanged("SelectedField");
            }
        }

        private IList<MapAnnotationCategory> stylenames = new List<MapAnnotationCategory>();
        public IList<MapAnnotationCategory> StyleNames {
            get { return stylenames; }
            set {
                stylenames = value;
                RaisePropertyChanged("StyleNames");
            }
        }
        
        private MapAnnotationCategory selectedstyle;
        public MapAnnotationCategory SelectedStyle {
            get { return selectedstyle; }
            set {
                if (selectedstyle == value) { return; }
                selectedstyle = value;
                RaisePropertyChanged("SelectedStyle");
            }
        }

        AbstractTreeItemViewModel selectedTreeItem;
        public AbstractTreeItemViewModel SelectedTreeItem {
            get { return selectedTreeItem; }
            set {
                selectedTreeItem = value;
                RaisePropertyChanged("SelectedTreeItem");
                if (value == null) { return; }
                ChangeSelectionExtent(selectedTreeItem);
            }
        }

        private IList<YieldLocationListItemViewModel> _destinationLocationlist = new List<YieldLocationListItemViewModel>();
        public IList<YieldLocationListItemViewModel> DestinationLocationList
        {
            get { return _destinationLocationlist; }
            set
            {
                _destinationLocationlist = value;
                RaisePropertyChanged("DestinationLocationList");
            }
        }

        private YieldLocationListItemViewModel _destinationLocation = null;
        public YieldLocationListItemViewModel SelectedDestinationLocation
        {
            get { return _destinationLocation; }
            set
            {
                _destinationLocation = value;
                RaisePropertyChanged("SelectedDestinationLocation");
            }
        }

        //// used for the custom combo box for yield locations
        //private bool _isLocationListVisible = false;
        //public bool IsLocationListVisible
        //{
        //    get { return _isLocationListVisible; }
        //    set
        //    {
        //        _isLocationListVisible = value;
        //        RaisePropertyChanged(() => IsLocationListVisible);
        //    }
        //}

        public bool HasUnsavedChanges {
            get { return hasunsavedchanges; }
            set {
                if (hasunsavedchanges == value) { return; }
                hasunsavedchanges = value;
                RaisePropertyChanged("HasUnsavedChanges");
            }
        }

        public bool IsOptionsMapPopupOpen {
            get { return isOptionsMapPopupOpen; }
            set {
                isOptionsMapPopupOpen = value;
                RaisePropertyChanged("IsOptionsMapPopupOpen");
            }
        }

        public int MapAnnotationOpacity {
            get { return annotationopacity; }
            set {
                if (annotationopacity == value) { return; }
                annotationopacity = value;
                mapsettings.MapAnnotationOpacity = annotationopacity;
                customMultiStyles.Opacity = annotationopacity;
                scalingTextStyle.MapScale = Map.CurrentScale;
                scalingTextStyle2.MapScale = Map.CurrentScale;
                customMultiStyles.MapScale = Map.CurrentScale;
                Map.Overlays["annotationOverlay"].Refresh();
                RaisePropertyChanged("MapAnnotationOpacity");
            }
        }

        public RectangleShape CurrentMapExtent
        {
            get { return currentmapextent; }
            set
            {
                if (currentmapextent == value) { return; }
                currentmapextent = value;
                RaisePropertyChanged("CurrentMapExtent");
                if (maploaded)
                {
                    ShapeValidationResult svr = currentmapextent.Validate(ShapeValidationMode.Simple);
                    if (svr.IsValid)
                    {
                        scalingTextStyle.MapScale = Map.CurrentScale;
                        scalingTextStyle2.MapScale = Map.CurrentScale;
                        customMultiStyles.MapScale = Map.CurrentScale;
                        Map.CurrentExtent = currentmapextent;
                        Map.Refresh();
                    }
                }
            }
        }

        private string displaycoordinates = string.Empty;
        public string DisplayCoordinates {
            get { return displaycoordinates; }
            set {
                displaycoordinates = value;
                RaisePropertyChanged("DisplayCoordinates");
            }
        }

        public void LoadAnnotations() {
            if (maploaded && canLoadAnnotations) {
                //this.mapsettings = clientEndpoint.GetMapSettings();
                fieldopacity = mapsettings.FieldsOpacity;
                annotationopacity = mapsettings.MapAnnotationOpacity;
                areaUnitString = mapsettings.MapAreaUnit;
                coordinateformat = mapsettings.DisplayCoordinateFormat;
                if (coordinateformat == "Decimal Degree") {
                    Map.MapTools.MouseCoordinate.MouseCoordinateType = MouseCoordinateType.DegreesMinutesSeconds;
                }
                else {
                    Map.MapTools.MouseCoordinate.MouseCoordinateType = MouseCoordinateType.LatitudeLongitude;
                }
                agcAreaUnit = UnitFactory.GetUnitByName(areaUnitString) as AgC.UnitConversion.Area.AreaUnit;
                mapAreaUnit = MapUnitFactory.AreaMeasureConversion(agcAreaUnit);
                mapDistanceUnit = MapUnitFactory.AreaDistanceMeasureConversion(agcAreaUnit);
                scalingTextStyle = new ScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, mapsettings.ScalingTextMaximumSize, this.mapsettings.ScalingTextMinimumSize);

                fieldsselectedfillcolor = mapsettings.FieldsSelectedFillColor;
                fieldsunselectedfillcolor = mapsettings.FieldsUnSelectedFillColor;
                if (!string.IsNullOrEmpty(fieldsselectedfillcolor)) {
                    fieldsselectedGeoColor = GeoColor.FromHtml(fieldsselectedfillcolor);
                }
                if (!string.IsNullOrEmpty(fieldsunselectedfillcolor)) {
                    fieldsunselectedGeoColor = GeoColor.FromHtml(fieldsunselectedfillcolor);
                }

                fieldsselectedbordercolor = mapsettings.FieldsSelectedBorderColor;
                fieldsunselectedbordercolor = mapsettings.FieldsUnSelectedBorderColor;
                if (!string.IsNullOrEmpty(fieldsselectedbordercolor)) {
                    fieldsselectedborderGeoColor = GeoColor.FromHtml(fieldsselectedbordercolor);
                }
                else {
                    fieldsselectedborderGeoColor = fieldsselectedGeoColor;
                }
                if (!string.IsNullOrEmpty(fieldsunselectedbordercolor)) {
                    fieldsunselectedborderGeoColor = GeoColor.FromHtml(fieldsunselectedbordercolor);
                }
                else {
                    fieldsunselectedborderGeoColor = fieldsunselectedGeoColor;
                }

                currentLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Clear();
                AreaStyle areastyle = new AreaStyle(new GeoPen(new GeoColor(100, GeoColor.StandardColors.Transparent), 2), new GeoSolidBrush(new GeoColor(60, GeoColor.StandardColors.Transparent)));
                LineStyle linestyle = new LineStyle(new GeoPen(new GeoColor(100, GeoColor.StandardColors.Transparent), 2));
                PointStyle pointstyle = new PointStyle(PointSymbolType.Circle, new GeoSolidBrush(new GeoColor(100, GeoColor.StandardColors.Transparent)), 10);
                customMultiStyles = new MapStyles.AnnotationMultiStyle(areastyle, linestyle, pointstyle, annotationopacity);
                customMultiStyles.ColumnsList = new List<string>() { @"Id", @"LdbLabel", @"TypeName", @"ShapeType" };
                scalingTextStyle2 = new AnnotationScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, mapsettings.ScalingTextMaximumSize, this.mapsettings.ScalingTextMinimumSize);
                scalingTextStyle2.ColumnsList = new List<string>() { @"Id", @"LdbLabel", @"TypeName", @"ShapeType" };
                //scalingTextStyle2.YOffsetInPixel = 20;
                customMultiStyles.Opacity = annotationopacity;
                customMultiStyles.MaximumLabelSize = this.mapsettings.ScalingTextMaximumSize;
                customMultiStyles.MinimumLabelSize = this.mapsettings.ScalingTextMinimumSize;
                customMultiStyles.MaximumScale = 19000;
                customMultiStyles.MinimumScale = 1200;
                currentLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customMultiStyles);
                currentLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(scalingTextStyle2);
                currentLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;

                importLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Clear();
                importLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customMultiStyles);
                importLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(scalingTextStyle2);
                importLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;

                RebuildAnnotations();
                RebuildMap2();
            }
        }

        public void SetCanLoadAnnotations() {
            canLoadAnnotations = true;
        }

        void OnDataSourceChanged(DataSourceChangedMessage message) {
            this.currentDataSourceId = message.DataSourceId;
            this.currentCropYear = message.CropYear;
            if (maploaded) {
                canLoadAnnotations = true;
                LoadAnnotations();
            }
        }

        void OnRemoteEventReceived(RemoteEventReceived message) {
            if (maploaded) {
                canLoadAnnotations = true;
                LoadAnnotations();
            }
        }

        private void InitializeSettings() {
            this.mapsettings = this.clientEndpoint.GetMapSettings();
            fieldopacity = mapsettings.FieldsOpacity;
            annotationopacity = mapsettings.MapAnnotationOpacity;
            areaUnitString = mapsettings.MapAreaUnit;
            coordinateformat = mapsettings.DisplayCoordinateFormat;
            agcAreaUnit = UnitFactory.GetUnitByName(areaUnitString) as AgC.UnitConversion.Area.AreaUnit;
            mapAreaUnit = MapUnitFactory.AreaMeasureConversion(agcAreaUnit);
            mapDistanceUnit = MapUnitFactory.AreaDistanceMeasureConversion(agcAreaUnit);
        }

        void InitializeMap()
        {
            Map = new WpfMap();
            startedOutOnline = Landdb.Client.Infrastructure.NetworkStatus.IsInternetAvailableFast();
            Map.MapUnit = GeographyUnit.Meter;
            Map.MapTools.PanZoomBar.Visibility = System.Windows.Visibility.Visible;
            Map.MapTools.PanZoomBar.GlobeButtonVisibility = System.Windows.Visibility.Hidden;
            Map.MapTools.PanZoomBar.VerticalAlignment = System.Windows.VerticalAlignment.Center;
            Map.MapTools.PanZoomBar.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            Map.Background = System.Windows.Media.Brushes.White;
            Map.Cursor = Cursors.Hand;
            zoomlevelfactory = new ZoomLevelFactory();
            Map.ZoomLevelSet = zoomlevelfactory.ZoomLevelSET;
            HasUnsavedChanges = false;
            Map.MouseLeftButtonDown += (sender, e) =>
            {
                ((MainWindow)System.Windows.Application.Current.MainWindow).Draggable = false;
            };

            projection = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetGoogleMapParametersString());
            //projection.Open();
            //projection = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetGoogleMapParametersString());

            unselectedarea = new AreaStyle(new GeoSolidBrush(GeoColor.FromArgb(annotationopacity, GeoColor.StandardColors.Transparent)));
            unselectedline = new LineStyle(new GeoPen(GeoColor.FromArgb(annotationopacity, GeoColor.StandardColors.Transparent), 2));
            unselectedpoint = PointStyles.CreateSimplePointStyle(PointSymbolType.Circle, new GeoColor(annotationopacity, GeoColor.StandardColors.Transparent), 10);
            customMultiStyles = new MapStyles.AnnotationMultiStyle(unselectedarea, unselectedline, unselectedpoint, annotationopacity);
            customMultiStyles.Opacity = annotationopacity;
            customMultiStyles.MaximumLabelSize = this.mapsettings.ScalingTextMaximumSize;
            customMultiStyles.MinimumLabelSize = this.mapsettings.ScalingTextMinimumSize;
            customMultiStyles.MaximumScale = 19000;
            customMultiStyles.MinimumScale = 1200;
            scalingTextStyle2 = new AnnotationScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, mapsettings.ScalingTextMaximumSize, this.mapsettings.ScalingTextMinimumSize);
            scalingTextStyle2.ColumnsList = new List<string>() { @"Id", @"LdbLabel", @"TypeName", @"ShapeType" };
            //scalingTextStyle2.YOffsetInPixel = 20;
            scalingTextStyle.RotationAngles = new Dictionary<string, double>();
            scalingTextStyle.LabelPositions = new Dictionary<string, PointShape>();
            scalingTextStyle2.RotationAngles = new Dictionary<string, double>();
            scalingTextStyle2.LabelPositions = new Dictionary<string, PointShape>();

            //fieldsLayer
            fieldOverlay = new LayerOverlay();
            fieldOverlay.DrawingExceptionMode = DrawingExceptionMode.DrawException;
            fieldsLayer.FeatureSource.Open();
            FeatureSourceColumn columnf1 = new FeatureSourceColumn(@"LdbLabel", DbfColumnType.Character.ToString(), 64);
            fieldsLayer.Columns.Add(columnf1);
            fieldsLayer.FeatureSource.Close();
            fieldsLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Clear();
            fieldsLayerAreaStyle = new ClientLayerAreaStyle();
            fieldsLayerAreaStyle.SelectedAreaStyle = new AreaStyle(new GeoPen(new GeoColor(fieldopacity, fieldsselectedborderGeoColor), 2), new GeoSolidBrush(new GeoColor(fieldopacity, fieldsselectedGeoColor)));
            fieldsLayerAreaStyle.UnselectedAreaStyle = new AreaStyle(new GeoPen(new GeoColor(fieldopacity, fieldsunselectedborderGeoColor), 2), new GeoSolidBrush(new GeoColor(fieldopacity, fieldsunselectedGeoColor)));
            //fieldsLayerAreaStyle.SelectedAreaStyle = new AreaStyle(new GeoPen(new GeoColor(fieldopacity, GeoColor.StandardColors.Blue), 2), new GeoSolidBrush(new GeoColor(fieldopacity, GeoColor.StandardColors.Transparent)));
            //fieldsLayerAreaStyle.UnselectedAreaStyle = new AreaStyle(new GeoPen(new GeoColor(fieldopacity, GeoColor.StandardColors.Gray), 2), new GeoSolidBrush(new GeoColor(fieldopacity, GeoColor.StandardColors.LightGray)));
            fieldsLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(fieldsLayerAreaStyle);
            //fieldsLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(scalingTextStyle);
            fieldsLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            fieldsLayer.FeatureSource.Projection = projection;
            fieldsLayerAreaStyle.SelectedFeatures = new List<Feature>();
            fieldsLayerAreaStyle.HiddenFeatures = new List<Feature>();
            fieldOverlay.Layers.Clear();
            fieldOverlay.Layers.Add("FieldsLayer", fieldsLayer);
            fieldOverlay.Layers.Add("measureLayer", measureLayer);
            popupOverlay = new PopupOverlay();


            EnsureFieldsLayerIsOpen();
            if (Map.Overlays.Contains("fieldOverlay")) {
                Map.Overlays.Remove("fieldOverlay");
            }
            if (fieldOverlay.Layers.Contains("FieldsLayer")) {
                Map.Overlays.Add("fieldOverlay", fieldOverlay);
            }
            
            Map.Loaded += (sender, e) =>
            {
                if (maploaded) { return; }
                //userstylenames = this.mapsettings.MapAnnotationCategories;
                if (userstylenames.Count > 0) {
                    StyleData.Self.UserStyleNames = userstylenames;
                    changedstyles = new List<AnnotationStyleItem>();
                    foreach (KeyValuePair<string, MapAnnotationCategory> asi in userstylenames) {
                        AnnotationStyleItem annotatonstyle = styleFactory.GetStyle(asi.Value.Name, asi.Value.ShapeType);
                        annotatonstyle.Visible = asi.Value.Visible;
                        changedstyles.Add(annotatonstyle);
                    }
                    StyleData.Self.CustomStyles = changedstyles;
                }

                IList<MapAnnotationCategory> stylenamelist = StyleData.Self.StyleNames.Values.ToList();
                var q = from c in stylenamelist
                        orderby c.Name
                        select c;

                StyleNames = q.ToList();
                RaisePropertyChanged("StyleNames");
                if (selectedstyle == null && stylenames.Count > 0) {
                    SelectedStyle = stylenames[0];
                }
                RaisePropertyChanged("SelectedStyle");

                LoadBingMapsOverlay();
                Map.CurrentExtent = CurrentMapExtent;
                ClientLayer = FieldTreeShapes;
                OpenAndLoadaShapefile();
                if (Map.Overlays.Contains("annotationOverlay")) {
                    Map.Overlays.Remove("annotationOverlay");
                }
                Map.Overlays.Add("annotationOverlay", layerOverlay);
                if (Map.Overlays.Contains("popupOverlay")) {
                    Map.Overlays.Remove("popupOverlay");
                }
                Map.Overlays.Add("popupOverlay", popupOverlay);
                maploaded = true;
                if (currentDataSourceId != Guid.Empty && currentCropYear > 1980) {
                    canLoadAnnotations = true;
                    LoadAnnotations();
                }

                scalingTextStyle.MapScale = Map.CurrentScale;
                scalingTextStyle2.MapScale = Map.CurrentScale;
                customMultiStyles.MapScale = Map.CurrentScale;
                Map.Refresh();
            };

            Map.CurrentScaleChanged += (sender, e) => {
                scalingTextStyle.MapScale = e.CurrentScale;
                scalingTextStyle2.MapScale = e.CurrentScale;
                customMultiStyles.MapScale = Map.CurrentScale;
            };

            Map.MouseMove += (sender, e) => {
                PointShape worldlocation2 = Map.ToWorldCoordinate(e.GetPosition(Map));
                PointShape centerpoint = projection.ConvertToInternalProjection(worldlocation2) as PointShape;
                Vertex geoVertex1 = new Vertex(centerpoint.X, centerpoint.Y);
                string _DecimalDegreeString = Math.Round(geoVertex1.Y, 5) + ", " + Math.Round(geoVertex1.X, 5);
                string displaycoordinates1 = DecimalDegrees.ddstring_To_DMs(_DecimalDegreeString, mapsettings.DisplayCoordinateFormat);
                DisplayCoordinates = displaycoordinates1;
            };

        }

        void ChangeSelectionExtent(AbstractTreeItemViewModel vm) {
            if (vm is AnnotationTreeItemViewModel) {
                try {
                    var shapelayerfeatures = currentLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);
                    if (shapelayerfeatures.Count > 0) {
                        RectangleShape extent = ExtentHelper.GetBoundingBoxOfItems(shapelayerfeatures);
                        scalingTextStyle.MapScale = Map.CurrentScale;
                        scalingTextStyle2.MapScale = Map.CurrentScale;
                        customMultiStyles.MapScale = Map.CurrentScale;
                        Map.CurrentExtent = extent;
                        Map.Refresh();
                    }
                    string vmname = ((AnnotationTreeItemViewModel)vm).Name;
                    log.Info("AnnotationDisplay - Change selected tree to " + vmname);
                }
                catch (Exception ex) {
                    log.ErrorException("Caught an exception while getting style tree features", ex);
                    //System.Windows.MessageBox.Show("ImportTools - Caught an exception while working with a simple draw shape.");
                }

                return;
            }
            if (vm is AnnotationStyleTreeItemViewModel) {
                try {
                    AnnotationStyleTreeItemViewModel item = vm as AnnotationStyleTreeItemViewModel;
                    string seltypename = item.Style.Name;
                    Collection<Feature> selectedFeatures = currentLayer.QueryTools.GetFeaturesByColumnValue(@"TypeName", seltypename, ReturningColumnsType.AllColumns);
                    if (selectedFeatures.Count > 0) {
                        scalingTextStyle.MapScale = Map.CurrentScale;
                        scalingTextStyle2.MapScale = Map.CurrentScale;
                        customMultiStyles.MapScale = Map.CurrentScale;
                        RectangleShape extent = ExtentHelper.GetBoundingBoxOfItems(selectedFeatures);
                        Map.CurrentExtent = extent;
                        Map.Refresh();
                    }
                    string vmname = ((AnnotationStyleTreeItemViewModel)vm).Name;
                    log.Info("AnnotationDisplay - Change selected style to " + vmname);
                }
                catch (Exception ex) {
                    log.ErrorException("Caught an exception while getting annotation tree features", ex);
                    //System.Windows.MessageBox.Show("ImportTools - Caught an exception while working with a simple draw shape.");
                }

                return;
            }
            if (vm is AnnotationShapeTreeItemViewModel) {
                try {
                    AnnotationShapeTreeItemViewModel shape = vm as AnnotationShapeTreeItemViewModel;
                    MapAnnotationListItem mapannotationlistitem = shape.AnnotationItem;
                    string annid = mapannotationlistitem.Id.Id.ToString();
                    Collection<Feature> selectedFeatures = currentLayer.QueryTools.GetFeaturesByColumnValue(@"Id", annid, ReturningColumnsType.AllColumns);
                    if (selectedFeatures.Count > 0) {
                        scalingTextStyle.MapScale = Map.CurrentScale;
                        scalingTextStyle2.MapScale = Map.CurrentScale;
                        customMultiStyles.MapScale = Map.CurrentScale;
                        RectangleShape extent = ExtentHelper.GetBoundingBoxOfItems(selectedFeatures);
                        Map.CurrentExtent = extent;
                        Map.Refresh();
                    }
                    string vmname = ((AnnotationShapeTreeItemViewModel)vm).Name;
                    AnnotationShapeTreeItemViewModel vmann = (AnnotationShapeTreeItemViewModel)vm;
                    string stringid = "null";
                    if (vm.Id != null) {
                        stringid = vm.Id.ToString();
                    }
                    log.Info("AnnotationDisplay - Change selected shape to " + vmname + "  " + stringid);
                }
                catch (Exception ex) {
                    log.ErrorException("Caught an exception while getting annotation features", ex);
                    //System.Windows.MessageBox.Show("ImportTools - Caught an exception while working with a simple draw shape.");
                }

                return;
            }
        }

        public void LoadAnnotationLists() {
            mapsettings = clientEndpoint.GetMapSettings();
            commandlist = new List<IDomainCommand>();
            RefreshDestinationLocationList();
        }

        public void ReLoadAnnotationImageryLayer() {
            mapsettings = clientEndpoint.GetMapSettings();
            RefreshImageryLayer();
        }

        public void RefreshImageryLayer() {
            log.Info("AnnotationTools - Refresh map");
            IsOptionsMapPopupOpen = false;
            InitializeSettings();

            //zoomlevelfactory = new ZoomLevelFactory(mapsettings.ZoomLevel);
            //Map.ZoomLevelSet = zoomlevelfactory.ZoomLevelSET;

            fieldsselectedfillcolor = mapsettings.FieldsSelectedFillColor;
            fieldsunselectedfillcolor = mapsettings.FieldsUnSelectedFillColor;
            if (!string.IsNullOrEmpty(fieldsselectedfillcolor)) {
                fieldsselectedGeoColor = GeoColor.FromHtml(fieldsselectedfillcolor);
            }
            if (!string.IsNullOrEmpty(fieldsunselectedfillcolor)) {
                fieldsunselectedGeoColor = GeoColor.FromHtml(fieldsunselectedfillcolor);
            }

            fieldsselectedbordercolor = mapsettings.FieldsSelectedBorderColor;
            fieldsunselectedbordercolor = mapsettings.FieldsUnSelectedBorderColor;
            if (!string.IsNullOrEmpty(fieldsselectedbordercolor)) {
                fieldsselectedborderGeoColor = GeoColor.FromHtml(fieldsselectedbordercolor);
            }
            else {
                fieldsselectedborderGeoColor = fieldsselectedGeoColor;
            }
            if (!string.IsNullOrEmpty(fieldsunselectedbordercolor)) {
                fieldsunselectedborderGeoColor = GeoColor.FromHtml(fieldsunselectedbordercolor);
            }
            else {
                fieldsunselectedborderGeoColor = fieldsunselectedGeoColor;
            }

            scalingTextStyle = new ScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, mapsettings.ScalingTextMaximumSize, this.mapsettings.ScalingTextMinimumSize);
            scalingTextStyle2 = new AnnotationScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, mapsettings.ScalingTextMaximumSize, this.mapsettings.ScalingTextMinimumSize);
            scalingTextStyle2.ColumnsList = new List<string>() { @"Id", @"LdbLabel", @"TypeName", @"ShapeType" };
            //scalingTextStyle2.YOffsetInPixel = 20;

            //LoadBingMapsOverlay();
            //Map.CurrentExtent = CurrentMapExtent;
            //ClientLayer = FieldTreeShapes;
            measureLayer.InternalFeatures.Clear();
            popupOverlay.Popups.Clear();

            if (maploaded) {
                canLoadAnnotations = true;
                OpenAndLoadaShapefile();
                LoadAnnotations();
            }
            if (currentLayer.InternalFeatures.Count > 0) {
                scalingTextStyle.MapScale = Map.CurrentScale;
                scalingTextStyle2.MapScale = Map.CurrentScale;
                customMultiStyles.MapScale = Map.CurrentScale;
                try {
                    var shapelayerfeatures = currentLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);
                    Map.CurrentExtent = ExtentHelper.GetBoundingBoxOfItems(shapelayerfeatures);
                    Map.Refresh();
                }
                catch (Exception ex) {
                    log.ErrorException("Caught an exception while refreshing annotations", ex);
                    //System.Windows.MessageBox.Show("ImportTools - Caught an exception while working with a simple draw shape.");
                }

            }
        }

        private void LoadBingMapsOverlay()
        {
            if (!startedOutOnline) { return; }
            IMapOverlay mapoverlay = new BingMapOverlay();
            Overlay bingOverlay = mapoverlay.Overlay;
            if (bingOverlay == null) { return; }
            if (Map.Overlays.Contains("Bing")) {
                Map.Overlays.Remove("Bing");
            }
            if (Map.Overlays.Contains("Bing Online")) {
                Map.Overlays.Remove("Bing Online");
            }
            if (Map.Overlays.Contains("Roads Only")) {
                Map.Overlays.Remove("Roads Only");
            }
            if (Map.Overlays.Contains("Road Online")) {
                Map.Overlays.Remove("Road Online");
            }
            if (Map.Overlays.Contains("None")) {
                Map.Overlays.Remove("None");
            }
            //bingOverlay.IsVisible = mapoverlay.Visibility;
            //((TileOverlay)bingOverlay).TileType = TileType.SingleTile;
            Map.Overlays.Add(mapoverlay.Name, bingOverlay);
            Map.Overlays.MoveToBottom(mapoverlay.Name);
        }

        internal IEnumerable<MapAnnotationListItemViewModel> GetListItemModels() {
            var resultsMaybe = clientEndpoint.GetView<MapAnnotationListView>(new DataSourceId(ApplicationEnvironment.CurrentDataSourceId));
            if (resultsMaybe.HasValue) {
                var sortedProjectedResults = from ann in resultsMaybe.Value.Annotations
                                               orderby ann.Name
                                               select new MapAnnotationListItemViewModel(ann);

                return sortedProjectedResults;
            }
            else {
                return null;
            }
        }

        private void OpenAndLoadaShapefile()
        {
            CreateInMemoryLayerAndOverlay();
            CreateImportLayerAndOverlay();
            LoadShapeFileOverlayOntoMap();
        }

        private void CreateInMemoryLayerAndOverlay()
        {
            layerOverlay = new LayerOverlay();
            layerOverlay.DrawingExceptionMode = DrawingExceptionMode.DrawException;
            currentLayer = new InMemoryFeatureLayer();
            currentLayer.FeatureSource.Open();
            FeatureSourceColumn column1 = new FeatureSourceColumn(@"Id", "string", 64);
            //FeatureSourceColumn column2 = new FeatureSourceColumn(@"Label", "string", 64);
            FeatureSourceColumn column2 = new FeatureSourceColumn(@"LdbLabel", "string", 64);
            FeatureSourceColumn column3 = new FeatureSourceColumn(@"TypeName", "string", 64);
            FeatureSourceColumn column4 = new FeatureSourceColumn(@"ShapeType", "string", 64);
            currentLayer.Columns.Add(column1);
            currentLayer.Columns.Add(column2);
            currentLayer.Columns.Add(column3);
            currentLayer.Columns.Add(column4);
            currentLayer.FeatureSource.Close();
            currentLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = null;
            currentLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = null;
            currentLayer.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = null;
            currentLayer.ZoomLevelSet.ZoomLevel01.DefaultTextStyle = null;
            currentLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Clear();
            AreaStyle areastyle = new AreaStyle(new GeoPen(new GeoColor(100, GeoColor.StandardColors.Transparent), 2), new GeoSolidBrush(new GeoColor(60, GeoColor.StandardColors.Transparent)));
            LineStyle linestyle = new LineStyle(new GeoPen(new GeoColor(100, GeoColor.StandardColors.Transparent), 2));
            PointStyle pointstyle = new PointStyle(PointSymbolType.Circle, new GeoSolidBrush(new GeoColor(100, GeoColor.StandardColors.Transparent)), 10);
            customMultiStyles = new MapStyles.AnnotationMultiStyle(areastyle, linestyle, pointstyle, annotationopacity);
            //scalingTextStyle2 = new ScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, mapsettings.ScalingTextMaximumSize, this.mapsettings.ScalingTextMinimumSize);
            scalingTextStyle2 = new AnnotationScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, mapsettings.ScalingTextMaximumSize, this.mapsettings.ScalingTextMinimumSize);
            scalingTextStyle2.ColumnsList = new List<string>() { @"Id", @"LdbLabel", @"TypeName", @"ShapeType" };
            //scalingTextStyle2.YOffsetInPixel = 20;
            //customMultiStyles.ColumnsList = new List<string>() { @"Id", @"Label", @"TypeName", @"ShapeType" };
            customMultiStyles.Opacity = annotationopacity;
            customMultiStyles.MaximumLabelSize = this.mapsettings.ScalingTextMaximumSize;
            customMultiStyles.MinimumLabelSize = this.mapsettings.ScalingTextMinimumSize;
            customMultiStyles.MaximumScale = 19000;
            customMultiStyles.MinimumScale = 1200;
            customMultiStyles.ColumnsList = new List<string>() { @"Id", @"LdbLabel", @"TypeName", @"ShapeType" };
            currentLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customMultiStyles);
            currentLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(scalingTextStyle2);
            currentLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            currentLayer.FeatureSource.Projection = projection;
            layerOverlay.Layers.Add("AnnotationLayer", currentLayer);
        }

        private void CreateImportLayerAndOverlay() {
            importLayer = new InMemoryFeatureLayer();
            importLayer.FeatureSource.Open();
            FeatureSourceColumn column1 = new FeatureSourceColumn(@"Id", "string", 64);
            FeatureSourceColumn column2 = new FeatureSourceColumn(@"LdbLabel", "string", 64);
            FeatureSourceColumn column3 = new FeatureSourceColumn(@"TypeName", "string", 64);
            FeatureSourceColumn column4 = new FeatureSourceColumn(@"ShapeType", "string", 64);
            importLayer.Columns.Add(column1);
            importLayer.Columns.Add(column2);
            importLayer.Columns.Add(column3);
            importLayer.Columns.Add(column4);
            importLayer.FeatureSource.Close();
            importLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = null;
            importLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = null;
            importLayer.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = null;
            importLayer.ZoomLevelSet.ZoomLevel01.DefaultTextStyle = null;
            importLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Clear();
            importLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customMultiStyles);
            importLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(scalingTextStyle2);
            importLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            importLayer.FeatureSource.Projection = projection;
            layerOverlay.Layers.Add("ImportAnnotationLayer", importLayer);
        }

        private void LoadShapeFileOverlayOntoMap()
        {
            if (Map.Overlays.Contains("annotationOverlay"))
            {
                Map.Overlays.Remove("annotationOverlay");
            }
            if (layerOverlay.Layers.Contains("AnnotationLayer"))
            {
                Map.Overlays.Add("annotationOverlay", layerOverlay);
            }

            EnsureCurrentLayerIsOpen();
            EnsureImportLayerIsOpen();
            EnsureFieldsLayerIsOpen();
            EnsureMeasureLayerIsOpen();
            var extent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
            try
            {
                if (currentLayer.InternalFeatures.Count > 0 || fieldsLayer.InternalFeatures.Count > 0) {
                    scalingTextStyle.MapScale = Map.CurrentScale;
                    scalingTextStyle2.MapScale = Map.CurrentScale;
                    customMultiStyles.MapScale = Map.CurrentScale;
                    Map.Refresh();
                }
                
                var shapelayerfeatures = currentLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);
                var fieldlayerfeatures = fieldsLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);
                if (shapelayerfeatures.Count > 0)
                {
                    var shapelayercolumns = currentLayer.FeatureSource.GetColumns();
                    extent = ExtentHelper.GetBoundingBoxOfItems(shapelayerfeatures);
                    Map.CurrentExtent = extent;
                    PointShape center = extent.GetCenterPoint();
                }
                else if (fieldlayerfeatures.Count > 0)
                {
                    var fieldlayercolumns = fieldsLayer.FeatureSource.GetColumns();
                    extent = ExtentHelper.GetBoundingBoxOfItems(fieldlayerfeatures);
                    Map.CurrentExtent = extent;
                    PointShape center = extent.GetCenterPoint();
                }
                else
                {
                    Map.CurrentExtent = CurrentMapExtent;
                    PointShape center = CurrentMapExtent.GetCenterPoint();
                }
            }
            catch
            {
                Map.CurrentExtent = CurrentMapExtent;
                PointShape center = CurrentMapExtent.GetCenterPoint();
            }
        }

        private void EnsureCurrentLayerIsOpen() {
            if (currentLayer == null) {
                currentLayer = new InMemoryFeatureLayer();
            }

            if (!currentLayer.IsOpen) {
                currentLayer.Open();
            }

            if (!projection.IsOpen) {
                projection.Open();
            }
        }

        private void EnsureImportLayerIsOpen() {
            if (importLayer == null) {
                importLayer = new InMemoryFeatureLayer();
            }

            if (!importLayer.IsOpen) {
                importLayer.Open();
            }

            if (!projection.IsOpen) {
                projection.Open();
            }
        }

        internal void EnsureMeasureLayerIsOpen() {
            if (measureLayer == null) {
                measureLayer = new InMemoryFeatureLayer();
            }

            if (!measureLayer.IsOpen) {
                measureLayer.Open();
            }

            if (!projection.IsOpen) {
                projection.Open();
            }
        }

        private void keyEvent_Esc(object sender, KeyEventArgs e) {
            if (e.Key == System.Windows.Input.Key.Escape) {
                if (Map.TrackOverlay.TrackMode != TrackMode.None) {
                    Map.TrackOverlay.TrackEnded -= new EventHandler<TrackEndedTrackInteractiveOverlayEventArgs>(CutLine_TrackEnded);
                    Map.MapClick -= new EventHandler<MapClickWpfMapEventArgs>(map_MapClick);
                    Map.KeyDown -= new KeyEventHandler(keyEvent_Esc);

                    if (Map.TrackOverlay.TrackMode == TrackMode.Polygon || Map.TrackOverlay.TrackMode == TrackMode.Line || Map.TrackOverlay.TrackMode == TrackMode.Circle) {
                        Map.TrackOverlay.MouseDoubleClick(new InteractionArguments());
                    }
                    else {
                        Map.TrackOverlay.MouseUp(new InteractionArguments());
                    }

                    Map.TrackOverlay.TrackMode = TrackMode.None;
                    Map.Cursor = previousCursor;

                    int count = Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Count;
                    if (count > 0) {
                        Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Remove("InTrackingFeature");
                    }
                    Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Clear();
                    Map.TrackOverlay.Refresh();

                }
            }
        }

        private void map_MapClick(object sender, MapClickWpfMapEventArgs e) {
            if (!Map.IsFocused) {
                Map.Focus();
            }
        }

        private void RebuildAnnotations() {
            dispatcher.BeginInvoke(new Action(() => {
                IsLoadingTree = true;
                canLoadAnnotations = false;
                var begin = DateTime.UtcNow;
                AbstractTreeItemViewModel rootNode = null;
                Dictionary<string, MapAnnotationCategory> categories = new Dictionary<string, MapAnnotationCategory>();
                var annotations = clientEndpoint.GetView<MapAnnotationListView>(new DataSourceId(this.currentDataSourceId));
                List<MapAnnotationListItem> anlist = annotations.HasValue && annotations.Value.Annotations != null ? annotations.Value.Annotations : new List<MapAnnotationListItem>();
                if (categories.Count == 0 && anlist.Count > 0) {
                    foreach (MapAnnotationListItem aa in anlist) {
                            if (!categories.ContainsKey(aa.AnnotationType)) {
                                if (AnnotationsDisplayItem.AnnotationStyleDictionary.Any(x => x.Value.untranslatedKey == aa.AnnotationType)) {
                                MapAnnotationCategory mac = styleFactory.GetMapAnnotationCategory(aa.AnnotationType);
                                categories.Add(mac.Name, mac);
                            }
                        }
                    }
                }
                RootTreeItemModels = new ObservableCollection<AnnotationTreeItemViewModel>() {
                    new AnnotationTreeItemViewModel(Strings.Annotations_Text, categories, anlist, true, filter: FilterModel.FilterItem)
                };
                rootNode = RootTreeItemModels.FirstOrDefault();
                if (rootNode != null) {
                    RootTreeItemModels[0].IsExpanded = true;
                }
                App.Current.Dispatcher.BeginInvoke(new Action(() => {
                    RootTreeItemModelsView = (CollectionView)CollectionViewSource.GetDefaultView(RootTreeItemModels);
                    RootTreeItemModelsView.Filter = FilterModel.FilterItem;
                    GenerateFilterHeader();
                    RaisePropertyChanged("RootTreeItemModelsView");
                }));
                RaisePropertyChanged("RootTreeItemModels");

                var end = DateTime.UtcNow;
                IsLoadingTree = false;
                log.Debug("Tree load took " + (end - begin).TotalMilliseconds + "ms");
            }));
        }

        void GenerateFilterHeader() {
            //var originalCount = (from r in RootTreeItemModels
            //                     from fa in r.Children
            //                     select fa.Children.Count).Sum();
            //var filteredCount = (from r in RootTreeItemModels
            //                     from fa in r.Children
            //                     select fa.FilteredChildren.Count).Sum();

            //string entityPluralityAwareName = originalCount == 1 ? "field" : "fields";

            //if (originalCount != filteredCount) {
            //    FilterHeader = string.Format("{0} (of {1}) {2}", filteredCount, originalCount, entityPluralityAwareName);
            //}
            //else {
            //    //FilterHeader = string.Format("{0} {1}", filteredCount, entityPluralityAwareName);
            //    FilterHeader = "All fields";
            //}
        }

        private void RebuildMap() {
            dispatcher.BeginInvoke(new Action(() => {
                //IsLoadingTree = true;
                var begin = DateTime.UtcNow;
                EnsureCurrentLayerIsOpen();
                currentLayer.InternalFeatures.Clear();
                var annotations = clientEndpoint.GetView<MapAnnotationListView>(new DataSourceId(this.currentDataSourceId));
                List<MapAnnotationListItem> anlist = annotations.HasValue && annotations.Value.Annotations != null ? annotations.Value.Annotations : new List<MapAnnotationListItem>();
                IList<MapAnnotationDetailView> detaillist = new List<MapAnnotationDetailView>();
                IList<AnnotationStyleItem> stylelist = new List<AnnotationStyleItem>();
                foreach (var m in anlist) {
                    if (m.Name != null && !string.IsNullOrEmpty(m.Name)) {
                        try {
                            var shapeItemMaybe = clientEndpoint.GetView<MapAnnotationDetailView>(m.Id);
                            MapAnnotationId mapAnnotationId = new MapAnnotationId();
                            string annotationName = string.Empty;
                            string annotationWktData = string.Empty;
                            string annotationShapeType = string.Empty;
                            string annotationType = string.Empty;
                            int? annotationCropYear = null;
                            shapeItemMaybe.IfValue(annotation => {
                                detaillist.Add(annotation);
                                string anid = annotation.Id.Id.ToString();
                                mapAnnotationId = annotation.Id;
                                annotationName = annotation.Name;
                                annotationWktData = annotation.WktData;
                                annotationShapeType = annotation.ShapeType;
                                annotationType = annotation.AnnotationType;
                                annotationCropYear = annotation.CropYear;

                                IDictionary<string, string> columnvalues = new Dictionary<string, string>();
                                columnvalues.Add(@"Id", anid);
                                columnvalues.Add(@"LdbLabel", annotationName);
                                columnvalues.Add(@"TypeName", annotationType);
                                columnvalues.Add(@"ShapeType", annotationShapeType);
                                Feature newfeature = new Feature(annotationWktData, anid, columnvalues);
                                currentLayer.InternalFeatures.Add(anid, newfeature);
                                var f = new Feature(annotationWktData, mapAnnotationId.Id.ToString(), columnvalues);
                                if (currentLayer.InternalFeatures.Contains(anid)) {
                                    currentLayer.InternalFeatures.Remove(anid);
                                }
                                currentLayer.InternalFeatures.Add(anid, newfeature);
                            });

                        }
                        catch { }
                    }
                }
                //if(maploaded && currentLayer.InternalFeatures.Count > 0) {
                //    Map.Refresh();
                //}
                var end = DateTime.UtcNow;
                //IsLoadingTree = false;
                log.Debug("Annotation Shapes load took " + (end - begin).TotalMilliseconds + "ms");
            }));
        }

        private void RebuildMap2() {
                //IsLoadingTree = true;
                var begin = DateTime.UtcNow;
                EnsureCurrentLayerIsOpen();
                currentLayer.InternalFeatures.Clear();
                var annotations = clientEndpoint.GetView<MapAnnotationListView>(new DataSourceId(this.currentDataSourceId));
                List<MapAnnotationListItem> anlist = annotations.HasValue && annotations.Value.Annotations != null ? annotations.Value.Annotations : new List<MapAnnotationListItem>();
                IList<MapAnnotationDetailView> detaillist = new List<MapAnnotationDetailView>();
                IList<AnnotationStyleItem> stylelist = new List<AnnotationStyleItem>();
                foreach (var m in anlist) {
                    if (m.Name != null && !string.IsNullOrEmpty(m.Name)) {
                        try {
                            var shapeItemMaybe = clientEndpoint.GetView<MapAnnotationDetailView>(m.Id);
                            MapAnnotationId mapAnnotationId = new MapAnnotationId();
                            string annotationName = string.Empty;
                            string annotationWktData = string.Empty;
                            string annotationShapeType = string.Empty;
                            string annotationType = string.Empty;
                            int? annotationCropYear = null;
                            shapeItemMaybe.IfValue(annotation => {
                                detaillist.Add(annotation);
                                string anid = annotation.Id.Id.ToString();
                                mapAnnotationId = annotation.Id;
                                annotationName = annotation.Name;
                                annotationWktData = annotation.WktData;
                                annotationShapeType = annotation.ShapeType;
                                annotationType = annotation.AnnotationType;
                                annotationCropYear = annotation.CropYear;
                                AnnotationStyleItem annotationstyleitem = null;

                                IDictionary<string, string> columnvalues = new Dictionary<string, string>();
                                columnvalues.Add(@"Id", anid);
                                columnvalues.Add(@"LdbLabel", annotationName);
                                columnvalues.Add(@"TypeName", annotationType);
                                columnvalues.Add(@"ShapeType", annotationShapeType);
                                Feature newfeature = new Feature(annotationWktData, anid, columnvalues);
                                currentLayer.InternalFeatures.Add(anid, newfeature);
                                var f = new Feature(annotationWktData, mapAnnotationId.Id.ToString(), columnvalues);
                                if (currentLayer.InternalFeatures.Contains(anid)) {
                                    currentLayer.InternalFeatures.Remove(anid);
                                }
                                currentLayer.InternalFeatures.Add(anid, newfeature);
                            });

                        }
                        catch { }
                    }
                }
                //if(maploaded && currentLayer.InternalFeatures.Count > 0) {
                //    Map.Refresh();
                //}
                var end = DateTime.UtcNow;
                //IsLoadingTree = false;
                log.Debug("Annotation Shapes load took " + (end - begin).TotalMilliseconds + "ms");
        }

        void EditMethod() {
            scalingTextStyle.MapScale = Map.CurrentScale;
            scalingTextStyle2.MapScale = Map.CurrentScale;
            customMultiStyles.MapScale = Map.CurrentScale;
            Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Clear();
            Map.Refresh();
            Map.TrackOverlay.TrackMode = TrackMode.None;
            previousCursor = Map.Cursor;
            Map.Cursor = Cursors.Hand;
        }

        void ExitCropzone()
        {
            Map.Cursor = Cursors.Arrow;
            var importmessage = new ShapeImportMessage() { ShapeBase = new MultipolygonShape() };
            Messenger.Default.Send<ShapeImportMessage>(importmessage);
        }

        internal void EnsureFieldsLayerIsOpen() {
            if (fieldsLayer == null) {
                fieldsLayer = new InMemoryFeatureLayer();
            }

            if (!fieldsLayer.IsOpen) {
                fieldsLayer.Open();
            }

            if (!projection.IsOpen) {
                projection.Open();
            }
        }

        void OnNewItemCreated(AbstractTreeItemViewModel vm) {
            if (vm != null) {
                SelectedTreeItem = vm;
            }
            else {
                SelectedTreeItem = null;
            }
            selectedstyle = createItemViewModel.SelectedStyle;

            SelectedDestinationLocation = createItemViewModel.SelectedDestinationLocation;
            foreach (var commmanditem in createItemViewModel.CommandAssociationList) {
                commandlist.Add(commmanditem);
            }
            createItemViewModel.CommandAssociationList.Clear();

            string anid = createItemViewModel.Id.Id.ToString();
            if (createItemViewModel.IsNew) {
                DrawShapesMethod();
            }
            else {
                EditShapesMethod();
            }
        }

        void OnNewItemIgnored() {
            HasUnsavedChanges = false;
        }

        void OnImportItemCompleted(AbstractTreeItemViewModel vm) {
            if (vm != null) {
                SelectedTreeItem = vm;
            }
            else {
                SelectedTreeItem = null;
            }
            selectedstyle = importAnnotationViewModel.SelectedStyle;
            GatherImportShapes(SelectedTreeItem);
        }

        private void GatherImportShapes(AbstractTreeItemViewModel selecteditem) {
            EnsureImportLayerIsOpen();
            Map.EditOverlay.EditShapesLayer.InternalFeatures.Clear();
            int iii = 0;
            foreach (Feature feat in importAnnotationViewModel.AllFeatures) {
                BaseShape baseShape = feat.GetShape();
                if (baseShape == null) { return; }
                BaseShape baseShape2 = baseShape;
                //baseShape2 = projection.ConvertToInternalProjection(baseShape);
                //baseShape2 = projection.ConvertToExternalProjection(baseShape);
                iii++;
                //Map.EditOverlay.EditShapesLayer.InternalFeatures.Add(new Feature(baseShape2.GetWellKnownText(), feat.ColumnValues["RecID"].ToString()));
                Map.EditOverlay.EditShapesLayer.InternalFeatures.Add(new Feature(baseShape2.GetWellKnownText(), iii.ToString()));
            }
            Map.EditOverlay.CalculateAllControlPoints();
            previousCursor = Map.Cursor;
            Map.Cursor = Cursors.Hand;
            HasUnsavedChanges = true;
            scalingTextStyle.MapScale = Map.CurrentScale;
            scalingTextStyle2.MapScale = Map.CurrentScale;
            customMultiStyles.MapScale = Map.CurrentScale;
            Map.Refresh();
        }

        void OnEditItemModified(AbstractTreeItemViewModel vm) {
            if (vm != null) {
                if (vm is AnnotationStyleTreeItemViewModel) {
                    HasUnsavedChanges = true;
                    bool treeitemfound = false;
                    changedstyles = new List<AnnotationStyleItem>();
                    for (int x = 0; x < RootTreeItemModels.Count; x++) {
                        if (treeitemfound) { break; }
                        AnnotationTreeItemViewModel root = RootTreeItemModels[x] as AnnotationTreeItemViewModel;
                        for (int y = 0; y < root.Children.Count; y++) {
                            if (treeitemfound) { break; }
                            AnnotationStyleTreeItemViewModel style = root.Children[y] as AnnotationStyleTreeItemViewModel;
                            AnnotationStyleTreeItemViewModel selstyle = selectedTreeItem as AnnotationStyleTreeItemViewModel;
                            AnnotationStyleTreeItemViewModel vmstyle = vm as AnnotationStyleTreeItemViewModel;
                            if (style.Name == selstyle.Name) {
                                changedstyles.Add(editCategoryViewModel.SelectedStyle);
                                StyleData.Self.CustomStyles = changedstyles;
                                style.Style.StyleName = editCategoryViewModel.SelectedStyle.TypeName;
                                style.Style.Visible = editCategoryViewModel.IsVisible;
                                RootTreeItemModels[x].Children[y] = style;
                                if (userstylenames.ContainsKey(style.Name)) {
                                    userstylenames[style.Name] = style.Style;
                                }
                                else {
                                    userstylenames.Add(style.Name, style.Style);
                                }
                                AnnotationStyleItem annotatonstyle = styleFactory.GetStyle(style.Name, style.Style.ShapeType);
                                annotatonstyle.Visible = editCategoryViewModel.IsVisible;
                                changedstyles.Add(annotatonstyle);
                                treeitemfound = true;
                                break;
                            }
                        }
                    }
                    StyleData.Self.CustomStyles = changedstyles;
                    StyleData.Self.UserStyleNames = userstylenames;
                    RaisePropertyChanged("RootTreeItemModelsView");
                    RaisePropertyChanged("RootTreeItemModels");
                    RaisePropertyChanged("HasUnsavedChanges");
                }
            }
        }

        void CreateNewItem() {
            try {
                importshapes = false;
                AbstractTreeItemViewModel parentNode = SelectedTreeItem;
                if (SelectedTreeItem == null) {
                    parentNode = RootTreeItemModels.FirstOrDefault();
                }
                if (SelectedTreeItem is AnnotationTreeItemViewModel) {
                    parentNode = RootTreeItemModels.FirstOrDefault();
                }
                if (SelectedTreeItem is AnnotationStyleTreeItemViewModel) {
                    parentNode = RootTreeItemModels.FirstOrDefault();
                }
                if (SelectedTreeItem is AnnotationShapeTreeItemViewModel) {
                    parentNode = SelectedTreeItem.Parent;
                }
                createItemViewModel.SelectedParentNode = parentNode;
                createItemViewModel.RootNode = RootTreeItemModels.FirstOrDefault();
                createItemViewModel.Id = new MapAnnotationId(this.currentDataSourceId, Guid.NewGuid());
                createItemViewModel.StyleNames = StyleNames;
                createItemViewModel.SelectedStyle = SelectedStyle;
                createItemViewModel.ApplyCropYear = false;
                createItemViewModel.YieldLocationList = YieldLocationList;
                createItemViewModel.IsNew = true;
                createItemViewModel.IsYieldLocation = false;
                createItemViewModel.NewItemName = String.Format(Strings.NewThing_Text, AnnotationsDisplayItem.GetAnnotationStyleDisplayTextFor(SelectedStyle.Name));
                createItemViewModel.CommandList = new List<IDomainCommand>();
                createItemViewModel.CommandAssociationList = new List<IDomainCommand>();
                createItemViewModel.CurrentCropYear = null;
                ScreenDescriptor descriptor = null;
                log.Info("AnnotationTools - create new item");
                descriptor = new ScreenDescriptor("Landdb.Views.Fields.Popups.AddMapAnnotationPopupView", "AddMa", createItemViewModel);
                Messenger.Default.Send(new ShowPopupMessage() { ScreenDescriptor = descriptor });
            }
            catch (Exception ex) {
                log.Info("AnnotationTools - Selected Annotation: " + createItemViewModel.NewItemName + "  ID: " + createItemViewModel.Id.Id.ToString());
                log.ErrorException("Caught an exception while creating a new annotation", ex);
                MessageBox.Show($"{Strings.ThereWasAProblemAddingAnnotation_Text} {Strings.OperationAborted_Text}");
            }
        }

        void DrawShapesMethod() {
            Map.TrackOverlay.TrackEnded += new EventHandler<TrackEndedTrackInteractiveOverlayEventArgs>(CutLine_TrackEnded);
            previousCursor = Map.Cursor;
            Map.Cursor = Cursors.Pen;
            if (selectedstyle.ShapeType == "Polygon") {
                //Map.TrackOverlay.TrackMode = TrackMode.Polygon;
                if (selectedstyle.StyleName == "GrainBins" || selectedstyle.StyleName == "Silo") {
                    Map.TrackOverlay.TrackMode = TrackMode.Point;
                }
                else {
                    Map.TrackOverlay.TrackMode = TrackMode.Polygon;
                }
            }
            else if (selectedstyle.ShapeType == "Line") {
                if (selectedstyle.StyleName == "Terrace") {
                    Map.TrackOverlay.TrackMode = TrackMode.Freehand;
                }
                else {
                    Map.TrackOverlay.TrackMode = TrackMode.Line;
                }
            }
            else {
                Map.TrackOverlay.TrackMode = TrackMode.Point;
            }
        }

        private void CutLine_TrackEnded(object sender, TrackEndedTrackInteractiveOverlayEventArgs e) {
            Map.TrackOverlay.TrackEnded -= new EventHandler<TrackEndedTrackInteractiveOverlayEventArgs>(CutLine_TrackEnded);
            Map.TrackOverlay.TrackMode = TrackMode.None;
            Map.Cursor = previousCursor;
            HasUnsavedChanges = true;
        }
        void ImportFile() {
            ImportAnnotationItem(SelectedTreeItem);
        }

        void ImportAnnotationItem(AbstractTreeItemViewModel vm) {
            IsOptionsMapPopupOpen = false;
            importshapes = true;
            string filename = string.Empty;
            string shapetype = string.Empty;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Multiselect = false;
            openFileDialog.AddExtension = true;
            openFileDialog.CheckFileExists = false;
            openFileDialog.Filter = "Shape files (*.shp)|*.shp";
            bool? dialogresult = openFileDialog.ShowDialog();
            if (dialogresult.HasValue && dialogresult.Value) {
                filename = openFileDialog.FileName;
                log.Info("AnnotationTools - Shape import file selected: {0}.", filename);
                if (File.Exists(filename)) {
                    ShapeFileFeatureLayer shapeLayer = new ShapeFileFeatureLayer(filename, ShapeFileReadWriteMode.ReadOnly);
                    shapeLayer.RequireIndex = false;
                    if (!shapeLayer.IsOpen) {
                        shapeLayer.Open();
                    }

                    if (projection != null) {
                        if (!projection.IsOpen) {
                            projection.Open();
                        }
                    }

                    shapeLayer.FeatureSource.Projection = projection;
                    Collection<Feature> allfeatures = shapeLayer.FeatureSource.GetAllFeatures(ReturningColumnsType.AllColumns);
                    foreach (Feature feat in allfeatures) {
                        BaseShape bs = feat.GetShape();
                        if (bs is PolygonShape) {
                            shapetype = "Polygon";
                            break;
                        }
                        if (bs is MultipolygonShape) {
                            shapetype = "Polygon";
                            break;
                        }
                        if (bs is LineShape) {
                            shapetype = "Line";
                            break;
                        }
                        if (bs is MultilineShape) {
                            shapetype = "Line";
                            break;
                        }
                        if (bs is PointShape) {
                            shapetype = "Point";
                            break;
                        }
                        if (bs is MultipointShape) {
                            shapetype = "Point";
                            break;
                        }
                    }

                    var stylelist = from c in StyleNames
                            orderby c.Name
                            where c.ShapeType == shapetype
                            select c;

                    IList<MapAnnotationCategory> stylenamelist = stylelist.ToList();

                    AbstractTreeItemViewModel parentNode = SelectedTreeItem;
                    if (SelectedTreeItem == null) {
                        parentNode = RootTreeItemModels.FirstOrDefault();
                    }
                    if (SelectedTreeItem is AnnotationTreeItemViewModel) {
                        parentNode = RootTreeItemModels.FirstOrDefault();
                    }
                    if (SelectedTreeItem is AnnotationStyleTreeItemViewModel) {
                        parentNode = RootTreeItemModels.FirstOrDefault();
                    }
                    if (SelectedTreeItem is AnnotationShapeTreeItemViewModel) {
                        parentNode = SelectedTreeItem.Parent;
                    }
                    MessageMetadata metadata = new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId);
                    importAnnotationViewModel.SelectedParentNode = parentNode;
                    importAnnotationViewModel.RootNode = RootTreeItemModels.FirstOrDefault();
                    importAnnotationViewModel.Id = new MapAnnotationId(this.currentDataSourceId, Guid.NewGuid());
                    importAnnotationViewModel.StyleNames = stylenamelist;
                    importAnnotationViewModel.SelectedStyle = stylenamelist[0];
                    importAnnotationViewModel.AllFeatures = allfeatures;
                    importAnnotationViewModel.CommandList = new List<IDomainCommand>();
                    importAnnotationViewModel.CurrentCropYear = null;
                    //if (vm != null) {
                    //    importAnnotationViewModel.SelectedTreeItem = vm;
                    //    if (vm.Parent != null) {
                    //        importAnnotationViewModel.SelectedParentNode = vm.Parent;
                    //    }
                    //}
                    if (shapeLayer.IsOpen) {
                        shapeLayer.Close();
                    }
                    log.Info("AnnotationTools - Import shape " + shapetype + "  " + importAnnotationViewModel.Id.Id.ToString());

                    ScreenDescriptor descriptor = null;
                    descriptor = new ScreenDescriptor("Landdb.Views.Fields.Popups.ImportAnnotationPopupView", "ImportMa", importAnnotationViewModel);
                    Messenger.Default.Send<ShowPopupMessage>(new ShowPopupMessage() { ScreenDescriptor = descriptor });
                }
            }
        }

        void EditItem() {
            EditAnnotationItem(SelectedTreeItem);
        }

        void EditAnnotationItem(AbstractTreeItemViewModel vm) {
            importshapes = false;
            string annotationinfo = string.Empty;
            try {
                if (vm is AnnotationTreeItemViewModel) {
                    annotationinfo = "Annotation Tools:  Tree Item";
                    return;
                }
                if (vm is AnnotationStyleTreeItemViewModel) {
                    annotationinfo = "Annotation Tools:  Style Item";
                    MessageMetadata metadata = new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId);
                    AnnotationStyleTreeItemViewModel item = vm as AnnotationStyleTreeItemViewModel;
                    if (item == null) { return; }
                    IList<AnnotationStyleItem> styleColorlist = styleFactory.GetStyleList(item.Style.ShapeType);
                    if (styleColorlist == null) { return; }
                    AnnotationStyleItem annotationStyle = styleFactory.GetStyle(item.Name, item.Style.ShapeType);
                    if (annotationStyle == null) { return; }

                    editCategoryViewModel.NewItemName = item.AnnotationsDisplayItem.ToString();
                    editCategoryViewModel.StyleNames = styleColorlist;
                    editCategoryViewModel.SelectedStyle = annotationStyle;
                    editCategoryViewModel.IsVisible = item.Style.Visible;
                    editCategoryViewModel.CommandList = new List<IDomainCommand>();
                    editCategoryViewModel.CurrentCropYear = null;
                    editCategoryViewModel.RootNode = RootTreeItemModels.FirstOrDefault();
                    editCategoryViewModel.SelectedParentNode = vm.Parent;
                    editCategoryViewModel.SelectedTreeItem = vm;
                    log.Info("AnnotationTools - Edit selected tree item " + item.Name);
                    ScreenDescriptor descriptor = null;
                    descriptor = new ScreenDescriptor("Landdb.Views.Fields.Popups.EditAnnotationCategoryPopupView", "EditMa", editCategoryViewModel);
                    Messenger.Default.Send<ShowPopupMessage>(new ShowPopupMessage() { ScreenDescriptor = descriptor });
                    return;
                }
                if (vm is AnnotationShapeTreeItemViewModel) {
                    annotationinfo = "Annotation Tools:  Shape Item";
                    MessageMetadata metadata = new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId);
                    AnnotationShapeTreeItemViewModel shape = vm as AnnotationShapeTreeItemViewModel;
                    if (shape == null) { return; }
                    MapAnnotationListItem mapannotationlistitem = shape.AnnotationItem;
                    AnnotationStyleTreeItemViewModel item = shape.Parent as AnnotationStyleTreeItemViewModel;
                    if (item == null) { return; }
                    string annid = mapannotationlistitem.Id.Id.ToString();
                    Feature feat = currentLayer.InternalFeatures.Where(x => x.Id == annid).FirstOrDefault();
                    if (feat == null) { return; }
                    BaseShape baseShape = feat.GetShape();
                    if (baseShape == null) { return; }

                    YieldLocationListItemViewModel selectedyieldlocation = null;
                    foreach(YieldLocationListItemViewModel yielditem in YieldLocationList) {
                        if(yielditem.MapAnnotationID == mapannotationlistitem.Id) {
                            selectedyieldlocation = yielditem;
                            break;
                        }
                    }

                    createItemViewModel.Id = mapannotationlistitem.Id;
                    createItemViewModel.IsNew = false;
                    createItemViewModel.NewItemName = mapannotationlistitem.Name;
                    createItemViewModel.StyleNames = StyleNames;
                    createItemViewModel.SelectedStyle = item.Style;
                    createItemViewModel.ApplyCropYear = false;
                    if (item.Style.ShapeType == "Polygon") {
                        createItemViewModel.IsYieldLocation = true;
                    } else {
                        createItemViewModel.IsYieldLocation = false;
                    }
                    createItemViewModel.YieldLocationList = YieldLocationList;
                    createItemViewModel.InitialDestinationLocation = selectedyieldlocation;
                    createItemViewModel.SelectedDestinationLocation = selectedyieldlocation;
                    createItemViewModel.CommandList = new List<IDomainCommand>();
                    createItemViewModel.CommandAssociationList = new List<IDomainCommand>();
                    createItemViewModel.CurrentCropYear = null;
                    createItemViewModel.RootNode = RootTreeItemModels.FirstOrDefault();
                    createItemViewModel.SelectedParentNode = SelectedTreeItem.Parent;
                    HasUnsavedChanges = true;
                    RaisePropertyChanged("HasUnsavedChanges");
                    log.Info("AnnotationTools - Edit selected shape item " + mapannotationlistitem.Name + "  " + mapannotationlistitem.Id.Id.ToString());
                    ScreenDescriptor descriptor = null;
                    descriptor = new ScreenDescriptor("Landdb.Views.Fields.Popups.AddMapAnnotationPopupView", "AddMa", createItemViewModel);
                    Messenger.Default.Send<ShowPopupMessage>(new ShowPopupMessage() { ScreenDescriptor = descriptor });
                }
            }
            catch (Exception ex) {
                string annotationtext = string.Format("Exception while processing annotations: {0}", annotationinfo);
                log.Error(annotationtext, ex);
            }
        }

        void EditShapesMethod() {
            if (SelectedTreeItem == null) {
                return;
            }
            if (SelectedTreeItem is AnnotationTreeItemViewModel) {
                return;
            }
            if (SelectedTreeItem is AnnotationStyleTreeItemViewModel) {
                return;
            }
            if (SelectedTreeItem is AnnotationShapeTreeItemViewModel) {
                GatherEditableShape(SelectedTreeItem as AnnotationShapeTreeItemViewModel);
            }
        }

        private void GatherEditableShape(AnnotationShapeTreeItemViewModel selecteditem) {
            EnsureCurrentLayerIsOpen();
            MapAnnotationListItem mapannotationlistitem = selecteditem.AnnotationItem;
            string annid = mapannotationlistitem.Id.Id.ToString();
            Feature feat = currentLayer.InternalFeatures.Where(x => x.Id == annid).FirstOrDefault();
            if (feat == null) { return; }
            BaseShape baseShape = feat.GetShape();
            if (baseShape == null) { return; }
            BaseShape baseShape2 = projection.ConvertToExternalProjection(baseShape);
            Map.EditOverlay.EditShapesLayer.InternalFeatures.Clear();
            Map.EditOverlay.EditShapesLayer.InternalFeatures.Add(new Feature(baseShape2));
            Map.EditOverlay.CalculateAllControlPoints();
            previousCursor = Map.Cursor;
            Map.Cursor = Cursors.Hand;
            //HasUnsavedChanges = true;
            scalingTextStyle.MapScale = Map.CurrentScale;
            scalingTextStyle2.MapScale = Map.CurrentScale;
            customMultiStyles.MapScale = Map.CurrentScale;
            Map.Refresh();
        }

        private void RemoveItem() {
            importshapes = false;
            if (SelectedTreeItem.CanBeDeleted()) {
                DialogFactory.ShowYesNoDialog(Strings.RemoveItem_Text.ToLower(), string.Format(Strings.AreYouSureYouWantToRemoveThis_Text, SelectedTreeItem.ToString()), () => {
                    RemoveAnnotationItem(SelectedTreeItem);
                }, () => { });
            }
        }

        void RemoveAnnotationItem(AbstractTreeItemViewModel vm) {
            if (vm is AnnotationTreeItemViewModel) {
                return;
            }
            if (vm is AnnotationStyleTreeItemViewModel) {
                return;
            }
            if (vm is AnnotationShapeTreeItemViewModel) {
                MessageMetadata metadata = new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId);
                AnnotationShapeTreeItemViewModel shape = vm as AnnotationShapeTreeItemViewModel;
                MapAnnotationListItem mapannotationlistitem = shape.AnnotationItem;
                log.Info("AnnotationTools - Delete selected shape " + mapannotationlistitem.Name + "  " + mapannotationlistitem.Id.Id.ToString());
                var removeMapAnnotationCommand = new RemoveMapAnnotation(mapannotationlistitem.Id as MapAnnotationId, metadata);
                clientEndpoint.SendOne(removeMapAnnotationCommand);

                EnsureCurrentLayerIsOpen();
                string annid = mapannotationlistitem.Id.Id.ToString();
                Feature feat = currentLayer.InternalFeatures.Where(x => x.Id == annid).FirstOrDefault();
                if (feat != null) {
                    currentLayer.InternalFeatures.Remove(feat);
                    scalingTextStyle.MapScale = Map.CurrentScale;
                    scalingTextStyle2.MapScale = Map.CurrentScale;
                    customMultiStyles.MapScale = Map.CurrentScale;
                    Map.Refresh();
                }
            }
            vm.Parent.Children.Remove(vm);
        }

        void ExitAnnotation() {
            log.Info("AnnotationTools - Exit edit annotation");
            HasUnsavedChanges = false;
            //ReSetMap();
            importLayer.InternalFeatures.Clear();
            measureLayer.InternalFeatures.Clear();
            Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Clear();
            Map.EditOverlay.EditShapesLayer.InternalFeatures.Clear();
            Map.EditOverlay.CalculateAllControlPoints();
            Map.Cursor = Cursors.Arrow;
            scalingTextStyle.MapScale = Map.CurrentScale;
            scalingTextStyle2.MapScale = Map.CurrentScale;
            customMultiStyles.MapScale = Map.CurrentScale;
            Map.Refresh();
            importshapes = false;
        }

        private void SaveAnnotation() {
            HasUnsavedChanges = false;
            log.Info("AnnotationTools - Save annotation");
            bool usecoordinatesinlabel = createItemViewModel.UseCoordinates;
            MessageMetadata metadata = new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId);
            if (Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Count > 0) {
                string anid = createItemViewModel.Id.Id.ToString();
                int shapecount = Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Count - 1;
                Feature feat = Map.TrackOverlay.TrackShapeLayer.InternalFeatures.FirstOrDefault();
                if (feat == null) { return; }
                BaseShape baseShape = feat.GetShape();
                if (baseShape == null) { return; }
                BaseShape baseShape3 = projection.ConvertToInternalProjection(baseShape);
                string spatialData = string.Empty;
                string wkt3 = baseShape3.GetWellKnownText();
                if (selectedstyle.StyleName == "GrainBins" || selectedstyle.StyleName == "Silo") {
                    if (baseShape3 is PointShape) {
                        PointShape ps1 = baseShape3 as PointShape;
                        pointx = ps1.X;
                        pointy = ps1.Y;
                        PointShape centerpointx1 = new PointShape();
                        centerpointx1.X = pointx;
                        centerpointx1.Y = pointy;
                        EllipseShape es = new EllipseShape(centerpointx1, 15, GeographyUnit.DecimalDegree, mapDistanceUnit);
                        PolygonShape grainbinpoly = es.ToPolygon();
                        wkt3 = grainbinpoly.GetWellKnownText();
                    }
                }
                spatialData = wkt3;
                spatialData = spatialData.Replace(@"\n", "");
                spatialData = spatialData.Replace(@"\r", "");
                spatialData = spatialData.Replace(@"\t", "");

                string annlabel = string.Empty;
                PointShape centerpoint = baseShape3.GetCenterPoint();
                Vertex geoVertex1 = new Vertex(centerpoint.X, centerpoint.Y);
                string decimaldegrees = Math.Round(geoVertex1.Y, 5) + ", " + Math.Round(geoVertex1.X, 5);
                string longlat = DecimalDegrees.d_To_DMs(geoVertex1.X, 2, DecimalDegrees.Type.Longitude) + "  " + DecimalDegrees.d_To_DMs(geoVertex1.Y, 2, DecimalDegrees.Type.Latitude);
                string longlatAG = DecimalDegrees.d_To_Dm(geoVertex1.X, 4, DecimalDegrees.Type.Longitude) + "  " + DecimalDegrees.d_To_Dm(geoVertex1.Y, 4, DecimalDegrees.Type.Latitude);
                string coordinatestring = string.Empty;
                if (usecoordinatesinlabel) {
                    coordinatestring = " " + longlat;
                    if (this.mapsettings.DisplayCoordinateFormat == "Decimal Degree") {
                        coordinatestring = " " + decimaldegrees;
                    }
                    if (this.mapsettings.DisplayCoordinateFormat == "Decimal Seconds AG") {
                        coordinatestring = " " + decimaldegrees;
                    }
                }

                foreach (AnnotationTreeItemViewModel root in RootTreeItemModelsView) {
                    bool stylefound = false;
                    try
                    {
                        foreach (AnnotationStyleTreeItemViewModel style in root.Children)
                        {
                            if (selectedstyle.Name == style.Name)
                            {
                                if (selectedTreeItem is AnnotationShapeTreeItemViewModel)
                                {
                                    AnnotationShapeTreeItemViewModel createdannotation = selectedTreeItem as AnnotationShapeTreeItemViewModel;
                                    style.Children.Add(createdannotation);
                                    stylefound = true;
                                    MapAnnotationListItem mapannotationlistitem = createdannotation.AnnotationItem;
                                    string annotationname = mapannotationlistitem.Name;
                                    if (usecoordinatesinlabel)
                                    {
                                        annotationname = mapannotationlistitem.Name + coordinatestring;
                                    }
                                    annlabel = annotationname;
                                    anid = mapannotationlistitem.Id.Id.ToString();
                                    CreateMapAnnotation createMapAnnotationCommand = new CreateMapAnnotation(mapannotationlistitem.Id, metadata, annotationname, spatialData, mapannotationlistitem.AnnotationType, mapannotationlistitem.ShapeType, mapannotationlistitem.CropYear, null);
                                    clientEndpoint.SendOne(createMapAnnotationCommand);
                                }
                                break;
                            }
                        }
                        if (!stylefound)
                        {
                            if (selectedTreeItem is AnnotationStyleTreeItemViewModel)
                            {
                                AnnotationStyleTreeItemViewModel createdannotation = selectedTreeItem as AnnotationStyleTreeItemViewModel;
                                createdannotation.IsExpanded = true;
                                root.Children.Add(createdannotation);
                                if (userstylenames.ContainsKey(createdannotation.Name))
                                {
                                    userstylenames[createdannotation.Name] = createdannotation.Style;
                                }
                                else
                                {
                                    userstylenames.Add(createdannotation.Name, createdannotation.Style);
                                }

                                AnnotationShapeTreeItemViewModel createdannotation1 = selectedTreeItem.Children[0] as AnnotationShapeTreeItemViewModel;
                                MapAnnotationListItem mapannotationlistitem = createdannotation1.AnnotationItem;
                                string annotationname = mapannotationlistitem.Name;
                                if (usecoordinatesinlabel)
                                {
                                    annotationname = mapannotationlistitem.Name + coordinatestring;
                                }
                                annlabel = annotationname;
                                anid = mapannotationlistitem.Id.Id.ToString();
                                CreateMapAnnotation createMapAnnotationCommand = new CreateMapAnnotation(mapannotationlistitem.Id, metadata, annotationname, spatialData, mapannotationlistitem.AnnotationType, mapannotationlistitem.ShapeType, mapannotationlistitem.CropYear, null);
                                clientEndpoint.SendOne(createMapAnnotationCommand);
                            }
                        }
                        break;
                    } catch (Exception ex01)
                    {
                        MessageBox.Show(Strings.ThereWasAProblemFIndingTheStyle_Text);
                    }
                }
                RaisePropertyChanged("RootTreeItemModelsView");
                RaisePropertyChanged("RootTreeItemModels");

                IDictionary<string, string> columnvalues = new Dictionary<string, string>();
                columnvalues.Add(@"Id", anid);
                columnvalues.Add(@"LdbLabel", annlabel);
                columnvalues.Add(@"TypeName", selectedstyle.Name);
                columnvalues.Add(@"ShapeType", selectedstyle.ShapeType);
                Feature newfeature = new Feature(spatialData, anid, columnvalues);
                currentLayer.InternalFeatures.Add(anid, newfeature);
                Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Clear();
                scalingTextStyle.MapScale = Map.CurrentScale;
                scalingTextStyle2.MapScale = Map.CurrentScale;
                customMultiStyles.MapScale = Map.CurrentScale;
                Map.Refresh();
            }
            if (Map.EditOverlay.EditShapesLayer.InternalFeatures.Count > 0) {
                if (importshapes) {
                    foreach (Feature feat in Map.EditOverlay.EditShapesLayer.InternalFeatures) {
                        try
                        {
                            BaseShape baseShape = feat.GetShape();
                            if (baseShape == null) { continue; }
                            BaseShape baseShape3 = projection.ConvertToInternalProjection(baseShape);
                            string spatialData = string.Empty;
                            string wkt3 = baseShape3.GetWellKnownText();
                            spatialData = wkt3;
                            spatialData = spatialData.Replace(@"\n", "");
                            spatialData = spatialData.Replace(@"\r", "");
                            spatialData = spatialData.Replace(@"\t", "");

                            string anid = Guid.NewGuid().ToString();
                            string featurekey = feat.Id;
                            string featurelabel = " ";
                            int iii = 0;
                            foreach (Feature shapefeature in importAnnotationViewModel.AllFeatures)
                            {
                                iii++;
                                //string recid = shapefeature.ColumnValues["RecID"];
                                string recid = iii.ToString();
                                if (featurekey == recid)
                                {
                                    if (shapefeature.ColumnValues.ContainsKey("LdbLabel"))
                                    {
                                        featurelabel = shapefeature.ColumnValues["LdbLabel"];
                                    } else
                                    {
                                        featurelabel = String.Format(Strings.Item_Format_Text, recid);
                                    }
                                }
                            }

                            foreach (AnnotationTreeItemViewModel root in RootTreeItemModelsView)
                            {
                                bool stylefound = false;
                                MapAnnotationId id = new MapAnnotationId(this.currentDataSourceId, Guid.NewGuid());
                                MapAnnotationListItem mapannotationlistitem = new MapAnnotationListItem() { Id = id, Name = featurelabel, AnnotationType = selectedstyle.Name, ShapeType = selectedstyle.ShapeType, CropYear = null };
                                List<MapAnnotationListItem> annotationlist = new List<MapAnnotationListItem>() { mapannotationlistitem };
                                CreateMapAnnotation createMapAnnotationCommand = new CreateMapAnnotation(id, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), featurelabel, spatialData, selectedstyle.Name, selectedstyle.ShapeType, null, null);
                                clientEndpoint.SendOne(createMapAnnotationCommand);

                                foreach (AnnotationStyleTreeItemViewModel style in root.Children)
                                {
                                    if (selectedstyle.Name == style.Name)
                                    {
                                        AnnotationShapeTreeItemViewModel annotationstyle = new AnnotationShapeTreeItemViewModel(mapannotationlistitem, style, true, filter: FilterModel.FilterItem);
                                        style.Children.Add(annotationstyle);
                                        stylefound = true;
                                        break;
                                    }
                                }
                                if (!stylefound)
                                {
                                    AnnotationStyleTreeItemViewModel annotationstyle = new AnnotationStyleTreeItemViewModel(selectedstyle, annotationlist, root, true, filter: FilterModel.FilterItem);
                                    annotationstyle.IsExpanded = true;
                                    root.Children.Add(annotationstyle);
                                }
                            }

                            IDictionary<string, string> columnvalues = new Dictionary<string, string>();
                            columnvalues.Add(@"Id", anid);
                            columnvalues.Add(@"LdbLabel", featurelabel);
                            columnvalues.Add(@"TypeName", selectedstyle.Name);
                            columnvalues.Add(@"ShapeType", selectedstyle.ShapeType);
                            Feature newfeature = new Feature(spatialData, anid, columnvalues);
                            currentLayer.InternalFeatures.Add(anid, newfeature);
                        }
                        catch (Exception ex02)
                        {
                            MessageBox.Show(Strings.ThereWasAProblemAddingShapesToCurrentLayer_Text);
                        }

                    }
                    Map.EditOverlay.EditShapesLayer.InternalFeatures.Clear();
                    scalingTextStyle.MapScale = Map.CurrentScale;
                    scalingTextStyle2.MapScale = Map.CurrentScale;
                    customMultiStyles.MapScale = Map.CurrentScale;
                    Map.Refresh();
                    RaisePropertyChanged("RootTreeItemModelsView");
                    RaisePropertyChanged("RootTreeItemModels");
                }
                else {
                    string anid = createItemViewModel.Id.Id.ToString();
                    int shapecount = Map.EditOverlay.EditShapesLayer.InternalFeatures.Count - 1;
                    Feature feat = Map.EditOverlay.EditShapesLayer.InternalFeatures.FirstOrDefault();
                    if (feat == null) { return; }
                    BaseShape baseShape = feat.GetShape();
                    if (baseShape == null) { return; }
                    BaseShape baseShape3 = projection.ConvertToInternalProjection(baseShape);
                    string spatialData = string.Empty;
                    string wkt3 = baseShape3.GetWellKnownText();
                    spatialData = wkt3;
                    spatialData = spatialData.Replace(@"\n", "");
                    spatialData = spatialData.Replace(@"\r", "");
                    spatialData = spatialData.Replace(@"\t", "");

                    string annlabel = string.Empty;
                    PointShape centerpoint = baseShape3.GetCenterPoint();
                    Vertex geoVertex1 = new Vertex(centerpoint.X, centerpoint.Y);
                    string decimaldegrees = Math.Round(geoVertex1.Y, 5) + ", " + Math.Round(geoVertex1.X, 5);
                    string longlat = DecimalDegrees.d_To_DMs(geoVertex1.X, 2, DecimalDegrees.Type.Longitude) + "  " + DecimalDegrees.d_To_DMs(geoVertex1.Y, 2, DecimalDegrees.Type.Latitude);
                    string longlatAG = DecimalDegrees.d_To_Dm(geoVertex1.X, 4, DecimalDegrees.Type.Longitude) + "  " + DecimalDegrees.d_To_Dm(geoVertex1.Y, 4, DecimalDegrees.Type.Latitude);
                    string coordinatestring = string.Empty;
                    if (usecoordinatesinlabel) {
                        coordinatestring = " " + longlat;
                        if (this.mapsettings.DisplayCoordinateFormat == "Decimal Degree") {
                            coordinatestring = " " + decimaldegrees;
                        }
                        if (this.mapsettings.DisplayCoordinateFormat == "Decimal Seconds") {
                            coordinatestring = " " + decimaldegrees;
                        }
                    }

                    bool treeitemfound = false;
                    for (int x = 0; x < RootTreeItemModels.Count; x++) {
                        if (treeitemfound) { break; }
                        try
                        {
                            AnnotationTreeItemViewModel root = RootTreeItemModels[x] as AnnotationTreeItemViewModel;
                            for (int y = 0; y < root.Children.Count; y++)
                            {
                                if (treeitemfound) { break; }
                                AnnotationStyleTreeItemViewModel style = root.Children[y] as AnnotationStyleTreeItemViewModel;
                                if (style.Id == selectedTreeItem.Id)
                                {
                                    style = selectedTreeItem as AnnotationStyleTreeItemViewModel;
                                    treeitemfound = true;
                                }
                                for (int z = 0; z < style.Children.Count; z++)
                                {
                                    if (treeitemfound) { break; }
                                    AnnotationShapeTreeItemViewModel shape = style.Children[z] as AnnotationShapeTreeItemViewModel;
                                    if (shape.Id == selectedTreeItem.Id)
                                    {
                                        shape = selectedTreeItem as AnnotationShapeTreeItemViewModel;
                                        treeitemfound = true;
                                        MapAnnotationListItem mapannotationlistitem = shape.AnnotationItem;
                                        var relocateMapAnnotationCommand = new RelocateMapAnnotation(mapannotationlistitem.Id as MapAnnotationId, metadata, spatialData);
                                        clientEndpoint.SendOne(relocateMapAnnotationCommand);
                                        string annotationname = mapannotationlistitem.Name;
                                        if (usecoordinatesinlabel)
                                        {
                                            annotationname = mapannotationlistitem.Name + coordinatestring;
                                        }
                                        annlabel = annotationname;
                                        anid = mapannotationlistitem.Id.Id.ToString();
                                        var renameMapAnnotationCommand = new RenameMapAnnotation(mapannotationlistitem.Id, metadata, annotationname);
                                        clientEndpoint.SendOne(renameMapAnnotationCommand);
                                        shape = new AnnotationShapeTreeItemViewModel(mapannotationlistitem, style, true, filter: FilterModel.FilterItem);
                                        RootTreeItemModels[x].Children[y].Children[z] = shape;
                                        break;
                                    }
                                }
                            }
                        }
                        catch (Exception ex03)
                        {
                            MessageBox.Show(Strings.ThereWasAProblemAddingTreeItems_Text);
                        }

                    }
                    RaisePropertyChanged("RootTreeItemModelsView");
                    RaisePropertyChanged("RootTreeItemModels");

                    if (currentLayer.InternalFeatures.Contains(anid)) {
                        Feature geofeature = currentLayer.InternalFeatures[anid];
                        currentLayer.InternalFeatures.Remove(anid);
                        IDictionary<string, string> columnvalues = new Dictionary<string, string>();
                        columnvalues.Add(@"Id", anid);
                        columnvalues.Add(@"LdbLabel", annlabel);
                        columnvalues.Add(@"TypeName", selectedstyle.Name);
                        columnvalues.Add(@"ShapeType", selectedstyle.ShapeType);
                        Feature newfeature = new Feature(spatialData, anid, columnvalues);
                        currentLayer.InternalFeatures.Add(anid, newfeature);
                        Map.EditOverlay.EditShapesLayer.InternalFeatures.Clear();
                        Map.EditOverlay.CalculateAllControlPoints();
                        scalingTextStyle.MapScale = Map.CurrentScale;
                        scalingTextStyle2.MapScale = Map.CurrentScale;
                        customMultiStyles.MapScale = Map.CurrentScale;
                        Map.Refresh();

                    }
                }
            }
            if(commandlist.Count > 0) {
                foreach(var commanditem in commandlist) {
                    clientEndpoint.SendOne(commanditem);
                }
                commandlist.Clear();
            }
            importshapes = false;
        }

        void Finish() {
            log.Info("AnnotationTools - Quit annotations");
            Dictionary<string, MapAnnotationCategory> stylecat = new Dictionary<string, MapAnnotationCategory>();
            AbstractTreeItemViewModel rootNode = RootTreeItemModels.FirstOrDefault();
            if (rootNode != null) {
                foreach (AnnotationStyleTreeItemViewModel stylevm in rootNode.Children) {
                    if (userstylenames.ContainsKey(stylevm.Name)) {
                        userstylenames[stylevm.Name] = stylevm.Style;
                    }
                    else {
                        userstylenames.Add(stylevm.Name, stylevm.Style);
                    }
                    if (this.mapsettings.MapAnnotationCategories.ContainsKey(stylevm.Name)) {
                        this.mapsettings.MapAnnotationCategories[stylevm.Name] = stylevm.Style;
                    }
                    else {
                        this.mapsettings.MapAnnotationCategories.Add(stylevm.Name, stylevm.Style);
                    }
                }
                StyleData.Self.UserStyleNames = userstylenames;
                this.mapsettings.MapAnnotationOpacity = annotationopacity;
                clientEndpoint.SaveMapSettings(this.mapsettings);
            }
            //ReSetMap();
            currentLayer.InternalFeatures.Clear();
            importLayer.InternalFeatures.Clear();
            measureLayer.InternalFeatures.Clear();
            Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Clear();
            Map.EditOverlay.EditShapesLayer.InternalFeatures.Clear();
            Map.EditOverlay.CalculateAllControlPoints();
            Map.Cursor = Cursors.Arrow;
            scalingTextStyle.MapScale = Map.CurrentScale;
            scalingTextStyle2.MapScale = Map.CurrentScale;
            customMultiStyles.MapScale = Map.CurrentScale;
            Map.Refresh();
            importshapes = false;
            Messenger.Default.Send<ShowMainViewMessage>(new ShowMainViewMessage());
        }

        public void ReSetMap() {
            currentLayer.InternalFeatures.Clear();
            importLayer.InternalFeatures.Clear();
            measureLayer.InternalFeatures.Clear();
        }

        internal void SaveMeasureLayer() {
            try {
                var features = Map.TrackOverlay.TrackShapeLayer.InternalFeatures;
                foreach (Feature feat in features) {
                    var shape = feat.GetShape();
                    PointShape centerpoint = shape.GetCenterPoint();
                    BaseShape bs1 = DisplayMeasureBalloon(shape);
                    BaseShape bs2 = projection.ConvertToInternalProjection(bs1);
                    measureLayer.InternalFeatures.Add(new Feature(bs2));
                    double areaTotal = 0;
                    double perimeterTotal = 0;
                    string displayerText = string.Empty;
                    if (bs2 is PolygonShape) {
                        PolygonShape poly1 = bs2 as PolygonShape;
                        areaTotal = poly1.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
                        perimeterTotal = poly1.GetPerimeter(GeographyUnit.DecimalDegree, mapDistanceUnit);
                        Measure perimetermeasure = MapUnitFactory.LengthConversion(mapDistanceUnit.ToString(), perimeterTotal);
                        Measure areameasure = MapUnitFactory.AreaConversion(mapAreaUnit.ToString(), areaTotal);
                        displayerText = $"{Strings.Area_Text}: {areameasure.FullDisplay} \r\n{Strings.Perimeter_Text}: {perimetermeasure.FullDisplay}";
                    }
                    if (bs2 is LineShape) {
                        LineShape line1 = bs2 as LineShape;
                        perimeterTotal = line1.GetLength(GeographyUnit.DecimalDegree, mapDistanceUnit);
                        Measure perimetermeasure = MapUnitFactory.LengthConversion(mapDistanceUnit.ToString(), perimeterTotal);
                        displayerText = $"{Strings.Length_Text}: {perimetermeasure.FullDisplay}";
                        LineShape lineshape2 = shape as LineShape;
                        for (int xyz = 0; xyz <= (lineshape2.Vertices.Count / 2); xyz++) {
                            centerpoint = new PointShape(lineshape2.Vertices[xyz]);
                        }
                    }
                    Popup popup = new Popup(centerpoint);
                    popup.Loaded += new System.Windows.RoutedEventHandler(popup_Loaded);
                    popup.MouseDoubleClick += new MouseButtonEventHandler(mouse_DoubleClick);
                    System.Windows.Controls.TextBox displayer = new System.Windows.Controls.TextBox();
                    displayer.Text = displayerText;
                    popup.Content = displayer;
                    //popup.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(255, 0, 0));
                    popupOverlay.Popups.Add(popup);
                    popupOverlay.Refresh();
                }
                Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Clear();
                Map.Refresh();
            }
            catch (Exception ex) {
                log.Error("Error saving measure line - {0}", ex);
                System.Windows.MessageBox.Show(Strings.ThereWasAProblemSavingAMeasureLine_Text);
            }
        }

        void popup_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            Popup p = sender as Popup;
            //System.Windows.Controls.Border b = p.Template.FindName("BorderTemplate", p) as System.Windows.Controls.Border;
            //b.CornerRadius = new System.Windows.CornerRadius(10);
        }

        void mouse_DoubleClick(object sender, MouseButtonEventArgs e) {
            Popup p = sender as Popup;
            if (popupOverlay.Popups.Contains(p)) {
                popupOverlay.Popups.Remove(p);
                popupOverlay.Refresh();
            }
        }

        private BaseShape DisplayMeasureBalloon(BaseShape shape) {
            if (shape is LineBaseShape) {
                if (shape is LineShape) {
                    LineShape lineshape = shape as LineShape;
                    var firstvert = lineshape.Vertices.First();
                    var lastvert = lineshape.Vertices.Last();
                    var allverts = from vt in lineshape.Vertices
                                   select vt;

                    PointShape point1 = new PointShape(firstvert);
                    PointShape point2 = new PointShape(lastvert);
                    if (lineshape.Vertices.Count > 2) {
                        ScreenPointF screenPoint1 = ExtentHelper.ToScreenCoordinate(Map.CurrentExtent, point1, System.Convert.ToSingle(Map.ActualWidth), System.Convert.ToSingle(Map.ActualHeight));
                        ScreenPointF screenPoint2 = ExtentHelper.ToScreenCoordinate(Map.CurrentExtent, point2, System.Convert.ToSingle(Map.ActualWidth), System.Convert.ToSingle(Map.ActualHeight));
                        double distance = Math.Sqrt(((screenPoint1.X - screenPoint2.X) * (screenPoint1.X - screenPoint2.X)) + ((screenPoint1.Y - screenPoint2.Y) * (screenPoint1.Y - screenPoint2.Y)));
                        if (distance >= -10 && distance <= 10) {
                            PolygonShape polygon = new PolygonShape();
                            Collection<Vertex> collectvertex = lineshape.Vertices;
                            collectvertex.Add(lineshape.Vertices[0]);
                            RingShape rs = new RingShape(collectvertex);
                            polygon.OuterRing = rs;
                            return polygon;
                        }
                    }
                    return lineshape;
                }
                if (shape is MultilineShape) {
                    MultilineShape mlshape = shape as MultilineShape;
                    var firstvert = mlshape.Lines.First().Vertices.First();
                    var lastvert = mlshape.Lines.Last().Vertices.Last();
                    var allverts = from ml in mlshape.Lines
                                   from vt in ml.Vertices
                                   select vt;

                    PointShape point1 = new PointShape(firstvert);
                    PointShape point2 = new PointShape(lastvert);
                    if (allverts.Count() > 2) {
                        ScreenPointF screenPoint1 = ExtentHelper.ToScreenCoordinate(Map.CurrentExtent, point1, System.Convert.ToSingle(Map.ActualWidth), System.Convert.ToSingle(Map.ActualHeight));
                        ScreenPointF screenPoint2 = ExtentHelper.ToScreenCoordinate(Map.CurrentExtent, point2, System.Convert.ToSingle(Map.ActualWidth), System.Convert.ToSingle(Map.ActualHeight));
                        double distance = Math.Sqrt(((screenPoint1.X - screenPoint2.X) * (screenPoint1.X - screenPoint2.X)) + ((screenPoint1.Y - screenPoint2.Y) * (screenPoint1.Y - screenPoint2.Y)));
                        if (distance >= -10 && distance <= 10) {
                            PolygonShape polygon = new PolygonShape();
                            polygon.OuterRing = new RingShape(allverts);
                            return polygon;
                        }
                    }
                    return mlshape;
                }
            }
            return null;
        }

        internal void RefreshDestinationLocationList() {
            this.currentCropYear = ApplicationEnvironment.CurrentCropYear;
            CropYearId currentCropYearId = new CropYearId(ApplicationEnvironment.CurrentDataSourceId, this.currentCropYear);

            YieldLocationList.Clear();

            var segmentTypes = clientEndpoint.GetMasterlistService()
                .GetYieldLocationSegmentTypes()
                .SegmentTypeList
                .Select(x => x.Name);

            var locList = YieldLocationListItemViewModel.GetFlattenedListItemsForCropYear(clientEndpoint, currentCropYearId, false);

            (from loc in locList
             let hasSublocs = loc.SubLocationList.Any()
             where !segmentTypes.Contains(loc.Descriptor) || hasSublocs
             select loc).ForEach(x => YieldLocationList.Add(x));


            for(int i = 0; i < YieldLocationList.Count; i++) {
                var locationsMaybe = clientEndpoint.GetView<YieldLocationDetailsView>(YieldLocationList[i].Id);
                if (locationsMaybe.HasValue) {
                    YieldLocationList[i].MapAnnotationID = locationsMaybe.Value.MapAnnotationID;
                }
            }

            DestinationLocationList = YieldLocationList.ToList();

            //RaisePropertyChanged("YieldLocationList");
            //RaisePropertyChanged("YieldLocationList");
        }
    }

    public class AnnotationItem : ViewModelBase {
        public AnnotationItem(string label, string typename, string shape) {
            this.label = label;
            this.typename = typename;
            this.shape = shape;
        }

        private string label = string.Empty;
        public string Label {
            get { return label; }
            set {
                if (label == value) { return; }
                label = value;
                RaisePropertyChanged("Label");
            }
        }

        private string typename = string.Empty;
        public string TypeName {
            get { return typename; }
            set {
                if (typename == value) { return; }
                typename = value;
                RaisePropertyChanged("TypeName");
            }
        }

        private string shape = string.Empty;
        public string Shape {
            get { return shape; }
            set {
                if (shape == value) { return; }
                shape = value;
                RaisePropertyChanged("Shape");
            }
        }
    }
}
