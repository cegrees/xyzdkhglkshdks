﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Shapes;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using AgC.UnitConversion;
using Landdb.Client.Spatial;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Secondary.Map;
using Landdb.Infrastructure;
using Landdb.Domain.ReadModels.MapAnnotation;
using Landdb.ViewModel.Fields;
using Landdb.MapStyles;
using System.Collections.ObjectModel;
using ThinkGeo.MapSuite.Core;

namespace Landdb.ViewModel.Secondary.Map {
    public class ImportAnnotationViewModel : ViewModelBase {
        readonly IClientEndpoint clientEndpoint;
        AbstractTreeItemViewModel rootNode;
        AbstractTreeItemViewModel selectedParentNode;
        AbstractTreeItemViewModel selectedTreeItem;
        //MiniCrop newItemCropSelection;
        bool isContinuouslyAdding = false;
        bool useparentboundary = false;
        Visibility useparentboundaryvisibility = Visibility.Collapsed;
        Visibility iscontinuouslyaddingvisibility = Visibility.Collapsed;
        bool isAddItemPopupOpen;
        Action<AbstractTreeItemViewModel> onNodeCreated;
        Action onNodeIgnored;

        int? currentCropYear;
        Guid currentDataSourceId;

        Path shapePreview;
        string mapData;

        public ImportAnnotationViewModel(IClientEndpoint clientEndpoint, Action<AbstractTreeItemViewModel> onNodeCreated, Action onNodeIgnored) {
            this.clientEndpoint = clientEndpoint;
            this.onNodeCreated = onNodeCreated;
            this.onNodeIgnored = onNodeIgnored;

            if (!string.IsNullOrEmpty(this.mapData)) {
                this.shapePreview = WpfTransforms.CreatePathFromMapData(new[] { this.mapData }, 55);
            }
            commandlist = new List<IDomainCommand>();

            CreateNewItemCommand = new RelayCommand(CreateNewItem);
            CancelCreateCommand = new RelayCommand(CancelCreate);

            FilterModel = new TreeFilterModel(clientEndpoint);
        }

        public ICommand CreateNewItemCommand { get; private set; }
        public ICommand CancelCreateCommand { get; private set; }

        MapAnnotationId id;
        public MapAnnotationId Id {
            get { return id; }
            set {
                id = value;
            }
        }

        private IList<MapAnnotationCategory> stylenames = new List<MapAnnotationCategory>();
        public IList<MapAnnotationCategory> StyleNames {
            get { return stylenames; }
            set {
                //if (stylenames == value) { return; }
                stylenames = value;
                RaisePropertyChanged("StyleNames");
            }
        }

        private MapAnnotationCategory selectedstyle;
        public MapAnnotationCategory SelectedStyle {
            get { return selectedstyle; }
            set {
                if (selectedstyle == value) { return; }
                selectedstyle = value;
                RaisePropertyChanged("SelectedStyle");
            }
        }

        string newItemName;
        public string NewItemName {
            get { return newItemName; }
            set {
                newItemName = value;
                RaisePropertyChanged("NewItemName");
            }
        }

        bool isVisible = true;
        public bool IsVisible {
            get { return isVisible; }
            set {
                isVisible = value;
                RaisePropertyChanged("IsVisible");
            }
        }

        private IList<IDomainCommand> commandlist = new List<IDomainCommand>();
        public IList<IDomainCommand> CommandList {
            get { return commandlist; }
            set { commandlist = value; }
        }

        private Collection<Feature> allfeatures = new Collection<Feature>();
        public Collection<Feature> AllFeatures {
            get { return allfeatures; }
            set { allfeatures = value; }
        }

        public int? CurrentCropYear {
            get { return currentCropYear; }
            set {
                currentCropYear = value;
            }
        }
        public Guid CurrentDataSourceId {
            get { return currentDataSourceId; }
            set {
                currentDataSourceId = value;
            }
        }

        public IPageFilterViewModel FilterModel { get; protected set; }

        public AbstractTreeItemViewModel RootNode {
            get { return rootNode; }
            set {
                rootNode = value;
                RaisePropertyChanged("RootNode");
            }
        }

        public AbstractTreeItemViewModel SelectedParentNode {
            get { return selectedParentNode; }
            set {
                selectedParentNode = value;
                RaisePropertyChanged("SelectedParentNode");
            }
        }

        public AbstractTreeItemViewModel SelectedTreeItem {
            get { return selectedTreeItem; }
            set { selectedTreeItem = value; }
        }

        public bool IsAddItemPopupOpen {
            get { return isAddItemPopupOpen; }
            set {
                isAddItemPopupOpen = value;
                RaisePropertyChanged("IsAddItemPopupOpen");
            }
        }

        void CreateNewItem() {
            SelectedStyle.Visible = isVisible;
            List<MapAnnotationListItem> annotationlist = new List<MapAnnotationListItem>();
            bool stylefound = false;
            foreach (AnnotationStyleTreeItemViewModel style in RootNode.Children) {
                if (selectedstyle.Name == style.Name) {
                    selectedParentNode = style;
                    stylefound = true;
                    break;
                }
            }
            if (!stylefound) {
                AnnotationStyleTreeItemViewModel annotationstyle = new AnnotationStyleTreeItemViewModel(selectedstyle, annotationlist, RootNode, true, filter: FilterModel.FilterItem);
                annotationstyle.IsExpanded = true;
                selectedTreeItem = annotationstyle.Children.FirstOrDefault();
                selectedParentNode = annotationstyle;
            }

            onNodeCreated(selectedTreeItem); // notify the parent
            IsAddItemPopupOpen = false;
            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
        }

        void CancelCreate() {
            IsAddItemPopupOpen = false;
            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
        }
    }
}
