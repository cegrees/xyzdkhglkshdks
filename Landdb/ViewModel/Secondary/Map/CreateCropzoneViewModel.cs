﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using AgC.UnitConversion;
using AgC.UnitConversion.Area;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.MapStyles;
using Landdb.ViewModel.Fields;
using ThinkGeo.MapSuite.Core;
using ThinkGeo.MapSuite.WpfDesktopEdition;
//using System.Reactive;
//using System.Reactive.Linq;
//using System.Reactive.Disposables;
using Landdb.ViewModel.Maps.ImageryOverlay;
using Landdb.ViewModel.Fields.FieldDetails.MapModels;
using NLog;
using AgC.UnitConversion.Length;
using Landdb.Resources;

namespace Landdb.ViewModel.Secondary.Map {
    public class CreateCropzoneViewModel : ViewModelBase
    {
        //IMapOverlay selectedoverlay;
        IClientEndpoint clientEndpoint;
        Dispatcher dispatcher;
        bool hasunsavedchanges = false;
        bool readytofinish = false;
        bool readytobuild = false;
        bool readytoassign = false;
        bool showgrid = false;
        Logger log = LogManager.GetCurrentClassLogger();

        InMemoryFeatureLayer currentLayer = new InMemoryFeatureLayer();         // Selected Field Layer
        InMemoryFeatureLayer zoneLayer = new InMemoryFeatureLayer();            // CropZone Layer
        InMemoryFeatureLayer labelLayer = new InMemoryFeatureLayer();           // CropZone Label Layer
        InMemoryFeatureLayer selectLayer = new InMemoryFeatureLayer();          // Selected Shape Assignment
        InMemoryFeatureLayer measureLayer = new InMemoryFeatureLayer();         // Measure Layer
        InMemoryFeatureLayer measureLayer2 = new InMemoryFeatureLayer();        // Measure upper and lower distances from previous cutline
        InMemoryFeatureLayer measureLayer9 = new InMemoryFeatureLayer();        // measure cutting polygons
        InMemoryFeatureLayer bboxLayer = new InMemoryFeatureLayer();            // Bounding Box
        InMemoryFeatureLayer ablinesVertLayer = new InMemoryFeatureLayer();     // AB Lines
        InMemoryFeatureLayer ablinesHorzLayer = new InMemoryFeatureLayer();     // AB Lines
        InMemoryFeatureLayer drawlinesVertLayer = new InMemoryFeatureLayer();   // Draw Lines
        InMemoryFeatureLayer drawlinesHorzLayer = new InMemoryFeatureLayer();   // Draw Lines

        InMemoryFeatureLayer spatialQueryResultLayer = new InMemoryFeatureLayer();

        LayerOverlay layerOverlay = new LayerOverlay();
        LayerOverlay zoneOverlay = new LayerOverlay();
        LayerOverlay gridOverlay = new LayerOverlay();
        LayerOverlay selectOverlay = new LayerOverlay();
        PopupOverlay popupOverlay = new PopupOverlay();
        Proj4Projection projection;
        Collection<Feature> spatialQueryResults = new Collection<Feature>();
        bool maploaded = false;
        bool startedOutOnline = false;
        bool skipwhilemousedown = false;
        bool skipwhiledrawing = false;
        bool draggingfeatures = false;
        bool removingvertex = false;
        bool trackingfeatures = false;
        bool measuretool = false;
        bool borderrowtool = false;
        bool drawpivottool = false;
        bool deletetool = false;
        bool assigntool = false;
        bool mergetool = false;
        bool isAssigningMode = false;
        bool isMergeMode = false;
        bool sortAlphabetically = false;
        bool isCreateCropzoneToolsPopupOpen = false;
        bool setupforoneline = false;
        bool buildalllines = true;
        bool buildonelines = true;
        bool buildborderrows = true;
        bool builddrawborderrows = true;
        bool buildonepivot = true;
        bool displayborderrow = false;
        bool showMeasurementsCommands = false;
        bool drawingpivot = true;
        bool drawingpartialpivot = true;
        bool snipapolygon = false;
        bool thiswasfromtracking = false;
        bool featureFound = false;
        string fieldname = string.Empty;
        string commandDescription = "                 ";
        object detailsViewModel;

        RectangleShape bbox = new RectangleShape();
        LineShape ablineVert = new LineShape();
        LineShape ablineHorz = new LineShape();

        System.Collections.ObjectModel.Collection<ThinkGeo.MapSuite.Core.Feature> selectedFeatures;
        Feature selectedFeature;
        Feature selectedPivot;
        MultipolygonShape compiledpoly = new MultipolygonShape();
        Feature selectedfield = new Feature();
        Feature loadedfeature = new Feature();
        BaseShape fieldshape = new MultipolygonShape();
        RectangleShape previousmapextent = new RectangleShape();
        Cursor previousCursor;
        string modeofoperation = "Drawing Mode";
        int operatingmode = 0;
        Vertex previousvertex = new Vertex();
        float prevx = 0;
        float prevy = 0;
        string labelcolumn = "Label";
        string namecolumn = "Name";
        string idcolumn = "Id";
        string areacolumn = "Area";
        int countmoving = 0;
        int countmoved = 0;
        //bool IsMeasureLine = false;
        MapStyles.CustomDistancePolygonStyle customDistancePolygonStyleText = new MapStyles.CustomDistancePolygonStyle(ThinkGeo.MapSuite.Core.AreaUnit.Acres, ThinkGeo.MapSuite.Core.DistanceUnit.Feet, new GeoFont("Arial", 12, DrawingFontStyles.Regular), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.White), new GeoSolidBrush(GeoColor.StandardColors.Red), new GeoPen(GeoColor.StandardColors.Red));
        MapStyles.CustomDistanceLineStyle customDistanceLineStyleText = new MapStyles.CustomDistanceLineStyle(DistanceUnit.Feet, new GeoFont("Arial", 12, DrawingFontStyles.Regular), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.DarkRed, 2), new GeoPen(GeoColor.StandardColors.DarkBlue, 2));
        MapStyles.CustomDistanceLineStyle customDistanceLineStyleText2 = new MapStyles.CustomDistanceLineStyle(DistanceUnit.Feet, new GeoFont("Arial", 12, DrawingFontStyles.Regular), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.DarkRed, 2), new GeoPen(GeoColor.StandardColors.DarkBlue, 2));
        MapStyles.CustomDistanceLineStyle customDistanceLineStyleText9 = new MapStyles.CustomDistanceLineStyle(DistanceUnit.Feet, new GeoFont("Arial", 12, DrawingFontStyles.Regular), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.DarkRed, 2), new GeoPen(GeoColor.StandardColors.DarkBlue, 2));
        LineStyle lineStylePen = new LineStyle(new GeoPen(GeoColor.FromArgb(150, GeoColor.StandardColors.Orange), 4));
        LineStyle lineStylePen2 = new LineStyle(new GeoPen(GeoColor.FromArgb(150, GeoColor.StandardColors.Orange), 4));
        AreaStyle areaStyleBrush2 = new AreaStyle(new GeoSolidBrush(GeoColor.StandardColors.Blue));
        CreateTreeItemMapCZAssignerViewModel createItemViewModel;
        int assignedFeatureIndex = 0;
        CropzoneAreaStyle fieldsLayerAreaStyle = null;
        ScalingTextStyle scalingTextStyle = new ScalingTextStyle(GeographyUnit.DecimalDegree, @"Label", 19000, 1200, 24, 2);
        private Dictionary<string, double> rotationAngles = new Dictionary<string, double>();
        private Dictionary<string, PointShape> labelPositions = new Dictionary<string, PointShape>();
        //IList<IDomainCommand> commandlist = new List<IDomainCommand>();
        private CropzoneCreationViewModel selectedCropZone;
        private ObservableCollection<CropzoneCreationViewModel> selectedCropZones = new ObservableCollection<CropzoneCreationViewModel>();
        private CropzoneCreationViewModel mergeCropZone;
        private ObservableCollection<CropzoneCreationViewModel> mergeCropZones = new ObservableCollection<CropzoneCreationViewModel>();
        private MultipointShape boundarypoints = new MultipointShape();
        //private ObservableCollection<PointShape> boundarypoints = new ObservableCollection<PointShape>();
        UserSettings settings;
        string areaUnitString = string.Empty;
        string coordinateformat = string.Empty;
        AgC.UnitConversion.Area.AreaUnit agcAreaUnit;
        ThinkGeo.MapSuite.Core.AreaUnit mapAreaUnit;
        ThinkGeo.MapSuite.Core.DistanceUnit mapDistanceUnit;
        int fieldopacity = 125;
        int annotationopacity = 125;
        MapSettings mapsettings;
        ZoomLevelFactory zoomlevelfactory = new ZoomLevelFactory();
        double cuttingwidth = 0;
        BaseShape mastershape9;
        string mastershapewkt9;
        string projectedmastershapewkt9;
        double cuttingwidthconstant9;
        double cuttingwidth9 = 0;

        double radius = 500;
        double area = 0;
        double desiredarea = 100;
        double oldarea = 0;
        double scalepercent = 0;
        double pivotpoints = 36;
        double pointx = 0;
        double pointy = 0;
        double multiplierconstant = .99731255;
        double fieldareatest = 0;
        double radius2 = 500;
        double point2x = 0;
        double point2y = 0;
        double angle1 = 0;
        double angle2 = 0;


        public CreateCropzoneViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher)
        {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;

            settings = clientEndpoint.GetUserSettings();
            sortAlphabetically = settings.AreFieldsSortedAlphabetically;
            InitializeSettings();
            scalingTextStyle = new ScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, mapsettings.ScalingTextMaximumSize, this.mapsettings.ScalingTextMinimumSize);
            customDistancePolygonStyleText = new MapStyles.CustomDistancePolygonStyle(mapAreaUnit, mapDistanceUnit, new GeoFont("Arial", 12, DrawingFontStyles.Regular), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.White), new GeoSolidBrush(GeoColor.StandardColors.Red), new GeoPen(GeoColor.StandardColors.Red));
            customDistanceLineStyleText = new MapStyles.CustomDistanceLineStyle(mapDistanceUnit, new GeoFont("Arial", 12, DrawingFontStyles.Regular), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.DarkRed, 2), new GeoPen(GeoColor.StandardColors.DarkBlue, 2));
            customDistanceLineStyleText.DisplayAngles = false;
            customDistanceLineStyleText2 = new MapStyles.CustomDistanceLineStyle(mapDistanceUnit, new GeoFont("Arial", 12, DrawingFontStyles.Regular), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.DarkRed, 2), new GeoPen(GeoColor.StandardColors.DarkBlue, 2));
            customDistanceLineStyleText2.DisplayAngles = false;
            customDistancePolygonStyleText.YOffsetInPixel = -20;
            customDistanceLineStyleText9 = new MapStyles.CustomDistanceLineStyle(mapDistanceUnit, new GeoFont("Arial", 12, DrawingFontStyles.Regular), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.DarkRed, 2), new GeoPen(GeoColor.StandardColors.DarkBlue, 2));
            customDistanceLineStyleText9.DisplayAngles = false;
            customDistanceLineStyleText9.DisplayTotalLength = false;

            operatingmode = 0;
            selectedFeatures = new Collection<Feature>();
            selectedFeature = new Feature();
            selectedPivot = new Feature();
            InitializeMap();
            InitializeViewModels();
            InitializeCommands();
        }

        private void InitializeSettings() {
            this.mapsettings = this.clientEndpoint.GetMapSettings();
            fieldopacity = mapsettings.FieldsOpacity;
            annotationopacity = mapsettings.MapAnnotationOpacity;
            areaUnitString = mapsettings.MapAreaUnit;
            coordinateformat = mapsettings.DisplayCoordinateFormat;
            agcAreaUnit = UnitFactory.GetUnitByName(areaUnitString) as AgC.UnitConversion.Area.AreaUnit;
            mapAreaUnit = MapUnitFactory.AreaMeasureConversion(agcAreaUnit);
            mapDistanceUnit = MapUnitFactory.AreaDistanceMeasureConversion(agcAreaUnit);
            if (this.mapsettings.BorderRowWidth.HasValue && this.mapsettings.BorderRowWidth.Value > 0)
            {
                BufferWidth = this.mapsettings.BorderRowWidth;
            } else
            {
                BufferWidth = 30;
            }
        }

        private void InitializeViewModels()
        {
            mergeCropZone = new CropzoneCreationViewModel(null, "", "", "", "", null);
            mergeCropZones = new ObservableCollection<CropzoneCreationViewModel>();
            selectedCropZone = new CropzoneCreationViewModel(null, "", "", "", "", null);
            selectedCropZones = new ObservableCollection<CropzoneCreationViewModel>();
            createItemViewModel = new CreateTreeItemMapCZAssignerViewModel(this.clientEndpoint, OnNewItemCreated, OnNewItemIgnored) { CurrentCropYear = ApplicationEnvironment.CurrentCropYear, CurrentDataSourceId = ApplicationEnvironment.CurrentDataSourceId };
        }

        private void InitializeCommands()
        {

            SelectMapShapesCommand = new RelayCommand(SelectMapShapes);
            MergeMapShapesCommand = new RelayCommand(MergeMapShapes);
            SliceLineCommand = new RelayCommand(SliceLine);
            ScissorsCommand = new RelayCommand(Scissors);
            SlicePolygonCommand = new RelayCommand(SlicePolygon);
            AssignShapeCommand = new RelayCommand(AssignShape);
            DeleteShapeCommand = new RelayCommand(DeleteShape);
            CompleteMergeCommand = new RelayCommand(MergeShape);
            ConfigureCommand = new RelayCommand<CropzoneCreationViewModel>(SelectThisItem);
            MergeShapesCommand = new RelayCommand<CropzoneCreationViewModel>(MergeThisItem);
            CancelCommand = new RelayCommand(Cancel);
            FinishedCommand = new RelayCommand(Finished);
            MeasureCommand = new RelayCommand(HandleMeasureLine);
            //FinishedCommand = new RelayCommand(Finished);
            ResetCommand = new RelayCommand(ResetVertical);
            BuildCommand = new RelayCommand(BuildLines);
            BuildOneCommand = new RelayCommand(BuildOneLine);
            CreateBorderRowsCommand = new RelayCommand(CreateBorderRows);
            //CreateBorderRowsCommand = new RelayCommand(DrawBorderRows);
            DrawBorderRowsCommand = new RelayCommand(DrawBorderRows);
            DrawPivotCenterCommand = new RelayCommand(DrawPivotCenter);
            DrawPartialPivotCenterCommand = new RelayCommand(DrawPartialPivotCenter);
            DrawPartialPivotCommand = new RelayCommand(Drawpartialpivot);
            ShowMeasurementsCommand = new RelayCommand(ShowMeasurements);
        }

        public ICommand SelectMapShapesCommand { get; private set; }
        public ICommand MergeMapShapesCommand { get; private set; }
        public ICommand SliceLineCommand { get; private set; }
        public ICommand ScissorsCommand { get; private set; }
        public ICommand SlicePolygonCommand { get; private set; }
        public ICommand AssignShapeCommand { get; private set; }
        public ICommand DeleteShapeCommand { get; private set; }
        public ICommand CompleteMergeCommand { get; private set; }
        public ICommand ConfigureCommand { get; private set; }
        public ICommand MergeShapesCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }
        public ICommand FinishedCommand { get; private set; }
        public ICommand MeasureCommand { get; private set; }
        public ICommand ResetCommand { get; private set; }
        public ICommand BuildCommand { get; private set; }
        public ICommand BuildOneCommand { get; private set; }
        public ICommand CreateBorderRowsCommand { get; private set; }
        public ICommand DrawBorderRowsCommand { get; private set; }
        public ICommand DrawPivotCenterCommand { get; private set; }
        public ICommand DrawPartialPivotCenterCommand { get; private set; }
        public ICommand DrawPartialPivotCommand { get; private set; }
        public ICommand ShowMeasurementsCommand { get; private set; }
        public ObservableCollection<CropZoneTreeItemViewModel> CropZoneModels { get; private set; }
        public FieldTreeItemViewModel FieldTreeItemVM { get; private set; }

        public WpfMap Map { get; set; }

        public Collection<Feature> SpatialQueryResults {
            get { return spatialQueryResults; }
            set {
                spatialQueryResults = value;
                RaisePropertyChanged("SpatialQueryResults");
            }
        }

        public Feature SelectedField
        {
            get { return selectedfield; }
            set
            {
                if (selectedfield == value) { return; }
                selectedfield = value;
                fieldareatest = 0;
                if (selectedfield == null) {
                    Cancel();
                    return;
                }
                RaisePropertyChanged("SelectedField");
                log.Info("SplitDisplay - Selected field");
                ReSetMap();
                if (selectedfield.Id != loadedfeature.Id)
                {
                    loadedfeature = selectedfield;
                    try {
                        selectedFeatures.Add(selectedfield);
                        selectedFeature = selectedfield;
                        fieldshape = selectedfield.GetShape();
                        if (fieldshape is MultipolygonShape) {
                            fieldareatest = ((MultipolygonShape)fieldshape).GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
                            if (fieldareatest <= 0) {
                                Cancel();
                                return;
                            }
                        }

                    }
                    catch {
                        Cancel();
                        return;
                    }
                    if (Map.EditOverlay.EditShapesLayer.InternalFeatures.Count > 0)
                    {
                        Map.EditOverlay.EditShapesLayer.InternalFeatures.Clear();
                    }
                    if (Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Count > 0)
                    {
                        Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Clear();
                    }
                    if (currentLayer.InternalFeatures.Count > 0)
                    {
                        currentLayer.InternalFeatures.Clear();
                    }
                    if (zoneLayer.InternalFeatures.Count > 0)
                    {
                        zoneLayer.InternalFeatures.Clear();
                    }
                    if (labelLayer.InternalFeatures.Count > 0)
                    {
                        labelLayer.InternalFeatures.Clear();
                    }
                    if (selectLayer.InternalFeatures.Count > 0)
                    {
                        selectLayer.InternalFeatures.Clear();
                    }
                    if (measureLayer.InternalFeatures.Count > 0)
                    {
                        measureLayer.InternalFeatures.Clear();
                    }
                    if (measureLayer2.InternalFeatures.Count > 0) {
                        measureLayer2.InternalFeatures.Clear();
                        popupOverlay.Popups.Clear();

                    }
                    if (measureLayer9.InternalFeatures.Count > 0) {
                        measureLayer9.InternalFeatures.Clear();

                    }
                    if (maploaded)
                    {
                        if (Map.Overlays.Contains("zoneOverlay"))
                        {
                            Map.Overlays.Remove("zoneOverlay");
                        }
                        if (Map.Overlays.Contains("selectOverlay"))
                        {
                            Map.Overlays.Remove("selectOverlay");
                        }
                        if (Map.Overlays.Contains("gridOverlay")) {
                            Map.Overlays.Remove("gridOverlay");
                        }
                        OpenAndLoadaShapefile();
                        log.Info("SplitDisplay - Map Loaded2.");

                    }
                }
                isAssigningMode = false;
                isMergeMode = false;
                operatingmode = 0;
                ChangeOperatingMode();
            }
        }

        public ObservableCollection<CropzoneCreationViewModel> SelectedCropZones
        {
            get { return selectedCropZones; }
            set
            {
                if (selectedCropZones == value) { return; }
                selectedCropZones = value;
                RaisePropertyChanged("SpatialQueryResults");
            }
        }

        public CropzoneCreationViewModel SelectedCropZone
        {
            get { return selectedCropZone; }
            set
            {
                if (selectedCropZone == value) { return; }
                selectedCropZone = value;
            }
        }

        public ObservableCollection<CropzoneCreationViewModel> MergeCropZones
        {
            get { return mergeCropZones; }
            set
            {
                if (mergeCropZones == value) { return; }
                mergeCropZones = value;
            }
        }

        public CropzoneCreationViewModel MergeCropZone
        {
            get { return mergeCropZone; }
            set
            {
                if (mergeCropZone == value) { return; }
                mergeCropZone = value;
                //selectLayer.InternalFeatures.Clear();
                //if (!string.IsNullOrEmpty(selectedCropZone.MapData))
                //{
                //    if (!selectOverlay.Layers.Contains("selectLayer"))
                //    {
                //        selectOverlay.Layers.Add("selectLayer", selectLayer);
                //    }
                //    selectLayer.InternalFeatures.Add(new Feature(mergeCropZone.MapData));
                //}
                //scalingTextStyle.MapScale = Map.CurrentScale;
                //Map.Refresh();
            }
        }

        public string CommandDescription {
            get { return commandDescription; }
            set {
                if (commandDescription == value) { return; }
                commandDescription = value;
                RaisePropertyChanged("CommandDescription");
            }
        }

        public string FieldName {
            get { return fieldname; }
            set {
                if (fieldname == value) { return; }
                fieldname = value;
                RaisePropertyChanged("FieldName");
            }
        }

        public object DetailsViewModel
        {
            get { return detailsViewModel; }
            set
            {
                detailsViewModel = value;
                LoadMapForItem(value);
                RaisePropertyChanged("DetailsViewModel");
            }
        }

        public bool HasUnsavedChanges
        {
            get { return hasunsavedchanges; }
            set
            {
                if (hasunsavedchanges == value) { return; }
                hasunsavedchanges = value;
                RaisePropertyChanged("HasUnsavedChanges");
            }
        }

        public bool ReadyToFinish
        {
            get { return readytofinish; }
            set
            {
                if (readytofinish == value) { return; }
                readytofinish = value;
                RaisePropertyChanged("ReadyToFinish");
            }
        }

        public bool ReadyToBuild {
            get { return readytobuild; }
            set {
                if (readytobuild == value) { return; }
                readytobuild = value;
                RaisePropertyChanged("ReadyToBuild");
            }
        }

        public bool IsAssigningMode
        {
            get { return isAssigningMode; }
            set
            {
                if (isAssigningMode == value) { return; }
                isAssigningMode = value;
                RaisePropertyChanged("IsAssigningMode");
                RaisePropertyChanged("IsDrawingMode");
                RaisePropertyChanged("IsMergeMode");
            }
        }

        public bool IsMergeMode
        {
            get { return isMergeMode; }
            set
            {
                if (isMergeMode == value) { return; }
                isMergeMode = value;
                RaisePropertyChanged("IsMergeMode");
            }
        }

        public bool IsDrawingMode
        {
            get { return (!isAssigningMode && readytoassign); }
        }
        public bool IsCreateCropzoneToolsPopupOpen {
            get { return isCreateCropzoneToolsPopupOpen; }
            set {
                isCreateCropzoneToolsPopupOpen = value;
                RaisePropertyChanged("IsCreateCropzoneToolsPopupOpen");
            }
        }

        public bool ShowMeasurementsCommands
        {
            get { return showMeasurementsCommands; }
            set
            {
                showMeasurementsCommands = value;
                RaisePropertyChanged("ShowMeasurementsCommands");
                RaisePropertyChanged("HideMeasurementsCommands");
            }
        }

        public bool HideMeasurementsCommands
        {
            get { return !showMeasurementsCommands; }
        }

        public bool ShowGrid {
            get { return showgrid; }
            set {
                if (showgrid == value) { return; }
                showgrid = value;
                RaisePropertyChanged("ShowGrid");
            }
        }

        public bool BuildAllLines {
            get { return buildalllines; }
            set {
                if (buildalllines == value) { return; }
                buildalllines = value;
                RaisePropertyChanged("BuildAllLines");
            }
        }

        public bool BuildOneLines
        {
            get { return buildonelines; }
            set
            {
                if (buildonelines == value) { return; }
                buildonelines = value;
                RaisePropertyChanged("BuildOneLines");
            }
        }

        public bool BuildBorderRows
        {
            get { return buildborderrows; }
            set
            {
                if (buildborderrows == value) { return; }
                buildborderrows = value;
                RaisePropertyChanged("BuildBorderRows");
            }
        }

        public bool BuildDrawBorderRows
        {
            get { return builddrawborderrows; }
            set
            {
                if (builddrawborderrows == value) { return; }
                builddrawborderrows = value;
                RaisePropertyChanged("BuildDrawBorderRows");
            }
        }

        public bool BuildOnePivot
        {
            get { return buildonepivot; }
            set
            {
                if (buildonepivot == value) { return; }
                buildonepivot = value;
                RaisePropertyChanged("BuildOnePivot");
            }
        }

        public bool DisplayBorderRow
        {
            get { return displayborderrow; }
            set
            {
                if (displayborderrow == value) { return; }
                displayborderrow = value;
                RaisePropertyChanged("DisplayBorderRow");
            }
        }

        public RectangleShape PreviousMapExtent
        {
            get { return previousmapextent; }
            set { previousmapextent = value; }
        }

        bool showBoundingBox = false;
        public bool ShowBoundingBox {
            get { return showBoundingBox; }
            set {
                if (showBoundingBox == value) { return; }
                showBoundingBox = value;
                //if (showBoundingBox) {
                //    DrawBBox();
                //}
                //else {
                //    ResetBBox();
                //}
                RaisePropertyChanged("ShowBoundingBox");
            }
        }

        double? cutWidth;
        public double? CutWidth {
            get { return cutWidth; }
            set {
                cutWidth = value;
                readytobuild = TestToBuild(cutWidth);
                RaisePropertyChanged("CutWidth");
            }
        }

        double? bufferWidth;
        public double? BufferWidth
        {
            get { return this.mapsettings.BorderRowWidth; }
            set
            {
                this.mapsettings.BorderRowWidth = value;
                bufferWidth = value;
                //readytobuild = TestToBuild(bufferWidth);
                RaisePropertyChanged("BufferWidth");
            }
        }

        public double Radius
        {
            get { return radius; }
            set
            {
                if (radius == value) { return; }
                radius = value;
                //if (buildonepivot)
                //{
                //    Drawpivot();
                //}
                if (Map.EditOverlay.EditShapesLayer.InternalFeatures.Count > 0)
                {
                    var ecount = Map.EditOverlay.EditShapesLayer.InternalFeatures.Count - 1;
                    Map.EditOverlay.EditShapesLayer.InternalFeatures.RemoveAt(ecount);
                    Drawpivot();
                }

                RaisePropertyChanged("Radius");
                RaisePropertyChanged("Area");
            }
        }

        public double Area
        {
            get { return area; }
            set
            {
                if (area == value) { return; }
                area = value;
                //if (buildonepivot)
                //{
                //    DrawPivotFromAreaCalculations();
                //}
                if (Map.EditOverlay.EditShapesLayer.InternalFeatures.Count > 0)
                {
                    var ecount = Map.EditOverlay.EditShapesLayer.InternalFeatures.Count - 1;
                    Map.EditOverlay.EditShapesLayer.InternalFeatures.RemoveAt(ecount);
                    DrawPivotFromAreaCalculations();
                }

                RaisePropertyChanged("Area");
                RaisePropertyChanged("Radius");
            }
        }

        public double DesiredArea
        {
            get { return desiredarea; }
            set
            {
                if (desiredarea == value) { return; }
                desiredarea = value;
                RaisePropertyChanged("DesiredArea");
            }
        }

        public double OldArea
        {
            get { return oldarea; }
            set
            {
                if (oldarea == value) { return; }
                oldarea = value;
                RaisePropertyChanged("OldArea");
            }
        }

        public double ScalePercent
        {
            get { return scalepercent; }
            set
            {
                if (scalepercent == value) { return; }
                scalepercent = value;
                RaisePropertyChanged("ScalePercent");
            }
        }

        public double PivotPoints
        {
            get { return pivotpoints; }
            set
            {
                if (pivotpoints == value) { return; }
                pivotpoints = value;
                RaisePropertyChanged("PivotPoints");
            }
        }

        public double PointX
        {
            get { return pointx; }
            set
            {
                if (pointx == value) { return; }
                pointx = value;
                RaisePropertyChanged("PointX");
            }
        }

        public double PointY
        {
            get { return pointy; }
            set
            {
                if (pointy == value) { return; }
                pointy = value;
                RaisePropertyChanged("PointY");
            }
        }

        public double Radius2
        {
            get { return radius2; }
            set
            {
                if (radius2 == value) { return; }
                radius2 = value;
                //if (buildonepivot)
                //{
                //    Drawpivot();
                //}
                //if (Map.EditOverlay.EditShapesLayer.InternalFeatures.Count > 0)
                //{
                //    var ecount = Map.EditOverlay.EditShapesLayer.InternalFeatures.Count - 1;
                //    Map.EditOverlay.EditShapesLayer.InternalFeatures.RemoveAt(ecount);
                //    Drawpivot();
                //}

                RaisePropertyChanged("Radius2");
                //RaisePropertyChanged("Area");
            }
        }

        public double Point2X
        {
            get { return point2x; }
            set
            {
                if (point2x == value) { return; }
                point2x = value;
                RaisePropertyChanged("Point2X");
            }
        }

        public double Point2Y
        {
            get { return point2y; }
            set
            {
                if (point2y == value) { return; }
                point2y = value;
                RaisePropertyChanged("Point2Y");
            }
        }

        public double Angle1
        {
            get { return angle1; }
            set
            {
                if (angle1 == value) { return; }
                angle1 = value;
                RaisePropertyChanged("Angle1");
            }
        }

        public double Angle2
        {
            get { return angle2; }
            set
            {
                if (angle2 == value) { return; }
                angle2 = value;
                RaisePropertyChanged("Angle2");
            }
        }

        private bool TestToBuild(double? cuttingwidth) {
            if (Map.EditOverlay.EditShapesLayer.InternalFeatures.Count == 1 && cuttingwidth > 0 && Map.EditOverlay.EditShapesLayer.InternalFeatures[0].GetShape() is LineShape) {
                return true;
            }
            else {
                return false;
            }
        }

        private void ResetBBox() {
            ResetVertical();
            //ResetHorizontal();
            bboxLayer.InternalFeatures.Clear();
            Map.Refresh();
        }

        private void ResetVertical() {
            log.Info("SplitTools - Reset cutlines.");
            cutWidth = 0;
            if (Map.EditOverlay.EditShapesLayer.InternalFeatures.Count > 0) {
                Map.EditOverlay.EditShapesLayer.InternalFeatures.Clear();
            }
            if (zoneLayer.InternalFeatures.Count > 0) {
                zoneLayer.InternalFeatures.Clear();
            }
            if (selectLayer.InternalFeatures.Count > 0) {
                selectLayer.InternalFeatures.Clear();
            }
            if (measureLayer.InternalFeatures.Count > 0) {
                measureLayer.InternalFeatures.Clear();
            }
            if (measureLayer2.InternalFeatures.Count > 0) {
                measureLayer2.InternalFeatures.Clear();
            }
            if (measureLayer9.InternalFeatures.Count > 0) {
                measureLayer9.InternalFeatures.Clear();
            }
            Map.Refresh();
            //ablinesLayer.InternalFeatures.Clear();
            //drawlinesLayer.InternalFeatures.Clear();
            RaisePropertyChanged("CutWidth");
        }

        private void BuildLines() {
            log.Info("SplitTools - Build cutlines.");
            if(cutWidth == null || cutWidth == 0) {
                System.Windows.MessageBox.Show($"{Strings.CuttingDistanceIsNullOrZero_Text} {Strings.AfterTheValueHasBeenTypedPressTab_Text}");
                return;
            }
            if (Map.EditOverlay.EditShapesLayer.InternalFeatures.Count != 1) {
                System.Windows.MessageBox.Show(Strings.ThereMustBeOneCutLineToProperlyUseGridTool_Text);
                return;
            }
            LoadCutBufferShapes();
            GeoCollection<Feature> cutlines = LoadCutLineShapes();
            SplitPolgonsWithLines(cutlines);
            Map.EditOverlay.CanRotate = false;
            Map.EditOverlay.CanResize = false;
            Map.EditOverlay.CalculateAllControlPoints();
            scalingTextStyle.MapScale = Map.CurrentScale;
            Map.EditOverlay.Refresh();
            Map.Refresh();
            Map.TrackOverlay.TrackMode = TrackMode.None;
            Map.Cursor = Cursors.Hand;
            CommandDescription = Strings.EditCuttingShapes_Text;
            draggingfeatures = false;
            trackingfeatures = false;
            readytobuild = TestToBuild(cutWidth);
            Map.EditOverlay.CanAddVertex = true;
            Map.EditOverlay.CanDrag = true;
            Map.EditOverlay.CanRemoveVertex = true;

            var edfeatures22 = Map.EditOverlay.EditShapesLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);

        }

        private void BuildOneLine() {
            log.Info("SplitTools - Build cutlines.");
            if (cutWidth == null || cutWidth == 0) {
                System.Windows.MessageBox.Show($"{Strings.CuttingDistanceIsNullOrZero_Text} {Strings.AfterTheValueHasBeenTypedPressTab_Text}");
                return;
            }
            if (Map.EditOverlay.EditShapesLayer.InternalFeatures.Count == 0) {
                System.Windows.MessageBox.Show(Strings.ThereMustBeOneCutLineToProperlyUseGridTool_Text);
                return;
            }
            if (setupforoneline) {
                LoadMoreCutBufferShape();
            }
            else {
                LoadOneCutBufferShape();
            }
            GeoCollection<Feature> cutlines = LoadCutLineShapes();
            SplitPolgonsWithLines(cutlines);
            Map.EditOverlay.CanRotate = false;
            Map.EditOverlay.CanResize = false;
            Map.EditOverlay.CalculateAllControlPoints();
            scalingTextStyle.MapScale = Map.CurrentScale;
            Map.EditOverlay.Refresh();
            Map.Refresh();
            Map.TrackOverlay.TrackMode = TrackMode.None;
            Map.Cursor = Cursors.Hand;
            CommandDescription = Strings.EditCuttingShapes_Text;
            draggingfeatures = false;
            trackingfeatures = false;
            readytobuild = TestToBuild(cutWidth);
            Map.EditOverlay.CanAddVertex = true;
            Map.EditOverlay.CanDrag = true;
            Map.EditOverlay.CanRemoveVertex = true;

            var edfeatures22 = Map.EditOverlay.EditShapesLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);

        }

        private void CreateBorderRows()
        {
            log.Info("SplitTools - Build borderrows.");
            if (bufferWidth == null || bufferWidth == 0)
            {
                System.Windows.MessageBox.Show($"{Strings.BufferDistanceIsNullOrZero_Text} {Strings.AfterTheValueHasBeenTypedPressTab_Text}");
                return;
            }
            //if (Map.EditOverlay.EditShapesLayer.InternalFeatures.Count > 0)
            //{
            //    System.Windows.MessageBox.Show("There must not be any other cut lines to properly create border rows.");
            //    return;
            //}
            LoadCutBorderRowShape();
            GeoCollection<Feature> cutlines = LoadCutLineShapes();
            SplitPolgonsWithLines(cutlines);
            Map.EditOverlay.CanRotate = false;
            Map.EditOverlay.CanResize = false;
            Map.EditOverlay.CalculateAllControlPoints();
            scalingTextStyle.MapScale = Map.CurrentScale;
            Map.EditOverlay.Refresh();
            Map.Refresh();
            Map.TrackOverlay.TrackMode = TrackMode.None;
            Map.Cursor = Cursors.Hand;
            CommandDescription = Strings.EditCuttingShapes_Text;
            draggingfeatures = false;
            trackingfeatures = false;
            readytobuild = TestToBuild(bufferWidth);
            Map.EditOverlay.CanAddVertex = true;
            Map.EditOverlay.CanDrag = true;
            Map.EditOverlay.CanRemoveVertex = true;

            var edfeatures22 = Map.EditOverlay.EditShapesLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);

        }

        private void ShowMeasurements()
        {
            ShowMeasurementsCommands = !ShowMeasurementsCommands;
            RaisePropertyChanged("ShowMeasurementsCommands");
            RaisePropertyChanged("HideMeasurementsCommands");
        }

        private void DrawBorderRows()
        {
            //BuildAllLines = false;
            //BuildOneLines = false;
            BuildBorderRows = false;
            if (skipwhiledrawing) {
                Map.TrackOverlay.TrackMode = TrackMode.None;
                Map.Cursor = Cursors.Hand;
                CommandDescription = "'Edit Cutting Shapes/Pan-Zoom Map'";
                draggingfeatures = false;
                trackingfeatures = false;
                borderrowtool = false;
                drawingpivot = false;
                snipapolygon = false;
                drawingpartialpivot = false;
                readytobuild = TestToBuild(cutWidth);
                Map.EditOverlay.CanAddVertex = true;
                Map.EditOverlay.CanDrag = true;
                Map.EditOverlay.CanRemoveVertex = true;
                skipwhiledrawing = false;
                return;
            }
            log.Info("SplitTools - Draw borderrows.");
            if (bufferWidth == null || bufferWidth == 0)
            {
                System.Windows.MessageBox.Show($"{Strings.BufferDistanceIsNullOrZero_Text} {Strings.AfterTheValueHasBeenTypedPressTab_Text}");
                return;
            }
            //if (Map.EditOverlay.EditShapesLayer.InternalFeatures.Count > 0)
            //{
            //    System.Windows.MessageBox.Show("There must not be any other cut lines to properly create border rows.");
            //    return;
            //}

            Map.TrackOverlay.TrackMode = TrackMode.Line;
            Map.Cursor = Cursors.Pen;
            skipwhiledrawing = true;
            borderrowtool = true;
            CommandDescription = Strings.DrawBorderRow_Text;
            Map.EditOverlay.CanAddVertex = false;
            Map.EditOverlay.CanDrag = false;
            Map.EditOverlay.CanRemoveVertex = false;
            Map.EditOverlay.CanResize = false;
            Map.EditOverlay.CanRotate = false;
        }

        private void ResetHorizontal() {
            //horizontalCutLines = false;
            //offsetEast = 0;
            //offsetWest = 0;
            //equalHeight = false;
            //cutHeight = 0;
            //heightItems = 0;
            ablinesHorzLayer.InternalFeatures.Clear();
            drawlinesHorzLayer.InternalFeatures.Clear();
            RaisePropertyChanged("OffsetWest");
            RaisePropertyChanged("OffsetEast");
            RaisePropertyChanged("EqualHeight");
            RaisePropertyChanged("CutHeight");
            RaisePropertyChanged("HeightItems");
        }

        //private void DrawBBox() {
        //    if (currentLayer.InternalFeatures.Count == 0) { return; }
        //    bbox = new RectangleShape();
        //    bbox = currentLayer.GetBoundingBox();
        //    bboxLayer.InternalFeatures.Clear();
        //    bboxLayer.InternalFeatures.Add(new Feature(bbox));
        //    CutWidth = 0;
        //    //BBoxWidth = bbox.Width;
        //    //BBoxHeight = bbox.Height;
        //    Map.Refresh();
        //}

        //private void DrawABLineVert() {
        //    ablineVert = new LineShape();
        //    ablineVert.Vertices.Add(new Vertex(bbox.UpperLeftPoint));
        //    ablineVert.Vertices.Add(new Vertex(bbox.LowerLeftPoint));
        //    ablinesVertLayer.InternalFeatures.Clear();
        //    ablinesVertLayer.InternalFeatures.Add(new Feature(ablineVert));
        //}

        //private void DrawABLineHorz() {
        //    ablineHorz = new LineShape();
        //    ablineHorz.Vertices.Add(new Vertex(bbox.UpperLeftPoint));
        //    ablineHorz.Vertices.Add(new Vertex(bbox.UpperRightPoint));
        //    ablinesHorzLayer.InternalFeatures.Clear();
        //    ablinesHorzLayer.InternalFeatures.Add(new Feature(ablineHorz));
        //}

        //public IMapOverlay SelectedOverlay {
        //    get { return selectedoverlay; }
        //    set {
        //        if (selectedoverlay == value) { return; }
        //        if (selectedoverlay == null) { return; }
        //        if (string.IsNullOrWhiteSpace(value.Name)) { return; }
        //        selectedoverlay = value;
        //        //LoadBingMapsOverlay();
        //        //Map.Refresh();
        //        RaisePropertyChanged("SelectedOverlay");
        //    }
        //}

        public void SetFieldTreeItemVM(FieldTreeItemViewModel fieldtreeitemvm)
        {
            if (fieldtreeitemvm == null)
            {
                FieldTreeItemVM = null;
            }
            else
            {
                FieldTreeItemVM = fieldtreeitemvm;
            }
            setupforoneline = false;
            BuildAllLines = true;
            BuildOneLines = true;
            BuildBorderRows = true;
            BuildDrawBorderRows = true;
            BuildOnePivot = true;
            RaisePropertyChanged("FieldTreeItemVM");
        }

        private string displaycoordinates = string.Empty;
        public string DisplayCoordinates {
            get { return displaycoordinates; }
            set {
                displaycoordinates = value;
                RaisePropertyChanged("DisplayCoordinates");
            }
        }

        public void ReSetMap()
        {
            HasUnsavedChanges = false;
            isAssigningMode = false;
            isMergeMode = false;
            readytofinish = false;
            readytoassign = false;
            skipwhilemousedown = false;
            skipwhiledrawing = false;
            draggingfeatures = false;
            trackingfeatures = false;
            zoneLayer.InternalFeatures.Clear();
            labelLayer.InternalFeatures.Clear();
            selectLayer.InternalFeatures.Clear();
            measureLayer.InternalFeatures.Clear();
            measureLayer2.InternalFeatures.Clear();
            selectedFeatures.Clear();
            //measureLayer9.InternalFeatures.Clear();
            selectedPivot = new Feature();
            popupOverlay.Popups.Clear();
            SelectedCropZones.Clear();
            operatingmode = 0;
            boundarypoints = new MultipointShape();
        }

        void InitializeMap()
        {
            Map = new WpfMap();
            startedOutOnline = Landdb.Client.Infrastructure.NetworkStatus.IsInternetAvailableFast();
            Map.MapUnit = GeographyUnit.Meter;
            Map.MapTools.PanZoomBar.Visibility = System.Windows.Visibility.Visible;
            Map.MapTools.PanZoomBar.GlobeButtonVisibility = System.Windows.Visibility.Hidden;
            Map.MapTools.PanZoomBar.VerticalAlignment = System.Windows.VerticalAlignment.Center;
            Map.MapTools.PanZoomBar.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            Map.Background = System.Windows.Media.Brushes.White;
            Map.Cursor = Cursors.Arrow;
            zoomlevelfactory = new ZoomLevelFactory(mapsettings.ZoomLevel);
            Map.ZoomLevelSet = zoomlevelfactory.ZoomLevelSET;
            Map.MouseLeftButtonDown += (sender, e) =>
            {
                ((MainWindow)System.Windows.Application.Current.MainWindow).Draggable = false;
            };

            projection = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetGoogleMapParametersString());
            scalingTextStyle.RotationAngles = rotationAngles;
            scalingTextStyle.LabelPositions = labelPositions;
            setupforoneline = false;
            HasUnsavedChanges = false;
            isAssigningMode = false;
            isMergeMode = false;
            if (Map.Overlays.Contains("zoneOverlay"))
            {
                Map.Overlays.Remove("zoneOverlay");
            }
            if (Map.Overlays.Contains("selectOverlay"))
            {
                Map.Overlays.Remove("selectOverlay");
            }
            if (Map.Overlays.Contains("gridOverlay")) {
                Map.Overlays.Remove("gridOverlay");
            }

            Map.Loaded += (sender, e) =>
            {
                if (maploaded) { return; }
                LoadBingMapsOverlay();
                OpenAndLoadaShapefile();
                if (!displayborderrow)
                {
                    SliceLine();
                } else
                {
                    Map.TrackOverlay.TrackMode = TrackMode.None;
                    Map.Cursor = Cursors.Hand;
                    CommandDescription = Strings.EditCuttingShapes_Text;
                    draggingfeatures = false;
                    trackingfeatures = false;
                    deletetool = false;
                    borderrowtool = false;
                    snipapolygon = false;
                    drawingpivot = false;
                    drawingpartialpivot = false;
                    skipwhiledrawing = false;
                }
                Map.Overlays.Add("popupOverlay", popupOverlay);
                maploaded = true;
                log.Info("SplitDisplay - Map Loaded.");

            };

            Map.MouseUp += (sender, e) =>
            {
                if (measuretool) { return; }
                if (borderrowtool) { return; }
                skipwhilemousedown = false;
                if (draggingfeatures) {
                    draggingfeatures = false;
                    try {
                        GeoCollection<Feature> cutlines = LoadCutLineShapes();
                        IList<Feature> cz = SplitPolgonsWithLines(cutlines);
                        Map.Refresh();
                    }
                    catch (Exception ex) {
                        int z = 0;
                    }
                }
            };

            Map.OverlaysDrawing += (sender, e) =>
            {
                //skipwhiledrawing = true;
            };

            Map.OverlaysDrawn += (sender, e) =>
            {
                //skipwhiledrawing = false;
            };

            Map.CurrentScaleChanged += (sender, e) => {
                scalingTextStyle.MapScale = e.CurrentScale;
            };

            Map.EditOverlay.FeatureDragging += (sender, e) => {
                draggingfeatures = true;
            };

            Map.EditOverlay.VertexMoving += (sender, e) => {
                if (isAssigningMode) { return; }
                if (measuretool) { return; }
                if (borderrowtool) { return; }
                draggingfeatures = true;
            };


            Map.EditOverlay.VertexRemoved += (sender, e) => {
                if (isAssigningMode) { return; }
                if (measuretool) { return; }
                if (borderrowtool) { return; }
                removingvertex = true;
            };

            Map.MouseMove += (sender, e) => {
                PointShape worldlocation2 = Map.ToWorldCoordinate(e.GetPosition(Map));
                PointShape centerpoint = projection.ConvertToInternalProjection(worldlocation2) as PointShape;
                Vertex geoVertex1 = new Vertex(centerpoint.X, centerpoint.Y);
                string _DecimalDegreeString = Math.Round(geoVertex1.Y, 5) + ", " + Math.Round(geoVertex1.X, 5);
                string displaycoordinates1 = DecimalDegrees.ddstring_To_DMs(_DecimalDegreeString, mapsettings.DisplayCoordinateFormat);
                DisplayCoordinates = displaycoordinates1;
                if (measuretool) { return; }
                if (borderrowtool) { return; }
                if (removingvertex) {
                    removingvertex = false;
                    try {
                        GeoCollection<Feature> cutlines = LoadCutLineShapes();
                        IList<Feature> cz = SplitPolgonsWithLines(cutlines);
                        Map.Refresh();
                    }
                    catch (Exception ex) {
                        int z = 0;
                    }
                }
            };

            Map.TrackOverlay.TrackStarted += (sender, e) => {
                //if (isAssigningMode) { return; }
                if (measuretool) { return; }
                trackingfeatures = true;
            };

            Map.TrackOverlay.TrackEnded += (sender, e) => {
                //if (isAssigningMode) { return; }
                if (measuretool) { return; }
                try {
                    BaseShape bs = e.TrackShape;

                    if (Map.TrackOverlay.TrackMode == TrackMode.Point) {
                        if (assigntool)
                        {
                            foreach (CropzoneCreationViewModel ccvm in selectedCropZones)
                            {
                                MultipolygonShape ps = new MultipolygonShape(ccvm.MapData);
                                PointShape ps1 = bs as PointShape;
                                PointShape ps2 = projection.ConvertToInternalProjection(ps1) as PointShape;
                                if (ps.Contains(ps2))
                                {
                                    SelectThisItem(ccvm);
                                    break;
                                }
                            }
                            Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Clear();
                            return;
                        }
                        if (mergetool)
                        {
                            foreach (CropzoneCreationViewModel ccvm in mergeCropZones)
                            {
                                MultipolygonShape ps = new MultipolygonShape(ccvm.MapData);
                                PointShape ps1 = bs as PointShape;
                                PointShape ps2 = projection.ConvertToInternalProjection(ps1) as PointShape;
                                if (ps.Contains(ps2))
                                {
                                    //MergeCropZones
                                    MergeThisItem(ccvm);
                                    break;
                                }
                            }
                            Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Clear();
                            return;
                        }
                        if (deletetool)
                        {
                            Collection<Feature> selectedFeatures = Map.EditOverlay.EditShapesLayer.QueryTools.GetFeaturesWithinDistanceOf(bs, GeographyUnit.Meter, mapDistanceUnit, 10, ReturningColumnsType.AllColumns);
                            if (selectedFeatures.Count > 0)
                            {
                                Map.EditOverlay.EditShapesLayer.InternalFeatures.Remove(selectedFeatures[0]);
                            }
                            //Map.EditOverlay.CanRotate = false;
                            //Map.EditOverlay.CanResize = false;
                            //Map.EditOverlay.CanAddVertex = true;
                            Map.EditOverlay.CanDrag = true;
                            Map.EditOverlay.CanRemoveVertex = true;
                            //Map.EditOverlay.CalculateAllControlPoints();
                            ////Map.EditOverlay.Refresh();
                            //Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Clear();
                            ////Map.TrackOverlay.Refresh();
                            //Map.Refresh();
                            //Map.TrackOverlay.TrackMode = TrackMode.None;
                            //Map.Cursor = Cursors.Hand;
                            //CommandDescription = "Edit Cutting Shapes";
                            //draggingfeatures = false;
                            //trackingfeatures = false;
                            //borderrowtool = false;
                            //drawingpivot = false;
                            //skipwhiledrawing = false;
                            //readytobuild = TestToBuild(cutWidth);
                            //return;
                        }
                        if (drawingpivot)
                        {
                            if (bs is PointShape)
                            {
                                try
                                {
                                    PointShape ps1 = bs as PointShape;
                                    PointShape ps2 = projection.ConvertToInternalProjection(ps1) as PointShape;
                                    pointx = ps2.X;
                                    pointy = ps2.Y;
                                    Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Clear();
                                    Map.TrackOverlay.Refresh();
                                    thiswasfromtracking = true;
                                    Drawpivot();
                                    return;
                                }
                                catch (Exception ex)
                                {
                                    log.Error("Pivottool - Error pointshape trackended - {0}", ex);
                                    return;
                                }
                            }
                        }
                        if (drawingpartialpivot)
                        {
                            if (bs is PointShape)
                            {
                                try
                                {
                                    PointShape ps1 = bs as PointShape;
                                    PointShape ps2 = projection.ConvertToInternalProjection(ps1) as PointShape;
                                    point2x = ps2.X;
                                    point2y = ps2.Y;
                                    Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Clear();
                                    Map.TrackOverlay.Refresh();
                                    thiswasfromtracking = true;
                                    //Drawpartialpivot();
                                    return;
                                }
                                catch (Exception ex)
                                {
                                    log.Error("Pivottool - Error pointshape trackended - {0}", ex);
                                    return;
                                }
                            }
                        }

                    }

                    if (borderrowtool)
                    {
                        if (Map.TrackOverlay.TrackMode == TrackMode.Line)
                        {
                            if(bs is LineShape)
                            {
                                LineShape drawnline = bs as LineShape;

                                if (drawnline.Vertices.Count > 2)
                                {
                                    try
                                    {
                                        MultipolygonShape mps = new MultipolygonShape();
                                        BaseShape baseshp = selectedFeature.GetShape();
                                        //try
                                        //{
                                        //    BaseShape baseshp2 = selectedPivot.GetShape();
                                        //    BaseShape baseshp3 = projection.ConvertToInternalProjection(baseshp2);
                                        //    //BaseShape baseshp3 = projection.ConvertToExternalProjection(baseshp2);
                                        //    baseshp = baseshp3;
                                        //}
                                        //catch (Exception ex)
                                        //{
                                        //    int iiii = 0;
                                        //}
                                        if (baseshp is PolygonShape)
                                        {
                                            mps.Polygons.Add(baseshp as PolygonShape);
                                        }
                                        if (baseshp is MultipolygonShape)
                                        {
                                            mps = baseshp as MultipolygonShape;
                                        }
                                        PointShape targetpoint1 = projection.ConvertToInternalProjection(new PointShape(drawnline.Vertices[0])) as PointShape;
                                        PointShape targetpoint2 = projection.ConvertToInternalProjection(new PointShape(drawnline.Vertices[1])) as PointShape;
                                        PointShape targetpoint3 = projection.ConvertToInternalProjection(new PointShape(drawnline.Vertices[drawnline.Vertices.Count - 1])) as PointShape;

                                        MultipolygonShape multipoly = CalculateBorderRows(mps, bufferWidth.Value, targetpoint1, targetpoint2, targetpoint3);

                                        MultipolygonShape newpoly = projection.ConvertToExternalProjection(multipoly) as MultipolygonShape;
                                        bs = newpoly as BaseShape;
                                    } catch (Exception ex4) {
                                        int iiiii = 0;
                                    }
                                }
                            }
                        }
                    }
                    if (snipapolygon)
                    {
                        if (Map.TrackOverlay.TrackMode == TrackMode.Line)
                        {
                            if (bs is LineShape)
                            {
                                LineShape drawnline = bs as LineShape;

                                if (drawnline.Vertices.Count > 1)
                                {
                                    try
                                    {
                                        BaseShape newpoly = bs.Buffer(.01, Map.MapUnit, MapUnitFactory.AreaDistanceMeasureConversion(Map.MapUnit));
                                        bs = newpoly as BaseShape;
                                        //MultipolygonShape newmultipoly2 = BufferShape(borderrowwidthx2, segmentMultiLineShape012) as MultipolygonShape;

                                        //MultipolygonShape mps = new MultipolygonShape();
                                        //BaseShape baseshp = selectedFeature.GetShape();
                                        //if (baseshp is PolygonShape)
                                        //{
                                        //    mps.Polygons.Add(baseshp as PolygonShape);
                                        //}
                                        //if (baseshp is MultipolygonShape)
                                        //{
                                        //    mps = baseshp as MultipolygonShape;
                                        //}
                                        //PointShape targetpoint1 = projection.ConvertToInternalProjection(new PointShape(drawnline.Vertices[0])) as PointShape;
                                        //PointShape targetpoint2 = projection.ConvertToInternalProjection(new PointShape(drawnline.Vertices[1])) as PointShape;
                                        //PointShape targetpoint3 = projection.ConvertToInternalProjection(new PointShape(drawnline.Vertices[drawnline.Vertices.Count - 1])) as PointShape;

                                        //MultipolygonShape multipoly = CalculateBorderRows(mps, bufferWidth.Value, targetpoint1, targetpoint2, targetpoint3);

                                        //MultipolygonShape newpoly = projection.ConvertToExternalProjection(multipoly) as MultipolygonShape;
                                        //bs = newpoly as BaseShape;
                                    }
                                    catch (Exception ex4)
                                    {
                                        int iiiii = 0;
                                    }
                                }
                            }
                        }
                    }

                    //if (mergetool)
                    //{
                    //    mergetool = false;
                    //    if (Map.TrackOverlay.TrackMode == TrackMode.Line)
                    //    {
                    //        if (bs is LineShape)
                    //        {
                    //            LineShape drawnline = bs as LineShape;

                    //            if (drawnline.Vertices.Count > 2)
                    //            {
                    //                //MultipolygonShape mps = new MultipolygonShape();
                    //                //BaseShape baseshp = selectedFeature.GetShape();
                    //                //if (baseshp is PolygonShape)
                    //                //{
                    //                //    mps.Polygons.Add(baseshp as PolygonShape);
                    //                //}
                    //                //if (baseshp is MultipolygonShape)
                    //                //{
                    //                //    mps = baseshp as MultipolygonShape;
                    //                //}
                    //                //PointShape targetpoint1 = projection.ConvertToInternalProjection(new PointShape(drawnline.Vertices[0])) as PointShape;
                    //                //PointShape targetpoint2 = projection.ConvertToInternalProjection(new PointShape(drawnline.Vertices[1])) as PointShape;
                    //                //PointShape targetpoint3 = projection.ConvertToInternalProjection(new PointShape(drawnline.Vertices[drawnline.Vertices.Count - 1])) as PointShape;

                    //                //MultipolygonShape multipoly = CalculateBorderRows(mps, bufferWidth.Value, targetpoint1, targetpoint2, targetpoint3);

                    //                //MultipolygonShape newpoly = projection.ConvertToExternalProjection(multipoly) as MultipolygonShape;
                    //                //bs = newpoly as BaseShape;
                    //            }
                    //        }
                    //    }
                    //    return;
                    //}

                    if (isAssigningMode)
                    {
                        Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Clear();
                        //Map.Refresh();
                        return;
                    }
                    if (!deletetool)
                    {
                        Map.EditOverlay.EditShapesLayer.InternalFeatures.Add(new Feature(bs));
                    }

                    GeoCollection<Feature> cutlines = LoadCutLineShapes();
                    SplitPolgonsWithLines(cutlines);
                    Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Clear();
                    Map.EditOverlay.CanRotate = false;
                    Map.EditOverlay.CanResize = false;
                    Map.EditOverlay.CalculateAllControlPoints();
                    scalingTextStyle.MapScale = Map.CurrentScale;
                    Map.EditOverlay.Refresh();
                    Map.Refresh();
                    Map.TrackOverlay.TrackMode = TrackMode.None;
                    Map.Cursor = Cursors.Hand;
                    CommandDescription = Strings.EditCuttingShapes_Text;
                    draggingfeatures = false;
                    trackingfeatures = false;
                    deletetool = false;
                    borderrowtool = false;
                    snipapolygon = false;
                    drawingpivot = false;
                    drawingpartialpivot = false;
                    skipwhiledrawing = false;
                    readytobuild = TestToBuild(cutWidth);
                    Map.EditOverlay.CanAddVertex = true;
                    Map.EditOverlay.CanDrag = true;
                    Map.EditOverlay.CanRemoveVertex = true;

                }
                catch (Exception ex2) {
                    int ddd = 0;
                }
            };

        }

        private void LoadBingMapsOverlay()
        {
            log.Info("SplitDisplay - Load Imagery.");
            if (!startedOutOnline) { return; }
            IMapOverlay mapoverlay = new BingMapOverlay();
            Overlay bingOverlay = mapoverlay.Overlay;
            if (bingOverlay == null) { return; }
            if (Map.Overlays.Contains("Bing")) {
                Map.Overlays.Remove("Bing");
            }
            if (Map.Overlays.Contains("Bing Online")) {
                Map.Overlays.Remove("Bing Online");
            }
            if (Map.Overlays.Contains("Roads Only")) {
                Map.Overlays.Remove("Roads Only");
            }
            if (Map.Overlays.Contains("Road Online")) {
                Map.Overlays.Remove("Road Online");
            }
            if (Map.Overlays.Contains("None")) {
                Map.Overlays.Remove("None");
            }
            Map.Overlays.Add(mapoverlay.Name, bingOverlay);
            Map.Overlays.MoveToBottom(mapoverlay.Name);
        }

        private void OpenAndLoadaShapefile()
        {
            log.Info("SplitDisplay - Open and load shapefiles.");
            CreateInMemoryLayerAndOverlay();
            log.Info("SplitDisplay - Open zone layer.");
            CreateZoneLayerAndOverlay();
            log.Info("SplitDisplay - Open grid layer.");
            CreateGridLayerAndOverlay();
            log.Info("SplitDisplay - Set current extent.");
            LoadShapeFileOverlayOntoMap();
            scalingTextStyle.MapScale = Map.CurrentScale;
            if (Map.Overlays.Contains("importOverlay"))
            {
                if (((LayerOverlay)Map.Overlays["importOverlay"]).Layers.Contains("ImportLayer"))
                {
                    Map.Refresh();
                }
            }
            if (Map.Overlays.Contains("zoneOverlay"))
            {
                if (((LayerOverlay)Map.Overlays["zoneOverlay"]).Layers.Contains("ZoneLayer"))
                {
                    Map.Refresh();
                }
            }
            if (Map.Overlays.Contains("selectOverlay"))
            {
                if (((LayerOverlay)Map.Overlays["selectOverlay"]).Layers.Contains("selectLayer"))
                {
                    Map.Refresh();
                }
            }
            if (Map.Overlays.Contains("grideOverlay")) {
                if (((LayerOverlay)Map.Overlays["gridOverlay"]).Layers.Contains("GridLayer")) {
                    Map.Refresh();
                }
            }
        }

        private void CreateInMemoryLayerAndOverlay()
        {
            layerOverlay = new LayerOverlay();
            currentLayer = new InMemoryFeatureLayer();
            AreaStyle currentLayerStyle = AreaStyles.CreateSimpleAreaStyle(new GeoColor(90, GeoColor.SimpleColors.PastelYellow), new GeoColor(100, GeoColor.StandardColors.Yellow), 2);
            currentLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = currentLayerStyle;
            currentLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            projection = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetGoogleMapParametersString());
            currentLayer.FeatureSource.Projection = projection;
            currentLayer.InternalFeatures.Add(SelectedField);
            layerOverlay.Layers.Add("ImportLayer", currentLayer);
            layerOverlay.DrawingExceptionMode = DrawingExceptionMode.DrawException;

            boundarypoints = new MultipointShape();
            if (currentLayer.InternalFeatures.Count > 0)
            {
                BaseShape bs = currentLayer.InternalFeatures[0].GetShape();
                MultipolygonShape mps2 = new MultipolygonShape();
                if (bs is MultipolygonShape)
                {
                    mps2 = bs as MultipolygonShape;
                }
                else if (bs is PolygonShape)
                {
                    mps2.Polygons.Add(bs as PolygonShape);
                }
                foreach (PolygonShape poly in mps2.Polygons)
                {
                    foreach (Vertex vert in poly.OuterRing.Vertices)
                    {
                        boundarypoints.Points.Add(new PointShape(vert));
                    }
                }
            }

            spatialQueryResultLayer = new InMemoryFeatureLayer();
            AreaStyle spatialQueryResultLayerStyle = new AreaStyle(new GeoPen(GeoColor.StandardColors.Black, 1), new GeoSolidBrush(new GeoColor(0, GeoColor.StandardColors.Transparent)));
            spatialQueryResultLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = spatialQueryResultLayerStyle;
            spatialQueryResultLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            Projection projectionx = new Proj4Projection(Proj4Projection.GetGoogleMapParametersString(), Proj4Projection.GetGoogleMapParametersString());
            spatialQueryResultLayer.FeatureSource.Projection = projectionx;
            foreach (Feature featureitem in spatialQueryResults) {
                spatialQueryResultLayer.InternalFeatures.Add(featureitem);
            }
            layerOverlay.Layers.Add("spatialQueryResultLayer", spatialQueryResultLayer);

            AreaStyle measureLayerStyle = AreaStyles.CreateSimpleAreaStyle(new GeoColor(50, GeoColor.StandardColors.Orange), new GeoColor(200, GeoColor.StandardColors.Orange), 2);
            measureLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(measureLayerStyle);
            measureLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customDistanceLineStyleText);
            measureLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(lineStylePen);
            measureLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            measureLayer.FeatureSource.Projection = projection;
            layerOverlay.Layers.Add("measureLayer", measureLayer);

            AreaStyle measureLayerStyle2 = AreaStyles.CreateSimpleAreaStyle(new GeoColor(50, GeoColor.StandardColors.Orange), new GeoColor(200, GeoColor.StandardColors.Orange), 2);
            measureLayer2.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(measureLayerStyle2);
            measureLayer2.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customDistanceLineStyleText2);
            measureLayer2.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(lineStylePen2);
            measureLayer2.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            measureLayer2.FeatureSource.Projection = projection;
            layerOverlay.Layers.Add("measureLayer2", measureLayer2);

            measureLayer9.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customDistanceLineStyleText9);
            measureLayer9.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            measureLayer9.FeatureSource.Projection = projection;
            layerOverlay.Layers.Add("measureLayer9", measureLayer9);

            popupOverlay = new PopupOverlay();
            popupOverlay.DrawingExceptionMode = DrawingExceptionMode.DrawException;
        }

        private void CreateZoneLayerAndOverlay()
        {
            selectOverlay = new LayerOverlay();
            selectLayer = new InMemoryFeatureLayer();
            selectLayer.FeatureSource.Open();
            selectLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = AreaStyles.CreateSimpleAreaStyle(GeoColor.FromArgb(80, GeoColor.SimpleColors.Red), GeoColor.FromArgb(100, GeoColor.SimpleColors.Red));
            selectLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = LineStyles.CreateSimpleLineStyle(GeoColor.FromArgb(100, GeoColor.SimpleColors.Red), 2, true);
            selectLayer.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = PointStyles.CreateSimplePointStyle(PointSymbolType.Circle, GeoColor.FromArgb(100, GeoColor.SimpleColors.Red), 8);
            selectLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            selectLayer.FeatureSource.Projection = projection;
            selectLayer.FeatureSource.Close();
            selectOverlay.Layers.Add("selectLayer", selectLayer);
            selectOverlay.DrawingExceptionMode = DrawingExceptionMode.DrawException;

            zoneOverlay = new LayerOverlay();
            zoneLayer = new InMemoryFeatureLayer();
            zoneLayer.FeatureSource.Open();
            FeatureSourceColumn column1 = new FeatureSourceColumn(labelcolumn, "string", 64);
            FeatureSourceColumn column2 = new FeatureSourceColumn(namecolumn, "string", 64);
            FeatureSourceColumn column3 = new FeatureSourceColumn(idcolumn, "string", 256);
            FeatureSourceColumn column4 = new FeatureSourceColumn(areacolumn, "string", 16);
            zoneLayer.Columns.Add(column1);
            zoneLayer.Columns.Add(column2);
            zoneLayer.Columns.Add(column3);
            zoneLayer.Columns.Add(column4);
            fieldsLayerAreaStyle = new CropzoneAreaStyle();
            fieldsLayerAreaStyle.SelectedAreaStyle = new AreaStyle(new GeoPen(GeoColor.StandardColors.Yellow, 2), new GeoSolidBrush(new GeoColor(50, GeoColor.StandardColors.Yellow)));
            fieldsLayerAreaStyle.AssignedAreaStyle = new AreaStyle(new GeoPen(GeoColor.StandardColors.Green, 2), new GeoSolidBrush(new GeoColor(50, GeoColor.StandardColors.Green)));
            fieldsLayerAreaStyle.UnselectedAreaStyle = new AreaStyle(new GeoPen(GeoColor.StandardColors.LightGray, 2), new GeoSolidBrush(new GeoColor(50, GeoColor.StandardColors.LightGray)));
            fieldsLayerAreaStyle.SelectedFeatures = new List<Feature>();
            fieldsLayerAreaStyle.AssignedFeatures = new List<Feature>();

            zoneLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(fieldsLayerAreaStyle);
            zoneLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(scalingTextStyle);
            zoneLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            zoneLayer.FeatureSource.Projection = projection;
            zoneLayer.FeatureSource.Close();
            zoneOverlay.Layers.Add("ZoneLayer", zoneLayer);
            zoneOverlay.DrawingExceptionMode = DrawingExceptionMode.DrawException;

            labelLayer = new InMemoryFeatureLayer();
            labelLayer.FeatureSource.Open();
            labelLayer.Columns.Add(column1);
            labelLayer.Columns.Add(column2);
            labelLayer.Columns.Add(column3);
            labelLayer.Columns.Add(column4);
            TextStyle zoneTextStyle = TextStyles.CreateSimpleTextStyle(labelcolumn, "Arial", 14, DrawingFontStyles.Regular, GeoColor.StandardColors.Black);
            zoneTextStyle.OverlappingRule = LabelOverlappingRule.NoOverlapping;
            zoneTextStyle.BestPlacement = true;
            labelLayer.ZoomLevelSet.ZoomLevel01.DefaultTextStyle = zoneTextStyle;
            labelLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            labelLayer.FeatureSource.Projection = projection;
            labelLayer.FeatureSource.Close();
            zoneOverlay.Layers.Add("LabelLayer", labelLayer);
        }
        private void CreateGridLayerAndOverlay() {
            gridOverlay = new LayerOverlay();
            gridOverlay.IsVisible = false;
            bboxLayer = new InMemoryFeatureLayer();     // Bounding Box
            bboxLayer.FeatureSource.Open();
            bboxLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = new AreaStyle(new GeoPen(GeoColor.StandardColors.Black, 2), new GeoSolidBrush(new GeoColor(0, GeoColor.StandardColors.Transparent)));
            bboxLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            bboxLayer.FeatureSource.Projection = projection;
            bboxLayer.FeatureSource.Close();
            gridOverlay.Layers.Add("BBoxLayer", bboxLayer);
            gridOverlay.DrawingExceptionMode = DrawingExceptionMode.DrawException;


            ablinesVertLayer = new InMemoryFeatureLayer();     // AB Lines
            ablinesVertLayer.FeatureSource.Open();
            ablinesVertLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = new LineStyle(new GeoPen(GeoColor.StandardColors.Yellow, 4));
            ablinesVertLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            ablinesVertLayer.FeatureSource.Projection = projection;
            ablinesVertLayer.FeatureSource.Close();
            gridOverlay.Layers.Add("ABLinesVertLayer", ablinesVertLayer);

            ablinesHorzLayer = new InMemoryFeatureLayer();     // AB Lines
            ablinesHorzLayer.FeatureSource.Open();
            ablinesHorzLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = new LineStyle(new GeoPen(GeoColor.StandardColors.Yellow, 4));
            ablinesHorzLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            ablinesHorzLayer.FeatureSource.Projection = projection;
            ablinesHorzLayer.FeatureSource.Close();
            gridOverlay.Layers.Add("ABLinesHorzLayer", ablinesHorzLayer);


            drawlinesVertLayer = new InMemoryFeatureLayer();     // Draw Lines
            drawlinesVertLayer.FeatureSource.Open();
            drawlinesVertLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = new LineStyle(new GeoPen(GeoColor.StandardColors.Red, 2));
            drawlinesVertLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            drawlinesVertLayer.FeatureSource.Projection = projection;
            drawlinesVertLayer.FeatureSource.Close();
            gridOverlay.Layers.Add("DrawLinesVertLayer", drawlinesVertLayer);

            drawlinesHorzLayer = new InMemoryFeatureLayer();     // Draw Lines
            drawlinesHorzLayer.FeatureSource.Open();
            drawlinesHorzLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = new LineStyle(new GeoPen(GeoColor.StandardColors.Red, 2));
            drawlinesHorzLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            drawlinesHorzLayer.FeatureSource.Projection = projection;
            drawlinesHorzLayer.FeatureSource.Close();
            gridOverlay.Layers.Add("DrawLinesHorzLayer", drawlinesHorzLayer);
        }

        private void GetBoundingBoxShape() {
            try {
                var shapelayerfeatures = currentLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);
                RectangleShape bbox = ExtentHelper.GetBoundingBoxOfItems(shapelayerfeatures);
                bboxLayer.InternalFeatures.Clear();
                bboxLayer.InternalFeatures.Add(new Feature(bbox));
                List<Vertex> vert1 = new List<Vertex>();
                vert1.Add(new Vertex(bbox.UpperLeftPoint));
                vert1.Add(new Vertex(bbox.UpperRightPoint));
                LineShape ls1 = new LineShape(vert1);
                double linelength1 = ls1.GetLength(GeographyUnit.DecimalDegree, mapDistanceUnit);

                List<Vertex> vert2 = new List<Vertex>();
                vert2.Add(new Vertex(bbox.UpperLeftPoint));
                vert2.Add(new Vertex(bbox.LowerLeftPoint));
                LineShape ls2 = new LineShape(vert2);
                double linelength2 = ls2.GetLength(GeographyUnit.DecimalDegree, mapDistanceUnit);

            }
            catch (Exception ex) {
                log.ErrorException("Caught an exception while getting bounding box", ex);
            }

        }

        private void LoadShapeFileOverlayOntoMap()
        {
            if (Map.Overlays.Contains("importOverlay"))
            {
                Map.Overlays.Remove("importOverlay");
            }
            if (layerOverlay.Layers.Contains("ImportLayer"))
            {
                Map.Overlays.Add("importOverlay", layerOverlay);
            }
            if (Map.Overlays.Contains("selectOverlay"))
            {
                Map.Overlays.Remove("selectOverlay");
            }
            if (selectOverlay.Layers.Contains("selectLayer"))
            {
                Map.Overlays.Add("selectOverlay", selectOverlay);
            }
            if (Map.Overlays.Contains("zoneOverlay"))
            {
                Map.Overlays.Remove("zoneOverlay");
            }
            if (zoneOverlay.Layers.Contains("ZoneLayer"))
            {
                Map.Overlays.Add("zoneOverlay", zoneOverlay);
            }
            if (Map.Overlays.Contains("gridOverlay")) {
                Map.Overlays.Remove("gridOverlay");
            }
            if (gridOverlay.Layers.Contains("BBoxLayer")) {
                Map.Overlays.Add("gridOverlay", gridOverlay);
            }

            EnsureCurrentLayerIsOpen();
            var extent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
            try
            {
                var shapelayerfeatures = currentLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);
                if (shapelayerfeatures.Count > 0)
                {
                    var shapelayercolumns = currentLayer.FeatureSource.GetColumns();
                    extent = ExtentHelper.GetBoundingBoxOfItems(shapelayerfeatures);
                    extent.ScaleUp(100);
                    Map.CurrentExtent = extent;
                    PointShape center = extent.GetCenterPoint();
                    Map.CenterAt(center);
                }
                else
                {
                    Map.CurrentExtent = extent;
                    PointShape center = extent.GetCenterPoint();
                    Map.CenterAt(center);
                }
            }
            catch
            {
                Map.CurrentExtent = extent;
                PointShape center = extent.GetCenterPoint();
                Map.CenterAt(center);
            }
        }

        private void EnsureCurrentLayerIsOpen()
        {
            if (currentLayer == null)
            {
                currentLayer = new InMemoryFeatureLayer();
            }
            if (!currentLayer.IsOpen)
            {
                currentLayer.Open();
            }
            if (zoneLayer == null)
            {
                zoneLayer = new InMemoryFeatureLayer();
            }
            if (!zoneLayer.IsOpen)
            {
                zoneLayer.Open();
            }
            if (labelLayer == null)
            {
                labelLayer = new InMemoryFeatureLayer();
            }
            if (!labelLayer.IsOpen)
            {
                labelLayer.Open();
            }
            if (measureLayer == null)
            {
                measureLayer = new InMemoryFeatureLayer();
            }
            if (!measureLayer.IsOpen)
            {
                measureLayer.Open();
            }
            if (measureLayer2 == null) {
                measureLayer2 = new InMemoryFeatureLayer();
            }
            if (!measureLayer2.IsOpen) {
                measureLayer2.Open();
            }
            if (measureLayer9 == null) {
                measureLayer9 = new InMemoryFeatureLayer();
            }
            if (!measureLayer9.IsOpen) {
                measureLayer9.Open();
            }
            if (selectLayer == null)
            {
                selectLayer = new InMemoryFeatureLayer();
            }
            if (!selectLayer.IsOpen)
            {
                selectLayer.Open();
            }
            if (bboxLayer == null) {
                bboxLayer = new InMemoryFeatureLayer();
            }
            if (!bboxLayer.IsOpen) {
                bboxLayer.Open();
            }
            if (ablinesVertLayer == null) {
                ablinesVertLayer = new InMemoryFeatureLayer();
            }
            if (!ablinesVertLayer.IsOpen) {
                ablinesVertLayer.Open();
            }
            if (ablinesHorzLayer == null) {
                ablinesHorzLayer = new InMemoryFeatureLayer();
            }
            if (!ablinesHorzLayer.IsOpen) {
                ablinesHorzLayer.Open();
            }
            if (drawlinesVertLayer == null) {
                drawlinesVertLayer = new InMemoryFeatureLayer();
            }
            if (!drawlinesVertLayer.IsOpen) {
                drawlinesVertLayer.Open();
            }
            if (drawlinesHorzLayer == null) {
                drawlinesHorzLayer = new InMemoryFeatureLayer();
            }
            if (!drawlinesHorzLayer.IsOpen) {
                drawlinesHorzLayer.Open();
            }
            if (!projection.IsOpen)
            {
                projection.Open();
            }
        }

        void SelectThisItem(CropzoneCreationViewModel itemToSelect)
        {
            if (itemToSelect == null) { return; }
            selectLayer.InternalFeatures.Clear();
            if (itemToSelect.Id == SelectedCropZone.Id) { return; }
            SelectedCropZone = SelectedCropZones.Where(x => x.Id == itemToSelect.Id).SingleOrDefault();
            for (int i = 0; i < SelectedCropZones.Count; i++)
            {
                if (SelectedCropZones[i].Id == itemToSelect.Id)
                {
                    SelectedCropZones[i].Completed = false;
                    break;
                }
            }

            foreach (var scz in SelectedCropZones)
            {
                if (scz.Completed == false)
                {
                    if (!string.IsNullOrEmpty(selectedCropZone.MapData))
                    {
                        if (!selectOverlay.Layers.Contains("selectLayer"))
                        {
                            selectOverlay.Layers.Add("selectLayer", selectLayer);
                        }
                        selectLayer.InternalFeatures.Add(new Feature(scz.MapData));
                    }

                }
            }
            scalingTextStyle.MapScale = Map.CurrentScale;
            Map.Refresh();

            IsMergeMode = false;
            ShowCreateNewItemPopup(selectedCropZone);
        }

        void MergeThisItem(CropzoneCreationViewModel itemToSelect)
        {
            if (itemToSelect == null) { return; }
            selectLayer.InternalFeatures.Clear();
            if (itemToSelect.Id == MergeCropZone.Id) { return; }
            MergeCropZone = MergeCropZones.Where(x => x.Id == itemToSelect.Id).SingleOrDefault();
            for (int i = 0; i < MergeCropZones.Count; i++)
            {
                if (MergeCropZones[i].Id == itemToSelect.Id)
                {
                    if (MergeCropZones[i].Completed == null)
                    {
                        MergeCropZones[i].Completed = false;
                        break;
                    } else if (MergeCropZones[i].Completed == false)
                    {
                        MergeCropZones[i].Completed = null;
                        break;
                    }
                }
            }

            foreach (var scz in MergeCropZones)
            {
                if(scz.Completed == false)
                {
                    if (!string.IsNullOrEmpty(selectedCropZone.MapData))
                    {
                        if (!selectOverlay.Layers.Contains("selectLayer"))
                        {
                            selectOverlay.Layers.Add("selectLayer", selectLayer);
                        }
                        selectLayer.InternalFeatures.Add(new Feature(scz.MapData));
                    }

                }
            }
            scalingTextStyle.MapScale = Map.CurrentScale;
            Map.Refresh();
        }

        void ShowCreateNewItemPopup(CropzoneCreationViewModel selcropzone)
        {
            if (FieldTreeItemVM == null) { return; }
            AbstractTreeItemViewModel parentNode = FieldTreeItemVM;
            createItemViewModel.Id = selectedCropZone.Id;
            createItemViewModel.SelectedParentNode = parentNode;
            createItemViewModel.SelectedCropZone = selectedCropZone;
            createItemViewModel.MapData = selectedCropZone.MapData;
            createItemViewModel.CommandList = selectedCropZone.CommandList;
            createItemViewModel.UseParentBoundary = false;
            ScreenDescriptor descriptor = null;
            if (parentNode is FieldTreeItemViewModel)
            {
                selectedCropZone.Completed = false;
                // Toggle Reported Area UI(ReportedArea)
                createItemViewModel.DisplayReportedAreaUI = true;
                descriptor = new ScreenDescriptor("Landdb.Views.Fields.Popups.AddCropZoneAssignerPopupView", "AddCz", createItemViewModel);
            }
            Messenger.Default.Send<ShowPopupMessage>(new ShowPopupMessage() { ScreenDescriptor = descriptor });
        }

        void OnNewItemCreated(AbstractTreeItemViewModel vm)
        {
            if (!createItemViewModel.IsContinuouslyAdding)
            {
                vm.IsSelected = true;
            }
            CropZoneTreeItemViewModel createdcropzone;
            if (vm != null)
            {
                createdcropzone = vm as CropZoneTreeItemViewModel;
                selectedCropZone.Name = createdcropzone.Name;
                selectedCropZone.Completed = true;
                selectedCropZone.CommandList = createItemViewModel.CommandList;
                ChangeCropZoneBoundary saveBoundaryCommand = new ChangeCropZoneBoundary(createdcropzone.CropZoneId, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), selectedCropZone.MapData, "WKT", selectedCropZone.SelectedArea.Value, selectedCropZone.SelectedArea.Unit.Name, ApplicationEnvironment.CurrentCropYear);
                selectedCropZone.CommandList.Add(saveBoundaryCommand);

                CropZoneTreeItemViewModel newItem = new CropZoneTreeItemViewModel(new Landdb.Domain.ReadModels.Tree.TreeViewCropZoneItem() { CropZoneId = createdcropzone.CropZoneId, CropId = createdcropzone.CropId, Name = createdcropzone.Name, IsPerennial = false, ReportedArea = null, ReportedAreaUnit = null, BoundaryArea = selectedCropZone.SelectedArea.Value, BoundaryAreaUnit = selectedCropZone.SelectedArea.Unit.Name }, FieldTreeItemVM, sortAlphabetically);
                selectedCropZone.Nodelist.Add(newItem);
                RaisePropertyChanged("FieldName");
                RaisePropertyChanged("Name");
                RaisePropertyChanged("SelectedArea");
                RaisePropertyChanged("Completed");
                log.Info("SplitTools - Assign Cropzone: " + createItemViewModel.NewItemName + "  ID: " + createdcropzone.CropZoneId.Id.ToString());
                for (int i = 0; i < SelectedCropZones.Count; i++) {
                    if (SelectedCropZones[i].Id == selectedCropZone.Id) {
                        SelectedCropZones[i].Completed = true;
                        break;
                    }
                }

                MergeCropZones.Clear();
                IsMergeMode = false;
                RaisePropertyChanged("MergeCropZones");

                if (SelectedCropZones.Count == 0 || SelectedCropZones.Where(x => x.Completed == null).Any())
                {
                    ReadyToFinish = false;
                }
                else
                {
                    ReadyToFinish = true;
                }
            }
        }

        void OnNewItemIgnored()
        {
            selectedCropZone.Completed = false;
            selectedCropZone.Crop = new MiniCrop();
            selectedCropZone.CommandList = new List<IDomainCommand>();
            RaisePropertyChanged("Completed");
            if (SelectedCropZones.Count == 0 || SelectedCropZones.Where(x => x.Completed == null).Any())
            {
                ReadyToFinish = false;
            }
            else
            {
                ReadyToFinish = true;
            }
        }

        private GeoCollection<Feature> LoadCutLineShapes()
        {
            GeoCollection<Feature> cutlines = new GeoCollection<Feature>();
            try
            {
                var edfeatures = Map.EditOverlay.EditShapesLayer.InternalFeatures;
                if (Map.EditOverlay.EditShapesLayer.InternalFeatures.Count > 0)
                {
                    LineShape editLineShape = new LineShape();
                    PolygonShape editPolygonShape = new PolygonShape();
                    MultipolygonShape multiPolygonShape = new MultipolygonShape();
                    foreach (var cfeat in edfeatures)
                    {
                        var shape = cfeat.GetShape();
                        if (shape is LineShape)
                        {
                            editLineShape = shape as LineShape;
                            editLineShape = projection.ConvertToInternalProjection(editLineShape) as LineShape;
                            cutlines.Add(new Feature(editLineShape));
                        }
                        if (shape is PolygonShape)
                        {
                            editPolygonShape = shape as PolygonShape;
                            editPolygonShape = projection.ConvertToInternalProjection(editPolygonShape) as PolygonShape;
                            cutlines.Add(new Feature(editPolygonShape));
                        }
                        if (shape is MultipolygonShape) {
                            multiPolygonShape = shape as MultipolygonShape;
                            multiPolygonShape = projection.ConvertToInternalProjection(multiPolygonShape) as MultipolygonShape;
                            foreach(PolygonShape polys in multiPolygonShape.Polygons)
                                cutlines.Add(new Feature(polys));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int z = 0;
            }
            return cutlines;
        }

        private void LoadCutBufferShapes() {
            BuildAllLines = false;
            BuildBorderRows = false;
            BuildDrawBorderRows = false;
            //BuildOnePivot = false;
            BaseShape mastershape = currentLayer.InternalFeatures[0].GetShape();
            string mastershapewkt = mastershape.GetWellKnownText();
            string projectedmastershapewkt = mastershape.GetWellKnownText();
            cuttingwidth = 0;
            double cuttingwidthconstant = System.Convert.ToDouble(cutWidth);
            double multiplierconstant = 1;
            GeoCollection<Feature> cutlines = new GeoCollection<Feature>();
            try {
                if (!Map.EditOverlay.EditShapesLayer.IsOpen) {
                    Map.EditOverlay.EditShapesLayer.Open();
                }
                var edfeatures = Map.EditOverlay.EditShapesLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);

                if (edfeatures.Count == 1) {
                    LineShape editLineShape = new LineShape();
                    foreach (var cfeat in edfeatures) {
                        var shape = cfeat.GetShape();
                        string shapewkt = shape.GetWellKnownText();
                        if (shape is LineShape) {
                            editLineShape = shape as LineShape;
                            string editLineShapewkt = editLineShape.GetWellKnownText();
                            editLineShape = projection.ConvertToInternalProjection(editLineShape) as LineShape;
                            string projectededitLineShapewkt = editLineShape.GetWellKnownText();
                        }

                        PointShape pointShape1 = new PointShape(editLineShape.Vertices[0]);
                        PointShape pointShape2 = new PointShape(editLineShape.Vertices[1]);
                        double Angle1 = GetAngleFromTwoVertices(editLineShape.Vertices[0], editLineShape.Vertices[1]);
                        double Angle2 = GetAngleFromTwoVertices(editLineShape.Vertices[1], editLineShape.Vertices[0]);

                        double offsetAngle1a = Angle1;
                        double offsetAngle1b = GetReverseAngle(Angle1);
                        double offsetAngle2a = Angle2;
                        double offsetAngle2b = GetReverseAngle(Angle2);

                        MultipointShape pointcrossings = mastershape.GetCrossing(editLineShape);
                        LineShape line1 = new LineShape();
                        LineShape line2 = new LineShape();

                        while (pointcrossings.Points.Count > 0) {
                            if (line1.Vertices.Count > 1 && mastershape.Intersects(line1)) {
                                BaseShape basshape = projection.ConvertToExternalProjection(line1);
                                Map.EditOverlay.EditShapesLayer.InternalFeatures.Add(new Feature(basshape));
                            }
                            if (line2.Vertices.Count > 1 && mastershape.Intersects(line2)) {
                                BaseShape basshape = projection.ConvertToExternalProjection(line2);
                                Map.EditOverlay.EditShapesLayer.InternalFeatures.Add(new Feature(basshape));
                            }

                            cuttingwidth += cuttingwidthconstant;
                            PointShape offsetPointShape1a = (PointShape)pointShape1.CloneDeep();
                            PointShape offsetPointShape1b = (PointShape)pointShape1.CloneDeep();
                            PointShape offsetPointShape2a = (PointShape)pointShape2.CloneDeep();
                            PointShape offsetPointShape2b = (PointShape)pointShape2.CloneDeep();
                            offsetPointShape1a.TranslateByDegree(cuttingwidth, offsetAngle1a, GeographyUnit.DecimalDegree, mapDistanceUnit);
                            offsetPointShape1b.TranslateByDegree(cuttingwidth, offsetAngle1b, GeographyUnit.DecimalDegree, mapDistanceUnit);
                            offsetPointShape2a.TranslateByDegree(cuttingwidth, offsetAngle2a, GeographyUnit.DecimalDegree, mapDistanceUnit);
                            offsetPointShape2b.TranslateByDegree(cuttingwidth, offsetAngle2b, GeographyUnit.DecimalDegree, mapDistanceUnit);
                            line1 = new LineShape();
                            line1.Vertices.Add(new Vertex(offsetPointShape1a));
                            line1.Vertices.Add(new Vertex(offsetPointShape2a));
                            line2 = new LineShape();
                            line2.Vertices.Add(new Vertex(offsetPointShape1b));
                            line2.Vertices.Add(new Vertex(offsetPointShape2b));
                            pointcrossings = mastershape.GetCrossing(line1);
                            if (pointcrossings.Points.Count == 0) {
                                pointcrossings = mastershape.GetCrossing(line2);
                            }
                        }
                    }
                }
                Map.EditOverlay.CalculateAllControlPoints();
                Map.EditOverlay.Refresh();
                var edfeatures22 = Map.EditOverlay.EditShapesLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);
            }
            catch (Exception ex) {
                log.ErrorException("Caught an exception while loading cut buffer shapes", ex);
            }
        }

        private void LoadOneCutBufferShape() {
            BuildAllLines = false;
            //BuildOneLines = false;
            BuildBorderRows = false;
            BuildDrawBorderRows = false;
            //BuildOnePivot = false;
            setupforoneline = true;
            mastershape9 = currentLayer.InternalFeatures[0].GetShape();
            mastershapewkt9 = mastershape9.GetWellKnownText();
            projectedmastershapewkt9 = mastershape9.GetWellKnownText();
            cuttingwidthconstant9 = System.Convert.ToDouble(cutWidth);
            double multiplierconstant = 1;
            try {
                if (!Map.EditOverlay.EditShapesLayer.IsOpen) {
                    Map.EditOverlay.EditShapesLayer.Open();
                }
                var edfeatures = Map.EditOverlay.EditShapesLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);

                if (edfeatures.Count >= 1) {
                    LineShape editLineShape = new LineShape();
                        var shape = edfeatures[0].GetShape();
                        string shapewkt = shape.GetWellKnownText();
                        if (shape is LineShape) {
                            editLineShape = shape as LineShape;
                            string editLineShapewkt = editLineShape.GetWellKnownText();
                            editLineShape = projection.ConvertToInternalProjection(editLineShape) as LineShape;
                            string projectededitLineShapewkt = editLineShape.GetWellKnownText();
                        }

                        PointShape pointShape1 = new PointShape(editLineShape.Vertices[0]);
                        PointShape pointShape2 = new PointShape(editLineShape.Vertices[1]);
                        double Angle1 = GetAngleFromTwoVertices(editLineShape.Vertices[0], editLineShape.Vertices[1]);
                        double Angle2 = GetAngleFromTwoVertices(editLineShape.Vertices[1], editLineShape.Vertices[0]);

                        double offsetAngle1a = Angle1;
                        double offsetAngle1b = GetReverseAngle(Angle1);
                        double offsetAngle2a = Angle2;
                        double offsetAngle2b = GetReverseAngle(Angle2);

                        MultipointShape pointcrossings = mastershape9.GetCrossing(editLineShape);
                        LineShape line1 = new LineShape();
                        LineShape line2 = new LineShape();

                            cuttingwidth9 += cuttingwidthconstant9;
                            PointShape offsetPointShape1a = (PointShape)pointShape1.CloneDeep();
                            PointShape offsetPointShape1b = (PointShape)pointShape1.CloneDeep();
                            PointShape offsetPointShape2a = (PointShape)pointShape2.CloneDeep();
                            PointShape offsetPointShape2b = (PointShape)pointShape2.CloneDeep();
                            offsetPointShape1a.TranslateByDegree(cuttingwidth9, offsetAngle1a, GeographyUnit.DecimalDegree, mapDistanceUnit);
                            offsetPointShape1b.TranslateByDegree(cuttingwidth9, offsetAngle1b, GeographyUnit.DecimalDegree, mapDistanceUnit);
                            offsetPointShape2a.TranslateByDegree(cuttingwidth9, offsetAngle2a, GeographyUnit.DecimalDegree, mapDistanceUnit);
                            offsetPointShape2b.TranslateByDegree(cuttingwidth9, offsetAngle2b, GeographyUnit.DecimalDegree, mapDistanceUnit);
                            line1 = new LineShape();
                            line1.Vertices.Add(new Vertex(offsetPointShape1a));
                            line1.Vertices.Add(new Vertex(offsetPointShape2a));
                            line2 = new LineShape();
                            line2.Vertices.Add(new Vertex(offsetPointShape1b));
                            line2.Vertices.Add(new Vertex(offsetPointShape2b));
                            MultipointShape pointcrossings1 = mastershape9.GetCrossing(line1);
                            if (pointcrossings1.Points.Count > 0) {
                                if (line1.Vertices.Count > 1 && mastershape9.Intersects(line1)) {
                                    BaseShape basshape = projection.ConvertToExternalProjection(line1);
                                    Map.EditOverlay.EditShapesLayer.InternalFeatures.Add(new Feature(basshape));
                                }
                            }
                            MultipointShape pointcrossings2 = mastershape9.GetCrossing(line2);
                            if (pointcrossings2.Points.Count > 0) {
                                if (line2.Vertices.Count > 1 && mastershape9.Intersects(line2)) {
                                    BaseShape basshape = projection.ConvertToExternalProjection(line2);
                                    Map.EditOverlay.EditShapesLayer.InternalFeatures.Add(new Feature(basshape));
                                }
                            }
                }
                Map.EditOverlay.CalculateAllControlPoints();
                Map.EditOverlay.Refresh();
                var edfeatures22 = Map.EditOverlay.EditShapesLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);
            }
            catch (Exception ex) {
                log.ErrorException("Caught an exception while loading cut buffer shapes", ex);
            }
        }

        private void LoadMoreCutBufferShape() {
            BuildAllLines = false;
            BuildBorderRows = false;
            BuildDrawBorderRows = false;
            //BuildOnePivot = false;
            setupforoneline = true;
            mastershape9 = currentLayer.InternalFeatures[0].GetShape();
            mastershapewkt9 = mastershape9.GetWellKnownText();
            projectedmastershapewkt9 = mastershape9.GetWellKnownText();
            cuttingwidthconstant9 = System.Convert.ToDouble(cutWidth);
            try {
                var edfeatures = Map.EditOverlay.EditShapesLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);

                if (edfeatures.Count == 1) {
                    return;
                }
                LineShape editLineShape = new LineShape();
                var shape = edfeatures[0].GetShape();
                string shapewkt = shape.GetWellKnownText();
                if (shape is LineShape) {
                    editLineShape = shape as LineShape;
                    string editLineShapewkt = editLineShape.GetWellKnownText();
                    editLineShape = projection.ConvertToInternalProjection(editLineShape) as LineShape;
                    string projectededitLineShapewkt = editLineShape.GetWellKnownText();
                }

                PointShape pointShape1 = new PointShape(editLineShape.Vertices[0]);
                PointShape pointShape2 = new PointShape(editLineShape.Vertices[1]);
                double Angle1 = GetAngleFromTwoVertices(editLineShape.Vertices[0], editLineShape.Vertices[1]);
                double Angle2 = GetAngleFromTwoVertices(editLineShape.Vertices[1], editLineShape.Vertices[0]);

                double offsetAngle1a = Angle1;
                double offsetAngle1b = GetReverseAngle(Angle1);
                double offsetAngle2a = Angle2;
                double offsetAngle2b = GetReverseAngle(Angle2);

                MultipointShape pointcrossings = mastershape9.GetCrossing(editLineShape);
                LineShape line1 = new LineShape();
                LineShape line2 = new LineShape();

                cuttingwidth9 += cuttingwidthconstant9;
                PointShape offsetPointShape1a = (PointShape)pointShape1.CloneDeep();
                PointShape offsetPointShape1b = (PointShape)pointShape1.CloneDeep();
                PointShape offsetPointShape2a = (PointShape)pointShape2.CloneDeep();
                PointShape offsetPointShape2b = (PointShape)pointShape2.CloneDeep();
                offsetPointShape1a.TranslateByDegree(cuttingwidth9, offsetAngle1a, GeographyUnit.DecimalDegree, mapDistanceUnit);
                offsetPointShape1b.TranslateByDegree(cuttingwidth9, offsetAngle1b, GeographyUnit.DecimalDegree, mapDistanceUnit);
                offsetPointShape2a.TranslateByDegree(cuttingwidth9, offsetAngle2a, GeographyUnit.DecimalDegree, mapDistanceUnit);
                offsetPointShape2b.TranslateByDegree(cuttingwidth9, offsetAngle2b, GeographyUnit.DecimalDegree, mapDistanceUnit);
                line1 = new LineShape();
                line1.Vertices.Add(new Vertex(offsetPointShape1a));
                line1.Vertices.Add(new Vertex(offsetPointShape2a));
                line2 = new LineShape();
                line2.Vertices.Add(new Vertex(offsetPointShape1b));
                line2.Vertices.Add(new Vertex(offsetPointShape2b));
                MultipointShape pointcrossings1 = mastershape9.GetCrossing(line1);
                if (pointcrossings1.Points.Count > 0) {
                    if (line1.Vertices.Count > 1 && mastershape9.Intersects(line1)) {
                        BaseShape basshape = projection.ConvertToExternalProjection(line1);
                        Map.EditOverlay.EditShapesLayer.InternalFeatures.Add(new Feature(basshape));
                    }
                }
                MultipointShape pointcrossings2 = mastershape9.GetCrossing(line2);
                if (pointcrossings2.Points.Count > 0) {
                    if (line2.Vertices.Count > 1 && mastershape9.Intersects(line2)) {
                        BaseShape basshape = projection.ConvertToExternalProjection(line2);
                        Map.EditOverlay.EditShapesLayer.InternalFeatures.Add(new Feature(basshape));
                    }
                }
                Map.EditOverlay.CalculateAllControlPoints();
                Map.EditOverlay.Refresh();
                var edfeatures22 = Map.EditOverlay.EditShapesLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);
            }
            catch (Exception ex) {
                log.ErrorException("Caught an exception while loading cut buffer shapes", ex);
            }
        }

        private void LoadCutBorderRowShape() {
            BuildBorderRows = false;
            BuildDrawBorderRows = false;
            setupforoneline = true;
            mastershape9 = currentLayer.InternalFeatures[0].GetShape();
            mastershapewkt9 = mastershape9.GetWellKnownText();
            projectedmastershapewkt9 = mastershape9.GetWellKnownText();
            double borderrowcuttingwidthconstant = System.Convert.ToDouble(bufferWidth);
            double multiplierconstant = 1;

            MultipolygonShape mps = new MultipolygonShape();
            BaseShape baseshp = selectedFeature.GetShape();
            if (baseshp is PolygonShape)
                                        {
                mps.Polygons.Add(baseshp as PolygonShape);
            }
            if (baseshp is MultipolygonShape)
            {
                mps = baseshp as MultipolygonShape;
            }
            borderrowtool = false;
            double borderrowwidth_0 = borderrowcuttingwidthconstant;
            //double borderrowwidth_8 = borderrowcuttingwidthconstant * .8;
            double borderrowwidthx2 = borderrowcuttingwidthconstant * 2.0;

            try
            {
                MultipolygonShape correctedpolygon = mps.Buffer(0, Map.MapUnit, MapUnitFactory.AreaDistanceMeasureConversion(Map.MapUnit));
                mps = correctedpolygon;
                MultipolygonShape newmultipoly = new MultipolygonShape();
                //MultilineShape multiLineShape = mps.Polygons[0].ToMultiLineShape();
                MultilineShape multiLineShape = mps.ToMultiLineShape();

                MultipolygonShape newmultipoly3;
                newmultipoly3 = BufferShape(borderrowcuttingwidthconstant, multiLineShape) as MultipolygonShape;

                BaseShape projectedpoly = projection.ConvertToExternalProjection(newmultipoly3);
                Map.EditOverlay.EditShapesLayer.InternalFeatures.Add(new Feature(projectedpoly));
                Map.EditOverlay.CalculateAllControlPoints();
                Map.EditOverlay.Refresh();
                //var edfeatures22 = Map.EditOverlay.EditShapesLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);
            }
            catch (Exception ex)
            {
                log.ErrorException("Caught an exception while loading cut buffer shapes", ex);
            }
        }

        private BaseShape BufferShape(double borderrowcuttingwidthconstant, MultilineShape multiLineShape)
        {
            PointShape center = multiLineShape.GetCenterPoint();
            int EPSG = 32600;
            int UTM_zone = (int)(Math.Floor((center.X + 180.0) / 6) + 1);
            if (center.Y < 0)
            {
                EPSG = 32700;
            }
            EPSG += UTM_zone;

            double borderrowwidth_0 = borderrowcuttingwidthconstant;
            if (areaUnitString == "acre")
            {
                LengthMeasure feetmeasure = Foot.Self.GetMeasure(borderrowcuttingwidthconstant) as LengthMeasure;
                Measure metersmeasure = feetmeasure.CreateAs(AgC.UnitConversion.Length.Meter.Self);
                borderrowwidth_0 = metersmeasure.Value;
            }
            else
            {
                LengthMeasure metersmeasure = Meter.Self.GetMeasure(borderrowcuttingwidthconstant) as LengthMeasure;
                borderrowwidth_0 = metersmeasure.Value;
            }

            Proj4Projection bufferprojection = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetEpsgParametersString(EPSG));
            if(!bufferprojection.IsOpen)
            {
                bufferprojection.Open();
            }
            BaseShape utmbuffershape = bufferprojection.ConvertToExternalProjection(multiLineShape);

            //MultipolygonShape newmultipoly3 = utmbuffershape.Buffer(borderrowwidth_0, 8, BufferCapType.Square, GeographyUnit.Meter, DistanceUnit.Meter);
            MultipolygonShape newmultipoly3 = utmbuffershape.Buffer(borderrowwidth_0, 8, BufferCapType.Butt, GeographyUnit.Meter, DistanceUnit.Meter);
            BaseShape wgs84buffershape = bufferprojection.ConvertToInternalProjection(newmultipoly3);
            return wgs84buffershape;
        }

        private MultipolygonShape NegativeBufferPolygon(PolygonShape polygonShape, double distance, GeographyUnit polygonShapeUnit, DistanceUnit distanceUnit) {
            MultipolygonShape finishedpoly = new MultipolygonShape();
            try {
                PolygonShape borderrows = new PolygonShape();
                MultipolygonShape ringpoly = polygonShape.OuterRing.Buffer(0, polygonShapeUnit, distanceUnit);

                MultilineShape mls = ringpoly.ToMultiLineShape();
                //mls.GetLineOnALine()
                double area1 = ringpoly.GetArea(polygonShapeUnit, mapAreaUnit);
                MultipolygonShape buffer1 = mls.Buffer(distance, polygonShapeUnit, distanceUnit);
                double area2 = buffer1.GetArea(polygonShapeUnit, mapAreaUnit);
                MultipolygonShape diff1 = ringpoly.GetIntersection(buffer1);
                //MultipolygonShape diff1 = ringpoly.GetDifference(buffer1);
                double area = diff1.GetArea(polygonShapeUnit, mapAreaUnit);
                finishedpoly = diff1;
            }
            catch (Exception ex) {
                log.ErrorException("Caught an exception while creating border row buffer", ex);
            }

            return finishedpoly;
        }

        private LineShape GetPerimeter(PolygonShape polygonShape) {
            LineShape perimeterLineShape = new LineShape();
            foreach (Vertex vertex in polygonShape.OuterRing.Vertices) {
                perimeterLineShape.Vertices.Add(new Vertex(vertex.X, vertex.Y));
            }
            return perimeterLineShape;
        }

        void ChangeOperatingMode()
        {
            if (operatingmode < 0)
            {
                modeofoperation = "Canceling Mode";
                IsAssigningMode = false;
                IsMergeMode = false;
                displayborderrow = false;
                ShowGrid = false;
                Map.Cursor = Cursors.Arrow;
                OnCancelCropzone();
            }
            if (operatingmode == 0)
            {
                modeofoperation = "Drawing Mode";
                IsAssigningMode = false;
                IsMergeMode = false;
                ShowGrid = true;
            }
            if (operatingmode == 1)
            {
                log.Info("SplitTools - Assign Crops To Shapes.");
                modeofoperation = "Assigning Mode";
                CommandDescription = "'Assigning Crops To Shapes'";
                IsAssigningMode = true;
                IsMergeMode = true;
                DisplayBorderRow = false;
                ShowGrid = false;
                //Map.TrackOverlay.TrackMode = TrackMode.Point;
                //Map.Cursor = Cursors.Arrow;
                selectedCropZones = new ObservableCollection<CropzoneCreationViewModel>();
                mergeCropZones = new ObservableCollection<CropzoneCreationViewModel>();
                foreach (Feature feature in zoneLayer.InternalFeatures)
                {
                    MultipolygonShape multiPolygonShape = feature.GetShape() as MultipolygonShape;
                    string spatialData = string.Empty;
                    if (multiPolygonShape.Polygons.Count > 0)
                    {
                        spatialData = multiPolygonShape.GetWellKnownText();
                        spatialData = spatialData.Replace(@"\n", "");
                        spatialData = spatialData.Replace(@"\r", "");
                        spatialData = spatialData.Replace(@"\t", "");
                    }
                    double area = 0;
                    string cenlatlon = string.Empty;
                    AreaMeasure selectedarea = UnitFactory.GetUnitByName(areaUnitString).GetMeasure((double)area) as AreaMeasure;
                    if (multiPolygonShape.Polygons.Count > 0)
                    {
                        try
                        {
                            area = multiPolygonShape.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
                            if (area < .01) {
                                continue;
                            }
                            selectedarea = UnitFactory.GetUnitByName(areaUnitString).GetMeasure((double)area) as AreaMeasure;
                            PointShape centerpoint = multiPolygonShape.GetCenterPoint();
                            Vertex geoVertex1 = new Vertex(centerpoint.X, centerpoint.Y);
                            string decimaldegrees = Math.Round(geoVertex1.Y, 5) + ", " + Math.Round(geoVertex1.X, 5);
                            string longlat = DecimalDegrees.d_To_DMs(geoVertex1.X, 2, DecimalDegrees.Type.Longitude) + "  " + DecimalDegrees.d_To_DMs(geoVertex1.Y, 2, DecimalDegrees.Type.Latitude);
                            string longlatAG = DecimalDegrees.d_To_Dm(geoVertex1.X, 4, DecimalDegrees.Type.Longitude) + "  " + DecimalDegrees.d_To_Dm(geoVertex1.Y, 4, DecimalDegrees.Type.Latitude);
                            cenlatlon = decimaldegrees;
                        }
                        catch
                        {
                            area = 0;
                            continue;
                        }
                    }
                    var czId = new CropZoneId(ApplicationEnvironment.CurrentDataSourceId, Guid.NewGuid());
                    selectedCropZone = new CropzoneCreationViewModel(czId, FieldTreeItemVM.Name, selectedarea.FullDisplay, spatialData, cenlatlon, selectedarea);
                    double calcareapercent = area / fieldareatest;
                    selectedCropZone.AreaPercent = calcareapercent;
                    SelectedCropZones.Add(selectedCropZone);
                    mergeCropZone = new CropzoneCreationViewModel(czId, FieldTreeItemVM.Name, selectedarea.FullDisplay, spatialData, cenlatlon, selectedarea);
                    mergeCropZone.AreaPercent = calcareapercent;
                    MergeCropZones.Add(mergeCropZone);
                    ReadyToFinish = false;
                    RaisePropertyChanged("SelectedCropZones");
                    RaisePropertyChanged("MergeCropZones");
                }
                measureLayer.InternalFeatures.Clear();
                measureLayer2.InternalFeatures.Clear();
                Map.EditOverlay.EditShapesLayer.InternalFeatures.Clear();
                Map.EditOverlay.CalculateAllControlPoints();
                scalingTextStyle.MapScale = Map.CurrentScale;
                Map.Refresh();
            }
            if (operatingmode > 1)
            {
                modeofoperation = "Saving Mode";
                IsAssigningMode = false;
                IsMergeMode = false;
                displayborderrow = false;
                ShowGrid = false;
                Map.Cursor = Cursors.Arrow;
                OnCompleteCropzone();
            }
        }

        void OnCancelCropzone()
        {
            log.Info("SplitTools - Cancel split shape.");
            operatingmode = 0;
            ReSetMap();
            Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Clear();
            Map.EditOverlay.EditShapesLayer.InternalFeatures.Clear();
            Map.EditOverlay.CalculateAllControlPoints();
            Map.Cursor = Cursors.Arrow;
            scalingTextStyle.MapScale = Map.CurrentScale;
            Map.Refresh();
            this.clientEndpoint.SaveMapSettings(this.mapsettings);
            Messenger.Default.Send<ShowMainViewMessage>(new ShowMainViewMessage());
        }

        void OnCompleteCropzone()
        {
            log.Info("SplitTools - Save split shape.");
            var sortedcropzones = from f in SelectedCropZones
                                  orderby f.Name
                                  select f;

            foreach (var ccvm in sortedcropzones)
            {
                if (ccvm.Completed.HasValue && ccvm.Completed.Value)
                {
                    foreach (var idc in ccvm.CommandList)
                    {
                        clientEndpoint.SendOne(idc);
                    }
                    foreach (var newItem in ccvm.Nodelist)
                    {
                        FieldTreeItemVM.Children.Add(newItem);
                        FieldTreeItemVM.IsExpanded = true;
                    }
                }
            }
            operatingmode = 0;
            ReSetMap();
            Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Clear();
            Map.EditOverlay.EditShapesLayer.InternalFeatures.Clear();
            Map.EditOverlay.CalculateAllControlPoints();
            scalingTextStyle.MapScale = Map.CurrentScale;
            Map.Refresh();
            Map.Cursor = Cursors.Arrow;
            Messenger.Default.Send<ShowMainViewMessage>(new ShowMainViewMessage());
        }

        private void MeasureNearestBoundary(Feature firstfeature)
        {
            MultilineShape multiLineShape = firstfeature.GetShape().GetShortestLineTo(boundarypoints, GeographyUnit.Meter);
            measureLayer.InternalFeatures.Add(new Feature(multiLineShape.GetWellKnownBinary()));
            try
            {
                selectLayer.InternalFeatures.Add(new Feature(new PointShape(multiLineShape.Lines[0].Vertices[0]).GetWellKnownBinary()));
                selectLayer.InternalFeatures.Add(new Feature(new PointShape(multiLineShape.Lines[0].Vertices[1]).GetWellKnownBinary()));
            }
            catch (Exception ex)
            {
                int z = 0;
            }
        }

        private IList<Feature> SplitPolgonsWithLines(GeoCollection<Feature> cutfeatures)
        {
            //lock (clientLayerLock) {
            IList<Feature> layerfeatures = new List<Feature>();
            IList<LineShape> lines = new List<LineShape>();
            IList<BaseShape> baseshapes = new List<BaseShape>();
            LineShape previousline = null;

            try
            {
                selectLayer.InternalFeatures.Clear();
                measureLayer.InternalFeatures.Clear();
                IList<MultipointShape> crossinglist = new List<MultipointShape>();
                BaseShape mastershape = currentLayer.InternalFeatures[0].GetShape();
                foreach (Feature feature in cutfeatures) {
                    try {
                        BaseShape baseshape = feature.GetShape();
                        baseshapes.Add(baseshape);
                        if (baseshape is LineShape) {
                            MultipointShape crossingpoints = baseshape.GetCrossing(mastershape);
                            crossinglist.Add(crossingpoints);
                        }
                    }
                    catch (Exception ex5) {
                        int sss = 0;
                    }
                }

                measureLayer.BuildIndex();

                zoneLayer.InternalFeatures.Clear();
                labelLayer.InternalFeatures.Clear();
                measureLayer9.InternalFeatures.Clear();
                readytoassign = false;
                //try {
                foreach (Feature feat in currentLayer.InternalFeatures)
                {
                    BaseShape baseshape2 = feat.GetShape();
                    MultipolygonShape mps2 = new MultipolygonShape();
                    if (baseshape2 is MultipolygonShape)
                    {
                        mps2 = baseshape2 as MultipolygonShape;
                    }
                    else if (baseshape2 is PolygonShape)
                    {
                        mps2.Polygons.Add(baseshape2 as PolygonShape);
                    }
                    try
                    {
                        MultipolygonShape mps1 = Splitter.Split(mps2, baseshapes);
                        foreach (PolygonShape poly4 in mps1.Polygons)
                        {
                            MultipolygonShape mps4 = new MultipolygonShape();
                            mps4.Polygons.Add(poly4);
                            try
                            {
                                double area = mps4.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
                                if(area < .01) {
                                    continue;
                                }
                                Feature feature4 = new Feature(mps4);
                                feature4.ColumnValues.Add(labelcolumn, area.ToString("N2"));
                                feature4.ColumnValues.Add(namecolumn, "UnAssigned");
                                feature4.ColumnValues.Add(idcolumn, "0000");
                                feature4.ColumnValues.Add(areacolumn, area.ToString("N2"));
                                zoneLayer.InternalFeatures.Add(feature4);
                                labelLayer.InternalFeatures.Add(feature4);
                                LineShape ls = new LineShape();
                                foreach(Vertex vert in poly4.OuterRing.Vertices) {
                                    ls.Vertices.Add(vert);
                                }

                                if(showMeasurementsCommands) {
                                    measureLayer9.InternalFeatures.Add(new Feature(ls));
                                }

                                layerfeatures.Add(feature4);
                                readytoassign = true;
                            }
                            catch (Exception ex)
                            {
                                int xxx = 0;
                            }
                        }
                    }
                    catch (Exception ex1)
                    {
                        int xxxx = 0;
                    }
                }
                zoneLayer.BuildIndex();
                labelLayer.BuildIndex();

            }
            catch (Exception ex2)
            {
                int xxxxx = 0;
            }
            finally
            {
            }
            RaisePropertyChanged("IsDrawingMode");
            return layerfeatures;
            //}
        }
        

        void SelectMapShapes()
        {
            if (skipwhiledrawing)
            {
                Map.TrackOverlay.TrackMode = TrackMode.None;
                Map.Cursor = Cursors.Hand;
                CommandDescription = "'Pan-Zoom Map'";
                draggingfeatures = false;
                trackingfeatures = false;
                borderrowtool = false;
                drawingpivot = false;
                snipapolygon = false;
                drawingpartialpivot = false;
                deletetool = false;
                assigntool = false;
                mergetool = false;
                readytobuild = TestToBuild(cutWidth);
                Map.EditOverlay.CanAddVertex = true;
                Map.EditOverlay.CanDrag = true;
                Map.EditOverlay.CanRemoveVertex = true;
                skipwhiledrawing = false;
                return;
            }
            log.Info("SplitTools - select shapes for assignment.");
            Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Clear();
            Map.Refresh();
            Map.TrackOverlay.TrackMode = TrackMode.Point;
            Map.Cursor = Cursors.Cross;
            assigntool = true;
            skipwhiledrawing = true;
            CommandDescription = "'Assigning Shape'";
            Map.EditOverlay.CanAddVertex = false;
            Map.EditOverlay.CanDrag = false;
            Map.EditOverlay.CanRemoveVertex = false;
            Map.EditOverlay.CanResize = false;
            Map.EditOverlay.CanRotate = false;
        }

        void MergeMapShapes()
        {
            if (skipwhiledrawing)
            {
                Map.TrackOverlay.TrackMode = TrackMode.None;
                Map.Cursor = Cursors.Hand;
                CommandDescription = "'Pan-Zoom Map'";
                draggingfeatures = false;
                trackingfeatures = false;
                borderrowtool = false;
                snipapolygon = false;
                drawingpivot = false;
                drawingpartialpivot = false;
                deletetool = false;
                assigntool = false;
                mergetool = false;
                readytobuild = TestToBuild(cutWidth);
                Map.EditOverlay.CanAddVertex = true;
                Map.EditOverlay.CanDrag = true;
                Map.EditOverlay.CanRemoveVertex = true;
                skipwhiledrawing = false;
                return;
            }
            log.Info("SplitTools - select shapes for merge.");
            Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Clear();
            Map.Refresh();
            Map.TrackOverlay.TrackMode = TrackMode.Point;
            Map.Cursor = Cursors.Cross;
            mergetool = true;
            skipwhiledrawing = true;
            CommandDescription = "'Merging Shapes'";
            Map.EditOverlay.CanAddVertex = false;
            Map.EditOverlay.CanDrag = false;
            Map.EditOverlay.CanRemoveVertex = false;
            Map.EditOverlay.CanResize = false;
            Map.EditOverlay.CanRotate = false;
        }

        void SliceLine() {
            if (skipwhiledrawing)
            {
                Map.TrackOverlay.TrackMode = TrackMode.None;
                Map.Cursor = Cursors.Hand;
                CommandDescription = Strings.EditCuttingShapes_Text;
                draggingfeatures = false;
                trackingfeatures = false;
                borderrowtool = false;
                drawingpivot = false;
                drawingpartialpivot = false;
                readytobuild = TestToBuild(cutWidth);
                Map.EditOverlay.CanAddVertex = true;
                Map.EditOverlay.CanDrag = true;
                Map.EditOverlay.CanRemoveVertex = true;
                snipapolygon = false;
                skipwhiledrawing = false;
                return;
            }
            log.Info("SplitTools - Slice line.");
            Map.TrackOverlay.TrackMode = TrackMode.Line;
            Map.Cursor = Cursors.Pen;
            skipwhiledrawing = true;
            CommandDescription = Strings.DrawSlicingLine_Text;
        }

        void Scissors()
        {
            if (skipwhiledrawing)
            {
                Map.TrackOverlay.TrackMode = TrackMode.None;
                Map.Cursor = Cursors.Hand;
                CommandDescription = Strings.EditCuttingShapes_Text;
                draggingfeatures = false;
                trackingfeatures = false;
                borderrowtool = false;
                drawingpivot = false;
                drawingpartialpivot = false;
                readytobuild = TestToBuild(cutWidth);
                Map.EditOverlay.CanAddVertex = true;
                Map.EditOverlay.CanDrag = true;
                Map.EditOverlay.CanRemoveVertex = true;
                skipwhiledrawing = false;
                return;
            }
            log.Info("SplitTools - Slice line.");
            Map.TrackOverlay.TrackMode = TrackMode.Line;
            Map.Cursor = Cursors.Pen;
            skipwhiledrawing = true;
            snipapolygon = true;
            CommandDescription = Strings.DrawSlicingLine_Text;
        }

        void SlicePolygon() {
            if (skipwhiledrawing)
            {
                Map.TrackOverlay.TrackMode = TrackMode.None;
                Map.Cursor = Cursors.Hand;
                CommandDescription = Strings.EditCuttingShapes_Text;
                draggingfeatures = false;
                trackingfeatures = false;
                borderrowtool = false;
                snipapolygon = false;
                drawingpivot = false;
                drawingpartialpivot = false;
                readytobuild = TestToBuild(cutWidth);
                Map.EditOverlay.CanAddVertex = true;
                Map.EditOverlay.CanDrag = true;
                Map.EditOverlay.CanRemoveVertex = true;
                skipwhiledrawing = false;
                return;
            }
            log.Info("SplitTools - Slice polygon.");
            Map.TrackOverlay.TrackMode = TrackMode.Polygon;
            Map.Cursor = Cursors.Cross;
            skipwhiledrawing = true;
            CommandDescription = Strings.DrawCuttingPolygon_Text;
        }

        void DrawPivotCenter()
        {
            if (skipwhiledrawing)
            {
                Map.TrackOverlay.TrackMode = TrackMode.None;
                Map.Cursor = Cursors.Hand;
                CommandDescription = Strings.EditCuttingShapes_Text;
                draggingfeatures = false;
                trackingfeatures = false;
                borderrowtool = false;
                snipapolygon = false;
                drawingpivot = false;
                drawingpartialpivot = false;
                readytobuild = TestToBuild(cutWidth);
                Map.EditOverlay.CanAddVertex = true;
                Map.EditOverlay.CanDrag = true;
                Map.EditOverlay.CanRemoveVertex = true;
                skipwhiledrawing = false;
                return;
            }
            log.Info("PivotTools - Set Pivot Center");
            Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Clear();
            Map.Refresh();
            Map.TrackOverlay.TrackMode = TrackMode.Point;
            previousCursor = Map.Cursor;
            Map.Cursor = Cursors.Cross;
            skipwhiledrawing = true;
            drawingpivot = true;
            CommandDescription = Strings.DrawPivot_Text;
        }

        void DrawPartialPivotCenter()
        {
            if (skipwhiledrawing)
            {
                Map.TrackOverlay.TrackMode = TrackMode.None;
                Map.Cursor = Cursors.Hand;
                CommandDescription = Strings.EditCuttingShapes_Text;
                draggingfeatures = false;
                trackingfeatures = false;
                borderrowtool = false;
                snipapolygon = false;
                drawingpivot = false;
                drawingpartialpivot = false;
                readytobuild = TestToBuild(cutWidth);
                Map.EditOverlay.CanAddVertex = true;
                Map.EditOverlay.CanDrag = true;
                Map.EditOverlay.CanRemoveVertex = true;
                skipwhiledrawing = false;
                return;
            }
            log.Info("PivotTools - Set Pivot Center");
            Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Clear();
            Map.Refresh();
            Map.TrackOverlay.TrackMode = TrackMode.Point;
            previousCursor = Map.Cursor;
            Map.Cursor = Cursors.Cross;
            skipwhiledrawing = true;
            drawingpartialpivot = true;
            CommandDescription = Strings.DrawPivot_Text;
        }

        void DeleteShape()
        {
            if (skipwhiledrawing)
            {
                Map.TrackOverlay.TrackMode = TrackMode.None;
                Map.Cursor = Cursors.Hand;
                CommandDescription = Strings.EditCuttingShapes_Text;
                draggingfeatures = false;
                trackingfeatures = false;
                borderrowtool = false;
                snipapolygon = false;
                drawingpivot = false;
                drawingpartialpivot = false;
                readytobuild = TestToBuild(cutWidth);
                Map.EditOverlay.CanAddVertex = true;
                Map.EditOverlay.CanDrag = true;
                Map.EditOverlay.CanRemoveVertex = true;
                skipwhiledrawing = false;
                return;
            }
            log.Info("SplitTools - delete shape.");
            Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Clear();
            Map.Refresh();
            Map.TrackOverlay.TrackMode = TrackMode.Point;
            Map.Cursor = Cursors.Cross;
            deletetool = true;
            skipwhiledrawing = true;
            CommandDescription = Strings.DeleteShape_Text;
            Map.EditOverlay.CanAddVertex = false;
            Map.EditOverlay.CanDrag = false;
            Map.EditOverlay.CanRemoveVertex = false;
            Map.EditOverlay.CanResize = false;
            Map.EditOverlay.CanRotate = false;

        }

        void MergeShape()
        {
            if(!IsAssigningMode) { return; }
            log.Info("SplitTools - merge shapes.");

            CropzoneCreationViewModel newcropzonemodel = null;
            if(mergeCropZones.Count == 0) { return; }
            foreach(CropzoneCreationViewModel newcropzone in mergeCropZones)
            {
                if (newcropzone.Completed == null) { continue; }
                if (newcropzone.Completed == true) { continue; }
                if (newcropzonemodel == null)
                {
                    newcropzonemodel = newcropzone;
                    continue;
                }
                MultipolygonShape multiPolygonShape = new MultipolygonShape();
                MultipolygonShape oldpoly = new MultipolygonShape(newcropzonemodel.MapData);
                MultipolygonShape newpoly = new MultipolygonShape(newcropzone.MapData);
                string spatialData = string.Empty;
                try
                {
                    multiPolygonShape = oldpoly;
                    foreach(var ply in newpoly.Polygons)
                    {
                        multiPolygonShape.Polygons.Add(ply);
                    }
                    Feature feat = new Feature(multiPolygonShape);
                    feat.MakeValid();
                    BaseShape newbaseshape = feat.GetShape();
                    if (oldpoly.Polygons.Count > 0 && newpoly.Polygons.Count > 0)
                    {
                        spatialData = newbaseshape.GetWellKnownText();
                        spatialData = spatialData.Replace(@"\n", "");
                        spatialData = spatialData.Replace(@"\r", "");
                        spatialData = spatialData.Replace(@"\t", "");
                    }
                }
                catch (Exception ex)
                {
                    log.Error("Error merging a shape - {0}", ex);
                    System.Windows.MessageBox.Show(Strings.ThereWasAProblemDeletingShape_Text);
                }
                double area = 0;
                string cenlatlon = string.Empty;
                AreaMeasure selectedarea = UnitFactory.GetUnitByName(areaUnitString).GetMeasure((double)area) as AreaMeasure;
                if (multiPolygonShape.Polygons.Count > 0)
                {
                    try
                    {
                        area = multiPolygonShape.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
                        if (area < .0001) { continue; }
                        selectedarea = UnitFactory.GetUnitByName(areaUnitString).GetMeasure((double)area) as AreaMeasure;
                        PointShape centerpoint = multiPolygonShape.GetCenterPoint();
                        Vertex geoVertex1 = new Vertex(centerpoint.X, centerpoint.Y);
                        string decimaldegrees = Math.Round(geoVertex1.Y, 5) + ", " + Math.Round(geoVertex1.X, 5);
                        string longlat = DecimalDegrees.d_To_DMs(geoVertex1.X, 2, DecimalDegrees.Type.Longitude) + "  " + DecimalDegrees.d_To_DMs(geoVertex1.Y, 2, DecimalDegrees.Type.Latitude);
                        string longlatAG = DecimalDegrees.d_To_Dm(geoVertex1.X, 4, DecimalDegrees.Type.Longitude) + "  " + DecimalDegrees.d_To_Dm(geoVertex1.Y, 4, DecimalDegrees.Type.Latitude);
                        cenlatlon = decimaldegrees;
                    }
                    catch
                    {
                        area = 0;
                        continue;
                    }
                }

                newcropzonemodel.SelectedArea = selectedarea;
                newcropzonemodel.FieldName = selectedarea.FullDisplay;
                newcropzonemodel.MapData = spatialData;
                newcropzonemodel.CenterLatLong = cenlatlon;
                double calcareapercent = area / fieldareatest;
                newcropzonemodel.AreaPercent = calcareapercent;
                newcropzonemodel.MakePreviewShape();
            }

            for(int i = mergeCropZones.Count - 1; i >= 0; i--)
            {
                if(mergeCropZones[i].Id == newcropzonemodel.Id)
                {
                    mergeCropZones[i].SelectedArea = newcropzonemodel.SelectedArea;
                    mergeCropZones[i].MapData = newcropzonemodel.MapData;
                    mergeCropZones[i].CenterLatLong = newcropzonemodel.CenterLatLong;
                    mergeCropZones[i].AreaPercent = newcropzonemodel.AreaPercent;
                    mergeCropZones[i].MakePreviewShape();
                    mergeCropZones[i].Completed = null;
                    continue;
                }
                if(mergeCropZones[i].Completed == false)
                {
                    mergeCropZones.RemoveAt(i);
                }
            }

            selectedCropZones.Clear();
            foreach(CropzoneCreationViewModel zone in mergeCropZones)
            {
                selectedCropZone = new CropzoneCreationViewModel(zone.Id, zone.Name, zone.FieldName, zone.MapData, zone.CenterLatLong, zone.SelectedArea);
                selectedCropZone.AreaPercent = zone.AreaPercent;
                selectedCropZones.Add(selectedCropZone);
            }

            mergeCropZones.Clear();
            foreach (CropzoneCreationViewModel zone in selectedCropZones)
            {
                mergeCropZone = new CropzoneCreationViewModel(zone.Id, zone.Name, zone.FieldName, zone.MapData, zone.CenterLatLong, zone.SelectedArea);
                mergeCropZone.AreaPercent = zone.AreaPercent;
                mergeCropZones.Add(mergeCropZone);
            }

            //SelectedCropZones = mergeCropZones;
            selectLayer.InternalFeatures.Clear();
            Map.Refresh();
            RaisePropertyChanged("SelectedCropZones");
            RaisePropertyChanged("MergeCropZones");
        }

        void AssignShape()
        {
            operatingmode = 1;
            ChangeOperatingMode();
        }

        void Cancel()
        {
            operatingmode = -1;
            ChangeOperatingMode();
        }

        void Finished()
        {
            operatingmode = 99;
            ChangeOperatingMode();
        }

        internal void LoadMapForItem(object item)
        {
            IIdentity itemId = null;
            compiledpoly = new MultipolygonShape();
            if (item == null) { return; }
            Landdb.ViewModel.Fields.FieldDetails.FieldDetailsViewModel fvitem = null;
            try {
                if (item is Landdb.ViewModel.Fields.FieldDetails.FieldDetailsViewModel) {
                    itemId = ((Landdb.ViewModel.Fields.FieldDetails.FieldDetailsViewModel)item).FieldView.Id;
                    fvitem = (Landdb.ViewModel.Fields.FieldDetails.FieldDetailsViewModel)item;
                    IList<CropZoneTreeItemViewModel> czvmlist = ((Landdb.ViewModel.Fields.FieldDetails.FieldDetailsViewModel)item).CropZoneTreeModels;
                    foreach (CropZoneTreeItemViewModel cztree in czvmlist) {
                        var maps = clientEndpoint.GetView<Landdb.Domain.ReadModels.Map.ItemMap>(cztree.CropZoneId);
                        if (maps.HasValue) {
                            Landdb.Domain.ReadModels.Map.ItemMapItem mri = maps.Value.MostRecentMapItem;
                            var feature = new Feature(mri.MapData);
                            BaseShape bs1 = feature.GetShape();
                            if (bs1 is MultipolygonShape) {
                                compiledpoly = compiledpoly.Union(feature);
                            }
                        }
                    }
                }
            }
            catch (Exception ex) {
                log.Error("CreateCropzoneTools - Error load map item - {0}", ex);
            }
        }

        public void HandleMeasureLine() {
            log.Info("SplitTools - Measure tool.");
            measuretool = true;
            MapStyles.CustomDistancePolygonStyle customDistancePolygonStyleText2 = new MapStyles.CustomDistancePolygonStyle(mapAreaUnit, mapDistanceUnit, new GeoFont("Arial", 12, DrawingFontStyles.Regular), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.White), new GeoSolidBrush(GeoColor.StandardColors.Red), new GeoPen(GeoColor.StandardColors.Red));
            customDistancePolygonStyleText.YOffsetInPixel = -20;
            customDistanceLineStyleText = new MapStyles.CustomDistanceLineStyle(mapDistanceUnit, new GeoFont("Arial", 12, DrawingFontStyles.Regular), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.DarkRed, 2), new GeoPen(GeoColor.StandardColors.DarkBlue, 2));
            lineStylePen2 = new LineStyle(new GeoPen(GeoColor.FromArgb(150, GeoColor.StandardColors.Blue), 2));
            areaStyleBrush2 = new AreaStyle(new GeoSolidBrush(GeoColor.StandardColors.Blue));
            Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = null;
            Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = null;
            Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = null;
            Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.DefaultTextStyle = null;
            Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Clear();
            Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customDistancePolygonStyleText);
            Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customDistanceLineStyleText2);
            Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(lineStylePen2);
            Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(areaStyleBrush2);
            Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            Map.TrackOverlay.TrackEnded += new EventHandler<TrackEndedTrackInteractiveOverlayEventArgs>(TrackOverlay_TrackEnded);
            Map.TrackOverlay.TrackMode = TrackMode.Line;
            previousCursor = Map.Cursor;
            Map.Cursor = Cursors.Pen;
        }

        private void TrackOverlay_TrackEnded(object sender, TrackEndedTrackInteractiveOverlayEventArgs e) {
            Map.TrackOverlay.TrackEnded -= new EventHandler<TrackEndedTrackInteractiveOverlayEventArgs>(TrackOverlay_TrackEnded);
            Map.TrackOverlay.TrackMode = TrackMode.None;
            Map.Cursor = previousCursor;
            SaveMeasureLayer();
            measuretool = false; ;
        }

        internal void SaveMeasureLayer() {
            try {
                var features = Map.TrackOverlay.TrackShapeLayer.InternalFeatures;
                foreach (Feature feat in features) {
                    var shape = feat.GetShape();
                    PointShape centerpoint = shape.GetCenterPoint();
                    BaseShape bs1 = DisplayMeasureBalloon(shape);
                    BaseShape bs2 = projection.ConvertToInternalProjection(bs1);
                    measureLayer2.InternalFeatures.Add(new Feature(bs2));
                    double areaTotal = 0;
                    double perimeterTotal = 0;
                    string displayerText = string.Empty;
                    if (bs2 is PolygonShape) {
                        PolygonShape poly1 = bs2 as PolygonShape;
                        areaTotal = poly1.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
                        perimeterTotal = poly1.GetPerimeter(GeographyUnit.DecimalDegree, mapDistanceUnit);
                        Measure perimetermeasure = MapUnitFactory.LengthConversion(mapDistanceUnit.ToString(), perimeterTotal);
                        Measure areameasure = MapUnitFactory.AreaConversion(mapAreaUnit.ToString(), areaTotal);
                        displayerText = $"{Strings.Area_Text}: " + areameasure.FullDisplay + $"\r\n{Strings.Perimeter_Text}: " + perimetermeasure.FullDisplay;
                    }
                    if (bs2 is LineShape) {
                        LineShape line1 = bs2 as LineShape;
                        perimeterTotal = line1.GetLength(GeographyUnit.DecimalDegree, mapDistanceUnit);
                        Measure perimetermeasure = MapUnitFactory.LengthConversion(mapDistanceUnit.ToString(), perimeterTotal);
                        displayerText = $"{Strings.Length_Text}: " + perimetermeasure.FullDisplay;
                        LineShape lineshape2 = shape as LineShape;
                        for (int xyz = 0; xyz <= (lineshape2.Vertices.Count / 2); xyz++) {
                            centerpoint = new PointShape(lineshape2.Vertices[xyz]);
                        }
                    }
                    Popup popup = new Popup(centerpoint);
                    popup.Loaded += new System.Windows.RoutedEventHandler(popup_Loaded);
                    popup.MouseDoubleClick += new MouseButtonEventHandler(mouse_DoubleClick);
                    System.Windows.Controls.TextBox displayer = new System.Windows.Controls.TextBox();
                    displayer.Text = displayerText;
                    popup.Content = displayer;
                    //popup.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(255, 0, 0));
                    popupOverlay.Popups.Add(popup);
                    popupOverlay.Refresh();
                }
                Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Clear();
                Map.Refresh();
            }
            catch (Exception ex) {
                log.Error("Error saving measure line - {0}", ex);
                System.Windows.MessageBox.Show(Strings.ThereWasAProblemSavingAMeasureLine_Text);
            }
        }
 

        void popup_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            Popup p = sender as Popup;
        }

        void mouse_DoubleClick(object sender, MouseButtonEventArgs e) {
            Popup p = sender as Popup;
            if (popupOverlay.Popups.Contains(p)) {
                popupOverlay.Popups.Remove(p);
                popupOverlay.Refresh();
            }
        }

        private BaseShape DisplayMeasureBalloon(BaseShape shape) {
            if (shape is LineBaseShape) {
                if (shape is LineShape) {
                    LineShape lineshape = shape as LineShape;
                    var firstvert = lineshape.Vertices.First();
                    var lastvert = lineshape.Vertices.Last();
                    var allverts = from vt in lineshape.Vertices
                                   select vt;

                    PointShape point1 = new PointShape(firstvert);
                    PointShape point2 = new PointShape(lastvert);
                    if (lineshape.Vertices.Count > 2) {
                        ScreenPointF screenPoint1 = ExtentHelper.ToScreenCoordinate(Map.CurrentExtent, point1, System.Convert.ToSingle(Map.ActualWidth), System.Convert.ToSingle(Map.ActualHeight));
                        ScreenPointF screenPoint2 = ExtentHelper.ToScreenCoordinate(Map.CurrentExtent, point2, System.Convert.ToSingle(Map.ActualWidth), System.Convert.ToSingle(Map.ActualHeight));
                        double distance = Math.Sqrt(((screenPoint1.X - screenPoint2.X) * (screenPoint1.X - screenPoint2.X)) + ((screenPoint1.Y - screenPoint2.Y) * (screenPoint1.Y - screenPoint2.Y)));
                        if (distance >= -10 && distance <= 10) {
                            PolygonShape polygon = new PolygonShape();
                            Collection<Vertex> collectvertex = lineshape.Vertices;
                            collectvertex.Add(lineshape.Vertices[0]);
                            RingShape rs = new RingShape(collectvertex);
                            polygon.OuterRing = rs;
                            return polygon;
                        }
                    }
                    return lineshape;
                }
                if (shape is MultilineShape) {
                    MultilineShape mlshape = shape as MultilineShape;
                    var firstvert = mlshape.Lines.First().Vertices.First();
                    var lastvert = mlshape.Lines.Last().Vertices.Last();
                    var allverts = from ml in mlshape.Lines
                                   from vt in ml.Vertices
                                   select vt;

                    PointShape point1 = new PointShape(firstvert);
                    PointShape point2 = new PointShape(lastvert);
                    if (allverts.Count() > 2) {
                        ScreenPointF screenPoint1 = ExtentHelper.ToScreenCoordinate(Map.CurrentExtent, point1, System.Convert.ToSingle(Map.ActualWidth), System.Convert.ToSingle(Map.ActualHeight));
                        ScreenPointF screenPoint2 = ExtentHelper.ToScreenCoordinate(Map.CurrentExtent, point2, System.Convert.ToSingle(Map.ActualWidth), System.Convert.ToSingle(Map.ActualHeight));
                        double distance = Math.Sqrt(((screenPoint1.X - screenPoint2.X) * (screenPoint1.X - screenPoint2.X)) + ((screenPoint1.Y - screenPoint2.Y) * (screenPoint1.Y - screenPoint2.Y)));
                        if (distance >= -10 && distance <= 10) {
                            PolygonShape polygon = new PolygonShape();
                            polygon.OuterRing = new RingShape(allverts);
                            return polygon;
                        }
                    }
                    return mlshape;
                }
            }
            return null;
        }

        private double GetAngleFromTwoVertices(Vertex fromVertex, Vertex toVertex) {
            double alpha = 0;
            if (fromVertex.X != toVertex.X) {
                double tangentAlpha = (fromVertex.Y - toVertex.Y) / (toVertex.X - fromVertex.X);
                alpha = Math.Atan(tangentAlpha) * 180 / Math.PI;
                if (alpha < 0) { alpha += 360; }
            }
            else {
                alpha = (fromVertex.Y > toVertex.Y) ? 90 : 270;
            }
            return alpha;
        }

        private double GetReverseAngle(double Angle) {
            double result = Angle + 180;
            if (result > 360) {
                result = result - 360;
            }
            return result;
        }



        private MultipolygonShape CalculateBorderRows(MultipolygonShape mps, double borderrowwidth, PointShape targetpoint1, PointShape targetpoint2, PointShape targetpoint3)
        {
            borderrowtool = false;
            double borderrowwidth_0 = borderrowwidth;
            //double borderrowwidth_8 = borderrowwidth * .8;
            double borderrowwidthx2 = borderrowwidth * 2.0;

            try
            {
                MultipolygonShape correctedpolygon = mps.Buffer(0, Map.MapUnit, MapUnitFactory.AreaDistanceMeasureConversion(Map.MapUnit));
                mps = correctedpolygon;
                MultipolygonShape newmultipoly = new MultipolygonShape();

                //MultilineShape multiLineShape = mps.Polygons[0].ToMultiLineShape();
                PolygonShape crossingpoly = FindPolygon(mps, targetpoint1, targetpoint2, targetpoint3);
                MultilineShape multiLineShapePoly = crossingpoly.ToMultiLineShape();

                MultilineShape line1 = multiLineShapePoly.GetShortestLineTo(targetpoint1, GeographyUnit.DecimalDegree);
                MultilineShape line2 = multiLineShapePoly.GetShortestLineTo(targetpoint2, GeographyUnit.DecimalDegree);
                MultilineShape line3 = multiLineShapePoly.GetShortestLineTo(targetpoint3, GeographyUnit.DecimalDegree);

                PointShape p101 = new PointShape(line1.Lines[0].Vertices[0]);
                PointShape p102 = new PointShape(line1.Lines[0].Vertices[line1.Lines[0].Vertices.Count - 1]);
                double distance101 = targetpoint1.GetDistanceTo(multiLineShapePoly, GeographyUnit.DecimalDegree, mapDistanceUnit);
                double distance102 = p101.GetDistanceTo(multiLineShapePoly, GeographyUnit.DecimalDegree, mapDistanceUnit);
                double distance103 = p102.GetDistanceTo(multiLineShapePoly, GeographyUnit.DecimalDegree, mapDistanceUnit);

                PointShape p201 = new PointShape(line2.Lines[0].Vertices[0]);
                PointShape p202 = new PointShape(line2.Lines[0].Vertices[line1.Lines[0].Vertices.Count - 1]);
                double distance201 = targetpoint1.GetDistanceTo(multiLineShapePoly, GeographyUnit.DecimalDegree, mapDistanceUnit);
                double distance202 = p201.GetDistanceTo(multiLineShapePoly, GeographyUnit.DecimalDegree, mapDistanceUnit);
                double distance203 = p202.GetDistanceTo(multiLineShapePoly, GeographyUnit.DecimalDegree, mapDistanceUnit);


                PointShape p301 = new PointShape(line3.Lines[0].Vertices[0]);
                PointShape p302 = new PointShape(line3.Lines[0].Vertices[line1.Lines[0].Vertices.Count - 1]);
                double distance301 = targetpoint3.GetDistanceTo(multiLineShapePoly, GeographyUnit.DecimalDegree, mapDistanceUnit);
                double distance302 = p301.GetDistanceTo(multiLineShapePoly, GeographyUnit.DecimalDegree, mapDistanceUnit);
                double distance303 = p302.GetDistanceTo(multiLineShapePoly, GeographyUnit.DecimalDegree, mapDistanceUnit);

                PointShape point000001 = p101;
                if(distance103 == 0)
                {
                    point000001 = p102;
                }

                PointShape point000002 = p201;
                if (distance203 == 0)
                {
                    point000002 = p202;
                }

                PointShape point000003 = p301;
                if (distance303 == 0)
                {
                    point000003 = p302;
                }

                MultilineShape multiLineShapeBig = new MultilineShape();
                LineShape lineshapeBig = new LineShape();
                for(int x = 1; x < multiLineShapePoly.Lines[0].Vertices.Count; x++)
                {
                    lineshapeBig.Vertices.Add(multiLineShapePoly.Lines[0].Vertices[x - 1]);

                    LineShape testline = new LineShape();
                    testline.Vertices.Add(multiLineShapePoly.Lines[0].Vertices[x - 1]);
                    testline.Vertices.Add(multiLineShapePoly.Lines[0].Vertices[x]);

                    MultilineShape line0001 = multiLineShapePoly.GetShortestLineTo(point000001, GeographyUnit.DecimalDegree);
                    MultilineShape line0002 = multiLineShapePoly.GetShortestLineTo(point000002, GeographyUnit.DecimalDegree);
                    MultilineShape line0003 = multiLineShapePoly.GetShortestLineTo(point000003, GeographyUnit.DecimalDegree);

                    double distance30001 = point000001.GetDistanceTo(testline, GeographyUnit.DecimalDegree, mapDistanceUnit);
                    double distance30002 = point000002.GetDistanceTo(testline, GeographyUnit.DecimalDegree, mapDistanceUnit);
                    double distance30003 = point000003.GetDistanceTo(testline, GeographyUnit.DecimalDegree, mapDistanceUnit);

                    if (distance30001 == 0)
                    {
                        lineshapeBig.Vertices.Add(new Vertex(point000001));
                    }
                    if (distance30002 == 0)
                    {
                        lineshapeBig.Vertices.Add(new Vertex(point000002));
                    }

                    if (distance30003 == 0)
                    {
                        lineshapeBig.Vertices.Add(new Vertex(point000003));
                    }
                }
                var count111 = multiLineShapePoly.Lines[0].Vertices.Count - 1;
                lineshapeBig.Vertices.Add(multiLineShapePoly.Lines[0].Vertices[count111]);
                multiLineShapeBig.Lines.Add(lineshapeBig);

                MultilineShape multiLineShape = multiLineShapeBig;

                //MultipointShape multipoint1 = multiLineShape.GetCrossing(line1);
                //MultipointShape multipoint2 = multiLineShape.GetCrossing(line2);
                //MultipointShape multipoint3 = multiLineShape.GetCrossing(line3);


                //PointShape point1 = new PointShape(line1.Lines[0].Vertices[0]);
                //PointShape point2 = new PointShape(line2.Lines[0].Vertices[0]);
                //PointShape point3 = new PointShape(line3.Lines[0].Vertices[0]);
                //PointShape point1 = multipoint1.Points[0];
                //PointShape point2 = multipoint2.Points[0];
                //PointShape point3 = multipoint3.Points[0];
                PointShape point1 = point000001;
                PointShape point2 = point000002;
                PointShape point3 = point000003;

                int verticescount = multiLineShape.Lines[0].Vertices.Count - 1;
                MultilineShape segmentMultiLineShape = new MultilineShape();

                MultilineShape segmentMultiLineShape001 = (MultilineShape)multiLineShape.GetLineOnALine(new PointShape(multiLineShape.Lines[0].Vertices[0]), point1);
                MultilineShape segmentMultiLineShape002 = (MultilineShape)multiLineShape.GetLineOnALine(new PointShape(multiLineShape.Lines[0].Vertices[0]), point2);
                MultilineShape segmentMultiLineShape003 = (MultilineShape)multiLineShape.GetLineOnALine(new PointShape(multiLineShape.Lines[0].Vertices[0]), point3);

                double distance001 = segmentMultiLineShape001.GetLength(GeographyUnit.DecimalDegree, mapDistanceUnit);
                double distance002 = segmentMultiLineShape002.GetLength(GeographyUnit.DecimalDegree, mapDistanceUnit);
                double distance003 = segmentMultiLineShape003.GetLength(GeographyUnit.DecimalDegree, mapDistanceUnit);

                //1   2   3
                //3   2   1
                //3   1   2
                //2   1   3
                //2   3   1
                //1   3   2

                if (distance001 < distance003)
                {
                    if (distance001 < distance002 && distance002 < distance003)
                    {
                        MultilineShape segmentMultiLineShape011 = (MultilineShape)multiLineShape.GetLineOnALine(point1, point3);
                        //segmentMultiLineShape011.Lines[0].Vertices.Insert(0, new Vertex(targetpoint1));
                        //segmentMultiLineShape011.Lines[0].Vertices.Add(new Vertex(targetpoint3));
                        //newmultipoly = segmentMultiLineShape011.Buffer(borderrowwidth_0, 8, BufferCapType.Square, GeographyUnit.DecimalDegree, DistanceUnit.Feet);
                        newmultipoly = BufferShape(borderrowwidth_0, segmentMultiLineShape011) as MultipolygonShape;
                        //segmentMultiLineShape = segmentMultiLineShape011;
                    }
                    else
                    {
                        MultilineShape segmentMultiLineShape012 = (MultilineShape)multiLineShape.GetLineOnALine(point1, point3);
                        //segmentMultiLineShape012.Lines[0].Vertices.Insert(0, new Vertex(targetpoint1));
                        //segmentMultiLineShape012.Lines[0].Vertices.Add(new Vertex(targetpoint3));
                        //MultipolygonShape newmultipoly2 = segmentMultiLineShape012.Buffer(borderrowwidthx2, 8, BufferCapType.Square, GeographyUnit.DecimalDegree, DistanceUnit.Feet);
                        //MultipolygonShape newmultipoly3 = multiLineShape.Buffer(borderrowwidth_0, 8, BufferCapType.Square, GeographyUnit.DecimalDegree, DistanceUnit.Feet);
                        MultipolygonShape newmultipoly2 = BufferShape(borderrowwidthx2, segmentMultiLineShape012) as MultipolygonShape;
                        MultipolygonShape newmultipoly3 = BufferShape(borderrowwidth_0, multiLineShape) as MultipolygonShape;
                        newmultipoly = newmultipoly3.GetDifference(newmultipoly2);
                    }
                }
                else
                {
                    if (distance001 > distance002 && distance002 > distance003)
                    {
                        MultilineShape segmentMultiLineShape011 = (MultilineShape)multiLineShape.GetLineOnALine(point3, point1);
                        //segmentMultiLineShape011.Lines[0].Vertices.Insert(0, new Vertex(targetpoint3));
                        //segmentMultiLineShape011.Lines[0].Vertices.Add(new Vertex(targetpoint1));
                        //newmultipoly = segmentMultiLineShape011.Buffer(borderrowwidth_0, 8, BufferCapType.Square, GeographyUnit.DecimalDegree, DistanceUnit.Feet);
                        newmultipoly = BufferShape(borderrowwidth_0, segmentMultiLineShape011) as MultipolygonShape;
                        //segmentMultiLineShape = segmentMultiLineShape011;
                    }
                    else
                    {
                        MultilineShape segmentMultiLineShape012 = (MultilineShape)multiLineShape.GetLineOnALine(point3, point1);
                        //segmentMultiLineShape012.Lines[0].Vertices.Insert(0, new Vertex(targetpoint3));
                        //segmentMultiLineShape012.Lines[0].Vertices.Add(new Vertex(targetpoint1));
                        //MultipolygonShape newmultipoly2 = segmentMultiLineShape012.Buffer(borderrowwidthx2, 8, BufferCapType.Square, GeographyUnit.DecimalDegree, DistanceUnit.Feet);
                        //MultipolygonShape newmultipoly3 = multiLineShape.Buffer(borderrowwidth_0, 8, BufferCapType.Square, GeographyUnit.DecimalDegree, DistanceUnit.Feet);
                        MultipolygonShape newmultipoly2 = BufferShape(borderrowwidthx2, segmentMultiLineShape012) as MultipolygonShape;
                        MultipolygonShape newmultipoly3 = BufferShape(borderrowwidth_0, multiLineShape) as MultipolygonShape;
                        newmultipoly = newmultipoly3.GetDifference(newmultipoly2);
                    }
                }

                MultipolygonShape diff1 = null;
                if (newmultipoly.Polygons.Count > 0)
                {
                    diff1 = new MultipolygonShape();
                    //diff1 = mps.GetIntersection(newmultipoly);
                    diff1 = newmultipoly;
                }
                return diff1;
            }
            catch (Exception ex3)
            {
                System.Windows.MessageBox.Show(Strings.ThereWasAProblemProcessingBorderRowCalculations_Text);
            }
            return null;
        }

        private static PolygonShape FindPolygon(MultipolygonShape mps, PointShape targetpoint1, PointShape targetpoint2, PointShape targetpoint3)
        {
            LineShape crossinglineshape = new LineShape();
            crossinglineshape.Vertices.Add(new Vertex(targetpoint1));
            crossinglineshape.Vertices.Add(new Vertex(targetpoint2));
            crossinglineshape.Vertices.Add(new Vertex(targetpoint3));
            PolygonShape crossingpoly = new PolygonShape();
            foreach (var cp in mps.Polygons)
            {
                if (cp.Contains(crossinglineshape))
                {
                    crossingpoly = cp;
                    break;
                }
                if (cp.Intersects(crossinglineshape))
                {
                    crossingpoly = cp;
                    break;
                }
                if (cp.Overlaps(crossinglineshape))
                {
                    crossingpoly = cp;
                    break;
                }
                if (cp.Crosses(crossinglineshape))
                {
                    crossingpoly = cp;
                    break;
                }
                if (cp.Touches(crossinglineshape))
                {
                    crossingpoly = cp;
                    break;
                }
            }

            return crossingpoly;
        }

        void Drawpivot()
        {
            log.Info("PivotTools - Draw pivot dimensions");
            if (radius == 0) { return; }
            if (pointx == 0) { return; }
            if (pointy == 0) { return; }
            if (pivotpoints == 0) { return; }
            PointShape centerpoint = new PointShape();
            centerpoint.X = pointx;
            centerpoint.Y = pointy;

            PointShape ps2 = centerpoint;
            EllipseShape es2 = new EllipseShape(ps2, (radius * .3048), GeographyUnit.DecimalDegree, DistanceUnit.Meter);
            PolygonShape newpoly2 = WeedPolygon(es2.ToPolygon(), pivotpoints);
            PolygonShape newpoly = projection.ConvertToExternalProjection(newpoly2) as PolygonShape;
            PolygonShape newpolyinternal = projection.ConvertToExternalProjection(newpoly2) as PolygonShape;
            selectedPivot = new Feature(newpolyinternal);
            double areacalc = newpoly2.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
            double areacalc2 = es2.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
            area = Math.Round(areacalc, 2);
            RaisePropertyChanged("Area");

            //if (!thiswasfromtracking && Map.EditOverlay.EditShapesLayer.InternalFeatures.Count > 0)
            //{
            //    Map.EditOverlay.EditShapesLayer.InternalFeatures.RemoveAt(Map.EditOverlay.EditShapesLayer.InternalFeatures.Count - 1);
            //}
            thiswasfromtracking = false;
            Map.EditOverlay.EditShapesLayer.InternalFeatures.Add(new Feature(newpoly));
            GeoCollection<Feature> cutlines = LoadCutLineShapes();
            SplitPolgonsWithLines(cutlines);
            Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Clear();
            Map.EditOverlay.CalculateAllControlPoints();
            Map.EditOverlay.Refresh();
            Map.Cursor = Cursors.Hand;
            Map.TrackOverlay.TrackMode = TrackMode.None;
            CommandDescription = Strings.EditCuttingShapes_Text;
            draggingfeatures = false;
            trackingfeatures = false;
            deletetool = false;
            borderrowtool = false;
            snipapolygon = false;
            drawingpivot = false;
            drawingpartialpivot = false;
            skipwhiledrawing = false;
            readytobuild = TestToBuild(cutWidth);
            Map.EditOverlay.CanAddVertex = true;
            Map.EditOverlay.CanDrag = true;
            Map.EditOverlay.CanRemoveVertex = true;
            //BuildOnePivot = false;
            Map.Refresh();
        }

        void Drawpartialpivot()
        {
            log.Info("PivotTools - Draw pivot dimensions");
            if (radius2 == 0) { return; }
            if (point2x == 0) { return; }
            if (point2y == 0) { return; }
            //if (angle1 == 0) { return; }
            //if (angle2 == 0) { return; }
            if (pivotpoints == 0) { return; }
            PointShape centerpoint = new PointShape();
            centerpoint.X = point2x;
            centerpoint.Y = point2y;

            PointShape ps2 = centerpoint;
            //PolygonShape newpoly2 = BuildPiePolygon(ps2, radius2, angle1, angle2);
            PolygonShape newpoly2 = BuildAPartialPivotCoverage(ps2, radius2, angle1, angle2);
            //EllipseShape es2 = new EllipseShape(ps2, (radius * .3048), GeographyUnit.DecimalDegree, DistanceUnit.Meter);
            //PolygonShape newpoly2 = WeedPolygon(es2.ToPolygon(), pivotpoints);
            PolygonShape newpoly = projection.ConvertToExternalProjection(newpoly2) as PolygonShape;
            PolygonShape newpolyinternal = projection.ConvertToExternalProjection(newpoly2) as PolygonShape;
            selectedPivot = new Feature(newpolyinternal);
            double areacalc = newpoly2.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
            //double areacalc2 = es2.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
            area = Math.Round(areacalc, 2);
            RaisePropertyChanged("Area");

            //if (!thiswasfromtracking && Map.EditOverlay.EditShapesLayer.InternalFeatures.Count > 0)
            //{
            //    Map.EditOverlay.EditShapesLayer.InternalFeatures.RemoveAt(Map.EditOverlay.EditShapesLayer.InternalFeatures.Count - 1);
            //}
            thiswasfromtracking = false;
            Map.EditOverlay.EditShapesLayer.InternalFeatures.Add(new Feature(newpoly));
            GeoCollection<Feature> cutlines = LoadCutLineShapes();
            SplitPolgonsWithLines(cutlines);
            Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Clear();
            Map.EditOverlay.CalculateAllControlPoints();
            Map.EditOverlay.Refresh();
            Map.Cursor = Cursors.Hand;
            Map.TrackOverlay.TrackMode = TrackMode.None;
            CommandDescription = Strings.EditCuttingShapes_Text;
            draggingfeatures = false;
            trackingfeatures = false;
            deletetool = false;
            borderrowtool = false;
            snipapolygon = false;
            drawingpivot = false;
            drawingpartialpivot = false;
            skipwhiledrawing = false;
            readytobuild = TestToBuild(cutWidth);
            Map.EditOverlay.CanAddVertex = true;
            Map.EditOverlay.CanDrag = true;
            Map.EditOverlay.CanRemoveVertex = true;
            //BuildOnePivot = false;
            Map.Refresh();
        }

        private PolygonShape WeedPolygon(PolygonShape weedpolygon, double pivotvertices)
        {
            Collection<Vertex> vertexcol = weedpolygon.OuterRing.Vertices;
            double vertexcount = vertexcol.Count;
            double vertexpercent = Math.Round(pivotvertices / vertexcount, 2) * 100;
            return WeedPolygonPercent(weedpolygon, vertexpercent);
        }

        private PolygonShape WeedPolygonPercent(PolygonShape weedpolygon, double pivotverticespertent)
        {
            Collection<Vertex> vertexcol = weedpolygon.OuterRing.Vertices;
            Collection<Vertex> vertexnew = new Collection<Vertex>();
            double vertexcount = vertexcol.Count;
            double vertexcountnew2 = Math.Truncate(vertexcount * (pivotverticespertent / 100));
            double vertexcountnew = Math.Truncate(vertexcount / vertexcountnew2);
            double i = 0;
            foreach (Vertex vert in vertexcol)
            {
                i++;
                if (i == 1)
                {
                    vertexnew.Add(vert);
                }
                if (i >= vertexcountnew)
                {
                    i = 0;
                }
            }
            RingShape ringshape = new RingShape(vertexnew);
            PolygonShape newpoly = new PolygonShape(ringshape);
            return newpoly;
        }

        private void DrawPivotFromAreaCalculations()
        {
            if (areaUnitString == "acre")
            {
                AreaMeasure acres = Acre.Self.GetMeasure(area) as AreaMeasure;
                Measure squaremeters = acres.CreateAs(AgC.UnitConversion.Area.SquareMeter.Self);
                double squaremetersDividedByPi = squaremeters.Value / Math.PI;
                double radiusmeters = Math.Sqrt(squaremetersDividedByPi) / multiplierconstant;
                LengthMeasure meters = Meter.Self.GetMeasure(radiusmeters) as LengthMeasure;
                Measure feet = meters.CreateAs(AgC.UnitConversion.Length.Foot.Self);
                radius = Math.Round(feet.Value, 2); ;
                Drawpivot();
            }
            else
            {
                AreaMeasure hectares = Acre.Self.GetMeasure(area) as AreaMeasure;
                Measure sqmeters = hectares.CreateAs(AgC.UnitConversion.Area.SquareMeter.Self);
                double areaDividedByPi = sqmeters.Value / Math.PI;
                double radiusm = Math.Sqrt(areaDividedByPi) / multiplierconstant;
                LengthMeasure meters = Meter.Self.GetMeasure(radiusm) as LengthMeasure;
                radius = Math.Round(meters.Value, 2);
                Drawpivot();
            }
        }

        private PolygonShape BuildPiePolygon(PointShape centerPointShape, double radius22, double startAngle, double sweepAngle)
        {
            PolygonShape polygonShape = new PolygonShape();
            EllipseShape ellipseShape = new EllipseShape(centerPointShape, radius22, GeographyUnit.DecimalDegree, mapDistanceUnit);

            PointShape pointShape1 = new PointShape(ellipseShape.Center.X, ellipseShape.Center.Y);
            pointShape1.TranslateByDegree(ellipseShape.Width, CorrectAngle(startAngle));
            polygonShape.OuterRing.Vertices.Add(new Vertex(ellipseShape.Center));
            polygonShape.OuterRing.Vertices.Add(new Vertex(pointShape1));

            if ((sweepAngle > 90) && (sweepAngle <= 180))
            {
                PointShape midPointShape1 = new PointShape(ellipseShape.Center.X, ellipseShape.Center.Y);
                midPointShape1.TranslateByDegree(ellipseShape.Width, CorrectAngle(startAngle + 90));
                polygonShape.OuterRing.Vertices.Add(new Vertex(midPointShape1));
            }
            else if ((sweepAngle > 180) && (sweepAngle <= 275))
            {
                PointShape midPointShape1 = new PointShape(ellipseShape.Center.X, ellipseShape.Center.Y);
                midPointShape1.TranslateByDegree(ellipseShape.Width, CorrectAngle(startAngle + 90));
                PointShape midPointShape2 = new PointShape(ellipseShape.Center.X, ellipseShape.Center.Y);
                midPointShape2.TranslateByDegree(ellipseShape.Width, CorrectAngle(startAngle + 180));

                polygonShape.OuterRing.Vertices.Add(new Vertex(midPointShape1));
                polygonShape.OuterRing.Vertices.Add(new Vertex(midPointShape2));

            }
            else if ((sweepAngle > 275) && (sweepAngle <= 360))
            {
                PointShape midPointShape1 = new PointShape(ellipseShape.Center.X, ellipseShape.Center.Y);
                midPointShape1.TranslateByDegree(ellipseShape.Width, CorrectAngle(startAngle + 90));
                PointShape midPointShape2 = new PointShape(ellipseShape.Center.X, ellipseShape.Center.Y);
                midPointShape2.TranslateByDegree(ellipseShape.Width, CorrectAngle(startAngle + 180));
                PointShape midPointShape3 = new PointShape(ellipseShape.Center.X, ellipseShape.Center.Y);
                midPointShape3.TranslateByDegree(ellipseShape.Width, CorrectAngle(startAngle + 275));

                polygonShape.OuterRing.Vertices.Add(new Vertex(midPointShape1));
                polygonShape.OuterRing.Vertices.Add(new Vertex(midPointShape2));
                polygonShape.OuterRing.Vertices.Add(new Vertex(midPointShape3));
            }

            PointShape pointShapeSweep = new PointShape(ellipseShape.Center.X, ellipseShape.Center.Y);
            pointShapeSweep.TranslateByDegree(ellipseShape.Width, CorrectAngle(startAngle + sweepAngle));
            polygonShape.OuterRing.Vertices.Add(new Vertex(pointShapeSweep));
            polygonShape.OuterRing.Vertices.Add(new Vertex(ellipseShape.Center));

            MultipolygonShape intersectionMultiPolygonShape = ellipseShape.GetIntersection(polygonShape);
            if (intersectionMultiPolygonShape.Polygons.Count == 1)
            {
                return intersectionMultiPolygonShape.Polygons[0];
            }
            return null;
            //return polygonShape; 
        }

        private double CorrectAngle(double angle)
        {
            double correctAngle;
            if (angle > 360)
                correctAngle = angle - 360;
            else
                correctAngle = angle;
            return correctAngle;

        }

        private PolygonShape BuildAPartialPivotCoverage(PointShape centerPoint, double radius, double start, double stop)
        {
            PolygonShape oPolygon = new PolygonShape();
            RingShape ringshape = new RingShape();
            double angle = 0;
            double startangle = start;
            double endangle = stop;
            if (start == 360)
            {
                stop = 0;
                startangle = start;
            }
            if (stop == 0 || stop == 360)
            {
                stop = 359.99;
                endangle = stop;
            }
            if (start > stop)
            {
                endangle = stop + 360;
            }
            double travelangle = endangle - startangle;
            double incrementangle = 5;
            double leftoverangle = travelangle;
            double currentangle = start;
            double angle360 = 0;
            ringshape.Vertices.Add(new Vertex(centerPoint));
            angle360 = start / 360;
            ringshape.Vertices.Add(new Vertex(FindPointByDistanceAndAngle(centerPoint, radius, angle360)));
            while (leftoverangle > 0)
            {
                leftoverangle = leftoverangle - incrementangle;
                currentangle = currentangle + incrementangle;
                if (leftoverangle <= 0) { break; }
                if (currentangle >= 360)
                {
                    currentangle = currentangle - 360;
                }
                angle360 = currentangle / 360;
                ringshape.Vertices.Add(new Vertex(FindPointByDistanceAndAngle(centerPoint, radius, angle360)));

            }
            angle360 = stop / 360;
            ringshape.Vertices.Add(new Vertex(FindPointByDistanceAndAngle(centerPoint, radius, angle360)));
            oPolygon.OuterRing = ringshape;
            return oPolygon;
        }

        private PointShape FindPointByDistanceAndAngle(PointShape a, double radius, double angle)
        {
            PointShape point;
            point = new PointShape();
            double angle2 = 2 * Math.PI * angle;
            double aX = a.X;
            double aY = a.Y;
            point.X = radius * Math.Sin(angle2) + a.X;
            point.Y = radius * Math.Cos(angle2) + a.Y;
            double pointX = point.X;
            double pointY = point.Y;

            PointShape point33;
            point33 = new PointShape();
            point33.X = a.X;
            point33.Y = a.Y;
            double angle3 = angle * 360;
            point33.TranslateByDegree(radius, CorrectAngle(angle3), GeographyUnit.DecimalDegree, mapDistanceUnit);
            return point33;
        }

        private double FindAngleFrom2Points(PointShape a, PointShape b)
        {
            double radians = 0;
            radians = Math.Atan2((b.Y - a.Y), (b.X - a.X));
            radians = Math.Atan2((b.Y - a.Y), (b.X - a.X));
            return radians;
        }

        private double get_angle_between_in_radians(PointShape a, PointShape b)
        {
            double dotproduct;
            double lengtha;
            double lengthb;
            double result;

            dotproduct = (a.X * b.X) + (a.Y * b.Y);
            lengtha = Math.Sqrt(a.X * a.X + a.Y * a.Y);
            lengthb = Math.Sqrt(b.X * b.X + b.Y * b.Y);

            result = Math.Acos(dotproduct / (lengtha * lengthb));

            if (dotproduct < 0)
            {
                if (result > 0)
                    result += Math.PI;
                else
                    result -= Math.PI;
            }
            return result;
        }

    }
}
