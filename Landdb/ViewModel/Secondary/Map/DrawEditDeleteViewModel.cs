﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using AgC.UnitConversion;
using AgC.UnitConversion.Area;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.MapStyles;
using Landdb.ViewModel.Fields;
using ThinkGeo.MapSuite.Core;
using ThinkGeo.MapSuite.WpfDesktopEdition;
using System.Reactive;
using System.Reactive.Linq;
using System.Reactive.Disposables;
using Landdb.ViewModel.Maps.ImageryOverlay;
using Landdb.ViewModel.Fields.FieldDetails.MapModels;
using Landdb.Domain.ReadModels.Map;
using System.Windows.Shapes;
using Landdb.Client.Spatial;
using Landdb.MapStyles.Overlays;
using Landdb.Resources;
using NLog;

namespace Landdb.ViewModel.Secondary.Map {
    public class DrawEditDeleteViewModel : ViewModelBase
    {
        Logger log = LogManager.GetCurrentClassLogger();
        //IMapOverlay selectedoverlay;
        IClientEndpoint clientEndpoint;
        Dispatcher dispatcher;
        bool hasunsavedchanges = false;
        bool readytofinish = false;
        bool readytoassign = false;

        float measurefontsize = 8;
        float measureareafontsize = 8;
        InMemoryFeatureLayer currentLayer = new InMemoryFeatureLayer();
        InMemoryFeatureLayer zoneLayer = new InMemoryFeatureLayer();
        InMemoryFeatureLayer labelLayer = new InMemoryFeatureLayer();
        InMemoryFeatureLayer selectLayer = new InMemoryFeatureLayer();
        InMemoryFeatureLayer measureLayer = new InMemoryFeatureLayer();
        InMemoryFeatureLayer dimensionLayer = new InMemoryFeatureLayer();
        InMemoryFeatureLayer spatialQueryResultLayer = new InMemoryFeatureLayer();

        LayerOverlay layerOverlay = new LayerOverlay();
        LayerOverlay zoneOverlay = new LayerOverlay();
        LayerOverlay selectOverlay = new LayerOverlay();
        PopupOverlay popupOverlay = new PopupOverlay();
        LayerOverlay dimensionOverlay = new LayerOverlay();
        Proj4Projection projection;
        bool maploaded = false;
        bool startedOutOnline = false;
        bool skipwhilemousedown = false;
        bool skipwhiledrawing = false;
        bool draggingfeatures = false;
        bool removingvertex = false;
        bool trackingfeatures = false;
        bool addingvertex = false;
        bool calculatingdimensions = false;
        bool drawingpivot = false;
        bool ismeasureline = false;

        string fieldname = string.Empty;
        string commandDescription = "                 ";
        RectangleShape currentmapextent = new RectangleShape();
        MultipolygonShape compiledpoly = new MultipolygonShape();
        MultipolygonShape savededitoverlaypoly = new MultipolygonShape();
        IIdentity selectedIdentity;
        Collection<Feature> spatialQueryResults = new Collection<Feature>();
        Feature selectedfield = new Feature();
        Feature loadedfeature = new Feature();
        BaseShape fieldshape = new MultipolygonShape();
        RectangleShape previousmapextent = new RectangleShape();
        Cursor previousCursor;
        string modeofoperation = Strings.DrawingMode_Text;
        int operatingmode = 0;
        bool isAssigningMode = false;
        Vertex previousvertex = new Vertex();
        float prevx = 0;
        float prevy = 0;
        string labelcolumn = Strings.Label_Text;
        string namecolumn = Strings.Name_Text;
        string idcolumn = Strings.Id_Text;
        string areacolumn = Strings.Area_Text;
        int countmoving = 0;
        int countmoved = 0;
        //bool IsMeasureLine = false;
        //CustomDistanceHaloLineStyle
        MapStyles.CustomDistanceLineStyle dimensionDistanceLineStyleText = new MapStyles.CustomDistanceLineStyle(DistanceUnit.Feet, new GeoFont("Arial", 16, DrawingFontStyles.Regular), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.DarkRed, 2), new GeoPen(GeoColor.StandardColors.DarkBlue, 2));
        //MapStyles.CustomDistanceHaloLineStyle dimensionDistanceLineStyleText = new MapStyles.CustomDistanceHaloLineStyle(DistanceUnit.Feet, new GeoFont("Arial", 16, DrawingFontStyles.Regular), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.White));
        MapStyles.CustomAreaPolygonStyle dimensionDistancePolygonStyleText = new MapStyles.CustomAreaPolygonStyle(ThinkGeo.MapSuite.Core.AreaUnit.Acres, new GeoFont("Arial", 16, DrawingFontStyles.Regular), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.White), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.DarkRed));
        MapStyles.CustomAreaPolygonStyle customDistancePolygonStyleText = new MapStyles.CustomAreaPolygonStyle(ThinkGeo.MapSuite.Core.AreaUnit.Acres, new GeoFont("Arial", 16, DrawingFontStyles.Regular), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.White), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.DarkRed));
        //MapStyles.CustomDistanceLineStyle customDistanceLineStyleText = new MapStyles.CustomDistanceLineStyle(DistanceUnit.Feet, new GeoFont("Arial", 16, DrawingFontStyles.Regular), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.DarkRed, 2), new GeoPen(GeoColor.StandardColors.DarkBlue, 2));
        MapStyles.CustomDistanceHaloLineStyle customDistanceLineStyleText = new MapStyles.CustomDistanceHaloLineStyle(DistanceUnit.Feet, new GeoFont("Arial", 14, DrawingFontStyles.Bold), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.Black, 3), new GeoPen(GeoColor.StandardColors.DarkRed, 3), new GeoPen(GeoColor.StandardColors.Blue, 3));
        //LineStyle lineStylePen = new LineStyle(new GeoPen(GeoColor.FromArgb(150, GeoColor.StandardColors.Orange), 4));
        LineStyle lineStylePen = new LineStyle(new GeoPen(GeoColor.FromArgb(150, GeoColor.StandardColors.Blue), 2));
        AreaStyle areaStyleBrush = new AreaStyle(new GeoSolidBrush(GeoColor.StandardColors.Blue));
        MapStyles.CustomAreaPolygonHaloStyle customAreaPolygonStyleText = new MapStyles.CustomAreaPolygonHaloStyle(ThinkGeo.MapSuite.Core.AreaUnit.Acres, new GeoFont("Arial", 16, DrawingFontStyles.Bold), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.White), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.DarkRed));

        //CreateTreeItemMapCZAssignerViewModel createItemViewModel;
        int assignedFeatureIndex = 0;
        bool featureFound = false;
        CropzoneAreaStyle fieldsLayerAreaStyle = null;
        ScalingTextStyle scalingTextStyle = new ScalingTextStyle(GeographyUnit.DecimalDegree, @"Label", 19000, 1200, 24, 2);
        private Dictionary<string, double> rotationAngles = new Dictionary<string, double>();
        private Dictionary<string, PointShape> labelPositions = new Dictionary<string, PointShape>();
        //IList<IDomainCommand> commandlist = new List<IDomainCommand>();
        private CropzoneCreationViewModel selectedCropZone;
        private ObservableCollection<CropzoneCreationViewModel> selectedCropZones = new ObservableCollection<CropzoneCreationViewModel>();
        //private MultipointShape boundarypoints = new MultipointShape();
        //private ObservableCollection<PointShape> boundarypoints = new ObservableCollection<PointShape>();
        UserSettings settings;
        string areaUnitString = string.Empty;
        string coordinateformat = string.Empty;
        AgC.UnitConversion.Area.AreaUnit agcAreaUnit;
        ThinkGeo.MapSuite.Core.AreaUnit mapAreaUnit;
        ThinkGeo.MapSuite.Core.DistanceUnit mapDistanceUnit;
        int fieldopacity = 125;
        int annotationopacity = 125;
        MapSettings mapsettings;
        bool sortAlphabetically = false;
        ZoomLevelFactory zoomlevelfactory = new ZoomLevelFactory();

        public DrawEditDeleteViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher)
        {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;

            settings = clientEndpoint.GetUserSettings();
            sortAlphabetically = settings.AreFieldsSortedAlphabetically;
            InitializeSettings();
            scalingTextStyle = new ScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, mapsettings.ScalingTextMaximumSize, this.mapsettings.ScalingTextMinimumSize);
            customDistancePolygonStyleText = new MapStyles.CustomAreaPolygonStyle(mapAreaUnit, new GeoFont("Arial", 16, DrawingFontStyles.Regular), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.White), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.DarkRed));
            customDistancePolygonStyleText.YOffsetInPixel = -20;
            //CustomDistanceHaloLineStyle
            //customDistanceLineStyleText = new MapStyles.CustomDistanceLineStyle(mapDistanceUnit, new GeoFont("Arial", measurefontsize, DrawingFontStyles.Regular), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.White));
            //customDistanceLineStyleText = new MapStyles.CustomDistanceHaloLineStyle(mapDistanceUnit, new GeoFont("Arial", measurefontsize, DrawingFontStyles.Regular), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.White));
            customDistanceLineStyleText = new MapStyles.CustomDistanceHaloLineStyle(mapDistanceUnit, new GeoFont("Arial", 14, DrawingFontStyles.Bold), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.Black, 3), new GeoPen(GeoColor.StandardColors.DarkRed, 3), new GeoPen(GeoColor.StandardColors.Blue, 3));
            areaStyleBrush = new AreaStyle(new GeoPen(new GeoSolidBrush(new GeoColor(150, GeoColor.StandardColors.Yellow)), 3), new GeoSolidBrush(new GeoColor(50, GeoColor.StandardColors.Blue)));
            lineStylePen = new LineStyle(new GeoPen(GeoColor.FromArgb(150, GeoColor.StandardColors.Blue), 2));

            dimensionDistancePolygonStyleText = new MapStyles.CustomAreaPolygonStyle(mapAreaUnit, new GeoFont("Arial", 16, DrawingFontStyles.Regular), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.White), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.DarkRed));
            dimensionDistancePolygonStyleText.YOffsetInPixel = -20;
            dimensionDistanceLineStyleText = new MapStyles.CustomDistanceLineStyle(mapDistanceUnit, new GeoFont("Arial", 16, DrawingFontStyles.Regular), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.DarkRed, 2), new GeoPen(GeoColor.StandardColors.DarkBlue, 2));
            //dimensionDistanceLineStyleText = new MapStyles.CustomDistanceHaloLineStyle(mapDistanceUnit, new GeoFont("Arial", 16, DrawingFontStyles.Regular), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.White));
            dimensionDistanceLineStyleText.DisplayAngles = false;
            dimensionDistanceLineStyleText.DisplayTotalLength = false;
            customAreaPolygonStyleText = new MapStyles.CustomAreaPolygonHaloStyle(mapAreaUnit, new GeoFont("Arial", 16, DrawingFontStyles.Bold), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.White), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.DarkRed));

            operatingmode = 0;

            //var locator = System.Windows.Application.Current.TryFindResource("Locator") as ViewModelLocator;
            //selectedoverlay = locator.ThinkGeoUtitlityVM.GetSelectedMapFromFactory("Bing");
            //selectedoverlay = new BingMapOverlay();

            InitializeMap();
            //InitializeViewModels();
            InitializeCommands();
        }

        private void InitializeSettings() {
            this.mapsettings = this.clientEndpoint.GetMapSettings();
            fieldopacity = mapsettings.FieldsOpacity;
            annotationopacity = mapsettings.MapAnnotationOpacity;
            areaUnitString = mapsettings.MapAreaUnit;
            coordinateformat = mapsettings.DisplayCoordinateFormat;
            agcAreaUnit = UnitFactory.GetUnitByName(areaUnitString) as AgC.UnitConversion.Area.AreaUnit;
            mapAreaUnit = MapUnitFactory.AreaMeasureConversion(agcAreaUnit);
            mapDistanceUnit = MapUnitFactory.AreaDistanceMeasureConversion(agcAreaUnit);
        }

        //private void InitializeViewModels()
        //{
        //    selectedCropZone = new CropzoneCreationViewModel(null, "", "", "", "", null);
        //    selectedCropZones = new ObservableCollection<CropzoneCreationViewModel>();
        //    //createItemViewModel = new CreateTreeItemMapCZAssignerViewModel(this.clientEndpoint, OnNewItemCreated, OnNewItemIgnored) { CurrentCropYear = ApplicationEnvironment.CurrentCropYear, CurrentDataSourceId = ApplicationEnvironment.CurrentDataSourceId };
        //}

        private void InitializeCommands()
        {
            DrawShapeCommand = new RelayCommand(HandleDrawAndEditPolygon);
            //DrawShapeCommand = new RelayCommand(DrawPolygon);
            EditShapeCommand = new RelayCommand(HandleShapeEdit);
            SubtractShapeCommand = new RelayCommand(HandleSubractShape);
            ResetCommand = new RelayCommand(ClearShape);
            //DeleteShapeCommand = new RelayCommand(HandleDeleteShape);
            MeasureCommand = new RelayCommand(HandleMeasureLine);
            SaveCropzoneCommand = new RelayCommand(SavePolygon);
            ExitCropzoneCommand = new RelayCommand(Cancel);
            CancelCommand = new RelayCommand(Cancel);
        }

        public ICommand SaveCropzoneCommand { get; private set; }
        public ICommand ExitCropzoneCommand { get; private set; }
        public ICommand DrawShapeCommand { get; private set; }
        public ICommand EditShapeCommand { get; private set; }
        public ICommand SubtractShapeCommand { get; private set; }
        public ICommand ResetCommand { get; private set; }
        //public ICommand DeleteShapeCommand { get; private set; }
        public ICommand MeasureCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }
        public ObservableCollection<CropZoneTreeItemViewModel> CropZoneModels { get; private set; }
        public FieldTreeItemViewModel FieldTreeItemVM { get; private set; }

        public WpfMap Map { get; set; }

        public IIdentity SelectedIdentity {
            get { return selectedIdentity; }
            set {
                selectedIdentity = value;
                RaisePropertyChanged("SelectedIdentity");
            }
        }

        public Collection<Feature> SpatialQueryResults {
            get { return spatialQueryResults; }
            set {
                spatialQueryResults = value;
                RaisePropertyChanged("SpatialQueryResults");
            }
        }

        public Feature SelectedField {
            get { return selectedfield; }
            set {
                if (selectedfield == value) { return; }
                selectedfield = value;
                if (maploaded) {
                    ResetPivot();
                    OpenAndLoadaShapefile();
                    LoadShapeHistory();
                }
                RaisePropertyChanged("SelectedField");
            }
        }

        public RectangleShape CurrentMapExtent {
            get { return currentmapextent; }
            set {
                if (currentmapextent == value) { return; }
                currentmapextent = value;
                RaisePropertyChanged("CurrentMapExtent");
                if (maploaded) {
                    ShapeValidationResult svr = currentmapextent.Validate(ShapeValidationMode.Simple);
                    if (svr.IsValid) {
                        Map.CurrentExtent = currentmapextent;
                        Map.Refresh();
                    }
                }
            }
        }

        public void ResetPivot() {
            if (!maploaded) { return; }
            Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Clear();
            Map.TrackOverlay.Refresh();
            Map.EditOverlay.EditShapesLayer.InternalFeatures.Clear();
            Map.EditOverlay.CalculateAllControlPoints();
            Map.EditOverlay.Refresh();

            if (Map.Overlays.Contains("importOverlay")) {
                if (layerOverlay.Layers.Contains("measureLayer")) {
                    measureLayer.InternalFeatures.Clear();
                    popupOverlay.Popups.Clear();
                    Map.Refresh();
                }
            }
        }

        public ObservableCollection<CropzoneCreationViewModel> SelectedCropZones
        {
            get { return selectedCropZones; }
            set
            {
                if (selectedCropZones == value) { return; }
                selectedCropZones = value;
            }
        }

        public CropzoneCreationViewModel SelectedCropZone
        {
            get { return selectedCropZone; }
            set
            {
                if (selectedCropZone == value) { return; }
                selectedCropZone = value;
                currentLayer.InternalFeatures.Clear();
                if (selectedCropZone != null && !string.IsNullOrEmpty(selectedCropZone.MapData)) {
                    if (!selectOverlay.Layers.Contains("selectLayer")) {
                        selectOverlay.Layers.Add("selectLayer", selectLayer);
                    }
                    try {
                        currentLayer.InternalFeatures.Add(new Feature(selectedCropZone.MapData));
                        var shapelayerfeatures = currentLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);
                        if (shapelayerfeatures.Count > 0) {
                            RectangleShape extent = ExtentHelper.GetBoundingBoxOfItems(shapelayerfeatures);
                            extent.ScaleUp(10);
                            Map.CurrentExtent = extent;
                            //PointShape center = extent.GetCenterPoint();
                            //Map.CenterAt(center);
                        }
                    }
                    catch (Exception ex) {
                        log.ErrorException("Caught an exception while working with a selected cropzone", ex);
                        //System.Windows.MessageBox.Show("ImportTools - Caught an exception while working with a simple draw shape.");
                    }

                }
                scalingTextStyle.MapScale = Map.CurrentScale;
                Map.Refresh();
                RaisePropertyChanged("SelectedCropZone");

            }
        }

        public string CommandDescription {
            get { return commandDescription; }
            set {
                if (commandDescription == value) { return; }
                commandDescription = value;
                RaisePropertyChanged("CommandDescription");
            }
        }

        public string FieldName {
            get { return fieldname; }
            set {
                if (fieldname == value) { return; }
                fieldname = value;
                RaisePropertyChanged("FieldName");
            }
        }

        //public object DetailsViewModel
        //{
        //    get { return detailsViewModel; }
        //    set
        //    {
        //        detailsViewModel = value;
        //        LoadMapForItem(value);
        //        RaisePropertyChanged("DetailsViewModel");
        //    }
        //}

        public bool HasUnsavedChanges
        {
            get { return hasunsavedchanges; }
            set
            {
                if (hasunsavedchanges == value) { return; }
                hasunsavedchanges = value;
                RaisePropertyChanged("HasUnsavedChanges");
            }
        }

        public bool ReadyToFinish
        {
            get { return readytofinish; }
            set
            {
                if (readytofinish == value) { return; }
                readytofinish = value;
                RaisePropertyChanged("ReadyToFinish");
            }
        }

        public bool IsAssigningMode
        {
            get { return isAssigningMode; }
            set
            {
                if (isAssigningMode == value) { return; }
                isAssigningMode = value;
                RaisePropertyChanged("IsAssigningMode");
                RaisePropertyChanged("IsDrawingMode");
            }
        }

        public bool IsDrawingMode
        {
            get { return (!isAssigningMode && readytoassign); }
        }

        public RectangleShape PreviousMapExtent
        {
            get { return previousmapextent; }
            set { previousmapextent = value; }
        }

        private string displaycoordinates = string.Empty;
        public string DisplayCoordinates {
            get { return displaycoordinates; }
            set {
                displaycoordinates = value;
                RaisePropertyChanged("DisplayCoordinates");
            }
        }

        //public IMapOverlay SelectedOverlay {
        //    get { return selectedoverlay; }
        //    set {
        //        if (selectedoverlay == value) { return; }
        //        if (selectedoverlay == null) { return; }
        //        if (string.IsNullOrWhiteSpace(value.Name)) { return; }
        //        selectedoverlay = value;
        //        //LoadBingMapsOverlay();
        //        //Map.Refresh();
        //        RaisePropertyChanged("SelectedOverlay");
        //    }
        //}

        public void SetFieldTreeItemVM(FieldTreeItemViewModel fieldtreeitemvm)
        {
            if (fieldtreeitemvm == null)
            {
                FieldTreeItemVM = null;
            }
            else
            {
                FieldTreeItemVM = fieldtreeitemvm;
            }
            RaisePropertyChanged("FieldTreeItemVM");
        }

        public void ReSetMap()
        {
            HasUnsavedChanges = false;
            isAssigningMode = false;
            readytofinish = false;
            readytoassign = false;
            skipwhilemousedown = false;
            skipwhiledrawing = false;
            draggingfeatures = false;
            trackingfeatures = false;
            zoneLayer.InternalFeatures.Clear();
            labelLayer.InternalFeatures.Clear();
            selectLayer.InternalFeatures.Clear();
            measureLayer.InternalFeatures.Clear();
            SelectedCropZones.Clear();
            operatingmode = 0;
            //boundarypoints = new MultipointShape();
        }

        void InitializeMap()
        {
            Map = new WpfMap();
            startedOutOnline = Landdb.Client.Infrastructure.NetworkStatus.IsInternetAvailableFast();
            Map.MapUnit = GeographyUnit.Meter;
            Map.MapTools.PanZoomBar.Visibility = System.Windows.Visibility.Visible;
            Map.MapTools.PanZoomBar.GlobeButtonVisibility = System.Windows.Visibility.Hidden;
            Map.MapTools.PanZoomBar.VerticalAlignment = System.Windows.VerticalAlignment.Center;
            Map.MapTools.PanZoomBar.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            Map.Background = System.Windows.Media.Brushes.White;
            Map.Cursor = Cursors.Arrow;
            zoomlevelfactory = new ZoomLevelFactory(mapsettings.ZoomLevel);
            Map.ZoomLevelSet = zoomlevelfactory.ZoomLevelSET;
            Map.MouseLeftButtonDown += (sender, e) =>
            {
                ((MainWindow)System.Windows.Application.Current.MainWindow).Draggable = false;
            };

            projection = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetGoogleMapParametersString());
            scalingTextStyle.RotationAngles = rotationAngles;
            scalingTextStyle.LabelPositions = labelPositions;
            HasUnsavedChanges = false;
            isAssigningMode = false;
            if (Map.Overlays.Contains("zoneOverlay"))
            {
                Map.Overlays.Remove("zoneOverlay");
            }
            if (Map.Overlays.Contains("selectOverlay"))
            {
                Map.Overlays.Remove("selectOverlay");
            }

            //measureLayer
            AreaStyle measureLayerStyle = AreaStyles.CreateSimpleAreaStyle(new GeoColor(50, GeoColor.StandardColors.Orange), new GeoColor(200, GeoColor.StandardColors.Orange), 2);
            measureLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(measureLayerStyle);
            measureLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(lineStylePen);
            measureLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            measureLayer.FeatureSource.Projection = projection;

            //dimensionLayer
            dimensionLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(dimensionDistancePolygonStyleText);
            dimensionLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(dimensionDistanceLineStyleText);
            dimensionLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            dimensionLayer.FeatureSource.Projection = projection;

            popupOverlay = new PopupOverlay();
            popupOverlay.DrawingExceptionMode = DrawingExceptionMode.DrawException;

            dimensionOverlay = new LayerOverlay();
            dimensionOverlay.DrawingExceptionMode = DrawingExceptionMode.DrawException;
            dimensionOverlay.Layers.Add("measureLayer", measureLayer);
            dimensionOverlay.Layers.Add("dimensionLayer", dimensionLayer);

            Map.Loaded += (sender, e) =>
            {
                if (maploaded) { return; }
                //EnsureDimensionLayerIsOpen();
                //LoadBingMapsOverlay();
                //ResetPivot();
                //OpenAndLoadaShapefile();
                //LoadShapeHistory();

                Map.Overlays.Add("popupOverlay", popupOverlay);
                Map.Overlays.Add("dimensionOverlay", dimensionOverlay);

                Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = lineStylePen;
                Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = areaStyleBrush;

                ////Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = null;
                ////Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = null;
                ////Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = null;
                ////Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.DefaultTextStyle = null;
                ////Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Clear();
                //////Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customDistancePolygonStyleText);
                //////Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customDistanceLineStyleText);
                ////Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(lineStylePen);
                ////Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(areaStyleBrush);
                //Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;

                //Map.EditOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = null;
                //Map.EditOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = null;
                //Map.EditOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = null;
                //Map.EditOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.DefaultTextStyle = null;
                //Map.EditOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Clear();
                ////Map.EditOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customDistancePolygonStyleText);
                ////Map.EditOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customDistanceLineStyleText);
                //Map.EditOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(lineStylePen);
                //Map.EditOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(areaStyleBrush);
                //Map.EditOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;



                //DragtInteractiveOverlay for setting the PointStyles of the control points and dragged points.
                EditPolygonOverlay dragInteractiveOverlay = new EditPolygonOverlay();
                dragInteractiveOverlay.ControlPointStyle = new PointStyle(PointSymbolType.Circle, new GeoSolidBrush(GeoColor.StandardColors.PaleGoldenrod), new GeoPen(GeoColor.StandardColors.Black), 8);
                dragInteractiveOverlay.DraggedControlPointStyle = new PointStyle(PointSymbolType.Circle, new GeoSolidBrush(GeoColor.StandardColors.Red), new GeoPen(GeoColor.StandardColors.Orange, 2), 10);
                dragInteractiveOverlay.CanDrag = true;
                dragInteractiveOverlay.CanReshape = true;
                dragInteractiveOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = null;
                dragInteractiveOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = null;
                dragInteractiveOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = null;
                dragInteractiveOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.DefaultTextStyle = null;
                dragInteractiveOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Clear();
                ////dragInteractiveOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(lineStylePen);
                dragInteractiveOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(areaStyleBrush);
                dragInteractiveOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customAreaPolygonStyleText);
                dragInteractiveOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
                //customAreaPolygonStyleText

                //dragInteractiveOverlay.CanAddVertex = false;
                //dragInteractiveOverlay.CanDrag = false;
                //dragInteractiveOverlay.CanRemoveVertex = false;
                //dragInteractiveOverlay.CanResize = false;
                //dragInteractiveOverlay.CanRotate = false;
                //dragInteractiveOverlay.CalculateAllControlPoints();

                Map.EditOverlay = dragInteractiveOverlay;

                EnsureDimensionLayerIsOpen();
                LoadBingMapsOverlay();
                ResetPivot();
                OpenAndLoadaShapefile();
                LoadShapeHistory();
                RaisePropertyChanged("SelectedCropZones");
                maploaded = true;
            };

            //Map.Loaded += (sender, e) => {
            //    if (maploaded) { return; }
            //    //EnsureDimensionLayerIsOpen();
            //    //LoadBingMapsOverlay();
            //    //ResetPivot();
            //    //OpenAndLoadaShapefile();
            //    //LoadShapeHistory();

            //    Map.Overlays.Add("popupOverlay", popupOverlay);
            //    Map.Overlays.Add("dimensionOverlay", dimensionOverlay);

            //    //Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = lineStylePen;
            //    //Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = areaStyleBrush;

            //    ////Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = null;
            //    ////Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = null;
            //    ////Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = null;
            //    ////Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.DefaultTextStyle = null;
            //    ////Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Clear();
            //    //////Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customDistancePolygonStyleText);
            //    //////Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customDistanceLineStyleText);
            //    ////Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(lineStylePen);
            //    ////Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(areaStyleBrush);
            //    //Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;

            //    //Map.EditOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = null;
            //    //Map.EditOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = null;
            //    //Map.EditOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = null;
            //    //Map.EditOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.DefaultTextStyle = null;
            //    //Map.EditOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Clear();
            //    ////Map.EditOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customDistancePolygonStyleText);
            //    ////Map.EditOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customDistanceLineStyleText);
            //    //Map.EditOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(lineStylePen);
            //    //Map.EditOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(areaStyleBrush);
            //    //Map.EditOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;


            //    TrackPolygonOverlay trackpolygonOverlay = new TrackPolygonOverlay();
            //    trackpolygonOverlay.ControlPointStyle = new PointStyle(PointSymbolType.Circle, new GeoSolidBrush(GeoColor.StandardColors.PaleGoldenrod), new GeoPen(GeoColor.StandardColors.Black), 8);
            //    trackpolygonOverlay.DraggedControlPointStyle = new PointStyle(PointSymbolType.Circle, new GeoSolidBrush(GeoColor.StandardColors.Red), new GeoPen(GeoColor.StandardColors.Orange, 2), 10);
            //    trackpolygonOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = null;
            //    trackpolygonOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = null;
            //    trackpolygonOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = null;
            //    trackpolygonOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.DefaultTextStyle = null;
            //    trackpolygonOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Clear();
            //    trackpolygonOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(lineStylePen);
            //    trackpolygonOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(areaStyleBrush);
            //    trackpolygonOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customAreaPolygonStyleText);
            //    trackpolygonOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            //    Map.TrackOverlay = trackpolygonOverlay;


            //    //DragtInteractiveOverlay for setting the PointStyles of the control points and dragged points.
            //    EditPolygonOverlay dragInteractiveOverlay = new EditPolygonOverlay();
            //    dragInteractiveOverlay.ControlPointStyle = new PointStyle(PointSymbolType.Circle, new GeoSolidBrush(GeoColor.StandardColors.PaleGoldenrod), new GeoPen(GeoColor.StandardColors.Black), 8);
            //    dragInteractiveOverlay.DraggedControlPointStyle = new PointStyle(PointSymbolType.Circle, new GeoSolidBrush(GeoColor.StandardColors.Red), new GeoPen(GeoColor.StandardColors.Orange, 2), 10);
            //    dragInteractiveOverlay.CanDrag = true;
            //    dragInteractiveOverlay.CanReshape = true;
            //    dragInteractiveOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = null;
            //    dragInteractiveOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = null;
            //    dragInteractiveOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = null;
            //    dragInteractiveOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.DefaultTextStyle = null;
            //    dragInteractiveOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Clear();
            //    ////dragInteractiveOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(lineStylePen);
            //    dragInteractiveOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(areaStyleBrush);
            //    dragInteractiveOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customAreaPolygonStyleText);
            //    dragInteractiveOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            //    //customAreaPolygonStyleText

            //    //dragInteractiveOverlay.CanAddVertex = false;
            //    //dragInteractiveOverlay.CanDrag = false;
            //    //dragInteractiveOverlay.CanRemoveVertex = false;
            //    //dragInteractiveOverlay.CanResize = false;
            //    //dragInteractiveOverlay.CanRotate = false;
            //    //dragInteractiveOverlay.CalculateAllControlPoints();

            //    Map.EditOverlay = dragInteractiveOverlay;

            //    EnsureDimensionLayerIsOpen();
            //    LoadBingMapsOverlay();
            //    ResetPivot();
            //    OpenAndLoadaShapefile();
            //    LoadShapeHistory();
            //    RaisePropertyChanged("SelectedCropZones");
            //    maploaded = true;
            //};

            Map.MouseUp += (sender, e) =>
            {
                //Map.ExtentOverlay.PanMode = MapPanMode.Default;
                skipwhilemousedown = false;
                if (draggingfeatures) {
                    draggingfeatures = false;
                    try {
                        //GeoCollection<Feature> cutlines = LoadCutLineShapes();
                        //IList<Feature> cz = SplitPolgonsWithLines(cutlines);
                        //Map.Refresh();
                    }
                    catch (Exception ex) {
                        int z = 0;
                    }
                }
            };

            Map.OverlaysDrawing += (sender, e) =>
            {
                skipwhiledrawing = true;
            };

            Map.OverlaysDrawn += (sender, e) =>
            {
                skipwhiledrawing = false;
            };

            Map.CurrentScaleChanged += (sender, e) => {
                scalingTextStyle.MapScale = e.CurrentScale;
            };

            Map.EditOverlay.MapMouseUp += (sender, e) => {
                if (calculatingdimensions) { return; }
                if (draggingfeatures) {
                    draggingfeatures = false;
                    try {
                        double worldx = e.InteractionArguments.WorldX;
                        double worldy = e.InteractionArguments.WorldY;
                        PointShape projectedpoint = projection.ConvertToInternalProjection(new PointShape(worldx, worldy)) as PointShape;
                        CalculateDimensionLayer(null, projectedpoint);
                    }
                    catch (Exception ex) {
                        int z = 0;
                    }
                }
            };

            Map.EditOverlay.FeatureDragging += (sender, e) => {
                draggingfeatures = true;
            };

            Map.EditOverlay.VertexMoving += (sender, e) => {
                draggingfeatures = true;
            };

            Map.EditOverlay.VertexMoved += (sender, e) => {
                if (calculatingdimensions) { return; }
                try {
                    Vertex worldvertex = e.MovedVertex;
                    PointShape projectedpoint = projection.ConvertToInternalProjection(new PointShape(worldvertex)) as PointShape;
                    CalculateDimensionLayer(projectedpoint, null);
                }
                catch (Exception ex) {
                    int z = 0;
                }
            };



            Map.EditOverlay.VertexRemoved += (sender, e) => {
                removingvertex = true;
            };

            //Map.TrackOverlay.TrackStarted += (sender, e) => {
            //    //if (Map.TrackOverlay.TrackMode != TrackMode.None) { 
            //    trackingfeatures = true;
            //    //}
            //};

            //Map.TrackOverlay.TrackEnded += (sender, e) => {
            //    trackingfeatures = false;
            //};

            //Map.TrackOverlay.MapMouseMove += (sender, e) => {
            //    if (trackingfeatures && CommandDescription == "Draw Polygon2") {

            //        if (Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Count > 0) {
            //            BaseShape baseShape = Map.TrackOverlay.TrackShapeLayer.InternalFeatures[0].GetShape();
            //            if (baseShape is PolygonShape) {
            //                PolygonShape polyShape = baseShape as PolygonShape;
            //                if (polyShape.OuterRing.Vertices.Count > 1) {
            //                    LineShape lineShape = new LineShape();
            //                    foreach (Vertex vert in polyShape.OuterRing.Vertices) {
            //                        lineShape.Vertices.Add(vert);
            //                    }
            //                    lineShape.Vertices.Add(polyShape.OuterRing.Vertices[0]);
            //                    //int index1 = polyShape.OuterRing.Vertices.Count - 2;
            //                    //int index2 = polyShape.OuterRing.Vertices.Count - 1;
            //                    //int index3 = 0;
            //                    //LineShape lineShape = new LineShape();
            //                    //lineShape.Vertices.Add(polyShape.OuterRing.Vertices[index1]);
            //                    //lineShape.Vertices.Add(polyShape.OuterRing.Vertices[index2]);
            //                    //lineShape.Vertices.Add(polyShape.OuterRing.Vertices[index3]);
            //                    if (Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Count > 1) {
            //                        Map.TrackOverlay.TrackShapeLayer.InternalFeatures[1] = new Feature(lineShape);
            //                    }
            //                    else {
            //                        Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Add(new Feature(lineShape));
            //                    }
            //                }
            //            }
            //        }

            //    }
            //};


            Map.MouseMove += (sender, e) => {
                PointShape worldlocation2 = Map.ToWorldCoordinate(e.GetPosition(Map));
                PointShape centerpoint = projection.ConvertToInternalProjection(worldlocation2) as PointShape;
                Vertex geoVertex1 = new Vertex(centerpoint.X, centerpoint.Y);
                string _DecimalDegreeString = Math.Round(geoVertex1.Y, 5) + ", " + Math.Round(geoVertex1.X, 5);
                string displaycoordinates1 = DecimalDegrees.ddstring_To_DMs(_DecimalDegreeString, mapsettings.DisplayCoordinateFormat);
                DisplayCoordinates = displaycoordinates1;
                if (removingvertex) {
                    removingvertex = false;
                    try {
                        //GeoCollection<Feature> cutlines = LoadCutLineShapes();
                        //IList<Feature> cz = SplitPolgonsWithLines(cutlines);
                        //Map.Refresh();
                    }
                    catch (Exception ex) {
                        int z = 0;
                    }
                }
            };
        }

        private void LoadBingMapsOverlay()
        {
            if (!startedOutOnline) { return; }
            IMapOverlay mapoverlay = new BingMapOverlay();
            Overlay bingOverlay = mapoverlay.Overlay;
            if (bingOverlay == null) { return; }
            if (Map.Overlays.Contains("Bing")) {
                Map.Overlays.Remove("Bing");
            }
            if (Map.Overlays.Contains("Bing Online")) {
                Map.Overlays.Remove("Bing Online");
            }
            if (Map.Overlays.Contains("Roads Only")) {
                Map.Overlays.Remove("Roads Only");
            }
            if (Map.Overlays.Contains("Road Online")) {
                Map.Overlays.Remove("Road Online");
            }
            if (Map.Overlays.Contains("None")) {
                Map.Overlays.Remove("None");
            }
            //bingOverlay.IsVisible = mapoverlay.Visibility;
            //((TileOverlay)bingOverlay).TileType = TileType.SingleTile;
            Map.Overlays.Add(mapoverlay.Name, bingOverlay);
            Map.Overlays.MoveToBottom(mapoverlay.Name);
        }


        private void OpenAndLoadaShapefile() {
            CreateInMemoryLayerAndOverlay();
            LoadShapeFileOverlayOntoMap();
            //LoadShapeHistory();
            if (currentLayer.InternalFeatures.Count > 0) {
                HandleShapeEdit();
            }
            else {
                HandleDrawAndEditPolygon();
                //DrawPolygon();
            }
        }

        private void LoadShapeHistory() {
            Path shapePreview = null;
            selectedCropZones = new ObservableCollection<CropzoneCreationViewModel>();
            if (selectedIdentity == null) { return; }
            var maps = clientEndpoint.GetView<ItemMap>(selectedIdentity);
            try {
                if (maps.HasValue) {
                    List<ItemMapItem> mrilist = new List<ItemMapItem>();
                    for (int i = maps.Value.MapItems.Count - 1; i >= 0; i--) {
                        try {
                            string itemindex = i.ToString();
                            ItemMapItem item = maps.Value.MapItems[i];
                            var feature = new Feature(item.MapData);
                            double area = ((AreaBaseShape)feature.GetShape()).GetArea(GeographyUnit.DecimalDegree, ThinkGeo.MapSuite.Core.AreaUnit.Acres);
                            AreaMeasure selectedarea = UnitFactory.GetUnitByName(areaUnitString).GetMeasure((double)area) as AreaMeasure;

                            dispatcher.BeginInvoke(new Action(() => {
                                shapePreview = WpfTransforms.CreatePathFromMapData(new[] { item.MapData }, 120);
                                CropzoneCreationViewModel ccvm = new CropzoneCreationViewModel(itemindex, item.MapData, selectedarea, item.CropYearChangeOccurred);
                                SelectedCropZones.Add(ccvm);
                            }));
                        }
                        catch (Exception ex) {
                            int d = 0;
                        }
                    }
                }
            }
            catch { }
            if (selectedCropZones.Count > 0) {
                selectedCropZone = selectedCropZones[0];
                RaisePropertyChanged("SelectedCropZone");
                RaisePropertyChanged("SelectedCropZones");
            }
        }

        private void CreateInMemoryLayerAndOverlay() {
            layerOverlay = new LayerOverlay();
            layerOverlay.DrawingExceptionMode = DrawingExceptionMode.DrawException;
            currentLayer = new InMemoryFeatureLayer();
            AreaStyle currentLayerStyle = new AreaStyle(new GeoPen(GeoColor.StandardColors.Red, 2), new GeoSolidBrush(new GeoColor(20, GeoColor.StandardColors.LightGray)));
            currentLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = currentLayerStyle;
            currentLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            projection = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetGoogleMapParametersString());
            currentLayer.FeatureSource.Projection = projection;
            try {
                var shape1 = SelectedField.GetShape();
                if (shape1 != null) {
                    if (shape1 is PolygonShape) {
                        PolygonShape poly222 = shape1 as PolygonShape;
                        if (poly222.OuterRing.Vertices.Count > 2) {
                            currentLayer.InternalFeatures.Add(SelectedField);
                        }
                    }
                    if (shape1 is MultipolygonShape) {
                        MultipolygonShape currentPolygonShape = shape1 as MultipolygonShape;
                        if (currentPolygonShape.Polygons.Count > 0) {
                            if (currentPolygonShape.Polygons[0].OuterRing.Vertices.Count > 2) {
                                currentLayer.InternalFeatures.Add(SelectedField);
                            }
                        }
                    }
                }
            }
            catch { }
            //currentLayer.InternalFeatures.Add(SelectedField);
            layerOverlay.Layers.Add("ImportLayer", currentLayer);

            spatialQueryResultLayer = new InMemoryFeatureLayer();
            AreaStyle spatialQueryResultLayerStyle = new AreaStyle(new GeoPen(GeoColor.StandardColors.Black, 2), new GeoSolidBrush(new GeoColor(100, GeoColor.StandardColors.LightGray)));
            spatialQueryResultLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = spatialQueryResultLayerStyle;
            spatialQueryResultLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            //Projection projectionx = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetGoogleMapParametersString());
            Projection projectionx = new Proj4Projection(Proj4Projection.GetGoogleMapParametersString(), Proj4Projection.GetGoogleMapParametersString());
            spatialQueryResultLayer.FeatureSource.Projection = projectionx;
            foreach (Feature featureitem in spatialQueryResults) {
                spatialQueryResultLayer.InternalFeatures.Add(featureitem);
            }
            layerOverlay.Layers.Add("spatialQueryResultLayer", spatialQueryResultLayer);

            //measureLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customDistancePolygonStyleText);
            measureLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customDistanceLineStyleText);
            AreaStyle measureLayerStyle = AreaStyles.CreateSimpleAreaStyle(new GeoColor(50, GeoColor.StandardColors.Orange), new GeoColor(200, GeoColor.StandardColors.Orange), 2);
            measureLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(measureLayerStyle);
            measureLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(lineStylePen);
            measureLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            measureLayer.FeatureSource.Projection = projection;
            layerOverlay.Layers.Add("measureLayer", measureLayer);
            popupOverlay = new PopupOverlay();

        }

        private void LoadShapeFileOverlayOntoMap() {
            if (Map.Overlays.Contains("importOverlay")) {
                Map.Overlays.Remove("importOverlay");
            }
            if (layerOverlay.Layers.Contains("ImportLayer")) {
                Map.Overlays.Add("importOverlay", layerOverlay);
            }

            EnsureCurrentLayerIsOpen();
            EnsureMeasureLayerIsOpen();
            var extent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
            try {
                var shapelayerfeatures = currentLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);
                if (shapelayerfeatures.Count > 0) {
                    var shapelayercolumns = currentLayer.FeatureSource.GetColumns();
                    extent = ExtentHelper.GetBoundingBoxOfItems(shapelayerfeatures);
                    Map.CurrentExtent = extent;
                    PointShape center = extent.GetCenterPoint();
                }
                else {
                    Map.CurrentExtent = CurrentMapExtent;
                    PointShape center = CurrentMapExtent.GetCenterPoint();
                }
            }
            catch {
                Map.CurrentExtent = CurrentMapExtent;
                PointShape center = CurrentMapExtent.GetCenterPoint();
            }
        }







        //private void OpenAndLoadaShapefile()
        //{
        //    CreateInMemoryLayerAndOverlay();
        //    CreateZoneLayerAndOverlay();
        //    LoadShapeFileOverlayOntoMap();
        //    scalingTextStyle.MapScale = Map.CurrentScale;
        //    if (Map.Overlays.Contains("importOverlay"))
        //    {
        //        if (((LayerOverlay)Map.Overlays["importOverlay"]).Layers.Contains("ImportLayer"))
        //        {
        //            Map.Refresh();
        //        }
        //    }
        //    if (Map.Overlays.Contains("zoneOverlay"))
        //    {
        //        if (((LayerOverlay)Map.Overlays["zoneOverlay"]).Layers.Contains("ZoneLayer"))
        //        {
        //            Map.Refresh();
        //        }
        //    }
        //    if (Map.Overlays.Contains("selectOverlay"))
        //    {
        //        if (((LayerOverlay)Map.Overlays["selectOverlay"]).Layers.Contains("selectLayer"))
        //        {
        //            Map.Refresh();
        //        }
        //    }
        //}

        //private void CreateInMemoryLayerAndOverlay()
        //{
        //    layerOverlay = new LayerOverlay();
        //    currentLayer = new InMemoryFeatureLayer();
        //    AreaStyle currentLayerStyle = AreaStyles.CreateSimpleAreaStyle(new GeoColor(90, GeoColor.SimpleColors.PastelYellow), new GeoColor(100, GeoColor.StandardColors.Yellow), 2);
        //    currentLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = currentLayerStyle;
        //    currentLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
        //    projection = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetGoogleMapParametersString());
        //    currentLayer.FeatureSource.Projection = projection;
        //    currentLayer.InternalFeatures.Add(SelectedField);
        //    layerOverlay.Layers.Add("ImportLayer", currentLayer);
        //    layerOverlay.DrawingExceptionMode = DrawingExceptionMode.DrawException;




        //    boundarypoints = new MultipointShape();
        //    if (currentLayer.InternalFeatures.Count > 0)
        //    {
        //        BaseShape bs = currentLayer.InternalFeatures[0].GetShape();
        //        MultipolygonShape mps2 = new MultipolygonShape();
        //        if (bs is MultipolygonShape)
        //        {
        //            mps2 = bs as MultipolygonShape;
        //        }
        //        else if (bs is PolygonShape)
        //        {
        //            mps2.Polygons.Add(bs as PolygonShape);
        //        }
        //        foreach (PolygonShape poly in mps2.Polygons)
        //        {
        //            foreach (Vertex vert in poly.OuterRing.Vertices)
        //            {
        //                boundarypoints.Points.Add(new PointShape(vert));
        //            }
        //        }
        //    }

        //    AreaStyle measureLayerStyle = AreaStyles.CreateSimpleAreaStyle(new GeoColor(50, GeoColor.StandardColors.Orange), new GeoColor(200, GeoColor.StandardColors.Orange), 2);
        //    measureLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(measureLayerStyle);
        //    //measureLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customDistancePolygonStyleText);
        //    measureLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customDistanceLineStyleText);
        //    measureLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(lineStylePen);
        //    measureLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
        //    measureLayer.FeatureSource.Projection = projection;
        //    layerOverlay.Layers.Add("measureLayer", measureLayer);
        //}

        //private void MeasureNearestBoundary(Feature firstfeature) {
        //    MultilineShape multiLineShape = firstfeature.GetShape().GetShortestLineTo(boundarypoints, GeographyUnit.Meter);
        //    measureLayer.InternalFeatures.Add(new Feature(multiLineShape.GetWellKnownBinary()));
        //    try {
        //        selectLayer.InternalFeatures.Add(new Feature(new PointShape(multiLineShape.Lines[0].Vertices[0]).GetWellKnownBinary()));
        //        selectLayer.InternalFeatures.Add(new Feature(new PointShape(multiLineShape.Lines[0].Vertices[1]).GetWellKnownBinary()));
        //    }
        //    catch (Exception ex) {
        //        int z = 0;
        //    }
        //}

        //private void CreateZoneLayerAndOverlay()
        //{
        //    selectOverlay = new LayerOverlay();
        //    selectLayer = new InMemoryFeatureLayer();
        //    selectLayer.FeatureSource.Open();
        //    selectLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = AreaStyles.CreateSimpleAreaStyle(GeoColor.FromArgb(80, GeoColor.SimpleColors.Red), GeoColor.FromArgb(100, GeoColor.SimpleColors.Red));
        //    selectLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = LineStyles.CreateSimpleLineStyle(GeoColor.FromArgb(100, GeoColor.SimpleColors.Red), 2, true);
        //    selectLayer.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = PointStyles.CreateSimplePointStyle(PointSymbolType.Circle, GeoColor.FromArgb(100, GeoColor.SimpleColors.Red), 8);
        //    selectLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
        //    selectLayer.FeatureSource.Projection = projection;
        //    selectLayer.FeatureSource.Close();
        //    selectOverlay.Layers.Add("selectLayer", selectLayer);
        //    selectOverlay.DrawingExceptionMode = DrawingExceptionMode.DrawException;

        //    zoneOverlay = new LayerOverlay();
        //    zoneLayer = new InMemoryFeatureLayer();
        //    zoneLayer.FeatureSource.Open();
        //    FeatureSourceColumn column1 = new FeatureSourceColumn(labelcolumn, "string", 64);
        //    FeatureSourceColumn column2 = new FeatureSourceColumn(namecolumn, "string", 64);
        //    FeatureSourceColumn column3 = new FeatureSourceColumn(idcolumn, "string", 256);
        //    FeatureSourceColumn column4 = new FeatureSourceColumn(areacolumn, "string", 16);
        //    zoneLayer.Columns.Add(column1);
        //    zoneLayer.Columns.Add(column2);
        //    zoneLayer.Columns.Add(column3);
        //    zoneLayer.Columns.Add(column4);
        //    fieldsLayerAreaStyle = new CropzoneAreaStyle();
        //    fieldsLayerAreaStyle.SelectedAreaStyle = new AreaStyle(new GeoPen(GeoColor.StandardColors.Yellow, 2), new GeoSolidBrush(new GeoColor(50, GeoColor.StandardColors.Yellow)));
        //    fieldsLayerAreaStyle.AssignedAreaStyle = new AreaStyle(new GeoPen(GeoColor.StandardColors.Green, 2), new GeoSolidBrush(new GeoColor(50, GeoColor.StandardColors.Green)));
        //    fieldsLayerAreaStyle.UnselectedAreaStyle = new AreaStyle(new GeoPen(GeoColor.StandardColors.LightGray, 2), new GeoSolidBrush(new GeoColor(50, GeoColor.StandardColors.LightGray)));
        //    fieldsLayerAreaStyle.SelectedFeatures = new List<Feature>();
        //    fieldsLayerAreaStyle.AssignedFeatures = new List<Feature>();

        //    zoneLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(fieldsLayerAreaStyle);
        //    zoneLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(scalingTextStyle);
        //    zoneLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
        //    zoneLayer.FeatureSource.Projection = projection;
        //    zoneLayer.FeatureSource.Close();
        //    zoneOverlay.Layers.Add("ZoneLayer", zoneLayer);
        //    zoneOverlay.DrawingExceptionMode = DrawingExceptionMode.DrawException;

        //    //selectLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = AreaStyles.CreateSimpleAreaStyle(GeoColor.SimpleColors.Transparent, GeoColor.FromArgb(100, GeoColor.SimpleColors.Red));
        //    //selectLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = LineStyles.CreateSimpleLineStyle(GeoColor.FromArgb(100, GeoColor.SimpleColors.Red), 2, true);
        //    //selectLayer.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = PointStyles.CreateSimplePointStyle(PointSymbolType.Circle, GeoColor.FromArgb(100, GeoColor.SimpleColors.Red), 8);
        //    //selectLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
        //    //selectLayer.FeatureSource.Projection = projection;
        //    //zoneOverlay.Layers.Add("selectLayer", selectLayer);

        //    labelLayer = new InMemoryFeatureLayer();
        //    labelLayer.FeatureSource.Open();
        //    labelLayer.Columns.Add(column1);
        //    labelLayer.Columns.Add(column2);
        //    labelLayer.Columns.Add(column3);
        //    labelLayer.Columns.Add(column4);
        //    TextStyle zoneTextStyle = TextStyles.CreateSimpleTextStyle(labelcolumn, "Arial", 14, DrawingFontStyles.Regular, GeoColor.StandardColors.Black);
        //    zoneTextStyle.OverlappingRule = LabelOverlappingRule.NoOverlapping;
        //    zoneTextStyle.BestPlacement = true;
        //    labelLayer.ZoomLevelSet.ZoomLevel01.DefaultTextStyle = zoneTextStyle;
        //    labelLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
        //    labelLayer.FeatureSource.Projection = projection;
        //    labelLayer.FeatureSource.Close();
        //    zoneOverlay.Layers.Add("LabelLayer", labelLayer);
        //}

        //private void LoadShapeFileOverlayOntoMap()
        //{
        //    if (Map.Overlays.Contains("importOverlay"))
        //    {
        //        Map.Overlays.Remove("importOverlay");
        //    }
        //    if (layerOverlay.Layers.Contains("ImportLayer"))
        //    {
        //        Map.Overlays.Add("importOverlay", layerOverlay);
        //    }
        //    if (Map.Overlays.Contains("selectOverlay"))
        //    {
        //        Map.Overlays.Remove("selectOverlay");
        //    }
        //    if (selectOverlay.Layers.Contains("selectLayer"))
        //    {
        //        Map.Overlays.Add("selectOverlay", selectOverlay);
        //    }
        //    if (Map.Overlays.Contains("zoneOverlay"))
        //    {
        //        Map.Overlays.Remove("zoneOverlay");
        //    }
        //    if (zoneOverlay.Layers.Contains("ZoneLayer"))
        //    {
        //        Map.Overlays.Add("zoneOverlay", zoneOverlay);
        //    }

        //    EnsureCurrentLayerIsOpen();
        //    var extent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
        //    try
        //    {
        //        var shapelayerfeatures = currentLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);
        //        if (shapelayerfeatures.Count > 0)
        //        {
        //            var shapelayercolumns = currentLayer.FeatureSource.GetColumns();
        //            extent = ExtentHelper.GetBoundingBoxOfItems(shapelayerfeatures);
        //            Map.CurrentExtent = extent;
        //            PointShape center = extent.GetCenterPoint();
        //            Map.CenterAt(center);
        //        }
        //        else
        //        {
        //            Map.CurrentExtent = extent;
        //            PointShape center = extent.GetCenterPoint();
        //            Map.CenterAt(center);
        //        }
        //    }
        //    catch
        //    {
        //        Map.CurrentExtent = extent;
        //        PointShape center = extent.GetCenterPoint();
        //        Map.CenterAt(center);
        //    }
        //}

        //private void EnsureCurrentLayerIsOpen()
        //{
        //    if (currentLayer == null)
        //    {
        //        currentLayer = new InMemoryFeatureLayer();
        //    }
        //    if (!currentLayer.IsOpen)
        //    {
        //        currentLayer.Open();
        //    }
        //    if (zoneLayer == null)
        //    {
        //        zoneLayer = new InMemoryFeatureLayer();
        //    }
        //    if (!zoneLayer.IsOpen)
        //    {
        //        zoneLayer.Open();
        //    }
        //    if (labelLayer == null)
        //    {
        //        labelLayer = new InMemoryFeatureLayer();
        //    }
        //    if (!labelLayer.IsOpen)
        //    {
        //        labelLayer.Open();
        //    }
        //    if (measureLayer == null)
        //    {
        //        measureLayer = new InMemoryFeatureLayer();
        //    }
        //    if (!measureLayer.IsOpen)
        //    {
        //        measureLayer.Open();
        //    }
        //    if (selectLayer == null)
        //    {
        //        selectLayer = new InMemoryFeatureLayer();
        //    }
        //    if (!selectLayer.IsOpen)
        //    {
        //        selectLayer.Open();
        //    }
        //    if (!projection.IsOpen)
        //    {
        //        projection.Open();
        //    }
        //}

        //void SelectThisItem(CropzoneCreationViewModel itemToSelect)
        //{
        //    if (itemToSelect == null) { return; }
        //    SelectedCropZone = SelectedCropZones.Where(x => x.Id == itemToSelect.Id).SingleOrDefault();
        //    for (int i = 0; i < SelectedCropZones.Count; i++) {
        //        if (SelectedCropZones[i].Id == itemToSelect.Id) {
        //            SelectedCropZones[i].Completed = false;
        //            break;
        //        }
        //    }

        //    //ShowCreateNewItemPopup(selectedCropZone);
        //}

        //void ShowCreateNewItemPopup(CropzoneCreationViewModel selcropzone)
        //{
        //    if (FieldTreeItemVM == null) { return; }
        //    AbstractTreeItemViewModel parentNode = FieldTreeItemVM;
        //    //SelectedCropZone = selcropzone;
        //    createItemViewModel.Id = selectedCropZone.Id;
        //    createItemViewModel.SelectedParentNode = parentNode;
        //    createItemViewModel.SelectedCropZone = selectedCropZone;
        //    createItemViewModel.ReportedArea = selectedCropZone.SelectedArea.Value;
        //    createItemViewModel.MapData = selectedCropZone.MapData;
        //    createItemViewModel.CommandList = selectedCropZone.CommandList;
        //    createItemViewModel.UseParentBoundary = false;
        //    ScreenDescriptor descriptor = null;
        //    if (parentNode is FieldTreeItemViewModel)
        //    {
        //        selectedCropZone.Completed = false;
        //        descriptor = new ScreenDescriptor("Landdb.Views.Fields.Popups.AddCropZoneAssignerPopupView", "AddCz", createItemViewModel);
        //    }
        //    Messenger.Default.Send<ShowPopupMessage>(new ShowPopupMessage() { ScreenDescriptor = descriptor });
        //}

        //void OnNewItemCreated(AbstractTreeItemViewModel vm)
        //{
        //    if (!createItemViewModel.IsContinuouslyAdding)
        //    {
        //        vm.IsSelected = true;
        //    }
        //    CropZoneTreeItemViewModel createdcropzone;
        //    if (vm != null)
        //    {
        //        createdcropzone = vm as CropZoneTreeItemViewModel;
        //        selectedCropZone.Name = createdcropzone.Name;
        //        selectedCropZone.SelectedArea = createdcropzone.Area;
        //        selectedCropZone.Completed = true;
        //        selectedCropZone.CommandList = createItemViewModel.CommandList;
        //        ChangeCropZoneBoundary saveBoundaryCommand = new ChangeCropZoneBoundary(createdcropzone.CropZoneId, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), selectedCropZone.MapData, "WKT", selectedCropZone.SelectedArea.Value, selectedCropZone.SelectedArea.Unit.Name, ApplicationEnvironment.CurrentCropYear);
        //        selectedCropZone.CommandList.Add(saveBoundaryCommand);

        //        CropZoneTreeItemViewModel newItem = new CropZoneTreeItemViewModel(new Landdb.Domain.ReadModels.Tree.TreeViewCropZoneItem() { CropZoneId = createdcropzone.CropZoneId, CropId = createdcropzone.Crop, Name = createdcropzone.Name, IsPerennial = false, ReportedArea = null, ReportedAreaUnit = null, BoundaryArea = selectedCropZone.SelectedArea.Value, BoundaryAreaUnit = selectedCropZone.SelectedArea.Unit.Name }, FieldTreeItemVM, sortAlphabetically);
        //        selectedCropZone.Nodelist.Add(newItem);
        //        RaisePropertyChanged("FieldName");
        //        RaisePropertyChanged("Name");
        //        RaisePropertyChanged("SelectedArea");
        //        RaisePropertyChanged("Completed");
        //        for (int i = 0; i < SelectedCropZones.Count; i++) {
        //            if (SelectedCropZones[i].Id == selectedCropZone.Id) {
        //                SelectedCropZones[i].Completed = true;
        //                break;
        //            }
        //        }

        //        if (SelectedCropZones.Count == 0 || SelectedCropZones.Where(x => x.Completed == null).Any())
        //        {
        //            ReadyToFinish = false;
        //        }
        //        else
        //        {
        //            ReadyToFinish = true;
        //        }
        //    }
        //}

        //void OnNewItemIgnored()
        //{
        //    selectedCropZone.Completed = false;
        //    selectedCropZone.Crop = new MiniCrop();
        //    selectedCropZone.CommandList = new List<IDomainCommand>();
        //    RaisePropertyChanged("Completed");
        //    if (SelectedCropZones.Count == 0 || SelectedCropZones.Where(x => x.Completed == null).Any())
        //    {
        //        ReadyToFinish = false;
        //    }
        //    else
        //    {
        //        ReadyToFinish = true;
        //    }
        //}

        //private GeoCollection<Feature> LoadCutLineShapes()
        //{
        //    GeoCollection<Feature> cutlines = new GeoCollection<Feature>();
        //    try
        //    {
        //        var edfeatures = Map.EditOverlay.EditShapesLayer.InternalFeatures;
        //        if (Map.EditOverlay.EditShapesLayer.InternalFeatures.Count > 0)
        //        {
        //            LineShape editLineShape = new LineShape();
        //            PolygonShape editPolygonShape = new PolygonShape();
        //            foreach (var cfeat in edfeatures)
        //            {
        //                var shape = cfeat.GetShape();
        //                if (shape is LineShape)
        //                {
        //                    editLineShape = shape as LineShape;
        //                    editLineShape = projection.ConvertToInternalProjection(editLineShape) as LineShape;
        //                    cutlines.Add(new Feature(editLineShape));
        //                }
        //                if (shape is PolygonShape)
        //                {
        //                    editPolygonShape = shape as PolygonShape;
        //                    editPolygonShape = projection.ConvertToInternalProjection(editPolygonShape) as PolygonShape;
        //                    cutlines.Add(new Feature(editPolygonShape));
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        int z = 0;
        //    }
        //    return cutlines;
        //}

        //void ChangeOperatingMode()
        //{
        //    if (operatingmode < 0)
        //    {
        //        modeofoperation = "Canceling Mode";
        //        IsAssigningMode = false;
        //        Map.Cursor = Cursors.Arrow;
        //        OnCancelCropzone();
        //    }
        //    if (operatingmode == 0)
        //    {
        //        modeofoperation = "Drawing Mode";
        //        IsAssigningMode = false;
        //    }
        //    if (operatingmode == 1)
        //    {
        //        modeofoperation = "Assigning Mode";
        //        CommandDescription = "Assign Crops To Shapes";
        //        IsAssigningMode = true;
        //        Map.Cursor = Cursors.Arrow;
        //        selectedCropZones = new ObservableCollection<CropzoneCreationViewModel>();
        //        foreach (Feature feature in zoneLayer.InternalFeatures)
        //        {
        //            MultipolygonShape multiPolygonShape = feature.GetShape() as MultipolygonShape;
        //            string spatialData = string.Empty;
        //            if (multiPolygonShape.Polygons.Count > 0)
        //            {
        //                spatialData = multiPolygonShape.GetWellKnownText();
        //                spatialData = spatialData.Replace(@"\n", "");
        //                spatialData = spatialData.Replace(@"\r", "");
        //                spatialData = spatialData.Replace(@"\t", "");
        //            }
        //            double area = 0;
        //            string cenlatlon = string.Empty;
        //            AreaMeasure selectedarea = UnitFactory.GetUnitByName(areaUnitString).GetMeasure((double)area) as AreaMeasure;
        //            if (multiPolygonShape.Polygons.Count > 0)
        //            {
        //                try
        //                {
        //                    area = multiPolygonShape.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
        //                    selectedarea = UnitFactory.GetUnitByName(areaUnitString).GetMeasure((double)area) as AreaMeasure;
        //                    PointShape centerpoint = multiPolygonShape.GetCenterPoint();
        //                    Vertex geoVertex1 = new Vertex(centerpoint.X, centerpoint.Y);
        //                    string decimaldegrees = Math.Round(geoVertex1.Y, 5) + ", " + Math.Round(geoVertex1.X, 5);
        //                    string longlat = DecimalDegrees.d_To_DMs(geoVertex1.X, 2, DecimalDegrees.Type.Longitude) + "  " + DecimalDegrees.d_To_DMs(geoVertex1.Y, 2, DecimalDegrees.Type.Latitude);
        //                    string longlatAG = DecimalDegrees.d_To_Dm(geoVertex1.X, 4, DecimalDegrees.Type.Longitude) + "  " + DecimalDegrees.d_To_Dm(geoVertex1.Y, 4, DecimalDegrees.Type.Latitude);
        //                    cenlatlon = decimaldegrees;
        //                }
        //                catch
        //                {
        //                    area = 0;
        //                }
        //            }
        //            var czId = new CropZoneId(ApplicationEnvironment.CurrentDataSourceId, Guid.NewGuid());
        //            selectedCropZone = new CropzoneCreationViewModel(czId, FieldTreeItemVM.Name, selectedarea.FullDisplay, spatialData, cenlatlon, selectedarea);
        //            SelectedCropZones.Add(selectedCropZone);
        //            ReadyToFinish = false;
        //            RaisePropertyChanged("SelectedCropZones");
        //        }
        //        measureLayer.InternalFeatures.Clear();
        //        Map.EditOverlay.EditShapesLayer.InternalFeatures.Clear();
        //        Map.EditOverlay.CalculateAllControlPoints();
        //        scalingTextStyle.MapScale = Map.CurrentScale;
        //        Map.Refresh();
        //    }
        //    if (operatingmode > 1)
        //    {
        //        modeofoperation = "Saving Mode";
        //        IsAssigningMode = false;
        //        Map.Cursor = Cursors.Arrow;
        //        OnCompleteCropzone();
        //    }
        //}

        //void OnCompleteCropzone()
        //{
        //    var sortedcropzones = from f in SelectedCropZones
        //                          orderby f.Name
        //                          select f;

        //    foreach (var ccvm in sortedcropzones)
        //    {
        //        if (ccvm.Completed.HasValue && ccvm.Completed.Value)
        //        {
        //            foreach (var idc in ccvm.CommandList)
        //            {
        //                clientEndpoint.SendOne(idc);
        //            }
        //            foreach (var newItem in ccvm.Nodelist)
        //            {
        //                FieldTreeItemVM.Children.Add(newItem);
        //                FieldTreeItemVM.IsExpanded = true;
        //            }
        //        }
        //    }
        //    operatingmode = 0;
        //    ReSetMap();
        //    Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Clear();
        //    Map.EditOverlay.EditShapesLayer.InternalFeatures.Clear();
        //    Map.EditOverlay.CalculateAllControlPoints();
        //    scalingTextStyle.MapScale = Map.CurrentScale;
        //    Map.Refresh();
        //    Map.Cursor = Cursors.Arrow;
        //    Messenger.Default.Send<ShowMainViewMessage>(new ShowMainViewMessage());
        //}


        ////static object clientLayerLock = new object();
        //private IList<Feature> SplitPolgonsWithLines(GeoCollection<Feature> cutfeatures)
        //{
        //    //lock (clientLayerLock) {

        //    IList<Feature> layerfeatures = new List<Feature>();
        //    IList<LineShape> lines = new List<LineShape>();
        //    IList<BaseShape> baseshapes = new List<BaseShape>();
        //    LineShape previousline = null;

        //    try
        //    {
        //        selectLayer.InternalFeatures.Clear();
        //        measureLayer.InternalFeatures.Clear();
        //        bool isfirstfeature = true;
        //        //measureLayer.EditTools.BeginTransaction();
        //        foreach (Feature feature in cutfeatures)
        //        {
        //            BaseShape baseshape = feature.GetShape();
        //            baseshapes.Add(baseshape);
        //            if (baseshape is LineShape)
        //            {
        //                if (isfirstfeature)
        //                {
        //                    MeasureNearestBoundary(feature);
        //                    isfirstfeature = false;
        //                }
        //                LineShape lineshape = baseshape as LineShape;
        //                lines.Add(lineshape);
        //                if (previousline != null)
        //                {
        //                    try
        //                    {
        //                        MultilineShape multiLineShape = lineshape.GetShortestLineTo(previousline, GeographyUnit.Meter);
        //                        measureLayer.InternalFeatures.Add(new Feature(multiLineShape.GetWellKnownBinary()));
        //                        //measureLayer.EditTools.Add(new Feature(multiLineShape.GetWellKnownBinary()));
        //                    }
        //                    catch { }
        //                }
        //                previousline = lineshape;
        //            }
        //        }
        //        //measureLayer.EditTools.CommitTransaction();
        //        measureLayer.BuildIndex();

        //        zoneLayer.InternalFeatures.Clear();
        //        labelLayer.InternalFeatures.Clear();
        //        //zoneLayer.EditTools.BeginTransaction();
        //        //labelLayer.EditTools.BeginTransaction();
        //        readytoassign = false;
        //        //try {
        //        foreach (Feature feat in currentLayer.InternalFeatures)
        //        {
        //            BaseShape baseshape2 = feat.GetShape();
        //            MultipolygonShape mps2 = new MultipolygonShape();
        //            if (baseshape2 is MultipolygonShape)
        //            {
        //                mps2 = baseshape2 as MultipolygonShape;
        //            }
        //            else if (baseshape2 is PolygonShape)
        //            {
        //                mps2.Polygons.Add(baseshape2 as PolygonShape);
        //            }
        //            try
        //            {
        //                //MultipolygonShape mps1 = Splitter.Split(mps2, lines);
        //                MultipolygonShape mps1 = Splitter.Split(mps2, baseshapes);
        //                foreach (PolygonShape poly4 in mps1.Polygons)
        //                {
        //                    MultipolygonShape mps4 = new MultipolygonShape();
        //                    mps4.Polygons.Add(poly4);
        //                    try
        //                    {
        //                        double area = mps4.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
        //                        Feature feature4 = new Feature(mps4);
        //                        feature4.ColumnValues.Add(labelcolumn, area.ToString("N2"));
        //                        feature4.ColumnValues.Add(namecolumn, "UnAssigned");
        //                        feature4.ColumnValues.Add(idcolumn, "0000");
        //                        feature4.ColumnValues.Add(areacolumn, area.ToString("N2"));
        //                        zoneLayer.InternalFeatures.Add(feature4);
        //                        labelLayer.InternalFeatures.Add(feature4);
        //                        //zoneLayer.EditTools.Add(feature4);
        //                        //labelLayer.EditTools.Add(feature4);
        //                        layerfeatures.Add(feature4);
        //                        readytoassign = true;
        //                    }
        //                    catch (Exception ex)
        //                    {
        //                        int xxx = 0;
        //                    }
        //                }
        //            }
        //            catch (Exception ex1)
        //            {
        //                int xxxx = 0;
        //            }
        //        }
        //        zoneLayer.BuildIndex();
        //        labelLayer.BuildIndex();
        //        //zoneLayer.EditTools.CommitTransaction();
        //        //labelLayer.EditTools.CommitTransaction();

        //    }
        //    catch (Exception ex2)
        //    {
        //        int xxxxx = 0;
        //    }
        //    finally
        //    {
        //    }
        //    RaisePropertyChanged("IsDrawingMode");
        //    return layerfeatures;
        //    //}
        //}


        //void SliceLine() {
        //    if (skipwhiledrawing) { return; }
        //    //Map.TrackOverlay.TrackMode = TrackMode.Line;
        //    //Map.Cursor = Cursors.Pen;
        //    //skipwhiledrawing = true;
        //    //CommandDescription = "Draw Slicing Line";



        //    //Map.KeyDown += new KeyEventHandler(keyEvent_Esc);
        //}

        //private void keyEvent_Esc(object sender, KeyEventArgs e) {
        //    if (e.Key == System.Windows.Input.Key.Escape) {
        //        CancelTrackingModeAndEdit();
        //    }
        //}

        //private void CancelTrackingModeAndEdit() {
        //    if (Map.TrackOverlay.TrackMode != TrackMode.None) {
        //        //Map.TrackOverlay.TrackEnded -= new EventHandler<TrackEndedTrackInteractiveOverlayEventArgs>(TrackOverlay_TrackEnded);
        //        Map.KeyDown -= new KeyEventHandler(keyEvent_Esc);

        //        if (Map.TrackOverlay.TrackMode == TrackMode.Polygon || Map.TrackOverlay.TrackMode == TrackMode.Line || Map.TrackOverlay.TrackMode == TrackMode.Circle) {
        //            Map.TrackOverlay.MouseDoubleClick(new InteractionArguments());
        //        }
        //        else {
        //            Map.TrackOverlay.MouseUp(new InteractionArguments());
        //        }

        //        Map.TrackOverlay.TrackMode = TrackMode.None;
        //        Map.Cursor = previousCursor;

        //        int count = Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Count;
        //        if (count > 0) {
        //            Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Remove("InTrackingFeature");
        //        }
        //        Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Clear();
        //        try {
        //            Map.TrackOverlay.Refresh();
        //        }
        //        catch { }
        //        Map.TrackOverlay = new TrackInteractiveOverlay();

        //        try {
        //            map.EditOverlay.EditShapesLayer.InternalFeatures.Clear();
        //            map.EditOverlay.CalculateAllControlPoints();
        //            map.EditOverlay.Refresh();
        //        }
        //        catch { }
        //    }
        //    if (map.TrackOverlay.TrackMode == TrackMode.None) {
        //        try {
        //            map.EditOverlay.EditShapesLayer.InternalFeatures.Clear();
        //            map.EditOverlay.CalculateAllControlPoints();
        //            map.EditOverlay.Refresh();
        //            map.Cursor = previousCursor;
        //        }
        //        catch { }
        //    }
        //    map.ZoomOut(10);
        //    map.Refresh();
        //}

        //private void CutLine_TrackEnded(object sender, TrackEndedTrackInteractiveOverlayEventArgs e) {
        //    Map.TrackOverlay.TrackEnded -= new EventHandler<TrackEndedTrackInteractiveOverlayEventArgs>(CutLine_TrackEnded);
        //    Map.TrackOverlay.TrackMode = TrackMode.None;
        //    Map.Cursor = previousCursor;
        //    if (Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Count > 0) {
        //        int shapecount = Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Count - 1;
        //        Feature feat = Map.TrackOverlay.TrackShapeLayer.InternalFeatures[shapecount];
        //        BaseShape baseShape = feat.GetShape();
        //        if (baseShape is PointShape) {
        //            PointShape ps1 = baseShape as PointShape;
        //            PointShape ps2 = projection.ConvertToInternalProjection(ps1) as PointShape;
        //            Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Clear();
        //            Map.TrackOverlay.Refresh();
        //        }
        //    }
        //}

        //void AssignShape()
        //{
        //    operatingmode = 1;
        //    //ChangeOperatingMode();
        //}

        //internal void LoadMapForItem(object item)
        //{
        //    IIdentity itemId = null;
        //    compiledpoly = new MultipolygonShape();
        //    if (item == null) { return; }
        //    Landdb.ViewModel.Fields.FieldDetails.FieldDetailsViewModel fvitem = null;
        //    if (item is Landdb.ViewModel.Fields.FieldDetails.FieldDetailsViewModel)
        //    {
        //        itemId = ((Landdb.ViewModel.Fields.FieldDetails.FieldDetailsViewModel)item).FieldView.Id;
        //        fvitem = (Landdb.ViewModel.Fields.FieldDetails.FieldDetailsViewModel)item;
        //        IList<CropZoneTreeItemViewModel> czvmlist = ((Landdb.ViewModel.Fields.FieldDetails.FieldDetailsViewModel)item).CropZoneTreeModels;
        //        foreach (CropZoneTreeItemViewModel cztree in czvmlist)
        //        {
        //            var maps = clientEndpoint.GetView<Landdb.Domain.ReadModels.Map.ItemMap>(cztree.CropZoneId);
        //            if (maps.HasValue)
        //            {
        //                Landdb.Domain.ReadModels.Map.ItemMapItem mri = maps.Value.MostRecentMapItem;
        //                var feature = new Feature(mri.MapData);
        //                BaseShape bs1 = feature.GetShape();
        //                if (bs1 is MultipolygonShape)
        //                {
        //                    compiledpoly = compiledpoly.Union(feature);
        //                }
        //            }
        //        }
        //    }
        //}



        private void EnsureCurrentLayerIsOpen() {
            if (currentLayer == null) {
                currentLayer = new InMemoryFeatureLayer();
            }

            if (!currentLayer.IsOpen) {
                currentLayer.Open();
            }

            if (!projection.IsOpen) {
                projection.Open();
            }
        }

        internal void EnsureMeasureLayerIsOpen() {
            if (measureLayer == null) {
                measureLayer = new InMemoryFeatureLayer();
            }

            if (!measureLayer.IsOpen) {
                measureLayer.Open();
            }

            if (!projection.IsOpen) {
                projection.Open();
            }
        }

        internal void EnsureDimensionLayerIsOpen() {
            if (dimensionLayer == null) {
                dimensionLayer = new InMemoryFeatureLayer();
            }

            if (!dimensionLayer.IsOpen) {
                dimensionLayer.Open();
            }

            if (!projection.IsOpen) {
                projection.Open();
            }
        }


        #region Draw

        // DRAW POLYGON
        //void DrawPolygon() {
        //    if (skipwhiledrawing) { return; }
        //    Map.TrackOverlay.TrackMode = TrackMode.Polygon;
        //    Map.Cursor = Cursors.Cross;
        //    skipwhiledrawing = true;
        //    CommandDescription = "Draw Polygon";

        //    //Map.KeyDown += new KeyEventHandler(keyEvent_Esc);
        //}

            ////Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customDistancePolygonStyleText);
            //Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customDistanceLineStyleText);
            //Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(lineStylePen);
            //Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(areaStyleBrush);


        //public void HandleDrawAndEditPolygon() {
        //    if (Map.TrackOverlay.TrackMode == TrackMode.Polygon) {
        //        CancelTrackingModeAndEdit();
        //        return;
        //    }
        //    Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Clear();
        //    Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = lineStylePen;
        //    Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = areaStyleBrush;
        //    Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
        //    Map.TrackOverlay.TrackEnded += new EventHandler<TrackEndedTrackInteractiveOverlayEventArgs>(TrackAndEditOverlay_TrackEnded);
        //    Map.TrackOverlay.TrackMode = TrackMode.Polygon;
        //    Map.MapClick += new EventHandler<MapClickWpfMapEventArgs>(map_MapClick);
        //    Map.KeyDown += new KeyEventHandler(keyEvent_Esc);
        //    previousCursor = Map.Cursor;
        //    Map.Cursor = Cursors.Pen;
        //    CommandDescription = "Draw Polygon";
        //}

        public void HandleDrawAndEditPolygon() {
            if (Map.TrackOverlay.TrackMode == TrackMode.Polygon) {
                CancelTrackingModeAndEdit();
                return;
            }
            Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Clear();
            Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = null;
            Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = null;
            Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = null;
            Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.DefaultTextStyle = null;
            Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(lineStylePen);
            Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(areaStyleBrush);
            Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customDistancePolygonStyleText);
            Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customDistanceLineStyleText);
            Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            Map.TrackOverlay.TrackEnded += new EventHandler<TrackEndedTrackInteractiveOverlayEventArgs>(TrackAndEditOverlay_TrackEnded);
            Map.TrackOverlay.TrackMode = TrackMode.Polygon;
            Map.MapClick += new EventHandler<MapClickWpfMapEventArgs>(map_MapClick);
            Map.KeyDown += new KeyEventHandler(keyEvent_Esc);
            previousCursor = Map.Cursor;
            Map.Cursor = Cursors.Pen;
            CommandDescription = Strings.DrawPolygon_Text;
        }

        //public void HandleDrawAndEditPolygon() {
        //    if (Map.TrackOverlay.TrackMode == TrackMode.Polygon) {
        //        CancelTrackingModeAndEdit();
        //        return;
        //    }
        //    //Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Clear();
        //    //Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = lineStylePen;
        //    //Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = areaStyleBrush;
        //    //Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
        //    //Map.TrackOverlay.TrackEnded += new EventHandler<TrackEndedTrackInteractiveOverlayEventArgs>(TrackAndEditOverlay_TrackEnded);
        //    //Map.TrackOverlay.TrackMode = TrackMode.Polygon;
        //    //Map.MapClick += new EventHandler<MapClickWpfMapEventArgs>(map_MapClick);
        //    //Map.KeyDown += new KeyEventHandler(keyEvent_Esc);
        //    //previousCursor = Map.Cursor;
        //    //Map.Cursor = Cursors.Pen;
        //    //CommandDescription = "Draw Polygon";

        //    TrackPolygonOverlay trackpolygonOverlay = new TrackPolygonOverlay();
        //    trackpolygonOverlay.ControlPointStyle = new PointStyle(PointSymbolType.Circle, new GeoSolidBrush(GeoColor.StandardColors.PaleGoldenrod), new GeoPen(GeoColor.StandardColors.Black), 8);
        //    trackpolygonOverlay.DraggedControlPointStyle = new PointStyle(PointSymbolType.Circle, new GeoSolidBrush(GeoColor.StandardColors.Red), new GeoPen(GeoColor.StandardColors.Orange, 2), 10);
        //    trackpolygonOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = null;
        //    trackpolygonOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = null;
        //    trackpolygonOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = null;
        //    trackpolygonOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.DefaultTextStyle = null;
        //    trackpolygonOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Clear();
        //    trackpolygonOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(lineStylePen);
        //    trackpolygonOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(areaStyleBrush);
        //    trackpolygonOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customAreaPolygonStyleText);
        //    trackpolygonOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
        //    Map.TrackOverlay = trackpolygonOverlay;
        //    Map.TrackOverlay.TrackMode = TrackMode.Polygon;
        //    Map.MapClick += new EventHandler<MapClickWpfMapEventArgs>(map_MapClick);
        //    Map.KeyDown += new KeyEventHandler(keyEvent_Esc);
        //    previousCursor = Map.Cursor;
        //    Map.Cursor = Cursors.Pen;
        //    CommandDescription = "Draw Polygon";
        //}

        private void TrackAndEditOverlay_TrackEnded(object sender, TrackEndedTrackInteractiveOverlayEventArgs e) {
            Map.TrackOverlay.TrackEnded -= new EventHandler<TrackEndedTrackInteractiveOverlayEventArgs>(TrackAndEditOverlay_TrackEnded);
            Map.TrackOverlay.TrackMode = TrackMode.None;
            Map.MapClick -= new EventHandler<MapClickWpfMapEventArgs>(map_MapClick);
            Map.KeyDown -= new KeyEventHandler(keyEvent_Esc);
            Map.Cursor = previousCursor;

            if (Map.EditOverlay.EditShapesLayer.InternalFeatures.Count == 0) {
                HandleShapeEdit();
            }

            if (Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Count > 0) {
                MultipolygonShape editmps = new MultipolygonShape();
                foreach (Feature feat in Map.EditOverlay.EditShapesLayer.InternalFeatures) {
                    BaseShape bs = feat.GetShape();
                    if (bs is PolygonShape) {
                        editmps.Polygons.Add(bs as PolygonShape);
                    }
                    if (bs is MultipolygonShape) {
                        MultipolygonShape mps2 = new MultipolygonShape();
                        mps2 = bs as MultipolygonShape;
                        foreach (PolygonShape ps in mps2.Polygons) {
                            editmps.Polygons.Add(ps);
                        }
                    }
                }
                MultipolygonShape mps = new MultipolygonShape();
                foreach (Feature feat in Map.TrackOverlay.TrackShapeLayer.InternalFeatures) {
                    BaseShape bs = feat.GetShape();
                    if (bs is PolygonShape) {
                        mps.Polygons.Add(bs as PolygonShape);
                    }
                    if (bs is MultipolygonShape) {
                        MultipolygonShape mps2 = new MultipolygonShape();
                        mps2 = bs as MultipolygonShape;
                        foreach (PolygonShape ps in mps2.Polygons) {
                            mps.Polygons.Add(ps);
                        }
                    }
                }
                MultipolygonShape multipoly = AddShapesAndCutOuts(editmps, mps);

                //map.EditOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = null;
                //map.EditOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = null;
                //map.EditOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = null;
                //map.EditOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.DefaultTextStyle = null;
                //map.EditOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Clear();
                //map.EditOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customAreaPolygonStyleText);
                //map.EditOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(areaStyleBrush);
                //map.EditOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;

                Map.EditOverlay.EditShapesLayer.InternalFeatures.Clear();
                Map.EditOverlay.EditShapesLayer.InternalFeatures.Add(new Feature(multipoly));
                Map.EditOverlay.CalculateAllControlPoints();
                Map.EditOverlay.Refresh();
                Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Clear();
                Map.Cursor = Cursors.Hand;

                try {
                    var shapelayerfeatures = Map.EditOverlay.EditShapesLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);
                    if (shapelayerfeatures.Count > 0) {
                        RectangleShape extent = ExtentHelper.GetBoundingBoxOfItems(shapelayerfeatures);
                        Map.CurrentExtent = extent;
                        PointShape center = extent.GetCenterPoint();
                        Map.CenterAt(center);
                    }
                }
                catch (Exception ex) {
                    log.ErrorException("Caught an exception while track ended", ex);
                    //System.Windows.MessageBox.Show("ImportTools - Caught an exception while working with a simple draw shape.");
                }



                Map.Refresh();
                //map.TrackOverlay = new TrackInteractiveOverlay();
                Map.Cursor = Cursors.Hand;
                HasUnsavedChanges = true;
                CommandDescription = Strings.EditShape_Text;
            }
        }

        private void HandleClearEditOverlay() {
            Map.TrackOverlay.Refresh();
            if (Map.EditOverlay.EditShapesLayer.InternalFeatures.Count > 1) {
                Map.EditOverlay.EditShapesLayer.InternalFeatures.RemoveAt(Map.EditOverlay.EditShapesLayer.InternalFeatures.Count - 1);
            }
            else {
                Map.EditOverlay.EditShapesLayer.InternalFeatures.Clear();
            }
            Map.EditOverlay.CalculateAllControlPoints();
            Map.EditOverlay.Refresh();
        }

        private void ClearCurrentLayer() {
            currentLayer.InternalFeatures.Clear();
            try {
                Map.Overlays["fieldOverlay"].Refresh();
            }
            catch { }
        }

        private void ClearShape() {
            System.Windows.MessageBoxResult result = System.Windows.MessageBox.Show(Strings.AreYouSureYouWantToDeleteShape_Text, Strings.ConfirmDeleteShape_Text, System.Windows.MessageBoxButton.OKCancel, System.Windows.MessageBoxImage.Information, System.Windows.MessageBoxResult.Cancel, (System.Windows.MessageBoxOptions)0);
            if (result == System.Windows.MessageBoxResult.OK || result == System.Windows.MessageBoxResult.Yes) {
                //currentLayer.InternalFeatures.Clear();
                Map.EditOverlay.EditShapesLayer.InternalFeatures.Clear();
                Map.EditOverlay.CalculateAllControlPoints();
                Map.Refresh();
            }
        }

        public void HandleSubractShape() {
            Map.TrackOverlay.Refresh();
            HandleDeleteAShape();
            //Map.EditOverlay.CalculateAllControlPoints();
            //Map.EditOverlay.Refresh();
            CommandDescription = Strings.RemoveAShape_Text;
        }

        public void HandleDeleteAShape() {
            Map.MapClick += new EventHandler<MapClickWpfMapEventArgs>(Map_DeleteAShape);
            Map.Cursor = Cursors.Hand;
            //parent.IsMeasureLine = false;
            //CommandDescription = "Delete Shape";

        }

        private void Map_DeleteAShape(object sender, MapClickWpfMapEventArgs e) {
            Map.MapClick -= new EventHandler<MapClickWpfMapEventArgs>(Map_DeleteAShape);
            Map.TrackOverlay.TrackMode = TrackMode.None;
            Map.Cursor = previousCursor;
            //parent.HasUnsavedChanges = true;
            DeleteAShape(e.WorldLocation);
            //parent.IsMeasureLine = false;
            Map.Cursor = Cursors.Hand;
            HasUnsavedChanges = true;
            CommandDescription = Strings.EditShape_Text;
        }

        void DeleteAShape(PointShape worldlocation) {
            bool foundashape = false;
            MultipolygonShape currentPolygonShape = new MultipolygonShape();
            Feature newfeature = new Feature();
            for (int x = 0; x < Map.EditOverlay.EditShapesLayer.InternalFeatures.Count; x++) {
                if (foundashape) {
                    break;
                }
                currentPolygonShape = new MultipolygonShape();
                Feature feature = Map.EditOverlay.EditShapesLayer.InternalFeatures[x];
                var shape = Map.EditOverlay.EditShapesLayer.InternalFeatures[x].GetShape();
                if (shape is MultipolygonShape) {
                    currentPolygonShape = shape as MultipolygonShape;
                }
                else if (shape is PolygonShape) {
                    currentPolygonShape.Polygons.Add(shape as PolygonShape);
                }
                try {
                    for (int i = 0; i < currentPolygonShape.Polygons.Count; i++) {
                        if (foundashape) { break; }
                        for (int j = 0; j < currentPolygonShape.Polygons[i].InnerRings.Count; j++) {
                            if (foundashape) {
                                break;
                            }
                            if (currentPolygonShape.Polygons[i].InnerRings[j].Contains(worldlocation)) {
                                currentPolygonShape.Polygons[i].InnerRings.RemoveAt(j);
                                foundashape = true;
                                break;
                            }
                        }
                        if (foundashape) {
                            break;
                        }
                        if (currentPolygonShape.Polygons[i].OuterRing.Contains(worldlocation)) {
                            currentPolygonShape.Polygons.RemoveAt(i);
                            foundashape = true;
                            break;
                        }
                    }
                    if (foundashape) {
                        break;
                    }
                }
                catch (Exception ex) {
                    log.ErrorException("There was a problem deleting a shape.", ex);
                }
            }
            if (foundashape) {
                Map.EditOverlay.EditShapesLayer.InternalFeatures.Clear();
                if (currentPolygonShape.Polygons.Count == 0) {
                    //currentLayer.InternalFeatures.Clear();
                }
                else {
                    Map.EditOverlay.EditShapesLayer.InternalFeatures.Add(new Feature(currentPolygonShape));
                }
                Map.EditOverlay.CalculateAllControlPoints();
                Map.EditOverlay.Refresh();
            }
        }

        private void CancelTrackingModeAndEdit() {
            if (Map.TrackOverlay.TrackMode != TrackMode.None) {
                Map.TrackOverlay.TrackEnded -= new EventHandler<TrackEndedTrackInteractiveOverlayEventArgs>(TrackOverlay_TrackEnded);
                Map.KeyDown -= new KeyEventHandler(keyEvent_Esc);

                if (Map.TrackOverlay.TrackMode == TrackMode.Polygon || Map.TrackOverlay.TrackMode == TrackMode.Line || Map.TrackOverlay.TrackMode == TrackMode.Circle) {
                    Map.TrackOverlay.MouseDoubleClick(new InteractionArguments());
                }
                else {
                    Map.TrackOverlay.MouseUp(new InteractionArguments());
                }

                Map.TrackOverlay.TrackMode = TrackMode.None;
                Map.Cursor = previousCursor;
                //parent.HasUnsavedChanges = false;
                //parent.ShowDeleteCommand = false;

                int count = Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Count;
                if (count > 0) {
                    Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Remove("InTrackingFeature");
                }
                Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Clear();
                try {
                    Map.TrackOverlay.Refresh();
                }
                catch { }
                //map.TrackOverlay = new TrackInteractiveOverlay();

                try {
                    Map.EditOverlay.EditShapesLayer.InternalFeatures.Clear();
                    Map.EditOverlay.CalculateAllControlPoints();
                    Map.EditOverlay.Refresh();
                }
                catch { }
            }
            if (Map.TrackOverlay.TrackMode == TrackMode.None) {
                try {
                    Map.EditOverlay.EditShapesLayer.InternalFeatures.Clear();
                    Map.EditOverlay.CalculateAllControlPoints();
                    Map.EditOverlay.Refresh();
                    Map.Cursor = previousCursor;
                }
                catch { }
            }
            //UnLoadDimensionLayer();
            //map.ZoomOut(10);
            //map.Refresh();
        }

        // MERGING SHAPES AND HOLES
        private static MultipolygonShape AddShapesAndCutOuts(MultipolygonShape multiPolygonShape, MultipolygonShape multipoly) {
            try {
                if (multiPolygonShape.Polygons.Count > 0 && multipoly.Polygons.Count > 0 && multiPolygonShape.Contains(multipoly)) {
                    MultipolygonShape resultingmulti = multiPolygonShape.GetDifference(multipoly);
                    multiPolygonShape = resultingmulti;
                }
                else if (multiPolygonShape.Polygons.Count > 0 && multipoly.Polygons.Count > 0 && multiPolygonShape.Overlaps(multipoly)) {
                    MultipolygonShape resultingmulti = multiPolygonShape.GetDifference(multipoly);
                    multiPolygonShape = resultingmulti;
                }
                else {
                    foreach (PolygonShape poly in multipoly.Polygons) {
                        multiPolygonShape.Polygons.Add(poly);
                    }
                }
            }
            catch (Exception ex) {
                //log.ErrorException("There was a problem deleting a shape.", ex);
            }

            return multiPolygonShape;
        }

        #endregion

        #region Edit

        // START EDIT MODE
        public void HandleShapeEdit() {
            //LayerOverlay fieldOverlay = (LayerOverlay)Map.Overlays["fieldOverlay"];
            //FeatureLayer currentLayer = (FeatureLayer)fieldOverlay.Layers["currentLayer"];
            EnsureCurrentLayerIsOpen();
            System.Collections.ObjectModel.Collection<ThinkGeo.MapSuite.Core.Feature> selectedFeatures = currentLayer.FeatureSource.GetAllFeatures(ThinkGeo.MapSuite.Core.ReturningColumnsType.AllColumns);
            Map.EditOverlay.EditShapesLayer.InternalFeatures.Clear();
            if (selectedFeatures.Count > 0) {

                //map.EditOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = null;
                //map.EditOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = null;
                //map.EditOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = null;
                //map.EditOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.DefaultTextStyle = null;
                //map.EditOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Clear();
                //map.EditOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customAreaPolygonStyleText);
                //map.EditOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(areaStyleBrush);
                //map.EditOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;

                Map.EditOverlay.EditShapesLayer.InternalFeatures.Add(selectedFeatures[0]);
                Map.EditOverlay.CalculateAllControlPoints();
                Map.EditOverlay.Refresh();
                Map.Cursor = Cursors.Hand;
                HasUnsavedChanges = true;
                CommandDescription = Strings.EditShape_Text;
            }
        }

        #endregion

        #region Delete

        // DELETE SHAPE
        public void HandleDeleteShape() {
            Map.MapClick += new EventHandler<MapClickWpfMapEventArgs>(Map_DeleteShapeDoubleClick);
            Map.Cursor = Cursors.Hand;
        }

        private void Map_DeleteShapeDoubleClick(object sender, MapClickWpfMapEventArgs e) {
            Map.MapClick -= new EventHandler<MapClickWpfMapEventArgs>(Map_DeleteShapeDoubleClick);
            Map.TrackOverlay.TrackMode = TrackMode.None;
            Map.Cursor = previousCursor;
            DeleteShape(e.WorldLocation);
        }

        void DeleteShape(PointShape eshape) {
            PointShape testpoint = new PointShape();
            if (eshape == null) { return; }
            if (eshape == testpoint) { return; }
            try {
                if (Map.TrackOverlay.TrackMode != TrackMode.None) { return; }
                EnsureCurrentLayerIsOpen();
                try {
                    Collection<Feature> selectedFeatures = currentLayer.QueryTools.GetFeaturesContaining(eshape, ReturningColumnsType.AllColumns);
                    PointShape worldlocation = eshape as PointShape;
                    PointShape latlonlocation = projection.ConvertToInternalProjection(worldlocation) as PointShape;
                    if (selectedFeatures.Count > 0) {
                        for (int i = 0; i < currentLayer.InternalFeatures.Count; i++) {
                            Feature feat = currentLayer.InternalFeatures[i];
                            MultipolygonShape multipoly = feat.GetShape() as MultipolygonShape;
                            if (multipoly.Polygons.Count > 0) {
                                if (multipoly.Contains(latlonlocation)) {
                                    currentLayer.InternalFeatures.RemoveAt(i);
                                    HasUnsavedChanges = true;
                                    break;
                                }
                            }
                        }
                        Map.Refresh();
                    }
                }
                catch (Exception ex) {
                    log.ErrorException("Caught an exception while deleting a shape", ex);
                    //System.Windows.MessageBox.Show("ImportTools - Caught an exception while working with a simple draw shape.");
                }

            }
            catch (Exception ex) {
                log.ErrorException("Caught an exception while working with a draw edit shape", ex);
                System.Windows.MessageBox.Show($"{Strings.ImportTools_Text} - {Strings.CaughtAnExceptionWhileWorkingWithDrawEditShape_Text}");
            }
        }

        #endregion

        #region  Cancel

        // CANCEL
        void Cancel() {
            OnCancelCropzone();
        }

        void OnCancelCropzone() {
            operatingmode = 0;
            ReSetMap();
            Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Clear();
            Map.EditOverlay.EditShapesLayer.InternalFeatures.Clear();
            Map.EditOverlay.CalculateAllControlPoints();
            Map.Cursor = Cursors.Arrow;
            scalingTextStyle.MapScale = Map.CurrentScale;
            Map.Refresh();
            Messenger.Default.Send<ShowMainViewMessage>(new ShowMainViewMessage());
        }

        #endregion

        #region Finished

        // FINISHED
        void Finished() {
            modeofoperation = Strings.SavingMode_Text;
            IsAssigningMode = false;
            Map.Cursor = Cursors.Arrow;
            //OnCompleteCropzone();
            SavePolygon();
        }

        void OnCompleteCropzone() {
            var sortedcropzones = from f in SelectedCropZones
                                  orderby f.Name
                                  select f;

            foreach (var ccvm in sortedcropzones) {
                if (ccvm.Completed.HasValue && ccvm.Completed.Value) {
                    foreach (var idc in ccvm.CommandList) {
                        clientEndpoint.SendOne(idc);
                    }
                    foreach (var newItem in ccvm.Nodelist) {
                        FieldTreeItemVM.Children.Add(newItem);
                        FieldTreeItemVM.IsExpanded = true;
                    }
                }
            }
            operatingmode = 0;
            ReSetMap();
            Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Clear();
            Map.EditOverlay.EditShapesLayer.InternalFeatures.Clear();
            Map.EditOverlay.CalculateAllControlPoints();
            scalingTextStyle.MapScale = Map.CurrentScale;
            Map.Refresh();
            Map.Cursor = Cursors.Arrow;
            Messenger.Default.Send<ShowMainViewMessage>(new ShowMainViewMessage());
        }

        private void SavePolygon() {
            MultipolygonShape multipoly = new MultipolygonShape();
            try {
                if (Map.EditOverlay.EditShapesLayer.InternalFeatures.Count > 0) {
                    foreach (Feature feature in Map.EditOverlay.EditShapesLayer.InternalFeatures) {
                        BaseShape baseShape = feature.GetShape();
                        MultipolygonShape basepoly = new MultipolygonShape();
                        if (baseShape is PolygonShape) {
                            PolygonShape ps = baseShape as PolygonShape;
                            basepoly.Polygons.Add(ps);
                        }
                        if (baseShape is MultipolygonShape) {
                            basepoly = baseShape as MultipolygonShape;
                        }
                        if (multipoly.Polygons.Count == 0) {
                            multipoly = basepoly;
                            continue;
                        }
                        if (feature is AreaBaseShape) {
                            MultipolygonShape multipoly2 = new MultipolygonShape(multipoly.Polygons);
                            multipoly = multipoly2.Union(feature);
                        }
                    }
                }
                if (multipoly.Polygons.Count > 0) {
                    var importmessage = new DrawEditDeleteMessage() { ShapeBase = multipoly };
                    Messenger.Default.Send<DrawEditDeleteMessage>(importmessage);
                }
            }
            catch (Exception ex) {
                log.Error("DrawTools - Error save polygon - {0}", ex);
                System.Windows.MessageBox.Show($"{Strings.DrawTools_Text} - {Strings.CaughtAnExceptionWhileSavingPolygon_Text}");
            }
        }

        #endregion

        #region Escape Key Event

        private void keyEvent_Esc(object sender, KeyEventArgs e) {
            if (e.Key == System.Windows.Input.Key.Escape) {
                if (Map.TrackOverlay.TrackMode != TrackMode.None) {
                    //Map.TrackOverlay.TrackEnded -= new EventHandler<TrackEndedTrackInteractiveOverlayEventArgs>(CutLine_TrackEnded);
                    Map.MapClick -= new EventHandler<MapClickWpfMapEventArgs>(map_MapClick);
                    Map.KeyDown -= new KeyEventHandler(keyEvent_Esc);

                    if (Map.TrackOverlay.TrackMode == TrackMode.Polygon || Map.TrackOverlay.TrackMode == TrackMode.Line || Map.TrackOverlay.TrackMode == TrackMode.Circle) {
                        Map.TrackOverlay.MouseDoubleClick(new InteractionArguments());
                    }
                    else {
                        Map.TrackOverlay.MouseUp(new InteractionArguments());
                    }

                    Map.TrackOverlay.TrackMode = TrackMode.None;
                    Map.Cursor = previousCursor;

                    int count = Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Count;
                    if (count > 0) {
                        Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Remove("InTrackingFeature");
                    }
                    Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Clear();
                    Map.TrackOverlay.Refresh();

                }
            }
        }

        #endregion


        #region Measure

        public void HandleMeasureLine() {
            //RullerTrackInteractiveOverlay rulerTrackInteractiveOverlay = new RullerTrackInteractiveOverlay();
            //Map.TrackOverlay = rulerTrackInteractiveOverlay;
            //Map.TrackOverlay.TrackMode = TrackMode.Line;

            //MapStyles.CustomDistancePolygonStyle customDistancePolygonStyleText = new MapStyles.CustomDistancePolygonStyle(mapAreaUnit, mapDistanceUnit, new GeoFont("Arial", measurefontsize, DrawingFontStyles.Regular), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.White), new GeoSolidBrush(GeoColor.StandardColors.Red), new GeoPen(GeoColor.StandardColors.Red));
            //customDistancePolygonStyleText.YOffsetInPixel = -20;
            //MapStyles.CustomDistanceLineStyle customDistanceLineStyleText = new MapStyles.CustomDistanceLineStyle(mapDistanceUnit, new GeoFont("Arial", measurefontsize, DrawingFontStyles.Regular), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.DarkRed, 2), new GeoPen(GeoColor.StandardColors.DarkBlue, 2));
            //LineStyle lineStylePen = new LineStyle(new GeoPen(GeoColor.FromArgb(150, GeoColor.StandardColors.Blue), 2));
            Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = null;
            Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = null;
            Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = null;
            Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.DefaultTextStyle = null;
            Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Clear();
            //Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customDistancePolygonStyleText);
            Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customDistanceLineStyleText);
            Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(lineStylePen);
            Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(areaStyleBrush);
            Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            Map.TrackOverlay.TrackEnded += new EventHandler<TrackEndedTrackInteractiveOverlayEventArgs>(TrackOverlay_TrackEnded);
            Map.TrackOverlay.TrackMode = TrackMode.Line;
            Map.MapClick += new EventHandler<MapClickWpfMapEventArgs>(map_MapClick);
            Map.KeyDown += new KeyEventHandler(keyEvent_Esc);
            previousCursor = Map.Cursor;
            Map.Cursor = Cursors.Pen;
        }

        private void TrackOverlay_TrackEnded(object sender, TrackEndedTrackInteractiveOverlayEventArgs e) {
            Map.TrackOverlay.TrackEnded -= new EventHandler<TrackEndedTrackInteractiveOverlayEventArgs>(TrackOverlay_TrackEnded);
            Map.TrackOverlay.TrackMode = TrackMode.None;
            Map.MapClick -= new EventHandler<MapClickWpfMapEventArgs>(map_MapClick);
            Map.KeyDown -= new KeyEventHandler(keyEvent_Esc);
            Map.Cursor = previousCursor;
            SaveMeasureLayer();
        }

        private void map_MapClick(object sender, MapClickWpfMapEventArgs e) {
            if (!Map.IsFocused) {
                Map.Focus();
            }
        }

        internal void SaveMeasureLayer() {
            var features = Map.TrackOverlay.TrackShapeLayer.InternalFeatures;
            Feature feat = features[0]; 
                var shape = feat.GetShape();
                PointShape centerpoint = shape.GetCenterPoint();
                BaseShape shape2 = projection.ConvertToInternalProjection(shape);
                BaseShape bs1 = DisplayMeasureBalloon(shape);
                BaseShape bs2 = projection.ConvertToInternalProjection(bs1);
                measureLayer.InternalFeatures.Add(new Feature(bs2));
                double areaTotal = 0;
                double perimeterTotal = 0;
                string displayerText = string.Empty;
                if (bs2 is PolygonShape) {
                    measureLayer.InternalFeatures.Add(new Feature(shape2));
                    PolygonShape poly1 = bs2 as PolygonShape;
                    areaTotal = poly1.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
                    perimeterTotal = poly1.GetPerimeter(GeographyUnit.DecimalDegree, mapDistanceUnit);
                    Measure perimetermeasure = MapUnitFactory.LengthConversion(mapDistanceUnit.ToString(), perimeterTotal);
                    Measure areameasure = MapUnitFactory.AreaConversion(mapAreaUnit.ToString(), areaTotal);
                    displayerText = $"{Strings.Area_Text}: {areameasure.FullDisplay} \r\n{Strings.Perimeter_Text}: {perimetermeasure.FullDisplay}";
                }
                if (bs2 is LineShape) {
                    LineShape line1 = bs2 as LineShape;
                    perimeterTotal = line1.GetLength(GeographyUnit.DecimalDegree, mapDistanceUnit);
                    Measure perimetermeasure = MapUnitFactory.LengthConversion(mapDistanceUnit.ToString(), perimeterTotal);
                    displayerText = $"{Strings.Length_Text}: {perimetermeasure.FullDisplay}";
                    LineShape lineshape2 = shape as LineShape;
                    for (int xyz = 0; xyz <= (lineshape2.Vertices.Count / 2); xyz++) {
                        centerpoint = new PointShape(lineshape2.Vertices[xyz]);
                    }
                }
                Popup popup = new Popup(centerpoint);
                popup.Loaded += new System.Windows.RoutedEventHandler(popup_Loaded);
                popup.MouseDoubleClick += new MouseButtonEventHandler(mouse_DoubleClick);
                System.Windows.Controls.TextBox displayer = new System.Windows.Controls.TextBox();
                displayer.Text = displayerText;
                popup.Content = displayer;
                //popup.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(255, 0, 0));
                popupOverlay.Popups.Add(popup);
                popupOverlay.Refresh();
            
            Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Clear();
            Map.Refresh();
        }

        void popup_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            Popup p = sender as Popup;
            //System.Windows.Controls.Border b = p.Template.FindName("BorderTemplate", p) as System.Windows.Controls.Border;
            //b.CornerRadius = new System.Windows.CornerRadius(10);
        }

        void mouse_DoubleClick(object sender, MouseButtonEventArgs e) {
            Popup p = sender as Popup;
            if (popupOverlay.Popups.Contains(p)) {
                popupOverlay.Popups.Remove(p);
                popupOverlay.Refresh();
            }
        }

        private BaseShape DisplayMeasureBalloon(BaseShape shape) {
            if (shape is LineBaseShape) {
                if (shape is LineShape) {
                    LineShape lineshape = shape as LineShape;
                    var firstvert = lineshape.Vertices.First();
                    var lastvert = lineshape.Vertices.Last();
                    var allverts = from vt in lineshape.Vertices
                                   select vt;

                    PointShape point1 = new PointShape(firstvert);
                    PointShape point2 = new PointShape(lastvert);
                    if (lineshape.Vertices.Count > 2) {
                        ScreenPointF screenPoint1 = ExtentHelper.ToScreenCoordinate(Map.CurrentExtent, point1, System.Convert.ToSingle(Map.ActualWidth), System.Convert.ToSingle(Map.ActualHeight));
                        ScreenPointF screenPoint2 = ExtentHelper.ToScreenCoordinate(Map.CurrentExtent, point2, System.Convert.ToSingle(Map.ActualWidth), System.Convert.ToSingle(Map.ActualHeight));
                        double distance = Math.Sqrt(((screenPoint1.X - screenPoint2.X) * (screenPoint1.X - screenPoint2.X)) + ((screenPoint1.Y - screenPoint2.Y) * (screenPoint1.Y - screenPoint2.Y)));
                        if (distance >= -10 && distance <= 10) {
                            PolygonShape polygon = new PolygonShape();
                            Collection<Vertex> collectvertex = lineshape.Vertices;
                            collectvertex.Add(lineshape.Vertices[0]);
                            RingShape rs = new RingShape(collectvertex);
                            polygon.OuterRing = rs;
                            return polygon;
                        }
                    }
                    return lineshape;
                }
                if (shape is MultilineShape) {
                    MultilineShape mlshape = shape as MultilineShape;
                    var firstvert = mlshape.Lines.First().Vertices.First();
                    var lastvert = mlshape.Lines.Last().Vertices.Last();
                    var allverts = from ml in mlshape.Lines
                                   from vt in ml.Vertices
                                   select vt;

                    PointShape point1 = new PointShape(firstvert);
                    PointShape point2 = new PointShape(lastvert);
                    if (allverts.Count() > 2) {
                        ScreenPointF screenPoint1 = ExtentHelper.ToScreenCoordinate(Map.CurrentExtent, point1, System.Convert.ToSingle(Map.ActualWidth), System.Convert.ToSingle(Map.ActualHeight));
                        ScreenPointF screenPoint2 = ExtentHelper.ToScreenCoordinate(Map.CurrentExtent, point2, System.Convert.ToSingle(Map.ActualWidth), System.Convert.ToSingle(Map.ActualHeight));
                        double distance = Math.Sqrt(((screenPoint1.X - screenPoint2.X) * (screenPoint1.X - screenPoint2.X)) + ((screenPoint1.Y - screenPoint2.Y) * (screenPoint1.Y - screenPoint2.Y)));
                        if (distance >= -10 && distance <= 10) {
                            PolygonShape polygon = new PolygonShape();
                            polygon.OuterRing = new RingShape(allverts);
                            return polygon;
                        }
                    }
                    return mlshape;
                }
            }
            return null;
        }

        #endregion

        internal void CalculateDimensionLayer(PointShape movingpoint, PointShape movedpoint) {
            if (calculatingdimensions) { return; }
            if (ismeasureline) { return; }
            calculatingdimensions = true;
            if (Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Count > 0) {
                BaseShape baseShape1 = Map.TrackOverlay.TrackShapeLayer.InternalFeatures[0].GetShape();
                BaseShape baseShape = projection.ConvertToInternalProjection(baseShape1);
                if (baseShape is PolygonShape) {
                    PolygonShape polyShape = baseShape as PolygonShape;
                    if (polyShape.OuterRing.Vertices.Count > 1) {
                        LineShape lineShape = new LineShape();
                        foreach (Vertex vert in polyShape.OuterRing.Vertices) {
                            lineShape.Vertices.Add(vert);
                        }
                        dimensionLayer.InternalFeatures.Clear();
                        dimensionLayer.InternalFeatures.Add(new Feature(polyShape));
                        //dimensionLayer.InternalFeatures.Add(new Feature(lineShape));
                    }
                }
            }
            if (Map.EditOverlay.EditShapesLayer.InternalFeatures.Count > 0) {
                BaseShape baseShape1 = Map.EditOverlay.EditShapesLayer.InternalFeatures[0].GetShape();
                BaseShape baseShape = projection.ConvertToInternalProjection(baseShape1);
                MultipolygonShape multipoly = new MultipolygonShape();
                if (baseShape is MultipolygonShape) {
                    multipoly = baseShape as MultipolygonShape;
                }
                if (baseShape is PolygonShape) {
                    PolygonShape poly = baseShape as PolygonShape;
                    multipoly.Polygons.Add(poly);
                }
                dimensionLayer.InternalFeatures.Clear();
                dimensionLayer.InternalFeatures.Add(new Feature(multipoly));
                LineShape lineShape = new LineShape();
                if (movingpoint != null || movedpoint != null) {
                    try {
                        Vertex foundvertex = new Vertex();
                        if (movingpoint != null && movedpoint == null) {
                            try {
                                PointShape foundpoint = multipoly.GetClosestPointTo(movingpoint, Map.MapUnit);
                                if (foundpoint == null) {
                                    foundpoint = movingpoint;
                                }
                                foundvertex = new Vertex(foundpoint.X, foundpoint.Y);
                            }
                            catch (Exception ex1) {
                                int aaa = 0;
                                calculatingdimensions = false;
                                return;
                            }
                        }
                        if (movedpoint != null && movingpoint == null) {
                            try {
                                foundvertex = new Vertex(movedpoint.X, movedpoint.Y);
                            }
                            catch (Exception ex2) {
                                int bbb = 0;
                                calculatingdimensions = false;
                                return;
                            }
                        }
                        foreach (PolygonShape polyShape in multipoly.Polygons) {
                            if (polyShape.OuterRing.Vertices.Count > 2) {
                                for (int i = 0; i < polyShape.OuterRing.Vertices.Count; i++) {
                                    if (polyShape.OuterRing.Vertices[i] == foundvertex) {
                                        if (i == 0) {
                                            lineShape = new LineShape();
                                            lineShape.Vertices.Add(polyShape.OuterRing.Vertices[polyShape.OuterRing.Vertices.Count - 1]);
                                            lineShape.Vertices.Add(polyShape.OuterRing.Vertices[i]);
                                            lineShape.Vertices.Add(polyShape.OuterRing.Vertices[i + 1]);
                                            break;
                                        }
                                        if (i == polyShape.OuterRing.Vertices.Count - 1) {
                                            lineShape = new LineShape();
                                            lineShape.Vertices.Add(polyShape.OuterRing.Vertices[i - 1]);
                                            lineShape.Vertices.Add(polyShape.OuterRing.Vertices[i]);
                                            lineShape.Vertices.Add(polyShape.OuterRing.Vertices[0]);
                                            break;
                                        }
                                        lineShape = new LineShape();
                                        lineShape.Vertices.Add(polyShape.OuterRing.Vertices[i - 1]);
                                        lineShape.Vertices.Add(polyShape.OuterRing.Vertices[i]);
                                        lineShape.Vertices.Add(polyShape.OuterRing.Vertices[i + 1]);
                                        break;

                                    }
                                }
                            }
                        }
                        if (lineShape.Vertices.Count == 0 && savededitoverlaypoly != null && savededitoverlaypoly.Polygons.Count > 0 && savededitoverlaypoly.Polygons.Count == multipoly.Polygons.Count) {
                            for (int x = 0; x < multipoly.Polygons.Count; x++) {
                                for (int y = 0; y < multipoly.Polygons[x].OuterRing.Vertices.Count; y++) {
                                    if (multipoly.Polygons[x].OuterRing.Vertices[y] != savededitoverlaypoly.Polygons[x].OuterRing.Vertices[y]) {
                                        if (y == 0) {
                                            lineShape = new LineShape();
                                            lineShape.Vertices.Add(multipoly.Polygons[x].OuterRing.Vertices[multipoly.Polygons[x].OuterRing.Vertices.Count - 1]);
                                            lineShape.Vertices.Add(multipoly.Polygons[x].OuterRing.Vertices[0]);
                                            lineShape.Vertices.Add(multipoly.Polygons[x].OuterRing.Vertices[1]);
                                            break;
                                        }
                                        if (y == multipoly.Polygons[x].OuterRing.Vertices.Count - 1) {
                                            lineShape = new LineShape();
                                            lineShape.Vertices.Add(multipoly.Polygons[x].OuterRing.Vertices[multipoly.Polygons[x].OuterRing.Vertices.Count - 2]);
                                            lineShape.Vertices.Add(multipoly.Polygons[x].OuterRing.Vertices[multipoly.Polygons[x].OuterRing.Vertices.Count - 1]);
                                            lineShape.Vertices.Add(multipoly.Polygons[x].OuterRing.Vertices[0]);
                                            break;
                                        }
                                        lineShape = new LineShape();
                                        lineShape.Vertices.Add(multipoly.Polygons[x].OuterRing.Vertices[y - 1]);
                                        lineShape.Vertices.Add(multipoly.Polygons[x].OuterRing.Vertices[y]);
                                        lineShape.Vertices.Add(multipoly.Polygons[x].OuterRing.Vertices[y + 1]);
                                        break;

                                    }
                                }
                            }
                        }
                        dimensionLayer.InternalFeatures.Add(new Feature(lineShape));
                        savededitoverlaypoly = multipoly;
                    }
                    catch (Exception ex) {
                        int x = 0;
                        calculatingdimensions = false;
                        return;
                    }

                }
            }

            if (Map.Overlays.Contains("dimensionOverlay")) {
                Map.Overlays["dimensionOverlay"].Refresh();
            }
            calculatingdimensions = false;
        }

        internal void CalculateDimensionLayer() {

        }
    }
}
