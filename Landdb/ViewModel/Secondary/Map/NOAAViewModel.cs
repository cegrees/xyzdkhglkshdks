﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GalaSoft.MvvmLight;
using ThinkGeo.MapSuite.WpfDesktopEdition;
using ThinkGeo.MapSuite.Core;
using Landdb.Infrastructure;
using System.IO;
using Microsoft.Win32;
using GalaSoft.MvvmLight.Command;
using System.Windows.Input;
using System.Collections.ObjectModel;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure.Messages;
using Landdb.Domain.ReadModels.Tree;
using Landdb.MapStyles;
using Landdb.Client.Infrastructure;
using System.Windows.Threading;
using Landdb.Resources;
using Landdb.ViewModel.Maps.ImageryOverlay;
using NLog;
using Landdb.ViewModel.Fields.FieldDetails.MapModels;

namespace Landdb.ViewModel.Secondary.Map {
    public class NOAAViewModel : ViewModelBase {
        bool maploaded = false;
        bool startedOutOnline = false;
        bool ismousedown = false;
        bool draglabel = true;
        bool hasunsavedchanges = false;
        bool isfieldselected = false;
        string fieldname = Strings.DragLabel_Text;
        double desiredarea = 100;
        double oldarea = 0;
        IList<MapItem> clientLayerItems;
        InMemoryFeatureLayer fieldsLayer = new InMemoryFeatureLayer();
        LayerOverlay layerOverlay = new LayerOverlay();
        Proj4Projection projection;
        Proj4Projection projection2;
        Proj4Projection projection84;
        //RectangleShape previousmapextent = new RectangleShape();
        Feature selectedfield = new Feature();
        Feature loadedfeature = new Feature();
        BaseShape fieldshape = new MultipolygonShape();
        ClientLayerAreaStyle fieldsLayerAreaStyle = null;
        Logger log = LogManager.GetCurrentClassLogger();
        string activemap = string.Empty;
        string activemap1 = "BingRadar";
        string activemap2 = "WorldMapKitRadar";
        ZoomLevelFactory zoomlevelfactory = new ZoomLevelFactory();
        string coordinateformat = string.Empty;

        public NOAAViewModel() {
            CheckForProjectionDLL();
            InitializeMap();
            HasUnsavedChanges = false;

            CancelCommand = new RelayCommand(Cancel);
        }

        private void CheckForProjectionDLL() {
            activemap = activemap2;
            string systemPath = Environment.SystemDirectory;
            TestAndSetMap(systemPath, @"Map Suite 8.0");
            TestAndSetMap(systemPath, @"Map Suite 8.5");
            TestAndSetMap(systemPath, @"Map Suite 9.0");
            TestAndSetMap(systemPath, @"Map Suite 9.5");
            TestAndSetMap(systemPath, @"Map Suite 10.0");
            systemPath = @"C:\WINDOWS\system32";
            TestAndSetMap(systemPath, @"Map Suite 8.0");
            TestAndSetMap(systemPath, @"Map Suite 8.5");
            TestAndSetMap(systemPath, @"Map Suite 9.0");
            TestAndSetMap(systemPath, @"Map Suite 9.5");
            TestAndSetMap(systemPath, @"Map Suite 10.0");
            systemPath = @"C:\WINDOWS\sysWOW64";
            TestAndSetMap(systemPath, @"Map Suite 8.0");
            TestAndSetMap(systemPath, @"Map Suite 8.5");
            TestAndSetMap(systemPath, @"Map Suite 9.0");
            TestAndSetMap(systemPath, @"Map Suite 9.5");
            TestAndSetMap(systemPath, @"Map Suite 10.0");
            systemPath = @"C:\WINDOWS\SysWOW64";
            TestAndSetMap(systemPath, @"Map Suite 8.0");
            TestAndSetMap(systemPath, @"Map Suite 8.5");
            TestAndSetMap(systemPath, @"Map Suite 9.0");
            TestAndSetMap(systemPath, @"Map Suite 9.5");
            TestAndSetMap(systemPath, @"Map Suite 10.0");
        }

        private void TestAndSetMap(string systemPath, string suitePath) {
            string path1 = Path.Combine(systemPath, suitePath);
            if (activemap != "BingRadar") {
                if (Directory.Exists(path1)) {
                    activemap = "BingRadar";
                }
            }
        }
        public ICommand CancelCommand { get; private set; }

        public WpfMap Map { get; set; }

        static object clientLayerLock = new object();
        public IList<MapItem> ClientLayer {
            get { return clientLayerItems; }
            set {
                lock (clientLayerLock) {
                    clientLayerItems = value;
                    LoadFieldLayerShapes();
                    RaisePropertyChanged("ClientLayer");
                }
            }
        }

        public Feature SelectedField {
            get { return selectedfield; }
            set {
                if (selectedfield == value) { return; }
                selectedfield = value;
                RaisePropertyChanged("SelectedField");
                if (selectedfield.Id != loadedfeature.Id) {
                    loadedfeature = selectedfield;
                    fieldshape = selectedfield.GetShape();
                    if (fieldshape is MultipolygonShape) {
                        MultipolygonShape ps = fieldshape as MultipolygonShape;
                    }

                    if (maploaded) {
                        OpenAndLoadaShapefile();
                    }
                }
            }
        }

        public string FieldName {
            get { return fieldname; }
            set {
                if (fieldname == value) { return; }
                fieldname = value;
                RaisePropertyChanged("FieldName");
            }
        }

        public bool HasUnsavedChanges {
            get { return hasunsavedchanges; }
            set {
                if (hasunsavedchanges == value) { return; }
                hasunsavedchanges = value;
                RaisePropertyChanged("HasUnsavedChanges");
            }
        }

        //public RectangleShape PreviousMapExtent
        //{
        //    get { return previousmapextent; }
        //    set
        //    {
        //        if (previousmapextent == value) { return; }
        //        previousmapextent = value;
        //        RaisePropertyChanged("PreviousMapExtent");
        //    }
        //}

        internal void EnsureFieldsLayerIsOpen() {
            if (fieldsLayer == null) {
                fieldsLayer = new InMemoryFeatureLayer();
            }

            if (!fieldsLayer.IsOpen) {
                fieldsLayer.Open();
            }

            if (!projection.IsOpen) {
                projection.Open();
            }

            if (!projection2.IsOpen) {
                projection2.Open();
            }

            if (!projection84.IsOpen) {
                projection84.Open();
            }
        }

        void InitializeMap() {
            Map = new WpfMap();
            startedOutOnline = Landdb.Client.Infrastructure.NetworkStatus.IsInternetAvailableFast();
            //TODO
            if (activemap == "BingRadar") {
                Map.MapUnit = GeographyUnit.Meter;
            }
            else {
                Map.MapUnit = GeographyUnit.DecimalDegree;
            }
            //Map.MapUnit = GeographyUnit.Meter;
            //Map.MapUnit = GeographyUnit.DecimalDegree;
            Map.MapTools.PanZoomBar.Visibility = System.Windows.Visibility.Visible;
            Map.MapTools.PanZoomBar.GlobeButtonVisibility = System.Windows.Visibility.Hidden;
            Map.MapTools.PanZoomBar.VerticalAlignment = System.Windows.VerticalAlignment.Center;
            Map.MapTools.PanZoomBar.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            Map.Background = System.Windows.Media.Brushes.White;
            zoomlevelfactory = new ZoomLevelFactory();
            Map.ZoomLevelSet = zoomlevelfactory.ZoomLevelSET;
            //Map.ExtentOverlay.PanMode = MapPanMode.Disabled;
            Map.ExtentOverlay.PanMode = MapPanMode.Default;

            //projectionstring = Proj4Projection.GetEpsgParametersString(4326);

            projection = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetGoogleMapParametersString());
            projection2 = new Proj4Projection(Proj4Projection.GetGoogleMapParametersString(), Proj4Projection.GetWgs84ParametersString());
            //projection84 = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetWgs84ParametersString());
            projection84 = new Proj4Projection(Proj4Projection.GetEpsgParametersString(4326), Proj4Projection.GetEpsgParametersString(4326));

            NoaaRadarMonitor.RefreshInterval = new TimeSpan(0, 0, 30);
            NoaaRadarMonitor.StartMonitoring();

            Map.Loaded += (sender, e) => {
                if (maploaded) { return; }
                try {
                    //TODO
                    if (activemap == "BingRadar") {
                        LoadBingMapsOverlay();
                    }
                    else {
                        LoadWMSOverlay();
                    }
                    //LoadBingMapsOverlay();
                    //LoadWMSOverlay();
                    OpenAndLoadaShapefile();
                    //NoaaRadarRasterLayer noaa = new NoaaRadarRasterLayer();
                    //noaa.ImageSource.Projection = projection84;

                    AddNoaaWeatherLayer<NoaaRadarRasterLayer>(radarLayer => {
                        radarLayer.Transparency = 50 * 2.55f;
                        //TODO
                        NoaaConfiguration(radarLayer);
                    });
                    System.Threading.Thread.Sleep(1000);
                    maploaded = true;
                }
                catch { }
            };

            Map.CurrentScaleChanged += (sender, e) => {
                //scalingTextStyle.MapScale = e.CurrentScale;
            };

            //Map.MapClick += (sender, e) => {
            //    SavePolygon();
            //};

        }

        private void NoaaConfiguration(NoaaRadarRasterLayer radarLayer) {
                radarLayer.UpperThreshold = Double.MaxValue;
                radarLayer.LowerThreshold = 0;
                if (activemap == "BingRadar") {
                    radarLayer.ImageSource.Projection = projection;
                }
                else {
                    //radarLayer.ImageSource.Projection = projection84;
                }

                //radarLayer.ImageSource.Projection = projection;
                //radarLayer.ImageSource.Projection = projection84;
                radarLayer.DrawingExceptionMode = DrawingExceptionMode.DrawException;
//#if DEBUG
//                radarLayer.DrawingExceptionMode = DrawingExceptionMode.Default;
//#endif

        }


        //private void SavePolygon() {
        //    EnsureFieldsLayerIsOpen();
        //    MultipolygonShape multipoly = new MultipolygonShape();
        //    Collection<Feature> selectedFeatures = fieldsLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);
        //    if (selectedFeatures.Count > 0) {
        //        for (int i = 0; i < fieldsLayer.InternalFeatures.Count; i++) {
        //            Feature feat = fieldsLayer.InternalFeatures[i];
        //            multipoly = feat.GetShape() as MultipolygonShape;
        //            break;
        //        }
        //        var importmessage = new ScalePolygonMessage() { ShapeBase = multipoly };
        //        Messenger.Default.Send<ScalePolygonMessage>(importmessage);
        //    }
        //}

        private void LoadBingMapsOverlay() {
            if (!startedOutOnline) { return; }
            //IMapOverlay mapoverlay = new BingMapOverlay();
            //SelectedOverlay.NewOverlay();
            IMapOverlay mapoverlay = new BingFastRoadMapsOverlay();
            Overlay bingOverlay = mapoverlay.Overlay;
            if (bingOverlay == null) { return; }
            if (Map.Overlays.Contains("Bing")) {
                Map.Overlays.Remove("Bing");
            }
            if (Map.Overlays.Contains("Bing Online")) {
                Map.Overlays.Remove("Bing Online");
            }
            if (Map.Overlays.Contains("Roads Only")) {
                Map.Overlays.Remove("Roads Only");
            }
            if (Map.Overlays.Contains("Road Online")) {
                Map.Overlays.Remove("Road Online");
            }
            if (Map.Overlays.Contains("None")) {
                Map.Overlays.Remove("None");
            }
            //((TileOverlay)bingOverlay).TileType = TileType.SingleTile;
            Map.Overlays.Add(mapoverlay.Name, bingOverlay);
            Map.Overlays.MoveToBottom(mapoverlay.Name);
        }

        private void LoadWMSOverlay() {
            if (!startedOutOnline) { return; }
            IMapOverlay mapoverlay = new WorldMapKitOverlay();
            Overlay bingOverlay = mapoverlay.Overlay;
            if (bingOverlay == null) { return; }
            Map.Overlays.Add("WorldMapKit", bingOverlay);
            //Map.CurrentExtent = new RectangleShape(-155.733, 95.60, 104.42, -81.9);
            //extent = new RectangleShape(-126.4, 48.8, -67.0, 19.0);

        }

        private void LoadFieldLayerShapes() {
            try {
                EnsureFieldsLayerIsOpen();
                fieldsLayer.InternalFeatures.Clear();
                //ieldsLayer.InternalFeatures.Add(SelectedField);
                foreach (var m in clientLayerItems) {
                    if (m.MapData != null && !string.IsNullOrEmpty(m.MapData) && m.MapData != "MULTIPOLYGON EMPTY") {
                        try {
                            IDictionary<string, string> columnvalues = new Dictionary<string, string>();
                            if (string.IsNullOrEmpty(m.Name)) { continue; }
                            if (string.IsNullOrEmpty(m.MapData)) { continue; }
                            if (string.IsNullOrEmpty(m.FieldId.ToString())) { continue; }
                            columnvalues.Add(@"LdbLabel", m.Name);
                            var f = new Feature(m.MapData, m.FieldId.ToString(), columnvalues);
                            if (fieldsLayer.InternalFeatures.Contains(f)) { continue; }
                            fieldsLayer.InternalFeatures.Add(m.FieldId.ToString(), f);
                        }
                        catch { }
                    }
                }

                if (Map.Overlays.Contains("fieldOverlay")) {
                    Map.Overlays["fieldOverlay"].Refresh();
                }
            }
            catch { }
        }

        private void OpenAndLoadaShapefile() {
            HasUnsavedChanges = false;
            try {
                fieldsLayer.InternalFeatures.Clear();
                CreateInMemoryLayerAndOverlay();
                LoadShapeFileOverlayOntoMap();
                if (Map.Overlays.Contains("importOverlay")) {
                    if (((LayerOverlay)Map.Overlays["importOverlay"]).Layers.Contains("ImportLayer")) {
                        Map.Refresh();
                    }
                }
            }
            catch { }
        }

        private void CreateInMemoryLayerAndOverlay() {
            layerOverlay = new LayerOverlay();
            layerOverlay.DrawingExceptionMode = DrawingExceptionMode.DrawException;
            fieldsLayer = new InMemoryFeatureLayer();

            //fieldsLayer
            fieldsLayer.FeatureSource.Open();
            FeatureSourceColumn column = new FeatureSourceColumn(@"LdbLabel", DbfColumnType.Character.ToString(), 64);
            fieldsLayer.Columns.Add(column);
            fieldsLayer.FeatureSource.Close();
            fieldsLayerAreaStyle = new ClientLayerAreaStyle();
            fieldsLayerAreaStyle.SelectedAreaStyle = new AreaStyle(new GeoPen(GeoColor.SimpleColors.Blue, 2), new GeoSolidBrush(new GeoColor(125, GeoColor.SimpleColors.Blue)));
            fieldsLayerAreaStyle.UnselectedAreaStyle = new AreaStyle(new GeoPen(GeoColor.StandardColors.Red, 2), new GeoSolidBrush(new GeoColor(125, GeoColor.StandardColors.Red)));
            fieldsLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(fieldsLayerAreaStyle);
            //fieldsLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(scalingTextStyle);
            fieldsLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            //TODO
            if (activemap == "BingRadar") {
                fieldsLayer.FeatureSource.Projection = projection;
            }
            else {
                fieldsLayer.FeatureSource.Projection = projection84;
            }
            //fieldsLayer.FeatureSource.Projection = projection;
            //fieldsLayer.FeatureSource.Projection = projection84;
            fieldsLayerAreaStyle.SelectedFeatures = new List<Feature>();
            fieldsLayerAreaStyle.HiddenFeatures = new List<Feature>();
            layerOverlay.Layers.Add("ImportLayer", fieldsLayer);
        }

        private void LoadShapeFileOverlayOntoMap() {
            if (Map.Overlays.Contains("importOverlay")) {
                Map.Overlays.Remove("importOverlay");
            }
            if (layerOverlay.Layers.Contains("ImportLayer")) {
                Map.Overlays.Add("importOverlay", layerOverlay);
            }

            EnsureFieldsLayerIsOpen();
            //var extent = new RectangleShape(-143.4, 109.3, 116.7, -76.3);
            //TODO
            RectangleShape extent = new RectangleShape();
            if (activemap == "BingRadar") {
                extent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
            }
            else {
                extent = new RectangleShape(-126.4, 48.8, -67.0, 19.0);
            }
            //var extent = new RectangleShape(-126.4, 48.8, -67.0, 19.0);
            //var extent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
            try {
                //fieldsLayer.InternalFeatures.Add(SelectedField);
                if (ClientLayer.Count > 0 && fieldsLayer.InternalFeatures.Count == 0) {
                    LoadFieldLayerShapes();
                }
                var shapelayerfeatures = fieldsLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);
                if (shapelayerfeatures.Count > 0) {
                    var shapelayercolumns = fieldsLayer.FeatureSource.GetColumns();
                    extent = ExtentHelper.GetBoundingBoxOfItems(shapelayerfeatures);
                    extent.ScaleUp(100);
                    Map.CurrentExtent = extent;
                    PointShape center = extent.GetCenterPoint();
                    //Map.CenterAt(center);
                }
                else {
                    Map.CurrentExtent = extent;
                    PointShape center = extent.GetCenterPoint();
                    //Map.CenterAt(center);
                }
            }
            catch (Exception ex) {
                log.ErrorException("Caught an exception while wloading weather", ex);
                //System.Windows.MessageBox.Show("ImportTools - Caught an exception while working with a simple draw shape.");
            }
        }

        void Cancel() {
            Map.Cursor = Cursors.Arrow;
            HasUnsavedChanges = false;
            fieldsLayer.InternalFeatures.Clear();
            var importmessage = new ShapeImportMessage() { ShapeBase = new MultipolygonShape() };
            Messenger.Default.Send<ShapeImportMessage>(importmessage);
        }

        private void AddNoaaWeatherLayer<T>(Action<T> setLayerPropertiesAction = null) where T : Layer, new() {
            LayerOverlay layerOverlay = GetLayerOverlay();
            if (!layerOverlay.Layers.Any(l => l is T)) {
                T layer = new T();
                if (setLayerPropertiesAction != null) {
                    setLayerPropertiesAction(layer);
                }
                layerOverlay.Layers.Add(layer);
                    layerOverlay.Refresh();
            }
        }

        private LayerOverlay GetLayerOverlay() {
            LayerOverlay layerOverlay = new LayerOverlay();
                layerOverlay = Map.Overlays.OfType<LayerOverlay>().FirstOrDefault();
                if (layerOverlay == null) {
                    layerOverlay = new LayerOverlay();
                    layerOverlay.DrawingExceptionMode = DrawingExceptionMode.DrawException;
                    Map.Overlays.Add(layerOverlay);
                    Map.Refresh(layerOverlay);
                }
            return layerOverlay;
        }
    }
}
