﻿using System;
using System.Collections.ObjectModel;
using ThinkGeo.MapSuite.Core;

namespace Landdb.ViewModel.Secondary.Map
{
    public static class Splitter
    {
        public static Collection<LineShape> GetPerpendicularLines(LineShape lineShape, Collection<PointShape> interceptedPoints, double length)
        {
            Collection<LineShape> returnFeatures = new Collection<LineShape>();
            double halfLength = length / 2; 
            double pi2 = Math.PI / 2;
            double searchingTolerance = 0.000001;
            foreach (PointShape interceptedPoint in interceptedPoints)
            {
                RectangleShape searchingArea = new RectangleShape(interceptedPoint.X - searchingTolerance, interceptedPoint.Y + searchingTolerance, interceptedPoint.X + searchingTolerance, interceptedPoint.Y - searchingTolerance);

                // Check if the intercepted point is the vertex will be ignore, because we can not create perpendicular line crosses vertex.
                foreach (Vertex vertex in lineShape.Vertices)
                {
                    if (new PointShape(vertex).IsWithin(searchingArea))
                    {
                        continue;
                    }
                }

                double middleX = interceptedPoint.X;
                double middleY = interceptedPoint.Y;

                Vertex vertex1 = new Vertex();
                Vertex vertex2 = new Vertex();
                // Find the crossing point is between which two vertices
                for (int i = 0; i < lineShape.Vertices.Count - 1; i++)
                {
                    vertex1 = lineShape.Vertices[i];
                    vertex2 = lineShape.Vertices[i + 1];
                    LineShape currentLine = new LineShape(new Vertex[] { vertex1, vertex2 });
                    if (searchingArea.Intersects(currentLine))
                    {
                        break;
                    }
                }

                double angle = Math.Atan((vertex1.Y - vertex2.Y) / (vertex1.X - vertex2.X));

                double x = 0;
                double y = 0;

                if (angle > 0 && angle < pi2)
                {
                    if (vertex1.X > vertex2.X)
                    {
                        x = middleX + Math.Sin(angle) * halfLength;
                        y = middleY - Math.Cos(angle) * halfLength;
                    }
                    else
                    {
                        x = middleX - Math.Sin(angle) * halfLength;
                        y = middleY + Math.Cos(angle) * halfLength;
                    }
                }
                else
                {
                    if (vertex1.X > vertex2.X)
                    {
                        x = middleX + Math.Sin(angle) * halfLength;
                        y = middleY - Math.Cos(angle) * halfLength;
                    }
                    else
                    {
                        x = middleX - Math.Sin(angle) * halfLength;
                        y = middleY + Math.Cos(angle) * halfLength;
                    }
                }

                Vertex anotherVertex = new Vertex(middleX * 2 - x, middleY * 2 - y);
                LineShape line = new LineShape(new Vertex[] { anotherVertex, new Vertex(x, y) });
                returnFeatures.Add(line);
            }

            return returnFeatures;
        }

        public static PolygonShape SplitPolygonWithTwoLines(PolygonShape processedPolygon, LineShape lineShape1, LineShape lineShape2)
        {
            MultipolygonShape resultMultipolygon = Split(processedPolygon, lineShape1);
            PolygonShape resultShape = new PolygonShape();
            foreach (PolygonShape polygonShape in resultMultipolygon.Polygons)
            {
                MultipointShape intersectionMultiPoint = lineShape2.GetCrossing(polygonShape.OuterRing);
                if (intersectionMultiPoint.Points.Count == 2)
                {
                    MultipolygonShape resultMultipolygon1 = Split(polygonShape, lineShape2);
                    foreach (PolygonShape polygonShape1 in resultMultipolygon1.Polygons)
                    {
                        MultipointShape intersectionMultiPoint1 = lineShape2.GetCrossing(polygonShape.OuterRing);
                        if (intersectionMultiPoint1.Points.Count == 2)
                        {
                            resultShape = polygonShape1;
                            break;
                        }
                    }
                }
            }
            return resultShape;
        }

        public static MultipolygonShape Split(MultipolygonShape processedMultiPolygon, System.Collections.Generic.IList<BaseShape> baseshapes) {
            MultipolygonShape resultShape = new MultipolygonShape();
            try {
                resultShape = processedMultiPolygon;
                foreach (BaseShape baseshape in baseshapes) {
                    if (baseshape is LineShape) {
                        LineShape lineshape = baseshape as LineShape;
                        MultipolygonShape mps = Split(resultShape, lineshape);
                        resultShape = mps;
                    }
                    else if (baseshape is PolygonShape) {
                        PolygonShape polygonshape = baseshape as PolygonShape;
                        MultipolygonShape mps = Split(resultShape, polygonshape);
                        resultShape = mps;
                    }
                }
            }
            catch (Exception ex) {
                int xxx = 0;
            }
            return resultShape;
        }

        public static MultipolygonShape Split(MultipolygonShape processedMultiPolygon, PolygonShape polygonshape) {
            MultipolygonShape resultShape = new MultipolygonShape();
            try {
                foreach (PolygonShape poly in processedMultiPolygon.Polygons) {
                    MultipolygonShape mps = Split(poly, polygonshape);
                    foreach (PolygonShape poly2 in mps.Polygons) {
                        resultShape.Polygons.Add(poly2);
                    }
                }
            }
            catch (Exception ex) {
                int xxx = 0;
            }
            return resultShape;
        }

        public static MultipolygonShape Split(PolygonShape multiPolygonShape, PolygonShape multipoly) {
            MultipolygonShape resultShape = new MultipolygonShape();
            try {
                if (multiPolygonShape.Contains(multipoly)) {
                    MultipolygonShape resultingmulti = multiPolygonShape.GetDifference(multipoly);
                    resultShape = resultingmulti;
                    resultShape.Polygons.Add(multipoly);
                }
                else if (multiPolygonShape.Overlaps(multipoly)) {
                    MultipolygonShape resultingmulti = multiPolygonShape.GetDifference(multipoly);
                    resultShape = resultingmulti;
                    MultipolygonShape resultingmulti2 = multiPolygonShape.GetDifference(resultingmulti);
                    foreach (PolygonShape ps in resultingmulti2.Polygons) {
                        resultShape.Polygons.Add(ps);
                    }
                }
                else {
                    resultShape.Polygons.Add(multiPolygonShape);
                }
            }
            catch (Exception ex) {
                int xxx = 0;
            }
            return resultShape;
        }

        public static MultipolygonShape Split(MultipolygonShape processedMultiPolygon, System.Collections.Generic.IList<LineShape> lines) {
            MultipolygonShape resultShape = new MultipolygonShape();
            try {
                resultShape = processedMultiPolygon;
                foreach (LineShape lineshape in lines) {
                    MultipolygonShape mps = Split(resultShape, lineshape);
                    resultShape = mps;
                }
            }
            catch (Exception ex) {
                int xxx = 0;
            }
            return resultShape;
        }

        public static MultipolygonShape Split(MultipolygonShape processedMultiPolygon, LineShape lineShape) {
            MultipolygonShape resultShape = new MultipolygonShape();
            try {
                foreach (PolygonShape poly in processedMultiPolygon.Polygons) {
                    MultipolygonShape mps = Split(poly, lineShape);
                    foreach (PolygonShape poly2 in mps.Polygons) {
                        resultShape.Polygons.Add(poly2);
                    }
                }
            }
            catch (Exception ex) {
                int xxx = 0;
            }
            return resultShape;
        }

        public static MultipolygonShape Split(PolygonShape processedPolygon, LineShape lineShape) {
            MultipolygonShape resultShape = new MultipolygonShape();
            try {
                MultipointShape intersectionMultiPoint = lineShape.GetCrossing(processedPolygon.OuterRing);

                if (intersectionMultiPoint.Points.Count == 2) {
                    PolygonShape polygonShape1 = GetPolygonForSplit(processedPolygon, lineShape, false);
                    PolygonShape polygonShape2 = GetPolygonForSplit(processedPolygon, lineShape, true);
                    resultShape.Polygons.Add(polygonShape1);
                    resultShape.Polygons.Add(polygonShape2);
                }
                else {
                    resultShape.Polygons.Add(processedPolygon);
                }
            }
            catch (Exception ex) {
                int xxx = 0;
            }
            return resultShape;
        }

        private static PolygonShape GetPolygonForSplit(PolygonShape processedPolygon, LineShape lineShape, bool changeOrder) {
            MultipointShape intersectionMultiPoint = lineShape.GetCrossing(processedPolygon.OuterRing);
            if (changeOrder)
            {
                PointShape tempPointShape = intersectionMultiPoint.Points[0];
                intersectionMultiPoint.Points[0] = intersectionMultiPoint.Points[1];
                intersectionMultiPoint.Points[1] = tempPointShape;
            }
            if (intersectionMultiPoint.Points.Count == 2)
            {
                PolygonShape resultPolygonShape = new PolygonShape();
                try {
                    RingShape resultOuterRing = SplitRing(processedPolygon.OuterRing, intersectionMultiPoint.Points[0], intersectionMultiPoint.Points[1]);
                    resultPolygonShape.OuterRing = resultOuterRing;

                    foreach (RingShape innerRing in processedPolygon.InnerRings) {
                        MultipointShape innerIntersectionMultiPoint = lineShape.GetCrossing(innerRing);
                        if (innerIntersectionMultiPoint.Points.Count == 2) {
                            RingShape resultInnerRing = SplitRing(innerRing, innerIntersectionMultiPoint.Points[0], innerIntersectionMultiPoint.Points[1]);
                            if (resultPolygonShape.Contains(resultInnerRing)) {
                                resultPolygonShape.InnerRings.Add(resultInnerRing);
                            }
                        }
                        else {
                            if (resultPolygonShape.Contains(innerRing)) {
                                resultPolygonShape.InnerRings.Add(innerRing);
                            }
                        }
                    }
                }
                catch (Exception ex) {
                    int xxx = 0;
                }
                return resultPolygonShape;
            }
            return null;
        }

        public static bool DoesPointShapeBelongToLineSegment(PointShape pointShape, PointShape linePointShape1, PointShape linePointShape2)
        {
            bool result = false;
            double a = 0;
            double b = 0;

            if ((pointShape.X == linePointShape1.X & pointShape.Y == linePointShape1.Y) | (pointShape.X == linePointShape2.X & pointShape.Y == linePointShape2.Y))
            {
                result = true;
            }
            else
            {
                if (linePointShape1.X != linePointShape2.X)
                {
                    a = (linePointShape2.Y - linePointShape1.Y) / (linePointShape2.X - linePointShape1.X);
                    b = linePointShape1.Y - (a * linePointShape1.X);

                    if (Math.Round(pointShape.Y, 5) == Math.Round((a * pointShape.X) + b, 5) & pointShape.X >= Math.Min(linePointShape1.X, linePointShape2.X) & pointShape.X <= Math.Max(linePointShape1.X, linePointShape2.X))
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                }
                else
                {
                    if (pointShape.X == linePointShape1.X & (pointShape.Y >= Math.Min(linePointShape1.Y, linePointShape2.Y) & pointShape.Y <= Math.Max(linePointShape1.Y, linePointShape2.Y)))
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                }
            }
            return result;
        }

        public static RingShape SplitRing(RingShape processedRing, PointShape intersectionPointShape1, PointShape intersectionPointShape2)
        {
            RingShape resultRingShape = new RingShape();

            int i = 0;
            int totalPointNumber = processedRing.Vertices.Count;
            while (i < totalPointNumber - 1)
            {
                int indexA = i + 1;
                if (DoesPointShapeBelongToLineSegment(intersectionPointShape1, new PointShape(processedRing.Vertices[i]), new PointShape(processedRing.Vertices[indexA])))
                {
                    resultRingShape.Vertices.Add(new Vertex(intersectionPointShape1));
                    if (DoesPointShapeBelongToLineSegment(intersectionPointShape2, new PointShape(processedRing.Vertices[i]), new PointShape(processedRing.Vertices[indexA])))
                    {
                        resultRingShape.Vertices.Add(new Vertex(intersectionPointShape2));
                        resultRingShape.Vertices.Add(new Vertex(intersectionPointShape1));
                    }
                    else
                    {
                        for (int j = i + 1; j <= processedRing.Vertices.Count - 1; j++)
                        {
                            //- 1
                            int indexB = j + 1;

                            if (j < processedRing.Vertices.Count - 1)
                            {
                                if (DoesPointShapeBelongToLineSegment(intersectionPointShape2, new PointShape(processedRing.Vertices[j]), new PointShape(processedRing.Vertices[indexB])))
                                {
                                    resultRingShape.Vertices.Add(processedRing.Vertices[j]);
                                    resultRingShape.Vertices.Add(new Vertex(intersectionPointShape2));
                                    resultRingShape.Vertices.Add(new Vertex(intersectionPointShape1));
                                    break; // TODO: might not be correct. Was : Exit For
                                }
                                else
                                {
                                    resultRingShape.Vertices.Add(processedRing.Vertices[j]);
                                }
                            }
                            else
                            {
                                for (int k = 0; k < i; k++)
                                {
                                    if (DoesPointShapeBelongToLineSegment(intersectionPointShape2, new PointShape(processedRing.Vertices[k]), new PointShape(processedRing.Vertices[(k + 1)])))
                                    {
                                        resultRingShape.Vertices.Add(processedRing.Vertices[k]);
                                        resultRingShape.Vertices.Add(new Vertex(intersectionPointShape2));
                                        resultRingShape.Vertices.Add(new Vertex(intersectionPointShape1));
                                        break; // TODO: might not be correct. Was : Exit For
                                    }
                                    else
                                    {
                                        resultRingShape.Vertices.Add(processedRing.Vertices[k]);
                                    }
                                }
                                break; // TODO: might not be correct. Was : Exit For
                            }
                        }
                    }

                    return resultRingShape;
                }
                i = i + 1;
            }
            return null;

        }

        private static void CalculateOffsetPoints()
        {
            double borderrowwidth = 40;
            double borderrowwidthx2 = borderrowwidth * 1.0;
            MultipolygonShape mps = new MultipolygonShape();
            MultilineShape multiLineShape = mps.Polygons[0].ToMultiLineShape();

            PointShape point1 = new PointShape();
            PointShape point2 = new PointShape();
            PointShape point3 = new PointShape();
            MultilineShape segmentMultiLineShape = (MultilineShape)multiLineShape.GetLineOnALine(point1, point3);

            Feature segmentFeature = new Feature(segmentMultiLineShape);

            PointShape pointShape1 = new PointShape(segmentMultiLineShape.Lines[0].Vertices[0]);
            int count = segmentMultiLineShape.Lines[0].Vertices.Count - 1;
            PointShape pointShape2 = new PointShape(segmentMultiLineShape.Lines[0].Vertices[count]);

            double Angle1 = GetAngleFromTwoVertices(segmentMultiLineShape.Lines[0].Vertices[0], segmentMultiLineShape.Lines[0].Vertices[1]);
            double Angle2 = GetAngleFromTwoVertices(segmentMultiLineShape.Lines[0].Vertices[count | -1], segmentMultiLineShape.Lines[0].Vertices[count]);

            double offsetAngle1a = Angle1;
            double offsetAngle1b = GetReverseAngle(Angle1);
            double offsetAngle2a = Angle2;
            double offsetAngle2b = GetReverseAngle(Angle2);

            PointShape offsetPointShape1a = (PointShape)pointShape1.CloneDeep();
            PointShape offsetPointShape1b = (PointShape)pointShape1.CloneDeep();
            PointShape offsetPointShape2a = (PointShape)pointShape2.CloneDeep();
            PointShape offsetPointShape2b = (PointShape)pointShape2.CloneDeep();

            offsetPointShape1a.TranslateByDegree(borderrowwidthx2, offsetAngle1a);
            offsetPointShape1b.TranslateByDegree(borderrowwidthx2, offsetAngle1b);
            offsetPointShape2a.TranslateByDegree(borderrowwidthx2, offsetAngle2a);
            offsetPointShape2b.TranslateByDegree(borderrowwidthx2, offsetAngle2b);
        }

        private static double GetAngleFromTwoVertices(Vertex fromVertex, Vertex toVertex)
        {
            double alpha = 0;

            if (fromVertex.X != toVertex.X)
            {
                double tangentAlpha = (fromVertex.Y - toVertex.Y) / (toVertex.X - fromVertex.X);
                alpha = Math.Atan(tangentAlpha) * 180 / Math.PI;
                if (alpha < 0) { alpha += 360; }
            }
            else
            {
                alpha = (fromVertex.Y > toVertex.Y) ? 90 : 270;
            }

            return alpha;
        }

        private static double GetReverseAngle(double Angle)
        {
            double result = Angle + 180;
            if (result > 360)
            {
                result = result - 360;
            }
            return result;
        }
    }
}

