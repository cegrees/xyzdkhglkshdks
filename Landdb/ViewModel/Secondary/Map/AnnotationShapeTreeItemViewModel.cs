﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AgC.UnitConversion;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure;
using Landdb.Domain.ReadModels.MapAnnotation;
using Landdb.ViewModel.Fields;

namespace Landdb.ViewModel.Secondary.Map {
    public class AnnotationShapeTreeItemViewModel : AbstractTreeItemViewModel {
        readonly MapAnnotationId id;
        public MapAnnotationListItem AnnotationItem { get; }
        public string Name { get; }
        public override IIdentity Id => id;

        public AnnotationShapeTreeItemViewModel(MapAnnotationListItem annotationitem, AbstractTreeItemViewModel parent, bool sortChildrenAlphabetically, Action<AbstractTreeItemViewModel> onCheckedChanged = null, Predicate<object> filter = null)
            : base(parent, onCheckedChanged, null, filter) {
            id = annotationitem.Id;
            Name = annotationitem.Name;
            AnnotationItem = annotationitem;
        }
        public override bool CanBeDeleted() {
                return true;
        }

        public override string ToString() {
            return Name;
        }
    }
}
