﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GalaSoft.MvvmLight;
using ThinkGeo.MapSuite.WpfDesktopEdition;
using ThinkGeo.MapSuite.Core;
using Landdb.Infrastructure;
using System.IO;
using Microsoft.Win32;
using GalaSoft.MvvmLight.Command;
using System.Windows.Input;
using System.Collections.ObjectModel;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure.Messages;
using Landdb.MapStyles;
using Landdb.ViewModel.Maps.ImageryOverlay;
using AgC.UnitConversion;
using Landdb.ViewModel.Fields.FieldDetails.MapModels;
using Landdb.Client.Infrastructure;
using AgC.UnitConversion.Area;
using AgC.UnitConversion.Length;
using Landdb.Client.Localization;
using Landdb.Resources;
using NLog;

namespace Landdb.ViewModel.Secondary.Map {
    public class ConstructPivotViewModel : ViewModelBase
    {
        IClientEndpoint clientEndpoint;
        Logger log = LogManager.GetCurrentClassLogger();
        float measurefontsize = 8;
        bool maploaded = false;
        bool startedOutOnline = false;
        InMemoryFeatureLayer currentLayer = new InMemoryFeatureLayer();
        InMemoryFeatureLayer measureLayer = new InMemoryFeatureLayer();
        IMapOverlay selectedoverlay;
        LayerOverlay layerOverlay = new LayerOverlay();
        PopupOverlay popupOverlay = new PopupOverlay();
        Proj4Projection projection;
        Feature selectedfield = new Feature();
        Feature loadedfeature = new Feature();
        RectangleShape currentmapextent = new RectangleShape();
        bool drawingpivot = true;
        string fieldname = string.Empty;
        double radius = 500;
        double area = 0;
        double desiredarea = 100;
        double oldarea = 0;
        double scalepercent = 0;
        double pivotpoints = 36;
        double pointx = 0;
        double pointy = 0;
        //double multiplierconstant = 1;
        //double multiplierconstant = .99733845;
        //double multiplierconstant = .999975;
        double multiplierconstant = .99731255;
        BaseShape fieldshape = new MultipolygonShape();
        Cursor previousCursor;
        MapStyles.CustomDistancePolygonStyle customDistancePolygonStyleText = new MapStyles.CustomDistancePolygonStyle(ThinkGeo.MapSuite.Core.AreaUnit.Acres, ThinkGeo.MapSuite.Core.DistanceUnit.Feet, new GeoFont("Arial", 8, DrawingFontStyles.Regular), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.White), new GeoSolidBrush(GeoColor.StandardColors.Red), new GeoPen(GeoColor.StandardColors.Red));
        MapStyles.CustomDistanceLineStyle customDistanceLineStyleText = new MapStyles.CustomDistanceLineStyle(DistanceUnit.Feet, new GeoFont("Arial", 8, DrawingFontStyles.Regular), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.DarkRed, 2), new GeoPen(GeoColor.StandardColors.DarkBlue, 2));
        LineStyle lineStylePen = new LineStyle(new GeoPen(GeoColor.FromArgb(150, GeoColor.StandardColors.Orange), 2));
        AreaStyle areaStyleBrush = new AreaStyle(new GeoSolidBrush(GeoColor.StandardColors.Blue));
        MapSettings mapsettings;
        string areaUnitString = string.Empty;
        string coordinateformat = string.Empty;
        AgC.UnitConversion.Area.AreaUnit agcAreaUnit;
        ThinkGeo.MapSuite.Core.AreaUnit mapAreaUnit;
        ThinkGeo.MapSuite.Core.DistanceUnit mapDistanceUnit;
        AgC.UnitConversion.Length.LengthUnit mapLengthUnit;
        bool thiswasfromtracking = false;
        bool isnotmeasuring = true;
        ZoomLevelFactory zoomlevelfactory = new ZoomLevelFactory();
        bool notcenteredyet = false;
        private Projection projectionFromSphericalMercator;
        private Projection projectionFromWGS84;
        private Projection projectionFromSM;
        RectangleShape mapextent = new RectangleShape();

        public ConstructPivotViewModel(IClientEndpoint clientEndpoint)
        {
            this.clientEndpoint = clientEndpoint;
            InitializeSettings();
            customDistancePolygonStyleText = new MapStyles.CustomDistancePolygonStyle(mapAreaUnit, mapDistanceUnit, new GeoFont("Arial", measurefontsize, DrawingFontStyles.Regular), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.White), new GeoSolidBrush(GeoColor.StandardColors.Red), new GeoPen(GeoColor.StandardColors.Red));
            customDistanceLineStyleText = new MapStyles.CustomDistanceLineStyle(mapDistanceUnit, new GeoFont("Arial", measurefontsize, DrawingFontStyles.Regular), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.DarkRed, 2), new GeoPen(GeoColor.StandardColors.DarkBlue, 2));
            customDistancePolygonStyleText.YOffsetInPixel = -20;

            InitializeMap();
            areaStyleBrush = new AreaStyle(new GeoPen(new GeoSolidBrush(new GeoColor(50, GeoColor.StandardColors.Yellow)), 3), new GeoSolidBrush(GeoColor.StandardColors.Blue));

            DrawPivotCenterCommand = new RelayCommand(DrawPivotCenter);
            ResetCommand = new RelayCommand(ResetPivot);
            ExitCropzoneCommand = new RelayCommand(ExitCropzone);

            MeasureCommand = new RelayCommand(HandleMeasureLine);
            SavePivotCommand = new RelayCommand(SavePolygon);
        }

        public ICommand DrawPivotCenterCommand { get; private set; }
        public ICommand ResetCommand { get; private set; }
        public ICommand MeasureCommand { get; private set; }
        public ICommand SavePivotCommand { get; private set; }
        public ICommand ExitCropzoneCommand { get; private set; }

        public WpfMap Map { get; set; }

        public IMapOverlay SelectedOverlay
        {
            get { return selectedoverlay; }
            set
            {
                if (selectedoverlay == value) { return; }
                if (selectedoverlay == null) { return; }
                if (string.IsNullOrWhiteSpace(value.Name)) { return; }
                selectedoverlay = value;
                RaisePropertyChanged("SelectedOverlay");
            }
        }
        public Feature SelectedField
        {
            get { return selectedfield; }
            set
            {
                IsNotMeasuring = true;
                if (selectedfield == value) { return; }
                selectedfield = value;
                log.Info("PivotTools - Set select tree feature to {0}.", selectedfield.Id);
                //log.Info("PivotTools - Ensure current layer open.");
                EnsureCurrentLayerIsOpen();
                //log.Info("PivotTools - Ensure measure layer open.");
                EnsureMeasureLayerIsOpen();
                if (drawingpivot) {
                    if (!maploaded) { return; }
                    //log.Info("PivotTools - Clear edit shapes layer internal features.");
                    Map.EditOverlay.EditShapesLayer.InternalFeatures.Clear();
                    ResetPivot();
                }
                log.Info("PivotTools - Open and load shape.");
                OpenAndLoadaShapefile();
                RaisePropertyChanged("SelectedField");
            }
        }

        public string FieldName
        {
            get { return fieldname; }
            set
            {
                if (fieldname == value) { return; }
                fieldname = value;
                RaisePropertyChanged("FieldName");
            }
        }

        public double Radius
        {
            get { return radius; }
            set
            {
                if (radius == value) { return; }
                radius = value;
                Drawpivot();
                RaisePropertyChanged("Radius");
                RaisePropertyChanged("Area");
            }
        }

        public double Area {
            get { return area; }
            set {
                if (area == value) { return; }
                area = value;
                DrawPivotFromAreaCalculations();
                RaisePropertyChanged("Area");
                RaisePropertyChanged("Radius");
            }
        }

        public double DesiredArea {
            get { return desiredarea; }
            set {
                if (desiredarea == value) { return; }
                desiredarea = value;
                RaisePropertyChanged("DesiredArea");
            }
        }

        public double OldArea {
            get { return oldarea; }
            set {
                if (oldarea == value) { return; }
                oldarea = value;
                RaisePropertyChanged("OldArea");
            }
        }

        public double ScalePercent {
            get { return scalepercent; }
            set {
                if (scalepercent == value) { return; }
                scalepercent = value;
                RaisePropertyChanged("ScalePercent");
            }
        }

        public double PivotPoints
        {
            get { return pivotpoints; }
            set
            {
                if (pivotpoints == value) { return; }
                pivotpoints = value;
                //Drawpivot();
                RaisePropertyChanged("PivotPoints");
            }
        }

        public double PointX
        {
            get { return pointx; }
            set
            {
                if (pointx == value) { return; }
                pointx = value;
                //Drawpivot();
                RaisePropertyChanged("PointX");
            }
        }

        public double PointY
        {
            get { return pointy; }
            set
            {
                if (pointy == value) { return; }
                pointy = value;
                //Drawpivot();
                RaisePropertyChanged("PointY");
            }
        }

        public string AreaUnitName {
            get { return string.Format("({0})", agcAreaUnit.AbbreviatedDisplay); }
        }

        public string LengthUnitName {
            get { return string.Format("({0})", mapLengthUnit.AbbreviatedDisplay); }
        }

        public bool IsNotMeasuring {
            get { return isnotmeasuring; }
            set {
                if (isnotmeasuring == value) { return; }
                isnotmeasuring = value;
                RaisePropertyChanged("IsNotMeasuring");
            }
        }

        public RectangleShape CurrentMapExtent
        {
            get { return currentmapextent; }
            set
            {
                if (currentmapextent == value) {
                    Map.Refresh();
                    return;
                }
                currentmapextent = value;
                RaisePropertyChanged("CurrentMapExtent");
                log.Info("PivotTools - Set map extent.");
                if (maploaded) {
                    //Map.CurrentExtent = currentmapextent;
                    //Map.Refresh();
                    ShapeValidationResult svr = currentmapextent.Validate(ShapeValidationMode.Simple);
                    if (svr.IsValid) {
                        notcenteredyet = true;
                        Map.CurrentExtent = currentmapextent;
                        notcenteredyet = false;
                        //PointShape center1 = currentLayer.GetBoundingBox().GetCenterPoint();
                        //Map.CenterAt(center1);
                        //Map.CurrentExtent = currentmapextent;
                        log.Info("PivotTools - Refresh extent.");
                        Map.Refresh();
                    }
                }
            }
        }

        public IMapOverlay MapFactory(MapInfoSelectorItem mapInfoSelectorItem, bool? allowofflinemap) {
            if (mapInfoSelectorItem.IsBing()) {
                return new BingMapOverlay(allowofflinemap);
            }
            if (mapInfoSelectorItem.IsRoadsOnly()) {
                return new BingRoadMapsOverlay(allowofflinemap);
            }
            if (mapInfoSelectorItem.IsGoogle()) {
                return new GoogleMapCustomOverlay();
            }
            if (mapInfoSelectorItem.IsGoogleOffline()) {
                return new GoogleMapOverlay(allowofflinemap);
            }
            return new NoneMapOverlay(allowofflinemap);
        }

        public void SnapToGoogleImagery() {
            if (SelectedOverlay != null && SelectedOverlayIsGoogle()) {
                double googlescale = zoomlevelfactory.SnapToGoogleZoomLevelScale(Map.CurrentScale);
                Map.ZoomToScale(googlescale);
            }
        }

        private bool SelectedOverlayIsGoogle() {
            return SelectedOverlay.MapInfoSelectorItem.IsGoogle();
        }

        public bool DrawingPivot
        {
            get { return drawingpivot; }
            set
            {
                if (drawingpivot == value) { return; }
                drawingpivot = value;
                log.Info("PivotTools - Set drawing pivot circle to {0}.", drawingpivot);

                if (drawingpivot) {
                    //log.Info("PivotTools - Initializing pivot settings.");
                    InitializeSettings();
                    //log.Info("PivotTools - Ensure current layer open.");
                    EnsureCurrentLayerIsOpen();
                    //log.Info("PivotTools - Ensure measure layer open.");
                    EnsureMeasureLayerIsOpen();
                    //log.Info("PivotTools - clear measure layer features.");
                    measureLayer.InternalFeatures.Clear();
                    //log.Info("PivotTools - clear measure popup overlay.");
                    popupOverlay.Popups.Clear();
                }
                RaisePropertyChanged("DrawingPivot");
            }
        }

        private string displaycoordinates = string.Empty;
        public string DisplayCoordinates {
            get { return displaycoordinates; }
            set {
                displaycoordinates = value;
                RaisePropertyChanged("DisplayCoordinates");
            }
        }

        public void ResetPivot() {
            IsNotMeasuring = true;
            log.Info("PivotTools - Reset pivot data");
            if (!maploaded) { return; }
            pointx = 0;
            pointy = 0;
            Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Clear();
            Map.TrackOverlay.Refresh();
            if (Map.EditOverlay.EditShapesLayer.InternalFeatures.Count > 1) {
                Map.EditOverlay.EditShapesLayer.InternalFeatures.RemoveAt(Map.EditOverlay.EditShapesLayer.InternalFeatures.Count - 1);
            }
            else {
                Map.EditOverlay.EditShapesLayer.InternalFeatures.Clear();
            }
            Map.EditOverlay.CalculateAllControlPoints();
            Map.EditOverlay.Refresh();

            if (Map.EditOverlay.EditShapesLayer.InternalFeatures.Count == 0) {
                if (Map.Overlays.Contains("importOverlay")) {
                    if (layerOverlay.Layers.Contains("measureLayer")) {
                        measureLayer.InternalFeatures.Clear();
                        popupOverlay.Popups.Clear();
                        Map.Refresh();
                    }
                }
            }
        }

        private void InitializeSettings() {
            this.mapsettings = this.clientEndpoint.GetMapSettings();
            areaUnitString = mapsettings.MapAreaUnit;
            coordinateformat = mapsettings.DisplayCoordinateFormat;
            agcAreaUnit = UnitFactory.GetUnitByName(areaUnitString) as AgC.UnitConversion.Area.AreaUnit;
            mapAreaUnit = MapUnitFactory.AreaMeasureConversion(agcAreaUnit);
            mapDistanceUnit = MapUnitFactory.AreaDistanceMeasureConversion(agcAreaUnit);
            mapLengthUnit = MapUnitFactory.MapLengthUnitConversion(mapDistanceUnit);
            selectedoverlay = MapFactory(mapsettings.MapInfoSelectorItem, mapsettings.AllowOfflineMap);
            RaisePropertyChanged("AreaUnitName");
            RaisePropertyChanged("LengthUnitName");

        }

        void InitializeMap()
        {
            Map = new WpfMap();
            startedOutOnline = Landdb.Client.Infrastructure.NetworkStatus.IsInternetAvailableFast();
            if (SelectedOverlayIsGoogle()) {
                Map.MaxHeight = 2048;
                Map.MaxWidth = 2048;
                //Map.Height = 640;
                //Map.Width = 640;
            }
            Map.MapUnit = GeographyUnit.Meter;
            Map.MapTools.PanZoomBar.Visibility = System.Windows.Visibility.Visible;
            Map.MapTools.PanZoomBar.GlobeButtonVisibility = System.Windows.Visibility.Hidden;
            Map.MapTools.PanZoomBar.VerticalAlignment = System.Windows.VerticalAlignment.Center;
            Map.MapTools.PanZoomBar.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            Map.Background = System.Windows.Media.Brushes.White;
            Map.Cursor = Cursors.Hand;
            //zoomlevelfactory = new ZoomLevelFactory();
            //Map.ZoomLevelSet = zoomlevelfactory.ZoomLevelSET;
            Map.MouseLeftButtonDown += (sender, e) =>
            {
                ((MainWindow)System.Windows.Application.Current.MainWindow).Draggable = false;
            };

            projection = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetGoogleMapParametersString());
            if (!projection.IsOpen) {
                projection.Open();
            }

            Map.Loaded += (sender, e) =>
            {
                if (maploaded) { return; }
                LoadBingMapsOverlay();
                Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = areaStyleBrush;


                CreateInMemoryLayerAndOverlay();
                if (Map.Overlays.Contains("importOverlay")) {
                    Map.Overlays.Remove("importOverlay");
                }
                if (layerOverlay.Layers.Contains("ImportLayer")) {
                    Map.Overlays.Add("importOverlay", layerOverlay);
                }
                EnsureCurrentLayerIsOpen();
                EnsureMeasureLayerIsOpen();
                notcenteredyet = true;
                Map.CurrentExtent = CurrentMapExtent;
                notcenteredyet = false;
                //PointShape center1 = CurrentMapExtent.GetCenterPoint();
                //Map.CenterAt(center1);
                Map.Refresh();
                Map.Overlays.Add("popupOverlay", popupOverlay);
                maploaded = true;
            };

            Map.CurrentExtentChanged += (sender, e) => {
                mapextent = e.CurrentExtent;
                if (!maploaded) { return; }
                if (!ApplicationEnvironment.ShowPremiumMap) { return; }
                if (notcenteredyet) {
                    notcenteredyet = false;
                    //return;
                }
                if (SelectedOverlayIsGoogle()) {

                    PointShape centerpoint = mapextent.GetCenterPoint();
                    PointShape wgscenterpoint = projection.ConvertToInternalProjection(centerpoint) as PointShape;
                    UTMConstruction utmclass = zoomlevelfactory.GetUTMZone(wgscenterpoint);
                    int epsgcode = utmclass.epsgcode;
                    projection.ExternalProjectionParametersString = Proj4Projection.GetEpsgParametersString(epsgcode);
                    Proj4Projection projectionutm = new Proj4Projection(Proj4Projection.GetEpsgParametersString(epsgcode), Proj4Projection.GetGoogleMapParametersString());
                    projectionutm.Open();
                    PointShape gglcenterpoint = projection.ConvertToExternalProjection(centerpoint) as PointShape;
                    RectangleShape gglextent = projection.ConvertToExternalProjection(mapextent) as RectangleShape;
                    RectangleShape wgsextent = projection.ConvertToInternalProjection(mapextent) as RectangleShape;
                    RectangleShape utmextent = projectionutm.ConvertToInternalProjection(mapextent) as RectangleShape;
                    int currentzoomlevel = zoomlevelfactory.GetZoomLevelFromScale(Map.CurrentScale);
                    bool isthisagooglezoomlevel = zoomlevelfactory.VerifyGoogleZoomLevelFromScale(Map.CurrentScale);
                    if (!isthisagooglezoomlevel) { return; }

                    if (this.projectionFromSphericalMercator == null) {
                        string googleMapParametersString = ManagedProj4Projection.GetEpsgParametersString(3857);
                        string googleMapParametersString2 = ManagedProj4Projection.GetEpsgParametersString(3857);
                        this.projectionFromSphericalMercator = new ManagedProj4Projection(googleMapParametersString, googleMapParametersString2);
                        this.projectionFromSphericalMercator.Open();
                    }
                    RectangleShape gglextent2 = (RectangleShape)this.projectionFromSphericalMercator.ConvertToInternalProjection(mapextent);
                    if (this.projectionFromWGS84 == null) {
                        string wgsParametersString = ManagedProj4Projection.GetWgs84ParametersString();
                        string wgspParametersString2 = ManagedProj4Projection.GetWgs84ParametersString();
                        this.projectionFromWGS84 = new ManagedProj4Projection(wgsParametersString, wgspParametersString2);
                        this.projectionFromWGS84.Open();
                    }
                    RectangleShape wgsextent2 = (RectangleShape)this.projectionFromWGS84.ConvertToInternalProjection(mapextent);
                    PointShape wgscenterpoint2 = wgsextent2.GetCenterPoint();
                    UTMConstruction utmclass2 = zoomlevelfactory.GetUTMZone(wgscenterpoint2);
                    int epsgcode2 = utmclass2.epsgcode;
                    PointShape wgscenterpoint3 = (PointShape)this.projectionFromWGS84.ConvertToInternalProjection(centerpoint);
                    UTMConstruction utmclass3 = zoomlevelfactory.GetUTMZone(wgscenterpoint3);
                    int epsgcode3 = utmclass3.epsgcode;

                    if (this.projectionFromSM == null) {
                        string smParametersString = ManagedProj4Projection.GetWgs84ParametersString();
                        string smParametersString2 = ManagedProj4Projection.GetWgs84ParametersString();
                        this.projectionFromSM = new ManagedProj4Projection(smParametersString, smParametersString2);
                        this.projectionFromSM.Open();
                    }
                    RectangleShape wgsextent4 = (RectangleShape)this.projectionFromSM.ConvertToInternalProjection(mapextent);

                    log.Info("MapDisplay - Get Google imagery.");

                    //Map.Height = 640;
                    //Map.Width = 640;
                    int tileHeight = System.Convert.ToInt32(Map.ActualHeight);
                    int tileWidth = System.Convert.ToInt32(Map.ActualWidth);
                    Overlay googloverlay = SelectedOverlay.ExtentHasChanged(tileHeight, tileWidth, utmextent, gglextent2, wgscenterpoint, currentzoomlevel, epsgcode);
                    if (Map.Overlays.Contains("Bing")) {
                        Map.Overlays.Remove("Bing");
                    }
                    if (Map.Overlays.Contains("Bing Online")) {
                        Map.Overlays.Remove("Bing Online");
                    }
                    if (Map.Overlays.Contains("Roads Only")) {
                        Map.Overlays.Remove("Roads Only");
                    }
                    if (Map.Overlays.Contains("Road Online")) {
                        Map.Overlays.Remove("Road Online");
                    }
                    if (Map.Overlays.Contains("Google")) {
                        Map.Overlays.Remove("Google");
                    }
                    if (Map.Overlays.Contains("Google1")) {
                        Map.Overlays.Remove("Google1");
                    }
                    if (Map.Overlays.Contains("None")) {
                        Map.Overlays.Remove("None");
                    }
                    if (googloverlay != null) {
                        Map.Overlays.Add(SelectedOverlay.Name, googloverlay);
                        Map.Overlays.MoveToBottom(SelectedOverlay.Name);
                        Map.Overlays[SelectedOverlay.Name].Refresh();
                    }
                }
            };

            Map.MouseMove += (sender, e) => {
                try {
                    PointShape worldlocation2 = Map.ToWorldCoordinate(e.GetPosition(Map));
                    PointShape centerpoint = projection.ConvertToInternalProjection(worldlocation2) as PointShape;
                    Vertex geoVertex1 = new Vertex(centerpoint.X, centerpoint.Y);
                    string _DecimalDegreeString = Math.Round(geoVertex1.Y, 5) + ", " + Math.Round(geoVertex1.X, 5);
                    string displaycoordinates1 = DecimalDegrees.ddstring_To_DMs(_DecimalDegreeString, mapsettings.DisplayCoordinateFormat);
                    DisplayCoordinates = displaycoordinates1;
                }
                catch (Exception ex) {
                    log.Error("Pivottool - Error mouse move - {0}", ex);
                }
            };

        }

        private void LoadBingMapsOverlay() {
            if (!startedOutOnline) { return; }
            //IMapOverlay mapoverlay = new BingMapOverlay();
            //SelectedOverlay.NewOverlay();
            IMapOverlay mapoverlay = SelectedOverlay;
            Overlay bingOverlay = mapoverlay.Overlay;
            if (bingOverlay == null) { return; }
            if (Map.Overlays.Contains("Bing")) {
                Map.Overlays.Remove("Bing");
            }
            if (Map.Overlays.Contains("Bing Online")) {
                Map.Overlays.Remove("Bing Online");
            }
            if (Map.Overlays.Contains("Roads Only")) {
                Map.Overlays.Remove("Roads Only");
            }
            if (Map.Overlays.Contains("Road Online")) {
                Map.Overlays.Remove("Road Online");
            }
            if (Map.Overlays.Contains("Google")) {
                Map.Overlays.Remove("Google");
            }
            if (Map.Overlays.Contains("Google1")) {
                Map.Overlays.Remove("Google1");
            }
            if (Map.Overlays.Contains("None")) {
                Map.Overlays.Remove("None");
            }

            Map.Overlays.Add(mapoverlay.Name, bingOverlay);
            Map.Overlays.MoveToBottom(mapoverlay.Name);
            //zoomlevelfactory = new ZoomLevelFactory(mapsettings.ZoomLevel);
            ZoomLevelSet zls = SelectedOverlay.GetZoomLevelSet();
            if (zls == null) {
                zoomlevelfactory = new ZoomLevelFactory("Google40");
                //zoomlevelfactory = new ZoomLevelFactory(mapsettings.ZoomLevel);
                Map.ZoomLevelSet = zoomlevelfactory.ZoomLevelSET;
            }
            else {
                Map.ZoomLevelSet = SelectedOverlay.GetZoomLevelSet();
            }
        }


        private void OpenAndLoadaShapefile()
        {
            //log.Info("PivotTools - Create in memory layer.");
            CreateInMemoryLayerAndOverlay();
            //log.Info("PivotTools - load shape onto map.");
            LoadShapeFileOverlayOntoMap();
        }

        private void CreateInMemoryLayerAndOverlay()
        {
            layerOverlay = new LayerOverlay();
            layerOverlay.DrawingExceptionMode = DrawingExceptionMode.DrawException;
            currentLayer = new InMemoryFeatureLayer();
            AreaStyle currentLayerStyle = new AreaStyle(new GeoPen(GeoColor.StandardColors.Red, 2), new GeoSolidBrush(new GeoColor(20, GeoColor.StandardColors.LightGray)));
            currentLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = currentLayerStyle;
            currentLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            projection = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetGoogleMapParametersString());
            currentLayer.FeatureSource.Projection = projection;
            if (!projection.IsOpen) {
                projection.Open();
            }
            try {
                var shape1 = SelectedField.GetShape();
                if (shape1 != null) {
                    if (shape1 is PolygonShape) {
                        PolygonShape poly222 = shape1 as PolygonShape;
                        if (poly222.OuterRing.Vertices.Count > 3) {
                            currentLayer.InternalFeatures.Add(SelectedField);
                        }
                    }
                    if (shape1 is MultipolygonShape) {
                        MultipolygonShape currentPolygonShape = shape1 as MultipolygonShape;
                        if (currentPolygonShape.Polygons.Count > 0) {
                            if (currentPolygonShape.Polygons[0].OuterRing.Vertices.Count > 3) {
                                currentLayer.InternalFeatures.Add(SelectedField);
                            }
                        }
                    }
                }
            }
            catch { }
            //currentLayer.InternalFeatures.Add(SelectedField);
            layerOverlay.Layers.Add("ImportLayer", currentLayer);

            //measureLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customDistancePolygonStyleText);
            measureLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = null;
            measureLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = null;
            measureLayer.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = null;
            measureLayer.ZoomLevelSet.ZoomLevel01.DefaultTextStyle = null;
            measureLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Clear();
            measureLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customDistanceLineStyleText);
            AreaStyle measureLayerStyle = AreaStyles.CreateSimpleAreaStyle(new GeoColor(50, GeoColor.StandardColors.Orange), new GeoColor(200, GeoColor.StandardColors.Orange), 2);
            measureLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(measureLayerStyle);
            measureLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(lineStylePen);
            measureLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            measureLayer.FeatureSource.Projection = projection;
            layerOverlay.Layers.Add("measureLayer", measureLayer);
            popupOverlay = new PopupOverlay();
        }

        private void LoadShapeFileOverlayOntoMap()
        {
            if (Map.Overlays.Contains("importOverlay")) {
                Map.Overlays.Remove("importOverlay");
            }
            if (layerOverlay.Layers.Contains("ImportLayer")) {
                Map.Overlays.Add("importOverlay", layerOverlay);
            }

            EnsureCurrentLayerIsOpen();
            EnsureMeasureLayerIsOpen();
            var extent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
            try
            {
                var shapelayerfeatures = currentLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);
                if (shapelayerfeatures.Count > 0)
                {
                    var shapelayercolumns = currentLayer.FeatureSource.GetColumns();
                    extent = ExtentHelper.GetBoundingBoxOfItems(shapelayerfeatures);
                    notcenteredyet = true;
                    Map.CurrentExtent = extent;
                    notcenteredyet = false;
                    //PointShape center1 = CurrentMapExtent.GetCenterPoint();
                    //Map.CenterAt(center1);
                }
                else
                {
                    notcenteredyet = true;
                    Map.CurrentExtent = CurrentMapExtent;
                    notcenteredyet = false;
                    //PointShape center1 = CurrentMapExtent.GetCenterPoint();
                    //Map.CenterAt(center1);
                }
            }
            catch
            {
                notcenteredyet = true;
                Map.CurrentExtent = CurrentMapExtent;
                notcenteredyet = false;
                //PointShape center1 = CurrentMapExtent.GetCenterPoint();
                //Map.CenterAt(center1);
            }
        }

        private void EnsureCurrentLayerIsOpen()
        {
            if (currentLayer == null)
            {
                currentLayer = new InMemoryFeatureLayer();
            }

            if (!currentLayer.IsOpen)
            {
                currentLayer.Open();
            }

            if (!projection.IsOpen)
            {
                projection.Open();
            }
        }

        internal void EnsureMeasureLayerIsOpen()
        {
            if (measureLayer == null)
            {
                measureLayer = new InMemoryFeatureLayer();
            }

            if (!measureLayer.IsOpen)
            {
                measureLayer.Open();
            }

            if (!projection.IsOpen)
            {
                projection.Open();
            }
        }

        private void CutLine_TrackEnded(object sender, TrackEndedTrackInteractiveOverlayEventArgs e)
        {
            Map.TrackOverlay.TrackEnded -= new EventHandler<TrackEndedTrackInteractiveOverlayEventArgs>(CutLine_TrackEnded);
            Map.TrackOverlay.TrackMode = TrackMode.None;
            Map.Cursor = previousCursor;
            thiswasfromtracking = false;
            if (Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Count > 0)
            {
                int shapecount = Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Count - 1;
                Feature feat = Map.TrackOverlay.TrackShapeLayer.InternalFeatures[shapecount];
                BaseShape baseShape = feat.GetShape();
                if (baseShape is LineShape)
                {
                    try {
                        LineShape lineshape = baseShape as LineShape;
                        Vertex vert = lineshape.Vertices[0];
                        PointShape ps1 = new PointShape(vert);
                        PointShape ps2 = projection.ConvertToInternalProjection(ps1) as PointShape;
                        EllipseShape es2 = new EllipseShape(ps2, (radius * .3048), GeographyUnit.DecimalDegree, DistanceUnit.Meter);
                        PolygonShape newpoly2 = WeedPolygon(es2.ToPolygon(), pivotpoints);
                        PolygonShape newpoly = projection.ConvertToExternalProjection(newpoly2) as PolygonShape;

                        Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Clear();
                        Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Add(new Feature(newpoly));
                        Map.Refresh();
                    }
                    catch (Exception ex) {
                        log.Error("Pivottool - Error lineshape trackended - {0}", ex);
                    }
                }
                if (baseShape is PointShape) {
                    try {
                        PointShape ps1 = baseShape as PointShape;
                        PointShape ps2 = projection.ConvertToInternalProjection(ps1) as PointShape;
                        pointx = ps2.X;
                        pointy = ps2.Y;
                        Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Clear();
                        Map.TrackOverlay.Refresh();
                        thiswasfromtracking = true;
                        if (area == 0) {
                            Drawpivot();
                        }
                        else {
                            DrawPivotFromAreaCalculations();
                        }
                    }
                    catch (Exception ex) {
                        log.Error("Pivottool - Error pointshape trackended - {0}", ex);
                    }
                }
            }
        }

        private void Drawpivot(bool calculateFromArea = false) 
        {
            log.Info("PivotTools - Draw pivot dimensions");
            if (radius == 0) { return; }
            if (pointx == 0) { return; }
            if (pointy == 0) { return; }
            if (pivotpoints == 0) { return; }
            var centerpoint = new PointShape();
            centerpoint.X = pointx;
            centerpoint.Y = pointy;

            var ps2 = centerpoint;
            var localizedRadius = DataSourceCultureInformation.UsesImperialUnits ? radius * .3048 : radius;
            var es2 = new EllipseShape(ps2, localizedRadius, GeographyUnit.DecimalDegree, DistanceUnit.Meter);
            var newpoly2 = WeedPolygon(es2.ToPolygon(), pivotpoints);
            var newPolygonShape = projection.ConvertToExternalProjection(newpoly2) as PolygonShape;
            if (!calculateFromArea) {
                double areacalc = newpoly2.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
                double areacalc2 = es2.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
                area = Math.Round(areacalc, 2);
            }

            RaisePropertyChanged("Area");

            if (!thiswasfromtracking && Map.EditOverlay.EditShapesLayer.InternalFeatures.Count > 0) {
                Map.EditOverlay.EditShapesLayer.InternalFeatures.RemoveAt(Map.EditOverlay.EditShapesLayer.InternalFeatures.Count - 1);
            }
            thiswasfromtracking = false;
            Map.EditOverlay.EditShapesLayer.InternalFeatures.Add(new Feature(newPolygonShape));
            Map.EditOverlay.CalculateAllControlPoints();
            Map.EditOverlay.Refresh();
            Map.Cursor = Cursors.Hand;
        }

        private void DrawPivotFromAreaCalculations() {
            if (areaUnitString == "acre") {
                AreaMeasure acres = Acre.Self.GetMeasure(area) as AreaMeasure;
                Measure squaremeters = acres.CreateAs(AgC.UnitConversion.Area.SquareMeter.Self);
                double squaremetersDividedByPi = squaremeters.Value / Math.PI;
                //double radiusmeters = Math.Sqrt(squaremetersDividedByPi) / .99733845;
                double radiusmeters = Math.Sqrt(squaremetersDividedByPi) / multiplierconstant;
                LengthMeasure meters = Meter.Self.GetMeasure(radiusmeters) as LengthMeasure;
                Measure feet = meters.CreateAs(AgC.UnitConversion.Length.Foot.Self);
                radius = Math.Round(feet.Value, 2); 
                Drawpivot(true);
            }
            else {
                AreaMeasure hectares = Hectare.Self.GetMeasure(area) as AreaMeasure;
                Measure sqmeters = hectares.CreateAs(AgC.UnitConversion.Area.SquareMeter.Self);
                double areaDividedByPi = sqmeters.Value / Math.PI;
                //double radiusm = Math.Sqrt(areaDividedByPi) / .99733845;
                double radiusm = Math.Sqrt(areaDividedByPi) / multiplierconstant;
                LengthMeasure meters = Meter.Self.GetMeasure(radiusm) as LengthMeasure;
                radius = Math.Round(meters.Value, 2);
                Drawpivot(true);
            }
        }

        private PolygonShape WeedPolygonPercent(PolygonShape weedpolygon, double pivotverticespertent)
        {
            Collection<Vertex> vertexcol = weedpolygon.OuterRing.Vertices;
            Collection<Vertex> vertexnew = new Collection<Vertex>();
            double vertexcount = vertexcol.Count;
            double vertexcountnew2 = Math.Truncate(vertexcount * (pivotverticespertent / 100));
            double vertexcountnew = Math.Truncate(vertexcount / vertexcountnew2);
            double i = 0;
            foreach (Vertex vert in vertexcol)
            {
                i++;
                if (i == 1)
                {
                    vertexnew.Add(vert);
                }
                if (i >= vertexcountnew)
                {
                    i = 0;
                }
            }
            RingShape ringshape = new RingShape(vertexnew);
            PolygonShape newpoly = new PolygonShape(ringshape);
            return newpoly;
        }

        private PolygonShape WeedPolygon(PolygonShape weedpolygon, double pivotvertices)
        {
            Collection<Vertex> vertexcol = weedpolygon.OuterRing.Vertices;
            double vertexcount = vertexcol.Count;
            double vertexpercent = Math.Round(pivotvertices / vertexcount, 2) * 100;
            return WeedPolygonPercent(weedpolygon, vertexpercent);
        }

        private void SavePolygon()
        {
            IsNotMeasuring = true;
            log.Info("PivotTools - Save Pivot");
            thiswasfromtracking = false;
            EnsureCurrentLayerIsOpen();
            EnsureMeasureLayerIsOpen();
            if (!projection.IsOpen) {
                projection.Open();
            }
            try {
                MultipolygonShape multipoly = new MultipolygonShape();
                if (Map.EditOverlay.EditShapesLayer.InternalFeatures.Count > 0) {
                    foreach (Feature feature in Map.EditOverlay.EditShapesLayer.InternalFeatures) {
                        BaseShape baseShape = feature.GetShape();
                        MultipolygonShape basepoly = new MultipolygonShape();
                        if (baseShape is PolygonShape) {
                            PolygonShape ps = baseShape as PolygonShape;
                            basepoly.Polygons.Add(ps);
                        }
                        if (baseShape is MultipolygonShape) {
                            basepoly = baseShape as MultipolygonShape;
                        }
                        if (multipoly.Polygons.Count == 0) {
                            multipoly = basepoly;
                            continue;
                        }
                        if (baseShape is PolygonShape || baseShape is MultipolygonShape) {
                            MultipolygonShape multipoly2 = new MultipolygonShape(multipoly.Polygons);
                            multipoly = multipoly2.Union(feature);
                        }
                    }
                }
                if (multipoly.Polygons.Count > 0) {
                    var importmessage = new PivotShapeMessage() { ShapeBase = multipoly, DrawingPivot = drawingpivot };
                    Messenger.Default.Send<PivotShapeMessage>(importmessage);
                }
            }
            catch (Exception ex) {
                log.Error(String.Format("PivotTools - Error save polygon - {0}", ex));
                System.Windows.MessageBox.Show(Strings.PivotToolsCaughtException_Text);
            }
        }

        private void ResetAndRefreshLayers()
        {
            Map.TrackOverlay.TrackShapeLayer.Open();
            Map.TrackOverlay.TrackShapeLayer.Clear();
            Map.TrackOverlay.TrackShapeLayer.Close();
            Map.EditOverlay.EditShapesLayer.Open();
            Map.EditOverlay.EditShapesLayer.Clear();
            Map.EditOverlay.CalculateAllControlPoints();
            Map.EditOverlay.EditShapesLayer.Close();
            Map.Overlays["fieldOverlay"].Refresh();
            Map.TrackOverlay.Refresh();
            Map.EditOverlay.Refresh();
        }

        public void HandleMeasureLine()
        {
            IsNotMeasuring = false;
            log.Info("PivotTools - Draw measure line");
            //MapStyles.CustomDistancePolygonStyle customDistancePolygonStyleText = new MapStyles.CustomDistancePolygonStyle(mapAreaUnit, mapDistanceUnit, new GeoFont("Arial", measurefontsize, DrawingFontStyles.Regular), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.White), new GeoSolidBrush(GeoColor.StandardColors.Red), new GeoPen(GeoColor.StandardColors.Red));
            //customDistancePolygonStyleText.YOffsetInPixel = -20;
            //MapStyles.CustomDistanceLineStyle customDistanceLineStyleText = new MapStyles.CustomDistanceLineStyle(mapDistanceUnit, new GeoFont("Arial", measurefontsize, DrawingFontStyles.Regular), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.DarkRed, 2), new GeoPen(GeoColor.StandardColors.DarkBlue, 2));
            //LineStyle lineStylePen = new LineStyle(new GeoPen(GeoColor.FromArgb(150, GeoColor.StandardColors.Blue), 2));
            Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = null;
            Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = null;
            Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = null;
            Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.DefaultTextStyle = null;
            Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Clear();
            //Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customDistancePolygonStyleText);
            Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customDistanceLineStyleText);
            Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(lineStylePen);
            Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(areaStyleBrush);
            Map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            Map.TrackOverlay.TrackEnded += new EventHandler<TrackEndedTrackInteractiveOverlayEventArgs>(TrackOverlay_TrackEnded);
            Map.TrackOverlay.TrackMode = TrackMode.Line;
            Map.MapClick += new EventHandler<MapClickWpfMapEventArgs>(map_MapClick);
            Map.KeyDown += new KeyEventHandler(keyEvent_Esc);
            previousCursor = Map.Cursor;
            Map.Cursor = Cursors.Pen;
        }

        private void TrackOverlay_TrackEnded(object sender, TrackEndedTrackInteractiveOverlayEventArgs e)
        {
            Map.TrackOverlay.TrackEnded -= new EventHandler<TrackEndedTrackInteractiveOverlayEventArgs>(TrackOverlay_TrackEnded);
            Map.TrackOverlay.TrackMode = TrackMode.None;
            Map.MapClick -= new EventHandler<MapClickWpfMapEventArgs>(map_MapClick);
            Map.KeyDown -= new KeyEventHandler(keyEvent_Esc);
            Map.Cursor = previousCursor;
            SaveMeasureLayer();
        }

        //private void SaveMeasureLayer()
        //{
        //    var features = Map.TrackOverlay.TrackShapeLayer.InternalFeatures;
        //    foreach (Feature feat in features)
        //    {
        //        var shape = feat.GetShape();
        //        if (shape is LineBaseShape)
        //        {
        //            if (shape is LineShape)
        //            {
        //                LineShape lshape = projection.ConvertToInternalProjection(shape) as LineShape;
        //                measureLayer.InternalFeatures.Add(new Feature(lshape));
        //            }
        //            if (shape is MultilineShape)
        //            {
        //                MultilineShape mlshape = projection.ConvertToInternalProjection(shape) as MultilineShape;
        //                measureLayer.InternalFeatures.Add(new Feature(mlshape));
        //            }
        //        }
        //    }
        //    Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Clear();
        //    Map.Refresh();
        //}

        private void keyEvent_Esc(object sender, KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Escape)
            {
                if (Map.TrackOverlay.TrackMode != TrackMode.None)
                {
                    Map.TrackOverlay.TrackEnded -= new EventHandler<TrackEndedTrackInteractiveOverlayEventArgs>(CutLine_TrackEnded);
                    Map.MapClick -= new EventHandler<MapClickWpfMapEventArgs>(map_MapClick);
                    Map.KeyDown -= new KeyEventHandler(keyEvent_Esc);

                    if (Map.TrackOverlay.TrackMode == TrackMode.Polygon || Map.TrackOverlay.TrackMode == TrackMode.Line || Map.TrackOverlay.TrackMode == TrackMode.Circle)
                    {
                        Map.TrackOverlay.MouseDoubleClick(new InteractionArguments());
                    }
                    else
                    {
                        Map.TrackOverlay.MouseUp(new InteractionArguments());
                    }

                    Map.TrackOverlay.TrackMode = TrackMode.None;
                    Map.Cursor = previousCursor;

                    int count = Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Count;
                    if (count > 0)
                    {
                        Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Remove("InTrackingFeature");
                    }
                    Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Clear();
                    Map.TrackOverlay.Refresh();

                }
            }
        }

        private void map_MapClick(object sender, MapClickWpfMapEventArgs e)
        {
            if (!Map.IsFocused)
            {
                Map.Focus();
            }
        }

        void DrawPivotCenter()
        {
            if (!IsNotMeasuring) { return; }
            log.Info("PivotTools - Set Pivot Center");
            Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Clear();
            Map.Refresh();
            Map.TrackOverlay.TrackEnded += new EventHandler<TrackEndedTrackInteractiveOverlayEventArgs>(CutLine_TrackEnded);
            Map.TrackOverlay.TrackMode = TrackMode.Point;
            previousCursor = Map.Cursor;
            Map.Cursor = Cursors.Pen;
        }

        void ExitCropzone()
        {
            IsNotMeasuring = true;
            log.Info("PivotTools - Cancel pivot");
            Map.Cursor = Cursors.Arrow;
            var importmessage = new ShapeImportMessage() { ShapeBase = new MultipolygonShape() };
            Messenger.Default.Send<ShapeImportMessage>(importmessage);
        }

        internal void SaveMeasureLayer() {
            log.Info("PivotTools - Save measure line");
            try {
                var features = Map.TrackOverlay.TrackShapeLayer.InternalFeatures;
                foreach (Feature feat in features) {
                    var shape = feat.GetShape();
                    PointShape centerpoint = shape.GetCenterPoint();
                    BaseShape shape2 = projection.ConvertToInternalProjection(shape);
                    BaseShape bs1 = DisplayMeasureBalloon(shape);
                    BaseShape bs2 = projection.ConvertToInternalProjection(bs1);
                    measureLayer.InternalFeatures.Add(new Feature(bs2));
                    double areaTotal = 0;
                    double perimeterTotal = 0;
                    string displayerText = string.Empty;
                    if (bs2 is PolygonShape) {
                        measureLayer.InternalFeatures.Add(new Feature(shape2));
                        PolygonShape poly1 = bs2 as PolygonShape;
                        areaTotal = poly1.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
                        perimeterTotal = poly1.GetPerimeter(GeographyUnit.DecimalDegree, mapDistanceUnit);
                        Measure perimetermeasure = MapUnitFactory.LengthConversion(mapDistanceUnit.ToString(), perimeterTotal);
                        Measure areameasure = MapUnitFactory.AreaConversion(mapAreaUnit.ToString(), areaTotal);
                        displayerText = $"{Strings.Area_Text}: {areameasure.FullDisplay} \r\n{Strings.Perimeter_Text}: {perimetermeasure.FullDisplay}";
                    }
                    if (bs2 is LineShape) {
                        LineShape line1 = bs2 as LineShape;
                        perimeterTotal = line1.GetLength(GeographyUnit.DecimalDegree, mapDistanceUnit);
                        Measure perimetermeasure = MapUnitFactory.LengthConversion(mapDistanceUnit.ToString(), perimeterTotal);
                        displayerText = $"{Strings.Length_Text}: {perimetermeasure.FullDisplay}";
                        LineShape lineshape2 = shape as LineShape;
                        for (int xyz = 0; xyz <= (lineshape2.Vertices.Count / 2); xyz++) {
                            centerpoint = new PointShape(lineshape2.Vertices[xyz]);
                        }
                    }
                    Popup popup = new Popup(centerpoint);
                    popup.Loaded += new System.Windows.RoutedEventHandler(popup_Loaded);
                    popup.MouseDoubleClick += new MouseButtonEventHandler(mouse_DoubleClick);
                    System.Windows.Controls.TextBox displayer = new System.Windows.Controls.TextBox();
                    displayer.Text = displayerText;
                    popup.Content = displayer;
                    //popup.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(255, 0, 0));
                    popupOverlay.Popups.Add(popup);
                    popupOverlay.Refresh();
                }
                Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Clear();
                Map.Refresh();
            }
            catch (Exception ex) {
                log.Error("Error saving measure line - {0}", ex);
                System.Windows.MessageBox.Show(Strings.ThereWasAProblemSavingAMeasureLine_Text);
            }
            IsNotMeasuring = true;
            
        }

        void popup_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            Popup p = sender as Popup;
            //System.Windows.Controls.Border b = p.Template.FindName("BorderTemplate", p) as System.Windows.Controls.Border;
            //b.CornerRadius = new System.Windows.CornerRadius(10);
        }

        void mouse_DoubleClick(object sender, MouseButtonEventArgs e) {
            Popup p = sender as Popup;
            if (popupOverlay.Popups.Contains(p)) {
                popupOverlay.Popups.Remove(p);
                popupOverlay.Refresh();
            }
        }

        private BaseShape DisplayMeasureBalloon(BaseShape shape) {
            if (shape is LineBaseShape) {
                if (shape is LineShape) {
                    LineShape lineshape = shape as LineShape;
                    var firstvert = lineshape.Vertices.First();
                    var lastvert = lineshape.Vertices.Last();
                    var allverts = from vt in lineshape.Vertices
                                   select vt;

                    PointShape point1 = new PointShape(firstvert);
                    PointShape point2 = new PointShape(lastvert);
                    if (lineshape.Vertices.Count > 2) {
                        ScreenPointF screenPoint1 = ExtentHelper.ToScreenCoordinate(Map.CurrentExtent, point1, System.Convert.ToSingle(Map.ActualWidth), System.Convert.ToSingle(Map.ActualHeight));
                        ScreenPointF screenPoint2 = ExtentHelper.ToScreenCoordinate(Map.CurrentExtent, point2, System.Convert.ToSingle(Map.ActualWidth), System.Convert.ToSingle(Map.ActualHeight));
                        double distance = Math.Sqrt(((screenPoint1.X - screenPoint2.X) * (screenPoint1.X - screenPoint2.X)) + ((screenPoint1.Y - screenPoint2.Y) * (screenPoint1.Y - screenPoint2.Y)));
                        if (distance >= -10 && distance <= 10) {
                            PolygonShape polygon = new PolygonShape();
                            Collection<Vertex> collectvertex = lineshape.Vertices;
                            collectvertex.Add(lineshape.Vertices[0]);
                            RingShape rs = new RingShape(collectvertex);
                            polygon.OuterRing = rs;
                            return polygon;
                        }
                    }
                    return lineshape;
                }
                if (shape is MultilineShape) {
                    MultilineShape mlshape = shape as MultilineShape;
                    var firstvert = mlshape.Lines.First().Vertices.First();
                    var lastvert = mlshape.Lines.Last().Vertices.Last();
                    var allverts = from ml in mlshape.Lines
                                   from vt in ml.Vertices
                                   select vt;

                    PointShape point1 = new PointShape(firstvert);
                    PointShape point2 = new PointShape(lastvert);
                    if (allverts.Count() > 2) {
                        ScreenPointF screenPoint1 = ExtentHelper.ToScreenCoordinate(Map.CurrentExtent, point1, System.Convert.ToSingle(Map.ActualWidth), System.Convert.ToSingle(Map.ActualHeight));
                        ScreenPointF screenPoint2 = ExtentHelper.ToScreenCoordinate(Map.CurrentExtent, point2, System.Convert.ToSingle(Map.ActualWidth), System.Convert.ToSingle(Map.ActualHeight));
                        double distance = Math.Sqrt(((screenPoint1.X - screenPoint2.X) * (screenPoint1.X - screenPoint2.X)) + ((screenPoint1.Y - screenPoint2.Y) * (screenPoint1.Y - screenPoint2.Y)));
                        if (distance >= -10 && distance <= 10) {
                            PolygonShape polygon = new PolygonShape();
                            polygon.OuterRing = new RingShape(allverts);
                            return polygon;
                        }
                    }
                    return mlshape;
                }
            }
            return null;
        }

        private MultipolygonShape map_ScaleShape(MultipolygonShape shapecalc) {
            double shapearea101 = 0;
            MultipolygonShape shape = null;
            MultipolygonShape shape2 = new MultipolygonShape();
            shape = shapecalc;
            shape2 = shapecalc;
            MultipolygonShape shape3 = new MultipolygonShape();

            if (shape != null) {
                shapearea101 = shape2.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
                OldArea = shapearea101;

                double ndiff = -.0001;
                double pdiff = .0001;
                double counter = 0;
                double areadifference = DesiredArea - shapearea101;
                if (areadifference < 0) {
                    areadifference = areadifference * -1;
                }
                ScalePercent = (areadifference / OldArea) * 2;

                while (areadifference < ndiff || areadifference > pdiff) {
                    shapearea101 = shape.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);

                    if (DesiredArea > shapearea101) {
                        shape.ScaleUp(System.Convert.ToSingle(ScalePercent));
                    }
                    else {
                        shape.ScaleDown(System.Convert.ToSingle(ScalePercent));
                    }

                    shapearea101 = shape.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
                    areadifference = DesiredArea - shapearea101;
                    if (areadifference < 0) {
                        areadifference = areadifference * -1;
                    }
                    ScalePercent = (areadifference / OldArea) * 2;

                    counter++;
                    if (counter > 1000) {
                        break;
                    }
                }
                return shape;
            }
            return null;
        }
    }
}
