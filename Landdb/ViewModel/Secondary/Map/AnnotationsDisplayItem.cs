﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Landdb.Resources;

namespace Landdb.ViewModel.Secondary.Map
{
    public class AnnotationsDisplayItem
    {
        static AnnotationsDisplayItem() {
            CreateAnnotationStyleDictionary();
        }

        public enum AnnotationStyleType {
            DrainTiles,
            DrainagePump,
            IrrigationWell,
            PivotPoint,
            WaterLine,
            WaterRiser,

            GrainBins,
            PackingHouse,
            PotatoCellar,
            SilageBag,
            SilagePile,
            SilagePit,
            Silo,
            StorageFacility,

            CottonGin,
            EthanolPlant,
            GrainElevator,
            ProcessorFacility,

            Building,
            ChemicalStorage,
            FarmBuildings,
            FarmShop,
            FeedLot,

            FieldEntrance,
            Label,

            PowerLines,
            UndergroundElectricLine,
            UndergroundMediaLine,
            UndergroundGasLine,

            BufferZone,
            Houses,
            NoFlyZone,
            OpenDrainageDitch,
            Schools,
            Terrace,
            Waterway,

            DuckBlind,
            DeerStand,
            FarmBoundary,
            FoodPlot,
            FuelTank,
            PortaPotty,
            Wetlands
        }

        public AnnotationsDisplayItem(string annotationStyleName) {
            AnnotationStyle = GetAnnotationStyleTypeFor(annotationStyleName);
        }
        public AnnotationStyleType AnnotationStyle { get; }
        public string DisplayText => GetAnnotationStyleDisplayTextFor(AnnotationStyle);
        public string KeyText => GetAnnotationStyleKeyTextFor(AnnotationStyle);
        public static Dictionary<AnnotationStyleType, (string untranslatedKey, string translatedValue)> AnnotationStyleDictionary { get; private set; }

        public static AnnotationStyleType GetAnnotationStyleTypeFor(string annotationStyleName) {
            try {
                return AnnotationStyleDictionary.First(x => x.Value.untranslatedKey == annotationStyleName).Key;
            } catch (Exception e) {
                Console.WriteLine(e);
                throw e;
            }
        }

        public static string GetAnnotationStyleDisplayTextFor(AnnotationStyleType annotationStyleType) {
            try {
                return AnnotationStyleDictionary[annotationStyleType].translatedValue;
            }
            catch (Exception e) {
                Console.WriteLine(e);
                throw new ArgumentOutOfRangeException(nameof(annotationStyleType), annotationStyleType, null);
            }
        }

        public static string GetAnnotationStyleDisplayTextFor(string annotationStyleKeyText) {
            try {
                var thisDisplayItem = new AnnotationsDisplayItem(annotationStyleKeyText);
                return thisDisplayItem.DisplayText;
            } catch (Exception e) {
                Console.WriteLine(e);
                throw e;
            }
        }

        public static string GetAnnotationStyleKeyTextFor(AnnotationStyleType annotationStyleType) {
            try {
                return AnnotationStyleDictionary[annotationStyleType].untranslatedKey;
            } catch (Exception e) {
                Console.WriteLine(e);
                throw new ArgumentOutOfRangeException(nameof(annotationStyleType), annotationStyleType, null);
            }
        }

        private static void CreateAnnotationStyleDictionary() {
            AnnotationStyleDictionary = new Dictionary<AnnotationStyleType, (string, string)> {
                {AnnotationStyleType.DrainTiles, ("Drain Tiles", Strings.MapAnnotationCategory_DrainTiles)},
                {AnnotationStyleType.DrainagePump, ("Drainage Pump", Strings.MapAnnotationCategory_DrainagePump)},
                {AnnotationStyleType.IrrigationWell, ("Irrigation Well", Strings.MapAnnotationCategory_IrrigationWell)},
                {AnnotationStyleType.PivotPoint, ("Pivot Point", Strings.MapAnnotationCategory_PivotPoint)},
                {AnnotationStyleType.WaterLine, ("Water Line", Strings.MapAnnotationCategory_WaterLine)},
                {AnnotationStyleType.WaterRiser, ("Water Riser", Strings.MapAnnotationCategory_WaterRiser)},
                {AnnotationStyleType.GrainBins, ("Grain Bins", Strings.MapAnnotationCategory_GrainBins)},
                {AnnotationStyleType.PackingHouse, ("Packing House", Strings.MapAnnotationCategory_PackingHouse)},
                {AnnotationStyleType.PotatoCellar, ("Potato Cellar", Strings.MapAnnotationCategory_PotatoCellar)},
                {AnnotationStyleType.SilageBag, ("Silage Bag", Strings.MapAnnotationCategory_SilageBag)},
                {AnnotationStyleType.SilagePile, ("Silage Pile", Strings.MapAnnotationCategory_SilagePile)},
                {AnnotationStyleType.SilagePit, ("Silage Pit", Strings.MapAnnotationCategory_SilagePit)},
                {AnnotationStyleType.Silo, ("Silo", Strings.MapAnnotationCategory_Silo)},
                {AnnotationStyleType.StorageFacility, ("Storage Facility", Strings.MapAnnotationCategory_StorageFacility)},
                {AnnotationStyleType.CottonGin, ("Cotton Gin", Strings.MapAnnotationCategory_CottonGin)},
                {AnnotationStyleType.EthanolPlant, ("Ethanol Plant", Strings.MapAnnotationCategory_EthanolPlant)},
                {AnnotationStyleType.GrainElevator, ("Grain Elevator", Strings.MapAnnotationCategory_GrainElevator)},
                {AnnotationStyleType.ProcessorFacility, ("Processor Facility", Strings.MapAnnotationCategory_ProcessorFacility)},
                {AnnotationStyleType.Building, ("Building", Strings.MapAnnotationCategory_Building)},
                {AnnotationStyleType.ChemicalStorage, ("Chemical Storage", Strings.MapAnnotationCategory_ChemicalStorage)},
                {AnnotationStyleType.FarmBuildings, ("Farm Buildings", Strings.MapAnnotationCategory_FarmBuildings)},
                {AnnotationStyleType.FarmShop, ("Farm Shop", Strings.MapAnnotationCategory_FarmShop)},
                {AnnotationStyleType.FeedLot, ("Feed Lot", Strings.MapAnnotationCategory_FeedLot)},
                {AnnotationStyleType.FieldEntrance, ("Field Entrance", Strings.MapAnnotationCategory_FieldEntrance)},
                {AnnotationStyleType.Label, ("Label", Strings.MapAnnotationCategory_Label)},
                {AnnotationStyleType.PowerLines, ("Power Lines", Strings.MapAnnotationCategory_PowerLines)},
                {AnnotationStyleType.UndergroundElectricLine, ("Underground Electric Line", Strings.MapAnnotationCategory_UndergroundElectricLine)},
                {AnnotationStyleType.UndergroundMediaLine, ("Underground Media Line", Strings.MapAnnotationCategory_UndergroundMediaLine)},
                {AnnotationStyleType.UndergroundGasLine, ("Underground Gas Line", Strings.MapAnnotationCategory_UndergroundGasLine)},
                {AnnotationStyleType.BufferZone, ("Buffer Zone", Strings.MapAnnotationCategory_BufferZone)},
                {AnnotationStyleType.Houses, ("Houses", Strings.MapAnnotationCategory_Houses)},
                {AnnotationStyleType.NoFlyZone, ("No Fly Zone", Strings.MapAnnotationCategory_NoFlyZone)},
                {AnnotationStyleType.OpenDrainageDitch, ("Open Drainage Ditch", Strings.MapAnnotationCategory_OpenDrainageDitch)},
                {AnnotationStyleType.Schools, ("Schools", Strings.MapAnnotationCategory_Schools)},
                {AnnotationStyleType.Terrace, ("Terrace", Strings.MapAnnotationCategory_Terrace)},
                {AnnotationStyleType.Waterway, ("Waterway", Strings.MapAnnotationCategory_Waterway)},
                {AnnotationStyleType.DuckBlind, ("Duck Blind", Strings.MapAnnotationCategory_DuckBlind)},
                {AnnotationStyleType.DeerStand, ("Deer Stand", Strings.MapAnnotationCategory_DeerStand)},
                {AnnotationStyleType.FarmBoundary, ("Farm Boundary", Strings.MapAnnotationCategory_FarmBoundary)},
                {AnnotationStyleType.FoodPlot, ("Food Plot", Strings.MapAnnotationCategory_FoodPlot)},
                {AnnotationStyleType.FuelTank, ("Fuel Tank", Strings.MapAnnotationCategory_FuelTank)},
                {AnnotationStyleType.PortaPotty, ("Porta Potty", Strings.MapAnnotationCategory_PortaPotty)},
                {AnnotationStyleType.Wetlands, ("Wetlands", Strings.MapAnnotationCategory_Wetlands)}
            };
        }

        private bool AnnotationStyleEquals(AnnotationStyleType toCompare) {
            if (AnnotationStyle.Equals(toCompare))
                return true;
            else 
                return false;
        }

        public override bool Equals(object toCompare) {
            var compareItem = toCompare as AnnotationsDisplayItem;
            return AnnotationStyleEquals(compareItem.AnnotationStyle);
        }

        public override string ToString() {
            return DisplayText;
        }
    }
}
