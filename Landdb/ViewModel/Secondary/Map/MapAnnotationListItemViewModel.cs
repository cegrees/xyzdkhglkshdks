﻿using System;
using System.Collections.Generic;
using System.Linq;
using GalaSoft.MvvmLight;
using Landdb.Domain.ReadModels.YieldLocation;
using Landdb.Domain.ReadModels.MapAnnotation;

namespace Landdb.ViewModel.Secondary.Map {
	public class MapAnnotationListItemViewModel : ViewModelBase {
		public MapAnnotationListItemViewModel() { }

		public MapAnnotationListItemViewModel(MapAnnotationListItem li) {
			Id = li.Id;
			Name = li.Name;
		}

		public MapAnnotationListItemViewModel(MapAnnotationListItem li, int currentCropYear)
			: this(li) {
		}

		public MapAnnotationId Id { get; set; }
		public string Name { get; set; }

		public override string ToString() {
			return Name;
		}
	}
}