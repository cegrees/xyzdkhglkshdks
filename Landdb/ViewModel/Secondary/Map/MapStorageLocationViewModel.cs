﻿using AgC.UnitConversion;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Map;
using Landdb.Domain.ReadModels.MapAnnotation;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Domain.ReadModels.YieldLocation;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.MapStyles;
using Landdb.ViewModel.Fields.FieldDetails;
using Landdb.ViewModel.Fields.FieldDetails.MapModels;
using Landdb.ViewModel.Maps.ImageryOverlay;
using Landdb.ViewModel.Yield;
using NLog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Resources;
using ThinkGeo.MapSuite.Core;
using ThinkGeo.MapSuite.WpfDesktopEdition;

namespace Landdb.ViewModel.Secondary.Map
{
    public class MapStorageLocationViewModel : ViewModelBase
    {
        IClientEndpoint clientEndpoint;
        readonly Dispatcher dispatcher;

        Guid currentDataSourceId;
        int currentCropYear;
        Logger log = LogManager.GetCurrentClassLogger();

        IStyleFactory styleFactory;

        bool canLoadAnnotations = false;
        MapStyles.AnnotationMultiStyle customMultiStyles = new MapStyles.AnnotationMultiStyle();
        AreaStyle unselectedarea = new AreaStyle(new GeoSolidBrush(GeoColor.FromArgb(255, GeoColor.StandardColors.Transparent)));
        LineStyle unselectedline = new LineStyle(new GeoPen(GeoColor.FromArgb(255, GeoColor.StandardColors.Transparent), 2));
        PointStyle unselectedpoint = PointStyles.CreateSimplePointStyle(PointSymbolType.Circle, new GeoColor(255, GeoColor.StandardColors.Transparent), 10);
        LineStyle lineStylePen = new LineStyle(new GeoPen(GeoColor.FromArgb(150, GeoColor.StandardColors.Orange), 4));
        AreaStyle areaStyleBrush = new AreaStyle(new GeoSolidBrush(GeoColor.StandardColors.Blue));
        ClientLayerAreaStyle fieldsLayerAreaStyle = null;
        ZoomLevelFactory zoomlevelfactory = new ZoomLevelFactory();
        List<AnnotationStyleItem> changedstyles = new List<AnnotationStyleItem>();
        Dictionary<string, MapAnnotationCategory> userstylenames = new Dictionary<string, MapAnnotationCategory>();
        Proj4Projection projection;

        string fieldsselectedfillcolor;
        string fieldsunselectedfillcolor;
        string fieldsselectedbordercolor;
        string fieldsunselectedbordercolor;
        GeoColor fieldsselectedGeoColor = GeoColor.StandardColors.Yellow;
        GeoColor fieldsunselectedGeoColor = GeoColor.StandardColors.Gray;
        GeoColor fieldsselectedborderGeoColor = new GeoColor();
        GeoColor fieldsunselectedborderGeoColor = new GeoColor();
        Cursor previousCursor;

        string areaUnitString = string.Empty;
        string coordinateformat = string.Empty;
        AgC.UnitConversion.Area.AreaUnit agcAreaUnit;
        ThinkGeo.MapSuite.Core.AreaUnit mapAreaUnit;
        ThinkGeo.MapSuite.Core.DistanceUnit mapDistanceUnit;
        int fieldopacity = 125;
        int annotationopacity = 125;
        MapSettings mapsettings;
        bool maploaded = false;
        YieldLocationId selectedYieldLocationId = new YieldLocationId();
        private IList<IDomainCommand> commandlist = new List<IDomainCommand>();

        public MapStorageLocationViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, YieldLocationId selectedYieldLocationId)
        {
            this.clientEndpoint = clientEndpoint;
            //this.parent = mapViewModel;
            this.dispatcher = dispatcher;
            this.currentDataSourceId = ApplicationEnvironment.CurrentDataSourceId;
            this.currentCropYear = ApplicationEnvironment.CurrentCropYear;
            this.selectedYieldLocationId = selectedYieldLocationId;

            var locationDetailsMaybe = clientEndpoint.GetView<YieldLocationDetailsView>(selectedYieldLocationId);
            var locationDetails = locationDetailsMaybe.HasValue ? locationDetailsMaybe.Value : null;
            var locationName = locationDetails != null ? locationDetails.Name : string.Empty;

            AnnotationName = locationName;
            RaisePropertyChanged(() => AnnotationName);

            styleFactory = new StyleFactory();
            changedstyles = new List<AnnotationStyleItem>();
            userstylenames = new Dictionary<string, MapAnnotationCategory>();
            commandlist = new List<IDomainCommand>();

            //BuildCurrentExtentBasedOnFarm();
            InitializeSettings();
            scalingTextStyle = new ScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, mapsettings.ScalingTextMaximumSize, this.mapsettings.ScalingTextMinimumSize);
            scalingTextStyle2 = new AnnotationScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, mapsettings.ScalingTextMaximumSize, this.mapsettings.ScalingTextMinimumSize);
            scalingTextStyle2.ColumnsList = new List<string>() { @"Id", @"LdbLabel", @"TypeName", @"ShapeType" };

            fieldsselectedGeoColor = GeoColor.StandardColors.Yellow;
            fieldsunselectedGeoColor = GeoColor.StandardColors.Gray;
            GeoColor fieldsunselectedGeoColor2 = GeoColor.StandardColors.Gray;

            fieldsselectedfillcolor = mapsettings.FieldsSelectedFillColor;
            fieldsunselectedfillcolor = mapsettings.FieldsUnSelectedFillColor;
            string fieldsunselectedfillcolor2 = GeoColor.ToHtml(fieldsunselectedGeoColor2);
            if (!string.IsNullOrEmpty(fieldsselectedfillcolor))
            {
                fieldsselectedGeoColor = GeoColor.FromHtml(fieldsselectedfillcolor);
            }
            if (!string.IsNullOrEmpty(fieldsunselectedfillcolor))
            {
                fieldsunselectedGeoColor = GeoColor.FromHtml(fieldsunselectedfillcolor);
                fieldsunselectedGeoColor2 = GeoColor.FromHtml(fieldsunselectedfillcolor2);
            }

            fieldsselectedbordercolor = mapsettings.FieldsSelectedBorderColor;
            fieldsunselectedbordercolor = mapsettings.FieldsUnSelectedBorderColor;
            if (!string.IsNullOrEmpty(fieldsselectedbordercolor))
            {
                fieldsselectedborderGeoColor = GeoColor.FromHtml(fieldsselectedbordercolor);
            }
            else
            {
                fieldsselectedborderGeoColor = fieldsselectedGeoColor;
            }
            if (!string.IsNullOrEmpty(fieldsunselectedbordercolor))
            {
                fieldsunselectedborderGeoColor = GeoColor.FromHtml(fieldsunselectedbordercolor);
            }
            else
            {
                fieldsunselectedborderGeoColor = fieldsunselectedGeoColor;
            }

            YieldLocationList = new ObservableCollection<YieldLocationListItemViewModel>();
            //RefreshDestinationLocationList();

            if (maploaded) { return; }
            //canLoadAnnotations = true;
            InitializeMap();
            areaStyleBrush = new AreaStyle(new GeoPen(new GeoSolidBrush(new GeoColor(50, GeoColor.StandardColors.Yellow)), 3), new GeoSolidBrush(GeoColor.StandardColors.Blue));

            Messenger.Default.Register<DataSourceChangedMessage>(this, OnDataSourceChanged);
            Messenger.Default.Register<RemoteEventReceived>(this, OnRemoteEventReceived);

            CreateCommand = new RelayCommand(MapLocationAndAssociate);
            CloseCommand = new RelayCommand(Close);
            DrawNew = new RelayCommand(DrawShapesMethod);
        }

        public ICommand CreateCommand { get; set; }
        public ICommand CloseCommand { get; set; }
        public ICommand DrawNew { get; set; }
        #region Private Properties
        private LayerOverlay FieldOverlay { get; set; }
        private LayerOverlay layerOverlay { get; set; }
        private InMemoryFeatureLayer currentLayer { get; set; }
        private InMemoryFeatureLayer FieldsLayer { get; set; }
        private InMemoryFeatureLayer importLayer { get; set; }
        private InMemoryFeatureLayer measureLayer { get; set; }
        private PopupOverlay popupOverlay { get; set; }
        private bool InternetAvailable { get; set; }

        private ScalingTextStyle scalingTextStyle { get; set; }
        private AnnotationScalingTextStyle scalingTextStyle2 { get; set; }

        #endregion

        private bool isLoadingTree;
        public bool IsLoadingTree
        {
            get { return isLoadingTree; }
            set
            {
                isLoadingTree = value;
                RaisePropertyChanged("IsLoadingTree");
            }
        }

        private MapAnnotationCategory selectedstyle;
        public MapAnnotationCategory SelectedStyle
        {
            get { return selectedstyle; }
            set
            {
                if (selectedstyle == value) { return; }
                selectedstyle = value;
                RaisePropertyChanged("SelectedStyle");
            }
        }
        private IList<MapAnnotationCategory> stylenames = new List<MapAnnotationCategory>();
        public IList<MapAnnotationCategory> StyleNames
        {
            get { return stylenames; }
            set
            {
                stylenames = value;
                RaisePropertyChanged("StyleNames");
            }
        }
        public ObservableCollection<YieldLocationListItemViewModel> YieldLocationList { get; set; }
        public WpfMap Map { get; set; }

        private RectangleShape currentMapExtent { get; set; }
        public RectangleShape CurrentMapExtent
        {
            get { return currentMapExtent; }
            set
            {
                if (currentMapExtent == value) { return; }
                currentMapExtent = value;
                RaisePropertyChanged("CurrentMapExtent");
                if (maploaded)
                {
                    ShapeValidationResult svr = currentMapExtent.Validate(ShapeValidationMode.Simple);
                    if (svr.IsValid)
                    {
                        scalingTextStyle.MapScale = Map.CurrentScale;
                        scalingTextStyle2.MapScale = Map.CurrentScale;
                        customMultiStyles.MapScale = Map.CurrentScale;
                        Map.CurrentExtent = currentMapExtent;
                        Map.Refresh();
                    }
                }
            }
        }

        private string displaycoordinates = string.Empty;
        public string DisplayCoordinates
        {
            get { return displaycoordinates; }
            set
            {
                displaycoordinates = value;
                RaisePropertyChanged("DisplayCoordinates");
            }
        }

        public string AnnotationName { get; set; }
        public bool _showLatLon;
        public bool ShowLatLonInName
        {
            get { return _showLatLon; }
            set {
                _showLatLon = value;

            }
        }
        private string LatLon { get; set; }

        static object clientLayerLock = new object();
        private IList<MapItem> clientLayerItems {get;set;}
        public IList<MapItem> ClientLayer
        {
            get { return clientLayerItems; }
            set
            {
                lock (clientLayerLock)
                {
                    clientLayerItems = value;
                    EnsureFieldsLayerIsOpen();
                    FieldsLayer.InternalFeatures.Clear();
                    foreach (var m in clientLayerItems)
                    {
                        if (m.MapData != null && !string.IsNullOrEmpty(m.MapData) && m.MapData != "MULTIPOLYGON EMPTY")
                        {
                            try
                            {
                                IDictionary<string, string> columnvalues = new Dictionary<string, string>();
                                columnvalues.Add(@"LdbLabel", m.Name);
                                var f = new Feature(m.MapData, m.FieldId.ToString(), columnvalues);
                                if (FieldsLayer.InternalFeatures.Contains(f)) { continue; }
                                FieldsLayer.InternalFeatures.Add(m.FieldId.ToString(), f);
                            }
                            catch { }
                        }
                    }
                    RaisePropertyChanged("ClientLayer");
                }
            }
        }

        public ObservableCollection<AnnotationTreeItemViewModel> RootTreeItemModels { get; private set; }
        public CollectionView RootTreeItemModelsView { get; private set; }
        //public IPageFilterViewModel FilterModel { get; protected set; }
        private void InitializeSettings()
        {
            this.mapsettings = this.clientEndpoint.GetMapSettings();
            fieldopacity = mapsettings.FieldsOpacity;
            annotationopacity = mapsettings.MapAnnotationOpacity;
            areaUnitString = mapsettings.MapAreaUnit;
            coordinateformat = mapsettings.DisplayCoordinateFormat;
            agcAreaUnit = UnitFactory.GetUnitByName(areaUnitString) as AgC.UnitConversion.Area.AreaUnit;
            mapAreaUnit = MapUnitFactory.AreaMeasureConversion(agcAreaUnit);
            mapDistanceUnit = MapUnitFactory.AreaDistanceMeasureConversion(agcAreaUnit);
        }
        void InitializeMap()
        {
            Map = new WpfMap();
            currentMapExtent = new RectangleShape();
            InternetAvailable = Landdb.Client.Infrastructure.NetworkStatus.IsInternetAvailableFast();
            Map.MapUnit = GeographyUnit.Meter;
            Map.MapTools.PanZoomBar.Visibility = System.Windows.Visibility.Visible;
            Map.MapTools.PanZoomBar.GlobeButtonVisibility = System.Windows.Visibility.Hidden;
            Map.MapTools.PanZoomBar.VerticalAlignment = System.Windows.VerticalAlignment.Center;
            Map.MapTools.PanZoomBar.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            Map.Background = System.Windows.Media.Brushes.White;
            Map.Cursor = Cursors.Hand;
            zoomlevelfactory = new ZoomLevelFactory();
            Map.ZoomLevelSet = zoomlevelfactory.ZoomLevelSET;
            //HasUnsavedChanges = false;
            Map.MouseLeftButtonDown += (sender, e) =>
            {
                ((MainWindow)System.Windows.Application.Current.MainWindow).Draggable = false;
            };

            projection = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetGoogleMapParametersString());
            //projection.Open();
            //projection = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetGoogleMapParametersString());

            unselectedarea = new AreaStyle(new GeoSolidBrush(GeoColor.FromArgb(annotationopacity, GeoColor.StandardColors.Transparent)));
            unselectedline = new LineStyle(new GeoPen(GeoColor.FromArgb(annotationopacity, GeoColor.StandardColors.Transparent), 2));
            unselectedpoint = PointStyles.CreateSimplePointStyle(PointSymbolType.Circle, new GeoColor(annotationopacity, GeoColor.StandardColors.Transparent), 10);
            customMultiStyles = new MapStyles.AnnotationMultiStyle(unselectedarea, unselectedline, unselectedpoint, annotationopacity);
            customMultiStyles.Opacity = annotationopacity;
            customMultiStyles.MaximumLabelSize = this.mapsettings.ScalingTextMaximumSize;
            customMultiStyles.MinimumLabelSize = this.mapsettings.ScalingTextMinimumSize;
            customMultiStyles.MaximumScale = 19000;
            customMultiStyles.MinimumScale = 1200;
            scalingTextStyle2 = new AnnotationScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, mapsettings.ScalingTextMaximumSize, this.mapsettings.ScalingTextMinimumSize);
            scalingTextStyle2.ColumnsList = new List<string>() { @"Id", @"LdbLabel", @"TypeName", @"ShapeType" };
            //scalingTextStyle2.YOffsetInPixel = 20;
            scalingTextStyle.RotationAngles = new Dictionary<string, double>();
            scalingTextStyle.LabelPositions = new Dictionary<string, PointShape>();
            scalingTextStyle2.RotationAngles = new Dictionary<string, double>();
            scalingTextStyle2.LabelPositions = new Dictionary<string, PointShape>();

            InitializeFieldLayer();

            EnsureFieldsLayerIsOpen();
            if (Map.Overlays.Contains("fieldOverlay"))
            {
                Map.Overlays.Remove("fieldOverlay");
            }
            if (FieldOverlay.Layers.Contains("FieldsLayer"))
            {
                Map.Overlays.Add("fieldOverlay", FieldOverlay);
            }

            Map.Loaded += (sender, e) =>
            {
                if (maploaded) { return; }
                //userstylenames = this.mapsettings.MapAnnotationCategories;
                if (userstylenames.Count > 0)
                {
                    StyleData.Self.UserStyleNames = userstylenames;
                    changedstyles = new List<AnnotationStyleItem>();
                    foreach (KeyValuePair<string, MapAnnotationCategory> asi in userstylenames)
                    {
                        AnnotationStyleItem annotatonstyle = styleFactory.GetStyle(asi.Value.Name, asi.Value.ShapeType);
                        annotatonstyle.Visible = asi.Value.Visible;
                        changedstyles.Add(annotatonstyle);
                    }
                    StyleData.Self.CustomStyles = changedstyles;
                }

                IList<MapAnnotationCategory> stylenamelist = StyleData.Self.StorageStyleNames.Values.ToList();
                var q = from c in stylenamelist
                        orderby c.Name
                        select c;

                StyleNames = q.ToList();
                RaisePropertyChanged("StyleNames");
                if (SelectedStyle == null && stylenames.Count > 0)
                {
                    SelectedStyle = stylenames[0];
                }
                RaisePropertyChanged("SelectedStyle");

                LoadBingMapsOverlay();
                Map.CurrentExtent = CurrentMapExtent;
                //NOT SURE IF I NEED THIS
                //ClientLayer = FieldTreeShapes;
                OpenAndLoadaShapefile();
                if (Map.Overlays.Contains("annotationOverlay"))
                {
                    Map.Overlays.Remove("annotationOverlay");
                }
                Map.Overlays.Add("annotationOverlay", layerOverlay);
                if (Map.Overlays.Contains("popupOverlay"))
                {
                    Map.Overlays.Remove("popupOverlay");
                }
                Map.Overlays.Add("popupOverlay", popupOverlay);
                maploaded = true;
                if (currentDataSourceId != Guid.Empty && currentCropYear > 1980)
                {
                    LoadAnnotations();
                }

                scalingTextStyle.MapScale = Map.CurrentScale;
                scalingTextStyle2.MapScale = Map.CurrentScale;
                customMultiStyles.MapScale = Map.CurrentScale;
                Map.Refresh();
            };

            Map.CurrentScaleChanged += (sender, e) => {
                scalingTextStyle.MapScale = e.CurrentScale;
                scalingTextStyle2.MapScale = e.CurrentScale;
                customMultiStyles.MapScale = Map.CurrentScale;
            };

            Map.MouseMove += (sender, e) => {
                PointShape worldlocation2 = Map.ToWorldCoordinate(e.GetPosition(Map));
                PointShape centerpoint = projection.ConvertToInternalProjection(worldlocation2) as PointShape;
                Vertex geoVertex1 = new Vertex(centerpoint.X, centerpoint.Y);
                string _DecimalDegreeString = Math.Round(geoVertex1.Y, 5) + ", " + Math.Round(geoVertex1.X, 5);
                string displaycoordinates1 = DecimalDegrees.ddstring_To_DMs(_DecimalDegreeString, mapsettings.DisplayCoordinateFormat);
                DisplayCoordinates = displaycoordinates1;
            };

        }

        public void LoadAnnotations()
        {
            if (maploaded)
            {
                //this.mapsettings = clientEndpoint.GetMapSettings();
                fieldopacity = mapsettings.FieldsOpacity;
                annotationopacity = mapsettings.MapAnnotationOpacity;
                areaUnitString = mapsettings.MapAreaUnit;
                coordinateformat = mapsettings.DisplayCoordinateFormat;
                if (coordinateformat == "Decimal Degree")
                {
                    Map.MapTools.MouseCoordinate.MouseCoordinateType = MouseCoordinateType.DegreesMinutesSeconds;
                }
                else
                {
                    Map.MapTools.MouseCoordinate.MouseCoordinateType = MouseCoordinateType.LatitudeLongitude;
                }
                agcAreaUnit = UnitFactory.GetUnitByName(areaUnitString) as AgC.UnitConversion.Area.AreaUnit;
                mapAreaUnit = MapUnitFactory.AreaMeasureConversion(agcAreaUnit);
                mapDistanceUnit = MapUnitFactory.AreaDistanceMeasureConversion(agcAreaUnit);
                scalingTextStyle = new ScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, mapsettings.ScalingTextMaximumSize, this.mapsettings.ScalingTextMinimumSize);

                fieldsselectedfillcolor = mapsettings.FieldsSelectedFillColor;
                fieldsunselectedfillcolor = mapsettings.FieldsUnSelectedFillColor;
                if (!string.IsNullOrEmpty(fieldsselectedfillcolor))
                {
                    fieldsselectedGeoColor = GeoColor.FromHtml(fieldsselectedfillcolor);
                }
                if (!string.IsNullOrEmpty(fieldsunselectedfillcolor))
                {
                    fieldsunselectedGeoColor = GeoColor.FromHtml(fieldsunselectedfillcolor);
                }

                fieldsselectedbordercolor = mapsettings.FieldsSelectedBorderColor;
                fieldsunselectedbordercolor = mapsettings.FieldsUnSelectedBorderColor;
                if (!string.IsNullOrEmpty(fieldsselectedbordercolor))
                {
                    fieldsselectedborderGeoColor = GeoColor.FromHtml(fieldsselectedbordercolor);
                }
                else
                {
                    fieldsselectedborderGeoColor = fieldsselectedGeoColor;
                }
                if (!string.IsNullOrEmpty(fieldsunselectedbordercolor))
                {
                    fieldsunselectedborderGeoColor = GeoColor.FromHtml(fieldsunselectedbordercolor);
                }
                else
                {
                    fieldsunselectedborderGeoColor = fieldsunselectedGeoColor;
                }

                currentLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Clear();
                AreaStyle areastyle = new AreaStyle(new GeoPen(new GeoColor(100, GeoColor.StandardColors.Transparent), 2), new GeoSolidBrush(new GeoColor(60, GeoColor.StandardColors.Transparent)));
                LineStyle linestyle = new LineStyle(new GeoPen(new GeoColor(100, GeoColor.StandardColors.Transparent), 2));
                PointStyle pointstyle = new PointStyle(PointSymbolType.Circle, new GeoSolidBrush(new GeoColor(100, GeoColor.StandardColors.Transparent)), 10);
                customMultiStyles = new MapStyles.AnnotationMultiStyle(areastyle, linestyle, pointstyle, annotationopacity);
                customMultiStyles.ColumnsList = new List<string>() { @"Id", @"LdbLabel", @"TypeName", @"ShapeType" };
                scalingTextStyle2 = new AnnotationScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, mapsettings.ScalingTextMaximumSize, this.mapsettings.ScalingTextMinimumSize);
                scalingTextStyle2.ColumnsList = new List<string>() { @"Id", @"LdbLabel", @"TypeName", @"ShapeType" };
                //scalingTextStyle2.YOffsetInPixel = 20;
                customMultiStyles.Opacity = annotationopacity;
                customMultiStyles.MaximumLabelSize = this.mapsettings.ScalingTextMaximumSize;
                customMultiStyles.MinimumLabelSize = this.mapsettings.ScalingTextMinimumSize;
                customMultiStyles.MaximumScale = 19000;
                customMultiStyles.MinimumScale = 1200;
                currentLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customMultiStyles);
                currentLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(scalingTextStyle2);
                currentLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;

                importLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Clear();
                importLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customMultiStyles);
                importLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(scalingTextStyle2);
                importLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;

                RebuildAnnotations();
                RebuildMap2();
            }
        }
        public void InitializeFieldLayer()
        {
            //fieldsLayer
            FieldOverlay = new LayerOverlay();
            FieldOverlay.DrawingExceptionMode = DrawingExceptionMode.DrawException;
            FieldsLayer = new InMemoryFeatureLayer();
            FieldsLayer.FeatureSource.Open();
            FeatureSourceColumn columnf1 = new FeatureSourceColumn(@"LdbLabel", DbfColumnType.Character.ToString(), 64);
            FieldsLayer.Columns.Add(columnf1);
            FieldsLayer.FeatureSource.Close();
            FieldsLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Clear();
            fieldsLayerAreaStyle = new ClientLayerAreaStyle();
            fieldsLayerAreaStyle.SelectedAreaStyle = new AreaStyle(new GeoPen(new GeoColor(fieldopacity, fieldsselectedborderGeoColor), 2), new GeoSolidBrush(new GeoColor(fieldopacity, fieldsselectedGeoColor)));
            fieldsLayerAreaStyle.UnselectedAreaStyle = new AreaStyle(new GeoPen(new GeoColor(fieldopacity, fieldsunselectedborderGeoColor), 2), new GeoSolidBrush(new GeoColor(fieldopacity, fieldsunselectedGeoColor)));
            //fieldsLayerAreaStyle.SelectedAreaStyle = new AreaStyle(new GeoPen(new GeoColor(fieldopacity, GeoColor.StandardColors.Blue), 2), new GeoSolidBrush(new GeoColor(fieldopacity, GeoColor.StandardColors.Transparent)));
            //fieldsLayerAreaStyle.UnselectedAreaStyle = new AreaStyle(new GeoPen(new GeoColor(fieldopacity, GeoColor.StandardColors.Gray), 2), new GeoSolidBrush(new GeoColor(fieldopacity, GeoColor.StandardColors.LightGray)));
            FieldsLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(fieldsLayerAreaStyle);
            //fieldsLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(scalingTextStyle);
            FieldsLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            BuildCurrentExtentBasedOnFarm();
            FieldsLayer.FeatureSource.Projection = projection;
            fieldsLayerAreaStyle.SelectedFeatures = new List<Feature>();
            fieldsLayerAreaStyle.HiddenFeatures = new List<Feature>();
            FieldOverlay.Layers.Clear();
            FieldOverlay.Layers.Add("FieldsLayer", FieldsLayer);
            measureLayer = new InMemoryFeatureLayer();
            FieldOverlay.Layers.Add("measureLayer", measureLayer);
            popupOverlay = new PopupOverlay();
        }
        private void OpenAndLoadaShapefile()
        {
            CreateInMemoryLayerAndOverlay();
            CreateImportLayerAndOverlay();
            LoadShapeFileOverlayOntoMap();
        }

        private void RebuildAnnotations()
        {
            dispatcher.BeginInvoke(new Action(() => {
                IsLoadingTree = true;
                canLoadAnnotations = false;
                var begin = DateTime.UtcNow;
                AbstractTreeItemViewModel rootNode = null;
                Dictionary<string, MapAnnotationCategory> categories = new Dictionary<string, MapAnnotationCategory>();
                var annotations = clientEndpoint.GetView<MapAnnotationListView>(new DataSourceId(this.currentDataSourceId));
                List<MapAnnotationListItem> anlist = annotations.HasValue && annotations.Value.Annotations != null ? annotations.Value.Annotations : new List<MapAnnotationListItem>();
                if (categories.Count == 0 && anlist.Count > 0)
                {
                    foreach (MapAnnotationListItem aa in anlist)
                    {
                        if (!categories.ContainsKey(aa.AnnotationType))
                        {
                            MapAnnotationCategory mac = styleFactory.GetMapAnnotationCategory(aa.AnnotationType);
                            categories.Add(mac.Name, mac);
                        }
                    }
                }

                RootTreeItemModels = new ObservableCollection<AnnotationTreeItemViewModel>() { new AnnotationTreeItemViewModel(Strings.Annotations_Text, categories, anlist, true) };
                rootNode = RootTreeItemModels.FirstOrDefault();
                if (rootNode != null)
                {
                    RootTreeItemModels[0].IsExpanded = true;
                }
                App.Current.Dispatcher.BeginInvoke(new Action(() => {
                    RootTreeItemModelsView = (CollectionView)CollectionViewSource.GetDefaultView(RootTreeItemModels);
                    //RootTreeItemModelsView.Filter = FilterModel.FilterItem;
                    RaisePropertyChanged("RootTreeItemModelsView");
                }));
                RaisePropertyChanged("RootTreeItemModels");

                var end = DateTime.UtcNow;
                IsLoadingTree = false;
                log.Debug("Tree load took " + (end - begin).TotalMilliseconds + "ms");
            }));
        }
        private void CreateInMemoryLayerAndOverlay()
        {
            layerOverlay = new LayerOverlay();
            layerOverlay.DrawingExceptionMode = DrawingExceptionMode.DrawException;
            currentLayer = new InMemoryFeatureLayer();
            currentLayer.FeatureSource.Open();
            FeatureSourceColumn column1 = new FeatureSourceColumn(@"Id", "string", 64);
            //FeatureSourceColumn column2 = new FeatureSourceColumn(@"Label", "string", 64);
            FeatureSourceColumn column2 = new FeatureSourceColumn(@"LdbLabel", "string", 64);
            FeatureSourceColumn column3 = new FeatureSourceColumn(@"TypeName", "string", 64);
            FeatureSourceColumn column4 = new FeatureSourceColumn(@"ShapeType", "string", 64);
            currentLayer.Columns.Add(column1);
            currentLayer.Columns.Add(column2);
            currentLayer.Columns.Add(column3);
            currentLayer.Columns.Add(column4);
            currentLayer.FeatureSource.Close();
            currentLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = null;
            currentLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = null;
            currentLayer.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = null;
            currentLayer.ZoomLevelSet.ZoomLevel01.DefaultTextStyle = null;
            currentLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Clear();
            AreaStyle areastyle = new AreaStyle(new GeoPen(new GeoColor(100, GeoColor.StandardColors.Transparent), 2), new GeoSolidBrush(new GeoColor(60, GeoColor.StandardColors.Transparent)));
            LineStyle linestyle = new LineStyle(new GeoPen(new GeoColor(100, GeoColor.StandardColors.Transparent), 2));
            PointStyle pointstyle = new PointStyle(PointSymbolType.Circle, new GeoSolidBrush(new GeoColor(100, GeoColor.StandardColors.Transparent)), 10);
            customMultiStyles = new MapStyles.AnnotationMultiStyle(areastyle, linestyle, pointstyle, annotationopacity);
            //scalingTextStyle2 = new ScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, mapsettings.ScalingTextMaximumSize, this.mapsettings.ScalingTextMinimumSize);
            scalingTextStyle2 = new AnnotationScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, mapsettings.ScalingTextMaximumSize, this.mapsettings.ScalingTextMinimumSize);
            scalingTextStyle2.ColumnsList = new List<string>() { @"Id", @"LdbLabel", @"TypeName", @"ShapeType" };
            //scalingTextStyle2.YOffsetInPixel = 20;
            //customMultiStyles.ColumnsList = new List<string>() { @"Id", @"Label", @"TypeName", @"ShapeType" };
            customMultiStyles.Opacity = annotationopacity;
            customMultiStyles.MaximumLabelSize = this.mapsettings.ScalingTextMaximumSize;
            customMultiStyles.MinimumLabelSize = this.mapsettings.ScalingTextMinimumSize;
            customMultiStyles.MaximumScale = 19000;
            customMultiStyles.MinimumScale = 1200;
            customMultiStyles.ColumnsList = new List<string>() { @"Id", @"LdbLabel", @"TypeName", @"ShapeType" };
            currentLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customMultiStyles);
            currentLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(scalingTextStyle2);
            currentLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            currentLayer.FeatureSource.Projection = projection;
            layerOverlay.Layers.Add("AnnotationLayer", currentLayer);
        }

        private void CreateImportLayerAndOverlay()
        {
            importLayer = new InMemoryFeatureLayer();
            importLayer.FeatureSource.Open();
            FeatureSourceColumn column1 = new FeatureSourceColumn(@"Id", "string", 64);
            FeatureSourceColumn column2 = new FeatureSourceColumn(@"LdbLabel", "string", 64);
            FeatureSourceColumn column3 = new FeatureSourceColumn(@"TypeName", "string", 64);
            FeatureSourceColumn column4 = new FeatureSourceColumn(@"ShapeType", "string", 64);
            importLayer.Columns.Add(column1);
            importLayer.Columns.Add(column2);
            importLayer.Columns.Add(column3);
            importLayer.Columns.Add(column4);
            importLayer.FeatureSource.Close();
            importLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = null;
            importLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = null;
            importLayer.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = null;
            importLayer.ZoomLevelSet.ZoomLevel01.DefaultTextStyle = null;
            importLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Clear();
            importLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customMultiStyles);
            importLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(scalingTextStyle2);
            importLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            importLayer.FeatureSource.Projection = projection;
            layerOverlay.Layers.Add("ImportAnnotationLayer", importLayer);
        }
        private void LoadShapeFileOverlayOntoMap()
        {
            if (Map.Overlays.Contains("annotationOverlay"))
            {
                Map.Overlays.Remove("annotationOverlay");
            }
            if (layerOverlay.Layers.Contains("AnnotationLayer"))
            {
                Map.Overlays.Add("annotationOverlay", layerOverlay);
            }

            EnsureCurrentLayerIsOpen();
            EnsureImportLayerIsOpen();
            EnsureFieldsLayerIsOpen();
            EnsureMeasureLayerIsOpen();
            var extent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
            try
            {
                if (currentLayer.InternalFeatures.Count > 0 || FieldsLayer.InternalFeatures.Count > 0)
                {
                    scalingTextStyle.MapScale = Map.CurrentScale;
                    scalingTextStyle2.MapScale = Map.CurrentScale;
                    customMultiStyles.MapScale = Map.CurrentScale;
                    Map.Refresh();
                }

                var shapelayerfeatures = currentLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);
                var fieldlayerfeatures = FieldsLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);
                if (shapelayerfeatures.Count > 0)
                {
                    var shapelayercolumns = currentLayer.FeatureSource.GetColumns();
                    extent = ExtentHelper.GetBoundingBoxOfItems(shapelayerfeatures);
                    Map.CurrentExtent = extent;
                    PointShape center = extent.GetCenterPoint();
                }
                else if (fieldlayerfeatures.Count > 0)
                {
                    var fieldlayercolumns = FieldsLayer.FeatureSource.GetColumns();
                    extent = ExtentHelper.GetBoundingBoxOfItems(fieldlayerfeatures);
                    Map.CurrentExtent = extent;
                    PointShape center = extent.GetCenterPoint();
                }
                else
                {
                    Map.CurrentExtent = CurrentMapExtent;
                    PointShape center = CurrentMapExtent.GetCenterPoint();
                }
            }
            catch
            {
                Map.CurrentExtent = CurrentMapExtent;
                PointShape center = CurrentMapExtent.GetCenterPoint();
            }
        }

        private void EnsureCurrentLayerIsOpen()
        {
            if (currentLayer == null)
            {
                currentLayer = new InMemoryFeatureLayer();
            }

            if (!currentLayer.IsOpen)
            {
                currentLayer.Open();
            }

            if (!projection.IsOpen)
            {
                projection.Open();
            }
        }

        private void EnsureImportLayerIsOpen()
        {
            if (importLayer == null)
            {
                importLayer = new InMemoryFeatureLayer();
            }

            if (!importLayer.IsOpen)
            {
                importLayer.Open();
            }

            if (!projection.IsOpen)
            {
                projection.Open();
            }
        }

        internal void EnsureMeasureLayerIsOpen()
        {
            if (measureLayer == null)
            {
                measureLayer = new InMemoryFeatureLayer();
            }

            if (!measureLayer.IsOpen)
            {
                measureLayer.Open();
            }

            if (!projection.IsOpen)
            {
                projection.Open();
            }
        }

        private void LoadBingMapsOverlay()
        {
            if (!InternetAvailable) { return; }
            IMapOverlay mapoverlay = new BingMapOverlay();
            Overlay bingOverlay = mapoverlay.Overlay;
            if (bingOverlay == null) { return; }
            if (Map.Overlays.Contains("Bing"))
            {
                Map.Overlays.Remove("Bing");
            }
            if (Map.Overlays.Contains("Bing Online"))
            {
                Map.Overlays.Remove("Bing Online");
            }
            if (Map.Overlays.Contains("Roads Only"))
            {
                Map.Overlays.Remove("Roads Only");
            }
            if (Map.Overlays.Contains("Road Online"))
            {
                Map.Overlays.Remove("Road Online");
            }
            if (Map.Overlays.Contains("None"))
            {
                Map.Overlays.Remove("None");
            }
            //bingOverlay.IsVisible = mapoverlay.Visibility;
            //((TileOverlay)bingOverlay).TileType = TileType.SingleTile;
            Map.Overlays.Add(mapoverlay.Name, bingOverlay);
            Map.Overlays.MoveToBottom(mapoverlay.Name);
        }

        internal void EnsureFieldsLayerIsOpen()
        {
            if (FieldsLayer == null)
            {
                FieldsLayer = new InMemoryFeatureLayer();
            }

            if (!FieldsLayer.IsOpen)
            {
                FieldsLayer.Open();
            }

            if (!projection.IsOpen)
            {
                projection.Open();
            }
        }

        void OnDataSourceChanged(DataSourceChangedMessage message)
        {
            this.currentDataSourceId = message.DataSourceId;
            this.currentCropYear = message.CropYear;
            if (maploaded)
            {
                LoadAnnotations();
            }
        }
        void OnRemoteEventReceived(RemoteEventReceived message)
        {
            if (maploaded)
            {
                LoadAnnotations();
            }
        }

        private void RebuildMap2()
        {
            //IsLoadingTree = true;
            var begin = DateTime.UtcNow;
            EnsureCurrentLayerIsOpen();
            currentLayer.InternalFeatures.Clear();
            var annotations = clientEndpoint.GetView<MapAnnotationListView>(new DataSourceId(this.currentDataSourceId));
            List<MapAnnotationListItem> anlist = annotations.HasValue && annotations.Value.Annotations != null ? annotations.Value.Annotations : new List<MapAnnotationListItem>();
            IList<MapAnnotationDetailView> detaillist = new List<MapAnnotationDetailView>();
            IList<AnnotationStyleItem> stylelist = new List<AnnotationStyleItem>();
            foreach (var m in anlist)
            {
                if (m.Name != null && !string.IsNullOrEmpty(m.Name))
                {
                    try
                    {
                        var shapeItemMaybe = clientEndpoint.GetView<MapAnnotationDetailView>(m.Id);
                        MapAnnotationId mapAnnotationId = new MapAnnotationId();
                        string annotationName = string.Empty;
                        string annotationWktData = string.Empty;
                        string annotationShapeType = string.Empty;
                        string annotationType = string.Empty;
                        int? annotationCropYear = null;
                        shapeItemMaybe.IfValue(annotation => {
                            detaillist.Add(annotation);
                            string anid = annotation.Id.Id.ToString();
                            mapAnnotationId = annotation.Id;
                            annotationName = annotation.Name;
                            annotationWktData = annotation.WktData;
                            annotationShapeType = annotation.ShapeType;
                            annotationType = annotation.AnnotationType;
                            annotationCropYear = annotation.CropYear;
                            AnnotationStyleItem annotationstyleitem = null;

                            IDictionary<string, string> columnvalues = new Dictionary<string, string>();
                            columnvalues.Add(@"Id", anid);
                            columnvalues.Add(@"LdbLabel", annotationName);
                            columnvalues.Add(@"TypeName", annotationType);
                            columnvalues.Add(@"ShapeType", annotationShapeType);
                            Feature newfeature = new Feature(annotationWktData, anid, columnvalues);
                            currentLayer.InternalFeatures.Add(anid, newfeature);
                            var f = new Feature(annotationWktData, mapAnnotationId.Id.ToString(), columnvalues);
                            if (currentLayer.InternalFeatures.Contains(anid))
                            {
                                currentLayer.InternalFeatures.Remove(anid);
                            }
                            currentLayer.InternalFeatures.Add(anid, newfeature);
                        });

                    }
                    catch { }
                }
            }
            //if(maploaded && currentLayer.InternalFeatures.Count > 0) {
            //    Map.Refresh();
            //}
            var end = DateTime.UtcNow;
            //IsLoadingTree = false;
            log.Debug("Annotation Shapes load took " + (end - begin).TotalMilliseconds + "ms");
        }

        void DrawShapesMethod()
        {
            Map.TrackOverlay.TrackEnded += new EventHandler<TrackEndedTrackInteractiveOverlayEventArgs>(CutLine_TrackEnded);
            previousCursor = Map.Cursor;
            Map.Cursor = Cursors.Pen;
            if (selectedstyle.ShapeType == "Polygon")
            {
                //Map.TrackOverlay.TrackMode = TrackMode.Polygon;
                if (selectedstyle.StyleName == "GrainBins" || selectedstyle.StyleName == "Silo")
                {
                    Map.TrackOverlay.TrackMode = TrackMode.Point;
                }
                else
                {
                    Map.TrackOverlay.TrackMode = TrackMode.Polygon;
                }
            }
            else if (selectedstyle.ShapeType == "Line")
            {
                if (selectedstyle.StyleName == "Terrace")
                {
                    Map.TrackOverlay.TrackMode = TrackMode.Freehand;
                }
                else
                {
                    Map.TrackOverlay.TrackMode = TrackMode.Line;
                }
            }
            else
            {
                Map.TrackOverlay.TrackMode = TrackMode.Point;
            }
        }
        private void CutLine_TrackEnded(object sender, TrackEndedTrackInteractiveOverlayEventArgs e)
        {
            Map.TrackOverlay.TrackEnded -= new EventHandler<TrackEndedTrackInteractiveOverlayEventArgs>(CutLine_TrackEnded);
            Map.TrackOverlay.TrackMode = TrackMode.None;
            Map.Cursor = previousCursor;
            //HasUnsavedChanges = true;
        }

        public void BuildCurrentExtentBasedOnFarm()
        {
            //should be able to get item map from the farm level
            var flattenedTree = clientEndpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear));
            List<string> farmMap = new List<string>();
            if (flattenedTree.HasValue)
            {
                var uniqueFieldIds = (from t in flattenedTree.Value.Items
                                      where t.FieldId != null
                                      select t.FieldId).Distinct();

                foreach (var fieldId in uniqueFieldIds)
                {
                    var fieldMap = clientEndpoint.GetView<ItemMap>(fieldId);

                    if (fieldMap.HasValue)
                    {
                        var data = fieldMap.Value.MapItems.OrderBy(y => y.CropYearChangeOccurred).First().MapData;
                        if (data != null && data != "MULTIPOLYGON EMPTY" && data != string.Empty)
                        {
                            var feature = new Feature(data);
                            var Fields = new List<Feature>();
                            Fields.Add(feature);
                            FieldsLayer.InternalFeatures.Add(feature);
                        }
                    }
                }

                //foreach (var data in mapData)
                //{
                //    if (data != null && data != "MULTIPOLYGON EMPTY" && data != string.Empty)
                //    {
                //        var feature = new Feature(data);
                //        var Fields = new List<Feature>();
                //        Fields.Add(feature);
                //        FieldsLayer.InternalFeatures.Add(string.Empty, feature);
                //        var simpleCandidate = new SimpleCandidate(data, feature.GetShape() as PolygonShape);
                //        //Candidates.Add(simpleCandidate);
                //        //if (zoneDistanceValue > 0 && !string.IsNullOrEmpty(Severity))
                //        //{
                //        //    MultipolygonShape mps1 = feature.GetShape().Buffer(0, GeographyUnit.DecimalDegree, DistanceUnit.Feet);
                //        //    MultipolygonShape mps2 = feature.GetShape().Buffer((double)zoneDistanceValue, GeographyUnit.DecimalDegree, DistanceUnit.Feet);
                //        //    MultipolygonShape mps3 = mps2.GetDifference(mps1);
                //        //    if (mps3 != null && mps3.Polygons.Count > 0)
                //        //    {
                //        //        var bufferfeature = new Feature(mps3);
                //        //        Buffers.Add(bufferfeature);
                //        //    }
                //        //}
                //    }
                //}

            }
            
            //build map in Think Geo
            

        }
        public void Close()
        {
            Messenger.Default.Send(new HidePopupMessage());
        }

        public void MapLocationAndAssociate()
        {
            if (Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Count > 0 && !string.IsNullOrEmpty(AnnotationName) && SelectedStyle != null && selectedYieldLocationId != null)
            {
                var id = new MapAnnotationId(this.currentDataSourceId, Guid.NewGuid());
                string anid = id.Id.ToString();
                int shapecount = Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Count - 1;
                Feature feat = Map.TrackOverlay.TrackShapeLayer.InternalFeatures.FirstOrDefault();
                if (feat == null) { return; }
                BaseShape baseShape = feat.GetShape();
                if (baseShape == null) { return; }
                BaseShape baseShape3 = projection.ConvertToInternalProjection(baseShape);
                string spatialData = string.Empty;
                string wkt3 = baseShape3.GetWellKnownText();
                if (selectedstyle.StyleName == "GrainBins" || selectedstyle.StyleName == "Silo")
                {
                    if (baseShape3 is PointShape)
                    {
                        PointShape ps1 = baseShape3 as PointShape;
                        var pointx = ps1.X;
                        var pointy = ps1.Y;
                        PointShape centerpointx1 = new PointShape();
                        centerpointx1.X = pointx;
                        centerpointx1.Y = pointy;
                        EllipseShape es = new EllipseShape(centerpointx1, 15, GeographyUnit.DecimalDegree, mapDistanceUnit);
                        PolygonShape grainbinpoly = es.ToPolygon();
                        wkt3 = grainbinpoly.GetWellKnownText();
                    }
                }
                spatialData = wkt3;
                spatialData = spatialData.Replace(@"\n", "");
                spatialData = spatialData.Replace(@"\r", "");
                spatialData = spatialData.Replace(@"\t", "");

                string annlabel = string.Empty;
                PointShape centerpoint = baseShape3.GetCenterPoint();
                Vertex geoVertex1 = new Vertex(centerpoint.X, centerpoint.Y);
                string decimaldegrees = Math.Round(geoVertex1.Y, 5) + ", " + Math.Round(geoVertex1.X, 5);
                string longlat = DecimalDegrees.d_To_DMs(geoVertex1.X, 2, DecimalDegrees.Type.Longitude) + "  " + DecimalDegrees.d_To_DMs(geoVertex1.Y, 2, DecimalDegrees.Type.Latitude);
                string longlatAG = DecimalDegrees.d_To_Dm(geoVertex1.X, 4, DecimalDegrees.Type.Longitude) + "  " + DecimalDegrees.d_To_Dm(geoVertex1.Y, 4, DecimalDegrees.Type.Latitude);
                string coordinatestring = string.Empty;
                if (ShowLatLonInName)
                {
                    coordinatestring = " " + longlat;
                    if (this.mapsettings.DisplayCoordinateFormat == "Decimal Degree")
                    {
                        coordinatestring = " " + decimaldegrees;
                    }
                    if (this.mapsettings.DisplayCoordinateFormat == "Decimal Seconds AG")
                    {
                        coordinatestring = " " + decimaldegrees;
                    }

                    AnnotationName = string.Format("{0} : {1}", AnnotationName, coordinatestring);
                }


                IDictionary<string, string> columnvalues = new Dictionary<string, string>();
                columnvalues.Add(@"Id", anid);
                columnvalues.Add(@"LdbLabel", annlabel);
                columnvalues.Add(@"TypeName", selectedstyle.Name);
                columnvalues.Add(@"ShapeType", selectedstyle.ShapeType);
                Feature newfeature = new Feature(spatialData, anid, columnvalues);
                currentLayer.InternalFeatures.Add(anid, newfeature);
                Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Clear();
                scalingTextStyle.MapScale = Map.CurrentScale;
                scalingTextStyle2.MapScale = Map.CurrentScale;
                customMultiStyles.MapScale = Map.CurrentScale;
                Map.Refresh();

                CreateMapAnnotation createMapAnnotationCommand = new CreateMapAnnotation(id, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), AnnotationName, spatialData, SelectedStyle.Name, SelectedStyle.ShapeType, currentCropYear, null);
                clientEndpoint.SendOne(createMapAnnotationCommand);

                var timer = new Timer(3000);
                timer.Start();

                AddRelatedBufferZone associateYieldLocation = new AddRelatedBufferZone(selectedYieldLocationId, clientEndpoint.GenerateNewMetadata(), id, SelectedStyle.Name);
                clientEndpoint.SendOne(associateYieldLocation);

                Close();
            }
            
        }
    }
}
