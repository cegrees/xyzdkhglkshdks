﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Account;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.Resources;
using Landdb.ViewModel.Secondary.Options;

namespace Landdb.ViewModel {
    public class OptionsPageViewModel : ViewModelBase {
        IClientEndpoint clientEndpoint;
        IOptionsPageViewModel selectedOptionsPage;
        IAccountStatus accountStatus;
        UserSettings userSettings;
        MapSettings mapSettings;

        public OptionsPageViewModel(IClientEndpoint clientEndpoint) {
            this.clientEndpoint = clientEndpoint;
            this.accountStatus = ServiceLocator.Get<IAccountStatus>(); // TODO: Bad factoring here. Redo.

            var settingsId = new UserSettingsId(accountStatus.UserId);
            this.userSettings = clientEndpoint.GetUserSettings();
            this.mapSettings = clientEndpoint.GetMapSettings();

            ReturnCommand = new RelayCommand(OnReturn);

            OptionCategories = new ObservableCollection<IOptionsPageViewModel>();
            OptionCategories.Add(new FieldOptionsViewModel(clientEndpoint, userSettings, settingsId));
            OptionCategories.Add(new MapOptionsViewModel(clientEndpoint, mapSettings, settingsId, ApplicationEnvironment.ShowPremiumMap));
            OptionCategories.Add(new DataOptionsViewModel(clientEndpoint));

            SelectedOptionsPage = OptionCategories.First();
        }

        public RelayCommand ReturnCommand { get; }

        public ObservableCollection<IOptionsPageViewModel> OptionCategories {get; }

        public IOptionsPageViewModel SelectedOptionsPage {
            get { return selectedOptionsPage; }
            set {
                selectedOptionsPage = value;

                if (selectedOptionsPage.Name == Strings.Map_Text.ToLower()) {
                    ((MapOptionsViewModel)selectedOptionsPage).RefreshMapSettings(ApplicationEnvironment.ShowPremiumMap);
                }

                RaisePropertyChanged(() => SelectedOptionsPage);
            }
        }

        void OnReturn() {
            Messenger.Default.Send(new ShowMainViewMessage());

            if (OptionCategories.Count > 1 && OptionCategories[1] is MapOptionsViewModel) {
                if(((MapOptionsViewModel)OptionCategories[1]).RebootRequired.HasValue && ((MapOptionsViewModel)OptionCategories[1]).RebootRequired.Value) {
                    var vm = new Landdb.ViewModel.Overlays.DataSourceMapZoomLevelChangeConfirmationViewModel(clientEndpoint);
                    ScreenDescriptor sd = new ScreenDescriptor("Landdb.Views.Overlays.DataSourceMapZoomLevelChangeConfirmationView", "zoomlevelChangeConfirm", vm);
                    Messenger.Default.Send<ShowOverlayMessage>(new ShowOverlayMessage() { ScreenDescriptor = sd });
                    return;
                }
            }
        }
    }
}
