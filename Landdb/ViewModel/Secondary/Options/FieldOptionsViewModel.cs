﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GalaSoft.MvvmLight;
using Landdb.Client.Infrastructure;
using Landdb.Resources;

namespace Landdb.ViewModel.Secondary.Options {
    public class FieldOptionsViewModel : ViewModelBase, IOptionsPageViewModel {
        IClientEndpoint clientEndpoint;
        UserSettings settings;
        UserSettingsId settingsId;
 
        public FieldOptionsViewModel(IClientEndpoint clientEndpoint, UserSettings settings, UserSettingsId settingsId) {
            this.clientEndpoint = clientEndpoint;
            this.settings = settings;
            this.settingsId = settingsId;

            if (this.settings == null) {
                this.settings = new UserSettings();
            }
        }

        public string Name {
            get { return Strings.Fields_Text.ToLower(); }
        }

        public bool AreFieldsSortedAlphabetically {
            get { return settings.AreFieldsSortedAlphabetically; }
            set {
                if (settings.AreFieldsSortedAlphabetically != value) {
                    settings.AreFieldsSortedAlphabetically = value;
                    clientEndpoint.SaveUserSettings();

                    RaisePropertyChanged("AreFieldsSortedAlphabetically");
                }
            }
        }
    }
}
