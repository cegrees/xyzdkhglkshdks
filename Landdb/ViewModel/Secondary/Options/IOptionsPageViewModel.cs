﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Landdb.ViewModel.Secondary.Options {
    public interface IOptionsPageViewModel {
        string Name { get; }
    }
}
