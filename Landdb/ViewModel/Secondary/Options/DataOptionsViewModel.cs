﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure;
using Landdb.Resources;

namespace Landdb.ViewModel.Secondary.Options {
    public class DataOptionsViewModel : ViewModelBase, IOptionsPageViewModel {
        IClientEndpoint clientEndpoint;

        public DataOptionsViewModel(IClientEndpoint clientEndpoint) {
            this.clientEndpoint = clientEndpoint;

            ClearAllDataCommand = new RelayCommand(ClearAllData);
        }

		public string Name => Strings.Data_Text.ToLower();

		public RelayCommand ClearAllDataCommand { get; }
		// TODO: Implement a command to clear user preference data as well.

		void ClearAllData() {
            DialogFactory.ShowYesNoDialog(Strings.Question_ReallyClearAllData_Text, Strings.Question_ClearAllLocalData_Text, () => {
                clientEndpoint.Stop(new Action(() => {
                    DirectoryInfo dataDirectory = new DirectoryInfo(ApplicationEnvironment.DataDirectory);
                    foreach (var d in dataDirectory.EnumerateDirectories()) {
                        if (d.Name == "landdb-tapes") {
                            foreach (var f in d.EnumerateFiles()) {
                                if (!f.Name.StartsWith("loc")) {
                                    f.Delete();
                                }
                            }
                            continue;
                        }
                        if (d.FullName != clientEndpoint.SettingsDirectory) {
                            d.Delete(true);
                        }
                    }

                    System.Windows.Forms.Application.Restart();
                    System.Windows.Application.Current.Shutdown();
                }));
            }, () => { });
        }
    }
}