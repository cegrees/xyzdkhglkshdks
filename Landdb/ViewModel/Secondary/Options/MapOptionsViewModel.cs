﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DocumentFormat.OpenXml.Office2010.ExcelAc;
using GalaSoft.MvvmLight;
using Landdb.Client.Infrastructure;
using Landdb.Resources;
using Landdb.ViewModel.Maps;
using ThinkGeo.MapSuite.Core;
using Landdb.ViewModel.Maps.ImageryOverlay;

namespace Landdb.ViewModel.Secondary.Options {
    public class MapOptionsViewModel : ViewModelBase, IOptionsPageViewModel {
        IClientEndpoint clientEndpoint;
        UserSettingsId settingsId;
        MapSettings settings;
        bool showPremiumMap;
        System.Windows.Media.Color selectedcolorPickerColor = new System.Windows.Media.Color();
        System.Windows.Media.Color unselectedcolorPickerColor = new System.Windows.Media.Color();
        System.Windows.Media.Color selectedbordercolorPickerColor = new System.Windows.Media.Color();
        System.Windows.Media.Color unselectedbordercolorPickerColor = new System.Windows.Media.Color();
        List<MapInfoSelectorItem> mapInfoSelectorItems = new List<MapInfoSelectorItem>();
        List<MapInfoSelectorItem> cmgMapInfoSelectorItems = new List<MapInfoSelectorItem>();
        List<string> coordinateformats = new List<string>();
        List<string> areaunits = new List<string>();
        List<string> zoomlevels = new List<string>();

        public MapOptionsViewModel(IClientEndpoint clientEndpoint, MapSettings settings, UserSettingsId settingsId, bool showPremiumMap) {
            this.clientEndpoint = clientEndpoint;
            this.settingsId = settingsId;
            this.showPremiumMap = showPremiumMap;
            string geocolor = GeoColor.ToHtml(new GeoColor(255, GeoColor.SimpleColors.Yellow));
            this.settings = settings;
            if (this.settings == null) {
                this.settings = new MapSettings();
            }
            CreateMapInfoSelectorsAndAddDefaults();
            CreateCMGMapInfoSelectorsAndAddDefaults();
            coordinateformats = new List<string>();
            coordinateformats.Add("Decimal Degree");
            coordinateformats.Add("Degrees Minutes Seconds");
            coordinateformats.Add("Decimal Seconds");
            areaunits = new List<string>();
            areaunits.Add("acre");
            areaunits.Add("hectare");
            zoomlevels = new List<string>();
            zoomlevels.Add("Standard");
            zoomlevels.Add("StandardPlus20");
            zoomlevels.Add("StandardPlus40");
            zoomlevels.Add("StandardPlus60");
            RaiseProperties();
        }

        private void CreateMapInfoSelectorsAndAddDefaults() {
            mapInfoSelectorItems = new List<MapInfoSelectorItem> {
                new MapInfoSelectorItem(MapStatusType.Bing),
                new MapInfoSelectorItem(MapStatusType.RoadsOnly),
                new MapInfoSelectorItem(MapStatusType.None)
            };
            if (showPremiumMap) {
                mapInfoSelectorItems.Add(new MapInfoSelectorItem(MapStatusType.Google));
            }
        }
        private void CreateCMGMapInfoSelectorsAndAddDefaults() {
            cmgMapInfoSelectorItems = new List<MapInfoSelectorItem>{
                new MapInfoSelectorItem(MapStatusType.Bing),
                new MapInfoSelectorItem(MapStatusType.RoadsOnly),
                new MapInfoSelectorItem(MapStatusType.None),
            };
            if (showPremiumMap)
            {
                cmgMapInfoSelectorItems.Add(new MapInfoSelectorItem(MapStatusType.Google));
            }
        }

        private void RaiseProperties() {
            RaisePropertyChanged("BingMapVisible");
            RaisePropertyChanged("MapInfoSelectorItem");
            RaisePropertyChanged("MapInfoSelectorItems");
            RaisePropertyChanged("CMGMapInfoSelectorItem");
            RaisePropertyChanged("CMGMapInfoSelectorItems");
            RaisePropertyChanged("DisplayCoordinateFormat");
            RaisePropertyChanged("CoordinateFormats");
            RaisePropertyChanged("MapAreaUnit");
            RaisePropertyChanged("AreaUnits");
            RaisePropertyChanged("ZoomLevel");
            RaisePropertyChanged("ZoomLevels");
            RaisePropertyChanged("AreaDecimals");
            RaisePropertyChanged("DisplayAreaInFieldLabel");
            RaisePropertyChanged("DisplayAreaOnlyLabel");
            RaisePropertyChanged("CMGFieldOrCropzones");
            RaisePropertyChanged("BorderSize");
            RaisePropertyChanged("DisplayFillColor");
            RaisePropertyChanged("AllowLabelOverlapping");
            RaisePropertyChanged("FittingPolygon");
            RaisePropertyChanged("FittingPolygonFactor");
            RaisePropertyChanged("DisplayCMGLegend");
            RaisePropertyChanged("FieldsOpacity");
            RaisePropertyChanged("WpfFillColorFields");
            RaisePropertyChanged("FieldsVisible");
            RaisePropertyChanged("UnselectedOpacity");
            RaisePropertyChanged("WpfFillColorUnselected");
            RaisePropertyChanged("UnselectedVisible");
            RaisePropertyChanged("MapAnnotationOpacity");
            RaisePropertyChanged("WpfFillColorMapAnnotation");
            RaisePropertyChanged("MapAnnotationsVisible");
            RaisePropertyChanged("CMGMapAnnotationsVisible");
            RaisePropertyChanged("MapAnnotationLabelsVisible");
            RaisePropertyChanged("ScalingTextVisible");
            RaisePropertyChanged("ScalingTextMinimumSize");
            RaisePropertyChanged("ScalingTextMaximumSize");
            RaisePropertyChanged("FieldsSelectedFillColor");
            RaisePropertyChanged("SelectedColorPickerColor");
            RaisePropertyChanged("FieldsUnSelectedFillColor");
            RaisePropertyChanged("UnSelectedColorPickerColor");
            RaisePropertyChanged("FieldsSelectedBorderColor");
            RaisePropertyChanged("SelectedBorderColorPickerColor");
            RaisePropertyChanged("FieldsUnSelectedBorderColor");
            RaisePropertyChanged("UnSelectedBorderColorPickerColor");
        }

        public void RefreshMapSettings(bool showPremiumMap) {
            this.settings = clientEndpoint.GetMapSettings();
            this.showPremiumMap = showPremiumMap;

            CreateMapInfoSelectorsAndAddDefaults();
            CreateCMGMapInfoSelectorsAndAddDefaults();
            RaiseProperties();
        }

        public string Name {
            get { return Strings.Map_Text.ToLower(); }
        }

        public bool BingMapVisible {
            get { return settings.BingMapVisible; }
            set {
                if (settings.BingMapVisible != value) {
                    settings.BingMapVisible = value;
                    clientEndpoint.SaveMapSettings(settings);
                    RaisePropertyChanged("BingMapVisible");
                }
            }
        }

        public List<MapInfoSelectorItem> MapInfoSelectorItems => mapInfoSelectorItems;

        public MapInfoSelectorItem MapInfoSelectorItem {
            get {
                if (MapInfoSelectorItems?.Any() ?? false) {
                    return MapInfoSelectorItems.FirstOrDefault(x => x.Equals(settings.MapInfoSelectorItem));
                } else {
                    return null;
                }
            }
            set {
                if (value == null) { return; }
                if (settings.MapInfoSelectorItem != value) {
                    if (!settings.MapInfoSelectorItem.IsGoogle() && value.IsGoogle()) {
                        rebootRequired = true;
                    } else if (settings.MapInfoSelectorItem.IsGoogle() && !value.IsGoogle()) {
                        rebootRequired = true;
                    }
                    settings.MapInfoSelectorItem = value;
                    if (rebootRequired.HasValue && rebootRequired.Value) {
                        settings.CMGMapInfoSelectorItem = CMGMapInfoSelectorItems.FirstOrDefault(x => x.DisplayText == settings.MapInfoSelectorItem.DisplayText);
                    }
                    clientEndpoint.SaveMapSettings(settings);
                    RaisePropertyChanged("MapInfoSelectorItem");
                    RaisePropertyChanged("MapInfoSelectorItems");
                }
            }
        }

        public List<MapInfoSelectorItem> CMGMapInfoSelectorItems => cmgMapInfoSelectorItems;

        public MapInfoSelectorItem CMGMapInfoSelectorItem {
            get {
                if (CMGMapInfoSelectorItems?.Any() ?? false) {
                    return CMGMapInfoSelectorItems.FirstOrDefault(x => x.Equals(settings.CMGMapInfoSelectorItem));
                }
                else {
                    return null;
                }
            }
            set {
                if (value == null) { return; }
                if (settings.CMGMapInfoSelectorItem != value) {
                    if (!settings.CMGMapInfoSelectorItem.IsGoogle() && value.IsGoogle()) {
                        rebootRequired = true;
                    }
                    else if (settings.CMGMapInfoSelectorItem.IsGoogle() && !value.IsGoogle()) {
                        rebootRequired = true;
                    }
                    settings.CMGMapInfoSelectorItem = value;
                    if (rebootRequired.HasValue && rebootRequired.Value) {
                        settings.MapInfoSelectorItem = MapInfoSelectorItems.FirstOrDefault(x => x.DisplayText == settings.CMGMapInfoSelectorItem.DisplayText);
                    }
                    clientEndpoint.SaveMapSettings(settings);
                    RaisePropertyChanged("CMGMapInfoSelectorItem");
                    RaisePropertyChanged("CMGMapInfoSelectorItems");
                }
            }
        }

        public List<string> CoordinateFormats {
            get {
                var objectToReturn = coordinateformats.Select(x => MapCoordinateFormatDisplayItem.GetMapCoordinateDisplayTextFor(x)).ToList();
                return objectToReturn;
            }
        }

        public string DisplayCoordinateFormat {
            get {
                var coordinateFormat = coordinateformats.FirstOrDefault(x => x == settings.DisplayCoordinateFormat);
                var objectToReturn = MapCoordinateFormatDisplayItem.GetMapCoordinateDisplayTextFor(coordinateFormat);
                return objectToReturn;
            }
            set {
                var keyText = MapCoordinateFormatDisplayItem.GetMapCoordinateKeyTextFor(value);
                if (settings.DisplayCoordinateFormat != keyText) {
                    settings.DisplayCoordinateFormat = keyText;
                    clientEndpoint.SaveMapSettings(settings);
                    RaisePropertyChanged("DisplayCoordinateFormat");
                    RaisePropertyChanged("CoordinateFormats");
                }
            }
        }

        public List<string> AreaUnits {
            get { return areaunits; }
        }

        public string MapAreaUnit {
            get {
                return settings.MapAreaUnit;
            }
            set {
                if (settings.MapAreaUnit != value) {
                    settings.MapAreaUnit = value;
                    clientEndpoint.SaveMapSettings(settings);
                    RaisePropertyChanged("MapAreaUnit");
                    RaisePropertyChanged("AreaUnits");
                }
            }
        }

        public List<string> ZoomLevels {
            get { return zoomlevels; }
        }

        public string ZoomLevel {
            get {
                return settings.ZoomLevel;
            }
            set {
                if (settings.ZoomLevel != value) {
                    settings.ZoomLevel = value;
                    clientEndpoint.SaveMapSettings(settings);
                    RaisePropertyChanged("ZoomLevel");
                    RaisePropertyChanged("ZoomLevels");
                }
            }
        }

        public int AreaDecimals {
            get { return settings.AreaDecimals; }
            set {
                if (settings.AreaDecimals != value) {
                    settings.AreaDecimals = value;
                    clientEndpoint.SaveMapSettings(settings);
                    RaisePropertyChanged("AreaDecimals");
                }
            }
        }

        public bool? DisplayAreaInFieldLabel {
            get { return settings.DisplayAreaInFieldLabel; }
            set {
                if (settings.DisplayAreaInFieldLabel != value) {
                    settings.DisplayAreaInFieldLabel = value;
                    clientEndpoint.SaveMapSettings(settings);
                    RaisePropertyChanged("DisplayAreaInFieldLabel");
                }
            }
        }

        public bool? DisplayAreaOnlyLabel {
            get { return settings.DisplayAreaOnlyLabel; }
            set {
                if (settings.DisplayAreaOnlyLabel != value) {
                    settings.DisplayAreaOnlyLabel = value;
                    clientEndpoint.SaveMapSettings(settings);
                    RaisePropertyChanged("DisplayAreaOnlyLabel");
                }
            }
        }
        public bool? CMGFieldOrCropzones {
            get { return settings.CMGFieldOrCropzones; }
            set {
                if (settings.CMGFieldOrCropzones != value) {
                    settings.CMGFieldOrCropzones = value;
                    clientEndpoint.SaveMapSettings(settings);
                    RaisePropertyChanged("CMGFieldOrCropzones");
                }
            }
        }

        public int FieldsOpacity {
            get { return settings.FieldsOpacity; }
            set {
                if (settings.FieldsOpacity != value) {
                    settings.FieldsOpacity = value;
                    clientEndpoint.SaveMapSettings(settings);
                    RaisePropertyChanged("FieldsOpacity");
                    RaisePropertyChanged("WpfFillColorFields");
                }
            }
        }

        public bool FieldsVisible {
            get { return settings.FieldsVisible; }
            set {
                if (settings.FieldsVisible != value) {
                    settings.FieldsVisible = value;
                    clientEndpoint.SaveMapSettings(settings);
                    RaisePropertyChanged("FieldsVisible");
                }
            }
        }

        public int UnselectedOpacity {
            get { return settings.UnselectedOpacity; }
            set {
                if (settings.UnselectedOpacity != value) {
                    settings.UnselectedOpacity = value;
                    clientEndpoint.SaveMapSettings(settings);
                    RaisePropertyChanged("UnselectedOpacity");
                    RaisePropertyChanged("WpfFillColorUnselected");
                }
            }
        }

        public bool UnselectedVisible {
            get { return settings.UnselectedVisible; }
            set {
                if (settings.UnselectedVisible != value) {
                    settings.UnselectedVisible = value;
                    clientEndpoint.SaveMapSettings(settings);
                    RaisePropertyChanged("UnselectedVisible");
                }
            }
        }

        public int MapAnnotationOpacity {
            get { return settings.MapAnnotationOpacity; }
            set {
                if (settings.MapAnnotationOpacity != value) {
                    settings.MapAnnotationOpacity = value;
                    clientEndpoint.SaveMapSettings(settings);
                    RaisePropertyChanged("MapAnnotationOpacity");
                    RaisePropertyChanged("WpfFillColorMapAnnotation");
                }
            }
        }

        public bool MapAnnotationsVisible {
            get { return settings.MapAnnotationsVisible; }
            set {
                if (settings.MapAnnotationsVisible != value) {
                    settings.MapAnnotationsVisible = value;
                    clientEndpoint.SaveMapSettings(settings);
                    RaisePropertyChanged("MapAnnotationsVisible");
                }
            }
        }

        public bool? CMGMapAnnotationsVisible {
            get { return settings.CMGMapAnnotationsVisible; }
            set {
                if (settings.CMGMapAnnotationsVisible != value) {
                    settings.CMGMapAnnotationsVisible = value;
                    clientEndpoint.SaveMapSettings(settings);
                    RaisePropertyChanged("CMGMapAnnotationsVisible");
                }
            }
        }

        public bool? MapAnnotationLabelsVisible {
            get { return settings.MapAnnotationLabelsVisible; }
            set {
                if (settings.MapAnnotationLabelsVisible != value) {
                    settings.MapAnnotationLabelsVisible = value;
                    clientEndpoint.SaveMapSettings(settings);
                    RaisePropertyChanged("MapAnnotationLabelsVisible");
                }
            }
        }

        public bool ScalingTextVisible {
            get { return settings.ScalingTextVisible; }
            set {
                if (settings.ScalingTextVisible != value) {
                    settings.ScalingTextVisible = value;
                    clientEndpoint.SaveMapSettings(settings);
                    RaisePropertyChanged("ScalingTextVisible");
                }
            }
        }

        public int ScalingTextMinimumSize {
            get { return settings.ScalingTextMinimumSize; }
            set {
                if (settings.ScalingTextMinimumSize != value) {
                    settings.ScalingTextMinimumSize = value;
                    clientEndpoint.SaveMapSettings(settings);
                    RaisePropertyChanged("ScalingTextMinimumSize");
                }
            }
        }

        public int ScalingTextMaximumSize {
            get { return settings.ScalingTextMaximumSize; }
            set {
                if (settings.ScalingTextMaximumSize != value) {
                    settings.ScalingTextMaximumSize = value;
                    clientEndpoint.SaveMapSettings(settings);
                    RaisePropertyChanged("ScalingTextMaximumSize");
                }
            }
        }

        public int BorderSize {
            get { return settings.BorderSize; }
            set {
                if (settings.BorderSize != value) {
                    settings.BorderSize = value;
                    clientEndpoint.SaveMapSettings(settings);
                    RaisePropertyChanged("BorderSize");
                }
            }
        }

        public bool? DisplayFillColor {
            get { return settings.DisplayFillColor; }
            set {
                if (settings.DisplayFillColor != value) {
                    settings.DisplayFillColor = value;
                    clientEndpoint.SaveMapSettings(settings);
                    RaisePropertyChanged("DisplayFillColor");
                }
            }
        }

        public bool? DisplayCMGLegend {
            get { return settings.DisplayCMGLegend; }
            set {
                if (settings.DisplayCMGLegend != value) {
                    settings.DisplayCMGLegend = value;
                    clientEndpoint.SaveMapSettings(settings);
                    RaisePropertyChanged("DisplayCMGLegend");
                }
            }
        }

        public bool? AllowLabelOverlapping {
            get { return settings.AllowLabelOverlapping; }
            set {
                if (settings.AllowLabelOverlapping != value) {
                    settings.AllowLabelOverlapping = value;
                    clientEndpoint.SaveMapSettings(settings);
                    RaisePropertyChanged("AllowLabelOverlapping");
                }
            }
        }

        public bool? FittingPolygon {
            get { return settings.FittingPolygon; }
            set {
                if (settings.FittingPolygon != value) {
                    settings.FittingPolygon = value;
                    clientEndpoint.SaveMapSettings(settings);
                    RaisePropertyChanged("FiggingPolygon");
                }
            }
        }

        public int FittingPolygonFactor {
            get { return settings.FittingPolygonFactor; }
            set {
                if (settings.FittingPolygonFactor != value) {
                    settings.FittingPolygonFactor = value;
                    clientEndpoint.SaveMapSettings(settings);
                    RaisePropertyChanged("FittingPolygonFactor");
                }
            }
        }

        public bool? AllowOfflineMap {
            get { return settings.AllowOfflineMap; }
            set {
                if (settings.AllowOfflineMap != value) {
                    settings.AllowOfflineMap = value;
                    clientEndpoint.SaveMapSettings(settings);
                    RaisePropertyChanged("AllowOfflineMap");
                }
            }
        }

        public bool? DisplayMapZoomBar
        {
            get { return settings.DisplayMapZoomBar; }
            set
            {
                if (settings.DisplayMapZoomBar != value) {
                    settings.DisplayMapZoomBar = value;
                    clientEndpoint.SaveMapSettings(settings);
                    RaisePropertyChanged("DisplayMapZoomBar");
                }
            }
        }

        private bool? rebootRequired;
        public bool? RebootRequired {
            get { return rebootRequired; }
            set { rebootRequired = value; }
        }

        public GeoColor FieldsSelectedFillColor {
            get {
                string color = settings.FieldsSelectedFillColor;
                if (string.IsNullOrEmpty(color)) {
                    color = GeoColor.ToHtml(new GeoColor(255, GeoColor.SimpleColors.Yellow));
                }

                return GeoColor.FromHtml(color);
            }
            set {
                if (settings.FieldsSelectedFillColor != GeoColor.ToHtml(value)) {
                    settings.FieldsSelectedFillColor = GeoColor.ToHtml(value);
                    clientEndpoint.SaveMapSettings(settings);
                    RaisePropertyChanged("FieldsSelectedFillColor");
                }
            }
        }

        public System.Windows.Media.Color SelectedColorPickerColor {
            get {
                selectedcolorPickerColor = System.Windows.Media.Color.FromArgb(255, FieldsSelectedFillColor.RedComponent, FieldsSelectedFillColor.GreenComponent, FieldsSelectedFillColor.BlueComponent);
                return selectedcolorPickerColor; 
            }
            set {
                if (selectedcolorPickerColor == value) { return; }
                selectedcolorPickerColor = value;
                FieldsSelectedFillColor = GeoColor.FromArgb(255, selectedcolorPickerColor.R, selectedcolorPickerColor.G, selectedcolorPickerColor.B);
                RaisePropertyChanged("FieldsSelectedFillColor");
                RaisePropertyChanged("SelectedColorPickerColor");
            }
        }

        public GeoColor FieldsSelectedBorderColor {
            get {
                string color = settings.FieldsSelectedBorderColor;
                if (string.IsNullOrEmpty(color)) {
                    color = settings.FieldsSelectedFillColor;
                }

                return GeoColor.FromHtml(color);
            }
            set {
                if (settings.FieldsSelectedBorderColor != GeoColor.ToHtml(value)) {
                    settings.FieldsSelectedBorderColor = GeoColor.ToHtml(value);
                    clientEndpoint.SaveMapSettings(settings);
                    RaisePropertyChanged("FieldsSelectedBorderColor");
                }
            }
        }

        public System.Windows.Media.Color SelectedBorderColorPickerColor {
            get {
                selectedbordercolorPickerColor = System.Windows.Media.Color.FromArgb(255, FieldsSelectedBorderColor.RedComponent, FieldsSelectedBorderColor.GreenComponent, FieldsSelectedBorderColor.BlueComponent);
                return selectedbordercolorPickerColor;
            }
            set {
                if (selectedbordercolorPickerColor == value) { return; }
                selectedbordercolorPickerColor = value;
                FieldsSelectedBorderColor = GeoColor.FromArgb(255, selectedbordercolorPickerColor.R, selectedbordercolorPickerColor.G, selectedbordercolorPickerColor.B);
                RaisePropertyChanged("FieldsSelectedBorderColor");
                RaisePropertyChanged("SelectedBorderColorPickerColor");
            }
        }

        public GeoColor FieldsUnSelectedFillColor {
            get {
                string color = settings.FieldsUnSelectedFillColor;
                if (string.IsNullOrEmpty(color)) {
                    color = GeoColor.ToHtml(new GeoColor(255, GeoColor.StandardColors.LightGray));
                }
                return GeoColor.FromHtml(color);
            }
            set {
                if (settings.FieldsUnSelectedFillColor != GeoColor.ToHtml(value)) {
                    settings.FieldsUnSelectedFillColor = GeoColor.ToHtml(value);
                    clientEndpoint.SaveMapSettings(settings);
                    RaisePropertyChanged("FieldsUnSelectedFillColor");
                }
            }
        }

        public System.Windows.Media.Color UnSelectedColorPickerColor {
            get {
                unselectedcolorPickerColor = System.Windows.Media.Color.FromArgb(255, FieldsUnSelectedFillColor.RedComponent, FieldsUnSelectedFillColor.GreenComponent, FieldsUnSelectedFillColor.BlueComponent);
                return unselectedcolorPickerColor;
            }
            set {
                if (unselectedcolorPickerColor == value) { return; }
                unselectedcolorPickerColor = value;
                FieldsUnSelectedFillColor = GeoColor.FromArgb(255, unselectedcolorPickerColor.R, unselectedcolorPickerColor.G, unselectedcolorPickerColor.B);
                RaisePropertyChanged("FieldsUnSelectedFillColor");
                RaisePropertyChanged("UnSelectedColorPickerColor");
            }
        }

        public GeoColor FieldsUnSelectedBorderColor {
            get {
                string color = settings.FieldsUnSelectedBorderColor;
                if (string.IsNullOrEmpty(color)) {
                    color = settings.FieldsUnSelectedFillColor;
                }
                return GeoColor.FromHtml(color);
            }
            set {
                if (settings.FieldsUnSelectedBorderColor != GeoColor.ToHtml(value)) {
                    settings.FieldsUnSelectedBorderColor = GeoColor.ToHtml(value);
                    clientEndpoint.SaveMapSettings(settings);
                    RaisePropertyChanged("FieldsUnSelectedBorderColor");
                }
            }
        }

        public System.Windows.Media.Color UnSelectedBorderColorPickerColor {
            get {
                unselectedbordercolorPickerColor = System.Windows.Media.Color.FromArgb(255, FieldsUnSelectedBorderColor.RedComponent, FieldsUnSelectedBorderColor.GreenComponent, FieldsUnSelectedBorderColor.BlueComponent);
                return unselectedbordercolorPickerColor;
            }
            set {
                if (unselectedbordercolorPickerColor == value) { return; }
                unselectedbordercolorPickerColor = value;
                FieldsUnSelectedBorderColor = GeoColor.FromArgb(255, unselectedbordercolorPickerColor.R, unselectedbordercolorPickerColor.G, unselectedbordercolorPickerColor.B);
                RaisePropertyChanged("FieldsUnSelectedBorderColor");
                RaisePropertyChanged("UnSelectedBorderColorPickerColor");
            }
        }

        public System.Windows.Media.SolidColorBrush WpfFillColorFields {
            get {
                return new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromScRgb((float)FieldsOpacity, System.Drawing.Color.Red.R, System.Drawing.Color.Red.G, System.Drawing.Color.Red.B));
            }
        }

        public System.Windows.Media.SolidColorBrush WpfFillColorUnselected {
            get {
                return new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromScRgb((float)UnselectedOpacity, System.Drawing.Color.Red.R, System.Drawing.Color.Red.G, System.Drawing.Color.Red.B));
            }
        }

        public System.Windows.Media.SolidColorBrush WpfFillColorMapAnnotation {
            get {
                return new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromScRgb((float)MapAnnotationOpacity, System.Drawing.Color.Red.R, System.Drawing.Color.Red.G, System.Drawing.Color.Red.B));
            }
        }

        public IMapOverlay MapFactory(MapInfoSelectorItem mapInfoSelectorItem, bool? allowofflinemap) {
            if (mapInfoSelectorItem.IsBing()) {
                return new BingMapOverlay(allowofflinemap);
            }
            if (mapInfoSelectorItem.IsRoadsOnly()) {
                return new BingRoadMapsOverlay(allowofflinemap);
            }
            if (mapInfoSelectorItem.IsGoogle()) {
                return new GoogleMapCustomOverlay();
            }
            if (mapInfoSelectorItem.IsGoogleOffline()) {
                return new GoogleMapOverlay(allowofflinemap);
            }
            return new NoneMapOverlay(allowofflinemap);
        }

        public IMapOverlay MapToggle(MapInfoSelectorItem mapInfoSelectorItem, bool? allowofflinemap) {
            if (mapInfoSelectorItem.IsBing()) {
                return new BingRoadMapsOverlay(allowofflinemap);
            }
            else if (mapInfoSelectorItem.IsRoadsOnly()) {
                return new NoneMapOverlay(allowofflinemap);
            }
            return new BingMapOverlay(allowofflinemap);
        }

    }
}
