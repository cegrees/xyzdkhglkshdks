﻿using AgC.UnitConversion;
using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Domain.ReadModels.Invoice;
using Landdb.Domain.ReadModels.Plan;
using Landdb.Domain.ReadModels.Product;
using Landdb.Domain.ReadModels.Recommendation;
using Landdb.Domain.ReadModels.Shared;
using Landdb.Domain.ReadModels.WorkOrder;
using Landdb.Infrastructure;
using Landdb.ViewModel.Production;
using Landdb.ViewModel.Shared;
using NLog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Secondary.Application {
	public class ProductSelectionViewModel : ValidationViewModelBase {

		readonly IClientEndpoint clientEndpoint;
		readonly Dispatcher dispatcher;

		readonly CropYearId currentCropYearId;

		readonly Logger log = LogManager.GetCurrentClassLogger();

		FieldSelectionViewModel fieldSelection;
		InventoryListView inventory;
		AppliedProductViewModel selectedAppliedProduct;
		decimal totalCost;
		bool showTankInfoPopup;
		bool isTankApplication;

		Task productLoadTask;

		ApplicationTypes selectedApplicationType;
		ApplicationTypes[] applicationTypesList = new ApplicationTypes[] { ApplicationTypes.RatePerArea, ApplicationTypes.TotalProduct, ApplicationTypes.RatePerTank };

		ApplicationMethod appMethod;

		WorkOrderView associatedWorkOrderView;
		InvoiceView associatedInvoiceView;
		RecommendationView associatedRecommendationView;
		private PlanView associatedPlanView;

		public ProductSelectionViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, CropYearId currentCropYearId, FieldSelectionViewModel fieldSelectionVm, bool isShareInfoVisible) {
			this.fieldSelection = fieldSelectionVm;
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			this.currentCropYearId = currentCropYearId;

			this.isShareInfoVisible = isShareInfoVisible;

			Task.Run(() => {
				PopulatePestList();
				BuildProductList();
			});

			TankInfo = new TankInformationModel(clientEndpoint, fieldSelection, OnTankInfoChanged);

			inventory = clientEndpoint.GetView<InventoryListView>(currentCropYearId).GetValue(() => new InventoryListView());

			AppliedProducts = new ObservableCollection<AppliedProductViewModel>();
			BindingOperations.EnableCollectionSynchronization(AppliedProducts, _appliedProductLock);

			ToggleTankInfoCommand = new RelayCommand(() => { ShowTankInfoPopup = !ShowTankInfoPopup; });

			AddAppliedProductCommand = new RelayCommand(AddProduct);
			DeleteAppliedProductCommand = new RelayCommand(DeleteProduct);
			AddAppliedProductCommand.Execute(null);

			AppliedProducts.CollectionChanged += (sender, args) => { ValidateAndRaisePropertyChanged(() => AppliedProducts); };
		}

		private readonly bool isShareInfoVisible;
		public bool IsShareInfoVisible { get { return isShareInfoVisible; } }

		private readonly object _appliedProductLock = new object();

		public InvoiceView AssociatedInvoiceView {
			get { return associatedInvoiceView; }
			set {
				associatedInvoiceView = value;

				if (associatedInvoiceView != null) {

					SelectedApplicationType = ApplicationTypes.TotalProduct;

					Task loadProductsTask = new Task(() => {
						lock (AppliedProducts) {
							AppliedProducts.Clear();

							if (associatedInvoiceView != null) {
								foreach (var p in associatedInvoiceView.Products) {
									var selectedProduct = (from pr in Products where pr.ProductId == p.ProductId select pr).FirstOrDefault();

									if (selectedProduct == null) { continue; } // TODO: Log that product couldn't be found.

									// TODO: This handles by-rate apps only right now.
									var newAppliedProduct = new AppliedProductViewModel(clientEndpoint, this, fieldSelection, inventory) {
										ApplyBy = SelectedApplicationType,
										SelectedProduct = selectedProduct,
										TotalProductValue = p.TotalProductValue,
										TotalProductUnit = clientEndpoint.GetProductUnitResolver().GetPackageSafeUnit(p.ProductId, p.TotalProductUnit),
									};

									AppliedProducts.Add(newAppliedProduct);
								}
							}

							if (AppliedProducts.Any()) {
								SelectedAppliedProduct = AppliedProducts.Last();
							}

							dispatcher.BeginInvoke(new Action(() => {
								RaisePropertyChanged(() => ProductCount);
							}));
						}
					});

					DelayRunProductPopulationTask(loadProductsTask);
				}

				RaisePropertyChanged(() => AssociatedInvoiceView);
			}
		}

        private ApplicationView associatedApplicationView { get; set; }
        public ApplicationView AssociatedApplicationView
        {
            get { return associatedApplicationView; }
            set
            {
                associatedApplicationView = value;

                if (associatedApplicationView != null)
                {
                    Task loadProductsTask = new Task(() =>
                    {
                        lock (AppliedProducts)
                        {
                            AppliedProducts.Clear();

                            switch (associatedApplicationView.ProductStrategy)
                            {
                                case ProductApplicationStrategy.ByRatePerArea:
                                    SelectedApplicationType = ApplicationTypes.RatePerArea;
                                    break;
                                case ProductApplicationStrategy.ByRatePerTank:
                                    SelectedApplicationType = ApplicationTypes.RatePerTank;
                                    break;
                                case ProductApplicationStrategy.ByTotalProduct:
                                    SelectedApplicationType = ApplicationTypes.TotalProduct;
                                    break;
                            }
                            RaisePropertyChanged(() => SelectedApplicationType);

                            if (associatedApplicationView.TankInformation != null)
                            {
                                TankInfo.SetTankInformation(associatedApplicationView.TankInformation);
                            }

                            if (associatedApplicationView != null)
                            {
                                foreach (var p in associatedApplicationView.Products)
                                {
                                    var selectedProduct = (from pr in Products where pr.ProductId == p.Id select pr).FirstOrDefault();
                                    var appMethod = clientEndpoint.GetMasterlistService().GetApplicationMethodList().SingleOrDefault(x => x.Name == p.ApplicationMethod);
                                    var selectedPest = p.TargetPest != null ? clientEndpoint.GetMasterlistService().GetPestList().Where(x => x.Name == p.TargetPest.CommonName && x.LatinName == p.TargetPest.LatinName) : null;
                                    if (selectedProduct == null) { continue; } // TODO: Log that product couldn't be found.
                                    switch (associatedApplicationView.ProductStrategy)
                                    {
                                        case ProductApplicationStrategy.ByRatePerArea:
                                            var newAppliedProduct = new AppliedProductViewModel(clientEndpoint, this, fieldSelection, inventory)
                                            {
                                                ApplyBy = SelectedApplicationType,
                                                SelectedProduct = selectedProduct,
                                                RatePerAreaValue = p.RateValue,
                                                ProductAreaPercent = p.PercentApplied,
                                                ApplicationMethod = appMethod,
                                                SelectedPest = (p.TargetPest != null) && PestList != null ? PestList.FirstOrDefault(x => x.CommonName == p.TargetPest.CommonName && x.LatinName == p.TargetPest.LatinName) : null,
                                            };

                                            newAppliedProduct.AssociatedProducts = new ObservableCollection<AssociatedProductItem>(GetAssociateProducts(p, newAppliedProduct.RemoveAssociatedProduct));
                                            newAppliedProduct.HasAssociatedProducts = newAppliedProduct.AssociatedProducts.Count > 0 ? true : false;
                                            newAppliedProduct.SelectedRatePerAreaUnitHelper = newAppliedProduct.AllowedRateUnits.Where(rpah => rpah.Unit.Name == p.RateUnit).FirstOrDefault();
                                            AppliedProducts.Add(newAppliedProduct);
                                            break;
                                        case ProductApplicationStrategy.ByRatePerTank:
                                            newAppliedProduct = new AppliedProductViewModel(clientEndpoint, this, fieldSelection, inventory)
                                            {
                                                ApplyBy = SelectedApplicationType,
                                                SelectedProduct = selectedProduct,
                                                SelectedRatePerTankUnit = UnitFactory.GetPackageSafeUnit(p.RatePerTankUnit, selectedProduct.Product.StdUnit, selectedProduct.Product.StdFactor, selectedProduct.Product.StdPackageUnit),
                                                RatePerTankValue = p.RatePerTankValue,
                                                ProductAreaPercent = p.PercentApplied,
                                                ApplicationMethod = appMethod,
                                                SelectedPest = (p.TargetPest != null) && PestList != null ? PestList.FirstOrDefault(x => x.CommonName == p.TargetPest.CommonName && x.LatinName == p.TargetPest.LatinName) : null,
                                            };

                                            newAppliedProduct.AssociatedProducts = new ObservableCollection<AssociatedProductItem>(GetAssociateProducts(p, newAppliedProduct.RemoveAssociatedProduct));
                                            newAppliedProduct.HasAssociatedProducts = newAppliedProduct.AssociatedProducts.Count > 0 ? true : false;
                                            newAppliedProduct.SelectedRatePerAreaUnitHelper = newAppliedProduct.AllowedRateUnits.Where(rpah => rpah.Unit.Name == p.RateUnit).FirstOrDefault();
                                            AppliedProducts.Add(newAppliedProduct);
                                            break;
                                        case ProductApplicationStrategy.ByTotalProduct:
                                            newAppliedProduct = new AppliedProductViewModel(clientEndpoint, this, fieldSelection, inventory)
                                            {
                                                ApplyBy = SelectedApplicationType,
                                                SelectedProduct = selectedProduct,
                                                TotalProductValue = p.TotalProductValue,
                                                TotalProductUnit = UnitFactory.GetPackageSafeUnit(p.TotalProductUnit, selectedProduct.Product.StdUnit, selectedProduct.Product.StdFactor, selectedProduct.Product.StdPackageUnit),
                                                ProductAreaPercent = p.PercentApplied,
                                                ApplicationMethod = appMethod,
                                                SelectedPest = (p.TargetPest != null) && PestList != null ? PestList.FirstOrDefault(x => x.CommonName == p.TargetPest.CommonName && x.LatinName == p.TargetPest.LatinName) : null,
                                            };

                                            newAppliedProduct.AssociatedProducts = new ObservableCollection<AssociatedProductItem>(GetAssociateProducts(p, newAppliedProduct.RemoveAssociatedProduct));
                                            newAppliedProduct.HasAssociatedProducts = newAppliedProduct.AssociatedProducts.Count > 0 ? true : false;
                                            newAppliedProduct.SelectedRatePerAreaUnitHelper = newAppliedProduct.AllowedRateUnits.Where(rpah => rpah.Unit.Name == p.RateUnit).FirstOrDefault();
                                            AppliedProducts.Add(newAppliedProduct);
                                            break;
                                    }
                                }
                            }

                            if (AppliedProducts.Any())
                            {
                                SelectedAppliedProduct = AppliedProducts.Last();
                            }
                        }

                        dispatcher.BeginInvoke(new Action(() =>
                        {
                            RaisePropertyChanged(() => ProductCount);
                        }));
                    });

                    DelayRunProductPopulationTask(loadProductsTask);

                }

                RaisePropertyChanged(() => AssociatedApplicationView);
            }
        }

		public WorkOrderView AssociatedWorkOrderView {
			get { return associatedWorkOrderView; }
			set {
				associatedWorkOrderView = value;

				if (associatedWorkOrderView != null) {
					Task loadProductsTask = new Task(() => {
						lock (AppliedProducts) {
							AppliedProducts.Clear();

							switch (associatedWorkOrderView.ApplicationStrategy) {
								case ProductApplicationStrategy.ByRatePerArea:
									SelectedApplicationType = ApplicationTypes.RatePerArea;
									break;
								case ProductApplicationStrategy.ByRatePerTank:
									SelectedApplicationType = ApplicationTypes.RatePerTank;
									break;
								case ProductApplicationStrategy.ByTotalProduct:
									SelectedApplicationType = ApplicationTypes.TotalProduct;
									break;
							}
							RaisePropertyChanged(() => SelectedApplicationType);

							if (associatedWorkOrderView.TankInformation != null) {
								TankInfo.SetTankInformation(associatedWorkOrderView.TankInformation);
							}

							if (associatedWorkOrderView != null) {
								foreach (var p in associatedWorkOrderView.Products) {
									var selectedProduct = (from pr in Products where pr.ProductId == p.Id select pr).FirstOrDefault();
									var appMethod = clientEndpoint.GetMasterlistService().GetApplicationMethodList().SingleOrDefault(x => x.Name == p.ApplicationMethod);
                                    var selectedPest = p.TargetPest != null ? clientEndpoint.GetMasterlistService().GetPestList().Where(x => x.Name == p.TargetPest.CommonName && x.LatinName == p.TargetPest.LatinName) : null;
									if (selectedProduct == null) { continue; } // TODO: Log that product couldn't be found.
									
                                    switch (associatedWorkOrderView.ApplicationStrategy) {
										case ProductApplicationStrategy.ByRatePerArea:
                                            var newAppliedProduct = new AppliedProductViewModel(clientEndpoint, this, fieldSelection, inventory) {
                                                ApplyBy = SelectedApplicationType,
                                                SelectedProduct = selectedProduct,
                                                RatePerAreaValue = p.RateValue,
                                                ProductAreaPercent = p.PercentApplied,
                                                ApplicationMethod = appMethod,
                                                SelectedPest = (p.TargetPest != null) && PestList != null ? PestList.FirstOrDefault(x => x.CommonName == p.TargetPest.CommonName && x.LatinName == p.TargetPest.LatinName) : null,
                                            };

                                            newAppliedProduct.AssociatedProducts = new ObservableCollection<AssociatedProductItem>(GetAssociateProducts(p, newAppliedProduct.RemoveAssociatedProduct));
                                            newAppliedProduct.HasAssociatedProducts = newAppliedProduct.AssociatedProducts.Count > 0 ? true : false;
                                            newAppliedProduct.SelectedRatePerAreaUnitHelper = newAppliedProduct.AllowedRateUnits.Where(rpah => rpah.Unit.Name == p.RateUnit).FirstOrDefault();

                                            if (p.SpecificCostPerUnit.HasValue) {
                                                var specificPriceItem = new ValueObjects.PriceItem(p.SpecificCostPerUnit.Value, p.SpecificCostUnit);
                                                newAppliedProduct.PriceItems.Add(specificPriceItem);
                                                newAppliedProduct.SelectedPriceItem = specificPriceItem;
                                            }

                                            AppliedProducts.Add(newAppliedProduct);
											break;
										case ProductApplicationStrategy.ByRatePerTank:
											newAppliedProduct = new AppliedProductViewModel(clientEndpoint, this, fieldSelection, inventory) {
												ApplyBy = SelectedApplicationType,
												SelectedProduct = selectedProduct,
												SelectedRatePerTankUnit = UnitFactory.GetPackageSafeUnit(p.RatePerTankUnit, selectedProduct.Product.StdUnit, selectedProduct.Product.StdFactor, selectedProduct.Product.StdPackageUnit),
												RatePerTankValue = p.RatePerTankValue,
												ProductAreaPercent = p.PercentApplied,
												ApplicationMethod = appMethod,
												SelectedPest = (p.TargetPest != null) && PestList != null ? PestList.FirstOrDefault(x => x.CommonName == p.TargetPest.CommonName && x.LatinName == p.TargetPest.LatinName) : null,
                                            };

                                            newAppliedProduct.AssociatedProducts = new ObservableCollection<AssociatedProductItem>(GetAssociateProducts(p, newAppliedProduct.RemoveAssociatedProduct));
                                            newAppliedProduct.HasAssociatedProducts = newAppliedProduct.AssociatedProducts.Count > 0 ? true : false;
                                            if (p.SpecificCostPerUnit.HasValue) {
                                                var specificPriceItem = new ValueObjects.PriceItem(p.SpecificCostPerUnit.Value, p.SpecificCostUnit);
                                                newAppliedProduct.PriceItems.Add(specificPriceItem);
                                                newAppliedProduct.SelectedPriceItem = specificPriceItem;
                                            }

											newAppliedProduct.SelectedRatePerAreaUnitHelper = newAppliedProduct.AllowedRateUnits.Where(rpah => rpah.Unit.Name == p.RateUnit).FirstOrDefault();
											AppliedProducts.Add(newAppliedProduct);
											break;
										case ProductApplicationStrategy.ByTotalProduct:
											newAppliedProduct = new AppliedProductViewModel(clientEndpoint, this, fieldSelection, inventory) {
												ApplyBy = SelectedApplicationType,
												SelectedProduct = selectedProduct,
												TotalProductValue = p.TotalProductValue,
												TotalProductUnit = UnitFactory.GetPackageSafeUnit(p.TotalProductUnit, selectedProduct.Product.StdUnit, selectedProduct.Product.StdFactor, selectedProduct.Product.StdPackageUnit),
												ProductAreaPercent = p.PercentApplied,
												ApplicationMethod = appMethod,
												SelectedPest = (p.TargetPest != null) && PestList != null ? PestList.FirstOrDefault(x => x.CommonName == p.TargetPest.CommonName && x.LatinName == p.TargetPest.LatinName) : null,
                                            };

                                            newAppliedProduct.AssociatedProducts = new ObservableCollection<AssociatedProductItem>(GetAssociateProducts(p, newAppliedProduct.RemoveAssociatedProduct));
                                            newAppliedProduct.HasAssociatedProducts = newAppliedProduct.AssociatedProducts.Count > 0 ? true : false;

                                            if (p.SpecificCostPerUnit.HasValue) {
                                                var specificPriceItem = new ValueObjects.PriceItem(p.SpecificCostPerUnit.Value, p.SpecificCostUnit);
                                                newAppliedProduct.PriceItems.Add(specificPriceItem);
                                                newAppliedProduct.SelectedPriceItem = specificPriceItem;
                                            }

											newAppliedProduct.SelectedRatePerAreaUnitHelper = newAppliedProduct.AllowedRateUnits.Where(rpah => rpah.Unit.Name == p.RateUnit).FirstOrDefault();
											AppliedProducts.Add(newAppliedProduct);
											break;
									}
                                }
							}

							if (AppliedProducts.Any()) {
								SelectedAppliedProduct = AppliedProducts.Last();
							}
						}

						dispatcher.BeginInvoke(new Action(() => {
							RaisePropertyChanged(() => ProductCount);
						}));
					});

					DelayRunProductPopulationTask(loadProductsTask);

				}

				RaisePropertyChanged(() => AssociatedWorkOrderView);
			}
		}

        private List<AssociatedProductItem> GetAssociateProducts(IncludedProduct p, Action<Guid> removeAssociate)
        {
            var associatedProducts = new List<AssociatedProductItem>();
            var mlp = clientEndpoint.GetMasterlistService().GetProduct(p.Id);
            foreach (var associate in p.AssociatedProducts)
            {
                var associateProd = new AssociatedProductItem(clientEndpoint, mlp, removeAssociate);
                var associateMLP = clientEndpoint.GetMasterlistService().GetProduct(associate.ProductId);
                associateProd.CustomUnit = UnitFactory.GetPackageSafeUnit(associate.CustomProductUnit, associateMLP.StdUnit, associateMLP.StdFactor, associateMLP.StdPackageUnit);
                associateProd.CustomRateValue = associate.CustomProductValue;
                associateProd.HasCost = associate.HasCost;
                //associateProd.ManufacturerName = associate.ManufacturerName;
                associateProd.ProductID = associate.ProductId;
                associateProd.ProductName = associate.ProductName;
                associateProd.RatePerAreaUnit = UnitFactory.GetPackageSafeUnit(associate.RatePerAreaUnit, associateMLP.StdUnit, associateMLP.StdFactor, associateMLP.StdPackageUnit);
                associateProd.RatePerAreaValue = associate.RatePerAreaValue;
                associateProd.CostPerUnitUnit = UnitFactory.GetPackageSafeUnit(associate.SpecificCostPerUnitUnit, associateMLP.StdUnit, associateMLP.StdFactor, associateMLP.StdPackageUnit);
                associateProd.CostPerUnitValue = associate.SpecificCostPerUnitValue;
                associateProd.TotalProductUnit = UnitFactory.GetPackageSafeUnit(associate.TotalProductUnit, associateMLP.StdUnit, associateMLP.StdFactor, associateMLP.StdPackageUnit);
                associateProd.TotalProductValue = associate.TotalProductValue;
                associateProd.TrackingID = associate.TrackingId;

                switch (associate.CustomRateType)
                {
                    case "ByBag":
                        associateProd.CustomRateType = AssociatedProductRateType.ByBag;
                        break;
                    case "ByCWT":
                        associateProd.CustomRateType = AssociatedProductRateType.ByCWT;
                        break;
                    case "ByRow":
                        associateProd.CustomRateType = AssociatedProductRateType.ByRow;
                        break;
                    case "BySeed":
                        associateProd.CustomRateType = AssociatedProductRateType.BySeed;
                        break;
                    default:
                        associateProd.CustomRateType = AssociatedProductRateType.BySeed;
                        break;
                }

                associatedProducts.Add(associateProd);
            }

            return associatedProducts;
        }

		public RecommendationView AssociatedRecommendationView {
			get { return associatedRecommendationView; }
			set {
				associatedRecommendationView = value;

				if (associatedRecommendationView != null) {
					Task loadProductsTask = new Task(() => {
						lock (AppliedProducts) {
							AppliedProducts.Clear();

							switch (associatedRecommendationView.ApplicationStrategy) {
								case ProductApplicationStrategy.ByRatePerArea:
									SelectedApplicationType = ApplicationTypes.RatePerArea;
									break;
								case ProductApplicationStrategy.ByTotalProduct:
									SelectedApplicationType = ApplicationTypes.TotalProduct;
									break;
								case ProductApplicationStrategy.ByRatePerTank:
									SelectedApplicationType = ApplicationTypes.RatePerTank;
									break;
								default:
									break;
							}

							if (associatedRecommendationView.TankInformation != null) {
								TankInfo.SetTankInformation(associatedRecommendationView.TankInformation);
							}

							if (associatedRecommendationView != null) {
								foreach (var p in associatedRecommendationView.Products) {
									var selectedProduct = (from pr in Products where pr.ProductId == p.ProductId select pr).FirstOrDefault();
									if (selectedProduct == null) { continue; } // TODO: Log that product couldn't be found.

									switch (associatedRecommendationView.ApplicationStrategy) {
										case ProductApplicationStrategy.ByRatePerArea:
											var newAppliedProduct = new AppliedProductViewModel(clientEndpoint, this, fieldSelection, inventory) {
												ApplyBy = SelectedApplicationType,
												SelectedProduct = selectedProduct,
												RatePerAreaValue = p.RatePerAreaValue.HasValue ? (decimal)p.RatePerAreaValue : 0m,
												ProductAreaPercent = (decimal)p.CoveragePercent,
												ApplicationMethod = string.IsNullOrEmpty(p.ApplicationMethod) ? null : ApplicationMethods.FirstOrDefault(x => x.Name == p.ApplicationMethod),
												SelectedPest = (p.TargetPest != null) && PestList != null ? PestList.FirstOrDefault(x => x.CommonName == p.TargetPest.CommonName && x.LatinName == p.TargetPest.LatinName) : null,
											};

											newAppliedProduct.SelectedRatePerAreaUnitHelper = newAppliedProduct.AllowedRateUnits.Where(rpah => rpah.Unit.Name == p.RatePerAreaUnit).FirstOrDefault();
											AppliedProducts.Add(newAppliedProduct);
											break;
										case ProductApplicationStrategy.ByTotalProduct:
											newAppliedProduct = new AppliedProductViewModel(clientEndpoint, this, fieldSelection, inventory) {
												ApplyBy = SelectedApplicationType,
												SelectedProduct = selectedProduct,
												TotalProductValue = p.TotalProductValue ?? 0,
												TotalProductUnit = UnitFactory.GetPackageSafeUnit(p.TotalProductUnit, selectedProduct.Product.StdUnit, selectedProduct.Product.StdFactor, selectedProduct.Product.StdPackageUnit),
												ProductAreaPercent = (decimal)p.CoveragePercent,
												ApplicationMethod = string.IsNullOrEmpty(p.ApplicationMethod) ? null : ApplicationMethods.FirstOrDefault(x => x.Name == p.ApplicationMethod),
                                                SelectedPest = p.TargetPest!= null && PestList != null ? PestList.FirstOrDefault(x => x.CommonName == p.TargetPest.CommonName && x.LatinName == p.TargetPest.LatinName) : null,
											};

											newAppliedProduct.SelectedRatePerAreaUnitHelper = newAppliedProduct.AllowedRateUnits.Where(rpah => rpah.Unit.Name == p.RatePerAreaUnit).FirstOrDefault();
											AppliedProducts.Add(newAppliedProduct);
											break;
										case ProductApplicationStrategy.ByRatePerTank:
											newAppliedProduct = new AppliedProductViewModel(clientEndpoint, this, fieldSelection, inventory) {
												ApplyBy = SelectedApplicationType,
												SelectedProduct = selectedProduct,
												SelectedRatePerTankUnit = UnitFactory.GetPackageSafeUnit(p.RatePerTankUnit, selectedProduct.Product.StdUnit, selectedProduct.Product.StdFactor, selectedProduct.Product.StdPackageUnit),
												RatePerTankValue = p.RatePerTankValue ?? 0,
												ProductAreaPercent = (decimal)p.CoveragePercent,
												ApplicationMethod = string.IsNullOrEmpty(p.ApplicationMethod) ? null : ApplicationMethods.FirstOrDefault(x => x.Name == p.ApplicationMethod),
                                                SelectedPest = p.TargetPest != null && PestList != null ? PestList.FirstOrDefault(x => x.CommonName == p.TargetPest.CommonName && x.LatinName == p.TargetPest.LatinName) : null,
											};

											newAppliedProduct.SelectedRatePerAreaUnitHelper = newAppliedProduct.AllowedRateUnits.Where(rpah => rpah.Unit.Name == p.RatePerAreaUnit).FirstOrDefault();
											AppliedProducts.Add(newAppliedProduct);
											break;
										default:
											break;
									}
								}
							}

							if (AppliedProducts.Any()) {
								SelectedAppliedProduct = AppliedProducts.Last();
							}
						}

						dispatcher.BeginInvoke(new Action(() => {
							RaisePropertyChanged(() => ProductCount);
						}));
					});

					DelayRunProductPopulationTask(loadProductsTask);

				}

				RaisePropertyChanged(() => AssociatedRecommendationView);
			}
		}

		public PlanView AssociatedPlanView {
			get { return associatedPlanView; }
			set {
				associatedPlanView = value;
				if (associatedPlanView != null) {
					Task loadProductsTask = new Task(() => {
						lock (AppliedProducts) {
							AppliedProducts.Clear();
							if (SelectedPlanProducts != null) {
								foreach (var p in SelectedPlanProducts) {
									var selectedProduct = (from pr in Products where pr.ProductId == p.ID select pr).FirstOrDefault();
									//var appMethod = clientEndpoint.GetMasterlistService().GetApplicationMethodList().SingleOrDefault(x => x.Name == p.ApplicationMethod);
									if (selectedProduct == null) { continue; } // TODO: Log that product couldn't be found.

									var newAppliedProduct = new AppliedProductViewModel(clientEndpoint, this, fieldSelection, inventory) {
										ApplyBy = SelectedApplicationType,
										SelectedProduct = selectedProduct,
										RatePerAreaValue = (decimal)p.RateMeasure.Value,
										ProductAreaPercent = p.ProductAreaPercent,
									};
									newAppliedProduct.SelectedRatePerAreaUnitHelper = newAppliedProduct.AllowedRateUnits.Where(rpah => rpah.Unit.Name == p.RateMeasure.Unit.Name).FirstOrDefault();
									AppliedProducts.Add(newAppliedProduct);
								}
							}
							if (AppliedProducts.Any()) {
								SelectedAppliedProduct = AppliedProducts.Last();
							}
						}
						dispatcher.BeginInvoke(new Action(() => {
							RaisePropertyChanged(() => ProductCount);
						}));
					});
					DelayRunProductPopulationTask(loadProductsTask);
				}
				RaisePropertyChanged(() => AssociatedPlanView);
			}
		}

		private ObservableCollection<ProductDetails> SelectedPlanProducts = new ObservableCollection<ProductDetails>();
		public void SetSelectedPlanProducts(ObservableCollection<ProductDetails> selectedPlanProducts) {
			SelectedPlanProducts = selectedPlanProducts;
		}

		void DelayRunProductPopulationTask(Task loadProductsTask) {
			while (productLoadTask == null) {
				System.Threading.Thread.Sleep(50); // This is stupid, right?
			}

			if (productLoadTask != null) {

				if (productLoadTask.Status != TaskStatus.RanToCompletion) {
					productLoadTask.ContinueWith(x => {
						loadProductsTask.Start();
					});
				} else {
					loadProductsTask.Start();
				}
			}
		}

		void PopulatePestList() {
			var ml = clientEndpoint.GetMasterlistService();
			var q = from p in ml.GetPestList()
					//where p.Language == "en"
					select new PestListItemDetails(p, (p.Name != string.Empty ? (short)0 : (short)1));
            q = q.OrderBy(b => b.GroupSortOrder).ThenBy(x => x.CommonName);
			PestList = new ObservableCollection<PestListItemDetails>(q);
			RaisePropertyChanged(() => PestList);
		}

		public decimal TotalCost {
			get { return totalCost; }
			set {
				var oldValue = totalCost;
				totalCost = value;

				RaisePropertyChanged(() => TotalCost);
			}
		}

		public RelayCommand AddAppliedProductCommand { get; private set; }
		public RelayCommand DeleteAppliedProductCommand { get; private set; }
		public RelayCommand ToggleTankInfoCommand { get; private set; }


		public ObservableCollection<ProductListItemDetails> Products { get; private set; }
		public ObservableCollection<PestListItemDetails> PestList { get; private set; }

		private ObservableCollection<AppliedProductViewModel> _appliedProducts;
		[CustomValidation(typeof(ProductSelectionViewModel), nameof(ValidateAppliedProducts))]
		public ObservableCollection<AppliedProductViewModel> AppliedProducts {
			get { return _appliedProducts; }
			private set {
				_appliedProducts = value;
				ValidateAndRaisePropertyChanged(() => AppliedProducts);
			}
		}

		public List<ApplicationMethod> ApplicationMethods {
			get {
				// return list
				return clientEndpoint.GetMasterlistService().GetApplicationMethodList().ToList();
			}
		}

		public ApplicationMethod ApplicationMethod {
			get { return appMethod; }
			set {
				appMethod = value;
				RaisePropertyChanged(() => ApplicationMethod);
			}
		}

		public bool ShowTankInfoPopup {
			get { return showTankInfoPopup; }
			set {
				showTankInfoPopup = value;
				RaisePropertyChanged(() => ShowTankInfoPopup);
			}
		}

	    public TankInformationModel TankInfo {
	        get;
	        set;
	    } 

		public ApplicationTypes SelectedApplicationType {
			get { return selectedApplicationType; }
			set {
				selectedApplicationType = value;
				RaisePropertyChanged(() => SelectedApplicationType);

				lock (AppliedProducts) {
					AppliedProducts.ForEach(x => x.ApplyBy = value);
				}

				//if (tankInfo.CarrierApplied.Value <= 0) {
				//	ShowTankInfoPopup = true;
				//}

				// open popup only when rate/tank is selected
				// https://fogbugz.agconnections.com/fogbugz/default.asp?26380
				switch (selectedApplicationType) {
					case ApplicationTypes.RatePerArea:
						ShowTankInfoPopup = false;
						break;
					case ApplicationTypes.TotalProduct:
						ShowTankInfoPopup = false;
						break;
					case ApplicationTypes.RatePerTank:
						ShowTankInfoPopup = true;
						break;
					default:
						ShowTankInfoPopup = false;
						break;
				}
			}
		}

		public IEnumerable<ApplicationTypes> ApplicationTypesList {
			get { return applicationTypesList; }
		}

		public int ProductCount {
			get {
				lock (AppliedProducts) {
					var q = from p in AppliedProducts
							where p.SelectedProduct != null
							select p;  // don't include products that have no data
					return q.Count();
				}
			}
		}

		public AppliedProductViewModel SelectedAppliedProduct {
			get { return selectedAppliedProduct; }
			set {
				selectedAppliedProduct = value;
				RaisePropertyChanged(() => SelectedAppliedProduct);
				if (selectedAppliedProduct != null) { ApplicationMethod = selectedAppliedProduct.ApplicationMethod; }
				RaisePropertyChanged(() => ApplicationMethod);
			}
		}

		public bool IsTankApplication {
			get { return isTankApplication; }
			set {
				isTankApplication = value;
				RaisePropertyChanged(() => IsTankApplication);
			}
		}

		void OnTankInfoChanged() {
			lock (AppliedProducts) {
				AppliedProducts.ForEach(x => x.RecalculateItem());
			}
		}

		void DeleteProduct() {
			AppliedProducts.Remove(SelectedAppliedProduct);
			ValidateViewModel();
			RaisePropertyChanged(() => ProductCount);
		}

		void AddProduct() {
			var newAppliedProduct = new AppliedProductViewModel(clientEndpoint, this, fieldSelection, inventory) {
				ApplyBy = SelectedApplicationType,
				ApplicationMethod = AppliedProducts.Count() > 0 ? AppliedProducts.Last().ApplicationMethod : null 
			};

			AppliedProducts.Add(newAppliedProduct);
			SelectedAppliedProduct = newAppliedProduct;
			RaisePropertyChanged(() => ProductCount);
		}

		async void BuildProductList() {
			var watch = Stopwatch.StartNew();

			productLoadTask = Task.Run(() => {
				var mpl = clientEndpoint.GetMasterlistService().GetProductList();
				var mur = clientEndpoint.GetView<ProductUsageView>(currentCropYearId).GetValue(new ProductUsageView());
				var ucp = clientEndpoint.GetView<UserCreatedProductList>(new DataSourceId(currentCropYearId.DataSourceId)).GetValue(new UserCreatedProductList());

				Guid mlDsId = GlobalIdentifiers.GetMasterlistDataSourceId();
				var productGuids = mur.GetProductGuids();

				var q = from p in mpl
						let cont = productGuids.Contains(p.Id)
						select new ProductListItemDetails(p, mlDsId, cont ? Strings.UsedThisYear_Text : Strings.Unused_Text, cont ? (short)0 : (short)1);

				var ucpQ = from p in ucp.UserCreatedProducts
						   let cont = productGuids.Contains(p.Id)
						   select new ProductListItemDetails(p, currentCropYearId.DataSourceId, cont ? Strings.UsedThisYear_Text : Strings.Unused_Text, cont ? (short)0 : (short)1);

				Products = new ObservableCollection<ProductListItemDetails>(q.Union(ucpQ).OrderBy(x => x.GroupSortOrder).ToList());
			});
			await productLoadTask;
			watch.Stop();
			log.Debug("Loading products took {0}ms", watch.Elapsed.TotalMilliseconds);
		}

		internal void RefreshCost() {
			lock (AppliedProducts) {
				TotalCost = (from ap in AppliedProducts
							 where ap.SelectedProduct != null
							 select ap.TotalGrowerCost).Sum();
			}
		}

		internal void RefreshProductCount() {
			RaisePropertyChanged(() => ProductCount);
		}

		internal ProductListItemDetails AddTemporaryMiniProduct(MiniProduct product, ProductId productId, bool isUsed) {
			var p = new ProductListItemDetails(product, productId.DataSourceId, isUsed ? Strings.UsedThisYear_Text : Strings.Unused_Text, isUsed ? (short)0 : (short)1);
			Products.Add(p);
			return p;
		}

		[Obsolete]
		internal ApplicationProductEntry[] GetAppliedProductEntries() {
			List<ApplicationProductEntry> appliedProducts = new List<ApplicationProductEntry>();

			lock (AppliedProducts) {
				foreach (var ap in AppliedProducts) {
					appliedProducts.Add(ap.ToApplicationProductEntry());
				}
			}

			return appliedProducts.ToArray();
		}


		internal ApplicationCommandProductEntry[] GetAppliedProducts() {
			List<ApplicationCommandProductEntry> appliedProducts = new List<ApplicationCommandProductEntry>();
			lock (AppliedProducts) {
				foreach (var ap in AppliedProducts) {
                    //ap.TotalProductFromGrowerInventory
					var cpe = ap.ToApplicationCommandProductEntry();
					if (cpe == null) { continue; }
					appliedProducts.Add(cpe);
				}
			}

			return appliedProducts.ToArray();
		}

		internal ApplicationTankInformation GetApplicationTankInformation() {
			return new ApplicationTankInformation((decimal) TankInfo.TankSize.Value, TankInfo.TankSize.Unit.Name, TankInfo.TankCount, (decimal)
			                                      TankInfo.CarrierPerArea.Value, TankInfo.CarrierPerArea.Unit.Name, (decimal) TankInfo.CarrierApplied.Value,
			                                      TankInfo.CarrierApplied.Unit.Name, string.IsNullOrEmpty(TankInfo.LockedValue) ? "CarrierPerArea" : TankInfo
			                                          .LockedValue);
		}

		internal string CreateNameFromProducts() {
			var sb = new StringBuilder();
			lock (AppliedProducts) {
				foreach (var ap in AppliedProducts) {
					if (sb.Length > 80) { break; }
					if (sb.Length > 0) { sb.Append(", "); }
					sb.Append(ap.ToName());
				}
			}

			return sb.ToString();
		}

		internal void ResetData() {
			AppliedProducts.Clear();
			RaisePropertyChanged(() => ProductCount);
			TotalCost = 0m;
			AddAppliedProductCommand.Execute(null); // Add the first product
		}

		#region Custom Validation
		public static ValidationResult ValidateAppliedProducts(ObservableCollection<AppliedProductViewModel> appliedProducts, ValidationContext context) {
			ValidationResult result = null;

			if (appliedProducts == null || !appliedProducts.Any()) {
				result = new ValidationResult(Strings.AtLeastOneProductIsRequired_Text);
			} else if (appliedProducts.Any(x => x.HasErrors)) {
				result = new ValidationResult(Strings.AtLeastOneProductHasErrors_Text);
			}

			return result;
		}
		#endregion
	}
}