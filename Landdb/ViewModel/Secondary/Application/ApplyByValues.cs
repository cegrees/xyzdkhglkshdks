﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Landdb.ViewModel.Secondary.Application {
    public enum ApplyByValues {
        ByArea,
        ByTank,
        ByTotal,
    }
}
