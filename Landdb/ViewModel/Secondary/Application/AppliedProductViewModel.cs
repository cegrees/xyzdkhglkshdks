using AgC.UnitConversion;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Contract;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Domain.ReadModels.ProductSetting;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ValueObjects;
using Landdb.ViewModel.Secondary.PopUp;
using Landdb.ViewModel.Secondary.UserCreatedProduct;
using Landdb.ViewModel.Shared;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Landdb.Resources;
using System.Globalization;
using Landdb.Client.Localization;

namespace Landdb.ViewModel.Secondary.Application
{
    public class AppliedProductViewModel : ValidationViewModelBase {

        readonly IClientEndpoint clientEndpoint;
        readonly ProductSelectionViewModel parent;
        readonly FieldSelectionViewModel fieldSelectionModel;

        readonly IApplicationCalculationService applicationCalculationService;
        readonly IApplicationCommandService applicationCommandService;

        private Guid trackingId;

        private InventoryListItem currentInventoryItem;
        private InventoryListView inventory;

        private Measure totalArea;

        public AppliedProductViewModel(IClientEndpoint clientEndpoint, ProductSelectionViewModel parent, FieldSelectionViewModel fieldSelectionModel, InventoryListView inventory, ApplicationTypes applyBy = ApplicationTypes.RatePerArea) {
            this.clientEndpoint = clientEndpoint;
            this.parent = parent;
            this.fieldSelectionModel = fieldSelectionModel;
            this._applyBy = applyBy;
            this.inventory = inventory;

            this.applicationCalculationService = new ApplicationCalculationService();
            this.applicationCommandService = new ApplicationCommandService();

            AllowedRateUnits = new ObservableCollection<UnitPerAreaHelper>();
            PriceItems = new ObservableCollection<PriceItem>();
            AllowedTotalUnits = new ObservableCollection<IUnit>();
            IsSeedProduct = false;

            totalArea = fieldSelectionModel.TotalArea;
            ProductArea = totalArea;

            fieldSelectionModel.PropertyChanged += fieldSelectionModel_PropertyChanged;

            trackingId = Guid.NewGuid();

            if (fieldSelectionModel.FirstAssociatedContract != null) {
                var contractDetailsMaybe = clientEndpoint.GetView<RentContractDetailsView>(fieldSelectionModel.FirstAssociatedContract);
                contractDetailsMaybe.IfValue(contractDetails => { Contract = contractDetails; });
            }

            CreateUserProductCommand = new RelayCommand(showCreateUserProductOverlay);
            ChangePreferredRateUnitCommand = new RelayCommand(changePreferredRateUnit);
            ChangePreferredTotalUnitCommand = new RelayCommand(changePreferredTotalUnit);
            ViewCalculator = new RelayCommand(showSpecialityCalculatorPopup);
            AssociateProduct = new RelayCommand(AssociateSeedTreatmentProduct);

            ErrorsChanged += (sender, args) => {
                parent.ValidateViewModel();
            };

            HasAssociatedProducts = false;
            AssociatedProducts = new ObservableCollection<AssociatedProductItem>();
            ValidateViewModel();
        }

        public RelayCommand CreateUserProductCommand { get; }
		public RelayCommand ChangePreferredRateUnitCommand { get; }
		public RelayCommand ChangePreferredTotalUnitCommand { get; }
		public RelayCommand ViewCalculator { get; }
		public RelayCommand AssociateProduct { get; }
		#region Properties
		public ObservableCollection<UnitPerAreaHelper> AllowedRateUnits { get; }
		public ObservableCollection<IUnit> AllowedTotalUnits { get; }
		public ObservableCollection<PriceItem> PriceItems { get; }

		public string ProductSearchText { get; set; }
		public string PestSearchText { get; set; }

		private ApplicationMethod _appMethod;
        public ApplicationMethod ApplicationMethod {
            get { return _appMethod; }
            set {
                _appMethod = value;
                RaisePropertyChanged(() => ApplicationMethod);
            }
        }

        private ApplicationTypes _applyBy;
        public ApplicationTypes ApplyBy {
            get { return _applyBy; }
            set {
                _applyBy = value;
                RaisePropertyChanged(() => ApplyBy);

                ValidateViewModel();
            }
        }

        private ProductListItemDetails _selectedProduct;
        [CustomValidation(typeof(AppliedProductViewModel), nameof(ValidateSelectedProduct))]
        public ProductListItemDetails SelectedProduct {
            get { return _selectedProduct; }
            set {
                _selectedProduct = value;

                ValidateAndRaisePropertyChanged(() => SelectedProduct);

                RaisePropertyChanged(() => DisplayName);
                RaisePropertyChanged(() => ApplicationMethod);
                RaisePropertyChanged(() => SelectedPest);

                updateAllowedUnits();
                updateInventoryListItem();
                updatePriceItems();

                if (value != null && value.Product != null && value.Product.ProductType == GlobalStrings.ProductType_Service && value.Product.StdPackageUnit == ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit) {
                    RatePerAreaValue = 1; // default to 1 for acre-based services because they're almost always "1 acre". Should probably extend this to other area units as well, for intl.
                } else {
                    RatePerAreaValue = 0;  // TODO: In the future, this should be the most recently used rate. We should also populate a list of the MR rates for this product.
                }

                RecalculateItem();

                ResetContractSpecificInfo();

                parent.RefreshProductCount();

                if (value == null || value.Product == null) {
                    IsCreateProductPopupOpen = true;
                }

                if (SelectedProduct != null && SelectedProduct.Product.ProductType == GlobalStrings.ProductType_Seed) { IsSeedProduct = true; } else { IsSeedProduct = false; }
            }
        }

        private decimal _ratePerAreaValue;
        [CustomValidation(typeof(AppliedProductViewModel), nameof(ValidateRate))]
        public decimal RatePerAreaValue {
            get { return _ratePerAreaValue; }
            set {
                _ratePerAreaValue = value;

                RaisePropertyChanged(() => RatePerAreaValue);
                RaisePropertyChanged(() => RatePerAreaDisplay);

                if (_applyBy == ApplicationTypes.RatePerArea) {
                    RecalculateItem();
                }

                if (AssociatedProducts.Any()) { ReCaculateAssociatedProducts(); }
            }
        }

        private UnitPerAreaHelper _ratePerAreaUnitHelper;

        [CustomValidation(typeof(AppliedProductViewModel), nameof(ValidateUnit))]
        public UnitPerAreaHelper SelectedRatePerAreaUnitHelper {
            get { return _ratePerAreaUnitHelper; }
            set {
                _ratePerAreaUnitHelper = value;

                RaisePropertyChanged(() => SelectedRatePerAreaUnitHelper);
                RaisePropertyChanged(() => RatePerAreaDisplay);

                if (_applyBy == ApplicationTypes.RatePerArea) {
                    RecalculateItem();
                }
            }
        }

        private decimal _ratePerTankValue;
        [CustomValidation(typeof(AppliedProductViewModel), nameof(ValidateRate))]
        public decimal RatePerTankValue {
            get { return _ratePerTankValue; }
            set {
                _ratePerTankValue = value;

                RaisePropertyChanged(() => RatePerTankValue);
                RaisePropertyChanged(() => RatePerTankDisplay);

                if (_applyBy == ApplicationTypes.RatePerTank) {
                    RecalculateItem();
                }
            }
        }

        private IUnit _ratePerTankUnit;
        [CustomValidation(typeof(AppliedProductViewModel), nameof(ValidateUnit))]
        public IUnit SelectedRatePerTankUnit {
            get { return _ratePerTankUnit; }
            set {
                _ratePerTankUnit = value;

                RaisePropertyChanged(() => SelectedRatePerTankUnit);
                RaisePropertyChanged(() => RatePerTankDisplay);

                if (_applyBy == ApplicationTypes.RatePerTank) {
                    RecalculateItem();
                }
            }
        }

        private PriceItem _averagePriceItem;
        public PriceItem AveragePriceItem {
            get { return _averagePriceItem; }
            set {
                _averagePriceItem = value;
                RaisePropertyChanged(() => AveragePriceItem);
                RecalculateItem();
            }
        }

        private PriceItem _selectedPriceItem;
        public PriceItem SelectedPriceItem {
            get { return _selectedPriceItem; }
            set {
                _selectedPriceItem = value;

                RaisePropertyChanged(() => SelectedPriceItem);

                RecalculateItem();

                if (value != null) {
                    _selectedPriceText = value.ToString();
                    RaisePropertyChanged(() => SelectedPriceText);
                }
            }
        }

        private string _selectedPriceText;
        public string SelectedPriceText {
            get { return _selectedPriceText; }
            set {
                if (string.IsNullOrWhiteSpace(value)) { return; }

                if (_selectedPriceText != value && (SelectedPriceItem == null || value != SelectedPriceItem.ToString())) {
                    _selectedPriceText = value;
                    decimal price = 0m;
                    var seeThis = decimal.TryParse(SelectedPriceText, out price);

                    if (decimal.TryParse(_selectedPriceText, out price)) {
                        var priceUnit = currentInventoryItem == null || currentInventoryItem.AveragePriceUnit == null ? _selectedProduct.Product.StdPackageUnit : currentInventoryItem.AveragePriceUnit;
                        PriceItem priceItem = new PriceItem(price, priceUnit);
                        if (!PriceItems.Contains(priceItem)) { PriceItems.Add(priceItem); }
                        SelectedPriceItem = priceItem;
                        _selectedPriceText = priceItem.ToString();
                    } else {
                        //INCASE OF PARSE FAILURE   
                        string parsedString = string.Empty;
                        for (int i = 0; i < _selectedPriceText.Count(); i++) {
                            if (Char.IsDigit(_selectedPriceText[i]) || _selectedPriceText[i] == '.') {
                                parsedString += _selectedPriceText[i];
                            }
                        }

                        if (decimal.TryParse(parsedString, out price)) {
                            var priceUnit = currentInventoryItem == null || currentInventoryItem.AveragePriceUnit == null ? _selectedProduct.Product.StdPackageUnit : currentInventoryItem.AveragePriceUnit;
                            PriceItem priceItem = new PriceItem(price, priceUnit);
                            if (!PriceItems.Contains(priceItem)) { PriceItems.Add(priceItem); }
                            SelectedPriceItem = priceItem;
                            _selectedPriceText = priceItem.ToString();
                        }
                    }
                } else {
                    _selectedPriceText = _selectedPriceItem.ToString();
                }
            }
        }

        private decimal _costValuePerUnit;
        public decimal CostValuePerUnit {
            get { return _costValuePerUnit; }
            set {
                _costValuePerUnit = value;
                RaisePropertyChanged(() => CostValuePerUnit);
                RecalculateItem();
            }
        }

        private IUnit _costUnit;
        public IUnit CostUnit {
            get { return _costUnit; }
            set {
                _costUnit = value;
                RaisePropertyChanged(() => CostUnit);
            }
        }

        private decimal _totalProductValue;
        [CustomValidation(typeof(AppliedProductViewModel), nameof(ValidateRate))]
        public decimal TotalProductValue {
            get { return _totalProductValue; }
            set {
                _totalProductValue = value;

                RaisePropertyChanged(() => TotalProductValue);
                RaisePropertyChanged(() => TotalProduct);

                if (_applyBy == ApplicationTypes.TotalProduct) {
                    RecalculateItem();
                }
            }
        }

        private IUnit _totalProductUnit;
        [CustomValidation(typeof(AppliedProductViewModel), nameof(ValidateUnit))]
        public IUnit TotalProductUnit {
            get { return _totalProductUnit; }
            set {
                _totalProductUnit = value;

                RaisePropertyChanged(() => TotalProductUnit);
                RaisePropertyChanged(() => TotalProduct);

                if (_applyBy == ApplicationTypes.TotalProduct) {
                    RecalculateItem();
                }
            }
        }

        private decimal _totalCost;
        public decimal TotalCost {
            get { return _totalCost; }
            set {
                _totalCost = value;

                RaisePropertyChanged(() => TotalCost);
                RaisePropertyChanged(() => TotalGrowerCost);

                parent.RefreshCost();
            }
        }

        private decimal _productAreaPercent = 1;
        public decimal ProductAreaPercent {
            get { return _productAreaPercent; }
            set {
                _productAreaPercent = value;
                RaisePropertyChanged(() => ProductAreaPercent);

                RecalculateItem();
            }
        }

        public string ProductAreaDisplayText {
            get { return ProductArea.AbbreviatedDisplay; }
            set {
                if (string.IsNullOrWhiteSpace(value)) { return; }

                double parsedValue = 0;
                if (double.TryParse(value, out parsedValue)) {
                    // this will fire off a recalc which will update the product area
                    ProductAreaPercent = totalArea.Value != 0 ? Convert.ToDecimal(parsedValue / totalArea.Value) : 1;
                }

                RaisePropertyChanged(() => ProductAreaDisplayText);

                RecalculateItem();
            }
        }

        private RentContractDetailsView _contractDetails;
        public RentContractDetailsView Contract {
            get { return _contractDetails; }
            set {
                _contractDetails = value;

                ResetContractSpecificInfo();

                RaisePropertyChanged(() => Contract);
                RaisePropertyChanged(() => HasContract);
                RaisePropertyChanged(() => IsShareInfoVisible);
                RaisePropertyChanged(() => CostLabelText);
            }
        }

        private decimal _growerSharePercent;
        [Range(typeof(decimal), "0", "1", ErrorMessageResourceName = "ValueMustBeBetweenZeroAndOneHundred_Text", ErrorMessageResourceType = typeof(Strings))]
        public decimal GrowerSharePercent {
            get { return _growerSharePercent; }
            set {
                _growerSharePercent = value;
                ValidateAndRaisePropertyChanged(() => GrowerSharePercent);
                updatePercentFromGrowerInventory();
            }
        }

        private decimal _percentFromGrowerInventory = 1;
        [Range(typeof(decimal), "0", "1", ErrorMessageResourceName = "ValueMustBeBetweenZeroAndOneHundred_Text", ErrorMessageResourceType = typeof(Strings))]
        public decimal PercentFromGrowerInventory {
            get { return _percentFromGrowerInventory; }
            set {
                _percentFromGrowerInventory = value;
                ValidateAndRaisePropertyChanged(() => PercentFromGrowerInventory);
                calculateTotalProductFromGrowerInventory();
            }
        }

        private bool _isSeedProduct { get; set; }
        public bool IsSeedProduct
        {
            get { return _isSeedProduct; }
            set
            {
                _isSeedProduct = value;
                RaisePropertyChanged(() => IsSeedProduct);
            }
        }

        private Measure _totalProductFromGrowerInventory;
        public Measure TotalProductFromGrowerInventory {
            get { return _totalProductFromGrowerInventory; }
            set {
                _totalProductFromGrowerInventory = value;
                RaisePropertyChanged(() => TotalProductFromGrowerInventory);
                RaisePropertyChanged(() => TotalGrowerCost);
            }
        }

        private bool _isProductListVisible;
        public bool IsProductListVisible {
            get { return _isProductListVisible; }
            set {
                _isProductListVisible = value;
                RaisePropertyChanged(() => IsProductListVisible);
            }
        }

        private bool _isCreateProductPopupOpen;
        public bool IsCreateProductPopupOpen {
            get { return _isCreateProductPopupOpen; }
            set {
                _isCreateProductPopupOpen = value;
                RaisePropertyChanged(() => IsCreateProductPopupOpen);
            }
        }

        private PestListItemDetails _selectedPest;
        [CustomValidation(typeof(AppliedProductViewModel), nameof(ValidateSelectedPest))]
        public PestListItemDetails SelectedPest {
            get { return _selectedPest; }
            set {
                _selectedPest = value;
                ValidateAndRaisePropertyChanged(() => SelectedPest);
            }
        }

		private Measure _productArea;
        // readonly property for displaying product area. Setter is only for conveniently raising the prop-changed event for the view
        public Measure ProductArea {
            get { return _productArea; }
            private set {
                _productArea = value;
                RaisePropertyChanged(() => ProductArea);
                RaisePropertyChanged(() => ProductAreaDisplayText);
            }
        }

		public bool IsShareInfoVisible => HasContract && parent.IsShareInfoVisible;
		public bool HasContract => _contractDetails != null;
		public string CostLabelText => HasContract ? Strings.GrowerCost_Text.ToUpper() : Strings.ProductCost_Text.ToUpper();
		public List<ApplicationMethod> ApplicationMethods => parent.ApplicationMethods;
		public TankInformationModel TankInfo => parent.TankInfo;

		public string RatePerAreaDisplay {
            get {
                if (_ratePerAreaUnitHelper == null) { return string.Empty; }
                var rateString = string.Format("{0} {1} / {2}", _ratePerAreaUnitHelper.Unit.GetMeasure((double)_ratePerAreaValue, SelectedProduct.Product.Density).Value.ToString("N2"), _ratePerAreaUnitHelper.Unit.AbbreviatedDisplay, _ratePerAreaUnitHelper.AreaUnit.AbbreviatedDisplay);
                return rateString;
            }
        }

        public string RatePerTankDisplay {
            get {
                if (_ratePerTankUnit == null) { return string.Empty; }
                var rateString = _ratePerTankUnit.GetMeasure((double)_ratePerTankValue, SelectedProduct.Product.Density).ToString();
                return rateString;
            }
        }

        public Measure TotalProduct {
            get {
                if (_totalProductUnit != null && !(TotalProductUnit is NullUnit) && SelectedProduct != null) {
                    return _totalProductUnit.GetMeasure((double)_totalProductValue, SelectedProduct.Product.Density);
                } else {
                    return null;
                }
            }
        }

        public string DisplayName {
            get {
                if (SelectedProduct == null) { return string.Empty; }
                return $"{SelectedProduct.Product.Name} ({SelectedProduct.Product.Manufacturer})";
            }
        }

		public decimal TotalGrowerCost {
            get {
                if (HasContract) {
                    //return TotalCost * PercentFromGrowerInventory;
                    return TotalCost * GrowerSharePercent;
                } else {
                    return TotalCost;
                }
            }
        }
		#endregion

		public void UpdateFromCalculator(Measure RatePerArea, Measure RatePerTank, Measure TotalProduct) {
            switch (_applyBy) {
                case ApplicationTypes.RatePerArea:
                default:
                    RatePerAreaValue = (decimal)RatePerArea.Value;
                    SelectedRatePerAreaUnitHelper = AllowedRateUnits.Where(x => x.Unit == RatePerArea.Unit).FirstOrDefault();
                    break;
                case ApplicationTypes.TotalProduct:
                    TotalProductValue = (decimal)TotalProduct.Value;
                    TotalProductUnit = TotalProduct.Unit;
                    break;
                case ApplicationTypes.RatePerTank:
                    RatePerTankValue = (decimal)RatePerTank.Value;
                    SelectedRatePerTankUnit = RatePerTank.Unit;
                    break;
            }
        }

        public void ResetContractSpecificInfo() {
            if (_contractDetails == null || SelectedProduct == null || SelectedProduct.Product == null) {
                _percentFromGrowerInventory = 1.0m;
                _growerSharePercent = 1.0m;
                return;
            }

            if (SelectedProduct.Product.ProductType == GlobalStrings.ProductType_CropProtection) {
                GrowerSharePercent = (decimal)_contractDetails.IncludedShareInfo.GrowerCropProtectionShare;
            } else if (SelectedProduct.Product.ProductType == GlobalStrings.ProductType_Fertilizer) {
                GrowerSharePercent = (decimal)_contractDetails.IncludedShareInfo.GrowerFertilizerShare;
            } else if (SelectedProduct.Product.ProductType == GlobalStrings.ProductType_Seed) {
                GrowerSharePercent = (decimal)_contractDetails.IncludedShareInfo.GrowerSeedShare;
            } else if (SelectedProduct.Product.ProductType == GlobalStrings.ProductType_Service) {
                GrowerSharePercent = (decimal)_contractDetails.IncludedShareInfo.GrowerServiceShare;
            }

            updatePercentFromGrowerInventory();
        }

        internal void RecalculateItem() {
            switch (ApplyBy) {
                case ApplicationTypes.RatePerTank:
                    recalculateByTank();
                    break;
                case ApplicationTypes.TotalProduct:
                    recalculateByTotal();
                    break;
                case ApplicationTypes.RatePerArea:
                default:
                    recalculateByArea();
                    break;
            }

            calculateTotalProductFromGrowerInventory();

            ValidateProperty(() => RatePerAreaValue);
            ValidateProperty(() => RatePerTankValue);
            ValidateProperty(() => TotalProductValue);

            ValidateProperty(() => SelectedRatePerAreaUnitHelper);
            ValidateProperty(() => SelectedRatePerTankUnit);
            ValidateProperty(() => TotalProductUnit);
        }

        internal string ToName() {
            if (SelectedProduct != null && SelectedProduct.Product != null) {
                return SelectedProduct.Product.Name;
            } else {
                return string.Empty;
            }
        }

        internal ApplicationCommandProductEntry ToApplicationCommandProductEntry() {
            switch (_applyBy) {
                case ApplicationTypes.RatePerArea:
                default:
                    return toByAreaApplicationCommandProductEntry();
                case ApplicationTypes.TotalProduct:
                    return toByTotalApplicationCommandProductEntry();
                case ApplicationTypes.RatePerTank:
                    return toByTankApplicationCommandProductEntry();
            }
        }

        [Obsolete]
        internal ApplicationProductEntry ToApplicationProductEntry() {
            // TODO: This needs to represent the actual rate unit the user has chosen.
            var psu = UnitFactory.GetPackageSafeUnit(SelectedProduct.Product.StdPackageUnit, SelectedProduct.Product.StdUnit, SelectedProduct.Product.StdFactor, SelectedProduct.Product.StdPackageUnit);

            PriceStrategy strategy = AveragePriceItem.IsSystem ? PriceStrategy.SystemDefined : PriceStrategy.Specific;
            decimal price = AveragePriceItem.IsSystem ? 0m : AveragePriceItem.Price;
            string priceUnit = AveragePriceItem.IsSystem ? null : AveragePriceItem.PriceUnit;

            decimal? specificCost = null;
            string specificCostUnit = null;

            if (SelectedPriceItem != null && !SelectedPriceItem.IsSystem) {
                specificCost = SelectedPriceItem.Price;
                specificCostUnit = SelectedPriceItem.PriceUnit;
            }

            return new ApplicationProductEntry(new ProductId(SelectedProduct.Product.Id), SelectedProduct.Product.Name, SelectedProduct.Product.Manufacturer, trackingId,
                _ratePerAreaValue, psu.Name, ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit, 0m, psu.Name, (decimal)TotalProduct.Value, TotalProduct.Unit.Name, price, priceUnit, TotalGrowerCost, ProductAreaPercent, (decimal)ProductArea.Value, ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit, getShareOwnerInformation(), SelectedProduct.Product.Density, ApplicationMethod.Name, null, new List<ApplicationAssociatedProduct>(), specificCost, specificCostUnit);
        }

        #region Private Recalculate Methods
        private void recalculateByArea() {
            if (SelectedProduct == null || SelectedRatePerAreaUnitHelper == null) {
                TotalProductValue = 0;
                TotalProductUnit = new NullUnit();

                SelectedRatePerTankUnit = null;
                RatePerTankValue = 0m;
                return;
            }

            var rate = SelectedRatePerAreaUnitHelper.Unit.GetMeasure((double)RatePerAreaValue, SelectedProduct.Product.Density);
            ProductArea = fieldSelectionModel.TotalArea.Unit.GetMeasure(fieldSelectionModel.TotalArea.Value * (double)_productAreaPercent);
            var calculation = applicationCalculationService.RecalculateByArea(
                SelectedProduct.Product,
                SelectedPriceItem ?? AveragePriceItem,
                rate,
                ProductArea,
                parent.TankInfo == null ? 0m : parent.TankInfo.TankCount,
                AllowedTotalUnits
            );

            var displayTotal = DisplayMeasureConverter.ConvertToTotalDisplayMeasure(SelectedProduct.ProductId, calculation.TotalProductValue, calculation.TotalProductUnit.Name, SelectedProduct.Product.Density, clientEndpoint, ApplicationEnvironment.CurrentDataSourceId);
            TotalProductValue = (decimal)displayTotal.Value;
            TotalProductUnit = (from r in AllowedTotalUnits where r.Name == displayTotal.Unit.Name select r).FirstOrDefault();

            if (calculation.RatePerTankUnit != null) {
                var displayTank = DisplayMeasureConverter.ConvertToRatePerTankDisplayMeasure(SelectedProduct.ProductId, calculation.RatePerTankValue, calculation.RatePerTankUnit.Name, SelectedProduct.Product.Density, clientEndpoint, ApplicationEnvironment.CurrentDataSourceId);

                RatePerTankValue = (decimal)displayTank.Value;
                SelectedRatePerTankUnit = displayTank.Unit;
            } else {
                RatePerTankValue = calculation.RatePerTankValue;
            }

            TotalCost = calculation.TotalCost;
        }

        private void recalculateByTotal() {
            if (SelectedProduct == null || TotalProductUnit == null) {
                SelectedRatePerAreaUnitHelper = null;
                RatePerAreaValue = 0m;

                SelectedRatePerTankUnit = null;
                RatePerTankValue = 0m;
                return;
            }

            var total = TotalProductUnit.GetMeasure((double)TotalProductValue, SelectedProduct.Product.Density);
            var productArea = fieldSelectionModel.TotalArea.Unit.GetMeasure(fieldSelectionModel.TotalArea.Value * (double)_productAreaPercent);

            var calculation = applicationCalculationService.RecalculateByTotal(
                SelectedProduct.Product,
                total,
                productArea,
                parent.TankInfo == null ? 0m : parent.TankInfo.TankCount,
                AllowedTotalUnits,
                SelectedPriceItem ?? AveragePriceItem
            );

            ProductArea = calculation.ProductArea;

            var displayRate = DisplayMeasureConverter.ConvertToRateDisplayMeasure(SelectedProduct.ProductId, calculation.RatePerAreaValue, calculation.PackageSafeUnit.Name, SelectedProduct.Product.Density, clientEndpoint, ApplicationEnvironment.CurrentDataSourceId);
            RatePerAreaValue = (decimal)displayRate.Value;
            SelectedRatePerAreaUnitHelper = (from r in AllowedRateUnits where r.Unit.Name == displayRate.Unit.Name select r).FirstOrDefault();

            if (calculation.RatePerTankUnit != null) {
                var displayTank = DisplayMeasureConverter.ConvertToRatePerTankDisplayMeasure(SelectedProduct.ProductId, calculation.RatePerTankValue, calculation.RatePerTankUnit.Name, SelectedProduct.Product.Density, clientEndpoint, ApplicationEnvironment.CurrentDataSourceId);

                RatePerTankValue = (decimal)displayTank.Value;
                SelectedRatePerTankUnit = displayTank.Unit;
            } else {
                RatePerTankValue = calculation.RatePerTankValue;
            }

            TotalCost = calculation.TotalCost;
        }

        private void recalculateByTank() {
            if (SelectedProduct == null || SelectedRatePerTankUnit == null) {
                SelectedRatePerAreaUnitHelper = null;
                RatePerAreaValue = 0m;

                TotalProductValue = 0;
                TotalProductUnit = new NullUnit();
                return;
            }

            var ratePerTank = SelectedRatePerTankUnit.GetMeasure((double)RatePerTankValue, SelectedProduct.Product.Density);
            var productArea = fieldSelectionModel.TotalArea.Unit.GetMeasure(fieldSelectionModel.TotalArea.Value * (double)_productAreaPercent);

            var calculation = applicationCalculationService.RecalculateByTank(
                SelectedProduct.Product,
                SelectedPriceItem ?? AveragePriceItem,
                ratePerTank,
                productArea,
                parent.TankInfo.TankCount
            );

            ProductArea = calculation.ProductArea;

            var displayTotal = DisplayMeasureConverter.ConvertToTotalDisplayMeasure(SelectedProduct.ProductId, calculation.TotalProductValue, calculation.TotalProductUnit.Name, SelectedProduct.Product.Density, clientEndpoint, ApplicationEnvironment.CurrentDataSourceId);
            TotalProductValue = (decimal)displayTotal.Value;
            TotalProductUnit = (from r in AllowedTotalUnits where r.Name == displayTotal.Unit.Name select r).FirstOrDefault();

            var displayRate = DisplayMeasureConverter.ConvertToRateDisplayMeasure(SelectedProduct.ProductId, calculation.RatePerAreaValue, calculation.PackageSafeUnit.Name, SelectedProduct.Product.Density, clientEndpoint, ApplicationEnvironment.CurrentDataSourceId);
            RatePerAreaValue = (decimal)displayRate.Value;
            SelectedRatePerAreaUnitHelper = (from r in AllowedRateUnits where r.Unit.Name == displayRate.Unit.Name select r).FirstOrDefault();

            TotalCost = calculation.TotalCost;
        }
        #endregion

        private void updatePercentFromGrowerInventory() {
            if (SelectedProduct != null && SelectedProduct.Product != null) {
                if (SelectedProduct.Product.ProductType == GlobalStrings.ProductType_CropProtection) {
                    PercentFromGrowerInventory = _contractDetails.IncludedInventoryInfo.IsCropProtectionFromInventory ? 1.0m : GrowerSharePercent;
                } else if (SelectedProduct.Product.ProductType == GlobalStrings.ProductType_Fertilizer) {
                    PercentFromGrowerInventory = _contractDetails.IncludedInventoryInfo.IsFertilizerFromInventory ? 1.0m : GrowerSharePercent;
                } else if (SelectedProduct.Product.ProductType == GlobalStrings.ProductType_Seed) {
                    PercentFromGrowerInventory = _contractDetails.IncludedInventoryInfo.IsSeedFromInventory ? 1.0m : GrowerSharePercent;
                } else if (SelectedProduct.Product.ProductType == GlobalStrings.ProductType_Service) {
                    PercentFromGrowerInventory = _contractDetails.IncludedInventoryInfo.IsServiceFromInventory ? 1.0m : GrowerSharePercent;
                }
            }
        }

        private void updateInventoryListItem() {
            if (_selectedProduct == null || _selectedProduct.Product == null) {
                currentInventoryItem = null;
                return;
            }

            currentInventoryItem = inventory.Products.ContainsKey(_selectedProduct.ProductId) ? inventory.Products[_selectedProduct.ProductId] : null;

            if (currentInventoryItem != null) {
                var priceUnit = currentInventoryItem.AveragePriceUnit != null ? currentInventoryItem.AveragePriceUnit : UnitFactory.GetPackageSafeUnit(_selectedProduct.Product.StdPackageUnit, _selectedProduct.Product.StdUnit, _selectedProduct.Product.StdFactor, _selectedProduct.Product.StdPackageUnit).Name;
                var priceItem = new PriceItem((decimal)currentInventoryItem.AveragePriceValue, priceUnit, true);
                AveragePriceItem = priceItem;
            } else {
                var psu = UnitFactory.GetPackageSafeUnit(_selectedProduct.Product.StdPackageUnit, _selectedProduct.Product.StdUnit, _selectedProduct.Product.StdFactor, _selectedProduct.Product.StdPackageUnit);
                var priceItem = new PriceItem(0m, psu.Name, true);
                AveragePriceItem = priceItem;
            }
        }

        private void updatePriceItems() {
            PriceItems.Clear();
            if (currentInventoryItem != null) {
                var priceItem = new PriceItem((decimal)currentInventoryItem.AveragePriceValue, currentInventoryItem.AveragePriceUnit, true);
                PriceItems.Add(priceItem);
                SelectedPriceItem = priceItem;
            } else {
                if (SelectedProduct == null) {
                    SelectedPriceItem = null;
                } else {
                    var priceItem = new PriceItem(0m, SelectedProduct.Product.StdPackageUnit, false);
                    PriceItems.Add(priceItem);
                    SelectedPriceItem = null;
                }
            }

            // Try to get the view with past price items
            var cachedPrices = clientEndpoint.GetView<Landdb.Domain.ReadModels.Product.SpecificPriceCacheView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear));
            if (cachedPrices.HasValue && SelectedProduct != null && cachedPrices.Value.Prices.ContainsKey(SelectedProduct.ProductId.Id)) {
                var priceList = cachedPrices.Value.Prices[SelectedProduct.ProductId.Id];
                foreach (var p in priceList) {
                    PriceItems.Add(new PriceItem(p.Price, p.PriceUnit, false));
                }
            }
        }

        private void updateAllowedUnits() {
            AllowedRateUnits.Clear();
            AllowedTotalUnits.Clear();

            SelectedRatePerAreaUnitHelper = null;

            if (SelectedProduct == null) { return; }

            var psu = UnitFactory.GetPackageSafeUnit(SelectedProduct.Product.StdPackageUnit, SelectedProduct.Product.StdUnit, SelectedProduct.Product.StdFactor, SelectedProduct.Product.StdPackageUnit);
            var compatibleUnits = AgC.UnitConversion.CompatibilityFactory.GetCompatibleUnits(psu, SelectedProduct.Product.Density);

            /////////////////////////////set default units//////////////////////////////////////////////////////////////////////
            var defaultUnit = clientEndpoint.GetUserSettings().PreferredRateUnits.ContainsKey(SelectedProduct.ProductId.Id) ?
                (from u in compatibleUnits where u.Name == clientEndpoint.GetUserSettings().PreferredRateUnits[SelectedProduct.ProductId.Id] select u).FirstOrDefault() ?? psu
                : psu;
            var defaultTotalUnit = clientEndpoint.GetUserSettings().PreferredTotalUnits.ContainsKey(SelectedProduct.ProductId.Id) ?
                (from u in compatibleUnits where u.Name == clientEndpoint.GetUserSettings().PreferredTotalUnits[SelectedProduct.ProductId.Id] select u).FirstOrDefault() ?? psu
                : psu;

            var defaultRatePerTankUnit = clientEndpoint.GetUserSettings().PreferredRatePerTankUnits.ContainsKey(SelectedProduct.ProductId.Id) ?
                (from u in compatibleUnits where u.Name == clientEndpoint.GetUserSettings().PreferredRatePerTankUnits[SelectedProduct.ProductId.Id] select u).FirstOrDefault() ?? psu
                : psu;

            var prodSettingsId = new ProductSettingId(ApplicationEnvironment.CurrentDataSourceId, SelectedProduct.ProductId.Id);
            var prodSettings = clientEndpoint.GetView<ProductSettingsView>(prodSettingsId);
            if (prodSettings.HasValue) {
                var settings = prodSettings.Value;
                var productSettingResolver = clientEndpoint.GetProductUnitResolver();
                defaultUnit = string.IsNullOrEmpty(settings.RatePerAreaUnit) ? defaultUnit : productSettingResolver.GetPackageSafeUnit(SelectedProduct.ProductId, settings.RatePerAreaUnit); // UnitFactory.GetPackageSafeUnit(settings.RatePerAreaUnit, SelectedProduct.Product.StdUnit, SelectedProduct.Product.StdFactor, SelectedProduct.Product.StdPackageUnit);
                defaultTotalUnit = string.IsNullOrEmpty(settings.TotalUnit) ? defaultTotalUnit : productSettingResolver.GetPackageSafeUnit(SelectedProduct.ProductId, settings.TotalUnit); //UnitFactory.GetPackageSafeUnit(settings.TotalUnit, SelectedProduct.Product.StdUnit, SelectedProduct.Product.StdFactor, SelectedProduct.Product.StdPackageUnit);
                defaultRatePerTankUnit = string.IsNullOrEmpty(settings.RatePerTankUnit) ? defaultRatePerTankUnit : productSettingResolver.GetPackageSafeUnit(SelectedProduct.ProductId, settings.RatePerTankUnit); //UnitFactory.GetPackageSafeUnit(settings.RatePerTankUnit, SelectedProduct.Product.StdUnit, SelectedProduct.Product.StdFactor, SelectedProduct.Product.StdPackageUnit);
            }
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            foreach (var u in compatibleUnits) {
                if (DetermineDatasourceUnits.ShouldThisUnitShowInThisCulture(u.Name)) { 
                    AllowedTotalUnits.Add(u);
                var uh = new UnitPerAreaHelper(u, UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit));
                AllowedRateUnits.Add(uh);
                switch (ApplyBy) {
                    case ApplicationTypes.RatePerArea:
                    default:
                        if (u.Name == defaultUnit.Name) {
                            SelectedRatePerAreaUnitHelper = uh;
                        }
                        break;
                    case ApplicationTypes.RatePerTank:
                        if (u.Name == defaultRatePerTankUnit.Name) {
                            SelectedRatePerTankUnit = u;
                        }
                        break;
                    case ApplicationTypes.TotalProduct:
                        if (u.Name == defaultTotalUnit.Name) {
                            TotalProductUnit = u;
                        }
                        break;
                }
            }
            }

            // If we couldn't find it in the list while we were building it, pick the first one, if any.
            if (SelectedRatePerAreaUnitHelper == null && AllowedRateUnits.Any()) { SelectedRatePerAreaUnitHelper = AllowedRateUnits[0]; }

            // TODO: Make CostUnit configurable
            CostUnit = UnitFactory.GetPackageSafeUnit(SelectedProduct.Product.StdPackageUnit, SelectedProduct.Product.StdUnit, SelectedProduct.Product.StdFactor, SelectedProduct.Product.StdPackageUnit);
        }

        private void fieldSelectionModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e) {
            if (e.PropertyName == nameof(fieldSelectionModel.TotalArea)) {
                totalArea = fieldSelectionModel.TotalArea;

                RecalculateItem();

                // Call to Re-Calculate Associated Products
                foreach (var associate in AssociatedProducts)
                {
                    var associateProd = clientEndpoint.GetMasterlistService().GetProduct(associate.ProductID);
                    var ratePerArea = associate.RatePerAreaValue;
                    var ratePerAreaMeasure = UnitFactory.GetPackageSafeUnit(associate.RatePerAreaUnit.Name, associateProd.StdUnit, associateProd.StdFactor, associateProd.StdPackageUnit).GetMeasure((double)ratePerArea, associateProd.Density);
                    var totalValue = (double)ratePerArea * totalArea.Value * (double)ProductAreaPercent;
                    var totalMeasure = UnitFactory.GetPackageSafeUnit(associate.RatePerAreaUnit.Name, associateProd.StdUnit, associateProd.StdFactor, associateProd.StdPackageUnit).GetMeasure(totalValue, associateProd.Density);
                    var converted = totalMeasure.Unit != associate.TotalProductUnit && totalMeasure.CanConvertTo(associate.TotalProductUnit) ? totalMeasure.GetValueAs(associate.TotalProductUnit) : totalMeasure.Value;
                    associate.TotalProductValue = (decimal)converted;
                }
                /////////////////////////////////////////////
            }

            if (e.PropertyName == nameof(fieldSelectionModel.FirstAssociatedContract)) {
                var contractId = fieldSelectionModel.FirstAssociatedContract;

                if (contractId != null) {
                    if (Contract == null || (Contract != null && Contract.Id != contractId)) {
                        var contractMaybe = clientEndpoint.GetView<RentContractDetailsView>(fieldSelectionModel.FirstAssociatedContract);
                        contractMaybe.IfValue(c => { Contract = c; });
                    }
                } else {
                    Contract = null;
                }
            }
        }

        private void OnProductCreated(MiniProduct product) {
            var pli = parent.AddTemporaryMiniProduct(product, new ProductId(ApplicationEnvironment.CurrentDataSourceId, product.Id), true);
            SelectedProduct = pli;
            ResetContractSpecificInfo();
            //UpdatePercentFromGrowerInventory();
        }

        private ApplicationProductShareOwnerInformation getShareOwnerInformation() {
            if (TotalProductFromGrowerInventory == null) {
                return null;
            } else {
                //make sure TotalProductFromGrowerInventory is of compatible units
                var psu = UnitFactory.GetPackageSafeUnit(SelectedProduct.Product.StdPackageUnit, SelectedProduct.Product.StdUnit, SelectedProduct.Product.StdFactor, SelectedProduct.Product.StdPackageUnit);
                var compatibleUnits = CompatibilityFactory.GetCompatibleUnits(psu, SelectedProduct.Product.Density);
                if (compatibleUnits.Contains(TotalProductFromGrowerInventory.Unit))
                {
                    return new ApplicationProductShareOwnerInformation(GrowerSharePercent, (decimal)TotalProductFromGrowerInventory.Value, TotalProductFromGrowerInventory.Unit.Name);
                }
                else
                {
                    return null;
                }
            }
        }

        #region Product Entry Getters
        private ApplicationProductByArea toByAreaApplicationCommandProductEntry() {
            if (SelectedProduct == null || _ratePerAreaUnitHelper == null) { return null; }

            //ASSOCIATED PRODUCTS
            List<ApplicationAssociatedProduct> appAssociatedProducts = new List<ApplicationAssociatedProduct>();
            foreach (var associate in AssociatedProducts)
            {
                var appProd = new ApplicationAssociatedProduct();
                appProd.ProductId = associate.ProductID;
                appProd.ProductName = associate.ProductName;
                appProd.ManufacturerName = clientEndpoint.GetMasterlistService().GetProduct(associate.ProductID).Manufacturer;
                appProd.RatePerAreaValue = associate.RatePerAreaValue;
                appProd.RatePerAreaUnit = associate.RatePerAreaUnit.Name;
                appProd.TotalProductValue = associate.TotalProductValue;
                appProd.TotalProductUnit = associate.TotalProductUnit.Name;
                appProd.TrackingId = associate.TrackingID;
                appProd.CustomProductUnit = associate.CustomUnit.Name;
                appProd.CustomProductValue = associate.CustomRateValue;
                appProd.CustomRateType = associate.CustomRateType.ToString();
                appProd.HasCost = associate.HasCost;
                appProd.SpecificCostPerUnitValue = associate.HasCost && associate.SelectedPriceItem != null && associate.SelectedPriceItem.IsSystem == false ? associate.SelectedPriceItem.Price : 0m;
                appProd.SpecificCostPerUnitUnit = associate.HasCost && associate.SelectedPriceItem != null && associate.SelectedPriceItem.IsSystem == false ? associate.SelectedPriceItem.PriceUnit : string.Empty;
                appAssociatedProducts.Add(appProd);
            }
            //

            MiniPest pest = new MiniPest();
            if (SelectedPest != null) { pest = new MiniPest() { id = SelectedPest.PestId, Name = SelectedPest.CommonName, LatinList = SelectedPest.LatinName }; }
            return applicationCommandService.ToByAreaApplicationCommandProductEntry(
                AveragePriceItem,
                TotalGrowerCost,
                SelectedProduct.Product,
                SelectedProduct.ProductId,
                trackingId,
                _ratePerAreaValue,
                _ratePerAreaUnitHelper.Unit.Name,
                _ratePerAreaUnitHelper.AreaUnit.Name,
                ProductAreaPercent,
                (decimal)ProductArea.Value,
                ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit,
                getShareOwnerInformation(),
                SelectedProduct.Product.Density,
                ApplicationMethod,
                pest,
                appAssociatedProducts,
                SelectedPriceItem
            );
        }

        private ApplicationProductByTotal toByTotalApplicationCommandProductEntry() {
            if (SelectedProduct == null || _totalProductUnit == null || _ratePerAreaUnitHelper == null) { return null; }

            //ASSOCIATED PRODUCTS
            List<ApplicationAssociatedProduct> appAssociatedProducts = new List<ApplicationAssociatedProduct>();
            foreach (var associate in AssociatedProducts)
            {
                var appProd = new ApplicationAssociatedProduct();
                appProd.ProductId = associate.ProductID;
                appProd.ProductName = associate.ProductName;
                appProd.ManufacturerName = clientEndpoint.GetMasterlistService().GetProduct(associate.ProductID).Manufacturer;
                appProd.RatePerAreaValue = associate.RatePerAreaValue;
                appProd.RatePerAreaUnit = associate.RatePerAreaUnit.Name;
                appProd.TotalProductValue = associate.TotalProductValue;
                appProd.TotalProductUnit = associate.TotalProductUnit.Name;
                appProd.TrackingId = associate.TrackingID;
                appProd.CustomProductUnit = associate.CustomUnit.Name;
                appProd.CustomProductValue = associate.CustomRateValue;
                appProd.CustomRateType = associate.CustomRateType.ToString();
                appProd.HasCost = associate.HasCost;
                appProd.SpecificCostPerUnitValue = associate.HasCost && associate.SelectedPriceItem != null && associate.SelectedPriceItem.IsSystem == false ? associate.SelectedPriceItem.Price : 0m;
                appProd.SpecificCostPerUnitUnit = associate.HasCost && associate.SelectedPriceItem != null && associate.SelectedPriceItem.IsSystem == false ? associate.SelectedPriceItem.PriceUnit : string.Empty;
                appAssociatedProducts.Add(appProd);
            }
            //

            MiniPest pest = new MiniPest();
            if (SelectedPest != null) { pest = new MiniPest() { id = SelectedPest.PestId, Name = SelectedPest.CommonName, LatinList = SelectedPest.LatinName }; }

            return applicationCommandService.ToByTotalApplicationCommandProductEntry(
                AveragePriceItem,
                TotalGrowerCost,
                SelectedProduct.Product,
                SelectedProduct.ProductId,
                trackingId,
                _totalProductValue,
                _totalProductUnit.Name,
                _ratePerAreaUnitHelper.AreaUnit.Name,
                ProductAreaPercent,
                (decimal)ProductArea.Value,
                ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit,
                getShareOwnerInformation(),
                SelectedProduct.Product.Density,
                ApplicationMethod,
                pest,
                appAssociatedProducts,
                SelectedPriceItem
            );
        }

        private ApplicationProductByTank toByTankApplicationCommandProductEntry() {
            if (SelectedProduct == null || _ratePerTankUnit == null || _ratePerAreaUnitHelper == null) { return null; }

            //ASSOCIATED PRODUCTS
            List<ApplicationAssociatedProduct> appAssociatedProducts = new List<ApplicationAssociatedProduct>();
            foreach (var associate in AssociatedProducts)
            {
                var appProd = new ApplicationAssociatedProduct();
                appProd.ProductId = associate.ProductID;
                appProd.ProductName = associate.ProductName;
                appProd.ManufacturerName = clientEndpoint.GetMasterlistService().GetProduct(associate.ProductID).Manufacturer;
                appProd.RatePerAreaValue = associate.RatePerAreaValue;
                appProd.RatePerAreaUnit = associate.RatePerAreaUnit.Name;
                appProd.TotalProductValue = associate.TotalProductValue;
                appProd.TotalProductUnit = associate.TotalProductUnit.Name;
                appProd.TrackingId = associate.TrackingID;
                appProd.CustomProductUnit = associate.CustomUnit.Name;
                appProd.CustomProductValue = associate.CustomRateValue;
                appProd.CustomRateType = associate.CustomRateType.ToString();
                appProd.HasCost = associate.HasCost;
                appProd.SpecificCostPerUnitValue = associate.HasCost && associate.SelectedPriceItem != null && associate.SelectedPriceItem.IsSystem == false ? associate.SelectedPriceItem.Price : 0m;
                appProd.SpecificCostPerUnitUnit = associate.HasCost && associate.SelectedPriceItem != null && associate.SelectedPriceItem.IsSystem == false ? associate.SelectedPriceItem.PriceUnit : string.Empty;
                appAssociatedProducts.Add(appProd);
            }
            //

            MiniPest pest = new MiniPest();
            if (SelectedPest != null) { pest = new MiniPest() { id = SelectedPest.PestId, Name = SelectedPest.CommonName, LatinList = SelectedPest.LatinName }; }

            return applicationCommandService.ToByTankApplicationCommandProductEntry(
                AveragePriceItem,
                TotalGrowerCost,
                SelectedProduct.Product,
                SelectedProduct.ProductId,
                trackingId,
                _ratePerTankValue,
                _ratePerTankUnit.Name,
                _ratePerAreaUnitHelper.AreaUnit.Name,
                ProductAreaPercent,
                (decimal)ProductArea.Value,
                ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit,
                getShareOwnerInformation(),
                SelectedProduct.Product.Density,
                ApplicationMethod,
                pest,
                appAssociatedProducts,
                SelectedPriceItem
            );
        }
        #endregion

        private void showCreateUserProductOverlay() {
            IsProductListVisible = false;
            IsCreateProductPopupOpen = false;

            var vm = new UserProductCreatorViewModel(clientEndpoint, ProductSearchText, OnProductCreated);
            Messenger.Default.Send(new ShowOverlayMessage() {
                ScreenDescriptor = new ScreenDescriptor(typeof(Views.Secondary.UserCreatedProduct.UserProductCreatorView), "createProduct", vm)
            });
        }

        private void changePreferredRateUnit() {
            if (SelectedRatePerAreaUnitHelper == null || SelectedRatePerAreaUnitHelper.Unit == null) { return; } // Prevents a crash when the user hasn't selected a product yet.

            var units = from u in AllowedRateUnits
                        select u.Unit;

            var vm = new ChangePreferredUnitViewModel(units.ToList(), SelectedRatePerAreaUnitHelper.Unit.Name, selectedUnit => {
                if (selectedUnit != null) {

                    var prodsettingMaybe = clientEndpoint.GetView<ProductSettingsView>(new ProductSettingId(ApplicationEnvironment.CurrentDataSourceId, SelectedProduct.ProductId.Id));
                    if (prodsettingMaybe.HasValue) {
                        var value = prodsettingMaybe.Value;
                        var ratePerTank = value.RatePerTankUnit;
                        var ratePer100 = value.RatePer100Unit;
                        var total = value.TotalUnit;

                        if (prodsettingMaybe.Value == null || selectedUnit.Name != prodsettingMaybe.Value.RatePerAreaUnit) {
                            var productCommand = new UpdateProductDefaultUnits(new ProductSettingId(ApplicationEnvironment.CurrentDataSourceId, SelectedProduct.ProductId), clientEndpoint.GenerateNewMetadata(), selectedUnit.Name, ratePer100, ratePerTank, total);
                            clientEndpoint.SendOne(productCommand);
                        }
                    } else {
                        var productCommand = new UpdateProductDefaultUnits(new ProductSettingId(ApplicationEnvironment.CurrentDataSourceId, SelectedProduct.ProductId), clientEndpoint.GenerateNewMetadata(), selectedUnit.Name, string.Empty, string.Empty, string.Empty);
                        clientEndpoint.SendOne(productCommand);

                    }

                    //RecalculateItem();
                    var mlp = clientEndpoint.GetMasterlistService().GetProduct(SelectedProduct.ProductId);
                    var rateMeasure = SelectedRatePerAreaUnitHelper.Unit.GetMeasure((double)RatePerAreaValue, mlp.Density);
                    var converted = rateMeasure.CreateAs(selectedUnit);
                    RatePerAreaValue = (decimal)converted.Value;
                    SelectedRatePerAreaUnitHelper = (from r in AllowedRateUnits where r.Unit.Name == selectedUnit.Name select r).FirstOrDefault();
                }
            });

            Messenger.Default.Send(new ShowPopupMessage() {
                ScreenDescriptor = new ScreenDescriptor(typeof(Views.Shared.Popups.ChangePreferredUnitView), "changeUnit", vm)
            });
        }

        private void changePreferredTotalUnit() {
            if (SelectedRatePerAreaUnitHelper != null) {
                var vm = new ChangePreferredUnitViewModel(AllowedTotalUnits, SelectedRatePerAreaUnitHelper.Unit.Name, selectedUnit => {
                    if (selectedUnit != null) {
                        var prodsettingMaybe = clientEndpoint.GetView<ProductSettingsView>(new ProductSettingId(ApplicationEnvironment.CurrentDataSourceId, SelectedProduct.ProductId.Id));
                        if (prodsettingMaybe.HasValue) {
                            var value = prodsettingMaybe.Value;
                            var ratePerTank = value.RatePerTankUnit;
                            var ratePer100 = value.RatePer100Unit;
                            var ratePerArea = value.RatePerAreaUnit;

                            if (prodsettingMaybe.Value == null || selectedUnit.Name != prodsettingMaybe.Value.TotalUnit) {
                                var productCommand = new UpdateProductDefaultUnits(new ProductSettingId(ApplicationEnvironment.CurrentDataSourceId, SelectedProduct.ProductId), clientEndpoint.GenerateNewMetadata(), ratePerArea, ratePer100, ratePerTank, selectedUnit.Name);
                                clientEndpoint.SendOne(productCommand);
                            }
                        } else {
                            var productCommand = new UpdateProductDefaultUnits(new ProductSettingId(ApplicationEnvironment.CurrentDataSourceId, SelectedProduct.ProductId), clientEndpoint.GenerateNewMetadata(), string.Empty, string.Empty, string.Empty, selectedUnit.Name);
                            clientEndpoint.SendOne(productCommand);

                        }

                        //RecalculateItem();
                        var mlp = clientEndpoint.GetMasterlistService().GetProduct(SelectedProduct.ProductId);
                        var totalMeasure = TotalProductUnit.GetMeasure((double)TotalProductValue, mlp.Density);
                        var converted = totalMeasure.CreateAs(selectedUnit);
                        TotalProductValue = (decimal)converted.Value;
                        TotalProductUnit = (from r in AllowedTotalUnits where r.Name == selectedUnit.Name select r).FirstOrDefault();
                    }
                });

                Messenger.Default.Send(new ShowPopupMessage() {
                    ScreenDescriptor = new ScreenDescriptor(typeof(Views.Shared.Popups.ChangePreferredUnitView), "changeUnit", vm)
                });
            }
        }

        private void showSpecialityCalculatorPopup() {
            if (SelectedProduct != null) {
                var calcVM = new AppliedProductCalculatorViewModel(clientEndpoint, this, parent.TankInfo);
                Messenger.Default.Send(new ShowPopupMessage() {
                    ScreenDescriptor = new ScreenDescriptor(typeof(Views.Secondary.PopUp.AppliedProductSpecialityCalculator), "calculator", calcVM)
                });
            }
        }

        private void calculateTotalProductFromGrowerInventory() {
            if (TotalProductUnit != null && !(TotalProductUnit is NullUnit) && SelectedProduct != null) {
                var total = TotalProductValue * PercentFromGrowerInventory;
                TotalProductFromGrowerInventory = TotalProductUnit.GetMeasure((double)total, SelectedProduct.Product.Density);
            }
        }

        private AssociatedProductItem _associatedProduct { get; set; }
        public AssociatedProductItem AssociatedProduct
        {
            get { return _associatedProduct; }
            set
            {
                _associatedProduct = value;
                RaisePropertyChanged(() => AssociatedProduct);
            }
        }

        public ObservableCollection<AssociatedProductItem> AssociatedProducts { get; set; }

        private void AssociateSeedTreatmentProduct()
        {
            var ratePerArea = SelectedRatePerAreaUnitHelper.Unit.GetMeasure((double)RatePerAreaValue, SelectedProduct.Product.Density);
            var calcVM = new AssociatedSeedTreatmentViewModel(clientEndpoint, ratePerArea, ProductArea, SelectedProduct.Product, AddSiblingProduct);
            Messenger.Default.Send(new ShowPopupMessage()
            {
                ScreenDescriptor = new ScreenDescriptor(typeof(Views.Secondary.PopUp.SeedTreatmentProductAssociationCalculator), "calculator", calcVM)
            });
        }

        public bool HasAssociatedProducts { get; set; }

        public void AddSiblingProduct(AssociatedSeedTreatmentViewModel siblingVM)
        {
            decimal customRateValue = 0m;
            IUnit customUnit;
            AssociatedProductRateType rateType;
            var tab = siblingVM.TabItem == null ? "PerSeedTab" : siblingVM.TabItem.Name;
            switch (tab)
            {
                case "PerSeedTab":
                    customRateValue = siblingVM.MGActivePerSeedValue;
                    var composite2 = new CompositeUnit(siblingVM.SelectedRatePerSeedUnit.AbbreviatedDisplay, 1, SelectedProduct.Product.StdUnit);
                    string text2 = composite2.AbbreviatedDisplay;
                    customUnit = siblingVM.SelectedRatePerSeedUnit;
                    rateType = AssociatedProductRateType.BySeed;
                    break;
                case "PerBagTab":
                    customRateValue = siblingVM.RatePerBagValue;
                    customUnit = siblingVM.SelectedRatePerBagUnit;
                    rateType = AssociatedProductRateType.ByBag;
                    break;
                case "PerCwtTab":
                    customRateValue = siblingVM.RatePerCWTValue;
                    customUnit = siblingVM.SelectedCWTUnit;
                    rateType = AssociatedProductRateType.ByCWT;
                    break;
                case "RowTab":
                    customRateValue = siblingVM.RatePer1000Value;
                    customUnit = siblingVM.SelectedRatePer1000FtRowHelper;
                    rateType = AssociatedProductRateType.ByRow;
                    break;
                default:
                    customRateValue = siblingVM.MGActivePerSeedValue;
                    customUnit = siblingVM.SelectedRatePerSeedUnit;
                    rateType = AssociatedProductRateType.BySeed;
                    break;
            }

            AssociatedProduct = new AssociatedProductItem(clientEndpoint, SelectedProduct.Product, RemoveAssociatedProduct)
            {
                RatePerAreaValue = siblingVM.RatePerAreaValue,
                RatePerAreaUnit = siblingVM.SelectedRatePerAreaUnitHelper.Unit,
                TotalProductValue = siblingVM.TotalProductValue,
                TotalProductUnit = siblingVM.SelectedTotalProductUnit,
                HasCost = false,
                ProductID = siblingVM.SelectedProduct.ProductId,
                TrackingID = Guid.NewGuid(),
                ProductName = siblingVM.SelectedProduct.Product.Name,
                CustomRateValue = customRateValue,
                CustomUnit = customUnit,
                CustomRateType = rateType
            };

            AssociatedProducts.Add(AssociatedProduct);
            HasAssociatedProducts = true;

            RaisePropertyChanged(() => AssociatedProduct);
            RaisePropertyChanged(() => HasAssociatedProducts);
        }

        public void RemoveAssociatedProduct(Guid associatedProductTrackingID)
        {
            var item = AssociatedProducts.SingleOrDefault(x => x.TrackingID == associatedProductTrackingID);
            AssociatedProducts.Remove(item);
            RaisePropertyChanged(() => AssociatedProducts);

            if (!AssociatedProducts.Any())
            {
                HasAssociatedProducts = false;
                RaisePropertyChanged(() => HasAssociatedProducts);
            }
        }

        void ReCaculateAssociatedProducts()
        {
            foreach(var associate in AssociatedProducts)
            {
                var associateProd = clientEndpoint.GetMasterlistService().GetProduct(associate.ProductID);

                if(associate.CustomRateType == AssociatedProductRateType.BySeed)
                {
                    var totalPercentAI = associateProd.ActiveIngredients.Sum(x => x.Percent);
                    var totalProdPerSeed = associate.CustomRateValue / totalPercentAI;

                    //TO DO :: MAKE SURE RATE IS IN SEED OR KERNEL OTHERWISE TRY TO CONVERT
                    var rateVal = RatePerAreaValue;
                    var parentProd = SelectedProduct.Product;
                    var parentStdUnit = parentProd.StdUnit;
                    if (parentStdUnit == "seed" || parentStdUnit == "kernel")
                    {
                        if (SelectedRatePerAreaUnitHelper.Unit.Name == parentStdUnit) { }
                        else
                        {
                            var unit = UnitFactory.GetPackageSafeUnit(parentStdUnit, parentStdUnit, parentProd.StdFactor, parentProd.StdPackageUnit);
                            var parentRatePerArea = SelectedRatePerAreaUnitHelper.Unit.GetMeasure((double)rateVal, SelectedProduct.Product.Density);
                            rateVal = parentRatePerArea.CanConvertTo(unit) ? (decimal)parentRatePerArea.GetValueAs(unit) : (decimal)parentRatePerArea.Value;
                        }
                    }
                    else { return; }

                    
                    var stdUnit = UnitFactory.GetPackageSafeUnit(associate.RatePerAreaUnit.Name, associateProd.StdUnit, associateProd.StdFactor, associateProd.StdPackageUnit);
                    var packageUnit = associate.TotalProductUnit;
                    var totalSeedTreatmentPerAreaValue = totalProdPerSeed * rateVal;
                    var associateSelectedRatePerSeedUnit = UnitFactory.GetUnitByName("milligram");
                    var totalSeedTreatmentPerArea =  associateSelectedRatePerSeedUnit.GetMeasure((double)totalSeedTreatmentPerAreaValue, associateProd.Density);

                    var convertedSeedTreatmentPerArea = totalSeedTreatmentPerArea.Unit != stdUnit && totalSeedTreatmentPerArea.CanConvertTo(stdUnit) ? totalSeedTreatmentPerArea.CreateAs(stdUnit) : totalSeedTreatmentPerArea;
                    var associateRatePerAreaValue = (decimal)convertedSeedTreatmentPerArea.Value;

                    var totalValue = associateRatePerAreaValue * (decimal)ProductArea.Value;
                    var totalMeasure = stdUnit.GetMeasure((double)totalValue, SelectedProduct.Product.Density);
                    var convertedTotalProductValue = totalMeasure.Unit != packageUnit && totalMeasure.CanConvertTo(packageUnit) ? totalMeasure.GetValueAs(packageUnit) : totalMeasure.Value;
                    var associateTotalProductValue = (decimal)convertedTotalProductValue;

                    associate.RatePerAreaValue = associateRatePerAreaValue;
                    associate.TotalProductValue = associateTotalProductValue;
                }
                else if(associate.CustomRateType == AssociatedProductRateType.ByBag)
                {
                    var rateVal = RatePerAreaValue;
                    var parentProd = SelectedProduct.Product;

                    if (SelectedRatePerAreaUnitHelper.Unit.Name == parentProd.StdPackageUnit) { }
                    else
                    {
                        var unit = UnitFactory.GetPackageSafeUnit(parentProd.StdPackageUnit, parentProd.StdUnit, parentProd.StdFactor, parentProd.StdPackageUnit);
                        var parentRatePerArea = SelectedRatePerAreaUnitHelper.Unit.GetMeasure((double)rateVal, SelectedProduct.Product.Density);
                        rateVal = parentRatePerArea.CanConvertTo(unit) ? (decimal)parentRatePerArea.GetValueAs(unit) : (decimal)parentRatePerArea.Value;
                    }

                    var stdUnit = SelectedRatePerAreaUnitHelper.Unit;
                    var packageUnit = associate.TotalProductUnit;

                    var ratePerAreaValue = rateVal * associate.CustomRateValue;
                    var ratePerBag = associate.CustomUnit;
                    var ratePerArea =  ratePerBag.GetMeasure((double)ratePerAreaValue, associateProd.Density);
                    var associateRatePerArea = ratePerArea.Unit != associate.RatePerAreaUnit && ratePerArea.CanConvertTo(associate.RatePerAreaUnit) ? (decimal)ratePerArea.GetValueAs(associate.RatePerAreaUnit) : (decimal)ratePerArea.Value;

                    var totalValue = associateRatePerArea * (decimal)ProductArea.Value;
                    var totalMeasure = stdUnit.GetMeasure((double)totalValue, SelectedProduct.Product.Density);
                    var convertedTotalProductValue = totalMeasure.Unit != packageUnit && totalMeasure.CanConvertTo(packageUnit) ? totalMeasure.GetValueAs(packageUnit) : totalMeasure.Value;
                    var associateTotalProdValue = (decimal)convertedTotalProductValue;

                    associate.TotalProductValue = associateTotalProdValue;
                    associate.RatePerAreaValue = associateRatePerArea;
                }
                else if (associate.CustomRateType == AssociatedProductRateType.ByCWT)
                {
                    var rateVal = RatePerAreaValue;
                    var parentProd = SelectedProduct.Product;
                    if (parentProd.StdPackageUnit == "bag" && parentProd.StdFactor == 100)
                    {
                        if (SelectedRatePerAreaUnitHelper.Unit.Name == parentProd.StdPackageUnit) { }
                        else
                        {
                            var unit = UnitFactory.GetPackageSafeUnit(parentProd.StdPackageUnit, parentProd.StdUnit, parentProd.StdFactor, parentProd.StdPackageUnit);
                            var parentRatePerArea = SelectedRatePerAreaUnitHelper.Unit.GetMeasure((double)rateVal, SelectedProduct.Product.Density);
                            rateVal = parentRatePerArea.CanConvertTo(unit) ? (decimal)parentRatePerArea.GetValueAs(unit) : (decimal)parentRatePerArea.Value;
                        }
                    }
                    else if (parentProd.StdPackageUnit == "bag" && parentProd.StdUnit == "pound")
                    {
                        var multiplier = parentProd.StdFactor / 100;
                        var unit = UnitFactory.GetPackageSafeUnit(parentProd.StdPackageUnit, parentProd.StdUnit, parentProd.StdFactor, parentProd.StdPackageUnit);
                        var parentRatePerArea = SelectedRatePerAreaUnitHelper.Unit.GetMeasure((double)rateVal, SelectedProduct.Product.Density);
                        rateVal = parentRatePerArea.CanConvertTo(unit) ? (decimal)parentRatePerArea.GetValueAs(unit) : (decimal)parentRatePerArea.Value;
                        rateVal = rateVal * (decimal)multiplier;
                    }
                    else { return; }

                    var stdUnit = associate.RatePerAreaUnit;
                    var packageUnit = associate.TotalProductUnit;

                    var ratePerAreaValue = rateVal * associate.CustomRateValue;
                    var ratePerArea = associate.CustomUnit.GetMeasure((double)ratePerAreaValue, associateProd.Density);
                    associate.RatePerAreaValue = ratePerArea.Unit != associate.RatePerAreaUnit && ratePerArea.CanConvertTo(associate.RatePerAreaUnit) ? (decimal)ratePerArea.GetValueAs(associate.RatePerAreaUnit) : (decimal)ratePerArea.Value;

                    var totalValue = associate.RatePerAreaValue * (decimal)ProductArea.Value;
                    var totalMeasure = stdUnit.GetMeasure((double)totalValue, associateProd.Density);
                    var convertedTotalProductValue = totalMeasure.Unit != associate.TotalProductUnit && totalMeasure.CanConvertTo(associate.TotalProductUnit) ? totalMeasure.GetValueAs(associate.TotalProductUnit) : totalMeasure.Value;
                    associate.TotalProductValue = (decimal)convertedTotalProductValue;
                }
                else if (associate.CustomRateType == AssociatedProductRateType.ByRow)
                {
                    //DO NOTHING ???
                }
                else
                {

                }
            }

            RaisePropertyChanged(() => AssociatedProducts);
        }

		public override string ToString() {
			// enhance this more if you need it
			return DisplayName;
		}

		#region Custom Validation
		public static ValidationResult ValidateSelectedProduct(ProductListItemDetails product, ValidationContext context) {
			ValidationResult result = null;

			if (product == null) {
				result = new ValidationResult(Strings.ProductIsRequired_Text);
			}

			return result;
		}

		public static ValidationResult ValidateRate(decimal rate, ValidationContext context) {
			ValidationResult result = null;

			var vm = (AppliedProductViewModel)context.ObjectInstance;

			var validateRatePerArea = vm.ApplyBy == ApplicationTypes.RatePerArea && context.MemberName == nameof(RatePerAreaValue);
			var validateRatePerTank = vm.ApplyBy == ApplicationTypes.RatePerTank && context.MemberName == nameof(RatePerTankValue);
			var validateTotalProduct = vm.ApplyBy == ApplicationTypes.TotalProduct && context.MemberName == nameof(TotalProductValue);

			var shouldValidate = validateRatePerArea || validateRatePerTank || validateTotalProduct;

			if (shouldValidate && rate == 0) {
				result = new ValidationResult(Strings.RateCannotBeZero_Text);
			}

			return result;
		}

		public static ValidationResult ValidateUnit(object unit, ValidationContext context) {
			ValidationResult result = null;

			var vm = (AppliedProductViewModel)context.ObjectInstance;

			var validateRatePerArea = vm.ApplyBy == ApplicationTypes.RatePerArea && context.MemberName == nameof(SelectedRatePerAreaUnitHelper);
			var validateRatePerTank = vm.ApplyBy == ApplicationTypes.RatePerTank && context.MemberName == nameof(SelectedRatePerTankUnit);
			var validateTotalProduct = vm.ApplyBy == ApplicationTypes.TotalProduct && context.MemberName == nameof(TotalProductUnit);

			var shouldValidate = validateRatePerArea || validateRatePerTank || validateTotalProduct;

			if (shouldValidate && unit == null) {
				result = new ValidationResult(Strings.ValidationResult_InvalidUnit_Text);
			}

			return result;
		}

		public static ValidationResult ValidateSelectedPest(PestListItemDetails selectedPest, ValidationContext context) {
			ValidationResult result = null;

			var vm = (AppliedProductViewModel)context.ObjectInstance;

			if (!string.IsNullOrWhiteSpace(vm.PestSearchText) && selectedPest == null) {
				result = new ValidationResult(Strings.InvalidPestSelected_Text);
			}

			return result;
		}
		#endregion
	}

    public enum AssociatedProductRateType
    {
        BySeed,
        ByBag,
        ByRow,
        ByCWT
    }
}