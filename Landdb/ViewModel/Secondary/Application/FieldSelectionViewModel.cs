﻿using AgC.UnitConversion;
using AgC.UnitConversion.Area;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Contract;
using Landdb.Domain.ReadModels.Invoice;
using Landdb.Domain.ReadModels.Map;
using Landdb.Domain.ReadModels.Plan;
using Landdb.Domain.ReadModels.Recommendation;
using Landdb.Domain.ReadModels.WorkOrder;
using Landdb.ViewModel.Fields;
using Landdb.ViewModel.Shared;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows.Markup;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Secondary.Application {
	public class FieldSelectionViewModel : BaseFieldSelectionViewModel {

		// TODO: Factor this dependency out. Prefer a method lamdba for notification of field selection perhaps?
		readonly ClearAgWeatherConditionsViewModel conditionsModel;
		readonly CropZoneRentContractView czrcView;
		readonly bool areFieldSelectionsRequired;

		List<CropZoneId> siteDataCropZones;
		private List<CropZoneRentContract> associatedContracts;

		public FieldSelectionViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, CropYearId currentCropYearId, ClearAgWeatherConditionsViewModel conditionsModel, bool areFieldSelectionsRequired)
			: base(clientEndpoint, dispatcher, currentCropYearId.Id) {

			this.conditionsModel = conditionsModel;

			// this could be done so much better, i think. 
			// split this vm into vms specific to applicator, and planner, and workorder creator like we do with rec creator?
			this.areFieldSelectionsRequired = areFieldSelectionsRequired;

			czrcView = clientEndpoint.GetView<CropZoneRentContractView>(currentCropYearId).GetValue(new CropZoneRentContractView());

			associatedContracts = new List<CropZoneRentContract>();
			siteDataCropZones = new List<CropZoneId>();

			ValidateViewModel();
		}

		private InvoiceView associatedInvoiceView;
		public InvoiceView AssociatedInvoiceView {
			get { return associatedInvoiceView; }
			set {
				associatedInvoiceView = value;
				RaisePropertyChanged(() => AssociatedInvoiceView);
			}
		}

		private WorkOrderView associatedWorkOrderView;
		public WorkOrderView AssociatedWorkOrderView {
			get { return associatedWorkOrderView; }
			set {
				associatedWorkOrderView = value;

				if (associatedWorkOrderView != null) {
					var czIds = from cz in associatedWorkOrderView.CropZones
								select cz.Id;

					RootTreeItemModels.First().CheckMatches(x => {
						return x is CropZoneTreeItemViewModel && czIds.Contains((CropZoneId)((CropZoneTreeItemViewModel)x).Id);
					});

					foreach (var scz in SelectedCropZones) {
						// match up areas
						var q = from cz in associatedWorkOrderView.CropZones
								where cz.Id == scz.Id
								select cz;
						var sourceCz = q.SingleOrDefault();
						if (sourceCz != null) {
							scz.ChangeSelectedArea(sourceCz.Area);
						}
					}
				}

				RaisePropertyChanged(() => AssociatedWorkOrderView);
			}
		}

		private RecommendationView associatedRecommendationView;
		public RecommendationView AssociatedRecommendationView {
			get { return associatedRecommendationView; }
			set {
				associatedRecommendationView = value;

				if (associatedRecommendationView != null) {
					var czIds = from cz in associatedRecommendationView.CropZones
								select cz.Id;

					RootTreeItemModels.First().CheckMatches(x => {
						return x is CropZoneTreeItemViewModel && czIds.Contains((CropZoneId)((CropZoneTreeItemViewModel)x).Id);
					});

					foreach (var scz in SelectedCropZones) {
						// match up areas
						var q = from cz in associatedRecommendationView.CropZones
								where cz.Id == scz.Id
								select cz;
                        RecommendedCropZone sourceCz = new RecommendedCropZone();

                        //DOING THIS TO HANDLE ODD DATA COMING FROM AGRAIN
                        if (q.Count() > 1)
                        {
                            sourceCz = q.FirstOrDefault();
                            var areaUnit = sourceCz.Area.Unit;
                            var areaMeasure = areaUnit.GetMeasure(q.Sum(x => x.Area.Value)) as AreaMeasure;
                            sourceCz.Area = areaMeasure;
                        }
                        else
                        {
                            sourceCz = q.FirstOrDefault();
                        }

						if (sourceCz != null) {
							scz.ChangeSelectedArea(sourceCz.Area);
						}
					}
				}

				RaisePropertyChanged(() => AssociatedRecommendationView);
			}
		}

		private PlanView associatedPlanView;
		public PlanView AssociatedPlanView {
			get { return associatedPlanView; }
			set {
				associatedPlanView = value;

				if (associatedPlanView != null) {
					var czIds = from cz in associatedPlanView.CropZones
								select cz.Id;

					RootTreeItemModels.First().CheckMatches(x => {
						return x is CropZoneTreeItemViewModel && czIds.Contains((CropZoneId)((CropZoneTreeItemViewModel)x).Id);
					});

					foreach (var scz in SelectedCropZones) {
						// match up areas
						var q = from cz in associatedPlanView.CropZones
								where cz.Id == scz.Id
								select cz;
						var sourceCz = q.SingleOrDefault();
						if (sourceCz != null) {
							scz.ChangeSelectedArea(sourceCz.Area);
						}
					}
				}

				RaisePropertyChanged(() => AssociatedPlanView);
			}
		}

		//TO DO :: FIX THIS FOR SITE DATA
		public List<CropZoneId> SiteDataCropZones {
			get { return siteDataCropZones; }
			set {
				siteDataCropZones = value;

				RootTreeItemModels.First().CheckMatches(x => {
					return x is CropZoneTreeItemViewModel && siteDataCropZones.Contains((CropZoneId)((CropZoneTreeItemViewModel)x).Id);
				});
			}
		}

		protected override void OnFieldCheckedChanged(CropZoneTreeItemViewModel treeItem) {
			if (treeItem is CropZoneTreeItemViewModel) {
				if (treeItem.IsChecked.HasValue && treeItem.IsChecked.Value) {
					if (SelectedCropZones.Count == 0 && conditionsModel != null) {
                        var mapItem = clientEndpoint.GetView<ItemMap>(treeItem.CropZoneId);
                        string mapData = null;
                        if (mapItem.HasValue && mapItem.Value.MostRecentMapItem != null)
                        {
                            mapData = mapItem.Value.MostRecentMapItem.MapData;
                        }
                        if (!string.IsNullOrWhiteSpace(mapData) && mapData != "MULTIPOLYGON EMPTY")
                        {
                            var shape = ThinkGeo.MapSuite.Core.PolygonShape.CreateShapeFromWellKnownData(mapData);
                            var center = shape.GetCenterPoint();
                            //conditionsModel.GeoLookupPoint = center; // string.Format("{0},{1}", center.Y.ToString("n1"), center.X.ToString("n1"));
                        }
					}

					if (czrcView.CropZoneRentContracts.ContainsKey(treeItem.CropZoneId.Id)) {
						var czrc = czrcView.CropZoneRentContracts[treeItem.CropZoneId.Id];

						if (czrc != null && czrc.RentContractId != null) {
							associatedContracts.Add(czrc);
						} else {
							// add fake contract with null id to warn when users mix contracted fields with uncontracted fields
							associatedContracts.Add(new CropZoneRentContract() { CropZoneId = treeItem.CropZoneId });
						}
					} else {
						// add fake contract with null id to warn when users mix contracted fields with uncontracted fields
						associatedContracts.Add(new CropZoneRentContract() { CropZoneId = treeItem.CropZoneId });
					}
				}

				if (treeItem.IsChecked.HasValue && !treeItem.IsChecked.Value) {
					associatedContracts.RemoveAll(x => x.CropZoneId == treeItem.CropZoneId);
				}

				RaisePropertyChanged(() => TotalCostLabelText);
				RaisePropertyChanged(() => FirstAssociatedContract);

				ValidateViewModel();
			}
		}

		private Measure selectedArea;
		[DependsOn("TotalArea")]
		public Measure TotalSelectedArea {
			get { return selectedArea; }
			set {
				selectedArea = value;
				ValidateAndRaisePropertyChanged(() => TotalSelectedArea);
			}
		}

		public string TotalCostLabelText {
			get { return associatedContracts.Any() ? $"{Strings.TotalGrowerCost_Text}:" : $"{Strings.TotalCost_Text}:"; }
		}

		public IEnumerable<ContractId> SortedAssociatedContracts {
			get {
				return from c in associatedContracts
					   group c by c.RentContractId into g
					   orderby g.Count() descending
					   select g.Key;
			}
		}

		public ContractId FirstAssociatedContract {
			get { return SortedAssociatedContracts.FirstOrDefault(); }
		}

		public bool AreInputSharesEqual() {
			var groupedContracts = from c in associatedContracts
								   group c by new { c.GrowerCropProtShare, c.GrowerFertShare, c.GrowerServiceShare, c.GrowerSeedShare } into g
								   select g.Key;

			return groupedContracts.Count() <= 1;
		}

		internal override void ResetData() {
			associatedContracts.Clear();

			RaisePropertyChanged(() => FirstAssociatedContract);
			RaisePropertyChanged(() => TotalCostLabelText);

			base.ResetData();
		}

		#region Custom Validation
		public static ValidationResult ValidateSelectedCropZones(ObservableCollection<CropZoneSelectionViewModel> selectedCropZones, ValidationContext context) {
			ValidationResult result = null;

			var vm = (FieldSelectionViewModel)context.ObjectInstance;

			if (vm.areFieldSelectionsRequired && !selectedCropZones.Any()) {
				result = new ValidationResult(Strings.AtLeastOneCropZoneIsRequired_Text);
			} else if (selectedCropZones.Any(x => x.SelectedArea.Value == 0)) {
				result = new ValidationResult(Strings.AtLeastOneSelectedCropZoneHasNoArea_Text);
			} else if (!vm.AreInputSharesEqual()) {
				result = new ValidationResult(Strings.WarningMultipleContractsSelected_Text + Strings.VariancesInInputSharingBetweenContractsCouldLeadToIncorrectData_Text);
			}

			return result;
		}

		[CustomValidation(typeof(FieldSelectionViewModel), nameof(ValidateSelectedCropZones))]
		public override ObservableCollection<CropZoneSelectionViewModel> SelectedCropZones {
			get { return base.SelectedCropZones; }
			protected set { base.SelectedCropZones = value; }
		}
		#endregion
	}
}