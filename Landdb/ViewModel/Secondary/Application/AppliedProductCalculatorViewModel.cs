﻿using AgC.UnitConversion;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.ProductSetting;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Secondary.Plan;
using Landdb.ViewModel.Secondary.Recommendation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Landdb.ViewModel.Secondary.Application
{
    public class AppliedProductCalculatorViewModel : ViewModelBase
    {
        TankInformationModel tankInfo;

        AppliedProductViewModel productVM;
        RecommendedProductViewModel recProductVM;

        // Rate per area values
        decimal ratePerAreaValue;
        UnitPerAreaHelper ratePerAreaUnitHelper;

        // Rate per 100 values
        decimal ratePer100Value;
        IUnit ratePer100UnitHelper;

        // Total product values
        decimal totalProductValue;
        IUnit totalProductUnit;

        // Rate per tank values
        decimal ratePerTankValue;
        IUnit ratePerTankUnit;

        decimal areaPerGallonValue;

        TabItem item;
        int tabIndex = 0;

        IClientEndpoint endpoint;
        IMasterlistService masterlist;
        public AppliedProductCalculatorViewModel(IClientEndpoint endpoint, AppliedProductViewModel productVM, TankInformationModel tankInfo)
        {
            this.endpoint = endpoint;
            this.productVM = productVM;
            this.tankInfo = tankInfo;

            masterlist = endpoint.GetMasterlistService();
            var mlp = masterlist.GetProduct(productVM.SelectedProduct.ProductId);
            var prodSettingsMaybe = endpoint.GetView<ProductSettingsView>(new ProductSettingId(ApplicationEnvironment.CurrentDataSourceId, mlp.Id));
            SelectedMasterlistProduct = mlp;
            ProductAreaValue = productVM.ProductArea.Value;
            ProductAreaUnit = productVM.ProductArea.Unit;

            AllowedRateUnits = productVM.AllowedRateUnits;
            AllowedTotalUnits = productVM.AllowedTotalUnits;           
            SelectedRatePerAreaUnitHelper = productVM.SelectedRatePerAreaUnitHelper;
            SelectedRatePer100UnitHelper = productVM.SelectedRatePerAreaUnitHelper != null ? productVM.SelectedRatePerAreaUnitHelper.Unit : AllowedRateUnits[0].Unit;
            SelectedRatePerTankUnit = productVM.SelectedRatePerTankUnit is NullUnit ? AllowedTotalUnits.FirstOrDefault(x => x.Name == mlp.StdPackageUnit) : productVM.SelectedRatePerTankUnit;
            SelectedTotalProductUnit = AllowedTotalUnits.FirstOrDefault(x => x.Name == mlp.StdPackageUnit);
            CalculateRatePer100Value();

            if (prodSettingsMaybe.HasValue)
            {
                var prodSettingsValue = prodSettingsMaybe.Value;
                var rateUnit = prodSettingsValue.RatePerAreaUnit;
                var ratePer100Unit = prodSettingsValue.RatePer100Unit;
                var ratePerTank = prodSettingsValue.RatePerTankUnit;
                var totalUnit = prodSettingsValue.TotalUnit;

                SelectedRatePerAreaUnitHelper = string.IsNullOrEmpty(rateUnit) ? SelectedRatePerAreaUnitHelper : AllowedRateUnits.FirstOrDefault(x => x.Unit.Name == rateUnit);
                SelectedRatePer100UnitHelper = string.IsNullOrEmpty(ratePer100Unit) ? SelectedRatePer100UnitHelper : AllowedRateUnits.FirstOrDefault(x => x.Unit.Name == ratePer100Unit).Unit;
                SelectedRatePerTankUnit = string.IsNullOrEmpty(ratePerTank) ? SelectedRatePerTankUnit : AllowedRateUnits.FirstOrDefault(x => x.Unit.Name == ratePerTank).Unit;
                SelectedTotalProductUnit = string.IsNullOrEmpty(totalUnit) ? SelectedTotalProductUnit : AllowedTotalUnits.FirstOrDefault(x => x.Name == totalUnit);
            }

            SelectedTabIndex = 0;
            RaisePropertyChanged(() => SelectedTabIndex);
            CancelCalculator = new RelayCommand(Cancel);
            Update = new RelayCommand(UpdateValues);
        }

        public AppliedProductCalculatorViewModel(IClientEndpoint endpoint, RecommendedProductViewModel productVM, TankInformationModel tankInfo)
        {
            this.endpoint = endpoint;
            this.recProductVM = productVM;
            this.tankInfo = tankInfo;

            masterlist = endpoint.GetMasterlistService();
            var mlp = masterlist.GetProduct(recProductVM.SelectedProduct.ProductId);
            var prodSettingsMaybe = endpoint.GetView<ProductSettingsView>(new ProductSettingId(ApplicationEnvironment.CurrentDataSourceId, mlp.Id));

            SelectedMasterlistProduct = mlp;
            ProductAreaValue = recProductVM.ProductArea.Value;
            ProductAreaUnit = recProductVM.ProductArea.Unit;
            AllowedRateUnits = recProductVM.AllowedRateUnits;
            AllowedTotalUnits = recProductVM.AllowedTotalUnits;
            SelectedRatePerAreaUnitHelper = recProductVM.SelectedRatePerAreaUnitHelper;
            SelectedRatePer100UnitHelper = recProductVM.SelectedRatePerAreaUnitHelper != null ? recProductVM.SelectedRatePerAreaUnitHelper.Unit : AllowedRateUnits[0].Unit;
            SelectedRatePerTankUnit = recProductVM.SelectedRatePerTankUnit is NullUnit ? AllowedTotalUnits.FirstOrDefault(x => x.Name == mlp.StdPackageUnit) : recProductVM.SelectedRatePerTankUnit;
            SelectedTotalProductUnit = AllowedTotalUnits.FirstOrDefault(x => x.Name == mlp.StdPackageUnit);
            CalculateRatePer100Value();

            if (prodSettingsMaybe.HasValue)
            {
                var prodSettingsValue = prodSettingsMaybe.Value;
                var rateUnit = prodSettingsValue.RatePerAreaUnit;
                var ratePer100Unit = prodSettingsValue.RatePer100Unit;
                var ratePerTank = prodSettingsValue.RatePerTankUnit;
                var totalUnit = prodSettingsValue.TotalUnit;

                SelectedRatePerAreaUnitHelper = string.IsNullOrEmpty(rateUnit) ? SelectedRatePerAreaUnitHelper : AllowedRateUnits.FirstOrDefault(x => x.Unit.Name == rateUnit);
                SelectedRatePer100UnitHelper = string.IsNullOrEmpty(ratePer100Unit) ? SelectedRatePer100UnitHelper : AllowedRateUnits.FirstOrDefault(x => x.Unit.Name == ratePer100Unit).Unit;
                SelectedRatePerTankUnit = string.IsNullOrEmpty(ratePerTank) ? SelectedRatePerTankUnit : AllowedRateUnits.FirstOrDefault(x => x.Unit.Name == ratePerTank).Unit;
                SelectedTotalProductUnit = string.IsNullOrEmpty(totalUnit) ? SelectedTotalProductUnit : AllowedTotalUnits.FirstOrDefault(x => x.Name == totalUnit);
            }

            SelectedTabIndex = 0;
            RaisePropertyChanged(() => SelectedTabIndex);
            CancelCalculator = new RelayCommand(Cancel);
            Update = new RelayCommand(UpdateValues);
        }

        //public AppliedProductCalculatorViewModel(IClientEndpoint endPoint, PlannedProductViewModel plannedProduct, TankInformationModel tankInfo)
        //{
        //    this.endpoint = endPoint;
        //    this.planProductVM = plannedProduct;
        //    this.tankInfo = tankInfo;

        //    masterlist = endpoint.GetMasterlistService();
        //    var mlp = masterlist.GetProduct(planProductVM.SelectedProduct.ProductId);
        //    var prodSettingsMaybe = endpoint.GetView<ProductSettingsView>(new ProductSettingId(ApplicationEnvironment.CurrentDataSourceId, mlp.Id));

        //    AllowedRateUnits = planProductVM.AllowedUnits;
        //    AllowedTotalUnits = planProductVM.AllowedUnits
        //    SelectedRatePerAreaUnitHelper = planProductVM.SelectedRatePerAreaUnitHelper;
        //    SelectedRatePer100UnitHelper = planProductVM.SelectedRatePerAreaUnitHelper != null ? planProductVM.SelectedRatePerAreaUnitHelper.Unit : AllowedRateUnits[0].Unit;
        //    SelectedRatePerTankUnit = planProductVM.SelectedRatePerTankUnit is NullUnit ? AllowedTotalUnits.FirstOrDefault(x => x.Name == mlp.StdPackageUnit) : planProductVM.SelectedRatePerTankUnit;
        //    SelectedTotalProductUnit = AllowedTotalUnits.FirstOrDefault(x => x.Name == mlp.StdPackageUnit);
        //    CalculateRatePer100Value();

        //    if (prodSettingsMaybe.HasValue)
        //    {
        //        var prodSettingsValue = prodSettingsMaybe.Value;
        //        var rateUnit = prodSettingsValue.RatePerAreaUnit;
        //        var ratePer100Unit = prodSettingsValue.RatePer100Unit;
        //        var ratePerTank = prodSettingsValue.RatePerTankUnit;
        //        var totalUnit = prodSettingsValue.TotalUnit;

        //        SelectedRatePerAreaUnitHelper = string.IsNullOrEmpty(rateUnit) ? SelectedRatePerAreaUnitHelper : AllowedRateUnits.FirstOrDefault(x => x.Unit.Name == rateUnit);
        //        SelectedRatePer100UnitHelper = string.IsNullOrEmpty(ratePer100Unit) ? SelectedRatePer100UnitHelper : AllowedRateUnits.FirstOrDefault(x => x.Unit.Name == ratePer100Unit).Unit;
        //        SelectedRatePerTankUnit = string.IsNullOrEmpty(ratePerTank) ? SelectedRatePerTankUnit : AllowedRateUnits.FirstOrDefault(x => x.Unit.Name == ratePerTank).Unit;
        //        SelectedTotalProductUnit = string.IsNullOrEmpty(totalUnit) ? SelectedTotalProductUnit : AllowedTotalUnits.FirstOrDefault(x => x.Name == totalUnit);
        //    }

        //    CancelCalculator = new RelayCommand(Cancel);
        //    Update = new RelayCommand(UpdateValues);
        //}

        public RelayCommand CancelCalculator { get; set; }
        public RelayCommand Update { get; set; }

        private decimal? totalPackageValue{get;set;}
        public decimal? TotalPackageValue { get { return totalPackageValue; } set { totalPackageValue = value; RaisePropertyChanged(() => TotalPackageValue); CalculatePerTotalPackageValues(); } }
        private decimal? totalPackageAreaValue { get; set; }
        public decimal? TotalPackageAreaValue { get { return totalPackageAreaValue; } set { totalPackageAreaValue = value; RaisePropertyChanged(() => TotalPackageAreaValue); CalculatePerTotalPackageValues(); } }
        public decimal RatePer100Value 
        { 
            get { return ratePer100Value; } 
            set 
            { 
                ratePer100Value = value;
                RaisePropertyChanged("RatePer100Value");

                ReCalculateByRatePer100();
            } 
        }

        public IUnit SelectedRatePer100UnitHelper
        {
            get { return ratePer100UnitHelper; }
            set
            {
                ratePer100UnitHelper = value;
                RaisePropertyChanged("SelectedRatePer100UnitHelper");

                ReCalculateByRatePer100();
            }
        }

        public decimal RatePerAreaValue
        {
            get { return ratePerAreaValue; }
            set
            {
                ratePerAreaValue = value;

                RaisePropertyChanged("RatePerAreaValue");
            }
        }

        public UnitPerAreaHelper SelectedRatePerAreaUnitHelper
        {
            get { return ratePerAreaUnitHelper; }
            set
            {
                if (ratePerAreaUnitHelper != value)
                {
                    RatePerAreaValueConversion(ratePerAreaUnitHelper, value);
                    ratePerAreaUnitHelper = value;

                    RaisePropertyChanged("SelectedRatePerAreaUnitHelper");
                    
                }
            }
        }


        public decimal RatePerTankValue
        {
            get { return ratePerTankValue; }
            set
            {
                ratePerTankValue = value;

                RaisePropertyChanged("RatePerTankValue");
            }
        }

        public IUnit SelectedRatePerTankUnit
        {
            get { return ratePerTankUnit; }
            set
            {
                if (ratePerTankUnit == null || ratePerTankUnit != value)
                {
                    ratePerTankUnit = value;
                    RaisePropertyChanged("SelectedRatePerTankUnit");
                    ReCalculateByRatePer100();
                    CalculatePerTotalPackageValues();
                    TotalProductValueCalculatorUpdated();
                }
            }
        }

        public decimal TotalProductValue
        {
            get { return totalProductValue; }
            set
            {
                totalProductValue = value;

                RaisePropertyChanged("TotalProductValue");
                TotalProductValueCalculatorUpdated();
            }
        }

        public IUnit TotalProductUnit { get { return totalProductUnit; } set { totalProductUnit = value; } }

        private IUnit selectedTotalPackageProductUnit { get; set; }
        public IUnit SelectedTotalPackageProductUnit
        {
            get { return selectedTotalPackageProductUnit; }
            set
            {
                selectedTotalPackageProductUnit = value;
                RaisePropertyChanged(() => SelectedTotalPackageProductUnit);
                CalculatePerTotalPackageValues();
            }

        }
        public IUnit SelectedTotalProductUnit
        {
            get { return totalProductUnit; ; }
            set
            {
                if (totalProductUnit == null || totalProductUnit != value)
                {
                    totalProductUnit = value;
                    RaisePropertyChanged("SelectedTotalProductUnit");
                    //ReCalculateByRatePer100();
                    TotalProductValueCalculatorUpdated();
                }
            }
        }

        public decimal AreaPerGallonValue 
        { 
            get 
            { 
                return areaPerGallonValue; 
            } 
            set 
            { 
                areaPerGallonValue = value; 
                RaisePropertyChanged("AreaPerGallonValue");

                AreaPerGallonValueUpdated();
            } 
        }

        public double ProductAreaValue { get; set; }
        public IUnit ProductAreaUnit { get; set; }

        public MiniProduct SelectedMasterlistProduct { get; set; }

        public string CarrierPerArea { get { return TankInfo.CarrierPerArea.ToString(); } }
        public string TankSize { get { return TankInfo.TankSize.ToString(); } }

        public ObservableCollection<UnitPerAreaHelper> AllowedRateUnits { get; private set; }
        public ObservableCollection<IUnit> AllowedTotalUnits { get; private set; }

        public TankInformationModel TankInfo { get { return tankInfo; } }

        public string TotalArea { get { return string.Format("{0} {1}", ProductAreaValue.ToString("N2"), ProductAreaUnit.AbbreviatedDisplay); } }

        public int SelectedTabIndex {
            get { 
                return tabIndex; 
            } 
            set { 
                tabIndex = value; 
            } 
        }

        public TabItem TabItem { get { return item; }
            set
            {
                item = value;
                TabSelectionChanged();
            }
        }
        void RatePerAreaValueConversion(UnitPerAreaHelper oldValue, UnitPerAreaHelper newValue)
        {
            if (RatePerAreaValue == null || RatePerAreaValue == 0m || SelectedRatePerAreaUnitHelper == null) { return; }

            var masterlistProd = SelectedMasterlistProduct;
            var ratePerAreaMeasure = oldValue.Unit.GetMeasure((double)RatePerAreaValue, masterlistProd.Density);// SelectedRatePer100UnitHelper.GetMeasure((double)newVal, masterlistProd.Density);

            var targetUnit = newValue.Unit;
            var convertRatePerArea = ratePerAreaMeasure.CanConvertTo(targetUnit) ? targetUnit.GetMeasure(ratePerAreaMeasure.GetValueAs(targetUnit)) : ratePerAreaMeasure;
            RatePerAreaValue = (decimal)convertRatePerArea.Value;
        }

        void CalculatePerTotalPackageValues()
        {
            if (TotalPackageAreaValue.HasValue && TotalPackageValue.HasValue)
            {
                SelectedRatePerAreaUnitHelper = SelectedRatePerAreaUnitHelper;
                SelectedRatePerTankUnit = SelectedRatePerTankUnit;

                var ratePerAreaPreConvert = TotalPackageValue.Value / TotalPackageAreaValue.Value;
                var preConvertMeasurePerArea = SelectedTotalPackageProductUnit.GetMeasure((double)ratePerAreaPreConvert, SelectedMasterlistProduct.Density);
                RatePerAreaValue = (decimal)preConvertMeasurePerArea.GetValueAs(SelectedRatePerAreaUnitHelper.Unit);
                var total = RatePerAreaValue * (decimal)ProductAreaValue;
                var totalMeasure = SelectedRatePerAreaUnitHelper.Unit.GetMeasure((double)total, SelectedMasterlistProduct.Density);
                TotalProductValue = (decimal)totalMeasure.GetValueAs(TotalProductUnit);
                SelectedTotalProductUnit = TotalProductUnit;

                //calc rate per tank value
                if (TankInfo.TankCount == 0m) { return; }
                var ratePerTankValue = TotalProductValue / TankInfo.TankCount;
                var unConvertedTankMeasure = SelectedTotalProductUnit.GetMeasure((double)ratePerTankValue, SelectedMasterlistProduct.Density);
                RatePerTankValue = (decimal)unConvertedTankMeasure.GetValueAs(SelectedRatePerTankUnit);
            }
        }
        void ReCalculateByRatePer100()
        {
            if (RatePer100Value == null || RatePer100Value == 0m || SelectedRatePer100UnitHelper == null) { return; }

            var newVal = (decimal)(TankInfo.CarrierPerArea.Value / 100.0) * RatePer100Value;
            var masterlistProd = SelectedMasterlistProduct;
            var ratePerAreaMeasure = SelectedRatePer100UnitHelper.GetMeasure((double)newVal, masterlistProd.Density);
            var targetUnit = SelectedRatePerAreaUnitHelper.Unit;
            var convertRatePerArea = ratePerAreaMeasure.CanConvertTo(targetUnit) ? targetUnit.GetMeasure(ratePerAreaMeasure.GetValueAs(targetUnit)) : ratePerAreaMeasure;
            RatePerAreaValue = (decimal)convertRatePerArea.Value;
            SelectedRatePerAreaUnitHelper = (from r in AllowedRateUnits where r.Unit.Name == convertRatePerArea.Unit.Name select r).FirstOrDefault();

            var tankVal = (tankInfo.TankSize.Value / 100.0);
            var ratePerTankUnConverted = SelectedRatePer100UnitHelper.GetMeasure((double)RatePer100Value * tankVal, masterlistProd.Density);

            var targetTankUnit = SelectedRatePerTankUnit != null ? SelectedRatePerTankUnit : SelectedRatePer100UnitHelper;
            var convertedRatePerTank = targetTankUnit.GetMeasure(ratePerTankUnConverted.GetValueAs(targetTankUnit), masterlistProd.Density);
            RatePerTankValue = (decimal)convertedRatePerTank.Value;
            SelectedRatePerTankUnit = convertedRatePerTank.Unit;
            
        }

        void CalculateRatePer100Value()
        {
            if (RatePerAreaValue != 0m && RatePerAreaValue != null && TankInfo.CarrierPerArea.Value > 0.0)
            {
                var ratePer100 = RatePerAreaValue / (decimal)(TankInfo.CarrierPerArea.Value / 100.0);
                RatePer100Value = ratePer100;
            }
        }

        void TotalProductValueCalculatorUpdated()
        {
            if (ProductAreaValue == 0.0 || SelectedTotalProductUnit == null) { return; }

            var density = SelectedMasterlistProduct.Density;
            //calc rate per area value
            var ratePerArea = TotalProductValue / (decimal)ProductAreaValue;
            var ratePerAreaUnConvertedMeasure = SelectedTotalProductUnit.GetMeasure((double)ratePerArea, density);
            RatePerAreaValue = (decimal)ratePerAreaUnConvertedMeasure.GetValueAs(SelectedRatePerAreaUnitHelper.Unit);

            //calc rate per tank value
            if (TankInfo.TankCount == 0m) { return; }
            var ratePerTankValue = TotalProductValue / TankInfo.TankCount;
            var unConvertedTankMeasure = SelectedTotalProductUnit.GetMeasure((double)ratePerTankValue, density);
            RatePerTankValue = (decimal)unConvertedTankMeasure.GetValueAs(SelectedRatePerTankUnit);
        }

        void AreaPerGallonValueUpdated()
        {
            var density = SelectedMasterlistProduct.Density;
            var gallonUnit = (from i in AllowedRateUnits
                             where i.Unit == UnitFactory.GetUnitByName("gallon")
                             select i).FirstOrDefault();
            if (gallonUnit != null)
            {
                // acres per gallon example 40 : 1
                var ratio = 1 / AreaPerGallonValue;
                var ratePerAreaMeasure = gallonUnit.Unit.GetMeasure((double)ratio, density);
                var convertedValue = ratePerAreaMeasure.GetValueAs(SelectedRatePerAreaUnitHelper.Unit);
                RatePerAreaValue = (decimal)convertedValue;

                var measure = SelectedRatePerAreaUnitHelper.Unit.GetMeasure((double)(RatePerAreaValue * (decimal)ProductAreaValue), density);
                var converted = SelectedTotalProductUnit != null ? SelectedTotalProductUnit.GetMeasure( measure.GetValueAs(SelectedTotalProductUnit), density) : measure;
                TotalProductValue = (decimal)converted.Value;
                SelectedTotalProductUnit = converted.Unit;

                if (TankInfo.TankCount == 0m) { return; }
                var ratePerTank = TotalProductValue / TankInfo.TankCount;
                var perTankmeasure = SelectedTotalProductUnit.GetMeasure((double)ratePerTank, density);
                var convertedPerTank = SelectedRatePerTankUnit != null ? SelectedRatePerTankUnit.GetMeasure(perTankmeasure.GetValueAs(SelectedRatePerTankUnit), density) : perTankmeasure;
                RatePerTankValue = (decimal)convertedPerTank.Value;
                SelectedRatePerTankUnit = convertedPerTank.Unit;
            }
            
        }

        public void TabSelectionChanged()
        {
            RatePerAreaValue = 0m;
            RatePerTankValue = 0m;
            TotalProductValue = 0m;
        }

        void Cancel()
        {
            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
        }

        void UpdateValues()
        {
            var mlp = SelectedMasterlistProduct;
            var density = mlp.Density;
            var ratePerAreaMeasure = SelectedRatePerAreaUnitHelper.Unit.GetMeasure((double)RatePerAreaValue, density);

            var ratePerTankMeasure = SelectedRatePerTankUnit.GetMeasure((double)RatePerTankValue, density);

            var totalvalue = ratePerAreaMeasure.Value * ProductAreaValue;
            var targetTotalUnit = TotalProductUnit != null ? TotalProductUnit : UnitFactory.GetPackageSafeUnit(mlp.StdPackageUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
            var totalMeasureNonConvert = ratePerAreaMeasure.Unit.GetMeasure(totalvalue, density);
            var totalConvertedValue = totalMeasureNonConvert.GetValueAs(targetTotalUnit);
            var totalMeasure = targetTotalUnit.GetMeasure(totalConvertedValue, density);

            if (productVM != null)
            {
                productVM.UpdateFromCalculator(ratePerAreaMeasure, ratePerTankMeasure, totalMeasure);
            }
            else if (recProductVM != null)
            {
                recProductVM.UpdateFromCalculator(ratePerAreaMeasure, ratePerTankMeasure, totalMeasure);
            }

            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
        }
    }
}
