﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Client.Services;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Company;
using Landdb.Domain.ReadModels.Invoice;
using Landdb.Domain.ReadModels.Person;
using Landdb.Domain.ReadModels.Plan;
using Landdb.Domain.ReadModels.Recommendation;
using Landdb.Domain.ReadModels.WorkOrder;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Secondary.PopUp;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Secondary.Application {
    public class InfoViewModel : ViewModelBase {
        IClientEndpoint clientEndpoint;
        Dispatcher dispatcher;

		DateTime startDate = DateTime.MinValue;
		DateTime startTime = DateTime.MinValue;
        DateTime? customEndDate = DateTime.Today + TimeSpan.FromHours(10);
        DateTime? customEndTime = DateTime.Today + TimeSpan.FromHours(10);
        DateTime? authorizationDate = DateTime.Today + TimeSpan.FromHours(8);
        DurationViewModel selectedDuration;
        TimingEvent timingEvent;
        string timingEventTag;
        string selectedTimingEventTag;
        List<string> timingEventTags;
        Dictionary<string, List<string>> availableTimingEventTags;
        Dictionary<string, List<string>> tempTimingEventTags;
        Dictionary<string, List<string>> mergedTimingEventTags;
        string name;
        string notes;
        PersonListItem authorizer;
        List<PersonListItem> authorizers;
        ClearAgWeatherConditionsViewModel conditionsModel;
        bool changeAuthDate;

        WorkOrderView associatedWorkOrderView;
        InvoiceView associatedInvoiceView;
        RecommendationView associatedRecommendationView;
        PlanView associatedPlanView;
        UserSettings userSettings;

        public InfoViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, ClearAgWeatherConditionsViewModel conditionsModel, int cropYear, Guid dataSourceId) {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            this.conditionsModel = conditionsModel;

			var personListView = clientEndpoint.GetView<PersonListView>(new DataSourceId(dataSourceId)).GetValue(new PersonListView());
			var sortedFilteredPersons = from p in personListView.Persons
										where p.IsActiveInCropYear(cropYear)
										orderby p.Name
										select p;

			authorizers = sortedFilteredPersons.ToList();

            availableTimingEventTags = new Dictionary<string, List<string>>();
            AvailableTimingEventTags = GetAvailableTimingEventTags(clientEndpoint);

            SelectedApplicators = new ObservableCollection<ApplicationApplicator>();

            AddApplicatorCommand = new RelayCommand(AddPersonAsApplicator);
            AddCompanyCommand = new RelayCommand(AddCompany);

			StartDate = DateTime.Today + TimeSpan.FromHours(8);
			StartTime = DateTime.Today + TimeSpan.FromHours(8);

            InitializeDurations();

            var nameGenerationService = ServiceLocator.Get<INameAutoGenerationService>();
            Name = nameGenerationService.DisplayName;

            IncludeAsApplicator = new IncludeApplicator(clientEndpoint, dispatcher, SelectedApplicators);
            IncludeCompany = new IncludeApplicationCompany(clientEndpoint, dispatcher, this);

            changeAuthDate = true;
        }

        private Dictionary<string, List<string>> GetAvailableTimingEventTags(IClientEndpoint clientEndpoint) {
            Dictionary<string, List<string>> availableTimingEventTagz = new Dictionary<string, List<string>>();
            var plantagviewMaybe = clientEndpoint.GetView<ApplicationTagProjectionView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear));
            if (plantagviewMaybe.HasValue && plantagviewMaybe.Value.TagInfo != null) {
                foreach (var v in plantagviewMaybe.Value.TagInfo) {
                    if (string.IsNullOrEmpty(v.TimingEvent)) { continue; }
                    if (!availableTimingEventTagz.ContainsKey(v.TimingEvent)) {
                        availableTimingEventTagz.Add(v.TimingEvent, new List<string>());
                    }
                    foreach (var x in v.Tags) {
                        if (availableTimingEventTagz.ContainsKey(v.TimingEvent)) {
                            if (!string.IsNullOrEmpty(x.Key) && !availableTimingEventTagz[v.TimingEvent].Contains(x.Key)) {
                                availableTimingEventTagz[v.TimingEvent].Add(x.Key);
                            }
                        }
                    }
                }
            }
            return availableTimingEventTagz;
        }

        public ICommand AddApplicatorCommand { get; }
		public ICommand AddCompanyCommand { get; }

		public IncludeApplicator IncludeAsApplicator { get; set; }
        public IncludeApplicationCompany IncludeCompany { get; set; }

        public InvoiceView AssociatedInvoiceView {
            get { return associatedInvoiceView; }
            set {
                associatedInvoiceView = value;

                if (associatedInvoiceView != null) {
                    StartDate = associatedInvoiceView.InvoiceDate.Date;

                    //////////////////////////////////////////////////////////////////////////
                    //Name of Vendors initials plus invoice # as requested in bug # 20451
                    //////////////////////////////////////////////////////////////////////////

                    var vendor = associatedInvoiceView.Vendor;
                    var invoiceNumber = associatedInvoiceView.InvoiceNumber;
                    string vendorInitials = string.Empty;
                    vendorInitials = Regex.Replace(vendor, "[^A-Z]", "");
                    Name = string.Format("{0}{1}-{2}{3}", vendorInitials, invoiceNumber, DateTime.Now.ToString("yyMMdd"), DateTime.Now.ToLocalTime().ToString("HHmm"));
                }

                RaisePropertyChanged(() => AssociatedInvoiceView);
            }
        }

        public WorkOrderView AssociatedWorkOrderView {
            get { return associatedWorkOrderView; }
            set {
                associatedWorkOrderView = value;

                if (associatedWorkOrderView != null) {
                    StartDate = associatedWorkOrderView.StartDateTime.ToLocalTime().Date;
                    StartTime = associatedWorkOrderView.StartDateTime.ToLocalTime();
                    //Name = string.Format("From Work Order {0}", associatedWorkOrderView.Name);
                    TimingEvent = (!string.IsNullOrEmpty(associatedWorkOrderView.TimingEvent)) ? TimingEvents.Where(x => x.Name == associatedWorkOrderView.TimingEvent).FirstOrDefault() : null;
                    TimingEventTag = associatedWorkOrderView.TimingEventTag;
                    
                    if (associatedWorkOrderView.Authorization != null) {
                        AuthorizationDate = associatedWorkOrderView.Authorization.AuthorizationDate;
                        Authorizer = (from auth in Authorizers
                                      where auth.Id == associatedWorkOrderView.Authorization.PersonId
                                      select auth).FirstOrDefault();
                    }

                    if (associatedWorkOrderView.Applicators != null) {
                        foreach (var a in associatedWorkOrderView.Applicators) {
                            // TODO: Account for this already being set by the user before picking a work order on the sources page
                            SelectedApplicators.Add(new ApplicationApplicator(a.PersonId, a.PersonName, a.CompanyId, a.CompanyName, a.LicenseNumber, a.Expires));
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(associatedWorkOrderView.Notes)) {
                        Notes += string.Format("{0}:\n--------------------------------------\n{1}", Strings.FromWorkOrder_Text, associatedWorkOrderView.Notes);
                    }

                    if(associatedWorkOrderView.EnvironmentalZoneDistanceValue != null)
                    {
                        IncludeAsApplicator.BufferWidth = associatedWorkOrderView.EnvironmentalZoneDistanceValue;
                        IncludeAsApplicator.SelectedBufferTolerance = associatedWorkOrderView.Severity;
                    }
                    // TODO: Set timing event & notes once they become available in the WO View.
                }

                RaisePropertyChanged(() => AssociatedWorkOrderView);
            }
        }

        public RecommendationView AssociatedRecommendationView
        {
            get { return associatedRecommendationView; }
            set
            {
                associatedRecommendationView = value;

                if (associatedRecommendationView != null)
                {
                    StartDate = associatedRecommendationView.ProposedDate.HasValue ? associatedRecommendationView.ProposedDate.Value.Date : DateTime.Now;
                    //StartTime = associatedRecommendationView.CreatedDateTime;
                    //Name = string.Format("From Recommendation {0}", associatedRecommendationView.RecTitle);
                    TimingEvent = (!string.IsNullOrEmpty(associatedRecommendationView.TimingEvent)) ? TimingEvents.Where(x => x.Name == associatedRecommendationView.TimingEvent).FirstOrDefault() : null;
                    TimingEventTag = associatedRecommendationView.TimingEventTag;

                    if (associatedRecommendationView.Authorization != null)
                    {
                        AuthorizationDate = associatedRecommendationView.Authorization.AuthorizationDate;
                        Authorizer = (from auth in Authorizers
                                      where auth.Id == associatedRecommendationView.Authorization.PersonId
                                      select auth).FirstOrDefault();
                    }

                    if (associatedRecommendationView.Applicators != null)
                    {
                        foreach (var a in associatedRecommendationView.Applicators)
                        {
                            // TODO: Account for this already being set by the user before picking a work order on the sources page
                            SelectedApplicators.Add(new ApplicationApplicator(a.PersonId, a.PersonName, a.CompanyId, a.CompanyName, a.LicenseNumber, a.Expires));
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(associatedRecommendationView.Notes))
                    {
                        Notes += string.Format("{0}:\n--------------------------------------\n{1}", Strings.FromRecommendation_Text, associatedRecommendationView.Notes);
                    }
                    
                }

                RaisePropertyChanged(() => AssociatedRecommendationView);
            }
        }

        public PlanView AssociatedPlanView {
            get { return associatedPlanView; }
            set {
                associatedPlanView = value;
                if (associatedPlanView != null) {
                    if (associatedPlanView.Products.Count > 0) {
                        string timingeventname = associatedPlanView.Products.FirstOrDefault().TimingEvent;
                        TimingEvent = (!string.IsNullOrEmpty(timingeventname)) ? TimingEvents.Where(x => x.Name == timingeventname).FirstOrDefault() : null;
                        TimingEventTag = associatedPlanView.Products.FirstOrDefault().TimingEventTag;
                    }

                    if (!string.IsNullOrWhiteSpace(associatedPlanView.Notes)) {
                        Notes += string.Format("{0}:\n--------------------------------------\n{1}", Strings.FromPlan_Text, associatedPlanView.Notes);
                    }
                }

                RaisePropertyChanged(() => AssociatedPlanView);
            }
        }

        public DateTime StartDate {
            get { return startDate; }
            set {
                if (startDate != value) {
                    if (startDate.Date == AuthorizationDate.GetValueOrDefault().Date && changeAuthDate) {
                        AuthorizationDate = value;
                    }
                    startDate = value;
                    RaisePropertyChanged(() => StartDate);
                    conditionsModel.ApplicationStartDate = startDate.Date + startTime.TimeOfDay;
                    EnsureEndDateIsValid();
                    ResetCustomEndDateOnStartDateChange();
                }
            }
        }

        public DateTime StartTime {
            get { return startTime; }
            set {
                if (startTime != value) {
                    startTime = value;
                    RaisePropertyChanged(() => StartTime);
                    conditionsModel.ApplicationStartDate = startDate.Date + startTime.TimeOfDay;
                    EnsureEndDateIsValid();
                }
            }
        }

        void ResetCustomEndDateOnStartDateChange()
        {
            var duration = new DurationViewModel(null);
            if ((SelectedDuration != null && SelectedDuration.IsCustom) || (Durations != null && !Durations.Contains(SelectedDuration)))
            {
                    CustomEndDate = StartDate.AddHours(SelectedDuration.Duration.HasValue ? SelectedDuration.Duration.Value.TotalHours : 2);
                CustomEndTime = StartTime + TimeSpan.FromHours(SelectedDuration.Duration.HasValue ? SelectedDuration.Duration.Value.TotalHours : 2);
            }
        }

        void EnsureEndDateIsValid() {
            if(CustomEndTime == null)
            {
                CustomEndTime = StartTime + TimeSpan.FromHours(2);
            }

            if (CustomEndDate.Value < StartDate) {
                CustomEndDate = StartDate;
            }
            else if (customEndDate.Value.Date == StartDate.Date && CustomEndTime.Value < StartTime) {
                CustomEndTime = StartTime + TimeSpan.FromHours(2);
            }
        }

        public DateTime? CustomEndDate {
            get { return customEndDate; }
            set {
                if (customEndDate != value) {
                    customEndDate = value;
                    RaisePropertyChanged(() => CustomEndDate);
                    EnsureEndDateIsValid();
                }
            }
        }

        public DateTime? CustomEndTime {
            get { return customEndTime; }
            set {
                if (customEndTime != value) {
                    customEndTime = value;
                    RaisePropertyChanged(() => CustomEndTime);
                    EnsureEndDateIsValid();
                }
            }
        }

        public DateTime? AuthorizationDate {
            get { return authorizationDate; }
            set {
                authorizationDate = value;
                RaisePropertyChanged(() => AuthorizationDate);
                changeAuthDate = false;
            }
        }

        public ObservableCollection<DurationViewModel> Durations { get; private set; }

        private string _durationText { get; set; }
        public string DurationText {
            get
            {
                return _durationText;
            }
            set
            {
                _durationText = value;
                decimal duration = 0m;

                if (decimal.TryParse(_durationText, out duration))
                {
                    var newDuration = new DurationViewModel(TimeSpan.FromHours(Convert.ToDouble(duration))) { IsCustom = true };
                    if (Durations != null && !Durations.Any(x => x.Duration == newDuration.Duration))
                    {
                        SelectedDuration = newDuration;
                    }
                }
                else
                {
                    //INCASE OF PARSE FAILURE   
                    string parsedString = string.Empty;
                    for (int i = 0; i < _durationText.Count(); i++)
                    {
                        if (Char.IsDigit(_durationText[i]) || _durationText[i] == '.')
                        {
                            parsedString += _durationText[i];
                        }
                    }

                    if (decimal.TryParse(parsedString, out duration))
                    {
                        var newDuration = new DurationViewModel(TimeSpan.FromHours(Convert.ToDouble(duration))) { IsCustom = true };
                        if (Durations != null && !Durations.Any(x => x.Duration == newDuration.Duration))
                        {
                            SelectedDuration = newDuration;
                        }
                    }
                }

                _durationText = selectedDuration.ToString();
            }
        }
        public DurationViewModel SelectedDuration {
            get { return selectedDuration; }
            set {
                selectedDuration = value;
                if(selectedDuration == null) { selectedDuration = Durations[3]; }
                ResetCustomEndDateOnStartDateChange();

                if (selectedDuration.Duration.HasValue && selectedDuration.Duration.Value.TotalHours != userSettings.LastUsedDuration)
                {
                    var durationDouble = selectedDuration.Duration.Value.TotalHours;
                    userSettings.LastUsedDuration = durationDouble;
                    clientEndpoint.SaveUserSettings();
                }

                RaisePropertyChanged(() => SelectedDuration);
            }
        }

        public string Name {
            get { return name; }
            set {
                name = value;
                RaisePropertyChanged(() => Name);
            }
        }

        public string Notes {
            get { return notes; }
            set {
                notes = value;
                RaisePropertyChanged(() => Notes);
            }
        }

        public ObservableCollection<ApplicationApplicator> SelectedApplicators { get; set; }
        public ObservableCollection<CompanyListItem> SelectedCompanies { get; set; }

        public List<PersonListItem> Authorizers {
            get { return authorizers; }
        }

        public bool IsEdit { get { return false; } }

        public PersonListItem Authorizer {
            get { return authorizer; }
            set {
                authorizer = value;
                RaisePropertyChanged(() => Authorizer);
            }
        }

        public List<TimingEvent> TimingEvents {
            get {
                List<TimingEvent> timingEvents = clientEndpoint.GetMasterlistService().GetTimingEventList().OrderBy(x => x.Order).ToList();
                return timingEvents;
            }
        }

        public TimingEvent TimingEvent {
            get { return timingEvent; }
            set {
                timingEvent = value;
                SelectAppropriateTags();
                RaisePropertyChanged(() => TimingEvent);
            }
        }

        public Dictionary<string, List<string>> AvailableTimingEventTags {
            get {
                return availableTimingEventTags;
            }
            set {
                availableTimingEventTags = value;
                SelectAppropriateTags();
            }
        }

        public Dictionary<string, List<string>> TempTimingEventTags {
            get {
                return tempTimingEventTags;
            }
            set {
                tempTimingEventTags = value;
                SelectAppropriateTags();
                RaisePropertyChanged(() => TempTimingEventTags);
            }
        }

        public List<string> TimingEventTags {
            get {
                if (timingEventTags == null) {
                    return new List<string>();
                }
                return timingEventTags.OrderBy(q => q).ToList();
            }
            set {
                timingEventTags = value;
                RaisePropertyChanged(() => TimingEventTags);
            }
        }

        public string TimingEventTag {
            get { return timingEventTag; }
            set {
                if (timingEventTag == value) { return; }
                timingEventTag = value;
                RaisePropertyChanged(() => TimingEventTag);
            }
        }

        public string SelectedTimingEventTag {
            get { return selectedTimingEventTag; }
            set {
                if (selectedTimingEventTag == value) { return; }
                selectedTimingEventTag = value;
                if (!string.IsNullOrEmpty(selectedTimingEventTag)) {
                    timingEventTag = selectedTimingEventTag;
                }
                RaisePropertyChanged(() => SelectedTimingEventTag);
            }
        }

        private void SelectAppropriateTags() {
            if (timingEvent != null && !string.IsNullOrEmpty(timingEvent.Name)) {
                mergedTimingEventTags = availableTimingEventTags;
                if (tempTimingEventTags != null && tempTimingEventTags.Count > 0) {
                    mergedTimingEventTags = availableTimingEventTags.Concat(tempTimingEventTags).GroupBy(d => d.Key).ToDictionary(d => d.Key, d => d.First().Value);
                }

                if (mergedTimingEventTags != null && mergedTimingEventTags.ContainsKey(timingEvent.Name)) {
                    TimingEventTags = mergedTimingEventTags[timingEvent.Name];
                }
                else {
                    TimingEventTags = new List<string>();
                }
            }
        }


        void InitializeDurations() {
            Durations = new ObservableCollection<DurationViewModel>() {
                new DurationViewModel(null),
                new DurationViewModel(TimeSpan.FromHours(4)),
                new DurationViewModel(TimeSpan.FromHours(3.5)),
                new DurationViewModel(TimeSpan.FromHours(3)),
                new DurationViewModel(TimeSpan.FromHours(2.5)),
                new DurationViewModel(TimeSpan.FromHours(2)),
                new DurationViewModel(TimeSpan.FromHours(1.5)),
                new DurationViewModel(TimeSpan.FromHours(1)),
                new DurationViewModel(TimeSpan.FromHours(0.5)),
                new DurationViewModel(TimeSpan.FromHours(0)),
            };

            userSettings = clientEndpoint.GetUserSettings();
            var lastUsedDuration = userSettings.LastUsedDuration;
            if (lastUsedDuration.HasValue)
            {
                SelectedDuration = Durations.FirstOrDefault(x => x.Duration.HasValue && x.Duration.Value.TotalHours == lastUsedDuration.Value);
            }
            else
            {
                SelectedDuration = Durations[0];
            }
            RaisePropertyChanged(() => SelectedDuration);
        }

        void AddPersonAsApplicator() {
            Messenger.Default.Send(new ShowPopupMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Secondary.PopUp.IncludeApplicatorView), "includeApplicator", IncludeAsApplicator)
			});
        }

        void AddCompany() {
            Messenger.Default.Send(new ShowPopupMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Secondary.PopUp.IncludeCompanyView), "includeCompany", IncludeCompany)
			});
        }


        internal void ResetData() {
            StartDate = DateTime.Today + TimeSpan.FromHours(8);
            CustomEndDate = DateTime.Today + TimeSpan.FromHours(10);
            CustomEndTime = DateTime.Today + TimeSpan.FromHours(10);
            AuthorizationDate = DateTime.Today + TimeSpan.FromHours(8);
            SelectedDuration = Durations[4];  // TODO: Maybe remember last used and default to it?
            availableTimingEventTags = new Dictionary<string, List<string>>();
            AvailableTimingEventTags = GetAvailableTimingEventTags(clientEndpoint);
            TimingEvent = null;
            TimingEventTag = null;
            var nameGenerationService = ServiceLocator.Get<INameAutoGenerationService>();
            Name = nameGenerationService.DisplayName;
            Notes = null;
            Authorizer = null;
        }
    }
}