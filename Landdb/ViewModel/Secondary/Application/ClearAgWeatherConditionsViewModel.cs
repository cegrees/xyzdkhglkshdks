﻿using GalaSoft.MvvmLight;
using Landdb.Client.Infrastructure;
using Landdb.Client.Infrastructure.DisplayItems;
using Landdb.Infrastructure;
using Landdb.ViewModel.Interfaces;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows.Threading;
using Landdb.Resources;
using NLog;
using ThinkGeo.MapSuite.Core;

namespace Landdb.ViewModel.Secondary.Application {
    public class ClearAgWeatherConditionsViewModel : ViewModelBase, IEnvironmentConditionsProperty {
        private readonly IClientEndpoint _clientEndpoint;

        private PointShape _geoLookUpPoint;
        private DateTime _applicationDate;
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public EnvironmentConditions EnvironmentConditions { get; set; }

        private readonly UserSettings _userSettings;

        public ClearAgWeatherConditionsViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, int cropYear) {
            EnvironmentConditions = new EnvironmentConditions();
            _clientEndpoint = clientEndpoint;
            _userSettings = clientEndpoint.GetUserSettings();
        }

        public PointShape GeoLookupPoint {
            get => _geoLookUpPoint;
            set {
                if (_geoLookUpPoint != value) {
                    _geoLookUpPoint = value;
                    RaisePropertyChanged(() => GeoLookupPoint);
                    ParseSummaryData();
                }
            }
        }

        public DateTime ApplicationStartDate {
            get => _applicationDate;
            set {
                if (_applicationDate != value) {
                    _applicationDate = value;
                    if (_applicationDate < DateTime.Now) {
                        ParseSummaryData();
                    }
                }
            }
        }

        public bool IncludeWeatherDataByDefault {
            get => _userSettings.IncludeWeatherByDefault;
            set {
                if (_userSettings.IncludeWeatherByDefault != value) {
                    _userSettings.IncludeWeatherByDefault = value;
                    _clientEndpoint.SaveUserSettings();
                }

                ParseSummaryData();
            }
        }

        public void SetApplicationStartDate(DateTime appstartdate) {
            _applicationDate = appstartdate;
        }

        private async Task<string> GetDailySummaryWeatherData(string lookupPoint, DateTime startDate, DateTime endDate) {
            if (NetworkStatus.IsInternetAvailable()) {
                var clearAgUri = $"{RemoteConfigurationSettings.GetBaseUri()}api/conditions/conditionsbytimestamp";
                var action = $"?latlon={lookupPoint}&timeStamp={startDate.ToUniversalTime():yyyy-MM-ddTHH:mm:ssZ}";
                //clearAgUri = string.Format("{0}{1}", clearAgUri, action);
                var cloudClientFactory = new CloudClientFactory();
                var token = cloudClientFactory.ResolveStorageCredentials();

                var client = new HttpClient {BaseAddress = new Uri(clearAgUri)};
                client.DefaultRequestHeaders.Add("Accept", "application / json");
                await token.SetBearerTokenAsync(client);
                var response = client.GetStringAsync(action);
                response.Wait();
                return response.Result;
            }

            return string.Empty;
        }

        private async void ParseSummaryData() {
            try {
                bool startdatemin = ApplicationStartDate == DateTime.MinValue;
                bool startdatelessthannow = !(ApplicationStartDate < DateTime.Now);
                bool geolookuppointnull = GeoLookupPoint == null;
                bool includeweatherdata = !IncludeWeatherDataByDefault;

                if (startdatemin || startdatelessthannow || geolookuppointnull || includeweatherdata) {
                    EnvironmentConditions.Clear();
                    return;
                }

                string dateText = ApplicationStartDate.ToString("yyyyMMdd");
                var centerPoint = $"{GeoLookupPoint.Y},{_geoLookUpPoint.X}";
                string dataText = await GetDailySummaryWeatherData(centerPoint, ApplicationStartDate, ApplicationStartDate.AddHours(2));

                var weatherObj = JsonConvert.DeserializeObject<ResultsObject>(dataText).Results;

                //EnvironmentConditions.ResetWindSpeedUnit();
                //EnvironmentConditions.ResetTemperatureUnit();
                EnvironmentConditions.Temperature = (decimal?) weatherObj.Temperature.Value;
                EnvironmentConditions.Temperature = EnvironmentConditions.ConvertTemperature(TemperatureDisplayType.F, "from");
                EnvironmentConditions.WindSpeed = (decimal?) weatherObj.WindSpeed.Value;
                EnvironmentConditions.WindSpeed = EnvironmentConditions.ConvertSpeed(SpeedDisplayType.MPH, "from");
                EnvironmentConditions.RelativeHumidity = (decimal?) weatherObj.Humidity.Value;
                var windType = ClearAgWindDirectionCategorizer.Categorize(weatherObj.WindDirection);
                EnvironmentConditions.WindDirection = new ClearAgWindDirectionDisplayItem(windType);
                EnvironmentConditions.SkyCondition = new ClearAgSkyCoverDisplayItem(weatherObj.SkyCondition);
            } catch (Exception ex) {
                _logger.ErrorException("the summary data was not parsed correctly", ex);
            }
        }

        internal ApplicationConditions GetApplicationConditions() {
            // TODO: When server can handle decimal? values, uncomment this so we can get true nulls round-tripping.
            //return new ApplicationConditions((decimal?)windSpeed, "mph", windDirection, (decimal?)temperature, "F", skyCondition, (decimal?)humidity, "-1");
            return EnvironmentConditions.CreateApplicationConditionsFromCurrentWeatherInfo();
        }
    }

    public class ValueObject {
        public double Value { get; set; }
        public string Unit { get; set; }
    }

    public class WeatherObject {
        public ValueObject Temperature { get; set; }
        public ValueObject Humidity { get; set; }
        public ValueObject WindSpeed { get; set; }
        public string SkyCondition { get; set; }
        public string WindDirection { get; set; }
    }

    public class ResultsObject {
        public WeatherObject Results { get; set; }
    }
}