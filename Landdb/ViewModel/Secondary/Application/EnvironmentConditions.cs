﻿using GalaSoft.MvvmLight;
using Landdb.Behaviors;
using Landdb.Client.Infrastructure.DisplayItems;
using Landdb.Client.Localization;
using Landdb.Domain.ReadModels.Application;
using Landdb.Resources;
using Landdb.ViewModel.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using UnitConversion;

namespace Landdb.ViewModel.Secondary.Application
{
    public class EnvironmentConditions : ViewModelBase {
        //TODO: Further extract each piece into its own class which has a displayitem, a valueitem, and the relevant conversion methods
        private decimal? _temperature;
        public decimal? Temperature {
            get => _temperature.HasValue? Math.Round(_temperature.Value) : _temperature;
            set {
                _temperature = value;
                RaisePropertyChanged(() => Temperature);
            }
        }
        
        /// <param name="toConvert"></param>
        /// <param name="conversionDirection">Valid values are "from" and "to".</param>
        /// <returns></returns>
        public decimal? ConvertTemperature(TemperatureDisplayType toConvert, string conversionDirection) {
            decimal? calculatedTemperature = _temperature;
            if (!_temperatureUnit.Equals(toConvert) && _temperature.HasValue) {
                var unitConverter = new UnitConverter();
                var tempDescription = EnumListExtension.ReadDescription(_temperatureUnit.TemperatureType);
                var conversionTempDescription = EnumListExtension.ReadDescription(toConvert);

                if (conversionDirection.ToLower().Equals("from")) {
                    if (unitConverter.CanConvert(conversionTempDescription, tempDescription)) {
                        calculatedTemperature = unitConverter.Convert(_temperature.Value, conversionTempDescription, tempDescription);
                    }
                } else if (conversionDirection.ToLower().Equals("to")) {
                    if (unitConverter.CanConvert(tempDescription, conversionTempDescription)) {
                        calculatedTemperature = unitConverter.Convert(_temperature.Value, tempDescription, conversionTempDescription);
                    }
                }

            }

            return calculatedTemperature;
        }


        private TemperatureDisplayItem _temperatureUnit = DefaultTemperatureUnitForThisDatasource;
        public TemperatureDisplayItem TemperatureUnit { 
            get => TemperatureUnits.FirstOrDefault(x => x.Equals(_temperatureUnit));
            set {
                _temperatureUnit = value == null? _temperatureUnit : TemperatureUnits.FirstOrDefault(x => x.Equals(value));
                RaisePropertyChanged(() => TemperatureUnit);
            }
        }
        private static TemperatureDisplayItem DefaultTemperatureUnitForThisDatasource =>
            DataSourceCultureInformation.UsesImperialUnits ? new TemperatureDisplayItem(TemperatureDisplayType.F) : new TemperatureDisplayItem(TemperatureDisplayType.C);
        public static List<TemperatureDisplayItem> TemperatureUnits => TemperatureDisplayItem.AllAsList();
        public string TemperatureText => Temperature.HasValue ? $"{Temperature:N0}°" : string.Empty;
        
        public void ResetTemperatureUnit() {
            _temperatureUnit = DefaultTemperatureUnitForThisDatasource;
        }



        private decimal? _windSpeed;
        public decimal? WindSpeed {
            get => _windSpeed.HasValue? Math.Round(_windSpeed.Value) : _windSpeed;
            set {
                _windSpeed = value;
                RaisePropertyChanged(() => WindSpeed);
            }
        }

        //TODO: Combine all of the converter adapters
        /// <param name="toConvert"></param>
        /// <param name="conversionDirection">Valid values are "from" and "to".</param>
        /// <returns></returns>
        public decimal? ConvertSpeed(SpeedDisplayType toConvert, string conversionDirection) {
            decimal? calculatedSpeed = _windSpeed;
            if (!_windSpeedUnit.Equals(toConvert) && _windSpeed.HasValue) {
                var unitConverter = new UnitConverter();
                var tempDescription = _windSpeedUnit.Equals(SpeedDisplayType.MPH) ? "mile" : "kilometer";
                var conversionSpeedDescription = toConvert.Equals(SpeedDisplayType.MPH) ? "mile" : "kilometer";

                if (conversionDirection.ToLower().Equals("from")) {
                    if (unitConverter.CanConvert(conversionSpeedDescription, tempDescription)) {
                        calculatedSpeed = unitConverter.Convert(_windSpeed.Value, conversionSpeedDescription, tempDescription);
                    }
                } else if (conversionDirection.ToLower().Equals("to")) {
                    if (unitConverter.CanConvert(tempDescription, conversionSpeedDescription)) {
                        calculatedSpeed = unitConverter.Convert(_windSpeed.Value, tempDescription, conversionSpeedDescription);
                    }
                }

            }

            return calculatedSpeed;
        }


        private SpeedDisplayItem _windSpeedUnit = DefaultWindSpeedUnitForThisDatasource;
        public SpeedDisplayItem WindSpeedUnit {
            get => WindSpeedUnits.FirstOrDefault(x => x.Equals(_windSpeedUnit));
            set {
                _windSpeedUnit = value == null ? DefaultWindSpeedUnitForThisDatasource : WindSpeedUnits.FirstOrDefault(x => x.Equals(value));
                RaisePropertyChanged(() => WindSpeedUnit);
            }
        }
        private static SpeedDisplayItem DefaultWindSpeedUnitForThisDatasource => 
            DataSourceCultureInformation.UsesImperialUnits ? 
            new SpeedDisplayItem(SpeedDisplayType.MPH) :
            new SpeedDisplayItem(SpeedDisplayType.KMPH);
        public static List<SpeedDisplayItem> WindSpeedUnits => SpeedDisplayItem.AllAsList();

        public void ResetWindSpeedUnit() {
            _windSpeedUnit = DefaultWindSpeedUnitForThisDatasource;
        }



        private ClearAgWindDirectionDisplayItem _windDirection = WindDirections.FirstOrDefault(x => x.Equals(ClearAgWindType.Unknown));
        public ClearAgWindDirectionDisplayItem WindDirection {
            get => WindDirections.FirstOrDefault(x => x.Equals(_windDirection));
            set {
                _windDirection = value == null ? _windDirection : WindDirections.FirstOrDefault(x => x.Equals(value));
                RaisePropertyChanged(() => WindDirection);
            }
        }
        public static List<ClearAgWindDirectionDisplayItem> WindDirections => ClearAgWindDirectionDisplayItem.AllAsList();
        public string WindText => WindSpeed.HasValue ? $"{WindSpeed.Value:N0} {WindSpeedUnit.DisplayText} {(WindDirection == null ? "" : WindDirection.DisplayText)}" : string.Empty;




        private ClearAgSkyCoverDisplayItem _skyCondition = SkyConditions.FirstOrDefault(x => x.Equals(ClearAgSkyCoverType.Unknown));
        public ClearAgSkyCoverDisplayItem SkyCondition {
            get => SkyConditions.FirstOrDefault(x => x.Equals(_skyCondition));
            set {
                _skyCondition = value == null? _skyCondition : SkyConditions.FirstOrDefault(x => x.Equals(value));
                RaisePropertyChanged(() => SkyCondition);
            }
        }
        public static List<ClearAgSkyCoverDisplayItem> SkyConditions => ClearAgSkyCoverDisplayItem.AllAsList();




        private decimal? _relativeHumidity;
        public decimal? RelativeHumidity {
            get => _relativeHumidity;
            set {
                _relativeHumidity = value;
                RaisePropertyChanged(() => RelativeHumidity);
            }
        }




        private SoilMoistureDisplayItem _soilMoistureDescriptor = new SoilMoistureDisplayItem(SoilMoistureDisplayType.Unknown);
        public SoilMoistureDisplayItem SoilMoistureDescriptor {
            get => SoilMoistureDescriptors.First(x => x.Equals(_soilMoistureDescriptor));
            set {
                _soilMoistureDescriptor = value == null? _soilMoistureDescriptor : SoilMoistureDescriptors.FirstOrDefault(x => x.Equals(value));
                RaisePropertyChanged(() => SoilMoistureDescriptor);
            }
        }
        public List<SoilMoistureDisplayItem> SoilMoistureDescriptors => SoilMoistureDisplayItem.AllAsList();


        /// <summary>
        /// Currently unused
        /// </summary>
        public List<string> BufferZoneList =>
            new List<string> {
                Strings.BufferZone_LerapA_Text,
                Strings.BufferZone_LerapB_Text,
                Strings.BufferZone_InterimAquatic_Text,
                Strings.BufferZone_SprayDrift_Text,
                Strings.BufferZone_BeeProtection_Text
            };




        public WeatherStationItem SelectedWeatherStation { get; set; }
        public ObservableCollection<WeatherStationItem> WeatherStations { get; private set; }

        public void Digest(IEnvironmentConditionsProperty conditionsModel) {
            var conditions = conditionsModel.EnvironmentConditions;
            Temperature = conditions.Temperature;
            TemperatureUnit = conditions.TemperatureUnit ?? TemperatureUnits.FirstOrDefault(x => x.Equals(TemperatureDisplayType.F)); //TODO: Should set the temp unit based on the culture
            WindSpeed = conditions.WindSpeed;
            WindSpeedUnit = conditions.WindSpeedUnit ?? WindSpeedUnits.FirstOrDefault(x => x.Equals(SpeedDisplayType.MPH));
            WindDirection = conditions.WindDirection;
            RelativeHumidity = conditions.RelativeHumidity;
            SkyCondition = conditions.SkyCondition;
            SoilMoistureDescriptor = conditions.SoilMoistureDescriptor ?? SoilMoistureDescriptors.FirstOrDefault(x => x.Equals(conditions.SoilMoistureDescriptor));
        }

        public ApplicationConditions CreateApplicationConditionsFromCurrentWeatherInfo(ApplicationView view = null, 
                                                                                       WeatherStationInformation weatherStation = null, 
                                                                                       DateTime? date = null,
                                                                                       bool? userDeviatedFromWeatherStationData = null) {
            if (view != null) {
                return new ApplicationConditions(WindSpeed,
                                                 WindSpeedUnit == null ? DefaultWindSpeedUnitForThisDatasource.KeyText : WindSpeedUnit.KeyText,
                                                 WindDirection.KeyText, 
                                                 Temperature,
                                                 TemperatureUnit == null ? DefaultTemperatureUnitForThisDatasource.KeyText : TemperatureUnit.KeyText, 
                                                 SkyCondition.DisplayText, 
                                                 RelativeHumidity,
                                                 SoilMoistureDescriptor.KeyText, 
                                                 view.Conditions?.WeatherStation,
                                                 view.Conditions?.WeatherStationTimestamp, 
                                                 true);
            } else {
                return new ApplicationConditions(WindSpeed, 
                                                 WindSpeedUnit == null? DefaultWindSpeedUnitForThisDatasource.KeyText : WindSpeedUnit.KeyText, 
                                                 WindDirection.KeyText, 
                                                 Temperature,
                                                 TemperatureUnit == null ? DefaultTemperatureUnitForThisDatasource.KeyText : TemperatureUnit.KeyText, 
                                                 SkyCondition.DisplayText, 
                                                 RelativeHumidity,
                                                 SoilMoistureDescriptor.KeyText, 
                                                 weatherStation, 
                                                 date, 
                                                 userDeviatedFromWeatherStationData);
            }
        }

        public void Clear() {
            Temperature = null;
            RelativeHumidity = null;
            WindSpeed = null;
            WindDirection = null;
            SkyCondition = null;
        }
    }
}