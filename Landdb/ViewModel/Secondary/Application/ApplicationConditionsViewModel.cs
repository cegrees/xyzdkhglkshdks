﻿using GalaSoft.MvvmLight;
using Landdb.Client.Infrastructure;
using Landdb.Client.Infrastructure.DisplayItems;
using Landdb.Resources;
using Newtonsoft.Json.Linq;
using NLog;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Windows.Threading;
using ThinkGeo.MapSuite.Core;

namespace Landdb.ViewModel.Secondary.Application {
    public class ApplicationConditionsViewModel : ViewModelBase {
        private readonly IClientEndpoint _clientEndpoint;
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly double _weatherStationLookupRadius = 32186.9; // 20 miles

        private PointShape _geoLookUpPoint;
        private DateTime _applicationDate;

        public EnvironmentConditions EnvironmentConditions { get; set; }

        private readonly UserSettings _userSettings;

        public ApplicationConditionsViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, int cropYear) {
            EnvironmentConditions = new EnvironmentConditions();
            _clientEndpoint = clientEndpoint;
            _userSettings = clientEndpoint.GetUserSettings();
        }

        public WeatherStationItem SelectedWeatherStation {
            get => EnvironmentConditions.SelectedWeatherStation;
            set {
                var oldValue = EnvironmentConditions.SelectedWeatherStation;
                EnvironmentConditions.SelectedWeatherStation = value;
                RaisePropertyChanged(() => SelectedWeatherStation, oldValue, EnvironmentConditions.SelectedWeatherStation, true);

                if (value == null) {
                    return;
                }

                if (value.Data == null && _geoLookUpPoint != null && _applicationDate != DateTime.MinValue) {
                    RetrieveStationDataAndSelect(value, _applicationDate);
                }

                if (value.Data != null) {
                    EnvironmentConditions.Temperature = value.Data.Temperature;
                    EnvironmentConditions.RelativeHumidity = value.Data.Humidity;
                    EnvironmentConditions.WindSpeed = value.Data.WindSpeed;
                    var windType = ClearAgWindDirectionCategorizer.Categorize(value.Data.WindDirection);
                    EnvironmentConditions.WindDirection = new ClearAgWindDirectionDisplayItem(windType);
                    EnvironmentConditions.SkyCondition = new ClearAgSkyCoverDisplayItem(value.Data.SkyCondition);
                }

                if (value.StationId != _userSettings.LastUsedWeatherStationId) {
                    // persist
                    _userSettings.LastUsedWeatherStationId = value.StationId;
                    _clientEndpoint.SaveUserSettings();
                }
            }
        }

        public PointShape GeoLookupPoint {
            get => _geoLookUpPoint;
            set {
                if (_geoLookUpPoint != value) {
                    _geoLookUpPoint = value;
                    RaisePropertyChanged(() => GeoLookupPoint);

                    BuildWeatherStationList(value, ApplicationDate);
                }
            }
        }

        public DateTime ApplicationDate {
            get => _applicationDate;
            set {
                if (_applicationDate != value) {
                    _applicationDate = value;

                    BuildWeatherStationList(GeoLookupPoint, value);
                }
            }
        }

        public bool IncludeWeatherDataByDefault {
            get => _userSettings.IncludeWeatherByDefault;
            set {
                if (_userSettings.IncludeWeatherByDefault != value) {
                    _userSettings.IncludeWeatherByDefault = value;
                    _clientEndpoint.SaveUserSettings();

                    if (value) {
                        if (GeoLookupPoint != null) {
                            BuildWeatherStationList(GeoLookupPoint, ApplicationDate);
                        }
                    } else {
                        // clear data
                        SelectedWeatherStation = null;
                        EnvironmentConditions.Clear();
                    }
                }
            }
        }

        private async void BuildWeatherStationList(PointShape lookupPoint, DateTime date) {
            if (!IncludeWeatherDataByDefault) {
                return;
            }

            try {
                if (date == DateTime.MinValue || lookupPoint == null) {
                    return;
                }

                string lookupPointText = $"{lookupPoint.Y:n1},{lookupPoint.X:n1}";
                string dateText = date.ToString("yyyyMMdd");
                string dataText = await GetSummaryWeatherData(lookupPointText, dateText);

                JObject data = JObject.Parse(dataText);

                int count = 0;
                var stations =
                    from s in
                        (from pws in data["location"]["nearby_weather_stations"]["pws"]["station"] where !string.IsNullOrWhiteSpace(pws["city"].Value<string>()) select pws)
                        .Union(from ap in data["location"]["nearby_weather_stations"]["airport"]["station"] where !string.IsNullOrWhiteSpace(ap["city"].Value<string>()) select ap)
                    let distance = lookupPoint.GetDistanceTo(new PointShape(s["lon"].Value<double>(), s["lat"].Value<double>()), GeographyUnit.DecimalDegree, DistanceUnit.Meter)
                    where distance <= _weatherStationLookupRadius
                    orderby distance
                    select new WeatherStationItem(s.SelectToken("id", false) == null ? s["icao"].Value<string>() : s["id"].Value<string>(), s.SelectToken("id", false) == null,
                                                  s["lat"].Value<double>(), s["lon"].Value<double>(), ++count,
                                                  s.SelectToken("neighborhood", false) == null ? "Airport" : s["neighborhood"].Value<string>());

                var summary = (from o in data["history"]["observations"]
                               let time = new DateTime(date.Year, date.Month, date.Day, int.Parse(o["date"]["hour"].Value<string>()), int.Parse(o["date"]["min"].Value<string>()), 0)
                               orderby (date - time).Duration()
                               //where int.Parse(o["date"]["hour"].Value<string>()) == date.Hour
                               select new WeatherStationData {
                                   Temperature = o["tempi"].Value<decimal>(),
                                   Humidity = o["hum"].Value<decimal>(),
                                   WindDirection = o["wdire"].Value<string>(),
                                   WindSpeed = o["wspdi"].Value<decimal>(),
                                   SkyCondition = o["conds"].Value<string>()
                               }).FirstOrDefault();

                EnvironmentConditions.WeatherStations.Clear();

                EnvironmentConditions.WeatherStations.Add(new WeatherStationItem("Summary", false, lookupPoint.Y, lookupPoint.X, 0, isSummary: true) {Data = summary});

                try {
                    stations.Take(10).ForEach(x => EnvironmentConditions.WeatherStations.Add(x));
                } catch (Exception ex) {
                    _logger.ErrorException("problems building the weather station list", ex);
                }

                if (!string.IsNullOrWhiteSpace(_userSettings.LastUsedWeatherStationId)) {
                    SelectedWeatherStation = EnvironmentConditions.WeatherStations.FirstOrDefault(x => x.StationId == _userSettings.LastUsedWeatherStationId);
                }

                if (SelectedWeatherStation == null) {
                    SelectedWeatherStation = EnvironmentConditions.WeatherStations.FirstOrDefault();
                }

                RaisePropertyChanged(() => EnvironmentConditions.WeatherStations);
            } catch (Exception ex) {
                _logger.ErrorException("problems building the weather station list", ex);
            }
        }

        private async void RetrieveStationDataAndSelect(WeatherStationItem station, DateTime date) {
            try {
                if (date == DateTime.MinValue || station == null) {
                    return;
                }

                string dateText = date.ToString("yyyyMMdd");
                string dataText = await GetStationWeatherData(station.StationId, station.IsAirport, dateText);

                JObject data = JObject.Parse(dataText);

                var stationData = (from o in data["history"]["observations"]
                                   let time = new DateTime(date.Year, date.Month, date.Day, int.Parse(o["date"]["hour"].Value<string>()), int.Parse(o["date"]["min"].Value<string>()), 0)
                                   orderby (date - time).Duration()
                                   //where int.Parse(o["date"]["hour"].Value<string>()) == date.Hour
                                   select new WeatherStationData {
                                       Temperature = o["tempi"] != null ? o["tempi"].Value<decimal>() : 0,
                                       Humidity = o["hum"] != null ? o["hum"].Value<decimal>() : 0,
                                       WindDirection = o["wdire"] != null ? o["wdire"].Value<string>() : string.Empty,
                                       WindSpeed = o["wspdi"] != null ? o["wspdi"].Value<decimal>() : 0,
                                       SkyCondition = o["conds"] != null ? o["conds"].Value<string>() : string.Empty
                                   }).FirstOrDefault();

                station.Data = stationData;
                if (station.Data != null) {
                    EnvironmentConditions.Temperature = station.Data.Temperature;
                    EnvironmentConditions.RelativeHumidity = station.Data.Humidity;
                    EnvironmentConditions.WindSpeed = station.Data.WindSpeed;
                    var windType = ClearAgWindDirectionCategorizer.Categorize(station.Data.WindDirection);
                    EnvironmentConditions.WindDirection = new ClearAgWindDirectionDisplayItem(windType);
                    EnvironmentConditions.SkyCondition = new ClearAgSkyCoverDisplayItem(station.Data.SkyCondition);
                }
            } catch (Exception ex) {
                _logger.ErrorException("station data not retrieved properly", ex);
            }
        }

        private static async Task<string> GetSummaryWeatherData(string lookupPoint, string date) {
            var requestUriFormat = @"http://api.wunderground.com/api/4d0c24e7c2ec562d/geolookup/history_{0}/q/{1}.json";
            var requestUri = string.Format(requestUriFormat, date, lookupPoint);

            WebClient client = new WebClient();
            string data = await client.DownloadStringTaskAsync(new Uri(requestUri));

            return data;
        }

        private static async Task<string> GetStationWeatherData(string stationName, bool isAirport, string date) {
            var formattedStationName = isAirport ? stationName : $"pws:{stationName}";
            var requestUriFormat = @"http://api.wunderground.com/api/4d0c24e7c2ec562d/history_{0}/q/{1}.json";
            var requestUri = string.Format(requestUriFormat, date, formattedStationName);

            WebClient client = new WebClient();
            string data = await client.DownloadStringTaskAsync(new Uri(requestUri));

            return data;
        }

        internal ApplicationConditions GetApplicationConditions() {
            // TODO: When server can handle decimal? values, uncomment this so we can get true nulls round-tripping.
            //return new ApplicationConditions((decimal?)windSpeed, "mph", windDirection, (decimal?)temperature, "F", skyCondition, (decimal?)humidity, "-1");
            return new ApplicationConditions((decimal?) EnvironmentConditions.WindSpeed.GetValueOrDefault(), 
                                             Strings.SpeedUnit_American, EnvironmentConditions.WindDirection.DisplayText,
                                             (decimal?) EnvironmentConditions.Temperature.GetValueOrDefault(), 
                                             Strings.TemperatureUnit_Text, EnvironmentConditions.SkyCondition.DisplayText,
                                             (decimal?) EnvironmentConditions.RelativeHumidity.GetValueOrDefault(),
                                             EnvironmentConditions.SoilMoistureDescriptor == null ? 
                                                 "-1" : 
                                                 EnvironmentConditions.SoilMoistureDescriptor.DisplayText);
        }
    }

    public class WeatherStationData {
        public decimal Temperature { get; set; }
        public decimal Humidity { get; set; }
        public string WindDirection { get; set; }
        public decimal WindSpeed { get; set; }
        public string SkyCondition { get; set; }
    }

    public class WeatherStationItem {
        public WeatherStationItem(string stationId, bool isAirport, double latitude, double longitude, int number, string neighborhood = "", bool isSummary = false) {
            StationId = stationId;
            IsAirport = isAirport;
            Latitude = latitude;
            Longitude = longitude;
            IsSummary = isSummary;
            Number = number;
            Neighborhood = neighborhood;
        }

        public string StationId { get; }
        public bool IsAirport { get; }
        public bool IsSummary { get; }
        public double Latitude { get; }
        public double Longitude { get; }
        public int Number { get; set; }
        public string Neighborhood { get; set; }

        public WeatherStationData Data { get; set; }

        public override string ToString() {
            if (Number > 0) {
                if (string.IsNullOrWhiteSpace(Neighborhood)) {
                    return $"{Number}. {StationId}";
                } else {
                    return $"{Number}. {StationId} - {Neighborhood}";
                }
            } else {
                return StationId;
            }
        }
    }
}