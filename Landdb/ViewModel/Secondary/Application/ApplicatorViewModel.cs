﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Client.Services;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Invoice;
using Landdb.Domain.ReadModels.Map;
using Landdb.Domain.ReadModels.Plan;
using Landdb.Domain.ReadModels.Recommendation;
using Landdb.Domain.ReadModels.TankInformationSetting;
using Landdb.Domain.ReadModels.WorkOrder;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Production;
using Landdb.ViewModel.Shared;
using Landdb.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Secondary.Application {
	public class ApplicatorViewModel : ValidationViewModelBase {

		readonly IClientEndpoint clientEndpoint;
		readonly Dispatcher dispatcher;

		readonly CropYearId currentCropYearId;

		private ApplicationId applicationId;

        private InvoiceView invoiceView = null;
        private WorkOrderView workOrderView = null;
        private RecommendationView recView = null;

		public ApplicatorViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, ApplicationId applicationId, int currentCropYear, ObservableCollection<ProductDetails> selectedPlanProducts, params DocumentDescriptor[] sourceDocuments) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			this.currentCropYearId = new CropYearId(applicationId.DataSourceId, currentCropYear);
            Enable = true;
			this.applicationId = applicationId;

			CancelCommand = new RelayCommand(OnCancelApplication);
			CompleteCommand = new RelayCommand(OnCompleteApplication);

			ReturnToMainViewCommand = new RelayCommand(ReturnToMainView);
			CreateNewApplicationCommand = new RelayCommand(CreateNewApplication);
			CreateApplicationCommand = new RelayCommand(CreateApplication);

			workOrderView = GetSourceDocumentView<WorkOrderView, WorkOrderId>(sourceDocuments, DocumentDescriptor.WorkOrderTypeString);
			invoiceView = GetSourceDocumentView<InvoiceView, InvoiceId>(sourceDocuments, DocumentDescriptor.InvoiceTypeString);
			recView = GetSourceDocumentView<RecommendationView, RecommendationId>(sourceDocuments, DocumentDescriptor.RecommendationTypeString);
			PlanView planView = GetSourceDocumentView<PlanView, CropPlanId>(sourceDocuments, DocumentDescriptor.PlanTypeString);
            ApplicationView appView = GetSourceDocumentView<ApplicationView, ApplicationId>(sourceDocuments, DocumentDescriptor.ApplicationTypeString);

			ConditionsModel = new ClearAgWeatherConditionsViewModel(clientEndpoint, dispatcher, currentCropYear);
			InfoPageModel = new InfoViewModel(clientEndpoint, dispatcher, ConditionsModel, currentCropYear, applicationId.DataSourceId);
			FieldsPageModel = new FieldSelectionViewModel(clientEndpoint, dispatcher, currentCropYearId, ConditionsModel, true);
			ProductsPageModel = new ProductSelectionViewModel(clientEndpoint, dispatcher, currentCropYearId, FieldsPageModel, true);
			DocumentsModel = new DocumentListPageViewModel(clientEndpoint, dispatcher, currentCropYear, sourceDocuments);

			FieldsPageModel.ErrorsChanged += (sender, args) => { RaisePropertyChanged(() => IsCompletionAllowed); };
			ProductsPageModel.ErrorsChanged += (sender, args) => { RaisePropertyChanged(() => IsCompletionAllowed); };

			//GET TANK INFORMATION
			var tankInfo = clientEndpoint.GetView<TankInformationView>(currentCropYearId).GetValue(() => null);
			if (tankInfo != null) {
				ProductsPageModel.TankInfo.TankSizeText = tankInfo.TankSizeValue.ToString("N4");
				ProductsPageModel.TankInfo.CarrierPerAreaText = tankInfo.CarrierPerAreaValue.ToString("N4");
			}

			if (invoiceView != null) {
				InfoPageModel.AssociatedInvoiceView = invoiceView;
				FieldsPageModel.AssociatedInvoiceView = invoiceView;
				ProductsPageModel.AssociatedInvoiceView = invoiceView;
			} else if (workOrderView != null) {
				InfoPageModel.AssociatedWorkOrderView = workOrderView;
				FieldsPageModel.AssociatedWorkOrderView = workOrderView;
				ProductsPageModel.AssociatedWorkOrderView = workOrderView;
			} else if (recView != null) {
				InfoPageModel.AssociatedRecommendationView = recView;
				FieldsPageModel.AssociatedRecommendationView = recView;
				ProductsPageModel.AssociatedRecommendationView = recView;
			} else if (planView != null) {
				InfoPageModel.AssociatedPlanView = planView;
				FieldsPageModel.AssociatedPlanView = planView;
				ProductsPageModel.SetSelectedPlanProducts(selectedPlanProducts);
				ProductsPageModel.AssociatedPlanView = planView;

                //TO DO :: SET TIMING EVENT BASED OFF OF SELECTEDPLANPRODUCTS
                var time = selectedPlanProducts[0].TimingEvent;
                var timingCnt = (from prod in selectedPlanProducts
                                 where prod.TimingEvent == time
                                 select prod).Count();
                if (timingCnt == selectedPlanProducts.Count())
                {
                    InfoPageModel.TimingEvent = InfoPageModel.TimingEvents.FirstOrDefault(x => x.Name == time);
                }
                else
                {
                    InfoPageModel.TimingEvent = null;
                }
            }
            else if (appView != null)
            {
                ProductsPageModel.AssociatedApplicationView = appView;
            }

            if (workOrderView != null || invoiceView != null || recView != null || planView != null || appView != null)
            {
                //InfoPageModel.StartDate = DateTime.Now - TimeSpan.FromMinutes(15);
                ConditionsModel.SetApplicationStartDate(InfoPageModel.StartDate);
                CheckCurrentWeatherForSelectedCropzones();
            }
        }

        private T GetSourceDocumentView<T, TId>(DocumentDescriptor[] sourceDocuments, string typeString) where TId : AbstractDataSourceIdentity<Guid, Guid> {
			var q = from d in sourceDocuments
					where d.DocumentType == typeString
					select d;
			return q.Any() ? clientEndpoint.GetView<T>((TId)q.First().Identity).GetValue(() => default(T)) : default(T);  // The lengths I'll go to for a one-line solution....
		}

		public ICommand CancelCommand { get; }
		public ICommand CompleteCommand { get; }

		public ICommand ReturnToMainViewCommand { get; }
		public ICommand CreateNewApplicationCommand { get; }
		public ICommand CreateApplicationCommand { get; }

		public InfoViewModel InfoPageModel { get; }
		public FieldSelectionViewModel FieldsPageModel { get; }
		public ProductSelectionViewModel ProductsPageModel { get; }
		public ClearAgWeatherConditionsViewModel ConditionsModel { get; }
		public DocumentListPageViewModel DocumentsModel { get; }

		public string TitleDisplay => Strings.CreateApplication_Text;
		public int CropYear => currentCropYearId.Id;
		public bool Enable { get; set; }

        private int previousPageIndex;
		private int selectedPageIndex;
		public int SelectedPageIndex {
			get { return selectedPageIndex; }
			set {

                previousPageIndex = selectedPageIndex;
				selectedPageIndex = value;
				RaisePropertyChanged(() => SelectedPageIndex);

                if (previousPageIndex == 1)
                {
                    CheckCurrentWeatherForSelectedCropzones();
                }
            }
		}

        public void CheckCurrentWeatherForSelectedCropzones()
        {
            if (FieldsPageModel.SelectedCropZones.Any())
            {
                var czs = FieldsPageModel.SelectedCropZones;

                //get center lat lon of the included fields then set conditions lookuppoint
                //string mapData = null;
                List<string> mapShapeStrings = new List<string>();
                foreach (var cz in czs)
                {
                    var mapItem = clientEndpoint.GetView<ItemMap>(cz.Id);

                    if (mapItem.HasValue && mapItem.Value.MostRecentMapItem != null && mapItem.Value.MostRecentMapItem.MapData != "MULTIPOLYGON EMPTY")
                    {
                        var mapString = mapItem.Value.MostRecentMapItem.MapData;
                        mapString = mapString.Substring(13, (mapString.Length - 14));
                        mapShapeStrings.Add(mapString);
                        //mapData += mapItem.Value.MostRecentMapItem.MapData;
                    }
                }


                if (mapShapeStrings.Any())
                {
                    var multiString = string.Join(",", mapShapeStrings.ToArray());
                    var multiPolygonString = string.Format("Multipolygon({0})", multiString);
                    var shape = ThinkGeo.MapSuite.Core.PolygonShape.CreateShapeFromWellKnownData(multiPolygonString);
                    var center = shape.GetCenterPoint();
                    ConditionsModel.GeoLookupPoint = center;
                    //conditionsModel.GeoLookupPoint = center; // string.Format("{0},{1}", center.Y.ToString("n1"), center.X.ToString("n1"));
                }
            }
        }

        public bool IsCompletionAllowed {
			get { return !(FieldsPageModel.HasErrors || ProductsPageModel.HasErrors); }
		}

		private void ResetData() {
			applicationId = new ApplicationId(applicationId.DataSourceId, Guid.NewGuid());
			SelectedPageIndex = 0;

			InfoPageModel.ResetData();
			FieldsPageModel.ResetData();
			ProductsPageModel.ResetData();
			//ConditionsModel.ResetData();
			DocumentsModel.ResetData();
		}

		private void CreateNewBasedOnPrevious() {
			applicationId = new ApplicationId(applicationId.DataSourceId, Guid.NewGuid());
			//InfoPageModel.Name = 
			SelectedPageIndex = 0;
		}

		void OnCompleteApplication() {
			if (!IsCompletionAllowed) { return; }

			// I stubbed in the conditions properties here --ED
			ApplicationAuthorization appAuthorization = null;

			if (InfoPageModel.Authorizer != null && InfoPageModel.AuthorizationDate != null) {
				appAuthorization = new ApplicationAuthorization(InfoPageModel.Authorizer.Id, InfoPageModel.Authorizer.Name, (DateTime)InfoPageModel.AuthorizationDate);
			}

			if (InfoPageModel.SelectedDuration == null) {
				InfoPageModel.SelectedDuration = new DurationViewModel(null);
			}

			var endTime = (InfoPageModel.CustomEndDate.Value.Date + InfoPageModel.CustomEndTime.Value.TimeOfDay).ToUniversalTime();
			var startDate = (InfoPageModel.StartDate.Date + InfoPageModel.StartTime.TimeOfDay).ToUniversalTime();

			SetPreferredUnits();

			var createApplicationCommand = new CreateApplication(
				applicationId,
				new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId),
				currentCropYearId.Id,
				GetName(),
				startDate,
				InfoPageModel.SelectedDuration.Duration,
				GetTimingEvent(),
				InfoPageModel.SelectedDuration.IsCustom ? (DateTime?)endTime : null,
				GetNotes(),
				appAuthorization,
				InfoPageModel.SelectedApplicators.ToArray(),
				FieldsPageModel.GetCropZoneAreas(),
				ProductsPageModel.GetApplicationTankInformation(),
				GetApplicationStrategy(),
				ProductsPageModel.GetAppliedProducts(),
				ConditionsModel.GetApplicationConditions(),
				DocumentsModel.GetDocuments(),
				GetTimingEventTag()
			);

			clientEndpoint.SendOne(createApplicationCommand);

            if(InfoPageModel.IncludeAsApplicator.BufferWidth != null && InfoPageModel.IncludeAsApplicator.BufferWidth > 0)
            {
                //SEND UP BUFFER ZONE
                var addBufferZone = new ChangeApplicationEnvironmentalZoneTolerance(applicationId, clientEndpoint.GenerateNewMetadata(), InfoPageModel.IncludeAsApplicator.BufferWidth.Value, InfoPageModel.IncludeAsApplicator.BufferWidthUnit.Name, InfoPageModel.IncludeAsApplicator.SelectedBufferTolerance);
                clientEndpoint.SendOne(addBufferZone);
            }

			// Stress Testing code: Create 10,000 semi-dupes of this application
			// iow: you *probably* don't want to uncomment this. ;)
			// 
			//for (int i = 0; i < 10000; i++) {
			//    ApplicationId appId = new ApplicationId(ApplicationEnvironment.CurrentDataSourceId, Guid.NewGuid());
			//    DateTime start = new DateTime(2012, (i % 11) + 1, (i % 27) + 1, 8, 0, 0);
			//    var aps = ProductsPageModel.GetAppliedProducts();

			//    foreach (var ap in aps) { ap.TrackingId = Guid.NewGuid(); }

			//    var createApplicationCommand2 = new CreateApplication(appId, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), cropYear, string.Format("Dummy App #{0}", i), start, InfoPageModel.SelectedDuration.Duration, InfoPageModel.SelectedDuration.IsCustom ? (DateTime?)endTime : null, InfoPageModel.Notes,
			//        appAuthorization, InfoPageModel.SelectedApplicators.ToArray(), FieldsPageModel.GetCropZoneAreas(), ProductsPageModel.GetApplicationTankInformation(),
			//        GetApplicationStrategy(), aps, ConditionsModel.GetApplicationConditions());
			//    clientEndpoint.SendOne(createApplicationCommand2);
			//}

			Messenger.Default.Send(new ShowOverlayMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Secondary.Application.PostApplicationOverlay), "postApp", this)
			});
		}

		void SetPreferredUnits() {
			var products = ProductsPageModel.GetAppliedProducts();
			var strategy = GetApplicationStrategy();
			var settings = clientEndpoint.GetUserSettings();

			foreach (var p in products) {
				if (p is ApplicationProductByArea) {
					var prod = ((ApplicationProductByArea)p);

					if (!settings.PreferredRateUnits.ContainsKey(prod.ProductId.Id)) {
						settings.PreferredRateUnits.Add(prod.ProductId.Id, prod.RatePerAreaUnit);
						clientEndpoint.SaveUserSettings();
					} else if (settings.PreferredRateUnits.ContainsKey(prod.ProductId.Id) && settings.PreferredRateUnits[prod.ProductId.Id] != prod.RatePerAreaUnit) {
						settings.PreferredRateUnits[prod.ProductId.Id] = prod.RatePerAreaUnit;
						clientEndpoint.SaveUserSettings();
					}
				}

				if (p is ApplicationProductByTotal) {
					var prod = ((ApplicationProductByTotal)p);

					if (!settings.PreferredTotalUnits.ContainsKey(prod.ProductId.Id)) {
						settings.PreferredTotalUnits.Add(prod.ProductId.Id, prod.TotalProductUnit);
						clientEndpoint.SaveUserSettings();
					} else if (settings.PreferredTotalUnits.ContainsKey(prod.ProductId.Id) && settings.PreferredTotalUnits[prod.ProductId.Id] != prod.TotalProductUnit) {
						settings.PreferredTotalUnits[prod.ProductId.Id] = prod.TotalProductUnit;
						clientEndpoint.SaveUserSettings();
					}
				}

				if (p is ApplicationProductByTank) {
					var prod = ((ApplicationProductByTank)p);

					if (!settings.PreferredRatePerTankUnits.ContainsKey(prod.ProductId.Id)) {
						settings.PreferredRatePerTankUnits.Add(prod.ProductId.Id, prod.RatePerTankUnit);
						clientEndpoint.SaveUserSettings();
					} else if (settings.PreferredRatePerTankUnits.ContainsKey(prod.ProductId.Id) && settings.PreferredRatePerTankUnits[prod.ProductId.Id] != prod.RatePerTankUnit) {
						settings.PreferredRatePerTankUnits[prod.ProductId.Id] = prod.RatePerTankUnit;
						clientEndpoint.SaveUserSettings();
					}
				}
			}
		}

		// TODO: Externalize all this command-creation logic. It doesn't feel right to be here.
		ProductApplicationStrategy GetApplicationStrategy() {
			switch (ProductsPageModel.SelectedApplicationType) {
				case ApplicationTypes.RatePerArea:
				default:
					return ProductApplicationStrategy.ByRatePerArea;
				case ApplicationTypes.TotalProduct:
					return ProductApplicationStrategy.ByTotalProduct;
				case ApplicationTypes.RatePerTank:
					return ProductApplicationStrategy.ByRatePerTank;
			}
		}

		void OnCancelApplication() {
			// TODO: Show UI to confirm cancel. Be nice to only do this if anything's actually been entered. Dirty tracking?
			Cleanup();

			Messenger.Default.Send(new ShowMainViewMessage());
		}

		#region Post Overlay
		void ReturnToMainView() {
			Cleanup();

			Messenger.Default.Send(new ApplicationsAddedMessage() { LastApplicationAdded = applicationId });
			Messenger.Default.Send(new ShowMainViewMessage());
			FieldsPageView view = new FieldsPageView();
		}

		void CreateApplication() {
			Messenger.Default.Send<ApplicationsAddedMessage>(new ApplicationsAddedMessage() { LastApplicationAdded = applicationId });
			applicationId = new ApplicationId(applicationId.DataSourceId, Guid.NewGuid());
			var nameGenerationService = ServiceLocator.Get<INameAutoGenerationService>();
            var carrierPerArea = ProductsPageModel.TankInfo.CarrierPerArea.Value;
            var tankSize = ProductsPageModel.TankInfo.TankSize.Value;
            FieldsPageModel.ResetData();

			InfoPageModel.Name = nameGenerationService.DisplayName;
			SelectedPageIndex = 0;

            if (invoiceView != null)
            {
                ProductsPageModel.AssociatedInvoiceView = invoiceView;
            }else if (workOrderView != null) {
				ProductsPageModel.AssociatedWorkOrderView = workOrderView;
			} else if (recView != null) {
				ProductsPageModel.AssociatedRecommendationView = recView;
            }
            else {
                ProductsPageModel.TankInfo.CarrierPerAreaText = carrierPerArea.ToString();
                ProductsPageModel.TankInfo.TankSizeText = tankSize.ToString();
            }
            
			Messenger.Default.Send(new HideOverlayMessage());
		}

		void CreateNewApplication() {
			Messenger.Default.Send(new ApplicationsAddedMessage() { LastApplicationAdded = applicationId });
			ResetData();
			Messenger.Default.Send(new HideOverlayMessage());
		}

		public string SourceContextDescription {
			get {
				var retString = Strings.ThisOne_Text.ToLower();

				if (DocumentsModel.ListItems.Any()) {
					var sourceDoc = DocumentsModel.ListItems.First();
				    var sourceDocTypeDisplay = sourceDoc.DocumentType.DisplayText;
					retString = $"{sourceDocTypeDisplay} \"{sourceDoc.Name}\"";
				}

				return retString;
			}
		}
		#endregion

		void onTotalAreaChanged() {
			ProductsPageModel.TankInfo.CurrentArea = FieldsPageModel.TotalArea;
		}

		string GetName() {
			if (!string.IsNullOrEmpty(InfoPageModel.Name)) {
				return InfoPageModel.Name;
			} else {
				return ProductsPageModel.CreateNameFromProducts(); // Is there anything better to create a name from?
			}
		}

		string GetNotes() {
			if (!string.IsNullOrEmpty(InfoPageModel.Notes)) {
				return InfoPageModel.Notes;
			} else { return string.Empty; }
		}

		string GetTimingEvent() {
			if (InfoPageModel.TimingEvent != null) {
				return InfoPageModel.TimingEvent.Name;
			} else { return string.Empty; }
		}

		string GetTimingEventTag() {
			if (InfoPageModel.TimingEventTag != null) {
				return InfoPageModel.TimingEventTag;
			} else { return string.Empty; }
		}

		public override void Cleanup() {
			base.Cleanup();

			InfoPageModel.Cleanup();
			FieldsPageModel.Cleanup();
			ProductsPageModel.Cleanup();
			ConditionsModel.Cleanup();
			DocumentsModel.Cleanup();

			if (ProductsPageModel != null) {
				if (ProductsPageModel.PestList != null) {
					ProductsPageModel.PestList.Clear();
				}
				if (ProductsPageModel.Products != null) {
					ProductsPageModel.Products.Clear();
				}
			}
		}
	}
}