﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AgC.UnitConversion;

namespace Landdb.ViewModel.Secondary.Application {
    // TODO: Place in a more common namespace
    public class UnitPerAreaHelper {
        public UnitPerAreaHelper(IUnit unit, IUnit areaUnit) {
            Unit = unit;
            AreaUnit = areaUnit;
        }

        public IUnit Unit { get; set; }
        public IUnit AreaUnit { get; set; }

		public string FormattedString => $"{Unit.AbbreviatedDisplay} / {AreaUnit.AbbreviatedDisplay}";

		public override string ToString() => FormattedString;
	}
}
