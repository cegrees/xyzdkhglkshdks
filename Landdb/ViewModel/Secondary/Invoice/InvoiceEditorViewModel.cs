﻿using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Invoice;
using Landdb.Domain.ReadModels.Product;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Shared;
using NLog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Secondary.Invoice {
	public class InvoiceEditorViewModel : ValidationViewModelBase {

		readonly IClientEndpoint clientEndpoint;
		readonly Dispatcher dispatcher;

		readonly Logger log = LogManager.GetCurrentClassLogger();

		InvoiceId invoiceId;
		DateTime invoiceDate = DateTime.Today;
		DateTime? invoiceDueDate = null;
		string vendor = string.Empty;
		string invoiceNumber = string.Empty;
		string notes = null;
		double totalCost;

		InvoiceProductViewModel selectedInvoiceProduct;

		public InvoiceEditorViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, InvoiceId invoiceId, int cropYear) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			this.invoiceId = invoiceId;

			CropYear = cropYear;

			CancelCommand = new RelayCommand(OnCancelInvoice);
			CompleteCommand = new RelayCommand(OnCompleteInvoice);

			AddInvoiceProductCommand = new RelayCommand(AddInvoiceProduct);
			ReturnToMainViewCommand = new RelayCommand(ReturnToMainView);
			CreateNewInvoiceCommand = new RelayCommand(CreateNewInvoice);
			CreateApplicationCommand = new RelayCommand(CreateApplication);
			ToggleNotesCommand = new RelayCommand(ToggleNotes);

			BuildProductList();
			BuildVendorList();

			InvoiceProducts = new ObservableCollection<InvoiceProductViewModel>();
			AddInvoiceProductCommand.Execute(null); // Adds the first product
			DeleteInvoiceProductCommand = new RelayCommand(DeleteProduct);
			ShowNotesPopup = false;

			ErrorsChanged += (sender, args) => { RaisePropertyChanged(() => IsCompletionAllowed); };

			ValidateViewModel();
		}

		public ICommand CancelCommand { get; }
		public ICommand CompleteCommand { get; }

		public ICommand ReturnToMainViewCommand { get; }
		public ICommand CreateNewInvoiceCommand { get; }
		public ICommand CreateApplicationCommand { get; }

		public ICommand AddInvoiceProductCommand { get; }
		public ICommand DeleteInvoiceProductCommand { get; }

		public ICommand ToggleNotesCommand { get; }

		public ObservableCollection<ProductListItemDetails> Products { get; private set; }
		public ObservableCollection<string> VendorNames { get; private set; }

		[CustomValidation(typeof(InvoiceEditorViewModel), nameof(ValidateInvoiceProducts))]
		public ObservableCollection<InvoiceProductViewModel> InvoiceProducts { get; }

		public bool ShowNotesPopup { get; set; }

		public int CropYear { get; }

		public bool IsCompletionAllowed => !HasErrors;

        [Required(AllowEmptyStrings = false, ErrorMessageResourceName = "InvoiceNumberIsRequired_Text", ErrorMessageResourceType = typeof(Strings))]
        public string InvoiceNumber {
			get { return invoiceNumber; }
			set {
				invoiceNumber = value;
				ValidateAndRaisePropertyChanged(() => InvoiceNumber);
			}
		}

		[CustomValidation(typeof(InvoiceEditorViewModel), nameof(ValidateInvoiceDate))]
		public DateTime InvoiceDate {
			get { return invoiceDate; }
			set {
				invoiceDate = value;
				ValidateAndRaisePropertyChanged(() => InvoiceDate);
			}
		}

		public DateTime? InvoiceDueDate {
			get { return invoiceDueDate; }
			set {
				invoiceDueDate = value;
				RaisePropertyChanged(() => InvoiceDueDate);
			}
		}

        [Required(AllowEmptyStrings = false, ErrorMessageResourceName = "VendorIsRequired_Text", ErrorMessageResourceType = typeof(Strings))]
        public string Vendor {
			get { return vendor; }
			set {
				vendor = value;
				ValidateAndRaisePropertyChanged(() => Vendor);
			}
		}

		public string Notes {
			get { return notes; }
			set {
				notes = value;
				RaisePropertyChanged(() => Notes);
			}
		}

		public double TotalCost {
			get { return totalCost; }
			private set {
				totalCost = value;
				RaisePropertyChanged(() => TotalCost);
			}
		}

		public InvoiceProductViewModel SelectedInvoiceProduct {
			get { return selectedInvoiceProduct; }
			set {
				selectedInvoiceProduct = value;
				RaisePropertyChanged(() => SelectedInvoiceProduct);
			}
		}

		void ResetData() {
			invoiceId = new InvoiceId(invoiceId.DataSourceId, Guid.NewGuid());

			InvoiceProducts.Clear();
			AddInvoiceProductCommand.Execute(null); // Adds the first product (again)
			Vendor = null;
			Notes = null;
			InvoiceNumber = null;
			TotalCost = 0;
		}

		void OnCompleteInvoice() {
			if (!string.IsNullOrEmpty(Vendor) && !string.IsNullOrEmpty(invoiceNumber)) {
				var createInvoiceCommand = new CreateInvoice(
					invoiceId,
					clientEndpoint.GenerateNewMetadata(),
					CropYear, 
					invoiceDate, 
					invoiceNumber, 
					vendor, 
					notes, 
					GetProductEntries(), 
					new DocumentDescriptor[] { },
					InvoiceDueDate
				);

				clientEndpoint.SendOne(createInvoiceCommand);

				Messenger.Default.Send(new ShowOverlayMessage() {
					ScreenDescriptor = new ScreenDescriptor(typeof(Views.Secondary.Invoice.PostInvoiceOverlay), "postInv", this)
				});
			}

			//Messenger.Default.Send<ShowMainViewMessage>(new ShowMainViewMessage());
		}

		void OnCancelInvoice() {
			Messenger.Default.Send(new ShowMainViewMessage());
		}

		InvoiceProductEntry[] GetProductEntries() {
			List<InvoiceProductEntry> products = new List<InvoiceProductEntry>();

			foreach (var ip in InvoiceProducts) {
				var ipp = ip.ToInvoiceProductEntry();
				if (ipp == null) { continue; }
				products.Add(ipp);
			}

			return products.ToArray();
		}

		internal void RefreshCost() {
			TotalCost = (from ip in InvoiceProducts
						 where ip.SelectedProduct != null
						 select (double)ip.TotalCost).Sum();
		}

		void DeleteProduct() {
			InvoiceProducts.Remove(SelectedInvoiceProduct);

			ValidateViewModel();
		}

		void ToggleNotes() {
			ShowNotesPopup = ShowNotesPopup == true ? false : true;
			RaisePropertyChanged(() => ShowNotesPopup);
		}

		async void BuildProductList() {
			var watch = System.Diagnostics.Stopwatch.StartNew();
			await Task.Run(() => {
				var mpl = clientEndpoint.GetMasterlistService().GetProductList();
				var mur = clientEndpoint.GetView<ProductUsageView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, CropYear)).GetValue(new ProductUsageView());
				var ucp = clientEndpoint.GetView<UserCreatedProductList>(new DataSourceId(ApplicationEnvironment.CurrentDataSourceId)).GetValue(new UserCreatedProductList());

				Guid mlDsId = GlobalIdentifiers.GetMasterlistDataSourceId();
				var productGuids = mur.GetProductGuids();

				var q = from p in mpl
						let cont = productGuids.Contains(p.Id)
						select new ProductListItemDetails(p, mlDsId, cont ? Strings.UsedThisYear_Text : Strings.Unused_Text, cont ? (short)0 : (short)1);

				var ucpQ = from p in ucp.UserCreatedProducts
						   let cont = productGuids.Contains(p.Id)
						   select new ProductListItemDetails(p, ApplicationEnvironment.CurrentDataSourceId, cont ? Strings.UsedThisYear_Text : Strings.Unused_Text, cont ? (short)0 : (short)1);

				Products = new ObservableCollection<ProductListItemDetails>(q.Union(ucpQ).OrderBy(x => x.GroupSortOrder).ToList());
				RaisePropertyChanged(() => Products);
			});
			watch.Stop();
			log.Debug("Loading products took {0}ms", watch.Elapsed.TotalMilliseconds);
		}

		internal ProductListItemDetails AddTemporaryMiniProduct(MiniProduct product, ProductId productId, bool isUsed) {
			var p = new ProductListItemDetails(product, productId.DataSourceId, isUsed ? Strings.UsedThisYear_Text : Strings.Unused_Text, isUsed ? (short)0 : (short)1);
			Products.Add(p);
			return p;
		}

		void BuildVendorList() {
			VendorNames = new ObservableCollection<string>();
			RaisePropertyChanged(() => VendorNames);

			Task.Run(() => {
				var dsId = new DataSourceId(invoiceId.DataSourceId);
				var vendorListView = clientEndpoint.GetView<VendorListView>(dsId).GetValue(new VendorListView());
				var sortedFilteredVendors = from v in vendorListView.Vendors
											where v.IsActiveInCropYear(CropYear)
											orderby v.Name
											select v.Name;

				dispatcher.BeginInvoke(new Action(() => {
					sortedFilteredVendors.ForEach(x => VendorNames.Add(x));
				}));
			});
		}

		void AddInvoiceProduct() {
			var newInvoiceProduct = new InvoiceProductViewModel(clientEndpoint, dispatcher, this, invoiceId);
			InvoiceProducts.Add(newInvoiceProduct);
			SelectedInvoiceProduct = newInvoiceProduct;
		}

		void ReturnToMainView() {
			Messenger.Default.Send(new InvoicesAddedMessage() { LastInvoiceAdded = invoiceId });
			Messenger.Default.Send(new ShowMainViewMessage());
		}

		void CreateNewInvoice() {
			ResetData();
			Messenger.Default.Send(new HideOverlayMessage());
		}

		void CreateApplication() {
			Messenger.Default.Send(new HideOverlayMessage());
			Messenger.Default.Send(new InvoicesAddedMessage() { LastInvoiceAdded = invoiceId });

			var applicationId = new ApplicationId(ApplicationEnvironment.CurrentDataSourceId, Guid.NewGuid());

			DocumentDescriptor sourceDoc = new DocumentDescriptor() { DocumentType = DocumentDescriptor.InvoiceTypeString, Identity = this.invoiceId, Name = this.InvoiceNumber };

			var vm = new Secondary.Application.ApplicatorViewModel(clientEndpoint, dispatcher, applicationId, ApplicationEnvironment.CurrentCropYear, null, sourceDoc);
			Messenger.Default.Send(new ChangeScreenDescriptorMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Secondary.Application.ApplicatorView), "create application", vm)
			});
		}

		public static ValidationResult ValidateInvoiceDate(DateTime date, ValidationContext context) {
			ValidationResult result = null;
            var cropYear = ApplicationEnvironment.CurrentCropYear;
			if (date.Year > cropYear + 1) {
				result = new ValidationResult(Strings.DateIsTooFarInTheFuture_Text);
			} else if (date.Year < cropYear - 1) {
				result = new ValidationResult(Strings.DateIsTooFarInThePast_Text);
			}

			return result;
		}

		public static ValidationResult ValidateInvoiceProducts(ObservableCollection<InvoiceProductViewModel> products, ValidationContext context) {
			ValidationResult result = null;

			if (products == null || !products.Any()) {
				result = new ValidationResult(Strings.AtLeastOneProductIsRequired_Text);
			} else if (products.Any(x => x.HasErrors)) {
				result = new ValidationResult(Strings.AtLeastOneProductHasErrors_Text);
			}

			return result;
		}
	}
}