﻿using AgC.UnitConversion;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Secondary.UserCreatedProduct;
using Landdb.ViewModel.Shared;
using NLog;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Resources;
using Landdb.Client.Localization;

namespace Landdb.ViewModel.Secondary.Invoice {
	public class InvoiceProductViewModel : ValidationViewModelBase {

		private readonly IClientEndpoint clientEndpoint;
		private readonly Dispatcher dispatcher;

		private readonly InvoiceEditorViewModel parent;

		private InvoiceId invoiceId;

		Guid trackingId;
		ProductListItemDetails selectedProduct;
		decimal unitCost = 0m;
		decimal totalCost = 0m;
		decimal totalQuantityValue = 0m;
		IUnit totalQuantityUnit;

		bool isProductListVisible = false;

		Logger log = LogManager.GetCurrentClassLogger();

		public InvoiceProductViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, InvoiceEditorViewModel parent, InvoiceId invoiceId) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			this.parent = parent;
			this.invoiceId = invoiceId;

			AllowedUnits = new ObservableCollection<IUnit>();

			trackingId = Guid.NewGuid();

			ErrorsChanged += (sender, args) => {
				parent.ValidateViewModel();
			};

			CreateUserProductCommand = new RelayCommand(createUserProduct);
			ChangeUnitCostCommand = new RelayCommand(changeUnitCost);

			ValidateViewModel();
		}

		public ICommand CreateUserProductCommand { get; }
		public ICommand ChangeUnitCostCommand { get; }

		public ObservableCollection<IUnit> AllowedUnits { get; }

		public string ProductSearchText { get; set; }

		[CustomValidation(typeof(InvoiceProductViewModel), nameof(ValidateSelectedProduct))]
		public ProductListItemDetails SelectedProduct {
			get { return selectedProduct; }
			set {
				selectedProduct = value;
				ValidateAndRaisePropertyChanged(() => SelectedProduct);
				RaisePropertyChanged(() => DisplayName);

				UpdateAllowedUnits();

				//RecalculateItem();
			}
		}

		public string DisplayName {
			get {
				if (SelectedProduct == null) { return string.Empty; }
				return string.Format("{0} ({1})", SelectedProduct.Product.Name, SelectedProduct.Product.Manufacturer);
			}
		}

		public decimal UnitCost {
			get { return unitCost; }
			set {
				unitCost = value;
				RaisePropertyChanged(() => UnitCost);
			}
		}

		public decimal TotalCost {
			get { return totalCost; }
			set {
				totalCost = value;
				RaisePropertyChanged(() => TotalCost);

				parent.RefreshCost();

				if (totalQuantityValue != 0) {
					UnitCost = TotalCost / totalQuantityValue;
				} else {
					UnitCost = 0;
				}
			}
		}

		public decimal TotalQuantityValue {
			get { return totalQuantityValue; }
			set {
				totalQuantityValue = value;
				RaisePropertyChanged(() => TotalQuantityValue);

				if (totalQuantityValue != 0) {
					UnitCost = TotalCost / totalQuantityValue;
				} else {
					UnitCost = 0;
				}
			}
		}

		// TODO: Not sure why I need this instead of just binding to TotalQuantityUnit, but it doesn't seem to want to do the ToString calc for some reason.
		public string TotalQuantityUnitDisplay {
			get { return totalQuantityUnit == null ? string.Empty : totalQuantityUnit.ToString(); }
		}

        [Required(ErrorMessageResourceName = "UnitIsRequired_Text", ErrorMessageResourceType = typeof(Strings))]
        public IUnit TotalQuantityUnit {
			get { return totalQuantityUnit; }
			set {
				totalQuantityUnit = value;
				ValidateAndRaisePropertyChanged(() => TotalQuantityUnit);
				RaisePropertyChanged(() => TotalQuantityUnitDisplay);
			}

		}

		public bool IsProductListVisible {
			get { return isProductListVisible; }
			set {
				log.Info("HashCode {0} | IsProductListVisible {1}", GetHashCode(), value);
				isProductListVisible = value;
				RaisePropertyChanged(() => IsProductListVisible);
			}
		}

		void UpdateAllowedUnits() {
			AllowedUnits.Clear();
			TotalQuantityUnit = null;

			if (SelectedProduct == null) { return; }

			var psu = UnitFactory.GetPackageSafeUnit(SelectedProduct.Product.StdPackageUnit, SelectedProduct.Product.StdUnit, SelectedProduct.Product.StdFactor, SelectedProduct.Product.StdPackageUnit);
			var compatibleUnits = CompatibilityFactory.GetCompatibleUnits(psu, SelectedProduct.Product.Density);
			var invId = new InventoryId(ApplicationEnvironment.CurrentDataSourceId, new Tuple<ProductId, int>(SelectedProduct.ProductId, ApplicationEnvironment.CurrentCropYear));
            //var inventoryProduct = clientEndpoint.GetView<InventoryDetailView>(invId);

            foreach (var u in compatibleUnits) {
                if (DetermineDatasourceUnits.ShouldThisUnitShowInThisCulture(u.Name)) { 
                    AllowedUnits.Add(u);
                if (u == psu) {
                    TotalQuantityUnit = u;
                }
            }
        }


			// If we couldn't find it in the list while we were building it, pick the first one, if any.
			if (TotalQuantityUnit == null && AllowedUnits.Any()) { TotalQuantityUnit = AllowedUnits[0]; }
		}

		void OnProductCreated(MiniProduct product) {
			var pli = parent.AddTemporaryMiniProduct(product, new ProductId(ApplicationEnvironment.CurrentDataSourceId, product.Id), true);
			SelectedProduct = pli;
		}

		private void createUserProduct() {
			IsProductListVisible = false;
			ShowOverlayMessage msg = new ShowOverlayMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Secondary.UserCreatedProduct.UserProductCreatorView), "createProduct",
					new UserProductCreatorViewModel(clientEndpoint, ProductSearchText, OnProductCreated))
			};
			Messenger.Default.Send(msg);
			//ShowPopupMessage msg = new ShowPopupMessage() {
			//    ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Secondary.UserCreatedProduct.UserProductCreatorView", "createProduct",
			//        new UserProductCreatorViewModel(clientEndpoint, ProductSearchText, OnProductCreated))
			//};
			//Messenger.Default.Send<ShowPopupMessage>(msg);
		}

		private void changeUnitCost() {
			if (TotalQuantityValue == 0) { return; }

			var warningText = Strings.InOrderToAvoidSubtleRoundingErrorsDueToFractionsOfCents_Text;

			DialogFactory.ShowNumericChangeDialog(Strings.UnitPrice_Text, unitCost, "C2", warningText, newValue => {
				TotalCost = TotalQuantityValue * newValue;
			});
		}

		internal InvoiceProductEntry ToInvoiceProductEntry() {
			if (SelectedProduct == null || TotalQuantityUnit == null) { return null; }
			// TODO: Handle currency deviation
			return new InvoiceProductEntry(SelectedProduct.ProductId, trackingId, TotalQuantityValue, TotalQuantityUnit.Name, TotalCost, "USD", SelectedProduct.Product.Density);
		}

		public static ValidationResult ValidateSelectedProduct(ProductListItemDetails product, ValidationContext context) {
			ValidationResult result = null;

			if (product == null) {
				result = new ValidationResult(Strings.AProductIsRequired_Text);
			}

			return result;
		}
	}
}
