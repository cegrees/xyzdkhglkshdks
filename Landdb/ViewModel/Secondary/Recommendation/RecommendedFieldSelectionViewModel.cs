﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using AgC.UnitConversion;
using GalaSoft.MvvmLight;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Map;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.ViewModel.Fields;
using NLog;
using Landdb.Domain.ReadModels.Contract;
using Landdb.Client.Spatial;
using Microsoft.Maps.MapControl.WPF;
using Landdb.Domain.ReadModels.WorkOrder;
using Landdb.Domain.ReadModels.Invoice;
using Landdb.ViewModel.Shared;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure.Messages;
using System.Windows.Markup;
using Landdb.Domain.ReadModels.Plan;

namespace Landdb.ViewModel.Secondary.Recommendation {
    public class RecommendedFieldSelectionViewModel : BaseFieldSelectionViewModel {

        Measure selected;

        public RecommendedFieldSelectionViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, int cropYear) : base(clientEndpoint, dispatcher, cropYear) {

        }

        protected override void OnFieldCheckedChanged(CropZoneTreeItemViewModel treeItem) {

            if (treeItem.IsChecked.HasValue && !treeItem.IsChecked.Value) {
                var found = SelectedCropZones.Where(x => x.Id == (CropZoneId)treeItem.Id).ToList();
                found.ForEach(x => {
                    SelectedCropZones.Remove(x);
                    TotalArea -= x.SelectedArea;
                });
            }
        }

        [DependsOn("TotalArea")]
        public Measure TotalSelectedArea
        {
            get { return selected; }
            set
            {
                selected = value;
                RaisePropertyChanged("TotalSelectedArea", new NullMeasure(), selected, true);
            }
        }

        private PlanView associatedPlanView;
        public PlanView AssociatedPlanView {
            get { return associatedPlanView; }
            set {
                associatedPlanView = value;

                if (associatedPlanView != null) {
                    var czIds = from cz in associatedPlanView.CropZones
                                select cz.Id;

                    RootTreeItemModels.First().CheckMatches(x => {
                        return x is CropZoneTreeItemViewModel && czIds.Contains((CropZoneId)((CropZoneTreeItemViewModel)x).Id);
                    });

                    foreach (var scz in SelectedCropZones) {
                        // match up areas
                        var q = from cz in associatedPlanView.CropZones
                                where cz.Id == scz.Id
                                select cz;
                        var sourceCz = q.SingleOrDefault();
                        if (sourceCz != null) {
                            scz.ChangeSelectedArea(sourceCz.Area);
                        }
                    }
                }

                RaisePropertyChanged("AssociatedPlanView");
            }
        }
    }
}
