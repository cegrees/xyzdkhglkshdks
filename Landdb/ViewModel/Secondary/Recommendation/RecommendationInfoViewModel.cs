﻿using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Client.Services;
using Landdb.Domain.ReadModels.Company;
using Landdb.Domain.ReadModels.Person;
using Landdb.Domain.ReadModels.Plan;
using Landdb.Domain.ReadModels.Recommendation;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Secondary.PopUp;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Secondary.Recommendation {
	public class RecommendationInfoViewModel : ValidationViewModelBase {

		readonly IClientEndpoint clientEndpoint;
		readonly Dispatcher dispatcher;

		DateTime? proposedDate = DateTime.Today;
		DateTime? expirationDate = DateTime.Today.AddDays(30.0);
		DateTime createdDate = DateTime.Today;
		DateTime? authorizationDate = DateTime.Today;

		TimingEvent timingEvent;
		string timingEventTag;
		string selectedTimingEventTag;
		List<string> timingEventTags;
		Dictionary<string, List<string>> availableTimingEventTags;
		Dictionary<string, List<string>> tempTimingEventTags;
		Dictionary<string, List<string>> mergedTimingEventTags;
		string notes;
		string recTitle;
		string recNumber;
		PersonListItem authorizer;
		List<PersonListItem> authorizers;
		bool changeAuthDate;
		PlanView associatedPlanView;

		public RecommendationInfoViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, int cropYear, Guid dataSourceId) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			var dsId = new DataSourceId(dataSourceId);

			var personListView = clientEndpoint.GetView<PersonListView>(dsId).GetValue(() => null);
			if (personListView != null) {
				var sortedFilteredPersons = from p in personListView.Persons
											where p.IsActiveInCropYear(cropYear)
											orderby p.Name
											select p;

				authorizers = sortedFilteredPersons.ToList();
			}

			SelectedApplicators = new ObservableCollection<ApplicationApplicator>();

			availableTimingEventTags = new Dictionary<string, List<string>>();
			AvailableTimingEventTags = GetAvailableTimingEventTags(clientEndpoint);

			AddApplicatorCommand = new RelayCommand(AddPersonAsApplicator);
			AddCompanyCommand = new RelayCommand(AddCompany);

			IncludeAsApplicator = new IncludeApplicator(clientEndpoint, dispatcher, this.SelectedApplicators);
            IncludeAsApplicator.IsEdit = false;
			IncludeCompany = new IncludeApplicationCompany(clientEndpoint, dispatcher, this);

			var nameGenerationService = ServiceLocator.Get<INameAutoGenerationService>();

			RecTitle = nameGenerationService.DisplayName;
		}

		private Dictionary<string, List<string>> GetAvailableTimingEventTags(IClientEndpoint clientEndpoint) {
			Dictionary<string, List<string>> availableTimingEventTagz = new Dictionary<string, List<string>>();
			var plantagviewMaybe = clientEndpoint.GetView<RecommendationTagProjectionView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear));

			if (plantagviewMaybe.HasValue && plantagviewMaybe.Value.TagInfo != null) {
				foreach (var v in plantagviewMaybe.Value.TagInfo) {
					if (string.IsNullOrEmpty(v.TimingEvent)) { continue; }

					if (!availableTimingEventTagz.ContainsKey(v.TimingEvent)) {
						availableTimingEventTagz.Add(v.TimingEvent, new List<string>());
					}

					foreach (var x in v.Tags) {
						if (availableTimingEventTagz.ContainsKey(v.TimingEvent)) {
							if (!string.IsNullOrEmpty(x.Key) && !availableTimingEventTagz[v.TimingEvent].Contains(x.Key)) {
								availableTimingEventTagz[v.TimingEvent].Add(x.Key);
							}
						}
					}
				}
			}

			return availableTimingEventTagz;
		}

		public ICommand AddApplicatorCommand { get; private set; }
		public ICommand AddCompanyCommand { get; private set; }

		public IncludeApplicator IncludeAsApplicator { get; set; }
		public IncludeApplicationCompany IncludeCompany { get; set; }

		public DateTime? ProposedDate {
			get { return proposedDate; }
			set {
				if (proposedDate != value) {
					proposedDate = value;
					RaisePropertyChanged(() => ProposedDate);
				}
			}
		}

		public DateTime? ExpirationDate {
			get { return expirationDate; }
			set {
				if (expirationDate != value) {
					expirationDate = value;
					RaisePropertyChanged(() => ExpirationDate);
				}
			}
		}

		public DateTime CreatedDate {
			get { return createdDate; }
			set {
				if (createdDate != value) {
					createdDate = value;
					RaisePropertyChanged(() => CreatedDate);
				}
			}
		}

        [Required(ErrorMessageResourceName = "ErrorMessage_RecommendationIdRequired_Text", ErrorMessageResourceType = typeof(Strings))]
        public string RecTitle {
			get { return recTitle; }
			set {
				recTitle = value;
				ValidateAndRaisePropertyChanged(() => RecTitle);
			}
		}

		public string RecNumber {
			get { return recNumber; }
			set {
				recNumber = value;
				RaisePropertyChanged(() => RecNumber);
			}
		}

		public string Notes {
			get { return notes; }
			set {
				notes = value;
				RaisePropertyChanged(() => Notes);
			}
		}

		public ObservableCollection<ApplicationApplicator> SelectedApplicators { get; set; }
		public ObservableCollection<CompanyListItem> SelectedCompanies { get; set; }

		public List<PersonListItem> Authorizers {
			get { return authorizers; }
		}

		public PersonListItem Authorizer {
			get { return authorizer; }
			set {
				authorizer = value;
				RaisePropertyChanged(() => Authorizer);
			}
		}

		public DateTime? AuthorizationDate {
			get { return authorizationDate; }
			set {
				authorizationDate = value;
				RaisePropertyChanged(() => AuthorizationDate);
				changeAuthDate = false;
			}
		}

		public List<TimingEvent> TimingEvents {
			get { return clientEndpoint.GetMasterlistService().GetTimingEventList().OrderBy(x => x.Order).ToList(); }
		}

		public TimingEvent TimingEvent {
			get { return timingEvent; }
			set {
				timingEvent = value;
				SelectAppropriateTags();
				RaisePropertyChanged(() => TimingEvent);
			}
		}

		public Dictionary<string, List<string>> AvailableTimingEventTags {
			get { return availableTimingEventTags; }
			set {
				availableTimingEventTags = value;
				SelectAppropriateTags();
			}
		}

		public Dictionary<string, List<string>> TempTimingEventTags {
			get { return tempTimingEventTags; }
			set {
				tempTimingEventTags = value;
				SelectAppropriateTags();
				RaisePropertyChanged(() => TempTimingEventTags);
			}
		}

		public List<string> TimingEventTags {
			get {
				if (timingEventTags == null) {
					return new List<string>();
				}

				return timingEventTags.OrderBy(q => q).ToList();
			}
			set {
				timingEventTags = value;
				RaisePropertyChanged(() => TimingEventTags);
			}
		}

		public string TimingEventTag {
			get { return timingEventTag; }
			set {
				if (timingEventTag == value) { return; }
				timingEventTag = value;
				RaisePropertyChanged(() => TimingEventTag);
			}
		}

		public string SelectedTimingEventTag {
			get { return selectedTimingEventTag; }
			set {
				if (selectedTimingEventTag == value) { return; }
				selectedTimingEventTag = value;
				if (!string.IsNullOrEmpty(selectedTimingEventTag)) {
					timingEventTag = selectedTimingEventTag;
				}
				RaisePropertyChanged(() => SelectedTimingEventTag);
			}
		}

		private void SelectAppropriateTags() {
			if (timingEvent != null && !string.IsNullOrEmpty(timingEvent.Name)) {
				mergedTimingEventTags = availableTimingEventTags;
				if (tempTimingEventTags != null && tempTimingEventTags.Count > 0) {
					mergedTimingEventTags = availableTimingEventTags.Concat(tempTimingEventTags).GroupBy(d => d.Key).ToDictionary(d => d.Key, d => d.First().Value);
				}

				if (mergedTimingEventTags != null && mergedTimingEventTags.ContainsKey(timingEvent.Name)) {
					TimingEventTags = mergedTimingEventTags[timingEvent.Name];
				} else {
					TimingEventTags = new List<string>();
				}
			}
		}

		void AddPersonAsApplicator() {
			Messenger.Default.Send(new ShowPopupMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Secondary.PopUp.IncludeApplicatorView), "includeApplicator", IncludeAsApplicator)
			});
		}

		void AddCompany() {
			Messenger.Default.Send(new ShowPopupMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Secondary.PopUp.IncludeCompanyView), "includeCompany", IncludeCompany) });
		}

		internal void ResetData() {
			ProposedDate = DateTime.Today;
			ExpirationDate = DateTime.Today.AddDays(30.0);
			CreatedDate = DateTime.Today;
			AuthorizationDate = DateTime.Today;
			availableTimingEventTags = new Dictionary<string, List<string>>();
			AvailableTimingEventTags = GetAvailableTimingEventTags(clientEndpoint);
			TimingEvent = null;
			TimingEventTag = null;
			RecTitle = ServiceLocator.Get<INameAutoGenerationService>().DisplayName;
			Notes = null;
			Authorizer = null;
		}

		public PlanView AssociatedPlanView {
			get { return associatedPlanView; }
			set {
				associatedPlanView = value;
				if (associatedPlanView != null) {
					if (associatedPlanView.Products.Count > 0) {
						string timingeventname = associatedPlanView.Products.FirstOrDefault().TimingEvent;
						TimingEvent = (!string.IsNullOrEmpty(timingeventname)) ? TimingEvents.Where(x => x.Name == timingeventname).FirstOrDefault() : null;
						TimingEventTag = associatedPlanView.Products.FirstOrDefault().TimingEventTag;
					}

					if (!string.IsNullOrWhiteSpace(associatedPlanView.Notes)) {
						Notes += string.Format("{0}:\n--------------------------------------\n{1}", Strings.FromPlan_Text, associatedPlanView.Notes);
					}
				}

				RaisePropertyChanged(() => AssociatedPlanView);
			}
		}
	}
}