using AgC.UnitConversion;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.ProductSetting;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Secondary.Application;
using Landdb.ViewModel.Secondary.UserCreatedProduct;
using Landdb.ViewModel.Shared;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Landdb.Resources;
using Landdb.Client.Localization;

namespace Landdb.ViewModel.Secondary.Recommendation {
	public class RecommendedProductViewModel : ValidationViewModelBase {

		readonly IClientEndpoint clientEndpoint;

		readonly RecommendedProductSelectionViewModel parent;
		readonly FieldSelectionViewModel fieldSelectionModel;

		Guid trackingId;
		ApplicationTypes applyBy;
		ProductListItemDetails selectedProduct;

		// Rate per area values
		decimal ratePerAreaValue;
		UnitPerAreaHelper ratePerAreaUnitHelper;

		// Total product values
		decimal totalProductValue;
		IUnit totalProductUnit;

		// Rate per tank values
		decimal ratePerTankValue;
		IUnit ratePerTankUnit;

		decimal productAreaPercent = 1;
		Measure productArea;
		ApplicationMethod appMethod;

		IApplicationCalculationService applicationCalculationService;
		IRecommendationCommandService recommendationCommandService;

		bool isProductListVisible = false;
		bool isCreateProductPopupOpen = false;

		Measure totalArea;

		public RecommendedProductViewModel(IClientEndpoint clientEndpoint, RecommendedProductSelectionViewModel parent, FieldSelectionViewModel fieldSelectionModel, ApplicationTypes applyBy = ApplicationTypes.RatePerArea) {
			this.clientEndpoint = clientEndpoint;
			this.parent = parent;
			this.fieldSelectionModel = fieldSelectionModel;
			this.applyBy = applyBy;

			SelectedPest = null;
			this.applicationCalculationService = new ApplicationCalculationService();
			this.recommendationCommandService = new RecommendationCommandService();

			AllowedRateUnits = new ObservableCollection<UnitPerAreaHelper>();
			AllowedTotalUnits = new ObservableCollection<IUnit>();

			totalArea = fieldSelectionModel.TotalArea;
			ProductArea = totalArea;

			fieldSelectionModel.PropertyChanged += fieldSelectionModel_PropertyChanged;

			trackingId = Guid.NewGuid();

			//Task.Run(() =>
			//{
			//    PopulatePests();
			//});

            ViewCalculator = new RelayCommand(showSpecialityCalculatorPopup);
			CreateUserProductCommand = new RelayCommand(CreateUserProduct);

			ErrorsChanged += (sender, args) => { parent.ValidateViewModel(); };

			ValidateViewModel();
		}

		public RelayCommand CreateUserProductCommand { get; }
		public RelayCommand ViewCalculator { get; }

		public ObservableCollection<UnitPerAreaHelper> AllowedRateUnits { get; }
		public ObservableCollection<IUnit> AllowedTotalUnits { get; }

		private PestListItemDetails _selectedPest;
		[CustomValidation(typeof(RecommendedProductViewModel), nameof(ValidateSelectedPest))]
		public PestListItemDetails SelectedPest {
			get { return _selectedPest; }
			set {
				_selectedPest = value;
				ValidateProperty(() => SelectedPest);
			}
		}

		private string _pestSearchText;
		public string PestSearchText {
			get { return _pestSearchText; }
			set {
				_pestSearchText = value;
				ValidateProperty(() => SelectedPest);
			}
		}

		public string ProductSearchText { get; set; }

		public List<ApplicationMethod> ApplicationMethods {
			get {
				// return list
				return parent.ApplicationMethods;
			}
		}

		public ApplicationMethod ApplicationMethod {
			get { return appMethod; }
			set {
				appMethod = value;
				RaisePropertyChanged(() => ApplicationMethod);
			}
		}

		public ApplicationTypes ApplyBy {
			get { return applyBy; }
			set {
				applyBy = value;
				RaisePropertyChanged(() => ApplyBy);
			}
		}

		[CustomValidation(typeof(RecommendedProductViewModel), nameof(ValidateSelectedProduct))]
		public ProductListItemDetails SelectedProduct {
			get { return selectedProduct; }
			set {
				selectedProduct = value;
				ValidateAndRaisePropertyChanged(() => SelectedProduct);
				RaisePropertyChanged(() => DisplayName);
				RaisePropertyChanged(() => ApplicationMethod);

				RaisePropertyChanged(() => SelectedPest);
				UpdateAllowedUnits();

				if (value != null && value.Product != null && value.Product.ProductType == GlobalStrings.ProductType_Service && value.Product.StdPackageUnit == ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit) {
					RatePerAreaValue = 1; // default to 1 for acre-based services because they're almost always "1 acre". Should probably extend this to other area units as well, for intl.
				} else {
					RatePerAreaValue = 0;  // TODO: In the future, this should be the most recently used rate. We should also populate a list of the MR rates for this product.
				}

				RecalculateItem();

				parent.RefreshProductCount();

				if (value == null || value.Product == null) {
					IsCreateProductPopupOpen = true;
				}
			}
		}

		public string DisplayName {
			get {
				if (SelectedProduct == null) { return string.Empty; }
				return string.Format("{0} ({1})", SelectedProduct.Product.Name, SelectedProduct.Product.Manufacturer);
			}
		}

		public decimal RatePerAreaValue {
			get { return ratePerAreaValue; }
			set {
				ratePerAreaValue = value;
				RaisePropertyChanged(() => RatePerAreaValue);
				RaisePropertyChanged(() => RatePerAreaDisplay);
				if (applyBy == ApplicationTypes.RatePerArea) {
					RecalculateItem();
				}
			}
		}

		[CustomValidation(typeof(RecommendedProductViewModel), nameof(ValidateUnit))]
		public UnitPerAreaHelper SelectedRatePerAreaUnitHelper {
			get { return ratePerAreaUnitHelper; }
			set {
				ratePerAreaUnitHelper = value;
				RaisePropertyChanged(() => SelectedRatePerAreaUnitHelper);
				RaisePropertyChanged(() => RatePerAreaDisplay);
				if (applyBy == ApplicationTypes.RatePerArea) {
					RecalculateItem();
				}
			}
		}

		public decimal RatePerTankValue {
			get { return ratePerTankValue; }
			set {
				ratePerTankValue = value;
				RaisePropertyChanged(() => RatePerTankValue);
				RaisePropertyChanged(() => RatePerTankDisplay);
				if (applyBy == ApplicationTypes.RatePerTank) {
					RecalculateItem();
				}
			}
		}

		[CustomValidation(typeof(RecommendedProductViewModel), nameof(ValidateUnit))]
		public IUnit SelectedRatePerTankUnit {
			get { return ratePerTankUnit; }
			set {
				ratePerTankUnit = value;
				RaisePropertyChanged(() => SelectedRatePerTankUnit);
				RaisePropertyChanged(() => RatePerTankDisplay);
				if (applyBy == ApplicationTypes.RatePerTank) {
					RecalculateItem();
				}
			}
		}

		public string RatePerAreaDisplay {
			get {
				if (ratePerAreaUnitHelper == null) { return string.Empty; }
				var rateString = string.Format("{0} / {1}", ratePerAreaUnitHelper.Unit.GetMeasure((double)ratePerAreaValue, SelectedProduct.Product.Density).ToString(), ratePerAreaUnitHelper.AreaUnit.FullDisplay);
				return rateString;
			}
		}

		public string RatePerTankDisplay {
			get {
				if (ratePerTankUnit == null) { return string.Empty; }
				var rateString = ratePerTankUnit.GetMeasure((double)ratePerTankValue, SelectedProduct.Product.Density).ToString();
				return rateString;
			}
		}

		public decimal TotalProductValue {
			get { return totalProductValue; }
			set {
				totalProductValue = value;
				RaisePropertyChanged(() => TotalProductValue);
				RaisePropertyChanged(() => TotalProduct);
				if (applyBy == ApplicationTypes.TotalProduct) {
					RecalculateItem();
				}
			}
		}

		[CustomValidation(typeof(RecommendedProductViewModel), nameof(ValidateUnit))]
		public IUnit TotalProductUnit {
			get { return totalProductUnit; }
			set {
				totalProductUnit = value;
				RaisePropertyChanged(() => TotalProductUnit);
				RaisePropertyChanged(() => TotalProduct);
				if (applyBy == ApplicationTypes.TotalProduct) {
					RecalculateItem();
				}
			}
		}

		public Measure TotalProduct {
			get {
				if (totalProductUnit != null && !(TotalProductUnit is NullUnit) && SelectedProduct != null) {
					return totalProductUnit.GetMeasure((double)totalProductValue, SelectedProduct.Product.Density);
				} else {
					return null;
				}
			}
		}

		public decimal ProductAreaPercent {
			get { return productAreaPercent; }
			set {
				productAreaPercent = value;
				RaisePropertyChanged(() => ProductAreaPercent);

				RecalculateItem();
			}
		}

		// readonly property for displaying product area. Setter is only for conveniently raising the prop-changed event for the view
		public Measure ProductArea {
			get { return productArea; }
			private set {
				productArea = value;
				RaisePropertyChanged(() => ProductArea);
				RaisePropertyChanged(() => ProductAreaDisplayText);
			}
		}

		public string ProductAreaDisplayText {
			get { return $"{ProductArea.Value:N2} {ProductArea.Unit.AbbreviatedDisplay}"; }
			set {
				double parsedValue = 0;

				if (double.TryParse(value, out parsedValue)) {
					// this will fire off a recalc which will update the area
					ProductAreaPercent = totalArea.Value != 0 ? Convert.ToDecimal(parsedValue / totalArea.Value) : 1;
				}

				RaisePropertyChanged(() => ProductAreaDisplayText);
			}
		}

		public bool IsProductListVisible {
			get { return isProductListVisible; }
			set {
				isProductListVisible = value;
				RaisePropertyChanged(() => IsProductListVisible);
			}
		}

		public bool IsCreateProductPopupOpen {
			get { return isCreateProductPopupOpen; }
			set {
				isCreateProductPopupOpen = value;
				RaisePropertyChanged(() => IsCreateProductPopupOpen);
			}
		}

		void UpdateAllowedUnits() {
			AllowedRateUnits.Clear();
			AllowedTotalUnits.Clear();

			SelectedRatePerAreaUnitHelper = null;

			if (SelectedProduct == null) { return; }

			var psu = UnitFactory.GetPackageSafeUnit(SelectedProduct.Product.StdPackageUnit, SelectedProduct.Product.StdUnit, SelectedProduct.Product.StdFactor, SelectedProduct.Product.StdPackageUnit);
			var compatibleUnits = AgC.UnitConversion.CompatibilityFactory.GetCompatibleUnits(psu, SelectedProduct.Product.Density);

            /////////////////////////////set default units//////////////////////////////////////////////////////////////////////
            var defaultUnit = clientEndpoint.GetUserSettings().PreferredRateUnits.ContainsKey(SelectedProduct.ProductId.Id) ?
                (from u in compatibleUnits where u.Name == clientEndpoint.GetUserSettings().PreferredRateUnits[SelectedProduct.ProductId.Id] select u).FirstOrDefault() ?? psu
                : psu;
            var defaultTotalUnit = clientEndpoint.GetUserSettings().PreferredTotalUnits.ContainsKey(SelectedProduct.ProductId.Id) ?
                (from u in compatibleUnits where u.Name == clientEndpoint.GetUserSettings().PreferredTotalUnits[SelectedProduct.ProductId.Id] select u).FirstOrDefault() ?? psu
                : psu;

            var defaultRatePerTankUnit = clientEndpoint.GetUserSettings().PreferredRatePerTankUnits.ContainsKey(SelectedProduct.ProductId.Id) ?
                (from u in compatibleUnits where u.Name == clientEndpoint.GetUserSettings().PreferredRatePerTankUnits[SelectedProduct.ProductId.Id] select u).FirstOrDefault() ?? psu
                : psu;

            var prodSettingsId = new ProductSettingId(ApplicationEnvironment.CurrentDataSourceId, SelectedProduct.ProductId.Id);
            var prodSettings = clientEndpoint.GetView<ProductSettingsView>(prodSettingsId);
            if (prodSettings.HasValue) {
                var settings = prodSettings.Value;
                defaultUnit = string.IsNullOrEmpty(settings.RatePerAreaUnit) ? defaultUnit : UnitFactory.GetUnitByName(settings.RatePerAreaUnit);
                defaultTotalUnit = string.IsNullOrEmpty(settings.TotalUnit) ? defaultTotalUnit : UnitFactory.GetUnitByName(settings.TotalUnit);
                defaultRatePerTankUnit = string.IsNullOrEmpty(settings.RatePerTankUnit) ? defaultRatePerTankUnit : UnitFactory.GetUnitByName(settings.RatePerTankUnit);
            }
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            foreach (var u in compatibleUnits)
            {
                if (DetermineDatasourceUnits.ShouldThisUnitShowInThisCulture(u.Name))
                {
                    AllowedTotalUnits.Add(u);
                    var uh = new UnitPerAreaHelper(u, UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit));
                    AllowedRateUnits.Add(uh);

                    switch (ApplyBy)
                    {
                        case ApplicationTypes.RatePerArea:
                        default:
                            if (u == defaultUnit)
                            {
                                SelectedRatePerAreaUnitHelper = uh;
                            }
                            break;
                        case ApplicationTypes.RatePerTank:
                            if (u == defaultRatePerTankUnit)
                            {
                                SelectedRatePerTankUnit = u;
                            }
                            break;
                        case ApplicationTypes.TotalProduct:
                            if (u == defaultTotalUnit)
                            {
                                TotalProductUnit = u;
                            }
                            break;
                    }
                }
            }


   //         foreach (var u in compatibleUnits) {
			//	AllowedTotalUnits.Add(u);
			//	var uh = new UnitPerAreaHelper(u, UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit));
			//	AllowedRateUnits.Add(uh);
			//	if (u == psu) {
			//		switch (ApplyBy) {
			//			case ApplicationTypes.RatePerArea:
			//			case ApplicationTypes.RatePerTank:
			//			default:
			//				SelectedRatePerAreaUnitHelper = uh;
			//				SelectedRatePerTankUnit = u;
			//				break;
			//			case ApplicationTypes.TotalProduct:
			//				TotalProductUnit = u;
			//				break;
			//		}
			//	}
			//}

			// If we couldn't find it in the list while we were building it, pick the first one, if any.
			if (SelectedRatePerAreaUnitHelper == null && AllowedRateUnits.Any()) { SelectedRatePerAreaUnitHelper = AllowedRateUnits[0]; }

		}

		void fieldSelectionModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e) {
			if (e.PropertyName == nameof(fieldSelectionModel.TotalArea)) {
				totalArea = fieldSelectionModel.TotalArea;

				RecalculateItem();
			}
		}

		internal void RecalculateItem() {
			switch (ApplyBy) {
				case ApplicationTypes.RatePerTank:
					RecalculateByTank();
					break;
				case ApplicationTypes.TotalProduct:
					RecalculateByTotal();
					break;
				case ApplicationTypes.RatePerArea:
				default:
					RecalculateByArea();
					break;
			}

			ValidateProperty(() => SelectedRatePerAreaUnitHelper);
			ValidateProperty(() => SelectedRatePerTankUnit);
			ValidateProperty(() => TotalProductUnit);
		}

		void RecalculateByArea() {
			if (SelectedProduct == null || SelectedRatePerAreaUnitHelper == null) {
				TotalProductValue = 0;
				TotalProductUnit = new NullUnit();

				SelectedRatePerTankUnit = null;
				RatePerTankValue = 0m;
				return;
			}

			var rate = SelectedRatePerAreaUnitHelper.Unit.GetMeasure((double)RatePerAreaValue, SelectedProduct.Product.Density);
			ProductArea = fieldSelectionModel.TotalArea.Unit.GetMeasure(fieldSelectionModel.TotalArea.Value * (double)productAreaPercent);
			var calculation = applicationCalculationService.RecalculateByArea(
				SelectedProduct.Product,
				null,
				rate,
				ProductArea,
				parent.TankInfo == null ? 0m : parent.TankInfo.TankCount,
				AllowedTotalUnits
			);

			TotalProductValue = calculation.TotalProductValue;
			TotalProductUnit = calculation.TotalProductUnit;
			RatePerTankValue = calculation.RatePerTankValue;

			if (calculation.RatePerTankUnit != null) {
				SelectedRatePerTankUnit = calculation.RatePerTankUnit;
			}

		}

		void RecalculateByTotal() {
			if (SelectedProduct == null || TotalProductUnit == null) {
				SelectedRatePerAreaUnitHelper = null;
				RatePerAreaValue = 0m;

				SelectedRatePerTankUnit = null;
				RatePerTankValue = 0m;
				return;
			}

			var total = TotalProductUnit.GetMeasure((double)TotalProductValue, SelectedProduct.Product.Density);
			var productArea = fieldSelectionModel.TotalArea.Unit.GetMeasure(fieldSelectionModel.TotalArea.Value * (double)productAreaPercent);

			var calculation = applicationCalculationService.RecalculateByTotal(
				SelectedProduct.Product,
				total,
				productArea,
				parent.TankInfo == null ? 0m : parent.TankInfo.TankCount,
				AllowedTotalUnits,
				null
			);

			ProductArea = calculation.ProductArea;
			RatePerAreaValue = calculation.RatePerAreaValue;
			SelectedRatePerAreaUnitHelper = (from r in AllowedRateUnits where r.Unit.Name == calculation.PackageSafeUnit.Name select r).FirstOrDefault();
			RatePerTankValue = calculation.RatePerTankValue;

			if (calculation.RatePerTankUnit != null) {
				SelectedRatePerTankUnit = calculation.RatePerTankUnit;
			}

		}

		void RecalculateByTank() {
			if (SelectedProduct == null || SelectedRatePerTankUnit == null) {
				SelectedRatePerAreaUnitHelper = null;
				RatePerAreaValue = 0m;

				TotalProductValue = 0;
				TotalProductUnit = new NullUnit();
				return;
			}

			var ratePerTank = SelectedRatePerTankUnit.GetMeasure((double)RatePerTankValue, SelectedProduct.Product.Density);
			var productArea = fieldSelectionModel.TotalArea.Unit.GetMeasure(fieldSelectionModel.TotalArea.Value * (double)productAreaPercent);

			var calculation = applicationCalculationService.RecalculateByTank(
				SelectedProduct.Product,
				null,
				ratePerTank,
				productArea,
				parent.TankInfo.TankCount
			);

			ProductArea = calculation.ProductArea;
			TotalProductValue = calculation.TotalProductValue;
			TotalProductUnit = calculation.TotalProductUnit;
			RatePerAreaValue = calculation.RatePerAreaValue;

			SelectedRatePerAreaUnitHelper = (from r in AllowedRateUnits where r.Unit.Name == calculation.PackageSafeUnit.Name select r).FirstOrDefault();

		}

		void OnProductCreated(MiniProduct product) {
			var pli = parent.AddTemporaryMiniProduct(product, new ProductId(ApplicationEnvironment.CurrentDataSourceId, product.Id), true);
			SelectedProduct = pli;
		}


		internal string ToName() {
			if (SelectedProduct != null && SelectedProduct.Product != null) {
				return SelectedProduct.Product.Name;
			} else {
				return string.Empty;
			}
		}

		internal RecommendationCommandProductEntry ToRecommendationCommandProductEntry() {
			switch (applyBy) {
				case ApplicationTypes.RatePerArea:
				default:
					return ToByAreaRecommendationCommandProductEntry();
				case ApplicationTypes.TotalProduct:
					return ToByTotalRecommendationCommandProductEntry();
				case ApplicationTypes.RatePerTank:
					return ToByTankRecommendationCommandProductEntry();
			}
		}

		RecommendationProductByArea ToByAreaRecommendationCommandProductEntry() {
			if (SelectedProduct == null || ratePerAreaUnitHelper == null) { return null; }
			MiniPest pest = new MiniPest();
			if (SelectedPest != null) { pest = new MiniPest() { id = SelectedPest.PestId, Name = SelectedPest.CommonName, LatinList = SelectedPest.LatinName }; }
			return recommendationCommandService.ToByAreaRecommendationCommandProductEntry(
				SelectedProduct.Product,
				SelectedProduct.ProductId,
				trackingId,
				ratePerAreaValue,
				ratePerAreaUnitHelper.Unit.Name,
				ratePerAreaUnitHelper.AreaUnit.Name,
				ProductAreaPercent,
				(decimal)ProductArea.Value,
				ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit,
				SelectedProduct.Product.Density,
				ApplicationMethod,
				pest
			);
		}

		RecommendationProductByTotal ToByTotalRecommendationCommandProductEntry() {
			if (SelectedProduct == null || totalProductUnit == null || ratePerAreaUnitHelper == null) { return null; }
			MiniPest pest = new MiniPest();
			if (SelectedPest != null) { pest = new MiniPest() { id = SelectedPest.PestId, Name = SelectedPest.CommonName, LatinList = SelectedPest.LatinName }; }
			return recommendationCommandService.ToByTotalRecommendationCommandProductEntry(
				SelectedProduct.Product,
				SelectedProduct.ProductId,
				trackingId,
				totalProductValue,
				totalProductUnit.Name,
				ratePerAreaUnitHelper.AreaUnit.Name,
				ProductAreaPercent,
				(decimal)ProductArea.Value,
				ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit,
				SelectedProduct.Product.Density,
				ApplicationMethod,
				pest
			);
		}

		RecommendationProductByTank ToByTankRecommendationCommandProductEntry() {
			if (SelectedProduct == null || ratePerTankUnit == null || ratePerAreaUnitHelper == null) { return null; }
			MiniPest pest = new MiniPest();
			if (SelectedPest != null) { pest = new MiniPest() { id = SelectedPest.PestId, Name = SelectedPest.CommonName, LatinList = SelectedPest.LatinName }; }
			return recommendationCommandService.ToByTankRecommendationCommandProductEntry(
				SelectedProduct.Product,
				SelectedProduct.ProductId,
				trackingId,
				ratePerTankValue,
				ratePerTankUnit.Name,
				ratePerAreaUnitHelper.AreaUnit.Name,
				ProductAreaPercent,
				(decimal)ProductArea.Value,
				ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit,
				SelectedProduct.Product.Density,
				ApplicationMethod,
				pest
			);
		}

		private void showSpecialityCalculatorPopup() {
			if (SelectedProduct != null) {
				var calcVM = new AppliedProductCalculatorViewModel(clientEndpoint, this, parent.TankInfo);
				Messenger.Default.Send(new ShowPopupMessage() {
					ScreenDescriptor = new ScreenDescriptor(typeof(Views.Secondary.PopUp.AppliedProductSpecialityCalculator), "calculator", calcVM)
				});
			}
		}

        public void UpdateFromCalculator(Measure RatePerArea, Measure RatePerTank, Measure TotalProduct)
        {
            switch (applyBy)
            {
                case ApplicationTypes.RatePerArea:
                default:
                    RatePerAreaValue = (decimal)RatePerArea.Value;
                    SelectedRatePerAreaUnitHelper = AllowedRateUnits.Where(x => x.Unit == RatePerArea.Unit).FirstOrDefault();
                    break;
                case ApplicationTypes.TotalProduct:
                    TotalProductValue = (decimal)TotalProduct.Value;
                    TotalProductUnit = TotalProduct.Unit;
                    break;
                case ApplicationTypes.RatePerTank:
                    RatePerTankValue = (decimal)RatePerTank.Value;
                    SelectedRatePerTankUnit = RatePerTank.Unit;
                    break;
            }
        }

		void CreateUserProduct() {
			IsProductListVisible = false;
			IsCreateProductPopupOpen = false;

			var vm = new UserProductCreatorViewModel(clientEndpoint, ProductSearchText, this.OnProductCreated);
			Messenger.Default.Send(new ShowOverlayMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Secondary.UserCreatedProduct.UserProductCreatorView), "createProduct", vm)
			});

			//ShowPopupMessage msg = new ShowPopupMessage() {
			//    ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Secondary.UserCreatedProduct.UserProductCreatorView", "createProduct",
			//        new UserProductCreatorViewModel(clientEndpoint, ProductSearchText, OnProductCreated))
			//};
			//Messenger.Default.Send<ShowPopupMessage>(msg);
		}

		#region Custom Validation
		public static ValidationResult ValidateSelectedProduct(ProductListItemDetails selectedProduct, ValidationContext context) {
			ValidationResult result = null;

			if (selectedProduct == null) {
				result = new ValidationResult(Strings.ValidationResult_ProductRequired_Text);
			}

			return result;
		}

		public static ValidationResult ValidateUnit(object unit, ValidationContext context) {
			ValidationResult result = null;

			var vm = (RecommendedProductViewModel)context.ObjectInstance;

			var validateRatePerArea = vm.ApplyBy == ApplicationTypes.RatePerArea && context.MemberName == "SelectedRatePerAreaUnitHelper";
			var validateRatePerTank = vm.ApplyBy == ApplicationTypes.RatePerTank && context.MemberName == "SelectedRatePerTankUnit";
			var validateTotalProduct = vm.ApplyBy == ApplicationTypes.TotalProduct && context.MemberName == "TotalProductUnit";

			var shouldValidate = validateRatePerArea || validateRatePerTank || validateTotalProduct;

			if (shouldValidate && unit == null) {
				result = new ValidationResult(Strings.ValidationResult_InvalidUnit_Text);
			}

			return result;
		}

		public static ValidationResult ValidateSelectedPest(PestListItemDetails selectedPest, ValidationContext context) {
			ValidationResult result = null;

			var vm = (RecommendedProductViewModel)context.ObjectInstance;

			if (!string.IsNullOrWhiteSpace(vm.PestSearchText) && selectedPest == null) {
				result = new ValidationResult(Strings.ValidationResult_InvalidPest_Text);
			}

			return result;
		}
		#endregion
	}
}