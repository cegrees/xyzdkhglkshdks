﻿using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Plan;
using Landdb.Domain.ReadModels.Product;
using Landdb.Infrastructure;
using Landdb.ViewModel.Production;
using Landdb.ViewModel.Secondary.Application;
using Landdb.ViewModel.Shared;
using NLog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Secondary.Recommendation {
	public class RecommendedProductSelectionViewModel : ValidationViewModelBase {

		readonly IClientEndpoint clientEndpoint;
		readonly Dispatcher dispatcher;

		FieldSelectionViewModel fieldSelection;
		readonly TankInformationModel tankInfo;
		readonly int cropYear;
		readonly Logger log = LogManager.GetCurrentClassLogger();

		RecommendedProductViewModel selectedRecommendedProduct;
		bool showTankInfoPopup;
		bool isTankApplication;

		Task productLoadTask;

		ApplicationTypes selectedApplicationType;
		ApplicationTypes[] applicationTypesList = new ApplicationTypes[] { ApplicationTypes.RatePerArea, ApplicationTypes.TotalProduct, ApplicationTypes.RatePerTank };

		ApplicationMethod appMethod;

		public RecommendedProductSelectionViewModel(FieldSelectionViewModel fieldSelection, IClientEndpoint clientEndpoint, Dispatcher dispatcher, int cropYear, Guid dataSourceId) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			this.fieldSelection = fieldSelection;
			this.cropYear = cropYear;

			Task.Run(() => {
				PopulatePests();
				BuildProductList();
			});

			this.tankInfo = new TankInformationModel(clientEndpoint, fieldSelection, OnTankInfoChanged);

			RecommendedProducts = new ObservableCollection<RecommendedProductViewModel>();
			BindingOperations.EnableCollectionSynchronization(RecommendedProducts, _appliedProductLock);

			ToggleTankInfoCommand = new RelayCommand(() => { ShowTankInfoPopup = !ShowTankInfoPopup; });
			AddRecommendedProductCommand = new RelayCommand(AddRecommendedProduct);
			RemoveRecommendedProductCommand = new RelayCommand(RemoveRecommendedProduct);

			AddRecommendedProductCommand.Execute(null); // Add the first product

			RecommendedProducts.CollectionChanged += (sender, args) => { ValidateAndRaisePropertyChanged(() => RecommendedProducts); };
		}

		private readonly object _appliedProductLock = new object();

		void DelayRunProductPopulationTask(Task loadProductsTask) {
			while (productLoadTask == null) {
				System.Threading.Thread.Sleep(50); // This is stupid, right?
			}

			if (productLoadTask != null) {
				if (productLoadTask.Status != TaskStatus.RanToCompletion) {
					productLoadTask.ContinueWith(x => {
						loadProductsTask.Start();
					});
				} else {
					loadProductsTask.Start();
				}
			}
		}

		void PopulatePests() {
			var ml = clientEndpoint.GetMasterlistService();
			var q = from p in ml.GetPestList()
					//where p.Language == "en"
					orderby p.Name
					select new PestListItemDetails(p, 0);
			PestList = new ObservableCollection<PestListItemDetails>(q);
			RaisePropertyChanged(() => PestList);
		}

		public ICommand AddRecommendedProductCommand { get; private set; }
		public ICommand RemoveRecommendedProductCommand { get; private set; }

		public ICommand ToggleTankInfoCommand { get; private set; }

		public ObservableCollection<ProductListItemDetails> Products { get; private set; }
		public ObservableCollection<PestListItemDetails> PestList { get; private set; }

		[CustomValidation(typeof(RecommendedProductSelectionViewModel), nameof(ValidateProducts))]
		public ObservableCollection<RecommendedProductViewModel> RecommendedProducts { get; private set; }

		private PlanView associatedPlanView;
		public PlanView AssociatedPlanView {
			get { return associatedPlanView; }
			set {
				associatedPlanView = value;
				if (associatedPlanView != null) {
					Task loadProductsTask = new Task(() => {
						lock (RecommendedProducts) {
							RecommendedProducts.Clear();
							if (SelectedPlanProducts != null) {
								foreach (var p in SelectedPlanProducts) {
									var selectedProduct = (from pr in Products where pr.ProductId == p.ID select pr).FirstOrDefault();
									if (selectedProduct == null) { continue; } // TODO: Log that product couldn't be found.
									var newAppliedProduct = new RecommendedProductViewModel(clientEndpoint, this, fieldSelection, SelectedApplicationType) {
										ApplyBy = SelectedApplicationType,
										SelectedProduct = selectedProduct,
										RatePerAreaValue = (decimal)p.RateMeasure.Value,
										ProductAreaPercent = p.ProductAreaPercent,
									};
									newAppliedProduct.SelectedRatePerAreaUnitHelper = newAppliedProduct.AllowedRateUnits.Where(rpah => rpah.Unit.Name == p.RateMeasure.Unit.Name).FirstOrDefault();
									RecommendedProducts.Add(newAppliedProduct);
								}
							}
							if (RecommendedProducts.Any()) {
								SelectedRecommendedProduct = RecommendedProducts.Last();
							}
						}
						dispatcher.BeginInvoke(new Action(() => {
							RaisePropertyChanged(() => ProductCount);
						}));
					});
					DelayRunProductPopulationTask(loadProductsTask);
				}
				RaisePropertyChanged(() => AssociatedPlanView);
			}
		}

		/////////////////////////////////////////
		//        associatedPlanView = value;
		//        if (associatedPlanView != null) {
		//            Task loadProductsTask = new Task(() => {
		//                lock (Products) {
		//                    Products.Clear();
		//                    SelectedApplicationType = ApplicationTypes.RatePerArea;
		//                    if (SelectedPlanProducts != null) {
		//                        foreach (var p in SelectedPlanProducts) {
		//                            var selectedProduct = (from pr in Products where pr.ProductId == p.ID select pr).FirstOrDefault();
		//                            //var appMethod = clientEndpoint.GetMasterlistService().GetApplicationMethodList().SingleOrDefault(x => x.Name == p.ApplicationMethod);
		//                            if (selectedProduct == null) { continue; } // TODO: Log that product couldn't be found.
		//                            var newRecommendedProduct = new RecommendedProductViewModel(clientEndpoint, this, fieldSelection) {
		//                                ApplyBy = SelectedApplicationType,
		//                                SelectedProduct = selectedProduct,
		//                                RatePerAreaValue = (decimal)p.RateMeasure.Value,
		//                                ProductAreaPercent = p.ProductAreaPercent,
		//                            };
		//                            newRecommendedProduct.SelectedRatePerAreaUnitHelper = newRecommendedProduct.AllowedRateUnits.Where(rpah => rpah.Unit.Name == p.RateMeasure.Unit.Name).FirstOrDefault();

		//                            RecommendedProducts.Add(newRecommendedProduct);
		//                            SelectedRecommendedProduct = newRecommendedProduct;
		//                        }
		//                    }
		//                    if (RecommendedProducts.Any()) {
		//                        SelectedRecommendedProduct = RecommendedProducts.Last();
		//                    }
		//                }
		//                dispatcher.BeginInvoke(new Action(() => {
		//                    RaisePropertyChanged(() => ProductCount");
		//                }));
		//            });
		//            DelayRunProductPopulationTask(loadProductsTask);
		//        }
		//        RaisePropertyChanged(() => AssociatedPlanView");
		//    }
		//}

		private ObservableCollection<ProductDetails> SelectedPlanProducts = new ObservableCollection<ProductDetails>();
		public void SetSelectedPlanProducts(ObservableCollection<ProductDetails> selectedPlanProducts) {
			SelectedPlanProducts = selectedPlanProducts;
		}


		public List<ApplicationMethod> ApplicationMethods {
			get { return clientEndpoint.GetMasterlistService().GetApplicationMethodList().ToList(); }
		}

		public ApplicationMethod ApplicationMethod {
			get { return appMethod; }
			set {
				appMethod = value;
				RaisePropertyChanged(() => ApplicationMethod);
			}
		}

		public bool ShowTankInfoPopup {
			get { return showTankInfoPopup; }
			set {
				showTankInfoPopup = value;
				RaisePropertyChanged(() => ShowTankInfoPopup);
			}
		}

		public TankInformationModel TankInfo {
			get { return tankInfo; }
		}

		public ApplicationTypes SelectedApplicationType {
			get { return selectedApplicationType; }
			set {
				selectedApplicationType = value;
				RaisePropertyChanged(() => SelectedApplicationType);

				lock (RecommendedProducts) {
					RecommendedProducts.ForEach(x => x.ApplyBy = value);
				}

				//if (tankInfo.CarrierApplied.Value <= 0) {
				//	ShowTankInfoPopup = true;
				//}

				// open popup only when rate/tank is selected
				// https://fogbugz.agconnections.com/fogbugz/default.asp?26380
				switch (selectedApplicationType) {
					case ApplicationTypes.RatePerArea:
						ShowTankInfoPopup = false;
						break;
					case ApplicationTypes.TotalProduct:
						ShowTankInfoPopup = false;
						break;
					case ApplicationTypes.RatePerTank:
						ShowTankInfoPopup = true;
						break;
					default:
						ShowTankInfoPopup = false;
						break;
				}
			}
		}

		public IEnumerable<ApplicationTypes> ApplicationTypesList {
			get { return applicationTypesList; }
		}

		public int ProductCount {
			get {
				lock (RecommendedProducts) {
					var q = from p in RecommendedProducts
							where p.SelectedProduct != null
							select p;  // don't include products that have no data
					return q.Count();
				}
			}
		}

		public RecommendedProductViewModel SelectedRecommendedProduct {
			get { return selectedRecommendedProduct; }
			set {
				selectedRecommendedProduct = value;
				RaisePropertyChanged(() => SelectedRecommendedProduct);
				if (selectedRecommendedProduct != null) { ApplicationMethod = selectedRecommendedProduct.ApplicationMethod; }
				RaisePropertyChanged(() => ApplicationMethod);
			}
		}

		public bool IsTankApplication {
			get { return isTankApplication; }
			set {
				isTankApplication = value;
				RaisePropertyChanged(() => IsTankApplication);
			}
		}

		void OnTankInfoChanged() {
			lock (RecommendedProducts) {
				RecommendedProducts.ForEach(x => x.RecalculateItem());
			}
		}

		async void BuildProductList() {
			var watch = Stopwatch.StartNew();

			productLoadTask = Task.Run(() => {
				var mpl = clientEndpoint.GetMasterlistService().GetProductList();
				var mur = clientEndpoint.GetView<ProductUsageView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new ProductUsageView());
				var ucp = clientEndpoint.GetView<UserCreatedProductList>(new DataSourceId(ApplicationEnvironment.CurrentDataSourceId)).GetValue(new UserCreatedProductList());

				Guid mlDsId = GlobalIdentifiers.GetMasterlistDataSourceId();
				var productGuids = mur.GetProductGuids();

				var q = from p in mpl
						let cont = productGuids.Contains(p.Id)
						select new ProductListItemDetails(p, mlDsId, cont ? Strings.UsedThisYear_Text : Strings.Unused_Text, cont ? (short)0 : (short)1);
				var ucpQ = from p in ucp.UserCreatedProducts
						   let cont = productGuids.Contains(p.Id)
						   select new ProductListItemDetails(p, ApplicationEnvironment.CurrentDataSourceId, cont ? Strings.UsedThisYear_Text : Strings.Unused_Text, cont ? (short)0 : (short)1);

				Products = new ObservableCollection<ProductListItemDetails>(q.Union(ucpQ).OrderBy(x => x.GroupSortOrder).ToList());
				RaisePropertyChanged(() => Products);
			});

			await productLoadTask;
			watch.Stop();
			log.Debug("Loading products took {0}ms", watch.Elapsed.TotalMilliseconds);
		}

		internal void RefreshProductCount() {
			RaisePropertyChanged(() => ProductCount);
		}

		internal ProductListItemDetails AddTemporaryMiniProduct(MiniProduct product, ProductId productId, bool isUsed) {
			var p = new ProductListItemDetails(product, productId.DataSourceId, isUsed ? Strings.UsedThisYear_Text : Strings.Unused_Text, isUsed ? (short)0 : (short)1);
			Products.Add(p);
			return p;
		}

		internal RecommendationCommandProductEntry[] GetRecommendedProducts() {
			List<RecommendationCommandProductEntry> recommendedProducts = new List<RecommendationCommandProductEntry>();
			lock (RecommendedProducts) {
				foreach (var ap in RecommendedProducts) {
					var cpe = ap.ToRecommendationCommandProductEntry();
					if (cpe == null) { continue; }
					recommendedProducts.Add(cpe);
				}
			}

			return recommendedProducts.ToArray();
		}

		internal ApplicationTankInformation GetApplicationTankInformation() {
			return new ApplicationTankInformation((decimal)tankInfo.TankSize.Value, tankInfo.TankSize.Unit.Name, tankInfo.TankCount, (decimal)tankInfo.CarrierPerArea.Value, tankInfo.CarrierPerArea.Unit.Name, (decimal)tankInfo.CarrierApplied.Value, tankInfo.CarrierApplied.Unit.Name, string.IsNullOrEmpty(tankInfo.LockedValue) ? "CarrierPerArea" : tankInfo.LockedValue);
		}

		internal void ResetData() {
			RecommendedProducts.Clear();
			RaisePropertyChanged(() => ProductCount);
			AddRecommendedProductCommand.Execute(null); // Add the first product
		}

		private void AddRecommendedProduct() {
			var newRecommendedProduct = new RecommendedProductViewModel(clientEndpoint, this, fieldSelection) { ApplyBy = SelectedApplicationType };
			RecommendedProducts.Add(newRecommendedProduct);

			if (SelectedRecommendedProduct != null) {
				// copy application method from currently selected rec to new
				newRecommendedProduct.ApplicationMethod = SelectedRecommendedProduct.ApplicationMethod;
			}

			SelectedRecommendedProduct = newRecommendedProduct;

			RaisePropertyChanged(() => ProductCount);
		}

		private void RemoveRecommendedProduct() {
			if (SelectedRecommendedProduct != null) {
				var removedProductIndex = RecommendedProducts.IndexOf(SelectedRecommendedProduct);
				RecommendedProducts.Remove(SelectedRecommendedProduct);

				var productCount = RecommendedProducts.Count();

				if (productCount != 0) {
					if (removedProductIndex == productCount) {
						SelectedRecommendedProduct = RecommendedProducts.Last();
					} else if (removedProductIndex == 0) {
						SelectedRecommendedProduct = RecommendedProducts.First();
					} else {
						SelectedRecommendedProduct = RecommendedProducts[removedProductIndex];
					}
					RaisePropertyChanged(() => ProductCount);
				}
			}
		}

		#region Custom Validation
		public static ValidationResult ValidateProducts(ObservableCollection<RecommendedProductViewModel> products, ValidationContext context) {
			ValidationResult result = null;

			if (products == null || !products.Any()) {
				result = new ValidationResult(Strings.ValidationResult_AtLeastOneProductRequired_Text);
			} else if (products.Any(x => x.HasErrors)) {
				result = new ValidationResult(Strings.ValidationResult_ProductsHaveErrors_Text);
			}

			return result;
		}
		#endregion
	}
}