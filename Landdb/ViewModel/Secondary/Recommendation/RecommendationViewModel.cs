﻿using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Plan;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Fields;
using Landdb.ViewModel.Production;
using Landdb.ViewModel.Secondary.Application;
using Landdb.ViewModel.Shared;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Secondary.Recommendation {
	public class RecommendationViewModel : ValidationViewModelBase {
		readonly IClientEndpoint clientEndpoint;
		readonly Dispatcher dispatcher;

		RecommendationId recommendationId;
		int cropYear;
		int selectedPageIndex;
		SiteDataDetailsViewModel associatedSiteData;
        readonly CropYearId currentCropYearId;

        public RecommendationViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, RecommendationId recommendationId, int cropYear, ObservableCollection<ProductDetails> selectedPlanProducts, params DocumentDescriptor[] sourceDocuments) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			this.recommendationId = recommendationId;
			this.cropYear = cropYear;
            this.currentCropYearId = new CropYearId(recommendationId.DataSourceId, cropYear);

            CancelCommand = new RelayCommand(OnCancelRecommendation);
			CompleteCommand = new RelayCommand(OnCompleteRecommendation);

			ReturnToMainViewCommand = new RelayCommand(ReturnToMainView);
			CreateNewRecommendationCommand = new RelayCommand(CreateNewRecommendation);

			PlanView planView = GetSourceDocumentView<PlanView, CropPlanId>(sourceDocuments, DocumentDescriptor.PlanTypeString);

			InfoPageModel = new RecommendationInfoViewModel(clientEndpoint, dispatcher, cropYear, recommendationId.DataSourceId);
            FieldsPageModel = new FieldSelectionViewModel(clientEndpoint, dispatcher, currentCropYearId, null, false);
            ProductsPageModel = new RecommendedProductSelectionViewModel(FieldsPageModel, clientEndpoint, dispatcher, cropYear, recommendationId.DataSourceId);
			DocumentsModel = new DocumentListPageViewModel(clientEndpoint, dispatcher, cropYear, sourceDocuments);

			InfoPageModel.ErrorsChanged += (sender, args) => { RaisePropertyChanged(() => IsCompletionAllowed); };
			ProductsPageModel.ErrorsChanged += (sender, args) => { RaisePropertyChanged(() => IsCompletionAllowed); };

			//SITE DATA AS A SOURCE
			var siteId = sourceDocuments.Where(x => x.DocumentType == "ScoutingApplication").Select(j => j.Identity as ScoutingApplicationId).FirstOrDefault();
			AssociatedSiteData = GetSiteDataDocument(siteId);

			if (planView != null) {
				InfoPageModel.AssociatedPlanView = planView;
				FieldsPageModel.AssociatedPlanView = planView;
				ProductsPageModel.SetSelectedPlanProducts(selectedPlanProducts);
				ProductsPageModel.AssociatedPlanView = planView;
			}
		}

		private T GetSourceDocumentView<T, TId>(DocumentDescriptor[] sourceDocuments, string typeString) where TId : AbstractDataSourceIdentity<Guid, Guid> {
			var q = sourceDocuments.Where(x => x.DocumentType == typeString);
			return q.Any() ? clientEndpoint.GetView<T>((TId)q.First().Identity).GetValue(() => default(T)) : default(T);  // The lengths I'll go to for a one-line solution....
		}

		public ICommand CancelCommand { get; }
		public ICommand CompleteCommand { get; }

		public ICommand ReturnToMainViewCommand { get; }
		public ICommand CreateNewRecommendationCommand { get; }

		public ICommand CreateNewRecommendationBasedOnCurrent => new RelayCommand(() =>
		{
			Messenger.Default.Send(new RecommendationsAddedMessage { LastRecommendationAdded = recommendationId });
			recommendationId = new RecommendationId(recommendationId.DataSourceId, Guid.NewGuid());
			SelectedPageIndex = 0;
			Messenger.Default.Send(new HideOverlayMessage());
		});

		public RecommendationInfoViewModel InfoPageModel { get; }

		public FieldSelectionViewModel FieldsPageModel { get; }

		public RecommendedProductSelectionViewModel ProductsPageModel { get; }
		public DocumentListPageViewModel DocumentsModel { get; }

		public string TitleDisplay {
			get { return Strings.CreateRecommendation_Text; }
		}

		public int CropYear {
			get { return cropYear; }
		}

		public int SelectedPageIndex {
			get { return selectedPageIndex; }
			set {
				selectedPageIndex = value;
				RaisePropertyChanged(() => SelectedPageIndex);
			}
		}

		public bool IsCompletionAllowed {
			get { return !(ProductsPageModel.HasErrors || InfoPageModel.HasErrors); }
		}

		private void ResetData() {
			recommendationId = new RecommendationId(recommendationId.DataSourceId, Guid.NewGuid());
			SelectedPageIndex = 0;

			InfoPageModel.ResetData();
			FieldsPageModel.ResetData();
			ProductsPageModel.ResetData();
			DocumentsModel.ResetData();
		}

		void OnCompleteRecommendation() {
			ApplicationAuthorization recAuthorization = null;

			if (InfoPageModel.Authorizer != null && InfoPageModel.AuthorizationDate != null) {
				recAuthorization = new ApplicationAuthorization(InfoPageModel.Authorizer.Id, InfoPageModel.Authorizer.Name, (DateTime)InfoPageModel.AuthorizationDate);
			}

			// NOTE: In the context of a Recommendation, the time part of ProposedDate and ExpirationDate are irrelevant and converting to UTC could possibly cause problems.
			var createRecommendationCommand = new CreateRecommendation(
				recommendationId,
				clientEndpoint.GenerateNewMetadata(),			
				cropYear,
				InfoPageModel.RecTitle,
				InfoPageModel.RecNumber,
				GlobalStrings.RecommendationType_Landdb,
				InfoPageModel.CreatedDate.ToUniversalTime(),
				InfoPageModel.ProposedDate,
				InfoPageModel.ExpirationDate,
				GetTimingEvent(),
				GetNotes(),
				recAuthorization,
				InfoPageModel.SelectedApplicators.ToArray(),
				FieldsPageModel.GetCropZoneAreas(),
				ProductsPageModel.GetApplicationTankInformation(),
				GetApplicationStrategy(),
				ProductsPageModel.GetRecommendedProducts(),
				DocumentsModel.GetDocuments(),
				GetTimingEventTag()
			);

			clientEndpoint.SendOne(createRecommendationCommand);

			Messenger.Default.Send(new ShowOverlayMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Secondary.Recommendation.PostRecommendationOverlay), "postRec", this)
			});
		}

		void OnCancelRecommendation() {
			Messenger.Default.Send(new ShowMainViewMessage());
		}

		ProductApplicationStrategy GetApplicationStrategy() {
			switch (ProductsPageModel.SelectedApplicationType) {
				case ApplicationTypes.RatePerArea:
				default:
					return ProductApplicationStrategy.ByRatePerArea;
				case ApplicationTypes.TotalProduct:
					return ProductApplicationStrategy.ByTotalProduct;
				case ApplicationTypes.RatePerTank:
					return ProductApplicationStrategy.ByRatePerTank;
			}
		}

		string GetNotes() {
			if (!string.IsNullOrEmpty(InfoPageModel.Notes)) {
				return InfoPageModel.Notes;
			} else { return string.Empty; }
		}

		string GetTimingEvent() {
			if (InfoPageModel.TimingEvent != null) {
				return InfoPageModel.TimingEvent.Name;
			} else { return string.Empty; }
		}

		string GetTimingEventTag() {
			if (InfoPageModel.TimingEventTag != null) {
				return InfoPageModel.TimingEventTag;
			} else { return string.Empty; }
		}

		void ReturnToMainView() {
			Messenger.Default.Send(new RecommendationsAddedMessage() { LastRecommendationAdded = recommendationId });
			Messenger.Default.Send(new ShowMainViewMessage());
		}
		void CreateNewRecommendation() {
			ResetData();
			Messenger.Default.Send(new HideOverlayMessage());
		}

		public SiteDataDetailsViewModel AssociatedSiteData {
			get { return associatedSiteData; }
			set {
				associatedSiteData = value;

				if (associatedSiteData != null) {
					List<CropZoneId> siteDataCropZone = new List<CropZoneId>();
					siteDataCropZone.Add(associatedSiteData.CropZone.CropZoneId);

					FieldsPageModel.RootTreeItemModels.First().CheckMatches(x => {
						return x is CropZoneTreeItemViewModel && siteDataCropZone.Contains((CropZoneId)((CropZoneTreeItemViewModel)x).Id);
					});

					InfoPageModel.Notes += string.Format("{0}, {1}: {2}\n--------------------------------------\n{3}", Strings.From_Text, associatedSiteData.DocumentType, associatedSiteData.Name, associatedSiteData.Notes);
                    InfoPageModel.TimingEvent = !string.IsNullOrEmpty(associatedSiteData.TimingEvent) ? InfoPageModel.TimingEvents.FirstOrDefault(x => x.Name == associatedSiteData.TimingEvent) : null;
				}
			}
		}

		private SiteDataDetailsViewModel GetSiteDataDocument(ScoutingApplicationId id) {
			var baseDir = ApplicationEnvironment.UserAppDataPath; // ApplicationEnvironment.BaseDataDirectory;
			var fileDir = Path.Combine(baseDir, "Scouting");
			string fileName = string.Format("{0}_{1}", ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear);
			var filePath = Path.Combine(fileDir, fileName);

			if (Directory.Exists(fileDir) && File.Exists(filePath)) {
				var data = File.ReadAllText(filePath);
				ScoutingList scoutingList = JsonConvert.DeserializeObject<ScoutingList>(data);

				//return scoutingList;
				var item = scoutingList.Results.FirstOrDefault(x => x.DocumentId == id);
				if (item != null) {
					var detail = new SiteDataDetailsViewModel(clientEndpoint, dispatcher, item);
					return detail;
				} else {
					return null;
				}
			} else {
				return null;
			}
		}
	}
}