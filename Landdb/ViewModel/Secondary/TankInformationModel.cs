﻿using Ninject.Planning.Bindings.Resolvers;
using System.Runtime.InteropServices.ComTypes;
using AgC.UnitConversion;
using AgC.UnitConversion.Volume;
using GalaSoft.MvvmLight;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure;
using Landdb.ViewModel.Shared;
using System;
using System.Globalization;
using Landdb.Resources;

namespace Landdb.ViewModel.Secondary {
    public class TankInformationModel : ViewModelBase {

        private readonly IClientEndpoint clientEndpoint;
        private readonly BaseFieldSelectionViewModel fieldSelection;
        bool isTankApplication;
		Action onTankInfoChanged;

        private static Measure DefaultMeasure(double value = 0) =>
            Equals(ApplicationEnvironment.CurrentDataSource.DatasourceCulture, "en-US") ? 
                Gallon.Self.GetMeasure(value) : 
                Liter.Self.GetMeasure(value);

        private Measure _tankSize = DefaultMeasure();

        private Measure _carrierApplied = DefaultMeasure();

        private Measure _carrierPerArea = DefaultMeasure();

        private Measure _currentArea = DefaultMeasure();

        decimal tankCount;

		public TankInformationModel(IClientEndpoint clientEndpoint, BaseFieldSelectionViewModel fieldSelection, Action onTankInfoChanged) {
			this.clientEndpoint = clientEndpoint;
			this.fieldSelection = fieldSelection;

			CurrentArea = this.fieldSelection.TotalArea;

			fieldSelection.PropertyChanged += fieldSelectionModel_PropertyChanged;

			if (onTankInfoChanged != null) {
				this.onTankInfoChanged = onTankInfoChanged;
			} else {
				this.onTankInfoChanged = new Action(() => { }); // no-op if null
			}
		}

		public Measure CurrentArea {
			get { return _currentArea; }
			set {
				_currentArea = value;

				if (CarrierPerArea.Value > 0 /* && TankSize.Value > 0 */) {
					CarrierPerAreaText = CarrierPerArea.Value.ToString("N4");
				}
			}
		}

		public Measure TankSize {
			get { return _tankSize; }
		}

		public Measure CarrierApplied {
			get { return _carrierApplied; }
		}

		public Measure CarrierPerArea {
			get { return _carrierPerArea; }
		}

		public decimal TankCount {
			get { return tankCount; }
		}

		public string TankInformationDisplayText {
			get {
				//if (isTankApplication) {
				//	return string.Format("{0:n2} tanks ({1} ea.)", tankCount, tankSize.FullDisplay);
				//} else {
				//	return "no tank used";
				//}

				// http://fogbugz.agconnections.com/fogbugz/default.asp?26380
				return string.Format(Strings.Format_TanksEach_Text, tankCount, _tankSize.FullDisplay);
			}
		}

		public string LockedValue { get; private set; }

		public string TankSizeText {
			get { return _tankSize.FullDisplay; }
			set {
				if (string.IsNullOrWhiteSpace(value)) { return; }

			    if (double.TryParse(value, out var measureValue)) {
					_tankSize = DefaultMeasure(measureValue);

					if (_tankSize.Value > 0) {
						tankCount = (decimal)(_carrierApplied.Value / _tankSize.Value);
					} else {
						tankCount = 0;
					}

					RaisePropertyChanged(() => TankCountText);
					RaisePropertyChanged(() => TankSizeText);

					SetTankUseStatus();
					onTankInfoChanged();
				}
			}
		}

		public string CarrierAppliedText {
			get { return _carrierApplied.FullDisplay; }
			set {
				if (string.IsNullOrWhiteSpace(value)) { return; }

				double measureValue = 0;

				if (double.TryParse(value, out measureValue)) {
					_carrierApplied = DefaultMeasure(measureValue);

					if (CurrentArea != null && CurrentArea.Value != 0) {
						_carrierPerArea = DefaultMeasure(measureValue / CurrentArea.Value);
					} else {
						//carrierPerArea = Gallon.Self.GetMeasure(0);

						// https://fogbugz.agconnections.com/fogbugz/default.asp?27292
						// rather than zeroing the cpa, let's try defaulting it instead
						var currentCropYearId = new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear);
						var tankInfo = clientEndpoint.GetView<Domain.ReadModels.TankInformationSetting.TankInformationView>(currentCropYearId).GetValue(() => null);
						if (tankInfo != null) {
							CarrierPerAreaText = tankInfo.CarrierPerAreaValue.ToString();
						}
					}

					if (_tankSize.Value > 0) {
						tankCount = (decimal)(_carrierApplied.Value / _tankSize.Value);
					} else {
						tankCount = 0;
					}

					LockedValue = "CarrierApplied";
					RaisePropertyChanged(() => TankCountText);
					RaisePropertyChanged(() => CarrierAppliedText);
					RaisePropertyChanged(() => CarrierPerAreaText);

					SetTankUseStatus();
					onTankInfoChanged();
				}
			}
		}

		public string CarrierPerAreaText {
			get { return _carrierPerArea.FullDisplay; }
			set {
				if (string.IsNullOrWhiteSpace(value)) { return; }

				double measureValue = 0;

				if (double.TryParse(value, out measureValue)) {
					_carrierPerArea = DefaultMeasure(measureValue);

					if ((measureValue * CurrentArea.Value) != _carrierApplied.Value) {
						CarrierAppliedText = (measureValue * CurrentArea.Value).ToString();
					}

					if (_tankSize.Value > 0) {
						tankCount = (decimal)(_carrierApplied.Value / _tankSize.Value);
					} else {
						tankCount = 0;
					}

					LockedValue = "CarrierPerArea";

					RaisePropertyChanged(() => TankCountText);
					RaisePropertyChanged(() => CarrierAppliedText);
					RaisePropertyChanged(() => CarrierPerAreaText);

					SetTankUseStatus();
					onTankInfoChanged();
				}
			}
		}

		public string TankCountText {
			get { return tankCount.ToString(); }
			set {
				if (string.IsNullOrWhiteSpace(value)) { return; }

				decimal measureValue = 0;
				if (decimal.TryParse(value, out measureValue)) {
					tankCount = measureValue;
					_carrierApplied = DefaultMeasure(_tankSize.Value * (double)tankCount);

					if (CurrentArea == null || CurrentArea.Value <= 0) {
						_carrierPerArea = DefaultMeasure();
					} else {
						_carrierPerArea = DefaultMeasure(_carrierApplied.Value / CurrentArea.Value);
					}

					LockedValue = "TankCount";
					RaisePropertyChanged(() => TankCountText);
					RaisePropertyChanged(() => CarrierAppliedText);
					RaisePropertyChanged(() => CarrierPerAreaText);

					SetTankUseStatus();
					onTankInfoChanged();
				}
			}
		}

		public void SetTankInformation(ApplicationTankInformation tankInfo) {
			if (tankInfo != null) {
				_tankSize = UnitFactory.GetUnitByName(tankInfo.TankSizeUnit).GetMeasure((double)tankInfo.TankSizeValue);
				//var totalCarrier = UnitFactory.GetUnitByName(tankInfo.TotalCarrierUnit).GetMeasure((double)tankInfo.TotalCarrierValue);
				//CarrierAppliedText = totalCarrier.Value.ToString(); // This is a bit of a hack, I admit...
				//CarrierPerAreaText = tankInfo.CarrierPerAreaValue.ToString();
				var carrierPerAreaMeasure = UnitFactory.GetUnitByName(tankInfo.CarrierPerAreaUnit).GetMeasure((double)tankInfo.CarrierPerAreaValue, 1);
				CarrierPerAreaText = carrierPerAreaMeasure.Value.ToString();

				RaisePropertyChanged(() => TankSize);
				RaisePropertyChanged(() => CarrierApplied);

				RaisePropertyChanged(() => TankCountText);
				RaisePropertyChanged(() => CarrierAppliedText);
				RaisePropertyChanged(() => CarrierPerAreaText);

				SetTankUseStatus();
			}
		}

		public bool IsTankApplication {
			get { return isTankApplication; }
			set {
				isTankApplication = value;
				RaisePropertyChanged(() => IsTankApplication);
				RaisePropertyChanged(() => TankInformationDisplayText);
			}
		}

		public void SetTankUseStatus() {
			if (_carrierApplied == null || _carrierApplied.Value <= 0) {
				IsTankApplication = false;
			} else {
				IsTankApplication = true;
			}

			RaisePropertyChanged(() => TankInformationDisplayText);
		}

		//private void GetSelectedArea(PropertyChangedMessage<Measure> measure)
		//{
		//    if (measure.PropertyName == "BaseFieldSelectionAreaChanged")
		//    {
		//        CurrentArea = measure.NewValue;

		//        if (CarrierPerArea.Value > 0 && TankSize.Value > 0)
		//        {
		//            CarrierPerAreaText = CarrierPerArea.Value.ToString("N4");
		//        }
		//    }
		//}

		void fieldSelectionModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e) {
			if (e.PropertyName == "TotalArea") {
				CurrentArea = fieldSelection.TotalArea;
			}
		}
	}
}