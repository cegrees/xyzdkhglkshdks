﻿using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Client.Services;
using Landdb.Domain.ReadModels.Company;
using Landdb.Domain.ReadModels.Person;
using Landdb.Domain.ReadModels.Plan;
using Landdb.Domain.ReadModels.Recommendation;
using Landdb.Domain.ReadModels.WorkOrder;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Production;
using Landdb.ViewModel.Secondary.PopUp;
using Landdb.ViewModel.Shared;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Client.Infrastructure.DisplayItems;
using Landdb.Resources;

namespace Landdb.ViewModel.Secondary.WorkOrder {
	public class WorkOrderEditorViewModel : ValidationViewModelBase {

		readonly IClientEndpoint clientEndpoint;
		readonly Dispatcher dispatcher;

		readonly CropYearId currentCropYearId;

		private WorkOrderId workOrderId;
		private Dictionary<string, List<string>> mergedTimingEventTags;
		private bool changeAuthorizeDate;

		public WorkOrderEditorViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, WorkOrderId workOrderId, int cropYear, ObservableCollection<ProductDetails> selectedPlanProducts, params DocumentDescriptor[] sourceDocuments) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			this.currentCropYearId = new CropYearId(workOrderId.DataSourceId, cropYear);

			this.workOrderId = workOrderId;

			StartDate = DateTime.Today + TimeSpan.FromHours(8);
			StartTime = DateTime.Today + TimeSpan.FromHours(8);
			AuthorizationDate = DateTime.Today + TimeSpan.FromHours(8);

			DataSourceId dsId = new DataSourceId(ApplicationEnvironment.CurrentDataSourceId);

			var personListView = clientEndpoint.GetView<PersonListView>(dsId).GetValue(() => null);
			if (personListView != null) {
				var sortedFilteredPersons = from p in personListView.Persons
											where p.IsActiveInCropYear(cropYear)
											orderby p.Name
											select p;

				Authorizers = sortedFilteredPersons.ToList();
			}

			AvailableTimingEventTags = new Dictionary<string, List<string>>();

			SelectedApplicators = new ObservableCollection<ApplicationApplicator>();

			CancelCommand = new RelayCommand(OnCancelWorkOrder);
			CompleteCommand = new RelayCommand(OnCompleteWorkOrder);
			AddApplicatorCommand = new RelayCommand(AddPersonAsApplicator);
			AddCompanyCommand = new RelayCommand(AddCompany);

			ReturnToMainViewCommand = new RelayCommand(ReturnToMainView);
			CreateNewWorkOrderCommand = new RelayCommand(CreateNewWorkOrder);
			CreateWorkOrderCommand = new RelayCommand(CreateWorkOrder);

			RecommendationView recView = GetSourceDocumentView<RecommendationView, RecommendationId>(sourceDocuments, DocumentDescriptor.RecommendationTypeString);
			PlanView planView = GetSourceDocumentView<PlanView, CropPlanId>(sourceDocuments, DocumentDescriptor.PlanTypeString);
			WorkOrderView woView = GetSourceDocumentView<WorkOrderView, WorkOrderId>(sourceDocuments, DocumentDescriptor.WorkOrderTypeString);

			AssociatedRecommendationView = recView;
			AssociatedPlanView = planView;
			AssociatedWorkOrderView = woView;

			FieldsPageModel = new Application.FieldSelectionViewModel(clientEndpoint, dispatcher, currentCropYearId, null, true);
			ProductsPageModel = new Application.ProductSelectionViewModel(clientEndpoint, dispatcher, currentCropYearId, FieldsPageModel, false);
			IncludeAsApplicator = new IncludeApplicator(clientEndpoint, dispatcher, this.SelectedApplicators);
			IncludeCompany = new IncludeApplicationCompany(clientEndpoint, dispatcher, this);
			DocumentsModel = new DocumentListPageViewModel(clientEndpoint, dispatcher, cropYear, sourceDocuments);

			FieldsPageModel.ErrorsChanged += (sender, args) => { RaisePropertyChanged(() => IsCompletionAllowed); };
			ProductsPageModel.ErrorsChanged += (sender, args) => { RaisePropertyChanged(() => IsCompletionAllowed); };

			//SITE DATA AS A SOURCE
			var siteId = sourceDocuments.Where(x => x.DocumentType == "ScoutingApplication").Select(j => j.Identity as ScoutingApplicationId).FirstOrDefault();
			AssociatedSiteData = GetSiteDataDocument(siteId);

			//GET TANK INFORMATION
			var tankInfoMaybe = clientEndpoint.GetView<Landdb.Domain.ReadModels.TankInformationSetting.TankInformationView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear));
			if (tankInfoMaybe.HasValue) {
				var tankInfo = tankInfoMaybe.Value;
				ProductsPageModel.TankInfo.TankSizeText = tankInfo.TankSizeValue.ToString("N4");
				ProductsPageModel.TankInfo.CarrierPerAreaText = tankInfo.CarrierPerAreaValue.ToString("N4");
			}
			if (recView != null) {
				//InfoPageModel.AssociatedWorkOrderView = workOrderView;
				FieldsPageModel.AssociatedRecommendationView = recView;
				ProductsPageModel.AssociatedRecommendationView = recView;
			} else if (planView != null) {
				//InfoPageModel.AssociatedPlanView = planView;
				FieldsPageModel.AssociatedPlanView = planView;
				ProductsPageModel.SetSelectedPlanProducts(selectedPlanProducts);
				ProductsPageModel.AssociatedPlanView = planView;
			} else if (woView != null) {
				//FieldsPageModel.AssociatedWorkOrderView = woView;
				ProductsPageModel.AssociatedWorkOrderView = woView;
			}

			var nameGenerationService = ServiceLocator.Get<INameAutoGenerationService>();
			Name = nameGenerationService.DisplayName;

			changeAuthorizeDate = true;
		}

		public ICommand CancelCommand { get; private set; }
		public ICommand CompleteCommand { get; private set; }

		// post overlay
		public ICommand ReturnToMainViewCommand { get; private set; }
		public ICommand CreateNewWorkOrderCommand { get; private set; }
		public ICommand CreateWorkOrderCommand { get; private set; }

		public ICommand AddApplicatorCommand { get; private set; }
		public ICommand AddCompanyCommand { get; private set; }

		public Application.FieldSelectionViewModel FieldsPageModel { get; private set; }
		public Application.ProductSelectionViewModel ProductsPageModel { get; private set; }
		public DocumentListPageViewModel DocumentsModel { get; private set; }

		public ObservableCollection<DurationViewModel> Durations { get; private set; }
		public List<PersonListItem> Authorizers { get; private set; }

		public IncludeApplicator IncludeAsApplicator { get; set; }
		public IncludeApplicationCompany IncludeCompany { get; set; }

		public ObservableCollection<ApplicationApplicator> SelectedApplicators { get; set; }
		public ObservableCollection<CompanyListItem> SelectedCompanies { get; set; }

		public int CropYear { get { return currentCropYearId.Id; } }

		private DateTime _startDate;
		public DateTime StartDate {
			get { return _startDate; }
			set {
				if (_startDate != value) {
					_startDate = value;
					RaisePropertyChanged(() => StartDate);

					if (changeAuthorizeDate) {
						AuthorizationDate = _startDate;
						changeAuthorizeDate = false;
						RaisePropertyChanged(() => AuthorizationDate);
					}
				}
			}
		}

		private DateTime _startTime;
		public DateTime StartTime {
			get { return _startTime; }
			set {
				if (_startTime != value) {
					_startTime = value;
					RaisePropertyChanged(() => StartTime);
				}
			}
		}

		private DurationViewModel _selectedDuration;
		public DurationViewModel SelectedDuration {
			get { return _selectedDuration; }
			set {
				_selectedDuration = value;
				RaisePropertyChanged(() => SelectedDuration);
			}
		}

		private DateTime? _authorizationDate;
		public DateTime? AuthorizationDate {
			get { return _authorizationDate; }
			set {
				_authorizationDate = value;
				changeAuthorizeDate = false;
				RaisePropertyChanged(() => AuthorizationDate);
			}
		}

		private string _name;
		public string Name {
			get { return _name; }
			set {
				_name = value;
				RaisePropertyChanged(() => Name);
			}
		}

		private string _notes;
		public string Notes {
			get { return _notes; }
			set {
				_notes = value;
				RaisePropertyChanged(() => Notes);
			}
		}

		private PersonListItem _authorizer;
		public PersonListItem Authorizer {
			get { return _authorizer; }
			set {
				_authorizer = value;
				RaisePropertyChanged(() => Authorizer);
			}
		}

		private TimingEvent _timingEvent;
		public TimingEvent TimingEvent {
			get { return _timingEvent; }
			set {
				_timingEvent = value;
				SelectAppropriateTags();
				RaisePropertyChanged(() => TimingEvent);
			}
		}

		private Dictionary<string, List<string>> _availableTimingEventTags;
		public Dictionary<string, List<string>> AvailableTimingEventTags {
			get { return _availableTimingEventTags; }
			set {
				_availableTimingEventTags = value;
				SelectAppropriateTags();
			}
		}

		private Dictionary<string, List<string>> _tempTimingEventTags;
		public Dictionary<string, List<string>> TempTimingEventTags {
			get { return _tempTimingEventTags; }
			set {
				_tempTimingEventTags = value;
				SelectAppropriateTags();
				RaisePropertyChanged(() => TempTimingEventTags);
			}
		}

		private List<string> _timingEventTags;
		public List<string> TimingEventTags {
			get {
				if (_timingEventTags == null) {
					return new List<string>();
				} else {
					return _timingEventTags.OrderBy(q => q).ToList();
				}
			}
			set {
				_timingEventTags = value;
				RaisePropertyChanged(() => TimingEventTags);
			}
		}

		private string _timingEventTag;
		public string TimingEventTag {
			get { return _timingEventTag; }
			set {
				if (_timingEventTag == value) { return; }

				_timingEventTag = value;
				RaisePropertyChanged(() => TimingEventTag);
			}
		}

		private string _selectedTimingEventTag;
		public string SelectedTimingEventTag {
			get { return _selectedTimingEventTag; }
			set {
				if (_selectedTimingEventTag == value) { return; }
				_selectedTimingEventTag = value;
				if (!string.IsNullOrEmpty(_selectedTimingEventTag)) {
					_timingEventTag = _selectedTimingEventTag;
				}
				RaisePropertyChanged(() => SelectedTimingEventTag);
			}
		}

		public List<TimingEvent> TimingEvents {
			get { return clientEndpoint.GetMasterlistService().GetTimingEventList().OrderBy(x => x.Order).ToList(); }
		}

		public bool IsCompletionAllowed {
			get { return !(FieldsPageModel.HasErrors || ProductsPageModel.HasErrors); }
		}

		private void SelectAppropriateTags() {
			if (_timingEvent != null && !string.IsNullOrEmpty(_timingEvent.Name)) {
				mergedTimingEventTags = _availableTimingEventTags;
				if (_tempTimingEventTags != null && _tempTimingEventTags.Count > 0) {
					mergedTimingEventTags = _availableTimingEventTags.Concat(_tempTimingEventTags).GroupBy(d => d.Key).ToDictionary(d => d.Key, d => d.First().Value);
				}

				if (mergedTimingEventTags != null && mergedTimingEventTags.ContainsKey(_timingEvent.Name)) {
					TimingEventTags = mergedTimingEventTags[_timingEvent.Name];
				} else {
					TimingEventTags = new List<string>();
				}
			}
		}

		private void ResetData() {
			workOrderId = new WorkOrderId(workOrderId.DataSourceId, Guid.NewGuid());
			SelectedPageIndex = 0;

			var nameGenerationService = ServiceLocator.Get<INameAutoGenerationService>();
			Name = nameGenerationService.DisplayName;

			StartDate = DateTime.Today + TimeSpan.FromHours(8);
			StartTime = DateTime.Today + TimeSpan.FromHours(8);

			//InfoPageModel.ResetData();
			FieldsPageModel.ResetData();
			ProductsPageModel.ResetData();
			//ConditionsModel.ResetData();
			DocumentsModel.ResetData();
		}

		private T GetSourceDocumentView<T, TId>(DocumentDescriptor[] sourceDocuments, string typeString) where TId : AbstractDataSourceIdentity<Guid, Guid> {
			var q = from d in sourceDocuments
					where d.DocumentType == typeString
					select d;

			return q.Any() ? clientEndpoint.GetView<T>((TId)q.First().Identity).GetValue(() => default(T)) : default(T);  // The lengths I'll go to for a one-line solution....
		}

		private SiteDataDetailsViewModel GetSiteDataDocument(ScoutingApplicationId id) {
			var baseDir = ApplicationEnvironment.UserAppDataPath; // ApplicationEnvironment.BaseDataDirectory;
			var fileDir = Path.Combine(baseDir, "Scouting");
			string fileName = string.Format("{0}_{1}", ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear);
			var filePath = Path.Combine(fileDir, fileName);

			if (Directory.Exists(fileDir) && File.Exists(filePath)) {
				var data = File.ReadAllText(filePath);
				ScoutingList scoutingList = Newtonsoft.Json.JsonConvert.DeserializeObject<ScoutingList>(data);

				//return scoutingList;
				var item = scoutingList.Results.FirstOrDefault(x => x.DocumentId == id);
				if (item != null) {
					var detail = new SiteDataDetailsViewModel(clientEndpoint, dispatcher, item);
					return detail;
				} else {
					return null;
				}
			} else {
				return null;
			}
		}

		void OnCompleteWorkOrder() {
			if (!IsCompletionAllowed) { return; }

			ApplicationAuthorization appAuthorization = null;

			if (Authorizer != null && AuthorizationDate != null) {
				appAuthorization = new ApplicationAuthorization(Authorizer.Id, Authorizer.Name, (DateTime)AuthorizationDate);
			}

			SelectedDuration = new DurationViewModel(null);
			var timingEventName = string.Empty;
			if (TimingEvent != null) { timingEventName = TimingEvent.Name; }
			var startTime = (StartDate.Date + StartTime.TimeOfDay).ToUniversalTime();
			SetPreferredUnits();

			var createWorkOrderCommand = new CreateWorkOrder(
				workOrderId,
				clientEndpoint.GenerateNewMetadata(),
				CropYear,
				GetName(),
				startTime,
				timingEventName,
				_notes,
				appAuthorization,
				SelectedApplicators.ToArray(),
				FieldsPageModel.GetCropZoneAreas(),
				ProductsPageModel.GetApplicationTankInformation(),
				(ProductApplicationStrategy)ProductsPageModel.SelectedApplicationType,
				ProductsPageModel.GetAppliedProducts(),
				DocumentsModel.GetDocuments(),
				GetTimingEventTag()
			);

			clientEndpoint.SendOne(createWorkOrderCommand);


            if (IncludeAsApplicator.BufferWidth != null && IncludeAsApplicator.BufferWidth > 0)
            {
                //SEND UP BUFFER ZONE
                var addBufferZone = new ChangeWorkOrderEnvironmentalZoneTolerance(workOrderId, clientEndpoint.GenerateNewMetadata(), IncludeAsApplicator.BufferWidth.Value, IncludeAsApplicator.BufferWidthUnit.Name, IncludeAsApplicator.SelectedBufferTolerance);
                clientEndpoint.SendOne(addBufferZone);
            }

            Messenger.Default.Send(new ShowOverlayMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Secondary.WorkOrder.PostWorkOrderOverlay), "postApp", this)
			});
		}

		void OnCancelWorkOrder() {
			Messenger.Default.Send(new ShowMainViewMessage());
		}

		void SetPreferredUnits() {
			var products = ProductsPageModel.GetAppliedProducts();
			var settings = clientEndpoint.GetUserSettings();

			foreach (var p in products) {
				if (p is ApplicationProductByArea) {
					var prod = ((ApplicationProductByArea)p);

					if (!settings.PreferredRateUnits.ContainsKey(prod.ProductId.Id)) {
						settings.PreferredRateUnits.Add(prod.ProductId.Id, prod.RatePerAreaUnit);
						clientEndpoint.SaveUserSettings();
					} else if (settings.PreferredRateUnits.ContainsKey(prod.ProductId.Id) && settings.PreferredRateUnits[prod.ProductId.Id] != prod.RatePerAreaUnit) {
						settings.PreferredRateUnits[prod.ProductId.Id] = prod.RatePerAreaUnit;
						clientEndpoint.SaveUserSettings();
					}
				}

				if (p is ApplicationProductByTotal) {
					var prod = ((ApplicationProductByTotal)p);

					if (!settings.PreferredTotalUnits.ContainsKey(prod.ProductId.Id)) {
						settings.PreferredTotalUnits.Add(prod.ProductId.Id, prod.TotalProductUnit);
						clientEndpoint.SaveUserSettings();
					} else if (settings.PreferredTotalUnits.ContainsKey(prod.ProductId.Id) && settings.PreferredTotalUnits[prod.ProductId.Id] != prod.TotalProductUnit) {
						settings.PreferredTotalUnits[prod.ProductId.Id] = prod.TotalProductUnit;
						clientEndpoint.SaveUserSettings();
					}
				}

				if (p is ApplicationProductByTank) {
					var prod = ((ApplicationProductByTank)p);

					if (!settings.PreferredRatePerTankUnits.ContainsKey(prod.ProductId.Id)) {
						settings.PreferredRatePerTankUnits.Add(prod.ProductId.Id, prod.RatePerTankUnit);
						clientEndpoint.SaveUserSettings();
					} else if (settings.PreferredRatePerTankUnits.ContainsKey(prod.ProductId.Id) && settings.PreferredRatePerTankUnits[prod.ProductId.Id] != prod.RatePerTankUnit) {
						settings.PreferredRatePerTankUnits[prod.ProductId.Id] = prod.RatePerTankUnit;
						clientEndpoint.SaveUserSettings();
					}
				}
			}
		}

		private int _selectedPageIndex;
		public int SelectedPageIndex {
			get { return _selectedPageIndex; }
			set {
				_selectedPageIndex = value;
				RaisePropertyChanged(() => SelectedPageIndex);
			}
		}

		string GetName() {
			if (!string.IsNullOrEmpty(_name)) {
				return _name;
			} else {
				return ProductsPageModel.CreateNameFromProducts(); // Is there anything better to create a name from?
			}
		}

		string GetTimingEventTag() {
			return TimingEventTag != null ? TimingEventTag : string.Empty;
		}

		void AddPersonAsApplicator() {
			Messenger.Default.Send(new ShowPopupMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Secondary.PopUp.IncludeApplicatorView), "includeApplicator", this.IncludeAsApplicator)
			});
		}

		void AddCompany() {
			Messenger.Default.Send(new ShowPopupMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Secondary.PopUp.IncludeCompanyView), "includeCompany", this.IncludeCompany)
			});
		}

		#region Post Overlay
		void ReturnToMainView() {
			Messenger.Default.Send<WorkOrdersAddedMessage>(new WorkOrdersAddedMessage() { LastWorkOrderAdded = workOrderId });
			Messenger.Default.Send<ShowMainViewMessage>(new ShowMainViewMessage());
		}

		void CreateNewWorkOrder() {
			ResetData();
			Messenger.Default.Send<WorkOrdersAddedMessage>(new WorkOrdersAddedMessage() { LastWorkOrderAdded = workOrderId });
			Messenger.Default.Send<HideOverlayMessage>(new HideOverlayMessage());
		}

		void CreateWorkOrder() {
			Messenger.Default.Send<WorkOrdersAddedMessage>(new WorkOrdersAddedMessage() { LastWorkOrderAdded = workOrderId });
			workOrderId = new WorkOrderId(workOrderId.DataSourceId, Guid.NewGuid());
			var nameGenerationService = ServiceLocator.Get<INameAutoGenerationService>();
			Name = nameGenerationService.DisplayName;
            FieldsPageModel.ResetData();
			SelectedPageIndex = 0;
			Messenger.Default.Send<HideOverlayMessage>(new HideOverlayMessage());
		}

		public string SourceContextDescription {
			get {
				var retString = Strings.ThisOne_Text.ToLower();

				if (DocumentsModel.ListItems.Any()) {
					var sourceDoc = DocumentsModel.ListItems.First();
				    var sourceDocTypeDisplay = sourceDoc.DocumentType.DisplayText;

					retString = $"{sourceDocTypeDisplay} \"{sourceDoc.Name}\"";
				}

				return retString;
			}
		}
		#endregion

		void onTotalAreaChanged() {
			ProductsPageModel.TankInfo.CurrentArea = FieldsPageModel.TotalArea;
		}

		private RecommendationView _associatedRecommendationView;
		public RecommendationView AssociatedRecommendationView {
			get { return _associatedRecommendationView; }
			set {
				_associatedRecommendationView = value;

				if (_associatedRecommendationView != null) {
                    if (_associatedRecommendationView.Authorization != null)
                    {
                        Authorizer = _associatedRecommendationView.Authorization != null ? Authorizers.FirstOrDefault(x => x.Id == _associatedRecommendationView.Authorization.PersonId) : null;
                        AuthorizationDate = _associatedRecommendationView.Authorization != null ? _associatedRecommendationView.Authorization.AuthorizationDate : _associatedRecommendationView.CreatedDateTime;
                        RaisePropertyChanged(() => Authorizer);
                        RaisePropertyChanged(() => AuthorizationDate);
                    }
                        //AuthorizationDate = _associatedRecommendationView.CreatedDateTime;
                        TimingEvent = string.IsNullOrEmpty(_associatedRecommendationView.TimingEvent) ? null : TimingEvents.FirstOrDefault(x => x.Name == _associatedRecommendationView.TimingEvent);
						TimingEventTag = _associatedRecommendationView.TimingEventTag;
						//Name = string.Format("From Recommendation {0}", associatedRecommendationView.RecNumber);
						
						RaisePropertyChanged(() => TimingEvent);
						RaisePropertyChanged(() => TimingEventTag);
					

					if (_associatedRecommendationView.Applicators != null) {
						foreach (var a in _associatedRecommendationView.Applicators) {
							// TODO: Account for this already being set by the user before picking a work order on the sources page
							SelectedApplicators.Add(new ApplicationApplicator(a.PersonId, a.PersonName, a.CompanyId, a.CompanyName, a.LicenseNumber, a.Expires));
						}
						RaisePropertyChanged(() => Authorizer);
					}

					if (!string.IsNullOrWhiteSpace(_associatedRecommendationView.Notes)) {
						Notes += string.Format("{0}:\n--------------------------------------\n{1}", Strings.FromRecommendation_Text,_associatedRecommendationView.Notes);
						RaisePropertyChanged(() => Notes);
					}
				}

				RaisePropertyChanged(() => AssociatedRecommendationView);
			}
		}

		private PlanView _associatedPlanView;
		public PlanView AssociatedPlanView {
			get { return _associatedPlanView; }
			set {
				_associatedPlanView = value;
				if (_associatedPlanView != null) {
					if (_associatedPlanView.Products.Count > 0) {
						string timingeventname = _associatedPlanView.Products.FirstOrDefault().TimingEvent;
						TimingEvent = (!string.IsNullOrEmpty(timingeventname)) ? TimingEvents.Where(x => x.Name == timingeventname).FirstOrDefault() : null;
						TimingEventTag = _associatedPlanView.Products.FirstOrDefault().TimingEventTag;
						RaisePropertyChanged(() => TimingEvent);
						RaisePropertyChanged(() => TimingEventTag);
					}

					if (!string.IsNullOrWhiteSpace(_associatedPlanView.Notes)) {
						Notes += string.Format("{0}:\n--------------------------------------\n{1}", Strings.FromPlan_Text, _associatedPlanView.Notes);
					}
				}
				RaisePropertyChanged(() => AssociatedPlanView);
			}
		}

		private WorkOrderView associatedWorkOrderView;
		public WorkOrderView AssociatedWorkOrderView {
			get { return associatedWorkOrderView; }
			set {
				associatedWorkOrderView = value;

				if (associatedWorkOrderView != null) {
					if (associatedWorkOrderView.Authorization != null) {
						Authorizer = associatedWorkOrderView.Authorization != null ? Authorizers.FirstOrDefault(x => x.Id == associatedWorkOrderView.Authorization.PersonId) : null;
						AuthorizationDate = associatedWorkOrderView.Authorization != null ? associatedWorkOrderView.Authorization.AuthorizationDate : DateTime.MinValue;
						TimingEvent = string.IsNullOrEmpty(associatedWorkOrderView.TimingEvent) ? null : TimingEvents.FirstOrDefault(x => x.Name == associatedWorkOrderView.TimingEvent);
						TimingEventTag = associatedWorkOrderView.TimingEventTag;

						RaisePropertyChanged(() => Authorizer);
						RaisePropertyChanged(() => AuthorizationDate);
						RaisePropertyChanged(() => TimingEvent);
						RaisePropertyChanged(() => TimingEventTag);
					}

					if (associatedWorkOrderView.Applicators != null) {
						foreach (var a in associatedWorkOrderView.Applicators) {
							// TODO: Account for this already being set by the user before picking a work order on the sources page
							SelectedApplicators.Add(new ApplicationApplicator(a.PersonId, a.PersonName, a.CompanyId, a.CompanyName, a.LicenseNumber, a.Expires));
						}
						RaisePropertyChanged("Authorizer");
					}

					if (!string.IsNullOrWhiteSpace(associatedWorkOrderView.Notes)) {
						Notes += string.Format("{0}:\n--------------------------------------\n{1}", Strings.FromWorkOrder_Text, associatedWorkOrderView.Notes);
						RaisePropertyChanged(() => Notes);
					}
				}

				RaisePropertyChanged(() => AssociatedWorkOrderView);
			}
		}

		private SiteDataDetailsViewModel _associatedSiteData;
		public SiteDataDetailsViewModel AssociatedSiteData {
			get { return _associatedSiteData; }
			set {
				_associatedSiteData = value;

				if (_associatedSiteData != null) {
					List<CropZoneId> siteDataCropZone = new List<CropZoneId>();
					siteDataCropZone.Add(_associatedSiteData.CropZone.CropZoneId);
					FieldsPageModel.SiteDataCropZones = siteDataCropZone;

                    //TO DO :: FIND AND ASSOCIATE THE SELECTED TIMING EVENT....
                    TimingEvent = !string.IsNullOrEmpty( _associatedSiteData.TimingEvent ) ? TimingEvents.FirstOrDefault(x => x.Name == _associatedSiteData.TimingEvent) : null;

					Notes += string.Format("{0} {1}: {2}\n--------------------------------------\n{2}", Strings.From_Text, _associatedSiteData.DocumentType, _associatedSiteData.Name, _associatedSiteData.Notes);
				}
				RaisePropertyChanged(() => Notes);
			}
		}
	}
}