﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using AgC.UnitConversion;
using AgC.UnitConversion.Area;
using AgC.UnitConversion.Density;
using AgC.UnitConversion.MassAndWeight;
using AgC.UnitConversion.Package;
using AgC.UnitConversion.Time;
using AgC.UnitConversion.Volume;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Client.Localization;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.Resources;

namespace Landdb.ViewModel.Secondary.UserCreatedProduct
{
    public class UserProductCreatorViewModel : ViewModelBase {
        private readonly IClientEndpoint _clientEndpoint;

        private string _productName;
        private string _manufacturerName = Strings.Unspecified_Text; // Default manufacturer name
        private string _selectedProductType;
        private IUnit _selectedUnit;
        private string _selectedUnitText;
        private bool _isShowingCreate = true;
        private bool _isCreatingFertilizer;
        private bool _isDensityAccepted;
        private bool _isNullFormulation;
        private FertilizerFormulation _formulation = new FertilizerFormulation();
        private string _currentOption;

        private readonly Action<MiniProduct> _onProductCreated;

        public static string FertilizerType = Strings.Fertilizer_Text;
        public static string ServiceType = Strings.Service_Text;
        private readonly string _cropProtectionType = Strings.CropProtection_Text;
        private readonly string _seedType = Strings.Seed_Text;

        public UserProductCreatorViewModel(IClientEndpoint clientEndpoint, string productName, Action<MiniProduct> onProductCreated) {
            _clientEndpoint = clientEndpoint;
            _onProductCreated = onProductCreated;
            _productName = productName;
            IsFormulationNull = true;
            ProductType = new List<string>();
            ProductType.Add(Strings.Liquid_Text);
            ProductType.Add(Strings.Solid_Text);
            //TODO from unknown developer: Yeah...I'm lazy. This'll have to change for localization, of course.
            ProductTypes = new ObservableCollection<string> { FertilizerType, ServiceType, _cropProtectionType, _seedType };
            AvailableUnits = new ObservableCollection<IUnit>();
            SelectedProductType = ProductTypes[0];

            CancelCommand = new RelayCommand(Cancel);
            RequestProductCommand = new RelayCommand(RequestProduct);
            CreateProductCommand = new RelayCommand(CreateProduct);
        }

        public RelayCommand CancelCommand { get; private set; }
        public RelayCommand CreateProductCommand { get; private set; }
        public RelayCommand RequestProductCommand { get; private set; }

        public MiniProduct FakedMiniProduct { get; set; }
        public bool AllowCreate { get; set; }

        public string ProductName {
            get => _productName;
            set {
                _productName = value;
                RaisePropertyChanged("ProductName");
            }
        }

        public string ManufacturerName {  
            get => _manufacturerName;
            set {
                _manufacturerName = value;
                RaisePropertyChanged("ManufacturerName");
            }
        }

        public string ProductRequestInfo { get; set; }

        public IUnit SelectedUnit {
            get => _selectedUnit;
            set {
                _selectedUnit = value;
                RaisePropertyChanged("SelectedUnit");

                IsDensityAccepted = value is VolumeUnit || value is MassAndWeightUnit;
                ShouldAllowCreation();
            }
        }

        public string SelectedUnitText {
            get => _selectedUnitText;
            set {
                if (_selectedUnitText != value) {
                    _selectedUnitText = value;
                }
            }
        }

        public decimal? Density {
            get =>
                SpecificGravity.HasValue ?
                    (decimal?) (DataSourceCultureInformation.UsesImperialUnits ?
                        AgC.UnitConversion.Density.SpecificGravity.Self.GetMeasure((double) SpecificGravity.Value).CreateAs(PoundsPerGallon.Self).Value :
                        (double) SpecificGravity.Value) :
                    null;
            set {
                SpecificGravity = value;
                RaisePropertyChanged("Density");
                ShouldAllowCreation();
            }
        }

        private decimal? _specificGravity;
        private decimal? SpecificGravity {
            get => _specificGravity;
            set =>
                _specificGravity = value.HasValue ?
                    (decimal?) (DataSourceCultureInformation.UsesImperialUnits ?
                        PoundsPerGallon.Self.GetMeasure((double) value.Value).GetValueAs(AgC.UnitConversion.Density.SpecificGravity.Self) :
                        (double) value.Value) :
                    null;
        }

        public static string DensityText => ApplicationEnvironment.CurrentDataSource.DatasourceCulture.Equals("en-US") ?
                Strings.LbsGallon_Text :
                Strings.KilogramsPerLiter_Text;

        public FertilizerFormulation Formulation {
            get => _formulation;
            set {
                _formulation = value;
                RaisePropertyChanged("Formulation");
            }
        }

        public bool IsFormulationNull { get => _isNullFormulation;
            set { _isNullFormulation = value; ShouldAllowCreation(); } }

        public ObservableCollection<string> ProductTypes { get; set; }
        public string SelectedProductType {
            get => _selectedProductType;
            set {
                _selectedProductType = value;
                RaisePropertyChanged("SelectedProductType");

                if (SelectedProductType == FertilizerType) {
                    IsShowingCreate = true;
                    IsCreatingFertilizer = true;
                    BuildFertilizerUnitList();
                    CurrentOption = Strings.Liquid_Text;
                    ShouldAllowCreation();
                } else if (SelectedProductType == ServiceType) {
                    AllowCreate = true;
                    IsShowingCreate = true;
                    IsCreatingFertilizer = false;
                    BuildServiceUnitList();
                    RaisePropertyChanged(() => AllowCreate);
                } else {
                    AllowCreate = true;
                    IsShowingCreate = false;
                    IsCreatingFertilizer = false;
                    RaisePropertyChanged(() => AllowCreate);
                }
            }
        }

        public ObservableCollection<IUnit> AvailableUnits { get; set; }

        public bool IsShowingCreate {
            get => _isShowingCreate;
            set {
                _isShowingCreate = value;
                RaisePropertyChanged("IsShowingCreate");
            }
        }

        public bool IsCreatingFertilizer {
            get => _isCreatingFertilizer;
            set {
                _isCreatingFertilizer = value;
                RaisePropertyChanged("IsCreatingFertilizer");
                RaisePropertyChanged("IsCreatingService");
            }
        }

        public bool IsCreatingService => !IsCreatingFertilizer;

        public bool IsDensityAccepted {
            get => _isDensityAccepted;
            set {
                if (_isDensityAccepted != value) {
                    _isDensityAccepted = value;
                    RaisePropertyChanged("IsDensityAccepted");
                }
            }
        }

        public string CurrentOption
        {
            get => _currentOption;
            set 
            { 
                _currentOption = value;
                ShouldAllowCreation();

                if (SelectedProductType == FertilizerType)
                {
                    if (CurrentOption == Strings.Solid_Text)
                    {
                        List<IUnit> units = AvailableUnits.ToList();
                        foreach (var unit in units)
                        {
                            if (unit is VolumeUnit)
                            {
                                AvailableUnits.Remove(unit);
                            }
                        }
                        SelectedUnit = AvailableUnits[0];
                    }
                    else
                    {
                        BuildFertilizerUnitList();
                    }
                }
            }
        }

        public List<string> ProductType
        {
            get;
            set;
        }

        private void ShouldAllowCreation()
        {
            if (SelectedProductType == FertilizerType)
            {
                if (CurrentOption == Strings.Solid_Text)
                {
                    AllowCreate = IsFormulationNull != true;
                    RaisePropertyChanged(() => AllowCreate);
                }
                else
                {
                    AllowCreate = Density > 0 && IsFormulationNull == false || SelectedProductType == ServiceType;
                    RaisePropertyChanged(() => AllowCreate);
                }
            }
        }

        //TODO: Change based on datasource
        private void BuildFertilizerUnitList() {
            AvailableUnits.Clear();

            if (DataSourceCultureInformation.UsesImperialUnits) {
                AvailableUnits.Add(Gallon.Self);
                AvailableUnits.Add(Quart.Self);
                AvailableUnits.Add(ShortTon.Self);
                AvailableUnits.Add(Pound.Self);
                AvailableUnits.Add(AcreInch.Self);
                AvailableUnits.Add(Acre.Self);
            }

            AvailableUnits.Add(Liter.Self);
            AvailableUnits.Add(MetricTon.Self);
            AvailableUnits.Add(Kilogram.Self);
            AvailableUnits.Add(Hectare.Self);
            SelectedUnit = DataSourceCultureInformation.UsesImperialUnits ? AvailableUnits.First(x => x.Equals(Gallon.Self)) : AvailableUnits.First(x => x.Equals(Liter.Self));
        }

        private void BuildServiceUnitList() {
            AvailableUnits.Clear();
            if (DataSourceCultureInformation.UsesImperialUnits) {
                AvailableUnits.Add(Acre.Self);
            }

            AvailableUnits.Add(Hectare.Self);
            AvailableUnits.Add(Hour.Self);
            AvailableUnits.Add(Day.Self);

            // TODO: Add previously used UCP units

            SelectedUnit = AvailableUnits.First();
        }

        private void CreateProduct() {
            var productId = new ProductId(ApplicationEnvironment.CurrentDataSourceId, Guid.NewGuid());
            var md = new MessageMetadata(_clientEndpoint.DeviceId, _clientEndpoint.UserId);

            var sg = SpecificGravity;
            decimal? sgVal = sg > 0 ? sg : null;

            if (SelectedProductType == FertilizerType) {
                var createFertilizerCommand = new CreateFertilizerProduct(productId, md, ProductName, ManufacturerName, Formulation, SelectedUnit.Name, sgVal);
                _clientEndpoint.SendOne(createFertilizerCommand);
                FakedMiniProduct = new MiniProduct { Id = productId.Id, Name = ProductName, Manufacturer = ManufacturerName, StdFactor = 1, StdUnit = SelectedUnit.Name, StdPackageUnit = SelectedUnit.Name, Density = (double?)sgVal, ProductType = "Fertilizer", Status = "Active", ModDate = DateTime.UtcNow };
            } else if (SelectedProductType == ServiceType) {
                IUnit unit = SelectedUnit;
                if ((unit== null || unit.ToString() != SelectedUnitText) && !string.IsNullOrWhiteSpace(SelectedUnitText)) {
                    unit = new PartUnit(SelectedUnitText);
                }
                if (unit == null) { return; }
                var createServiceCommand = new CreateServiceProduct(productId, md, ProductName, ManufacturerName, unit.Name);
                _clientEndpoint.SendOne(createServiceCommand);
                FakedMiniProduct = new MiniProduct { Id = productId.Id, Name = ProductName, Manufacturer = ManufacturerName, StdFactor = 1, StdUnit = unit.Name, StdPackageUnit = unit.Name, ProductType = "Service", Status = "Active", ModDate = DateTime.UtcNow };
            }

            _onProductCreated(FakedMiniProduct);
            Messenger.Default.Send(new UpdateResourcesProductListMessage());
            Messenger.Default.Send(new HideOverlayMessage());
            //Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
        }

        private static void Cancel() {
            Messenger.Default.Send(new HideOverlayMessage());
            //Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
        }

        private static void RequestProduct() {
            //Messenger.Default.Send<HideOverlayMessage>(new HideOverlayMessage());
            var url = @"http://www.agconnections.com/product_request";
            Process.Start(url);
        }
    }
}
