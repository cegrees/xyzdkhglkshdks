﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Secondary.Reports.Recommendation.Generators;
using Landdb.ViewModel.Secondary.Reports.WorkOrder.Generators;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using Telerik.Reporting;
using Telerik.Reporting.Processing;

namespace Landdb.ViewModel.Secondary.Reports.Recommendation
{
    public class SingleRecommendationViewModel : ViewModelBase
    {
        IClientEndpoint endpoint;
        Dispatcher dispatcher;
        RecommendationId recId;

        public SingleRecommendationViewModel(IClientEndpoint endPoint, Dispatcher dispatcher, RecommendationId recID)
        {
            this.endpoint = endPoint;
            this.dispatcher = dispatcher;
            this.recId = recID;

            UpdateReport();

            HideReport = new RelayCommand(this.CloseReport);
            PDFExport = new RelayCommand(this.PDF);
        }

        public RelayCommand HideReport { get; private set; }
        public RelayCommand PDFExport { get; set; }
        public void CloseReport()
        {
            Messenger.Default.Send(new HideOverlayMessage() { });
        }

        public ReportSource ReportSource { get; set; }
        public ObservableCollection<ReportSource> ReportSources { get; set; }
        public IRecommendationGenerator ReportGenerator { get; set; }

        public void UpdateReport()
        {
            var generator = new RecommendationGenerator(endpoint, ApplicationEnvironment.CurrentCropYear);
            ReportGenerator = generator;
            if (ReportGenerator != null)
            {
                RecommendationQueryCriteria criteria = BuildCriteria();
                ReportSource = ReportGenerator.Generate(criteria);               
                RaisePropertyChanged("BingMapLayer");
                RaisePropertyChanged("ReportSource");
            }
        }

        RecommendationQueryCriteria BuildCriteria()
        {
            RecommendationQueryCriteria criteria = new RecommendationQueryCriteria();

            if (recId != null)
            {
                criteria.SelectedRecommendationId = recId;
            }

            return criteria;
        }

        public void PDF()
        {
            try { 
            if (ReportSource == null) return;
            ReportSource reportExport = ReportSource;

            ReportProcessor reportProcessor = new ReportProcessor();
            System.Collections.Hashtable deviceInfo =
            new System.Collections.Hashtable();
            RenderingResult result = reportProcessor.RenderReport("PDF", reportExport, deviceInfo);

            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "Adobe Reader|*.pdf";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;

            Nullable<bool> resultSaved = saveFileDialog1.ShowDialog();

            // Process save file dialog box results 
            if (resultSaved == true)
            {
                // Save document 
                string filename = saveFileDialog1.FileName;

                try
                {
                    using (System.IO.FileStream fs = new System.IO.FileStream(filename, System.IO.FileMode.Create))
                    {
                        fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
                    }

                    System.Diagnostics.Process.Start(filename);
                }
                catch (Exception ex) { }

            }
            }
            catch (Exception ex)
            {
                //do nothing
            }
        }
    }
}
