﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Secondary.Reports.Recommendation
{
    public class RecommendationQueryCriteria
    {
        public RecommendationQueryCriteria()
        {

        }

        public RecommendationId SelectedRecommendationId { get; set; }
    }
}
