﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.Recommendation.Generators
{
    public interface IRecommendationGenerator
    {
        ReportSource Generate(RecommendationQueryCriteria criteria);
        string DisplayName { get; }
    }
}
