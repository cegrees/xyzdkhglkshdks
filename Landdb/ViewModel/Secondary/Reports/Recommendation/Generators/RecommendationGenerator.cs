﻿using AgC.UnitConversion;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Domain.ReadModels.Map;
using Landdb.Domain.ReadModels.Recommendation;
using Landdb.Domain.ReadModels.Shared;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.ReportModels.WorkOrder;
using Landdb.ReportServices;
using Landdb.ViewModel.Secondary.Map;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Threading;
using Landdb.Resources;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.Recommendation.Generators
{
    public class RecommendationGenerator : IRecommendationGenerator
    {
        IClientEndpoint endpoint;
        int cropYear;
        //InMemoryFeatureLayer fieldsLayer = new InMemoryFeatureLayer();
        //LayerOverlay layerOverlay = new LayerOverlay();
        List<string> mapData = new List<string>();
        string currentPPE = string.Empty;
        List<ProductWorkerProtection> ProductPPEList;

        public RecommendationGenerator(IClientEndpoint endPoint,  int cropYear)
        {
            this.endpoint = endPoint;
            this.cropYear = cropYear;
            ProductPPEList = new List<ProductWorkerProtection>();
        }

        public Telerik.Reporting.ReportSource Generate(RecommendationQueryCriteria criteria)
        {
            ReportBook book = new ReportBook();
            var rs = new InstanceReportSource();

            var recData = endpoint.GetView<RecommendationView>(criteria.SelectedRecommendationId).Value;
            var inventory = endpoint.GetView<InventoryListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new InventoryListView());
            var flattenedHierarchy = endpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new FlattenedTreeHierarchyView());
            var masterlist = endpoint.GetMasterlistService();


            if (recData != null)
            {

                Task[] ppeTask = new Task[1];
                ppeTask[0] = Task.Factory.StartNew(() =>
                {
                    if (NetworkStatus.IsInternetAvailable())
                    {
                        ProductPPEList.AddRange(GetPPE(recData.Products));
                    }
                });

                var data = new WorkOrderData();
                data.DatasourceName = ApplicationEnvironment.CurrentDataSourceName;
                //data.StreetAddress = 
                data.WorkOrderName = recData.RecTitle;
                data.WorkOrderDate = recData.CreatedDateTime.ToLocalTime().ToShortDateString();
                data.WorkOrderType = recData.ApplicationStrategy.ToString();
                data.TimingEvent = recData.TimingEvent;

                data.TankInformationVisibility = recData.TankInformation == null ? false : true;
                if (recData.TankInformation != null)
                {
                    data.TankSizeValue = recData.TankInformation.TankSizeValue;
                    data.TankSizeUnit = recData.TankInformation.TankSizeUnit;
                    data.CarrierPerAreaValue = recData.TankInformation.CarrierPerAreaValue;
                    data.CarrierPerAreaUnit = recData.TankInformation.CarrierPerAreaUnit;
                    data.TotalCarrierValue = recData.TankInformation.TotalCarrierValue;
                    data.TotalCarrierUnit = recData.TankInformation.TotalCarrierUnit;
                    data.TankCount = recData.TankInformation.TankCount > 0 ? (decimal?)recData.TankInformation.TankCount : null;
                }

                foreach (var prod in recData.Products)
                {
                    var masterlistProduct = masterlist.GetProduct(prod.ProductId);
                    var czCropIds = from m in recData.CropZones
                                    select endpoint.GetView<CropZoneDetailsView>(m.Id).Value.CropId;
                    var productService = new ProductServices(endpoint);
                    ReiPhi reiPhi = productService.ReturnLargestReiPhi(prod.ProductId, czCropIds.ToList()); //new ReiPhi();

                    var aiList = from i in masterlistProduct.ActiveIngredients
                                 select masterlist.GetActiveIngredientDisplay(new ActiveIngredientId(i.Id));

                    string ai = string.Join(",", aiList.ToArray());

                    var registrationNumber = masterlistProduct.RegistrationNumber;
                    string epaNoTrailingChar = string.Empty;
                    if (registrationNumber == null) { epaNoTrailingChar = string.Empty; }
                    else
                    {
                        Regex epaNumberRegex = new Regex("([1-9][0-9]*)");

                        var matches = from m in epaNumberRegex.Matches(registrationNumber).Cast<Match>()
                                      select m.Value;
                        epaNoTrailingChar = string.Join("-", matches.ToArray());
                    }

                    var displayRate = prod.RatePerAreaValue != null ? DisplayMeasureConverter.ConvertToRateDisplayMeasure(prod.ProductId, (decimal)prod.RatePerAreaValue, prod.RatePerAreaUnit, masterlistProduct.Density, endpoint, ApplicationEnvironment.CurrentDataSourceId) : new NullMeasure();
                    var displayTotal = prod.TotalProductValue != null
                        ? DisplayMeasureConverter.ConvertToTotalDisplayMeasure(prod.ProductId,
                            (decimal) prod.TotalProductValue, prod.TotalProductUnit, masterlistProduct.Density,
                            endpoint, ApplicationEnvironment.CurrentDataSourceId)
                        : new NullMeasure();

                    var perTankMeasure = prod.RatePerTankValue != null ? UnitFactory.GetUnitByName(prod.RatePerTankUnit).GetMeasure((double)prod.RatePerTankValue, masterlistProduct.Density) : new NullMeasure();
                    var displayPerTank = DisplayMeasureConverter.ConvertToRatePerTankDisplayMeasure(prod.ProductId, perTankMeasure, endpoint, ApplicationEnvironment.CurrentDataSourceId);
                     WorkOrderData.ProductLineItemData prodData = new WorkOrderData.ProductLineItemData()
                    {
                        ApplicationMethod = string.IsNullOrEmpty(prod.ApplicationMethod) ? string.Empty : prod.ApplicationMethod,
                        EPANumber = epaNoTrailingChar,
                        Pest = prod.TargetPest == null ? string.Empty : prod.TargetPest.CommonName,
                        ProductName = masterlistProduct.Name,
                        RateUnit = displayRate != new NullMeasure() ? displayRate.Unit.Name : string.Empty,
                        RateValue = displayRate != new NullMeasure() ? displayRate.Value : 0.0,
                        RatePerTankUnit = data.WorkOrderType == "ByRatePerTank" && displayPerTank != new NullMeasure() ? displayPerTank.Unit.Name : prod.RatePerAreaUnit,
                        RatePerTank = data.WorkOrderType == "ByRatePerTank" && displayPerTank != new NullMeasure() ? displayPerTank.Value : 0.0,
                        TotalUnit = displayTotal != new NullMeasure() ? displayTotal.Unit.Name : string.Empty,
                        TotalValue = displayTotal != new NullMeasure() ? displayTotal.Value : 0.0,
                        PHI = reiPhi != null && reiPhi.Phi != null ? reiPhi.Phi.ToString() : "*",
                        REI = reiPhi != null && reiPhi.Rei != null ? reiPhi.Rei.ToString() : "*",
                        Active = ai,
                        RateType = data.WorkOrderType == "ByRatePerTank" ? Strings.RateTank_Text : Strings.RateType_Text,
                        TotalArea = (decimal)recData.TotalArea.Value * prod.CoveragePercent ?? 0,
                        PercentApplied = prod.CoveragePercent * 100 ?? 0 
                        //RatePer100 = recData.TankInformation != null ? (double)prod.RatePerTankValue / ((double)recData.TankInformation.TankSizeValue / 100) : 0
                    };

                    data.Products.Add(prodData);
                }

                List<string> filteredMapData = new List<string>();

                foreach (var cz in recData.CropZones)
                {
                    var czDetails = endpoint.GetView<CropZoneDetailsView>(cz.Id).GetValue(new CropZoneDetailsView());

                    var czTreeData = (from f in flattenedHierarchy.Items
                                      where f.CropZoneId == cz.Id
                                      select f).FirstOrDefault();

                    //var reportedArea = ReportedArea(czDetails);
                    var area = recData.CropZones.SingleOrDefault(x => x.Id == cz.Id).Area;
                    WorkOrderData.FieldLineItemData field = new WorkOrderData.FieldLineItemData()
                    {
                        Area = area.Value,
                        CenterLatLong = czDetails.CenterLatLong,
                        Crop = masterlist.GetCropDisplay(czDetails.CropId),
                        CropZone = czDetails.Name,
                        Field = czTreeData != null ? czTreeData.FieldName : endpoint.GetView<FieldDetailsView>(czDetails.FieldId).Value.FieldNameByCropYear[cropYear], //.Name,
                        Farm = czTreeData != null ? czTreeData.FarmName : Strings.UnknownFarm_Text,
                    };

                    data.Fields.Add(field);

                    var maps = endpoint.GetView<ItemMap>(cz.Id);
                    try
                    {
                        if (maps.HasValue)
                        {
                            if (maps.Value.MostRecentMapItem != null && maps.Value.MostRecentMapItem.MapData != string.Empty && maps.Value.MostRecentMapItem.MapData != "MULTIPOLYGON EMPTY") {
                                filteredMapData.Add(maps.Value.MostRecentMapItem.MapData);
                            }
                        }
                    }
                    finally { }
                }
                if (filteredMapData.Count() > 0)
                {
                    var converter = new ConvertToBitmap(filteredMapData, this.endpoint, 0, "", "");
                    data.MapImage = converter.BitMap;
                }

                foreach (var applicator in recData.Applicators)
                {
                    WorkOrderData.ApplicatorLineItemData appLineItem = new WorkOrderData.ApplicatorLineItemData()
                    {
                        Name = applicator.PersonName,
                        Company = applicator.CompanyName,
                        LicenseNumber = applicator.LicenseNumber,
                        ExpirationDate = applicator.Expires,
                        DisplayName = string.Format("{0} ({1})", applicator.PersonName, applicator.CompanyName),
                    };

                    data.Applicators.Add(appLineItem);
                }

                //add all the ppe info into data....
                Task.WaitAll(ppeTask);
                foreach (var prod in ProductPPEList)
                {
                    foreach (var ppe in prod.WorkerProtections)
                    {
                        var mlp = masterlist.GetProduct(prod.ProductId);
                        data.WorkerProtectionItems.Add(new WorkOrderData.WorkerProtectionItem() { ProductName = mlp.Name, PPE = ppe.PPE, ReEntry = ppe.PPEReentry, SignalWord = mlp.SignalWord, RestrictedUse = mlp.RestrictedUse ? "True" : "False" });
                    }
                }

                data.Authorizer = recData.Authorization != null ? recData.Authorization.PersonName : string.Empty;
                data.AuthorizerDate = recData.Authorization != null ? recData.Authorization.AuthorizationDate : DateTime.Now;

                data.CropYear = cropYear;
                data.Notes = recData.Notes;

                var report = new Landdb.Views.Secondary.Reports.Recommendation.Recommendation();
                report.DataSource = data;
                report.Name = Strings.ReportName_Recommendation_Text;

                book.Reports.Add(report);
            }


            rs.ReportDocument = book;
            return rs;
        }

        public string DisplayName
        {
            get { throw new NotImplementedException(); }
        }

        public List<ProductWorkerProtection> GetPPE(List<Landdb.Domain.ReadModels.Recommendation.RecommendedProduct> includedProducts)
        {
            List<ProductWorkerProtection> ppeList = new List<ProductWorkerProtection>();
            foreach (var prod in includedProducts)
            {
                try
                {
                    string dataText = string.Empty;

                    //create task....and wait for it to complete...
                    Task getPPE = Task.Factory.StartNew(() =>
                    {
                        dataText = GetWorkerProtectionData(prod.ProductId);
                    });

                    getPPE.Wait();
                    JObject data = JObject.Parse(dataText);

                    var ppe = from d in JArray.Parse(data["ProductWorkerProtections"].ToString())
                              select JArray.Parse(d["WorkerProtections"].ToString());

                    var workerProtectionList = from d in JArray.Parse(data["ProductWorkerProtections"].ToString())
                                               from i in JArray.Parse(d["WorkerProtections"].ToString())
                                               select new WorkerProtection { PPE = i["PPE"] != null ? i["PPE"].ToString() : string.Empty, PPEReentry = i["PPEReentry"] != null ? i["PPEReentry"].ToString() : string.Empty };
                    var prodWorkProtection = new ProductWorkerProtection() { ProductId = prod.ProductId, WorkerProtections = workerProtectionList.ToList() };
                    ppeList.Add(prodWorkProtection);
                }
                catch (Exception x)
                {

                }

            }

            return ppeList;
        }

        public string GetWorkerProtectionData(ProductId productId)
        {
            var requestUriFormat = @"{0}/workerprotections?format=json&c=en-US&u=Y29yZXkucGVya2lucw%3D%3D&p=Y29yZXlwMQ%3D%3D&ProductIds={1}";
            var requestUri = string.Format(requestUriFormat, Infrastructure.ApplicationEnvironment.MasterlistRemoteUri, productId.Id);

            WebClient client = new WebClient();
            client.Headers.Add("Authorization", "basic QWdDQVBJOlJhbmRvbU1vbmtleVN1cmZzT25MYXZhIQ==");
            string data = client.DownloadString(new Uri(requestUri));

            return data;
        }
    }

    public class ProductWorkerProtection
    {
        public ProductId ProductId { get; set; }
        public List<WorkerProtection> WorkerProtections { get; set; }
        //public string PPE { get; set; }
        //public string PPEReEntry { get; set; }
    }

    public class WorkerProtection
    {
        public string PPE { get; set; }
        public string PPEReentry { get; set; }
    }
}
