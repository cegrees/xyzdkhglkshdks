﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Converters.Reports;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Reports;
using Landdb.ViewModel.Secondary.Reports.Inventory.Generators;
using Microsoft.Win32;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Telerik.Reporting;
using Telerik.Reporting.Processing;

namespace Landdb.ViewModel.Secondary.Reports.Inventory
{
    public class InventoryReportCreatorViewModel : ViewModelBase
    {
        IClientEndpoint endpoint;
        int cropYear;

        ReportTemplateViewModelToFile templateService = new ReportTemplateViewModelToFile();
        IInventoryReportGenerator selectedReportGenerator;
        InventoryQueryCriteria criteria;
        ReportTemplateViewModel template;
        string reportType;

        public InventoryReportCreatorViewModel(IClientEndpoint endpoint, int cropyear, ReportTemplateViewModel template, string reportType)
        {
            this.cropYear = cropyear;
            this.endpoint = endpoint;

            
            CancelCommand = new RelayCommand(OnCancelReport);
            SaveCommand = new RelayCommand(SaveTemplate);

            InfoPageModel = new InventoryReportInfoViewModel(endpoint, cropYear);

            ReportGenerators = new ObservableCollection<IInventoryReportGenerator>();
            //Call to add all the Applied Product Reports
            AddAppliedProductReportGenerators(endpoint, cropYear);

            PDFExport = new RelayCommand(this.PDF);

            UpdateReport();
            this.reportType = reportType;

            if (template != null) { InitializeFromTemplate(template); this.template = template; };

            SelectedPageIndex = 0;
        }

        public ICommand CancelCommand { get; private set; }
        public RelayCommand PDFExport { get; set; }
        public ICommand SaveCommand { get; set; }

        public int CropYear { get { return ApplicationEnvironment.CurrentCropYear; } }
        public int SelectedPageIndex { get; set; }
        public string TemplateName { get; set; }

        public ReportSource ReportSource { get; set; }

        public InventoryReportInfoViewModel InfoPageModel { get; set; }

        public ObservableCollection<IInventoryReportGenerator> ReportGenerators { get; private set; }
        public IInventoryReportGenerator SelectedReportGenerator
        {
            get { return selectedReportGenerator; }
            set
            {
                selectedReportGenerator = value;
                UpdateReport();
                RaisePropertyChanged("SelectedReportGenerator");
            }
        }

        void OnCancelReport()
        {
            Messenger.Default.Send<ShowMainViewMessage>(new ShowMainViewMessage());
        }

        public void UpdateReport()
        {
            if (SelectedReportGenerator != null)
            {
                criteria = BuildCriteria();
                ReportSource = SelectedReportGenerator.Generate(criteria);
                RaisePropertyChanged("ReportSource");
            }
        }

        InventoryQueryCriteria BuildCriteria()
        {
            InventoryQueryCriteria criteria = new InventoryQueryCriteria();

            var includedProds = from p in InfoPageModel.Products
                                where p.Checked == true
                                select p.ProductId;
            criteria.IncludedProducts.AddRange(includedProds);
            criteria.StartDate = InfoPageModel.ReportStartDate;
            criteria.EndDate = InfoPageModel.ReportEndDate;

            return criteria;
        }

        void AddAppliedProductReportGenerators(IClientEndpoint clientEndpoint, int cropYear)
        {
            ReportGenerators.Add(new InventorySummaryGenerator(endpoint, cropYear));
            ReportGenerators.Add(new InventoryExcelDumpGenerator(clientEndpoint, cropYear));
            ReportGenerators.Add(new VendorSummaryByProductGenerator(clientEndpoint, cropYear));
            ReportGenerators.Add(new VendorSummaryByInvoiceGenerator(clientEndpoint, cropYear));
        }

        public void PDF()
        {
            try
            {
                if (ReportSource == null) return;
                ReportSource reportExport = ReportSource;

                ReportProcessor reportProcessor = new ReportProcessor();
                System.Collections.Hashtable deviceInfo =
                new System.Collections.Hashtable();

                if (reportExport != null)
                {
                    RenderingResult result = reportProcessor.RenderReport("PDF", reportExport, deviceInfo);


                    SaveFileDialog saveFileDialog1 = new SaveFileDialog();

                    saveFileDialog1.Filter = "Adobe Reader|*.pdf";
                    saveFileDialog1.FilterIndex = 2;
                    saveFileDialog1.RestoreDirectory = true;

                    Nullable<bool> resultSaved = saveFileDialog1.ShowDialog();

                    // Process save file dialog box results 
                    if (resultSaved == true)
                    {
                        // Save document 
                        string filename = saveFileDialog1.FileName;

                        try
                        {
                            using (System.IO.FileStream fs = new System.IO.FileStream(filename, System.IO.FileMode.Create))
                            {
                                fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
                            }

                            System.Diagnostics.Process.Start(filename);
                        }
                        catch (Exception ex) { }

                    }
                }
            }
            catch (Exception ex)
            {
                //do nothing
            }
        }

        void SaveTemplate()
        {
            criteria = BuildCriteria();
            if (template != null && template.TemplateName == TemplateName)
            {
                ReportTemplateViewModel editedTemplate = new ReportTemplateViewModel()
                {
                    TemplateName = TemplateName,
                    Created = template.Created,
                    LastUsed = DateTime.Now,
                    CriteriaJSON = JsonConvert.SerializeObject(criteria),
                    ReportType = template.ReportType,
                    DataSourceId = template.DataSourceId
                };

                //TODO :: CALL TO EDIT PREVIOUS TEMPLATE...
                templateService.EditTemplate(editedTemplate);
            }
            else
            {
                ReportTemplateViewModel newTemplate = new ReportTemplateViewModel()
                {
                    TemplateName = TemplateName,
                    Created = DateTime.Now,
                    LastUsed = DateTime.Now,
                    CriteriaJSON = JsonConvert.SerializeObject(criteria),
                    ReportType = reportType,
                    DataSourceId = ApplicationEnvironment.CurrentDataSourceId
                };

                //TODO :: call service to save the new report template
                templateService.SaveNewTemplate(newTemplate);
                template = newTemplate;
            }

            RaisePropertyChanged("NewTemplateCreated", null, new ReportTemplateViewModel(), true);
        }

        void InitializeFromTemplate(ReportTemplateViewModel temp)
        {

            TemplateName = temp.TemplateName;

            InventoryQueryCriteria crit = JsonConvert.DeserializeObject<InventoryQueryCriteria>(temp.CriteriaJSON);

            var dsID = new DataSourceId(ApplicationEnvironment.CurrentDataSourceId);

            var unSelectedProds = from infoProds in InfoPageModel.Products
                                  where !crit.IncludedProducts.Contains(infoProds.ProductId)
                                  select infoProds;
            unSelectedProds.ForEach(x => x.Checked = false);
            InfoPageModel.ReportStartDate = crit.StartDate;
            InfoPageModel.ReportEndDate = crit.EndDate;
        }
    }
}
