﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Infrastructure;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Secondary.Reports.Inventory
{
    public class InventoryReportInfoViewModel : ViewModelBase
    {
        bool selectAll;
        bool includeCP;
        bool includeFert;
        bool includeSeed;
        bool includeService;
        bool filter;
        int cropYear;
        DateTime? reportStartDate;
        DateTime? reportEndDate;

        IClientEndpoint endPoint;
        IMasterlistService service;
        Dictionary<ProductId, InventoryListItem> usedProducts;

        public InventoryReportInfoViewModel(IClientEndpoint clientEndpoint, int cropYear)
        {
            this.endPoint = clientEndpoint;
            this.cropYear = cropYear;

            
            service = endPoint.GetMasterlistService();
            usedProducts = new Dictionary<ProductId, InventoryListItem>();
            var inventoryMaybe = endPoint.GetView<InventoryListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear));
            usedProducts = inventoryMaybe.HasValue ? inventoryMaybe.Value.Products : new Dictionary<ProductId, InventoryListItem>(); ;

            Messenger.Default.Register<PropertyChangedMessage<bool>>(this, UpdateFilterHeader);

            InitializeProperties();

        }

        public DateTime? ReportStartDate
        {
            get { return reportStartDate; }
            set
            {
                reportStartDate = value;

                RaisePropertyChanged("ReportStartDate");
            }
        }

        public DateTime? ReportEndDate
        {
            get { return reportEndDate; }
            set
            {
                reportEndDate = value;

                RaisePropertyChanged("ReportEndDate");
            }
        }

        public ObservableCollection<ProductTemplateViewModel> Products { get; set; }
        public string FilterHeader { get; set; }
        public bool IsFilterVisible
        {
            get
            {
                return filter;
            }
            set
            {
                filter = value;
                RaisePropertyChanged("IsFilterVisible");
            }
        }
        public bool SelectAll
        {
            get { return selectAll; }
            set
            {
                selectAll = value;
                if (!selectAll)
                {
                    foreach (var p in Products)
                    {
                        p.Checked = false;
                    }
                }
                else
                {
                    foreach (var p in Products)
                    {
                        p.Checked = true;
                    }
                }

                FilterHeader = $"{Strings.FilterHeader_IncludedProducts_Text}: " + Products.Where(x => x.Checked == true).Count();
                RaisePropertyChanged("FilterHeader");
                RaisePropertyChanged("Products");
            }
        }
        public bool IncludeCP
        {
            get
            {
                return includeCP;
            }
            set
            {
                includeCP = value;
                ProductsOfType("CropProtection", includeCP);
            }
        }
        public bool IncludeFert
        {
            get
            {
                return includeFert;
            }
            set
            {
                includeFert = value;
                ProductsOfType("Fertilizer", includeFert);
            }
        }
        public bool IncludeSeed
        {
            get
            {
                return includeSeed;
            }
            set
            {
                includeSeed = value;
                ProductsOfType("Seed", includeSeed);
            }
        }

        public bool IncludeService
        {
            get
            {
                return includeService;
            }
            set
            {
                includeService = value;
                ProductsOfType("Service", includeService);
            }
        }

        public void InitializeProperties()
        {
            Products = new ObservableCollection<ProductTemplateViewModel>();
            IncludeCP = true;
            IncludeFert = true;
            IncludeSeed = true;
            IncludeService = true;
            SelectAll = true;
            IsFilterVisible = false;
        }

        public void ProductsOfType(string prodType, bool add)
        {
            if (!add)
            {
                var cps = Products.ToList();
                cps.RemoveAll(x => x.Type == prodType);
                Products = new ObservableCollection<ProductTemplateViewModel>(cps);
                RaisePropertyChanged("Products");
            }
            else
            {
                foreach (var p in usedProducts)
                {
                    if (p.Value.ApplicationCount > 0 || p.Value.InvoiceCount > 0)
                    {
                        var details = service.GetProduct(p.Key);
                        if (details != null)
                        {
                            var ptVM = new ProductTemplateViewModel() { Name = details.Name, Manufacturer = details.Manufacturer, Checked = SelectAll, Id = details.Id, ProductId = p.Key, Type = details.ProductType };

                            if (Products.Count(x => x.Id == ptVM.Id) <= 0)
                            {
                                Products.Add(ptVM);
                            }
                        }
                    }
                    else
                    {
                        var nonDescript = p.Value;
                    }
                }

                Products = new ObservableCollection<ProductTemplateViewModel>(Products.OrderBy(x => x.Name));
                RaisePropertyChanged("Products");
            }

            FilterHeader = $"{Strings.FilterHeader_IncludedProducts_Text}: " + Products.Where(x => x.Checked == true).Count();
            RaisePropertyChanged("FilterHeader");
        }

        void UpdateFilterHeader(PropertyChangedMessage<bool> _checked)
        {
            if (_checked.PropertyName == "Checked")
            {
                FilterHeader = $"{Strings.FilterHeader_IncludedProducts_Text}: " + Products.Where(x => x.Checked == true).Count();
                RaisePropertyChanged("FilterHeader");
            }
        }
    }

    public class ProductTemplateViewModel : ViewModelBase
    {
        bool _checked;
        public ProductTemplateViewModel()
        {

        }

        public bool Checked
        {
            get { return _checked; }
            set
            {

                _checked = value;
                RaisePropertyChanged("Checked", true, _checked, true);
            }
        }
        public string Name { get; set; }
        public string Manufacturer { get; set; }
        public Guid Id { get; set; }
        public ProductId ProductId { get; set; }
        public string Type { get; set; }
    }
}
