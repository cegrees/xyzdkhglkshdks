﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.Inventory.Generators
{
    public interface IInventoryReportGenerator
    {
        ReportSource Generate(InventoryQueryCriteria crit);
        string DisplayName { get; }
    }
}
