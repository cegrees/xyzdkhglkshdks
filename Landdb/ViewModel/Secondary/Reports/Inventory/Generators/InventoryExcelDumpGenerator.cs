﻿using AgC.UnitConversion;
using DocumentFormat.OpenXml.Drawing.Charts;
using ExportToExcel;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Domain.ReadModels.Invoice;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ReportModels.Inventory;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Landdb.Resources;

namespace Landdb.ViewModel.Secondary.Reports.Inventory.Generators
{
    public class InventoryExcelDumpGenerator : IInventoryReportGenerator
    {
        string filename;
        IClientEndpoint endpoint;
        int cropYear;

        public InventoryExcelDumpGenerator(IClientEndpoint endpoint, int cropYear)
        {
            this.endpoint = endpoint;
            this.cropYear = cropYear;

        }

        public void Generate(InventoryQueryCriteria crit)
        {
            //create inventory items and send them to excel...
            var masterlist = endpoint.GetMasterlistService();
            var inventoryItemList = endpoint.GetView<InventoryListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear));
            var invoiceList = endpoint.GetView<InvoiceListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear));

            //TO DO :: SWITCH THIS OUT FOR THE APPLICATION LIST VIEW
            var applicationData = endpoint.GetView<CropZoneApplicationDataView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new CropZoneApplicationDataView());

            if (invoiceList.HasValue)
            {
                var data = new List<InventoryItem>();

                //data.IsSubSet = includedItems.Count() < cnt ? true : false;

                List<CropZoneApplicationDataItem> appData = new List<CropZoneApplicationDataItem>();
                List<InvoiceView> includedInvoiceDetailList = new List<InvoiceView>();
                List<InvoiceProduct> invoiceProductDetails = new List<InvoiceProduct>();

                if (crit.StartDate != null && crit.EndDate != null)
                {
                    var trueEndDate = crit.EndDate.HasValue ? crit.EndDate.Value.AddHours(24.0 - (double)crit.EndDate.Value.Hour) : new DateTime?();
                    appData = (from x in applicationData.Items
                               where x.EndDate >= crit.StartDate &&
                               x.EndDate <= trueEndDate && crit.IncludedProducts.Contains(x.ProductId) == true
                               select x).ToList();

                    includedInvoiceDetailList = invoiceList != null ? (from i in invoiceList.Value.Invoices
                                                                       where i.InvoiceDate >= crit.StartDate && i.InvoiceDate <= trueEndDate
                                                                       select endpoint.GetView<InvoiceView>(i.Id).Value).ToList() : null;
                }
                else if (crit.StartDate == null && crit.EndDate != null)
                {
                    var trueEndDate = crit.EndDate.HasValue ? crit.EndDate.Value.AddHours(24.0 - (double)crit.EndDate.Value.Hour) : new DateTime?();
                    appData = (from x in applicationData.Items
                               where x.EndDate <= trueEndDate && crit.IncludedProducts.Contains(x.ProductId) == true
                               select x).ToList();

                    includedInvoiceDetailList = invoiceList != null ? (from i in invoiceList.Value.Invoices
                                                                       where i.InvoiceDate <= crit.EndDate
                                                                       select endpoint.GetView<InvoiceView>(i.Id).Value).ToList() : null;
                }
                else if (crit.EndDate == null && crit.StartDate != null)
                {
                    appData = (from x in applicationData.Items
                               where x.EndDate >= crit.StartDate && crit.IncludedProducts.Contains(x.ProductId) == true
                               select x).ToList();

                    includedInvoiceDetailList = invoiceList != null ? (from i in invoiceList.Value.Invoices
                                                                       where i.InvoiceDate >= crit.StartDate
                                                                       select endpoint.GetView<InvoiceView>(i.Id).Value).ToList() : null;
                }
                else
                {
                    appData = (from x in applicationData.Items
                               where crit.IncludedProducts.Contains(x.ProductId) == true
                               select x).ToList();

                    includedInvoiceDetailList = invoiceList != null ? (from i in invoiceList.Value.Invoices
                                                                       select endpoint.GetView<InvoiceView>(i.Id).Value).ToList() : null;
                }

                invoiceProductDetails = (from i in includedInvoiceDetailList
                                         from p in i.Products
                                         where crit.IncludedProducts.Contains(p.ProductId)
                                         select p).ToList();

                List<ProductId> distinctProdList = invoiceProductDetails.Distinct(x => x.ProductId).Select(p => p.ProductId).ToList();
                var distinctAppliedProductIDs = appData.Distinct(x => x.ProductId).Select(p => p.ProductId);
                var nonRepresentedProducts = from p in distinctAppliedProductIDs
                                             where distinctProdList.Contains(p) == false
                                             select p;

                distinctProdList.AddRange(nonRepresentedProducts);

                foreach (var prodId in distinctProdList)
                {
                    var mlp = endpoint.GetMasterlistService().GetProduct(prodId);
                    var prod = new InventoryItem();

                    prod.Manufacturer = mlp.Manufacturer;
                    prod.Product = mlp.Name;
                    prod.RegistrationNumber = mlp.RegistrationNumber;
                    prod.ProductType = mlp.ProductType;
                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    ////This adds up the invoiced products during the selected time frame
                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    var selectedInvoiceProdList = from j in invoiceProductDetails
                                                  where j.ProductId == prodId
                                                  select j;

                    IUnit invoicedUnit = null;
                    double invoicedTotal = 0.0;
                    decimal totalCost = 0m;
                    foreach (var invoiceProd in selectedInvoiceProdList)
                    {
                        var totalPurchasedVal = invoiceProd.TotalProductValue;
                        var totalPurchasedUnit = UnitFactory.GetPackageSafeUnit(invoiceProd.TotalProductUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                        var totalMeasure = totalPurchasedUnit.GetMeasure((double)totalPurchasedVal, mlp.Density);
                        totalCost += invoiceProd.TotalCost;
                        if (invoicedUnit == null)
                        {
                            invoicedUnit = totalMeasure.Unit;
                            invoicedTotal += totalMeasure.Value;
                        }
                        else
                        {
                            try
                            {
                                var convertedTotal = totalMeasure.CanConvertTo(invoicedUnit) ? totalMeasure.CreateAs(invoicedUnit) : totalMeasure;
                                invoicedTotal += convertedTotal.Value;
                            }
                            catch (Exception ex)
                            {
                                invoicedTotal += totalMeasure.Value;
                            }
                        }

                    }

                    prod.PurchasedQuantity = (decimal)invoicedTotal;
                    prod.PurchasedUnit = invoicedUnit;
                    prod.TotalCost = totalCost;

                    if (invoicedTotal != 0)
                    {
                        prod.AverageCost = totalCost / (decimal)invoicedTotal;
                    }
                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    //This adds up the products applied during the selected timeframe
                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    var distinctApplicationlist = (from a in appData
                                                   where a.ProductId == prodId
                                                   select a.ApplicationId).Distinct();

                    List<ApplicationView> appDetailsList = (from f in distinctApplicationlist
                                                            select endpoint.GetView<ApplicationView>(f).Value).ToList();
                    var selectedAppliedProdList = from a in appDetailsList
                                                  from p in a.Products
                                                  where p.Id == prodId
                                                  select p;
                    IUnit totalUnit = null;
                    decimal totalValue = 0m;

                    foreach (var applied in selectedAppliedProdList)
                    {
                        var totalAppliedValue = applied.TotalProductValue; // * applied.FromGrowerInventory;
                        var totalAppliedUnit = UnitFactory.GetPackageSafeUnit(applied.TotalProductUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                        var totalAppliedMeasure = totalAppliedUnit.GetMeasure((double)totalAppliedValue, mlp.Density);

                        //////TO DO :: IF THEY SELECT SPECIFIC OR INVOICE PRICE I WILL NEED TO TALLY UP THE TOTAL COST INSIDE THIS LOOP

                        if (totalUnit == null)
                        {
                            totalUnit = totalAppliedUnit;
                            totalValue += (decimal)totalAppliedValue;
                        }
                        else
                        {
                            try
                            {
                                var convertedTotal = totalAppliedMeasure.CanConvertTo(totalUnit) ? totalAppliedMeasure.CreateAs(totalUnit) : totalAppliedMeasure;
                                totalValue += (decimal)convertedTotal.Value;
                            }
                            catch (Exception ex)
                            {
                                totalValue += (decimal)totalAppliedMeasure.Value;
                            }
                        }
                    }

                    prod.AppliedQuantity = totalValue;
                    prod.AppliedUnit = totalUnit;

                    prod.AppliedCost = prod.AverageCost * totalValue;
                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    if (totalUnit != null)
                    {
                        var appliedMeasure = prod.AppliedUnit.GetMeasure((double)prod.AppliedQuantity, mlp.Density);

                        try
                        {
                            var converted = appliedMeasure.CanConvertTo(prod.PurchasedUnit) ? appliedMeasure.CreateAs(prod.PurchasedUnit) : appliedMeasure;
                            prod.InventoryQuantity = prod.PurchasedQuantity - (decimal)converted.Value;
                            prod.InventoryUnit = converted.Unit;
                        }
                        catch (Exception ex)
                        {
                            prod.InventoryQuantity = prod.PurchasedQuantity - prod.AppliedQuantity;
                            prod.InventoryUnit = prod.PurchasedUnit;
                        }
                    }
                    else
                    {
                        prod.InventoryQuantity = prod.PurchasedQuantity;
                        prod.InventoryUnit = prod.PurchasedUnit;
                    }

                    prod.InventoryCost = prod.InventoryQuantity * prod.AverageCost;

                    data.Add(prod);
                }
                data = data.OrderBy(x => x.Product).ToList();

                System.Data.DataTable dt1 = new System.Data.DataTable();

                dt1 = ExportToExcel.CreateExcelFile.ListToDataTable(data, true);

                dt1.TableName = Strings.TableName_Inventory_Text;

                DataSet ds = new DataSet("excelDS");
                ds.Tables.Add(dt1);


                try
                {
                    //open up file dialog to save file....
                    //then call createexcelfile to create the excel...
                    SaveFileDialog saveFileDialog1 = new SaveFileDialog();

                    saveFileDialog1.Filter = "Excel|*.xlsx";
                    saveFileDialog1.FilterIndex = 2;
                    saveFileDialog1.RestoreDirectory = true;

                    Nullable<bool> resultSaved = saveFileDialog1.ShowDialog();

                    // Process save file dialog box results 
                    if (resultSaved == true)
                    {
                        // Save document 
                        filename = saveFileDialog1.FileName;
                        CreateExcelFile.CreateExcelDocument(ds, filename);
                    }

                    //now open file....
                    System.Diagnostics.Process.Start(filename);

                    Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
                }
                catch (Exception ex)
                {
                    Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
                    return;
                }

            }
        }

        public string DisplayName
        {
            get { return Strings.GeneratorName_InventoryExcel_Text; }
        }

        Telerik.Reporting.ReportSource IInventoryReportGenerator.Generate(InventoryQueryCriteria crit)
        {
            Generate(crit);
            return null;
        }
    }

    public class InventoryItem
    {
        public string Product { get; set; }
        public string Manufacturer { get; set; }
        public string RegistrationNumber { get; set; }
        public string ProductType { get; set; }
        public decimal PurchasedQuantity { get; set; }
        public IUnit PurchasedUnit { get; set; }
        public decimal AverageCost { get; set; }
        public decimal TotalCost { get; set; }
        public decimal AppliedQuantity { get; set; }
        public IUnit AppliedUnit { get; set; }
        public decimal AppliedCost { get; set; }

        public decimal InventoryQuantity { get; set; }
        public IUnit InventoryUnit { get; set; }
        public decimal InventoryCost { get; set; }
    }
}
