﻿using AgC.UnitConversion;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Domain.ReadModels.Invoice;
using Landdb.Infrastructure;
using Landdb.ReportModels.Inventory;
using Landdb.Views.Secondary.Reports.Inventory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Landdb.Resources;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.Inventory.Generators
{
    public class VendorSummaryByProductGenerator : IInventoryReportGenerator
    {
        IClientEndpoint endpoint;
        int cropYear;
        public VendorSummaryByProductGenerator(IClientEndpoint endpoint, int cropYear)
        {
            this.endpoint = endpoint;
            this.cropYear = cropYear;
        }
        public string DisplayName
        {
            get
            {
                return Strings.GeneratorName_VendorSummaryByProduct_Text;
            }
        }

        public ReportSource Generate(InventoryQueryCriteria crit)
        {
            ReportBook book = new ReportBook();
            var rs = new InstanceReportSource();

            //var invData = endpoint.GetView<InvoiceView>(crit.SelectedInvoice).Value;
            var masterlist = endpoint.GetMasterlistService();
            var inventoryItemList = endpoint.GetView<InventoryListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear));
            var invoiceListMaybe = endpoint.GetView<InvoiceListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear));
            //var applicationData = endpoint.GetView<CropZoneApplicationDataView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new CropZoneApplicationDataView());
            InvoiceListView invoiceList = null;
            if (invoiceListMaybe.HasValue)
            {
                invoiceList = invoiceListMaybe.Value;
            }

            List<InvoiceView> includedInvoiceDetailList = new List<InvoiceView>();
            List<InvoiceProduct> invoiceProductDetails = new List<InvoiceProduct>();

            if (crit.StartDate != null && crit.EndDate != null)
            {
                var trueEndDate = crit.EndDate.HasValue ? crit.EndDate.Value.AddHours(24.0 - (double)crit.EndDate.Value.Hour) : new DateTime?();
                trueEndDate = trueEndDate.HasValue ? trueEndDate.Value.AddMinutes(-1) : new DateTime?();

                includedInvoiceDetailList = invoiceList != null ? (from i in invoiceList.Invoices
                                                                   where i.InvoiceDate >= crit.StartDate && i.InvoiceDate <= trueEndDate
                                                                   select endpoint.GetView<InvoiceView>(i.Id).Value).ToList() : null;
            }
            else if (crit.StartDate == null && crit.EndDate != null)
            {
                var trueEndDate = crit.EndDate.HasValue ? crit.EndDate.Value.AddHours(24.0 - (double)crit.EndDate.Value.Hour) : new DateTime?();

                includedInvoiceDetailList = invoiceList != null ? (from i in invoiceList.Invoices
                                                                   where i.InvoiceDate <= crit.EndDate
                                                                   select endpoint.GetView<InvoiceView>(i.Id).Value).ToList() : null;
            }
            else if (crit.EndDate == null && crit.StartDate != null)
            {
                includedInvoiceDetailList = invoiceList != null ? (from i in invoiceList.Invoices
                                                                   where i.InvoiceDate >= crit.StartDate
                                                                   select endpoint.GetView<InvoiceView>(i.Id).Value).ToList() : null;
            }
            else
            {
                includedInvoiceDetailList = invoiceList != null ? (from i in invoiceList.Invoices
                                                                   select endpoint.GetView<InvoiceView>(i.Id).Value).ToList() : null;
            }

            var invoiceDetailsMaybe = from invoice in includedInvoiceDetailList
                                      select endpoint.GetView<InvoiceView>(invoice.Id);

            var invoiceDetails = invoiceDetailsMaybe.Where(x => x.HasValue).Select(y => y.Value);
            invoiceDetails = invoiceDetails.OrderBy(v => v.Vendor);

            List<ProductSummaryData> products = new List<ProductSummaryData>();
            foreach (var invoice in invoiceDetails)
            {

                foreach (var prod in invoice.Products)
                {
                    if (crit.IncludedProducts.Contains(prod.ProductId))
                    {
                        var mlp = endpoint.GetMasterlistService().GetProduct(prod.ProductId);

                        var selectedProduct = (from product in products
                                               where product.Vendor == invoice.Vendor && product.ProductID == prod.ProductId
                                               select product).SingleOrDefault();

                        if (selectedProduct != null)
                        {
                            selectedProduct.TotalCost += prod.TotalCost;

                            var totalMeasure = UnitFactory.GetPackageSafeUnit(prod.TotalProductUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit).GetMeasure((double)prod.TotalProductValue, mlp.Density);
                            var converted = totalMeasure.Unit != selectedProduct.TotalUnit && totalMeasure.CanConvertTo(selectedProduct.TotalUnit) ? totalMeasure.CreateAs(selectedProduct.TotalUnit) : totalMeasure;

                            selectedProduct.TotalQuantity = (decimal)converted.Value + selectedProduct.TotalQuantity;
                        }
                        else
                        {
                            var data = new ProductSummaryData();
                            data.CropYear = cropYear;
                            data.DataSourceName = ApplicationEnvironment.CurrentDataSourceName;
                            data.ProductID = prod.ProductId;
                            data.Product = mlp.Name;
                            data.Manufacturer = mlp.Manufacturer;
                            data.TotalCost = prod.TotalCost;
                            data.TotalQuantity = prod.TotalProductValue;
                            data.TotalUnit = UnitFactory.GetPackageSafeUnit(prod.TotalProductUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                            data.Vendor = invoice.Vendor;
                            data.DateRangeDisplay = $"{crit.StartDate?.ToShortDateString()} - {crit.EndDate?.ToShortDateString()}".Equals(" - ")
                                ? $"1/1/{cropYear} - 12/31/{cropYear}"
                                : $"{crit.StartDate?.ToShortDateString()} - {crit.EndDate?.ToShortDateString()}";

                            products.Add(data);
                        }
                    }
                }
            }

            products = products.OrderBy(x => x.Vendor).ThenBy(p => p.Product).ToList();
            var report = new VendorSummaryByProduct();
            report.DataSource = products;
            report.Name = Strings.ReportName_ProductSummaryByInvoice_Text;

            book.Reports.Add(report);


            rs.ReportDocument = book;
            return rs;
        }
    }
}
