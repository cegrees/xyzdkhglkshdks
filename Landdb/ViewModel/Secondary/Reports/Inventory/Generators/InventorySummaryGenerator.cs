﻿using AgC.UnitConversion;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Domain.ReadModels.Invoice;
using Landdb.Infrastructure;
using Landdb.ReportModels.Inventory;
using Landdb.Views.Secondary.Reports.Inventory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Landdb.Resources;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.Inventory.Generators
{
    public class InventorySummaryGenerator : IInventoryReportGenerator
    {

        IClientEndpoint endpoint;
        int cropYear;

        public InventorySummaryGenerator(IClientEndpoint endPoint, int cropyear)
        {
            this.endpoint = endPoint;
            this.cropYear = cropyear;

        }



        public Telerik.Reporting.ReportSource Generate(InventoryQueryCriteria crit)
        {
            ReportBook book = new ReportBook();
            var rs = new InstanceReportSource();

            //var invData = endpoint.GetView<InvoiceView>(crit.SelectedInvoice).Value;
            var masterlist = endpoint.GetMasterlistService();
            var inventoryItemList = endpoint.GetView<InventoryListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear));
            var invoiceListMaybe = endpoint.GetView<InvoiceListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear));
            var applicationData = endpoint.GetView<CropZoneApplicationDataView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new CropZoneApplicationDataView());
            InvoiceListView invoiceList = null;
            if (invoiceListMaybe.HasValue)
            {
                invoiceList = invoiceListMaybe.Value;
            }

            var data = new InventorySummaryData();

            data.CropYear = cropYear.ToString();
            data.DataSourceName = ApplicationEnvironment.CurrentDataSourceName;
            data.DateRangeDisplay = $"{crit.StartDate?.ToShortDateString()} - {crit.EndDate?.ToShortDateString()}".Equals(" - ") 
                ? $"1/1/{cropYear} - 12/31/{cropYear}"
                : $"{crit.StartDate?.ToShortDateString()} - {crit.EndDate?.ToShortDateString()}";
            //data.IsSubSet = includedItems.Count() < cnt ? true : false;

            List<CropZoneApplicationDataItem> appData = new List<CropZoneApplicationDataItem>();
            List<InvoiceView> includedInvoiceDetailList = new List<InvoiceView>();
            List<InvoiceProduct> invoiceProductDetails = new List<InvoiceProduct>();

            if (crit.StartDate != null && crit.EndDate != null)
            {
                var trueEndDate = crit.EndDate.HasValue ? crit.EndDate.Value.AddHours(24.0 - (double)crit.EndDate.Value.Hour) : new DateTime?();
                trueEndDate = trueEndDate.HasValue ? trueEndDate.Value.AddMinutes(-1) : new DateTime?();
                appData = (from x in applicationData.Items
                           where x.EndDate >= crit.StartDate &&
                           x.EndDate <= trueEndDate && crit.IncludedProducts.Contains(x.ProductId) == true
                           select x).ToList();

                includedInvoiceDetailList = invoiceList != null ? (from i in invoiceList.Invoices
                                                                   where i.InvoiceDate >= crit.StartDate && i.InvoiceDate <= trueEndDate
                                                                   select endpoint.GetView<InvoiceView>(i.Id).Value).ToList() : null;
            }
            else if (crit.StartDate == null && crit.EndDate != null)
            {
                var trueEndDate = crit.EndDate.HasValue ? crit.EndDate.Value.AddHours(24.0 - (double)crit.EndDate.Value.Hour) : new DateTime?();
                appData = (from x in applicationData.Items
                           where x.EndDate <= trueEndDate && crit.IncludedProducts.Contains(x.ProductId) == true
                           select x).ToList();

                includedInvoiceDetailList =invoiceList != null ?  (from i in invoiceList.Invoices
                                             where i.InvoiceDate <= crit.EndDate
                                             select endpoint.GetView<InvoiceView>(i.Id).Value).ToList() : null;
            }
            else if (crit.EndDate == null && crit.StartDate != null)
            {
                appData = (from x in applicationData.Items
                           where x.EndDate >= crit.StartDate && crit.IncludedProducts.Contains(x.ProductId) == true
                           select x).ToList();

                includedInvoiceDetailList =invoiceList != null ?  (from i in invoiceList.Invoices
                                             where i.InvoiceDate >= crit.StartDate
                                             select endpoint.GetView<InvoiceView>(i.Id).Value).ToList() : null;
            }
            else
            {
                appData = (from x in applicationData.Items
                           where crit.IncludedProducts.Contains(x.ProductId) == true
                           select x).ToList();

                includedInvoiceDetailList =invoiceList != null ?  (from i in invoiceList.Invoices
                                             select endpoint.GetView<InvoiceView>(i.Id).Value).ToList() : null;
            }

            List<ProductId> distinctProdList = new List<ProductId>();
            if (includedInvoiceDetailList != null)
            {
                invoiceProductDetails = (from i in includedInvoiceDetailList
                                         from p in i.Products
                                         where crit.IncludedProducts.Contains(p.ProductId)
                                         select p).ToList();

                distinctProdList = invoiceProductDetails.Distinct(x => x.ProductId).Select(p => p.ProductId).ToList();
            }
            var distinctAppliedProductIDs = appData.Distinct(x => x.ProductId).Select(p => p.ProductId);
            distinctProdList.AddRange(distinctAppliedProductIDs);

            if (distinctProdList.Any())
            {
                distinctProdList = distinctProdList.Distinct().ToList();

                foreach (var prodId in distinctProdList)
                {
                    var mlp = endpoint.GetMasterlistService().GetProduct(prodId);
                    var prod = new ProductLineItem();

                    prod.Manufacturer = mlp.Manufacturer;
                    prod.Product = mlp.Name;

                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    ////This adds up the invoiced products during the selected time frame
                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    var selectedInvoiceProdList = from j in invoiceProductDetails
                                                  where j.ProductId == prodId
                                                  select j;

                    IUnit invoicedUnit = null;
                    double invoicedTotal = 0.0;
                    decimal totalCost = 0m;
                    foreach (var invoiceProd in selectedInvoiceProdList)
                    {
                        var totalPurchasedVal = invoiceProd.TotalProductValue;
                        var totalPurchasedUnit = UnitFactory.GetPackageSafeUnit(invoiceProd.TotalProductUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                        var totalMeasure = totalPurchasedUnit.GetMeasure((double)totalPurchasedVal, mlp.Density);
                        totalCost += invoiceProd.TotalCost;
                        if (invoicedUnit == null)
                        {
                            invoicedUnit = totalMeasure.Unit;
                            invoicedTotal += totalMeasure.Value;
                        }
                        else
                        {
                            try
                            {
                                var convertedTotal = totalMeasure.CanConvertTo(invoicedUnit) ? totalMeasure.CreateAs(invoicedUnit) : totalMeasure;
                                invoicedTotal += convertedTotal.Value;
                            }
                            catch (Exception ex)
                            {
                                invoicedTotal += totalMeasure.Value;
                            }
                        }

                    }

                    prod.PurchasedValue = (decimal)invoicedTotal;
                    prod.PurchasedUnit = invoicedUnit;
                    prod.TotalCostValue = totalCost;

                    if (invoicedTotal != 0.0)
                    {
                        prod.AveragePriceValue = totalCost / (decimal)invoicedTotal;
                        prod.PurchasedCost = totalCost;
                    }
                    prod.AveragePriceUnit = invoicedUnit;
                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    //This adds up the products applied during the selected timeframe
                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    var distinctApplicationlist = (from a in appData
                                                   where a.ProductId == prodId
                                                   select a.ApplicationId).Distinct();

                    List<ApplicationView> appDetailsList = (from f in distinctApplicationlist
                                                            select endpoint.GetView<ApplicationView>(f).Value).ToList();
                    var selectedAppliedProdList = from a in appDetailsList
                                                  from p in a.Products
                                                  where p.Id == prodId
                                                  select p;
                    IUnit totalUnit = null;
                    decimal totalValue = 0m;

                    foreach (var applied in selectedAppliedProdList)
                    {
                        var totalAppliedValue = applied.TotalProductValue; // * applied.FromGrowerInventory;
                        var totalAppliedUnit = UnitFactory.GetPackageSafeUnit(applied.TotalProductUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                        var totalAppliedMeasure = totalAppliedUnit.GetMeasure((double)totalAppliedValue, mlp.Density);

                        //////TO DO :: IF THEY SELECT SPECIFIC OR INVOICE PRICE I WILL NEED TO TALLY UP THE TOTAL COST INSIDE THIS LOOP

                        if (totalUnit == null)
                        {
                            totalUnit = totalAppliedUnit;
                            totalValue += (decimal)totalAppliedValue;
                        }
                        else
                        {
                            try
                            {
                                var convertedTotal = totalAppliedMeasure.CanConvertTo(totalUnit) ? totalAppliedMeasure.CreateAs(totalUnit) : totalAppliedMeasure;
                                totalValue += (decimal)convertedTotal.Value;
                            }
                            catch (Exception ex)
                            {
                                totalValue += (decimal)totalAppliedMeasure.Value;
                            }
                        }
                    }

                    prod.AppliedValue = totalValue;
                    prod.AppliedUnit = totalUnit;

                    ////CONVERT APPLIED TO PURCHASED UNIT AND CALCULATE APPLIED COST
                    if (invoicedUnit != null && totalUnit != null)
                    {
                        try
                        {
                            var totalAppliedValue = totalValue;
                            var totalAppliedUnit = UnitFactory.GetPackageSafeUnit(totalUnit.Name, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                            var totalAppliedMeasure = totalAppliedUnit.GetMeasure((double)totalAppliedValue, mlp.Density);
                            var convertedMeasure = totalAppliedMeasure.GetValueAs(invoicedUnit);
                            prod.AppliedCost = (decimal)convertedMeasure * prod.AveragePriceValue;
                        }
                        catch (Exception ex)
                        {
                            prod.AppliedCost = 0;
                        }
                    }
                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    if (totalUnit != null)
                    {
                        var appliedMeasure = prod.AppliedUnit.GetMeasure((double)prod.AppliedValue, mlp.Density);

                        try
                        {
                            var converted = appliedMeasure.CanConvertTo(prod.PurchasedUnit) ? appliedMeasure.CreateAs(prod.PurchasedUnit) : appliedMeasure;
                            prod.RemainingValue = prod.PurchasedValue - (decimal)converted.Value;
                            prod.RemainingUnit = converted.Unit;
                            prod.InventoryCost = prod.RemainingValue * prod.AveragePriceValue;
                        }
                        catch (Exception ex)
                        {
                            prod.RemainingValue = prod.PurchasedValue - prod.AppliedValue;
                            prod.RemainingUnit = prod.PurchasedUnit;
                            prod.InventoryCost = prod.RemainingValue * prod.AveragePriceValue;
                        }
                    }
                    else
                    {
                        prod.RemainingValue = prod.PurchasedValue;
                        prod.RemainingUnit = prod.PurchasedUnit;
                        prod.InventoryCost = prod.RemainingValue * prod.AveragePriceValue;
                    }

                    data.InventoryItems.Add(prod);
                }

                data.InventoryItems = data.InventoryItems.OrderBy(x => x.Product).ToList();
                var report = new InventorySummaryReport();
                report.DataSource = data;
                report.Name = Strings.ReportName_Invoice_Text;

                book.Reports.Add(report);
            }

            rs.ReportDocument = book;
            return rs;
        }

        public string DisplayName
        {
            get { return Strings.GeneratorName_InventorySummaryReport_Text; }
        }
    }
}
