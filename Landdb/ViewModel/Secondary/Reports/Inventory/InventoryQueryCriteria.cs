﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Secondary.Reports.Inventory
{
    public class InventoryQueryCriteria
    {
        public InventoryQueryCriteria()
        {
            IncludedProducts = new List<ProductId>();
        }

        public List<ProductId> IncludedProducts { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
