﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Secondary.Reports.Inventory.Generators;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Reporting;
using Telerik.Reporting.Processing;

namespace Landdb.ViewModel.Secondary.Reports.Inventory
{
    public class SingleInventoryReportViewModel : ViewModelBase
    {
        IClientEndpoint endpoint;
        int cropyear;

        public SingleInventoryReportViewModel(IClientEndpoint endPoint, int cropYear)
        {
            this.endpoint = endPoint;
            this.cropyear = cropYear;

            UpdateReport();

            HideReport = new RelayCommand(this.CloseReport);
            PDFExport = new RelayCommand(this.PDF);
        }

        public RelayCommand HideReport { get; private set; }
        public RelayCommand PDFExport { get; private set; }
        public void CloseReport()
        {
            Messenger.Default.Send(new HideOverlayMessage() { });
        }

        public ReportSource ReportSource { get; set; }
        public IInventoryReportGenerator ReportGenerator { get; set; }

        public void UpdateReport()
        {
            var generator = new InventorySummaryGenerator(endpoint, cropyear);
            ReportGenerator = generator;
            if (ReportGenerator != null)
            {
                InventoryQueryCriteria criteria = BuildCriteria();
                ReportSource = ReportGenerator.Generate(criteria);
                RaisePropertyChanged("BingMapLayer");
                RaisePropertyChanged("ReportSource");
            }
        }

        InventoryQueryCriteria BuildCriteria()
        {
            InventoryQueryCriteria criteria = new InventoryQueryCriteria();

            //get all inventory products
            var products = endpoint.GetView<InventoryListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropyear));

            if (products.HasValue)
            {
                var idList = products.Value.Products.Where(p => p.Value.ApplicationCount > 0 || p.Value.InvoiceCount > 0).Select(d => d.Key).ToList();
                criteria.IncludedProducts = idList;
            }
            return criteria;
        }

        public void PDF()
        {
            try { 
            if (ReportSource == null) return;
            ReportSource reportExport = ReportSource;

            ReportProcessor reportProcessor = new ReportProcessor();
            System.Collections.Hashtable deviceInfo =
            new System.Collections.Hashtable();
            RenderingResult result = reportProcessor.RenderReport("PDF", reportExport, deviceInfo);

            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "Adobe Reader|*.pdf";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;

            Nullable<bool> resultSaved = saveFileDialog1.ShowDialog();

            // Process save file dialog box results 
            if (resultSaved == true)
            {
                // Save document 
                string filename = saveFileDialog1.FileName;

                try
                {
                    using (System.IO.FileStream fs = new System.IO.FileStream(filename, System.IO.FileMode.Create))
                    {
                        fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
                    }
                }
                catch (Exception ex) { }
            }
            }
            catch (Exception ex)
            {
                //do nothing
            }
        }
    }
}
