﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Secondary.Reports.Plan.Generator;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Reporting;
using Telerik.Reporting.Processing;

namespace Landdb.ViewModel.Secondary.Reports.Plan
{
    public class SinglePlanViewModel : ViewModelBase
    {
        IClientEndpoint endpoint;
        CropPlanId planId;
        IPlanReportGenerator selectedReportGenerator;

        public SinglePlanViewModel(IClientEndpoint endpoint, CropPlanId planId)
        {
            this.endpoint = endpoint;
            this.planId = planId;

            ReportGenerators = new List<IPlanReportGenerator>();
            AddPlanReportGenerators(endpoint, ApplicationEnvironment.CurrentCropYear);

            HideReport = new RelayCommand(this.CloseReport);
            PDFExport = new RelayCommand(this.PDF);
        }

        public RelayCommand HideReport { get; private set; }
        public RelayCommand PDFExport { get; set; }

        public void CloseReport()
        {
            Messenger.Default.Send(new HideOverlayMessage() { });
        }

        public ReportSource ReportSource { get; set; }
        public IPlanReportGenerator SelectedReportGenerator
        {
            get { return selectedReportGenerator; }
            set
            {
                selectedReportGenerator = value;
                UpdateReport();
                //SaveTemplate();
                RaisePropertyChanged("SelectedReportGenerator");
            }

        }
        public List<IPlanReportGenerator> ReportGenerators { get; set; }

        void AddPlanReportGenerators(IClientEndpoint clientEndpoint, int cropYear)
        {
            ReportGenerators.Add(new PlanReportGenerator(endpoint, ApplicationEnvironment.CurrentCropYear));
            ReportGenerators.Add(new PlanExcelGenerator(endpoint, ApplicationEnvironment.CurrentCropYear));
            ReportGenerators.Add(new PlanProductsDetail(endpoint, ApplicationEnvironment.CurrentCropYear));
            //CREATED THIS AS A PROOF OF CONCEPT
            //ReportGenerators.Add(new ClosedXMLPlanExcelGenerator(endpoint, ApplicationEnvironment.CurrentCropYear));
        }

        public void UpdateReport()
        {
            if (SelectedReportGenerator != null)
            {
                PlanQueryCriteria criteria = BuildCriteria();
                ReportSource = SelectedReportGenerator.Generate(criteria);
                RaisePropertyChanged("BingMapLayer");
                RaisePropertyChanged("ReportSource");
            }
        }

        PlanQueryCriteria BuildCriteria()
        {
            PlanQueryCriteria criteria = new PlanQueryCriteria();

            if (planId != null)
            {
                criteria.SelectedPlan = planId;
            }

            return criteria;
        }

        public void PDF()
        {
            try { 
            if (ReportSource == null) return;
            ReportSource reportExport = ReportSource;

            ReportProcessor reportProcessor = new ReportProcessor();
            System.Collections.Hashtable deviceInfo =
            new System.Collections.Hashtable();
            RenderingResult result = reportProcessor.RenderReport("PDF", reportExport, deviceInfo);

            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "Adobe Reader|*.pdf";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;

            Nullable<bool> resultSaved = saveFileDialog1.ShowDialog();

            // Process save file dialog box results 
            if (resultSaved == true)
            {
                // Save document 
                string filename = saveFileDialog1.FileName;

                try
                {
                    using (System.IO.FileStream fs = new System.IO.FileStream(filename, System.IO.FileMode.Create))
                    {
                        fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
                    }

                    System.Diagnostics.Process.Start(filename);
                }
                catch (Exception ex) { }

            }
            }
            catch (Exception ex)
            {
                //do nothing
            }
        }
    }
}
