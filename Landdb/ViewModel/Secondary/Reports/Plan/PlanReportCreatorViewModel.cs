﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Converters.Reports;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Reports;
using Landdb.ViewModel.Secondary.Reports.Document.Generators;
using Landdb.ViewModel.Secondary.Reports.Plan.Generator;
using Microsoft.Win32;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using Telerik.Reporting;
using Telerik.Reporting.Processing;

namespace Landdb.ViewModel.Secondary.Reports.Plan
{
    public class PlanReportCreatorViewModel : ViewModelBase
    {
        ReportTemplateViewModel template;
        ReportTemplateViewModelToFile templateService = new ReportTemplateViewModelToFile();
        PlanQueryCriteria criteria;
        IPlanReportGenerator selectedReportGenerator;
        ReportSource source;
        string reportType;

        public PlanReportCreatorViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, int cropYear, ReportTemplateViewModel template, string reportType)
        {
            CancelCommand = new RelayCommand(OnCancelReport);
            SaveCommand = new RelayCommand(SaveTemplate);
            PDFExport = new RelayCommand(PDF);
            ReportGenerators = new ObservableCollection<IPlanReportGenerator>();

            InfoViewModel = new PlanReportInfoViewModel(clientEndpoint, cropYear);
            AddDocumentReportGenerators(clientEndpoint, cropYear);

            this.reportType = reportType;

            SelectedPageIndex = 0;
        }

        public ICommand CancelCommand { get; private set; }
        public RelayCommand PDFExport { get; set; }
        public ICommand SaveCommand { get; set; }
        public string TemplateName { get; set; }
        public int SelectedPageIndex { get; set; }
        public int CropYear { get { return ApplicationEnvironment.CurrentCropYear; } }

        public PlanReportInfoViewModel InfoViewModel { get; set; }

        public ReportSource ReportSource
        {
            get
            {
                return source;
            }
            set
            {
                source = value;
                RaisePropertyChanged("ReportSource");
            }
        }

        public ObservableCollection<IPlanReportGenerator> ReportGenerators { get; private set; }
        public IPlanReportGenerator SelectedReportGenerator
        {
            get { return selectedReportGenerator; }
            set
            {
                selectedReportGenerator = value;
                UpdateReport();
                RaisePropertyChanged("SelectedReportGenerator");
            }
        }

        void AddDocumentReportGenerators(IClientEndpoint clientEndpoint, int cropYear)
        {
            //ReportGenerators.Add(new MultiPrintDocumentGenerator(clientEndpoint, cropYear));
            //ReportGenerators.Add(new ByFieldDocumentGenerator(clientEndpoint, cropYear));
            //ReportGenerators.Add(new BreakOutDocumentGenerator(clientEndpoint, cropYear));
            ReportGenerators.Add(new PlanProductSummaryGenerator(clientEndpoint, cropYear));
            ReportGenerators.Add(new PlanSummaryRentContractReport(clientEndpoint, cropYear));
            ReportGenerators.Add(new PlanDetailGenerator(clientEndpoint, cropYear));
        }

        public void UpdateReport()
        {
            if (SelectedReportGenerator != null)
            {
                criteria = BuildCriteria();

                ReportSource = SelectedReportGenerator.Generate(criteria);
                RaisePropertyChanged("ReportSource");
            }
        }

        void OnCancelReport()
        {
            Messenger.Default.Send<ShowMainViewMessage>(new ShowMainViewMessage());
        }

        PlanQueryCriteria BuildCriteria()
        {
            PlanQueryCriteria criteria = new PlanQueryCriteria();

            criteria.SelectedPlans.AddRange(InfoViewModel.PlanList.Where(x => x.IsChecked).Select(y => y.Id)); 

            return criteria;
        }

        public void PDF()
        {
            try { 
            if (ReportSource == null) return;
            ReportSource reportExport = ReportSource;

            ReportProcessor reportProcessor = new ReportProcessor();
            System.Collections.Hashtable deviceInfo =
            new System.Collections.Hashtable();
            RenderingResult result = reportProcessor.RenderReport("PDF", reportExport, deviceInfo);

            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "Adobe Reader|*.pdf";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;

            Nullable<bool> resultSaved = saveFileDialog1.ShowDialog();

            // Process save file dialog box results 
            if (resultSaved == true)
            {
                // Save document 
                string filename = saveFileDialog1.FileName;

                try
                {
                    using (System.IO.FileStream fs = new System.IO.FileStream(filename, System.IO.FileMode.Create))
                    {
                        fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
                    }

                    System.Diagnostics.Process.Start(filename);
                }
                catch (Exception ex) { }

            }
            }
            catch (Exception ex)
            {
                //do nothing
            }
        }


        void SaveTemplate()
        {
            criteria = BuildCriteria();
            if (template != null && template.TemplateName == TemplateName)
            {
                ReportTemplateViewModel editedTemplate = new ReportTemplateViewModel()
                {
                    TemplateName = TemplateName,
                    Created = template.Created,
                    LastUsed = DateTime.Now,
                    CriteriaJSON = JsonConvert.SerializeObject(criteria),
                    ReportType = template.ReportType,
                    DataSourceId = template.DataSourceId
                };

                //TODO :: CALL TO EDIT PREVIOUS TEMPLATE...
                templateService.EditTemplate(editedTemplate);
            }
            else
            {
                ReportTemplateViewModel newTemplate = new ReportTemplateViewModel()
                {
                    TemplateName = TemplateName,
                    Created = DateTime.Now,
                    LastUsed = DateTime.Now,
                    CriteriaJSON = JsonConvert.SerializeObject(criteria),
                    ReportType = reportType,
                    DataSourceId = ApplicationEnvironment.CurrentDataSourceId
                };

                //TODO :: call service to save the new report template
                templateService.SaveNewTemplate(newTemplate);
            }

            RaisePropertyChanged("NewTemplateCreated", null, new ReportTemplateViewModel(), true);
        }
    }
}
