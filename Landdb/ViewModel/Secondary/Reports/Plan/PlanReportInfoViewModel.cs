﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Plan;
using Landdb.Infrastructure;
using Landdb.ViewModel.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Secondary.Reports.Plan
{
    public class PlanReportInfoViewModel : ViewModelBase
    {
        IClientEndpoint endpoint;
        int cropYear;

        public PlanReportInfoViewModel(IClientEndpoint endpoint, int cropyear)
        {
            this.endpoint = endpoint;
            this.cropYear = cropyear;

            SelectAll = new RelayCommand(Select);
            UnselectAll = new RelayCommand(UnSelect);

            Initialize();
        }

        public RelayCommand SelectAll { get; set; }
        public RelayCommand UnselectAll { get; set; }

        public List<PlanItemViewModel> PlanList { get; set; }

        private PlanItemViewModel _selectedPlan { get; set; }
        public PlanItemViewModel SelectedPlan
        {
            get { return _selectedPlan; }
            set
            {
                _selectedPlan = value;

                var details = CreateDetailsModel(_selectedPlan.Id);
                DetailsView = new PlanPreviewViewModel(endpoint, details);
                RaisePropertyChanged(() => DetailsView);
            }
        }

        public PlanPreviewViewModel DetailsView {get; set;}

        PlanView CreateDetailsModel(CropPlanId id)
        {
            var details = endpoint.GetView<PlanView>(id).Value;
            return details;
        }

        void Initialize()
        {
            var plansMaybe = endpoint.GetView<PlanList>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear));

            if (plansMaybe.HasValue)
            {
                PlanList = (from p in plansMaybe.Value.Plans
                           select new PlanItemViewModel(p)).ToList();
                RaisePropertyChanged(() => PlanList);
            }
        }

        void Select()
        {
            foreach (var plan in PlanList)
            {
                plan.IsChecked = true;
            }
        }

        void UnSelect()
        {
            foreach (var plan in PlanList)
            {
                plan.IsChecked = false;
            }
        }
    }
}
