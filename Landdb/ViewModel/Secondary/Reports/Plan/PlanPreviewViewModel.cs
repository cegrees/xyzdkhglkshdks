﻿using AgC.UnitConversion;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Plan;
using Landdb.Domain.ReadModels.Shared;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.ViewModel.Production;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Secondary.Reports.Plan
{
    public class PlanPreviewViewModel
    {
        PlanView planDetails;
        IClientEndpoint endpoint;

        public PlanPreviewViewModel(IClientEndpoint endpoint, PlanView view)
        {
            planDetails = view;
            this.endpoint = endpoint;

            Initialize();
        }

        public ObservableCollection<ProductDetails> PlannedProducts { get; set; }
        public ObservableCollection<PlannedFieldItem> PlannedFields { get; set; }
        public string Name { get; set; }
        public decimal Revenue { get; set; }

        void Initialize()
        {
            var flattenedTree = endpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear)).Value;
            PlannedProducts = new ObservableCollection<ProductDetails>();

            var plannedFields = from f in planDetails.CropZones
                                let t = flattenedTree.Items.FirstOrDefault(x => x.CropZoneId == f.Id)
                                select new PlannedFieldItem() { CropZone = t.CropZoneName, Farm = t.FarmName, Field = t.FieldName, Area = (decimal)f.Area.Value };
            PlannedFields = new ObservableCollection<PlannedFieldItem>(plannedFields);
            Name = planDetails.Name;
            var income = planDetails.PricePerUnit * planDetails.YieldPerArea * planDetails.EstimatedArea.Value + (planDetails.FSAPayment * planDetails.EstimatedArea.Value);
            var cost = (double)planDetails.Products.Sum(x => x.TotalCost) + ((planDetails.Equipment + planDetails.Insurance + planDetails.Labor + planDetails.LandRent + planDetails.Repairs + planDetails.ReturnToMgt + planDetails.Taxes) * planDetails.EstimatedArea.Value);
            Revenue = (decimal)(income - cost);

            var totalArea = (decimal)planDetails.EstimatedArea.Value;
            var timingEvents = endpoint.GetMasterlistService().GetTimingEventList().ToList();

            foreach (var p in planDetails.Products)
            {
                if (p == null || string.IsNullOrWhiteSpace(p.RateUnit) || string.IsNullOrWhiteSpace(p.AreaUnit) || string.IsNullOrWhiteSpace(p.RateUnit) || string.IsNullOrWhiteSpace(p.CostUnit))
                {
                    continue; // Guard statement for weeding out badly formatted products
                }

                //HACK :: SOME USER CREATED PRODUCTS ARE GETTING THE INCORRECT PRODUCTID
                var masterlistProdQuery = endpoint.GetMasterlistService().GetProduct(p.Id);
                var masterlistProd = masterlistProdQuery == null ? endpoint.GetMasterlistService().GetProduct(new ProductId(ApplicationEnvironment.CurrentDataSourceId, p.Id.Id)) : masterlistProdQuery;

                // calculate the total from the rate
                var totalProduct = p.RateValue * totalArea * p.PercentApplied * p.ApplicationCount;
                var totalProductMeasure = UnitFactory.GetPackageSafeUnit(p.RateUnit, masterlistProd.StdUnit, masterlistProd.StdFactor, masterlistProd.StdPackageUnit).GetMeasure((double)totalProduct, masterlistProd.Density);
                var totalProductUnit = UnitFactory.GetPackageSafeUnit(p.TotalProductUnit, masterlistProd.StdUnit, masterlistProd.StdFactor, masterlistProd.StdPackageUnit);
                var convertedTotalProduct = totalProductMeasure.GetValueAs(totalProductUnit);
                var displayTotal = DisplayMeasureConverter.ConvertToTotalDisplayMeasure(p.Id, (decimal)convertedTotalProduct, p.TotalProductUnit, masterlistProd.Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);

                var displayRate = DisplayMeasureConverter.ConvertToRateDisplayMeasure(p.Id, p.RateValue, p.RateUnit, masterlistProd.Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);
                var unit = string.IsNullOrEmpty(p.SpecificCostUnit) ? null : UnitFactory.GetPackageSafeUnit(p.SpecificCostUnit, masterlistProd.StdUnit, masterlistProd.StdFactor, masterlistProd.StdPackageUnit);
                decimal totalCost = 0m;

                try
                {
                    if (displayTotal.CanConvertTo(unit))
                    {
                        var convertedTotal = unit != null && displayTotal.CanConvertTo(unit) ? displayTotal.GetValueAs(unit) : displayTotal.Value;
                        totalCost = p.SpecificCostPerUnit.HasValue ? (decimal)convertedTotal * p.SpecificCostPerUnit.Value : p.ProductTotalCost;
                    }
                    else
                    {
                        totalCost = 0m;
                    }
                }
                catch (Exception ex)
                {
                    totalCost = 0m;
                }

                var timing = !string.IsNullOrEmpty(p.TimingEvent) ? timingEvents.SingleOrDefault(x => x.Name == p.TimingEvent) : null;

                PlannedProducts.Add(new ProductDetails()
                {
                    ProductName = masterlistProd.Name,
                    RateMeasure = displayRate,
                    AreaUnit = UnitFactory.GetUnitByName(p.AreaUnit),
                    TotalProduct = displayTotal,
                    TotalCost = totalCost,
                    CostPerUnitValue = p.SpecificCostPerUnit.HasValue ? p.SpecificCostPerUnit.Value : 0m,
                    CostUnit = !string.IsNullOrEmpty(p.SpecificCostUnit) ? UnitFactory.GetUnitByName(p.SpecificCostUnit) : UnitFactory.GetUnitByName(p.CostUnit),
                    ID = p.Id,
                    ProductAreaPercent = p.PercentApplied,
                    TrackingId = p.TrackingId,
                    TimingEvent = p.TimingEvent,
                    TargetDate = p.TargetDate,
                    TimingEventTag = p.TimingEventTag,
                    TimingEventOrder = timing != null ? timing.Order : 0,
                    ApplicationCount = p.ApplicationCount.HasValue ? p.ApplicationCount.Value : 1,
                });
            }
        }
    }

    public class PlannedFieldItem
    {
        public string Farm { get; set; }
        public string Field { get; set; }
        public string CropZone { get; set; }
        public decimal Area { get; set; }
    }
}
