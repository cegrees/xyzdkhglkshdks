﻿using AgC.UnitConversion;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Domain.ReadModels.Plan;
using Landdb.Infrastructure;
using Landdb.ReportModels.Plan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Landdb.Resources;
using Telerik.Reporting;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Client.Services.Fertilizer;

namespace Landdb.ViewModel.Secondary.Reports.Plan.Generator
{
    class PlanDetailGenerator : IPlanReportGenerator
    {
        IClientEndpoint endpoint;
        int cropyear;

        public PlanDetailGenerator(IClientEndpoint endpoint, int cropyear)
        {
            this.endpoint = endpoint;
            this.cropyear = cropyear;
        }

        public string DisplayName
        {
            get
            {
                return Strings.GeneratorName_PlanDetails_Text;
            }
        }

        public ReportSource Generate(PlanQueryCriteria crit)
        {
            ReportBook book = new ReportBook();
            var rs = new InstanceReportSource();

            if (crit.SelectedPlans.Any())
            {
                //UpdateProgressMessage(progress, "Creating Product Summary Report...");
                //UpdateProgressTotalRecords(progress, criteria.IncludedDocuments.Count());
                //UpdateProgressCurrentRecord(progress, 0);
                int cnt = 0;

                var planProducts = new List<ReportModels.Plan.ProductSummaryData>();
                var woProducts = new List<ReportModels.Plan.ProductSummaryData>();
                var appProducts = new List<ReportModels.Plan.ProductSummaryData>();
                var inventory = endpoint.GetView<InventoryListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear)).GetValue(new InventoryListView());
                var masterlistService = endpoint.GetMasterlistService();
                foreach (var doc in crit.SelectedPlans)
                {

                    var planId = new CropPlanId(doc.DataSourceId, doc.Id);
                    var planDetails = endpoint.GetView<PlanView>(planId).Value;
                    var planData = endpoint.GetView<PlanView>(planId).Value;
                    var flattenedHierarchy = endpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, planData.CropYear)).GetValue(new FlattenedTreeHierarchyView());
                    var masterlist = endpoint.GetMasterlistService();

                    if (planData != null)
                    {
                        var data = new PlanData();
                        data.DataSourceName = ApplicationEnvironment.CurrentDataSourceName;
                        data.CropDisplay = masterlist.GetCropDisplay(new CropId(planData.CropId));
                        data.PlanName = planData.Name;
                        data.CropShare = planData.CropSharePercent;
                        data.CropYear = planData.CropYear;

                        data.EstimatedArea = planData.EstimatedArea.Value;
                        data.Equipment = planData.Equipment * data.EstimatedArea;
                        data.FSAPayment = planData.FSAPayment;
                        data.Insurance = planData.Insurance * data.EstimatedArea;
                        data.Labor = planData.Labor * data.EstimatedArea;
                        data.PricePerUnit = planData.PricePerUnit;
                        data.Rent = planData.LandRent * data.EstimatedArea;
                        data.Repairs = planData.Repairs * data.EstimatedArea;
                        data.ReturnToMgt = planData.ReturnToMgt * data.EstimatedArea;
                        data.Taxes = planData.Taxes * data.EstimatedArea;
                        data.YieldPerArea = planData.YieldPerArea;
                        data.Notes = planData.Notes;

                        var service = new FertilizerAccumulator(endpoint, ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear);

                        FertilizerFormulationModel fertModel = new FertilizerFormulationModel();

                        Task fertTask = Task.Factory.StartNew(async () =>
                        {
                            fertModel = await service.CalculateTotalPlanFertilizer(planData.Products, planData.EstimatedArea);
                        });

                        var timingEventList = masterlist.GetTimingEventList();
                        foreach (var prod in planData.Products)
                        {
                            var planProd = new ReportModels.Plan.PlanData.PlanProducts();
                            var masterListProd = masterlist.GetProduct(prod.Id);
                            if (masterListProd == null) { masterListProd = masterlist.GetProduct(new ProductId(ApplicationEnvironment.CurrentDataSourceId, prod.Id.Id)); }
                            planProd.ApplicationCount = (int)prod.ApplicationCount;
                            planProd.PercentApplied = (double)prod.PercentApplied;
                            planProd.ProductName = masterListProd.Name;
                            planProd.Rate = (double)prod.RateValue;
                            planProd.RateUnit = prod.RateUnit;
                            planProd.TimingEvent = prod.TimingEvent;
                            planProd.TimingEventOrder = string.IsNullOrEmpty(prod.TimingEvent) ? 0 : timingEventList.SingleOrDefault(x => x.Name == prod.TimingEvent).Order + 1;
                            planProd.TimingEventTag = prod.TimingEventTag;

                            // calculate the total from the rate
                            var totalProduct = prod.RateValue * (decimal)planData.EstimatedArea.Value * prod.PercentApplied * prod.ApplicationCount;
                            var totalProductMeasure = UnitFactory.GetPackageSafeUnit(prod.RateUnit, masterListProd.StdUnit, masterListProd.StdFactor, masterListProd.StdPackageUnit).GetMeasure((double)totalProduct, masterListProd.Density);
                            var totalProductUnit = UnitFactory.GetPackageSafeUnit(prod.TotalProductUnit, masterListProd.StdUnit, masterListProd.StdFactor, masterListProd.StdPackageUnit);
                            var convertedTotalProduct = totalProductMeasure.GetValueAs(totalProductUnit);
                            var displayTotal = DisplayMeasureConverter.ConvertToTotalDisplayMeasure(prod.Id, (decimal)convertedTotalProduct, prod.TotalProductUnit, masterListProd.Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);

                            planProd.TotalProduct = displayTotal.Value;
                            planProd.TotalProductUnit = displayTotal.Unit.Name;

                            var unit = string.IsNullOrEmpty(prod.SpecificCostUnit) ? null : UnitFactory.GetPackageSafeUnit(prod.SpecificCostUnit, masterListProd.StdUnit, masterListProd.StdFactor, masterListProd.StdPackageUnit);
                            var convertedTotal = unit != null && displayTotal.CanConvertTo(unit) ? displayTotal.GetValueAs(unit) : displayTotal.Value;
                            var totalCost = prod.SpecificCostPerUnit.HasValue ? (decimal)convertedTotal * prod.SpecificCostPerUnit.Value : prod.ProductTotalCost;

                            planProd.TotalCost = (double)totalCost;
                            planProd.Area = (double)prod.PercentApplied * planData.EstimatedArea.Value * (double)prod.ApplicationCount;
                            planProd.CostPerArea = (double)totalCost / data.EstimatedArea;
                            planProd.TargetDate = prod.TargetDate;
                            data.InputCost += (double)planProd.TotalCost;

                            data.Products.Add(planProd);
                        }

                        data.Products = data.Products.OrderBy(x => x.TimingEventOrder).ThenBy(y => y.TimingEventTag).ThenBy(b => b.TargetDate).ToList();

                        foreach (var cz in planData.CropZones)
                        {
                            var czTreeData = (from f in flattenedHierarchy.Items
                                              where f.CropZoneId == cz.Id
                                              select f).FirstOrDefault();

                            var includedCZ = new ReportModels.Plan.PlanData.IncludedPlanCropZones();
                            includedCZ.Area = cz.Area.Value;

                            if (czTreeData != null)
                            {
                                includedCZ.CropZone = string.IsNullOrEmpty(czTreeData.CropZoneName) ? string.Empty : czTreeData.CropZoneName;
                                includedCZ.Field = czTreeData.FieldName;
                                includedCZ.Farm = czTreeData.FarmName;
                            }
                            else
                            {
                                try
                                {
                                    var czDetails = endpoint.GetView<CropZoneDetailsView>(cz.Id).Value;
                                    var fieldDetails = endpoint.GetView<FieldDetailsView>(czDetails.FieldId).Value;

                                    includedCZ.CropZone = czDetails != null ? czDetails.Name : Strings.UnknownCropZone_Text;
                                    includedCZ.Field = fieldDetails != null ? fieldDetails.FieldNameByCropYear[planData.CropYear] : Strings.UnknownField_Text;
                                    includedCZ.Farm = Strings.UnknownFarm_Text;
                                }
                                catch (Exception ex)
                                {

                                }
                            }

                            data.CropZones.Add(includedCZ);
                        }
                        data.ExpensePerArea = planData.Equipment + planData.Insurance + planData.Labor + planData.LandRent + planData.Repairs + planData.ReturnToMgt + planData.Taxes + (data.InputCost / data.EstimatedArea);

                        var expense = data.Equipment + data.Insurance + data.Labor + data.Rent + data.Repairs + data.ReturnToMgt + data.Taxes;
                        data.TotalFixedCost = data.Equipment + data.Insurance + data.Labor + data.Repairs + data.ReturnToMgt + data.Taxes;
                        data.TotalExpense = expense + data.InputCost;
                        data.VariableCostPerArea = (data.InputCost + data.Rent) / data.EstimatedArea;


                        data.TotalRevenue = planData.YieldPerArea * planData.EstimatedArea.Value * planData.PricePerUnit * planData.CropSharePercent + (planData.FSAPayment * planData.EstimatedArea.Value);
                        data.NetRevenue = data.TotalRevenue - data.TotalExpense;
                        data.RevenuePerArea = data.NetRevenue / planData.EstimatedArea.Value;

                        fertTask.Wait();
                        data.TotalFertilizer = fertModel;

                        var report = new Views.Secondary.Reports.Plan.Plan();
                        report.DataSource = data;
                        report.Name = Strings.ReportName_Plan_Text;

                        book.Reports.Add(report);
                    }

                }

                ///plan
                var planReport = new Landdb.Views.Secondary.Reports.Plan.ProductSummary();
                planReport.DataSource = planProducts.OrderBy(x => x.ProductName);
                planReport.Name = "";
                book.Reports.Add(planReport);

            }

            rs.ReportDocument = book;
            return rs;
        }
    }
}
