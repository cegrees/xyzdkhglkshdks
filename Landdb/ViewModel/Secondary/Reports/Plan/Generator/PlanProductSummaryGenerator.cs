﻿using AgC.UnitConversion;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Domain.ReadModels.Plan;
using Landdb.Infrastructure;
using Landdb.ReportModels.Plan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Landdb.Resources;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.Plan.Generator
{
    class PlanProductSummaryGenerator : IPlanReportGenerator
    {
        IClientEndpoint endpoint;
        int cropyear;

        public PlanProductSummaryGenerator(IClientEndpoint endpoint, int cropyear)
        {
            this.endpoint = endpoint;
            this.cropyear = cropyear;
        }

        public string DisplayName
        {
            get
            {
                return Strings.GeneratorName_ProductSummary_Text;
            }
        }

        public ReportSource Generate(PlanQueryCriteria crit)
        {
            ReportBook book = new ReportBook();
            var rs = new InstanceReportSource();

            if (crit.SelectedPlans.Any())
            {
                //UpdateProgressMessage(progress, "Creating Product Summary Report...");
                //UpdateProgressTotalRecords(progress, criteria.IncludedDocuments.Count());
                //UpdateProgressCurrentRecord(progress, 0);
                int cnt = 0;

                var planProducts = new List<ReportModels.Plan.ProductSummaryData>();
                var woProducts = new List<ReportModels.Plan.ProductSummaryData>();
                var appProducts = new List<ReportModels.Plan.ProductSummaryData>();
                var inventory = endpoint.GetView<InventoryListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear)).GetValue(new InventoryListView());
                var masterlistService = endpoint.GetMasterlistService();
                foreach (var doc in crit.SelectedPlans)
                {

                    var planId = new CropPlanId(doc.DataSourceId, doc.Id);
                    var planDetails = endpoint.GetView<PlanView>(planId).Value;

                    foreach (var prod in planDetails.Products)
                    {
                        var planProd = new ProductSummaryData();
                        planProd.ReportName = Strings.ReportName_PlanProductSummary_Text;
                        planProd.ProductId = prod.Id;
                        planProd.ProductName = masterlistService.GetProductDisplay(prod.Id);
                        planProd.DocumentName = planDetails.Name;
                        planProd.TimingEvent = prod.TimingEvent + prod.TimingEventTag;
                        planProd.TotalArea = (decimal)planDetails.EstimatedArea.Value;
                        planProd.DataSourceName = ApplicationEnvironment.CurrentDataSourceName;
                        planProd.TargetDateDisplay = prod.TargetDate?.ToShortDateString() ?? string.Empty;
                        planProd.CropYear = cropyear.ToString();

                        if (inventory.Products.ContainsKey(prod.Id))
                        {
                            try
                            {
                                var masterListProd = masterlistService.GetProduct(prod.Id);
                                // calculate the total from the rate
                                var totalProduct = prod.RateValue * (decimal)planDetails.EstimatedArea.Value * prod.PercentApplied * prod.ApplicationCount;
                                var totalProductMeasure = UnitFactory.GetPackageSafeUnit(prod.RateUnit, masterListProd.StdUnit, masterListProd.StdFactor, masterListProd.StdPackageUnit).GetMeasure((double)totalProduct, masterListProd.Density);
                                var totalProductUnit = UnitFactory.GetPackageSafeUnit(prod.TotalProductUnit, masterListProd.StdUnit, masterListProd.StdFactor, masterListProd.StdPackageUnit);
                                var convertedTotalProduct = totalProductMeasure.GetValueAs(totalProductUnit);
                                var totalMeasure = totalProductUnit.GetMeasure((double)convertedTotalProduct, masterListProd.Density);

                                planProd.TotalProductValue = (decimal)convertedTotalProduct;
                                planProd.TotalProductUnit = totalMeasure.Unit;

                                var unit = string.IsNullOrEmpty(prod.SpecificCostUnit) ? null : UnitFactory.GetPackageSafeUnit(prod.SpecificCostUnit, masterListProd.StdUnit, masterListProd.StdFactor, masterListProd.StdPackageUnit);
                                var specificPriceUnit = string.IsNullOrEmpty(prod.SpecificCostUnit) ? null : unit;
                                var convertedTotalMeasureValue = totalMeasure.CanConvertTo(specificPriceUnit) ? totalMeasure.GetValueAs(specificPriceUnit) : totalMeasure.Value;
                                planProd.TotalCost = prod.SpecificCostPerUnit.HasValue ? (decimal)(convertedTotalMeasureValue * (double)prod.SpecificCostPerUnit) : 0m;
                                planProd.CostPerArea = planProd.TotalCost / (decimal)planDetails.EstimatedArea.Value;
                            }
                            catch (Exception ex)
                            {
                                planProd.TotalCost = 0m;
                                planProd.CostPerArea = 0m;
                            }
                        }
                        else
                        {
                            planProd.TotalCost = 0m;
                            planProd.CostPerArea = 0m;
                        }

                        planProducts.Add(planProd);
                    }

                }

                ///plan
                var planReport = new Landdb.Views.Secondary.Reports.Plan.ProductSummary();
                planReport.DataSource = planProducts.OrderBy(x => x.ProductName);
                planReport.Name = "";
                book.Reports.Add(planReport);

            }

            rs.ReportDocument = book;
            return rs;
        }
    }
}
