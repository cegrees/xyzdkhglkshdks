﻿using AgC.UnitConversion;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Plan;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.ReportModels.Plan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Landdb.Resources;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.Plan.Generator
{
    public class PlanProductsDetail : IPlanReportGenerator
    {
        IClientEndpoint endpoint;
        int cropYear;

        public PlanProductsDetail(IClientEndpoint endpoint, int cropYear)
        {
            this.endpoint = endpoint;
            this.cropYear = cropYear;
        }

        public string DisplayName
        {
            get
            {
                return Strings.DetailName_PlannedProductSchedule_Text;
            }
        }

        public ReportSource Generate(PlanQueryCriteria crit)
        {
            ReportBook book = new ReportBook();
            var rs = new InstanceReportSource();

            var planData = endpoint.GetView<PlanView>(crit.SelectedPlan).Value;
            var flattenedHierarchy = endpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new FlattenedTreeHierarchyView());
            var masterlist = endpoint.GetMasterlistService();

            if (planData != null)
            {
                var data = new PlanData();
                

                var timingEventList = masterlist.GetTimingEventList();
                foreach (var prod in planData.Products)
                {
                    var planProd = new ReportModels.Plan.PlanData.PlanProducts();

                    planProd.DataSourceName = ApplicationEnvironment.CurrentDataSourceName;
                    planProd.CropDisplay = masterlist.GetCropDisplay(new CropId(planData.CropId));
                    planProd.PlanName = planData.Name;
                    var masterListProd = masterlist.GetProduct(prod.Id);
                    if (masterListProd == null) { masterListProd = masterlist.GetProduct(new ProductId(ApplicationEnvironment.CurrentDataSourceId, prod.Id.Id)); }
                    planProd.Manufacturer = masterListProd.Manufacturer;
                    planProd.ApplicationCount = (int)prod.ApplicationCount;
                    planProd.PercentApplied = (double)prod.PercentApplied;
                    planProd.ProductName = masterListProd.Name;
                    planProd.Rate = (double)prod.RateValue;
                    planProd.RateUnit = prod.RateUnit;
                    planProd.TimingEvent = prod.TimingEvent;
                    planProd.TimingEventOrder = string.IsNullOrEmpty(prod.TimingEvent) ? 0 : timingEventList.SingleOrDefault(x => x.Name == prod.TimingEvent).Order + 1;
                    planProd.TimingEventTag = prod.TimingEventTag;
                    planProd.CropYear = cropYear;

                    // calculate the total from the rate
                    var totalProduct = prod.RateValue * (decimal)planData.EstimatedArea.Value * prod.PercentApplied * prod.ApplicationCount;
                    var totalProductMeasure = UnitFactory.GetPackageSafeUnit(prod.RateUnit, masterListProd.StdUnit, masterListProd.StdFactor, masterListProd.StdPackageUnit).GetMeasure((double)totalProduct, masterListProd.Density);
                    var totalProductUnit = UnitFactory.GetPackageSafeUnit(prod.TotalProductUnit, masterListProd.StdUnit, masterListProd.StdFactor, masterListProd.StdPackageUnit);
                    var convertedTotalProduct = totalProductMeasure.GetValueAs(totalProductUnit);
                    var displayTotal = DisplayMeasureConverter.ConvertToTotalDisplayMeasure(prod.Id, (decimal)convertedTotalProduct, prod.TotalProductUnit, masterListProd.Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);

                    planProd.TotalProduct = displayTotal.Value;
                    planProd.TotalProductUnit = displayTotal.Unit.Name;

                    var unit = string.IsNullOrEmpty(prod.SpecificCostUnit) ? null : UnitFactory.GetPackageSafeUnit(prod.SpecificCostUnit, masterListProd.StdUnit, masterListProd.StdFactor, masterListProd.StdPackageUnit);
                    var convertedTotal = unit != null && displayTotal.CanConvertTo(unit) ? displayTotal.GetValueAs(unit) : displayTotal.Value;
                    var totalCost = prod.SpecificCostPerUnit.HasValue ? (decimal)convertedTotal * prod.SpecificCostPerUnit.Value : prod.ProductTotalCost;

                    planProd.TotalCost = (double)totalCost;
                    planProd.Area = (double)prod.PercentApplied * planData.EstimatedArea.Value * (double)prod.ApplicationCount;
                    planProd.AreaUnit = planData.EstimatedArea.Unit;
                    planProd.CostPerArea = (double)totalCost / planData.EstimatedArea.Value;
                    planProd.TargetDate = prod.TargetDate;

                    data.Products.Add(planProd);
                }

                data.Products = data.Products.OrderBy(x => x.TimingEventOrder).ThenBy(y => y.TimingEventTag).ThenBy(b => b.TargetDate).ToList();

                var report = new Views.Secondary.Reports.Plan.ProductsDetail();
                report.DataSource = data.Products;
                report.Name = Strings.ReportName_PlannedProductSchedule_Text;

                book.Reports.Add(report);
            }

            rs.ReportDocument = book;
            return rs;
        }
    }
}
