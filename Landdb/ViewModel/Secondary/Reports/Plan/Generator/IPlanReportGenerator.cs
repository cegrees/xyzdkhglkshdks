﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.Plan.Generator
{
    public interface IPlanReportGenerator
    {
        ReportSource Generate(PlanQueryCriteria crit);
        string DisplayName { get; }
    }
}
