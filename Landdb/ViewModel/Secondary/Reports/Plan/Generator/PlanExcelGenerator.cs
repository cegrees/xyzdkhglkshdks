﻿using AgC.UnitConversion;
using ExportToExcel;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Plan;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ReportModels.Plan;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Landdb.Resources;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.Plan.Generator
{
    public class PlanExcelGenerator : IPlanReportGenerator
    {
        IClientEndpoint endpoint;
        int cropYear;

        string filename;

        public PlanExcelGenerator(IClientEndpoint endpoint, int cropYear)
        {
            this.endpoint = endpoint;
            this.cropYear = cropYear;
        }

        public Telerik.Reporting.ReportSource Generate(PlanQueryCriteria crit)
        {
            //ReportBook book = new ReportBook();
            //var rs = new InstanceReportSource();

            var planData = endpoint.GetView<PlanView>(crit.SelectedPlan).Value;
            var flattenedHierarchy = endpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new FlattenedTreeHierarchyView());
            var masterlist = endpoint.GetMasterlistService();
            List<DetailProducts> products = new List<DetailProducts>();
            List<ExcelPlanData> planList = new List<ExcelPlanData>();


            //PLAN DATA TO A WORKSHEET/////////////////////////////////////////////////

            var data = new ExcelPlanData();
            data.Rent = (decimal)planData.LandRent;
            data.Equipment = (decimal)planData.Equipment;
            data.Insurance = (decimal)planData.Insurance;
            data.Labor = (decimal)planData.Labor;
            data.Repairs = (decimal)planData.Repairs;
            data.Taxes = (decimal)planData.Taxes;
            data.ReturnToMgt = (decimal)planData.ReturnToMgt;
            data.EstimatedArea = (decimal)planData.EstimatedArea.Value;
            data.PlanId = planData.Id;
            data.PlanName = planData.Name;
            data.CropYear = planData.CropYear;
            data.Notes = planData.Notes;
            data.YieldPerArea = (decimal)planData.YieldPerArea;
            data.PricePerUnit = (decimal)planData.PricePerUnit;
            data.FSAPayment = (decimal)planData.FSAPayment;
            data.CropShare = (decimal)planData.CropSharePercent;

            planList.Add(data);

            ///////////////////////////////////////////////////////////////////////////

            if (planData.CropZones.Any() && planData.CropZones.Count() > 0)
            {

                foreach (var cz in planData.CropZones)
                {
                    var czDetails = endpoint.GetView<CropZoneDetailsView>(cz.Id).Value;
                    var flattened = flattenedHierarchy.Items.FirstOrDefault(x => x.CropZoneId == cz.Id);

                    foreach (var prod in planData.Products)
                    {
                        var prodDetail = new DetailProducts();
                        prodDetail.CropZoneId = cz.Id.Id;
                        prodDetail.CropZone = czDetails.Name;
                        prodDetail.FieldId = flattened.FieldId.Id;
                        prodDetail.FarmId = flattened.FarmId.Id;
                        prodDetail.Field = flattened.FieldName;
                        prodDetail.Farm = flattened.FarmName;
                        prodDetail.Area = (decimal?)cz.Area.Value;

                        //get crop zone Details 
                        prodDetail.Tags = string.Join(" + ", czDetails.Tags.ToArray());

                        var mlp = masterlist.GetProduct(prod.Id);
                        prodDetail.PlanId = planData.Id;
                        prodDetail.PlanName = planData.Name;
                        prodDetail.CropYear = cropYear;
                        prodDetail.Crop = flattened.CropName;
                        prodDetail.ProductName = mlp.Name;
                        prodDetail.ProductId = mlp.Id;
                        prodDetail.ApplicationCount = prod.ApplicationCount.HasValue ? prod.ApplicationCount.Value : 1;
                        prodDetail.PercentApplied = prod.PercentApplied;
                        prodDetail.TimingEvent = prod.TimingEvent;
                        var timingEventObj = !string.IsNullOrEmpty(prod.TimingEvent) ? masterlist.GetTimingEventList().FirstOrDefault(x => x.Name == prod.TimingEvent) : null;
                        prodDetail.TimingEventOrder = timingEventObj != null ? timingEventObj.Order + 1 : 0;
                        prodDetail.TimingEventTag = prod.TimingEventTag;
                        prodDetail.TargetDate = prod.TargetDate;

                        //get total product based on crop zone and total price.
                        var totalProd = prod.RateValue * ((decimal)cz.Area.Value * prodDetail.PercentApplied) * prodDetail.ApplicationCount;
                        var totalProdMeasure = UnitFactory.GetPackageSafeUnit(prod.RateUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit).GetMeasure((double)totalProd.Value, mlp.Density);
                        totalProdMeasure = totalProdMeasure.CreateAs(UnitFactory.GetPackageSafeUnit(mlp.StdPackageUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit));
                        var displayRate = DisplayMeasureConverter.ConvertToRateDisplayMeasure(prod.Id, prod.RateValue, prod.RateUnit, mlp.Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);
                        var displayTotal = DisplayMeasureConverter.ConvertToTotalDisplayMeasure(prod.Id, (decimal)totalProdMeasure.Value, totalProdMeasure.Unit.Name, mlp.Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);

                        prodDetail.Rate = (decimal?)displayRate.Value;
                        prodDetail.RateUnit = displayRate.Unit.AbbreviatedDisplay;
                        prodDetail.TotalProduct = (decimal?)displayTotal.Value;
                        prodDetail.TotalProductUnit = displayTotal.Unit.AbbreviatedDisplay;

                        var costUnit = UnitFactory.GetPackageSafeUnit(prod.SpecificCostUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                        var convertedTotalValue = displayTotal.CanConvertTo(costUnit) ? displayTotal.GetValueAs(costUnit) : displayTotal.Value;
                        var totalCost = prod.SpecificCostPerUnit.HasValue ? (decimal)convertedTotalValue * prod.SpecificCostPerUnit.Value : 0m;
                        prodDetail.TotalCost = totalCost;
                        prodDetail.CostPerArea = totalCost / (decimal)cz.Area.Value;
                        products.Add(prodDetail);
                    }
                }
            }
            else
            {
                //run thru the products with a reported area equal to the estimated area of the plan.
                foreach (var prod in planData.Products)
                {
                    var prodDetail = new DetailProducts();

                    var mlp = masterlist.GetProduct(prod.Id);
                    prodDetail.PlanId = planData.Id;
                    prodDetail.PlanName = planData.Name;
                    prodDetail.CropYear = cropYear;
                    prodDetail.Crop = masterlist.GetCropDisplay(new CropId( planData.CropId ));
                    prodDetail.ProductName = mlp.Name;
                    prodDetail.ProductId = mlp.Id;
                    prodDetail.ApplicationCount = prod.ApplicationCount.HasValue ? prod.ApplicationCount.Value : 1;
                    prodDetail.PercentApplied = prod.PercentApplied;
                    prodDetail.TimingEvent = prod.TimingEvent;
                    var timingEventObj = !string.IsNullOrEmpty(prod.TimingEvent) ? masterlist.GetTimingEventList().FirstOrDefault(x => x.Name == prod.TimingEvent) : null;
                    prodDetail.TimingEventOrder = timingEventObj != null ? timingEventObj.Order + 1 : 0;
                    prodDetail.TimingEventTag = prod.TimingEventTag;

                    //get total product based on crop zone and total price.
                    var totalProd = prod.RateValue * ((decimal)planData.EstimatedArea.Value * prodDetail.PercentApplied) * prodDetail.ApplicationCount;
                    var totalProdMeasure = UnitFactory.GetPackageSafeUnit(prod.RateUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit).GetMeasure((double)totalProd.Value, mlp.Density);
                    totalProdMeasure = totalProdMeasure.CreateAs(UnitFactory.GetPackageSafeUnit(mlp.StdPackageUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit));
                    var displayRate = DisplayMeasureConverter.ConvertToRateDisplayMeasure(prod.Id, prod.RateValue, prod.RateUnit, mlp.Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);
                    var displayTotal = DisplayMeasureConverter.ConvertToTotalDisplayMeasure(prod.Id, (decimal)totalProdMeasure.Value, totalProdMeasure.Unit.Name, mlp.Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);

                    prodDetail.Rate = (decimal?)displayRate.Value;
                    prodDetail.RateUnit = prod.RateUnit;
                    prodDetail.TotalProduct = (decimal?)displayTotal.Value;
                    prodDetail.TotalProductUnit = displayTotal.Unit.Name;

                    var costUnit = UnitFactory.GetPackageSafeUnit(prod.SpecificCostUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                    var convertedTotalValue = displayTotal.CanConvertTo(costUnit) ? displayTotal.GetValueAs(costUnit) : displayTotal.Value;
                    var totalCost = prod.SpecificCostPerUnit.HasValue ? (decimal)convertedTotalValue * prod.SpecificCostPerUnit.Value : 0m;
                    prodDetail.TotalCost = totalCost;

                    products.Add(prodDetail);
                }
            }

            products = products.OrderBy(x => x.TimingEventOrder).ThenBy(y => y.TimingEventTag).ThenBy(b => b.TargetDate).ToList();

            var planTable = ExportToExcel.CreateExcelFile.ListToDataTable(planList);
            planTable.TableName = Strings.TableName_PlanDetails_Text;
            var prodTable = ExportToExcel.CreateExcelFile.ListToDataTable(products);
            prodTable.TableName = Strings.TableName_PlannedProducts_Text;

            DataSet ds = new DataSet("planDS");
            ds.Tables.Add(planTable);
            ds.Tables.Add(prodTable);

            try
            {
                //open up file dialog to save file....
                //then call createexcelfile to create the excel...
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();

                saveFileDialog1.Filter = "Excel|*.xlsx";
                saveFileDialog1.FilterIndex = 2;
                saveFileDialog1.RestoreDirectory = true;

                Nullable<bool> resultSaved = saveFileDialog1.ShowDialog();

                // Process save file dialog box results 
                if (resultSaved == true)
                {
                    // Save document 
                    filename = saveFileDialog1.FileName;
                    CreateExcelFile.CreateExcelDocument(ds, filename);
                }

                //now open file....
                System.Diagnostics.Process.Start(filename);
            }
            catch (Exception ex)
            {

            }
            //rs.ReportDocument = book;
            return null;
        }

        public string DisplayName
        {
            get { return Strings.GeneratorName_PlanExcel_Text; }
        }
    }
}
