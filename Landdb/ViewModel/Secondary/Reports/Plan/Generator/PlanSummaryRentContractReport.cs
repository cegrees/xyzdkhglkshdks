﻿using AgC.UnitConversion;
using ClosedXML.Excel;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Contract;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Domain.ReadModels.Plan;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ReportModels.Plan;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Landdb.Resources;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.Plan.Generator
{
    public class PlanSummaryRentContractReport : IPlanReportGenerator
    {
        IClientEndpoint endpoint;
        int cropYear;
        string filename;

        public PlanSummaryRentContractReport(IClientEndpoint endPoint, int cropyear)
        {
            endpoint = endPoint;
            cropYear = cropyear;
        }

        public string DisplayName
        {
            get
            {
                return Strings.ReportName_PlanSummaryFromRentContractInfo_Text;
            }
        }

        public ReportSource Generate(PlanQueryCriteria crit)
        {
            var currentCropYearId = new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear);

            var masterlist = endpoint.GetMasterlistService();
            //var cropZoneApplications = endpoint.GetView<CropZoneApplicationDataView>(currentCropYearId).GetValue(new CropZoneApplicationDataView()).Items;
            var inventoryList = endpoint.GetView<InventoryListView>(currentCropYearId).GetValue(new InventoryListView()).Products;
            var flattenedHierarchyView = endpoint.GetView<FlattenedTreeHierarchyView>(currentCropYearId).GetValue(new FlattenedTreeHierarchyView());
            var contractedCropZoneList = endpoint.GetView<CropZoneRentContractView>(currentCropYearId).GetValue(() => new CropZoneRentContractView()).CropZoneRentContracts;
            List<PlanSummaryCostBreakOut> dataList = new List<PlanSummaryCostBreakOut>();

            foreach (var plan in crit.SelectedPlans)
            {
                PlanView planDetails = endpoint.GetView<PlanView>(plan).Value;

                foreach (var cz in planDetails.CropZones)
                {
                    var czDetials = endpoint.GetView<CropZoneDetailsView>(cz.Id).Value;
                    var flattenedItem = flattenedHierarchyView.Items.FirstOrDefault(c => c.CropZoneId == cz.Id);
                    PlanSummaryCostBreakOut data = new PlanSummaryCostBreakOut();
                    data.PlanId = planDetails.Id;
                    data.PlanName = planDetails.Name;
                    var fieldDetails = endpoint.GetView<FieldDetailsView>(czDetials.FieldId).Value;
                    var area = czDetials.ReportedArea.HasValue ? czDetials.ReportedArea.Value : (czDetials.BoundaryArea.HasValue ? czDetials.BoundaryArea.Value : 0.0);

                    var fixedCost = (planDetails.Equipment + planDetails.Insurance + planDetails.Labor + planDetails.Repairs + planDetails.ReturnToMgt + planDetails.Taxes) * area;
                    data.CropZonePlanFixedCost = (decimal)fixedCost;

                    data.CropZoneTotalFSAPayment = planDetails.FSAPayment * area;

                    var totalFertvariableCost = 0.0;
                    var totalSeedVariableCost = 0.0;
                    var totalServiceVariableCost = 0.0;
                    var totalCPVariableCost = 0.0;

                    foreach (var prod in planDetails.Products)
                    {
                        var mlp = masterlist.GetProduct(prod.Id);
                        mlp = mlp != null ? mlp : masterlist.GetProduct(new ProductId(ApplicationEnvironment.CurrentDataSourceId, prod.Id.Id));

                        var ratePerArea = prod.RateValue;
                        var ratePerAreaUnit = UnitFactory.GetPackageSafeUnit(prod.RateUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                        var rate = ratePerAreaUnit.GetMeasure((double)ratePerArea, mlp.Density);

                        var total = rate * area * (double)prod.PercentApplied;
                        var cost = prod.SpecificCostPerUnit;
                        var costUnit = UnitFactory.GetPackageSafeUnit(prod.SpecificCostUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);

                        var convertedTotal = costUnit != total.Unit && total.CanConvertTo(costUnit) ? total.GetValueAs(costUnit) : total.Value;
                        var totalCZCost = convertedTotal * (double)cost;

                        switch (mlp.ProductType)
                        {
                            case "Fertilizer":
                                totalFertvariableCost += totalCZCost;

                                break;
                            case "Seed":
                                totalSeedVariableCost += totalCZCost;

                                break;
                            case "Service":
                                totalServiceVariableCost += totalCZCost;

                                break;
                            default:
                                totalCPVariableCost += totalCZCost;

                                break;
                        }
                    }

                    //get rent contract
                    if (contractedCropZoneList.ContainsKey(cz.Id.Id) && contractedCropZoneList[cz.Id.Id] != null && contractedCropZoneList[cz.Id.Id].RentContractId != null)
                    {
                        var contractMaybe = endpoint.GetView<RentContractDetailsView>(contractedCropZoneList[cz.Id.Id].RentContractId);
                        if (contractMaybe.HasValue)
                        {
                            var rentContract = contractMaybe.Value;

                            var totalYield = planDetails.YieldPerArea * area;
                            data.CropZonePlannedYieldTotal = totalYield * rentContract.IncludedShareInfo.GrowerCropShare;
                            data.CropZonePlannedYieldPricePerUnit = planDetails.PricePerUnit;

                            data.Farm = flattenedItem.FarmName;
                            data.FarmId = flattenedItem.FarmId;
                            data.Field = flattenedItem.FieldName;
                            data.FieldId = flattenedItem.FieldId;
                            data.CropZone = czDetials.Name;
                            data.CropZoneId = czDetials.Id;
                            data.Crop = masterlist.GetCropDisplay(czDetials.CropId);
                            data.LandOwner = rentContract.LandOwner != null ? rentContract.LandOwner.Name : string.Empty;
                            data.Contract = Strings.Yes_Text.ToUpper();
                            data.County = fieldDetails.County;
                            data.Area = (decimal)area;

                            var shareInfo = rentContract.IncludedShareInfo;
                            var totalGSCost = (totalFertvariableCost * shareInfo.GrowerFertilizerShare) + (totalCPVariableCost * shareInfo.GrowerCropProtectionShare)
                                + (totalSeedVariableCost * shareInfo.GrowerSeedShare) + (totalServiceVariableCost * shareInfo.GrowerServiceShare);

                            var czTotalGSVariableCost = totalGSCost + (rentContract.PerAcre * area);
                            data.GrowerShareCost = (decimal)czTotalGSVariableCost;
                            data.GrowerShareCostPerArea = (decimal)(czTotalGSVariableCost / area);


                            data.ContractName = rentContract.Name;

                        }
                        else
                        {
                            //treat as uncontracted field
                            var totalYield = planDetails.YieldPerArea * area;
                            data.CropZonePlannedYieldTotal = totalYield;
                            data.CropZonePlannedYieldPricePerUnit = planDetails.PricePerUnit;

                            data.Farm = flattenedItem.FarmName;
                            data.FarmId = flattenedItem.FarmId;
                            data.Field = flattenedItem.FieldName;
                            data.FieldId = flattenedItem.FieldId;
                            data.CropZone = czDetials.Name;
                            data.CropZoneId = czDetials.Id;
                            data.Crop = masterlist.GetCropDisplay(czDetials.CropId);
                            data.LandOwner = "";
                            data.Contract = Strings.No_Text.ToUpper();
                            data.County = fieldDetails.County;
                            data.Area = (decimal)area;

                            var totalGSCost = totalFertvariableCost + totalCPVariableCost + totalSeedVariableCost + totalServiceVariableCost;

                            data.GrowerShareCost = (decimal)totalGSCost;
                            data.GrowerShareCostPerArea = (decimal)(totalGSCost / area);
                        }
                    }

                    else
                    {
                        //uncontracted field
                        var totalYield = planDetails.YieldPerArea * area;
                        data.CropZonePlannedYieldTotal = totalYield;
                        data.CropZonePlannedYieldPricePerUnit = planDetails.PricePerUnit;

                        data.Farm = flattenedItem.FarmName;
                        data.FarmId = flattenedItem.FarmId;
                        data.Field = flattenedItem.FieldName;
                        data.FieldId = flattenedItem.FieldId;
                        data.CropZone = czDetials.Name;
                        data.CropZoneId = czDetials.Id;
                        data.Crop = masterlist.GetCropDisplay(czDetials.CropId);
                        data.LandOwner = "";
                        data.Contract = Strings.No_Text.ToUpper(); // false;
                        data.County = fieldDetails.County;
                        data.Area = (decimal)area;

                        var totalGSCost = totalFertvariableCost + totalCPVariableCost + totalSeedVariableCost + totalServiceVariableCost;

                        data.GrowerShareCost = (decimal)totalGSCost;
                        data.GrowerShareCostPerArea = (decimal)(totalGSCost / area);
                    }
                    dataList.Add(data);
                }
            }

            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //SET UP SPREAD SHEET
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            var wb = new XLWorkbook();
            var ws = wb.AddWorksheet(Strings.ExcelWorksheet_Title_EnterprisePlanSummary_Text);

            if (crit.SelectedPlans.Any())
            {
                #region HEADERS
                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                //HEADERS
                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                ws.Cell("A1").Value = Strings.ExcelWorksheet_Header_EnterprisePlanSummary_Text;
                ws.Cell("A2").Value = Strings.ExcelWorksheet_Header_Farm_Text;
                ws.Cell("B2").Value = Strings.ExcelWorksheet_Header_Field_Text;
                ws.Cell("C2").Value = Strings.ExcelWorksheet_Header_CropZone_Text;
                ws.Cell("D2").Value = Strings.ExcelWorksheet_Header_Crop_Text;
                ws.Cell("E2").Value = Strings.ExcelWorksheet_Header_LandOwner_Text;
                ws.Cell("F2").Value = Strings.ExcelWorksheet_Header_Contract_Text;
                ws.Cell("G2").Value = Strings.ExcelWorksheet_County_Text;

                ws.Cell("H2").Value = Strings.ExcelWorksheet_FixedCost_Text;
                ws.Cell("I2").Value = Strings.ExcelWorksheet_Revenue_Text;

                ws.Cell("J2").Value = Strings.ExcelWorksheet_YieldArea_Text;
                ws.Cell("K2").Value = Strings.ExcelWorksheet_TotalYield_Text;

                ws.Cell("L2").Value = Strings.ExcelWorksheet_Header_Area_Text;
                ws.Cell("M2").Value = Strings.ExcelWorksheet_Header_GrowerShareTotalCost_Text;
                ws.Cell("N2").Value = Strings.ExcelWorksheet_Header_GrowerShareCostArea_Text;

                ws.Cell("P2").Value = Strings.ExcelWorksheet_Header_ContractName_Text;
                ws.Cell("Q2").Value = Strings.ExcelWorksheet_Header_PlanName_Text;

                var planHeaders = ws.Range("A1:Q2");
                planHeaders.Style.Font.Bold = true;
                planHeaders.Style.Alignment.SetWrapText(true);
                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                #endregion
            }

            int rowCounter = 3;
            foreach(var item in dataList)
            {
                ws.Cell("A" + rowCounter.ToString()).Value = item.Farm;
                ws.Cell("B" + rowCounter.ToString()).Value = item.Field;
                ws.Cell("C" + rowCounter.ToString()).Value = item.CropZone;
                ws.Cell("D" + rowCounter.ToString()).Value = item.Crop;
                ws.Cell("E" + rowCounter.ToString()).Value = item.LandOwner;
                ws.Cell("F" + rowCounter.ToString()).Value = item.Contract;
                ws.Cell("G" + rowCounter.ToString()).Value = item.County;

                ws.Cell("H" + rowCounter.ToString()).Value = item.CropZonePlanFixedCost;
                ws.Cell("I" + rowCounter.ToString()).Value = item.CropZoneTotalFSAPayment + (item.CropZonePlannedYieldTotal * item.CropZonePlannedYieldPricePerUnit);

                ws.Cell("J" + rowCounter.ToString()).Value = item.CropZonePlannedYieldTotal / (double)item.Area;
                ws.Cell("K" + rowCounter.ToString()).Value = item.CropZonePlannedYieldTotal;

                ws.Cell("L" + rowCounter.ToString()).Value = item.Area; 
                ws.Cell("M" + rowCounter.ToString()).Value = item.GrowerShareCost; 
                ws.Cell("N" + rowCounter.ToString()).Value = item.GrowerShareCostPerArea;

                ws.Cell("P" + rowCounter.ToString()).Value = item.ContractName;
                ws.Cell("Q" + rowCounter.ToString()).Value = item.PlanName;

                rowCounter++;
            }

            ws.Cell("J" + rowCounter.ToString()).FormulaA1 = "=K" + rowCounter.ToString() + "/" + "L" + rowCounter.ToString();
            ws.Cell("K" + rowCounter.ToString()).FormulaA1 = "=SUM(K3:K" + (rowCounter - 1).ToString() + ")";
            ws.Cell("L" + rowCounter.ToString()).FormulaA1 = "=SUM(L3:L" + (rowCounter - 1).ToString() + ")";
            ws.Cell("M" + rowCounter.ToString()).FormulaA1 = "=SUM(M3:M" + (rowCounter - 1).ToString() + ")";
            ws.Cell("N" + rowCounter.ToString()).FormulaA1 = "=SUM(N3:N" + (rowCounter - 1).ToString() + ")";

            ws.Cell("L" + (rowCounter + 2).ToString()).Value = Strings.ExcelWorksheet_Header_GrossRevenue_Text;
            ws.Cell("M" + (rowCounter + 2).ToString()).FormulaA1 = "=SUM(I3:I" + (rowCounter - 1).ToString() + ")";
            ws.Cell("N" + (rowCounter + 2).ToString()).FormulaA1 = "=M" + (rowCounter + 2).ToString() + "/" + "L" + rowCounter.ToString();

            ws.Cell("L" + (rowCounter + 3).ToString()).Value = Strings.ExcelWorksheet_Header_FixedCost_Text;
            ws.Cell("M" + (rowCounter + 3).ToString()).FormulaA1 = "=SUM(H3:H" + (rowCounter - 1).ToString() + ")";
            ws.Cell("N" + (rowCounter + 3).ToString()).FormulaA1 = "=K" + (rowCounter + 3).ToString() + "/" + "L" + rowCounter.ToString();

            ws.Cell("L" + (rowCounter + 4).ToString()).Value = Strings.ExcelWorksheet_Header_TotalCost_Text;
            ws.Cell("M" + (rowCounter + 4).ToString()).FormulaA1 = "=M" + rowCounter.ToString() + " + M" + (rowCounter + 3).ToString();
            ws.Cell("N" + (rowCounter + 4).ToString()).FormulaA1 = "=M" + (rowCounter + 4).ToString() + "/ L" + rowCounter.ToString();

            ws.Cell("L" + (rowCounter + 6).ToString()).Value = Strings.ExcelWorksheet_Header_NetProfit_Text;
            ws.Cell("M" + (rowCounter + 6).ToString()).FormulaA1 = "=M" + (rowCounter + 2).ToString() + "-" + "M" + (rowCounter + 4).ToString();
            ws.Cell("N" + (rowCounter + 6).ToString()).FormulaA1 = "=M" + (rowCounter + 6).ToString() + "/" + "L" + rowCounter.ToString();

            ws.Cell("A" + (rowCounter + 3).ToString()).Value = Strings.ExcelWorksheet_Header_AverageCostPerUnit_Text;
            ws.Cell("B" + (rowCounter + 3).ToString()).FormulaA1 = "=M" + (rowCounter + 2).ToString() + " / K" + rowCounter.ToString();

            ws.Cell("A" + (rowCounter + 4).ToString()).Value = Strings.ExcelWorksheet_Header_AverageYield_Text;
            ws.Cell("B" + (rowCounter + 4).ToString()).FormulaA1 = "=K" + rowCounter.ToString() + " / L" + rowCounter.ToString();

            ws.Cell("A" + (rowCounter + 5).ToString()).Value = Strings.ExcelWorksheet_Header_BreakevenYieldPerArea_Text;
            ws.Cell("B" + (rowCounter + 5).ToString()).FormulaA1 = "=M" + (rowCounter + 4).ToString() + " / B" + (rowCounter + 3).ToString();

            ws.Cell("A" + (rowCounter + 6).ToString()).Value = Strings.ExcelWorksheet_Header_BreakevenCostPerUnit_Text;
            ws.Cell("B" + (rowCounter + 6).ToString()).FormulaA1 = "=M" + (rowCounter + 4).ToString() + " / K" + rowCounter.ToString();

            //////////////////////////////////////////////////////////////////////////////////////////////////////
            //Styling Range
            //////////////////////////////////////////////////////////////////////////////////////////////////////

            var labelRange = ws.Range("A" + (rowCounter + 3).ToString(), "A" + (rowCounter + 6).ToString());
            labelRange.Style.Font.Bold = true;

            var yieldInfoRange = ws.Range("B" + (rowCounter + 4).ToString(), "B" + (rowCounter + 5).ToString());
            yieldInfoRange.Style.NumberFormat.Format = "0.00";

            var fixedLabelStye = ws.Range("L" + (rowCounter + 2).ToString(), "L" + (rowCounter + 6).ToString());
            fixedLabelStye.Style.Font.Bold = true;

            var numbersStyleRange = ws.Range("J2", "L" + rowCounter.ToString());
            numbersStyleRange.Style.NumberFormat.Format = "0.00";

            var currencyStyleRange = ws.Range("M2", "N" + (rowCounter + 6).ToString());
            currencyStyleRange.Style.NumberFormat.Format = "$0.00";

            var currencyStyleRange2 = ws.Range("H2", "I" + (rowCounter -1).ToString());
            currencyStyleRange2.Style.NumberFormat.Format = "$0.00";

            try
            {
                //open up file dialog to save file....
                //then call createexcelfile to create the excel...
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();

                saveFileDialog1.Filter = "Excel|*.xlsx";
                saveFileDialog1.FilterIndex = 2;
                saveFileDialog1.RestoreDirectory = true;

                Nullable<bool> resultSaved = saveFileDialog1.ShowDialog();

                // Process save file dialog box results 
                if (resultSaved == true)
                {
                    // Save document 
                    filename = saveFileDialog1.FileName;
                    wb.SaveAs(filename);
                }

                //now open file....
                System.Diagnostics.Process.Start(filename);
            }
            catch (Exception ex)
            {


            }
            return null;
        }
    }
}
