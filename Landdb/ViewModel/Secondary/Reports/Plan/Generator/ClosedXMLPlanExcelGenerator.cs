﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Reporting;
using ClosedXML.Excel;
using ClosedXML;
using AgC.UnitConversion;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Plan;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.ReportModels.Plan;
using System.Data;
using Microsoft.Win32;
using System.Text.RegularExpressions;
using Landdb.Resources;

namespace Landdb.ViewModel.Secondary.Reports.Plan.Generator
{

    public class ClosedXMLPlanExcelGenerator : IPlanReportGenerator
    {
        IClientEndpoint endpoint;
        int cropYear;
        static string columnHeaders = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        string filename;

        public ClosedXMLPlanExcelGenerator(IClientEndpoint endPoint, int cropyear)
        {
            endpoint = endPoint;
            cropYear = cropyear;
        }
        public string DisplayName
        {
            get
            {
                return Strings.GeneratorName_PlanReportTest_Text;
            }
        }

        public ReportSource Generate(PlanQueryCriteria crit)
        {
            var planData = endpoint.GetView<PlanView>(crit.SelectedPlan).Value;
            var flattenedHierarchy = endpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new FlattenedTreeHierarchyView());
            var masterlist = endpoint.GetMasterlistService();
            List<DetailProducts> products = new List<DetailProducts>();
            List<ExcelPlanData> planList = new List<ExcelPlanData>();

            var wb = new XLWorkbook();
            var ws = wb.AddWorksheet(Strings.ExcelWorkbook_Title_PlanDetails_Text);
            //PLAN DATA TO A WORKSHEET/////////////////////////////////////////////////

            //var data = new ExcelPlanData();
            //data.Rent = (decimal)planData.LandRent;
            ws.Cell("A1").Value = Strings.ExcelWorksheet_Title_FixedCost_Text;
            var fixedCostHeader = ws.Range("A1:P1");
            fixedCostHeader.Merge();

            ws.Cell("A3").Value = Strings.ExcelWorksheet_Header_Equipment_Text;
            ws.Cell("B3").Value = Strings.ExcelWorksheet_Header_Insurance_Text;
            ws.Cell("C3").Value = Strings.ExcelWorksheet_Header_Labor_Text;
            ws.Cell("D3").Value = Strings.ExcelWorksheet_Header_Repairs_Text;
            ws.Cell("E3").Value = Strings.ExcelWorksheet_Header_Taxes_Text;
            ws.Cell("F3").Value = Strings.ExcelWorksheet_Header_ReturnToManagement_Text;
            ws.Cell("G3").Value = Strings.ExcelWorksheet_Header_EstimatedArea_Text;
            ws.Cell("H3").Value = Strings.ExcelWorksheet_Header_PlanId_Text;
            ws.Cell("I3").Value = Strings.ExcelWorksheet_Header_PlanName_Text;
            ws.Cell("J3").Value = Strings.ExcelWorksheet_Header_CropYear_Text;
            ws.Cell("K3").Value = Strings.ExcelWorksheet_Header_Notes_Text;
            ws.Cell("L3").Value = Strings.ExcelWorksheet_Header_YieldPerArea_Text;
            ws.Cell("M3").Value = Strings.ExcelWorksheet_Header_PricePerUnit_Text;
            ws.Cell("N3").Value = Strings.ExcelWorksheet_Header_FsaPayment_Text;
            ws.Cell("O3").Value = Strings.ExcelWorksheet_Header_CropShare_Text;
            var planHeaders = ws.Range("A3:P3");
            planHeaders.Style.Font.Bold = true;

            ws.Cell("A4").Value = (decimal)planData.Equipment;
            ws.Cell("B4").Value = (decimal)planData.Insurance;
            ws.Cell("C4").Value = (decimal)planData.Labor;
            ws.Cell("D4").Value = (decimal)planData.Repairs;
            ws.Cell("E4").Value = (decimal)planData.Taxes;
            ws.Cell("F4").Value = (decimal)planData.ReturnToMgt;
            ws.Cell("G4").Value = (decimal)planData.EstimatedArea.Value;
            ws.Cell("H4").Value = planData.Id;
            ws.Cell("I4").Value = planData.Name;
            ws.Cell("J4").Value = planData.CropYear;
            ws.Cell("K4").Value = planData.Notes;
            ws.Cell("L4").Value = (decimal)planData.YieldPerArea;
            ws.Cell("M4").Value = (decimal)planData.PricePerUnit;
            ws.Cell("N4").Value = (decimal)planData.FSAPayment;
            ws.Cell("O4").Value = (decimal)planData.CropSharePercent;

            ///////////////////////////////////////////////////////////////////////////
            //INITIALIZE HEADERS
            //////////////////////////////////////////////////////////////////////////
            ws.Cell("A7").Value = Strings.ExcelWorksheet_Header_PlanId_Text;
            ws.Cell("B7").Value = Strings.ExcelWorksheet_Header_PlanName_Text;
            ws.Cell("C7").Value = Strings.ExcelWorksheet_Header_CropYear_Text;
            ws.Cell("D7").Value = Strings.ExcelWorksheet_Header_Crop_Text;
            ws.Cell("E7").Value = Strings.ExcelWorksheet_Header_ProductId_Text;
            ws.Cell("F7").Value = Strings.ExcelWorksheet_Header_ProductName_Text;
            ws.Cell("G7").Value = Strings.ExcelWorksheet_Header_Rate_Text;
            ws.Cell("H7").Value = Strings.ExcelWorksheet_Header_Unit_Text;
            ws.Cell("I7").Value = Strings.ExcelWorksheet_Header_Total_Text;
            ws.Cell("J7").Value = Strings.ExcelWorksheet_Header_Unit_Text;
            ws.Cell("K7").Value = Strings.ExcelWorksheet_Header_TotalCost_Text;
            ws.Cell("L7").Value = Strings.ExcelWorksheet_Header_CostPerArea_Text;
            ws.Cell("M7").Value = Strings.ExcelWorksheet_Header_ApplicationCount_Text;
            ws.Cell("N7").Value = Strings.ExcelWorksheet_Header_PercentApplied_Text;
            ws.Cell("O7").Value = Strings.ExcelWorksheet_Header_TimingEvent_Text;
            ws.Cell("P7").Value = Strings.ExcelWorksheet_Header_TmingEventOrder_Text;
            ws.Cell("Q7").Value = Strings.ExcelWorksheet_Header_TimingEventTag_Text;
            ws.Cell("R7").Value = Strings.ExcelWorksheet_Header_TargetDate_Text;
            ws.Cell("S7").Value = Strings.ExcelWorksheet_Header_FarmId_Text;
            ws.Cell("T7").Value = Strings.ExcelWorksheet_Header_Farm_Text;
            ws.Cell("U7").Value = Strings.ExcelWorksheet_Header_FieldId_Text;
            ws.Cell("V7").Value = Strings.ExcelWorksheet_Header_Field_Text;
            ws.Cell("W7").Value = Strings.ExcelWorksheet_Header_CropZoneID_Text;
            ws.Cell("X7").Value = Strings.ExcelWorksheet_Header_CropZone_Text;
            ws.Cell("Y7").Value = Strings.ExcelWorksheet_Header_Area_Text;
            ws.Cell("Z7").Value = Strings.ExcelWorksheet_Header_Tags_Text;

            var productHeaders = ws.Range("A7:Z7");
            productHeaders.Style.Font.Bold = true;
            productHeaders.Style.Fill.BackgroundColor = XLColor.Gainsboro;
            /////////////////////////////////////////////////////////////////////////

            int rowCnt = 8;

            if (planData.CropZones.Any() && planData.CropZones.Count() > 0)
            {
                foreach (var cz in planData.CropZones)
                {
                    var czDetails = endpoint.GetView<CropZoneDetailsView>(cz.Id).Value;
                    var flattened = flattenedHierarchy.Items.FirstOrDefault(x => x.CropZoneId == cz.Id);

                    foreach (var prod in planData.Products)
                    {
                        var mlp = masterlist.GetProduct(prod.Id);

                        //get total product based on crop zone and total price.
                        var totalProd = prod.RateValue * ((decimal)cz.Area.Value * prod.PercentApplied) * prod.ApplicationCount;
                        var totalProdMeasure = UnitFactory.GetPackageSafeUnit(prod.RateUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit).GetMeasure((double)totalProd.Value, mlp.Density);
                        totalProdMeasure = totalProdMeasure.CreateAs(UnitFactory.GetPackageSafeUnit(mlp.StdPackageUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit));
                        var displayRate = DisplayMeasureConverter.ConvertToRateDisplayMeasure(prod.Id, prod.RateValue, prod.RateUnit, mlp.Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);
                        var displayTotal = DisplayMeasureConverter.ConvertToTotalDisplayMeasure(prod.Id, (decimal)totalProdMeasure.Value, totalProdMeasure.Unit.Name, mlp.Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);

                        var costUnit = UnitFactory.GetPackageSafeUnit(prod.SpecificCostUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                        var convertedTotalValue = displayTotal.CanConvertTo(costUnit) ? displayTotal.GetValueAs(costUnit) : displayTotal.Value;
                        var totalCost = prod.SpecificCostPerUnit.HasValue ? (decimal)convertedTotalValue * prod.SpecificCostPerUnit.Value : 0m;

                        ws.Cell("A" + rowCnt.ToString()).Value = planData.Id;
                        ws.Cell("B" + rowCnt.ToString()).Value = planData.Name;
                        ws.Cell("C" + rowCnt.ToString()).Value = cropYear;
                        ws.Cell("D" + rowCnt.ToString()).Value = flattened.CropName;
                        ws.Cell("E" + rowCnt.ToString()).Value = mlp.Id;
                        ws.Cell("F" + rowCnt.ToString()).Value = mlp.Name;
                        ws.Cell("G" + rowCnt.ToString()).Value = displayRate.Value;
                        ws.Cell("H" + rowCnt.ToString()).Value = displayRate.Unit.AbbreviatedDisplay;
                        ws.Cell("I" + rowCnt.ToString()).Value = displayTotal.Value;
                        ws.Cell("J" + rowCnt.ToString()).Value = displayTotal.Unit.AbbreviatedDisplay;
                        ws.Cell("K" + rowCnt.ToString()).Value = totalCost;
                        ws.Cell("L" + rowCnt.ToString()).Value = totalCost / (decimal)cz.Area.Value;
                        ws.Cell("M" + rowCnt.ToString()).Value = prod.ApplicationCount;
                        ws.Cell("N" + rowCnt.ToString()).Value = prod.PercentApplied;
                        ws.Cell("O" + rowCnt.ToString()).Value = prod.TimingEvent;

                        var timingEventObj = !string.IsNullOrEmpty(prod.TimingEvent) ? masterlist.GetTimingEventList().FirstOrDefault(x => x.Name == prod.TimingEvent) : null;
                        ws.Cell("P" + rowCnt.ToString()).Value = timingEventObj != null ? timingEventObj.Order + 1 : 0;

                        ws.Cell("Q" + rowCnt.ToString()).Value = prod.TimingEventTag;
                        ws.Cell("R" + rowCnt.ToString()).Value = prod.TargetDate;
                        ws.Cell("S" + rowCnt.ToString()).Value = flattened.FarmId;
                        ws.Cell("T" + rowCnt.ToString()).Value = flattened.FarmName;
                        ws.Cell("U" + rowCnt.ToString()).Value = flattened.FieldId;
                        ws.Cell("V" + rowCnt.ToString()).Value = flattened.FieldName;
                        ws.Cell("W" + rowCnt.ToString()).Value = flattened.CropZoneId;
                        ws.Cell("X" + rowCnt.ToString()).Value = flattened.CropZoneName;
                        ws.Cell("Y" + rowCnt.ToString()).Value = cz.Area.Value;
                        ws.Cell("Z" + rowCnt.ToString()).Value = string.Join(" + ", czDetails.Tags.ToArray());

                        rowCnt++;
                    }
                }
            }
            else
            {
                //run thru the products with a reported area equal to the estimated area of the plan.
                foreach (var prod in planData.Products)
                {
                    var mlp = masterlist.GetProduct(prod.Id);

                    //get total product based on crop zone and total price.
                    var totalProd = prod.RateValue * ((decimal)planData.EstimatedArea.Value * prod.PercentApplied) * prod.ApplicationCount;
                    var totalProdMeasure = UnitFactory.GetPackageSafeUnit(prod.RateUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit).GetMeasure((double)totalProd.Value, mlp.Density);
                    totalProdMeasure = totalProdMeasure.CreateAs(UnitFactory.GetPackageSafeUnit(mlp.StdPackageUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit));
                    var displayRate = DisplayMeasureConverter.ConvertToRateDisplayMeasure(prod.Id, prod.RateValue, prod.RateUnit, mlp.Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);
                    var displayTotal = DisplayMeasureConverter.ConvertToTotalDisplayMeasure(prod.Id, (decimal)totalProdMeasure.Value, totalProdMeasure.Unit.Name, mlp.Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);

                    var costUnit = UnitFactory.GetPackageSafeUnit(prod.SpecificCostUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                    var convertedTotalValue = displayTotal.CanConvertTo(costUnit) ? displayTotal.GetValueAs(costUnit) : displayTotal.Value;
                    var totalCost = prod.SpecificCostPerUnit.HasValue ? (decimal)convertedTotalValue * prod.SpecificCostPerUnit.Value : 0m;

                    ws.Cell("A" + rowCnt.ToString()).Value = planData.Id;
                    ws.Cell("B" + rowCnt.ToString()).Value = planData.Name;
                    ws.Cell("C" + rowCnt.ToString()).Value = cropYear;
                    ws.Cell("D" + rowCnt.ToString()).Value = masterlist.GetCropDisplay(new CropId( planData.CropId ));
                    ws.Cell("E" + rowCnt.ToString()).Value = mlp.Id;
                    ws.Cell("F" + rowCnt.ToString()).Value = mlp.Name;
                    ws.Cell("G" + rowCnt.ToString()).Value = displayRate.Value;
                    ws.Cell("H" + rowCnt.ToString()).Value = displayRate.Unit.AbbreviatedDisplay;
                    ws.Cell("I" + rowCnt.ToString()).Value = displayTotal.Value;
                    ws.Cell("J" + rowCnt.ToString()).Value = displayTotal.Unit.AbbreviatedDisplay;
                    ws.Cell("K" + rowCnt.ToString()).Value = totalCost;
                    ws.Cell("L" + rowCnt.ToString()).Value = totalCost / (decimal)planData.EstimatedArea.Value;
                    ws.Cell("M" + rowCnt.ToString()).Value = prod.ApplicationCount;
                    ws.Cell("N" + rowCnt.ToString()).Value = prod.PercentApplied;
                    ws.Cell("O" + rowCnt.ToString()).Value = prod.TimingEvent;

                    var timingEventObj = !string.IsNullOrEmpty(prod.TimingEvent) ? masterlist.GetTimingEventList().FirstOrDefault(x => x.Name == prod.TimingEvent) : null;
                    ws.Cell("P" + rowCnt.ToString()).Value = timingEventObj != null ? timingEventObj.Order + 1 : 0;

                    ws.Cell("Q" + rowCnt.ToString()).Value = prod.TimingEventTag;
                    ws.Cell("R" + rowCnt.ToString()).Value = prod.TargetDate;

                    rowCnt++;
                }
            }
            ////////////////////////////////////////////////////////////////////////
            //FORMAT CELLS
            ////////////////////////////////////////////////////////////////////////
            var costRange = ws.Range("K7:L" + rowCnt.ToString());
            costRange.Style.NumberFormat.Format = "$ #,##0.00";
            ///////////////////////////////////////////////////////////////////////

            try
            {
                //open up file dialog to save file....
                //then call createexcelfile to create the excel...
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();

                saveFileDialog1.Filter = "Excel|*.xlsx";
                saveFileDialog1.FilterIndex = 2;
                saveFileDialog1.RestoreDirectory = true;

                Nullable<bool> resultSaved = saveFileDialog1.ShowDialog();

                // Process save file dialog box results 
                if (resultSaved == true)
                {
                    // Save document 
                    filename = saveFileDialog1.FileName;
                    wb.SaveAs(filename);
                    //CreateExcelFile.CreateExcelDocument(ds, filename);
                }

                //now open file....
                System.Diagnostics.Process.Start(filename);
            }
            catch (Exception ex)
            {

            }

            return null;
        }
    }
}
