﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Secondary.Reports.Plan
{
    public class PlanQueryCriteria
    {
        public PlanQueryCriteria()
        {
            SelectedPlans = new List<CropPlanId>();
        }

        public CropPlanId SelectedPlan { get; set; }

        public List<CropPlanId> SelectedPlans { get; set; }
        public DateTime StartDatetime { get; set; }
        public DateTime EndDateTime { get; set; }
        
    }
}
