﻿using GalaSoft.MvvmLight;
using Landdb.Domain.ReadModels.Plan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Secondary.Reports.Plan
{
    public class PlanItemViewModel : ViewModelBase
    {
        public PlanItemViewModel( PlanListItem item)
        {
            this.Id = item.Id;
            this.IsChecked = false;
            this.EstimatedCost = item.EstimatedCost;
            this.Name = item.Name;
            this.FieldCount = item.FieldCount;
            this.ProductCount = item.ProductCount;
            this.Name = item.Name;
            this.CreateDate = item.CreateDate;
        }

        public CropPlanId Id { get; set; }
        public decimal EstimatedCost { get; set; }
        public string Name { get; set; }
        public int FieldCount { get; set; }
        public int ProductCount { get; set; }
        public DateTime CreateDate { get; set; }

        private bool _isChecked;
        public bool IsChecked { get { return _isChecked; } set { _isChecked = value; RaisePropertyChanged(()=> IsChecked); } }
    }
}
