﻿using AgC.UnitConversion;
using GalaSoft.MvvmLight;
using Landdb.Client.Infrastructure;
using Landdb.Client.Spatial;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.ViewModel.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Shapes;

namespace Landdb.ViewModel.Secondary.Reports.Crop_History
{
    public class FieldSelectionViewModel: ViewModelBase {
        readonly IClientEndpoint clientEndpoint;
        FieldId id;
        string name;
        Measure selectedArea;
        Measure baseArea;
        Path shapePreview;
        string mapData;

        public FieldSelectionViewModel(FieldTreeItemViewModel treeModel, string mapData, IClientEndpoint clientEndpoint) {
            this.clientEndpoint = clientEndpoint;
            id = treeModel.Id as FieldId;
            name = treeModel.Name;
            var fieldDetailsMaybe = clientEndpoint.GetView<FieldDetailsView>(id);

            if (fieldDetailsMaybe.HasValue)
            {
                var details = fieldDetailsMaybe.Value;
                var area = details.ReportedArea.HasValue ? details.ReportedArea : null;
                area = details.BoundaryArea.HasValue && area.HasValue == false ? details.BoundaryArea : area;
                baseArea = area.HasValue ? UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(area.Value, null) : null;
                selectedArea = area.HasValue ? UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(area.Value, null) : null;
            }

            this.mapData = mapData;
            shapePreview = WpfTransforms.CreatePathFromMapData(new[] { mapData }, 55);
        }

        public FieldId Id {
            get { return id; }
        }

        public string Name {
            get { return name; }
            set {
                name = value;
                RaisePropertyChanged("Name");
            }
        }

        public Measure SelectedArea {
            get { return selectedArea; }
            set {
                selectedArea = value;
                RaisePropertyChanged("SelectedArea");
            }
        }

        public Path ShapePreview {
            get { return shapePreview; }
        }

        public string MapData {
            get { return mapData; }
        }

        public FieldSelectionEditViewModel CreateEditModel() {
            return new FieldSelectionEditViewModel(Id, SelectedArea, ShapePreview, Name, clientEndpoint);
        }
    }
}
