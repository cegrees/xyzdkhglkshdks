﻿using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Company;
using Landdb.Domain.ReadModels.Person;
using Landdb.Infrastructure;
using Landdb.ViewModel.Secondary.Reports.AppliedProduct;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Landdb.ViewModel.Secondary.Reports.Crop_History
{

    public class CropHistoryInfoViewModel
    {
        IClientEndpoint endpoint;
        Dispatcher dispatcher;
        EntityItem selectedEntity;

        public CropHistoryInfoViewModel(IClientEndpoint endpoint, Dispatcher dispatcher)
        {
            this.endpoint = endpoint;
            this.dispatcher = dispatcher;
            InitializeData();
        }

        public ObservableCollection<CropYearItem> AvailableCropYears { get; set; }

        public List<EntityItem> Entities { get; set; }
        public EntityItem Entity
        {
            get { return selectedEntity; }
            set
            {
                selectedEntity = value;

                //save selected item to user settings
                var settings = endpoint.GetUserSettings();
                settings.LastReportHeaderEntityId = selectedEntity.Id;
                endpoint.SaveUserSettings();
            }
        }

        void InitializeData()
        {
            var cropYears = (from year in ApplicationEnvironment.CurrentDataSource.AvailableYears.ToList()
                            select new CropYearItem { IsChecked = false, Year = year });
            AvailableCropYears = new ObservableCollection<CropYearItem>(cropYears.OrderByDescending(x => x.Year));

            DataSourceId dsID = new DataSourceId(ApplicationEnvironment.CurrentDataSourceId);
            var companiesMaybe = endpoint.GetView<CompanyListView>(dsID);
            var peopleMaybe = endpoint.GetView<PersonListView>(dsID);
            Entities = new List<EntityItem>();
            if (companiesMaybe.HasValue)
            {
                var entities = from c in companiesMaybe.Value.Companies
                               where !c.RemovedCropYear.HasValue || !(c.RemovedCropYear.Value <= ApplicationEnvironment.CurrentCropYear)
                               select new EntityItem() { Name = c.Name, Id = c.Id.Id, Type = "Company" };
                Entities.AddRange(entities);
            }

            if (peopleMaybe.HasValue)
            {
                var peopleEntities = from p in peopleMaybe.Value.Persons
                                     where !p.RemovedCropYear.HasValue || !(p.RemovedCropYear.Value <= ApplicationEnvironment.CurrentCropYear)
                                     select new EntityItem() { Name = p.Name, Id = p.Id.Id, Type = "Person" };
                Entities.AddRange(peopleEntities);
            }

            Entities = Entities.OrderBy(n => n.Name).ToList();

            //initialize to previously used entity...
            var settings = endpoint.GetUserSettings();
            if (settings.LastReportHeaderEntityId != null)
            {
                selectedEntity = Entities.SingleOrDefault(x => x.Id == settings.LastReportHeaderEntityId);
            }
            //////////////////////////////////////////
        }
    }

    public class CropYearItem
    {
        public bool IsChecked { get; set; }
        public int Year { get; set; }
    }
}
