﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Converters.Reports;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Fields;
using Landdb.ViewModel.Reports;
using Landdb.ViewModel.Secondary.Reports.Crop_History.Generators;
using Microsoft.Win32;
using Newtonsoft.Json;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using System.Windows.Threading;
using Telerik.Reporting;
using Telerik.Reporting.Processing;

namespace Landdb.ViewModel.Secondary.Reports.Crop_History
{
    public class CropHistoryReportCreatorViewModel : ViewModelBase, Updatable
    {
        IClientEndpoint endPoint;
        ReportTemplateViewModel template;
        ReportTemplateViewModelToFile templateService = new ReportTemplateViewModelToFile();
        string reportType;
        CropHistoryQueryCriteria criteria;
        ICropHistoryReportGenerator selectedReportGenerator;

        public CropHistoryReportCreatorViewModel(IClientEndpoint endpoint, Dispatcher dispatcher, ReportTemplateViewModel template, string reportType)
        {
            this.endPoint = endpoint;
            CancelCommand = new RelayCommand(OnCancelReport);
            SaveCommand = new RelayCommand(SaveTemplate);
            var cropYear = ApplicationEnvironment.CurrentCropYear;

            FieldsPageModel = new CropHistoryFieldSelectionViewModel(endPoint, dispatcher, cropYear);
            InfoPageModel = new CropHistoryInfoViewModel(endPoint, dispatcher);

            ReportGenerators = new ObservableCollection<ICropHistoryReportGenerator>();
            //Call to add all the Applied Product Reports
            AddReportGenerators(endPoint, cropYear);

            SelectedReportGenerator = ReportGenerators.FirstOrDefault();
            PDFExport = new RelayCommand(this.PDF);

            criteria = BuildCriteria();
            ReportSource = SelectedReportGenerator.Generate(criteria);
            this.reportType = reportType;
            //take passed in template and set values on associated ViewModels along with Criteria page
            if (template != null) { InitializeFromTemplate(template); this.template = template; };

            SelectedPageIndex = 0;
        }

        public ICommand CancelCommand { get; private set; }
        public RelayCommand PDFExport { get; set; }
        public ICommand SaveCommand { get; set; }

        public int SelectedPageIndex { get; set; }
        public ReportSource ReportSource { get; set; }

        public string TemplateName { get; set; }

        public CropHistoryFieldSelectionViewModel FieldsPageModel { get; private set; }
        public CropHistoryInfoViewModel InfoPageModel { get; private set; }

        public ObservableCollection<ICropHistoryReportGenerator> ReportGenerators { get; private set; }
        public ICropHistoryReportGenerator SelectedReportGenerator
        {
            get { return selectedReportGenerator; }
            set
            {
                selectedReportGenerator = value;
                criteria = BuildCriteria();
                ReportSource = SelectedReportGenerator.Generate(criteria);
                RaisePropertyChanged("SelectedReportGenerator");
            }
        }

        public string CropYear { get { return ApplicationEnvironment.CurrentCropYear.ToString(); } }

        void OnCancelReport()
        {
            Messenger.Default.Send<ShowMainViewMessage>(new ShowMainViewMessage());
        }

        public void Update()
        {
            if (SelectedReportGenerator != null)
            {
                SelectedReportGenerator = ReportGenerators.FirstOrDefault();
                criteria = BuildCriteria();
                ReportSource = SelectedReportGenerator.Generate(criteria); 
                RaisePropertyChanged("ReportSource");
            }
        }

        CropHistoryQueryCriteria BuildCriteria()
        {
            CropHistoryQueryCriteria criteria = new CropHistoryQueryCriteria();

            criteria.SelectedFields.AddRange(FieldsPageModel.SelectedFields.Select(i=>i.Id));
            var years = InfoPageModel.AvailableCropYears.Where(x => x.IsChecked == true).Select(i => i.Year);
            criteria.SelectedCropYears.AddRange(years);
            return criteria;
        }

        void AddReportGenerators(IClientEndpoint clientEndpoint, int cropYear)
        {
            ReportGenerators.Add(new CropHistoryReportGenerator(endPoint));
            ReportGenerators.Add(new CropHistoryExcelDumpGenerator(endPoint));
        }

        public void PDF()
        {
            try { 
            if (ReportSource == null) return;
            ReportSource reportExport = ReportSource;

            ReportProcessor reportProcessor = new ReportProcessor();
            System.Collections.Hashtable deviceInfo =
            new System.Collections.Hashtable();
            RenderingResult result = reportProcessor.RenderReport("PDF", reportExport, deviceInfo);

            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "Adobe Reader|*.pdf";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;

            Nullable<bool> resultSaved = saveFileDialog1.ShowDialog();

            // Process save file dialog box results 
            if (resultSaved == true)
            {
                // Save document 
                string filename = saveFileDialog1.FileName;

                try
                {
                    using (System.IO.FileStream fs = new System.IO.FileStream(filename, System.IO.FileMode.Create))
                    {
                        fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
                    }

                    System.Diagnostics.Process.Start(filename);
                }
                catch (Exception ex) { }

            }
            }
            catch (Exception ex)
            {
                //do nothing
            }
        }

        void SaveTemplate()
        {
            criteria = BuildCriteria();
            if (template != null && template.TemplateName == TemplateName)
            {
                ReportTemplateViewModel editedTemplate = new ReportTemplateViewModel()
                {
                    TemplateName = TemplateName,
                    Created = template.Created,
                    LastUsed = DateTime.Now,
                    CriteriaJSON = JsonConvert.SerializeObject(criteria),
                    ReportType = template.ReportType,
                    DataSourceId = template.DataSourceId
                };

                //TODO :: CALL TO EDIT PREVIOUS TEMPLATE...
                templateService.EditTemplate(editedTemplate);
            }
            else
            {
                ReportTemplateViewModel newTemplate = new ReportTemplateViewModel()
                {
                    TemplateName = TemplateName,
                    Created = DateTime.Now,
                    LastUsed = DateTime.Now,
                    CriteriaJSON = JsonConvert.SerializeObject(criteria),
                    ReportType = reportType,
                    DataSourceId = ApplicationEnvironment.CurrentDataSourceId
                };

                //TODO :: call service to save the new report template
                templateService.SaveNewTemplate(newTemplate);
                template = newTemplate;
            }

            RaisePropertyChanged("NewTemplateCreated", null, new ReportTemplateViewModel(), true);
        }

        void InitializeFromTemplate(ReportTemplateViewModel temp)
        {
            TemplateName = temp.TemplateName;

            CropHistoryQueryCriteria crit = JsonConvert.DeserializeObject<CropHistoryQueryCriteria>(temp.CriteriaJSON);

            InfoPageModel.AvailableCropYears.Where(item => crit.SelectedCropYears.Contains(item.Year)).ForEach(x => x.IsChecked = true);
            InfoPageModel.Entity = crit.SelectedEntity;

            var dsID = new DataSourceId(ApplicationEnvironment.CurrentDataSourceId);

            var RootTreeItemModels = FieldsPageModel.RootTreeItemModels;
            if (RootTreeItemModels.Any())
            {
                if (RootTreeItemModels.First() is GrowerTreeItemViewModel)
                {
                    var toCheck = from farm in RootTreeItemModels.First().Children
                                  from field in farm.Children
                                  where crit.SelectedFields.Contains(field.Id)
                                  select field;
                    toCheck.ForEach(x => x.IsChecked = true);
                }
                else
                {
                    var toCheck = from crop in RootTreeItemModels.First().Children
                                  from farm in crop.Children
                                  from field in farm.Children
                                  where crit.SelectedFields.Contains(field.Id)
                                  select field;
                    toCheck.ForEach(x => x.IsChecked = true);
                }
            }

        }
    }
}
