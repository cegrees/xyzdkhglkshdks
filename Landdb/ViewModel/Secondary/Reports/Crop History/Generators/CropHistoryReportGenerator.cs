﻿using AgC.UnitConversion;
using Landdb.Client.Infrastructure;
using Landdb.Client.Services.Fertilizer;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Domain.ReadModels.Yield;
using Landdb.Domain.ReadModels.YieldLocation;
using Landdb.Infrastructure;
using Landdb.ReportModels.Crop_History;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Landdb.Resources;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.Crop_History.Generators {
	public class CropHistoryReportGenerator : ICropHistoryReportGenerator {
		IClientEndpoint clientEndpoint;

		public CropHistoryReportGenerator(IClientEndpoint clientEndpoint) {
			this.clientEndpoint = clientEndpoint;
		}

		public Telerik.Reporting.ReportSource Generate(CropHistoryQueryCriteria criteria) {
			ReportBook book = new ReportBook();
			var rs = new InstanceReportSource();
			List<CropHistoryData> cropHistories = new List<CropHistoryData>();
			var masterlist = clientEndpoint.GetMasterlistService();

			if (criteria.SelectedCropYears.Any() && criteria.SelectedFields.Any()) {
				foreach (var cropYear in criteria.SelectedCropYears) {
					var cropYearId = new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear);

					var flattenedHierarchy = clientEndpoint.GetView<FlattenedTreeHierarchyView>(cropYearId).GetValue(new FlattenedTreeHierarchyView());
					var applicationData = clientEndpoint.GetView<CropZoneApplicationDataView>(cropYearId).GetValue(new CropZoneApplicationDataView()).Items;
					var fertService = new FertilizerAccumulator(clientEndpoint, cropYearId.DataSourceId, cropYear);
					var cropZoneLoadData = clientEndpoint.GetView<CropZoneLoadDataView>(cropYearId).GetValue(() => new CropZoneLoadDataView()).Items;
					var yieldLocations = clientEndpoint.GetView<YieldLocationListView>(new DataSourceId(cropYearId.DataSourceId)).GetValue(() => new YieldLocationListView()).YieldLocations;

					var locationsExcludedFromInventory = from loc in yieldLocations
														 where loc.IsActiveInCropYear(cropYearId.Id) && loc.IsQuantityExcludedFromInventory
														 select loc.Id.Id;
                    var selectedFields = criteria.SelectedFields.Distinct(x => x.Id).ToList();
					foreach (var field in selectedFields) {
						var flattenedItems = flattenedHierarchy.Items.Where(x => x.FieldId == field);
						var deletedItems = flattenedHierarchy.DeletedItems.Where(x => x.FieldId == field);

						var fieldDetails = clientEndpoint.GetView<FieldDetailsView>(field).Value;
						foreach (var item in flattenedItems) {
							if (deletedItems.Contains(item)) {

							} else {
								FertilizerFormulationModel totalFertModel = new FertilizerFormulationModel();
								var cropHistoryItem = new CropHistoryData();

								if (item.CropZoneId != null) {
									var czDetails = clientEndpoint.GetView<CropZoneDetailsView>(item.CropZoneId).Value;

									var area = czDetails.BoundaryArea.HasValue ? czDetails.BoundaryArea : null;
									area = czDetails.ReportedArea.HasValue ? czDetails.ReportedArea : area;

									Task averageYieldTask = Task.Factory.StartNew(() => {
										var czLoads = from load in cropZoneLoadData
													  from loc in load.DestinationLocations
													  where load.CropZoneId == item.CropZoneId
													   && !locationsExcludedFromInventory.Contains(loc.Id.Id)
													  select load;

										if (czLoads.Any()) {
											decimal totalQuantity = 0m;
											CompositeUnit setUnit = null;
											foreach (var yieldItem in czLoads) {
												//var qty = yieldItem.WeightedFinalQuantity;
												//var unit = yieldItem.FinalQuantityUnit;

												if (setUnit == null) {
													setUnit = yieldItem.FinalQuantityUnit;
													totalQuantity += yieldItem.AreaWeightedFinalQuantity;
												} else {
													if (yieldItem.FinalQuantityUnit == setUnit) {
														totalQuantity += yieldItem.AreaWeightedFinalQuantity;
													} else if (CompositeUnitConverter.CanConvert(yieldItem.FinalQuantityUnit, setUnit)) {
														var convertedValue = CompositeUnitConverter.ConvertValue(yieldItem.AreaWeightedFinalQuantity, yieldItem.FinalQuantityUnit, setUnit);
														totalQuantity += convertedValue;
													}
												}
											}
											var areaVal = area.HasValue ? area.Value : 0.0;
											double avgYieldVal = 0.0;
											if (areaVal != 0) {
												avgYieldVal = (double)totalQuantity / areaVal;
											} else {
												avgYieldVal = 0.0;
											}

											if (setUnit != null) {
												cropHistoryItem.AvgYieldDisplay = string.Format("{0} {1}", avgYieldVal.ToString("N2"), setUnit.AbbreviatedDisplay);
											}
										}
									});

									var unit = !string.IsNullOrEmpty(czDetails.BoundaryAreaUnit) ? czDetails.BoundaryAreaUnit : czDetails.ReportedAreaUnit;
									var areaMeasure = area.HasValue ? UnitFactory.GetUnitByName(unit).GetMeasure(area.Value, null) : null;

									var czAppData = applicationData.Where(czId => czId.CropZoneId == item.CropZoneId);

									Task totalFertTask = Task.Factory.StartNew(async () => {
										totalFertModel = await fertService.CalculateFertilizerUsage(czAppData.ToList(), areaMeasure);
									});

									var plantingApps = (from apps in applicationData
														where masterlist.GetProduct(apps.ProductId).ProductType.ToLower() == "seed" && apps.CropZoneId == item.CropZoneId
														select apps).OrderBy(x => x.StartDate);

									var varieties = string.Join(" + ", plantingApps.Select(x => masterlist.GetProduct(x.ProductId).Name));
									var dates = string.Join(" + ", plantingApps.Select(x => x.StartDate.ToLocalTime().ToShortDateString()));

									cropHistoryItem.Area = area.HasValue ? (decimal)area.Value : 0m;
									cropHistoryItem.Variety = varieties;
									cropHistoryItem.PlantingDate = dates;
									cropHistoryItem.Tillage = czDetails.Tillage;
									totalFertTask.Wait();
									cropHistoryItem.FertSummary = totalFertModel.ToString();
									averageYieldTask.Wait();
								} else {

									var fieldArea = fieldDetails.BoundaryArea.HasValue ? fieldDetails.BoundaryArea.Value : 0.0;
									fieldArea = fieldDetails.ReportedArea.HasValue ? fieldDetails.ReportedArea.Value : fieldArea;
									cropHistoryItem.Area = (decimal)fieldArea;
								}

								cropHistoryItem.FarmId = item.FarmId.Id;
								cropHistoryItem.FarmName = item.FarmName;
								cropHistoryItem.FieldId = item.FieldId.Id;
                                cropHistoryItem.FieldName = fieldDetails.FieldNameByCropYear[cropYear];//.Name;
								cropHistoryItem.Crop = masterlist.GetCropDisplay(item.CropId);
								cropHistoryItem.CropZoneName = item.CropZoneName;
								cropHistoryItem.CropYear = cropYear;
                                cropHistoryItem.Township = !string.IsNullOrEmpty(fieldDetails.Township) || !string.IsNullOrEmpty(fieldDetails.TownshipDirection) ? string.Format("{0} {1} {2}",Landdb.Resources.Strings.Township_Text, fieldDetails.Township, fieldDetails.TownshipDirection) : "";
                                cropHistoryItem.Section = !string.IsNullOrEmpty(fieldDetails.Section) ? string.Format("{0} {1}", Landdb.Resources.Strings.Section_Text, fieldDetails.Section) : "";
                                cropHistoryItem.Range = !string.IsNullOrEmpty(fieldDetails.Range) || !string.IsNullOrEmpty(fieldDetails.RangeDirection) ? string.Format("{0} {1} {2}", Landdb.Resources.Strings.Range_Text, fieldDetails.Range, fieldDetails.RangeDirection) : "";

                                string selectedCropYearRange = string.Empty;
							    criteria.SelectedCropYears.ForEach(selectedYear =>
							    {
							        if (selectedCropYearRange.Equals(string.Empty))
							        {
							            selectedCropYearRange = selectedYear.ToString();
							        }
							        else
							        {
							            selectedCropYearRange += $", {selectedYear.ToString()}";
							        }
							    });
							    cropHistoryItem.DateRangeDisplay = selectedCropYearRange;

                                cropHistories.Add(cropHistoryItem);
							}
						}
					}
				}
			}

			var orderedCropHistories = cropHistories.OrderBy(farm => farm.FarmName).ThenBy(i => i.FieldName).ThenByDescending(y => y.CropYear).ThenBy(c => c.CropZoneName);

			var report = new Landdb.Views.Secondary.Reports.Crop_History.CropHistory();
			report.DataSource = orderedCropHistories;
			report.Name = Strings.ReportName_CropHistoryReport_Text;

			if (cropHistories.Any()) {
				book.Reports.Add(report);
			}

			rs.ReportDocument = book;
			return rs;
		}

		public string DisplayName {
			get { return Strings.ReportName_CropHistoryReport_Text; }
		}
	}
}
