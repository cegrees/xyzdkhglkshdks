﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.Crop_History.Generators
{
    public interface ICropHistoryReportGenerator
    {
        ReportSource Generate(CropHistoryQueryCriteria criteria);
        string DisplayName { get; }
    }
}
