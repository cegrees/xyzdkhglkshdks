﻿using AgC.UnitConversion;
using ExportToExcel;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Client.Services.Fertilizer;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Domain.ReadModels.Yield;
using Landdb.Domain.ReadModels.YieldLocation;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ReportModels.Crop_History;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Landdb.Resources;

namespace Landdb.ViewModel.Secondary.Reports.Crop_History.Generators {
	public class CropHistoryExcelDumpGenerator : ICropHistoryReportGenerator {
		IClientEndpoint clientEndpoint;

		public CropHistoryExcelDumpGenerator(IClientEndpoint clientEndpoint) {
			this.clientEndpoint = clientEndpoint;
		}

		public Telerik.Reporting.ReportSource Generate(CropHistoryQueryCriteria criteria) {
			//ReportBook book = new ReportBook();
			//var rs = new InstanceReportSource();
			List<CropHistoryData> cropHistories = new List<CropHistoryData>();
			var masterlist = clientEndpoint.GetMasterlistService();

			if (criteria.SelectedCropYears.Any() && criteria.SelectedFields.Any()) {
				foreach (var cropYear in criteria.SelectedCropYears) {
					var cropYearId = new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear);

					var flattenedHierarchy = clientEndpoint.GetView<FlattenedTreeHierarchyView>(cropYearId).GetValue(new FlattenedTreeHierarchyView());
					var applicationData = clientEndpoint.GetView<CropZoneApplicationDataView>(cropYearId).GetValue(new CropZoneApplicationDataView()).Items;
					var fertService = new FertilizerAccumulator(clientEndpoint, cropYearId.DataSourceId, cropYear);
					var cropZoneLoadData = clientEndpoint.GetView<CropZoneLoadDataView>(cropYearId).GetValue(new CropZoneLoadDataView()).Items;
					var yieldLocations = clientEndpoint.GetView<YieldLocationListView>(new DataSourceId(cropYearId.DataSourceId)).GetValue(() => new YieldLocationListView()).YieldLocations;

					var locationsExcludedFromInventory = from loc in yieldLocations
														 where loc.IsActiveInCropYear(cropYearId.Id) && loc.IsQuantityExcludedFromInventory
														 select loc.Id.Id;

					foreach (var field in criteria.SelectedFields) {
						var fieldDetails = clientEndpoint.GetView<FieldDetailsView>(field).Value;
						var flattenedItems = flattenedHierarchy.Items.Where(x => x.FieldId == field);
						foreach (var item in flattenedItems) {
							FertilizerFormulationModel totalFertModel = new FertilizerFormulationModel();
							var cropHistoryItem = new CropHistoryData();

							if (item.CropZoneId != null) {
								var czDetails = clientEndpoint.GetView<CropZoneDetailsView>(item.CropZoneId).Value;
								var area = czDetails.BoundaryArea.HasValue ? czDetails.BoundaryArea : null;
								area = czDetails.ReportedArea.HasValue ? czDetails.ReportedArea : area;

								Task averageYieldTask = Task.Factory.StartNew(() => {
									var czLoads = from load in cropZoneLoadData
												  from loc in load.DestinationLocations
												  where load.CropZoneId == item.CropZoneId
													&& !locationsExcludedFromInventory.Contains(loc.Id.Id)
												  select load;

									if (czLoads.Any()) {
										decimal totalQuantity = 0m;
										CompositeUnit setUnit = null;
										foreach (var yieldItem in czLoads) {
											//var qty = yieldItem.WeightedFinalQuantity;
											//var unit = yieldItem.FinalQuantityUnit;

											if (setUnit == null) {
												setUnit = yieldItem.FinalQuantityUnit;
												totalQuantity += yieldItem.AreaWeightedFinalQuantity;
											} else {
												if (yieldItem.FinalQuantityUnit == setUnit) {
													totalQuantity += yieldItem.AreaWeightedFinalQuantity;
												} else if (CompositeUnitConverter.CanConvert(yieldItem.FinalQuantityUnit, setUnit)) {
													var convertedValue = CompositeUnitConverter.ConvertValue(yieldItem.AreaWeightedFinalQuantity, yieldItem.FinalQuantityUnit, setUnit);
													totalQuantity += convertedValue;
												}
											}
										}
										var areaVal = area.HasValue ? area.Value : 0.0;
										double avgYieldVal = 0.0;
										if (areaVal != 0) {
											avgYieldVal = (double)totalQuantity / areaVal;
										} else {
											avgYieldVal = 0.0;
										}

										if (setUnit != null) {
											cropHistoryItem.AvgYieldDisplay = string.Format("{0} {1}", avgYieldVal.ToString("N2"), setUnit.AbbreviatedDisplay);
										}
									}
								});

								var unit = !string.IsNullOrEmpty(czDetails.BoundaryAreaUnit) ? czDetails.BoundaryAreaUnit : czDetails.ReportedAreaUnit;
								var areaMeasure = area.HasValue ? UnitFactory.GetUnitByName(unit).GetMeasure(area.Value, null) : null;

								var czAppData = applicationData.Where(czId => czId.CropZoneId == item.CropZoneId);

								Task totalFertTask = Task.Factory.StartNew(async () => {
									totalFertModel = await fertService.CalculateFertilizerUsage(czAppData.ToList(), areaMeasure);
								});

								var plantingApps = (from apps in applicationData
													where masterlist.GetProduct(apps.ProductId).ProductType.ToLower() == "seed" && apps.CropZoneId == item.CropZoneId
													select apps).OrderBy(x => x.StartDate);
								;

								var varieties = string.Join(" + ", plantingApps.Select(x => masterlist.GetProduct(x.ProductId).Name));
								var dates = string.Join(" + ", plantingApps.Select(x => x.StartDate.ToLocalTime().ToShortDateString()));

								cropHistoryItem.Area = area.HasValue ? (decimal)area.Value : 0m;
								cropHistoryItem.Variety = varieties;
								cropHistoryItem.PlantingDate = dates;
								cropHistoryItem.Tillage = czDetails.Tillage;
								totalFertTask.Wait();
								cropHistoryItem.FertSummary = totalFertModel.ToString();
								if (totalFertModel != null) {
									cropHistoryItem.N = totalFertModel.N;
									cropHistoryItem.P = totalFertModel.P;
									cropHistoryItem.K = totalFertModel.K;
									cropHistoryItem.Mg = totalFertModel.Mg;
									cropHistoryItem.Mn = totalFertModel.Mn;
									cropHistoryItem.Mo = totalFertModel.Mo;
									cropHistoryItem.S = totalFertModel.S;
									cropHistoryItem.B = totalFertModel.B;
									cropHistoryItem.Ca = totalFertModel.Ca;
									cropHistoryItem.Cl = totalFertModel.Cl;
									cropHistoryItem.Cu = totalFertModel.Cu;
									cropHistoryItem.Fe = totalFertModel.Fe;
									cropHistoryItem.Zn = totalFertModel.Zn;
								}

							} else {

								var fieldArea = fieldDetails.BoundaryArea.HasValue ? fieldDetails.BoundaryArea.Value : 0.0;
								fieldArea = fieldDetails.ReportedArea.HasValue ? fieldDetails.ReportedArea.Value : fieldArea;
								cropHistoryItem.Area = (decimal)fieldArea;
							}

							cropHistoryItem.FarmId = item.FarmId.Id;
							cropHistoryItem.FarmName = item.FarmName;
							cropHistoryItem.FieldId = item.FieldId.Id;
                            cropHistoryItem.FieldName = fieldDetails.FieldNameByCropYear[cropYear]; //.Name;
							cropHistoryItem.Crop = masterlist.GetCropDisplay(item.CropId);
							cropHistoryItem.CropZoneName = item.CropZoneName;
							cropHistoryItem.CropYear = cropYear;
                            cropHistoryItem.Township = string.Format("{0} {1}", fieldDetails.Township, fieldDetails.TownshipDirection);
                            cropHistoryItem.Section = string.Format("{0}", fieldDetails.Section);
                            cropHistoryItem.Range = string.Format("{0} {1}", fieldDetails.Range, fieldDetails.RangeDirection);

                            cropHistories.Add(cropHistoryItem);
						}
					}
				}
			}

			var orderedCropHistories = cropHistories.OrderBy(farm => farm.FarmName).ThenBy(i => i.FieldName).ThenByDescending(y => y.CropYear).ThenBy(c => c.CropZoneName);

			DataTable cropHistoryTable = new DataTable();

			cropHistoryTable = ExportToExcel.CreateExcelFile.ListToDataTable(orderedCropHistories.ToList());
			cropHistoryTable.TableName = Strings.TableName_CropHistory_Text;
			DataSet ds = new DataSet();
			ds.Tables.Add(cropHistoryTable);

			try {
				//open up file dialog to save file....
				//then call createexcelfile to create the excel...
				string filename = string.Empty;
				SaveFileDialog saveFileDialog1 = new SaveFileDialog();

				saveFileDialog1.Filter = "Excel|*.xlsx";
				saveFileDialog1.FilterIndex = 2;
				saveFileDialog1.RestoreDirectory = true;

				Nullable<bool> resultSaved = saveFileDialog1.ShowDialog();

				// Process save file dialog box results 
				if (resultSaved == true) {
					// Save document 
					filename = saveFileDialog1.FileName;
					CreateExcelFile.CreateExcelDocument(ds, filename);
				}

				//now open file....
				System.Diagnostics.Process.Start(filename);

				Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
			} catch (Exception ex) {
				Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
			}
			return null;
		}

		public string DisplayName {
			get { return Strings.GeneratorName_CropHistoryExcel_Text; }
		}
	}
}
