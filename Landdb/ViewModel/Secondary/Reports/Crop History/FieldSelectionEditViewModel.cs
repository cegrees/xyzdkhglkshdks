﻿using AgC.UnitConversion;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Shapes;

namespace Landdb.ViewModel.Secondary.Reports.Crop_History
{
    public class FieldSelectionEditViewModel : ViewModelBase
    {
        double percent;
        double customArea;
        IUnit customUnit;
        IClientEndpoint endPoint;
        FieldId fieldId;

        public FieldSelectionEditViewModel(FieldId fieldId, Measure currentArea, Path shapePreview, string name, IClientEndpoint clientEndpoint)
        {
            Name = string.Format("{0}", name);
            ShapePreview = shapePreview;
            endPoint = clientEndpoint;
            this.fieldId = fieldId;
            var model = clientEndpoint.GetView<FieldDetailsView>(fieldId).GetValue(new FieldDetailsView());

            if (model.BoundaryArea != null && !string.IsNullOrWhiteSpace(model.BoundaryAreaUnit))
            {
                BoundaryArea = UnitFactory.GetUnitByName(model.BoundaryAreaUnit).GetMeasure((double)model.BoundaryArea.Value);
            }
            if (model.ReportedArea != null && !string.IsNullOrWhiteSpace(model.ReportedAreaUnit))
            {
                ReportedArea = UnitFactory.GetUnitByName(model.ReportedAreaUnit).GetMeasure((double)model.ReportedArea.Value);
            }
            if (model.FsaArea != null && !string.IsNullOrWhiteSpace(model.FsaAreaUnit))
            {
                FsaArea = UnitFactory.GetUnitByName(model.FsaAreaUnit).GetMeasure((double)model.FsaArea.Value);
            }

            var customMeasure = currentArea ?? ReportedArea ?? BoundaryArea ?? FsaArea ?? UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(0);
            CustomUnit = customMeasure.Unit;
            CustomArea = customMeasure.Value;
            var area = ReportedArea != null ? ReportedArea : BoundaryArea;
            Percent = (CustomArea / area.Value);


        }

        public RelayCommand CancelCommand { get; private set; }
        public RelayCommand UseBoundaryAreaCommand { get; private set; }
        public RelayCommand UseReportedAreaCommand { get; private set; }
        public RelayCommand UseFsaAreaCommand { get; private set; }
        public RelayCommand UseCustomAreaCommand { get; private set; }
        public ICommand SetFieldID { get; private set; }
        public ICommand Cancel { get; private set; }

        public string Name { get; private set; }

        public Path ShapePreview { get; set; }

        public Measure BoundaryArea { get; set; }
        public Measure ReportedArea { get; set; }
        public Measure FsaArea { get; set; }

        public bool HasBoundaryArea { get { return BoundaryArea != null; } }
        public bool HasReportedArea { get { return ReportedArea != null; } }
        public bool HasFsaArea { get { return FsaArea != null; } }
        public double Percent { get { return percent; } set { percent = value; RaisePropertyChanged("Percent"); } }
        public double CustomArea
        {
            get { return customArea; }
            set
            {
                customArea = value;

                var area = ReportedArea != null ? ReportedArea : BoundaryArea;
                Percent = (customArea / area.Value);

                RaisePropertyChanged("CustomArea");
            }
        }

        public IUnit CustomUnit
        {
            get { return customUnit; }
            set
            {
                customUnit = value;
                RaisePropertyChanged("CustomUnit");
            }
        }

        void cancel()
        {
            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
        }

    }
}
