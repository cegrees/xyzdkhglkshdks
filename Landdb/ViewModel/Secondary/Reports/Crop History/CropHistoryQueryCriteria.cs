﻿using Landdb.ViewModel.Secondary.Reports.AppliedProduct;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Secondary.Reports.Crop_History
{
    public class CropHistoryQueryCriteria
    {
        public CropHistoryQueryCriteria()
        {
            SelectedCropYears = new List<int>();
            SelectedFields = new List<FieldId>();
        }

        public List<FieldId> SelectedFields { get; set; }
        public List<int> SelectedCropYears { get; set; }
        public EntityItem SelectedEntity { get; set; }
    }
}
