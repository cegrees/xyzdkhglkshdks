﻿using AgC.UnitConversion;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using Landdb.Client.Spatial;
using Landdb.Domain.ReadModels.Map;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.ViewModel.Fields;
using Landdb.ViewModel.Shared;
using Microsoft.Maps.MapControl.WPF;
using NLog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Secondary.Reports.Crop_History
{
    public class CropHistoryFieldSelectionViewModel: ViewModelBase 
    {
        protected readonly Dispatcher dispatcher;
        protected readonly IClientEndpoint clientEndpoint;
        protected int cropYear;
        protected Logger log = LogManager.GetCurrentClassLogger();
        protected Measure totalArea = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(0);
        TreeDisplayModeDisplayItem selectedTreeDisplayMode;
        CropZoneSelectionEditMode editMode;
        bool isEditPopupOpen = false;
        FieldSelectionViewModel selected;
        string errorMessage;
        string totalAreaText;
        ObservableCollection<FieldSelectionViewModel> selectedFields;
        bool isFilterVisible = false;
        string filterHeader = string.Empty;

        public event EventHandler MapUpdated;
        public EventArgs e = null;

        public CropHistoryFieldSelectionViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher) {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            Enable = false;
            editMode = CropZoneSelectionEditMode.Application;
            SelectedFields = new ObservableCollection<FieldSelectionViewModel>();

            ApplyFilterCommand = new RelayCommand(ApplyFilter);
            ClearFilterCommand = new RelayCommand(ClearFilter);

            FilterModel = new TreeFilterModel(clientEndpoint);

            selectedTreeDisplayMode = new TreeDisplayModeDisplayItem("Farms");
        }

        public CropHistoryFieldSelectionViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, int cropYear) {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            this.cropYear = cropYear;
            Enable = false;
            editMode = CropZoneSelectionEditMode.Application;
            SelectedFields = new ObservableCollection<FieldSelectionViewModel>();

            ApplyFilterCommand = new RelayCommand(ApplyFilter);
            ClearFilterCommand = new RelayCommand(ClearFilter);

            FilterModel = new TreeFilterModel(clientEndpoint);

            selectedTreeDisplayMode = new TreeDisplayModeDisplayItem("Farms");
            BuildTreeList();

        }

        public RelayCommand ApplyFilterCommand { get; private set; }
        public RelayCommand ClearFilterCommand { get; private set; }

        public IPageFilterViewModel FilterModel { get; protected set; }

        public bool Enable { get; set; }

        public ObservableCollection<AbstractTreeItemViewModel> RootTreeItemModels { get; private set; }
        public CollectionView RootTreeItemModelsView { get; private set; }

        public ObservableCollection<FieldSelectionViewModel> SelectedFields
        {
            get { return selectedFields; }
            set
            {
                selectedFields = value;
            }
        }

        public FieldSelectionViewModel SelectedField
        {
            get
            {
                return selected;
            }
            set
            {
                selected = value;
            }
        }

        public MapLayer BingMapsLayer { get; private set; }

        public string MapCulture
        {
            get { return Infrastructure.ApplicationEnvironment.BingWpfMapCulture; }
        }

        public bool IsEditPopupOpen {
            get { return isEditPopupOpen; }
            set {
                isEditPopupOpen = value;
                RaisePropertyChanged("IsEditPopupOpen");
            }
        }

        public Measure TotalArea {
            get { return totalArea; }
            set {
                var oldValue = totalArea;
                totalArea = value;
                //TotalAreaText = totalArea.Value.ToString();

                if (TotalAreaText == null || TotalAreaText == string.Empty || TotalAreaText != totalArea.Value.ToString("N2")) { totalAreaText = totalArea.Value.ToString("N2"); RaisePropertyChanged("TotalAreaText"); }
                RaisePropertyChanged("TotalArea");
            }
        }

        public string TotalAreaText {
            get { return totalAreaText; }
            set {
                if (string.IsNullOrWhiteSpace(value)) { return; }
                var oldArea = TotalArea.Value;
                if (totalAreaText != value && TotalArea != null)
                {
                    totalAreaText = value;
                }
                else
                {
                    totalAreaText = "0";
                }
            }
        }

        public int FieldCount {
            get { return SelectedFields.Count; }
        }

        public List<TreeDisplayModeDisplayItem> AvailableTreeDisplayModes {
            get {
                return new List<TreeDisplayModeDisplayItem> {
                    new TreeDisplayModeDisplayItem("Farms"),
                    new TreeDisplayModeDisplayItem("Crops")
                };
            }
        }

        public TreeDisplayModeDisplayItem SelectedTreeDisplayMode {
            get { return selectedTreeDisplayMode; }
            set {
                if (value != null)
                    if (!Equals(selectedTreeDisplayMode, value)) {
                    selectedTreeDisplayMode = value;
                    if (RootTreeItemModels != null) {
                        RootTreeItemModels.Clear();
                    }
                    BuildTreeList();
                    RaisePropertyChanged("SelectedTreeDisplayMode");
                }
            }
        }

        public string ErrorMessage {
            get { return errorMessage; }
            set {
                errorMessage = value;
                RaisePropertyChanged("ErrorMessage");
                RaisePropertyChanged("IsErrorMessageVisible");
            }
        }

        public bool IsErrorMessageVisible {
            get { return !string.IsNullOrWhiteSpace(ErrorMessage); }
        }

        void BuildTreeList() {

            //await Task.Run(new Action(() => {
            var begin = DateTime.UtcNow;
            AbstractTreeItemViewModel rootNode = null;
            var settings = clientEndpoint.GetUserSettings();
            bool sortAlphabetically = settings.AreFieldsSortedAlphabetically;

            // TODO: When maps are separate, move this back to the Farms case in the switch below.
            var years = clientEndpoint.GetView<FieldTreeView>(new DataSourceId(ApplicationEnvironment.CurrentDataSourceId));
            var yearItem = (years.HasValue && years.Value.CropYearList.ContainsKey(cropYear)) ? years.Value.CropYearList[cropYear] : new TreeViewYearItem();

            if (SelectedTreeDisplayMode.IsCrops) {
                var tree = clientEndpoint.GetView<CropTreeView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new CropTreeView());
                IList<TreeViewCropItem> crops = tree.Crops ?? new List<TreeViewCropItem>();
                rootNode = new GrowerCropTreeItemViewModel("Home", crops, sortAlphabetically, OnFieldCheckedChanged_internal, null, FilterModel.FilterItem);
            } else { //if it's farms or anything else
                IList<TreeViewFarmItem> farms = yearItem.Farms ?? new List<TreeViewFarmItem>();
                rootNode = new GrowerTreeItemViewModel("Home", farms, sortAlphabetically, OnFieldCheckedChanged_internal, null, FilterModel.FilterItem);
            }

            dispatcher.BeginInvoke(new Action(() => {
                // initialize map
                var mapQ = from m in yearItem.Maps
                           where m.IsVisible
                           select m.MapData;
                BingMapsLayer = BingMapsUtility.GetLayerForMapData(mapQ);
                RaisePropertyChanged("BingMapsLayer");
            }));

            if (rootNode != null) {
                RootTreeItemModels = new ObservableCollection<AbstractTreeItemViewModel>() { rootNode };
                RootTreeItemModels[0].IsExpanded = true;

                App.Current.Dispatcher.BeginInvoke(new Action(() => {
                    RootTreeItemModelsView = (CollectionView)CollectionViewSource.GetDefaultView(RootTreeItemModels);
                    RootTreeItemModelsView.Filter = FilterModel.FilterItem;
                    GenerateFilterHeader();
                    RaisePropertyChanged("RootTreeItemModelsView");
                }));
            }
            RaisePropertyChanged("RootTreeItemModels");

            var end = DateTime.UtcNow;

            log.Debug("Applicator tree load took " + (end - begin).TotalMilliseconds + "ms");
            //}));

        }

        void OnFieldCheckedChanged(FieldTreeItemViewModel treeItem) { }

        private void OnFieldCheckedChanged_internal(AbstractTreeItemViewModel treeItem) {
            if (treeItem is FieldTreeItemViewModel) {
                if (treeItem.IsChecked.HasValue && treeItem.IsChecked.Value && !SelectedFields.Any(x => x.Id == (treeItem.Id as CropZoneId))) {
                    var mapItem = clientEndpoint.GetView<ItemMap>(treeItem.Id);
                    string mapData = null;
                    if (mapItem.HasValue && mapItem.Value.MostRecentMapItem != null) {
                        mapData = mapItem.Value.MostRecentMapItem.MapData;
                    }

                    var cztivm = new FieldSelectionViewModel(treeItem as FieldTreeItemViewModel, mapData, clientEndpoint);

                    SelectedFields.Add(cztivm);

                    dispatcher.BeginInvoke(new Action(() =>
                    {
                        List<string> selectedMapData = new List<string>();
                        foreach (var sCZ in SelectedFields)
                        {
                            if (sCZ.MapData != null)
                            {
                                selectedMapData.Add(sCZ.MapData);
                            }
                        }
                        BingMapsLayer = BingMapsUtility.GetLayerForMapData(selectedMapData.ToArray());
                        RaisePropertyChanged("BingMapsLayer");

                        try
                        {
                            MapUpdated(this, e);
                        }
                        catch(Exception ex) { var error = ex.InnerException; }
                    }));
                    if (cztivm.SelectedArea != null && cztivm.SelectedArea.CanConvertTo(TotalArea.Unit)) {
                        TotalArea += cztivm.SelectedArea;
                    }
                    else {
                        log.Debug("Selected unit {0} is not compatible with total unit {1} in cropzone {2} ", cztivm.SelectedArea != null ? cztivm.SelectedArea.Unit.Name : string.Empty, totalArea.Unit.Name, cztivm.Name);

                    }
                }

                if (treeItem.IsChecked.HasValue && !treeItem.IsChecked.Value) {
                    var found = SelectedFields.Where(x => x.Id == (FieldId)treeItem.Id).ToList();
                    found.ForEach(x => {
                        SelectedFields.Remove(x);

                        dispatcher.BeginInvoke(new Action(() =>
                        {
                            List<string> selectedMapData = new List<string>();
                            foreach (var sCZ in SelectedFields)
                            {
                                if (sCZ.MapData != null)
                                {
                                    selectedMapData.Add(sCZ.MapData);
                                }
                            }
                            BingMapsLayer = BingMapsUtility.GetLayerForMapData(selectedMapData.ToArray());
                            RaisePropertyChanged("BingMapsLayer");
                            try
                            {
                                MapUpdated(this, e);
                            }
                            catch(Exception ex) { var error = ex.InnerException; }
                        }));

                        TotalArea -= x.SelectedArea;
                    });
                }

                RaisePropertyChanged("FieldCount");

                OnFieldCheckedChanged(treeItem as FieldTreeItemViewModel);
            }
        }

        //internal CropZoneArea[] GetCropZoneAreas() {
        //    List<CropZoneArea> areas = new List<CropZoneArea>();

        //    foreach (var cz in SelectedCropZones) {
        //        areas.Add(new CropZoneArea(cz.Id, cz.SelectedArea.Value, cz.SelectedArea.Unit.Name));
        //    }

        //    return areas.ToArray();
        //}

        internal virtual void ResetData() {
            SelectedFields.Clear();
            TotalArea = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(0);
            RaisePropertyChanged("FieldCount");
            BuildTreeList();
        }

        public void ChangeCropYearForTree(int cropyear) {
            this.cropYear = cropyear;
            ResetData();
        }

        public bool IsFilterVisible {
            get { return isFilterVisible; }
            set {
                isFilterVisible = value;
                RaisePropertyChanged("IsFilterVisible");
            }
        }

        public string FilterHeader {
            get { return filterHeader; }
            set {
                filterHeader = value;
                RaisePropertyChanged("FilterHeader");
            }
        }

        void ClearFilter() {
            if (FilterModel != null && RootTreeItemModelsView != null) {
                FilterModel.ClearFilter();
                FilterModel.BeforeFilter();
                RootTreeItemModelsView.Refresh();
                GenerateFilterHeader();
                IsFilterVisible = false;
            }
        }

        void ApplyFilter() {
            if (FilterModel != null && RootTreeItemModelsView != null) {
                FilterModel.BeforeFilter();
                RootTreeItemModelsView.Refresh();
                GenerateFilterHeader();
                IsFilterVisible = false;
            }
        }

        void GenerateFilterHeader() {
            var originalCount = (from r in RootTreeItemModels
                                 from fa in r.Children
                                 select fa.Children.Count).Sum();
            var filteredCount = (from r in RootTreeItemModels
                                 from fa in r.Children
                                 select fa.FilteredChildren.Count).Sum();

            string entityPluralityAwareName = originalCount == 1 ? Strings.Field_Text : Strings.Fields_Text;

            if (originalCount != filteredCount) {
                FilterHeader = string.Format(Strings.FilterHeader_Format_CountOfCountPlurality_Text, filteredCount, originalCount, entityPluralityAwareName);
            } else {
                //FilterHeader = string.Format("{0} {1}", filteredCount, entityPluralityAwareName);
                FilterHeader = Strings.FilterHeader_AllFields_Text;
            }
        }

    }

    public enum CropZoneSelectionEditMode {
        Application,
        Contract,
        Yield
    }
}
