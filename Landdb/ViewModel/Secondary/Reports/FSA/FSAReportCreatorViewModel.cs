﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Converters.Reports;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Reports;
using Landdb.ViewModel.Secondary.Reports.FSA.Generators;
using Microsoft.Win32;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using Telerik.Reporting;
using Telerik.Reporting.Processing;

namespace Landdb.ViewModel.Secondary.Reports.FSA
{
    public class FSAReportCreatorViewModel : ViewModelBase
    {
        IClientEndpoint endpoint;
        Dispatcher dispatcher;
        IFSAReportGenerator selectedReportGenerator;
        ReportTemplateViewModel template;
        ReportTemplateViewModelToFile templateService = new ReportTemplateViewModelToFile();
        string reportType;
        FSAQueryCriteria criteria;
        int pageIndex = 0;

        public FSAReportCreatorViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, int cropYear, ReportTemplateViewModel template, string reportType)
        {
            this.endpoint = clientEndpoint;
            this.dispatcher = dispatcher;

            CancelCommand = new RelayCommand(OnCancelReport);
            SaveCommand = new RelayCommand(SaveTemplate);
            criteria = new FSAQueryCriteria();
            InfoPageModel = new FSAReportInfoViewModel(clientEndpoint, dispatcher, cropYear);

            ReportGenerators = new ObservableCollection<IFSAReportGenerator>();
            //Call to add all the Applied Product Reports
            AddFSAReportGenerators(clientEndpoint, cropYear);

            //SelectedReportGenerator = ReportGenerators.First();
            PDFExport = new RelayCommand(this.PDF);

            UpdateReport();
            this.reportType = reportType;

            //take passed in template and set values on associated ViewModels along with Criteria page
            if (template != null) { InitializeFromTemplate(template); this.template = template; };
        }

        public ICommand CancelCommand { get; private set; }
        public RelayCommand PDFExport { get; set; }
        public ICommand SaveCommand { get; set; }

        public ReportSource ReportSource { get; set; }
        public int SelectedPageIndex { get { return pageIndex; } set { pageIndex = value; SelectedReportGenerator = null; } }
        public string TemplateName { get; set; }

        public FSAReportInfoViewModel InfoPageModel { get; private set; }

        public ObservableCollection<IFSAReportGenerator> ReportGenerators { get; private set; }
        public IFSAReportGenerator SelectedReportGenerator
        {
            get { return selectedReportGenerator; }
            set
            {
                selectedReportGenerator = value;
                UpdateReport();
                RaisePropertyChanged("SelectedReportGenerator");
            }
        }

        public string CropYear { get { return ApplicationEnvironment.CurrentCropYear.ToString(); } }

        void OnCancelReport()
        {
            Messenger.Default.Send<ShowMainViewMessage>(new ShowMainViewMessage());
        }

        public void UpdateReport()
        {
            if (SelectedReportGenerator != null)
            {
                criteria = BuildCriteria();
                ReportSource = SelectedReportGenerator.Generate(criteria);
                RaisePropertyChanged("ReportSource");
            }
        }

        FSAQueryCriteria BuildCriteria()
        {
            criteria.SelectedCropZones.Clear();
            criteria.SelectedCropZones.AddRange(InfoPageModel.IncludedCropZones);
            return criteria;
        }

        void AddFSAReportGenerators(IClientEndpoint clientEndpoint, int cropYear)
        {
            ReportGenerators.Add(new FSADetailReportGenerator(clientEndpoint, cropYear));
            ReportGenerators.Add(new FSAMultiplePlantingGenerator(clientEndpoint, cropYear));
            ReportGenerators.Add(new FSAReportGenerator(clientEndpoint, cropYear));
            ReportGenerators.Add(new FSAValidationExcelDumpGenerator(clientEndpoint, cropYear));
        }

        public void PDF()
        {
            try { 
            if (ReportSource == null) return;
            ReportSource reportExport = ReportSource;
            
            ReportProcessor reportProcessor = new ReportProcessor();
            System.Collections.Hashtable deviceInfo =
            new System.Collections.Hashtable();
            RenderingResult result = reportProcessor.RenderReport("PDF", reportExport, deviceInfo);
            
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "Adobe Reader|*.pdf";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;

            Nullable<bool> resultSaved = saveFileDialog1.ShowDialog();

            // Process save file dialog box results 
            if (resultSaved == true)
            {
                // Save document 
                string filename = saveFileDialog1.FileName;

                try
                {
                    using (System.IO.FileStream fs = new System.IO.FileStream(filename, System.IO.FileMode.Create))
                    {
                        fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
                    }

                    System.Diagnostics.Process.Start(filename);
                }
                catch (Exception ex) { }

            }
            }
            catch (Exception ex)
            {
                //do nothing
            }
        }

        void SaveTemplate()
        {
            criteria = BuildCriteria();
            if (template != null && template.TemplateName == TemplateName)
            {
                ReportTemplateViewModel editedTemplate = new ReportTemplateViewModel()
                {
                    TemplateName = TemplateName,
                    Created = template.Created,
                    LastUsed = DateTime.Now,
                    CriteriaJSON = JsonConvert.SerializeObject(criteria),
                    ReportType = template.ReportType,
                    DataSourceId = template.DataSourceId
                };

                //TODO :: CALL TO EDIT PREVIOUS TEMPLATE...
                templateService.EditTemplate(editedTemplate);
            }
            else
            {
                ReportTemplateViewModel newTemplate = new ReportTemplateViewModel()
                {
                    TemplateName = TemplateName,
                    Created = DateTime.Now,
                    LastUsed = DateTime.Now,
                    CriteriaJSON = JsonConvert.SerializeObject(criteria),
                    ReportType = reportType,
                    DataSourceId = ApplicationEnvironment.CurrentDataSourceId
                };

                //TODO :: call service to save the new report template
                templateService.SaveNewTemplate(newTemplate);
                template = newTemplate;
            }

            RaisePropertyChanged("NewTemplateCreated", null, new ReportTemplateViewModel(), true);
        }

        void InitializeFromTemplate(ReportTemplateViewModel temp)
        {
            TemplateName = temp.TemplateName;

            FSAQueryCriteria crit = JsonConvert.DeserializeObject<FSAQueryCriteria>(temp.CriteriaJSON);
            var flattenedHierarchy = endpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear)).GetValue(new FlattenedTreeHierarchyView());

            if (crit.SelectedCropZones.Any())
            {

                var fields = (from f in flattenedHierarchy.Items
                              where crit.SelectedCropZones.Contains(f.CropZoneId)
                              select f.FieldId).Distinct();

                var fsaFarmNumbers = (from n in fields
                                      select endpoint.GetView<FieldDetailsView>(n).Value.FsaFarmNumber).Distinct();
                var fsatractNumbers = (from m in fields
                                       select endpoint.GetView<FieldDetailsView>(m).Value.FsaTractNumber).Distinct();

                foreach (var num in fsaFarmNumbers)
                {
                    if (!string.IsNullOrEmpty( num ))
                    {
                        InfoPageModel.FSAFarms.FirstOrDefault(f => f.FSANumber == num).IsChecked = true;
                    }
                }

                foreach (var tract in fsatractNumbers)
                {
                    if (!string.IsNullOrEmpty( tract ))
                    {
                        InfoPageModel.FSATracts.FirstOrDefault(t => t.FSANumber == tract).IsChecked = true;
                    }
                }
            }
            //AppliedQueryCriteria crit = JsonConvert.DeserializeObject<AppliedQueryCriteria>(temp.CriteriaJSON);

            ////InfoPageModel.Products = crit.SelectedProducts;
            //InfoPageModel.ReportStartDate = crit.QueryStartDate;
            //InfoPageModel.ReportEndDate = crit.QueryEndDate;
            //InfoPageModel.CostType = crit.CostType;

            //var dsID = new DataSourceId(ApplicationEnvironment.CurrentDataSourceId);

            //var RootTreeItemModels = FieldsPageModel.RootTreeItemModels;
            //if (RootTreeItemModels.Any())
            //{
            //    if (RootTreeItemModels.First() is GrowerTreeItemViewModel)
            //    {
            //        var toCheck = from farm in RootTreeItemModels.First().Children
            //                      from field in farm.Children
            //                      from cz in field.Children
            //                      where crit.SelectedCropZones.Contains(cz.Id)
            //                      select cz;
            //        toCheck.ForEach(x => x.IsChecked = true);
            //    }
            //    else
            //    {
            //        var toCheck = from crop in RootTreeItemModels.First().Children
            //                      from farm in crop.Children
            //                      from field in farm.Children
            //                      from cz in field.Children
            //                      where crit.SelectedCropZones.Contains(cz.Id)
            //                      select cz;
            //        toCheck.ForEach(x => x.IsChecked = true);
            //    }
            //}

            //var unSelectedProds = from infoProds in InfoPageModel.Products
            //                      where !crit.SelectedProducts.Contains(infoProds.Id)
            //                      select infoProds;
            //unSelectedProds.ForEach(x => x.Checked = false);
        }
    }
}
