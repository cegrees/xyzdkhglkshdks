﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Secondary.Reports.FSA
{
    public class FSAQueryCriteria
    {
        public FSAQueryCriteria()
        {
            SelectedCropZones = new List<CropZoneId>();
        }

        public List<CropZoneId> SelectedCropZones { get; set; }
        public List<FieldId> SelectedFields { get; set; }
        public List<string> SelectedFarmNumbers { get; set; }
        public List<string> SelectedTractNumbers { get; set; }
    }
}
