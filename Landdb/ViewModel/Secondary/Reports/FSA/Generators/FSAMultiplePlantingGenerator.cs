﻿using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Map;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.ReportModels.CropZone;
using Landdb.ViewModel.Secondary.Map;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Landdb.Resources;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.FSA.Generators
{
    public class FSAMultiplePlantingGenerator : IFSAReportGenerator
    {
        IClientEndpoint clientEndpoint;
        int cropYear;

        public FSAMultiplePlantingGenerator(IClientEndpoint clientEndpoint, int cropYear)
        {
            this.clientEndpoint = clientEndpoint;
            this.cropYear = cropYear;
        }



        public Telerik.Reporting.ReportSource Generate(FSAQueryCriteria criteria)
        {
            ReportBook book = new ReportBook();
            var rs = new InstanceReportSource();

            var flattenedHierarchy = clientEndpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new FlattenedTreeHierarchyView());
            var masterlist = clientEndpoint.GetMasterlistService();
            var applicationData = clientEndpoint.GetView<CropZoneApplicationDataView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new CropZoneApplicationDataView());
            

            if (criteria.SelectedCropZones.Any())
            {

                var fields = (from f in flattenedHierarchy.Items
                              where criteria.SelectedCropZones.Contains(f.CropZoneId)
                              select f.FieldId).Distinct();

                var fsaFarmNumbers = (from n in fields
                                      select clientEndpoint.GetView<FieldDetailsView>(n).Value.FsaFarmNumber).Distinct();
                var data = new FSAData();
                data.FSAFarms = new List<FSAFarmItem>();
                foreach (var num in fsaFarmNumbers)
                {
                    var selectFields = from f in fields
                                       where clientEndpoint.GetView<FieldDetailsView>(f).Value.FsaFarmNumber == num
                                       select f;
                    foreach (var field in selectFields)
                    {
                        var czs = from f in flattenedHierarchy.Items
                                  where f.FieldId == field && criteria.SelectedCropZones.Contains(f.CropZoneId)
                                  select f.CropZoneId;
                        foreach (var cz in czs)
                        {
                            if (cz != null)
                            {
                                var fsaFarm = new FSAFarmItem();
                                var czDetails = clientEndpoint.GetView<CropZoneDetailsView>(cz).Value;
                                fsaFarm.Varieties = new List<VarietyItem>();

                                var fieldData = clientEndpoint.GetView<FieldDetailsView>(field).Value;

                                fsaFarm.DataSourceName = ApplicationEnvironment.CurrentDataSourceName;
                                fsaFarm.CropYear = ApplicationEnvironment.CurrentCropYear;
                                fsaFarm.FSAFarmNumber = fieldData.FsaFarmNumber;
                                fsaFarm.FSAFieldNumber = fieldData.FsaFieldNumber;
                                fsaFarm.FSATractNumber = fieldData.FsaTractNumber;
                                fsaFarm.FSAArea = czDetails.FsaArea.HasValue ? (decimal)czDetails.FsaArea.Value : (fieldData.FsaArea != null ? (decimal)fieldData.FsaArea : 0m);
                                fsaFarm.County = fieldData.County;
                                fsaFarm.CropInsuranceId = fieldData.CluID;
                                fsaFarm.Meridian = fieldData.Meridian;
                                fsaFarm.Range = fieldData.Range + fieldData.RangeDirection;
                                fsaFarm.Section = fieldData.Section;
                                fsaFarm.Township = fieldData.Township + fieldData.TownshipDirection;
                                fsaFarm.CashRent = fieldData.RentType == Strings.RentType_CashRent_Text ? "Yes" : "No";
                                fsaFarm.CropShare = fieldData.RentType == Strings.RentType_CashRent_Text ? "No" : "Yes";
                                fsaFarm.DisplayName = flattenedHierarchy.Items.FirstOrDefault(x => x.FieldId == field).FarmName + " :: " + fieldData.FieldNameByCropYear[cropYear] + " :: " + czDetails.Name;
                                fsaFarm.Irrigated = fieldData.Irrigated;
                                fsaFarm.LandLordPercent = (decimal)fieldData.LandLordPercent;
                                fsaFarm.TenantPercent = (decimal)fieldData.TenantPercent;
                                fsaFarm.CropInsuranceId = czDetails.InsuranceId;
                                //////////////////////////////////////////////////////
                                List<string> filteredMapData = new List<string>();

                                var maps = clientEndpoint.GetView<ItemMap>(cz);
                                try
                                {
                                    if (maps.HasValue)
                                    {
                                        if (maps.Value.MostRecentMapItem != null && maps.Value.MostRecentMapItem.MapData != string.Empty && maps.Value.MostRecentMapItem.MapData != "MULTIPOLYGON EMPTY") {
                                            filteredMapData.Add(maps.Value.MostRecentMapItem.MapData);
                                        }
                                    }
                                }
                                finally { }

                                var converter = new ConvertToBitmap(filteredMapData, 175, 175, this.clientEndpoint, 0, "", "");
                                fsaFarm.Map = converter.BitMap;
                                /////////////////////////////////////////////////////////

                                var varietyProducts = from a in applicationData.Items
                                                      where a.CropZoneId == cz && masterlist.GetProduct(a.ProductId).ProductType == GlobalStrings.ProductType_Seed
                                                      select a;

                                //send varietyProducts to be checked and returned as turnRow.
                                var turnRowValue = TurnRowCalculation(cz, fieldData, varietyProducts.ToList());
                                var summedArea = (from v in varietyProducts
                                                  select v.AreaValue).Sum();
                                foreach (var v in varietyProducts)
                                {
                                    var varietyItem = new VarietyItem();
                                    var appDetails = clientEndpoint.GetView<ApplicationView>(v.ApplicationId).Value;
                                    var appFieldArea = appDetails.CropZones.FirstOrDefault(x => x.Id.Id == cz.Id).Area.Value;
                                    varietyItem.Area = (decimal)(appFieldArea * v.CoveragePercent); //czDetails.ReportedArea != null ? (decimal)czDetails.ReportedArea : czDetails.BoundaryArea != null ? (decimal)czDetails.BoundaryArea : 0m;
                                    varietyItem.Crop = masterlist.GetCropDisplay(czDetails.CropId);
                                    varietyItem.PlantingDate = v.StartDate;
                                    varietyItem.Variety = masterlist.GetProductDisplay(v.ProductId);
                                    //varietyItem.TurnRow = (decimal)turnRowValue;

                                    fsaFarm.Varieties.Add(varietyItem);
                                }
                                fsaFarm.TurnRow = (decimal)turnRowValue;
                                data.FSAFarms.Add(fsaFarm);
                            }
                        }
                    }

                }

                data.FSAFarms = data.FSAFarms.OrderBy(x => x.FSAFarmNumber).ThenBy(y => y.FSATractNumber).ThenBy(p => p.FSAFieldNumber).ToList();
                var report = new Landdb.Views.Secondary.Reports.FSA.FSAMultiplePlantings();
                report.DataSource = data.FSAFarms;
                report.Name = Strings.ReportName_SomeName_Text;

                book.Reports.Add(report);
            }

            rs.ReportDocument = book;
            return rs;
        }

        public string DisplayName
        {
            get { return Strings.GeneratorName_CropZoneWithMap_Text; }
        }

        public double TurnRowCalculation(CropZoneId czId, FieldDetailsView field, List<CropZoneApplicationDataItem> seedApplications)
        {
            var czDetails = clientEndpoint.GetView<CropZoneDetailsView>(czId).Value;
            //var fsaArea = czDetails.FsaArea != null ? czDetails.FsaArea.Value : null;
            var bdyArea = czDetails.BoundaryArea != null ? czDetails.BoundaryArea.Value : 0;
            var area = czDetails.ReportedArea != null && czDetails.ReportedArea > 0 ? czDetails.ReportedArea.Value : bdyArea;
            area = field.FsaArea.HasValue && field.FsaArea > 0 ? field.FsaArea.Value : area;
            area = czDetails.FsaArea.HasValue && czDetails.FsaArea > 0 ? czDetails.FsaArea.Value : area;
            List<double> plantingAreaPossibilities = new List<double>();
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //all permutations method with percent error ... //////////////////////////////////////////////////////////////////

            var currentPlantings = (from a in seedApplications
                                    select a.AreaValue).ToList();

            var permutations = from m in Enumerable.Range(0, 1 << currentPlantings.Count)
                  select
                      from i in Enumerable.Range(0, currentPlantings.Count)
                      where (m & (1 << i)) != 0
                      select currentPlantings[i];

            foreach (var permutaion in permutations)
            {
                if (permutaion != null)
                {
                    var areaValue = permutaion.Sum();
                    plantingAreaPossibilities.Add(areaValue);
                }
            }

            var percentErrorList = (from e in plantingAreaPossibilities
                                    select new ErrorClass { Area = e, Error = (((e - area) / area) * 100), Distance = Math.Abs( (((e - area) / area) * 100) - 0 ) }).ToList();
            var turnRow = area - percentErrorList.OrderBy(x => x.Distance).First().Area;
            return turnRow;
        }
    }

    public class ErrorClass
    {
        public double Area { get; set; }
        public double Error { get; set; }
        public double Distance { get; set; }
    }
}
