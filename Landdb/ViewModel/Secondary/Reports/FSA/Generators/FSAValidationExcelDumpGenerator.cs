﻿using ExportToExcel;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.ReportModels.CropZone;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Landdb.Resources;

namespace Landdb.ViewModel.Secondary.Reports.FSA.Generators
{
    public class FSAValidationExcelDumpGenerator : IFSAReportGenerator
    {
        IClientEndpoint clientEndpoint;
        int cropYear;

        public FSAValidationExcelDumpGenerator(IClientEndpoint clientEndpoint, int cropYear)
        {
            this.clientEndpoint = clientEndpoint;
            this.cropYear = cropYear;
        }

        public Telerik.Reporting.ReportSource Generate(FSAQueryCriteria criteria)
        {
            var flattenedHierarchy = clientEndpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new FlattenedTreeHierarchyView());
            var applicationData = clientEndpoint.GetView<CropZoneApplicationDataView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new CropZoneApplicationDataView());
            var masterlist = clientEndpoint.GetMasterlistService();

            if (criteria.SelectedCropZones.Any())
            {
                var fields = (from f in flattenedHierarchy.Items
                              where criteria.SelectedCropZones.Contains(f.CropZoneId)
                              select f.FieldId).Distinct();

                var fsaFarmNumbers = (from n in fields
                                      select clientEndpoint.GetView<FieldDetailsView>(n).Value.FsaFarmNumber).Distinct();
                var data = new List<FSAValidationData>();
                foreach (var num in fsaFarmNumbers)
                {
                    var selectFields = from f in fields
                                       where clientEndpoint.GetView<FieldDetailsView>(f).Value.FsaFarmNumber == num
                                       select f;
                    foreach (var field in selectFields)
                    {
                        var czs = from f in flattenedHierarchy.Items
                                  where f.FieldId == field && criteria.SelectedCropZones.Contains(f.CropZoneId)
                                  select f.CropZoneId;
                        foreach (var cz in czs)
                        {
                            if (cz != null)
                            {
                                var czDetails = clientEndpoint.GetView<CropZoneDetailsView>(cz).Value;
                                var fieldData = clientEndpoint.GetView<FieldDetailsView>(field).Value;
                                var reported = fieldData.FsaArea.HasValue  && fieldData.FsaArea.Value > 0 ? fieldData.FsaArea.Value : 
                                    (czDetails.ReportedArea.HasValue ? czDetails.ReportedArea.Value : 0);
                                reported = reported == 0 && czDetails.BoundaryArea.HasValue ? czDetails.BoundaryArea.Value : reported;
                                

                                var varietyProducts = from a in applicationData.Items
                                                      where a.CropZoneId == cz && masterlist.GetProduct(a.ProductId).ProductType == GlobalStrings.ProductType_Seed
                                                      select a;

                                if (varietyProducts.Any())
                                {
                                    foreach (var seedProduct in varietyProducts)
                                    {
                                        var fsaFarm = new FSAValidationData();

                                        fsaFarm.DatasourceName = ApplicationEnvironment.CurrentDataSourceName;
                                        fsaFarm.CropYear = ApplicationEnvironment.CurrentCropYear;
                                        var flat = flattenedHierarchy.Items.FirstOrDefault(x => x.CropZoneId == czDetails.Id);
                                        fsaFarm.FsaFarmNumber = fieldData.FsaFarmNumber;
                                        fsaFarm.FsaFieldNumber = fieldData.FsaFieldNumber;
                                        fsaFarm.FsaTractNumber = fieldData.FsaTractNumber;

                                        fsaFarm.ReportedArea = (decimal)reported;

                                        fsaFarm.FsaArea = czDetails.FsaArea.HasValue ? (decimal)czDetails.FsaArea : 
                                            (fieldData.FsaArea != null ? (decimal)fieldData.FsaArea : 0m);
                                        fsaFarm.County = fieldData.County;
                                        fsaFarm.Crop = masterlist.GetCropDisplay(czDetails.CropId);
                                        fsaFarm.CropInsuranceId = fieldData.CluID;
                                        fsaFarm.Meridian = fieldData.Meridian;
                                        fsaFarm.Range = fieldData.Range + fieldData.RangeDirection;
                                        fsaFarm.Section = fieldData.Section;
                                        fsaFarm.Township = fieldData.Township + fieldData.TownshipDirection;
                                        fsaFarm.CashRent = fieldData.RentType == Strings.RentType_CashRent_Text ? "Yes" : "No";
                                        fsaFarm.CropShare = fieldData.RentType == Strings.RentType_CashRent_Text ? "No" : "Yes";
                                        fsaFarm.DisplayName = flattenedHierarchy.Items.FirstOrDefault(x => x.FieldId == field).FarmName + " :: " + fieldData.FieldNameByCropYear[cropYear] + " :: " + czDetails.Name;
                                        fsaFarm.Irrigated = fieldData.Irrigated;
                                        fsaFarm.LandLordPercent = (decimal)fieldData.LandLordPercent;
                                        fsaFarm.TenantPercent = (decimal)fieldData.TenantPercent;
                                        fsaFarm.CropInsuranceId = czDetails.InsuranceId;

                                        fsaFarm.Variety = masterlist.GetProductDisplay(seedProduct.ProductId);
                                        var fieldDetails = clientEndpoint.GetView<FieldDetailsView>(czDetails.FieldId).Value;
                                        List<CropZoneApplicationDataItem> currentSeedItem = new List<CropZoneApplicationDataItem>();
                                        currentSeedItem.Add(seedProduct);
                                        var turnRowCalcs = TurnRowCalculation(czDetails.Id, fieldDetails, currentSeedItem);
                                        fsaFarm.PlantingArea = (decimal)turnRowCalcs.Item2;
                                        fsaFarm.PlantingDate = seedProduct.StartDate.ToShortDateString();
                                        fsaFarm.TurnRow = (decimal)turnRowCalcs.Item1;

                                        data.Add(fsaFarm);
                                    }
                                }
                                else
                                {
                                    var fsaFarm = new FSAValidationData();

                                    fsaFarm.DatasourceName = ApplicationEnvironment.CurrentDataSourceName;
                                    fsaFarm.CropYear = ApplicationEnvironment.CurrentCropYear;
                                    var flat = flattenedHierarchy.Items.FirstOrDefault(x => x.CropZoneId == czDetails.Id);
                                    fsaFarm.FsaFarmNumber = fieldData.FsaFarmNumber;
                                    fsaFarm.FsaFieldNumber = fieldData.FsaFieldNumber;
                                    fsaFarm.FsaTractNumber = fieldData.FsaTractNumber;
                                    fsaFarm.ReportedArea = (decimal)reported;

                                    fsaFarm.FsaArea = czDetails.FsaArea.HasValue ? (decimal)czDetails.FsaArea : 
                                        (fieldData.FsaArea != null ? (decimal)fieldData.FsaArea : 0m);
                                    fsaFarm.County = fieldData.County;
                                    fsaFarm.Crop = masterlist.GetCropDisplay(czDetails.CropId);
                                    fsaFarm.CropInsuranceId = fieldData.CluID;
                                    fsaFarm.Meridian = fieldData.Meridian;
                                    fsaFarm.Range = fieldData.Range + fieldData.RangeDirection;
                                    fsaFarm.Section = fieldData.Section;
                                    fsaFarm.Township = fieldData.Township + fieldData.TownshipDirection;
                                    fsaFarm.CashRent = fieldData.RentType == Strings.RentType_CashRent_Text ? "Yes" : "No";
                                    fsaFarm.CropShare = fieldData.RentType == Strings.RentType_CashRent_Text ? "No" : "Yes";
                                    fsaFarm.DisplayName = flattenedHierarchy.Items.FirstOrDefault(x => x.FieldId == field).FarmName + " :: " + fieldData.FieldNameByCropYear[ApplicationEnvironment.CurrentCropYear] + " :: " + czDetails.Name;
                                    fsaFarm.Irrigated = fieldData.Irrigated;
                                    fsaFarm.LandLordPercent = (decimal)fieldData.LandLordPercent;
                                    fsaFarm.TenantPercent = (decimal)fieldData.TenantPercent;
                                    fsaFarm.CropInsuranceId = czDetails.InsuranceId;

                                    data.Add(fsaFarm);
                                }
                            }
                        }
                    }
                }

                //send to excel and open
                data = data.OrderBy(x => x.FsaFarmNumber).ThenBy(y => y.County).ThenBy(m => m.Crop).ToList();

                System.Data.DataTable dt1 = new System.Data.DataTable();

                dt1 = ExportToExcel.CreateExcelFile.ListToDataTable(data, true);

                dt1.TableName = Strings.TableName_FsaValidation_Text;

                DataSet ds = new DataSet("excelDS");
                ds.Tables.Add(dt1);

                try
                {
                    //open up file dialog to save file....
                    //then call createexcelfile to create the excel...
                    SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                    string filename = string.Empty;

                    saveFileDialog1.Filter = "Excel|*.xlsx";
                    saveFileDialog1.FilterIndex = 2;
                    saveFileDialog1.RestoreDirectory = true;

                    Nullable<bool> resultSaved = saveFileDialog1.ShowDialog();

                    // Process save file dialog box results 
                    if (resultSaved == true)
                    {
                        // Save document 
                        filename = saveFileDialog1.FileName;
                        CreateExcelFile.CreateExcelDocument(ds, filename);
                    }

                    //now open file....
                    System.Diagnostics.Process.Start(filename);

                    //Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
                }
                catch (Exception ex)
                {
                    //Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
                    return null;
                }
            }
            return null;
        }

        public string DisplayName
        {
            get { return Strings.TableName_FsaValidation_Text; }
        }

        public Tuple<double, double> TurnRowCalculation(CropZoneId czId, FieldDetailsView field, List<CropZoneApplicationDataItem> seedApplications)
        {
            var czDetails = clientEndpoint.GetView<CropZoneDetailsView>(czId).Value;
            //var fsaArea = czDetails.FsaArea != null ? czDetails.FsaArea.Value : null;
            var bdyArea = czDetails.BoundaryArea != null ? czDetails.BoundaryArea.Value : 0;
            var area = czDetails.ReportedArea != null && czDetails.ReportedArea > 0 ? czDetails.ReportedArea.Value : bdyArea;
            area = field.FsaArea.HasValue && field.FsaArea > 0 ? field.FsaArea.Value : area;
            area = czDetails.FsaArea.HasValue && czDetails.FsaArea > 0 ? czDetails.FsaArea.Value : area;
            List<double> plantingAreaPossibilities = new List<double>();
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //all permutations method with percent error ... //////////////////////////////////////////////////////////////////

            var currentPlantings = (from a in seedApplications
                                    select a.AreaValue).ToList();

            var permutations = from m in Enumerable.Range(0, 1 << currentPlantings.Count)
                               select
                                   from i in Enumerable.Range(0, currentPlantings.Count)
                                   where (m & (1 << i)) != 0
                                   select currentPlantings[i];

            foreach (var permutaion in permutations)
            {
                if (permutaion != null)
                {
                    var areaValue = permutaion.Sum();
                    plantingAreaPossibilities.Add(areaValue);
                }
            }

            var percentErrorList = (from e in plantingAreaPossibilities
                                    select new ErrorClass { Area = e, Error = (((e - area) / area) * 100), Distance = Math.Abs((((e - area) / area) * 100) - 0) }).ToList();
            var turnRow = area - percentErrorList.OrderBy(x => x.Distance).First().Area;
            return Tuple.Create(turnRow, percentErrorList.OrderBy(x => x.Distance).First().Area);
        }
    }
}
