﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.FSA.Generators
{
    public interface IFSAReportGenerator
    {
        ReportSource Generate(FSAQueryCriteria criteria);
        string DisplayName { get; }
    }
}
