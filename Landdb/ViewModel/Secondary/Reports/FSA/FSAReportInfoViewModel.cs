﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Client.Spatial;
using Landdb.Domain.ReadModels.Map;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Microsoft.Maps.MapControl.WPF;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Landdb.ViewModel.Secondary.Reports.FSA
{
    public class FSAReportInfoViewModel : ViewModelBase
    {
        IClientEndpoint endpoint;
        int cropYear;
        bool includeMap;
        IMasterlistService masterlist;
        IncludedCrop selectedCrop;
        List<CropZoneId> includedCropZones;
        FlattenedTreeHierarchyView flattenedHierarchy = new FlattenedTreeHierarchyView();
        Dispatcher dispatch;
        FSAItem county;
        List<FSAItem> counties;
        List<FSAItem> tracts;

        public FSAReportInfoViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, int cropYear)
        {
            this.endpoint = clientEndpoint;
            this.cropYear = cropYear;
            this.dispatch = dispatcher;
            includedCropZones = new List<CropZoneId>();
            counties = new List<FSAItem>();
            Counties = new List<FSAItem>();
            tracts = new List<FSAItem>();
            masterlist = endpoint.GetMasterlistService();
            includeMap = false;
            Crops = new ObservableCollection<IncludedCrop>();
            FSAFarms = new List<FSAItem>();
            FSATracts = new List<FSAItem>();
            IncludedCropZones = new List<CropZoneId>();
            IncludedFields = new List<FieldId>();
            flattenedHierarchy = endpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new FlattenedTreeHierarchyView());
            Initialize();
            
            Messenger.Default.Register<PropertyChangedMessage<bool>>(this, PopulateItems);
        }

        public ObservableCollection<IncludedCrop> Crops { get; set; }
        public IncludedCrop SelectedCrop { get { return selectedCrop; } set { selectedCrop = value; PopulateFSAFarms(); } }

        public List<FSAItem> FSAFarms { get; set; }
        public FSAItem SelectedFarm { get; set; }

        public List<FSAItem> FSATracts { get { return tracts; } set { tracts = value; RaisePropertyChanged("FSATracts"); } }
        public FSAItem SelectedTract { get; set; }


        public List<FSAItem> Counties { get; set; }
        public FSAItem SelectedCounty
        {
            get { return county; }
            set
            {
                county = value;
            }
        }

        public bool IncludeMap { get { return includeMap; } set { includeMap = value; RaisePropertyChanged("IncludeMap"); } }

        public List<CropZoneId> IncludedCropZones { get { return includedCropZones; } set { includedCropZones = value; UpdateMap(); } }
        public List<FieldId> IncludedFields { get; set; }

        public MapLayer BingMapsLayer { get; private set; }

        public string MapCulture
        {
            get { return Infrastructure.ApplicationEnvironment.BingWpfMapCulture; }
        }

        void Initialize()
        {
            SelectedCrop = new IncludedCrop();
            var crops = (from c in flattenedHierarchy.Items
                         orderby c.CropName
                        select c.CropId).Distinct();

            foreach (var item in crops)
            {
                Crops.Add(new IncludedCrop() { CropID = item, CropName = masterlist.GetCropDisplay(item) });
            }

            var fields = (from f in flattenedHierarchy.Items
                          where f.FieldId != null
                          select f.FieldId).Distinct();

            foreach (var fieldItem in fields)
            {
                var fieldDetail = endpoint.GetView<FieldDetailsView>(fieldItem);
                
                if (fieldDetail.HasValue)
                {
                    var fieldValue = fieldDetail.Value;
                    if (FSAFarms.Where(x => x.FSANumber == fieldValue.FsaFarmNumber).Count() == 0 && !string.IsNullOrEmpty( fieldValue.FsaFarmNumber ) )
                    {
                        FSAFarms.Add(new FSAItem() { FSANumber = fieldValue.FsaFarmNumber, IsChecked = false, Type = "Farm" });
                    }

                    if (FSATracts.Where(x=> x.FSANumber == fieldValue.FsaTractNumber).Count() == 0 && !string.IsNullOrEmpty( fieldValue.FsaTractNumber ) )
                    {
                        FSATracts.Add(new FSAItem() { FSANumber = fieldValue.FsaTractNumber, IsChecked = false, Type = "Tract" });
                    }

                    if (Counties.Where(x => x.FSANumber == fieldValue.County).Count() == 0 && !string.IsNullOrEmpty(fieldValue.County))
                    {
                        Counties.Add(new FSAItem() { FSANumber = fieldValue.County, IsChecked = false, Type = "County" }); 
                    }
                }
            }

        }

        public List<FieldDetailsView> InitialFieldDetailsViewList;
        void PopulateFSAFarms()
        {
            InitialFieldDetailsViewList = new List<FieldDetailsView>();
            if (SelectedCrop != null)
            {
                FSAFarms.Clear();
                FSATracts.Clear();
                var fields = (from f in flattenedHierarchy.Items
                              where f.FieldId != null && f.CropId == SelectedCrop.CropID
                              select f.FieldId).Distinct();
                FSAFarms = new List<FSAItem>();
                foreach (var fieldItem in fields)
                {
                    var fieldDetail = endpoint.GetView<FieldDetailsView>(fieldItem);

                    if (fieldDetail.HasValue)
                    {
                        InitialFieldDetailsViewList.Add(fieldDetail.Value);
                        var fieldValue = fieldDetail.Value;
                        if (FSAFarms.Where(x => x.FSANumber == fieldValue.FsaFarmNumber).Count() == 0 && !string.IsNullOrEmpty(fieldValue.FsaFarmNumber))
                        {
                            FSAFarms.Add(new FSAItem() { FSANumber = fieldValue.FsaFarmNumber, IsChecked = false, Type = "Farm" });
                            FSAFarms = FSAFarms.Distinct(x => x.FSANumber).ToList();
                            RaisePropertyChanged("FSAFarms");
                        }

                        if (FSATracts.Where(x => x.FSANumber == fieldValue.FsaTractNumber).Count() == 0 && !string.IsNullOrEmpty(fieldValue.FsaTractNumber))
                        {
                            FSATracts.Add(new FSAItem() { FSANumber = fieldValue.FsaTractNumber, IsChecked = false, Type = "Tract" });
                            FSATracts = FSATracts.Distinct(t => t.FSANumber).ToList();
                            RaisePropertyChanged("FSATracts");
                        }
                    }
                }
            }
        }

        void FSAFarmsChanged()
        {
            IncludedCropZones.Clear();
            FSATracts.Clear();

            if (SelectedCrop.CropID != null)
            {
                var fields = from f in flattenedHierarchy.Items
                             from n in FSAFarms
                             where f.FieldId != null && f.CropId == SelectedCrop.CropID && n.IsChecked == true && endpoint.GetView<FieldDetailsView>(f.FieldId).Value.FsaFarmNumber == n.FSANumber
                             select f;

                fields.ForEach(x => IncludedCropZones.Add(x.CropZoneId));
                UpdateMap();
                try
                {
                    var uniqueTracts = (from t in fields
                                        where !string.IsNullOrEmpty(endpoint.GetView<FieldDetailsView>(t.FieldId).Value.FsaTractNumber)
                                        select endpoint.GetView<FieldDetailsView>(t.FieldId).Value.FsaTractNumber).Distinct().ToList();

                    if (uniqueTracts.Count() > 0)
                    {
                        uniqueTracts.ForEach(t => FSATracts.Add(new FSAItem() { FSANumber = t, IsChecked = false, Type = "Tract" }));
                        FSATracts = FSATracts.Distinct(x => x.FSANumber).ToList();
                    }
                }catch(Exception ex){}


                //var availableCrops = (from c in fields
                //                     select new IncludedCrop() { CropID = c.CropId, CropName = c.CropName }).Distinct();
                //Crops.Clear();
                //Crops = new ObservableCollection<IncludedCrop>( availableCrops.ToList() );
                
                RaisePropertyChanged("FSATracts");
            }
            else
            {

                var field = from f in flattenedHierarchy.Items
                            from n in FSAFarms
                            where f.FieldId != null && n.IsChecked == true && endpoint.GetView<FieldDetailsView>(f.FieldId).Value.FsaFarmNumber == n.FSANumber
                            select f;

                field.ForEach(x => IncludedCropZones.Add(x.CropZoneId));
                UpdateMap();
                var uniqueTracts = (from t in field
                                    where !string.IsNullOrEmpty(endpoint.GetView<FieldDetailsView>(t.FieldId).Value.FsaTractNumber)
                                    select endpoint.GetView<FieldDetailsView>(t.FieldId).Value.FsaTractNumber).Distinct();


                uniqueTracts.ForEach(t => FSATracts.Add(new FSAItem() { FSANumber = t, IsChecked = false, Type = "Tract" }));
                FSATracts = FSATracts.Distinct(x => x.FSANumber).ToList();
                RaisePropertyChanged("FSATracts");
            }
        }

        void FSATractsChanged()
        {
            IncludedCropZones.Clear();

            if (SelectedCrop.CropID != null)
            {
                var fields = from f in flattenedHierarchy.Items
                             from n in FSATracts
                             where f.CropId == SelectedCrop.CropID && n.IsChecked == true && endpoint.GetView<FieldDetailsView>(f.FieldId).Value.FsaTractNumber == n.FSANumber
                             select f;

                fields.ForEach(x => IncludedCropZones.Add(x.CropZoneId));
                UpdateMap();
            }
            else
            {

                var field = from f in flattenedHierarchy.Items
                            from n in FSATracts
                            where n.IsChecked == true && endpoint.GetView<FieldDetailsView>(f.FieldId).Value.FsaTractNumber == n.FSANumber
                            select f;

                field.ForEach(x => IncludedCropZones.Add(x.CropZoneId));
                UpdateMap();
            }
        }

        void CountyChanged()
        {
            IncludedCropZones.Clear();

            if (SelectedCrop.CropID != null)
            {
                var fields = from f in flattenedHierarchy.Items
                             from c in Counties
                             where f.CropId == SelectedCrop.CropID && c.IsChecked == true && endpoint.GetView<FieldDetailsView>(f.FieldId).Value.FsaTractNumber == c.FSANumber && endpoint.GetView<FieldDetailsView>(f.FieldId).Value.County == c.FSANumber
                             select f;

                fields.ForEach(x => IncludedCropZones.Add(x.CropZoneId));
                UpdateMap();
            }
            else
            {
                var field = from f in flattenedHierarchy.Items
                            from n in Counties
                            where n.IsChecked == true && endpoint.GetView<FieldDetailsView>(f.FieldId).Value.County == n.FSANumber
                            select f;

                field.ForEach(x => IncludedCropZones.Add(x.CropZoneId));
                UpdateMap();
            }
        }

        void PopulateItems(PropertyChangedMessage<bool> includeChanged)
        {
            var sender = includeChanged.Sender as FSAItem;
            if (sender != null)
            {
                if (sender.Type == "Farm")
                {
                    FSAFarmsChanged();
                }
                else if (sender.Type == "Tract")
                {
                    FSATractsChanged();
                }
                else
                {
                    CountyChanged();
                }
            }
        }

        void UpdateMap()
        {
            dispatch.BeginInvoke(new Action(() =>
            {
                var fields = (from f in flattenedHierarchy.Items
                             from c in IncludedCropZones
                             where f.CropZoneId == c
                             select f.FieldId).Distinct().ToList();
                List<string> selectedMapData = new List<string>();
                foreach (var f in fields)
                {
                    if (f != null)
                    {
                        var mapData = endpoint.GetView<ItemMap>(f);
                        if (mapData.HasValue && mapData.Value.MostRecentMapItem != null)
                        {
                            selectedMapData.Add(mapData.Value.MostRecentMapItem.MapData);
                        }
                    }
                }
                try
                {
                    BingMapsLayer = BingMapsUtility.GetLayerForMapData(selectedMapData.ToArray());
                    RaisePropertyChanged("BingMapsLayer", null, BingMapsLayer, true);
                }
                catch (Exception ex) { }
            }));
        }
    }

    public class IncludedCrop
    {
        public CropId CropID { get; set; }
        public string CropName { get; set; }

        public override string ToString()
        {
            return this.CropName;
        }
    }

    public class FSAItem : ViewModelBase
    {
        public FSAItem()
        {
            IsChecked = false;
        }

        bool ischecked;

        public bool IsChecked { get { return ischecked; } set { if (ischecked != value) { var oldValue = ischecked; ischecked = value; RaisePropertyChanged("IsChecked", oldValue, ischecked, true); } } }
        public string FSANumber { get; set; }
        public string Type { get; set; }
    }
}
