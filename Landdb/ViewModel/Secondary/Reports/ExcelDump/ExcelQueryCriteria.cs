﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Secondary.Reports.ExcelDump
{

    public class ExcelQueryCriteria
    {
        public ExcelQueryCriteria()
        {
            SelectedCropZones = new List<CropZoneId>();
        }
        public List<CropZoneId> SelectedCropZones { get; set; }
        public DateTime QueryStartDate { get; set; }
        public DateTime QueryEndDate { get; set; }
        public bool IncludeField { get; set; }
        public bool IncludeYield { get; set; }
        public bool IncludeGuids { get; set; }
        public bool IncludeSourceRecords { get; set; }
        public bool IncludeInvoiceData { get; set; }
        public bool IncludePlans { get; set; }
        public List<ColumnVisibilityViewModel> IncludedColumns { get; set; }
    }
}
