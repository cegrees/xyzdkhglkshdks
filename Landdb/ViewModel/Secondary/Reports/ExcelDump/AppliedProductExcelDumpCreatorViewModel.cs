﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Converters.Reports;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Fields;
using Landdb.ViewModel.Reports;
using Landdb.ViewModel.Secondary.Reports.AppliedProduct;
using Landdb.ViewModel.Secondary.Reports.AppliedProduct.Generators;
using Landdb.ViewModel.Secondary.Reports.ExcelDump.Generators;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.ExcelDump
{
    public class AppliedProductExcelDumpCreatorViewModel : ViewModelBase
    {
        IExcelDumpCreator selectedReportGenerator;
        ExcelQueryCriteria criteria = new ExcelQueryCriteria();
        ReportTemplateViewModel template;
        ReportTemplateViewModelToFile templateService = new ReportTemplateViewModelToFile();
        string reportType;

        public AppliedProductExcelDumpCreatorViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, int cropYear, ReportTemplateViewModel template, string reportType)
        {
            CancelCommand = new RelayCommand(OnCancelReport);
            SaveCommand = new RelayCommand(SaveTemplate);

            FieldsPageModel = new AppliedProductReportFieldSelectionViewModel(clientEndpoint, dispatcher, cropYear);
            InfoPageModel = new ExcelReportInfoViewModel(clientEndpoint, dispatcher, cropYear);

            ReportGenerators = new ObservableCollection<IExcelDumpCreator>();
            AddAppliedProductReportGenerators(clientEndpoint, cropYear);
            this.reportType = reportType;

            if (template != null) { InitializeFromTemplate(template); this.template = template; };
            SelectedPageIndex = 0;
        }

        public ICommand CancelCommand { get; private set; }
        public ICommand SaveCommand { get; set; }

        public int CropYear { get { return ApplicationEnvironment.CurrentCropYear; } }
        public int SelectedPageIndex { get; set; }
        public string TemplateName { get; set; }
        public AppliedProductReportFieldSelectionViewModel FieldsPageModel { get; private set; }
        public ExcelReportInfoViewModel InfoPageModel { get; private set; }
        public ReportSource ReportSource { get; set; }
        public ObservableCollection<IExcelDumpCreator> ReportGenerators { get; private set; }
        public IExcelDumpCreator SelectedReportGenerator
        {
            get { return selectedReportGenerator; }
            set
            {
                selectedReportGenerator = value;
                UpdateReport();
                //SaveTemplate();
                RaisePropertyChanged("SelectedReportGenerator");
            }

        }

        void OnCancelReport()
        {
            Messenger.Default.Send<ShowMainViewMessage>(new ShowMainViewMessage());
        }

        ExcelQueryCriteria BuildCriteria()
        {
            criteria = new ExcelQueryCriteria();

            var czq = from cz in FieldsPageModel.SelectedCropZones
                      select cz.Id;
            criteria.SelectedCropZones.AddRange(czq);

            criteria.QueryStartDate = InfoPageModel.ReportStartDate;
            criteria.QueryEndDate = InfoPageModel.ReportEndDate.AddDays(1);
            criteria.IncludeField = InfoPageModel.IncludeFieldData;
            criteria.IncludeYield = InfoPageModel.IncludeYieldData;
            criteria.IncludeGuids = InfoPageModel.IncludeGUIDS;
            criteria.IncludePlans = InfoPageModel.IncludePlanData;
            criteria.IncludeSourceRecords = InfoPageModel.IncludeSourceRecords;
            criteria.IncludeInvoiceData = InfoPageModel.IncludeInvoiceData;
            criteria.IncludedColumns = InfoPageModel.ColumnVisibilityList.ToList();
            return criteria;
        }

        public void UpdateReport()
        {
            if (SelectedReportGenerator != null)
            {
                ExcelQueryCriteria criteria = BuildCriteria();
                ReportSource = SelectedReportGenerator.Generate(criteria);
                RaisePropertyChanged("ReportSource");
            }
        }

        void AddAppliedProductReportGenerators(IClientEndpoint clientEndpoint, int cropYear)
        {
            ReportGenerators.Add(new AppliedProductExcelDataDump(clientEndpoint, cropYear));
        }

        void SaveTemplate()
        {
            criteria = BuildCriteria();
            if (template != null && template.TemplateName == TemplateName)
            {
                ReportTemplateViewModel editedTemplate = new ReportTemplateViewModel()
                {
                    TemplateName = TemplateName,
                    Created = template.Created,
                    LastUsed = DateTime.Now,
                    CriteriaJSON = JsonConvert.SerializeObject(criteria),
                    ReportType = template.ReportType,
                    DataSourceId = template.DataSourceId
                };

                //TODO :: CALL TO EDIT PREVIOUS TEMPLATE...
                templateService.EditTemplate(editedTemplate);
            }
            else
            {
                ReportTemplateViewModel newTemplate = new ReportTemplateViewModel()
                {
                    TemplateName = TemplateName,
                    Created = DateTime.Now,
                    LastUsed = DateTime.Now,
                    CriteriaJSON = JsonConvert.SerializeObject(criteria),
                    ReportType = reportType,
                    DataSourceId = ApplicationEnvironment.CurrentDataSourceId
                };

                //TODO :: call service to save the new report template
                templateService.SaveNewTemplate(newTemplate);
                template = newTemplate;
            }

            RaisePropertyChanged("NewTemplateCreated", null, new ReportTemplateViewModel(), true);
        }

        void InitializeFromTemplate(ReportTemplateViewModel temp)
        {
            TemplateName = temp.TemplateName;

            ExcelQueryCriteria crit = JsonConvert.DeserializeObject<ExcelQueryCriteria>(temp.CriteriaJSON);

            InfoPageModel.IncludeFieldData = crit.IncludeField;
            InfoPageModel.IncludeGUIDS = crit.IncludeGuids;
            InfoPageModel.IncludeYieldData = crit.IncludeYield;
            InfoPageModel.ReportStartDate = crit.QueryStartDate;
            InfoPageModel.ReportEndDate = crit.QueryEndDate;
            InfoPageModel.ColumnVisibilityList =  new ObservableCollection<ColumnVisibilityViewModel>(crit.IncludedColumns.ToArray());

            var dsID = new DataSourceId(ApplicationEnvironment.CurrentDataSourceId);

            var RootTreeItemModels = FieldsPageModel.RootTreeItemModels;
            if (RootTreeItemModels.Any())
            {
                if (RootTreeItemModels.First() is GrowerTreeItemViewModel)
                {
                    var toCheck = from farm in RootTreeItemModels.First().Children
                                  from field in farm.Children
                                  from cz in field.Children
                                  where crit.SelectedCropZones.Contains(cz.Id)
                                  select cz;
                    toCheck.ForEach(x => x.IsChecked = true);
                }
                else
                {
                    var toCheck = from crop in RootTreeItemModels.First().Children
                                  from farm in crop.Children
                                  from field in farm.Children
                                  from cz in field.Children
                                  where crit.SelectedCropZones.Contains(cz.Id)
                                  select cz;
                    toCheck.ForEach(x => x.IsChecked = true);
                }
            }
        }

    }
}
