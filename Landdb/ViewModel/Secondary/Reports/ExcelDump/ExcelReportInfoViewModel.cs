﻿using GalaSoft.MvvmLight;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Secondary.Reports.ExcelDump
{
    public class ExcelReportInfoViewModel : ViewModelBase
    {
        DateTime reportStartDate;
        DateTime reportEndDate;
        bool fieldData;
        bool yieldData;
        bool sourceData;
        bool invoiceData;
        bool guidData;
        bool planData;
        UserSettings settings;
        IClientEndpoint endPoint;

        public ExcelReportInfoViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, int cropYear)
        {
            getPropertyNames();

            IncludeGUIDS = false;
            IncludeFieldData = true;
            IncludeYieldData = true;
            IncludeSourceRecords = true;
            IncludeInvoiceData = true;
            IncludePlanData = true;

            endPoint = clientEndpoint;
            settings = endPoint.GetUserSettings();

            if (settings.LastUsedReportStartDate != null && settings.LastUsedReportStartDate != new DateTime())
            {
                ReportStartDate = settings.LastUsedReportStartDate;
            }
            else
            {
                ReportStartDate = new DateTime(cropYear, 1, 1);
            }

            ReportEndDate = DateTime.Now;

            RaisePropertyChanged("IncludeGUIDS");
            RaisePropertyChanged("IncludeFieldData");
            RaisePropertyChanged("IncludeFieldData");
            RaisePropertyChanged("IncludePlanData");

        }


        public bool IncludeGUIDS
        {
            get { return guidData; }
            set
            {
                guidData = value;


                foreach (PropertyInfo info in typeof(Landdb.ReportModels.ExcelDump.ExcelDumpData.AppliedProductDumpData).GetProperties())
                {
                    if (info.PropertyType == typeof(Guid))
                    {
                        ColumnVisibilityList.SingleOrDefault(w => w.Header == Strings.ColumnHeader_AppliedProducts_Text).Children.Where(i => i.Name == info.Name).ForEach(p => p.Visibility = guidData);
                    }
                }
                int cnt = 0;
                foreach (PropertyInfo info in typeof(Landdb.ReportModels.ExcelDump.ExcelDumpData.AppliedProductDumpData).GetProperties())
                {
                    if (info.PropertyType == typeof(Guid))
                    {
                        cnt++;
                        var holder = ColumnVisibilityList.SingleOrDefault(w => w.Header == Strings.ColumnHeader_WorkOrderProducts_Text);
                        var issueArea = ColumnVisibilityList.SingleOrDefault(w => w.Header == Strings.ColumnHeader_WorkOrderProducts_Text).Children.Where(i => i.Name == info.Name);
                        ColumnVisibilityList.SingleOrDefault(w => w.Header == Strings.ColumnHeader_WorkOrderProducts_Text).Children.Where(i => i.Name == info.Name).ForEach(p => p.Visibility = guidData);
                    }
                }
                foreach (PropertyInfo info in typeof(Landdb.ReportModels.ExcelDump.ExcelDumpData.FieldLineItemData).GetProperties())
                {
                    if (info.PropertyType == typeof(Guid))
                    {
                        ColumnVisibilityList.SingleOrDefault(x => x.Name == Strings.ColumnHeader_Field_Text).Children.Where(i => i.Name == info.Name).ForEach(p => p.Visibility = guidData);
                    }
                }
                foreach (PropertyInfo info in typeof(Landdb.ReportModels.ExcelDump.ExcelDumpData.CropZoneYieldItemData).GetProperties())
                {
                    if (info.PropertyType == typeof(Guid))
                    {
                        ColumnVisibilityList.SingleOrDefault(x => x.Name == Strings.ColumnHeader_FieldToStorage_Text).Children.Where(i => i.Name == info.Name).ForEach(p => p.Visibility = guidData);
                    }
                }
                foreach (PropertyInfo info in typeof(Landdb.ReportModels.ExcelDump.ExcelDumpData.StorageToStorage).GetProperties())
                {
                    if (info.PropertyType == typeof(Guid))
                    {
                        ColumnVisibilityList.SingleOrDefault(x => x.Name == Strings.ColumnHeader_StorageToStorage_Text).Children.Where(i => i.Name == info.Name).ForEach(p => p.Visibility = guidData);
                    }
                }
                foreach (PropertyInfo info in typeof(Landdb.ReportModels.ExcelDump.ExcelDumpData.FieldToSale).GetProperties())
                {
                    if (info.PropertyType == typeof(Guid))
                    {
                        ColumnVisibilityList.SingleOrDefault(x => x.Name == Strings.ColumnHeader_FieldToSale_Text).Children.Where(i => i.Name == info.Name).ForEach(p => p.Visibility = guidData);
                    }
                }
                foreach (PropertyInfo info in typeof(Landdb.ReportModels.ExcelDump.ExcelDumpData.StorageToSale).GetProperties())
                {
                    if (info.PropertyType == typeof(Guid))
                    {
                        ColumnVisibilityList.SingleOrDefault(x => x.Name == Strings.ColumnHeader_StorageToSale_Text).Children.Where(i => i.Name == info.Name).ForEach(p => p.Visibility = guidData);
                    }
                }
                foreach (PropertyInfo info in typeof(Landdb.ReportModels.ExcelDump.ExcelDumpData.SourceDocumentItemData).GetProperties())
                {
                    if (info.PropertyType == typeof(Guid))
                    {
                        ColumnVisibilityList.SingleOrDefault(x => x.Name == Strings.ColumnHeader_Sources_Text).Children.Where(i => i.Name == info.Name).ForEach(p => p.Visibility = guidData);
                    }
                }
                foreach (PropertyInfo info in typeof(Landdb.ReportModels.ExcelDump.ExcelDumpData.InvoiceItemData).GetProperties())
                {
                    if (info.PropertyType == typeof(Guid))
                    {
                        ColumnVisibilityList.SingleOrDefault(x => x.Name == Strings.ColumnHeader_Invoices_Text).Children.Where(i => i.Name == info.Name).ForEach(p => p.Visibility = guidData);
                    }
                }
                foreach (PropertyInfo info in typeof(Landdb.ReportModels.ExcelDump.ExcelDumpData.PlannedProductDumpData).GetProperties())
                {
                    if (info.PropertyType == typeof(Guid))
                    {
                        ColumnVisibilityList.SingleOrDefault(x => x.Name == Strings.ColumnHeader_Plans_Text).Children.Where(i => i.Name == info.Name).ForEach(p => p.Visibility = guidData);
                    }
                }
                foreach (PropertyInfo info in typeof(Landdb.ReportModels.Plan.DetailProducts).GetProperties())
                {
                    if (info.PropertyType == typeof(Guid))
                    {
                        ColumnVisibilityList.SingleOrDefault(x => x.Name == Strings.ColumnHeader_PlanProductsByFieldDetail_Text).Children.Where(i => i.Name == info.Name).ForEach(p => p.Visibility = guidData);
                    }
                }

                //foreach (PropertyInfo info in typeof(Landdb.ReportModels.ExcelDump.ExcelDumpData.ExcelPlanInfoData).GetProperties())
                //{
                //    if (info.PropertyType == typeof(Guid))
                //    {
                //        ColumnVisibilityList.SingleOrDefault(x => x.Name == "Plan Info By Field Detail").Children.Where(i => i.Name == info.Name).ForEach(p => p.Visibility = guidData);
                //    }
                //}
            }
        }
        public bool IncludeFieldData
        {
            get { return fieldData; }
            set
            {
                fieldData = value;

                ColumnVisibilityList.SingleOrDefault(x => x.Name == Strings.ColumnHeader_Field_Text).Visibility = fieldData;
                ColumnVisibilityList.SingleOrDefault(x => x.Name == Strings.ColumnHeader_Field_Text).Enabled = fieldData;
                RaisePropertyChanged("IncludeFieldData");
                RaisePropertyChanged("ColumnVisibilityList");
            }

        }
        public bool IncludeYieldData
        {
            get { return yieldData; }
            set
            {
                yieldData = value;

                ColumnVisibilityList.SingleOrDefault(x => x.Name == Strings.ColumnHeader_FieldToStorage_Text).Visibility = yieldData;
                ColumnVisibilityList.SingleOrDefault(x => x.Name == Strings.ColumnHeader_FieldToStorage_Text).Enabled = yieldData;

                ColumnVisibilityList.SingleOrDefault(x => x.Name == Strings.ColumnHeader_FieldToStorage_Text).Visibility = yieldData;
                ColumnVisibilityList.SingleOrDefault(x => x.Name == Strings.ColumnHeader_FieldToStorage_Text).Enabled = yieldData;

                ColumnVisibilityList.SingleOrDefault(x => x.Name == Strings.ColumnHeader_FieldToSale_Text).Visibility = yieldData;
                ColumnVisibilityList.SingleOrDefault(x => x.Name == Strings.ColumnHeader_FieldToSale_Text).Enabled = yieldData;

                ColumnVisibilityList.SingleOrDefault(x => x.Name == Strings.ColumnHeader_StorageToSale_Text).Visibility = yieldData;
                ColumnVisibilityList.SingleOrDefault(x => x.Name == Strings.ColumnHeader_StorageToSale_Text).Enabled = yieldData;

                RaisePropertyChanged("IncludeYieldData");
                RaisePropertyChanged("ColumnVisibilityList");
            }
        }

        public bool IncludeSourceRecords
        {
            get { return sourceData; }
            set
            {
                sourceData = value;

                ColumnVisibilityList.SingleOrDefault(x => x.Name == Strings.ColumnHeader_Sources_Text).Visibility = sourceData;
                ColumnVisibilityList.SingleOrDefault(x => x.Name == Strings.ColumnHeader_Sources_Text).Enabled = sourceData;
                RaisePropertyChanged("IncludeSourceRecords");
                RaisePropertyChanged("ColumnVisibilityList");
            }
        }

        public bool IncludeInvoiceData
        {
            get { return invoiceData; }
            set
            {
                invoiceData = value;

                ColumnVisibilityList.SingleOrDefault(x => x.Name == Strings.ColumnHeader_Invoices_Text).Visibility = invoiceData;
                ColumnVisibilityList.SingleOrDefault(x => x.Name == Strings.ColumnHeader_Invoices_Text).Enabled = invoiceData;
                RaisePropertyChanged("IncludeInvoiceData");
                RaisePropertyChanged("ColumnVisibilityList");
            }
        }

        public bool IncludePlanData
        {
            get { return planData; }
            set
            {
                planData = value;

                ColumnVisibilityList.SingleOrDefault(x => x.Name == Strings.ColumnHeader_Plans_Text).Visibility = planData;
                ColumnVisibilityList.SingleOrDefault(x => x.Name == Strings.ColumnHeader_Plans_Text).Enabled = planData;
                RaisePropertyChanged("IncludePlanData");
                RaisePropertyChanged("ColumnVisibilityList");
            }
        }

        public ObservableCollection<ColumnVisibilityViewModel> ColumnVisibilityList { get; set; }

        public DateTime ReportStartDate
        {
            get { return reportStartDate; }
            set
            {
                reportStartDate = value;

                if (reportStartDate != settings.LastUsedReportStartDate)
                {
                    settings.LastUsedReportStartDate = reportStartDate;
                    endPoint.SaveUserSettings();
                }

                RaisePropertyChanged("ReportStartDate");
            }
        }

        public DateTime ReportEndDate
        {
            get { return reportEndDate; }
            set
            {
                reportEndDate = value;

                RaisePropertyChanged("ReportEndDate");
            }
        }

        void getPropertyNames()
        {
            ColumnVisibilityList = new ObservableCollection<ColumnVisibilityViewModel>();


            var propArray = typeof(Landdb.ReportModels.ExcelDump.ExcelDumpData.AppliedProductDumpData).GetProperties();

            ColumnVisibilityList.Add(new ColumnVisibilityViewModel() { Name = Strings.ColumnHeader_AppliedProducts_Text, Visibility = true, IsExpanded = false, IsSelected = false, Enabled = true, Children = new ObservableCollection<ColumnVisibilityViewModel>() });
            foreach (PropertyInfo infoApp in propArray)
            {
                ColumnVisibilityList.SingleOrDefault(x => x.Name == Strings.ColumnHeader_AppliedProducts_Text).Children.Add(new ColumnVisibilityViewModel() { Name = infoApp.Name, Visibility = true, Enabled = true });
            }
            ColumnVisibilityList.Add(new ColumnVisibilityViewModel() { Name = Strings.ColumnHeader_Field_Text, Visibility = true, IsExpanded = false, IsSelected = false, Enabled = true, Children = new ObservableCollection<ColumnVisibilityViewModel>() });
            foreach (PropertyInfo info in typeof(Landdb.ReportModels.ExcelDump.ExcelDumpData.FieldLineItemData).GetProperties())
            {
                ColumnVisibilityList.SingleOrDefault(x => x.Name == Strings.ColumnHeader_Field_Text).Children.Add(new ColumnVisibilityViewModel() { Name = info.Name, Visibility = true, Enabled = true });
            }
            ColumnVisibilityList.Add(new ColumnVisibilityViewModel() { Name = Strings.ColumnHeader_FieldToStorage_Text, Visibility = true, IsExpanded = false, IsSelected = false, Enabled = true, Children = new ObservableCollection<ColumnVisibilityViewModel>() });
            foreach (PropertyInfo info in typeof(Landdb.ReportModels.ExcelDump.ExcelDumpData.CropZoneYieldItemData).GetProperties())
            {
                ColumnVisibilityList.SingleOrDefault(x => x.Name == Strings.ColumnHeader_FieldToStorage_Text).Children.Add(new ColumnVisibilityViewModel() { Name = info.Name, Visibility = true, Enabled = true });
            }
            ColumnVisibilityList.Add(new ColumnVisibilityViewModel() { Name = Strings.ColumnHeader_StorageToStorage_Text, Visibility = true, IsExpanded = false, IsSelected = false, Enabled = true, Children = new ObservableCollection<ColumnVisibilityViewModel>() });
            foreach (PropertyInfo info in typeof(Landdb.ReportModels.ExcelDump.ExcelDumpData.StorageToStorage).GetProperties())
            {
                ColumnVisibilityList.SingleOrDefault(x => x.Name == Strings.ColumnHeader_StorageToStorage_Text).Children.Add(new ColumnVisibilityViewModel() { Name = info.Name, Visibility = true, Enabled = true });
            }
            ColumnVisibilityList.Add(new ColumnVisibilityViewModel() { Name = Strings.ColumnHeader_FieldToSale_Text, Visibility = true, IsExpanded = false, IsSelected = false, Enabled = true, Children = new ObservableCollection<ColumnVisibilityViewModel>() });
            foreach (PropertyInfo info in typeof(Landdb.ReportModels.ExcelDump.ExcelDumpData.FieldToSale).GetProperties())
            {
                ColumnVisibilityList.SingleOrDefault(x => x.Name == Strings.ColumnHeader_FieldToSale_Text).Children.Add(new ColumnVisibilityViewModel() { Name = info.Name, Visibility = true, Enabled = true });
            }
            ColumnVisibilityList.Add(new ColumnVisibilityViewModel() { Name = Strings.ColumnHeader_StorageToSale_Text, Visibility = true, IsExpanded = false, IsSelected = false, Enabled = true, Children = new ObservableCollection<ColumnVisibilityViewModel>() });
            foreach (PropertyInfo info in typeof(Landdb.ReportModels.ExcelDump.ExcelDumpData.StorageToSale).GetProperties())
            {
                ColumnVisibilityList.SingleOrDefault(x => x.Name == Strings.ColumnHeader_StorageToSale_Text).Children.Add(new ColumnVisibilityViewModel() { Name = info.Name, Visibility = true, Enabled = true });
            }
            ColumnVisibilityList.Add(new ColumnVisibilityViewModel() { Name = Strings.ColumnHeader_Sources_Text, Visibility = true, IsExpanded = false, IsSelected = false, Enabled = true, Children = new ObservableCollection<ColumnVisibilityViewModel>() });
            foreach (PropertyInfo info in typeof(Landdb.ReportModels.ExcelDump.ExcelDumpData.SourceDocumentItemData).GetProperties())
            {
                ColumnVisibilityList.SingleOrDefault(x => x.Name == Strings.ColumnHeader_Sources_Text).Children.Add(new ColumnVisibilityViewModel() { Name = info.Name, Visibility = true, Enabled = true });
            }
            ColumnVisibilityList.Add(new ColumnVisibilityViewModel() { Name = Strings.ColumnHeader_Invoices_Text, Visibility = true, IsExpanded = false, IsSelected = false, Enabled = true, Children = new ObservableCollection<ColumnVisibilityViewModel>() });
            foreach (PropertyInfo info in typeof(Landdb.ReportModels.ExcelDump.ExcelDumpData.InvoiceItemData).GetProperties())
            {
                ColumnVisibilityList.SingleOrDefault(x => x.Name == Strings.ColumnHeader_Invoices_Text).Children.Add(new ColumnVisibilityViewModel() { Name = info.Name, Visibility = true, Enabled = true });
            }
            ColumnVisibilityList.Add(new ColumnVisibilityViewModel() { Name = Strings.ColumnHeader_Plans_Text, Visibility = true, IsExpanded = false, IsSelected = false, Enabled = true, Children = new ObservableCollection<ColumnVisibilityViewModel>() });
            foreach (PropertyInfo info in typeof(Landdb.ReportModels.ExcelDump.ExcelDumpData.PlannedProductDumpData).GetProperties())
            {
                ColumnVisibilityList.SingleOrDefault(x => x.Name == Strings.ColumnHeader_Plans_Text).Children.Add(new ColumnVisibilityViewModel() { Name = info.Name, Visibility = true, Enabled = true });
            }
            ColumnVisibilityList.Add(new ColumnVisibilityViewModel() { Name = Strings.ColumnHeader_PlanProductsByFieldDetail_Text, Visibility = true, IsExpanded = false, IsSelected = false, Enabled = true, Children = new ObservableCollection<ColumnVisibilityViewModel>() });
            foreach (PropertyInfo info in typeof(Landdb.ReportModels.Plan.DetailProducts).GetProperties())
            {
                ColumnVisibilityList.SingleOrDefault(x => x.Name == Strings.ColumnHeader_PlanProductsByFieldDetail_Text).Children.Add(new ColumnVisibilityViewModel() { Name = info.Name, Visibility = true, Enabled = true });
            }
            var colVM = new ColumnVisibilityViewModel() { Name = Strings.ColumnHeader_WorkOrderProducts_Text, Visibility = true, IsExpanded = false, IsSelected = false, Enabled = true, Children = new ObservableCollection<ColumnVisibilityViewModel>() };
            ColumnVisibilityList.Add(colVM);
            foreach (PropertyInfo info in typeof(Landdb.ReportModels.ExcelDump.ExcelDumpData.WorkOrderProductDumpData).GetProperties())
            {
                ColumnVisibilityList.SingleOrDefault(x => x.Name == Strings.ColumnHeader_WorkOrderProducts_Text).Children.Add(new ColumnVisibilityViewModel() { Name = info.Name, Visibility = true, Enabled = true });
            }

            RaisePropertyChanged("ColumnVisibilityList");
        }
    }
}
