﻿using Landdb.Client.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using ExportToExcel;
using Landdb.ReportModels.ExcelDump;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Telerik.Reporting;
using Microsoft.Win32;
using Landdb.Domain.ReadModels.Yield;
using Landdb.Domain.ReadModels.Inventory;
using AgC.UnitConversion;
using Landdb.ViewModel.Overlays;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure.Messages;
using Landdb.Client.Services;
using System.Threading;
using System.Collections.Concurrent;
using Landdb.ViewModel.Secondary.Reports.ExcelDump;
using Landdb.Domain.ReadModels.Contract;
using Landdb.ViewModel.Secondary.Reports.ExcelDump.Generators;
using Landdb.Domain.ReadModels.Invoice;
using Landdb.Domain.ReadModels.YieldLocation;
using System.Text.RegularExpressions;
using Landdb.Domain.ReadModels.Plan;
using Landdb.Client.Services.Fertilizer;
using Landdb.ReportModels.Plan;
using Landdb.ValueObjects;
using Landdb.Domain.ReadModels.WorkOrder;
using Landdb.Domain.ReadModels.Shared;
using Landdb.Resources;
using Landdb.ReportServices;

namespace Landdb.ViewModel.Secondary.Reports.AppliedProduct.Generators
{
    public class AppliedProductExcelDataDump : IExcelDumpCreator
    {
        IClientEndpoint clientEndpoint;
        DateTime startDate;
        DateTime endDate;
        List<CropZoneId> selectedCropZones;
        int cropYear;
        bool detailedFieldInfo;
        bool detailedYieldInfo;
        bool detailedSourceInfo;
        bool detailedPlanInfo;
        bool includeGuids;
        string filename;
        List<ColumnVisibilityViewModel> includedColumns;
        List<ExcelDumpData.CropZoneYieldItemData> YieldItems;
        ProgressOverlayViewModel vm;
        IProgressTracker progress;

        int cnt;

        public AppliedProductExcelDataDump(IClientEndpoint endpoint, int cropYear)
        {
            this.clientEndpoint = endpoint;
            this.selectedCropZones = new List<CropZoneId>();
            this.cropYear = cropYear;
            vm = new ProgressOverlayViewModel();
            progress = vm;
            includedColumns = new List<ColumnVisibilityViewModel>();
        }

        public ReportSource Generate(ExcelQueryCriteria criteria)
        {
            startDate = criteria.QueryStartDate;
            endDate = criteria.QueryEndDate;
            detailedFieldInfo = criteria.IncludeField;
            detailedYieldInfo = criteria.IncludeYield;
            detailedSourceInfo = criteria.IncludeSourceRecords;
            detailedPlanInfo = criteria.IncludePlans;
            includeGuids = criteria.IncludeGuids;
            includedColumns = criteria.IncludedColumns;
            selectedCropZones.AddRange(criteria.SelectedCropZones);
            StartProcess();

            return null;
        }

        public void StartProcess()
        {
            ScreenDescriptor progressDesc = new ScreenDescriptor("Landdb.Views.Shared.Popups.ProgressPopUpView", "fullprogress", vm);
            Messenger.Default.Send<ShowPopupMessage>(new ShowPopupMessage() { ScreenDescriptor = progressDesc });

            Action StartGeneration;
            StartGeneration = () => GenerateReport();
            Thread t;
            t = new Thread(StartGeneration.Invoke);
            t.Start();
        }

        public void GenerateReport()
        {
            ExcelDumpData data = new ExcelDumpData();
            YieldItems = new List<ExcelDumpData.CropZoneYieldItemData>();
            List<DetailProducts> DetailedPlanProducts = new List<DetailProducts>();
            List<Landdb.ReportModels.ExcelDump.ExcelDumpData.ExcelPlanInfoData> DetailPlanInfo = new List<Landdb.ReportModels.ExcelDump.ExcelDumpData.ExcelPlanInfoData>();
            var flattenedHierarchy = clientEndpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new FlattenedTreeHierarchyView());
            var masterlist = clientEndpoint.GetMasterlistService();
            var inventoryItems = clientEndpoint.GetView<InventoryListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear));
            var items = inventoryItems.HasValue ? inventoryItems.Value : null;
            cnt = 0;

            var cropYearId = new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear);
            var value = clientEndpoint.GetView<CropZoneApplicationDataView>(cropYearId).GetValue(new CropZoneApplicationDataView());
            var allApplied = (from t in value.Items
                              where selectedCropZones.Contains(t.CropZoneId) && t.StartDate >= startDate && t.EndDate <= endDate
                              select t).ToList();

            var totalCount = selectedCropZones.Count() + allApplied.Count;

            Task Plans = Task.Factory.StartNew(() =>
            {
                List<ExcelDumpData.PlannedProductDumpData> planItems = GetPlannedProducts(masterlist);
                data.Plans.AddRange(planItems);
            });

            Task DetailPlannedProducts = Task.Factory.StartNew(() =>
            {
                List<DetailProducts> plannedProductItems = GetDetailedPlannedProducts(masterlist, flattenedHierarchy);
                DetailedPlanProducts = plannedProductItems;
            });

            //Task DetailPlanInfoTask = Task.Factory.StartNew(() =>
            //    {
            //        List<Landdb.ReportModels.ExcelDump.ExcelDumpData.ExcelPlanInfoData> planDetails = GetDetailedPlanInfo(masterlist, flattenedHierarchy);
            //        DetailPlanInfo = planDetails;
            //    });

            Task StorageToStorageTask = Task.Factory.StartNew(() =>
            {
                List<ExcelDumpData.StorageToStorage> storageToStorageItems = GetStorageToStorageLoads();
                data.StorageToStorageItems.AddRange(storageToStorageItems);
            });

            Task StorageToSaleTask = Task.Factory.StartNew(() =>
            {
                List<ExcelDumpData.StorageToSale> storageToSaleItems = GetStorageToSaleLoads();
                data.StorageToSaleItems.AddRange(storageToSaleItems);
            });

            Task appliedProductTask = Task.Factory.StartNew(() =>
            {
                List<ExcelDumpData.AppliedProductDumpData> products = GetAppliedProducts(allApplied, masterlist, items, flattenedHierarchy.Items);
                data.AppliedProducts.AddRange(products);
            });

            Task workOrderTask = Task.Factory.StartNew(() =>
            {
                var workOrdersMaybe = clientEndpoint.GetView<WorkOrderListView>(cropYearId);
                var workOrders = workOrdersMaybe.HasValue ? workOrdersMaybe.Value.WorkOrders : null;
                workOrders = workOrders != null ? workOrders.Where(x => x.StartDateTime >= startDate && x.StartDateTime <= endDate).ToList() : null;
                if(workOrders == null) { return; }
                List<ExcelDumpData.WorkOrderProductDumpData> products = GetWorkOrderProducts(workOrders, masterlist, items, flattenedHierarchy.Items, selectedCropZones);
                data.WorkOrderProducts.AddRange(products);
            });

            var uniqueAppIds = (from a in allApplied
                                select a.ApplicationId).Distinct();

            Task sourceItemTask = Task.Factory.StartNew(() =>
            {
                List<ExcelDumpData.SourceDocumentItemData> sources = GetSourceRecords(uniqueAppIds.ToList());
                data.SourceDocuments.AddRange(sources);
            });

            Task invoiceTask = Task.Factory.StartNew(() =>
            {
                List<ExcelDumpData.InvoiceItemData> invoices = GetInvoiceItems();
                data.Invoices.AddRange(invoices);
            });

            UpdateProgressMessage(progress, Strings.ProgressMessage_ProcessingRecords_Text);
            UpdateProgressTotalRecords(progress, totalCount);
            UpdateProgressCurrentRecord(progress, 0);

            var fieldsWithNoCZs = from f in flattenedHierarchy.Items
                                  where f.CropZoneId == null
                                  select f;
            var fertService = new FertilizerAccumulator(clientEndpoint, ApplicationEnvironment.CurrentDataSourceId, cropYear);
            foreach (var field in fieldsWithNoCZs)
            {
                var fieldInfo = new ExcelDumpData.FieldLineItemData();
                var fieldDetails = field.FieldId != null ? clientEndpoint.GetView<FieldDetailsView>(field.FieldId).Value : null;

                if (fieldDetails != null)
                {
                    var farmDetailsMaybe = clientEndpoint.GetView<FarmDetailsView>(field.FarmId);
                    if (farmDetailsMaybe.HasValue)
                    {
                        var farmDetails = farmDetailsMaybe.Value;
                        fieldInfo.Farm = farmDetails.Name;
                        fieldInfo.PermitId = string.IsNullOrEmpty(farmDetails.PermitId) ? fieldDetails.PermitId : farmDetails.PermitId;
                    }

                    //if (fieldDetails.BoundaryArea != null) fieldInfo.FieldArea = (decimal)fieldDetails.BoundaryArea;
                    //if (fieldDetails.ReportedArea != null) fieldInfo.FieldArea = (decimal)fieldDetails.ReportedArea;
                    fieldInfo.FieldArea = fieldDetails.ReportedArea != null ? (decimal)fieldDetails.ReportedArea : 0m;
                    fieldInfo.FieldBoundaryArea = fieldDetails.BoundaryArea != null ? (decimal)fieldDetails.BoundaryArea : 0m;
                    fieldInfo.Crop = string.Empty;
                    fieldInfo.CropZone = string.Empty;
                    //fieldInfo.CropZoneId = Nullable<Guid>;
                    fieldInfo.FarmId = field.FarmId.Id;

                    fieldInfo.FieldId = fieldDetails.Id.Id;
                    fieldInfo.Field = fieldDetails.FieldNameByCropYear[cropYear]; //.Name;

                    fieldInfo.CropYear = cropYear;
                    fieldInfo.CustomFieldId = fieldDetails.FieldID;
                    fieldInfo.FieldCenterLatLong = fieldDetails.CenterLatLong;
                    fieldInfo.CluId = fieldDetails.CluID;
                    fieldInfo.County = fieldDetails.County;
                    fieldInfo.FSAArea = fieldDetails.FsaArea != null ? (decimal)fieldDetails.FsaArea : 0;
                    fieldInfo.FSAFarm = fieldDetails.FsaFarmNumber;
                    fieldInfo.FSAField = fieldDetails.FsaFieldNumber;
                    fieldInfo.FSATract = fieldDetails.FsaTractNumber;
                    fieldInfo.Irrigated = fieldDetails.Irrigated;
                    fieldInfo.IrrigationSystem = fieldDetails.IrrigationSystem;
                    fieldInfo.LandOwner = fieldDetails.LandOwner != null ? fieldDetails.LandOwner.Name : string.Empty;
                    fieldInfo.LandOwnerPercent = (decimal)fieldDetails.LandLordPercent;
                    fieldInfo.LegalDesc = fieldDetails.LegalDescription;
                    fieldInfo.Meridian = fieldDetails.Meridian;
                    fieldInfo.FieldNotes = fieldDetails.Notes;
                    
                    fieldInfo.PumpEnergySource = fieldDetails.EnergySource;
                    fieldInfo.PumpLift = fieldDetails.PumpLift;
                    fieldInfo.PumpPressure = fieldDetails.PumpPressure;
                    fieldInfo.Range = fieldDetails.Range + fieldDetails.RangeDirection;
                    fieldInfo.RentType = fieldDetails.RentType;
                    fieldInfo.Section = fieldDetails.Section;
                    fieldInfo.ShortDesc = fieldDetails.ShortDescription;
					fieldInfo.SiteId = fieldDetails.SiteIdsByCropYear.ContainsKey(cropYear) ? fieldDetails.SiteIdsByCropYear[cropYear] : null;
                    fieldInfo.SlopeLength = fieldDetails.SlopeLength != null ? (decimal)fieldDetails.SlopeLength : 0;
                    fieldInfo.SlopePercent = fieldDetails.SlopePercent != null ? (decimal)fieldDetails.SlopePercent : 0;
                    fieldInfo.SoilTexture = fieldDetails.SoilTexture;
                    fieldInfo.State = fieldDetails.State;
                    fieldInfo.TaxId = fieldDetails.PropertyTaxId;
                    fieldInfo.TenantPercent = (decimal)fieldDetails.TenantPercent;
                    fieldInfo.Township = fieldDetails.Township + fieldDetails.TownshipDirection;
                    fieldInfo.WaterOrigin = fieldDetails.WaterOrigin;
                    fieldInfo.WaterSource = fieldDetails.WaterSource;
                    fieldInfo.CenterLatLong = fieldDetails.CenterLatLong;

                    data.Fields.Add(fieldInfo);
                }
            }

            foreach (var czID in selectedCropZones)
            {
                var czTreeData = (from f in flattenedHierarchy.Items
                                  where f.CropZoneId == czID
                                  select f).FirstOrDefault();

                //////Move this to its own thread
                var czYieldItems = GetFieldToStorageLoads(czID, cropYearId, czTreeData);
                YieldItems.AddRange(czYieldItems);
                /////////////////////////////////////////////

                //Move this to its own thread//
                Task fieldToSaleTask = Task.Factory.StartNew(() =>
                {
                    List<ExcelDumpData.FieldToSale> fieldToSaleItems = GetFieldToSaleLoads(czID, cropYearId, czTreeData);
                    data.FieldToSaleItems.AddRange(fieldToSaleItems);
                });
                ////////////////////////////////



                if (data.Fields.Where(x => x.CropZoneId == czID.Id).Count() == 0)
                {
                    var fieldInfo = new ExcelDumpData.FieldLineItemData();
                    var fieldDetails = clientEndpoint.GetView<FieldDetailsView>(czTreeData.FieldId).Value;
                    var czDetails = clientEndpoint.GetView<CropZoneDetailsView>(czID).Value;
                    var farmDetails = clientEndpoint.GetView<FarmDetailsView>(czTreeData.FarmId).Value;

                    var totalFertModel = new FertilizerFormulationModel();
                    Task totalFertTask = Task.Factory.StartNew(async () =>
                    {
                        totalFertModel = await fertService.CalculateTotalCropZoneFertilizer(czID, null, null);
                    });

                    fieldInfo.CzArea = czDetails.ReportedArea != null ? (decimal)czDetails.ReportedArea : 0m;
                    fieldInfo.CzArea = fieldInfo.CzArea == 0m && czDetails.BoundaryArea != null ? (decimal)czDetails.BoundaryArea : fieldInfo.CzArea;
                    fieldInfo.Crop = czTreeData.CropName;
                    fieldInfo.CropZone = czTreeData.CropZoneName;
                    fieldInfo.CropZoneId = czID.Id;
                    fieldInfo.FarmId = czTreeData.FarmId.Id;
                    fieldInfo.Farm = czTreeData.FarmName;
                    fieldInfo.FieldId = czTreeData.FieldId.Id;

                    fieldInfo.CropYear = czDetails.CropYears.FirstOrDefault();
                    fieldInfo.Field = czTreeData.FieldName;
                    fieldInfo.Tags = string.Join(" + ", czDetails.Tags.ToArray());
                    fieldInfo.CustomFieldId = fieldDetails.FieldID;
                    fieldInfo.FieldCenterLatLong = fieldDetails.CenterLatLong;
                    //fieldInfo.FieldArea = fieldDetails.ReportedArea != null ? (decimal)fieldDetails.ReportedArea : 0m;
                    //fieldInfo.FieldArea = fieldInfo.FieldArea == 0 && fieldDetails.BoundaryArea != null ? (decimal)fieldDetails.BoundaryArea : fieldInfo.FieldArea;
                    fieldInfo.FieldArea = fieldDetails.ReportedArea != null ? (decimal)fieldDetails.ReportedArea : 0m;
                    fieldInfo.FieldBoundaryArea = fieldDetails.BoundaryArea != null ? (decimal)fieldDetails.BoundaryArea : 0m;
                    fieldInfo.CluId = fieldDetails.CluID;
                    fieldInfo.County = fieldDetails.County;
                    fieldInfo.FSAArea = fieldDetails.FsaArea != null ? (decimal)fieldDetails.FsaArea : 0;
                    fieldInfo.FSAFarm = fieldDetails.FsaFarmNumber;
                    fieldInfo.FSAField = fieldDetails.FsaFieldNumber;
                    fieldInfo.FSATract = fieldDetails.FsaTractNumber;
                    fieldInfo.Irrigated = fieldDetails.Irrigated;
                    fieldInfo.IrrigationSystem = fieldDetails.IrrigationSystem;
                    fieldInfo.LandOwner = fieldDetails.LandOwner != null ? fieldDetails.LandOwner.Name : string.Empty;
                    fieldInfo.LandOwnerPercent = (decimal)fieldDetails.LandLordPercent;
                    fieldInfo.LegalDesc = fieldDetails.LegalDescription;
                    fieldInfo.Meridian = fieldDetails.Meridian;
                    fieldInfo.FieldNotes = fieldDetails.Notes;
                    fieldInfo.PermitId = string.IsNullOrEmpty(farmDetails.PermitId) ? fieldDetails.PermitId : farmDetails.PermitId;
                    fieldInfo.PumpEnergySource = fieldDetails.EnergySource;
                    fieldInfo.PumpLift = fieldDetails.PumpLift;
                    fieldInfo.PumpPressure = fieldDetails.PumpPressure;
                    fieldInfo.Range = fieldDetails.Range + fieldDetails.RangeDirection;
                    fieldInfo.RentType = fieldDetails.RentType;
                    fieldInfo.Section = fieldDetails.Section;
                    fieldInfo.ShortDesc = fieldDetails.ShortDescription;
					fieldInfo.SiteId = fieldDetails.SiteIdsByCropYear.ContainsKey(cropYear) ? fieldDetails.SiteIdsByCropYear[cropYear] : null;
                    fieldInfo.SlopeLength = fieldDetails.SlopeLength != null ? (decimal)fieldDetails.SlopeLength : 0;
                    fieldInfo.SlopePercent = fieldDetails.SlopePercent != null ? (decimal)fieldDetails.SlopePercent : 0;
                    fieldInfo.SoilTexture = fieldDetails.SoilTexture;
                    fieldInfo.State = fieldDetails.State;
                    fieldInfo.TaxId = fieldDetails.PropertyTaxId;
                    fieldInfo.TenantPercent = (decimal)fieldDetails.TenantPercent;
                    fieldInfo.Township = fieldDetails.Township + fieldDetails.TownshipDirection;
                    fieldInfo.WaterOrigin = fieldDetails.WaterOrigin;
                    fieldInfo.WaterSource = fieldDetails.WaterSource;

                    fieldInfo.Crop = masterlist.GetCrop(czDetails.CropId).Name;
                    fieldInfo.CropNotes = czDetails.Notes;
                    fieldInfo.CenterLatLong = czDetails.CenterLatLong;
                    fieldInfo.SeedLotId = czDetails.SeedLotId;
                    fieldInfo.SeedSource = czDetails.SeedSource;
                    fieldInfo.InsuranceId = czDetails.InsuranceId;
                    fieldInfo.CropZoneSiteId = czDetails.SiteId;
                    fieldInfo.CropZonePermitId = czDetails.PermitId;
                    //fieldInfo.ContractID = czDetails.con

                    totalFertTask.Wait();
                    fieldInfo.N = totalFertModel.N;
                    fieldInfo.P = totalFertModel.P;
                    fieldInfo.K = totalFertModel.K;
                    fieldInfo.Ca = totalFertModel.Ca;
                    fieldInfo.Mg = totalFertModel.Mg;
                    fieldInfo.S = totalFertModel.S;
                    fieldInfo.B = totalFertModel.B;
                    fieldInfo.Cl = totalFertModel.Cl;
                    fieldInfo.Cu = totalFertModel.Cu;
                    fieldInfo.Fe = totalFertModel.Fe;
                    fieldInfo.Mn = totalFertModel.Mn;
                    fieldInfo.Mo = totalFertModel.Mo;
                    fieldInfo.Zn = totalFertModel.Zn;

                    data.Fields.Add(fieldInfo);
                    fieldToSaleTask.Wait();
                }
                cnt++;
                UpdateProgressCurrentRecord(progress, cnt);
            }



            data.Loads.AddRange(YieldItems);

            //Send List to Data Table...Add DataTable to DataSet...do this for each Work Sheet you want.
            appliedProductTask.Wait();
            sourceItemTask.Wait();
            invoiceTask.Wait();
            StorageToStorageTask.Wait();
            StorageToSaleTask.Wait();
            Plans.Wait();
            DetailPlannedProducts.Wait();
            workOrderTask.Wait();
            //DetailPlanInfoTask.Wait();

            Task[] conversionTask = new Task[11];

            DataTable dt1 = new DataTable();
            DataTable dt2 = new DataTable();
            DataTable dt3 = new DataTable();
            DataTable dt4 = new DataTable();
            DataTable dt5 = new DataTable();
            DataTable YieldStoS = new DataTable();
            DataTable YieldFtoS = new DataTable();
            DataTable YieldStorageToSale = new DataTable();
            DataTable PlanTable = new DataTable();
            DataTable PlannedProductsTable = new DataTable();
            DataTable DetailPlansTable = new DataTable();
            DataTable WorkOrderTable = new DataTable();

            conversionTask[0] = Task.Factory.StartNew(() =>
            {
                List<string> colNames = (from i in includedColumns
                                         from j in i.Children
                                         where i.Name == Strings.ColumnHeader_AppliedProducts_Text && j.Visibility == true
                                         select j.Name).ToList();

                var groupedAndOrdered = data.AppliedProducts.OrderBy(m => m.FarmId).ThenBy(x => x.FieldId).ThenBy(t => t.CropZoneId).ThenBy(x => x.StartDate).ToList();
                dt1 = ExportToExcel.CreateExcelFile.ListToDataTable(groupedAndOrdered, includeGuids, colNames);
            });

            conversionTask[1] = Task.Factory.StartNew(() =>
            {
                List<string> colNames = (from i in includedColumns
                                         from j in i.Children
                                         where i.Name == Strings.ColumnHeader_Field_Text && j.Visibility == true
                                         select j.Name).ToList();

                var groupedAndOrdered = data.Fields.OrderBy(j => j.FarmId).ThenBy(i => i.FieldId).ThenBy(y => y.CropZone).ToList();
                dt2 = ExportToExcel.CreateExcelFile.ListToDataTable(groupedAndOrdered, includeGuids, colNames);
            });

            conversionTask[2] = Task.Factory.StartNew(() =>
            {
                List<string> colNames = (from i in includedColumns
                                         from j in i.Children
                                         where i.Name == Strings.ColumnHeader_FieldToStorage_Text && j.Visibility == true
                                         select j.Name).ToList();

                var groupedAndOrdered = data.Loads.OrderBy(x => x.CropZoneId).ThenBy(s => s.LoadDate).ToList();
                dt3 = ExportToExcel.CreateExcelFile.ListToDataTable(groupedAndOrdered, includeGuids, colNames);
            });

            conversionTask[3] = Task.Factory.StartNew(() =>
            {
                List<string> colNames = (from i in includedColumns
                                         from j in i.Children
                                         where i.Name == Strings.ColumnHeader_StorageToStorage_Text && j.Visibility == true
                                         select j.Name).ToList();

                var groupedAndOrdered = data.StorageToStorageItems.ToList();
                YieldStoS = ExportToExcel.CreateExcelFile.ListToDataTable(groupedAndOrdered, includeGuids, colNames);
            });

            conversionTask[4] = Task.Factory.StartNew(() =>
            {
                List<string> colNames = (from i in includedColumns
                                         from j in i.Children
                                         where i.Name == Strings.ColumnHeader_FieldToSale_Text && j.Visibility == true
                                         select j.Name).ToList();

                var groupedAndOrdered = data.FieldToSaleItems.ToList();
                YieldFtoS = ExportToExcel.CreateExcelFile.ListToDataTable(groupedAndOrdered, includeGuids, colNames);
            });

            conversionTask[5] = Task.Factory.StartNew(() =>
            {
                List<string> colNames = (from i in includedColumns
                                         from j in i.Children
                                         where i.Name == Strings.ColumnHeader_StorageToSale_Text && j.Visibility == true
                                         select j.Name).ToList();

                var groupedAndOrdered = data.StorageToSaleItems.ToList();
                YieldStorageToSale = ExportToExcel.CreateExcelFile.ListToDataTable(groupedAndOrdered, includeGuids, colNames);
            });

            conversionTask[6] = Task.Factory.StartNew(() =>
            {
                List<string> colNames = (from i in includedColumns
                                         from j in i.Children
                                         where i.Name == Strings.ColumnHeader_Sources_Text && j.Visibility == true
                                         select j.Name).ToList();

                var groupedAndOrdered = data.SourceDocuments.ToList();
                dt4 = ExportToExcel.CreateExcelFile.ListToDataTable(groupedAndOrdered, includeGuids, colNames);
            });

            conversionTask[7] = Task.Factory.StartNew(() =>
            {
                List<string> colNames = (from i in includedColumns
                                         from j in i.Children
                                         where i.Name == Strings.ColumnHeader_Invoices_Text && j.Visibility == true
                                         select j.Name).ToList();

                var groupedAndOrdered = data.Invoices.ToList();
                dt5 = ExportToExcel.CreateExcelFile.ListToDataTable(groupedAndOrdered, includeGuids, colNames);
            });
            conversionTask[8] = Task.Factory.StartNew(() =>
            {
                List<string> colNames = (from i in includedColumns
                                         from j in i.Children
                                         where i.Name == Strings.ColumnHeader_Plans_Text && j.Visibility == true
                                         select j.Name).ToList();

                var groupedAndOrdered = data.Plans.ToList();
                PlanTable = ExportToExcel.CreateExcelFile.ListToDataTable(groupedAndOrdered, includeGuids, colNames);
            });
            conversionTask[9] = Task.Factory.StartNew(() =>
            {
                List<string> colNames = (from i in includedColumns
                                         from j in i.Children
                                         where i.Name == Strings.ColumnHeader_PlanProductsByFieldDetail_Text && j.Visibility == true
                                         select j.Name).ToList();

                var groupedAndOrdered = DetailedPlanProducts.ToList();
                PlannedProductsTable = ExportToExcel.CreateExcelFile.ListToDataTable(groupedAndOrdered, includeGuids, colNames);
            });

            conversionTask[10] = Task.Factory.StartNew(() =>
            {
                List<string> colNames = (from i in includedColumns
                                         from j in i.Children
                                         where i.Name == Strings.ColumnHeader_WorkOrderProducts_Text && j.Visibility == true
                                         select j.Name).ToList();

                var groupedAndOrdered = data.WorkOrderProducts.OrderBy(wo => wo.StartDate).ThenBy(w => w.WorkOrderId).ThenBy(x => x.CropZoneId).ThenBy(p => p.Product).ToList();
                WorkOrderTable = ExportToExcel.CreateExcelFile.ListToDataTable(groupedAndOrdered, includeGuids, colNames);
            });

            #region
            //conversionTask[10] = Task.Factory.StartNew(() =>
            //{
            //    List<string> colNames = (from i in includedColumns
            //                             from j in i.Children
            //                             where i.Name == "Plan Info By Field Detail" && j.Visibility == true
            //                             select j.Name).ToList();

            //    var groupedAndOrdered = DetailPlanInfo.ToList();
            //    DetailPlansTable = ExportToExcel.CreateExcelFile.ListToDataTable(groupedAndOrdered, includeGuids, colNames);
            //});
            #endregion
            Task.WaitAll(conversionTask);

            dt2.TableName = Strings.ColumnHeader_Field_Text;
            dt1.TableName = Strings.ColumnHeader_AppliedProducts_Text;
            dt3.TableName = "Yield Field To Storage";
            YieldStoS.TableName = "Yield Storage To Storage";
            YieldFtoS.TableName = "Yield Field To Sale";
            YieldStorageToSale.TableName = "Yield Storage To Sale";
            dt4.TableName = "Source Records";
            dt5.TableName = Strings.ColumnHeader_Invoices_Text;
            PlanTable.TableName = Strings.ColumnHeader_Plans_Text;
            PlannedProductsTable.TableName = Strings.ColumnHeader_PlanProductsByFieldDetail_Text;
            WorkOrderTable.TableName = Strings.ColumnHeader_WorkOrderProducts_Text;
            //DetailPlansTable.TableName = "Plan Info By Field Detail";

            DataSet ds = new DataSet("excelDS");
            ds.Tables.Add(dt1);
            ds.Tables.Add(WorkOrderTable);
            if (detailedFieldInfo) ds.Tables.Add(dt2);
            if (detailedYieldInfo)
            {
                ds.Tables.Add(dt3);
                ds.Tables.Add(YieldStoS);
                ds.Tables.Add(YieldFtoS);
                ds.Tables.Add(YieldStorageToSale);
            }
            if (detailedSourceInfo) ds.Tables.Add(dt4);
            ds.Tables.Add(dt5);
            if (detailedPlanInfo) { ds.Tables.Add(PlanTable); 
                ds.Tables.Add(PlannedProductsTable); 
            //    ds.Tables.Add(DetailPlansTable); 
            }
            try
            {
                //open up file dialog to save file....
                //then call createexcelfile to create the excel...
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();

                saveFileDialog1.Filter = "Excel|*.xlsx";
                saveFileDialog1.FilterIndex = 2;
                saveFileDialog1.RestoreDirectory = true;

                Nullable<bool> resultSaved = saveFileDialog1.ShowDialog();
                bool completedTask = false;
                // Process save file dialog box results 
                if (resultSaved == true)
                {
                    // Save document 
                    filename = saveFileDialog1.FileName;
                    completedTask = CreateExcelFile.CreateExcelDocument(ds, filename);
                }

                //now open file....
                if (completedTask)
                {
                System.Diagnostics.Process.Start(filename);
                }

                Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
            }
            catch (Exception ex)
            {
                Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
                return;
            }
        }

        public List<ExcelDumpData.WorkOrderProductDumpData> GetWorkOrderProducts(List<WorkOrderListItem> woItems, IMasterlistService masterlist, InventoryListView inventoryItems, List<FlattenedTreeHierarchyItem> czTreeDataItem, List<CropZoneId> cropZones)
        {
            List<ExcelDumpData.WorkOrderProductDumpData> woProdItems = new List<ExcelDumpData.WorkOrderProductDumpData>();
            var dsID = ApplicationEnvironment.CurrentDataSourceId;
            //Task[] queTask = new Task[1];

            //ConcurrentQueue<WorkOrderListItem> uniqueWOs = new ConcurrentQueue<WorkOrderListItem>();

            //queTask[0] = Task.Factory.StartNew(() =>
            //{
            //    uniqueWOs = new ConcurrentQueue<WorkOrderListItem>(woItems.AsEnumerable());
            //});

            //ParallelOptions parallelOptions = new ParallelOptions();
            //parallelOptions.MaxDegreeOfParallelism = (System.Environment.ProcessorCount - 1);
            //if (parallelOptions.MaxDegreeOfParallelism < 1)
            //{
            //    parallelOptions.MaxDegreeOfParallelism = 1;
            //}

            var timingEventsObj = masterlist.GetTimingEventList();

            //Task.WaitAll(queTask);

            foreach(var item in woItems)
            {
                var woDetails = clientEndpoint.GetView<WorkOrderView>(item.Id).Value;

                foreach (var cz in woDetails.CropZones)
                {
                    if (cropZones.Contains(cz.Id))
                    {


                        var czDetails = clientEndpoint.GetView<CropZoneDetailsView>(cz.Id).Value;
                        var czArea = czDetails.BoundaryArea.HasValue ? czDetails.BoundaryArea.Value : 0;
                        czArea = czDetails.ReportedArea.HasValue && czDetails.ReportedArea.Value > 0 ? czDetails.ReportedArea.Value : czArea;

                        var czTreeItem = czTreeDataItem.FirstOrDefault(x => x.CropZoneId == cz.Id);
                        var multiplier = cz.Area.Value / woDetails.CropZones.Sum(s => s.Area.Value);
                        foreach (var prod in woDetails.Products)
                        {
                            var mlp = masterlist.GetProduct(prod.Id);
                            var inventoryProd = inventoryItems.Products[prod.Id];

                            var totalMeasure = UnitFactory.GetUnitByName(prod.TotalProductUnit).GetMeasure((double)prod.TotalProductValue, mlp.Density);

                            var totalAvgCostUnit = !string.IsNullOrEmpty(inventoryProd.AveragePriceUnit) ? UnitFactory.GetUnitByName(inventoryProd.AveragePriceUnit) : null;
                            var totalMeasureConverted = totalAvgCostUnit != null && totalMeasure.Unit != totalAvgCostUnit && totalMeasure.CanConvertTo(totalAvgCostUnit) ? totalMeasure.CreateAs(totalAvgCostUnit) : totalMeasure;
                            var totalAvgCost = totalMeasureConverted.Value * inventoryProd.AveragePriceValue;

                            var specificCostUnit = !string.IsNullOrEmpty(prod.SpecificCostUnit) ? UnitFactory.GetUnitByName(prod.SpecificCostUnit) : null;
                            totalMeasureConverted = specificCostUnit != null && totalMeasure.Unit != specificCostUnit && totalMeasure.CanConvertTo(specificCostUnit) ? totalMeasure.CreateAs(specificCostUnit) : totalMeasure;
                            var totalSpecificCost = prod.SpecificCostPerUnit != null ? totalMeasureConverted.Value * (double)prod.SpecificCostPerUnit : 0.0;
                            var totalFertModel = new FertilizerFormulationModel();
                            Task totalFertTask = Task.Factory.StartNew(async () =>
                            {
                                if (mlp.ProductType == "Fertilizer")
                                {
                                    var czAreaMeasure = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(czArea);
                                    var area = cz.Area.Value * (double)prod.PercentApplied;
                                    var areaMeasure = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(area);
                                    var fertService = new FertilizerAccumulator(clientEndpoint, ApplicationEnvironment.CurrentDataSourceId, cropYear);
                                    totalFertModel = await fertService.CalculateFertilizerAnalysis(prod.Id, czAreaMeasure, areaMeasure, (double)prod.RateValue, prod.RateUnit);
                                }
                                else
                                {
                                    totalFertModel = null;
                                }
                            });

                            double? Rei;
                            double? Phi;
                            double rei = 0.0;
                            double phi = 0.0;

                            var productService = new ProductServices(clientEndpoint);
                            ReiPhi reiPhi = productService.ReturnReiPhi(prod.Id, czDetails.CropId);

                            if (reiPhi != null)
                            {
                                rei = reiPhi.Rei.HasValue && reiPhi.Rei.Value > rei ? reiPhi.Rei.Value : rei;
                                phi = reiPhi.Phi.HasValue && reiPhi.Phi.Value > phi ? reiPhi.Phi.Value : phi;
                            }


                            Rei = rei;
                            Phi = phi;

                            //double? Rei;
                            //double? Phi;

                            //List<CropId> cropIds = new List<CropId>();
                            //cropIds.Add(czDetails.CropId);

                            //var parentCropIds = (from cid in cropIds
                            //                     let mlCrop = masterlist.GetCrop(cid)
                            //                     where mlCrop.ParentId.HasValue
                            //                     select new CropId(mlCrop.ParentId.Value)).ToList();
                            //cropIds.AddRange(parentCropIds);
                            //List<ReiPhi> newReiPhis = new List<ReiPhi>();
                            //var ReiPhis = masterlist.GetProductReiPhis(prod.Id); //  uniqueProds.Where(x => x.ProductId == prod.Id).FirstOrDefault().REIPHIS;
                            //foreach (var reiphi in ReiPhis)
                            //{
                            //    if (cropIds.Any(c => c.Id == reiphi.Crop))
                            //    {
                            //        newReiPhis.Add(reiphi);
                            //    }
                            //}

                            //if (newReiPhis != null && newReiPhis.Count() > 0)
                            //{
                            //    Rei = (from r in newReiPhis select r.Rei).Max();
                            //    Phi = (from r in newReiPhis select r.Phi).Max();
                            //    //prodLineItem.ReiDate = item.EndDate.AddHours((double)(from r in newReiPhis select r.Rei).Max()).ToString("g");
                            //    //prodLineItem.PhiDate = item.EndDate.AddDays((double)(from r in newReiPhis select r.Phi).Max()).ToString("g");
                            //}
                            //else
                            //{
                            //    Rei = null;
                            //    Phi = null;
                            //    //prodLineItem.ReiDate = string.Empty;
                            //    //prodLineItem.PhiDate = string.Empty;
                            //}

                            string actives = string.Empty;
                            if (mlp.ProductType == GlobalStrings.ProductType_CropProtection)
                            {
                                //string.Join(" + ", mlp.ActiveIngredients.Select(x => { return masterlist.GetActiveIngredientDisplay(new ActiveIngredientId(x.Id)); })) : string.Empty
                                List<string> activesList = new List<string>();
                                foreach (var ai in mlp.ActiveIngredients)
                                {
                                    string currentActive = masterlist.GetActiveIngredientDisplay(new ActiveIngredientId(ai.Id));
                                    activesList.Add(currentActive);
                                }

                                actives = string.Join(" + ", activesList);
                            }

                            totalFertTask.Wait();

                            var woProd = new ExcelDumpData.WorkOrderProductDumpData()
                            {
                                WorkOrderId = item.Id.Id,
                                ApplicationMethod = prod.ApplicationMethod,
                                WorkOrderName = woDetails.Name,
                                AreaApplied = (decimal)cz.Area.Value * prod.PercentApplied,
                                AuthorizedDate = woDetails.Authorization != null ? woDetails.Authorization.AuthorizationDate : new DateTime(),
                                Authorizer = woDetails.Authorization != null && !string.IsNullOrEmpty(woDetails.Authorization.PersonName) ? woDetails.Authorization.PersonName : string.Empty,
                                AuthorizerId = woDetails.Authorization != null && woDetails.Authorization.PersonId != null ? woDetails.Authorization.PersonId.Id : new Guid(),
                                Category = mlp.Category,
                                CostPerAppliedArea = (decimal)(totalAvgCost / woDetails.TotalArea.Value),
                                CostPerCropZoneArea = ((decimal)totalAvgCost * (decimal)(cz.Area.Value / woDetails.CropZones.Sum(x => x.Area.Value))) / (decimal)cz.Area.Value,
                                Crop = czTreeItem.CropName,
                                CropYear = woDetails.CropYear,
                                CropId = czTreeItem.CropId.Id,
                                CropZoneId = cz.Id.Id,
                                CropZoneArea = (decimal)czArea,
                                CropZoneName = cz.Name,
                                FarmId = czTreeItem.FarmId.Id,
                                FarmName = czTreeItem.FarmName,
                                FieldId = czTreeItem.FieldId.Id,
                                FieldName = czTreeItem.FieldName,
                                Manufacturer = mlp.Manufacturer,
                                Product = mlp.Name,
                                ProductId = mlp.Id,
                                ClientId = dsID,
                                GrowerCost = (prod.GrowerShare * (decimal)totalAvgCost) * (decimal)multiplier,
                                SpecificCostPerArea = (decimal)totalSpecificCost / (decimal)woDetails.CropZones.Sum(y => y.Area.Value),
                                GrowerSharePercent = prod.GrowerShare,
                                GrowerShareQty = (prod.TotalProductValue * (decimal)multiplier) * prod.GrowerShare,
                                OwnerShareQty = (prod.TotalProductValue * (decimal)multiplier) * (1m - prod.GrowerShare),
                                OwnerCost = ((1m - prod.GrowerShare) * (decimal)totalAvgCost) * (decimal)multiplier,
                                StartDate = woDetails.StartDateTime.ToString(),
                                RateValue = prod.RateValue,
                                RateUnit = !string.IsNullOrEmpty(prod.RateUnit) ? UnitFactory.GetUnitByName(prod.RateUnit).AbbreviatedDisplay : string.Empty,
                                RegNumber = mlp.RegistrationNumber,
                                TotalProductQty = prod.TotalProductValue * (decimal)multiplier,
                                TotalAverageCost = (decimal)totalAvgCost,
                                TotalSpecificCost = (decimal)totalSpecificCost,
                                TotalProductUnit = !string.IsNullOrEmpty(prod.TotalProductUnit) ? UnitFactory.GetUnitByName(prod.TotalProductUnit).AbbreviatedDisplay : string.Empty,
                                Type = mlp.ProductType,
                                B = totalFertModel != null ? totalFertModel.B : 0m,
                                Ca = totalFertModel != null ? totalFertModel.Ca : 0m,
                                Cl = totalFertModel != null ? totalFertModel.Cl : 0m,
                                Cu = totalFertModel != null ? totalFertModel.Cu : 0m,
                                Fe = totalFertModel != null ? totalFertModel.Fe : 0m,
                                K = totalFertModel != null ? totalFertModel.K : 0m,
                                Mg = totalFertModel != null ? totalFertModel.Mg : 0m,
                                Mn = totalFertModel != null ? totalFertModel.Mn : 0m,
                                Mo = totalFertModel != null ? totalFertModel.Mo : 0m,
                                N = totalFertModel != null ? totalFertModel.N : 0m,
                                P = totalFertModel != null ? totalFertModel.P : 0m,
                                S = totalFertModel != null ? totalFertModel.S : 0m,
                                Zn = totalFertModel != null ? totalFertModel.Zn : 0m,
                                PercentOfCropZoneArea = czArea != 0.0 ? (decimal)((cz.Area.Value * (double)prod.PercentApplied) / czArea) : 1,
                                TargetPest = prod.TargetPest != null ? prod.TargetPest.CommonName : string.Empty,
                                TimingEvent = woDetails.TimingEvent,
                                TimingEventOrder = timingEventsObj != null && !string.IsNullOrEmpty(woDetails.TimingEvent) ? timingEventsObj.FirstOrDefault(x => x.Name == woDetails.TimingEvent).Order : 0,
                                Rei = Rei.HasValue ? Rei.Value.ToString() : "*",
                                Phi = Phi.HasValue ? Phi.Value.ToString() : "*",
                                ReiDate = Rei.HasValue ? woDetails.StartDateTime.AddHours(Rei.Value).ToString("g") : string.Empty,
                                PhiDate = Phi.HasValue ? woDetails.StartDateTime.AddDays(Phi.Value).ToString("g") : string.Empty,
                                ActiveIngredient = actives,

                            };

                            woProdItems.Add(woProd);
                        }
                    }
                }
            }

            
            return woProdItems;
        }

        public List<ExcelDumpData.AppliedProductDumpData> GetAppliedProducts(List<CropZoneApplicationDataItem> czAppliedItems, IMasterlistService masterlist, InventoryListView inventoryItems, List<FlattenedTreeHierarchyItem> czTreeDataItems)
        {
            ConcurrentQueue<ExcelDumpData.AppliedProductDumpData> prodItems = new ConcurrentQueue<ExcelDumpData.AppliedProductDumpData>();
            var dsID = ApplicationEnvironment.CurrentDataSourceId;
            Task[] queTask = new Task[2];

            ConcurrentQueue<UniqueProducts> uniqueProds = new ConcurrentQueue<UniqueProducts>();
            ConcurrentQueue<UniqueApplications> uniqueApps = new ConcurrentQueue<UniqueApplications>();

            queTask[0] = Task.Factory.StartNew(() =>
            {
                var uniqueProdIDList = (from p in czAppliedItems
                                        select new { ProductID = p.ProductId }).Distinct();

                uniqueProds = new ConcurrentQueue<UniqueProducts>(uniqueProdIDList.Select(x => new UniqueProducts { ProductId = x.ProductID, MasterListProduct = masterlist.GetProduct(x.ProductID), REIPHIS = masterlist.GetProductReiPhis(x.ProductID).ToList() }));
            });

            queTask[1] = Task.Factory.StartNew(() =>
            {
                var uniqueAppsID = (from a in czAppliedItems
                                    select new { AppID = a.ApplicationId }).Distinct();

                uniqueApps = new ConcurrentQueue<UniqueApplications>(uniqueAppsID.Select(x => new UniqueApplications { AppID = x.AppID, AppDetails = clientEndpoint.GetView<ApplicationView>(x.AppID).Value }));
            });

            ParallelOptions parallelOptions = new ParallelOptions();
            parallelOptions.MaxDegreeOfParallelism = (System.Environment.ProcessorCount - 1);
            if (parallelOptions.MaxDegreeOfParallelism < 1)
            {
                parallelOptions.MaxDegreeOfParallelism = 1;
            }

            Task.WaitAll(queTask);

            Parallel.ForEach(czAppliedItems, parallelOptions, item =>
            {

                var czTreeData = (from i in czTreeDataItems
                                  where i.CropZoneId == item.CropZoneId
                                  select i).FirstOrDefault();

                var prodLineItem = new ExcelDumpData.AppliedProductDumpData();

                prodLineItem.ClientId = dsID;
                var applicationDetails = uniqueApps.Where(x => x.AppID == item.ApplicationId).FirstOrDefault().AppDetails; // endpoint.GetView<ApplicationView>(item.ApplicationId).Value;
                prodLineItem.Duration = applicationDetails.Duration.ToString();

                //Authorizer
                var authorization = applicationDetails.Authorization;

                prodLineItem.AuthorizedDate = authorization != null ? authorization.AuthorizationDate : new DateTime();
                prodLineItem.Authorizer = authorization != null ? authorization.PersonName : string.Empty;
                prodLineItem.AuthorizerId = authorization != null && authorization.PersonId != null ? authorization.PersonId.Id : Guid.Empty;
                ////
                //List<string> applicatorStrings = new List<string>();

                //foreach (var applicator in applicationDetails.Applicators)
                //{
                //    var personDetails = endpoint.GetView<PersonDetailsView>(applicator.PersonId);
                //    var applicatorString = string.Format("{0} :: {1} :: {2}", applicator.CompanyName, personDetails.Value.Name, personDetails.Value.ApplicatorLicenseNumber);
                //    applicatorStrings.Add(applicatorString);
                //}

                //prodLineItem.Applicators = applicationDetails.Applicators.Any() ? string.Join(" + ", applicatorStrings) : string.Empty;


                var applicatorArray = from a in applicationDetails.Applicators
                                      select string.Format("{0} :: {1}", a.CompanyName, a.PersonName);
                prodLineItem.Applicators = applicationDetails.Applicators.Any() ? string.Join(" + ", applicatorArray) : string.Empty;

                ////Weather
                var condtions = applicationDetails.Conditions;

                if (condtions != null)
                {
                    prodLineItem.Direction = condtions.WindDirection != null ? condtions.WindDirection : string.Empty;
                    prodLineItem.Temp = (decimal)(condtions.TemperatureValue != null ? condtions.TemperatureValue : 0);
                    prodLineItem.Humidity = (decimal)(condtions.HumidityPercent != null ? condtions.HumidityPercent : 0);
                    prodLineItem.SoilCondition = condtions.SoilMoisture != null ? condtions.SoilMoisture : string.Empty;
                    prodLineItem.SkyCondition = condtions.SkyCondition != null ? condtions.SkyCondition : string.Empty;
                    prodLineItem.Wind = (decimal)(condtions.WindSpeedValue != null ? condtions.WindSpeedValue : 0);
                }
                //

                var masterlistProd = uniqueProds.Where(x => x.ProductId == item.ProductId).FirstOrDefault().MasterListProduct;

                string actives = string.Empty;
                if (masterlistProd.ProductType == GlobalStrings.ProductType_CropProtection)
                {
                    //string.Join(" + ", mlp.ActiveIngredients.Select(x => { return masterlist.GetActiveIngredientDisplay(new ActiveIngredientId(x.Id)); })) : string.Empty
                    List<string> activesList = new List<string>();
                    foreach (var ai in masterlistProd.ActiveIngredients)
                    {
                        string currentActive = masterlist.GetActiveIngredientDisplay(new ActiveIngredientId(ai.Id));
                        activesList.Add(currentActive);
                    }

                    actives = string.Join(" + ", activesList);
                }

                prodLineItem.Manufacturer = masterlistProd.Manufacturer;
                prodLineItem.Product = masterlistProd.Name;
                prodLineItem.Category = masterlistProd.ProductType == GlobalStrings.ProductType_CropProtection ? masterlistProd.Category : string.Empty;
                prodLineItem.Type = masterlistProd.ProductType;
                prodLineItem.ActiveIngredient = actives;
                var czDetails = clientEndpoint.GetView<CropZoneDetailsView>(czTreeData.CropZoneId).Value;
                var czArea = czDetails.BoundaryArea.HasValue ? czDetails.BoundaryArea.Value : 0;
                czArea = czDetails.ReportedArea.HasValue && czDetails.ReportedArea.Value > 0 ? czDetails.ReportedArea.Value : czArea;
                var totalFertModel = new FertilizerFormulationModel();

                Task totalFertTask = Task.Factory.StartNew(async () =>
                {
                    if (masterlistProd.ProductType == "Fertilizer")
                    {
                        var czAreaMeasure = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(czArea);
                        var area = item.AreaValue * item.CoveragePercent;
                        var appAreaMeasure = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(area);
                        var fertService = new FertilizerAccumulator(clientEndpoint, ApplicationEnvironment.CurrentDataSourceId, cropYear);
                        totalFertModel = await fertService.CalculateFertilizerAnalysis(item.ProductId, czAreaMeasure, appAreaMeasure, (double)item.RateValue, item.RateUnit);
                    }
                    else
                    {
                        totalFertModel = null;
                    }
                });
                
                
                var registrationNumber = masterlistProd.RegistrationNumber;

                if (registrationNumber == null) { prodLineItem.RegNumber = string.Empty; }
                else
                {
                    Regex epaNumberRegex = new Regex("([1-9][0-9]*)");

                    var matches = from m in epaNumberRegex.Matches(registrationNumber).Cast<Match>()
                                  select m.Value;
                    prodLineItem.RegNumber = string.Join("-", matches.ToArray());
                }

                //Crop Zone Info
                prodLineItem.FarmId = item.FarmId.Id;
                prodLineItem.FarmName = czTreeData.FarmName;
                prodLineItem.FieldId = item.FieldId.Id;
                prodLineItem.FieldName = czTreeData.FieldName;
                prodLineItem.CropZoneId = item.CropZoneId.Id;
                prodLineItem.CropZoneName = czTreeData.CropZoneName;
                
                prodLineItem.Tags = string.Join(" + ", czDetails.Tags.ToArray());
                prodLineItem.Crop = masterlist.GetCrop(czTreeData.CropId).Name;
                prodLineItem.CropId = czTreeData.CropId.Id;

                //Application Info
                prodLineItem.ApplicationId = item.ApplicationId.Id;
                prodLineItem.ApplicationName = applicationDetails.Name;
                prodLineItem.StartDate = item.StartDate.ToLocalTime().ToString("MM/dd/yy hh:mm tt");
                prodLineItem.EndDate = item.EndDate.ToLocalTime().ToString("MM/dd/yy hh:mm tt");
                //

                //Product Info
                var prodDetails = applicationDetails.Products.Where(x => x.Id == item.ProductId).FirstOrDefault();

                var coverage = item.CoveragePercent == null || item.CoveragePercent == 0.0 ? 1.0 : item.CoveragePercent;
                prodLineItem.AreaApplied = (decimal)(item.AreaValue * coverage);

                prodLineItem.CropZoneArea = (decimal)czArea;
                prodLineItem.PercentOfCropZoneArea = czArea != 0.0 ? prodLineItem.AreaApplied / (decimal)czArea : 1;
                var multiplier = (applicationDetails.TotalArea.Value * coverage) != 0 ? (item.AreaValue * coverage) / (applicationDetails.TotalArea.Value * coverage) : 0;
                var displayRate = DisplayMeasureConverter.ConvertToRateDisplayMeasure(item.ProductId, item.RateValue, item.RateUnit, masterlistProd.Density, clientEndpoint, ApplicationEnvironment.CurrentDataSourceId);
                var displayTotal = DisplayMeasureConverter.ConvertToTotalDisplayMeasure(item.ProductId, prodDetails.TotalProductValue * (decimal)multiplier, item.TotalProductUnit, masterlistProd.Density, clientEndpoint, ApplicationEnvironment.CurrentDataSourceId);

                prodLineItem.ProductId = item.ProductId.Id;
                prodLineItem.RateUnit = displayRate.Unit.AbbreviatedDisplay; // item.RateUnit;
                prodLineItem.RateValue = (decimal)displayRate.Value; // (decimal)item.RateValue;
                prodLineItem.TotalProductUnit = displayTotal.Unit.AbbreviatedDisplay; // item.TotalProductUnit;
                prodLineItem.TotalProductQty = (decimal)displayTotal.Value; // (decimal)item.TotalProductValue;

                prodLineItem.CropYear = applicationDetails.CropYear;
                prodLineItem.ApplicationMethod = prodDetails.ApplicationMethod;
                prodLineItem.TargetPest = prodDetails.TargetPest != null ? prodDetails.TargetPest.CommonName : string.Empty;
                prodLineItem.Rup = masterlistProd.RestrictedUse;
                prodLineItem.GrowerSharePercent = prodDetails.GrowerShare;
                prodLineItem.GrowerShareQty = prodLineItem.TotalProductQty * prodDetails.GrowerShare;
                prodLineItem.OwnerShareQty = prodLineItem.TotalProductQty - (prodLineItem.TotalProductQty * prodDetails.GrowerShare);
                prodLineItem.ProductShareUnit = prodLineItem.TotalProductUnit;
                prodLineItem.TimingEvent = applicationDetails.TimingEvent;
                var timingEventObj = !string.IsNullOrEmpty(applicationDetails.TimingEvent) ? masterlist.GetTimingEventList().FirstOrDefault(x => x.Name == applicationDetails.TimingEvent) : null;
                prodLineItem.TimingEventOrder = timingEventObj != null ? timingEventObj.Order + 1 : 0;

                try
                {
                    var sources = applicationDetails.Sources.Where(x => x.DocumentType == "Invoice");
                    decimal totalInvoiceCost = 0m;
                    decimal totalInvoiceProduct = 0m;
                    string totalInvoiceProductUnit = string.Empty;
                    foreach (var inv in sources)
                    {
                        var invDetail = inv.Identity.Id != new Guid() ? clientEndpoint.GetView<InvoiceView>(new InvoiceId(inv.Identity.DataSourceId, inv.Identity.Id)).Value : null;
                        var products = invDetail != null ? invDetail.Products.Where(p => p.ProductId == item.ProductId) : new List<InvoiceProduct>();
                        IUnit commonUnit = null;
                        foreach (var product in products)
                        {
                            var mlp = masterlist.GetProduct(product.ProductId);
                            if (commonUnit == null)
                            {
                                commonUnit = UnitFactory.GetUnitByName(product.TotalProductUnit);
                            }

                            var currentUnit = UnitFactory.GetUnitByName(product.TotalProductUnit);
                            var productMeasure = currentUnit.GetMeasure((double)product.TotalProductValue, mlp.Density);
                            var convertedMeasure = productMeasure.CreateAs(commonUnit);
                            totalInvoiceCost += product.TotalCost;
                            totalInvoiceProduct += (decimal)convertedMeasure.Value;
                            totalInvoiceProductUnit = convertedMeasure.Unit.Name;
                        }
                    }
                    //NEED TO CONVERT TOTAL PRODUCT TO THE SAME UNIT AS THE INVOICE
                    var unit = UnitFactory.GetUnitByName(totalInvoiceProductUnit);
                    var totalProduct = UnitFactory.GetUnitByName(item.TotalProductUnit).GetMeasure(item.TotalProductValue, masterlistProd.Density);
                    var convertedValue = totalProduct.GetValueAs(unit);
                    prodLineItem.TotalInvoiceCost = (decimal)convertedValue * (totalInvoiceCost / totalInvoiceProduct);
                    prodLineItem.InvoiceCostPerArea = ((decimal)convertedValue * (totalInvoiceCost / totalInvoiceProduct)) / (decimal)(item.CoveragePercent * item.AreaValue);
                }
                catch
                {
                    // prodLineItem.TotalInvoiceCost = 0m;                
                }

                try
                {
					if (!string.IsNullOrWhiteSpace(prodDetails.SpecificCostUnit)) {
						var totalMeasure = UnitFactory.GetUnitByName(item.TotalProductUnit).GetMeasure(item.TotalProductValue, masterlistProd.Density);
						var specificUnit = UnitFactory.GetUnitByName(prodDetails.SpecificCostUnit);
						var converted = totalMeasure.CanConvertTo(specificUnit) ? totalMeasure.GetValueAs(specificUnit) : totalMeasure.Value;
						prodLineItem.TotalSpecificCost = (decimal)prodDetails.SpecificCostPerUnit * (decimal)converted;
						prodLineItem.SpecificCostPerArea = ((decimal)prodDetails.SpecificCostPerUnit * (decimal)converted) / (decimal)(item.AreaValue * item.CoveragePercent);
					}
                }
                catch (Exception x) { }

                if (inventoryItems != null)
                {
                    var prod = inventoryItems.Products.ContainsKey(item.ProductId) ? inventoryItems.Products[item.ProductId] : null;

                    if (prod != null && prod.AveragePriceUnit != null)
                    {
                        //get price per unit of product
                        var totalMeasure = displayTotal;
                        var avgPriceUnit = UnitFactory.GetPackageSafeUnit(prod.AveragePriceUnit, masterlistProd.StdUnit, masterlistProd.StdFactor, masterlistProd.StdPackageUnit);
                        var convertedMeasure = totalMeasure.CanConvertTo(avgPriceUnit) && totalMeasure.Unit != avgPriceUnit ? totalMeasure.GetValueAs(avgPriceUnit) : totalMeasure.Value;
                        var contractId = czDetails.Contracts.Any() ? czDetails.Contracts.FirstOrDefault() : null;
                        double margin = 0.0;
                        bool fromInventory = true;
                        if (contractId != null)
                        {
                            var contractDetailsMaybe = clientEndpoint.GetView<RentContractDetailsView>(contractId.Id);

                            if (contractDetailsMaybe.HasValue)
                            {
                                var contract = contractDetailsMaybe.Value;
                                switch( masterlistProd.ProductType.ToLower() ){
                                    case "cropprotection":
                                        margin = contract.IncludedInventoryInfo.CropProtectionMargin;
                                        fromInventory = contract.IncludedInventoryInfo.IsCropProtectionFromInventory;
                                        break;
                                    case "fertilizer":
                                        margin = contract.IncludedInventoryInfo.FertilizerMargin;
                                        fromInventory = contract.IncludedInventoryInfo.IsFertilizerFromInventory;
                                        break;
                                    case "service":
                                        margin = contract.IncludedInventoryInfo.ServiceMargin;
                                        fromInventory = contract.IncludedInventoryInfo.IsServiceFromInventory;
                                        break;
                                    case "seed":
                                        margin = contract.IncludedInventoryInfo.SeedMargin;
                                        fromInventory = contract.IncludedInventoryInfo.IsSeedFromInventory;
                                        break;
                                }
                            }
                        }
                        prodLineItem.AverageCostPerTotalProductUnit = (decimal)prod.AveragePriceValue;
                        prodLineItem.AverageTotalProductUnit = prod.AveragePriceUnit;
                        prodLineItem.TotalAverageCost = (decimal)(convertedMeasure * prod.AveragePriceValue);
                        prodLineItem.CostPerAppliedArea = item.AreaValue > 0 ? (prodLineItem.TotalAverageCost / prodLineItem.AreaApplied) : 0m;

                        prodLineItem.CostPerCropZoneArea = czArea > 0 ? (prodLineItem.TotalAverageCost / (decimal)czArea) : 0m;

                        prodLineItem.GrowerCost = prodLineItem.GrowerSharePercent * prodLineItem.TotalAverageCost;

                        if (fromInventory)
                        {
                            prodLineItem.OwnerCost = (prodLineItem.TotalAverageCost - prodLineItem.GrowerCost) * (decimal)( 1.0 + margin);
                        }
                    }
                    else
                    {
                        prodLineItem.TotalAverageCost = 0m;
                    }
                }
                

                //TO DO :: SOME FERTS HAVE REI PHI VALUES .... PLUS WE ALLOW USERS TO EDIT REI PHI VALUES ON ALL PRODUCTS.
                if (masterlistProd.ProductType == GlobalStrings.ProductType_CropProtection)
                {
                    //////////////////////////////////////////

                    double? Rei;
                    double? Phi;
                    double rei = 0.0;
                    double phi = 0.0;

                    var productService = new ProductServices(clientEndpoint);
                    ReiPhi reiPhi = productService.ReturnReiPhi(item.ProductId, czDetails.CropId);

                    //if (reiPhi != null)
                    //{
                    //    rei = reiPhi.Rei.HasValue && reiPhi.Rei.Value > rei ? reiPhi.Rei.Value : rei;
                    //    phi = reiPhi.Phi.HasValue && reiPhi.Phi.Value > phi ? reiPhi.Phi.Value : phi;
                    //}


                    //Rei = rei;
                    //Phi = phi;

                    if (reiPhi != null)
                    {

                        prodLineItem.Rei = reiPhi.Rei.HasValue ? reiPhi.Rei.Value.ToString("g") : "";
                        prodLineItem.Phi = reiPhi.Phi.HasValue ? reiPhi.Phi.Value.ToString("g") : "";
                        prodLineItem.ReiDate = reiPhi.Rei.HasValue ? item.EndDate.AddHours((double) reiPhi.Rei.Value).ToString("g") : "";
                        prodLineItem.PhiDate = reiPhi.Phi.HasValue ? item.EndDate.AddDays((double)reiPhi.Phi.Value).ToString("g") : "";


                        //prodLineItem.Rei = (from r in newReiPhis select r.Rei).Max().ToString();
                        //prodLineItem.Phi = (from r in newReiPhis select r.Phi).Max().ToString();
                        //prodLineItem.ReiDate = item.EndDate.AddHours((double)(from r in newReiPhis select r.Rei).Max()).ToString("g");
                        //prodLineItem.PhiDate = item.EndDate.AddDays((double)(from r in newReiPhis select r.Phi).Max()).ToString("g");


                    }
                    else
                    {
                        prodLineItem.Rei = "*";
                        prodLineItem.Phi = "*";
                        prodLineItem.ReiDate = string.Empty;
                        prodLineItem.PhiDate = string.Empty;
                    }

                    ///////////////////////////
                }

                totalFertTask.Wait();
                if (totalFertModel != null)
                {
                    prodLineItem.N = totalFertModel.N;
                    prodLineItem.P = totalFertModel.P;
                    prodLineItem.K = totalFertModel.K;
                    prodLineItem.Ca = totalFertModel.Ca;
                    prodLineItem.Mg = totalFertModel.Mg;
                    prodLineItem.S = totalFertModel.S;
                    prodLineItem.B = totalFertModel.B;
                    prodLineItem.Cl = totalFertModel.Cl;
                    prodLineItem.Cu = totalFertModel.Cu;
                    prodLineItem.Fe = totalFertModel.Fe;
                    prodLineItem.Mn = totalFertModel.Mn;
                    prodLineItem.Mo = totalFertModel.Mo;
                    prodLineItem.Zn = totalFertModel.Zn;
                }

                prodItems.Enqueue(prodLineItem);

                cnt++;
                UpdateProgressCurrentRecord(progress, cnt);
            });

            return prodItems.ToList();
        }

        public List<ExcelDumpData.PlannedProductDumpData> GetPlannedProducts(IMasterlistService masterlist)
        {
            ConcurrentQueue<ExcelDumpData.PlannedProductDumpData> prodItems = new ConcurrentQueue<ExcelDumpData.PlannedProductDumpData>();
            var dsID = ApplicationEnvironment.CurrentDataSourceId;
            ConcurrentQueue<CropPlanId> uniquePlans = new ConcurrentQueue<CropPlanId>();
            var currentDataSourceId = new DataSourceId(ApplicationEnvironment.CurrentDataSourceId);
            Task queTask = Task.Factory.StartNew(() =>
            {
                var currentCropYear = ApplicationEnvironment.CurrentCropYear;
                var cropYearId = new CropYearId(currentDataSourceId.Id, currentCropYear);
                var uniquePlanList = clientEndpoint.GetView<PlanList>(cropYearId);

                if (uniquePlanList.HasValue)
                {
                    uniquePlans = new ConcurrentQueue<CropPlanId>(uniquePlanList.Value.Plans.Select(x => x.Id));
                }
            });

            ParallelOptions parallelOptions = new ParallelOptions();
            parallelOptions.MaxDegreeOfParallelism = (System.Environment.ProcessorCount - 1);
            if (parallelOptions.MaxDegreeOfParallelism < 1)
            {
                parallelOptions.MaxDegreeOfParallelism = 1;
            }

            Task.WaitAll(queTask);

            Parallel.ForEach(uniquePlans, parallelOptions, item =>
            {
                var planDetails = clientEndpoint.GetView<PlanView>(new CropPlanId(currentDataSourceId.Id, item.Id)).Value;

                foreach (var prod in planDetails.Products)
                {
                    var prodLineItem = new ExcelDumpData.PlannedProductDumpData();
                    var mlp = masterlist.GetProduct(prod.Id);
                    var productId = new ProductId(ApplicationEnvironment.CurrentDataSource.DataSourceId, prod.Id.Id);
                    mlp = mlp == null ? masterlist.GetProduct(productId) : mlp;
                    var expend = (planDetails.LandRent + planDetails.Taxes + planDetails.Repairs + planDetails.ReturnToMgt + planDetails.Labor + planDetails.Equipment + planDetails.Insurance) * planDetails.EstimatedArea.Value;
                    //prodLineItem.AnnualCost = (decimal)expend;
                    var revenue = ((planDetails.YieldPerArea * planDetails.EstimatedArea.Value * (double)planDetails.PricePerUnit) * planDetails.CropSharePercent) + (planDetails.FSAPayment * planDetails.EstimatedArea.Value);
                    //prodLineItem.Revenue = (decimal)revenue;
                    prodLineItem.PlanId = planDetails.Id.Id;
                    prodLineItem.PlanName = planDetails.Name;
                    prodLineItem.ProductId = prod.Id.Id;
                    prodLineItem.Product = mlp.Name;
                    prodLineItem.Category = mlp.Category;
                    prodLineItem.Type = mlp.ProductType;
                    prodLineItem.Area = (decimal)planDetails.EstimatedArea.Value;
                    prodLineItem.Crop = planDetails.CropId != new Guid() ? masterlist.GetCropDisplay(new CropId(planDetails.CropId)) : string.Empty;
                    prodLineItem.CropId = planDetails.CropId;
                    prodLineItem.CropYear = planDetails.CropYear;
                    prodLineItem.RegNumber = mlp.RegistrationNumber;
                    prodLineItem.Manufacturer = mlp.Manufacturer;
                    prodLineItem.RateUnit = prod.RateUnit;
                    prodLineItem.RateValue = prod.RateValue;
                    prodLineItem.Rup = mlp.RestrictedUse;
                    prodLineItem.TimingEvent = prod.TimingEvent;
                    prodLineItem.TimingEventTag = prod.TimingEventTag;
                    var timingEventObj = !string.IsNullOrEmpty(prod.TimingEvent) ? masterlist.GetTimingEventList().FirstOrDefault(x => x.Name == prod.TimingEvent) : null;
                    prodLineItem.TimingEventOrder = timingEventObj != null ? timingEventObj.Order + 1 : 0;
                    var appCnt = prod.ApplicationCount.HasValue ? prod.ApplicationCount.Value : 1;
                    var total = prod.RateValue * ((decimal)planDetails.EstimatedArea.Value * prod.PercentApplied) * appCnt;
                    var totalMeasure = UnitFactory.GetPackageSafeUnit(prod.RateUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit).GetMeasure((double)total, mlp.Density);
                    var totalUnitToConvertTo = UnitFactory.GetPackageSafeUnit( prod.TotalProductUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                    var converted = totalMeasure.CanConvertTo(totalUnitToConvertTo) && totalMeasure.Unit != totalUnitToConvertTo ? totalMeasure.GetValueAs(totalUnitToConvertTo) : totalMeasure.Value;
                    prodLineItem.TotalProductQty = (decimal)converted;
                    prodLineItem.TotalProductUnit = prod.TotalProductUnit;
                    prodLineItem.TargetDate = prod.TargetDate;
                    var totalPriceMeasure = UnitFactory.GetPackageSafeUnit(prod.TotalProductUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit).GetMeasure((double)prod.TotalProductValue, mlp.Density);
                    var unitToConvertTo = UnitFactory.GetPackageSafeUnit(prod.SpecificCostUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit); // .GetUnitByName(prod.SpecificCostUnit);
                    var convertedTotalValue = totalPriceMeasure.CanConvertTo(unitToConvertTo) ? totalPriceMeasure.GetValueAs(unitToConvertTo) : totalPriceMeasure.Value;
                    var totalCost = prod.SpecificCostPerUnit.HasValue ? (decimal)convertedTotalValue * prod.SpecificCostPerUnit.Value : prod.SpecificCostTotal;
                    prodLineItem.TotalSpecificCost = totalCost;
                    prodLineItem.CostPerArea = totalCost.HasValue && prodLineItem.Area != 0 ? totalCost.Value / prodLineItem.Area : 0m;

                    prodItems.Enqueue(prodLineItem);
                }
            });

            return prodItems.ToList();
        }

        public List<Landdb.ReportModels.ExcelDump.ExcelDumpData.ExcelPlanInfoData> GetDetailedPlanInfo(IMasterlistService masterlist, FlattenedTreeHierarchyView flattenedView)
        {
            ConcurrentQueue<Landdb.ReportModels.ExcelDump.ExcelDumpData.ExcelPlanInfoData> planItems = new ConcurrentQueue<Landdb.ReportModels.ExcelDump.ExcelDumpData.ExcelPlanInfoData>();
            var dsID = ApplicationEnvironment.CurrentDataSourceId;
            ConcurrentQueue<CropPlanId> uniquePlans = new ConcurrentQueue<CropPlanId>();
            var currentDataSourceId = new DataSourceId(ApplicationEnvironment.CurrentDataSourceId);
            Task queTask = Task.Factory.StartNew(() =>
            {
                var currentCropYear = ApplicationEnvironment.CurrentCropYear;
                var cropYearId = new CropYearId(currentDataSourceId.Id, currentCropYear);
                var uniquePlanList = clientEndpoint.GetView<PlanList>(cropYearId);

                if (uniquePlanList.HasValue)
                {
                    uniquePlans = new ConcurrentQueue<CropPlanId>(uniquePlanList.Value.Plans.Select(x => x.Id));
                }
            });

            ParallelOptions parallelOptions = new ParallelOptions();
            parallelOptions.MaxDegreeOfParallelism = (System.Environment.ProcessorCount - 1);
            if (parallelOptions.MaxDegreeOfParallelism < 1)
            {
                parallelOptions.MaxDegreeOfParallelism = 1;
            }

            Task.WaitAll(queTask);

            Parallel.ForEach(uniquePlans, parallelOptions, item =>
            {
                var planData = clientEndpoint.GetView<PlanView>(new CropPlanId(currentDataSourceId.Id, item.Id)).Value;

                var data = new Landdb.ReportModels.ExcelDump.ExcelDumpData.ExcelPlanInfoData();

                foreach (var cz in planData.CropZones)
                {
                    var czDetails = clientEndpoint.GetView<CropZoneDetailsView>(cz.Id).Value;
                    var flattened = flattenedView.Items.FirstOrDefault(x => x.CropZoneId == cz.Id);

                    data.CropZoneId = cz.Id.Id;
                    data.CropZone = flattened.CropZoneName;
                    data.FieldId = flattened.FieldId.Id;
                    data.FarmId = flattened.FarmId.Id;
                    data.Field = flattened.FieldName;
                    data.Farm = flattened.FarmName;
                    data.CzArea = (decimal?)cz.Area.Value;

                    data.Rent = (decimal)(planData.LandRent * cz.Area.Value);
                    data.Equipment = (decimal)(planData.Equipment * cz.Area.Value);
                    data.Insurance = (decimal)(planData.Insurance * cz.Area.Value);
                    data.Labor = (decimal)(planData.Labor * cz.Area.Value);
                    data.Repairs = (decimal)(planData.Repairs * cz.Area.Value);
                    data.Taxes = (decimal)(planData.Taxes * cz.Area.Value);
                    data.ReturnToMgt = (decimal)(planData.ReturnToMgt * cz.Area.Value);
                    data.EstimatedArea = (decimal)(planData.EstimatedArea.Value * cz.Area.Value);
                    data.PlanId = planData.Id;
                    data.PlanName = planData.Name;
                    data.CropYear = planData.CropYear;
                    data.Notes = planData.Notes;
                    data.YieldPerArea = (decimal)(planData.YieldPerArea * cz.Area.Value);
                    data.PricePerUnit = (decimal)(planData.PricePerUnit * cz.Area.Value);
                    data.FSAPayment = (decimal)(planData.FSAPayment * cz.Area.Value);
                    data.CropShare = (decimal)(planData.CropSharePercent);

                    planItems.Enqueue(data);
                }
            });

            return planItems.ToList();
        }

        public List<ReportModels.Plan.DetailProducts> GetDetailedPlannedProducts(IMasterlistService masterlist, FlattenedTreeHierarchyView flattenedHi)
        {
            ConcurrentQueue<DetailProducts> prodItems = new ConcurrentQueue<DetailProducts>();
            var dsID = ApplicationEnvironment.CurrentDataSourceId;
            ConcurrentQueue<CropPlanId> uniquePlans = new ConcurrentQueue<CropPlanId>();
            var currentDataSourceId = new DataSourceId(ApplicationEnvironment.CurrentDataSourceId);
            Task queTask = Task.Factory.StartNew(() =>
            {
                var currentCropYear = ApplicationEnvironment.CurrentCropYear;
                var cropYearId = new CropYearId(currentDataSourceId.Id, currentCropYear);
                var uniquePlanList = clientEndpoint.GetView<PlanList>(cropYearId);

                if (uniquePlanList.HasValue)
                {
                    uniquePlans = new ConcurrentQueue<CropPlanId>(uniquePlanList.Value.Plans.Select(x => x.Id));
                }
            });

            ParallelOptions parallelOptions = new ParallelOptions();
            parallelOptions.MaxDegreeOfParallelism = (System.Environment.ProcessorCount - 1);
            if (parallelOptions.MaxDegreeOfParallelism < 1)
            {
                parallelOptions.MaxDegreeOfParallelism = 1;
            }

            Task.WaitAll(queTask);

            Parallel.ForEach(uniquePlans, parallelOptions, item =>
            {
                var planData = clientEndpoint.GetView<PlanView>(new CropPlanId(currentDataSourceId.Id, item.Id)).Value;

                if (planData.CropZones.Any() && planData.CropZones.Count() > 0)
                {

                    foreach (var cz in planData.CropZones)
                    {
                        var czDetails = clientEndpoint.GetView<CropZoneDetailsView>(cz.Id).Value;
                        var flattened = flattenedHi.Items.FirstOrDefault(x => x.CropZoneId == cz.Id);

                        if(flattened == null) { return; }
                        foreach (var prod in planData.Products)
                        {
                            var prodDetail = new DetailProducts();
                            prodDetail.CropZoneId = cz.Id.Id;
                            prodDetail.CropZone = flattened.CropZoneName;
                            prodDetail.FieldId = flattened.FieldId.Id;
                            prodDetail.FarmId = flattened.FarmId.Id;
                            prodDetail.Field = flattened.FieldName;
                            prodDetail.Farm = flattened.FarmName;
                            prodDetail.Area = (decimal?)cz.Area.Value;

                            //get crop zone Details 
                            prodDetail.Tags = string.Join(" + ", czDetails.Tags.ToArray());

                            var mlp = masterlist.GetProduct(prod.Id);
                            prodDetail.PlanId = planData.Id;
                            prodDetail.PlanName = planData.Name;
                            prodDetail.CropYear = cropYear;
                            prodDetail.Crop = flattened.CropName;
                            prodDetail.ProductName = mlp.Name;
                            prodDetail.ApplicationCount = prod.ApplicationCount.HasValue ? prod.ApplicationCount.Value : 1;
                            prodDetail.PercentApplied = prod.PercentApplied;
                            prodDetail.TimingEvent = prod.TimingEvent;
                            prodDetail.TimingEventTag = prod.TimingEventTag;
                            var timingEventObj = !string.IsNullOrEmpty(prod.TimingEvent) ? masterlist.GetTimingEventList().FirstOrDefault(x => x.Name == prod.TimingEvent) : null;
                            prodDetail.TimingEventOrder = timingEventObj != null ? timingEventObj.Order + 1 : 0;
                            prodDetail.TargetDate = prod.TargetDate;
                            //get total product based on crop zone and total price.                        
                            var totalProd = prod.RateValue * ((decimal)cz.Area.Value * prodDetail.PercentApplied) * prodDetail.ApplicationCount;
                            var totalProdMeasure = UnitFactory.GetPackageSafeUnit(prod.RateUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit).GetMeasure((double)totalProd.Value, mlp.Density);
                            totalProdMeasure = totalProdMeasure.CanConvertTo(UnitFactory.GetPackageSafeUnit(mlp.StdPackageUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit)) ? totalProdMeasure.CreateAs(UnitFactory.GetPackageSafeUnit(mlp.StdPackageUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit)) : totalProdMeasure;
                            var displayRate = DisplayMeasureConverter.ConvertToRateDisplayMeasure(prod.Id, prod.RateValue, prod.RateUnit, mlp.Density, clientEndpoint, ApplicationEnvironment.CurrentDataSourceId);
                            var displayTotal = DisplayMeasureConverter.ConvertToTotalDisplayMeasure(prod.Id, (decimal)totalProdMeasure.Value, totalProdMeasure.Unit.Name, mlp.Density, clientEndpoint, ApplicationEnvironment.CurrentDataSourceId);

                            prodDetail.Rate = (decimal?)displayRate.Value;
                            prodDetail.RateUnit = prod.RateUnit;
                            prodDetail.TotalProduct = (decimal?)displayTotal.Value;
                            prodDetail.TotalProductUnit = displayTotal.Unit.Name;

                            var costUnit = UnitFactory.GetPackageSafeUnit(prod.SpecificCostUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                            var convertedTotalValue = displayTotal.CanConvertTo(costUnit) ? displayTotal.GetValueAs(costUnit) : displayTotal.Value;
                            var totalCost = prod.SpecificCostPerUnit.HasValue ? (decimal)convertedTotalValue * prod.SpecificCostPerUnit.Value : 0m;
                            prodDetail.TotalCost = totalCost;
                            prodDetail.CostPerArea = cz.Area.Value != 0 ? (decimal?)(totalCost / (decimal)cz.Area.Value) : null;
                            prodItems.Enqueue(prodDetail);
                        }
                    }
                }
                else
                {
                    //run thru the products with a reported area equal to the estimated area of the plan.
                    foreach (var prod in planData.Products)
                    {
                        var prodDetail = new DetailProducts();

                        var mlp = masterlist.GetProduct(prod.Id);
                        prodDetail.PlanId = planData.Id;
                        prodDetail.PlanName = planData.Name;
                        prodDetail.CropYear = cropYear;
                        prodDetail.Crop = masterlist.GetCropDisplay(new CropId(planData.CropId));
                        prodDetail.ProductName = mlp.Name;
                        prodDetail.ApplicationCount = prod.ApplicationCount.HasValue ? prod.ApplicationCount.Value : 1;
                        prodDetail.PercentApplied = prod.PercentApplied;
                        prodDetail.TimingEvent = prod.TimingEvent;
                        prodDetail.TimingEventTag = prod.TimingEventTag;
                        var timingEventObj = !string.IsNullOrEmpty(prod.TimingEvent) ? masterlist.GetTimingEventList().FirstOrDefault(x => x.Name == prod.TimingEvent) : null;
                        prodDetail.TimingEventOrder = timingEventObj != null ? timingEventObj.Order + 1 : 0;
                        prodDetail.TargetDate = prod.TargetDate;
                        
                        //get total product based on crop zone and total price.
                        var totalProd = prod.RateValue * ((decimal)planData.EstimatedArea.Value * prodDetail.PercentApplied) * prodDetail.ApplicationCount;
                        var totalProdMeasure = UnitFactory.GetPackageSafeUnit(prod.RateUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit).GetMeasure((double)totalProd.Value, mlp.Density);
                        totalProdMeasure = totalProdMeasure.CreateAs(UnitFactory.GetPackageSafeUnit(mlp.StdPackageUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit));
                        var displayRate = DisplayMeasureConverter.ConvertToRateDisplayMeasure(prod.Id, prod.RateValue, prod.RateUnit, mlp.Density, clientEndpoint, ApplicationEnvironment.CurrentDataSourceId);
                        var displayTotal = DisplayMeasureConverter.ConvertToTotalDisplayMeasure(prod.Id, (decimal)totalProdMeasure.Value, totalProdMeasure.Unit.Name, mlp.Density, clientEndpoint, ApplicationEnvironment.CurrentDataSourceId);

                        prodDetail.Rate = (decimal?)displayRate.Value;
                        prodDetail.RateUnit = prod.RateUnit;
                        prodDetail.TotalProduct = (decimal?)displayTotal.Value;
                        prodDetail.TotalProductUnit = displayTotal.Unit.Name;

                        var costUnit = UnitFactory.GetPackageSafeUnit(prod.SpecificCostUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                        var convertedTotalValue = displayTotal.CanConvertTo(costUnit) ? displayTotal.GetValueAs(costUnit) : displayTotal.Value;
                        var totalCost = prod.SpecificCostPerUnit.HasValue ? (decimal)convertedTotalValue * prod.SpecificCostPerUnit.Value : 0m;
                        prodDetail.TotalCost = totalCost;

                        prodItems.Enqueue(prodDetail);
                    }
                }
            });

            return prodItems.OrderBy(p=>p.PlanName).ThenBy(x => x.CropZone).ToList();
        }

        void UpdateProgressMessage(IProgressTracker progress, string message)
        {
            if (progress != null)
            {
                progress.StatusMessage = message;
            }
        }

        void UpdateProgressTotalRecords(IProgressTracker progress, int totalRecords)
        {
            if (progress != null)
            {
                progress.TotalRecords = totalRecords;
            }
        }

        void UpdateProgressCurrentRecord(IProgressTracker progress, long currentRecord)
        {
            if (progress != null)
            {
                progress.CurrentRecord = (int)currentRecord;
            }
        }

        public string DisplayName
        {
            get { return Strings.DataDumpName_Excel_Text; }
        }

        public List<ExcelDumpData.CropZoneYieldItemData> GetFieldToStorageLoads(CropZoneId czID, CropYearId cropYearID, FlattenedTreeHierarchyItem treeData)
        {
            var cropZoneLoadData = clientEndpoint.GetView<CropZoneLoadDataView>(cropYearID).GetValue(() => null);
            List<ExcelDumpData.CropZoneYieldItemData> items = new List<ExcelDumpData.CropZoneYieldItemData>();
            var contracts = clientEndpoint.GetView<CropZoneDetailsView>(czID).Value.Contracts;
            var contractList = clientEndpoint.GetView<RentContractListView>(cropYearID);
            var rentContractId = (from c in contracts
                                  where clientEndpoint.GetView<RentContractDetailsView>(c.Id).HasValue && (contractList.HasValue && contractList.Value.RentContracts.Any(x => x.Id == c.Id))
                                  select c.Id).FirstOrDefault();

            var rentContract = rentContractId != null ? clientEndpoint.GetView<RentContractDetailsView>(rentContractId).Value : null;

            if (cropZoneLoadData != null)
            {
				var yieldLocations = clientEndpoint.GetView<YieldLocationListView>(new DataSourceId(cropYearID.DataSourceId)).GetValue(() => new YieldLocationListView()).YieldLocations;

				var locationsExcludedFromInventory = from loc in yieldLocations
													 where loc.IsActiveInCropYear(cropYearID.Id) && loc.IsQuantityExcludedFromInventory
													 select loc.Id.Id;


                var czLoads = (from load in cropZoneLoadData.Items
							  from loc in load.DestinationLocations
                              where load.CropZoneId == czID 
								&& load.OperationType == YieldOperationTypes.FieldToStorage
                                && !locationsExcludedFromInventory.Contains(loc.Id.Id)
                              select load).Distinct();
                
				foreach (var load in czLoads) {
					foreach (var destinationLocation in load.DestinationLocations) {
						var yieldLoadData = new ExcelDumpData.CropZoneYieldItemData();
						LoadDetailsView loadDetails = new LoadDetailsView();

						Task detailsTask = Task.Factory.StartNew(() => {
							loadDetails = clientEndpoint.GetView<LoadDetailsView>(load.LoadId).Value;
						});

						var item = treeData;
						yieldLoadData.CropZone = item.CropZoneName;
						yieldLoadData.Field = item.FieldName;
						yieldLoadData.Farm = item.FarmName;
						yieldLoadData.Crop = clientEndpoint.GetMasterlistService().GetCropDisplay(item.CropId);

						yieldLoadData.CropZoneArea = load.AreaValue;
						yieldLoadData.CropZoneId = load.CropZoneId.Id;
						yieldLoadData.DestinationName = destinationLocation.Name;
						yieldLoadData.DestinationType = destinationLocation.YieldLocationType.ToString();
						yieldLoadData.FinalUnit = load.FinalQuantityUnit.AbbreviatedDisplay;
						yieldLoadData.LoadDate = load.StartDateTime;
						yieldLoadData.LoadId = load.LoadId.Id;
						yieldLoadData.LoadNumber = load.LoadNumber;
						//yieldLoadData.WeightedFinalPrice = load.WeightedFinalPrice;
						yieldLoadData.WeightedFinalDryQuantity = load.AreaWeightedFinalQuantity / load.DestinationLocations.Count;

						//////////////////////////////////////////////////////////////////////
						yieldLoadData.GrowerQtyShare = rentContract != null ? (load.AreaWeightedFinalQuantity / load.DestinationLocations.Count) * (decimal)rentContract.IncludedShareInfo.GrowerCropShare : (load.AreaWeightedFinalQuantity * load.DestinationLocations.Count);
						yieldLoadData.LandOwnerQtyShare = rentContract != null ? load.AreaWeightedFinalQuantity * (decimal)(1 - rentContract.IncludedShareInfo.GrowerCropShare) : 0;
						//////////////////////////////////////////////////////////////////////


						Task.WaitAll(detailsTask);

						yieldLoadData.DriverId = loadDetails.DriverId != null ? loadDetails.DriverId.Id : Guid.Empty;
						yieldLoadData.DriverName = loadDetails.DriverName;
						yieldLoadData.Notes = loadDetails.Notes;
						yieldLoadData.OperationId = loadDetails.OperationId.Id;
						yieldLoadData.OperationType = loadDetails.OperationType.ToString();
						yieldLoadData.TruckId = loadDetails.TruckId != null ? loadDetails.TruckId.Id : Guid.Empty;
						yieldLoadData.TruckName = loadDetails.TruckName;

						if (loadDetails.QuantityInformation != null) {
							var multiplier = loadDetails.CropZones.Sum(x => x.Area.Value) > 0 ? load.AreaValue / (decimal)loadDetails.CropZones.Sum(x => x.Area.Value) : 1;
							//yieldLoadData.TotalLoadQty = loadDetails.QuantityInformation.FinalQuantityValue;
							//yieldLoadData.TotalQtyUnit = loadDetails.QuantityInformation.FinalQuantityUnit.AbbreviatedDisplay;
							yieldLoadData.InitialMoisturePercent = loadDetails.QuantityInformation.InitialMoisturePercentage;
							yieldLoadData.TargetMoisturePercent = loadDetails.QuantityInformation.HandlingShrinkTargetMoisturePercentage == 0 ? loadDetails.QuantityInformation.StandardTargetMoisturePercentage : loadDetails.QuantityInformation.HandlingShrinkTargetMoisturePercentage;
							yieldLoadData.MoistureShrinkPercent = loadDetails.QuantityInformation.StandardMoistureShrinkPercentage;
							yieldLoadData.MoistureShrinkQty = (loadDetails.QuantityInformation.StandardMoistureShrinkQuantity / loadDetails.DestinationLocations.Count) * multiplier;
							yieldLoadData.ShrinkAdjustmentPercent = loadDetails.QuantityInformation.AdjustmentShrinkPercentage;
							yieldLoadData.ShrinkAdjustmentQty = loadDetails.QuantityInformation.AdjustmentShrinkQuantity * multiplier;
							yieldLoadData.HandlingShrinkPercent = loadDetails.QuantityInformation.HandlingShrinkPercentage;
							yieldLoadData.HandlingShrinkQty = (loadDetails.QuantityInformation.HandlingShrinkQuantity / loadDetails.DestinationLocations.Count) * multiplier;
							yieldLoadData.Gross = loadDetails.QuantityInformation.TruckGrossWeight;
							yieldLoadData.Net = loadDetails.QuantityInformation.TruckNetWeight;
							yieldLoadData.Tare = loadDetails.QuantityInformation.TruckTareWeight;
							yieldLoadData.Unit = loadDetails.QuantityInformation.TruckWeightUnit.AbbreviatedDisplay;
							yieldLoadData.WeightedFinalDeliveredQuantity = ((loadDetails.QuantityInformation.FinalQuantityValue + loadDetails.QuantityInformation.StandardMoistureShrinkQuantity + loadDetails.QuantityInformation.HandlingShrinkQuantity + loadDetails.QuantityInformation.AdjustmentShrinkQuantity) / load.DestinationLocations.Count) * (multiplier);

						}

						var loadProperties = loadDetails.ReportedPropertySets;

						foreach (var prop in loadProperties) {
							decimal tmpvalue;
							decimal? tmpresult = null;

							switch (prop.LoadPropertySetId.ToString().ToUpper()) {
								case "C9931DD8-F654-44F7-9F6C-E702FD9BDF1C":
									//do stuff
									if (decimal.TryParse(prop.ReportedPropertyValues.First().ReportedValue, out tmpvalue)) { tmpresult = tmpvalue; }
									yieldLoadData.TestWeight = prop != null && prop.ReportedPropertyValues.Any() && prop.ReportedPropertyValues.First().ReportedValue != null ? tmpresult : null;
									yieldLoadData.TestWeightUnit = prop != null && prop.ReportedPropertyValues.Any() && prop.ReportedPropertyValues.First().ReportedUnit != null ? prop.ReportedPropertyValues.First().ReportedUnit.AbbreviatedDisplay : string.Empty;
									break;
								case "71200E91-1717-479A-BF20-F8E2C4901969":
									//fm%
									if (decimal.TryParse(prop.ReportedPropertyValues.First().ReportedValue, out tmpvalue)) { tmpresult = tmpvalue; }
									yieldLoadData.ForiegnMaterialPercent = prop != null && prop.ReportedPropertyValues.Any() && prop.ReportedPropertyValues.First().ReportedValue != null ? tmpresult : null;
									break;
								case "CF0C81F5-C266-4AD7-B31D-947A3F2331EF":
									//Damage
									if (decimal.TryParse(prop.ReportedPropertyValues.First().ReportedValue, out tmpvalue)) { tmpresult = tmpvalue; }
									yieldLoadData.DamagePercent = prop != null && prop.ReportedPropertyValues.Any() && prop.ReportedPropertyValues.First().ReportedValue != null ? tmpresult : null;
									break;
								case "B8A44DF0-E93B-4240-A56E-FC7393960EFB":
									// large square bales
									var large = prop.ReportedPropertyValues.Any(id => id.LoadPropertyId == new Guid("0D25AF79-C6B2-4C81-8A58-6356F71C5BA6")) ? prop.ReportedPropertyValues.FirstOrDefault(i => i.LoadPropertyId == new Guid("0D25AF79-C6B2-4C81-8A58-6356F71C5BA6")) : null;
									yieldLoadData.LargeSquareBaleCount = large != null && !string.IsNullOrEmpty(large.ReportedValue) ? decimal.Parse(large.ReportedValue) as decimal? : null;
									break;
								case "853C150B-1B91-459F-96C2-205B90F35FAF":
									// small square bales
									var small = prop.ReportedPropertyValues.Any(id => id.LoadPropertyId == new Guid("2C268AB3-B0B3-4C5A-81B8-B6EA720D1474")) ? prop.ReportedPropertyValues.FirstOrDefault(i => i.LoadPropertyId == new Guid("0D25AF79-C6B2-4C81-8A58-6356F71C5BA6")) : null;
									yieldLoadData.SquareBaleCount = small != null && small.ReportedValue != null ? decimal.Parse(small.ReportedValue) as decimal? : null;
									break;
								case "90EEF973-58A0-44D8-BC83-8B4690B452FC":
									// round bales
									var round = prop.ReportedPropertyValues.Any(id => id.LoadPropertyId == new Guid("32C20EA8-8B41-42AE-ABF0-CDF5E7C1B34E")) ? prop.ReportedPropertyValues.FirstOrDefault(i => i.LoadPropertyId == new Guid("32C20EA8-8B41-42AE-ABF0-CDF5E7C1B34E")) : null;
									yieldLoadData.RoundBaleCount = round != null && round.ReportedValue != null ? decimal.Parse(round.ReportedValue) as decimal? : null;
									break;
								case "0D8F01EE-5CEE-4B1A-B442-13831C5D930C":
									// cotton modules
									var modules = prop.ReportedPropertyValues.Any(id => id.LoadPropertyId == new Guid("7E73EE8A-A401-4F1C-AF31-48CC9670A98B")) ? prop.ReportedPropertyValues.FirstOrDefault(i => i.LoadPropertyId == new Guid("7E73EE8A-A401-4F1C-AF31-48CC9670A98B")) : null;
									yieldLoadData.ModuleCount = modules != null && modules.ReportedValue != null ? decimal.Parse(modules.ReportedValue) as decimal? : null;
									break;
								case "82CB84C9-B851-4A49-A02D-51A26AB1605D":
									// mini cotton modules
									var minimodules = prop.ReportedPropertyValues.Any(id => id.LoadPropertyId == new Guid("537B880A-8130-4F38-966A-6003B0E27413")) ? prop.ReportedPropertyValues.FirstOrDefault(i => i.LoadPropertyId == new Guid("537B880A-8130-4F38-966A-6003B0E27413")) : null;
									yieldLoadData.MiniModuleCount = minimodules != null && minimodules.ReportedValue != null ? decimal.Parse(minimodules.ReportedValue) as decimal? : null;
									break;
								case "b9075209-a856-497c-bf2a-e2cfda835eb8":
									// bill of lading
									var bol = prop.ReportedPropertyValues.Any(id => id.LoadPropertyId == new Guid("f093acfd-562e-4480-9b9d-268bc28d2fa8")) ? prop.ReportedPropertyValues.FirstOrDefault(i => i.LoadPropertyId == new Guid("f093acfd-562e-4480-9b9d-268bc28d2fa8")) : null;
									yieldLoadData.BillofLadingCount = bol != null && bol.ReportedValue != null ? decimal.Parse(bol.ReportedValue) as decimal? : null;
									break;
								case "0CE65910-A1F2-4E09-906B-AC4F3C6F3C8A":
									// lint weight
									yieldLoadData.LintWeight = prop != null && prop.ReportedPropertyValues.Any() && prop.ReportedPropertyValues.First().ReportedValue != null ? decimal.Parse(prop.ReportedPropertyValues.First().ReportedValue) as decimal? : null;
									yieldLoadData.LintWeightUnit = prop != null && prop.ReportedPropertyValues.Any() && prop.ReportedPropertyValues.First().ReportedUnit != null ? prop.ReportedPropertyValues.First().ReportedUnit.AbbreviatedDisplay : string.Empty;
									break;
								case "F70E9E2A-4D90-4A81-9F53-7780B4F61D18":
									// seed weight
									yieldLoadData.SeedWeight = prop != null && prop.ReportedPropertyValues.Any() && prop.ReportedPropertyValues.First().ReportedValue != null ? decimal.Parse(prop.ReportedPropertyValues.First().ReportedValue) as decimal? : null;
									yieldLoadData.SeedWeightUnit = prop != null && prop.ReportedPropertyValues.Any() && prop.ReportedPropertyValues.First().ReportedUnit != null ? prop.ReportedPropertyValues.First().ReportedUnit.AbbreviatedDisplay : string.Empty;
									break;
								case "E394CACF-2DF5-4065-88A6-A93E0BEEDD40":
									// box count: boxes
									var boxes = prop.ReportedPropertyValues.Any(id => id.LoadPropertyId == new Guid("D48BBA6D-E24D-4ABA-B9BE-71BAEAA9301A")) ? prop.ReportedPropertyValues.FirstOrDefault(i => i.LoadPropertyId == new Guid("D48BBA6D-E24D-4ABA-B9BE-71BAEAA9301A")) : null;
									yieldLoadData.BoxCount = boxes != null && boxes.ReportedValue != null ? decimal.Parse(boxes.ReportedValue) as decimal? : null;
									break;
								case "06f43787-4ea8-4a2b-af2a-55753c73f475":
									//protein%
									yieldLoadData.ProteinPercent = prop != null && prop.ReportedPropertyValues.Any() && prop.ReportedPropertyValues.First().ReportedValue != null ? decimal.Parse(prop.ReportedPropertyValues.First().ReportedValue) as decimal? : null;
									break;
								case "96a50646-f70b-469a-8d5b-ce700ba6dae5":
									//plump%
									yieldLoadData.PlumpPercent = prop != null && prop.ReportedPropertyValues.Any() && prop.ReportedPropertyValues.First().ReportedValue != null ? decimal.Parse(prop.ReportedPropertyValues.First().ReportedValue) as decimal? : null;
									break;
								case "42365775-b399-44e9-9a8b-09089d2281f1":
									//sugar%
									yieldLoadData.SugarPercent = prop != null && prop.ReportedPropertyValues.Any() && prop.ReportedPropertyValues.First().ReportedValue != null ? decimal.Parse(prop.ReportedPropertyValues.First().ReportedValue) as decimal? : null;
									break;
								case "ddfb676d-bc91-47e8-b5d0-22896634f942":
									//sugar%
									yieldLoadData.SLMPercent = prop != null && prop.ReportedPropertyValues.Any() && prop.ReportedPropertyValues.First().ReportedValue != null ? decimal.Parse(prop.ReportedPropertyValues.First().ReportedValue) as decimal? : null;
									break;
								default:
									break;
							}

							//PLUMP
							//SLM
							//SUGAR
							//NOTES
						}

						items.Add(yieldLoadData);

					}
				}
            }

            return items;
        }

        public List<ExcelDumpData.StorageToStorage> GetStorageToStorageLoads()
        {
            var loadsMaybe = clientEndpoint.GetView<LoadListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear));
            List<ExcelDumpData.StorageToStorage> items = new List<ExcelDumpData.StorageToStorage>();

            if (loadsMaybe.HasValue)
            {
                var allLoads = loadsMaybe.Value;

                var selectedLoads = from l in allLoads.Loads
                                    where l.OperationType == ValueObjects.YieldOperationTypes.StorageToStorage
                                    select l;

                foreach (var load in selectedLoads)
                {
					foreach (var sourceLocation in load.SourceLocations) {
						foreach (var destinationLocation in load.DestinationLocations) {

							// TODO: weigh quantities by (1 / sourcelocations.count) * (1 / destinationlocations.count)

							var storagetoStorageData = new ExcelDumpData.StorageToStorage();
							storagetoStorageData.LoadDate = load.StartDateTime;
							storagetoStorageData.LoadId = load.Id.Id;
							storagetoStorageData.LoadNumber = load.LoadNumber;
							storagetoStorageData.Crop = load.Commodity.CropName;
							storagetoStorageData.StorageLocation = clientEndpoint.GetView<YieldLocationDetailsView>(sourceLocation.Id).Value.Name;
							storagetoStorageData.DestinationName = clientEndpoint.GetView<YieldLocationDetailsView>(destinationLocation.Id).Value.Name;
							storagetoStorageData.DestinationType = destinationLocation.YieldLocationType.ToString();
							storagetoStorageData.OperationType = load.OperationType.ToString();

							var loadDetails = clientEndpoint.GetView<LoadDetailsView>(load.Id).Value;

							if (loadDetails.QuantityInformation != null) {
                                storagetoStorageData.WeightedTotalQuantity = loadDetails.QuantityInformation.FinalQuantityValue / loadDetails.DestinationLocations.Count;
								storagetoStorageData.TotalQuantity = loadDetails.QuantityInformation.FinalQuantityValue;
								storagetoStorageData.TotalUnit = loadDetails.QuantityInformation.FinalQuantityUnit.AbbreviatedDisplay;
								storagetoStorageData.Gross = loadDetails.QuantityInformation.TruckGrossWeight;
								storagetoStorageData.Tare = loadDetails.QuantityInformation.TruckTareWeight;
								storagetoStorageData.Net = loadDetails.QuantityInformation.TruckNetWeight;
                                storagetoStorageData.WeightedGross = loadDetails.QuantityInformation.TruckGrossWeight / loadDetails.DestinationLocations.Count;
                                storagetoStorageData.WeightedTare = loadDetails.QuantityInformation.TruckTareWeight / loadDetails.DestinationLocations.Count;
                                storagetoStorageData.WeightedNet = loadDetails.QuantityInformation.TruckNetWeight / loadDetails.DestinationLocations.Count;
                                storagetoStorageData.Unit = loadDetails.QuantityInformation.TruckWeightUnit.AbbreviatedDisplay;

								storagetoStorageData.InitialMoisturePercent = loadDetails.QuantityInformation.InitialMoisturePercentage;
								storagetoStorageData.TargetMoisturePercent = loadDetails.QuantityInformation.StandardTargetMoisturePercentage;
								storagetoStorageData.MoistureShrinkPercent = loadDetails.QuantityInformation.StandardMoistureShrinkPercentage;
								storagetoStorageData.MoistureShrinkQty = (loadDetails.QuantityInformation.StandardMoistureShrinkQuantity / loadDetails.DestinationLocations.Count);
								storagetoStorageData.ShrinkAdjustmentPercent = loadDetails.QuantityInformation.AdjustmentShrinkPercentage;
								storagetoStorageData.ShrinkAdjustmentQty = loadDetails.QuantityInformation.AdjustmentShrinkQuantity;
								storagetoStorageData.HandlingShrinkPercent = loadDetails.QuantityInformation.HandlingShrinkPercentage;
								storagetoStorageData.HandlingShrinkQty = (loadDetails.QuantityInformation.HandlingShrinkQuantity / loadDetails.DestinationLocations.Count);
							}

							storagetoStorageData.DriverId = loadDetails.DriverId != null ? loadDetails.DriverId.Id : new Guid();
							storagetoStorageData.DriverName = loadDetails.DriverName;
							storagetoStorageData.TruckId = loadDetails.TruckId != null ? loadDetails.TruckId.Id : new Guid();
							storagetoStorageData.TruckName = loadDetails.TruckName;
							storagetoStorageData.Notes = loadDetails.Notes;

							var loadProperties = loadDetails.ReportedPropertySets;

							foreach (var prop in loadProperties) {
								decimal tmpvalue;
								decimal? tmpresult = null;

								switch (prop.LoadPropertySetId.ToString().ToUpper()) {
									case "C9931DD8-F654-44F7-9F6C-E702FD9BDF1C":
										//do stuff
										if (decimal.TryParse(prop.ReportedPropertyValues.First().ReportedValue, out tmpvalue)) { tmpresult = tmpvalue; }
										storagetoStorageData.TestWeight = prop != null && prop.ReportedPropertyValues.Any() && prop.ReportedPropertyValues.First().ReportedValue != null ? tmpresult : null;
										storagetoStorageData.TestWeightUnit = prop != null && prop.ReportedPropertyValues.Any() && prop.ReportedPropertyValues.First().ReportedUnit != null ? prop.ReportedPropertyValues.First().ReportedUnit.AbbreviatedDisplay : string.Empty;
										break;
									case "71200E91-1717-479A-BF20-F8E2C4901969":
										//fm%
										if (decimal.TryParse(prop.ReportedPropertyValues.First().ReportedValue, out tmpvalue)) { tmpresult = tmpvalue; }
										storagetoStorageData.ForiegnMaterialPercent = prop != null && prop.ReportedPropertyValues.Any() && prop.ReportedPropertyValues.First().ReportedValue != null ? tmpresult : null;
										break;
									case "CF0C81F5-C266-4AD7-B31D-947A3F2331EF":
										//Damage
										if (decimal.TryParse(prop.ReportedPropertyValues.First().ReportedValue, out tmpvalue)) { tmpresult = tmpvalue; }
										storagetoStorageData.DamagePercent = prop != null && prop.ReportedPropertyValues.Any() && prop.ReportedPropertyValues.First().ReportedValue != null ? tmpresult : null;
										break;
									case "B8A44DF0-E93B-4240-A56E-FC7393960EFB":
										// large square bales
										var large = prop.ReportedPropertyValues.Any(id => id.LoadPropertyId == new Guid("0D25AF79-C6B2-4C81-8A58-6356F71C5BA6")) ? prop.ReportedPropertyValues.FirstOrDefault(i => i.LoadPropertyId == new Guid("0D25AF79-C6B2-4C81-8A58-6356F71C5BA6")) : null;
										storagetoStorageData.LargeSquareBaleCount = large != null && large.ReportedValue != null ? decimal.Parse(large.ReportedValue) as decimal? : null;
										break;
									case "853C150B-1B91-459F-96C2-205B90F35FAF":
										// small square bales
										var small = prop.ReportedPropertyValues.Any(id => id.LoadPropertyId == new Guid("2C268AB3-B0B3-4C5A-81B8-B6EA720D1474")) ? prop.ReportedPropertyValues.FirstOrDefault(i => i.LoadPropertyId == new Guid("0D25AF79-C6B2-4C81-8A58-6356F71C5BA6")) : null;
										storagetoStorageData.SquareBaleCount = small != null && small.ReportedValue != null ? decimal.Parse(small.ReportedValue) as decimal? : null;
										break;
									case "90EEF973-58A0-44D8-BC83-8B4690B452FC":
										// round bales
										var round = prop.ReportedPropertyValues.Any(id => id.LoadPropertyId == new Guid("32C20EA8-8B41-42AE-ABF0-CDF5E7C1B34E")) ? prop.ReportedPropertyValues.FirstOrDefault(i => i.LoadPropertyId == new Guid("32C20EA8-8B41-42AE-ABF0-CDF5E7C1B34E")) : null;
										storagetoStorageData.RoundBaleCount = round != null && round.ReportedValue != null ? decimal.Parse(round.ReportedValue) as decimal? : null;
										break;
									case "0D8F01EE-5CEE-4B1A-B442-13831C5D930C":
										// cotton modules
										var modules = prop.ReportedPropertyValues.Any(id => id.LoadPropertyId == new Guid("7E73EE8A-A401-4F1C-AF31-48CC9670A98B")) ? prop.ReportedPropertyValues.FirstOrDefault(i => i.LoadPropertyId == new Guid("7E73EE8A-A401-4F1C-AF31-48CC9670A98B")) : null;
										storagetoStorageData.ModuleCount = modules != null && modules.ReportedValue != null ? decimal.Parse(modules.ReportedValue) as decimal? : null;
										break;
									case "82CB84C9-B851-4A49-A02D-51A26AB1605D":
										// mini cotton modules
										var minimodules = prop.ReportedPropertyValues.Any(id => id.LoadPropertyId == new Guid("537B880A-8130-4F38-966A-6003B0E27413")) ? prop.ReportedPropertyValues.FirstOrDefault(i => i.LoadPropertyId == new Guid("537B880A-8130-4F38-966A-6003B0E27413")) : null;
										storagetoStorageData.MiniModuleCount = minimodules != null && minimodules.ReportedValue != null ? decimal.Parse(minimodules.ReportedValue) as decimal? : null;
										break;
									case "b9075209-a856-497c-bf2a-e2cfda835eb8":
										// bill of lading
										var bol = prop.ReportedPropertyValues.Any(id => id.LoadPropertyId == new Guid("f093acfd-562e-4480-9b9d-268bc28d2fa8")) ? prop.ReportedPropertyValues.FirstOrDefault(i => i.LoadPropertyId == new Guid("f093acfd-562e-4480-9b9d-268bc28d2fa8")) : null;
										storagetoStorageData.BillofLadingCount = bol != null && bol.ReportedValue != null ? decimal.Parse(bol.ReportedValue) as decimal? : null;
										break;
									case "0CE65910-A1F2-4E09-906B-AC4F3C6F3C8A":
										// lint weight
										storagetoStorageData.LintWeight = prop != null && prop.ReportedPropertyValues.Any() && prop.ReportedPropertyValues.First().ReportedValue != null ? decimal.Parse(prop.ReportedPropertyValues.First().ReportedValue) as decimal? : null;
										storagetoStorageData.LintWeightUnit = prop != null && prop.ReportedPropertyValues.Any() && prop.ReportedPropertyValues.First().ReportedUnit != null ? prop.ReportedPropertyValues.First().ReportedUnit.AbbreviatedDisplay : string.Empty;
										break;
									case "F70E9E2A-4D90-4A81-9F53-7780B4F61D18":
										// seed weight
										storagetoStorageData.SeedWeight = prop != null && prop.ReportedPropertyValues.Any() && prop.ReportedPropertyValues.First().ReportedValue != null ? decimal.Parse(prop.ReportedPropertyValues.First().ReportedValue) as decimal? : null;
										storagetoStorageData.SeedWeightUnit = prop != null && prop.ReportedPropertyValues.Any() && prop.ReportedPropertyValues.First().ReportedUnit != null ? prop.ReportedPropertyValues.First().ReportedUnit.AbbreviatedDisplay : string.Empty;
										break;
									case "E394CACF-2DF5-4065-88A6-A93E0BEEDD40":
										// box count: boxes
										var boxes = prop.ReportedPropertyValues.Any(id => id.LoadPropertyId == new Guid("D48BBA6D-E24D-4ABA-B9BE-71BAEAA9301A")) ? prop.ReportedPropertyValues.FirstOrDefault(i => i.LoadPropertyId == new Guid("D48BBA6D-E24D-4ABA-B9BE-71BAEAA9301A")) : null;
										storagetoStorageData.BoxCount = boxes != null && boxes.ReportedValue != null ? decimal.Parse(boxes.ReportedValue) as decimal? : null;
										break;
									case "06f43787-4ea8-4a2b-af2a-55753c73f475":
										//protein%
										storagetoStorageData.ProteinPercent = prop != null && prop.ReportedPropertyValues.Any() && prop.ReportedPropertyValues.First().ReportedValue != null ? decimal.Parse(prop.ReportedPropertyValues.First().ReportedValue) as decimal? : null;
										break;
									case "96a50646-f70b-469a-8d5b-ce700ba6dae5":
										//plump%
										storagetoStorageData.PlumpPercent = prop != null && prop.ReportedPropertyValues.Any() && prop.ReportedPropertyValues.First().ReportedValue != null ? decimal.Parse(prop.ReportedPropertyValues.First().ReportedValue) as decimal? : null;
										break;
									case "42365775-b399-44e9-9a8b-09089d2281f1":
										//sugar%
										storagetoStorageData.SugarPercent = prop != null && prop.ReportedPropertyValues.Any() && prop.ReportedPropertyValues.First().ReportedValue != null ? decimal.Parse(prop.ReportedPropertyValues.First().ReportedValue) as decimal? : null;
										break;
									case "ddfb676d-bc91-47e8-b5d0-22896634f942":
										//sugar%
										storagetoStorageData.SLMPercent = prop != null && prop.ReportedPropertyValues.Any() && prop.ReportedPropertyValues.First().ReportedValue != null ? decimal.Parse(prop.ReportedPropertyValues.First().ReportedValue) as decimal? : null;
										break;
									default:
										break;
								}
							}
							items.Add(storagetoStorageData);

						}
					}                }
            }
            return items;
        }

        public List<ExcelDumpData.FieldToSale> GetFieldToSaleLoads(CropZoneId czID, CropYearId cropYearID, FlattenedTreeHierarchyItem treeData)
        {
            var cropZoneLoadData = clientEndpoint.GetView<CropZoneLoadDataView>(cropYearID).GetValue(() => null);
            List<ExcelDumpData.FieldToSale> items = new List<ExcelDumpData.FieldToSale>();

            var contracts = clientEndpoint.GetView<CropZoneDetailsView>(czID).Value.Contracts;
            var rentContractId = (from c in contracts
                                  where clientEndpoint.GetView<RentContractDetailsView>(c.Id).HasValue
                                  select c.Id).FirstOrDefault();

            var rentContract = rentContractId != null ? clientEndpoint.GetView<RentContractDetailsView>(rentContractId).Value : null;

            //This had to be done since the read model does not handle ContractRemove
            rentContract = rentContract != null && rentContract.CropZones.Any(x => x.Id == czID) ? rentContract : null;

            if (cropZoneLoadData != null)
            {
				var yieldLocations = clientEndpoint.GetView<YieldLocationListView>(new DataSourceId(cropYearID.DataSourceId)).GetValue(() => new YieldLocationListView()).YieldLocations;

				var locationsExcludedFromInventory = from loc in yieldLocations
													 where loc.IsActiveInCropYear(cropYearID.Id) && loc.IsQuantityExcludedFromInventory
													 select loc.Id.Id;

                var selectedLoads = from load in cropZoneLoadData.Items
									from loc in load.DestinationLocations
                                    where load.IsLoadSold
										&& load.CropZoneId == czID
										&& !locationsExcludedFromInventory.Contains(loc.Id.Id)
                                    select load;

                foreach (var load in selectedLoads)
                {
					foreach (var destinationLocation in load.DestinationLocations) {
						var loadDetail = clientEndpoint.GetView<LoadDetailsView>(load.LoadId);
						var fieldToSaleItem = new ExcelDumpData.FieldToSale();
						fieldToSaleItem.LoadDate = load.StartDateTime;
						fieldToSaleItem.LoadNumber = load.LoadNumber;
                        fieldToSaleItem.CropZoneId = treeData.CropZoneId.Id;
						fieldToSaleItem.Farm = treeData.FarmName;
						fieldToSaleItem.Field = treeData.FieldName;
						fieldToSaleItem.CropZone = treeData.CropZoneName;
						fieldToSaleItem.Crop = treeData.CropName;
						fieldToSaleItem.CropZoneArea = load.AreaValue;
						var contractDetails = load.RentContractId != null ? clientEndpoint.GetView<RentContractDetailsView>(load.RentContractId).Value : null;
						fieldToSaleItem.RentShares = contractDetails != null ? contractDetails.Name : string.Empty;
						fieldToSaleItem.DestinationName = destinationLocation.Name;
						fieldToSaleItem.OperationId = loadDetail.Value.OperationId.Id;
						fieldToSaleItem.OperationType = loadDetail.Value.OperationType.ToString();
						fieldToSaleItem.WeightedFinalDryQuantity = load.AreaWeightedFinalQuantity;

						/////////////////////////////////////////////////////////////////////////////
						fieldToSaleItem.GrowerQtyShare = rentContract != null ? (load.AreaWeightedFinalQuantity / load.DestinationLocations.Count) * (decimal)rentContract.IncludedShareInfo.GrowerCropShare : (load.AreaWeightedFinalQuantity / load.DestinationLocations.Count);
						fieldToSaleItem.LandOwnerQtyShare = rentContract != null ? (load.AreaWeightedFinalQuantity / load.DestinationLocations.Count) * (decimal)(1 - rentContract.IncludedShareInfo.GrowerCropShare) : 0;
						/////////////////////////////////////////////////////////////////////////////

						fieldToSaleItem.FinalUnit = load.FinalQuantityUnit.AbbreviatedDisplay;
						fieldToSaleItem.WeightedFinalPrice = load.AreaWeightedGrowerPrice + load.AreaWeightedLandOwnerPrice;

						if (loadDetail.Value.QuantityInformation != null) {
							var multiplier = load.AreaValue / (decimal)loadDetail.Value.CropZones.Sum(x => x.Area.Value);
							var qtyInfo = loadDetail.Value.QuantityInformation;
							fieldToSaleItem.Gross = qtyInfo.TruckGrossWeight;
							fieldToSaleItem.Tare = qtyInfo.TruckTareWeight;
							fieldToSaleItem.Net = qtyInfo.TruckNetWeight;
							fieldToSaleItem.Unit = qtyInfo.TruckWeightUnit.AbbreviatedDisplay;
							fieldToSaleItem.FinalQty = qtyInfo.FinalQuantityValue;
							fieldToSaleItem.FinalQtyUnit = qtyInfo.FinalQuantityUnit.AbbreviatedDisplay;
							fieldToSaleItem.InitialMoisturePercent = qtyInfo.InitialMoisturePercentage;
							fieldToSaleItem.TargetMoisturePercent = qtyInfo.StandardTargetMoisturePercentage;
							fieldToSaleItem.MoistureShrinkPercent = qtyInfo.StandardMoistureShrinkPercentage;
							fieldToSaleItem.MoistureShrinkQty = (qtyInfo.StandardMoistureShrinkQuantity / load.DestinationLocations.Count) * multiplier;
							fieldToSaleItem.ShrinkAdjustmentPercent = qtyInfo.AdjustmentShrinkPercentage;
							fieldToSaleItem.ShrinkAdjustmentQty = qtyInfo.AdjustmentShrinkQuantity * multiplier;
							fieldToSaleItem.HandlingShrinkPercent = qtyInfo.HandlingShrinkPercentage;
							fieldToSaleItem.HandlingShrinkQty = (qtyInfo.HandlingShrinkQuantity / load.DestinationLocations.Count) * multiplier;
						}

						fieldToSaleItem.DriverId = loadDetail.Value.DriverId != null ? loadDetail.Value.DriverId.Id : new Guid();
						fieldToSaleItem.DriverName = load.DriverName;
						fieldToSaleItem.TruckId = loadDetail.Value.TruckId != null ? loadDetail.Value.TruckId.Id : new Guid();
						fieldToSaleItem.TruckName = loadDetail.Value.TruckName;
						fieldToSaleItem.Notes = loadDetail.Value.Notes;


						//Production Contract Info
						var growerPriceInfo = loadDetail.Value.GrowerSharePriceInformation != null ? loadDetail.Value.GrowerSharePriceInformation : null;
						if (growerPriceInfo != null) {
							if (growerPriceInfo.ContractId != null) {
								var productionContract = clientEndpoint.GetView<ProductionContractDetailsView>(growerPriceInfo.ContractId).Value;
								fieldToSaleItem.Buyer = productionContract.Buyer != null ? productionContract.Buyer.Name : string.Empty;
								fieldToSaleItem.ContractId = productionContract.ContractNumber;
								fieldToSaleItem.GrowerId = productionContract.GrowerId;
							}
							fieldToSaleItem.GrossSalePrice = growerPriceInfo.GrossSalePrice;
							fieldToSaleItem.GrossUnitPrice = growerPriceInfo.GrossUnitSalePrice;
							fieldToSaleItem.SalePriceAdjustment = growerPriceInfo.SalePriceAdjustment;
							fieldToSaleItem.FinalUnitPrice = growerPriceInfo.FinalUnitSalePrice;
							fieldToSaleItem.FinalSalePrice = growerPriceInfo.FinalSalePrice;
						}

						var loadProperties = loadDetail.Value.ReportedPropertySets;

						foreach (var prop in loadProperties) {
							decimal tmpvalue;
							decimal? tmpresult = null;
							switch (prop.LoadPropertySetId.ToString().ToUpper()) {
								case "C9931DD8-F654-44F7-9F6C-E702FD9BDF1C":
									//do stuff
									if (decimal.TryParse(prop.ReportedPropertyValues.First().ReportedValue, out tmpvalue)) { tmpresult = tmpvalue; }
									fieldToSaleItem.TestWeight = prop != null && prop.ReportedPropertyValues.Any() && prop.ReportedPropertyValues.First().ReportedValue != null ? tmpresult : null;
									fieldToSaleItem.TestWeightUnit = prop != null && prop.ReportedPropertyValues.Any() && prop.ReportedPropertyValues.First().ReportedUnit != null ? prop.ReportedPropertyValues.First().ReportedUnit.AbbreviatedDisplay : string.Empty;
									break;
								case "71200E91-1717-479A-BF20-F8E2C4901969":
									//fm%
									if (decimal.TryParse(prop.ReportedPropertyValues.First().ReportedValue, out tmpvalue)) { tmpresult = tmpvalue; }
									fieldToSaleItem.ForiegnMaterialPercent = prop != null && prop.ReportedPropertyValues.Any() && prop.ReportedPropertyValues.First().ReportedValue != null ? tmpresult : null;
									break;
								case "CF0C81F5-C266-4AD7-B31D-947A3F2331EF":
									//Damage
									if (decimal.TryParse(prop.ReportedPropertyValues.First().ReportedValue, out tmpvalue)) { tmpresult = tmpvalue; }
									fieldToSaleItem.DamagePercent = prop != null && prop.ReportedPropertyValues.Any() && prop.ReportedPropertyValues.First().ReportedValue != null ? tmpresult : null;
									break;
								case "B8A44DF0-E93B-4240-A56E-FC7393960EFB":
									// large square bales
									var large = prop.ReportedPropertyValues.Any(id => id.LoadPropertyId == new Guid("0D25AF79-C6B2-4C81-8A58-6356F71C5BA6")) ? prop.ReportedPropertyValues.FirstOrDefault(i => i.LoadPropertyId == new Guid("0D25AF79-C6B2-4C81-8A58-6356F71C5BA6")) : null;
									fieldToSaleItem.LargeSquareBaleCount = large != null && large.ReportedValue != null ? decimal.Parse(large.ReportedValue) as decimal? : null;
									break;
								case "853C150B-1B91-459F-96C2-205B90F35FAF":
									// small square bales
									var small = prop.ReportedPropertyValues.Any(id => id.LoadPropertyId == new Guid("2C268AB3-B0B3-4C5A-81B8-B6EA720D1474")) ? prop.ReportedPropertyValues.FirstOrDefault(i => i.LoadPropertyId == new Guid("0D25AF79-C6B2-4C81-8A58-6356F71C5BA6")) : null;
									fieldToSaleItem.SquareBaleCount = small != null && small.ReportedValue != null ? decimal.Parse(small.ReportedValue) as decimal? : null;
									break;
								case "90EEF973-58A0-44D8-BC83-8B4690B452FC":
									// round bales
									var round = prop.ReportedPropertyValues.Any(id => id.LoadPropertyId == new Guid("32C20EA8-8B41-42AE-ABF0-CDF5E7C1B34E")) ? prop.ReportedPropertyValues.FirstOrDefault(i => i.LoadPropertyId == new Guid("32C20EA8-8B41-42AE-ABF0-CDF5E7C1B34E")) : null;
									fieldToSaleItem.RoundBaleCount = round != null && round.ReportedValue != null ? decimal.Parse(round.ReportedValue) as decimal? : null;
									break;
								case "0D8F01EE-5CEE-4B1A-B442-13831C5D930C":
									// cotton modules
									var modules = prop.ReportedPropertyValues.Any(id => id.LoadPropertyId == new Guid("7E73EE8A-A401-4F1C-AF31-48CC9670A98B")) ? prop.ReportedPropertyValues.FirstOrDefault(i => i.LoadPropertyId == new Guid("7E73EE8A-A401-4F1C-AF31-48CC9670A98B")) : null;
									fieldToSaleItem.ModuleCount = modules != null && modules.ReportedValue != null ? decimal.Parse(modules.ReportedValue) as decimal? : null;
									break;
								case "82CB84C9-B851-4A49-A02D-51A26AB1605D":
									// mini cotton modules
									var minimodules = prop.ReportedPropertyValues.Any(id => id.LoadPropertyId == new Guid("537B880A-8130-4F38-966A-6003B0E27413")) ? prop.ReportedPropertyValues.FirstOrDefault(i => i.LoadPropertyId == new Guid("537B880A-8130-4F38-966A-6003B0E27413")) : null;
									fieldToSaleItem.MiniModuleCount = minimodules != null && minimodules.ReportedValue != null ? decimal.Parse(minimodules.ReportedValue) as decimal? : null;
									break;
								case "b9075209-a856-497c-bf2a-e2cfda835eb8":
									// bill of lading
									var bol = prop.ReportedPropertyValues.Any(id => id.LoadPropertyId == new Guid("f093acfd-562e-4480-9b9d-268bc28d2fa8")) ? prop.ReportedPropertyValues.FirstOrDefault(i => i.LoadPropertyId == new Guid("f093acfd-562e-4480-9b9d-268bc28d2fa8")) : null;
									fieldToSaleItem.BillofLadingCount = bol != null && bol.ReportedValue != null ? decimal.Parse(bol.ReportedValue) as decimal? : null;
									break;
								case "0CE65910-A1F2-4E09-906B-AC4F3C6F3C8A":
									// lint weight
									fieldToSaleItem.LintWeight = prop != null && prop.ReportedPropertyValues.Any() && prop.ReportedPropertyValues.First().ReportedValue != null ? decimal.Parse(prop.ReportedPropertyValues.First().ReportedValue) as decimal? : null;
									fieldToSaleItem.LintWeightUnit = prop != null && prop.ReportedPropertyValues.Any() && prop.ReportedPropertyValues.First().ReportedUnit != null ? prop.ReportedPropertyValues.First().ReportedUnit.AbbreviatedDisplay : string.Empty;
									break;
								case "F70E9E2A-4D90-4A81-9F53-7780B4F61D18":
									// seed weight
									fieldToSaleItem.SeedWeight = prop != null && prop.ReportedPropertyValues.Any() && prop.ReportedPropertyValues.First().ReportedValue != null ? decimal.Parse(prop.ReportedPropertyValues.First().ReportedValue) as decimal? : null;
									fieldToSaleItem.SeedWeightUnit = prop != null && prop.ReportedPropertyValues.Any() && prop.ReportedPropertyValues.First().ReportedUnit != null ? prop.ReportedPropertyValues.First().ReportedUnit.AbbreviatedDisplay : string.Empty;
									break;
								case "E394CACF-2DF5-4065-88A6-A93E0BEEDD40":
									// box count: boxes
									var boxes = prop.ReportedPropertyValues.Any(id => id.LoadPropertyId == new Guid("D48BBA6D-E24D-4ABA-B9BE-71BAEAA9301A")) ? prop.ReportedPropertyValues.FirstOrDefault(i => i.LoadPropertyId == new Guid("D48BBA6D-E24D-4ABA-B9BE-71BAEAA9301A")) : null;
									fieldToSaleItem.BoxCount = boxes != null && boxes.ReportedValue != null ? decimal.Parse(boxes.ReportedValue) as decimal? : null;
									break;
								case "06f43787-4ea8-4a2b-af2a-55753c73f475":
									//protein%
									fieldToSaleItem.ProteinPercent = prop != null && prop.ReportedPropertyValues.Any() && prop.ReportedPropertyValues.First().ReportedValue != null ? decimal.Parse(prop.ReportedPropertyValues.First().ReportedValue) as decimal? : null;
									break;
								case "96a50646-f70b-469a-8d5b-ce700ba6dae5":
									//plump%
									fieldToSaleItem.PlumpPercent = prop != null && prop.ReportedPropertyValues.Any() && prop.ReportedPropertyValues.First().ReportedValue != null ? decimal.Parse(prop.ReportedPropertyValues.First().ReportedValue) as decimal? : null;
									break;
								case "42365775-b399-44e9-9a8b-09089d2281f1":
									//sugar%
									fieldToSaleItem.SugarPercent = prop != null && prop.ReportedPropertyValues.Any() && prop.ReportedPropertyValues.First().ReportedValue != null ? decimal.Parse(prop.ReportedPropertyValues.First().ReportedValue) as decimal? : null;
									break;
								case "ddfb676d-bc91-47e8-b5d0-22896634f942":
									//sugar%
									fieldToSaleItem.SLMPercent = prop != null && prop.ReportedPropertyValues.Any() && prop.ReportedPropertyValues.First().ReportedValue != null ? decimal.Parse(prop.ReportedPropertyValues.First().ReportedValue) as decimal? : null;
									break;
								default:
									break;
							}
						}

						items.Add(fieldToSaleItem);

					}                }
            }

            return items;
        }

        public List<ExcelDumpData.StorageToSale> GetStorageToSaleLoads()
        {
            var loadsMaybe = clientEndpoint.GetView<LoadListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear));
            List<ExcelDumpData.StorageToSale> items = new List<ExcelDumpData.StorageToSale>();

            if (loadsMaybe.HasValue)
            {
                var allLoads = loadsMaybe.Value;

                var selectedLoads = from l in allLoads.Loads
                                    where l.OperationType == ValueObjects.YieldOperationTypes.StorageToSale
                                    select l;

                foreach (var load in selectedLoads)
                {
					foreach (var sourceLocation in load.SourceLocations) {
						foreach (var destinationLocation in load.DestinationLocations) {
							var storagetoSaleItem = new ExcelDumpData.StorageToSale();
							storagetoSaleItem.LoadDate = load.StartDateTime;
							storagetoSaleItem.LoadId = load.Id.Id;
							storagetoSaleItem.LoadNumber = load.LoadNumber;
							storagetoSaleItem.Crop = load.Commodity.CropName;
							storagetoSaleItem.StorageLocation = clientEndpoint.GetView<YieldLocationDetailsView>(sourceLocation.Id).Value.Name;

							storagetoSaleItem.DestinationType = destinationLocation.YieldLocationType.ToString();
							storagetoSaleItem.OperationType = load.OperationType.ToString();

							var loadDetails = clientEndpoint.GetView<LoadDetailsView>(load.Id).Value;
							storagetoSaleItem.WeightedFinalDryQuantity = load.FinalQuantityValue;
							storagetoSaleItem.FinalUnit = load.FinalQuantityUnit.AbbreviatedDisplay;

							//TO DO :: FIND THE WEIGHTED FINAL PRICE

							storagetoSaleItem.RentShares = loadDetails.RentContractName;
							storagetoSaleItem.DestinationName = destinationLocation.Name;
							if (loadDetails.QuantityInformation != null) {

								storagetoSaleItem.Gross = loadDetails.QuantityInformation.TruckGrossWeight;
								storagetoSaleItem.Tare = loadDetails.QuantityInformation.TruckTareWeight;
								storagetoSaleItem.Net = loadDetails.QuantityInformation.TruckNetWeight;
								storagetoSaleItem.Unit = loadDetails.QuantityInformation.TruckWeightUnit.AbbreviatedDisplay;

								storagetoSaleItem.InitialMoisturePercent = loadDetails.QuantityInformation.InitialMoisturePercentage;
								storagetoSaleItem.TargetMoisturePercent = loadDetails.QuantityInformation.StandardTargetMoisturePercentage;
								storagetoSaleItem.MoistureShrinkPercent = loadDetails.QuantityInformation.StandardMoistureShrinkPercentage;
								storagetoSaleItem.MoistureShrinkQty = loadDetails.QuantityInformation.StandardMoistureShrinkQuantity;
								storagetoSaleItem.ShrinkAdjustmentPercent = loadDetails.QuantityInformation.AdjustmentShrinkPercentage;
								storagetoSaleItem.ShrinkAdjustmentQty = loadDetails.QuantityInformation.AdjustmentShrinkQuantity;
								storagetoSaleItem.HandlingShrinkPercent = loadDetails.QuantityInformation.HandlingShrinkPercentage;
								storagetoSaleItem.HandlingShrinkQty = (loadDetails.QuantityInformation.HandlingShrinkQuantity / loadDetails.DestinationLocations.Count);

								storagetoSaleItem.FinalQty = loadDetails.QuantityInformation.FinalQuantityValue;
								storagetoSaleItem.FinalQtyUnit = loadDetails.QuantityInformation.FinalQuantityUnit.AbbreviatedDisplay;
							}

							var growerPriceInfo = loadDetails.GrowerSharePriceInformation != null ? loadDetails.GrowerSharePriceInformation : null;
							if (growerPriceInfo != null) {
								if (growerPriceInfo.ContractId != null) {
									var productionContract = clientEndpoint.GetView<ProductionContractDetailsView>(growerPriceInfo.ContractId).Value;
									storagetoSaleItem.Buyer = productionContract.Buyer != null ? productionContract.Buyer.Name : string.Empty;
									storagetoSaleItem.ContractId = productionContract.ContractNumber;
									storagetoSaleItem.GrowerId = productionContract.GrowerId;
								}
								storagetoSaleItem.WeightedFinalPrice = growerPriceInfo.FinalSalePrice;
								storagetoSaleItem.GrossSalePrice = growerPriceInfo.GrossSalePrice;
								storagetoSaleItem.GrossUnitPrice = growerPriceInfo.GrossUnitSalePrice;
								storagetoSaleItem.SalePriceAdjustment = growerPriceInfo.SalePriceAdjustment;
								storagetoSaleItem.FinalUnitPrice = growerPriceInfo.FinalUnitSalePrice;
								storagetoSaleItem.FinalSalePrice = growerPriceInfo.FinalSalePrice;
							}

							storagetoSaleItem.DriverId = loadDetails.DriverId != null ? loadDetails.DriverId.Id : new Guid();
							storagetoSaleItem.DriverName = loadDetails.DriverName;
							storagetoSaleItem.TruckId = loadDetails.TruckId != null ? loadDetails.TruckId.Id : new Guid();
							storagetoSaleItem.TruckName = loadDetails.TruckName;
							storagetoSaleItem.Notes = loadDetails.Notes;

							var loadProperties = loadDetails.ReportedPropertySets;

							foreach (var prop in loadProperties) {
								decimal tmpvalue;
								decimal? tmpresult = null;

								switch (prop.LoadPropertySetId.ToString().ToUpper()) {
									case "C9931DD8-F654-44F7-9F6C-E702FD9BDF1C":
										//do stuff
										if (decimal.TryParse(prop.ReportedPropertyValues.First().ReportedValue, out tmpvalue)) { tmpresult = tmpvalue; }
										storagetoSaleItem.TestWeight = prop != null && prop.ReportedPropertyValues.Any() && !string.IsNullOrEmpty(prop.ReportedPropertyValues.First().ReportedValue) ? tmpresult : null;
										storagetoSaleItem.TestWeightUnit = prop != null && prop.ReportedPropertyValues.Any() && prop.ReportedPropertyValues.First().ReportedUnit != null ? prop.ReportedPropertyValues.First().ReportedUnit.AbbreviatedDisplay : string.Empty;
										break;
									case "71200E91-1717-479A-BF20-F8E2C4901969":
										//fm%
										if (decimal.TryParse(prop.ReportedPropertyValues.First().ReportedValue, out tmpvalue)) { tmpresult = tmpvalue; }
										storagetoSaleItem.ForiegnMaterialPercent = prop != null && prop.ReportedPropertyValues.Any() && prop.ReportedPropertyValues.First().ReportedValue != null ? tmpresult : null;
										break;
									case "CF0C81F5-C266-4AD7-B31D-947A3F2331EF":
										//Damage
										if (decimal.TryParse(prop.ReportedPropertyValues.First().ReportedValue, out tmpvalue)) { tmpresult = tmpvalue; }
										storagetoSaleItem.DamagePercent = prop != null && prop.ReportedPropertyValues.Any() && prop.ReportedPropertyValues.First().ReportedValue != null ? tmpresult : null;
										break;
									case "B8A44DF0-E93B-4240-A56E-FC7393960EFB":
										// large square bales
										var large = prop.ReportedPropertyValues.Any(id => id.LoadPropertyId == new Guid("0D25AF79-C6B2-4C81-8A58-6356F71C5BA6")) ? prop.ReportedPropertyValues.FirstOrDefault(i => i.LoadPropertyId == new Guid("0D25AF79-C6B2-4C81-8A58-6356F71C5BA6")) : null;
										storagetoSaleItem.LargeSquareBaleCount = large != null && large.ReportedValue != null ? decimal.Parse(large.ReportedValue) as decimal? : null;
										break;
									case "853C150B-1B91-459F-96C2-205B90F35FAF":
										// small square bales
										var small = prop.ReportedPropertyValues.Any(id => id.LoadPropertyId == new Guid("2C268AB3-B0B3-4C5A-81B8-B6EA720D1474")) ? prop.ReportedPropertyValues.FirstOrDefault(i => i.LoadPropertyId == new Guid("0D25AF79-C6B2-4C81-8A58-6356F71C5BA6")) : null;
										storagetoSaleItem.SquareBaleCount = small != null && small.ReportedValue != null ? decimal.Parse(small.ReportedValue) as decimal? : null;
										break;
									case "90EEF973-58A0-44D8-BC83-8B4690B452FC":
										// round bales
										var round = prop.ReportedPropertyValues.Any(id => id.LoadPropertyId == new Guid("32C20EA8-8B41-42AE-ABF0-CDF5E7C1B34E")) ? prop.ReportedPropertyValues.FirstOrDefault(i => i.LoadPropertyId == new Guid("32C20EA8-8B41-42AE-ABF0-CDF5E7C1B34E")) : null;
										storagetoSaleItem.RoundBaleCount = round != null && round.ReportedValue != null ? decimal.Parse(round.ReportedValue) as decimal? : null;
										break;
									case "0D8F01EE-5CEE-4B1A-B442-13831C5D930C":
										// cotton modules
										var modules = prop.ReportedPropertyValues.Any(id => id.LoadPropertyId == new Guid("7E73EE8A-A401-4F1C-AF31-48CC9670A98B")) ? prop.ReportedPropertyValues.FirstOrDefault(i => i.LoadPropertyId == new Guid("7E73EE8A-A401-4F1C-AF31-48CC9670A98B")) : null;
										storagetoSaleItem.ModuleCount = modules != null && modules.ReportedValue != null ? decimal.Parse(modules.ReportedValue) as decimal? : null;
										break;
									case "82CB84C9-B851-4A49-A02D-51A26AB1605D":
										// mini cotton modules
										var minimodules = prop.ReportedPropertyValues.Any(id => id.LoadPropertyId == new Guid("537B880A-8130-4F38-966A-6003B0E27413")) ? prop.ReportedPropertyValues.FirstOrDefault(i => i.LoadPropertyId == new Guid("537B880A-8130-4F38-966A-6003B0E27413")) : null;
										storagetoSaleItem.MiniModuleCount = minimodules != null && minimodules.ReportedValue != null ? decimal.Parse(minimodules.ReportedValue) as decimal? : null;
										break;
									case "b9075209-a856-497c-bf2a-e2cfda835eb8":
										// bill of lading
										var bol = prop.ReportedPropertyValues.Any(id => id.LoadPropertyId == new Guid("f093acfd-562e-4480-9b9d-268bc28d2fa8")) ? prop.ReportedPropertyValues.FirstOrDefault(i => i.LoadPropertyId == new Guid("f093acfd-562e-4480-9b9d-268bc28d2fa8")) : null;
										storagetoSaleItem.BillofLadingCount = bol != null && bol.ReportedValue != null ? decimal.Parse(bol.ReportedValue) as decimal? : null;
										break;
									case "0CE65910-A1F2-4E09-906B-AC4F3C6F3C8A":
										// lint weight
										storagetoSaleItem.LintWeight = prop != null && prop.ReportedPropertyValues.Any() && prop.ReportedPropertyValues.First().ReportedValue != null ? decimal.Parse(prop.ReportedPropertyValues.First().ReportedValue) as decimal? : null;
										storagetoSaleItem.LintWeightUnit = prop != null && prop.ReportedPropertyValues.Any() && prop.ReportedPropertyValues.First().ReportedUnit != null ? prop.ReportedPropertyValues.First().ReportedUnit.AbbreviatedDisplay : string.Empty;
										break;
									case "F70E9E2A-4D90-4A81-9F53-7780B4F61D18":
										// seed weight
										storagetoSaleItem.SeedWeight = prop != null && prop.ReportedPropertyValues.Any() && prop.ReportedPropertyValues.First().ReportedValue != null ? decimal.Parse(prop.ReportedPropertyValues.First().ReportedValue) as decimal? : null;
										storagetoSaleItem.SeedWeightUnit = prop != null && prop.ReportedPropertyValues.Any() && prop.ReportedPropertyValues.First().ReportedUnit != null ? prop.ReportedPropertyValues.First().ReportedUnit.AbbreviatedDisplay : string.Empty;
										break;
									case "E394CACF-2DF5-4065-88A6-A93E0BEEDD40":
										// box count: boxes
										var boxes = prop.ReportedPropertyValues.Any(id => id.LoadPropertyId == new Guid("D48BBA6D-E24D-4ABA-B9BE-71BAEAA9301A")) ? prop.ReportedPropertyValues.FirstOrDefault(i => i.LoadPropertyId == new Guid("D48BBA6D-E24D-4ABA-B9BE-71BAEAA9301A")) : null;
										storagetoSaleItem.BoxCount = boxes != null && boxes.ReportedValue != null ? decimal.Parse(boxes.ReportedValue) as decimal? : null;
										break;
									case "06f43787-4ea8-4a2b-af2a-55753c73f475":
										//protein%
										storagetoSaleItem.ProteinPercent = prop != null && prop.ReportedPropertyValues.Any() && prop.ReportedPropertyValues.First().ReportedValue != null ? decimal.Parse(prop.ReportedPropertyValues.First().ReportedValue) as decimal? : null;
										break;
									case "96a50646-f70b-469a-8d5b-ce700ba6dae5":
										//plump%
										storagetoSaleItem.PlumpPercent = prop != null && prop.ReportedPropertyValues.Any() && prop.ReportedPropertyValues.First().ReportedValue != null ? decimal.Parse(prop.ReportedPropertyValues.First().ReportedValue) as decimal? : null;
										break;
									case "42365775-b399-44e9-9a8b-09089d2281f1":
										//sugar%
										storagetoSaleItem.SugarPercent = prop != null && prop.ReportedPropertyValues.Any() && prop.ReportedPropertyValues.First().ReportedValue != null ? decimal.Parse(prop.ReportedPropertyValues.First().ReportedValue) as decimal? : null;
										break;
									case "ddfb676d-bc91-47e8-b5d0-22896634f942":
										//sugar%
										storagetoSaleItem.SLMPercent = prop != null && prop.ReportedPropertyValues.Any() && prop.ReportedPropertyValues.First().ReportedValue != null ? decimal.Parse(prop.ReportedPropertyValues.First().ReportedValue) as decimal? : null;
										break;
									default:
										break;
								}
							}
							items.Add(storagetoSaleItem);

						}
					} 
				}
            }
            return items;
        }


        public List<ExcelDumpData.SourceDocumentItemData> GetSourceRecords(List<ApplicationId> appIds)
        {
            List<ExcelDumpData.SourceDocumentItemData> items = new List<ExcelDumpData.SourceDocumentItemData>();
            foreach (var app in appIds)
            {
                var appDetail = clientEndpoint.GetView<ApplicationView>(app).Value;
                foreach (var source in appDetail.Sources)
                {
                    ExcelDumpData.SourceDocumentItemData sourceItem = new ExcelDumpData.SourceDocumentItemData() { SourceId = source.Identity, ApplicationId = app, SourceType = source.DocumentType };
                    items.Add(sourceItem);
                }
            }

            return items;
        }

        public List<ExcelDumpData.InvoiceItemData> GetInvoiceItems()
        {
            List<ExcelDumpData.InvoiceItemData> invoices = new List<ExcelDumpData.InvoiceItemData>();

            var invoiceListMaybe = clientEndpoint.GetView<InvoiceListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear));
            if (invoiceListMaybe.HasValue)
            {
                foreach (var inv in invoiceListMaybe.Value.Invoices)
                {
                    var invDetail = clientEndpoint.GetView<InvoiceView>(inv.Id).Value;
                    foreach (var invProd in invDetail.Products)
                    {
                        var mlp = clientEndpoint.GetMasterlistService().GetProduct(invProd.ProductId);
                        var invoice = new ExcelDumpData.InvoiceItemData()
                        {
                            Id = invDetail.Id.Id,
                            InvoiceDate = invDetail.InvoiceDate,
                            InvoiceDueDate = invDetail.InvoiceDueDate,
                            InvoiceNumber = invDetail.InvoiceNumber,
                            ProductId = invProd.ProductId.Id,
                            Vendor = invDetail.Vendor,
                            Product = mlp.Name,
                            TotalCost = invProd.TotalCost,
                            CostPerUnit = invProd.TotalProductValue > 0 && invProd.TotalCost > 0 ? invProd.TotalCost / invProd.TotalProductValue : 0m,
                            TotalProductUnit = invProd.TotalProductUnit,
                            TotalProductValue = invProd.TotalProductValue,
                            ProductType = mlp.ProductType,
                            Manufacturer = mlp.Manufacturer,
                            Notes = invDetail.Notes,
                        };

                        invoices.Add(invoice);
                    }
                }
            }


            return invoices;
        }
    }

    public class UniqueProducts
    {
        public ProductId ProductId { get; set; }
        public MiniProduct MasterListProduct { get; set; }
        public List<ReiPhi> REIPHIS { get; set; }
    }

    public class UniqueApplications
    {
        public ApplicationId AppID { get; set; }
        public ApplicationView AppDetails { get; set; }
    }
}
