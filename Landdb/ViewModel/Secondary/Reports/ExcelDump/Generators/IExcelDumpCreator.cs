﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.ExcelDump.Generators
{
    public interface IExcelDumpCreator
    {
        ReportSource Generate(ExcelQueryCriteria criteria);
        string DisplayName { get; }
    }
}
