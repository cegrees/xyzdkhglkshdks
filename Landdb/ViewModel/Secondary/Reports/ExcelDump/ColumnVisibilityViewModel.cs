﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Landdb.ViewModel.Secondary.Reports.ExcelDump
{
    public class ColumnVisibilityViewModel : ViewModelBase
    {
        bool vis;
        bool enabled;

        public ColumnVisibilityViewModel()
        {
            Children = new ObservableCollection<ColumnVisibilityViewModel>();
        }

        public string Name { get; set; }

        public string Header
        {
            get
            {
                //string newColName = Name;

                //for (int i = 1; i < newColName.Length; i++)
                //{
                //    if (char.IsUpper(newColName[i]))
                //    {
                //        newColName = newColName.Insert(i, " ");
                //        i++;
                //    }
                //}
                return Name;
            }
        }

        public bool Visibility
        {
            get
            {
                return vis;
            }
            set
            {
                vis = value;

                foreach (var child in Children)
                {
                    child.Visibility = vis;
                }

                RaisePropertyChanged("Visibility");
                RaisePropertyChanged("Children");
            }
        }

        public bool IsExpanded { get; set; }
        public bool IsSelected { get; set; }

        public bool Enabled
        {
            get
            {
                return enabled;
            }
            set
            {
                enabled = value;
                IsExpanded = false;
                foreach (var child in Children) { child.Enabled = enabled; }
                RaisePropertyChanged("Enabled");
                RaisePropertyChanged("IsExpanded");
            }
        }

        public ObservableCollection<ColumnVisibilityViewModel> Children { get; set; }
    }
}
