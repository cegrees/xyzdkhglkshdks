﻿using GalaSoft.MvvmLight;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Infrastructure;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Landdb.Resources;

namespace Landdb.ViewModel.Secondary.Reports.Specialty
{
    public class PesticideUseViewModel : ViewModelBase
    {
        IClientEndpoint endPoint;
        int cropYear;
        DateTime startDate;
        bool showCalendar;
        string selectedMonth;
        string company;
        string address;
        string city;
        string zipCode;

        public PesticideUseViewModel(IClientEndpoint endPoint, int cropYear)
        {
            this.endPoint = endPoint;
            this.cropYear = cropYear;
            startDate = DateTime.Now;
            SigningDate = DateTime.Now;
            CropYears = new List<int>();
            PopulateCropYearList();
            ApplicationCompanys = new List<ApplicationCompany>();
            Task appCompanyTask = Task.Factory.StartNew(() => GetApplicationCompanies());
            SelectedCropYear = ApplicationEnvironment.CurrentCropYear;
            ShowCalendar = false;

            UpdateDataSourceAddress();
        }

        public DateTime StartDate { get { return startDate; } set { startDate = value; EndDate = startDate.AddDays(7.0); RaisePropertyChanged("EndDate"); } }
        public DateTime EndDate { get; set; }
        public DateTime SigningDate { get; set; }
        public List<int> CropYears { get; set; }
        public int SelectedCropYear { get; set; }
        public string SignedBy { get; set; }

        public string Company { get { return company; } set { company = value; } }
        public string Address { get { return address; } set { address = value; } }
        public string City { get { return city; } set { city = value; } }
        public string ZipCode { get { return zipCode; } set { zipCode = value; } }

        public CompanyId ApplicationCompanyID { get; set; }
        public string Type { get { return "PesticideUse"; } }
        public List<string> Months { get { var months = DateTimeFormatInfo.CurrentInfo.MonthNames.ToList(); SelectedMonth = months[0]; return months; } }
        public string SelectedMonth 
        { 
            get 
            { 
                return selectedMonth; 
            } 
            set 
            { 
                selectedMonth = value;
                var monthDictionary = MonthDictionary();
                StartDate = new DateTime(cropYear, monthDictionary[selectedMonth], 1);
                RaisePropertyChanged("StartDate");
            } 
        }

        private Dictionary<string, int> MonthDictionary()
        {
            Dictionary<string, int> months = new Dictionary<string,int>();

            months.Add(Strings.Month_January_Text, 1);
            months.Add(Strings.Month_February_Text, 2);
            months.Add(Strings.Month_March_Text, 3);
            months.Add(Strings.Month_April_Text, 4);
            months.Add(Strings.Month_May_Text, 5);
            months.Add(Strings.Month_June_Text, 6);
            months.Add(Strings.Month_July_Text, 7);
            months.Add(Strings.Month_August_Text, 8);
            months.Add(Strings.Month_September_Text, 9);
            months.Add(Strings.Month_October_Text, 10);
            months.Add(Strings.Month_November_Text, 11);
            months.Add(Strings.Month_December_Text, 12);

            return months;
        }

        public bool ShowCalendar { get { return showCalendar; } set { showCalendar = value; RaisePropertyChanged("ShowCalendar"); } }
        public List<ApplicationCompany> ApplicationCompanys { get; set; }
        public ApplicationCompany SelectedCompany { get; set; }

        void GetApplicationCompanies()
        {
            ApplicationCompanys.Add(new ApplicationCompany() { Id = new CompanyId(ApplicationEnvironment.CurrentDataSourceId, Guid.Empty), Company = Strings.All_Text.ToUpper() });
            var applicationsMaybe = endPoint.GetView<ApplicationList>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear));
            applicationsMaybe.IfValue(x =>
            {
                var applications = x.Applications;
                foreach (var a in applications)
                {
                    var appDetail = endPoint.GetView<ApplicationView>(a.Id).Value;
                    if (appDetail.Applicators != null && appDetail.Applicators.Count() > 0)
                    {
                        var companies = (from c in appDetail.Applicators
                                        select new ApplicationCompany() { Company = c.CompanyName, Id = c.CompanyId }).Distinct().ToList();
                        foreach (var comp in companies)
                        {
                            if (ApplicationCompanys.Where(t => t.Id == comp.Id).Count() == 0)
                            {
                                ApplicationCompanys.Add(comp);
                            }
                        }
                    }
                }
                
                RaisePropertyChanged("ApplicationCompanys");
            });
        }

        void PopulateCropYearList()
        {
            var currentCropYear = ApplicationEnvironment.CurrentCropYear;
            var nextCropYear = currentCropYear + 1;

            CropYears.Add(nextCropYear);
            CropYears.Add(currentCropYear);
            

            for (int i = 5; i > 0; i--)
            {
                currentCropYear -= 1;
                CropYears.Add(currentCropYear);
            }

        }

        public void ChangeDataSourceAddress()
        {
            var settings = endPoint.GetUserSettings();

            var comp = settings.UserDataSourcesInfo != null ? settings.UserDataSourcesInfo.FirstOrDefault(x => x.DataSourceId == new DataSourceId(ApplicationEnvironment.CurrentDataSourceId)) : null;

            if (comp != null)
            {
                if (comp.DataSourceName != Company || comp.Address != Address || comp.City != City || comp.ZipCode != ZipCode)
                {
                    comp.ZipCode = ZipCode;
                    comp.City = City;
                    comp.Address = Address;
                    comp.DataSourceName = Company;
                    endPoint.SaveUserSettings();
                }
            }
            else
            {
                var newDSCompInfo = new DataSourceCompanyInfo()
                {
                     DataSourceId = new DataSourceId(ApplicationEnvironment.CurrentDataSourceId),
                     DataSourceName = Company,
                     Address = Address,
                     City = City,
                     ZipCode = ZipCode,
                };

                settings.UserDataSourcesInfo.Add(newDSCompInfo);
            }

			endPoint.SaveUserSettings();
        }

        void UpdateDataSourceAddress()
        {
            var settings = endPoint.GetUserSettings();
            var comp = settings.UserDataSourcesInfo != null ? settings.UserDataSourcesInfo.FirstOrDefault(x => x.DataSourceId == new DataSourceId(ApplicationEnvironment.CurrentDataSourceId)) : null;

            if (comp != null)
            {
                ZipCode = comp.ZipCode;
                City = comp.City;
                Address = comp.Address;
                Company = comp.DataSourceName;
            }
        }
    }

    public class ApplicationCompany
    {
        public string Company { get; set; }
        public CompanyId Id { get; set; }

        public override string ToString()
        {
            return Company;
        }
    }
}
