﻿using Landdb.Client.Infrastructure;
using Landdb.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Secondary.Reports.Specialty
{
    public class WSDAViewModel
    {
        IClientEndpoint endPoint;
        int cropYear;
        string company;
        string address;
        string city;
        string state;
        string zipCode;

        public WSDAViewModel(IClientEndpoint endPoint, int cropYear)
        {
            this.endPoint = endPoint;
            this.cropYear = cropYear;
            ReportStartDate = new DateTime(cropYear, 1, 1);
            ReportEndDate = new DateTime(cropYear, 12, 1);

            UpdateDataSourceAddress();
        }

        public DateTime ReportStartDate { get; set; }
        public DateTime ReportEndDate { get; set; }

        public string Company { get { return company; } set { company = value; } }
        public string Address { get { return address; } set { address = value; } }
        public string City { get { return city; } set { city = value; } }
        public string State { get { return state; } set { state = value; } }
        public string ZipCode { get { return zipCode; } set { zipCode = value; } }

        public void ChangeDataSourceAddress()
        {
            var settings = endPoint.GetUserSettings();

            var comp = settings.UserDataSourcesInfo != null ? settings.UserDataSourcesInfo.FirstOrDefault(x => x.DataSourceId == new DataSourceId(ApplicationEnvironment.CurrentDataSourceId)) : null;

            if (comp != null)
            {
                if (comp.DataSourceName != Company || comp.Address != Address || comp.City != City || comp.ZipCode != ZipCode || comp.State != State)
                {
                    comp.ZipCode = ZipCode;
                    comp.City = City;
                    comp.Address = Address;
                    comp.DataSourceName = Company;
                    comp.State = State;
                    endPoint.SaveUserSettings();
                }
            }
            else
            {
                var newDSCompInfo = new DataSourceCompanyInfo()
                {
                    DataSourceId = new DataSourceId(ApplicationEnvironment.CurrentDataSourceId),
                    DataSourceName = Company,
                    Address = Address,
                    City = City,
                    State = State,
                    ZipCode = ZipCode,
                };

                settings.UserDataSourcesInfo.Add(newDSCompInfo);
            }

        }

        void UpdateDataSourceAddress()
        {
            var settings = endPoint.GetUserSettings();
            var comp = settings.UserDataSourcesInfo != null ? settings.UserDataSourcesInfo.FirstOrDefault(x => x.DataSourceId == new DataSourceId(ApplicationEnvironment.CurrentDataSourceId)) : null;

            if (comp != null)
            {
                ZipCode = comp.ZipCode;
                City = comp.City;
                State = comp.State;
                Address = comp.Address;
                Company = comp.DataSourceName;
            }
        }
    }
}
