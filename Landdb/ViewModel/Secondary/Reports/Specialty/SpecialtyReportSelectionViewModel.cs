﻿using GalaSoft.MvvmLight;
using Landdb.Client.Infrastructure;
using Landdb.ViewModel.Secondary.Reports.Specialty.Generators;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Landdb.ViewModel.Secondary.Reports.Specialty
{
    public class SpecialtyReportSelectionViewModel : AbstractListPage<ISpecialtyReportGenerator, object>
    {
        IClientEndpoint endpoint;
        int cropyear;
        ISpecialtyReportGenerator selectedGenerator;
        Object model;

        public SpecialtyReportSelectionViewModel(IClientEndpoint endpoint, Dispatcher dispatcher, int cropYear)
            : base(endpoint, dispatcher)
        {
            this.endpoint = endpoint;
            this.cropyear = cropYear;
            ReportGenerators = new ObservableCollection<ISpecialtyReportGenerator>();
            AddSpecialtyReportGenerators(endpoint, cropYear);
            RefreshItemsList();
        }

        public ObservableCollection<ISpecialtyReportGenerator> ReportGenerators { get; private set; }



        public ISpecialtyReportGenerator SelectedReportGenerator
        {
            get { return selectedGenerator; }
            set
            {
                selectedGenerator = value;
                //UpdateReport();
                CreateDetailsModel(selectedGenerator);
                RaisePropertyChanged("SelectedReportGenerator");
            }
        }

        public Object DetailsModel 
        { 
            get { return model; } 
            set 
            { 
                model = value; 
                RaisePropertyChanged("DetailsModel"); 
            } 
        }

        void AddSpecialtyReportGenerators(IClientEndpoint clientEndpoint, int cropYear)
        {
            ReportGenerators.Add(new MonthlyPesticideUseGenerator(clientEndpoint, cropyear));
            ReportGenerators.Add(new WSDAGenerator(clientEndpoint, cropyear));
            ReportGenerators.Add(new Form1080Generator(clientEndpoint, cropyear));
            ReportGenerators.Add(new AerialApplicatorGenerator(clientEndpoint, cropyear));
            SelectedReportGenerator = ReportGenerators[0];
        }

        protected override IEnumerable<ISpecialtyReportGenerator> GetListItemModels()
        {
            return ReportGenerators;
        }

        protected override object CreateDetailsModel(ISpecialtyReportGenerator selectedItem)
        {
            switch (selectedGenerator.ReportType)
            {
                case "PesticideUse":
                    DetailsModel = new PesticideUseViewModel(endpoint, cropyear);
                    break;
                case "WSDA":
                    DetailsModel = new WSDAViewModel(endpoint, cropyear);
                    break;
                case "Form 1080":
                    DetailsModel = new WSDAViewModel(endpoint, cropyear);
                    break;
                case "AerialApplicator":
                    DetailsModel = new AerialInfoViewModel(endpoint, cropyear);
                    break;
                default:
                    break;
            }
            return DetailsModel;
        }

        protected override async Task<ScreenDescriptor> CreateScreenDescriptorForNewItemCreation()
        {
            throw new NotImplementedException();
        }

        protected override bool PerformItemRemoval(ISpecialtyReportGenerator selectedItem)
        {
            throw new NotImplementedException();
        }

        protected override IDomainCommand CreateSetCharmCommand(ISpecialtyReportGenerator selectedItem, string charmName)
        {
            throw new NotImplementedException();
        }
    }
}
