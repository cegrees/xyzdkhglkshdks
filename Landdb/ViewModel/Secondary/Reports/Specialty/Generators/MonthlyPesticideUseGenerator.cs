﻿using AgC.UnitConversion;
using AgC.UnitConversion.Area;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.ReportModels.SpecialtyReportsModel;
using Landdb.ViewModel.Secondary.Reports.Specialty.Generators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Landdb.Resources;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.Specialty
{
    public class MonthlyPesticideUseGenerator : ISpecialtyReportGenerator
    {

        IClientEndpoint clientEndpoint;
        int cropYear;
        Dictionary<int, string> CountyDictionary;

        public MonthlyPesticideUseGenerator(IClientEndpoint clientEndpoint, int cropYear)
        {
            this.clientEndpoint = clientEndpoint;
            this.cropYear = cropYear;
            CountyDictionary = new Dictionary<int, string>();
            InitializeCounties();
        }

        public ReportSource Generate(SpecialtyQueryCriteria criteria)
        {
            ReportBook book = new ReportBook();
            var rs = new InstanceReportSource();

            var applicationData = clientEndpoint.GetView<CropZoneApplicationDataView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new CropZoneApplicationDataView());
            var inventory = clientEndpoint.GetView<InventoryListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new InventoryListView());
            var flattenedHierarchy = clientEndpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new FlattenedTreeHierarchyView());
            var masterlist = clientEndpoint.GetMasterlistService();

            //Group by applicationId and add up the product info associated with the selected cropzones.  
            //iterate over by application id count...unique ids....

            if (criteria.SelectedCropZones.Any())
            {
                foreach (var czId in criteria.SelectedCropZones)
                {
                    var czTreeData = (from f in flattenedHierarchy.Items
                                      where f.CropZoneId == czId
                                      select f).FirstOrDefault();
                    var czDetails = clientEndpoint.GetView<CropZoneDetailsView>(czId).GetValue(new CropZoneDetailsView());
                    var fieldDetails = clientEndpoint.GetView<FieldDetailsView>(czDetails.FieldId).GetValue(new FieldDetailsView());
                    var data = new PesticideUseData();

                    
                    var settings = clientEndpoint.GetUserSettings();
                    data.DataSourceName = ApplicationEnvironment.CurrentDataSourceName; //string.IsNullOrEmpty(settings.DataSourceName) ? ApplicationEnvironment.CurrentDataSourceName : settings.DataSourceName;
                    var comp = settings.UserDataSourcesInfo != null ? settings.UserDataSourcesInfo.FirstOrDefault(x => x.DataSourceId == new DataSourceId(ApplicationEnvironment.CurrentDataSourceId)) : null;

                    if (comp != null)
                    {
                        data.DataSourceName = string.IsNullOrEmpty(comp.DataSourceName) ? ApplicationEnvironment.CurrentDataSourceName : comp.DataSourceName;
                        data.Address = string.IsNullOrEmpty(comp.Address) ? string.Empty : comp.Address;
                        data.City = string.IsNullOrEmpty(comp.City) ? string.Empty : comp.City;
                        data.ZipCode = string.IsNullOrEmpty(comp.ZipCode) ? string.Empty : comp.ZipCode;
                    }
                    if (czDetails.ReportedArea != null && czDetails.ReportedArea > 0)
                    {
                        data.AreaDisplay = UnitFactory.GetUnitByName(czDetails.ReportedAreaUnit).GetMeasure(czDetails.ReportedArea.Value).FullDisplay;
                    }
                    else if (czDetails.BoundaryArea != null)
                    {
                        data.AreaDisplay = UnitFactory.GetUnitByName(czDetails.BoundaryAreaUnit).GetMeasure(czDetails.BoundaryArea.Value).FullDisplay;
                    }
                    else
                    {
                        data.AreaDisplay = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(0).FullDisplay;
                    }
                    
                    data.Month = new DateTime(criteria.QueryStartDate.Year, criteria.QueryStartDate.Month, 1).ToString("MMMM");
                    data.Year = criteria.QueryStartDate.Year.ToString();
                    var czPermitNumber = czDetails.PermitId;
                    var fieldPermitNumber = fieldDetails.PermitId;
                    if (!string.IsNullOrEmpty(czPermitNumber))
                    {
                        data.PermitNumber = czPermitNumber;
                    }
                    else if (!string.IsNullOrEmpty(fieldPermitNumber))
                    {
                        data.PermitNumber = fieldPermitNumber;
                    }
                    else
                    {
                        var farmDetails = clientEndpoint.GetView<FarmDetailsView>(czTreeData.FarmId);
                        var farmPermit = farmDetails.HasValue ? farmDetails.Value.PermitId : null;
                        data.PermitNumber = !string.IsNullOrEmpty(farmPermit) ? farmPermit : string.Empty;
                    }

                    data.TownshipDisplay = fieldDetails.Township + fieldDetails.TownshipDirection;
                    data.RangeDisplay = fieldDetails.Range + fieldDetails.RangeDirection;
                    data.MeridianDisplay = fieldDetails.Meridian;
                    if(fieldDetails.County != null && CountyDictionary.Any(x => x.Value == fieldDetails.County.ToUpper())){
                        var value = CountyDictionary.FirstOrDefault(x => x.Value == fieldDetails.County.ToUpper());
                        data.CountyNumber = value.Key;
                    }
                    data.Section = fieldDetails.Section;
					data.SiteId = !string.IsNullOrEmpty(czDetails.SiteId) ? czDetails.SiteId : fieldDetails.SiteIdsByCropYear.ContainsKey(cropYear) ? fieldDetails.SiteIdsByCropYear[cropYear] : null;
                    data.CropZoneDisplay = masterlist.GetCropDisplay(czDetails.CropId);
                    data.FieldName = string.Format("{0} :: {1}", fieldDetails.FieldNameByCropYear[cropYear], czDetails.Name);
                    data.SignedBy = string.IsNullOrEmpty(criteria.SignedBy) ? string.Empty : criteria.SignedBy;
                    data.SigningDate = criteria.SigningDate;
                    data.ReportTitle = criteria.SevenDay ? Strings.ReportTitle_ProductionAgriculture7DayPesticideUseReport_Text.ToUpper() : Strings.ReportTitle_ProductionAgricultureMontlyPesticideUseReport_Text.ToUpper();
                    var czAppData = from x in applicationData.Items
                                    where x.CropZoneId == czId &&
                                    x.EndDate.ToLocalTime() >= criteria.QueryStartDate &&
                                    x.EndDate.ToLocalTime() <= criteria.QueryEndDate
                                    select x;

                    foreach (var czAppRecord in czAppData)
                    {
                        var item = new ReportLineItemData();
                        ApplicationView appDetails = clientEndpoint.GetView<ApplicationView>(czAppRecord.ApplicationId).Value;
                        var masterlistProd = masterlist.GetProduct(czAppRecord.ProductId);
                        if (masterlistProd.ProductType == GlobalStrings.ProductType_CropProtection)
                        {
                            var ReiPhis = masterlist.GetProductReiPhis(czAppRecord.ProductId);
                            //To Do :: affect this to reflect new changes
                            var help = 0.0; //to get this branch deployed
                            item.StartDate = appDetails.StartDateTime;
                            item.EndDate = appDetails.CustomEndDate.HasValue ? (DateTime)appDetails.CustomEndDate.Value.ToLocalTime() : czAppRecord.EndDate.ToLocalTime();
                            item.Area = string.Format("{0} {1}", (czAppRecord.AreaValue * czAppRecord.CoveragePercent).ToString("N2"), czAppRecord.AreaUnit);
                            item.ApplicationMethod = appDetails.Products.SingleOrDefault(x => x.TrackingId == czAppRecord.ProductTrackingId).ApplicationMethod;

                            var registrationNumber = masterlistProd.RegistrationNumber;

                            if (registrationNumber == null) { item.EPANumber = string.Empty; }
                            else
                            {
                                Regex epaNumberRegex = new Regex("([1-9][0-9]*)");

                                var matches = from m in epaNumberRegex.Matches(registrationNumber).Cast<Match>()
                                              select m.Value;
                                item.EPANumber = string.Join("-", matches.ToArray());
                            }

                            var displayTotal = DisplayMeasureConverter.ConvertToTotalDisplayMeasure(czAppRecord.ProductId, czAppRecord.TotalProductValue, czAppRecord.TotalProductUnit, masterlistProd.Density, clientEndpoint, ApplicationEnvironment.CurrentDataSourceId);
                            item.TotalProduct = displayTotal.Value;
                            item.TotalUnit = displayTotal.Unit.AbbreviatedDisplay;
                            var displayRate = DisplayMeasureConverter.ConvertToRateDisplayMeasure(czAppRecord.ProductId, czAppRecord.RateValue, czAppRecord.RateUnit, masterlistProd.Density, clientEndpoint, ApplicationEnvironment.CurrentDataSourceId);
                            item.Rate = displayRate.Value;
                            item.RateUnit = displayRate.Unit.AbbreviatedDisplay;
                            item.CarrierPerArea = appDetails.TankInformation != null ? (double)appDetails.TankInformation.CarrierPerAreaValue : 0.0;
                            item.ProductDisplay = string.Format("{0} {1}", masterlistProd.Name, masterlistProd.Manufacturer);
                            List<CropId> cropIds = new List<CropId>();
                            cropIds.Add(czDetails.CropId);

                            var parentCropIds = (from cid in cropIds
                                                 let mlCrop = masterlist.GetCrop(cid)
                                                 where mlCrop.ParentId.HasValue
                                                 select new CropId(mlCrop.ParentId.Value)).ToList();
                            cropIds.AddRange(parentCropIds);
                            List<ReiPhi> newReiPhis = new List<ReiPhi>();
                            foreach (var reiphi in ReiPhis)
                            {
                                if (cropIds.Any(c => c.Id == reiphi.Crop))
                                {
                                    newReiPhis.Add(reiphi);
                                }
                            }

                            if (newReiPhis != null && newReiPhis.Count() > 0)
                            {
                                item.REI = ((from r in newReiPhis select r.Rei).Max() / 24).ToString();
                            }
                            else
                            {
                                item.REI = "*";
                            }

                            var allCompaniesID = new CompanyId(ApplicationEnvironment.CurrentDataSourceId, Guid.Empty);
                            if (criteria.IncludedCompany == allCompaniesID)
                            {
                                //all applications...
                                data.CropProtectionItems.Add(item);
                            }
                            else
                            {
                                //applications that include this applicator....
                                var appCompList = from a in appDetails.Applicators
                                                  select a.CompanyId;

                                if (appCompList.Contains(criteria.IncludedCompany))
                                {
                                    data.CropProtectionItems.Add(item);
                                }
                            }
                        }
                    }

                    if (data.CropProtectionItems.Count() > 0)
                    {
                        var report = new Landdb.Views.Secondary.Reports.Specialty.MonthlyPesticideUse();
                        report.DataSource = data;
                        report.Name = data.DataSourceName;

                        book.Reports.Add(report);
                    }
                }
            }

            rs.ReportDocument = book;
            return rs;
        }

        void InitializeCounties()
        {
            var counties = clientEndpoint.GetMasterlistService().GetStateList().FirstOrDefault(c => c.Abbreviation == "CA").Counties;
            CountyDictionary.Add(1, "ALAMEDA"); CountyDictionary.Add(2, "ALPINE ");
            CountyDictionary.Add(3, "AMADOR"); CountyDictionary.Add(4, "BUTTE");
            CountyDictionary.Add(5, "CALAVERAS"); CountyDictionary.Add(6, "COLUSA");
            CountyDictionary.Add(7, "CONTRA COSTA"); CountyDictionary.Add(8, "DEL NORTE");
            CountyDictionary.Add(9, "EL DORADO"); CountyDictionary.Add(10, "FRESNO");
            CountyDictionary.Add(11, "GLENN"); CountyDictionary.Add(12, "HUMBOLDT");
            CountyDictionary.Add(13, "IMPERIAL"); CountyDictionary.Add(14, "INYO");
            CountyDictionary.Add(15, "KERN"); CountyDictionary.Add(16, "KINGS");
            CountyDictionary.Add(17, "LAKE"); CountyDictionary.Add(18, "LASSEN");
            CountyDictionary.Add(19, "LOS ANGELES"); CountyDictionary.Add(20, "MADERA");
            CountyDictionary.Add(21, "MARIN"); CountyDictionary.Add(22, "MARIPOSA");
            CountyDictionary.Add(23, "MENDOCINO"); CountyDictionary.Add(24, "MERCED");
            CountyDictionary.Add(25, "MODOC"); CountyDictionary.Add(26, "MONO");
            CountyDictionary.Add(27, "MONTEREY"); CountyDictionary.Add(28, "NAPA");
            CountyDictionary.Add(29, "NEVADA"); CountyDictionary.Add(30, "ORANGE");
            CountyDictionary.Add(31, "PLACER"); CountyDictionary.Add(32, "PLUMAS");
            CountyDictionary.Add(33, "RIVERSIDE"); CountyDictionary.Add(34, "SACRAMENTO");
            CountyDictionary.Add(35, "SAN BENITO"); CountyDictionary.Add(36, "SAN BERNARDINO");
            CountyDictionary.Add(37, "SAN DIEGO"); 
            CountyDictionary.Add(38, "SAN FRANCISCO");
            CountyDictionary.Add(39, "SAN JOAQUIN"); 
            CountyDictionary.Add(40, "SAN LUIS OBISPO");
            CountyDictionary.Add(41, "SAN MATEO"); 
            CountyDictionary.Add(42, "SANTA BARBARA");
            CountyDictionary.Add(43, "SANTA CLARA"); 
            CountyDictionary.Add(44, "SANTA CRUZ");
            CountyDictionary.Add(45, "SHASTA"); 
            CountyDictionary.Add(46, "SIERRA");
            CountyDictionary.Add(47, "SISKIYOU"); 
            CountyDictionary.Add(48, "SOLANO");
            CountyDictionary.Add(49, "SONOMA"); 
            CountyDictionary.Add(50, "STANISLAUS");
            CountyDictionary.Add(51, "SUTTER"); 
            CountyDictionary.Add(52, "TEHAMA");
            CountyDictionary.Add(53, "TRINITY"); 
            CountyDictionary.Add(54, "TULARE");
            CountyDictionary.Add(55, "TUOLUMNE"); 
            CountyDictionary.Add(56, "VENTURA");
            CountyDictionary.Add(57, "YOLO"); 
            CountyDictionary.Add(58, "YUBA");
        }

        public string DisplayName
        {
            get { return Strings.GeneratorName_PesticideUseReport_Text; }
        }


        public string ReportType
        {
            get { return "PesticideUse"; }
        }
    }
}
