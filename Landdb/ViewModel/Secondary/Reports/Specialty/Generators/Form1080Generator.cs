﻿using AgC.UnitConversion;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.ReportModels.SpecialtyReportsModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Landdb.Resources;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.Specialty.Generators
{
    public class Form1080Generator : ISpecialtyReportGenerator
    {
        IClientEndpoint endpoint;
        int cropyear;

        public Form1080Generator(IClientEndpoint endpoint, int cropyear)
        {
            this.cropyear = cropyear;
            this.endpoint = endpoint;
        }

        public Telerik.Reporting.ReportSource Generate(SpecialtyQueryCriteria criteria)
        {
            ReportBook book = new ReportBook();
            var rs = new InstanceReportSource();

            var applicationData = endpoint.GetView<CropZoneApplicationDataView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropyear)).GetValue(new CropZoneApplicationDataView());
            //var inventory = endpoint.GetView<InventoryListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropyear)).GetValue(new InventoryListView());
            var flattenedHierarchy = endpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropyear)).GetValue(new FlattenedTreeHierarchyView());
            var masterlist = endpoint.GetMasterlistService();

            //Group by applicationId and add up the product info associated with the selected cropzones.  
            //iterate over by application id count...unique ids....

            if (criteria.SelectedCropZones.Any())
            {
                var czAppData = (from x in applicationData.Items
                                from czId in criteria.SelectedCropZones
                                where x.CropZoneId == czId &&
                                x.StartDate >= criteria.QueryStartDate &&
                                x.StartDate <= criteria.QueryEndDate
                                select x.ApplicationId).Distinct().ToList();

                foreach (var app in czAppData)
                {
                    var data = new Form1080Data();
                    ///////////////////////////////////////////////
                    ////DO THE LIFTING HERE///////////////////////

                    ApplicationView appDetails = endpoint.GetView<ApplicationView>(app).Value;
                    data.ApplicationDate = appDetails.StartDateTime;
                    var totalArea = (from a in appDetails.CropZones
                                     where criteria.SelectedCropZones.Contains(a.Id)
                                     select a.Area.Value).Sum();
                    data.TotalArea = (decimal)totalArea;
                    var tankInfo = appDetails.TankInformation != null ? appDetails.TankInformation : null;
                    data.CarrierPerArea = tankInfo != null ? (tankInfo.CarrierPerAreaValue != null ? tankInfo.CarrierPerAreaValue : 0m) : 0m;

                    var multiplier = totalArea / appDetails.TotalArea.Value;
                    
                    foreach (var prod in appDetails.Products)
                    {
                        var productItem = new ProductItem();
                        var masterlistProd = masterlist.GetProduct(prod.Id);

                        if (masterlistProd.ProductType == GlobalStrings.ProductType_CropProtection)
                        {
                            productItem.RateValue = prod.RateValue;
                            productItem.RateUnit = UnitFactory.GetUnitByName(prod.RateUnit);
                            productItem.Product = masterlistProd.Name;

                            var registrationNumber = masterlistProd.RegistrationNumber;

                            if (registrationNumber == null) { productItem.EpaNumber = string.Empty; }
                            else
                            {
                                Regex epaNumberRegex = new Regex("([1-9][0-9]*)");

                                var matches = from m in epaNumberRegex.Matches(registrationNumber).Cast<Match>()
                                              select m.Value;
                                productItem.EpaNumber = string.Join("-", matches.ToArray());
                            }

                            var activeArray = from a in masterlistProd.ActiveIngredients
                                              select masterlist.GetActiveIngredientDisplay(new ActiveIngredientId(a.Id));
                            productItem.Active = string.Join(", ", activeArray);
                            productItem.TotalValue = prod.TotalProductValue * (decimal)multiplier;
                            productItem.TotalUnit = UnitFactory.GetUnitByName(prod.TotalProductUnit);
                            productItem.DilutionPer100 = "";
                            data.Products.Add(productItem);
                        }
                    }

                    
                    foreach (var cz in appDetails.CropZones)
                    {
                        var cropZoneItem = new CropZoneItem();
                        if (criteria.SelectedCropZones.Contains(cz.Id))
                        {
                            var czDetails = endpoint.GetView<CropZoneDetailsView>(cz.Id).Value;
                            var czField = endpoint.GetView<FieldDetailsView>(cz.FieldId).Value;
                            cropZoneItem.Acres = (decimal)cz.Area.Value;
                            cropZoneItem.Crop = masterlist.GetCropDisplay(czDetails.CropId);
                            cropZoneItem.Range = czField.Range + czField.RangeDirection;
                            cropZoneItem.Section = czField.Section;
                            cropZoneItem.Township = czField.Township + czField.TownshipDirection;

                            data.CropZones.Add(cropZoneItem);
                        }
                    }


                    ///////////////////////////////////////////////
                    if (data.Products.Count() > 0)
                    {
                        var report = new Landdb.Views.Secondary.Reports.Specialty.Arizona1080();
                        report.DataSource = data;
                        report.Name = data.DataSourceDisplay;

                        book.Reports.Add(report);
                    }
                }
            }

            rs.ReportDocument = book;
            return rs;
        }

        public string DisplayName
        {
            get { return Strings.GeneratorName_ArizonaForm1080_Text; }
        }

        public string ReportType
        {
            get { return "Form 1080"; }
        }
    }
}
