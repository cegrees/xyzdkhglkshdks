﻿using AgC.UnitConversion;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Company;
using Landdb.Domain.ReadModels.Map;
using Landdb.Domain.ReadModels.Person;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.ReportModels.SpecialtyReportsModel;
using Landdb.ViewModel.Secondary.Map;
using Landdb.Views.Secondary.Reports.Specialty;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Landdb.Resources;
using Telerik.Reporting;
using ThinkGeo.MapSuite.Core;

namespace Landdb.ViewModel.Secondary.Reports.Specialty.Generators
{
    public class AerialApplicatorGenerator : ISpecialtyReportGenerator
    {
        IClientEndpoint clientEndpoint;
        int cropYear;
        int areadecimals = 2;

        public AerialApplicatorGenerator(IClientEndpoint endpoint, int cropYear)
        {
            this.clientEndpoint = endpoint;
            this.cropYear = cropYear;
            var mapsettings = endpoint.GetMapSettings();
            areadecimals = mapsettings.AreaDecimals;
        }

        public Telerik.Reporting.ReportSource Generate(SpecialtyQueryCriteria criteria)
        {
            ReportBook book = new ReportBook();
            var rs = new InstanceReportSource();

            var flattenedHierarchy = clientEndpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new FlattenedTreeHierarchyView());

            if (criteria.SelectedCropZones.Any())
            {
                List<CropZoneDetailsView> CropZoneList = new List<CropZoneDetailsView>();
                List<AerialApplicator> data = new List<AerialApplicator>();
                List<Feature> allfilteredMapData = new List<Feature>();
                double totalArea = 0.0;

                foreach (var czId in criteria.SelectedCropZones)
                {
                    var aerialInfo = new AerialApplicator();
                    var czDetails = clientEndpoint.GetView<CropZoneDetailsView>(czId).Value;
                    CropZoneList.Add(czDetails);

                    var flattened = flattenedHierarchy.Items.FirstOrDefault(x => x.CropZoneId == czId);

                    aerialInfo.Farm = flattened.FarmName;
                    aerialInfo.Field = flattened.FieldName;
                    aerialInfo.CropZone = flattened.CropZoneName;
                    var area = czDetails.ReportedArea.HasValue ? czDetails.ReportedArea.Value : 0.0;
                    area = area == 0.0 && czDetails.BoundaryArea.HasValue ? czDetails.BoundaryArea.Value : area;
                    totalArea += area;
                    aerialInfo.AreaDisplay = string.Format("{0} {1}", area.ToString("N2"), UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit));
                    aerialInfo.Crop = flattened.CropName;

                    var mapsettings = clientEndpoint.GetMapSettings();
                    if (!string.IsNullOrEmpty(czDetails.CenterLatLong))
                    {
                        aerialInfo.CenterLatLong = DecimalDegrees.ddstring_To_DMs(czDetails.CenterLatLong, mapsettings.DisplayCoordinateFormat);
                    }

                    var companyDetails = clientEndpoint.GetView<CompanyDetailsView>(new CompanyId(ApplicationEnvironment.CurrentDataSourceId, criteria.ApplicationCompany));
                    aerialInfo.ApplicationCompany = companyDetails.HasValue ? companyDetails.Value.Name : string.Empty;

                    var applicatorMaybe = clientEndpoint.GetView<PersonDetailsView>(new PersonId(ApplicationEnvironment.CurrentDataSourceId, criteria.Applicator));
                    var applicator = applicatorMaybe.HasValue ? applicatorMaybe.Value : null;
                    aerialInfo.Applicator = applicator != null ? applicator.Name : string.Empty;
                    aerialInfo.LicenseNumber = applicator != null ? applicator.ApplicatorLicenseNumber : string.Empty;

                    var vendorDetails = clientEndpoint.GetView<CompanyDetailsView>(new CompanyId(ApplicationEnvironment.CurrentDataSourceId, criteria.Vendor));
                    aerialInfo.Vendor = vendorDetails.HasValue ? vendorDetails.Value.Name : string.Empty;

                    //GET MAP FOR CZ
                    List<Feature> filteredMapData = new List<Feature>();

                    var maps = clientEndpoint.GetView<ItemMap>(czId);
                    try
                    {
                        if (maps.HasValue)
                        {
                            if (maps.Value.MostRecentMapItem != null && maps.Value.MostRecentMapItem.MapData != string.Empty && maps.Value.MostRecentMapItem.MapData != "MULTIPOLYGON EMPTY") {
                                var fieldName = flattenedHierarchy.Items.FirstOrDefault(c => c.CropZoneId == czId).FieldName;
                                string customareaonlylabel = GetCustomAreaLabel(czId.Id, false);
                                string customarealabel = DisplayLabelWithArea(fieldName, customareaonlylabel);
                                IDictionary<string, string> columnvalues = new Dictionary<string, string>();
                                columnvalues.Add(@"LdbLabel", fieldName);
                                columnvalues.Add(@"AreaLabel", customarealabel);
                                columnvalues.Add(@"AreaOnlyLabel", customareaonlylabel);
                                Feature newfeature = new Feature(maps.Value.MostRecentMapItem.MapData, czId.Id.ToString(), columnvalues);
                                filteredMapData.Add(newfeature);
                            }
                        }
                    }
                    finally { }
                    allfilteredMapData.AddRange(filteredMapData);
                    //TODO :: need to change this up so we can send up the "scale"
                    var converter = new ConvertToBitmap(filteredMapData, true, clientEndpoint, criteria.ScaleBy, null, string.Empty, string.Empty);
                    aerialInfo.MapImage = converter.BitMap;

                    data.Add(aerialInfo);
                }


                //build info for all selected map and initial page

                var fullMapconverter = new ConvertToBitmap(allfilteredMapData, true, clientEndpoint, criteria.ScaleBy, null, string.Empty, string.Empty);
                var overViewPage = new AerialApplicator();
                overViewPage.MapImage = fullMapconverter.BitMap;
                overViewPage.DataSourceName = ApplicationEnvironment.CurrentDataSourceName;
                overViewPage.CropYear = cropYear.ToString();
                overViewPage.Notes = criteria.Notes;
                overViewPage.TotalAreaDisplay = string.Format("{0} {1}", totalArea.ToString("N2"), ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit);
                //add all the crop zone specific pages...

                var report = new AerialApplicatorOverview();
                report.DataSource = overViewPage;
                report.Name = Strings.ReportName_AerialApplicatorOverview_Text;

                book.Reports.Add(report);

                var details = new AerialApplicatorCropZone();
                details.DataSource = data;
                details.Name = Strings.ReportName_AerialApplicatorDetails_Text;

                book.Reports.Add(details);
            }

            rs.ReportDocument = book;
            return rs;
        }

        public string DisplayName
        {
            get { return Strings.GeneratorName_AerialApplicator_Text; }
        }

        public string ReportType
        {
            get { return "AerialApplicator"; }
        }

        private string DisplayLabelWithArea(string name, string customarealabel) {
            return name + "\r\n" + customarealabel;
        }

        private string GetCustomAreaLabel(Guid Id, bool isfieldlabel) {
            string stringid = Id.ToString();
            string labelname = string.Empty;
            if (isfieldlabel) {
                var fieldMaybe = clientEndpoint.GetView<FieldDetailsView>(new FieldId(ApplicationEnvironment.CurrentDataSourceId, Id));
                fieldMaybe.IfValue(field => {
                    labelname = string.Empty;
                    if (field.ReportedArea != null && !string.IsNullOrWhiteSpace(field.ReportedAreaUnit)) {
                        labelname = DisplaySelectedAreaOnlyDecimals(field.ReportedArea.Value, field.ReportedAreaUnit);
                    }
                    else if (field.BoundaryArea != null && !string.IsNullOrWhiteSpace(field.BoundaryAreaUnit)) {
                        labelname = DisplaySelectedAreaOnlyDecimals(field.BoundaryArea.Value, field.BoundaryAreaUnit);
                    }
                });
            }
            else {
                var fieldMaybe = clientEndpoint.GetView<CropZoneDetailsView>(new CropZoneId(ApplicationEnvironment.CurrentDataSourceId, Id));
                fieldMaybe.IfValue(field => {
                    labelname = string.Empty;
                    if (field.ReportedArea != null && !string.IsNullOrWhiteSpace(field.ReportedAreaUnit)) {
                        labelname = DisplaySelectedAreaOnlyDecimals(field.ReportedArea.Value, field.ReportedAreaUnit);
                    }
                    else if (field.BoundaryArea != null && !string.IsNullOrWhiteSpace(field.BoundaryAreaUnit)) {
                        labelname = DisplaySelectedAreaOnlyDecimals(field.BoundaryArea.Value, field.BoundaryAreaUnit);
                    }
                });
            }
            return labelname;
        }

        private string DisplaySelectedAreaOnlyDecimals(double unitvalue, string unitname) {
            IUnit _unit = UnitFactory.GetUnitByName(unitname);
            string decimalstring = "N" + areadecimals.ToString("N0");
            return unitvalue.ToString(decimalstring);
        }

    }
}
