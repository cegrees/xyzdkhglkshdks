﻿using AgC.UnitConversion;
using AgC.UnitConversion.Area;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Domain.ReadModels.Map;
using Landdb.Domain.ReadModels.Person;
using Landdb.Domain.ReadModels.Shared;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.ReportModels.SpecialtyReportsModel;
using Landdb.ViewModel.Secondary.Map;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Landdb.Resources;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.Specialty.Generators
{
    public class WSDAGenerator : ISpecialtyReportGenerator
    {
        IClientEndpoint endpoint;
        int cropyear;
        public WSDAGenerator(IClientEndpoint endpoint, int cropyear)
        {
            this.cropyear = cropyear;
            this.endpoint = endpoint;
        }

        public ReportSource Generate(SpecialtyQueryCriteria criteria)
        {
            ReportBook book = new ReportBook();
            var rs = new InstanceReportSource();

            var applicationData = endpoint.GetView<CropZoneApplicationDataView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropyear)).GetValue(new CropZoneApplicationDataView());
            var inventory = endpoint.GetView<InventoryListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropyear)).GetValue(new InventoryListView());
            var flattenedHierarchy = endpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropyear)).GetValue(new FlattenedTreeHierarchyView());
            var masterlist = endpoint.GetMasterlistService();

            //Group by applicationId and add up the product info associated with the selected cropzones.  
            //iterate over by application id count...unique ids....

            if (criteria.SelectedCropZones.Any())
            {
                foreach (var czId in criteria.SelectedCropZones)
                {
                    var czTreeData = (from f in flattenedHierarchy.Items
                                      where f.CropZoneId == czId
                                      select f).FirstOrDefault();
                    var czDetails = endpoint.GetView<CropZoneDetailsView>(czId).GetValue(new CropZoneDetailsView());
                    var fieldDetails = endpoint.GetView<FieldDetailsView>(czDetails.FieldId).GetValue(new FieldDetailsView());
                    var data = new WSDAData();


                    var settings = endpoint.GetUserSettings();
                    data.DataSourceName = ApplicationEnvironment.CurrentDataSourceName; //string.IsNullOrEmpty(settings.DataSourceName) ? ApplicationEnvironment.CurrentDataSourceName : settings.DataSourceName;
                    var comp = settings.UserDataSourcesInfo != null ? settings.UserDataSourcesInfo.FirstOrDefault(x => x.DataSourceId == new DataSourceId(ApplicationEnvironment.CurrentDataSourceId)) : null;

                    if (comp != null)
                    {
                        data.DataSourceName = string.IsNullOrEmpty(comp.DataSourceName) ? ApplicationEnvironment.CurrentDataSourceName : comp.DataSourceName;
                        data.Address = string.IsNullOrEmpty(comp.Address) ? string.Empty : comp.Address;
                        data.City = string.IsNullOrEmpty(comp.City) ? string.Empty : comp.City;
                        data.ZipCode = string.IsNullOrEmpty(comp.ZipCode) ? string.Empty : comp.ZipCode;
                        data.State = string.IsNullOrEmpty(comp.State) ? string.Empty : comp.State;
                    }
                    if (czDetails.ReportedArea != null && czDetails.ReportedArea > 0)
                    {
                        data.Area = UnitFactory.GetUnitByName(czDetails.ReportedAreaUnit).GetMeasure(czDetails.ReportedArea.Value).FullDisplay;
                    }
                    else if (czDetails.BoundaryArea != null)
                    {
                        data.Area = UnitFactory.GetUnitByName(czDetails.BoundaryAreaUnit).GetMeasure(czDetails.BoundaryArea.Value).FullDisplay;
                    }
                    else
                    {
                        data.Area = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(0).FullDisplay;
                    }

                    
                    data.CropYear = cropyear;
                    data.Township = fieldDetails.Township + fieldDetails.TownshipDirection;
                    data.Range = fieldDetails.Range + fieldDetails.RangeDirection;
                    data.Section = fieldDetails.Section;
                    data.CropZone = czDetails.Name;
                    data.Field = fieldDetails.FieldNameByCropYear[cropyear]; //.Name;
                    data.County = fieldDetails.County;
                    data.Farm = czTreeData.FarmName;
                    data.CenterLatLon = czDetails.CenterLatLong;
                    data.Crop = masterlist.GetCropDisplay(czDetails.CropId);
                    
                    var maps = endpoint.GetView<ItemMap>(czId);
                    try
                    {
                        if (maps.HasValue)
                        {
                            if (maps.Value.MostRecentMapItem != null && maps.Value.MostRecentMapItem.MapData != string.Empty && maps.Value.MostRecentMapItem.MapData != "MULTIPOLYGON EMPTY") {
                                var converter = new ConvertToBitmap(maps.Value.MostRecentMapItem.MapData, this.endpoint);
                                data.Map = converter.BitMap;
                            }
                        }
                    }
                    catch { }

                    var czAppData = from x in applicationData.Items
                                    where x.CropZoneId == czId &&
                                    x.StartDate >= criteria.QueryStartDate &&
                                    x.StartDate <= criteria.QueryEndDate
                                    select x;
                    int cnt = 0;
                    foreach (var czAppRecord in czAppData)
                    {
                        var item = new ProductLineItem();
                        ApplicationView appDetails = endpoint.GetView<ApplicationView>(czAppRecord.ApplicationId).Value;
                        var masterlistProd = masterlist.GetProduct(czAppRecord.ProductId);
                        if (masterlistProd.ProductType == GlobalStrings.ProductType_CropProtection)
                        {
                            item.StartDate = czAppRecord.StartDate.ToLocalTime();
                            item.EndDate = czAppRecord.EndDate.ToLocalTime();
                            item.Area = string.Format("{0} {1}", (czAppRecord.AreaValue * czAppRecord.CoveragePercent).ToString("N2"), czAppRecord.AreaUnit);
                            item.AppMethod = appDetails.Products.SingleOrDefault(x => x.TrackingId == czAppRecord.ProductTrackingId).ApplicationMethod;

                            var registrationNumber = masterlistProd.RegistrationNumber;

                            if (registrationNumber == null) { item.EpaNumber = string.Empty; }
                            else
                            {
                                Regex epaNumberRegex = new Regex("([1-9][0-9]*)");

                                var matches = from m in epaNumberRegex.Matches(registrationNumber).Cast<Match>()
                                              select m.Value;
                                item.EpaNumber = string.Join("-", matches.ToArray());
                            }
                            
                            
                            item.TotalValue = czAppRecord.TotalProductValue;
                            item.TotalUnit = UnitFactory.GetUnitByName(czAppRecord.TotalProductUnit);
                            item.RateValue = (double)czAppRecord.RateValue;
                            item.RateUnit = UnitFactory.GetUnitByName(czAppRecord.RateUnit);
                            item.CarrierPerArea = appDetails.TankInformation != null ? appDetails.TankInformation.CarrierPerAreaValue : 0m;
                            item.ProductName = string.Format("{0} {1}", masterlistProd.Name, masterlistProd.Manufacturer);
                            var aiArray = from i in masterlistProd.ActiveIngredients
                                          select masterlist.GetActiveIngredientDisplay(new ActiveIngredientId(i.Id));
                            item.Active = string.Join(", ", aiArray);

                            List<string> applicatorStrings = new List<string>();

                            foreach (var applicator in appDetails.Applicators)
                            {
								string address = string.Empty;
                                var personDetails = endpoint.GetView<Domain.ReadModels.Person.PersonDetailsView>(applicator.PersonId).GetValue(() => null);

                                //var applicatorString = string.Format("{0} :: {1}", personDetails.Name, personDetails.ApplicatorLicenseNumber);
                                var applicatorString = string.Format("{0} :: {1}", personDetails.Name, applicator.LicenseNumber);
                                applicatorStrings.Add(applicatorString);

								if (personDetails != null && personDetails.StreetAddress != null && !string.IsNullOrWhiteSpace(personDetails.StreetAddress.AddressLine1)) {
									address = personDetails.StreetAddress.AddressLine1;
								}

                                var app = new ApplicatorItem()
                                {
                                     Applicator = applicator.PersonName,
                                     Company = applicator.CompanyName,
                                     LicenseNumber = personDetails.ApplicatorLicenseNumber,
                                     Address = address,
                                     PhoneNumber = string.IsNullOrEmpty(personDetails.PrimaryPhoneNumber) ? string.Empty : personDetails.PrimaryPhoneNumber,
                                };

                                data.Applicators.Add(app);
                                data.Applicators = data.Applicators.Distinct(x => x.Applicator).ToList();
                            }

                            item.Applicator = string.Join(", ", applicatorStrings);

                            var conditions = appDetails.Conditions;
                            if(conditions != null){
                                item.Temp = conditions.TemperatureValue;
                                item.Humidity = conditions.HumidityPercent;
                                item.WindSpeed = conditions.WindSpeedValue;
                                item.WindDirection = string.IsNullOrEmpty(conditions.WindDirection) ? string.Empty : conditions.WindDirection;
                                item.SkyCondition = string.IsNullOrEmpty(conditions.SkyCondition) ? string.Empty : conditions.SkyCondition;
                                item.SoilCondition = string.IsNullOrEmpty(conditions.SoilMoisture) || conditions.SoilMoisture == "-1" ? string.Empty : conditions.SoilMoisture;
                            }
                            item.ItemCount = cnt;
                            data.Products.Add(item);
                            cnt++;
                        }

                        else if (masterlistProd.ProductType == GlobalStrings.ProductType_Seed)
                        {
                            data.VarietyDisplay += masterlistProd.Name + " (" + czAppRecord.StartDate.ToShortDateString() + ") ";

                            if (czAppRecord.AssociatedProducts != null && czAppRecord.AssociatedProducts.Count() > 0)
                            {
                                List<string> applicatorStrings = new List<string>();
                                foreach (var applicator in appDetails.Applicators)
                                {
                                    string address = string.Empty;
                                    var personDetails = endpoint.GetView<Domain.ReadModels.Person.PersonDetailsView>(applicator.PersonId).GetValue(() => null);

                                    //var applicatorString = string.Format("{0} :: {1}", personDetails.Name, personDetails.ApplicatorLicenseNumber);
                                    var applicatorString = string.Format("{0} :: {1}", personDetails.Name, applicator.LicenseNumber);
                                    applicatorStrings.Add(applicatorString);

                                    if (personDetails != null && personDetails.StreetAddress != null && !string.IsNullOrWhiteSpace(personDetails.StreetAddress.AddressLine1))
                                    {
                                        address = personDetails.StreetAddress.AddressLine1;
                                    }

                                    var app = new ApplicatorItem()
                                    {
                                        Applicator = applicator.PersonName,
                                        Company = applicator.CompanyName,
                                        LicenseNumber = personDetails.ApplicatorLicenseNumber,
                                        Address = address,
                                        PhoneNumber = string.IsNullOrEmpty(personDetails.PrimaryPhoneNumber) ? string.Empty : personDetails.PrimaryPhoneNumber,
                                    };

                                    data.Applicators.Add(app);
                                    data.Applicators = data.Applicators.Distinct(x => x.Applicator).ToList();
                                }

                                foreach (var treatment in czAppRecord.AssociatedProducts)
                                {
                                    var seedTreatmentItem = SeedTreatmentProductItem(treatment, czAppRecord, appDetails.Id, masterlist);

                                    seedTreatmentItem.ItemCount = cnt;
                                    data.Products.Add(seedTreatmentItem);
                                    cnt++;
                                }
                            }
                        }
                    }

                    if (data.Products.Count() > 0)
                    {
                        data.Products = data.Products.OrderBy(p => p.StartDate).ToList();
                        var report = new Landdb.Views.Secondary.Reports.Specialty.WSDA();
                        report.DataSource = data;
                        report.Name = data.DataSourceName;

                        book.Reports.Add(report);
                    }
                }
            }

            rs.ReportDocument = book;
            return rs;
        }

        private ProductLineItem SeedTreatmentProductItem(IncludedAssociatedProduct seedTreatment, CropZoneApplicationDataItem czAppItem,  ApplicationId appId ,IMasterlistService mlService)
        {
            var item = new ProductLineItem();
            ApplicationView appDetails = endpoint.GetView<ApplicationView>(appId).Value;
            var masterlistProd = mlService.GetProduct(seedTreatment.ProductId);
            if (masterlistProd.ProductType == GlobalStrings.ProductType_CropProtection)
            {
                item.StartDate = appDetails.StartDateTime.ToLocalTime();
                item.EndDate = czAppItem.EndDate.ToLocalTime();
                item.Area = string.Format("{0} {1}", (czAppItem.AreaValue * czAppItem.CoveragePercent).ToString("N2"), czAppItem.AreaUnit);
                item.AppMethod = appDetails.Products.SingleOrDefault(x => x.TrackingId == czAppItem.ProductTrackingId).ApplicationMethod;

                var registrationNumber = masterlistProd.RegistrationNumber;

                if (registrationNumber == null) { item.EpaNumber = string.Empty; }
                else
                {
                    Regex epaNumberRegex = new Regex("([1-9][0-9]*)");

                    var matches = from m in epaNumberRegex.Matches(registrationNumber).Cast<Match>()
                                  select m.Value;
                    item.EpaNumber = string.Join("-", matches.ToArray());
                }


                item.TotalValue = (double)seedTreatment.TotalProductValue;
                item.TotalUnit = UnitFactory.GetUnitByName(seedTreatment.TotalProductUnit);
                item.RateValue = (double)seedTreatment.RatePerAreaValue;
                item.RateUnit = UnitFactory.GetUnitByName(seedTreatment.RatePerAreaUnit);
                item.CarrierPerArea = appDetails.TankInformation != null ? appDetails.TankInformation.CarrierPerAreaValue : 0m;
                item.ProductName = string.Format("{0} {1}", masterlistProd.Name, masterlistProd.Manufacturer);
                var aiArray = from i in masterlistProd.ActiveIngredients
                              select mlService.GetActiveIngredientDisplay(new ActiveIngredientId(i.Id));
                item.Active = string.Join(", ", aiArray);

                List<string> applicatorStrings = new List<string>();

                item.Applicator = string.Join(", ", applicatorStrings);

                var conditions = appDetails.Conditions;
                if (conditions != null)
                {
                    item.Temp = conditions.TemperatureValue;
                    item.Humidity = conditions.HumidityPercent;
                    item.WindSpeed = conditions.WindSpeedValue;
                    item.WindDirection = string.IsNullOrEmpty(conditions.WindDirection) ? string.Empty : conditions.WindDirection;
                    item.SkyCondition = string.IsNullOrEmpty(conditions.SkyCondition) ? string.Empty : conditions.SkyCondition;
                    item.SoilCondition = string.IsNullOrEmpty(conditions.SoilMoisture) || conditions.SoilMoisture == "-1" ? string.Empty : conditions.SoilMoisture;
                }
            }
            return item;
        }

        public string DisplayName
        {
            get { return Strings.GeneratorName_Wsda_Text; }
        }

        public string ReportType
        {
            get { return "WSDA"; }
        }
    }
}
