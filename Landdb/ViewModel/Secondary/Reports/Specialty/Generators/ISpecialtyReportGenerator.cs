﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.Specialty.Generators
{
    public interface ISpecialtyReportGenerator
    {
        ReportSource Generate(SpecialtyQueryCriteria criteria);
        string DisplayName { get; }
        string ReportType { get; }
    }
}
