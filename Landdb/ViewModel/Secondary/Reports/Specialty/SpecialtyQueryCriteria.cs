﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Secondary.Reports.Specialty
{
    public class SpecialtyQueryCriteria
    {
        public SpecialtyQueryCriteria()
        {
            SelectedCropZones = new List<CropZoneId>();
        }

        public List<CropZoneId> SelectedCropZones { get; set; }
        public DateTime QueryStartDate { get; set; }
        public DateTime QueryEndDate { get; set; }
        public List<Guid> SelectedProducts { get; set; }
        public CompanyId IncludedCompany { get; set; }
        public DateTime SigningDate { get; set; }
        public string SignedBy { get; set; }
        public bool SevenDay { get; set; }
        public int ScaleBy { get; set; }
        public Guid ApplicationCompany { get; set; }
        public Guid Applicator { get; set; }
        public Guid Vendor { get; set; }
        public string Notes { get; set; }

    }
}
