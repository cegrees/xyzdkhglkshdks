﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Converters.Reports;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Reports;
using Landdb.ViewModel.Secondary.Reports.AppliedProduct;
using Landdb.ViewModel.Secondary.Reports.Specialty.Generators;
using Microsoft.Win32;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Resources;
using Telerik.Reporting;
using Telerik.Reporting.Processing;

namespace Landdb.ViewModel.Secondary.Reports.Specialty
{
    public class SpecialtyReportCreatorViewModel : ViewModelBase
    {
        IClientEndpoint endpoint;
        int cropYear;
        SpecialtyQueryCriteria criteria = new SpecialtyQueryCriteria();
        ReportTemplateViewModel template;
        ReportTemplateViewModelToFile templateService = new ReportTemplateViewModelToFile();
        string reportType;
        Dictionary<string, int> months = new Dictionary<string, int>();
        public SpecialtyReportCreatorViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, int cropYear, ReportTemplateViewModel template, string reportType)
        {
            this.endpoint = clientEndpoint;
            this.cropYear = cropYear;

            
            BuildMonthDictionary();

            FieldsPageModel = new AppliedProductReportFieldSelectionViewModel(clientEndpoint, dispatcher, cropYear);
            InfoPageModel = new SpecialtyReportSelectionViewModel(clientEndpoint, dispatcher, cropYear);

            CancelCommand = new RelayCommand(OnCancelReport);
            SaveCommand = new RelayCommand(SaveTemplate);
            PDFExport = new RelayCommand(this.PDF);
            SelectedPageIndex = 0;
        }

        public ICommand CancelCommand { get; private set; }
        public ICommand SaveCommand { get; set; }
        public RelayCommand PDFExport { get; set; }

        public int CropYear { get { return ApplicationEnvironment.CurrentCropYear; } }
        public int SelectedPageIndex { get; set; }
        public ReportSource ReportSource { get; set; }
        public AppliedProductReportFieldSelectionViewModel FieldsPageModel { get; private set; }
        public SpecialtyReportSelectionViewModel InfoPageModel { get; set; }
        public string TemplateName { get; set; }


        public void UpdateReport()
        {
            if (InfoPageModel.SelectedReportGenerator != null)
            {
                SpecialtyQueryCriteria criteria = BuildCriteria();
                ReportSource = InfoPageModel.SelectedReportGenerator.Generate(criteria);
                RaisePropertyChanged("ReportSource");
            }
        }

        void OnCancelReport()
        {
            Messenger.Default.Send<ShowMainViewMessage>(new ShowMainViewMessage());
        }

        

        SpecialtyQueryCriteria BuildCriteria()
        {
            criteria = new SpecialtyQueryCriteria();

            var czq = from cz in FieldsPageModel.SelectedCropZones
                      select cz.Id;
            criteria.SelectedCropZones.AddRange(czq);

            switch (InfoPageModel.SelectedReportGenerator.ReportType)
            {
                case "PesticideUse":
                    
                    var puVM = InfoPageModel.DetailsModel as PesticideUseViewModel;
                    puVM.ChangeDataSourceAddress();
                    var month = months[puVM.SelectedMonth];
                    if (puVM.ShowCalendar)
                    {
                        criteria.QueryStartDate = puVM.StartDate;
                        criteria.QueryEndDate = puVM.EndDate.AddDays(1);
                    }
                    else
                    {
                        DateTime start = new DateTime(puVM.SelectedCropYear, month, 01);
                        criteria.QueryStartDate = start;
                        criteria.QueryEndDate = start.AddMonths(1);
                    }

                    criteria.SignedBy = string.IsNullOrEmpty(puVM.SignedBy) ? string.Empty : puVM.SignedBy;
                    criteria.SigningDate = puVM.SigningDate;
                    criteria.IncludedCompany = puVM.SelectedCompany ==  null ? new CompanyId(ApplicationEnvironment.CurrentDataSourceId, Guid.Empty) : puVM.SelectedCompany.Id;
                    criteria.SevenDay = puVM.ShowCalendar;
                    break;
                case "WSDA":

                    var wVM = InfoPageModel.DetailsModel as WSDAViewModel;
                    wVM.ChangeDataSourceAddress();
                    criteria.QueryStartDate = wVM.ReportStartDate;
                    criteria.QueryEndDate = wVM.ReportEndDate.AddDays(1);
                    break;
                case "Form 1080":
                    var theVM = InfoPageModel.DetailsModel as WSDAViewModel;
                    theVM.ChangeDataSourceAddress();
                    criteria.QueryStartDate = theVM.ReportStartDate;
                    criteria.QueryEndDate = theVM.ReportEndDate.AddDays(1);
                    break;
                case "AerialApplicator":
                    var aerialVM = InfoPageModel.DetailsModel as AerialInfoViewModel;
                    criteria.ApplicationCompany = aerialVM.SelectedCompany != null ? aerialVM.SelectedCompany.Id.Id : new Guid();
                    criteria.Applicator = aerialVM.SelectedApplicator != null ? aerialVM.SelectedApplicator.Id.Id : new Guid();
                    criteria.Vendor = aerialVM.SelectedVendor != null ? aerialVM.SelectedVendor.Id.Id : new Guid();
                    criteria.Notes = aerialVM.Notes;

                    switch (aerialVM.CurrentOption.ToString())
                    {
                        case "NONE":
                            criteria.ScaleBy = 20;
                            break;
                        case "Quarter":
                            criteria.ScaleBy = 100;
                            break;
                        case "Half":
                            criteria.ScaleBy = 250;
                            break;
                        case "Whole":
                            criteria.ScaleBy = 400;
                            break;
                        default:
                            criteria.ScaleBy = 20;
                            break;
                    }

                    break;
                default:

                    break;
            }

            return criteria;
        }

        void SaveTemplate()
        {
            criteria = BuildCriteria();
            if (template != null && template.TemplateName == TemplateName)
            {
                ReportTemplateViewModel editedTemplate = new ReportTemplateViewModel()
                {
                    TemplateName = TemplateName,
                    Created = template.Created,
                    LastUsed = DateTime.Now,
                    CriteriaJSON = JsonConvert.SerializeObject(criteria),
                    ReportType = template.ReportType,
                    DataSourceId = template.DataSourceId
                };

                //TODO :: CALL TO EDIT PREVIOUS TEMPLATE...
                templateService.EditTemplate(editedTemplate);
            }
            else
            {
                ReportTemplateViewModel newTemplate = new ReportTemplateViewModel()
                {
                    TemplateName = TemplateName,
                    Created = DateTime.Now,
                    LastUsed = DateTime.Now,
                    CriteriaJSON = JsonConvert.SerializeObject(criteria),
                    ReportType = reportType,
                    DataSourceId = ApplicationEnvironment.CurrentDataSourceId
                };

                //TODO :: call service to save the new report template
                templateService.SaveNewTemplate(newTemplate);
                template = newTemplate;
            }

            RaisePropertyChanged("NewTemplateCreated", null, new ReportTemplateViewModel(), true);
        }

        public void PDF()
        {
            try { 
            if (ReportSource == null) return;
            ReportSource reportExport = ReportSource;

            ReportProcessor reportProcessor = new ReportProcessor();
            System.Collections.Hashtable deviceInfo =
            new System.Collections.Hashtable();
            RenderingResult result = reportProcessor.RenderReport("PDF", reportExport, deviceInfo);

            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "Adobe Reader|*.pdf";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;

            Nullable<bool> resultSaved = saveFileDialog1.ShowDialog();

            // Process save file dialog box results 
            if (resultSaved == true)
            {
                // Save document 
                string filename = saveFileDialog1.FileName;

                try
                {
                    using (System.IO.FileStream fs = new System.IO.FileStream(filename, System.IO.FileMode.Create))
                    {
                        fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
                    }

                    System.Diagnostics.Process.Start(filename);
                }
                catch (Exception)
                {

                }

            }
            }
            catch (Exception)
            {
                //do nothing
            }
        }

        public void BuildMonthDictionary()
        {
            months.Add(Strings.Month_January_Text, 1);
            months.Add(Strings.Month_February_Text, 2);
            months.Add(Strings.Month_March_Text, 3);
            months.Add(Strings.Month_April_Text, 4);
            months.Add(Strings.Month_May_Text, 5);
            months.Add(Strings.Month_June_Text, 6);
            months.Add(Strings.Month_July_Text, 7);
            months.Add(Strings.Month_August_Text, 8);
            months.Add(Strings.Month_September_Text,9);
            months.Add(Strings.Month_October_Text, 10);
            months.Add(Strings.Month_November_Text,11);
            months.Add(Strings.Month_December_Text, 12);
        }
    }
}
