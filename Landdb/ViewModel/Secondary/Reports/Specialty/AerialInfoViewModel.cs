﻿using GalaSoft.MvvmLight;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Company;
using Landdb.Domain.ReadModels.Person;
using Landdb.Infrastructure;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Secondary.Reports.Specialty
{
    public class AerialInfoViewModel : ViewModelBase
    {
        IClientEndpoint endPoint;
        int cropYear;
        ScaleOptions option;
        
        public AerialInfoViewModel(IClientEndpoint endPoint, int cropYear)
        {
            this.endPoint = endPoint;
            this.cropYear = cropYear;
            var dsId = (new DataSourceId(ApplicationEnvironment.CurrentDataSourceId));
            var companies = endPoint.GetView<CompanyListView>(dsId);
            if (companies.HasValue)
            {
                var items = companies.Value;
                Companies = new ObservableCollection<CompanyListItem>(items.Companies);                
            }

            var people = endPoint.GetView<PersonListView>(dsId);
            if (people.HasValue)
            {
                Applicators = new ObservableCollection<PersonListItem>(people.Value.Persons);
            }
        }

        public ObservableCollection<CompanyListItem> Companies { get; set; }
        public CompanyListItem SelectedCompany { get; set; }
        public CompanyListItem SelectedVendor { get; set; }

        public ObservableCollection<PersonListItem> Applicators { get; set; }
        public PersonListItem SelectedApplicator { get; set; }

        public string Notes { get; set; }

        public ScaleOptions CurrentOption { 
            get { 
                return option;
            } 
            set { 
                option = value;
                RaisePropertyChanged("CurrentOption");
            } 
        }
    }

    public enum ScaleOptions
    {
        NONE,
        Quarter,
        Half,
        Whole
    }
}
