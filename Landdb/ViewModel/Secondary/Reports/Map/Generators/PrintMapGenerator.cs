﻿using Landdb.Client.Infrastructure;
using Landdb.Infrastructure;
using Landdb.ReportModels.CropZone;
using System.Drawing;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Resources;
using Landdb.Views.Secondary.Reports.Map;
using Lokad.Cqrs;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.Map.Generators
{
    public class PrintMapGenerator
    {
        private readonly IClientEndpoint clientEndpoint;
        private readonly Bitmap mapImage;
        private readonly IIdentity fieldId;
        private readonly string subHeader;

        public PrintMapGenerator(IClientEndpoint clientEndpoint, Bitmap mapImage, IIdentity fieldId, string subHeader)
        {
            this.clientEndpoint = clientEndpoint;
            this.mapImage = mapImage;
            this.fieldId = fieldId;
            this.subHeader = subHeader;
        }

        public ReportSource Generate()
        {
            return Generate(GetCoordinates(clientEndpoint, fieldId), mapImage, subHeader);
        }

        private static ReportSource Generate(string centerCoordinates, Bitmap mapImage, string subHeader)
        {
            InstanceReportSource reportSource = new InstanceReportSource();
            ReportBook reportBook = new ReportBook();
            MapData data = new MapData
            {
                DataSourceName = ApplicationEnvironment.CurrentDataSourceName,
                CropYear = ApplicationEnvironment.CurrentCropYear.ToString(),
                MapImage = mapImage,
                SubHeader = subHeader,
                DisplayCoordinates = centerCoordinates,
                Title = ApplicationEnvironment.CurrentDataSourceName
            };
            MapReport report = new MapReport{DataSource = data, Name = Strings.ReportName_Application_Text};
            reportBook.Reports.Add(report);
            reportSource.ReportDocument = reportBook;
            return reportSource;
        }

        private static string GetCoordinates(IClientEndpoint clientEndpoint, IIdentity identity)
        {
            string coordinates = string.Empty;

            if (identity != null)
            {
                coordinates = GetFieldCoordinates(clientEndpoint, identity);

                if (string.IsNullOrWhiteSpace(coordinates))
                {
                    coordinates = GetCropZoneCoordinates(clientEndpoint, identity);
                }
            }
            return coordinates;
        }

        private static string GetFieldCoordinates(IClientEndpoint clientEndpoint, IIdentity identity)
        {
            string fieldCoordinates = string.Empty;

            Maybe<FieldDetailsView> possibleFieldDetails = clientEndpoint.GetView<FieldDetailsView>(identity);

            if (possibleFieldDetails.HasValue)
            {
                fieldCoordinates = possibleFieldDetails.Value.CenterLatLong;
            }

            return fieldCoordinates;
        }

        private static string GetCropZoneCoordinates(IClientEndpoint clientEndpoint, IIdentity identity)
        {
            string cropZoneCoordinates = string.Empty;

            Maybe<CropZoneDetailsView> possibleCropZone = clientEndpoint.GetView<CropZoneDetailsView>(identity);

            if (possibleCropZone.HasValue)
            {
                cropZoneCoordinates = possibleCropZone.Value.CenterLatLong;
            }

            return cropZoneCoordinates;
        }

        public string DisplayName => Strings.GeneratorName_Map_Text;
    }
}