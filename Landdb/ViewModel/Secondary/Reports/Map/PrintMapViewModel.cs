﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Secondary.Reports.Map.Generators;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Threading;
using Telerik.Reporting;
using Telerik.Reporting.Processing;

namespace Landdb.ViewModel.Secondary.Reports.Map
{
    public class PrintMapViewModel : ViewModelBase
    {
        IClientEndpoint endpoint;
        Bitmap mapImage;
        string subHeader;
        private readonly IIdentity fieldId;

        public PrintMapViewModel(IClientEndpoint endPoint, Dispatcher dispatcher, Bitmap mapImage, List<Guid> czIds, string subHeaderLabel, IIdentity fieldId)
        {
            this.endpoint = endPoint;
            this.mapImage = mapImage;
            this.subHeader = subHeaderLabel;
            this.fieldId = fieldId;
            var datasourceId = ApplicationEnvironment.CurrentDataSourceId;

            var list = (from c in czIds
                        where c != null
                        select new CropZoneId(datasourceId, c)).ToList();
            IncludedCropZones = new List<CropZoneId>();
            IncludedCropZones.AddRange(list);

            UpdateReport();

            HideReport = new RelayCommand(this.CloseReport);
            PDFExport = new RelayCommand(this.PDF);
        }

        public RelayCommand HideReport { get; }
        public RelayCommand PDFExport { get; set; }
        public List<CropZoneId> IncludedCropZones { get; set; }

        public void CloseReport()
        {
            Messenger.Default.Send(new HideOverlayMessage() { });
        }

        public ReportSource ReportSource { get; set; }
        //public IApplicationReportGenerator ReportGenerator { get; set; }

        public void UpdateReport()
        {
            var generator = new PrintMapGenerator(endpoint, mapImage, fieldId, subHeader);

            ReportSource = generator.Generate();
        }

        public void PDF()
        {
            try { 
            if (ReportSource == null) return;
            ReportSource reportExport = ReportSource;

            ReportProcessor reportProcessor = new ReportProcessor();
            System.Collections.Hashtable deviceInfo =
            new System.Collections.Hashtable();
            RenderingResult result = reportProcessor.RenderReport("PDF", reportExport, deviceInfo);

            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "Adobe Reader|*.pdf";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;

            Nullable<bool> resultSaved = saveFileDialog1.ShowDialog();

            // Process save file dialog box results 
            if (resultSaved == true)
            {
                // Save document 
                string filename = saveFileDialog1.FileName;

                try
                {
                    using (System.IO.FileStream fs = new System.IO.FileStream(filename, System.IO.FileMode.Create))
                    {
                        fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
                    }

                    System.Diagnostics.Process.Start(filename);
                }
                catch (Exception ex) { }

            }
            }
            catch (Exception ex)
            {
                //do nothing
            }
        }
    }
}