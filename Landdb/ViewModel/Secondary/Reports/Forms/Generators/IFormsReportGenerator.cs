﻿using System;
using System.Windows.Documents;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.Forms.Generators
{
    public interface IFormsReportGenerator
    {
        ReportSource Generate(FormsQueryCriteria criteria);
        string DisplayName { get; }
    }
}
