﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AgC.UnitConversion;
using AgC.UnitConversion.Area;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Domain.ReadModels.Invoice;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.Infrastructure.FlowDocuments;
using Landdb.ReportModels.AppliedProductByFieldDetail;
using System.Windows;
using System.Windows.Documents;
using Landdb.Resources;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.Forms.Generators
{
    public class ApplicationFormGenerator : IFormsReportGenerator
    {
        IClientEndpoint clientEndpoint;
        int cropYear;

        public ApplicationFormGenerator(IClientEndpoint clientEndpoint, int cropYear)
        {
            this.clientEndpoint = clientEndpoint;
            this.cropYear = cropYear;
        }

        public string DisplayName
        {
            get { return Strings.ApplicationForm_Text; }
        }

        public ReportSource Generate(FormsQueryCriteria criteria)
        {
            ReportBook book = new ReportBook();
            var rs = new InstanceReportSource();
            if (criteria.SelectedCropZones.Any() && criteria.SelectedProducts.Any())//If they want to see both crop zones and products
            {
                var applicationData = clientEndpoint.GetView<CropZoneApplicationDataView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new CropZoneApplicationDataView());
                var inventory = clientEndpoint.GetView<InventoryListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new InventoryListView());
                var flattenedHierarchy = clientEndpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new FlattenedTreeHierarchyView());
                var masterlist = clientEndpoint.GetMasterlistService();

                var czDetails = (from i in criteria.SelectedCropZones
                                 select clientEndpoint.GetView<CropZoneDetailsView>(i).Value).ToList();

                double totalArea = 0.0;

                foreach (var det in czDetails)
                {
                    var area = det.BoundaryArea != null ? (double)det.BoundaryArea : 0.0;
                    totalArea += det.ReportedArea != null ? (double)det.ReportedArea : area;
                }

                var czapplicationList = (from a in applicationData.Items
                                         where criteria.SelectedCropZones.Contains(a.CropZoneId) &&
                                         a.EndDate >= criteria.QueryStartDate && a.EndDate <= criteria.QueryEndDate
                                         select a).ToList();


                var data = new ReportData();

                foreach (var cz in czDetails)
                {
                    var czItem = new IncludedCropZoneItemData();
                    var flattened = (from f in flattenedHierarchy.Items
                                     where f.CropZoneId == cz.Id
                                     select f).FirstOrDefault();

                    czItem.FarmName = flattened.FarmName;
                    czItem.FieldName = flattened.FieldName;
                    czItem.CropZoneName = flattened.CropZoneName;
                    var area = cz.BoundaryArea != null ? (double)cz.BoundaryArea : 0.0;
                    czItem.Area = cz.ReportedArea != null ? (double)cz.ReportedArea : area;
                    czItem.DisplayArea = czItem.Area.ToString("N2") + " " + czDetails[0].ReportedAreaUnit;
                    data.CropZones.Add(czItem);
                }

                data.Area = totalArea;

                var areaUnit = string.IsNullOrEmpty(czDetails[0].BoundaryAreaUnit) ? czDetails[0].ReportedAreaUnit : czDetails[0].BoundaryAreaUnit;
                data.AreaDisplay = string.Format("{0} {1}", data.Area.ToString("N2"), areaUnit);
                data.DateRangeDisplay = string.Format("{0} - {1}", criteria.QueryStartDate.ToString("d"), criteria.QueryEndDate.ToString("d"));
                data.CropYear = cropYear.ToString();
                data.DataSourceName = !string.IsNullOrWhiteSpace(criteria.SelectedEntity.Name) ? criteria.SelectedEntity.Name : ApplicationEnvironment.CurrentDataSourceName;

                var productIdList = criteria.SelectedProducts;
                productIdList.ToList();

                foreach (var id in productIdList)
                {
                    //var masterListProd = masterlist.GetProduct(new ProductId(ApplicationEnvironment.CurrentDataSourceId, id));
                    var masterlistProdQuery = clientEndpoint.GetMasterlistService().GetProduct(id);
                    var masterlistProd = masterlistProdQuery == null ? masterlist.GetProduct(id) : masterlistProdQuery;

                    var prodLineItem = new ReportLineItemData();
                    prodLineItem.ProductDisplay = masterlistProd.Name;
                    if (masterlistProd.RegistrationNumber != null) { prodLineItem.EPANumber = masterlistProd.RegistrationNumber; }
                    else { prodLineItem.EPANumber = ""; }
                    prodLineItem.ProductType = masterlistProd.ProductType;
                    data.LineItems.Add(prodLineItem);
                }

                if (data.LineItems.Count() > 0)
                {
                    data.LineItems = data.LineItems.OrderBy(x => x.ProductDisplay).ToList();
                    var report = new Landdb.Views.Secondary.Reports.Forms.ApplicationFormReport();
                    report.DataSource = data;
                    report.Name = data.DataSourceName;

                    book.Reports.Add(report);
                }


            }
            else if (!criteria.SelectedCropZones.Any() && !criteria.SelectedProducts.Any())
            {//if they want a blank template
                var data = new ReportData();
                var czItem = new IncludedCropZoneItemData();
                data.AddressDisplay = "";
                data.AreaDisplay = "";
                data.CostType = "";
                data.CropDisplay = "";
                data.CropYear = "";
                data.DataSourceName = "";

                czItem.DisplayArea = "";
                czItem.FarmName = "";
                czItem.FieldName = "";
                czItem.CropZoneName = "";
                data.CropZones.Add(czItem);

                var prodLineItem = new ReportLineItemData();
                prodLineItem.ProductDisplay = "";
                prodLineItem.EPANumber = "";
                prodLineItem.ProductType = "";
                data.LineItems.Add(prodLineItem);

                var report = new Landdb.Views.Secondary.Reports.Forms.ApplicationFormReport();
                report.DataSource = data;
                report.Name = data.DataSourceName;

                book.Reports.Add(report);
            }
            else if (criteria.SelectedCropZones.Any() && !criteria.SelectedProducts.Any())//If they just want to see crop information
            {
                var applicationData = clientEndpoint.GetView<CropZoneApplicationDataView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new CropZoneApplicationDataView());
                var inventory = clientEndpoint.GetView<InventoryListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new InventoryListView());
                var flattenedHierarchy = clientEndpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new FlattenedTreeHierarchyView());
                var masterlist = clientEndpoint.GetMasterlistService();

                var czDetails = (from i in criteria.SelectedCropZones
                                 select clientEndpoint.GetView<CropZoneDetailsView>(i).Value).ToList();
                double totalArea = 0.0;

                foreach (var det in czDetails)
                {
                    var area = det.BoundaryArea != null ? (double)det.BoundaryArea : 0.0;
                    totalArea += det.ReportedArea != null ? (double)det.ReportedArea : area;
                }

                var data = new ReportData();

                foreach (var cz in czDetails)
                {
                    var czItem = new IncludedCropZoneItemData();
                    var flattened = (from f in flattenedHierarchy.Items
                                     where f.CropZoneId == cz.Id
                                     select f).FirstOrDefault();
                    czItem.FarmName = flattened.FarmName;
                    czItem.FieldName = flattened.FieldName;
                    czItem.CropZoneName = flattened.CropZoneName;
                    var area = cz.BoundaryArea != null ? (double)cz.BoundaryArea : 0.0;
                    czItem.Area = cz.ReportedArea != null ? (double)cz.ReportedArea : area;
                    czItem.DisplayArea = czItem.Area.ToString("N2") + " " + czDetails[0].ReportedAreaUnit;
                    data.CropZones.Add(czItem);
                }

                var areaUnit = string.IsNullOrEmpty(czDetails[0].BoundaryAreaUnit) ? czDetails[0].ReportedAreaUnit : czDetails[0].BoundaryAreaUnit;
                data.AreaDisplay = string.Format("{0} {1}", data.Area.ToString("N2"), areaUnit);
                data.DateRangeDisplay = string.Format("{0} - {1}", criteria.QueryStartDate.ToString("d"), criteria.QueryEndDate.ToString("d"));
                data.CropYear = cropYear.ToString();
                data.DataSourceName = !string.IsNullOrWhiteSpace(criteria.SelectedEntity.Name) ? criteria.SelectedEntity.Name : ApplicationEnvironment.CurrentDataSourceName;

                var report = new Landdb.Views.Secondary.Reports.Forms.ApplicationFormReport();
                report.DataSource = data;
                report.Name = data.DataSourceName;

                book.Reports.Add(report);


            }
            else if (!criteria.SelectedCropZones.Any() && criteria.SelectedProducts.Any())//If they just want to see product information
            {
                var masterlist = clientEndpoint.GetMasterlistService();

                var cr = criteria;
                var data = new ReportData();
                var czItem = new IncludedCropZoneItemData();
                data.AddressDisplay = "";
                data.AreaDisplay = "";
                data.CostType = "";
                data.CropDisplay = "";
                data.CropYear = "";
                data.DataSourceName = "";

                czItem.DisplayArea = "";
                czItem.FarmName = "";
                czItem.FieldName = "";
                czItem.CropZoneName = "";
                data.CropZones.Add(czItem);

                var productIdList = criteria.SelectedProducts;
                productIdList.ToList();

                foreach (var id in productIdList)
                {
                    //var masterListProd = masterlist.GetProduct(new ProductId(ApplicationEnvironment.CurrentDataSourceId, id));
                    var masterlistProdQuery = clientEndpoint.GetMasterlistService().GetProduct(id);
                    var masterlistProd = masterlistProdQuery == null ? masterlist.GetProduct(id) : masterlistProdQuery;

                    var prodLineItem = new ReportLineItemData();
                    prodLineItem.ProductDisplay = masterlistProd.Name;
                    if (masterlistProd.RegistrationNumber != null) { prodLineItem.EPANumber = masterlistProd.RegistrationNumber; }
                    else { prodLineItem.EPANumber = ""; }
                    prodLineItem.ProductType = masterlistProd.ProductType;
                    data.LineItems.Add(prodLineItem);
                }




                var report = new Landdb.Views.Secondary.Reports.Forms.ApplicationFormReport();
                report.DataSource = data;
                report.Name = data.DataSourceName;

                book.Reports.Add(report);


            }

            rs.ReportDocument = book;
            return rs;
        }



    }



}

