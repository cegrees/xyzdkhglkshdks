﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Plan;
using Landdb.Domain.ReadModels.WorkOrder;
using Landdb.Infrastructure;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Landdb.ViewModel.Secondary.Reports.Document
{
    public class DocumentReportSelectionViewModel : ViewModelBase
    {
        IClientEndpoint endpoint;
        int cropYear;
        DocumentItemViewModel selected;
        ConcurrentQueue<DocumentItemViewModel> queue;
        bool wos = true;
        bool apps = true;
        bool plans = true;
        
        public DocumentReportSelectionViewModel(IClientEndpoint endpoint, int cropyear)
        {
            this.endpoint = endpoint;
            this.cropYear = cropyear;
            queue = new ConcurrentQueue<DocumentItemViewModel>();
            ConcurrentDocuments = new List<DocumentItemViewModel>();
            StartDate = new DateTime(cropyear, 01, 01);
            EndDate = new DateTime(cropyear, 12, 31);
            GetDocuments();
            Messenger.Default.Register<PropertyChangedMessage<DateTime>>(this, OnStartDateChanged);
            Messenger.Default.Register<PropertyChangedMessage<bool>>(this, OnIncludeChanged);

            SelectAll = new RelayCommand(selectAll);
            UnselectAll = new RelayCommand(unselectAll);
        }

        public ICommand SelectAll { get; set; }
        public ICommand UnselectAll { get; set; }

        public List<DocumentItemViewModel> ConcurrentDocuments
        {
            get;
            set;
        }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool IncludeWOs { get { return wos; } set { wos = value; } }
        public bool IncludeApps { get { return apps; } set { apps = value; } }
        public bool IncludePlans { get { return plans; } set { plans = value; } }
        public bool SpanishTranslated { get; set; }
        public DocumentItemViewModel SelectedDocument
        {
            get { return selected; }
            set
            {
                selected = value;

                if (selected != null)
                {
                    if (selected.Checked)
                    {
                        selected.Checked = false;
                    }
                    else
                    {
                        selected.Checked = true;
                    }
                }
            }
        }

        public void selectAll()
        {
            if (ConcurrentDocuments.Any())
            {
                ConcurrentDocuments.ForEach(x => { x.Checked = true; });
            }
        }

        public void unselectAll()
        {
            if (ConcurrentDocuments.Any())
            {
                ConcurrentDocuments.ForEach(x => { x.Checked = false; });
            }
        }

        public void OnStartDateChanged(PropertyChangedMessage<DateTime> dateChanged)
        {
            if (dateChanged.PropertyName == "ReportStartDate")
            {
                //get all 
                StartDate = dateChanged.NewValue;
                GetDocuments();
            }

            if (dateChanged.PropertyName == "ReportEndDate")
            {
                //get all
                EndDate = dateChanged.NewValue;
                GetDocuments();
            }
        }

        public void OnIncludeChanged(PropertyChangedMessage<bool> includeChanged)
        {
            if (includeChanged.PropertyName == "IncludeWorkOrders")
            {
                IncludeWOs = includeChanged.NewValue;
                GetDocuments();
            }
            if (includeChanged.PropertyName == "IncludeApplications")
            {
                IncludeApps = includeChanged.NewValue;
                GetDocuments();
            }
            if (includeChanged.PropertyName == "IncludePlans")
            {
                IncludePlans = includeChanged.NewValue;
                GetDocuments();
            }
        }

        void GetDocuments()
        {
            ConcurrentDocuments = new List<DocumentItemViewModel>();
            List<DocumentItemViewModel> appList = new List<DocumentItemViewModel>();

            Task applicationListTask = Task.Factory.StartNew(() =>
            {
                if (IncludeApps)
                {
                    var applicaitonsMaybe = endpoint.GetView<ApplicationList>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear));

                    if (applicaitonsMaybe.HasValue)
                    {
                        appList = (from a in applicaitonsMaybe.Value.Applications
                                   where a.StartDateTime >= StartDate && a.StartDateTime <= EndDate
                                   select new DocumentItemViewModel() { FieldCount = a.FieldCount, ProductCount = a.ProductCount, Name = a.Name, StartDateTime = (DateTime)a.StartDateTime, TotalCost = a.TotalCost, Descriptor = new DocumentDescriptor() { DocumentType = DocumentDescriptor.ApplicationTypeString, Identity = a.Id as AbstractDataSourceIdentity<Guid, Guid>, Name = a.Name }, Checked = true }).ToList();
                    }
                }
            });
            

            List<DocumentItemViewModel> workOrderList = new List<DocumentItemViewModel>();

            Task workorderListTask = Task.Factory.StartNew(() =>
            {
                if (IncludeWOs)
                {
                    var workOrdersMaybe = endpoint.GetView<WorkOrderListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear));

                    if (workOrdersMaybe.HasValue)
                    {
                        workOrderList = (from a in workOrdersMaybe.Value.WorkOrders
                                         where a.StartDateTime >= StartDate && a.StartDateTime <= EndDate
                                         select new DocumentItemViewModel() { FieldCount = a.FieldCount, ProductCount = a.ProductCount, Name = a.Name, StartDateTime = (DateTime)a.StartDateTime, TotalCost = a.TotalCost, Descriptor = new DocumentDescriptor() { DocumentType = DocumentDescriptor.WorkOrderTypeString, Identity = a.Id as AbstractDataSourceIdentity<Guid, Guid>, Name = a.Name }, Checked = true }).ToList();
                    }
                }
            });

            List<DocumentItemViewModel> planList = new List<DocumentItemViewModel>();

            Task planListTask = Task.Factory.StartNew(() =>
            {
                if (IncludePlans)
                {
                    var plansMaybe = endpoint.GetView<PlanList>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear));

                    if (plansMaybe.HasValue)
                    {
                        planList = (from p in plansMaybe.Value.Plans
                                    select new DocumentItemViewModel() { FieldCount = p.FieldCount, ProductCount = p.ProductCount, Name = p.Name, StartDateTime = p.CreateDate, Descriptor = new DocumentDescriptor() { DocumentType = DocumentDescriptor.PlanTypeString, Identity = p.Id as AbstractDataSourceIdentity<Guid, Guid>, Name = p.Name }, Checked = true }).ToList();
                    }
                }
            });

            applicationListTask.Wait();
            workorderListTask.Wait();
            planListTask.Wait();
            List<DocumentItemViewModel> list = new List<DocumentItemViewModel>();
            if(appList.Any())
                list.AddRange(appList);
            if(workOrderList.Any())
                list.AddRange(workOrderList);
            if (planList.Any())
                list.AddRange(planList);
            ConcurrentDocuments = list.OrderByDescending(x => x.StartDateTime).ToList();
            RaisePropertyChanged("ConcurrentDocuments");
        }
    }
}
