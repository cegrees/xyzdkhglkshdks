﻿using GalaSoft.MvvmLight;
using Landdb.Client.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Secondary.Reports.Document
{
    public class DocumentReportInfoViewModel :ViewModelBase
    {
        DateTime startDate;
        DateTime endDate;
        bool workOrders;
        bool applications;
        bool plans;
        bool spanishTranslated;

        public DocumentReportInfoViewModel(IClientEndpoint endpoint, int cropyear)
        {
            ReportStartDate = new DateTime(cropyear, 01, 01);
            ReportEndDate = new DateTime(cropyear, 12, 31);
            IncludeApplications = true;
            IncludeWorkOrders = true;
            IncludePlans = true;
        }

        public DateTime ReportStartDate { get { return startDate; } set { startDate = value; RaisePropertyChanged("ReportStartDate", new DateTime(), startDate, true); } }
        public DateTime ReportEndDate { get { return endDate; } set { endDate = value; RaisePropertyChanged("ReportEndDate", new DateTime(), endDate, true); } }
        public bool IncludeWorkOrders { get { return workOrders; } set { workOrders = value; RaisePropertyChanged("IncludeWorkOrders", false, workOrders, true ); } }
        public bool IncludeApplications { get { return applications; } set { applications = value; if (applications == true && spanishTranslated == true) { spanishTranslated = false; RaisePropertyChanged("SpanishTranslated"); } RaisePropertyChanged("IncludeApplications", false, applications, true); } }
        public bool IncludePlans { get { return plans; } set { plans = value; RaisePropertyChanged(()=>IncludePlans, false, plans, true); } }
        public bool SpanishTranslated { get { return spanishTranslated; } set { spanishTranslated = value; } }
    }
}
