﻿using AgC.UnitConversion;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Client.Services;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Map;
using Landdb.Domain.ReadModels.ProductSetting;
using Landdb.Domain.ReadModels.Shared;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Domain.ReadModels.WorkOrder;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ReportModels.Application;
using Landdb.ReportModels.WorkOrder;
using Landdb.ViewModel.Overlays;
using Landdb.ViewModel.Secondary.Map;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Landdb.Resources;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.Document.Generators
{
    public class BreakOutDocumentGenerator : IDocumentReportGenerator
    {
        IClientEndpoint endpoint;
        int cropyear;
        List<ProductWorkerProtection> ProductPPEList;
        ProgressOverlayViewModel vm;
        IProgressTracker progress;

        public BreakOutDocumentGenerator(IClientEndpoint endpoint, int cropyear)
        {
            this.endpoint = endpoint;
            this.cropyear = cropyear;

            vm = new ProgressOverlayViewModel();
            progress = vm;
        }


        public Telerik.Reporting.ReportSource Generate(DocumentQueryCriteria criteria, DocumentReportCreatorViewModel dcvm)
        {
            ScreenDescriptor progressDesc = new ScreenDescriptor("Landdb.Views.Shared.Popups.ProgressPopUpView", "fullprogress", vm);
            Messenger.Default.Send<ShowPopupMessage>(new ShowPopupMessage() { ScreenDescriptor = progressDesc });

            Action StartGeneration;
            StartGeneration = () => ProcessData(criteria, dcvm);
            Thread t;
            t = new Thread(StartGeneration.Invoke);
            t.Start();

            return null;
        }

        void ProcessData(DocumentQueryCriteria criteria, DocumentReportCreatorViewModel dcvm)
        {
            ReportBook book = new ReportBook();
            var rs = new InstanceReportSource();

            if (criteria.IncludedDocuments.Any())
            {
                UpdateProgressMessage(progress, Strings.ProgressMessage_CreatingByFieldDocs_Text);
                UpdateProgressTotalRecords(progress, criteria.IncludedDocuments.Count());
                UpdateProgressCurrentRecord(progress, 0);
                int cnt = 0;

                foreach (var doc in criteria.IncludedDocuments)
                {

                    switch (doc.DocumentType)
                    {
                        case "WorkOrder":

                            //call this for each crop zone associated with the work order
                            var woId = new WorkOrderId(doc.Identity.DataSourceId, doc.Identity.Id);
                            var woDetail = endpoint.GetView<WorkOrderView>(woId).Value;

                            var woData = ReturnWOData(woDetail);
                    
                            var brReport = new Landdb.Views.Secondary.Reports.Work_Order.BreakOutWorkOrder();
                            brReport.DataSource = woData;
                            brReport.Name = woData.DatasourceName;
                            book.Reports.Add(brReport);

                            break;
                        case "Application":
                            var appId = new ApplicationId(doc.Identity.DataSourceId, doc.Identity.Id);
                            var appDetails = endpoint.GetView<ApplicationView>(appId).Value;

                            var appData = ReturnAppData(appDetails);
                            var appreport = new Landdb.Views.Secondary.Reports.Work_Order.BreakOutWorkOrder();
                            appreport.DataSource = appData;
                            appreport.Name = appData.DatasourceName;
                            book.Reports.Add(appreport);

                            break;
                        default:
                            break;
                    }

                    //update progress
                    cnt++;
                    UpdateProgressCurrentRecord(progress, cnt);
                }
            }

            //close progress
            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());

            rs.ReportDocument = book;
            dcvm.ReportSource = rs;
        }

        void UpdateProgressMessage(IProgressTracker progress, string message)
        {
            if (progress != null)
            {
                progress.StatusMessage = message;
            }
        }

        void UpdateProgressTotalRecords(IProgressTracker progress, int totalRecords)
        {
            if (progress != null)
            {
                progress.TotalRecords = totalRecords;
            }
        }

        void UpdateProgressCurrentRecord(IProgressTracker progress, long currentRecord)
        {
            if (progress != null)
            {
                progress.CurrentRecord = (int)currentRecord;
            }
        }

        public WorkOrderBreakoutData ReturnWOData(WorkOrderView view)
        {
            var data = new WorkOrderBreakoutData();

            var flattenedHierarchy = endpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropyear)).GetValue(new FlattenedTreeHierarchyView());
            var masterlist = endpoint.GetMasterlistService();

            if (view != null)
            {
                Task[] ppeTask = new Task[1];
                ppeTask[0] = Task.Factory.StartNew(() =>
                {
                    if (NetworkStatus.IsInternetAvailable())
                    {
                        ProductPPEList = new List<ProductWorkerProtection>();
                        ProductPPEList.AddRange(GetPPE(view.Products));
                    }
                });

                List<string> filteredMapData = new List<string>();

                data.DatasourceName = ApplicationEnvironment.CurrentDataSourceName;
                data.CropYear = ApplicationEnvironment.CurrentCropYear;
                data.WorkOrderName = view.Name;
                data.WorkOrderDate = view.StartDateTime.ToLocalTime().ToShortDateString();
                data.StartDate = string.Empty;
                data.EndDate = string.Empty;
                data.WorkOrderType = view.ApplicationStrategy.ToString();
                data.TotalArea = view.CropZones.Sum(x => x.Area.Value).ToString("N2") + " " + view.CropZones.FirstOrDefault(x => x.Area.Unit != null).Area.Unit.AbbreviatedDisplay;
                data.Fields = new List<WorkOrderBreakoutData.FieldLineItemData>();
                data.Title = Strings.DataTitle_WorkOrder_Text;

                data.Authorizer = view.Authorization != null ? view.Authorization.PersonName : string.Empty;
                data.AuthorizerDate = view.Authorization != null ? view.Authorization.AuthorizationDate : DateTime.Now;

                foreach (var prod in view.Products)
                {
                    var masterlistProduct = masterlist.GetProduct(prod.Id);
                    ReiPhi reiPhi = new ReiPhi();
                    List<ReiPhi> reiPhis = new List<ReiPhi>();
                    reiPhis.AddRange(masterlist.GetProductReiPhis(prod.Id));
                    //////////////////////////////////////////////////////////
                    var cropIds = (from cz in view.CropZones
                                   select endpoint.GetView<CropZoneDetailsView>(cz.Id).Value.CropId).ToList();
                    var parentCropIds = (from cid in cropIds
                                         let mlCrop = masterlist.GetCrop(cid)
                                         where mlCrop.ParentId.HasValue
                                         select new CropId(mlCrop.ParentId.Value)).ToList();
                    cropIds.AddRange(parentCropIds);

                    var prodSettings = endpoint.GetView<ProductSettingsView>(new ProductSettingId(ApplicationEnvironment.CurrentDataSourceId, prod.Id));
                    var cropYearItem = prodSettings.HasValue && prodSettings.Value.ItemsByYear.ContainsKey(ApplicationEnvironment.CurrentCropYear) ? prodSettings.Value.ItemsByYear[ApplicationEnvironment.CurrentCropYear] : null;

                    List<ProductSettingItem> cropItems = new List<ProductSettingItem>();
                    if (cropYearItem != null)
                    {
                        cropItems = (from c in cropYearItem
                                     where cropIds.Contains(c.CropId)
                                     select c).ToList();
                    }

                    List<ReiPhi> newReiPhis = new List<ReiPhi>();
                    foreach (var reiphi in reiPhis)
                    {
                        if (cropIds.Any(c => c.Id == reiphi.Crop))
                        {
                            newReiPhis.Add(reiphi);
                        }
                    }

                    foreach (var item in cropItems)
                    {
                        var rP = (from r in newReiPhis
                                  where r.Crop == item.CropId.Id
                                  select r).SingleOrDefault();
                        newReiPhis.Remove(rP);
                        newReiPhis.Add(new ReiPhi() { Crop = item.CropId.Id, Phi = (double)item.PhiValue.Value, PhiU = item.PhiUnit, Rei = (double)item.ReiValue.Value, ReiU = item.ReiUnit });
                    }
                    /////////////////////////////////////////////////////////
                    var therei = newReiPhis != null && newReiPhis.Count() > 0 ? (double)(from r in newReiPhis select r.Rei).Max() : new double?();
                    var thephi = newReiPhis != null && newReiPhis.Count() > 0 ? (double)(from r in newReiPhis select r.Phi).Max() : new double?();
                    reiPhi.Rei = reiPhi.Rei == null || reiPhi.Rei < therei ? therei : reiPhi.Rei;
                    reiPhi.Phi = reiPhi.Phi == null || reiPhi.Phi < thephi ? thephi : reiPhi.Phi;

                    var aiList = from i in masterlistProduct.ActiveIngredients
                                 select masterlist.GetActiveIngredientDisplay(new ActiveIngredientId(i.Id));

                    string ai = string.Join(",", aiList.ToArray());

                    //string epaNoTrailingChar = string.Empty;
                    //try
                    //{
                    //    var epaNoLeadingZeros = string.IsNullOrEmpty(masterlistProduct.RegistrationNumber) ? string.Empty : masterlistProduct.RegistrationNumber.TrimStart('0');
                    //    var epaNoTrailingZeros = string.IsNullOrEmpty(epaNoLeadingZeros) ? string.Empty : epaNoLeadingZeros.TrimEnd('0');
                    //    epaNoTrailingChar = string.IsNullOrEmpty(epaNoTrailingZeros) ? string.Empty : epaNoTrailingZeros.TrimEnd('-');
                    //}
                    //catch (Exception ex) { }

                    var displayRate = DisplayMeasureConverter.ConvertToRateDisplayMeasure(prod.Id, prod.RateValue, prod.RateUnit, masterlistProduct.Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);
                    var displayTotal = DisplayMeasureConverter.ConvertToTotalDisplayMeasure(prod.Id, (decimal)prod.TotalProductValue, prod.TotalProductUnit, masterlistProduct.Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);
                    var perTankMeasure = UnitFactory.GetUnitByName(prod.RatePerTankUnit).GetMeasure((double)prod.RatePerTankValue, masterlistProduct.Density);
                    var displayPerTank = DisplayMeasureConverter.ConvertToRatePerTankDisplayMeasure(prod.Id, perTankMeasure, endpoint, ApplicationEnvironment.CurrentDataSourceId);
                    WorkOrderBreakoutData.ProductLineItemData prodData = new WorkOrderBreakoutData.ProductLineItemData()
                    {
                        ApplicationMethod = string.IsNullOrEmpty(prod.ApplicationMethod) ? string.Empty : prod.ApplicationMethod,
                        EPANumber = masterlistProduct.RegistrationNumber,
                        Pest = prod.TargetPest == null ? string.Empty : prod.TargetPest.CommonName,
                        ProductName = masterlistProduct.Name,
                        RateUnit = displayRate.Unit.Name,
                        RateValue = displayRate.Value,
                        RatePerTankUnit = data.WorkOrderType == "ByRatePerTank" ? displayPerTank.Unit.Name : prod.RateUnit,
                        RatePerTank = data.WorkOrderType == "ByRatePerTank" ? displayPerTank.Value : 0.0,
                        TotalUnit = displayTotal.Unit.Name,
                        TotalValue = displayTotal.Value,
                        PHI = reiPhi != null && reiPhi.Phi != null ? reiPhi.Phi.ToString() : "*",
                        REI = reiPhi != null && reiPhi.Rei != null ? reiPhi.Rei.ToString() : "*",
                        Active = ai,
                        RateType = data.WorkOrderType == "ByRatePerTank" ? "Rate/Tank" : "Rate",
                        RatePer100 = view.TankInformation != null ? (double)prod.RatePerTankValue / ((double)view.TankInformation.TankSizeValue / 100) : 0,
                        RatePer100Unit = displayPerTank.Unit != null ? displayPerTank.Unit.Name : string.Empty,
                    };
                    data.Products.Add(prodData);
                }

                foreach (var cz in view.CropZones)
                {
                    var includedCZ = view.CropZones.SingleOrDefault(x => x.Id == cz.Id);
                    var multiplier = includedCZ.Area.Value / view.CropZones.Sum(x => x.Area.Value);
                    
                    var maps = endpoint.GetView<ItemMap>(cz.Id);
                    try
                    {
                        if (maps.HasValue)
                        {
                            if (maps.Value.MostRecentMapItem != null && maps.Value.MostRecentMapItem.MapData != string.Empty && maps.Value.MostRecentMapItem.MapData != "MULTIPOLYGON EMPTY") {
                                filteredMapData.Add(maps.Value.MostRecentMapItem.MapData);
                            }
                        }
                    }
                    finally { }

                    var czDetails = endpoint.GetView<CropZoneDetailsView>(cz.Id).GetValue(new CropZoneDetailsView());

                    var czTreeData = (from f in flattenedHierarchy.Items
                                      where f.CropZoneId == cz.Id
                                      select f).FirstOrDefault();


                    var area = view.CropZones.SingleOrDefault(x => x.Id == cz.Id).Area;

                    var fieldDetails = endpoint.GetView<FieldDetailsView>(czDetails.FieldId).Value;
                    var centerLatLon = string.IsNullOrEmpty(czDetails.CenterLatLong) ? fieldDetails.CenterLatLong : czDetails.CenterLatLong;
                    List<WorkOrderBreakoutData.ProductLineItemData> productLineItems = new List<WorkOrderBreakoutData.ProductLineItemData>();
                    foreach (var product in view.Products)
                    {
                        var mlp = masterlist.GetProduct(product.Id);

                        var displayRate = DisplayMeasureConverter.ConvertToRateDisplayMeasure(product.Id, product.RateValue, product.RateUnit, mlp.Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);
                        var displayTotal = DisplayMeasureConverter.ConvertToTotalDisplayMeasure(product.Id, (decimal)product.TotalProductValue, product.TotalProductUnit, mlp.Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);
                        var perTankMeasure = UnitFactory.GetUnitByName(product.RatePerTankUnit).GetMeasure((double)product.RatePerTankValue, mlp.Density);
                        var displayPerTank = DisplayMeasureConverter.ConvertToRatePerTankDisplayMeasure(product.Id, perTankMeasure, endpoint, ApplicationEnvironment.CurrentDataSourceId);

                        ReiPhi reiPhi = new ReiPhi();
                        List<ReiPhi> reiPhis = new List<ReiPhi>();
                        reiPhis.AddRange(masterlist.GetProductReiPhis(product.Id));
                        //////////////////////////////////////////////////////////
                        List<ReiPhi> newReiPhis = new List<ReiPhi>();

                        var cropId = endpoint.GetView<CropZoneDetailsView>(cz.Id).Value.CropId;
                        var holder = masterlist.GetCrop(cropId).ParentId;
                        var parentCropIds = holder.HasValue ? new CropId(holder.Value) : null;

                        List<CropId> cropIds = new List<CropId>();
                        if (parentCropIds != null)
                        {
                            cropIds.Add(parentCropIds);
                        }
                        cropIds.Add(cropId);

                        foreach (var reiphi in reiPhis)
                        {
                            if (cropIds.Any(c => c.Id == reiphi.Crop))
                            {
                                newReiPhis.Add(reiphi);
                            }
                        }
                        /////////////////////////////////////////////////////////
                        var therei = newReiPhis != null && newReiPhis.Count() > 0 ? (double)(from r in newReiPhis select r.Rei).Max() : new double?();
                        var thephi = newReiPhis != null && newReiPhis.Count() > 0 ? (double)(from r in newReiPhis select r.Phi).Max() : new double?();
                        reiPhi.Rei = reiPhi.Rei == null || reiPhi.Rei < therei ? therei : reiPhi.Rei;
                        reiPhi.Phi = reiPhi.Phi == null || reiPhi.Phi < thephi ? thephi : reiPhi.Phi;

                        var aiList = from i in mlp.ActiveIngredients
                                     select masterlist.GetActiveIngredientDisplay(new ActiveIngredientId(i.Id));
                        
                        string ai = string.Join(",", aiList.ToArray());

                        WorkOrderBreakoutData.ProductLineItemData prod = new WorkOrderBreakoutData.ProductLineItemData()
                        {
                            ApplicationMethod = string.IsNullOrEmpty(product.ApplicationMethod) ? string.Empty : product.ApplicationMethod,
                            EPANumber = mlp.RegistrationNumber,
                            Pest = product.TargetPest == null ? string.Empty : product.TargetPest.CommonName,
                            ProductName = mlp.Name,
                            RateUnit = displayRate.Unit.Name,
                            RateValue = displayRate.Value,
                            RatePerTankUnit = data.WorkOrderType == "ByRatePerTank" ? displayPerTank.Unit.Name : product.RateUnit,
                            RatePerTank = data.WorkOrderType == "ByRatePerTank" ? displayPerTank.Value : 0.0,
                            TotalUnit = displayTotal.Unit.Name,
                            TotalValue = displayTotal.Value * multiplier,
                            PHI = reiPhi != null && reiPhi.Phi != null ? reiPhi.Phi.ToString() : "*",
                            REI = reiPhi != null && reiPhi.Rei != null ? reiPhi.Rei.ToString() : "*",
                            Active = ai,
                            RateType = data.WorkOrderType == "ByRatePerTank" ? "Rate/Tank" : "Rate",
                            RatePer100 = view.TankInformation != null ? (double)product.RatePerTankValue / ((double)view.TankInformation.TankSizeValue / 100) : 0
                        };

                        productLineItems.Add(prod);
                    }

                    WorkOrderBreakoutData.FieldLineItemData field = new WorkOrderBreakoutData.FieldLineItemData()
                    {
                        Area = area.Value,
                        CenterLatLong = centerLatLon,
                        Crop = masterlist.GetCropDisplay(czDetails.CropId),
                        CropZone = czDetails.Name,
                        Field = czTreeData != null ? czTreeData.FieldName : fieldDetails.FieldNameByCropYear[cropyear],// .Name,
                        Farm = czTreeData != null ? czTreeData.FarmName : Strings.UnknownFarm_Text,
                        Products = productLineItems,
                    };

                    data.Fields.Add(field);
                }

                var converter = new ConvertToBitmap(filteredMapData, this.endpoint, view.EnvironmentalZoneDistanceValue.HasValue ? view.EnvironmentalZoneDistanceValue.Value : 0m, view.EnvironmentalZoneDistanceUnit, view.Severity);
                data.MapImage = converter.BitMap;

                Task.WaitAll(ppeTask);
                foreach (var prod in ProductPPEList)
                {
                    var mlp = masterlist.GetProduct(prod.ProductId);
                    foreach (var ppe in prod.WorkerProtections)
                    {
                        data.WorkerProtectionItems.Add(new WorkOrderBreakoutData.WorkerProtectionItem() { ProductName = masterlist.GetProduct(prod.ProductId).Name, PPE = ppe.PPE, ReEntry = ppe.PPEReentry, SignalWord = mlp.SignalWord, RestrictedUse = mlp.RestrictedUse ? "True" : "False" });
                    }
                }
            }

            return data;
        }

        public WorkOrderBreakoutData ReturnAppData(ApplicationView view)
        {
            var data = new WorkOrderBreakoutData();

            var flattenedHierarchy = endpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropyear)).GetValue(new FlattenedTreeHierarchyView());
            var masterlist = endpoint.GetMasterlistService();

            if (view != null)
            {
                Task[] ppeTask = new Task[1];
                ppeTask[0] = Task.Factory.StartNew(() =>
                {
                    if (NetworkStatus.IsInternetAvailable())
                    {
                        ProductPPEList = new List<ProductWorkerProtection>();
                        ProductPPEList.AddRange(GetPPE(view.Products));
                    }
                });

                List<string> filteredMapData = new List<string>();

                data.DatasourceName = ApplicationEnvironment.CurrentDataSourceName;
                data.CropYear = ApplicationEnvironment.CurrentCropYear;
                data.WorkOrderName = view.Name;
                data.WorkOrderDate = view.StartDateTime.ToLocalTime().ToShortDateString();
                data.StartDate = view.StartDateTime.ToLocalTime().ToString();
                data.EndDate = view.CustomEndDate.HasValue ? view.CustomEndDate.Value.ToLocalTime().ToString() : string.Empty;
                data.WorkOrderType = view.ProductStrategy.ToString();
                data.TotalArea = view.CropZones.Sum(x => x.Area.Value).ToString("N2") + " " + view.CropZones.FirstOrDefault(x => x.Area.Unit != null).Area.Unit.AbbreviatedDisplay;
                data.Fields = new List<WorkOrderBreakoutData.FieldLineItemData>();
                data.Title = Strings.DataTitle_Application_Text;
                data.AuthorizerDate = view.Authorization != null ? view.Authorization.AuthorizationDate : DateTime.Now;
                data.Authorizer = view.Authorization != null ? view.Authorization.PersonName : string.Empty;

                if (view.TankInformation != null)
                {
                    data.CarrierPerAreaValue = view.TankInformation.CarrierPerAreaValue;
                    data.CarrierPerAreaUnit = view.TankInformation.CarrierPerAreaUnit;
                    data.TankCount = view.TankInformation.TankCount;
                    data.TankSizeValue = view.TankInformation.TankSizeValue;
                    data.TankSizeUnit = view.TankInformation.TankSizeUnit;
                    data.TotalCarrierValue = view.TankInformation.TotalCarrierValue;
                    data.TotalCarrierUnit = view.TankInformation.TotalCarrierUnit;
                }

                foreach (var prod in view.Products)
                {
                    var masterlistProduct = masterlist.GetProduct(prod.Id);
                    ReiPhi reiPhi = new ReiPhi();
                    List<ReiPhi> reiPhis = new List<ReiPhi>();
                    reiPhis.AddRange(masterlist.GetProductReiPhis(prod.Id));
                    //////////////////////////////////////////////////////////
                    List<CropId> cropIds = new List<CropId>();
                    var czCropIds = from m in view.CropZones
                                    select endpoint.GetView<CropZoneDetailsView>(m.Id).Value.CropId;
                    cropIds.AddRange(czCropIds);

                    var parentCropIds = (from cid in cropIds
                                         let mlCrop = masterlist.GetCrop(cid)
                                         where mlCrop.ParentId.HasValue
                                         select new CropId(mlCrop.ParentId.Value)).ToList();
                    cropIds.AddRange(parentCropIds);

                    var prodSettings = endpoint.GetView<ProductSettingsView>(new ProductSettingId(ApplicationEnvironment.CurrentDataSourceId, prod.Id));
                    var cropYearItem = prodSettings.HasValue && prodSettings.Value.ItemsByYear.ContainsKey(ApplicationEnvironment.CurrentCropYear) ? prodSettings.Value.ItemsByYear[ApplicationEnvironment.CurrentCropYear] : null;

                    List<ProductSettingItem> cropItems = new List<ProductSettingItem>();
                    if (cropYearItem != null)
                    {
                        cropItems = (from c in cropYearItem
                                     where cropIds.Contains(c.CropId)
                                     select c).ToList();
                    }

                    List<ReiPhi> newReiPhis = new List<ReiPhi>();
                    foreach (var reiphi in reiPhis)
                    {
                        if (cropIds.Any(c => c.Id == reiphi.Crop))
                        {
                            newReiPhis.Add(reiphi);
                        }
                    }

                    foreach (var item in cropItems)
                    {
                        var rP = (from r in newReiPhis
                                  where r.Crop == item.CropId.Id
                                  select r).SingleOrDefault();
                        newReiPhis.Remove(rP);
                        newReiPhis.Add(new ReiPhi() { Crop = item.CropId.Id, Phi = (double)item.PhiValue.Value, PhiU = item.PhiUnit, Rei = (double)item.ReiValue.Value, ReiU = item.ReiUnit });
                    }
                    /////////////////////////////////////////////////////////
                    var therei = newReiPhis != null && newReiPhis.Count() > 0 ? (double)(from r in newReiPhis select r.Rei).Max() : new double?();
                    var thephi = newReiPhis != null && newReiPhis.Count() > 0 ? (double)(from r in newReiPhis select r.Phi).Max() : new double?();
                    reiPhi.Rei = reiPhi.Rei == null || reiPhi.Rei < therei ? therei : reiPhi.Rei;
                    reiPhi.Phi = reiPhi.Phi == null || reiPhi.Phi < thephi ? thephi : reiPhi.Phi;

                    var aiList = from i in masterlistProduct.ActiveIngredients
                                 select masterlist.GetActiveIngredientDisplay(new ActiveIngredientId(i.Id));

                    string ai = string.Join(",", aiList.ToArray());

                    var displayRate = DisplayMeasureConverter.ConvertToRateDisplayMeasure(prod.Id, prod.RateValue, prod.RateUnit, masterlistProduct.Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);
                    var displayTotal = DisplayMeasureConverter.ConvertToTotalDisplayMeasure(prod.Id, (decimal)prod.TotalProductValue, prod.TotalProductUnit, masterlistProduct.Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);
                    var perTankMeasure = UnitFactory.GetUnitByName(prod.RatePerTankUnit).GetMeasure((double)prod.RatePerTankValue, masterlistProduct.Density);
                    var displayPerTank = DisplayMeasureConverter.ConvertToRatePerTankDisplayMeasure(prod.Id, perTankMeasure, endpoint, ApplicationEnvironment.CurrentDataSourceId);
                    WorkOrderBreakoutData.ProductLineItemData prodData = new WorkOrderBreakoutData.ProductLineItemData()
                    {
                        ApplicationMethod = string.IsNullOrEmpty(prod.ApplicationMethod) ? string.Empty : prod.ApplicationMethod,
                        EPANumber = masterlistProduct.RegistrationNumber,
                        Pest = prod.TargetPest == null ? string.Empty : prod.TargetPest.CommonName,
                        ProductName = masterlistProduct.Name,
                        RateUnit = displayRate.Unit.Name,
                        RateValue = displayRate.Value,
                        RatePerTankUnit = data.WorkOrderType == "ByRatePerTank" ? displayPerTank.Unit.Name : prod.RateUnit,
                        RatePerTank = data.WorkOrderType == "ByRatePerTank" ? displayPerTank.Value : 0.0,
                        TotalUnit = displayTotal.Unit.Name,
                        TotalValue = displayTotal.Value,
                        PHI = reiPhi != null && reiPhi.Phi != null ? reiPhi.Phi.ToString() : "*",
                        REI = reiPhi != null && reiPhi.Rei != null ? reiPhi.Rei.ToString() : "*",
                        Active = ai,
                        RateType = data.WorkOrderType == "ByRatePerTank" ? "Rate/Tank" : "Rate",
                        RatePer100 = view.TankInformation != null ? (double)prod.RatePerTankValue / ((double)view.TankInformation.TankSizeValue / 100) : 0,
                        RatePer100Unit = displayPerTank.Unit != null ? displayPerTank.Unit.Name : string.Empty,
                    };
                    data.Products.Add(prodData);
                }

                foreach (var cz in view.CropZones)
                {
                    var includedCZ = view.CropZones.SingleOrDefault(x => x.Id == cz.Id);
                    var multiplier = includedCZ.Area.Value / view.CropZones.Sum(x => x.Area.Value);

                    var maps = endpoint.GetView<ItemMap>(cz.Id);
                    try
                    {
                        if (maps.HasValue)
                        {
                            if (maps.Value.MostRecentMapItem != null && maps.Value.MostRecentMapItem.MapData != string.Empty && maps.Value.MostRecentMapItem.MapData != "MULTIPOLYGON EMPTY") {
                                filteredMapData.Add(maps.Value.MostRecentMapItem.MapData);
                            }
                        }
                    }
                    finally { }

                    var czDetails = endpoint.GetView<CropZoneDetailsView>(cz.Id).GetValue(new CropZoneDetailsView());

                    var czTreeData = (from f in flattenedHierarchy.Items
                                      where f.CropZoneId == cz.Id
                                      select f).FirstOrDefault();


                    var area = view.CropZones.SingleOrDefault(x => x.Id == cz.Id).Area;

                    var fieldDetails = endpoint.GetView<FieldDetailsView>(czDetails.FieldId).Value;
                    var centerLatLon = string.IsNullOrEmpty(czDetails.CenterLatLong) ? fieldDetails.CenterLatLong : czDetails.CenterLatLong;
                    List<WorkOrderBreakoutData.ProductLineItemData> productLineItems = new List<WorkOrderBreakoutData.ProductLineItemData>();
                    foreach (var product in view.Products)
                    {
                        var mlp = masterlist.GetProduct(product.Id);

                        var displayRate = DisplayMeasureConverter.ConvertToRateDisplayMeasure(product.Id, product.RateValue, product.RateUnit, mlp.Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);
                        var displayTotal = DisplayMeasureConverter.ConvertToTotalDisplayMeasure(product.Id, (decimal)product.TotalProductValue, product.TotalProductUnit, mlp.Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);
                        var perTankMeasure = UnitFactory.GetUnitByName(product.RatePerTankUnit).GetMeasure((double)product.RatePerTankValue, mlp.Density);
                        var displayPerTank = DisplayMeasureConverter.ConvertToRatePerTankDisplayMeasure(product.Id, perTankMeasure, endpoint, ApplicationEnvironment.CurrentDataSourceId);

                        ReiPhi reiPhi = new ReiPhi();
                        List<ReiPhi> reiPhis = new List<ReiPhi>();
                        reiPhis.AddRange(masterlist.GetProductReiPhis(product.Id));
                        //////////////////////////////////////////////////////////
                        List<CropId> cropIds = new List<CropId>();
                        cropIds.Add(endpoint.GetView<CropZoneDetailsView>(cz.Id).Value.CropId);

                        var parentCropIds = (from cid in cropIds
                                             let mlCrop = masterlist.GetCrop(cid)
                                             where mlCrop.ParentId.HasValue
                                             select new CropId(mlCrop.ParentId.Value)).ToList();
                        cropIds.AddRange(parentCropIds);

                        var prodSettings = endpoint.GetView<ProductSettingsView>(new ProductSettingId(ApplicationEnvironment.CurrentDataSourceId, product.Id));
                        var cropYearItem = prodSettings.HasValue && prodSettings.Value.ItemsByYear.ContainsKey(ApplicationEnvironment.CurrentCropYear) ? prodSettings.Value.ItemsByYear[ApplicationEnvironment.CurrentCropYear] : null;

                        List<ProductSettingItem> cropItems = new List<ProductSettingItem>();
                        if (cropYearItem != null)
                        {
                            cropItems = (from c in cropYearItem
                                         where cropIds.Contains(c.CropId)
                                         select c).ToList();
                        }

                        List<ReiPhi> newReiPhis = new List<ReiPhi>();
                        foreach (var reiphi in reiPhis)
                        {
                            if (cropIds.Any(c => c.Id == reiphi.Crop))
                            {
                                newReiPhis.Add(reiphi);
                            }
                        }

                        foreach (var item in cropItems)
                        {
                            var rP = (from r in newReiPhis
                                      where r.Crop == item.CropId.Id
                                      select r).SingleOrDefault();
                            newReiPhis.Remove(rP);
                            newReiPhis.Add(new ReiPhi() { Crop = item.CropId.Id, Phi = (double)item.PhiValue.Value, PhiU = item.PhiUnit, Rei = (double)item.ReiValue.Value, ReiU = item.ReiUnit });
                        }
                        /////////////////////////////////////////////////////////
                        var therei = newReiPhis != null && newReiPhis.Count() > 0 ? (double)(from r in newReiPhis select r.Rei).Max() : new double?();
                        var thephi = newReiPhis != null && newReiPhis.Count() > 0 ? (double)(from r in newReiPhis select r.Phi).Max() : new double?();
                        reiPhi.Rei = reiPhi.Rei == null || reiPhi.Rei < therei ? therei : reiPhi.Rei;
                        reiPhi.Phi = reiPhi.Phi == null || reiPhi.Phi < thephi ? thephi : reiPhi.Phi;

                        var aiList = from i in mlp.ActiveIngredients
                                     select masterlist.GetActiveIngredientDisplay(new ActiveIngredientId(i.Id));

                        string ai = string.Join(",", aiList.ToArray());

                        WorkOrderBreakoutData.ProductLineItemData prod = new WorkOrderBreakoutData.ProductLineItemData()
                        {
                            ApplicationMethod = string.IsNullOrEmpty(product.ApplicationMethod) ? string.Empty : product.ApplicationMethod,
                            EPANumber = mlp.RegistrationNumber,
                            Pest = product.TargetPest == null ? string.Empty : product.TargetPest.CommonName,
                            ProductName = mlp.Name,
                            RateUnit = displayRate.Unit.Name,
                            RateValue = displayRate.Value,
                            RatePerTankUnit = data.WorkOrderType == "ByRatePerTank" ? displayPerTank.Unit.Name : product.RateUnit,
                            RatePerTank = data.WorkOrderType == "ByRatePerTank" ? displayPerTank.Value : 0.0,
                            TotalUnit = displayTotal.Unit.Name,
                            TotalValue = displayTotal.Value * multiplier,
                            PHI = reiPhi != null && reiPhi.Phi != null ? reiPhi.Phi.ToString() : "*",
                            REI = reiPhi != null && reiPhi.Rei != null ? reiPhi.Rei.ToString() : "*",
                            Active = ai,
                            RateType = data.WorkOrderType == "ByRatePerTank" ? "Rate/Tank" : "Rate",
                            RatePer100 = view.TankInformation != null ? (double)product.RatePerTankValue / ((double)view.TankInformation.TankSizeValue / 100) : 0
                        };

                        productLineItems.Add(prod);
                    }

                    WorkOrderBreakoutData.FieldLineItemData field = new WorkOrderBreakoutData.FieldLineItemData()
                    {
                        Area = area.Value,
                        CenterLatLong = centerLatLon,
                        Crop = masterlist.GetCropDisplay(czDetails.CropId),
                        CropZone = czDetails.Name,
                        Field = czTreeData != null ? czTreeData.FieldName : fieldDetails.FieldNameByCropYear[cropyear], //.Name,
                        Farm = czTreeData != null ? czTreeData.FarmName : Strings.UnknownFarm_Text,
                        Products = productLineItems,
                    };

                    data.Fields.Add(field);
                }

                var converter = new ConvertToBitmap(filteredMapData, this.endpoint, view.EnvironmentalZoneDistanceValue.HasValue ? view.EnvironmentalZoneDistanceValue.Value : 0m, view.EnvironmentalZoneDistanceUnit, view.Severity);
                data.MapImage = converter.BitMap;

                Task.WaitAll(ppeTask);
                foreach (var prod in ProductPPEList)
                {
                    var mlp = masterlist.GetProduct(prod.ProductId);
                    foreach (var ppe in prod.WorkerProtections)
                    {
                        data.WorkerProtectionItems.Add(new WorkOrderBreakoutData.WorkerProtectionItem() { ProductName = masterlist.GetProduct(prod.ProductId).Name, PPE = ppe.PPE, ReEntry = ppe.PPEReentry, SignalWord = mlp.SignalWord, RestrictedUse = mlp.RestrictedUse ? "True" : "False" });
                    }
                }
            }

            return data;
        }

        public string DisplayName
        {
            get { return Strings.GeneratorName_BreakOut_Text; }
        }

        private double ReportedArea(CropZoneDetailsView czDetails)
        {
            if (czDetails.ReportedArea != null)
            {
                return (double)czDetails.ReportedArea;
            }
            else if (czDetails.ReportedArea == null && czDetails.BoundaryArea != null)
            {
                return (double)czDetails.BoundaryArea;
            }
            else if (czDetails.ReportedArea == null && czDetails.BoundaryArea == null)
            {
                return 0.0;
            }
            else
            {
                return 0.0;
            }
        }

        public List<ProductWorkerProtection> GetPPE(List<IncludedProduct> includedProducts)
        {
            List<ProductWorkerProtection> ppeList = new List<ProductWorkerProtection>();
            foreach (var prod in includedProducts)
            {
                try
                {
                    string dataText = string.Empty;

                    //create task....and wait for it to complete...
                    Task getPPE = Task.Factory.StartNew(() =>
                    {
                        dataText = GetWorkerProtectionData(prod.Id);
                    });

                    getPPE.Wait();
                    JObject data = JObject.Parse(dataText);

                    var ppe = from d in JArray.Parse(data["ProductWorkerProtections"].ToString())
                              select JArray.Parse(d["WorkerProtections"].ToString());

                    var workerProtectionList = from d in JArray.Parse(data["ProductWorkerProtections"].ToString())
                                               from i in JArray.Parse(d["WorkerProtections"].ToString())
                                               select new WorkerProtection { PPE = i["PPE"] != null ? i["PPE"].ToString() : string.Empty, PPEReentry = i["PPEReentry"] != null ? i["PPEReentry"].ToString() : string.Empty };
                    var prodWorkProtection = new ProductWorkerProtection() { ProductId = prod.Id, WorkerProtections = workerProtectionList.ToList() };
                    ppeList.Add(prodWorkProtection);
                }
                catch (Exception x)
                {

                }
            }

            return ppeList;
        }

        public string GetWorkerProtectionData(ProductId productId)
        {
            var requestUriFormat = @"{0}/workerprotections?format=json&c=en-US&u=Y29yZXkucGVya2lucw%3D%3D&p=Y29yZXlwMQ%3D%3D&ProductIds={1}";
            var requestUri = string.Format(requestUriFormat, Infrastructure.ApplicationEnvironment.MasterlistRemoteUri, productId.Id);

            WebClient client = new WebClient();
            client.Headers.Add("Authorization", "basic QWdDQVBJOlJhbmRvbU1vbmtleVN1cmZzT25MYXZhIQ==");
            string data = client.DownloadString(new Uri(requestUri));

            return data;
        }
    }
}
