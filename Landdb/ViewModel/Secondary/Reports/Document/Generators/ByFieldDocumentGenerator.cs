﻿using AgC.UnitConversion;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Client.Services;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Map;
using Landdb.Domain.ReadModels.ProductSetting;
using Landdb.Domain.ReadModels.Shared;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Domain.ReadModels.WorkOrder;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ReportModels.Application;
using Landdb.ReportModels.WorkOrder;
using Landdb.ViewModel.Overlays;
using Landdb.ViewModel.Secondary.Map;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Landdb.Resources;
using Telerik.Reporting;
using ThinkGeo.MapSuite.Core;

namespace Landdb.ViewModel.Secondary.Reports.Document.Generators
{
    public class ByFieldDocumentGenerator : IDocumentReportGenerator
    {
        IClientEndpoint endpoint;
        int cropyear;
        List<ProductWorkerProtection> ProductPPEList;
        ProgressOverlayViewModel vm;
        IProgressTracker progress;
        int areadecimals = 2;

        public ByFieldDocumentGenerator(IClientEndpoint endpoint, int cropyear)
        {
            this.endpoint = endpoint;
            this.cropyear = cropyear;

            vm = new ProgressOverlayViewModel();
            progress = vm;
        }


        public Telerik.Reporting.ReportSource Generate (DocumentQueryCriteria criteria, DocumentReportCreatorViewModel dcvm)
        {
            ScreenDescriptor progressDesc = new ScreenDescriptor("Landdb.Views.Shared.Popups.ProgressPopUpView", "fullprogress", vm);
            Messenger.Default.Send<ShowPopupMessage>(new ShowPopupMessage() { ScreenDescriptor = progressDesc });

            Action StartGeneration;
            StartGeneration = () => ProcessData(criteria, dcvm);
            Thread t;
            t = new Thread(StartGeneration.Invoke);
            t.Start();

            return null;
        }

        void ProcessData(DocumentQueryCriteria criteria, DocumentReportCreatorViewModel dcvm)
        {
            ReportBook book = new ReportBook();
            var rs = new InstanceReportSource();

            if (criteria.IncludedDocuments.Any())
            {
                UpdateProgressMessage(progress, Strings.ProgressMessage_CreatingByFieldDocs_Text);
                UpdateProgressTotalRecords(progress, criteria.IncludedDocuments.Count());
                UpdateProgressCurrentRecord(progress, 0);
                int cnt = 0;

                foreach (var doc in criteria.IncludedDocuments)
                {

                    switch (doc.DocumentType)
                    {
                        case "WorkOrder":

                            //call this for each crop zone associated with the work order
                            var woId = new WorkOrderId(doc.Identity.DataSourceId, doc.Identity.Id);
                            var woDetail = endpoint.GetView<WorkOrderView>(woId).Value;

                            foreach (var cz in woDetail.CropZones)
                            {
                                var woData = ReturnWOData(woDetail, cz.Id);

                                if (criteria.SpanishTranslation)
                                {
                                    var spReport = new Landdb.Views.Secondary.Reports.Work_Order.SpanishWorkOrder();
                                    spReport.DataSource = woData;
                                    spReport.Name = woData.DatasourceName;

                                    ////EXAMPLE OF HOW TO BIND TO A SUB REPORT
                                    var mainReport = spReport;
                                    var itemDetail = (Telerik.Reporting.SubReport)mainReport.Items.Find("subReport1", true)[0];
                                    var repSource = (InstanceReportSource)itemDetail.ReportSource;
                                    var subReport = (Telerik.Reporting.Report)repSource.ReportDocument;
                                    var odsItems = (Telerik.Reporting.ObjectDataSource)subReport.DataSource;
                                    odsItems.DataSource = woData.Products;

                                    book.Reports.Add(spReport);
                                }
                                else
                                {
                                    var report = new Landdb.Views.Secondary.Reports.Work_Order.LandscapeWorkOrder();
                                    report.DataSource = woData;
                                    report.Name = woData.DatasourceName;

                                    ////EXAMPLE OF HOW TO BIND TO A SUB REPORT
                                    var mainReport = report;
                                    var itemDetail = (Telerik.Reporting.SubReport)mainReport.Items.Find("subReport1", true)[0];
                                    var repSource = (InstanceReportSource)itemDetail.ReportSource;
                                    var subReport = (Telerik.Reporting.Report)repSource.ReportDocument;
                                    var odsItems = (Telerik.Reporting.ObjectDataSource)subReport.DataSource;
                                    odsItems.DataSource = woData.Products;

                                    book.Reports.Add(report);
                                }
                            }

                            break;
                        case "Application":
                            var appId = new ApplicationId(doc.Identity.DataSourceId, doc.Identity.Id);
                            var appDetails = endpoint.GetView<ApplicationView>(appId).Value;

                            foreach (var cz in appDetails.CropZones)
                            {
                                var appData = ReturnAppData(appDetails, cz.Id);

                                if (criteria.SpanishTranslation)
                                {   
                                    var appreport = new Landdb.Views.Secondary.Reports.Application.SpanishApplication();
                                    appreport.DataSource = appData;
                                    appreport.Name = appData.DatasourceName;

                                    ////EXAMPLE OF HOW TO BIND TO A SUB REPORT
                                    var mainReport = appreport;
                                    var itemDetail = (Telerik.Reporting.SubReport)mainReport.Items.Find("subReport1", true)[0];
                                    var repSource = (InstanceReportSource)itemDetail.ReportSource;
                                    var subReport = (Telerik.Reporting.Report)repSource.ReportDocument;
                                    var odsItems = (Telerik.Reporting.ObjectDataSource)subReport.DataSource;
                                    odsItems.DataSource = appData.Products;

                                    book.Reports.Add(appreport);
                                }
                                else
                                {
                                    var appreport = new Landdb.Views.Secondary.Reports.Application.StandardApplication();
                                    appreport.DataSource = appData;
                                    appreport.Name = appData.DatasourceName;

                                    ////EXAMPLE OF HOW TO BIND TO A SUB REPORT
                                    var mainReport = appreport;
                                    var itemDetail = (Telerik.Reporting.SubReport)mainReport.Items.Find("subReport1", true)[0];
                                    var repSource = (InstanceReportSource)itemDetail.ReportSource;
                                    var subReport = (Telerik.Reporting.Report)repSource.ReportDocument;
                                    var odsItems = (Telerik.Reporting.ObjectDataSource)subReport.DataSource;
                                    odsItems.DataSource = appData.Products;

                                    book.Reports.Add(appreport);
                                }
                            }

                            break;
                        default:
                            break;
                    }

                    //update progress
                    cnt++;
                    UpdateProgressCurrentRecord(progress, cnt);
                }
            }

            //close progress
            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());

            rs.ReportDocument = book;
            dcvm.ReportSource = rs;
        }

        void UpdateProgressMessage(IProgressTracker progress, string message)
        {
            if (progress != null)
            {
                progress.StatusMessage = message;
            }
        }

        void UpdateProgressTotalRecords(IProgressTracker progress, int totalRecords)
        {
            if (progress != null)
            {
                progress.TotalRecords = totalRecords;
            }
        }

        void UpdateProgressCurrentRecord(IProgressTracker progress, long currentRecord)
        {
            if (progress != null)
            {
                progress.CurrentRecord = (int)currentRecord;
            }
        }

        public WorkOrderData ReturnWOData(WorkOrderView view, CropZoneId czId)
        {
            var data = new WorkOrderData();

            var mapsettings = endpoint.GetMapSettings();
            areadecimals = mapsettings.AreaDecimals; var flattenedHierarchy = endpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropyear)).GetValue(new FlattenedTreeHierarchyView());
            var masterlist = endpoint.GetMasterlistService();

            if (view != null)
            {
                Task[] ppeTask = new Task[1];
                ppeTask[0] = Task.Factory.StartNew(() =>
                {
                    if (NetworkStatus.IsInternetAvailable())
                    {
                        ProductPPEList = new List<ProductWorkerProtection>();
                        ProductPPEList.AddRange(GetPPE(view.Products));
                    }
                });

                data.DatasourceName = ApplicationEnvironment.CurrentDataSourceName;
                //data.StreetAddress = 
                data.WorkOrderName = view.Name;
                data.WorkOrderDate = view.StartDateTime.ToLocalTime().ToShortDateString();
                data.WorkOrderType = view.ApplicationStrategy.ToString();
                data.TotalArea = view.CropZones.SingleOrDefault(id => id.Id == czId).Area.Value.ToString("N2") + " " + view.CropZones.SingleOrDefault(id => id.Id == czId).Area.Unit.AbbreviatedDisplay;
                
                var includedCZ = view.CropZones.SingleOrDefault(x => x.Id == czId);
                var multiplier = includedCZ.Area.Value / view.CropZones.Sum(x => x.Area.Value);
                
                data.TankInformationVisibility = view.TankInformation == null ? false : true;
                if (view.TankInformation != null)
                {
                    data.TankSizeValue = view.TankInformation.TankSizeValue;
                    data.TankSizeUnit = view.TankInformation.TankSizeUnit;
                    data.CarrierPerAreaValue = view.TankInformation.CarrierPerAreaValue;
                    data.CarrierPerAreaUnit = view.TankInformation.CarrierPerAreaUnit;

                    //TO DO :: Tank Info will need to be recalculated based on the included field.....
                    data.TotalCarrierValue = view.TankInformation.TotalCarrierValue * (decimal)multiplier;
                    data.TotalCarrierUnit = view.TankInformation.TotalCarrierUnit;
                    data.TankCount = view.TankInformation.TankCount * (decimal)multiplier;
                }

                //TO DO :: PRODUCTS WILL NEED TO BE CALCULATED BASED OF INCLUDED FIELD
                int cnt = 0;
                foreach (var prod in view.Products)
                {
                    var masterlistProduct = masterlist.GetProduct(prod.Id);
                    ReiPhi reiPhi = new ReiPhi();
                    List<ReiPhi> reiPhis = new List<ReiPhi>();
                    reiPhis.AddRange(masterlist.GetProductReiPhis(prod.Id));
                    //////////////////////////////////////////////////////////
                    List<CropId> cropIds = new List<CropId>();
                    cropIds.Add(endpoint.GetView<CropZoneDetailsView>(czId).Value.CropId);

                    var parentCropIds = (from cid in cropIds
                                         let mlCrop = masterlist.GetCrop(cid)
                                         where mlCrop.ParentId.HasValue
                                         select new CropId(mlCrop.ParentId.Value)).ToList();
                    cropIds.AddRange(parentCropIds);

                    var prodSettings = endpoint.GetView<ProductSettingsView>(new ProductSettingId(ApplicationEnvironment.CurrentDataSourceId, prod.Id));
                    var cropYearItem = prodSettings.HasValue && prodSettings.Value.ItemsByYear.ContainsKey(ApplicationEnvironment.CurrentCropYear) ? prodSettings.Value.ItemsByYear[ApplicationEnvironment.CurrentCropYear] : null;

                    List<ProductSettingItem> cropItems = new List<ProductSettingItem>();
                    if (cropYearItem != null)
                    {
                        cropItems = (from c in cropYearItem
                                     where cropIds.Contains(c.CropId)
                                     select c).ToList();
                    }

                    List<ReiPhi> newReiPhis = new List<ReiPhi>();
                    foreach (var reiphi in reiPhis)
                    {
                        if (cropIds.Any(c => c.Id == reiphi.Crop))
                        {
                            newReiPhis.Add(reiphi);
                        }
                    }

                    foreach (var item in cropItems)
                    {
                        var rP = (from r in newReiPhis
                                  where r.Crop == item.CropId.Id
                                  select r).SingleOrDefault();
                        newReiPhis.Remove(rP);
                        newReiPhis.Add(new ReiPhi() { Crop = item.CropId.Id, Phi = (double)item.PhiValue.Value, PhiU = item.PhiUnit, Rei = (double)item.ReiValue.Value, ReiU = item.ReiUnit });
                    }
                    /////////////////////////////////////////////////////////
                    var therei = newReiPhis != null && newReiPhis.Count() > 0 ? (double)(from r in newReiPhis select r.Rei).Max() : new double?();
                    var thephi = newReiPhis != null && newReiPhis.Count() > 0 ? (double)(from r in newReiPhis select r.Phi).Max() : new double?();
                    reiPhi.Rei = reiPhi.Rei == null || reiPhi.Rei < therei ? therei : reiPhi.Rei;
                    reiPhi.Phi = reiPhi.Phi == null || reiPhi.Phi < thephi ? thephi : reiPhi.Phi;

                    var aiList = from i in masterlistProduct.ActiveIngredients
                                 select masterlist.GetActiveIngredientDisplay(new ActiveIngredientId(i.Id));

                    string ai = string.Join(",", aiList.ToArray());

                    var registrationNumber = masterlistProduct.RegistrationNumber;
                    string epaNoTrailingChar = string.Empty;
                    if (registrationNumber == null) { epaNoTrailingChar = string.Empty; }
                    else
                    {
                        Regex epaNumberRegex = new Regex("([1-9][0-9]*)");

                        var matches = from m in epaNumberRegex.Matches(registrationNumber).Cast<Match>()
                                      select m.Value;
                        epaNoTrailingChar = string.Join("-", matches.ToArray());
                    }

                    var displayRate = DisplayMeasureConverter.ConvertToRateDisplayMeasure(prod.Id, prod.RateValue, prod.RateUnit, masterlistProduct.Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);
                    var displayTotal = DisplayMeasureConverter.ConvertToTotalDisplayMeasure(prod.Id, (decimal)prod.TotalProductValue, prod.TotalProductUnit, masterlistProduct.Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);
                    var perTankMeasure = UnitFactory.GetUnitByName(prod.RatePerTankUnit).GetMeasure((double)prod.RatePerTankValue, masterlistProduct.Density);
                    var displayPerTank = DisplayMeasureConverter.ConvertToRatePerTankDisplayMeasure(prod.Id, perTankMeasure, endpoint, ApplicationEnvironment.CurrentDataSourceId);

                    var percentage = includedCZ.Area.Value / view.CropZones.Sum(x => x.Area.Value);

                    WorkOrderData.ProductLineItemData prodData = new WorkOrderData.ProductLineItemData()
                    {
                        ApplicationMethod = string.IsNullOrEmpty(prod.ApplicationMethod) ? string.Empty : prod.ApplicationMethod,
                        EPANumber = epaNoTrailingChar,
                        Pest = prod.TargetPest == null ? string.Empty : prod.TargetPest.CommonName,
                        ProductName = masterlistProduct.Name,
                        RateUnit = displayRate.Unit.Name,
                        RateValue = displayRate.Value,
                        RatePerTankUnit = data.WorkOrderType == "ByRatePerTank" ? displayPerTank.Unit.Name : prod.RateUnit,
                        RatePerTank = data.WorkOrderType == "ByRatePerTank" ? displayPerTank.Value : 0.0,
                        TotalUnit = displayTotal.Unit.Name,
                        TotalValue = displayTotal.Value * percentage,
                        PHI = reiPhi != null && reiPhi.Phi != null ? reiPhi.Phi.ToString() : "*",
                        REI = reiPhi != null && reiPhi.Rei != null ? reiPhi.Rei.ToString() : "*",
                        Active = ai,
                        RateType = data.WorkOrderType == "ByRatePerTank" ? "Rate/Tank" : "Rate",
                        RatePer100 = view.TankInformation != null ? (double)prod.RatePerTankValue / ((double)view.TankInformation.TankSizeValue / 100) : 0,
                        RatePer100Unit = displayPerTank.Unit != null ? displayPerTank.Unit.Name : string.Empty,
                        PercentApplied = prod.PercentApplied,
                        TotalArea = (decimal)view.CropZones.SingleOrDefault(id => id.Id == czId).Area.Value,
                        TotalAreaUnit = view.CropZones.SingleOrDefault(id => id.Id == czId).Area.Unit,
                        RowCount = cnt,
                    };

                    List<WorkOrderData.AssociatedProductItem> associatedProducts = new List<WorkOrderData.AssociatedProductItem>();

                    foreach (var associate in prod.AssociatedProducts)
                    {
                        var mlp = masterlist.GetProduct(associate.ProductId);
                        var item = new WorkOrderData.AssociatedProductItem();
                        item.CustomRateType = associate.CustomRateType;
                        item.CustomRateValue = associate.CustomProductValue;
                        item.HasCost = associate.HasCost;
                        item.ProductName = associate.ProductName;
                        item.RatePerAreaUnit = UnitFactory.GetPackageSafeUnit(associate.RatePerAreaUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                        item.RatePerAreaValue = associate.RatePerAreaValue;
                        item.TotalProductUnit = UnitFactory.GetPackageSafeUnit(associate.TotalProductUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                        item.TotalProductValue = associate.TotalProductValue * (decimal)percentage;
                        IUnit customUnit = UnitFactory.GetPackageSafeUnit(associate.CustomProductUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);

                        switch (associate.CustomRateType)
                        {
                            case "ByBag":
                                item.DisplayCustomUnit = new CompositeUnit(customUnit.AbbreviatedDisplay, 1, Strings.Unit_Bag_Text);
                                break;
                            case "ByCWT":
                                item.DisplayCustomUnit = new CompositeUnit(customUnit.AbbreviatedDisplay, 100, Strings.Unit_Weight_Text);
                                break;
                            case "ByRow":
                                item.DisplayCustomUnit = new CompositeUnit(customUnit.AbbreviatedDisplay, 1000, Strings.Unit_LengthRow_Text);
                                break;
                            case "BySeed":
                                var parentProd = masterlist.GetProduct(prod.Id);
                                item.DisplayCustomUnit = new CompositeUnit(customUnit.AbbreviatedDisplay, 1, parentProd.StdUnit);
                                break;
                            default:

                                break;
                        }

                        //if (associate.HasCost)
                        //{
                        //    var associateInvProd = in inventory.Products.SingleOrDefault(x => x.Key == associate.ProductId).Value;

                        //    var avgCost = associateInvProd != null ? associateInvProd.AveragePriceValue : 0;
                        //    var avgCostUnit = associateInvProd != null && !string.IsNullOrEmpty(associateInvProd.AveragePriceUnit) ? UnitFactory.GetPackageSafeUnit(associateInvProd.AveragePriceUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit) : null;
                        //    var totalAssociateProd = UnitFactory.GetPackageSafeUnit(associate.TotalProductUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit).GetMeasure((double)associate.TotalProductValue, mlp.Density);
                        //    var convertedValue = avgCostUnit != null && totalAssociateProd.Unit != avgCostUnit && totalAssociateProd.CanConvertTo(avgCostUnit) ? totalAssociateProd.GetValueAs(avgCostUnit) : totalAssociateProd.Value;
                        //    item.TotalCost = (decimal)convertedValue;
                        //    //TotalAvgCost += avgCost != 0 ? (decimal)(convertedValue * avgCost) : 0;


                        //    if (associate.SpecificCostPerUnitValue != 0 && !string.IsNullOrEmpty(associate.SpecificCostPerUnitUnit))
                        //    {
                        //        var specificUnit = UnitFactory.GetPackageSafeUnit(associate.SpecificCostPerUnitUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                        //        var convertedSpecificValue = totalAssociateProd.Unit != specificUnit && totalAssociateProd.CanConvertTo(specificUnit) ? totalAssociateProd.GetValueAs(specificUnit) : totalAssociateProd.Value;
                        //        var specificTotalCost = (decimal)convertedSpecificValue * associate.SpecificCostPerUnitValue;
                        //        item.TotalCost = specificTotalCost;
                        //        //TotalSpecificCost += specificTotalCost;
                        //    }
                        //}

                        prodData.AssociatedProducts.Add(item);
                    }

                    data.Products.Add(prodData);
                    cnt++;
                }

                List<string> filteredMapData = new List<string>();


                var czDetails = endpoint.GetView<CropZoneDetailsView>(czId).GetValue(new CropZoneDetailsView());

                var czTreeData = (from f in flattenedHierarchy.Items
                                    where f.CropZoneId == czId
                                    select f).FirstOrDefault();

                
                var area = view.CropZones.SingleOrDefault(x => x.Id == czId).Area;

                var fieldDetails = endpoint.GetView<FieldDetailsView>(czDetails.FieldId).Value;
                var centerLatLon = string.IsNullOrEmpty(czDetails.CenterLatLong) ? fieldDetails.CenterLatLong : czDetails.CenterLatLong;

                WorkOrderData.FieldLineItemData field = new WorkOrderData.FieldLineItemData()
                {
                    Area = area.Value,
                    CenterLatLong = centerLatLon,
                    Crop = masterlist.GetCropDisplay(czDetails.CropId),
                    CropZone = czDetails.Name,
                    Field = czTreeData != null ? czTreeData.FieldName : fieldDetails.FieldNameByCropYear[cropyear], //.Name,
                    Farm = czTreeData != null ? czTreeData.FarmName : Strings.UnknownFarm_Text,
                };

                data.Fields.Add(field);

                var maps = endpoint.GetView<ItemMap>(czId);
                try
                {
                    List<Feature> filteredLargeMapData = new List<Feature>();
                    if (maps.HasValue)
                    {
                        if (maps.Value.MostRecentMapItem != null && maps.Value.MostRecentMapItem.MapData != string.Empty && maps.Value.MostRecentMapItem.MapData != "MULTIPOLYGON EMPTY") {
                            var fieldName = flattenedHierarchy.Items.FirstOrDefault(c => c.CropZoneId == czId).FieldName;
                            //string customarealabel = GetCustomCropZoneName(czId.Id, fieldName);
                            string customareaonlylabel = GetCustomAreaLabel(czId.Id, false);
                            string customarealabel = DisplayLabelWithArea(fieldName, customareaonlylabel);
                            IDictionary<string, string> columnvalues = new Dictionary<string, string>();
                            columnvalues.Add(@"LdbLabel", fieldName);
                            columnvalues.Add(@"AreaLabel", customarealabel);
                            columnvalues.Add(@"AreaOnlyLabel", customareaonlylabel);
                            Feature newfeature = new Feature(maps.Value.MostRecentMapItem.MapData, czId.Id.ToString(), columnvalues);
                            filteredLargeMapData.Add(newfeature);

                            var detailedMap = new ConvertToBitmap(filteredLargeMapData, true, endpoint, 20.0, view.EnvironmentalZoneDistanceValue, view.EnvironmentalZoneDistanceUnit, view.Severity);
                            data.MapImage = detailedMap.BitMap;
                        }
                    }
                }
                finally { }

                foreach (var applicator in view.Applicators)
                {
                    WorkOrderData.ApplicatorLineItemData appLineItem = new WorkOrderData.ApplicatorLineItemData()
                    {
                        Name = applicator.PersonName,
                        Company = applicator.CompanyName,
                        LicenseNumber = applicator.LicenseNumber,
                        ExpirationDate = applicator.Expires,
                        DisplayName = string.Format("{0} ({1})", applicator.PersonName, applicator.CompanyName),
                    };

                    data.Applicators.Add(appLineItem);
                }

                //add all the ppe info into data....
                Task.WaitAll(ppeTask);
                foreach (var prod in ProductPPEList)
                {
                    var mlp = masterlist.GetProduct(prod.ProductId);
                    foreach (var ppe in prod.WorkerProtections)
                    {
                        data.WorkerProtectionItems.Add(new WorkOrderData.WorkerProtectionItem() { ProductName = masterlist.GetProduct(prod.ProductId).Name, PPE = ppe.PPE, ReEntry = ppe.PPEReentry, SignalWord = mlp.SignalWord, RestrictedUse = mlp.RestrictedUse ? "True" : "False" });
                    }
                }

                data.Authorizer = view.Authorization != null ? view.Authorization.PersonName : string.Empty;
                data.AuthorizerDate = view.Authorization != null ? view.Authorization.AuthorizationDate : DateTime.Now;

                data.CropYear = cropyear;
                data.Notes = view.Notes;
            }

            return data;
        }

        //private string GetCustomCropZoneName(Guid Id, string name)
        //{
        //    string stringid = Id.ToString();
        //    string labelname = name;
        //    var fieldMaybe = endpoint.GetView<CropZoneDetailsView>(new CropZoneId(ApplicationEnvironment.CurrentDataSourceId, Id));
        //    fieldMaybe.IfValue(field =>
        //    {
        //        if (field.ReportedArea != null && !string.IsNullOrWhiteSpace(field.ReportedAreaUnit))
        //        {
        //            labelname = DisplaySelectedAreaDecimals(name, field.ReportedArea.Value, field.ReportedAreaUnit);
        //        }
        //        else if (field.BoundaryArea != null && !string.IsNullOrWhiteSpace(field.BoundaryAreaUnit))
        //        {
        //            labelname = DisplaySelectedAreaDecimals(name, field.BoundaryArea.Value, field.BoundaryAreaUnit);
        //        }
        //        else
        //        {
        //            labelname = name;
        //        }

        //    });
        //    return labelname;
        //}

        //private string DisplaySelectedAreaDecimals(string name, double unitvalue, string unitname)
        //{
        //    int areadecimals = 2;
        //    IUnit _unit = UnitFactory.GetUnitByName(unitname);
        //    string decimalstring = "N" + areadecimals.ToString("N0");
        //    return name + "\r\n" + unitvalue.ToString(decimalstring);
        //}

        private string DisplayLabelWithArea(string name, string customarealabel) {
            return name + "\r\n" + customarealabel;
        }

        private string GetCustomAreaLabel(Guid Id, bool isfieldlabel) {
            string stringid = Id.ToString();
            string labelname = string.Empty;
            if (isfieldlabel) {
                var fieldMaybe = endpoint.GetView<FieldDetailsView>(new FieldId(ApplicationEnvironment.CurrentDataSourceId, Id));
                fieldMaybe.IfValue(field => {
                    labelname = string.Empty;
                    if (field.ReportedArea != null && !string.IsNullOrWhiteSpace(field.ReportedAreaUnit)) {
                        labelname = DisplaySelectedAreaOnlyDecimals(field.ReportedArea.Value, field.ReportedAreaUnit);
                    }
                    else if (field.BoundaryArea != null && !string.IsNullOrWhiteSpace(field.BoundaryAreaUnit)) {
                        labelname = DisplaySelectedAreaOnlyDecimals(field.BoundaryArea.Value, field.BoundaryAreaUnit);
                    }
                });
            }
            else {
                var fieldMaybe = endpoint.GetView<CropZoneDetailsView>(new CropZoneId(ApplicationEnvironment.CurrentDataSourceId, Id));
                fieldMaybe.IfValue(field => {
                    labelname = string.Empty;
                    if (field.ReportedArea != null && !string.IsNullOrWhiteSpace(field.ReportedAreaUnit)) {
                        labelname = DisplaySelectedAreaOnlyDecimals(field.ReportedArea.Value, field.ReportedAreaUnit);
                    }
                    else if (field.BoundaryArea != null && !string.IsNullOrWhiteSpace(field.BoundaryAreaUnit)) {
                        labelname = DisplaySelectedAreaOnlyDecimals(field.BoundaryArea.Value, field.BoundaryAreaUnit);
                    }
                });
            }
            return labelname;
        }

        private string DisplaySelectedAreaOnlyDecimals(double unitvalue, string unitname) {
            IUnit _unit = UnitFactory.GetUnitByName(unitname);
            string decimalstring = "N" + areadecimals.ToString("N0");
            return unitvalue.ToString(decimalstring);
        }
        public ApplicationData ReturnAppData(ApplicationView view, CropZoneId czId)
        {
            var applicationData = new ApplicationData();
            var appData = view;

            var flattenedHierarchy = endpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropyear)).GetValue(new FlattenedTreeHierarchyView());
            var masterlist = endpoint.GetMasterlistService();

            Task[] ppeTask = new Task[1];
            ppeTask[0] = Task.Factory.StartNew(() =>
            {
                ProductPPEList = new List<ProductWorkerProtection>(); 
                if (NetworkStatus.IsInternetAvailable())
                {
                    ProductPPEList.AddRange(GetPPE(view.Products));
                }
            });

            applicationData.DatasourceName = ApplicationEnvironment.CurrentDataSourceName;
            applicationData.ApplicationName = appData.Name;
            applicationData.ApplicationDate = appData.StartDateTime.ToShortDateString();
            applicationData.StartDate = appData.StartDateTime.ToLocalTime();
            applicationData.ApplicationType = appData.ProductStrategy.ToString();
            var area = appData.CropZones.FirstOrDefault(cid => cid.Id == czId).Area;
            applicationData.TotalArea = area.Value.ToString("N2") + " " + area.Unit.AbbreviatedDisplay;
            applicationData.EndDate = appData.CustomEndDate != null ? ((DateTime)appData.CustomEndDate).ToLocalTime() : appData.StartDateTime.ToLocalTime().AddHours(1.0);

            if (appData.Conditions != null)
            {
                applicationData.Temperature = appData.Conditions.TemperatureValue;
                applicationData.Humidity = appData.Conditions.HumidityPercent;
                applicationData.WindDirection = appData.Conditions.WindDirection;
                applicationData.WindSpeed = appData.Conditions.WindSpeedValue;
                applicationData.SkyCondition = appData.Conditions.SkyCondition == "-1" ? string.Empty : appData.Conditions.SkyCondition;
                applicationData.SoilCondition = appData.Conditions.SoilMoisture == "-1" ? string.Empty : appData.Conditions.SoilMoisture;
            }

            var includedCZ = view.CropZones.SingleOrDefault(x => x.Id == czId);
            var multiplier = includedCZ.Area.Value / view.CropZones.Sum(x => x.Area.Value);

            if (view.TankInformation != null)
            {
                applicationData.CarrierPerAreaValue = view.TankInformation.CarrierPerAreaValue;
                applicationData.CarrierPerAreaUnit = view.TankInformation.CarrierPerAreaUnit;
                applicationData.TankCount = view.TankInformation.TankCount;
                applicationData.TankSizeValue = view.TankInformation.TankSizeValue;
                applicationData.TankSizeUnit = view.TankInformation.TankSizeUnit;
                applicationData.TotalCarrierValue = view.TankInformation.CarrierPerAreaValue * (decimal)includedCZ.Area.Value;
                applicationData.TotalCarrierUnit = view.TankInformation.TotalCarrierUnit;
            }

            foreach (var prod in appData.Products)
            {
                var masterlistProduct = masterlist.GetProduct(prod.Id);
                ReiPhi reiPhi = new ReiPhi();
                List<ReiPhi> reiPhis = new List<ReiPhi>();
                reiPhis.AddRange(masterlist.GetProductReiPhis(prod.Id));
                //////////////////////////////////////////////////////////
                List<CropId> cropIds = new List<CropId>();
                var czCropIds = from m in appData.CropZones
                                select endpoint.GetView<CropZoneDetailsView>(m.Id).Value.CropId;
                cropIds.AddRange(czCropIds);

                var parentCropIds = (from cid in cropIds
                                     let mlCrop = masterlist.GetCrop(cid)
                                     where mlCrop.ParentId.HasValue
                                     select new CropId(mlCrop.ParentId.Value)).ToList();
                cropIds.AddRange(parentCropIds);

                var prodSettings = endpoint.GetView<ProductSettingsView>(new ProductSettingId(ApplicationEnvironment.CurrentDataSourceId, prod.Id));
                var cropYearItem = prodSettings.HasValue && prodSettings.Value.ItemsByYear.ContainsKey(ApplicationEnvironment.CurrentCropYear) ? prodSettings.Value.ItemsByYear[ApplicationEnvironment.CurrentCropYear] : null;

                List<ProductSettingItem> cropItems = new List<ProductSettingItem>();
                if (cropYearItem != null)
                {
                    cropItems = (from c in cropYearItem
                                 where cropIds.Contains(c.CropId)
                                 select c).ToList();
                }

                List<ReiPhi> newReiPhis = new List<ReiPhi>();
                foreach (var reiphi in reiPhis)
                {
                    if (cropIds.Any(c => c.Id == reiphi.Crop))
                    {
                        newReiPhis.Add(reiphi);
                    }
                }

                foreach (var item in cropItems)
                {
                    var rP = (from r in newReiPhis
                              where r.Crop == item.CropId.Id
                              select r).SingleOrDefault();
                    newReiPhis.Remove(rP);
                    newReiPhis.Add(new ReiPhi() { Crop = item.CropId.Id, Phi = (double)item.PhiValue.Value, PhiU = item.PhiUnit, Rei = (double)item.ReiValue.Value, ReiU = item.ReiUnit });
                }
                /////////////////////////////////////////////////////////
                var therei = newReiPhis != null && newReiPhis.Count() > 0 ? (double)(from r in newReiPhis select r.Rei).Max() : new double?();
                var thephi = newReiPhis != null && newReiPhis.Count() > 0 ? (double)(from r in newReiPhis select r.Phi).Max() : new double?();
                reiPhi.Rei = reiPhi.Rei == null || reiPhi.Rei < therei ? therei : reiPhi.Rei;
                reiPhi.Phi = reiPhi.Phi == null || reiPhi.Phi < thephi ? thephi : reiPhi.Phi;

                var aiList = from i in masterlistProduct.ActiveIngredients
                             select masterlist.GetActiveIngredientDisplay(new ActiveIngredientId(i.Id));

                string ai = string.Join(",", aiList.ToArray());

                var registrationNumber = masterlistProduct.RegistrationNumber;
                string epaNoTrailingChar = string.Empty;
                if (registrationNumber == null) { epaNoTrailingChar = string.Empty; }
                else
                {
                    Regex epaNumberRegex = new Regex("([1-9][0-9]*)");

                    var matches = from m in epaNumberRegex.Matches(registrationNumber).Cast<Match>()
                                  select m.Value;
                    epaNoTrailingChar = string.Join("-", matches.ToArray());
                }

                var displayRate = DisplayMeasureConverter.ConvertToRateDisplayMeasure(prod.Id, prod.RateValue, prod.RateUnit, masterlistProduct.Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);
                var displayTotal = DisplayMeasureConverter.ConvertToTotalDisplayMeasure(prod.Id, (decimal)prod.TotalProductValue, prod.TotalProductUnit, masterlistProduct.Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);
                var perTankMeasure = UnitFactory.GetUnitByName(prod.RatePerTankUnit).GetMeasure((double)prod.RatePerTankValue, masterlistProduct.Density);
                var displayPerTank = DisplayMeasureConverter.ConvertToRatePerTankDisplayMeasure(prod.Id, perTankMeasure, endpoint, ApplicationEnvironment.CurrentDataSourceId);

                var cnt = 0;
                ApplicationData.ProductLineItemData prodData = new ApplicationData.ProductLineItemData()
                {
                    ApplicationMethod = string.IsNullOrEmpty(prod.ApplicationMethod) ? string.Empty : prod.ApplicationMethod,
                    EPANumber = epaNoTrailingChar,
                    Pest = prod.TargetPest == null ? string.Empty : prod.TargetPest.CommonName,
                    ProductName = masterlistProduct.Name,
                    RateUnit = displayRate.Unit.Name,
                    RateValue = displayRate.Value,
                    RatePerTankUnit = applicationData.ApplicationType == "ByRatePerTank" ? displayPerTank.Unit.Name : prod.RateUnit,
                    RatePerTank = applicationData.ApplicationType == "ByRatePerTank" ? displayPerTank.Value : 0.0,
                    TotalUnit = displayTotal.Unit.Name,
                    TotalValue = displayTotal.Value,
                    PHI = reiPhi != null && reiPhi.Phi != null ? reiPhi.Phi.ToString() : "*",
                    REI = reiPhi != null && reiPhi.Rei != null ? reiPhi.Rei.ToString() : "*",
                    Active = ai,
                    RateType = applicationData.ApplicationType == "ByRatePerTank" ? "Rate/Tank" : "Rate",
                    RatePer100 = prod.RatePerTankValue > 0 ? ((double)prod.RatePerTankValue / ((double)appData.TankInformation.TankSizeValue / 100)) : 0.0,
                    RatePer100Unit = displayPerTank.Unit != null ? displayPerTank.Unit.Name : string.Empty,
                    TotalArea = (decimal)appData.TotalArea.Value,
                    TotalAreaUnit = appData.TotalArea.Unit,
                    PercentApplied = prod.PercentApplied,
                    RowCount = cnt,
                };

                List<ApplicationData.AssociatedProductItem> associatedProducts = new List<ApplicationData.AssociatedProductItem>();

                foreach (var associate in prod.AssociatedProducts)
                {
                    var mlp = masterlist.GetProduct(associate.ProductId);
                    var item = new ApplicationData.AssociatedProductItem();
                    item.CustomRateType = associate.CustomRateType;
                    item.CustomRateValue = associate.CustomProductValue;
                    item.HasCost = associate.HasCost;
                    item.ProductName = associate.ProductName;
                    item.RatePerAreaUnit = UnitFactory.GetPackageSafeUnit(associate.RatePerAreaUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                    item.RatePerAreaValue = associate.RatePerAreaValue;
                    item.TotalProductUnit = UnitFactory.GetPackageSafeUnit(associate.TotalProductUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                    item.TotalProductValue = associate.TotalProductValue * (decimal)multiplier;
                    IUnit customUnit = UnitFactory.GetPackageSafeUnit(associate.CustomProductUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);

                    switch (associate.CustomRateType)
                    {
                        case "ByBag":
                            item.DisplayCustomUnit = new CompositeUnit(customUnit.AbbreviatedDisplay, 1, Strings.Unit_Bag_Text);
                            break;
                        case "ByCWT":
                            item.DisplayCustomUnit = new CompositeUnit(customUnit.AbbreviatedDisplay, 100, Strings.Unit_Weight_Text);
                            break;
                        case "ByRow":
                            item.DisplayCustomUnit = new CompositeUnit(customUnit.AbbreviatedDisplay, 1000, Strings.Unit_LengthRow_Text);
                            break;
                        case "BySeed":
                            var parentProd = masterlist.GetProduct(prod.Id);
                            item.DisplayCustomUnit = new CompositeUnit(customUnit.AbbreviatedDisplay, 1, parentProd.StdUnit);
                            break;
                        default:

                            break;
                    }

                    prodData.AssociatedProducts.Add(item);
                }

                applicationData.Products.Add(prodData);
                cnt++;
            }
            List<string> filteredMapData = new List<string>();

     
            var details = endpoint.GetView<CropZoneDetailsView>(czId).GetValue(new CropZoneDetailsView());

            var czTreeData = (from f in flattenedHierarchy.Items
                                where f.CropZoneId == czId
                                select f).FirstOrDefault();

            var reportedArea = view.CropZones.SingleOrDefault(f => f.Id == details.Id).Area;

            var fieldDetails = endpoint.GetView<FieldDetailsView>(details.FieldId).Value;
            var centerLatLon = string.IsNullOrEmpty(details.CenterLatLong) ? fieldDetails.CenterLatLong : details.CenterLatLong;

            ApplicationData.FieldLineItemData field = new ApplicationData.FieldLineItemData()
            {
                Area = reportedArea.Value,
                CenterLatLong = centerLatLon,
                Crop = masterlist.GetCropDisplay(details.CropId),
                CropZone = details.Name,
                Field = czTreeData.FieldName,
                Farm = czTreeData.FarmName,
            };
            applicationData.Fields.Add(field);

            var maps = endpoint.GetView<ItemMap>(czId);

            try
            {
                List<Feature> filteredLargeMapData = new List<Feature>();
                if (maps.HasValue)
                {
                    if (maps.Value.MostRecentMapItem != null && maps.Value.MostRecentMapItem.MapData != string.Empty && maps.Value.MostRecentMapItem.MapData != "MULTIPOLYGON EMPTY") {
                        var fieldName = flattenedHierarchy.Items.FirstOrDefault(c => c.CropZoneId == czId).FieldName;
                        //string customarealabel = GetCustomCropZoneName(czId.Id, fieldName);
                        string customareaonlylabel = GetCustomAreaLabel(czId.Id, false);
                        string customarealabel = DisplayLabelWithArea(fieldName, customareaonlylabel);
                        IDictionary<string, string> columnvalues = new Dictionary<string, string>();
                        columnvalues.Add(@"LdbLabel", fieldName);
                        columnvalues.Add(@"AreaLabel", customarealabel);
                        columnvalues.Add(@"AreaOnlyLabel", customareaonlylabel);
                        Feature newfeature = new Feature(maps.Value.MostRecentMapItem.MapData, czId.Id.ToString(), columnvalues);
                        filteredLargeMapData.Add(newfeature);

                        var detailedMap = new ConvertToBitmap(filteredLargeMapData, true, endpoint, 20.0, view.EnvironmentalZoneDistanceValue, view.EnvironmentalZoneDistanceUnit, view.Severity);
                        applicationData.MapImage = detailedMap.BitMap;
                    }
                }
            }
            finally { }

            foreach (var applicator in appData.Applicators)
            {
                ApplicationData.ApplicatorLineItemData appLineItem = new ApplicationData.ApplicatorLineItemData()
                {
                    Name = applicator.PersonName,
                    Company = applicator.CompanyName,
                    LicenseNumber = applicator.LicenseNumber,
                    ExpirationDate = applicator.Expires,
                    DisplayName = string.Format("{0} ({1})", applicator.PersonName, applicator.CompanyName),
                };

                applicationData.Applicators.Add(appLineItem);
            }

            //add all the ppe info into data....
            Task.WaitAll(ppeTask);
            foreach (var prod in ProductPPEList)
            {
                var mlp = masterlist.GetProduct(prod.ProductId);
                foreach (var ppe in prod.WorkerProtections)
                {
                    applicationData.WorkerProtectionItems.Add(new ApplicationData.WorkerProtectionItem() { ProductName = masterlist.GetProduct(prod.ProductId).Name, PPE = ppe.PPE, ReEntry = ppe.PPEReentry, SignalWord = mlp.SignalWord, RestrictedUse = mlp.RestrictedUse ? "True" : "False" });
                }
            }

            applicationData.Authorizer = appData.Authorization != null ? appData.Authorization.PersonName : string.Empty;
            applicationData.AuthorizerDate = appData.Authorization != null ? appData.Authorization.AuthorizationDate : DateTime.Now;

            applicationData.CropYear = cropyear;
            applicationData.Notes = appData.Notes;

            var report = new Landdb.Views.Secondary.Reports.Application.StandardApplication();
            report.DataSource = applicationData;
            report.Name = Strings.ReportName_Application_Text;

            return applicationData;
        }

        public string DisplayName
        {
            get { return Strings.GeneratorName_ByFIeld_Text; }
        }

        private double ReportedArea(CropZoneDetailsView czDetails)
        {
            if (czDetails.ReportedArea != null)
            {
                return (double)czDetails.ReportedArea;
            }
            else if (czDetails.ReportedArea == null && czDetails.BoundaryArea != null)
            {
                return (double)czDetails.BoundaryArea;
            }
            else if (czDetails.ReportedArea == null && czDetails.BoundaryArea == null)
            {
                return 0.0;
            }
            else
            {
                return 0.0;
            }
        }

        public List<ProductWorkerProtection> GetPPE(List<IncludedProduct> includedProducts)
        {
            List<ProductWorkerProtection> ppeList = new List<ProductWorkerProtection>();
            foreach (var prod in includedProducts)
            {
                try
                {
                    string dataText = string.Empty;

                    //create task....and wait for it to complete...
                    Task getPPE = Task.Factory.StartNew(() =>
                    {
                        dataText = GetWorkerProtectionData(prod.Id);
                    });

                    getPPE.Wait();
                    JObject data = JObject.Parse(dataText);

                    var ppe = from d in JArray.Parse(data["ProductWorkerProtections"].ToString())
                              select JArray.Parse(d["WorkerProtections"].ToString());

                    var workerProtectionList = from d in JArray.Parse(data["ProductWorkerProtections"].ToString())
                                               from i in JArray.Parse(d["WorkerProtections"].ToString())
                                               select new WorkerProtection { PPE = i["PPE"] != null ? i["PPE"].ToString() : string.Empty, PPEReentry = i["PPEReentry"] != null ? i["PPEReentry"].ToString() : string.Empty };
                    var prodWorkProtection = new ProductWorkerProtection() { ProductId = prod.Id, WorkerProtections = workerProtectionList.ToList() };
                    ppeList.Add(prodWorkProtection);
                }
                catch (Exception x)
                {

                }
            }

            return ppeList;
        }

        public string GetWorkerProtectionData(ProductId productId)
        {
            var requestUriFormat = @"{0}/workerprotections?format=json&c=en-US&u=Y29yZXkucGVya2lucw%3D%3D&p=Y29yZXlwMQ%3D%3D&ProductIds={1}";
            var requestUri = string.Format(requestUriFormat, Infrastructure.ApplicationEnvironment.MasterlistRemoteUri, productId.Id);

            WebClient client = new WebClient();
            client.Headers.Add("Authorization", "basic QWdDQVBJOlJhbmRvbU1vbmtleVN1cmZzT25MYXZhIQ=="); 
            string data = client.DownloadString(new Uri(requestUri));

            return data;
        }
    }
}
