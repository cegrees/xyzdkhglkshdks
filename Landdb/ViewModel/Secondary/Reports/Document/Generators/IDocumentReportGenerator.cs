﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.Document.Generators
{
    public interface IDocumentReportGenerator
    {
        ReportSource Generate(DocumentQueryCriteria criteria, DocumentReportCreatorViewModel dcvm);
        string DisplayName { get; }
    }
}
