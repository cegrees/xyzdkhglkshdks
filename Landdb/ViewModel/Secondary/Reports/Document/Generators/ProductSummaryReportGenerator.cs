﻿using AgC.UnitConversion;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Client.Services;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Domain.ReadModels.Plan;
using Landdb.Domain.ReadModels.WorkOrder;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ReportModels.Plan;
using Landdb.ViewModel.Overlays;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Landdb.Resources;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.Document.Generators
{
    public class ProductSummaryReportGenerator : IDocumentReportGenerator
    {
        IClientEndpoint endpoint;
        int cropyear;

        public ProductSummaryReportGenerator(IClientEndpoint endpoint, int cropyear)
        {
            this.endpoint = endpoint;
            this.cropyear = cropyear;
        }

        public Telerik.Reporting.ReportSource Generate(DocumentQueryCriteria criteria, DocumentReportCreatorViewModel dcvm)
        {
            //ScreenDescriptor progressDesc = new ScreenDescriptor("Landdb.Views.Shared.Popups.ProgressPopUpView", "fullprogress", vm);
            //Messenger.Default.Send<ShowPopupMessage>(new ShowPopupMessage() { ScreenDescriptor = progressDesc });

            Action StartGeneration;
            StartGeneration = () => ProcessData(criteria, dcvm);
            Thread t;
            t = new Thread(StartGeneration.Invoke);
            t.Start();

            return null;
        }

        void ProcessData(DocumentQueryCriteria criteria, DocumentReportCreatorViewModel dcvm)
        {
            ReportBook book = new ReportBook();
            var rs = new InstanceReportSource();

            if (criteria.IncludedDocuments.Any())
            {
                //UpdateProgressMessage(progress, "Creating Product Summary Report...");
                //UpdateProgressTotalRecords(progress, criteria.IncludedDocuments.Count());
                //UpdateProgressCurrentRecord(progress, 0);
                int cnt = 0;

                var planProducts = new List<ReportModels.Plan.ProductSummaryData>();
                var woProducts = new List<ReportModels.Plan.ProductSummaryData>();
                var appProducts = new List<ReportModels.Plan.ProductSummaryData>();
                var inventory = endpoint.GetView<InventoryListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear)).GetValue(new InventoryListView());
                var masterlistService = endpoint.GetMasterlistService();
                foreach (var doc in criteria.IncludedDocuments)
                {
                    switch (doc.DocumentType)
                    {
                        case "WorkOrder":

                            //call this for each crop zone associated with the work order
                            var woId = new WorkOrderId(doc.Identity.DataSourceId, doc.Identity.Id);
                            var woDetail = endpoint.GetView<WorkOrderView>(woId).Value;

                            foreach (var prod in woDetail.Products)
                            {
                                ////////////////////////////////////////////////////////////////////////////

                                var woProd = new ProductSummaryData();
                                woProd.ReportName = Strings.ReportName_WorkOrderProductSummary_Text;
                                woProd.ProductId = prod.Id;
                                woProd.ProductName = masterlistService.GetProductDisplay(prod.Id);
                                woProd.DocumentName = doc.Name;
                                woProd.TimingEvent = woDetail.TimingEvent + woDetail.TimingEventTag;
                                woProd.TotalProductValue = prod.TotalProductValue;
                                woProd.TotalProductUnit = UnitFactory.GetUnitByName(prod.TotalProductUnit);
                                woProd.TotalArea = (decimal)woDetail.TotalArea.Value;

                                if (inventory.Products.ContainsKey(prod.Id))
                                {
                                    try
                                    {
                                        var totalMeasure = woProd.TotalProductUnit.GetMeasure((double)prod.TotalProductValue, masterlistService.GetProduct(prod.Id).Density);
                                        var avgPriceUnit = string.IsNullOrEmpty(inventory.Products[prod.Id].AveragePriceUnit) ? null : UnitFactory.GetUnitByName(inventory.Products[prod.Id].AveragePriceUnit);
                                        var convertedTotalMeasureValue = totalMeasure.CanConvertTo(avgPriceUnit) ? totalMeasure.GetValueAs(avgPriceUnit) : totalMeasure.Value;
                                        woProd.TotalCost = (decimal)(convertedTotalMeasureValue * inventory.Products[prod.Id].AveragePriceValue);
                                        woProd.CostPerArea = woProd.TotalCost / (decimal)woDetail.TotalArea.Value;
                                    }
                                    catch (Exception ex)
                                    {
                                        woProd.TotalCost = 0m;
                                        woProd.CostPerArea = 0m;
                                    }
                                }
                                else
                                {
                                    woProd.TotalCost = 0m;
                                    woProd.CostPerArea = 0m;
                                }

                                woProducts.Add(woProd);

                            }

                            break;
                        case "Application":
                            var appId = new ApplicationId(doc.Identity.DataSourceId, doc.Identity.Id);
                            var appDetails = endpoint.GetView<ApplicationView>(appId).Value;

                            foreach (var prod in appDetails.Products)
                            {
                                var appProd = new ProductSummaryData();
                                appProd.CropYear = cropyear.ToString();
                                appProd.DataSourceName = ApplicationEnvironment.CurrentDataSourceName;
                                appProd.ReportName = Strings.ReportName_ApplicationProductSummary_Text;
                                appProd.ProductId = prod.Id;
                                appProd.ProductName = masterlistService.GetProductDisplay(prod.Id);
                                appProd.DocumentName = doc.Name;
                                appProd.TimingEvent = appDetails.TimingEvent + appDetails.TimingEventTag;
                                appProd.TotalProductValue = prod.TotalProductValue;
                                appProd.TotalProductUnit = UnitFactory.GetUnitByName(prod.TotalProductUnit);
                                appProd.TotalArea = (decimal)appDetails.TotalArea.Value;

                                if (inventory.Products.ContainsKey(prod.Id))
                                {
                                    try
                                    {
                                        var totalMeasure = appProd.TotalProductUnit.GetMeasure((double)prod.TotalProductValue, masterlistService.GetProduct(prod.Id).Density);
                                        var avgPriceUnit = string.IsNullOrEmpty(inventory.Products[prod.Id].AveragePriceUnit) ? null : UnitFactory.GetUnitByName(inventory.Products[prod.Id].AveragePriceUnit);
                                        var convertedTotalMeasureValue = totalMeasure.CanConvertTo(avgPriceUnit) ? totalMeasure.GetValueAs(avgPriceUnit) : totalMeasure.Value;
                                        appProd.TotalCost = (decimal)(convertedTotalMeasureValue * inventory.Products[prod.Id].AveragePriceValue);
                                        appProd.CostPerArea = appProd.TotalCost / (decimal)appDetails.TotalArea.Value;

                                    }
                                    catch (Exception ex)
                                    {
                                        appProd.TotalCost = 0m;
                                        appProd.CostPerArea = 0m;
                                    }
                                }
                                else
                                {
                                    appProd.TotalCost = 0m;
                                    appProd.CostPerArea = 0m;
                                }

                                appProducts.Add(appProd);

                            }

                            
                            break;
                        case "Plan":
                                var planId = new CropPlanId(doc.Identity.DataSourceId, doc.Identity.Id);
                            var planDetails = endpoint.GetView<PlanView>(planId).Value;

                            foreach (var prod in planDetails.Products)
                            {
                                var planProd = new ProductSummaryData();
                                planProd.ReportName = Strings.ReportName_PlanProductSummary_Text;
                                planProd.ProductId = prod.Id;
                                planProd.ProductName = masterlistService.GetProductDisplay(prod.Id);
                                planProd.DocumentName = doc.Name;
                                planProd.TimingEvent = prod.TimingEvent + prod.TimingEventTag;
                                planProd.TotalArea = (decimal)planDetails.EstimatedArea.Value;

                                if (inventory.Products.ContainsKey(prod.Id))
                                {
                                    try
                                    {
                                        var masterListProd = masterlistService.GetProduct(prod.Id);
                                        // calculate the total from the rate
                                        var totalProduct = prod.RateValue * (decimal)planDetails.EstimatedArea.Value * prod.PercentApplied * prod.ApplicationCount;
                                        var totalProductMeasure = UnitFactory.GetPackageSafeUnit(prod.RateUnit, masterListProd.StdUnit, masterListProd.StdFactor, masterListProd.StdPackageUnit).GetMeasure((double)totalProduct, masterListProd.Density);
                                        var totalProductUnit = UnitFactory.GetPackageSafeUnit(prod.TotalProductUnit, masterListProd.StdUnit, masterListProd.StdFactor, masterListProd.StdPackageUnit);
                                        var convertedTotalProduct = totalProductMeasure.GetValueAs(totalProductUnit);
                                        var totalMeasure = totalProductUnit.GetMeasure((double)convertedTotalProduct, masterListProd.Density);

                                        planProd.TotalProductValue = (decimal)convertedTotalProduct;
                                        planProd.TotalProductUnit = totalMeasure.Unit;

                                        var unit = string.IsNullOrEmpty(prod.SpecificCostUnit) ? null : UnitFactory.GetPackageSafeUnit(prod.SpecificCostUnit, masterListProd.StdUnit, masterListProd.StdFactor, masterListProd.StdPackageUnit);
                                        var specificPriceUnit = string.IsNullOrEmpty(prod.SpecificCostUnit) ? null : unit;
                                        var convertedTotalMeasureValue = totalMeasure.CanConvertTo(specificPriceUnit) ? totalMeasure.GetValueAs(specificPriceUnit) : totalMeasure.Value;
                                        planProd.TotalCost = prod.SpecificCostPerUnit.HasValue ? (decimal)(convertedTotalMeasureValue * (double)prod.SpecificCostPerUnit) : 0m;
                                        planProd.CostPerArea = planProd.TotalCost / (decimal)planDetails.EstimatedArea.Value;
                                    }
                                    catch (Exception ex)
                                    {
                                        planProd.TotalCost = 0m;
                                        planProd.CostPerArea = 0m;
                                    }
                                }
                                else
                                {
                                    planProd.TotalCost = 0m;
                                    planProd.CostPerArea = 0m;
                                }

                                planProducts.Add(planProd);
                            }

                            break;
                        default:
                            break;
                    }

                    cnt++;
                    //UpdateProgressCurrentRecord(progress, cnt);

                }

                ///gen reports...
                ///wo
                if (woProducts.Any())
                {
                    var brReport = new Landdb.Views.Secondary.Reports.Plan.ProductSummary();
                    brReport.DataSource = woProducts.OrderBy(x => x.ProductName);
                    brReport.Name = "";
                    book.Reports.Add(brReport);
                }

                if (appProducts.Any())
                {
                    ///app
                    var appreport = new Landdb.Views.Secondary.Reports.Plan.ProductSummary();
                    appreport.DataSource = appProducts.OrderBy(x => x.ProductName);
                    appreport.Name = "";
                    book.Reports.Add(appreport);
                }

                if (planProducts.Any())
                {
                    ///plan
                    var planReport = new Landdb.Views.Secondary.Reports.Plan.ProductSummary();
                    planReport.DataSource = planProducts.OrderBy(x => x.ProductName);
                    planReport.Name = "";
                    book.Reports.Add(planReport);
                }
            }

            //close progress
            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());

            rs.ReportDocument = book;
            dcvm.ReportSource = rs;
        }

        //void UpdateProgressMessage(IFullSynchronizationProgress progress, string message)
        //{
        //    if (progress != null)
        //    {
        //        progress.StatusMessage = message;
        //    }
        //}

        //void UpdateProgressTotalRecords(IFullSynchronizationProgress progress, int totalRecords)
        //{
        //    if (progress != null)
        //    {
        //        progress.TotalRecords = totalRecords;
        //    }
        //}

        //void UpdateProgressCurrentRecord(IFullSynchronizationProgress progress, long currentRecord)
        //{
        //    if (progress != null)
        //    {
        //        progress.CurrentRecord = (int)currentRecord;
        //    }
        //}

        public string DisplayName
        {
            get { return Strings.GeneratorName_ProductSummary_Text; }
        }
    }
}
