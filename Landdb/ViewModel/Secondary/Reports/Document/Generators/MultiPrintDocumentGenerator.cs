﻿using AgC.UnitConversion;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Client.Services;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Domain.ReadModels.Map;
using Landdb.Domain.ReadModels.ProductSetting;
using Landdb.Domain.ReadModels.Shared;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Domain.ReadModels.WorkOrder;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ReportModels.Application;
using Landdb.ReportModels.WorkOrder;
using Landdb.ViewModel.Overlays;
using Landdb.ViewModel.Secondary.Map;
using Landdb.ViewModel.Secondary.Reports.WorkOrder;
using Landdb.ViewModel.Secondary.Reports.WorkOrder.Generators;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Landdb.Resources;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.Document.Generators
{
    public class MultiPrintDocumentGenerator : IDocumentReportGenerator
    {
        IClientEndpoint endpoint;
        int cropyear;
        List<ProductWorkerProtection> ProductPPEList;
        ProgressOverlayViewModel vm;
        IProgressTracker progress;

        public MultiPrintDocumentGenerator(IClientEndpoint endpoint, int cropyear)
        {
            this.endpoint = endpoint;
            this.cropyear = cropyear;
            ProductPPEList = new List<ProductWorkerProtection>();

            vm = new ProgressOverlayViewModel();
            progress = vm;
        }


        public Telerik.Reporting.ReportSource Generate(DocumentQueryCriteria criteria, DocumentReportCreatorViewModel dcVM)
        {
            ScreenDescriptor progressDesc = new ScreenDescriptor("Landdb.Views.Shared.Popups.ProgressPopUpView", "fullprogress", vm);
            Messenger.Default.Send<ShowPopupMessage>(new ShowPopupMessage() { ScreenDescriptor = progressDesc });

            Action StartGeneration;
            StartGeneration = () => ProcessData(criteria, dcVM);
            Thread t;
            t = new Thread(StartGeneration.Invoke);
            t.Start();

            return null;
        }

        void ProcessData(DocumentQueryCriteria criteria, DocumentReportCreatorViewModel dcvm)
        {
            ReportBook book = new ReportBook();
            InstanceReportSource rs = new InstanceReportSource();

            if (criteria.IncludedDocuments.Any())
            {
                UpdateProgressMessage(progress, Strings.ProgressMessage_CreatingByFieldDocs_Text);
                UpdateProgressTotalRecords(progress, criteria.IncludedDocuments.Count());
                UpdateProgressCurrentRecord(progress, 0);
                int cnt = 0;

                foreach (var doc in criteria.IncludedDocuments)
                {
                    switch (doc.DocumentType)
                    {
                        case "WorkOrder":
                            var woService = new WorkOrderService(endpoint, new WorkOrderQueryCriteria() { SelectedWorkOrderId = new WorkOrderId(doc.Identity.DataSourceId, doc.Identity.Id) });
                            
                            var woData = woService.GenerateWorkOrderData(); //ReturnWOData(new WorkOrderId(doc.Identity.DataSourceId, doc.Identity.Id));

                            if (criteria.SpanishTranslation)
                            {
                                var spReport = new Landdb.Views.Secondary.Reports.Work_Order.SpanishWorkOrder();
                                spReport.DataSource = woData;
                                spReport.Name = woData.DatasourceName;

                                ////EXAMPLE OF HOW TO BIND TO A SUB REPORT
                                var mainReport = spReport;
                                var itemDetail = (Telerik.Reporting.SubReport)mainReport.Items.Find("subReport1", true)[0];
                                var repSource = (InstanceReportSource)itemDetail.ReportSource;
                                var subReport = (Telerik.Reporting.Report)repSource.ReportDocument;
                                var odsItems = (Telerik.Reporting.ObjectDataSource)subReport.DataSource;
                                odsItems.DataSource = woData.Products;

                                book.Reports.Add(spReport);
                            }
                            else
                            {
                                var report = new Landdb.Views.Secondary.Reports.Work_Order.LandscapeWorkOrder();
                                report.DataSource = woData;
                                report.Name = woData.DatasourceName;

                                ////EXAMPLE OF HOW TO BIND TO A SUB REPORT
                                var mainReport = report;
                                var itemDetail = (Telerik.Reporting.SubReport)mainReport.Items.Find("subReport1", true)[0];
                                var repSource = (InstanceReportSource)itemDetail.ReportSource;
                                var subReport = (Telerik.Reporting.Report)repSource.ReportDocument;
                                var odsItems = (Telerik.Reporting.ObjectDataSource)subReport.DataSource;
                                odsItems.DataSource = woData.Products;

                                book.Reports.Add(report);
                            }
                            break;
                        case "Application":
                            var appData = ReturnAppData(new ApplicationId(doc.Identity.DataSourceId, doc.Identity.Id));

                            if (criteria.SpanishTranslation)
                            {
                                var appreport = new Landdb.Views.Secondary.Reports.Application.SpanishApplication();
                                appreport.DataSource = appData;
                                appreport.Name = appData.DatasourceName;

                                ////EXAMPLE OF HOW TO BIND TO A SUB REPORT
                                var mainReport = appreport;
                                var itemDetail = (Telerik.Reporting.SubReport)mainReport.Items.Find("subReport1", true)[0];
                                var repSource = (InstanceReportSource)itemDetail.ReportSource;
                                var subReport = (Telerik.Reporting.Report)repSource.ReportDocument;
                                var odsItems = (Telerik.Reporting.ObjectDataSource)subReport.DataSource;
                                odsItems.DataSource = appData.Products;

                                book.Reports.Add(appreport);
                            }
                            else
                            {
                                var appreport = new Landdb.Views.Secondary.Reports.Application.StandardApplication();
                                appreport.DataSource = appData;
                                appreport.Name = appData.DatasourceName;

                                ////EXAMPLE OF HOW TO BIND TO A SUB REPORT
                                var mainReport = appreport;
                                var itemDetail = (Telerik.Reporting.SubReport)mainReport.Items.Find("subReport1", true)[0];
                                var repSource = (InstanceReportSource)itemDetail.ReportSource;
                                var subReport = (Telerik.Reporting.Report)repSource.ReportDocument;
                                var odsItems = (Telerik.Reporting.ObjectDataSource)subReport.DataSource;
                                odsItems.DataSource = appData.Products;

                                book.Reports.Add(appreport);
                            }

                            break;
                        default:
                            break;
                    }
                    //update progress
                    cnt++;
                    UpdateProgressCurrentRecord(progress, cnt);

                }
            }

            //close progress
            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());

            rs.ReportDocument = book;
            dcvm.ReportSource = rs;
            
        }

        void UpdateProgressMessage(IProgressTracker progress, string message)
        {
            if (progress != null)
            {
                progress.StatusMessage = message;
            }
        }

        void UpdateProgressTotalRecords(IProgressTracker progress, int totalRecords)
        {
            if (progress != null)
            {
                progress.TotalRecords = totalRecords;
            }
        }

        void UpdateProgressCurrentRecord(IProgressTracker progress, long currentRecord)
        {
            if (progress != null)
            {
                progress.CurrentRecord = (int)currentRecord;
            }
        }

        public string DisplayName
        {
            get { return Strings.GeneratorName_MultiPrint_Text; }
        }

        public WorkOrderData ReturnWOData(WorkOrderId id)
        {
            var data = new WorkOrderData();

            var workOrderData = endpoint.GetView<WorkOrderView>(id).Value;
            //var inventory = endpoint.GetView<InventoryListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropyear)).GetValue(new InventoryListView());
            var flattenedHierarchy = endpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropyear)).GetValue(new FlattenedTreeHierarchyView());
            var masterlist = endpoint.GetMasterlistService();

            if (workOrderData != null)
            {
                Task[] ppeTask = new Task[1];
                ppeTask[0] = Task.Factory.StartNew(() =>
                {
                    if (NetworkStatus.IsInternetAvailable())
                    {
                        ProductPPEList = new List<ProductWorkerProtection>();
                        ProductPPEList.AddRange(GetPPE(workOrderData.Products));
                    }
                });

                data.DatasourceName = ApplicationEnvironment.CurrentDataSourceName;
                //data.StreetAddress = 
                data.WorkOrderName = workOrderData.Name;
                data.WorkOrderDate = workOrderData.StartDateTime.ToLocalTime().ToShortDateString();
                data.WorkOrderType = workOrderData.ApplicationStrategy.ToString();
                data.TotalArea = workOrderData.CropZones.Sum(x => x.Area.Value).ToString("N2") + " " + workOrderData.CropZones.FirstOrDefault(x => x.Area.Unit != null).Area.Unit.AbbreviatedDisplay;
                
                data.TankInformationVisibility = workOrderData.TankInformation == null ? false : true;
                if (workOrderData.TankInformation != null)
                {
                    data.TankSizeValue = workOrderData.TankInformation.TankSizeValue;
                    data.TankSizeUnit = workOrderData.TankInformation.TankSizeUnit;
                    data.CarrierPerAreaValue = workOrderData.TankInformation.CarrierPerAreaValue;
                    data.CarrierPerAreaUnit = workOrderData.TankInformation.CarrierPerAreaUnit;
                    data.TotalCarrierValue = workOrderData.TankInformation.TotalCarrierValue;
                    data.TotalCarrierUnit = workOrderData.TankInformation.TotalCarrierUnit;
                    data.TankCount = workOrderData.TankInformation.TankCount;
                }

                int cnt = 0;
                foreach (var prod in workOrderData.Products)
                {
                    var masterlistProduct = masterlist.GetProduct(prod.Id);
                    ReiPhi reiPhi = new ReiPhi();
                    List<ReiPhi> reiPhis = new List<ReiPhi>();
                    reiPhis.AddRange(masterlist.GetProductReiPhis(prod.Id));
                    //////////////////////////////////////////////////////////
                    List<CropId> cropIds = new List<CropId>();
                    var czCropIds = from m in workOrderData.CropZones
                                    select endpoint.GetView<CropZoneDetailsView>(m.Id).Value.CropId;
                    cropIds.AddRange(czCropIds);

                    var parentCropIds = (from cid in cropIds
                                         let mlCrop = masterlist.GetCrop(cid)
                                         where mlCrop.ParentId.HasValue
                                         select new CropId(mlCrop.ParentId.Value)).ToList();
                    cropIds.AddRange(parentCropIds);

                    var prodSettings = endpoint.GetView<ProductSettingsView>(new ProductSettingId(ApplicationEnvironment.CurrentDataSourceId, prod.Id));
                    var cropYearItem = prodSettings.HasValue && prodSettings.Value.ItemsByYear.ContainsKey(ApplicationEnvironment.CurrentCropYear) ? prodSettings.Value.ItemsByYear[ApplicationEnvironment.CurrentCropYear] : null;

                    List<ProductSettingItem> cropItems = new List<ProductSettingItem>();
                    if (cropYearItem != null)
                    {
                        cropItems = (from c in cropYearItem
                                     where cropIds.Contains(c.CropId)
                                     select c).ToList();
                    }

                    List<ReiPhi> newReiPhis = new List<ReiPhi>();
                    foreach (var reiphi in reiPhis)
                    {
                        if (cropIds.Any(c => c.Id == reiphi.Crop))
                        {
                            newReiPhis.Add(reiphi);
                        }
                    }

                    foreach (var item in cropItems)
                    {
                        var rP = (from r in newReiPhis
                                  where r.Crop == item.CropId.Id
                                  select r).SingleOrDefault();
                        newReiPhis.Remove(rP);
                        newReiPhis.Add(new ReiPhi() { Crop = item.CropId.Id, Phi = (double)item.PhiValue.Value, PhiU = item.PhiUnit, Rei = (double)item.ReiValue.Value, ReiU = item.ReiUnit });
                    }
                    /////////////////////////////////////////////////////////
                    var therei = newReiPhis != null && newReiPhis.Count() > 0 ? (double)(from r in newReiPhis select r.Rei).Max() : new double?();
                    var thephi = newReiPhis != null && newReiPhis.Count() > 0 ? (double)(from r in newReiPhis select r.Phi).Max() : new double?();
                    reiPhi.Rei = reiPhi.Rei == null || reiPhi.Rei < therei ? therei : reiPhi.Rei;
                    reiPhi.Phi = reiPhi.Phi == null || reiPhi.Phi < thephi ? thephi : reiPhi.Phi;

                    var aiList = from i in masterlistProduct.ActiveIngredients
                                 select masterlist.GetActiveIngredientDisplay(new ActiveIngredientId(i.Id));

                    string ai = string.Join(",", aiList.ToArray());

                    var registrationNumber = masterlistProduct.RegistrationNumber;
                    string epaNoTrailingChar = string.Empty;
                    if (registrationNumber == null) { epaNoTrailingChar = string.Empty; }
                    else
                    {
                        Regex epaNumberRegex = new Regex("([1-9][0-9]*)");

                        var matches = from m in epaNumberRegex.Matches(registrationNumber).Cast<Match>()
                                      select m.Value;
                        epaNoTrailingChar = string.Join("-", matches.ToArray());
                    }

                    var displayRate = DisplayMeasureConverter.ConvertToRateDisplayMeasure(prod.Id, prod.RateValue, prod.RateUnit, masterlistProduct.Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);
                    var displayTotal = DisplayMeasureConverter.ConvertToTotalDisplayMeasure(prod.Id, (decimal)prod.TotalProductValue, prod.TotalProductUnit, masterlistProduct.Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);
                    var perTankMeasure = UnitFactory.GetUnitByName(prod.RatePerTankUnit).GetMeasure((double)prod.RatePerTankValue, masterlistProduct.Density);
                    var displayPerTank = DisplayMeasureConverter.ConvertToRatePerTankDisplayMeasure(prod.Id, perTankMeasure, endpoint, ApplicationEnvironment.CurrentDataSourceId);
                    WorkOrderData.ProductLineItemData prodData = new WorkOrderData.ProductLineItemData()
                    {
                        ApplicationMethod = string.IsNullOrEmpty(prod.ApplicationMethod) ? string.Empty : prod.ApplicationMethod,
                        EPANumber = epaNoTrailingChar,
                        Pest = prod.TargetPest == null ? string.Empty : prod.TargetPest.CommonName,
                        ProductName = masterlistProduct.Name,
                        RateUnit = displayRate.Unit.Name,
                        RateValue = displayRate.Value,
                        RatePerTankUnit = data.WorkOrderType == "ByRatePerTank" ? displayPerTank.Unit.Name : prod.RateUnit,
                        RatePerTank = data.WorkOrderType == "ByRatePerTank" ? displayPerTank.Value : 0.0,
                        TotalUnit = displayTotal.Unit.Name,
                        TotalValue = displayTotal.Value,
                        PHI = reiPhi != null && reiPhi.Phi != null ? reiPhi.Phi.ToString() : "*",
                        REI = reiPhi != null && reiPhi.Rei != null ? reiPhi.Rei.ToString() : "*",
                        Active = ai,
                        RateType = data.WorkOrderType == "ByRatePerTank" ? Strings.RateTank_Text : Strings.RateType_Text,
                        RatePer100 = workOrderData.TankInformation != null ? (double)prod.RatePerTankValue / ((double)workOrderData.TankInformation.TankSizeValue / 100) : 0,
                        RatePer100Unit = displayPerTank.Unit != null ? displayPerTank.Unit.Name : string.Empty,
                        PercentApplied = prod.PercentApplied,
                        TotalArea = (decimal)workOrderData.TotalArea.Value,
                        TotalAreaUnit = workOrderData.TotalArea.Unit,
                        RowCount = cnt,
                    };

                    List<WorkOrderData.AssociatedProductItem> associatedProducts = new List<WorkOrderData.AssociatedProductItem>();

                    foreach (var associate in prod.AssociatedProducts)
                    {
                        var mlp = masterlist.GetProduct(associate.ProductId);
                        var item = new WorkOrderData.AssociatedProductItem();
                        item.CustomRateType = associate.CustomRateType;
                        item.CustomRateValue = associate.CustomProductValue;
                        item.HasCost = associate.HasCost;
                        item.ProductName = associate.ProductName;
                        item.RatePerAreaUnit = UnitFactory.GetPackageSafeUnit(associate.RatePerAreaUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                        item.RatePerAreaValue = associate.RatePerAreaValue;
                        item.TotalProductUnit = UnitFactory.GetPackageSafeUnit(associate.TotalProductUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                        item.TotalProductValue = associate.TotalProductValue;
                        IUnit customUnit = UnitFactory.GetPackageSafeUnit(associate.CustomProductUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);

                        switch (associate.CustomRateType)
                        {
                            case "ByBag":
                                item.DisplayCustomUnit = new CompositeUnit(customUnit.AbbreviatedDisplay, 1, Strings.Unit_Bag_Text);
                                break;
                            case "ByCWT":
                                item.DisplayCustomUnit = new CompositeUnit(customUnit.AbbreviatedDisplay, 100, Strings.Unit_Weight_Text);
                                break;
                            case "ByRow":
                                item.DisplayCustomUnit = new CompositeUnit(customUnit.AbbreviatedDisplay, 1000, Strings.Unit_LengthRow_Text);
                                break;
                            case "BySeed":
                                var parentProd = masterlist.GetProduct(prod.Id);
                                item.DisplayCustomUnit = new CompositeUnit(customUnit.AbbreviatedDisplay, 1, parentProd.StdUnit);
                                break;
                            default:

                                break;
                        }

                        prodData.AssociatedProducts.Add(item);
                    }

                    data.Products.Add(prodData);
                    cnt++;
                }

                List<string> filteredMapData = new List<string>();

                foreach (var cz in workOrderData.CropZones)
                {
                    var czDetails = endpoint.GetView<CropZoneDetailsView>(cz.Id).GetValue(new CropZoneDetailsView());

                    var czTreeData = (from f in flattenedHierarchy.Items
                                      where f.CropZoneId == cz.Id
                                      select f).FirstOrDefault();

                    //var reportedArea = ReportedArea(czDetails);
                    var area = workOrderData.CropZones.SingleOrDefault(x => x.Id == cz.Id).Area;

                    var fieldDetails = endpoint.GetView<FieldDetailsView>(czDetails.FieldId).Value;
                    var centerLatLon = string.IsNullOrEmpty(czDetails.CenterLatLong) ? fieldDetails.CenterLatLong : czDetails.CenterLatLong;

                    WorkOrderData.FieldLineItemData field = new WorkOrderData.FieldLineItemData()
                    {
                        Area = area.Value,
                        CenterLatLong = centerLatLon,
                        Crop = masterlist.GetCropDisplay(czDetails.CropId),
                        CropZone = czDetails.Name,
                        Field = czTreeData != null ? czTreeData.FieldName : fieldDetails.FieldNameByCropYear[cropyear],//.Name,
                        Farm = czTreeData != null ? czTreeData.FarmName : Strings.UnknownFarm_Text,
                    };

                    data.Fields.Add(field);

                    var maps = endpoint.GetView<ItemMap>(cz.Id);
                    try
                    {
                        if (maps.HasValue)
                        {
                            if (maps.Value.MostRecentMapItem != null && maps.Value.MostRecentMapItem.MapData != string.Empty && maps.Value.MostRecentMapItem.MapData != "MULTIPOLYGON EMPTY") {
                                filteredMapData.Add(maps.Value.MostRecentMapItem.MapData);
                            }
                        }
                    }
                    finally { }
                }

                var converter = new ConvertToBitmap(filteredMapData, this.endpoint, workOrderData.EnvironmentalZoneDistanceValue.HasValue ? workOrderData.EnvironmentalZoneDistanceValue.Value :  0m, workOrderData.EnvironmentalZoneDistanceUnit, workOrderData.Severity);
                data.MapImage = converter.BitMap;

                //data.MapLocation = mapFile;

                foreach (var applicator in workOrderData.Applicators)
                {
                    WorkOrderData.ApplicatorLineItemData appLineItem = new WorkOrderData.ApplicatorLineItemData()
                    {
                        Name = applicator.PersonName,
                        Company = applicator.CompanyName,
                        LicenseNumber = applicator.LicenseNumber,
                        ExpirationDate = applicator.Expires,
                        DisplayName = string.Format("{0} ({1})", applicator.PersonName, applicator.CompanyName),
                    };

                    data.Applicators.Add(appLineItem);
                }

                //add all the ppe info into data....
                Task.WaitAll(ppeTask);
                foreach (var prod in ProductPPEList)
                {
                    var mlp = masterlist.GetProduct(prod.ProductId);
                    foreach (var ppe in prod.WorkerProtections)
                    {
                        data.WorkerProtectionItems.Add(new WorkOrderData.WorkerProtectionItem() { ProductName = masterlist.GetProduct(prod.ProductId).Name, PPE = ppe.PPE, ReEntry = ppe.PPEReentry, SignalWord = mlp.SignalWord, RestrictedUse = mlp.RestrictedUse ? "True" : "False" });
                    }
                }

                data.Authorizer = workOrderData.Authorization != null ? workOrderData.Authorization.PersonName : string.Empty;
                data.AuthorizerDate = workOrderData.Authorization != null ? workOrderData.Authorization.AuthorizationDate : DateTime.Now;

                data.CropYear = cropyear;
                data.Notes = workOrderData.Notes;
            }

            return data;

        }

        public ApplicationData ReturnAppData(ApplicationId id)
        {
            var data = new ApplicationData();
            var appData = endpoint.GetView<ApplicationView>(id).Value;
            //var inventory = endpoint.GetView<InventoryListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropyear)).GetValue(new InventoryListView());
            var flattenedHierarchy = endpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropyear)).GetValue(new FlattenedTreeHierarchyView());
            var masterlist = endpoint.GetMasterlistService();

            Task[] ppeTask = new Task[1];
            ppeTask[0] = Task.Factory.StartNew(() =>
            {
                if (NetworkStatus.IsInternetAvailable())
                {
                    ProductPPEList = new List<ProductWorkerProtection>();
                    ProductPPEList.AddRange(GetPPE(appData.Products));
                }
            });

            data.DatasourceName = ApplicationEnvironment.CurrentDataSourceName;
            data.ApplicationName = appData.Name;
            data.ApplicationDate = appData.StartDateTime.ToShortDateString();
            data.ApplicationType = appData.ProductStrategy.ToString();
            data.TotalArea = appData.CropZones.Sum(x => x.Area.Value).ToString("N2") + " " + appData.CropZones.FirstOrDefault(x => x.Area.Unit != null).Area.Unit.AbbreviatedDisplay;
            data.StartDate = appData.StartDateTime.ToLocalTime();
            data.EndDate = appData.CustomEndDate != null ? ((DateTime)appData.CustomEndDate).ToLocalTime() : appData.StartDateTime.ToLocalTime().AddHours(1.0);

            if (appData.TankInformation != null)
            {
                data.CarrierPerAreaValue = appData.TankInformation.CarrierPerAreaValue;
                data.CarrierPerAreaUnit = appData.TankInformation.CarrierPerAreaUnit;
                data.TankCount = appData.TankInformation.TankCount;
                data.TankSizeValue = appData.TankInformation.TankSizeValue;
                data.TankSizeUnit = appData.TankInformation.TankSizeUnit;
                data.TotalCarrierValue = appData.TankInformation.TotalCarrierValue;
                data.TotalCarrierUnit = appData.TankInformation.TotalCarrierUnit;
            }

            if (appData.Conditions != null)
            {
                data.Temperature = appData.Conditions.TemperatureValue;
                data.Humidity = appData.Conditions.HumidityPercent;
                data.WindDirection = appData.Conditions.WindDirection;
                data.WindSpeed = appData.Conditions.WindSpeedValue;
                data.SkyCondition = appData.Conditions.SkyCondition == "-1" ? string.Empty : appData.Conditions.SkyCondition;
                data.SoilCondition = appData.Conditions.SoilMoisture == "-1" ? string.Empty : appData.Conditions.SoilMoisture;
            }

            var cnt = 0;
            foreach (var prod in appData.Products)
            {
                var masterlistProduct = masterlist.GetProduct(prod.Id);
                ReiPhi reiPhi = new ReiPhi();
                List<ReiPhi> reiPhis = new List<ReiPhi>();
                reiPhis.AddRange(masterlist.GetProductReiPhis(prod.Id));
                //////////////////////////////////////////////////////////
                List<CropId> cropIds = new List<CropId>();
                var czCropIds = from m in appData.CropZones
                                select endpoint.GetView<CropZoneDetailsView>(m.Id).Value.CropId;
                cropIds.AddRange(czCropIds);

                var parentCropIds = (from cid in cropIds
                                     let mlCrop = masterlist.GetCrop(cid)
                                     where mlCrop.ParentId.HasValue
                                     select new CropId(mlCrop.ParentId.Value)).ToList();
                cropIds.AddRange(parentCropIds);

                var prodSettings = endpoint.GetView<ProductSettingsView>(new ProductSettingId(ApplicationEnvironment.CurrentDataSourceId, prod.Id));
                var cropYearItem = prodSettings.HasValue && prodSettings.Value.ItemsByYear.ContainsKey(ApplicationEnvironment.CurrentCropYear) ? prodSettings.Value.ItemsByYear[ApplicationEnvironment.CurrentCropYear] : null;

                List<ProductSettingItem> cropItems = new List<ProductSettingItem>();
                if (cropYearItem != null)
                {
                    cropItems = (from c in cropYearItem
                                 where cropIds.Contains(c.CropId)
                                 select c).ToList();
                }

                List<ReiPhi> newReiPhis = new List<ReiPhi>();
                foreach (var reiphi in reiPhis)
                {
                    if (cropIds.Any(c => c.Id == reiphi.Crop))
                    {
                        newReiPhis.Add(reiphi);
                    }
                }

                foreach (var item in cropItems)
                {
                    var rP = (from r in newReiPhis
                              where r.Crop == item.CropId.Id
                              select r).SingleOrDefault();
                    newReiPhis.Remove(rP);
                    newReiPhis.Add(new ReiPhi() { Crop = item.CropId.Id, Phi = (double)item.PhiValue.Value, PhiU = item.PhiUnit, Rei = (double)item.ReiValue.Value, ReiU = item.ReiUnit });
                }
                /////////////////////////////////////////////////////////
                var therei = newReiPhis != null && newReiPhis.Count() > 0 ? (double)(from r in newReiPhis select r.Rei).Max() : new double?();
                var thephi = newReiPhis != null && newReiPhis.Count() > 0 ? (double)(from r in newReiPhis select r.Phi).Max() : new double?();
                reiPhi.Rei = reiPhi.Rei == null || reiPhi.Rei < therei ? therei : reiPhi.Rei;
                reiPhi.Phi = reiPhi.Phi == null || reiPhi.Phi < thephi ? thephi : reiPhi.Phi;

                var aiList = from i in masterlistProduct.ActiveIngredients
                             select masterlist.GetActiveIngredientDisplay(new ActiveIngredientId(i.Id));

                string ai = string.Join(",", aiList.ToArray());

                var registrationNumber = masterlistProduct.RegistrationNumber;
                string epaNoTrailingChar = string.Empty;
                if (registrationNumber == null) { epaNoTrailingChar = string.Empty; }
                else
                {
                    Regex epaNumberRegex = new Regex("([1-9][0-9]*)");

                    var matches = from m in epaNumberRegex.Matches(registrationNumber).Cast<Match>()
                                  select m.Value;
                    epaNoTrailingChar = string.Join("-", matches.ToArray());
                }

                var displayRate = DisplayMeasureConverter.ConvertToRateDisplayMeasure(prod.Id, prod.RateValue, prod.RateUnit, masterlistProduct.Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);
                var displayTotal = DisplayMeasureConverter.ConvertToTotalDisplayMeasure(prod.Id, (decimal)prod.TotalProductValue, prod.TotalProductUnit, masterlistProduct.Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);
                var perTankMeasure = UnitFactory.GetUnitByName(prod.RatePerTankUnit).GetMeasure((double)prod.RatePerTankValue, masterlistProduct.Density);
                var displayPerTank = DisplayMeasureConverter.ConvertToRatePerTankDisplayMeasure(prod.Id, perTankMeasure, endpoint, ApplicationEnvironment.CurrentDataSourceId);

                ApplicationData.ProductLineItemData prodData = new ApplicationData.ProductLineItemData()
                {
                    ApplicationMethod = string.IsNullOrEmpty(prod.ApplicationMethod) ? string.Empty : prod.ApplicationMethod,
                    EPANumber = epaNoTrailingChar,
                    Pest = prod.TargetPest == null ? string.Empty : prod.TargetPest.CommonName,
                    ProductName = masterlistProduct.Name,
                    RateUnit = displayRate.Unit.Name,
                    RateValue = displayRate.Value,
                    RatePerTankUnit = data.ApplicationType == "ByRatePerTank" ? displayPerTank.Unit.Name : prod.RateUnit,
                    RatePerTank = data.ApplicationType == "ByRatePerTank" ? displayPerTank.Value : 0.0,
                    TotalUnit = displayTotal.Unit.Name,
                    TotalValue = displayTotal.Value,
                    PHI = reiPhi != null && reiPhi.Phi != null ? reiPhi.Phi.ToString() : "*",
                    REI = reiPhi != null && reiPhi.Rei != null ? reiPhi.Rei.ToString() : "*",
                    Active = ai,
                    RateType = data.ApplicationType == "ByRatePerTank" ? Strings.RateTank_Text : Strings.RateType_Text,
                    RatePer100 = prod.RatePerTankValue > 0 ? ((double)prod.RatePerTankValue / ((double)appData.TankInformation.TankSizeValue / 100)) : 0.0,
                    RatePer100Unit = displayPerTank.Unit != null ? displayPerTank.Unit.Name : string.Empty,
                    TotalArea = (decimal)appData.TotalArea.Value,
                    TotalAreaUnit = appData.TotalArea.Unit,
                    PercentApplied = prod.PercentApplied,
                    DefaultAreaUnit = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).AbbreviatedDisplay,
                    RowCount = cnt,
                };

                List<ApplicationData.AssociatedProductItem> associatedProducts = new List<ApplicationData.AssociatedProductItem>();

                foreach (var associate in prod.AssociatedProducts)
                {
                    var mlp = masterlist.GetProduct(associate.ProductId);
                    var item = new ApplicationData.AssociatedProductItem();
                    item.CustomRateType = associate.CustomRateType;
                    item.CustomRateValue = associate.CustomProductValue;
                    item.HasCost = associate.HasCost;
                    item.ProductName = associate.ProductName;
                    item.RatePerAreaUnit = UnitFactory.GetPackageSafeUnit(associate.RatePerAreaUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                    item.RatePerAreaValue = associate.RatePerAreaValue;
                    item.TotalProductUnit = UnitFactory.GetPackageSafeUnit(associate.TotalProductUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                    item.TotalProductValue = associate.TotalProductValue;
                    IUnit customUnit = UnitFactory.GetPackageSafeUnit(associate.CustomProductUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);

                    switch (associate.CustomRateType)
                    {
                        case "ByBag":
                            item.DisplayCustomUnit = new CompositeUnit(customUnit.AbbreviatedDisplay, 1, Strings.Unit_Bag_Text);
                            break;
                        case "ByCWT":
                            item.DisplayCustomUnit = new CompositeUnit(customUnit.AbbreviatedDisplay, 100, Strings.Unit_Weight_Text);
                            break;
                        case "ByRow":
                            item.DisplayCustomUnit = new CompositeUnit(customUnit.AbbreviatedDisplay, 1000, Strings.Unit_LengthRow_Text);
                            break;
                        case "BySeed":
                            var parentProd = masterlist.GetProduct(prod.Id);
                            item.DisplayCustomUnit = new CompositeUnit(customUnit.AbbreviatedDisplay, 1, parentProd.StdUnit);
                            break;
                        default:

                            break;
                    }

                    prodData.AssociatedProducts.Add(item);
                }

                data.Products.Add(prodData);
                cnt++;
            }
            List<string> filteredMapData = new List<string>();

            foreach (var cz in appData.CropZones)
            {
                var czDetails = endpoint.GetView<CropZoneDetailsView>(cz.Id).GetValue(new CropZoneDetailsView());

                var czTreeData = (from f in flattenedHierarchy.Items
                                  where f.CropZoneId == cz.Id
                                  select f).FirstOrDefault();

                var area = appData.CropZones.SingleOrDefault(f => f.Id == cz.Id).Area.Value;

                var fieldDetails = endpoint.GetView<FieldDetailsView>(czDetails.FieldId).Value;
                var centerLatLon = string.IsNullOrEmpty(czDetails.CenterLatLong) ? fieldDetails.CenterLatLong : czDetails.CenterLatLong;

                ApplicationData.FieldLineItemData field = new ApplicationData.FieldLineItemData()
                {
                    Area = area,
                    CenterLatLong = centerLatLon,
                    Crop = masterlist.GetCropDisplay(czDetails.CropId),
                    CropZone = czDetails.Name,
                    Field = czTreeData.FieldName,
                    Farm = czTreeData.FarmName,
                };
                data.Fields.Add(field);
                var maps = endpoint.GetView<ItemMap>(cz.Id);
                try
                {
                    if (maps.HasValue)
                    {
                        if (maps.Value.MostRecentMapItem != null && maps.Value.MostRecentMapItem.MapData != string.Empty && maps.Value.MostRecentMapItem.MapData != "MULTIPOLYGON EMPTY") {
                            filteredMapData.Add(maps.Value.MostRecentMapItem.MapData);
                        }
                    }
                }
                finally { }
            }

            var converter = new ConvertToBitmap(filteredMapData, this.endpoint, appData.EnvironmentalZoneDistanceValue.HasValue ? appData.EnvironmentalZoneDistanceValue.Value : 0m, appData.EnvironmentalZoneDistanceUnit, appData.Severity);
            data.MapImage = converter.BitMap;

            foreach (var applicator in appData.Applicators)
            {
                ApplicationData.ApplicatorLineItemData appLineItem = new ApplicationData.ApplicatorLineItemData()
                {
                    Name = applicator.PersonName,
                    Company = applicator.CompanyName,
                    LicenseNumber = applicator.LicenseNumber,
                    ExpirationDate = applicator.Expires,
                    DisplayName = string.Format("{0} ({1})", applicator.PersonName, applicator.CompanyName),
                };

                data.Applicators.Add(appLineItem);
            }

            //add all the ppe info into data....
            Task.WaitAll(ppeTask);
            foreach (var prod in ProductPPEList)
            {
                var mlp = masterlist.GetProduct(prod.ProductId);
                foreach (var ppe in prod.WorkerProtections)
                {
                    data.WorkerProtectionItems.Add(new ApplicationData.WorkerProtectionItem() { ProductName = masterlist.GetProduct(prod.ProductId).Name, PPE = ppe.PPE, ReEntry = ppe.PPEReentry, SignalWord = mlp.SignalWord, RestrictedUse = mlp.RestrictedUse ? "True" : "False" });
                }
            }

            data.Authorizer = appData.Authorization != null ? appData.Authorization.PersonName : string.Empty;
            data.AuthorizerDate = appData.Authorization != null ? appData.Authorization.AuthorizationDate : DateTime.Now;

            data.CropYear = cropyear;
            data.Notes = appData.Notes;

            var report = new Landdb.Views.Secondary.Reports.Application.StandardApplication();
            report.DataSource = data;
            report.Name = Strings.ReportName_Application_Text;

            return data;
        }

        public List<ProductWorkerProtection> GetPPE(List<IncludedProduct> includedProducts)
        {
            List<ProductWorkerProtection> ppeList = new List<ProductWorkerProtection>();
            foreach (var prod in includedProducts)
            {
                try
                {
                    string dataText = string.Empty;

                    //create task....and wait for it to complete...
                    Task getPPE = Task.Factory.StartNew(() =>
                    {
                        dataText = GetWorkerProtectionData(prod.Id);
                    });

                    getPPE.Wait();
                    JObject data = JObject.Parse(dataText);

                    var ppe = from d in JArray.Parse(data["ProductWorkerProtections"].ToString())
                              select JArray.Parse(d["WorkerProtections"].ToString());

                    var workerProtectionList = from d in JArray.Parse(data["ProductWorkerProtections"].ToString())
                                               from i in JArray.Parse(d["WorkerProtections"].ToString())
                                               select new WorkerProtection { PPE = i["PPE"] != null ? i["PPE"].ToString() : string.Empty, PPEReentry = i["PPEReentry"] != null ? i["PPEReentry"].ToString() : string.Empty };
                    var prodWorkProtection = new ProductWorkerProtection() { ProductId = prod.Id, WorkerProtections = workerProtectionList.ToList() };
                    ppeList.Add(prodWorkProtection);
                }
                catch (Exception x)
                {

                }

            }

            return ppeList;
        }

        public string GetWorkerProtectionData(ProductId productId)
        {
            var requestUriFormat = @"{0}/workerprotections?format=json&c=en-US&u=Y29yZXkucGVya2lucw%3D%3D&p=Y29yZXlwMQ%3D%3D&ProductIds={1}";
            var requestUri = string.Format(requestUriFormat, Infrastructure.ApplicationEnvironment.MasterlistRemoteUri, productId.Id);

            WebClient client = new WebClient();
            client.Headers.Add("Authorization", "basic QWdDQVBJOlJhbmRvbU1vbmtleVN1cmZzT25MYXZhIQ=="); 
            string data = client.DownloadString(new Uri(requestUri));

            return data;
        }

        private double ReportedArea(CropZoneDetailsView czDetails)
        {
            if (czDetails.ReportedArea != null)
            {
                return (double)czDetails.ReportedArea;
            }
            else if (czDetails.ReportedArea == null && czDetails.BoundaryArea != null)
            {
                return (double)czDetails.BoundaryArea;
            }
            else if (czDetails.ReportedArea == null && czDetails.BoundaryArea == null)
            {
                return 0.0;
            }
            else
            {
                return 0.0;
            }
        }

    }

    public class ProductWorkerProtection
    {
        public ProductId ProductId { get; set; }
        public List<WorkerProtection> WorkerProtections { get; set; }
        //public string PPE { get; set; }
        //public string PPEReEntry { get; set; }
    }

    public class WorkerProtection
    {
        public string PPE { get; set; }
        public string PPEReentry { get; set; }
    }
}
