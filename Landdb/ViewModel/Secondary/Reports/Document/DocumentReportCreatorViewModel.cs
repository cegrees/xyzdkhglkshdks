﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Converters.Reports;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Reports;
using Landdb.ViewModel.Secondary.Reports.Document.Generators;
using Microsoft.Win32;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using Telerik.Reporting;
using Telerik.Reporting.Processing;

namespace Landdb.ViewModel.Secondary.Reports.Document
{
    public class DocumentReportCreatorViewModel : ViewModelBase
    {
        ReportTemplateViewModelToFile templateService = new ReportTemplateViewModelToFile();
        IDocumentReportGenerator selectedReportGenerator;
        DocumentQueryCriteria criteria;
        ReportSource source;
        bool viewerSelected = false;
        DocumentReportInfoViewModel infoPageModel;
        DocumentReportSelectionViewModel selectionPageModel;

        public DocumentReportCreatorViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, int cropYear, ReportTemplateViewModel template, string reportType)
        {
            CancelCommand = new RelayCommand(OnCancelReport);
            SaveCommand = new RelayCommand(SaveTemplate);
            PDFExport = new RelayCommand(PDF);

            InfoPageModel = new DocumentReportInfoViewModel(clientEndpoint, cropYear);
            SelectionPageModel = new DocumentReportSelectionViewModel(clientEndpoint, cropYear);

            ReportGenerators = new ObservableCollection<IDocumentReportGenerator>();
            AddDocumentReportGenerators(clientEndpoint, cropYear);
            SelectedPageIndex = 0;
        }

        public ICommand CancelCommand { get; private set; }
        public RelayCommand PDFExport { get; set; }
        public ICommand SaveCommand { get; set; }
        public string TemplateName { get; set; }
        public int SelectedPageIndex { get; set; }
        public int CropYear { get { return ApplicationEnvironment.CurrentCropYear; } }

        public ReportSource ReportSource
        {
            get
            {
                return source;
            }
            set
            {
                source = value;
                RaisePropertyChanged("ReportSource");
            }
        }

        public ObservableCollection<IDocumentReportGenerator> ReportGenerators { get; private set; }
        public IDocumentReportGenerator SelectedReportGenerator
        {
            get { return selectedReportGenerator; }
            set
            {
                selectedReportGenerator = value;
                UpdateReport();
                RaisePropertyChanged("SelectedReportGenerator");
            }
        }

        public bool OnViewerSelected 
        {
            get { return viewerSelected; }
            set { viewerSelected = value; if (viewerSelected) { SelectedReportGenerator = null; } }
        
        }

        DocumentQueryCriteria BuildCriteria()
        {
            DocumentQueryCriteria criteria = new DocumentQueryCriteria();

            criteria.StartDatetime = InfoPageModel.ReportStartDate.AddHours(InfoPageModel.ReportStartDate.Hour * -1);
            criteria.EndDateTime = InfoPageModel.ReportEndDate.AddDays(1);
            criteria.IncludedDocuments = (from d in SelectionPageModel.ConcurrentDocuments
                                         where d.Checked == true
                                         select d.Descriptor).ToList();
            criteria.SpanishTranslation = InfoPageModel.SpanishTranslated;
            return criteria;
        }

        public DocumentReportInfoViewModel InfoPageModel { get { return infoPageModel; } set { infoPageModel = value; SelectedReportGenerator = null; } }
        public DocumentReportSelectionViewModel SelectionPageModel { get { return selectionPageModel; } set { selectionPageModel = value; SelectedReportGenerator = null; } }

        public void UpdateReport()
        {
            if (SelectedReportGenerator != null)
            {
                criteria = BuildCriteria();

                ReportSource = SelectedReportGenerator.Generate(criteria, this);
                RaisePropertyChanged("ReportSource");
            }
        }

        void AddDocumentReportGenerators(IClientEndpoint clientEndpoint, int cropYear)
        {
            ReportGenerators.Add(new MultiPrintDocumentGenerator(clientEndpoint, cropYear));
            ReportGenerators.Add(new ByFieldDocumentGenerator(clientEndpoint, cropYear));
            ReportGenerators.Add(new BreakOutDocumentGenerator(clientEndpoint, cropYear));
            ReportGenerators.Add(new ProductSummaryReportGenerator(clientEndpoint, cropYear));
        }

        void OnCancelReport()
        {
            Messenger.Default.Send<ShowMainViewMessage>(new ShowMainViewMessage());
        }

        public void PDF()
        {
            try { 
            if (ReportSource == null) return;
            ReportSource reportExport = ReportSource;

            ReportProcessor reportProcessor = new ReportProcessor();
            System.Collections.Hashtable deviceInfo =
            new System.Collections.Hashtable();
            RenderingResult result = reportProcessor.RenderReport("PDF", reportExport, deviceInfo);

            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "Adobe Reader|*.pdf";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;

            Nullable<bool> resultSaved = saveFileDialog1.ShowDialog();

            // Process save file dialog box results 
            if (resultSaved == true)
            {
                // Save document 
                string filename = saveFileDialog1.FileName;

                try
                {
                    using (System.IO.FileStream fs = new System.IO.FileStream(filename, System.IO.FileMode.Create))
                    {
                        fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
                    }

                    System.Diagnostics.Process.Start(filename);
                }
                catch (Exception ex) { }

            }
            }
            catch (Exception ex)
            {
                //do nothing
            }
        }

        void SaveTemplate()
        {
            //criteria = BuildCriteria();
            //if (template != null && template.TemplateName == TemplateName)
            //{
            //    ReportTemplateViewModel editedTemplate = new ReportTemplateViewModel()
            //    {
            //        TemplateName = TemplateName,
            //        Created = template.Created,
            //        LastUsed = DateTime.Now,
            //        CriteriaJSON = JsonConvert.SerializeObject(criteria),
            //        ReportType = template.ReportType,
            //        DataSourceId = template.DataSourceId
            //    };

            //    //TODO :: CALL TO EDIT PREVIOUS TEMPLATE...
            //    templateService.EditTemplate(editedTemplate);
            //}
            //else
            //{
            //    ReportTemplateViewModel newTemplate = new ReportTemplateViewModel()
            //    {
            //        TemplateName = TemplateName,
            //        Created = DateTime.Now,
            //        LastUsed = DateTime.Now,
            //        CriteriaJSON = JsonConvert.SerializeObject(criteria),
            //        ReportType = reportType,
            //        DataSourceId = ApplicationEnvironment.CurrentDataSourceId
            //    };

            //    //TODO :: call service to save the new report template
            //    templateService.SaveNewTemplate(newTemplate);
            //}

            //RaisePropertyChanged("NewTemplateCreated", null, new ReportTemplateViewModel(), true);
        }
    }
}
