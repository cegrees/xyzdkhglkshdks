﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Secondary.Reports.Document
{
    public class DocumentItemViewModel : ViewModelBase
    {
        bool ischecked;
        public DocumentItemViewModel()
        {
            
        }

        public DateTime StartDateTime { get; set; }
        public string Name { get; set; }
        public int FieldCount { get; set; }
        public int ProductCount { get; set; }
        public decimal TotalCost { get; set; }
        public DocumentDescriptor Descriptor { get; set; }
        public bool Checked { get { return ischecked; } set { ischecked = value; RaisePropertyChanged("Checked"); } }
    
        
    }
}
