﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Converters.Reports;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Fields;
using Landdb.ViewModel.Reports;
using Landdb.ViewModel.Secondary.Reports.AppliedProduct;
using Landdb.ViewModel.Secondary.Reports.CropZone.Generators;
using Microsoft.Win32;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using Telerik.Reporting;
using Telerik.Reporting.Processing;

namespace Landdb.ViewModel.Secondary.Reports.CropZone
{
    public class CropZoneReportCreatorViewModel : ViewModelBase
    {
        ICropZoneGenerator selectedReportGenerator;
        ReportTemplateViewModel template;
        ReportTemplateViewModelToFile templateService = new ReportTemplateViewModelToFile();
        string reportType;
        CropZoneQueryCriteria criteria;
        int pageIndex = 0;
        Dispatcher dispatcher;

        public CropZoneReportCreatorViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, int cropYear, ReportTemplateViewModel template, string reportType)
        {
            CancelCommand = new RelayCommand(OnCancelReport);
            SaveCommand = new RelayCommand(SaveTemplate);

            FieldsPageModel = new AppliedProductReportFieldSelectionViewModel(clientEndpoint, dispatcher, cropYear);
            //InfoPageModel = new AppliedProductReportInfoViewModel(clientEndpoint, dispatcher, cropYear);

            ReportGenerators = new ObservableCollection<ICropZoneGenerator>();
            //Call to add all the Applied Product Reports
            AddAppliedProductReportGenerators(clientEndpoint, dispatcher, cropYear);

            //SelectedReportGenerator = ReportGenerators.First();
            PDFExport = new RelayCommand(this.PDF);

            UpdateReport();
            this.reportType = reportType;
            //take passed in template and set values on associated ViewModels along with Criteria page
            if (template != null) { InitializeFromTemplate(template); this.template = template; };
        }

        public ICommand CancelCommand { get; private set; }
        public RelayCommand PDFExport { get; set; }
        public ICommand SaveCommand { get; set; }
        public string TemplateName { get; set; }
        public int CropYear { get { return ApplicationEnvironment.CurrentCropYear; } }
        public int SelectedPageIndex { get { return pageIndex; } set { pageIndex = value; UpdateReport(); } }
        public ReportSource ReportSource { get; set; }

        public AppliedProductReportFieldSelectionViewModel FieldsPageModel { get; private set; }
        //public AppliedProductReportInfoViewModel InfoPageModel {get; private set;}

        public ObservableCollection<ICropZoneGenerator> ReportGenerators { get; private set; }
        public ICropZoneGenerator SelectedReportGenerator
        {
            get { return selectedReportGenerator; }
            set {
                selectedReportGenerator = value;
                UpdateReport();
                RaisePropertyChanged("SelectedReportGenerator");
            }
        }

        void OnCancelReport() {
            Messenger.Default.Send<ShowMainViewMessage>(new ShowMainViewMessage());
        }

        public void UpdateReport() {
            if (SelectedReportGenerator != null) {
                criteria = BuildCriteria();
                ReportSource = SelectedReportGenerator.Generate(criteria);
                RaisePropertyChanged("ReportSource");
            }
        }

        CropZoneQueryCriteria BuildCriteria()
        {
            CropZoneQueryCriteria criteria = new CropZoneQueryCriteria();

            var czq = from cz in FieldsPageModel.SelectedCropZones
                      select cz.Id;
            criteria.SelectedCropZones.AddRange(czq);

            return criteria;
        }

        void AddAppliedProductReportGenerators(IClientEndpoint clientEndpoint, Dispatcher dispatcher, int cropYear)
        {
            ReportGenerators.Add(new CropLayoutGenerator(clientEndpoint, cropYear));
            ReportGenerators.Add(new CropLayoutWithTagsGenerator(clientEndpoint, cropYear));
            //ReportGenerators.Add(new MapTestGenerator(clientEndpoint, cropYear));
            ReportGenerators.Add(new MapInfoGenerator(clientEndpoint, cropYear));
            ReportGenerators.Add(new CropZoneSummaryReportGenerator(clientEndpoint, dispatcher, cropYear));
        }

        public void PDF()
        {
            try { 
            if (ReportSource == null) return;
            ReportSource reportExport = ReportSource;

            ReportProcessor reportProcessor = new ReportProcessor();
            System.Collections.Hashtable deviceInfo =
            new System.Collections.Hashtable();
            RenderingResult result = reportProcessor.RenderReport("PDF", reportExport, deviceInfo);

            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "Adobe Reader|*.pdf";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;

            Nullable<bool> resultSaved = saveFileDialog1.ShowDialog();

            // Process save file dialog box results 
            if (resultSaved == true)
            {
                // Save document 
                string filename = saveFileDialog1.FileName;

                try
                {
                    using (System.IO.FileStream fs = new System.IO.FileStream(filename, System.IO.FileMode.Create))
                    {
                        fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
                    }

                    System.Diagnostics.Process.Start(filename);
                }
                catch (Exception ex) { }

            }
            }
            catch (Exception ex)
            {
                //do nothing
            }
        }

        void SaveTemplate()
        {
            criteria = BuildCriteria();
            if (template != null && template.TemplateName == TemplateName)
            {
                ReportTemplateViewModel editedTemplate = new ReportTemplateViewModel()
                {
                    TemplateName = TemplateName,
                    Created = template.Created,
                    LastUsed = DateTime.Now,
                    CriteriaJSON = JsonConvert.SerializeObject(criteria),
                    ReportType = template.ReportType,
                    DataSourceId = template.DataSourceId
                };

                //TODO :: CALL TO EDIT PREVIOUS TEMPLATE...
                templateService.EditTemplate(editedTemplate);
            }
            else
            {
                ReportTemplateViewModel newTemplate = new ReportTemplateViewModel()
                {
                    TemplateName = TemplateName,
                    Created = DateTime.Now,
                    LastUsed = DateTime.Now,
                    CriteriaJSON = JsonConvert.SerializeObject(criteria),
                    ReportType = reportType,
                    DataSourceId = ApplicationEnvironment.CurrentDataSourceId
                };

                //TODO :: call service to save the new report template
                templateService.SaveNewTemplate(newTemplate);
            }

            RaisePropertyChanged("NewTemplateCreated", null, new ReportTemplateViewModel(), true);
        }

        void InitializeFromTemplate(ReportTemplateViewModel temp)
        {
            TemplateName = temp.TemplateName;

            CropZoneQueryCriteria crit = JsonConvert.DeserializeObject<CropZoneQueryCriteria>(temp.CriteriaJSON);

            var dsID = new DataSourceId(ApplicationEnvironment.CurrentDataSourceId);

            var RootTreeItemModels = FieldsPageModel.RootTreeItemModels;
            if (RootTreeItemModels.Any())
            {
                if (RootTreeItemModels.First() is GrowerTreeItemViewModel)
                {
                    var toCheck = from farm in RootTreeItemModels.First().Children
                                  from field in farm.Children
                                  from cz in field.Children
                                  where crit.SelectedCropZones.Contains(cz.Id)
                                  select cz;
                    toCheck.ForEach(x => x.IsChecked = true);
                }
                else
                {
                    var toCheck = from crop in RootTreeItemModels.First().Children
                                  from farm in crop.Children
                                  from field in farm.Children
                                  from cz in field.Children
                                  where crit.SelectedCropZones.Contains(cz.Id)
                                  select cz;
                    toCheck.ForEach(x => x.IsChecked = true);
                }
            }
        }
    }
}

