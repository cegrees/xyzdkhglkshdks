﻿using AgC.UnitConversion;
using AgC.UnitConversion.Area;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Contract;
using Landdb.Domain.ReadModels.Map;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.MapStyles;
using Landdb.ReportModels.CropZone;
using Landdb.ViewModel.Secondary.Map;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Landdb.Resources;
using Telerik.Reporting;
using ThinkGeo.MapSuite.Core;

namespace Landdb.ViewModel.Secondary.Reports.CropZone.Generators
{
    public class MapInfoGenerator : ICropZoneGenerator
    {
        IClientEndpoint clientEndpoint;
        int cropYear;

        bool startedOutOnline = false;
        InMemoryFeatureLayer fieldsLayer = new InMemoryFeatureLayer();
        ScalingTextStyle scalingTextStyle = new ScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, 24, 6);
        Feature selectedfield = new Feature();
        Feature loadedfeature = new Feature();
        BaseShape fieldshape = new MultipolygonShape();
        ClientLayerAreaStyle fieldsLayerAreaStyle = null;
        private Bitmap bitMap = null;

        public MapInfoGenerator(IClientEndpoint endPoint, int cropYear)
        {
            this.clientEndpoint = endPoint;
            this.cropYear = cropYear;
        }

        public Telerik.Reporting.ReportSource Generate(CropZoneQueryCriteria criteria)
        {
            ReportBook book = new ReportBook();
            var rs = new InstanceReportSource();

            var flattenedHierarchy = clientEndpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new FlattenedTreeHierarchyView());
            var masterlist = clientEndpoint.GetMasterlistService();

            if (criteria.SelectedCropZones.Any())
            {
                foreach (var czId in criteria.SelectedCropZones)
                {

                    var data = new MapInfoData();
                    
                    var maps = clientEndpoint.GetView<ItemMap>(czId);

                    try
                    {
                        if (maps.HasValue)
                        {
                            if (maps.Value.MostRecentMapItem != null && maps.Value.MostRecentMapItem.MapData != string.Empty && maps.Value.MostRecentMapItem.MapData != "MULTIPOLYGON EMPTY")
                            {
                                var newHeight = 400;
                                var newWidth = 400;
                                var converter = new ViewModel.Secondary.Map.ConvertToBitmap(maps.Value.MostRecentMapItem.MapData, (int)newWidth, (int)newHeight, this.clientEndpoint);
                                data.MapImage = converter.BitMap;
                            }
                        }
                    }
                    finally { }

                    var czTreeData = (from f in flattenedHierarchy.Items
                                      where f.CropZoneId == czId
                                      select f).FirstOrDefault();

                    var czDetails = clientEndpoint.GetView<CropZoneDetailsView>(czId).GetValue(new CropZoneDetailsView());

                    var fieldDetails = clientEndpoint.GetView<FieldDetailsView>(czDetails.FieldId).GetValue(new FieldDetailsView());
                    var settings = clientEndpoint.GetUserSettings();
                    data.DataSourceName = ApplicationEnvironment.CurrentDataSourceName; //string.IsNullOrEmpty(settings.DataSourceName) ? ApplicationEnvironment.CurrentDataSourceName : settings.DataSourceName;
                    var comp = settings.UserDataSourcesInfo != null ? settings.UserDataSourcesInfo.FirstOrDefault(x => x.DataSourceId == new DataSourceId(ApplicationEnvironment.CurrentDataSourceId)) : null;

                    if (comp != null)
                    {
                        data.DataSourceName = string.IsNullOrEmpty(comp.DataSourceName) ? ApplicationEnvironment.CurrentDataSourceName : comp.DataSourceName;
                        data.Address = string.IsNullOrEmpty(comp.Address) ? string.Empty : comp.Address;
                        data.City = string.IsNullOrEmpty(comp.City) ? string.Empty : comp.City;
                        data.ZipCode = string.IsNullOrEmpty(comp.ZipCode) ? string.Empty : comp.ZipCode;
                        data.State = string.IsNullOrEmpty(comp.State) ? string.Empty : comp.State;
                    }
                    if (czDetails.ReportedArea != null && czDetails.ReportedArea > 0)
                    {
                        data.Area = UnitFactory.GetUnitByName(czDetails.ReportedAreaUnit).GetMeasure(czDetails.ReportedArea.Value).FullDisplay;
                    }
                    else if (czDetails.BoundaryArea != null)
                    {
                        data.Area = UnitFactory.GetUnitByName(czDetails.BoundaryAreaUnit).GetMeasure(czDetails.BoundaryArea.Value).FullDisplay;
                    }
                    else
                    {
                        data.Area = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(0).FullDisplay;
                    }

                    data.CropYear = cropYear;
                    data.Township = fieldDetails.Township + fieldDetails.TownshipDirection;
                    data.Range = fieldDetails.Range + fieldDetails.RangeDirection;
                    data.Section = fieldDetails.Section;
                    data.Merdian = fieldDetails.Meridian;
                    data.FSANumber = fieldDetails.FsaFarmNumber;
                    data.FSATract = fieldDetails.FsaTractNumber;
                    data.FieldNumber = fieldDetails.FsaFieldNumber;
                    data.CropZone = czDetails.Name;
                    data.Field = fieldDetails.FieldNameByCropYear[cropYear];//.Name;
                    data.Farm = czTreeData.FarmName;

                    var mapSettings = clientEndpoint.GetMapSettings();

                    if (!string.IsNullOrEmpty(czDetails.CenterLatLong))
                    {
                        var centerLatLon = DecimalDegrees.ddstring_To_DMs(czDetails.CenterLatLong, mapSettings.DisplayCoordinateFormat);
                        data.CenterLatLon = centerLatLon;
                    }

                    data.Crop = masterlist.GetCropDisplay(czDetails.CropId);
                    data.County = fieldDetails.County;
                    data.LegalDescription = fieldDetails.LegalDescription;

                    ////////////////////////////////////////////////////////////////////////////////////
                    //Get Planting Information
                    ////////////////////////////////////////////////////////////////////////////////////
                    var applicationData = clientEndpoint.GetView<CropZoneApplicationDataView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new CropZoneApplicationDataView());
                    var czAppData = from x in applicationData.Items
                                    where x.CropZoneId == czId
                                    select x;
                    List<string> seedList = new List<string>();
                    List<string> seedDates = new List<string>();
                    foreach (var prod in czAppData)
                    {
                        var product = masterlist.GetProduct(prod.ProductId);

                        if (product.ProductType.ToLower() == "seed")
                        {
                            seedList.Add(product.Name +" (" + prod.EndDate.ToLocalTime().ToShortDateString() + ")");
                        }
                    }

                    data.VarietyDisplay = string.Join(" :: ", seedList.ToArray());
                    ////////////////////////////////////////////////////////////////////////////////////////////

                    if (czDetails.ProductionContracts.Any())
                    {
                        var contractId = czDetails.ProductionContracts.FirstOrDefault().Id;
                        var contractDetails = clientEndpoint.GetView<ProductionContractDetailsView>(contractId).Value;
                        data.ContractId = contractDetails.ContractNumber;
                        data.GrowerId = contractDetails.GrowerId;
                    }

                    var report = new Landdb.Views.Secondary.Reports.Field.MapInfo();
                    
                    report.DataSource = data;
                    report.Name = czDetails.Name;
                    book.Reports.Add(report);
                }
            }

            rs.ReportDocument = book;
            return rs;
        }

        public string DisplayName
        {
            get { return Strings.GeneratorName_MapInfo_Text; }
        }

    }
}
