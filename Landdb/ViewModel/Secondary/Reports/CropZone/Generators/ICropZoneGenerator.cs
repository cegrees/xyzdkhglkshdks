﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.CropZone.Generators
{
    public interface ICropZoneGenerator
    {
        ReportSource Generate(CropZoneQueryCriteria criteria);
        string DisplayName { get; }
    }
}
