﻿using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Map;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.MapStyles;
using Landdb.ReportModels.CropZone;
using Landdb.ViewModel.Maps;
using Landdb.ViewModel.Maps.ImageryOverlay;
using Landdb.ViewModel.Secondary.Map;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using Landdb.Resources;
using Telerik.Reporting;
using ThinkGeo.MapSuite.Core;
using ThinkGeo.MapSuite.WpfDesktopEdition;

namespace Landdb.ViewModel.Secondary.Reports.CropZone.Generators
{
    public class MapTestGenerator : ICropZoneGenerator
    {
        IClientEndpoint endpoint;
        int cropYear;
        ///Map Generation 
        InMemoryFeatureLayer fieldsLayer = new InMemoryFeatureLayer();
        IList<CropZoneId> includedCZs;
        ColoredMapGeneratorMultiAreaStyle cmgAreaStyle = null;
        ScalingTextStyle scalingTextStyle = new ScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, 24, 6);
        LayerOverlay layerOverlay = new LayerOverlay();
        Guid currentDataSourceId;
        bool displaycropzones = true;
        int currentCropYear;
        int fieldtransparency = 255;
        bool maploaded = false;
        bool startedOutOnline = false;
        bool repaintthescreen = false;
        ClassBreakStyle classBreakStyle = new ClassBreakStyle();
        private Dictionary<string, string> cropzonelabels = new Dictionary<string, string>();
        private Dictionary<string, string> cropzonenamess = new Dictionary<string, string>();
        private Dictionary<string, string> cropzonefeatures = new Dictionary<string, string>();
        private Dictionary<string, string> farmnamess = new Dictionary<string, string>();

        public MapTestGenerator(IClientEndpoint endPoint, int cropYear)
        {
            includedCZs = new List<CropZoneId>();
            this.cropYear = cropYear;
            this.endpoint = endPoint;
            this.currentDataSourceId = ApplicationEnvironment.CurrentDataSourceId;
            this.currentCropYear = ApplicationEnvironment.CurrentCropYear;
        }

        public MapEngine Map { get; set; }

        public string DisplayName { get { return Strings.GeneratorName_MapTest_Text; } }

        public ReportSource Generate(CropZoneQueryCriteria crit)
        {
            ReportBook book = new ReportBook();
            var rs = new InstanceReportSource();

            var flattenedHierarchy = endpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new FlattenedTreeHierarchyView());
            //var applicationData = endpoint.GetView<CropZoneApplicationDataView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new CropZoneApplicationDataView());
            if (flattenedHierarchy != null)
            {
                
                
                foreach (var czId in crit.SelectedCropZones)
                {
                    var data = new MapData();

                    var czTreeData = (from f in flattenedHierarchy.Items
                                      where f.CropZoneId == czId
                                      select f).FirstOrDefault();

                    var cropzoneDetails = endpoint.GetView<CropZoneDetailsView>(czTreeData.CropZoneId).Value;
                    var fieldDetails = endpoint.GetView<FieldDetailsView>(cropzoneDetails.FieldId).Value;

                    if (cropzoneDetails != null)
                    {
                        var maps = endpoint.GetView<ItemMap>(czId);
                        try
                        {
                            if (maps.HasValue)
                            {
                                if (maps.Value.MostRecentMapItem != null && maps.Value.MostRecentMapItem.MapData != string.Empty && maps.Value.MostRecentMapItem.MapData != "MULTIPOLYGON EMPTY") {
                                    var converter = new ConvertToBitmap(maps.Value.MostRecentMapItem.MapData, this.endpoint);
                                    //var converter = new ConvertToBitmap();

                                    //Map = converter.Map;
                                    
                                    //ScreenDescriptor sd = new ScreenDescriptor("Landdb.Views.Secondary.Reports.Field.mapTemp", "map", this);
                                    //Messenger.Default.Send<ShowPopupMessage>(new ShowPopupMessage() { ScreenDescriptor = sd });

                                    data.MapImage = converter.BitMap;
                                }
                                //czmaps.Add(mi);
                            }
                        }
                        finally { }
                    }

                    var report = new Landdb.Views.Secondary.Reports.Field.MapInfo();
                    report.DataSource = data;
                    book.Reports.Add(report);
                }
            }

            rs.ReportDocument = book;
            return rs;
        }
    }
}
