﻿using AgC.UnitConversion;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.ActiveIngredientSetting;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Contract;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Domain.ReadModels.Product;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.ReportModels.CropZone;
using Landdb.Resources;
using Landdb.Services;
using Landdb.ViewModel.Fields;
using Landdb.ViewModel.Fields.FieldDetails;
using Landdb.ViewModel.Fields.FieldDetails.PropertyParts;
using NLog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using Telerik.Reporting;


namespace Landdb.ViewModel.Secondary.Reports.CropZone.Generators
{
    class CropZoneSummaryReportGenerator : ICropZoneGenerator
    {
        readonly IClientEndpoint clientEndpoint;
        readonly Dispatcher dispatcher;
        readonly int cropYear;
        readonly decimal totalProductionCostByAverage = 0m;
        readonly decimal productionCostPerAreaByAverage = 0m;
        readonly Logger log = LogManager.GetCurrentClassLogger();
        private Stopwatch stopwatch = new Stopwatch();
        double cropzonecount = 0;
        double applicationscount = 0;
        double loadscount = 0;
        double reportduration = 0;



        public CropZoneSummaryReportGenerator(IClientEndpoint endPoint, Dispatcher dispatcher, int cropYear)
        {
            clientEndpoint = endPoint;
            this.dispatcher = dispatcher;
            this.cropYear = cropYear;
            ApplicationLineItems = new ObservableCollection<ApplicationLineItemViewModel>();
            ShowAverageCost = true;
        }

        

        public ReportSource Generate(CropZoneQueryCriteria criteria)
        {
            if (stopwatch.IsRunning)
            {
                stopwatch.Stop();
            }
            stopwatch = new Stopwatch();
            stopwatch.Start();
            cropzonecount = 0;
            applicationscount = 0;
            loadscount = 0;
            reportduration = 0;
            string reportnamestring = string.Empty;


            ReportBook book = new ReportBook();
            var rs = new InstanceReportSource();
            var applicationData = clientEndpoint.GetView<CropZoneApplicationDataView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new CropZoneApplicationDataView());
            var inventory = clientEndpoint.GetView<InventoryListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new InventoryListView());
            var flattenedHierarchy = clientEndpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new FlattenedTreeHierarchyView());
            var masterlist = clientEndpoint.GetMasterlistService();
            var currentCropYearId = new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear);
            var userSettings = clientEndpoint.GetUserSettings();
            double divisor = 1;
            double prodRate = 0;
            double areaRatio = 1;
            Measure rateMeasure;
            List<ActiveIngredientContent> aiContent = new List<ActiveIngredientContent>();
            FertilizerFormulationViewModel fertModel = new FertilizerFormulationViewModel();
            AnalysisModel model = new AnalysisModel() { FertilizerFormulation = fertModel };
            Dictionary<Guid, double> aiQuantitiesUsed = new Dictionary<Guid, double>();
            List<FlattenedTreeHierarchyItem> sortedFlattenedHierarchyItems = sortFlattenedHierarchyItemsByFieldId(flattenedHierarchy);

            

            foreach (var allCropZones in sortedFlattenedHierarchyItems)
            {
                if (criteria.SelectedCropZones.Contains(allCropZones.CropZoneId))
                {
                    var data = new SummaryData();
                    List<string> filteredMapData = new List<string>();

                    data.CropYear = ApplicationEnvironment.CurrentCropYear;
                    data.DataSourceName = ApplicationEnvironment.CurrentDataSourceName;

                    data.CropDisplay = allCropZones.FieldName + " :: " + allCropZones.CropZoneName;
                    var fieldView = clientEndpoint.GetView<FieldDetailsView>(allCropZones.FieldId).Value;
                    CropHistoryInformation cropHistory = new CropHistoryInformation(clientEndpoint, dispatcher, fieldView);
                    data = BuildCropHistoryData(data, cropHistory, currentCropYearId, allCropZones.CropZoneId);

                    var maps = clientEndpoint.GetView<Domain.ReadModels.Map.ItemMap>(allCropZones.CropZoneId);
                    try
                    {
                        if (maps.HasValue)
                        {
                            if (maps.Value.MostRecentMapItem != null && maps.Value.MostRecentMapItem.MapData != string.Empty && maps.Value.MostRecentMapItem.MapData != "MULTIPOLYGON EMPTY")
                            {
                                filteredMapData.Add(maps.Value.MostRecentMapItem.MapData);
                                cropzonecount++;
                            }
                        }
                    }
                    finally { }

                    foreach (var prodId in applicationData.Items)
                    {
                        if (allCropZones.CropZoneId == prodId.CropZoneId && criteria.SelectedCropZones.Contains(allCropZones.CropZoneId))
                        {
                            applicationscount++;
                            var masterlistProdQuery = clientEndpoint.GetMasterlistService().GetProduct(prodId.ProductId);
                            var masterlistProd = masterlistProdQuery == null ? masterlist.GetProduct(prodId.ProductId) : masterlistProdQuery;
                            IUnit rateUnit = UnitFactory.GetUnitByName(prodId.RateUnit);
                            rateMeasure = rateUnit.GetMeasure((double)prodId.RateValue, masterlistProd.Density);
                            IInventoryCalculationService inventoryService = new InventoryCalculationService(); 
                            ApplicationLineItems.Add(new ApplicationLineItemViewModel(clientEndpoint, inventoryService, inventory, prodId));

                            
                            if (masterlistProd.ProductType == GlobalStrings.ProductType_Seed)
                            {
                                data.Varieties.Add(new Variety(string.Format("{0} ({1})", masterlistProd.Name, masterlistProd.Manufacturer), prodId.StartDate.ToString()));
                            }

                            if (masterlistProd == null)
                            {
                                masterlistProd = AssignToUserCreatedProduct(prodId, currentCropYearId);
                            }

                            aiQuantitiesUsed = UpdateActiveIngredientsQuantitiesUsedByAssociateProducts(aiQuantitiesUsed, prodId, masterlist, userSettings, divisor, areaRatio);


                            foreach (var history in cropHistory.CropHistoryList)
                            {
                                if (allCropZones.CropZoneId == history.CropZoneID && criteria.SelectedCropZones.Contains(history.CropZoneID))
                                {
                                    areaRatio = DetermineAreaRatio(prodId, history);
                                    divisor = DetermineDivisor(rateMeasure, userSettings);
                                    prodRate = DetermineProdRate(rateMeasure, userSettings);

                                    if (rateMeasure is AgC.UnitConversion.MassAndWeight.MassAndWeightMeasure || (rateMeasure is AgC.UnitConversion.Volume.VolumeMeasure && rateMeasure.CanConvertTo(userSettings.UserMassUnit)))
                                    {
                                        aiQuantitiesUsed = UpdateActiveIngredientsQuantitiesUsedByMasterlistActiveIngredients(masterlistProd, prodRate, areaRatio, aiQuantitiesUsed);
                                    }
                                    else
                                    {
                                        continue;
                                    }

                                    fertModel = CalculateFertilizerFormulationTotal(prodRate, areaRatio, divisor, masterlistProd, fertModel);

                                    foreach (var activeIngredientId in aiQuantitiesUsed.Keys)
                                    {
                                        var aiId = new ActiveIngredientId(activeIngredientId);
                                        var aiName = masterlist.GetActiveIngredientDisplay(aiId);
                                        var usedQuantity = aiQuantitiesUsed[activeIngredientId];
                                        double? maxQuantity = null;

                                        var aiSetting = clientEndpoint.GetView<ActiveIngredientSettingsView>(new ActiveIngredientSettingId(currentCropYearId.DataSourceId, aiId));
                                        aiSetting.IfValue(x =>
                                        {
                                            //added in an else statement to grab previous years value of max ai ~~ Shea
                                            var yearSettings = new List<ActiveIngredientSettingItem>();

                                            if (x.ItemsByYear.ContainsKey(currentCropYearId.Id))
                                            {
                                                var item = x.ItemsByYear[currentCropYearId.Id].Where(y => y.CropId == allCropZones.CropId).FirstOrDefault();
                                                if (item != null) { maxQuantity = (double?)item.MaximumValue; }
                                            }
                                            else
                                            {
                                                yearSettings = (from m in x.ItemsByYear where m.Key < currentCropYearId.Id select m).Any()
                                                    ? (from m in x.ItemsByYear where m.Key < currentCropYearId.Id select m).Last().Value : new List<ActiveIngredientSettingItem>();

                                                var item = yearSettings.Where(c => c.CropId == allCropZones.CropId).Any() ? yearSettings.Where(c => c.CropId == allCropZones.CropId).Last() : null;
                                                if (item != null) { maxQuantity = (double?)item.MaximumValue; }
                                            }
                                        });

                                        model.ActiveIngredients.Add(new ActiveIngredientLineItemViewModel(clientEndpoint, userSettings, aiId, aiName, usedQuantity, maxQuantity, allCropZones.CropId));
                                    }
                                }
                            }
                            var analysis = model;
                            FertilizerInformation = analysis.FertilizerFormulation;
                            data.FertilizerAnalysis = FertilizerInformation != null ? FertilizerDetailsString(FertilizerInformation) : string.Empty;

                        }
                    }

                    var report = new Landdb.Views.Secondary.Reports.Field.SummaryDetails();
                    if (filteredMapData.Count() > 0)
                    {
                        var converter = new Secondary.Map.ConvertToBitmap(filteredMapData, this.clientEndpoint, 0, "", "");
                        data.MapImage = converter.BitMap;
                    }
                    data = BuildInputItems(currentCropYearId, data);
                    var reportData = data;
                    report.DataSource = reportData;
                    report.Name = Landdb.Resources.Strings.GeneratorName_CropzoneSummary_Text;
                    reportnamestring = report.ToString();
                    book.Reports.Add(report);
                    fertModel = ClearFertilizerFormulationTotal(fertModel);
                    
                }
            }
            if (stopwatch.IsRunning)
            {
                stopwatch.Stop();
            }
            if(cropzonecount > 0)
            {
                double duration = stopwatch.ElapsedMilliseconds;
                var properties = new Dictionary<string, string> {{"Item Type", "Report"}, {"Item Name", Landdb.Resources.Strings.GeneratorName_CropzoneSummary_Text }, { "Action", "Generate" } };
                var metrics = new Dictionary<string, double> { { "Process Duration", duration }, { "Cropzones", cropzonecount }, { "Applications", applicationscount }, { "Yield Loads", loadscount } };
                App.CurrentApp.Telemetry.TrackEvent(reportnamestring, properties, metrics);

                //App.CurrentApp.Telemetry.TrackEvent("REPORT - " + Landdb.Resources.Strings.GeneratorName_CropzoneSummary_Text);
                //App.CurrentApp.Telemetry.TrackEvent(report.ToString());
            }
            rs.ReportDocument = book;
            
            return rs;
        }
        public FertilizerFormulationViewModel ClearFertilizerFormulationTotal(FertilizerFormulationViewModel fertModel)
        {
            fertModel.N = 0;
            fertModel.P = 0;
            fertModel.K = 0;
            fertModel.Ca = 0;
            fertModel.Mg = 0;
            fertModel.S = 0;
            fertModel.B = 0;
            fertModel.Cl = 0;
            fertModel.Cu = 0;
            fertModel.Fe = 0;
            fertModel.Mn = 0;
            fertModel.Mo = 0;
            fertModel.Zn = 0;
            return fertModel;
        }
        public SummaryData ClearData(SummaryData data)
        {
            var dummydata = data;
            data.FertilizerAnalysis = "";
            data.AreaDisplay = "";
            data.CropDisplay = "";
            data.CropHistorys.Clear();
            data.CropYear = 0;
            data.FieldDisplay = "";
            data.InputItems.Clear();
            data.Varieties.Clear();
            data.YieldItems.Clear();
            return data;
        }

        public WebMiniProduct AssignToUserCreatedProduct(CropZoneApplicationDataItem prodId, CropYearId currentCropYearId)
        {
            var userCreatedList = clientEndpoint.GetView<UserCreatedProductList>(new DataSourceId(currentCropYearId.DataSourceId)).GetValue(new UserCreatedProductList());
            var userCreated = userCreatedList.UserCreatedProducts.Where(x => x.Id == prodId.ProductId.Id).FirstOrDefault();
            return userCreated;
        }

        public Dictionary<Guid, double> UpdateActiveIngredientsQuantitiesUsedByAssociateProducts(Dictionary<Guid, double> aiQuantities, CropZoneApplicationDataItem prodId, IMasterlistService masterlist, UserSettings userSettings, double divisor, double areaRatio)
        {
            foreach (var associate in prodId.AssociatedProducts)
            {
                var associateMlp = masterlist.GetProduct(associate.ProductId);
                var associateRatePerArea = UnitFactory.GetPackageSafeUnit(associate.RatePerAreaUnit, associateMlp.StdUnit, associateMlp.StdFactor, associateMlp.StdPackageUnit).GetMeasure((double)associate.RatePerAreaValue, associateMlp.Density);
                if (associateRatePerArea is AgC.UnitConversion.MassAndWeight.MassAndWeightMeasure || (associateRatePerArea is AgC.UnitConversion.Volume.VolumeMeasure && associateRatePerArea.CanConvertTo(userSettings.UserMassUnit)))
                {
                    divisor = 100;
                    var associateRate = associateRatePerArea.CreateAs(userSettings.UserMassUnit).Value;

                    foreach (var ai in associateMlp.ActiveIngredients ?? new List<ActiveIngredientContent>())
                    {
                        if (ai.Id == Guid.Empty) { continue; }
                        double massOfAiApplied = associateRate * areaRatio * (double)ai.Percent;

                        if (Landdb.Domain.ReadModels.Services.ActiveIngredientXRef.Self.IsGoodActiveIngredient(ai.Id))
                        {
                            var _id = Landdb.Domain.ReadModels.Services.ActiveIngredientXRef.Self.CrossReferenceActiveIngredient(ai.Id);
                            if (aiQuantities.ContainsKey(_id))
                            {
                                aiQuantities[_id] += massOfAiApplied;
                            }
                            else
                            {
                                aiQuantities.Add(_id, massOfAiApplied);
                            }
                        }
                    }
                }
            }
            return aiQuantities;
        }

        public double DetermineAreaRatio(CropZoneApplicationDataItem prodId, CropHistoryListItemViewModel history)
        {
            if (prodId.AreaUnit == history.AreaUnit)
            {
                return (prodId.AreaValue * prodId.CoveragePercent) / history.Area;
            }
            else
            {
                var historyAreaUnit = UnitFactory.GetUnitByName(history.AreaUnit);
                var itemAreaUnit = UnitFactory.GetUnitByName(prodId.AreaUnit);
                var itemArea = itemAreaUnit.GetMeasure(prodId.AreaValue);
                var consistentItemArea = itemArea.GetValueAs((historyAreaUnit));
                return (consistentItemArea * prodId.CoveragePercent) / history.Area;
            }
        }

        public double DetermineDivisor(Measure rateMeasure, UserSettings userSettings)
        {
            if (rateMeasure is AgC.UnitConversion.MassAndWeight.MassAndWeightMeasure || (rateMeasure is AgC.UnitConversion.Volume.VolumeMeasure && rateMeasure.CanConvertTo(userSettings.UserMassUnit)))
            {
                return 100;
            }
            else { return 1; }
        }

        public double DetermineProdRate(Measure rateMeasure, UserSettings userSettings)
        {
            if (rateMeasure is AgC.UnitConversion.Area.AreaMeasure)
            {
                return rateMeasure.CreateAs(userSettings.UserAreaUnit).Value;
            }
            else if (rateMeasure is AgC.UnitConversion.MassAndWeight.MassAndWeightMeasure || (rateMeasure is AgC.UnitConversion.Volume.VolumeMeasure && rateMeasure.CanConvertTo(userSettings.UserMassUnit)))
            {
                return rateMeasure.CreateAs(userSettings.UserMassUnit).Value;
            }
            else { return 0; }
        }

        public FertilizerFormulationViewModel CalculateFertilizerFormulationTotal(double prodRate, double areaRatio, double divisor, MiniProduct masterlistProd, FertilizerFormulationViewModel fertModel)
        {
            decimal multiplicand = (decimal)prodRate * (decimal)areaRatio / (decimal)divisor;

            if (masterlistProd.Formulation != null)
            {
                var f = masterlistProd.Formulation;

                if (f.N.HasValue) { fertModel.N += f.N.Value * multiplicand; }
                if (f.P.HasValue) { fertModel.P += f.P.Value * multiplicand; }
                if (f.K.HasValue) { fertModel.K += f.K.Value * multiplicand; }
                if (f.Ca.HasValue) { fertModel.Ca += f.Ca.Value * multiplicand; }
                if (f.Mg.HasValue) { fertModel.Mg += f.Mg.Value * multiplicand; }
                if (f.S.HasValue) { fertModel.S += f.S.Value * multiplicand; }
                if (f.B.HasValue) { fertModel.B += f.B.Value * multiplicand; }
                if (f.Cl.HasValue) { fertModel.Cl += f.Cl.Value * multiplicand; }
                if (f.Cu.HasValue) { fertModel.Cu += f.Cu.Value * multiplicand; }
                if (f.Fe.HasValue) { fertModel.Fe += f.Fe.Value * multiplicand; }
                if (f.Mn.HasValue) { fertModel.Mn += f.Mn.Value * multiplicand; }
                if (f.Mo.HasValue) { fertModel.Mo += f.Mo.Value * multiplicand; }
                if (f.Zn.HasValue) { fertModel.Zn += f.Zn.Value * multiplicand; }
            }
            return fertModel;
        }

        public Dictionary<Guid, double> UpdateActiveIngredientsQuantitiesUsedByMasterlistActiveIngredients(MiniProduct masterlistProd, double prodRate, double areaRatio, Dictionary<Guid, double> aiQuantitiesUsed)
        {
            foreach (var ai in masterlistProd.ActiveIngredients ?? new List<ActiveIngredientContent>())
            {
                if (ai.Id == Guid.Empty) { continue; }
                double massOfAiApplied = prodRate * areaRatio * (double)ai.Percent;

                if (Landdb.Domain.ReadModels.Services.ActiveIngredientXRef.Self.IsGoodActiveIngredient(ai.Id))
                {
                    var _id = Landdb.Domain.ReadModels.Services.ActiveIngredientXRef.Self.CrossReferenceActiveIngredient(ai.Id);
                    if (aiQuantitiesUsed.ContainsKey(_id))
                    {
                        aiQuantitiesUsed[_id] += massOfAiApplied;
                    }
                    else
                    {
                        aiQuantitiesUsed.Add(_id, massOfAiApplied);
                    }
                }
            }
            return aiQuantitiesUsed;
        }

        public SummaryData BuildYieldItems(FieldCropHistoryItem fch, CropYearId currentCropYearId, SummaryData data)
        {
            var yieldSummaryItems = new List<YieldSummaryItemViewModel>();
            decimal cashRentPerArea = 0;
            RevenueSummary = new RevenueSummaryViewModel((double)fch.Area, fch.AreaUnit, cashRentPerArea);
            
            var loads = new List<LoadLineItemViewModel>();
            var yieldLoads = clientEndpoint.GetView<Domain.ReadModels.Yield.CropZoneLoadDataView>(currentCropYearId);
            if (yieldLoads.HasValue)
            {
                var czLoads = yieldLoads.Value.Items.Where(x => x.CropZoneId == fch.CropZoneID);
                loadscount += czLoads.Count();
                foreach (var item in czLoads)
                {
                    //TO DO :: FIND THE CORRECT GROWER SHARE SPLIT
                    loads.Add(new LoadLineItemViewModel(item, 1m));
                }
            }

            var commodityList = from item in loads
                                group item by item.Commodity into g
                                orderby g.Count() descending
                                select g.Key;

            foreach (var commodity in commodityList)
            {
                var itemsWithThisCommodity = loads.Where(x => x.Commodity == commodity);

                var sortedUnitFrequencyList = from item in itemsWithThisCommodity
                                              where item.FinalQuantityUnit != null
                                              group item by item.FinalQuantityUnit into g
                                              orderby g.Count() descending
                                              select g.Key;

                foreach (var unit in sortedUnitFrequencyList)
                {
                    var itemsWithThisUnit = itemsWithThisCommodity.Where(x => x.FinalQuantityUnit == unit);

                    var totalQuantity = itemsWithThisUnit.Sum(x => x.WeightedFinalQuantity);
                    var growerShare = itemsWithThisUnit.Sum(x => x.GrowerShareQuantity);

                    decimal commodityAverageSalePrice = 0;
                    decimal fieldToPosAverageSalePrice = 0;

                    var commoditySummary = new Landdb.ViewModel.Yield.CommoditySummaryViewModel(clientEndpoint, dispatcher, currentCropYearId, commodity.CropId, commodity.CommodityDescription);
                    if (commoditySummary.UnitSummaries.ContainsKey(unit))
                    {
                        commodityAverageSalePrice = commoditySummary.UnitSummaries[unit].GrowerAverageRevenue;
                    }

                    var growerSale = itemsWithThisUnit.Sum(x => x.GrowerPrice);
                    fieldToPosAverageSalePrice = growerShare != 0 ? growerSale / growerShare : 0;

                    var contractedCropZones = clientEndpoint.GetView<CropZoneRentContractView>(currentCropYearId).GetValue(new CropZoneRentContractView());
                    if (contractedCropZones.CropZoneRentContracts.ContainsKey(fch.CropZoneID.Id))
                    {
                        cashRentPerArea = contractedCropZones.CropZoneRentContracts[fch.CropZoneID.Id].CashRentPerArea;
                    }

                    RevenueSummary.YieldSummaryItems.Add(new YieldSummaryItemViewModel(fch.Area)
                    {
                        Description = !string.IsNullOrWhiteSpace(commodity.CommodityDescription) ? commodity.CommodityDescription : commodity.CropName,
                        TotalYieldQuantity = totalQuantity,
                        GrowerQuantity = growerShare,
                        YieldQuantityUnit = unit,
                        CommodityAverageSalePrice = commodityAverageSalePrice,
                        FieldToPosAverageSalePrice = fieldToPosAverageSalePrice,
                    });
                }
            }

            // add a blank item if there aren't any

            if (!RevenueSummary.YieldSummaryItems.Any())
            {
                RevenueSummary.YieldSummaryItems.Add(new YieldSummaryItemViewModel(fch.Area));
            }

            foreach (var item in RevenueSummary.YieldSummaryItems)
            {
                fch.AverageYieldPerArea = item.TotalYieldQuantityPerArea;
                fch.YieldUnit = item.YieldQuantityUnit;
                fch.Crop = string.IsNullOrEmpty(item.Description) ? fch.Crop : item.Description;
                data.CropHistorys.Add(fch);
            }


            var areaUnit = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).AbbreviatedDisplay;
            foreach (var y in RevenueSummary.YieldSummaryItems)
            {
                var yieldUnit = y.YieldQuantityUnit != null ? y.YieldQuantityUnit.AbbreviatedDisplay : string.Empty;
                data.YieldItems.Add(new GenericListItem(y.Description, string.Format("{0} {1}", y.TotalYieldQuantity.ToString("N2"), yieldUnit), string.Format("{0} / {1}", y.TotalYieldQuantityPerArea.ToString("N2"), areaUnit)));
                data.YieldItems.Add(new GenericListItem(Landdb.Resources.Strings.AverageSale_Text.ToLower(), y.AverageSalePrice.ToString("C2"), string.Empty));
                data.YieldItems.Add(new GenericListItem(Landdb.Resources.Strings.GrowerShares_Text.ToLower(), string.Format("{0} {1}", y.GrowerQuantity.ToString("N2"), yieldUnit), string.Format("{0} / {1}", y.GrowerQuantityPerArea.ToString("N2"), areaUnit)));
                data.YieldItems.Add(new GenericListItem(Landdb.Resources.Strings.GrowerRevenue_Text.ToLower(), y.GrowerRevenue.ToString("C2"), string.Empty));
                data.YieldItems.Add(new GenericListItem(string.Empty, string.Empty, string.Empty));
                RevenueSummary.GrowerRevenue = y.GrowerRevenue;
            }

            return data;
        }

        public SummaryData BuildInputItems(CropYearId currentCropYearId, SummaryData data)
        {
            var areaUnit = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).AbbreviatedDisplay;
            RecalculateCostSummaryByAverage();
            data.InputItems.Add(new GenericListItem(Landdb.Resources.Strings.Seed_Text.ToLower(), RevenueSummary.SeedTotalCost.ToString("C2"), string.Format("{0} / {1}", RevenueSummary.SeedCostPerArea.ToString("C2"), areaUnit)));
            data.InputItems.Add(new GenericListItem(Landdb.Resources.Strings.Fertilizer_Text.ToLower(), RevenueSummary.FertilizerTotalCost.ToString("C2"), string.Format("{0} / {1}", RevenueSummary.FertilizerCostPerArea.ToString("C2"), areaUnit)));
            data.InputItems.Add(new GenericListItem(Landdb.Resources.Strings.CropProtection_Text.ToLower(), RevenueSummary.CropProtectionTotalCost.ToString("C2"), string.Format("{0} / {1}", RevenueSummary.CropProtectionCostPerArea.ToString("C2"), areaUnit)));
            data.InputItems.Add(new GenericListItem(Landdb.Resources.Strings.FilterType_Services.ToLower(), RevenueSummary.ServicesTotalCost.ToString("C2"), string.Format("{0} / {1}", RevenueSummary.ServicesCostPerArea.ToString("C2"), areaUnit)));
            data.InputItems.Add(new GenericListItem(Landdb.Resources.Strings.Total_Text.ToLower(), RevenueSummary.TotalCost.ToString("C2"), string.Format("{0} / {1}", RevenueSummary.CostPerArea.ToString("C2"), areaUnit)));
            if (!ShowAverageCost) { data.InputItems.Add(new GenericListItem(Landdb.Resources.Strings.AppAnalysis_DeviationFromAvg_Text.ToLower(), AlternativeProductionTotalCostDeviation.ToString("C2"), string.Format("{0} / {1}", AlternativeProductionCostPerAreaDeviation.ToString("C2"), areaUnit))); }
            data.InputItems.Add(new GenericListItem(string.Empty, string.Empty, string.Empty));
            if (RevenueSummary.TotalCashRent != 0) { data.InputItems.Add(new GenericListItem(Landdb.Resources.Strings.CashRent_Text.ToLower(), RevenueSummary.TotalCashRent.ToString("C2"), string.Format("{0} / {1}", RevenueSummary.CashRentPerArea.ToString("C2"), areaUnit))); }
            data.InputItems.Add(new GenericListItem(Landdb.Resources.Strings.AppAnalysis_ContributionMargin_Text.ToLower(), RevenueSummary.TotalContributionMargin.ToString("C2"), string.Format("{0} / {1}", RevenueSummary.ContributionMarginPerArea.ToString("C2"), areaUnit)));

            if (RevenueSummary.BreakEvenYieldVisible)
            {
                data.InputItems.Add(new GenericListItem(RevenueSummary.BreakEvenYieldDescription, string.Empty, RevenueSummary.BreakEvenYield.ToString("N2")));
            }
            if (RevenueSummary.BreakEvenCostVisible)
            {
                data.InputItems.Add(new GenericListItem(RevenueSummary.BreakEvenCostDescription, string.Empty, RevenueSummary.BreakEvenCost.ToString("C2")));
            }
            
            return data;
        }

        public SummaryData onlyCalculateAverageYield(FieldCropHistoryItem fch, CropYearId currentCropYearId, SummaryData data)
        {
            CropYearId thisCropsCropYearId = new CropYearId(ApplicationEnvironment.CurrentDataSourceId, fch.CropYear);
            var yieldSummaryItems = new List<YieldSummaryItemViewModel>();
            decimal cashRentPerArea = 0;
            var _revenueSummary = new RevenueSummaryViewModel((double)fch.Area, fch.AreaUnit, cashRentPerArea);
            
            var loads = new List<LoadLineItemViewModel>();
            var yieldLoads = clientEndpoint.GetView<Domain.ReadModels.Yield.CropZoneLoadDataView>(thisCropsCropYearId);
            if (yieldLoads.HasValue)
            {
                var czLoads = yieldLoads.Value.Items.Where(x => x.CropZoneId == fch.CropZoneID);
                foreach (var item in czLoads)
                {
                    //TO DO :: FIND THE CORRECT GROWER SHARE SPLIT
                    loads.Add(new LoadLineItemViewModel(item, 1m));
                }
            }

            var commodityList = from item in loads
                                group item by item.Commodity into g
                                orderby g.Count() descending
                                select g.Key;

            foreach (var commodity in commodityList)
            {
                var itemsWithThisCommodity = loads.Where(x => x.Commodity == commodity);

                var sortedUnitFrequencyList = from item in itemsWithThisCommodity
                                              where item.FinalQuantityUnit != null
                                              group item by item.FinalQuantityUnit into g
                                              orderby g.Count() descending
                                              select g.Key;

                foreach (var unit in sortedUnitFrequencyList)
                {
                    var itemsWithThisUnit = itemsWithThisCommodity.Where(x => x.FinalQuantityUnit == unit);

                    var totalQuantity = itemsWithThisUnit.Sum(x => x.WeightedFinalQuantity);
                    var growerShare = itemsWithThisUnit.Sum(x => x.GrowerShareQuantity);

                    decimal commodityAverageSalePrice = 0;
                    decimal fieldToPosAverageSalePrice = 0;

                    var commoditySummary = new Landdb.ViewModel.Yield.CommoditySummaryViewModel(clientEndpoint, dispatcher, thisCropsCropYearId, commodity.CropId, commodity.CommodityDescription);
                    if (commoditySummary.UnitSummaries.ContainsKey(unit))
                    {
                        commodityAverageSalePrice = commoditySummary.UnitSummaries[unit].GrowerAverageRevenue;
                    }

                    var growerSale = itemsWithThisUnit.Sum(x => x.GrowerPrice);
                    fieldToPosAverageSalePrice = growerShare != 0 ? growerSale / growerShare : 0;

                    
                    _revenueSummary.YieldSummaryItems.Add(new YieldSummaryItemViewModel(fch.Area)
                    {
                        Description = !string.IsNullOrWhiteSpace(commodity.CommodityDescription) ? commodity.CommodityDescription : commodity.CropName,
                        TotalYieldQuantity = totalQuantity,
                        GrowerQuantity = growerShare,
                        YieldQuantityUnit = unit,
                        CommodityAverageSalePrice = commodityAverageSalePrice,
                        FieldToPosAverageSalePrice = fieldToPosAverageSalePrice,
                    });
                }
            }

            // add a blank item if there aren't any

            if (!_revenueSummary.YieldSummaryItems.Any())
            {
                _revenueSummary.YieldSummaryItems.Add(new YieldSummaryItemViewModel(fch.Area));
            }

            foreach (var item in _revenueSummary.YieldSummaryItems)
            {
                fch.AverageYieldPerArea = item.TotalYieldQuantityPerArea;
                fch.YieldUnit = item.YieldQuantityUnit;
                fch.Crop = string.IsNullOrEmpty(item.Description) ? fch.Crop : item.Description;
                data.CropHistorys.Add(fch);
            }
            //data.CropHistorys.Add(fch);

            return data;
        }

        public SummaryData BuildCropHistoryData(SummaryData data, CropHistoryInformation cropHistory, CropYearId currentCropYearId, CropZoneId czid)
        {

            foreach (var history in cropHistory.CropHistoryList)
            {
                if (czid == history.CropZoneID)
                {
                    FieldCropHistoryItem fch = new FieldCropHistoryItem();
                    fch.Area = (decimal)history.Area;
                    fch.AreaUnit = UnitFactory.GetUnitByName(history.AreaUnit);
                    fch.Crop = history.Crop;
                    fch.CropID = history.CropId;
                    fch.CropYear = history.CropYear;
                    fch.CropZoneID = history.CropZoneID;
                    data.AreaDisplay = fch.Area.ToString("N2") + " " + fch.AreaUnit.ToString();


                    data = BuildYieldItems(fch, currentCropYearId, data);
                }
                else
                {
                    FieldCropHistoryItem fch = new FieldCropHistoryItem();
                    fch.Area = (decimal)history.Area;
                    fch.AreaUnit = UnitFactory.GetUnitByName(history.AreaUnit);
                    fch.Crop = history.Crop;
                    fch.CropID = history.CropId;
                    fch.CropYear = history.CropYear;
                    fch.CropZoneID = history.CropZoneID;

                    data = onlyCalculateAverageYield(fch, currentCropYearId, data);
                }
            }
            return data;
        }

        public string DisplayName
        {
            get { return Strings.GeneratorName_CropzoneSummary_Text; }
        }
        public bool ShowAverageCost { get; private set; }
        public FertilizerFormulationViewModel FertilizerInformation { get; set; }
        public ObservableCollection<ApplicationLineItemViewModel> ApplicationLineItems { get; private set; }
        public ObservableCollection<VarietyItemViewModel> Varieties { get; private set; }
        public CropId CropId { get; private set; }
        public string CropZoneName { get; private set; }
        public string FieldName { get; private set; }
        public string AreaDisplay { get; private set; }
        public CropZoneId CropZoneID { get; set; }
        public FieldId FieldID { get; set; }
        public List<ActiveIngredientLineItemViewModel> ActiveIngredients { get; private set; }
        public ObservableCollection<PricingViewStrategy> PricingViewStrategies { get; private set; }
        public ObservableCollection<YieldPricingViewStrategy> YieldPricingViewStrategies { get; private set; }
        public bool ShowInvoiceCost { get; private set; }
        public bool ShowSpecificCost { get; private set; }
        public bool IsCropZone { get; private set; }
        private RevenueSummaryViewModel revenueSummary;
        public RevenueSummaryViewModel RevenueSummary
        {
            get { return revenueSummary ?? new RevenueSummaryViewModel(0, null, 0); }
            private set
            {
                revenueSummary = value;
                //RaisePropertyChanged(() => RevenueSummary);
            }
        }

        private string FertilizerDetailsString(FertilizerFormulationViewModel fertInfo)
        {
            StringBuilder formulation = new StringBuilder(30);
            formulation.Append(String.Format("{0:0#.##}", fertInfo.N)); formulation.Append("N - ");
            formulation.Append(String.Format("{0:0#.##}", fertInfo.P)); formulation.Append("P - ");
            formulation.Append(String.Format("{0:0#.##}", fertInfo.K)); formulation.Append("K");

            if (fertInfo.Ca != 0)
            {
                formulation.Append(" - "); formulation.Append(String.Format("{0:0#.##}", fertInfo.Ca)); formulation.Append("Ca");
            }
            if (fertInfo.Mg != 0)
            {
                formulation.Append(" - "); formulation.Append(String.Format("{0:0#.##}", fertInfo.Mg)); formulation.Append("Mg");
            }
            if (fertInfo.S != 0)
            {
                formulation.Append(" - "); formulation.Append(String.Format("{0:0#.##}", fertInfo.S)); formulation.Append("S");
            }
            if (fertInfo.B != 0)
            {
                formulation.Append(" - "); formulation.Append(String.Format("{0:0#.##}", fertInfo.B)); formulation.Append("B");
            }
            if (fertInfo.Cl != 0)
            {
                formulation.Append(" - "); formulation.Append(String.Format("{0:0#.##}", fertInfo.Cl)); formulation.Append("Cl");
            }
            if (fertInfo.Cu != 0)
            {
                formulation.Append(" - "); formulation.Append(String.Format("{0:0#.##}", fertInfo.Cu)); formulation.Append("Cu");
            }
            if (fertInfo.Fe != 0)
            {
                formulation.Append(" - "); formulation.Append(String.Format("{0:0#.##}", fertInfo.Fe)); formulation.Append("Fe");
            }
            if (fertInfo.Mn != 0)
            {
                formulation.Append(" - "); formulation.Append(String.Format("{0:0#.##}", fertInfo.Mn)); formulation.Append("Mn");
            }
            if (fertInfo.Mo != 0)
            {
                formulation.Append(" - "); formulation.Append(String.Format("{0:0#.##}", fertInfo.Mo)); formulation.Append("Mo");
            }
            if (fertInfo.Zn != 0)
            {
                formulation.Append(" - "); formulation.Append(String.Format("{0:0#.##}", fertInfo.Zn)); formulation.Append("Zn");
            }

            return formulation.ToString();
        }
        public decimal AlternativeProductionTotalCostDeviation
        {
            get
            {
                if (RevenueSummary != null)
                {
                    return RevenueSummary.TotalCost - totalProductionCostByAverage;
                }
                else
                {
                    return 0m;
                }
            }
        }
        public decimal AlternativeProductionCostPerAreaDeviation
        {
            get
            {
                if (RevenueSummary != null)
                {
                    return RevenueSummary.CostPerArea - productionCostPerAreaByAverage;
                }
                else
                {
                    return 0m;
                }
            }

        }
        async void RecalculateCostSummaryByAverage()
        {
            RevenueSummary.ClearProductionCosts();
            

            foreach (var x in ApplicationLineItems)
            {
                
                var p = clientEndpoint.GetMasterlistService().GetProduct(x.ProductId);
                if (p != null)
                {
                    // TODO: Calculating cost summaries like this will have perf implications for large lists. 
                    // Look to offload this to another thread in the future. -bs

                    if (p.ProductType == GlobalStrings.ProductType_Seed)
                    {
                        RevenueSummary.SeedTotalCost += (decimal)x.TotalCost;
                    }
                    if (p.ProductType == GlobalStrings.ProductType_Fertilizer)
                    {
                        RevenueSummary.FertilizerTotalCost += (decimal)x.TotalCost;
                    }
                    if (p.ProductType == GlobalStrings.ProductType_CropProtection)
                    {
                        RevenueSummary.CropProtectionTotalCost += (decimal)x.TotalCost;
                    }
                    if (p.ProductType == GlobalStrings.ProductType_Service)
                    {
                        RevenueSummary.ServicesTotalCost += (decimal)x.TotalCost;
                    }
                }
            }
            ApplicationLineItems.Clear();
        }
        public List<FlattenedTreeHierarchyItem> sortFlattenedHierarchyItemsByFieldId(FlattenedTreeHierarchyView flattenedhierarchy)
        {
            List<FieldId> distinctFieldIds = new List<FieldId>();
            List<FlattenedTreeHierarchyItem> sortedFlattenedhierarchy = new List<FlattenedTreeHierarchyItem>();

            foreach (var flattenedhierarchyItems in flattenedhierarchy.Items)
            {
                if (!distinctFieldIds.Contains(flattenedhierarchyItems.FieldId))
                {
                    distinctFieldIds.Add(flattenedhierarchyItems.FieldId);
                }
            }

            foreach (var fieldIds in distinctFieldIds)
            {
                foreach (var flattenedhierarchyItems in flattenedhierarchy.Items)
                {
                    if (fieldIds == flattenedhierarchyItems.FieldId)
                    {
                        sortedFlattenedhierarchy.Add(flattenedhierarchyItems);
                    }
                }
            }

            return sortedFlattenedhierarchy;
        }
    }
    public enum PricingViewStrategy
    {
        AverageCost = 0,
        InvoiceMatching = 1,
        SpecificCost = 2
    }

    public enum YieldPricingViewStrategy
    {
        AverageSalePrice,
        FieldDirectToPointOfSale
    }



}
