﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ReportModels.CropZone;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Landdb.Resources;
using Telerik.Reporting;
using Telerik.Reporting.Processing;

namespace Landdb.ViewModel.Secondary.Reports.CropZone.Generators
{
    public class SummaryGenerator : ViewModelBase
    {
        IClientEndpoint endpoint;
        SummaryData data;

        public SummaryGenerator(IClientEndpoint endPoint, SummaryData data)
        {
            this.endpoint = endPoint;
            this.data = data;
            HideReport = new RelayCommand(this.CloseReport);
            PDFExport = new RelayCommand(this.PDF);

            UpdateReport();
        }

        public RelayCommand HideReport { get; private set; }
        public RelayCommand PDFExport { get; set; }

        public void CloseReport()
        {
            Messenger.Default.Send(new HideOverlayMessage() { });
        }

        public ReportSource ReportSource { get; set; }
        public string DisplayName { get { return Strings.GeneratorName_SummaryReport_Text; } }

        public ReportSource Generate()
        {
            ReportBook book = new ReportBook();
            var rs = new InstanceReportSource();

            var report = new Landdb.Views.Secondary.Reports.Field.SummaryDetails();
            report.DataSource = data;
            book.Reports.Add(report);

            rs.ReportDocument = book;
            return rs;
        }

        public void UpdateReport()
        {
            //PlanQueryCriteria criteria = BuildCriteria();
            ReportSource = Generate();
            RaisePropertyChanged("ReportSource");
        }

        public void PDF()
        {
            try { 
            if (ReportSource == null) return;
            ReportSource reportExport = ReportSource;

            ReportProcessor reportProcessor = new ReportProcessor();
            System.Collections.Hashtable deviceInfo =
            new System.Collections.Hashtable();
            RenderingResult result = reportProcessor.RenderReport("PDF", reportExport, deviceInfo);

            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "Adobe Reader|*.pdf";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;

            Nullable<bool> resultSaved = saveFileDialog1.ShowDialog();

            // Process save file dialog box results 
            if (resultSaved == true)
            {
                // Save document 
                string filename = saveFileDialog1.FileName;

                try
                {
                    using (System.IO.FileStream fs = new System.IO.FileStream(filename, System.IO.FileMode.Create))
                    {
                        fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
                    }
                }
                catch (Exception ex) { }

            }
            }
            catch (Exception ex)
            {
                //do nothing
            }
        }
    }
}
