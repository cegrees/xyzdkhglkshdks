﻿using AgC.UnitConversion;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Contract;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.ReportModels.CropZone;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using Landdb.Resources;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.CropZone.Generators
{
    public class CropLayoutWithTagsGenerator : ICropZoneGenerator
    {
        IClientEndpoint endpoint;
        int cropYear;

        public CropLayoutWithTagsGenerator(IClientEndpoint endPoint, int cropYear)
        {
            this.endpoint = endPoint;
            this.cropYear = cropYear;
            //var cropLayoutData = endpoint.GetView<CropZoneDetails>(null)
        }

        public string DisplayName { get { return Strings.GeneratorName_CropLayoutWithTags_Text; } }

        public ReportSource Generate(CropZoneQueryCriteria crit)
        {
            ReportBook book = new ReportBook();
            var rs = new InstanceReportSource();

            var flattenedHierarchy = endpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new FlattenedTreeHierarchyView());
            var applicationData = endpoint.GetView<CropZoneApplicationDataView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new CropZoneApplicationDataView());
            if (flattenedHierarchy != null)
            {
                var data = new CropZoneData();
                data.CropYear = cropYear.ToString();
                data.CropZoneLineItems = new List<CropZoneData.CropZoneLineItemData>();
                foreach (var czId in crit.SelectedCropZones)
                {
                    var czLineItem = new Landdb.ReportModels.CropZone.CropZoneData.CropZoneLineItemData();
                    var czTreeData = (from f in flattenedHierarchy.Items
                                      where f.CropZoneId == czId
                                      select f).FirstOrDefault();
                    
                    var cropzoneDetails = endpoint.GetView<CropZoneDetailsView>(czTreeData.CropZoneId).Value;
                    var fieldDetails = endpoint.GetView<FieldDetailsView>(cropzoneDetails.FieldId).Value;
                    
                    czLineItem.Farm = czTreeData.FarmName;
                    czLineItem.Field = czTreeData.FieldName;
                    czLineItem.CropZone = czTreeData.CropZoneName;
                    czLineItem.Tags = string.Join(" + ", cropzoneDetails.Tags.ToArray());
                    var cropName = endpoint.GetMasterlistService().GetCrop(czTreeData.CropId).Name;
                    czLineItem.Crop = cropName;
                    czLineItem.CropYear = cropYear;
                    ////////////////////////////////////////////////////////////////////////////////////
                    //Get Planting Information
                    ////////////////////////////////////////////////////////////////////////////////////

                    var czAppData = from x in applicationData.Items
                                    where x.CropZoneId == czId
                                    select x;
                    List<string> seedList = new List<string>();
                    List<string> seedDates = new List<string>();
                    foreach(var prod in czAppData)
                    {
                        var product = endpoint.GetMasterlistService().GetProduct(prod.ProductId);
                        
                        if (product.ProductType.ToLower() == "seed") 
                        { 
                            //czLineItem.Varieties.Add(product.Name); 
                            //czLineItem.PlantingDates.Add(prod.EndDate); 
                            seedList.Add(product.Name);
                            seedDates.Add(prod.EndDate.ToLocalTime().ToShortDateString());
                        }
                    }

                    czLineItem.Varieties = string.Join(" :: ", seedList.ToArray());
                    czLineItem.PlantingDates = string.Join(" :: ", seedDates.ToArray());
                    ////////////////////////////////////////////////////////////////////////////////////////////
                    

                    if (cropzoneDetails != null)
                    {
                        czLineItem.BoundaryArea = cropzoneDetails.BoundaryArea != null ? (double)cropzoneDetails.BoundaryArea : 0.0;                         
                        czLineItem.ReportedArea = cropzoneDetails.ReportedArea != null ? (double)cropzoneDetails.ReportedArea : 0.0;

                        if(fieldDetails != null)
                        {
                            czLineItem.Irrigation = fieldDetails.Irrigated;
                            czLineItem.WaterSource = fieldDetails.WaterSource;
                        }

                        czLineItem.Rented = cropzoneDetails.Contracts.Count() > 0 ? true : false;
                    }
                    
                    data.CropZoneLineItems.Add(czLineItem);

                    //Summaries
                    CropZoneData.CropSummary cpSummary = new CropZoneData.CropSummary() { CropId = cropzoneDetails.CropId.Id, Name = cropName, AreaValue = (decimal)(czLineItem.ReportedArea != 0 ? czLineItem.ReportedArea : czLineItem.BoundaryArea), AreaUnit = !string.IsNullOrEmpty(cropzoneDetails.ReportedAreaUnit) ? cropzoneDetails.ReportedAreaUnit : cropzoneDetails.BoundaryAreaUnit };

                    if (data.CropSummaries.Where(p => p.CropId == cpSummary.CropId).Count() > 0)
                    {
                        if (!string.IsNullOrEmpty(cpSummary.AreaUnit))
                        {
                            var summary = data.CropSummaries.SingleOrDefault(p => p.CropId == cpSummary.CropId);
                            summary.AreaValue += summary.AreaUnit == cpSummary.AreaUnit ? cpSummary.AreaValue : (decimal)(UnitFactory.GetUnitByName(cpSummary.AreaUnit).GetMeasure((double)cpSummary.AreaValue).CreateAs(UnitFactory.GetUnitByName(summary.AreaUnit)).Value);
                        }
                    }
                    else
                    {
                        data.CropSummaries.Add(cpSummary);
                    }
                }

                data.CropSummaries = data.CropSummaries.OrderBy(x => x.Name).ToList();

                //var report = new Landdb.Views.Secondary.Reports.Field.CropLayout();
                var report = new Landdb.Views.Secondary.Reports.Field.CropLayoutWithTags();
                report.DataSource = data.CropZoneLineItems.OrderBy(x => x.Farm).ThenBy(y => y.Crop);
                book.Reports.Add(report);
            }

            rs.ReportDocument = book;
            return rs;
        }
    }
}
