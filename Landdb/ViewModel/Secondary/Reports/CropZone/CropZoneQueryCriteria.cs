﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Secondary.Reports.CropZone
{
    public class CropZoneQueryCriteria
    {
        public CropZoneQueryCriteria()
        {
            SelectedCropZones = new List<CropZoneId>();
        }

        public List<CropZoneId> SelectedCropZones { get; set; }
    }
}
