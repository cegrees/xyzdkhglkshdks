﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Secondary.Reports.WorkOrder
{
    public class WorkOrderQueryCriteria
    {
        public WorkOrderQueryCriteria()
        {

        }

        public WorkOrderId SelectedWorkOrderId { get; set; }
    }
}
