﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Secondary.Reports.WorkOrder.Generators;
using Microsoft.Maps.MapControl.WPF;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using Telerik.Reporting;
using Telerik.Reporting.Processing;

namespace Landdb.ViewModel.Secondary.Reports.WorkOrder
{
    public class SingleWorkOrderViewModel : ViewModelBase
    {
        IClientEndpoint endpoint;
        Dispatcher dispatcher;
        WorkOrderId workOrderId;
        IWorkOrderReportGenerator woGenerator;

        public SingleWorkOrderViewModel(IClientEndpoint endPoint, Dispatcher dispatcher, WorkOrderId workOrderId)
        {
            this.endpoint = endPoint;
            this.dispatcher = dispatcher;
            this.workOrderId = workOrderId;

            ReportGenerators = new ObservableCollection<IWorkOrderReportGenerator>();

            UpdateReport();
            AddWorkOrderGenerators();

            HideReport = new RelayCommand(this.CloseReport);
            PDFExport = new RelayCommand(this.PDF);
            //PDFExport = new RelayCommand(this.RTF);
        }

        public RelayCommand HideReport { get; private set; }
        public RelayCommand PDFExport { get; set; }
        public void CloseReport()
        {
            Messenger.Default.Send(new HideOverlayMessage() { });
        }

        public ObservableCollection<IWorkOrderReportGenerator> ReportGenerators { get; private set; }
        public ReportSource ReportSource { get; set; }
        public IWorkOrderReportGenerator SelectedReportGenerator { get { return woGenerator; } set { woGenerator = value; UpdateReport(); } }

        void AddWorkOrderGenerators()
        {
            ReportGenerators.Add(new WorkOrderGenerator(endpoint, dispatcher, ApplicationEnvironment.CurrentCropYear));
            ReportGenerators.Add(new WorkOrderWithCostGenerator(endpoint, dispatcher, ApplicationEnvironment.CurrentCropYear));
            ReportGenerators.Add(new WorkOrderWithMapGenerator(endpoint, dispatcher, ApplicationEnvironment.CurrentCropYear));
            ReportGenerators.Add(new PortraitWorkOrderGenerator(endpoint, dispatcher, ApplicationEnvironment.CurrentCropYear, false));
            ReportGenerators.Add(new PortraitWorkOrderGenerator(endpoint, dispatcher, ApplicationEnvironment.CurrentCropYear, true));
            //ReportGenerators.Add(new WorkOrderWithMapGeneratorAndLabels(endpoint, dispatcher, ApplicationEnvironment.CurrentCropYear));
            SelectedReportGenerator = ReportGenerators[0];
        }

        public void UpdateReport()
        {
            if (SelectedReportGenerator != null)
            {
                WorkOrderQueryCriteria criteria = BuildCriteria();
                ReportSource = SelectedReportGenerator.Generate(criteria);               
                RaisePropertyChanged("BingMapLayer");
                RaisePropertyChanged("ReportSource");
            }
        }

        WorkOrderQueryCriteria BuildCriteria()
        {
            WorkOrderQueryCriteria criteria = new WorkOrderQueryCriteria();

            if (workOrderId != null)
            {
                criteria.SelectedWorkOrderId = workOrderId;
            }

            return criteria;
        }

        public void PDF()
        {
            try { 
            if (ReportSource == null) return;
            ReportSource reportExport = ReportSource;

            ReportProcessor reportProcessor = new ReportProcessor();
            System.Collections.Hashtable deviceInfo =
            new System.Collections.Hashtable();
            RenderingResult result = reportProcessor.RenderReport("PDF", reportExport, deviceInfo);

            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "Adobe Reader|*.pdf";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;

            Nullable<bool> resultSaved = saveFileDialog1.ShowDialog();

            // Process save file dialog box results 
            if (resultSaved == true)
            {
                // Save document 
                string filename = saveFileDialog1.FileName;

                try
                {
                    using (System.IO.FileStream fs = new System.IO.FileStream(filename, System.IO.FileMode.Create))
                    {
                        fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
                    }

                    System.Diagnostics.Process.Start(filename);
                }
                catch (Exception ex)
                {
                    var exception = ex;
                }

            }
            }
            catch (Exception ex)
            {
                //do nothing
            }
        }

        public void RTF()
        {
            ReportSource reportExport = ReportSource;

            ReportProcessor reportProcessor = new ReportProcessor();
            System.Collections.Hashtable deviceInfo = new System.Collections.Hashtable();
            deviceInfo.Add("StartPage", 0);
            deviceInfo.Add("EndPage", 0);
            deviceInfo.Add("RenderingMode", "Frames");
            deviceInfo.Add("UseMetafile", false);
            RenderingResult result = reportProcessor.RenderReport("RTF", reportExport, deviceInfo);

            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "Text Files (*.txt)|*.txt|RTF Files (*.rtf)|*.rtf";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;

            Nullable<bool> resultSaved = saveFileDialog1.ShowDialog();

            // Process save file dialog box results 
            if (resultSaved == true)
            {
                // Save document 
                string filename = saveFileDialog1.FileName;

                try
                {
                    using (System.IO.FileStream fs = new System.IO.FileStream(filename, System.IO.FileMode.Create))
                    {
                        fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
                    }

                    System.Diagnostics.Process.Start(filename);
                }
                catch (Exception ex) { }

                
            }

            
        }
    }
}
