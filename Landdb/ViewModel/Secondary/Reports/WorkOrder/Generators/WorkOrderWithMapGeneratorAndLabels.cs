﻿using AgC.UnitConversion;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Domain.ReadModels.Map;
using Landdb.Domain.ReadModels.ProductSetting;
using Landdb.Domain.ReadModels.Shared;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Domain.ReadModels.WorkOrder;
using Landdb.Infrastructure;
using Landdb.ReportModels.WorkOrder;
using Landdb.ViewModel.Secondary.Map;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Threading;
using Landdb.Resources;
using Telerik.Reporting;
using ThinkGeo.MapSuite.Core;
using ThinkGeo.MapSuite.WpfDesktopEdition;

namespace Landdb.ViewModel.Secondary.Reports.WorkOrder.Generators
{
    public class WorkOrderWithMapGeneratorAndLabels: IWorkOrderReportGenerator
    {
        IClientEndpoint endpoint;
        Dispatcher dispatcher;
        int cropYear;
        InMemoryFeatureLayer fieldsLayer = new InMemoryFeatureLayer();
        LayerOverlay layerOverlay = new LayerOverlay();
        List<string> mapData = new List<string>();
        string currentPPE = string.Empty;
        List<ProductWorkerProtection> ProductPPEList;
        int areadecimals = 2;

        public WorkOrderWithMapGeneratorAndLabels(IClientEndpoint endPoint, Dispatcher dispatcher, int cropYear)
        {
            this.endpoint = endPoint;
            this.dispatcher = dispatcher;
            this.cropYear = cropYear;
            ProductPPEList = new List<ProductWorkerProtection>();
            var mapsettings = endpoint.GetMapSettings();
            areadecimals = mapsettings.AreaDecimals;
        }

        public ReportSource Generate(WorkOrderQueryCriteria criteria)
        {
            ReportBook book = new ReportBook();
            var rs = new InstanceReportSource();

            var mapsettings = endpoint.GetMapSettings();
            areadecimals = mapsettings.AreaDecimals;
            var workOrderData = endpoint.GetView<WorkOrderView>(criteria.SelectedWorkOrderId).Value;
            var inventory = endpoint.GetView<InventoryListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new InventoryListView());
            var flattenedHierarchy = endpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new FlattenedTreeHierarchyView());
            var masterlist = endpoint.GetMasterlistService();


            if (workOrderData != null)
            {

                Task[] ppeTask = new Task[1];
                ppeTask[0] = Task.Factory.StartNew(() =>
                {
                    if (NetworkStatus.IsInternetAvailable())
                    {
                        ProductPPEList.AddRange(GetPPE(workOrderData.Products));
                    }
                });

                var data = new WorkOrderData();
                data.DatasourceName = ApplicationEnvironment.CurrentDataSourceName;
                //data.StreetAddress = 
                data.WorkOrderName = workOrderData.Name;
                data.WorkOrderDate = workOrderData.StartDateTime.ToLocalTime().ToShortDateString();
                data.WorkOrderType = workOrderData.ApplicationStrategy.ToString();
                data.TotalArea = workOrderData.CropZones.Sum(x => x.Area.Value).ToString("N2") + " " + workOrderData.CropZones.FirstOrDefault(x=> x.Area.Unit != null).Area.Unit.AbbreviatedDisplay;

                data.TankInformationVisibility = workOrderData.TankInformation == null ? false : true;
                if (workOrderData.TankInformation != null)
                {
                    data.TankSizeValue = workOrderData.TankInformation.TankSizeValue;
                    data.TankSizeUnit = workOrderData.TankInformation.TankSizeUnit;
                    data.CarrierPerAreaValue = workOrderData.TankInformation.CarrierPerAreaValue;
                    data.CarrierPerAreaUnit = workOrderData.TankInformation.CarrierPerAreaUnit;
                    data.TotalCarrierValue = workOrderData.TankInformation.TotalCarrierValue;
                    data.TotalCarrierUnit = workOrderData.TankInformation.TotalCarrierUnit;
                    data.TankCount = workOrderData.TankInformation.TankCount > 0 ? (decimal?)workOrderData.TankInformation.TankCount : null;
                }
                var cnt = 0;
                foreach (var prod in workOrderData.Products)
                {
                    var masterlistProduct = masterlist.GetProduct(prod.Id);
                    ReiPhi reiPhi = new ReiPhi();
                    List<ReiPhi> reiPhis = new List<ReiPhi>();
                    reiPhis.AddRange(masterlist.GetProductReiPhis(prod.Id));
                    //////////////////////////////////////////////////////////

                    List<CropId> cropIds = new List<CropId>();
                    var czCropIds = from m in workOrderData.CropZones
                                    select endpoint.GetView<CropZoneDetailsView>(m.Id).Value.CropId;
                    cropIds.AddRange(czCropIds);

                    var parentCropIds = (from cid in cropIds
                                         let mlCrop = masterlist.GetCrop(cid)
                                         where mlCrop.ParentId.HasValue
                                         select new CropId(mlCrop.ParentId.Value)).ToList();
                    cropIds.AddRange(parentCropIds);

                    var prodSettings = endpoint.GetView<ProductSettingsView>(new ProductSettingId(ApplicationEnvironment.CurrentDataSourceId, prod.Id));
                    var cropYearItem = prodSettings.HasValue && prodSettings.Value.ItemsByYear.ContainsKey(ApplicationEnvironment.CurrentCropYear) ? prodSettings.Value.ItemsByYear[ApplicationEnvironment.CurrentCropYear] : null;

                    List<ProductSettingItem> cropItems = new List<ProductSettingItem>();
                    if (cropYearItem != null)
                    {
                        cropItems = (from c in cropYearItem
                                     where cropIds.Contains(c.CropId)
                                     select c).ToList();
                    }

                    List<ReiPhi> newReiPhis = new List<ReiPhi>();
                    foreach (var reiphi in reiPhis)
                    {
                        if (cropIds.Any(c => c.Id == reiphi.Crop))
                        {
                            newReiPhis.Add(reiphi);
                        }
                    }

                    foreach (var item in cropItems)
                    {
                        var rP = (from r in newReiPhis
                                 where r.Crop == item.CropId.Id
                                 select r).SingleOrDefault();
                        newReiPhis.Remove(rP);
                        newReiPhis.Add(new ReiPhi() { Crop = item.CropId.Id, Phi = (double)item.PhiValue.Value, PhiU = item.PhiUnit, Rei = (double)item.ReiValue.Value, ReiU = item.ReiUnit });
                    }
                    /////////////////////////////////////////////////////////
                    var therei = newReiPhis != null && newReiPhis.Count() > 0 ? (double)(from r in newReiPhis select r.Rei).Max() : new double?();
                    var thephi = newReiPhis != null && newReiPhis.Count() > 0 ? (double)(from r in newReiPhis select r.Phi).Max() : new double?();
                    reiPhi.Rei = reiPhi.Rei == null || reiPhi.Rei < therei ? therei : reiPhi.Rei;
                    reiPhi.Phi = reiPhi.Phi == null || reiPhi.Phi < thephi ? thephi : reiPhi.Phi;

                    var aiList = from i in masterlistProduct.ActiveIngredients
                                 select masterlist.GetActiveIngredientDisplay(new ActiveIngredientId(i.Id));

                    string ai = string.Join(",", aiList.ToArray());

                    var registrationNumber = masterlistProduct.RegistrationNumber;
                    string epaNoTrailingChar = string.Empty;
                    if (registrationNumber == null) { epaNoTrailingChar = string.Empty; }
                    else
                    {
                        Regex epaNumberRegex = new Regex("([1-9][0-9]*)");

                        var matches = from m in epaNumberRegex.Matches(registrationNumber).Cast<Match>()
                                      select m.Value;
                        epaNoTrailingChar = string.Join("-", matches.ToArray());
                    }

                    var displayRate = DisplayMeasureConverter.ConvertToRateDisplayMeasure(prod.Id, prod.RateValue, prod.RateUnit, masterlistProduct.Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);
                    var displayTotal = DisplayMeasureConverter.ConvertToTotalDisplayMeasure(prod.Id, (decimal)prod.TotalProductValue, prod.TotalProductUnit, masterlistProduct.Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);
                    var perTankMeasure = UnitFactory.GetUnitByName(prod.RatePerTankUnit).GetMeasure((double)prod.RatePerTankValue, masterlistProduct.Density);
                    var displayPerTank = DisplayMeasureConverter.ConvertToRatePerTankDisplayMeasure(prod.Id, perTankMeasure, endpoint, ApplicationEnvironment.CurrentDataSourceId);
                    WorkOrderData.ProductLineItemData prodData = new WorkOrderData.ProductLineItemData()
                    {
                        ApplicationMethod = string.IsNullOrEmpty(prod.ApplicationMethod) ? string.Empty : prod.ApplicationMethod,
                        EPANumber = epaNoTrailingChar,
                        Pest = prod.TargetPest == null ? string.Empty : prod.TargetPest.CommonName,
                        ProductName = masterlistProduct.Name,
                        RateUnit = displayRate.Unit.Name,
                        RateValue = displayRate.Value,
                        RatePerTankUnit = data.WorkOrderType == "ByRatePerTank" ? displayPerTank.Unit.Name : prod.RateUnit,
                        RatePerTank = data.WorkOrderType == "ByRatePerTank" ? displayPerTank.Value : 0.0,
                        TotalUnit = displayTotal.Unit.Name,
                        TotalValue = displayTotal.Value,
                        PHI = reiPhi != null && reiPhi.Phi != null ? reiPhi.Phi.ToString() : "*",
                        REI = reiPhi != null && reiPhi.Rei != null ? reiPhi.Rei.ToString() : "*",
                        Active = ai,
                        RateType = data.WorkOrderType == "ByRatePerTank" ? Strings.RateTank_Text : Strings.RateType_Text,
                        RatePer100 = prod.RatePerTankValue > 0 ? ((double)prod.RatePerTankValue / ((double)workOrderData.TankInformation.TankSizeValue / 100)) : 0.0,
                        RatePer100Unit = displayPerTank.Unit != null ? displayPerTank.Unit.Name : string.Empty,
                        TotalArea = (decimal)workOrderData.TotalArea.Value,
                        TotalAreaUnit = workOrderData.TotalArea.Unit,
                        PercentApplied = prod.PercentApplied,
                        RowCount = cnt,

                    };

                    List<WorkOrderData.AssociatedProductItem> associatedProducts = new List<WorkOrderData.AssociatedProductItem>();

                    foreach (var associate in prod.AssociatedProducts)
                    {
                        var mlp = masterlist.GetProduct(associate.ProductId);
                        var item = new WorkOrderData.AssociatedProductItem();
                        item.CustomRateType = associate.CustomRateType;
                        item.CustomRateValue = associate.CustomProductValue;
                        item.HasCost = associate.HasCost;
                        item.ProductName = associate.ProductName;
                        item.RatePerAreaUnit = UnitFactory.GetPackageSafeUnit(associate.RatePerAreaUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                        item.RatePerAreaValue = associate.RatePerAreaValue;
                        item.TotalProductUnit = UnitFactory.GetPackageSafeUnit(associate.TotalProductUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                        item.TotalProductValue = associate.TotalProductValue;
                        IUnit customUnit = UnitFactory.GetPackageSafeUnit(associate.CustomProductUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);

                        switch (associate.CustomRateType)
                        {
                            case "ByBag":
                                item.DisplayCustomUnit = new CompositeUnit(customUnit.AbbreviatedDisplay, 1, Strings.Unit_Bag_Text);
                                break;
                            case "ByCWT":
                                item.DisplayCustomUnit = new CompositeUnit(customUnit.AbbreviatedDisplay, 100, Strings.Unit_Weight_Text);
                                break;
                            case "ByRow":
                                item.DisplayCustomUnit = new CompositeUnit(customUnit.AbbreviatedDisplay, 1000, Strings.Unit_LengthRow_Text);
                                break;
                            case "BySeed":
                                var parentProd = masterlist.GetProduct(prod.Id);
                                item.DisplayCustomUnit = new CompositeUnit(customUnit.AbbreviatedDisplay, 1, parentProd.StdUnit);
                                break;
                            default:

                                break;
                        }

                        if (associate.HasCost)
                        {
                            var associateInvProd = inventory.Products.SingleOrDefault(x => x.Key == associate.ProductId).Value;

                            var avgCost = associateInvProd != null ? associateInvProd.AveragePriceValue : 0;
                            var avgCostUnit = associateInvProd != null && !string.IsNullOrEmpty(associateInvProd.AveragePriceUnit) ? UnitFactory.GetPackageSafeUnit(associateInvProd.AveragePriceUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit) : null;
                            var totalAssociateProd = UnitFactory.GetPackageSafeUnit(associate.TotalProductUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit).GetMeasure((double)associate.TotalProductValue, mlp.Density);
                            var convertedValue = avgCostUnit != null && totalAssociateProd.Unit != avgCostUnit && totalAssociateProd.CanConvertTo(avgCostUnit) ? totalAssociateProd.GetValueAs(avgCostUnit) : totalAssociateProd.Value;
                            item.TotalCost = (decimal)convertedValue;
                            //TotalAvgCost += avgCost != 0 ? (decimal)(convertedValue * avgCost) : 0;


                            if (associate.SpecificCostPerUnitValue != 0 && !string.IsNullOrEmpty(associate.SpecificCostPerUnitUnit))
                            {
                                var specificUnit = UnitFactory.GetPackageSafeUnit(associate.SpecificCostPerUnitUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                                var convertedSpecificValue = totalAssociateProd.Unit != specificUnit && totalAssociateProd.CanConvertTo(specificUnit) ? totalAssociateProd.GetValueAs(specificUnit) : totalAssociateProd.Value;
                                var specificTotalCost = (decimal)convertedSpecificValue * associate.SpecificCostPerUnitValue;
                                item.TotalCost = specificTotalCost;
                                //TotalSpecificCost += specificTotalCost;
                            }
                        }

                        prodData.AssociatedProducts.Add(item);
                    }

                    data.Products.Add(prodData);
                    cnt++;
                }

                List<Feature> filteredMapData = new List<Feature>();

                foreach (var cz in workOrderData.CropZones)
                {
                    var czDetails = endpoint.GetView<CropZoneDetailsView>(cz.Id).GetValue(new CropZoneDetailsView());

                    var czTreeData = (from f in flattenedHierarchy.Items
                                      where f.CropZoneId == cz.Id
                                      select f).FirstOrDefault();

                    //var reportedArea = ReportedArea(czDetails);
                    var area  = workOrderData.CropZones.SingleOrDefault(x => x.Id == cz.Id).Area;
                    var fieldDetails = endpoint.GetView<FieldDetailsView>(czDetails.FieldId).Value;
                    var centerLatLon = string.IsNullOrEmpty(czDetails.CenterLatLong) ? fieldDetails.CenterLatLong : czDetails.CenterLatLong;
                    WorkOrderData.FieldLineItemData field = new WorkOrderData.FieldLineItemData()
                    {
                        Area = area.Value,
                        CenterLatLong = centerLatLon,
                        Crop = masterlist.GetCropDisplay(czDetails.CropId),
                        CropZone = czDetails.Name,
                        Field = czTreeData != null ? czTreeData.FieldName : fieldDetails.FieldNameByCropYear[cropYear], //.Name,
                        Farm = czTreeData != null ? czTreeData.FarmName : Strings.UnknownFarm_Text,
                    };

                    data.Fields.Add(field);

                    var maps = endpoint.GetView<ItemMap>(cz.Id);
                    try
                    {
                        if (maps.HasValue)
                        {
                            if (maps.Value.MostRecentMapItem != null && maps.Value.MostRecentMapItem.MapData != string.Empty && maps.Value.MostRecentMapItem.MapData != "MULTIPOLYGON EMPTY") {
                                //string customarealabel = GetCustomCropZoneName(cz.Id.Id, czDetails.Name);
                                string customareaonlylabel = GetCustomAreaLabel(cz.Id.Id, false);
                                string customarealabel = DisplayLabelWithArea(czDetails.Name, customareaonlylabel);
                                IDictionary<string, string> columnvalues = new Dictionary<string, string>();
                                columnvalues.Add(@"LdbLabel", czDetails.Name);
                                columnvalues.Add(@"AreaLabel", customarealabel);
                                columnvalues.Add(@"AreaOnlyLabel", customareaonlylabel);
                                Feature newfeature = new Feature(maps.Value.MostRecentMapItem.MapData, cz.Id.Id.ToString(), columnvalues);
                                filteredMapData.Add(newfeature);
                            }
                        }
                    }
                    finally { }
                }
                var converter = new ConvertToBitmap(filteredMapData, true, endpoint, 20, workOrderData.EnvironmentalZoneDistanceValue, workOrderData.EnvironmentalZoneDistanceUnit, workOrderData.Severity);
                data.MapImage = converter.BitMap;       

                foreach (var applicator in workOrderData.Applicators)
                {
                    WorkOrderData.ApplicatorLineItemData appLineItem = new WorkOrderData.ApplicatorLineItemData()
                    {
                        Name = applicator.PersonName,
                        Company = applicator.CompanyName,
                        LicenseNumber = applicator.LicenseNumber,
                        ExpirationDate = applicator.Expires,
                        DisplayName = string.Format("{0} ({1})", applicator.PersonName, applicator.CompanyName),
                    };

                    data.Applicators.Add(appLineItem);
                }

                //add all the ppe info into data....
                Task.WaitAll(ppeTask);
                foreach (var prod in ProductPPEList)
                {
                    foreach (var ppe in prod.WorkerProtections)
                    {
                        var mlp = masterlist.GetProduct(prod.ProductId);
                        data.WorkerProtectionItems.Add(new WorkOrderData.WorkerProtectionItem() { ProductName = mlp.Name, PPE = ppe.PPE, ReEntry = ppe.PPEReentry, SignalWord = mlp.SignalWord, RestrictedUse = mlp.RestrictedUse ? "True" : "False"});
                    }
                }

                data.Authorizer = workOrderData.Authorization != null ? $"{Strings.AuthorizedBy_Text} " + workOrderData.Authorization.PersonName : string.Empty;
                data.AuthorizerDate = workOrderData.Authorization != null ? workOrderData.Authorization.AuthorizationDate : DateTime.Now;

                data.CropYear = cropYear;
                data.Notes = workOrderData.Notes;

                var report = new Landdb.Views.Secondary.Reports.Work_Order.LandscapeWorkOrder(); //.StandardWorkOrder();
                report.DataSource = data;
                report.Name = Strings.ReportName_WorkOrder_Text;

                book.Reports.Add(report);

                var newWOdata = new WorkOrderData();
                newWOdata.MapImage = data.MapImage;
                newWOdata.CropYear = data.CropYear;
                var mapReportPage = new Landdb.Views.Secondary.Reports.Work_Order.WorkOrderLargeMap();
                mapReportPage.DataSource = newWOdata;
                mapReportPage.Name = Strings.Map_Text;

                ////EXAMPLE OF HOW TO BIND TO A SUB REPORT
                var mainReport = mapReportPage;
                var itemDetail = (Telerik.Reporting.SubReport)mainReport.Items.Find("subReport1", true)[0];
                var repSource = (InstanceReportSource)itemDetail.ReportSource;
                var subReport = (Telerik.Reporting.Report)repSource.ReportDocument;
                var odsItems = (Telerik.Reporting.ObjectDataSource)subReport.DataSource;
                odsItems.DataSource = data.Products;

                book.Reports.Add(mapReportPage);
            }
            
            rs.ReportDocument = book;
            return rs;
        }

        public string DisplayName
        {
            get { return Strings.GeneratorName_WorkOrderWithMapAndLabels_Text; }
        }

        private double ReportedArea(CropZoneDetailsView czDetails)
        {
            if (czDetails.ReportedArea != null)
            {
                return (double)czDetails.ReportedArea;
            }
            else if (czDetails.ReportedArea == null && czDetails.BoundaryArea != null)
            {
                return (double)czDetails.BoundaryArea;
            }
            else if (czDetails.ReportedArea == null && czDetails.BoundaryArea == null)
            {
                return 0.0;
            }
            else
            {
                return 0.0;
            }
        }

        public List<ProductWorkerProtection> GetPPE(List<IncludedProduct> includedProducts)
        {
            List<ProductWorkerProtection> ppeList = new List<ProductWorkerProtection>();
            foreach (var prod in includedProducts)
            {
                try
                {
                    string dataText = string.Empty;

                    //create task....and wait for it to complete...
                    Task getPPE = Task.Factory.StartNew(() =>
                    {
                        dataText = GetWorkerProtectionData(prod.Id);
                    });

                    getPPE.Wait();
                    JObject data = JObject.Parse(dataText);

                    var ppe = from d in JArray.Parse(data["ProductWorkerProtections"].ToString())
                              select JArray.Parse(d["WorkerProtections"].ToString());

                    var workerProtectionList = from d in JArray.Parse(data["ProductWorkerProtections"].ToString())
                                               from i in JArray.Parse(d["WorkerProtections"].ToString())
                                               select new WorkerProtection { PPE = i["PPE"] != null ? i["PPE"].ToString() : string.Empty, PPEReentry = i["PPEReentry"] != null ? i["PPEReentry"].ToString() : string.Empty };
                    var prodWorkProtection = new ProductWorkerProtection() { ProductId = prod.Id, WorkerProtections = workerProtectionList.ToList() };
                    ppeList.Add(prodWorkProtection);
                }
                catch (Exception x)
                {

                }

            }

            return ppeList;
        }

        public string GetWorkerProtectionData(ProductId productId)
        {
            var requestUriFormat = @"{0}/workerprotections?format=json&c=en-US&u=Y29yZXkucGVya2lucw%3D%3D&p=Y29yZXlwMQ%3D%3D&ProductIds={1}";
            var requestUri = string.Format(requestUriFormat, Infrastructure.ApplicationEnvironment.MasterlistRemoteUri, productId.Id);

            WebClient client = new WebClient();
            client.Headers.Add("Authorization", "basic QWdDQVBJOlJhbmRvbU1vbmtleVN1cmZzT25MYXZhIQ==");
            string data = client.DownloadString(new Uri(requestUri));

            return data;
        }

        //private string GetCustomCropZoneName(Guid Id, string name) {
        //    string stringid = Id.ToString();
        //    string labelname = name;
        //    var fieldMaybe = endpoint.GetView<CropZoneDetailsView>(new CropZoneId(ApplicationEnvironment.CurrentDataSourceId, Id));
        //    fieldMaybe.IfValue(field => {
        //        if (field.ReportedArea != null && !string.IsNullOrWhiteSpace(field.ReportedAreaUnit)) {
        //            labelname = DisplaySelectedAreaDecimals(name, field.ReportedArea.Value, field.ReportedAreaUnit);
        //        }
        //        else if (field.BoundaryArea != null && !string.IsNullOrWhiteSpace(field.BoundaryAreaUnit)) {
        //            labelname = DisplaySelectedAreaDecimals(name, field.BoundaryArea.Value, field.BoundaryAreaUnit);
        //        }
        //        else {
        //            labelname = name;
        //        }

        //    });
        //    return labelname;
        //}

        //private string DisplaySelectedAreaDecimals(string name, double unitvalue, string unitname) {
        //    IUnit _unit = UnitFactory.GetUnitByName(unitname);
        //    string decimalstring = "N" + areadecimals.ToString("N0");
        //    return name + "\r\n" + unitvalue.ToString(decimalstring);
        //}

        private string DisplayLabelWithArea(string name, string customarealabel) {
            return name + "\r\n" + customarealabel;
        }

        private string GetCustomAreaLabel(Guid Id, bool isfieldlabel) {
            string stringid = Id.ToString();
            string labelname = string.Empty;
            if (isfieldlabel) {
                var fieldMaybe = endpoint.GetView<FieldDetailsView>(new FieldId(ApplicationEnvironment.CurrentDataSourceId, Id));
                fieldMaybe.IfValue(field => {
                    labelname = string.Empty;
                    if (field.ReportedArea != null && !string.IsNullOrWhiteSpace(field.ReportedAreaUnit)) {
                        labelname = DisplaySelectedAreaOnlyDecimals(field.ReportedArea.Value, field.ReportedAreaUnit);
                    }
                    else if (field.BoundaryArea != null && !string.IsNullOrWhiteSpace(field.BoundaryAreaUnit)) {
                        labelname = DisplaySelectedAreaOnlyDecimals(field.BoundaryArea.Value, field.BoundaryAreaUnit);
                    }
                });
            }
            else {
                var fieldMaybe = endpoint.GetView<CropZoneDetailsView>(new CropZoneId(ApplicationEnvironment.CurrentDataSourceId, Id));
                fieldMaybe.IfValue(field => {
                    labelname = string.Empty;
                    if (field.ReportedArea != null && !string.IsNullOrWhiteSpace(field.ReportedAreaUnit)) {
                        labelname = DisplaySelectedAreaOnlyDecimals(field.ReportedArea.Value, field.ReportedAreaUnit);
                    }
                    else if (field.BoundaryArea != null && !string.IsNullOrWhiteSpace(field.BoundaryAreaUnit)) {
                        labelname = DisplaySelectedAreaOnlyDecimals(field.BoundaryArea.Value, field.BoundaryAreaUnit);
                    }
                });
            }
            return labelname;
        }

        private string DisplaySelectedAreaOnlyDecimals(double unitvalue, string unitname) {
            IUnit _unit = UnitFactory.GetUnitByName(unitname);
            string decimalstring = "N" + areadecimals.ToString("N0");
            return unitvalue.ToString(decimalstring);
        }
    }
}
