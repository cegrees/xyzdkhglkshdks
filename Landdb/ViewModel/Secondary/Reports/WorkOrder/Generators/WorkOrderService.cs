﻿using AgC.UnitConversion;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Domain.ReadModels.Map;
using Landdb.Domain.ReadModels.ProductSetting;
using Landdb.Domain.ReadModels.Shared;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Domain.ReadModels.WorkOrder;
using Landdb.Infrastructure;
using Landdb.ReportModels.WorkOrder;
using Landdb.ReportServices;
using Landdb.ViewModel.Secondary.Map;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Landdb.Resources;
using ThinkGeo.MapSuite.Core;

namespace Landdb.ViewModel.Secondary.Reports.WorkOrder.Generators
{
    public class WorkOrderService
    {
        IClientEndpoint endpoint;
        WorkOrderQueryCriteria criteria;
        int cropYear;
        FlattenedTreeHierarchyView flattenedHierarchy;
        WorkOrderView workOrderData;
        List<ProductWorkerProtection> ProductPPEList;
        int areadecimals = 2;

        public WorkOrderService(IClientEndpoint endPoint, WorkOrderQueryCriteria criteria)
        {
            this.endpoint = endPoint;
            this.criteria = criteria;
            cropYear = ApplicationEnvironment.CurrentCropYear;
            flattenedHierarchy = endpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new FlattenedTreeHierarchyView());
            workOrderData = endpoint.GetView<WorkOrderView>(criteria.SelectedWorkOrderId).Value;
            ProductPPEList = new List<ProductWorkerProtection>();
        }

        public WorkOrderData GenerateWorkOrderData()
        {
            
            var inventory = endpoint.GetView<InventoryListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new InventoryListView());
            var defaultAreaUnit = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit);
            var mapsettings = endpoint.GetMapSettings();
            areadecimals = mapsettings.AreaDecimals;
            var masterlist = endpoint.GetMasterlistService();
            var data = new WorkOrderData();
            if (workOrderData != null)
            {

                Task[] ppeTask = new Task[1];
                ppeTask[0] = Task.Factory.StartNew(() =>
                {
                    if (NetworkStatus.IsInternetAvailable())
                    {
                        ProductPPEList.AddRange(GetPPE(workOrderData.Products));
                    }
                });

                data = new WorkOrderData();
                data.DatasourceName = ApplicationEnvironment.CurrentDataSourceName;
                //data.StreetAddress = 
                data.WorkOrderName = workOrderData.Name;
                data.WorkOrderDate = workOrderData.StartDateTime.ToLocalTime().ToShortDateString();
                data.WorkOrderType = workOrderData.ApplicationStrategy.ToString();
                data.TotalArea = workOrderData.CropZones.Sum(x => x.Area.Value).ToString("N2") + " " + workOrderData.CropZones.FirstOrDefault(x => x.Area.Unit != null).Area.Unit.AbbreviatedDisplay;
                data.TimingEvent = workOrderData.TimingEvent;
                data.TankInformationVisibility = workOrderData.TankInformation == null ? false : true;
                if (workOrderData.TankInformation != null)
                {
                    data.TankSizeValue = workOrderData.TankInformation.TankSizeValue;
                    data.TankSizeUnit = workOrderData.TankInformation.TankSizeUnit;
                    data.CarrierPerAreaValue = workOrderData.TankInformation.CarrierPerAreaValue;
                    data.CarrierPerAreaUnit = workOrderData.TankInformation.CarrierPerAreaUnit;
                    data.TotalCarrierValue = workOrderData.TankInformation.TotalCarrierValue;
                    data.TotalCarrierUnit = workOrderData.TankInformation.TotalCarrierUnit;
                    data.TankCount = workOrderData.TankInformation.TankCount > 0 ? (decimal?)workOrderData.TankInformation.TankCount : null;
                }
                var cnt = 0;
                foreach (var prod in workOrderData.Products)
                {
                    var masterlistProduct = masterlist.GetProduct(prod.Id);
                    var czCropIds = from m in workOrderData.CropZones
                                    select endpoint.GetView<CropZoneDetailsView>(m.Id).Value.CropId;
                    var productService = new ProductServices(endpoint);
                    ReiPhi reiPhi = productService.ReturnLargestReiPhi(prod.Id, czCropIds.ToList());

                    var aiList = from i in masterlistProduct.ActiveIngredients
                                 select masterlist.GetActiveIngredientDisplay(new ActiveIngredientId(i.Id));

                    string ai = string.Join(",", aiList.ToArray());

                    var registrationNumber = masterlistProduct.RegistrationNumber;
                    string epaNoTrailingChar = string.Empty;
                    if (registrationNumber == null) { epaNoTrailingChar = string.Empty; }
                    else
                    {
                        Regex epaNumberRegex = new Regex("([1-9][0-9]*)");

                        var matches = from m in epaNumberRegex.Matches(registrationNumber).Cast<Match>()
                                      select m.Value;
                        epaNoTrailingChar = string.Join("-", matches.ToArray());
                    }

                    var invProd = inventory.Products[prod.Id];
                    var totalProd = UnitFactory.GetPackageSafeUnit(prod.TotalProductUnit, masterlistProduct.StdUnit, masterlistProduct.StdFactor, masterlistProduct.StdPackageUnit).GetMeasure((double)prod.TotalProductValue, masterlist.GetProduct(prod.Id).Density);
                    var avgPriceUnit = !string.IsNullOrEmpty( invProd.AveragePriceUnit ) ? UnitFactory.GetPackageSafeUnit(invProd.AveragePriceUnit, masterlistProduct.StdUnit, masterlistProduct.StdFactor, masterlistProduct.StdPackageUnit) : null;
                    var convertedProdValue = !string.IsNullOrEmpty(invProd.AveragePriceUnit) && totalProd.CanConvertTo(avgPriceUnit) ? totalProd.GetValueAs(avgPriceUnit) : totalProd.Value;
                    var totalCost = convertedProdValue * invProd.AveragePriceValue;
                    var specificCostUnit = !string.IsNullOrEmpty(prod.SpecificCostUnit) ? UnitFactory.GetPackageSafeUnit(prod.SpecificCostUnit, masterlistProduct.StdUnit, masterlistProduct.StdFactor, masterlistProduct.StdPackageUnit) : null; //
                    var convertedForSpecific = !string.IsNullOrEmpty(prod.SpecificCostUnit) && totalProd.CanConvertTo(specificCostUnit) ? totalProd.GetValueAs(specificCostUnit) : totalProd.Value;
                    totalCost = prod.SpecificCostPerUnit.HasValue ? (double)prod.SpecificCostPerUnit.Value * convertedForSpecific : totalCost;

                    var displayRate = DisplayMeasureConverter.ConvertToRateDisplayMeasure(prod.Id, prod.RateValue, prod.RateUnit, masterlistProduct.Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);
                    var displayTotal = DisplayMeasureConverter.ConvertToTotalDisplayMeasure(prod.Id, (decimal)prod.TotalProductValue, prod.TotalProductUnit, masterlistProduct.Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);
                    var perTankMeasure = UnitFactory.GetPackageSafeUnit(prod.RatePerTankUnit, masterlistProduct.StdUnit, masterlistProduct.StdFactor, masterlistProduct.StdPackageUnit).GetMeasure((double)prod.RatePerTankValue, masterlistProduct.Density); // UnitFactory.GetUnitByName(prod.RatePerTankUnit).GetMeasure((double)prod.RatePerTankValue, masterlistProduct.Density);
                    var displayPerTank = DisplayMeasureConverter.ConvertToRatePerTankDisplayMeasure(prod.Id, perTankMeasure, endpoint, ApplicationEnvironment.CurrentDataSourceId);

                    Measure displayPer100 = null;
                    if (data.WorkOrderType == "ByRatePerTank")
                    {
                        var ratePer100Value = data.WorkOrderType == "ByRatePerTank" ? ((double)prod.RatePerTankValue / ((double)workOrderData.TankInformation.TankSizeValue / 100)) : 0;
                        var ratePer100Unit = data.WorkOrderType == "ByRatePerTank" ? UnitFactory.GetPackageSafeUnit(prod.RatePerTankUnit, masterlistProduct.StdUnit, masterlistProduct.StdFactor, masterlistProduct.StdPackageUnit) : null;
                        var ratePer100Measure = ratePer100Unit.GetMeasure(ratePer100Value, masterlistProduct.Density);
                        displayPer100 = DisplayMeasureConverter.ConvertToRatePer100DisplayMeasure(prod.Id, ratePer100Measure, endpoint, ApplicationEnvironment.CurrentDataSourceId);
                    }

                    WorkOrderData.ProductLineItemData prodData = new WorkOrderData.ProductLineItemData()
                    {
                        ApplicationMethod = string.IsNullOrEmpty(prod.ApplicationMethod) ? string.Empty : prod.ApplicationMethod,
                        EPANumber = epaNoTrailingChar,
                        Pest = prod.TargetPest == null ? string.Empty : (!string.IsNullOrEmpty(prod.TargetPest.CommonName) ? prod.TargetPest.CommonName : prod.TargetPest.LatinName),
                        ProductName = masterlistProduct.Name,
                        RateUnit = displayRate.Unit.Name,
                        RateValue = displayRate.Value,
                        RatePerTankUnit = data.WorkOrderType == "ByRatePerTank" ? displayPerTank.Unit.Name : prod.RateUnit,
                        RatePerTank = data.WorkOrderType == "ByRatePerTank" ? displayPerTank.Value : 0.0,
                        TotalUnit = displayTotal.Unit.Name,
                        TotalValue = displayTotal.Value,
                        PHI = reiPhi != null && reiPhi.Phi != null ? reiPhi.Phi.ToString() : "*",
                        REI = reiPhi != null && reiPhi.Rei != null ? reiPhi.Rei.ToString() : "*",
                        Active = ai,
                        RateType = data.WorkOrderType == "ByRatePerTank" ? Strings.RateTank_Text : Strings.RateType_Text,
                        RatePer100 = data.WorkOrderType == "ByRatePerTank" ? displayPer100.Value : 0,
                        RatePer100Unit = data.WorkOrderType == "ByRatePerTank" ? displayPer100.Unit.Name : string.Empty,
                        TotalArea = (decimal)workOrderData.TotalArea.Value,
                        TotalAreaUnit = workOrderData.TotalArea.Unit,
                        PercentApplied = prod.PercentApplied,
                        RowCount = cnt,
                        TotalCost = totalCost,
                        CostPerArea = totalCost > 0 ? totalCost / workOrderData.TotalArea.Value : 0.0,
                        DefaultAreaUnit = defaultAreaUnit.AbbreviatedDisplay,
                    };

                    List<WorkOrderData.AssociatedProductItem> associatedProducts = new List<WorkOrderData.AssociatedProductItem>();

                    foreach (var associate in prod.AssociatedProducts)
                    {
                        var mlp = masterlist.GetProduct(associate.ProductId);
                        var item = new WorkOrderData.AssociatedProductItem();
                        item.CustomRateType = associate.CustomRateType;
                        item.CustomRateValue = associate.CustomProductValue;
                        item.HasCost = associate.HasCost;
                        item.ProductName = associate.ProductName;
                        item.RatePerAreaUnit = UnitFactory.GetPackageSafeUnit(associate.RatePerAreaUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                        item.RatePerAreaValue = associate.RatePerAreaValue;
                        item.TotalProductUnit = UnitFactory.GetPackageSafeUnit(associate.TotalProductUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                        item.TotalProductValue = associate.TotalProductValue;
                        IUnit customUnit = UnitFactory.GetPackageSafeUnit(associate.CustomProductUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);

                        switch (associate.CustomRateType)
                        {
                            case "ByBag":
                                item.DisplayCustomUnit = new CompositeUnit(customUnit.AbbreviatedDisplay, 1, Strings.Unit_Bag_Text);
                                break;
                            case "ByCWT":
                                item.DisplayCustomUnit = new CompositeUnit(customUnit.AbbreviatedDisplay, 100, Strings.Unit_Weight_Text);
                                break;
                            case "ByRow":
                                item.DisplayCustomUnit = new CompositeUnit(customUnit.AbbreviatedDisplay, 1000, Strings.Unit_LengthRow_Text);
                                break;
                            case "BySeed":
                                var parentProd = masterlist.GetProduct(prod.Id);
                                item.DisplayCustomUnit = new CompositeUnit(customUnit.AbbreviatedDisplay, 1, parentProd.StdUnit);
                                break;
                            default:

                                break;
                        }

                        if (associate.HasCost)
                        {
                            var associateInvProd = inventory.Products.SingleOrDefault(x => x.Key == associate.ProductId).Value;

                            var avgCost = associateInvProd != null ? associateInvProd.AveragePriceValue : 0;
                            var avgCostUnit = associateInvProd != null && !string.IsNullOrEmpty(associateInvProd.AveragePriceUnit) ? UnitFactory.GetPackageSafeUnit(associateInvProd.AveragePriceUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit) : null;
                            var totalAssociateProd = UnitFactory.GetPackageSafeUnit(associate.TotalProductUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit).GetMeasure((double)associate.TotalProductValue, mlp.Density);
                            var convertedValue = avgCostUnit != null && totalAssociateProd.Unit != avgCostUnit && totalAssociateProd.CanConvertTo(avgCostUnit) ? totalAssociateProd.GetValueAs(avgCostUnit) : totalAssociateProd.Value;
                            item.TotalCost = (decimal)convertedValue;
                            //TotalAvgCost += avgCost != 0 ? (decimal)(convertedValue * avgCost) : 0;


                            if (associate.SpecificCostPerUnitValue != 0 && !string.IsNullOrEmpty(associate.SpecificCostPerUnitUnit))
                            {
                                var specificUnit = UnitFactory.GetPackageSafeUnit(associate.SpecificCostPerUnitUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                                var convertedSpecificValue = totalAssociateProd.Unit != specificUnit && totalAssociateProd.CanConvertTo(specificUnit) ? totalAssociateProd.GetValueAs(specificUnit) : totalAssociateProd.Value;
                                var specificTotalCost = (decimal)convertedSpecificValue * associate.SpecificCostPerUnitValue;
                                item.TotalCost = specificTotalCost;
                                //TotalSpecificCost += specificTotalCost;
                            }
                        }

                        prodData.AssociatedProducts.Add(item);
                    }

                    data.Products.Add(prodData);
                    cnt++;
                }

                data.TotalCost = data.Products.Sum(x => x.TotalCost);
                List<string> filteredMapData = new List<string>();
                List<string> filteredDetailedMapData = new List<string>();
                foreach (var cz in workOrderData.CropZones)
                {
                    var czDetails = endpoint.GetView<CropZoneDetailsView>(cz.Id).GetValue(new CropZoneDetailsView());

                    var czTreeData = (from f in flattenedHierarchy.Items
                                      where f.CropZoneId == cz.Id
                                      select f).FirstOrDefault();

                    //var reportedArea = ReportedArea(czDetails);
                    var area = workOrderData.CropZones.SingleOrDefault(x => x.Id == cz.Id).Area;
                    var fieldDetails = endpoint.GetView<FieldDetailsView>(czDetails.FieldId).Value;
                    var centerLatLon = string.IsNullOrEmpty(czDetails.CenterLatLong) ? fieldDetails.CenterLatLong : czDetails.CenterLatLong;

                    if (!string.IsNullOrEmpty(centerLatLon))
                    {
                        centerLatLon = DecimalDegrees.ddstring_To_DMs(centerLatLon, mapsettings.DisplayCoordinateFormat);
                    }

                    WorkOrderData.FieldLineItemData field = new WorkOrderData.FieldLineItemData()
                    {
                        Area = area.Value,
                        CenterLatLong = centerLatLon,
                        Crop = masterlist.GetCropDisplay(czDetails.CropId),
                        CropZone = czDetails.Name,
                        Field = czTreeData != null ? czTreeData.FieldName : endpoint.GetView<FieldDetailsView>(czDetails.FieldId).Value.FieldNameByCropYear[cropYear], //.Name,
                        Farm = czTreeData != null ? czTreeData.FarmName : Strings.UnknownFarm_Text,
                    };

                    data.Fields.Add(field);

                    var maps = endpoint.GetView<ItemMap>(cz.Id);
                    try
                    {
                        if (maps.HasValue)
                        {
                            if (maps.Value.MostRecentMapItem != null && maps.Value.MostRecentMapItem.MapData != string.Empty && maps.Value.MostRecentMapItem.MapData != "MULTIPOLYGON EMPTY") {
                                filteredMapData.Add(maps.Value.MostRecentMapItem.MapData);
                            }
                        }
                    }
                    finally { }
                }
                decimal envvalue = 0;
                if (workOrderData.EnvironmentalZoneDistanceValue.HasValue)
                {
                    envvalue = workOrderData.EnvironmentalZoneDistanceValue.Value;
                }
                var converter = new ConvertToBitmap(filteredMapData, this.endpoint, envvalue, workOrderData.EnvironmentalZoneDistanceUnit, workOrderData.Severity);
                //var converter = new ConvertToBitmap(filteredMapData, this.endpoint, 0, "", "");
                data.MapImage = converter.BitMap;
                //data.MapImage = GenerateDetailedMap();

                foreach (var applicator in workOrderData.Applicators)
                {
                    WorkOrderData.ApplicatorLineItemData appLineItem = new WorkOrderData.ApplicatorLineItemData()
                    {
                        Name = applicator.PersonName,
                        Company = applicator.CompanyName,
                        LicenseNumber = applicator.LicenseNumber,
                        ExpirationDate = applicator.Expires,
                        DisplayName = string.Format("{0} ({1})", applicator.PersonName, applicator.CompanyName),
                    };

                    data.Applicators.Add(appLineItem);
                }

                //add all the ppe info into data....
                Task.WaitAll(ppeTask);
                foreach (var prod in ProductPPEList)
                {
                    foreach (var ppe in prod.WorkerProtections)
                    {
                        var mlp = masterlist.GetProduct(prod.ProductId);
                        data.WorkerProtectionItems.Add(new WorkOrderData.WorkerProtectionItem() { ProductName = mlp.Name, PPE = ppe.PPE, ReEntry = ppe.PPEReentry, SignalWord = mlp.SignalWord, RestrictedUse = mlp.RestrictedUse ? "True" : "False" });
                    }
                }

                data.Authorizer = workOrderData.Authorization != null ? $"{Strings.AuthorizedBy_Text} " + workOrderData.Authorization.PersonName : string.Empty;
                data.AuthorizerDate = workOrderData.Authorization != null ? workOrderData.Authorization.AuthorizationDate : DateTime.Now;

                data.CropYear = cropYear;
                data.Notes = workOrderData.Notes;
            }

            return data;
        }

        private double ReportedArea(CropZoneDetailsView czDetails)
        {
            if (czDetails.ReportedArea != null)
            {
                return (double)czDetails.ReportedArea;
            }
            else if (czDetails.ReportedArea == null && czDetails.BoundaryArea != null)
            {
                return (double)czDetails.BoundaryArea;
            }
            else if (czDetails.ReportedArea == null && czDetails.BoundaryArea == null)
            {
                return 0.0;
            }
            else
            {
                return 0.0;
            }
        }

        public System.Drawing.Bitmap GenerateDetailedMap( )
        {
            List<Feature> filteredLargeMapData = new List<Feature>();
            foreach (var cz in workOrderData.CropZones)
            {
                var maps = endpoint.GetView<ItemMap>(cz.Id);
                try
                {
                    if (maps.HasValue)
                    {
                        if (maps.Value.MostRecentMapItem != null && maps.Value.MostRecentMapItem.MapData != string.Empty && maps.Value.MostRecentMapItem.MapData != "MULTIPOLYGON EMPTY") {
                            var fieldName = flattenedHierarchy.Items.FirstOrDefault(c => c.CropZoneId == cz.Id).FieldName;
                            //string customarealabel = GetCustomCropZoneName(cz.Id.Id, fieldName);
                            string customareaonlylabel = GetCustomAreaLabel(cz.Id.Id, false);
                            string customarealabel = DisplayLabelWithArea(fieldName, customareaonlylabel);
                            IDictionary<string, string> columnvalues = new Dictionary<string, string>();
                            columnvalues.Add(@"LdbLabel", fieldName);
                            columnvalues.Add(@"AreaLabel", customarealabel);
                            columnvalues.Add(@"AreaOnlyLabel", customareaonlylabel);
                            Feature newfeature = new Feature(maps.Value.MostRecentMapItem.MapData, cz.Id.Id.ToString(), columnvalues);
                            filteredLargeMapData.Add(newfeature);
                        }
                    }
                }
                finally { }
            }
            var largeMapConverted = new ConvertToBitmap(filteredLargeMapData, true, endpoint, 20.0, workOrderData.EnvironmentalZoneDistanceValue, workOrderData.EnvironmentalZoneDistanceUnit, workOrderData.Severity);
            System.Drawing.Bitmap largeMap = largeMapConverted.BitMap;

            return largeMap;
        }

        //private string GetCustomCropZoneName(Guid Id, string name)
        //{
        //    string stringid = Id.ToString();
        //    string labelname = name;
        //    var fieldMaybe = endpoint.GetView<CropZoneDetailsView>(new CropZoneId(ApplicationEnvironment.CurrentDataSourceId, Id));
        //    fieldMaybe.IfValue(field =>
        //    {
        //        if (field.ReportedArea != null && !string.IsNullOrWhiteSpace(field.ReportedAreaUnit))
        //        {
        //            labelname = DisplaySelectedAreaDecimals(name, field.ReportedArea.Value, field.ReportedAreaUnit);
        //        }
        //        else if (field.BoundaryArea != null && !string.IsNullOrWhiteSpace(field.BoundaryAreaUnit))
        //        {
        //            labelname = DisplaySelectedAreaDecimals(name, field.BoundaryArea.Value, field.BoundaryAreaUnit);
        //        }
        //        else
        //        {
        //            labelname = name;
        //        }

        //    });
        //    return labelname;
        //}

        //private string DisplaySelectedAreaDecimals(string name, double unitvalue, string unitname)
        //{
        //    int areadecimals = 2;
        //    IUnit _unit = UnitFactory.GetUnitByName(unitname);
        //    string decimalstring = "N" + areadecimals.ToString("N0");
        //    return name + "\r\n" + unitvalue.ToString(decimalstring);
        //}

        private string DisplayLabelWithArea(string name, string customarealabel) {
            return name + "\r\n" + customarealabel;
        }

        private string GetCustomAreaLabel(Guid Id, bool isfieldlabel) {
            string stringid = Id.ToString();
            string labelname = string.Empty;
            if (isfieldlabel) {
                var fieldMaybe = endpoint.GetView<FieldDetailsView>(new FieldId(ApplicationEnvironment.CurrentDataSourceId, Id));
                fieldMaybe.IfValue(field => {
                    labelname = string.Empty;
                    if (field.ReportedArea != null && !string.IsNullOrWhiteSpace(field.ReportedAreaUnit)) {
                        labelname = DisplaySelectedAreaOnlyDecimals(field.ReportedArea.Value, field.ReportedAreaUnit);
                    }
                    else if (field.BoundaryArea != null && !string.IsNullOrWhiteSpace(field.BoundaryAreaUnit)) {
                        labelname = DisplaySelectedAreaOnlyDecimals(field.BoundaryArea.Value, field.BoundaryAreaUnit);
                    }
                });
            }
            else {
                var fieldMaybe = endpoint.GetView<CropZoneDetailsView>(new CropZoneId(ApplicationEnvironment.CurrentDataSourceId, Id));
                fieldMaybe.IfValue(field => {
                    labelname = string.Empty;
                    if (field.ReportedArea != null && !string.IsNullOrWhiteSpace(field.ReportedAreaUnit)) {
                        labelname = DisplaySelectedAreaOnlyDecimals(field.ReportedArea.Value, field.ReportedAreaUnit);
                    }
                    else if (field.BoundaryArea != null && !string.IsNullOrWhiteSpace(field.BoundaryAreaUnit)) {
                        labelname = DisplaySelectedAreaOnlyDecimals(field.BoundaryArea.Value, field.BoundaryAreaUnit);
                    }
                });
            }
            return labelname;
        }

        private string DisplaySelectedAreaOnlyDecimals(double unitvalue, string unitname) {
            IUnit _unit = UnitFactory.GetUnitByName(unitname);
            string decimalstring = "N" + areadecimals.ToString("N0");
            return unitvalue.ToString(decimalstring);
        }
        public List<ProductWorkerProtection> GetPPE(List<IncludedProduct> includedProducts)
        {
            List<ProductWorkerProtection> ppeList = new List<ProductWorkerProtection>();
            foreach (var prod in includedProducts)
            {
                try
                {
                    string dataText = string.Empty;

                    //create task....and wait for it to complete...
                    Task getPPE = Task.Factory.StartNew(() =>
                    {
                        dataText = GetWorkerProtectionData(prod.Id);
                    });

                    getPPE.Wait();
                    JObject data = JObject.Parse(dataText);

                    var ppe = from d in JArray.Parse(data["ProductWorkerProtections"].ToString())
                              select JArray.Parse(d["WorkerProtections"].ToString());

                    var workerProtectionList = from d in JArray.Parse(data["ProductWorkerProtections"].ToString())
                                               from i in JArray.Parse(d["WorkerProtections"].ToString())
                                               select new WorkerProtection { PPE = i["PPE"] != null ? i["PPE"].ToString() : string.Empty, PPEReentry = i["PPEReentry"] != null ? i["PPEReentry"].ToString() : string.Empty };
                    var prodWorkProtection = new ProductWorkerProtection() { ProductId = prod.Id, WorkerProtections = workerProtectionList.ToList() };
                    ppeList.Add(prodWorkProtection);
                }
                catch (Exception x)
                {

                }

            }

            return ppeList;
        }

        public string GetWorkerProtectionData(ProductId productId)
        {
            var requestUriFormat = @"{0}/workerprotections?format=json&c=en-US&u=Y29yZXkucGVya2lucw%3D%3D&p=Y29yZXlwMQ%3D%3D&ProductIds={1}";
            var requestUri = string.Format(requestUriFormat, Infrastructure.ApplicationEnvironment.MasterlistRemoteUri, productId.Id);

            WebClient client = new WebClient();
            client.Headers.Add("Authorization", "basic QWdDQVBJOlJhbmRvbU1vbmtleVN1cmZzT25MYXZhIQ==");
            string data = client.DownloadString(new Uri(requestUri));

            return data;
        }
    }

    public class ProductWorkerProtection
    {
        public ProductId ProductId { get; set; }
        public List<WorkerProtection> WorkerProtections { get; set; }
        //public string PPE { get; set; }
        //public string PPEReEntry { get; set; }
    }

    public class WorkerProtection
    {
        public string PPE { get; set; }
        public string PPEReentry { get; set; }
    }

}
