﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.WorkOrder.Generators
{
    public interface IWorkOrderReportGenerator
    {
        ReportSource Generate(WorkOrderQueryCriteria criteria);
        string DisplayName { get; }
    }
}
