﻿using AgC.UnitConversion;
using Landdb.Client.Infrastructure;
using Landdb.Client.Spatial;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Domain.ReadModels.Map;
using Landdb.Domain.ReadModels.ProductSetting;
using Landdb.Domain.ReadModels.Shared;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Domain.ReadModels.WorkOrder;
using Landdb.Infrastructure;
using Landdb.MapStyles;
using Landdb.ReportModels.WorkOrder;
using Landdb.ViewModel.Maps.ImageryOverlay;
using Landdb.ViewModel.Secondary.Map;
using Landdb.ViewModel.Secondary.Reports.Document.Generators;
using Microsoft.Maps.MapControl.WPF;
using Microsoft.Win32;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using Landdb.Resources;
using Telerik.Reporting;
using ThinkGeo.MapSuite.Core;
using ThinkGeo.MapSuite.WpfDesktopEdition;

namespace Landdb.ViewModel.Secondary.Reports.WorkOrder.Generators
{
    public class WorkOrderGenerator : IWorkOrderReportGenerator
    {
        IClientEndpoint endpoint;
        Dispatcher dispatcher;
        int cropYear;
        InMemoryFeatureLayer fieldsLayer = new InMemoryFeatureLayer();
        LayerOverlay layerOverlay = new LayerOverlay();
        List<string> mapData = new List<string>();
        string currentPPE = string.Empty;
        List<ProductWorkerProtection> ProductPPEList;
        public WorkOrderGenerator(IClientEndpoint endPoint, Dispatcher dispatcher, int cropYear)
        {
            this.endpoint = endPoint;
            this.dispatcher = dispatcher;
            this.cropYear = cropYear;
            ProductPPEList = new List<ProductWorkerProtection>();
        }

        public ReportSource Generate(WorkOrderQueryCriteria criteria)
        {
            ReportBook book = new ReportBook();
            var rs = new InstanceReportSource();

            WorkOrderService woService = new WorkOrderService(endpoint, criteria);
            WorkOrderData data = woService.GenerateWorkOrderData();

            var report = new Landdb.Views.Secondary.Reports.Work_Order.LandscapeWorkOrder(); //.StandardWorkOrder();
            report.DataSource = data;
            report.Name = Strings.ReportName_WorkOrder_Text;

            ////EXAMPLE OF HOW TO BIND TO A SUB REPORT
            var mainReport = report;
            var itemDetail = (Telerik.Reporting.SubReport)mainReport.Items.Find("subReport1", true)[0];
            var repSource = (InstanceReportSource)itemDetail.ReportSource;
            var subReport = (Telerik.Reporting.Report)repSource.ReportDocument;
            var odsItems = (Telerik.Reporting.ObjectDataSource)subReport.DataSource;
            odsItems.DataSource = data.Products;

            book.Reports.Add(report);

            rs.ReportDocument = book;
            return rs;
        }

        public string DisplayName
        {
            get { return Strings.GeneratorName_WorkOrder_Text; }
        }

        
    }
}
