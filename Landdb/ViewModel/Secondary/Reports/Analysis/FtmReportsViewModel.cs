﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Analysis.FieldToMarket;
using Landdb.ViewModel.Secondary.Reports.Analysis.Generators;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using Telerik.Reporting;
using Telerik.Reporting.Processing;

namespace Landdb.ViewModel.Secondary.Reports.Analysis
{
    public class FtmReportsViewModel : ViewModelBase
    {
        IClientEndpoint endpoint;
        FtmDetailsViewModel model;
        CropZoneId czId;
        Visifire.Charts.Chart chart;
        IFtmReportGenerator selectedReportGenerator;

        public FtmReportsViewModel(IClientEndpoint endpoint, FtmDetailsViewModel model, Visifire.Charts.Chart chart, CropZoneId czId)
        {
            this.endpoint = endpoint;
            this.model = model;
            this.czId = czId;
            this.chart = chart;

            ReportGenerators = new List<IFtmReportGenerator>();
            AddPlanReportGenerators(endpoint, ApplicationEnvironment.CurrentCropYear);
            SelectedReportGenerator = ReportGenerators[0];

            HideReport = new RelayCommand(this.CloseReport);
            PDFExport = new RelayCommand(this.PDF);
        }

        public RelayCommand HideReport { get; private set; }
        public RelayCommand PDFExport { get; set; }

        public void CloseReport()
        {
            Messenger.Default.Send(new HideOverlayMessage() { });
        }

        public ReportSource ReportSource { get; set; }
        public IFtmReportGenerator SelectedReportGenerator
        {
            get { return selectedReportGenerator; }
            set
            {
                selectedReportGenerator = value;
                UpdateReport();
                //SaveTemplate();
                RaisePropertyChanged("SelectedReportGenerator");
            }

        }
        public List<IFtmReportGenerator> ReportGenerators { get; set; }

        void AddPlanReportGenerators(IClientEndpoint clientEndpoint, int cropYear)
        {
            ReportGenerators.Add(new FtmDetailsReport(endpoint));
        }

        public void UpdateReport()
        {
            if (SelectedReportGenerator != null)
            {
                //PlanQueryCriteria criteria = BuildCriteria();
                ReportSource = SelectedReportGenerator.Generate(model, czId, chart);
                RaisePropertyChanged("BingMapLayer");
                RaisePropertyChanged("ReportSource");
            }
        }

        public void PDF()
        {
            try { 
            if (ReportSource == null) return;
            ReportSource reportExport = ReportSource;

            ReportProcessor reportProcessor = new ReportProcessor();
            System.Collections.Hashtable deviceInfo =
            new System.Collections.Hashtable();
            RenderingResult result = reportProcessor.RenderReport("PDF", reportExport, deviceInfo);

            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "Adobe Reader|*.pdf";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;

            Nullable<bool> resultSaved = saveFileDialog1.ShowDialog();

            // Process save file dialog box results 
            if (resultSaved == true)
            {
                // Save document 
                string filename = saveFileDialog1.FileName;

                try
                {
                    using (System.IO.FileStream fs = new System.IO.FileStream(filename, System.IO.FileMode.Create))
                    {
                        fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
                    }
                }
                catch (Exception ex) { }

            }
            }
            catch (Exception ex)
            {
                //do nothing
            }
        }
    }
}
