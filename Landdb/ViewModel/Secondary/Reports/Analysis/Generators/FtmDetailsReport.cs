﻿using AgC.UnitConversion;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Map;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.ReportModels.Analysis;
using Landdb.ViewModel.Secondary.Map;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Landdb.Resources;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.Analysis.Generators
{
    public class FtmDetailsReport : IFtmReportGenerator
    {
        IClientEndpoint clientEndpoint;

        public FtmDetailsReport(IClientEndpoint clientEndpoint)
        {
            this.clientEndpoint = clientEndpoint;
        }
        public Telerik.Reporting.ReportSource Generate(ViewModel.Analysis.FieldToMarket.FtmDetailsViewModel model, CropZoneId cropZoneId, Visifire.Charts.Chart chart)
        {
            ReportBook book = new ReportBook();
            var rs = new InstanceReportSource();

            var reportData = new FTMReportData();
            RenderTargetBitmap renderer = new RenderTargetBitmap((int)chart.Width,(int)chart.Height, 96d, 96d, PixelFormats.Pbgra32);
            renderer.Render(chart);
            BitmapSource bmp = renderer;

            using (MemoryStream outStream = new MemoryStream())
            {
                PngBitmapEncoder enc = new PngBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create(renderer));
                enc.Save(outStream);
                var bitMap = new System.Drawing.Bitmap(outStream);
                //bitMap.SetResolution(80.0F, 80.0F);
                reportData.Graph = bitMap;
            }

            reportData.DataSourceName = ApplicationEnvironment.CurrentDataSourceName;
            reportData.FieldDisplay = model.FieldName;
            

            var czDetailsMaybe = clientEndpoint.GetView<CropZoneDetailsView>(cropZoneId);
            if (czDetailsMaybe.HasValue)
            {
                var details = czDetailsMaybe.Value;
                var area = details.ReportedArea.HasValue ? details.ReportedArea.Value : 0.0;
                area = details.BoundaryArea.HasValue && area == 0 ? details.BoundaryArea.Value : area;
                reportData.AreaDisplay = string.Format("{0} {1}", area.ToString("N2"), UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).AbbreviatedDisplay);

                var maps = clientEndpoint.GetView<ItemMap>(cropZoneId);
                string mapData = string.Empty;
                try
                {
                    if (maps.HasValue)
                    {
                        if (maps.Value.MostRecentMapItem != null && maps.Value.MostRecentMapItem.MapData != string.Empty && maps.Value.MostRecentMapItem.MapData != "MULTIPOLYGON EMPTY")
                        {
                            mapData = maps.Value.MostRecentMapItem.MapData;

                            var converter = new ConvertToBitmap(mapData, clientEndpoint);
                            reportData.Map = converter.BitMap;
                        }
                    }
                }
                finally { }


                //TO DO :: GET VARIETIES LIST AND PREVIOUS CROP
            }

            reportData.LandUseState = model.StateLandUseValueDisplay;
            reportData.LandUseField = model.LandUseValueDisplay;
            reportData.LandUseDiff = model.DeviationLandUseDisplay;
            reportData.ErosionState = model.StateSoilConservationValueDisplay;
            reportData.ErosionField = model.SoilConservationValueDisplay;
            reportData.ErosionDiff = model.DeviationSoilConservationDisplay;
            reportData.SoilCarbonState = model.StateSoilCarbonValueDisplay;
            reportData.SoilCarbonField = model.SoilCarbonValueDisplay;
            reportData.SoilCarbonDiff = model.DeviationSoilCarbonDisplay;
            reportData.EnergyDiff = model.DeviationEnergyUseDisplay;
            reportData.EnergyField = model.EnergyUseValueDisplay;
            reportData.EnergyState = model.StateEnergyUseValueDisplay;
            reportData.GreenhouseGasDiff = model.DeviationGreenhouseGasDisplay;
            reportData.GreenhouseGasField = model.GreenhouseGasValueDisplay;
            reportData.GreenhouseGasState = model.StateGreenhouseGasValueDisplay;

            reportData.EfficiencyValues.Add(new EfficiencyValue() { CropYear = ApplicationEnvironment.CurrentCropYear, Type = "Land Use", Value = (decimal)(-0.02 * (model.NormalizedLandUseValue - 50)) });
            reportData.EfficiencyValues.Add(new EfficiencyValue() { CropYear = ApplicationEnvironment.CurrentCropYear, Type = "Energy", Value = (decimal)(-0.02 * (model.NormalizedEnergyUseValue - 50)) });
            reportData.EfficiencyValues.Add(new EfficiencyValue() { CropYear = ApplicationEnvironment.CurrentCropYear, Type = "Greenhouse Gas", Value = (decimal)(-0.02 * (model.NormalizedGreenhouseGasValue - 50)) });
            reportData.EfficiencyValues.Add(new EfficiencyValue() { CropYear = ApplicationEnvironment.CurrentCropYear, Type = "Soil Carbon", Value = (decimal)(-0.02 * (model.NormalizedSoilCarbonValue - 50)) });
            reportData.EfficiencyValues.Add(new EfficiencyValue() { CropYear = ApplicationEnvironment.CurrentCropYear, Type = "Soil Conservation", Value = (decimal)(-0.02 * (model.NormalizedSoilConservationValue - 50)) });
            reportData.EfficiencyValues.Add(new EfficiencyValue() { CropYear = ApplicationEnvironment.CurrentCropYear, Type = "Water Use", Value = (decimal)(-0.02 * (model.NormalizedWaterUseValue - 50)) });

            reportData.EfficiencyValues = reportData.EfficiencyValues.OrderByDescending(x => x.Value).ToList();

            reportData.TillageProfile = model.InputModel.NrcsTillageManagement;
            reportData.EstYield = (decimal?)model.InputModel.Yield;
            //reportData.AreaDisplay
            reportData.Slope = model.InputModel.SlopeDisplay;
            reportData.Irrigated = model.IsIrrigated ? Strings.Report_Irrigated_Text : Strings.Report_NotIrrigated_Text;

            reportData.VarietyDisplay = model.WaterModel.Variety;
            reportData.HarvestDate = model.WaterModel.HarvestDate;
            reportData.PlantingDate = model.WaterModel.PlantingDate;
            reportData.PreviousCropDisplay = model.WaterModel.PreviousCrop != null ? model.WaterModel.PreviousCrop.Name : string.Empty;
            //BioticPressureValues
            reportData.SelectedDiseasePressure = model.WaterModel.DiseasePressure.ToString();
            reportData.SelectedInsectPressure = model.WaterModel.InsectPressure.ToString();
            reportData.SelectedWeedPressure = model.WaterModel.WeedPressure.ToString();

            reportData.HydrologicSoilGroup = model.WaterModel.HydrologicSoilGroup.ToString();
            reportData.NitrogenCredit = string.Format("{0} {1}", model.WaterModel.NCreditFromCoverCrop.ToString("N2"), "lb / ac"); //TODO: Localize if we ever need FTM in another country
            reportData.ResidualNitrogen = string.Format("{0} {1} @ {2}", model.WaterModel.ResidualNitrogenSampleValue.ToString("N2"), model.WaterModel.ResidualNitrogenSampleUnit, model.WaterModel.ResidualNitrogenLevelSampleDepth.ToString());
            reportData.IPMStrategy = model.WaterModel.IpmStrategy.ToString();
            reportData.RelativeAppRate = model.WaterModel.UniversityNutrientAppRateComparison.ToString();
            reportData.SoilCondition = model.WaterModel.SoilConditionAtNApplication.ToString();

            int cnt = 0;
            foreach (var conv in model.WaterModel.AvailableConservationPractices)
            {
                if (conv.IsChecked && cnt < 3)
                {
                    reportData.ConservationPractices.Add(conv.Practice.ToString());
                    cnt++;
                }
            }

            //TO DO :: ADD ALL THE VEG COVER VALUES


            var report = new Views.Secondary.Reports.Analysis.FTM_QuickReport();
            report.DataSource = reportData;
            report.Name = "FTM";

            book.Reports.Add(report);

            rs.ReportDocument = book;
            return rs;
        }

        public string DisplayName
        {
            get { return Strings.ReportName_FtmDetails_Text; }
        }
    }
}
