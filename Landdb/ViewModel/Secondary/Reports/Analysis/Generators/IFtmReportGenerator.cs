﻿using Landdb.ViewModel.Analysis.FieldToMarket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.Analysis.Generators
{
    public interface IFtmReportGenerator
    {
        ReportSource Generate(FtmDetailsViewModel model, CropZoneId czId, Visifire.Charts.Chart chart);
        string DisplayName { get; }
    }
}
