﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Secondary.Reports.Contract
{
    public class ContractQueryCriteria
    {

        public ContractId SelectedContractId { get; set; }
    }
}
