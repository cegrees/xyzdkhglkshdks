﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.Contract.Generators
{
    public interface IContractReportGenerator
    {
        ReportSource Generate(ContractQueryCriteria criteria);
        string DisplayName { get; }
    }
}
