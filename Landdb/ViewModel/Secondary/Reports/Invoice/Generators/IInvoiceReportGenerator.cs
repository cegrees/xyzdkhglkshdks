﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.Invoice.Generators
{
    public interface IInvoiceReportGenerator
    {
        ReportSource Generate(InvoiceQueryCriteria crit);
        string DisplayName { get; }
    }
}
