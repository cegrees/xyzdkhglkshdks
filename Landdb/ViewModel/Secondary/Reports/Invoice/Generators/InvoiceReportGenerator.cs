﻿using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Invoice;
using Landdb.Infrastructure;
using Landdb.ReportModels.Invoice;
using Landdb.Views.Secondary.Reports.Invoice;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Landdb.Resources;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.Invoice.Generators
{
    public class InvoiceReportGenerator : IInvoiceReportGenerator
    {
        IClientEndpoint endpoint;
        int cropYear;

        public InvoiceReportGenerator(IClientEndpoint endPoint, int cropyear)
        {
            this.endpoint = endPoint;
            this.cropYear = cropyear;

        }

        public ReportSource Generate(InvoiceQueryCriteria crit)
        {
            ReportBook book = new ReportBook();
            var rs = new InstanceReportSource();

            var invData = endpoint.GetView<InvoiceView>(crit.SelectedInvoice).Value;
            var masterlist = endpoint.GetMasterlistService();

            if (invData != null)
            {
                var data = new InvoiceData();
                data.DataSource = ApplicationEnvironment.CurrentDataSourceName;
                data.CropYear = cropYear;
                data.InvoiceNumber = invData.InvoiceNumber;
                data.InvoiceDate = invData.InvoiceDate;
                data.Vendor = invData.Vendor;
                data.Notes = invData.Notes;

                foreach (var prod in invData.Products)
                {
                    var invProd = new InvoiceData.InvoiceProductLineItem();
                    var masterlistProduct = masterlist.GetProduct(prod.ProductId);
                    invProd.ProductName = masterlistProduct.Name;
                    invProd.RegistrationNumber = masterlistProduct.RegistrationNumber;
                    invProd.Manufacturer = masterlistProduct.Manufacturer;
                    invProd.TotalQuantity = prod.TotalProductUnit != null ? (double)prod.TotalProductValue : 0.0;
                    invProd.TotalUnit = prod.TotalProductUnit;
                    invProd.TotalPrice = prod.TotalCost != null ? (double)prod.TotalCost : 0.0;
                    invProd.PricePer = prod.TotalProductValue != 0 ? (double)(prod.TotalCost / prod.TotalProductValue) : 0;
                    invProd.PricePerUnit = prod.TotalProductUnit;
                    data.Products.Add(invProd);
                }


                var report = new StandardInvoice();
                report.DataSource = data;
                report.Name = Strings.ReportName_Invoice_Text;

                book.Reports.Add(report);
            }
            rs.ReportDocument = book;
            return rs;
        }

        public string DisplayName
        {
            get { return Strings.ReportName_Invoice_Text; }
        }
    }
}
