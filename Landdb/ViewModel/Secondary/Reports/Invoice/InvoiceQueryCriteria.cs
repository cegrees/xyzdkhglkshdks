﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Secondary.Reports.Invoice
{
    public class InvoiceQueryCriteria
    {
        public InvoiceQueryCriteria()
        {

        }

        public InvoiceId SelectedInvoice { get; set; }
    }
}
