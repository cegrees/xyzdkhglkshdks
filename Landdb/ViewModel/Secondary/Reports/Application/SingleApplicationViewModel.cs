﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Secondary.Reports.Application.Generators;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using Telerik.Reporting;
using Telerik.Reporting.Processing;

namespace Landdb.ViewModel.Secondary.Reports.Application
{
    public class SingleApplicationViewModel : ViewModelBase
    {
        IClientEndpoint endpoint;
        ApplicationId appId;
        IApplicationReportGenerator selectedGenerator;

        public SingleApplicationViewModel(IClientEndpoint endPoint, ApplicationId appId)
        {
            this.endpoint = endPoint;
            this.appId = appId;
            ReportGenerators = new List<IApplicationReportGenerator>();

            AddGenerators();
            SelectedReportGenerator = ReportGenerators[0];

            HideReport = new RelayCommand(this.CloseReport);
            PDFExport = new RelayCommand(this.PDF);
        }

        public RelayCommand HideReport { get; private set; }
        public RelayCommand PDFExport { get; set; }

        public void CloseReport()
        {
            Messenger.Default.Send(new HideOverlayMessage() { });
        }

        public ReportSource ReportSource { get; set; }
        public IApplicationReportGenerator SelectedReportGenerator 
        {
            get
            {
                return selectedGenerator;
            }
            set
            {
                selectedGenerator = value;
                UpdateReport();
            }
        }
        public List<IApplicationReportGenerator> ReportGenerators { get; set; }

        public void UpdateReport()
        {
            if (SelectedReportGenerator != null)
            {
                ApplicationQueryCriteria criteria = BuildCriteria();
                ReportSource = SelectedReportGenerator.Generate(criteria);               
                RaisePropertyChanged("BingMapLayer");
                RaisePropertyChanged("ReportSource");
            }
        }

        public void AddGenerators()
        {
            ReportGenerators.Add(new ApplicationReportGenerator(endpoint, ApplicationEnvironment.CurrentCropYear));
            ReportGenerators.Add(new PortraitApplicationGenerator(endpoint, ApplicationEnvironment.CurrentCropYear));
            ReportGenerators.Add(new ApplicationWithCostGenerator(endpoint, ApplicationEnvironment.CurrentCropYear));
            ReportGenerators.Add(new WPSGenerator(endpoint, ApplicationEnvironment.CurrentCropYear));

        }

        ApplicationQueryCriteria BuildCriteria()
        {
            ApplicationQueryCriteria criteria = new ApplicationQueryCriteria();

            if (appId != null)
            {
                criteria.SelectedApplication = appId;
            }

            return criteria;
        }

        public void PDF()
        {
            try { 
            if (ReportSource == null) return;
            ReportSource reportExport = ReportSource;

            ReportProcessor reportProcessor = new ReportProcessor();
            System.Collections.Hashtable deviceInfo =
            new System.Collections.Hashtable();
            RenderingResult result = reportProcessor.RenderReport("PDF", reportExport, deviceInfo);

            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "Adobe Reader|*.pdf";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;

            Nullable<bool> resultSaved = saveFileDialog1.ShowDialog();

            // Process save file dialog box results 
            if (resultSaved == true)
            {
                // Save document 
                string filename = saveFileDialog1.FileName;

                try
                {
                    using (System.IO.FileStream fs = new System.IO.FileStream(filename, System.IO.FileMode.Create))
                    {
                        fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
                    }

                    System.Diagnostics.Process.Start(filename);
                }
                catch (Exception ex) { }

            }
            }
            catch (Exception ex)
            {
                //do nothing
            }
        }
    }
}
