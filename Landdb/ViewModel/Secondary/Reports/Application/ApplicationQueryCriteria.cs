﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Secondary.Reports.Application
{
    public class ApplicationQueryCriteria
    {
        public ApplicationQueryCriteria()
        {

        }

        public ApplicationId SelectedApplication { get; set; }
    }
}
