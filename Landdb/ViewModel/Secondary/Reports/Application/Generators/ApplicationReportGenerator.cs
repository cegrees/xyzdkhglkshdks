﻿using AgC.UnitConversion;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Domain.ReadModels.Map;
using Landdb.Domain.ReadModels.ProductSetting;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.ReportModels.Application;
using Landdb.ViewModel.Secondary.Map;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Landdb.Resources;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.Application.Generators
{
    public class ApplicationReportGenerator : IApplicationReportGenerator
    {
        IClientEndpoint endpoint;
        int cropYear;

        public ApplicationReportGenerator(IClientEndpoint endPoint, int cropYear)
        {
            this.endpoint = endPoint;
            this.cropYear = cropYear;
        }


        public ReportSource Generate(ApplicationQueryCriteria crit)
        {
            ReportBook book = new ReportBook();
            var rs = new InstanceReportSource();

            var data = new ApplicationData();
            var appService = new ApplicationService(endpoint, crit);
            data = appService.ApplicationGenerator();

            var report = new Landdb.Views.Secondary.Reports.Application.StandardApplication();
            report.DataSource = data;
            report.Name = Strings.ReportName_Application_Text;

            ////EXAMPLE OF HOW TO BIND TO A SUB REPORT
            var mainReport = report;
            var itemDetail = (Telerik.Reporting.SubReport)mainReport.Items.Find("subReport1", true)[0];
            var repSource = (InstanceReportSource)itemDetail.ReportSource;
            var subReport = (Telerik.Reporting.Report)repSource.ReportDocument;
            var odsItems = (Telerik.Reporting.ObjectDataSource)subReport.DataSource;
            odsItems.DataSource = data.Products;


            book.Reports.Add(report);

            rs.ReportDocument = book;
            return rs;
        }

        public string DisplayName
        {
            get { return Strings.ReportName_Application_Text; }
        }

        private double ReportedArea(CropZoneDetailsView czDetails)
        {
            if (czDetails.ReportedArea != null)
            {
                return (double)czDetails.ReportedArea;
            }
            else if (czDetails.ReportedArea == null && czDetails.BoundaryArea != null)
            {
                return (double)czDetails.BoundaryArea;
            }
            else if (czDetails.ReportedArea == null && czDetails.BoundaryArea == null)
            {
                return 0.0;
            }
            else
            {
                return 0.0;
            }
        }
    }
}
