﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.Application.Generators
{
    public interface IApplicationReportGenerator
    {
        ReportSource Generate(ApplicationQueryCriteria crit);
        string DisplayName { get; }
    }
}
