﻿using AgC.UnitConversion;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Domain.ReadModels.Invoice;
using Landdb.Domain.ReadModels.Map;
using Landdb.Domain.ReadModels.ProductSetting;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.ReportModels.Application;
using Landdb.ReportServices;
using Landdb.ViewModel.Secondary.Map;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Landdb.Resources;

namespace Landdb.ViewModel.Secondary.Reports.Application.Generators
{
    public class ApplicationService
    {
        IClientEndpoint endpoint;
        int cropYear;
        ApplicationQueryCriteria crit;

        public ApplicationService(IClientEndpoint endPoint, ApplicationQueryCriteria criteria)
        {
            this.endpoint = endPoint;
            this.cropYear = ApplicationEnvironment.CurrentCropYear;
            this.crit = criteria;
        }

        public ApplicationData ApplicationGenerator()
        {
            var productUnits = endpoint.GetProductUnitResolver();
            var appData = endpoint.GetView<ApplicationView>(crit.SelectedApplication).Value;
            var inventory = endpoint.GetView<InventoryListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new InventoryListView());
            var flattenedHierarchy = endpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new FlattenedTreeHierarchyView());
            var masterlist = endpoint.GetMasterlistService();
            var data = new ApplicationData();
            var defaultAreaUnit = UnitFactory.GetUnitByName( ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit);
            if (appData != null)
            {

                data.DatasourceName = ApplicationEnvironment.CurrentDataSourceName;
                data.ApplicationName = appData.Name;
                data.ApplicationDate = appData.StartDateTime.ToLocalTime().ToShortDateString();
                data.ApplicationType = appData.ProductStrategy.ToString();
                data.StartDate = appData.StartDateTime.ToLocalTime();
                data.EndDate = appData.CustomEndDate != null ? ((DateTime)appData.CustomEndDate).ToLocalTime() : appData.StartDateTime.ToLocalTime().AddHours(1.0);
                data.TotalArea = appData.CropZones.Sum(x => x.Area.Value).ToString("N2") + " " + appData.CropZones.FirstOrDefault(x => x.Area.Unit != null).Area.Unit.AbbreviatedDisplay;
                data.TimingEvent = appData.TimingEvent;
                if (appData.Conditions != null)
                {
                    data.Temperature = appData.Conditions.TemperatureValue;
                    data.Humidity = appData.Conditions.HumidityPercent;
                    data.WindDirection = appData.Conditions.WindDirection;
                    data.WindSpeed = appData.Conditions.WindSpeedValue;
                    data.SkyCondition = appData.Conditions.SkyCondition == "-1" || string.IsNullOrEmpty(appData.Conditions.SkyCondition) ? string.Empty : appData.Conditions.SkyCondition;
                    data.SoilCondition = appData.Conditions.SoilMoisture == "-1" ? string.Empty : appData.Conditions.SoilMoisture;
                }

                data.TankInformationVisibility = appData.TankInformation == null ? false : true;
                if (appData.TankInformation != null)
                {
                    data.TankSizeValue = appData.TankInformation.TankSizeValue;
                    data.TankSizeUnit = appData.TankInformation.TankSizeUnit;
                    data.CarrierPerAreaValue = appData.TankInformation.CarrierPerAreaValue;
                    data.CarrierPerAreaUnit = appData.TankInformation.CarrierPerAreaUnit;
                    data.TotalCarrierValue = appData.TankInformation.TotalCarrierValue;
                    data.TotalCarrierUnit = appData.TankInformation.TotalCarrierUnit;
                    data.TankCount = appData.TankInformation.TankCount;
                }

                var invoices = appData.Sources.Where(x => x.DocumentType.ToLower() == "invoice");
                List<InvoiceView> _invoices = new List<InvoiceView>();
                foreach (var source in invoices)
                {
                    var invoiceId = new InvoiceId(source.Identity.DataSourceId, source.Identity.Id);
                    var invoiceDetails = endpoint.GetView<InvoiceView>(invoiceId).Value;
                    _invoices.Add(invoiceDetails);
                }

                var cnt = 0;
                foreach (var prod in appData.Products)
                {
                    var invoicedProducts = from i in _invoices
                                           from p in i.Products
                                           where p.ProductId == prod.Id
                                           select p;

                    var masterlistProduct = masterlist.GetProduct(prod.Id);
                    var czCropIds = from m in appData.CropZones
                                    select endpoint.GetView<CropZoneDetailsView>(m.Id).Value.CropId;
                    var productService = new ProductServices(endpoint);
                    ReiPhi reiPhi = productService.ReturnLargestReiPhi(prod.Id, czCropIds.ToList()); //new ReiPhi();

                    if (reiPhi != null)
                    {
                        reiPhi.Rei = reiPhi.Rei;
                        reiPhi.Phi = reiPhi.Phi;
                    }

                    var aiList = from i in masterlistProduct.ActiveIngredients
                                 select masterlist.GetActiveIngredientDisplay(new ActiveIngredientId(i.Id));
                    string ai = string.Join(",", aiList.ToArray());

                    var registrationNumber = masterlistProduct.RegistrationNumber;
                    string epaNoTrailingChar = string.Empty;
                    if (registrationNumber == null) { epaNoTrailingChar = string.Empty; }
                    else
                    {
                        Regex epaNumberRegex = new Regex("([1-9][0-9]*)");

                        var matches = from m in epaNumberRegex.Matches(registrationNumber).Cast<Match>()
                                      select m.Value;
                        epaNoTrailingChar = string.Join("-", matches.ToArray());
                    }

                    var displayRate = DisplayMeasureConverter.ConvertToRateDisplayMeasure(prod.Id, prod.RateValue, prod.RateUnit, masterlistProduct.Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);
                    var displayTotal = DisplayMeasureConverter.ConvertToTotalDisplayMeasure(prod.Id, (decimal)prod.TotalProductValue, prod.TotalProductUnit, masterlistProduct.Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);
                    var perTankMeasure = UnitFactory.GetPackageSafeUnit(prod.RatePerTankUnit, masterlistProduct.StdUnit, masterlistProduct.StdFactor, masterlistProduct.StdPackageUnit).GetMeasure((double)prod.RatePerTankValue, masterlistProduct.Density);
                    var displayPerTank = DisplayMeasureConverter.ConvertToRatePerTankDisplayMeasure(prod.Id, perTankMeasure, endpoint, ApplicationEnvironment.CurrentDataSourceId);

                    Measure displayPer100 = null;
                    if (data.ApplicationType == "ByRatePerTank")
                    {
                        var ratePer100Value = data.ApplicationType == "ByRatePerTank" ? ((double)prod.RatePerTankValue / ((double)appData.TankInformation.TankSizeValue / 100)) : 0;
                        var ratePer100Unit = data.ApplicationType == "ByRatePerTank" ? UnitFactory.GetPackageSafeUnit(prod.RatePerTankUnit, masterlistProduct.StdUnit, masterlistProduct.StdFactor, masterlistProduct.StdPackageUnit) : null;
                        var ratePer100Measure = ratePer100Unit.GetMeasure(ratePer100Value, masterlistProduct.Density);
                        displayPer100 = DisplayMeasureConverter.ConvertToRatePer100DisplayMeasure(prod.Id, ratePer100Measure, endpoint, ApplicationEnvironment.CurrentDataSourceId);
                    }
                    double invoiceCostPerUnit = 0.0;
                    int counter = 0;
                    IUnit inventoryPriceUnit = null;
                    if (invoicedProducts.Any())
                    {
                        var containsKey = inventory.Products.ContainsKey(prod.Id);

                        if (containsKey)
                        {
                            var product = inventory.Products[prod.Id]; 
                            var fallbackUnit = invoicedProducts.Any() ? UnitFactory.GetUnitByName(invoicedProducts.First().TotalProductUnit) : UnitFactory.GetPackageSafeUnit(prod.TotalProductUnit, masterlistProduct.StdUnit, masterlistProduct.StdFactor, masterlistProduct.StdPackageUnit);
                            IUnit unit = !string.IsNullOrEmpty(product.AveragePriceUnit) ? UnitFactory.GetPackageSafeUnit(inventory.Products[prod.Id].AveragePriceUnit, masterlistProduct.StdUnit, masterlistProduct.StdFactor, masterlistProduct.StdPackageUnit) : fallbackUnit;  //.GetUnitByName(inventory.Products[czAppRecord.ProductId].AveragePriceUnit) : fallbackUnit;
                            
                            foreach (var invoiceprod in invoicedProducts)
                            {
                                var measurePreConvertUnit = UnitFactory.GetPackageSafeUnit(invoiceprod.TotalProductUnit, masterlistProduct.StdUnit, masterlistProduct.StdFactor, masterlistProduct.StdPackageUnit);
                                var measurePreConvert = measurePreConvertUnit.GetMeasure((double)invoiceprod.TotalProductValue, masterlistProduct.Density);
                                var canConvert = measurePreConvert.CanConvertTo(unit);
                                var convertedMeasure = measurePreConvert.CanConvertTo(unit) && measurePreConvert.Unit != unit ? unit.GetMeasure(measurePreConvert.GetValueAs(unit)) : measurePreConvert;
                                inventoryPriceUnit = convertedMeasure.Unit;
                                invoiceCostPerUnit += convertedMeasure.Value != 0 ? (double)(invoiceprod.TotalCost / (decimal)convertedMeasure.Value) : (double)invoiceprod.TotalCost;
                                counter++;
                            }
                        }
                    }

                    var invoicedPrice = invoiceCostPerUnit / counter;
                    var invProd = inventory.Products[prod.Id];
                    var totalUnit = UnitFactory.GetPackageSafeUnit(prod.TotalProductUnit, masterlistProduct.StdUnit, masterlistProduct.StdFactor, masterlistProduct.StdPackageUnit);
                    var totalProd = totalUnit.GetMeasure((double)prod.TotalProductValue, masterlist.GetProduct(prod.Id).Density);
                    var avgPriceUnit = !string.IsNullOrEmpty(invProd.AveragePriceUnit) ? UnitFactory.GetPackageSafeUnit(invProd.AveragePriceUnit, masterlistProduct.StdUnit, masterlistProduct.StdFactor, masterlistProduct.StdPackageUnit) : null;
                    var convertedProdValue = avgPriceUnit != null ? totalProd.GetValueAs(avgPriceUnit) : totalProd.Value;
                    var totalCost = convertedProdValue * invProd.AveragePriceValue;
                    var specificCostUnit = !string.IsNullOrEmpty(prod.SpecificCostUnit) ? UnitFactory.GetPackageSafeUnit(prod.SpecificCostUnit, masterlistProduct.StdUnit, masterlistProduct.StdFactor, masterlistProduct.StdPackageUnit) : null;
                    //var invoiceCostUnit = !string.IsNullOrEmpty(prod.SpecificCostUnit) ? UnitFactory.GetPackageSafeUnit(prod.SpecificCostUnit, masterlistProduct.StdUnit, masterlistProduct.StdFactor, masterlistProduct.StdPackageUnit) : null;
                    var convertedForSpecific = !string.IsNullOrEmpty(prod.SpecificCostUnit) && specificCostUnit != totalProd.Unit && totalProd.CanConvertTo(specificCostUnit) ? totalProd.GetValueAs(specificCostUnit) : totalProd.Value;
                    var convertedForInvoice = inventoryPriceUnit != totalProd.Unit && totalProd.CanConvertTo(inventoryPriceUnit) ? totalProd.GetValueAs(inventoryPriceUnit) : totalProd.Value;
                    totalCost = invoicedProducts.Any() ? (invoicedPrice * convertedForInvoice) / counter : ( prod.SpecificCostPerUnit.HasValue ? (double)prod.SpecificCostPerUnit.Value * convertedForSpecific : totalCost);


                    var totalProdUnit = UnitFactory.GetPackageSafeUnit(prod.TotalProductUnit, masterlistProduct.StdUnit, masterlistProduct.StdFactor, masterlistProduct.StdPackageUnit);
                    var totalProdMeasure = totalProdUnit.GetMeasure((double)prod.TotalProductValue, prod.Density);
                    //var convertedTotalProdMeasure = totalProdMeasure.CanConvertTo(avgPriceUnit) ? avgPriceUnit.GetMeasure(totalProdMeasure.GetValueAs(avgPriceUnit)) : totalProdMeasure;
                    //var totalCost = convertedTotalProdMeasure.Value * avgPriceValue;

                    ApplicationData.ProductLineItemData prodData = new ApplicationData.ProductLineItemData()
                    {
                        ApplicationMethod = string.IsNullOrEmpty(prod.ApplicationMethod) ? string.Empty : prod.ApplicationMethod,
                        EPANumber = epaNoTrailingChar,
                        Pest = prod.TargetPest == null ? string.Empty : ( !string.IsNullOrEmpty(prod.TargetPest.CommonName) ? prod.TargetPest.CommonName : prod.TargetPest.LatinName ),
                        ProductName = masterlistProduct.Name,
                        RateUnit = displayRate.Unit.Name,
                        RateValue = displayRate.Value,
                        RatePerTankUnit = data.ApplicationType == "ByRatePerTank" ? displayPerTank.Unit.Name : prod.RateUnit,
                        RatePerTank = data.ApplicationType == "ByRatePerTank" ? displayPerTank.Value : 0.0,
                        TotalUnit = displayTotal.Unit.Name,
                        TotalValue = displayTotal.Value,
                        PHI = reiPhi != null && reiPhi.Phi != null ? reiPhi.Phi.ToString() : "*",
                        REI = reiPhi != null && reiPhi.Rei != null ? reiPhi.Rei.ToString() : "*",
                        Active = ai,
                        RateType = data.ApplicationType == "ByRatePerTank" ? "Rate/Tank" : "Rate",
                        RatePer100 = data.ApplicationType == "ByRatePerTank" ? displayPer100.Value : 0,
                        RatePer100Unit = data.ApplicationType == "ByRatePerTank" ? displayPer100.Unit.Name : string.Empty,
                        TotalArea = (decimal)appData.TotalArea.Value,
                        TotalAreaUnit = appData.TotalArea.Unit,
                        PercentApplied = prod.PercentApplied,
                        TotalCost = totalCost,
                        CostPerArea = totalCost > 0 ? totalCost / (appData.TotalArea.Value * (double)prod.PercentApplied) : 0.0,
                        RowCount = cnt,
                        DefaultAreaUnit = defaultAreaUnit.AbbreviatedDisplay,
                        
                    };

                    List<ApplicationData.AssociatedProductItem> associatedProducts = new List<ApplicationData.AssociatedProductItem>();

                    foreach (var associate in prod.AssociatedProducts)
                    {
                        var mlp = masterlist.GetProduct(associate.ProductId);
                        var item = new ApplicationData.AssociatedProductItem();
                        item.CustomRateType = associate.CustomRateType;
                        item.CustomRateValue = associate.CustomProductValue;
                        item.HasCost = associate.HasCost;
                        item.ProductName = associate.ProductName;
                        item.RatePerAreaUnit = UnitFactory.GetPackageSafeUnit(associate.RatePerAreaUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                        item.RatePerAreaValue = associate.RatePerAreaValue;
                        item.TotalProductUnit = UnitFactory.GetPackageSafeUnit(associate.TotalProductUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                        item.TotalProductValue = associate.TotalProductValue;
                        IUnit customUnit = UnitFactory.GetPackageSafeUnit(associate.CustomProductUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                        
                        switch (associate.CustomRateType)
                        {
                            case "ByBag":
                                item.DisplayCustomUnit = new CompositeUnit(customUnit.AbbreviatedDisplay, 1, Strings.Unit_Bag_Text.ToLower());
                                break;
                            case "ByCWT":
                                item.DisplayCustomUnit = new CompositeUnit(customUnit.AbbreviatedDisplay, 100, Strings.Unit_Weight_Text.ToLower());
                                break;
                            case "ByRow":
                                item.DisplayCustomUnit = new CompositeUnit(customUnit.AbbreviatedDisplay, 1000, Strings.Unit_LengthRow_Text.ToLower());
                                break;
                            case "BySeed":
                                var parentProd = masterlist.GetProduct(prod.Id);
                                item.DisplayCustomUnit = new CompositeUnit(customUnit.AbbreviatedDisplay, 1, parentProd.StdUnit);
                                break;
                            default:
                                
                                break;
                        }

                        if (associate.HasCost)
                        {
                            var associateInvProd = inventory.Products.SingleOrDefault(x => x.Key == associate.ProductId).Value;

                            var avgCost = associateInvProd != null ? associateInvProd.AveragePriceValue : 0;
                            var avgCostUnit = associateInvProd != null && !string.IsNullOrEmpty(associateInvProd.AveragePriceUnit) ? UnitFactory.GetPackageSafeUnit(associateInvProd.AveragePriceUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit) : null;
                            var totalAssociateProd = UnitFactory.GetPackageSafeUnit(associate.TotalProductUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit).GetMeasure((double)associate.TotalProductValue, mlp.Density);
                            var convertedValue = avgCostUnit != null && totalAssociateProd.Unit != avgCostUnit && totalAssociateProd.CanConvertTo(avgCostUnit) ? totalAssociateProd.GetValueAs(avgCostUnit) : totalAssociateProd.Value;
                            item.TotalCost = (decimal)convertedValue;
                            //TotalAvgCost += avgCost != 0 ? (decimal)(convertedValue * avgCost) : 0;


                            if (associate.SpecificCostPerUnitValue != 0 && !string.IsNullOrEmpty(associate.SpecificCostPerUnitUnit))
                            {
                                var specificUnit = UnitFactory.GetPackageSafeUnit(associate.SpecificCostPerUnitUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                                var convertedSpecificValue = totalAssociateProd.Unit != specificUnit && totalAssociateProd.CanConvertTo(specificUnit) ? totalAssociateProd.GetValueAs(specificUnit) : totalAssociateProd.Value;
                                var specificTotalCost = (decimal)convertedSpecificValue * associate.SpecificCostPerUnitValue;
                                item.TotalCost = specificTotalCost;
                                //TotalSpecificCost += specificTotalCost;
                            }
                        }

                        prodData.AssociatedProducts.Add(item);
                    }

                    data.Products.Add(prodData);
                    cnt++;
                }

                data.TotalCost = data.Products.Sum(x => x.TotalCost);
                List<string> filteredMapData = new List<string>();

                foreach (var cz in appData.CropZones)
                {
                    var czDetails = endpoint.GetView<CropZoneDetailsView>(cz.Id).GetValue(new CropZoneDetailsView());

                    var czTreeData = (from f in flattenedHierarchy.Items
                                      where f.CropZoneId == cz.Id
                                      select f).FirstOrDefault();

                    var reportedArea = ReportedArea(czDetails);

                    var fieldDetails = endpoint.GetView<FieldDetailsView>(czDetails.FieldId).Value;
                    var centerLatLon = string.IsNullOrEmpty(czDetails.CenterLatLong) ? fieldDetails.CenterLatLong : czDetails.CenterLatLong;

                    var mapsettings = endpoint.GetMapSettings();
                    if (!string.IsNullOrEmpty(centerLatLon))
                    {
                        centerLatLon = DecimalDegrees.ddstring_To_DMs(centerLatLon, mapsettings.DisplayCoordinateFormat);
                    }

                    ApplicationData.FieldLineItemData field = new ApplicationData.FieldLineItemData()
                    {
                        Area = cz.Area.Value,
                        CenterLatLong = centerLatLon,
                        Crop = masterlist.GetCropDisplay(czDetails.CropId),
                        CropZone = czDetails.Name,
                        Field = czTreeData.FieldName,
                        Farm = czTreeData.FarmName,
                    };

                    data.Fields.Add(field);
                    var maps = endpoint.GetView<ItemMap>(cz.Id);
                    try
                    {
                        if (maps.HasValue)
                        {
                            if (maps.Value.MostRecentMapItem != null && maps.Value.MostRecentMapItem.MapData != string.Empty && maps.Value.MostRecentMapItem.MapData != "MULTIPOLYGON EMPTY")
                            {
                                filteredMapData.Add(maps.Value.MostRecentMapItem.MapData);
                            }
                        }
                    }
                    finally { }
                }
                decimal envvalue = 0;
                if(appData.EnvironmentalZoneDistanceValue.HasValue) {
                    envvalue = appData.EnvironmentalZoneDistanceValue.Value;
                }
                var converter = new ConvertToBitmap(filteredMapData, this.endpoint, envvalue, appData.EnvironmentalZoneDistanceUnit, appData.Severity);
                //var converter = new ConvertToBitmap(filteredMapData, this.endpoint);
                data.MapImage = converter.BitMap;

                foreach (var applicator in appData.Applicators)
                {
                    ApplicationData.ApplicatorLineItemData appLineItem = new ApplicationData.ApplicatorLineItemData()
                    {
                        Name = applicator.PersonName,
                        Company = applicator.CompanyName,
                        LicenseNumber = applicator.LicenseNumber,
                        ExpirationDate = applicator.Expires,
                        DisplayName = string.Format("{0} ({1})", applicator.PersonName, applicator.CompanyName),
                    };

                    data.Applicators.Add(appLineItem);
                }

                data.Authorizer = appData.Authorization != null ? appData.Authorization.PersonName : string.Empty;
                data.AuthorizerDate = appData.Authorization != null ? appData.Authorization.AuthorizationDate : DateTime.Now;

                data.CropYear = cropYear;
                var sourceArray = from source in appData.Sources
                                  select "Source :: " + source.DocumentType + "  #" + source.Name + "\n";
                var sourcesString = string.Join("  ", sourceArray);
                data.Notes = appData.Notes;
                if (appData.Sources.Any())
                {
                    data.Notes = data.Notes + "\n" + "******************************************************************** \n" + sourcesString;
                }
            }

            data.Fields = data.Fields.OrderBy(x => x.Farm).ThenBy(y => y.Field).ThenBy(m => m.CropZone).ThenBy(n => n.Crop).ToList();
            return data;
        }

        public ApplicationData ApplicationGenerator(int width, int height)
        {
            var productUnits = endpoint.GetProductUnitResolver();
            var appData = endpoint.GetView<ApplicationView>(crit.SelectedApplication).Value;
            var inventory = endpoint.GetView<InventoryListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new InventoryListView());
            var flattenedHierarchy = endpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new FlattenedTreeHierarchyView());
            var masterlist = endpoint.GetMasterlistService();
            var defaultAreaUnit = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit);
            var data = new ApplicationData();
            if (appData != null)
            {

                data.DatasourceName = ApplicationEnvironment.CurrentDataSourceName;
                data.ApplicationName = appData.Name;
                data.ApplicationDate = appData.StartDateTime.ToLocalTime().ToShortDateString();
                data.ApplicationType = appData.ProductStrategy.ToString();
                data.StartDate = appData.StartDateTime.ToLocalTime();
                data.EndDate = appData.CustomEndDate != null ? ((DateTime)appData.CustomEndDate).ToLocalTime() : appData.StartDateTime.ToLocalTime().AddHours(1.0);
                data.TotalArea = appData.CropZones.Sum(x => x.Area.Value).ToString("N2") + " " + appData.CropZones.FirstOrDefault(x => x.Area.Unit != null).Area.Unit.AbbreviatedDisplay;

                if (appData.Conditions != null)
                {
                    data.Temperature = appData.Conditions.TemperatureValue;
                    data.Humidity = appData.Conditions.HumidityPercent;
                    data.WindDirection = appData.Conditions.WindDirection;
                    data.WindSpeed = appData.Conditions.WindSpeedValue;
                    data.SkyCondition = appData.Conditions.SkyCondition == "-1" || string.IsNullOrEmpty(appData.Conditions.SkyCondition) ? string.Empty : appData.Conditions.SkyCondition;
                    data.SoilCondition = appData.Conditions.SoilMoisture == "-1" ? string.Empty : appData.Conditions.SoilMoisture;
                }

                data.TankInformationVisibility = appData.TankInformation == null ? false : true;
                if (appData.TankInformation != null)
                {
                    data.TankSizeValue = appData.TankInformation.TankSizeValue;
                    data.TankSizeUnit = appData.TankInformation.TankSizeUnit;
                    data.CarrierPerAreaValue = appData.TankInformation.CarrierPerAreaValue;
                    data.CarrierPerAreaUnit = appData.TankInformation.CarrierPerAreaUnit;
                    data.TotalCarrierValue = appData.TankInformation.TotalCarrierValue;
                    data.TotalCarrierUnit = appData.TankInformation.TotalCarrierUnit;
                    data.TankCount = appData.TankInformation.TankCount;
                }

                var cnt = 0;
                foreach (var prod in appData.Products)
                {
                    var masterlistProduct = masterlist.GetProduct(prod.Id);
                    var czCropIds = from m in appData.CropZones
                                    select endpoint.GetView<CropZoneDetailsView>(m.Id).Value.CropId;
                    var productService = new ProductServices(endpoint);
                    ReiPhi reiPhi = productService.ReturnLargestReiPhi(prod.Id, czCropIds.ToList()); //new ReiPhi();

                    if (reiPhi != null)
                    {
                        reiPhi.Rei = reiPhi.Rei;
                        reiPhi.Phi = reiPhi.Phi;
                    }

                    var aiList = from i in masterlistProduct.ActiveIngredients
                                 select masterlist.GetActiveIngredientDisplay(new ActiveIngredientId(i.Id));
                    string ai = string.Join(",", aiList.ToArray());

                    var registrationNumber = masterlistProduct.RegistrationNumber;
                    string epaNoTrailingChar = string.Empty;
                    if (registrationNumber == null) { epaNoTrailingChar = string.Empty; }
                    else
                    {
                        Regex epaNumberRegex = new Regex("([1-9][0-9]*)");

                        var matches = from m in epaNumberRegex.Matches(registrationNumber).Cast<Match>()
                                      select m.Value;
                        epaNoTrailingChar = string.Join("-", matches.ToArray());
                    }

                    var displayRate = DisplayMeasureConverter.ConvertToRateDisplayMeasure(prod.Id, prod.RateValue, prod.RateUnit, masterlistProduct.Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);
                    var displayTotal = DisplayMeasureConverter.ConvertToTotalDisplayMeasure(prod.Id, (decimal)prod.TotalProductValue, prod.TotalProductUnit, masterlistProduct.Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);
                    var perTankMeasure = UnitFactory.GetUnitByName(prod.RatePerTankUnit).GetMeasure((double)prod.RatePerTankValue, masterlistProduct.Density);
                    var displayPerTank = DisplayMeasureConverter.ConvertToRatePerTankDisplayMeasure(prod.Id, perTankMeasure, endpoint, ApplicationEnvironment.CurrentDataSourceId);

                    Measure displayPer100 = null;
                    if (data.ApplicationType == "ByRatePerTank")
                    {
                        var ratePer100Value = data.ApplicationType == "ByRatePerTank" ? ((double)prod.RatePerTankValue / ((double)appData.TankInformation.TankSizeValue / 100)) : 0;
                        var ratePer100Unit = data.ApplicationType == "ByRatePerTank" ? UnitFactory.GetPackageSafeUnit(prod.RatePerTankUnit, masterlistProduct.StdUnit, masterlistProduct.StdFactor, masterlistProduct.StdPackageUnit) : null;
                        var ratePer100Measure = ratePer100Unit.GetMeasure(ratePer100Value, masterlistProduct.Density);
                        displayPer100 = DisplayMeasureConverter.ConvertToRatePer100DisplayMeasure(prod.Id, ratePer100Measure, endpoint, ApplicationEnvironment.CurrentDataSourceId);
                    }

                    var invProd = inventory.Products[prod.Id];
                    var totalUnit = UnitFactory.GetPackageSafeUnit(prod.TotalProductUnit, masterlistProduct.StdUnit, masterlistProduct.StdFactor, masterlistProduct.StdPackageUnit);
                    var totalProd = totalUnit.GetMeasure((double)prod.TotalProductValue, masterlist.GetProduct(prod.Id).Density);
                    var avgPriceUnit = !string.IsNullOrEmpty(invProd.AveragePriceUnit) ? UnitFactory.GetPackageSafeUnit(invProd.AveragePriceUnit, masterlistProduct.StdUnit, masterlistProduct.StdFactor, masterlistProduct.StdPackageUnit) : null;
                    var convertedProdValue = avgPriceUnit != null ? totalProd.GetValueAs(avgPriceUnit) : totalProd.Value;
                    var totalCost = convertedProdValue * invProd.AveragePriceValue;
                    var specificCostUnit = !string.IsNullOrEmpty(prod.SpecificCostUnit) ? UnitFactory.GetPackageSafeUnit(prod.SpecificCostUnit, masterlistProduct.StdUnit, masterlistProduct.StdFactor, masterlistProduct.StdPackageUnit) : null;
                    var convertedForSpecific = specificCostUnit != null && totalProd.Unit !=  specificCostUnit ? totalProd.GetValueAs(specificCostUnit) : totalProd.Value;
                    totalCost = prod.SpecificCostPerUnit.HasValue ? (double)prod.SpecificCostPerUnit.Value * convertedForSpecific : totalCost;


                    var totalProdUnit = UnitFactory.GetPackageSafeUnit(prod.TotalProductUnit, masterlistProduct.StdUnit, masterlistProduct.StdFactor, masterlistProduct.StdPackageUnit);
                    var totalProdMeasure = totalProdUnit.GetMeasure((double)prod.TotalProductValue, prod.Density);
                    //var convertedTotalProdMeasure = totalProdMeasure.CanConvertTo(avgPriceUnit) ? avgPriceUnit.GetMeasure(totalProdMeasure.GetValueAs(avgPriceUnit)) : totalProdMeasure;
                    //var totalCost = convertedTotalProdMeasure.Value * avgPriceValue;

                    ApplicationData.ProductLineItemData prodData = new ApplicationData.ProductLineItemData()
                    {
                        ApplicationMethod = string.IsNullOrEmpty(prod.ApplicationMethod) ? string.Empty : prod.ApplicationMethod,
                        EPANumber = epaNoTrailingChar,
                        Pest = prod.TargetPest == null ? string.Empty : prod.TargetPest.CommonName,
                        ProductName = masterlistProduct.Name,
                        RateUnit = displayRate.Unit.Name,
                        RateValue = displayRate.Value,
                        RatePerTankUnit = data.ApplicationType == "ByRatePerTank" ? displayPerTank.Unit.Name : prod.RateUnit,
                        RatePerTank = data.ApplicationType == "ByRatePerTank" ? displayPerTank.Value : 0.0,
                        TotalUnit = displayTotal.Unit.Name,
                        TotalValue = displayTotal.Value,
                        PHI = reiPhi != null && reiPhi.Phi != null ? reiPhi.Phi.ToString() : "*",
                        REI = reiPhi != null && reiPhi.Rei != null ? reiPhi.Rei.ToString() : "*",
                        Active = ai,
                        RateType = data.ApplicationType == "ByRatePerTank" ? "Rate/Tank" : "Rate",
                        RatePer100 = data.ApplicationType == "ByRatePerTank" ? displayPer100.Value : 0,
                        RatePer100Unit = data.ApplicationType == "ByRatePerTank" ? displayPer100.Unit.Name : string.Empty,
                        TotalArea = (decimal)appData.TotalArea.Value,
                        TotalAreaUnit = appData.TotalArea.Unit,
                        PercentApplied = prod.PercentApplied,
                        TotalCost = totalCost,
                        CostPerArea = totalCost > 0 ? totalCost / (appData.TotalArea.Value * (double)prod.PercentApplied) : 0.0,
                        RowCount = cnt,
                        DefaultAreaUnit = defaultAreaUnit.AbbreviatedDisplay,
                    };

                    List<ApplicationData.AssociatedProductItem> associatedProducts = new List<ApplicationData.AssociatedProductItem>();

                    foreach (var associate in prod.AssociatedProducts)
                    {
                        var mlp = masterlist.GetProduct(associate.ProductId);
                        var item = new ApplicationData.AssociatedProductItem();
                        item.CustomRateType = associate.CustomRateType;
                        item.CustomRateValue = associate.CustomProductValue;
                        item.HasCost = associate.HasCost;
                        item.ProductName = associate.ProductName;
                        item.RatePerAreaUnit = UnitFactory.GetPackageSafeUnit(associate.RatePerAreaUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                        item.RatePerAreaValue = associate.RatePerAreaValue;
                        item.TotalProductUnit = UnitFactory.GetPackageSafeUnit(associate.TotalProductUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                        item.TotalProductValue = associate.TotalProductValue;
                        IUnit customUnit = UnitFactory.GetPackageSafeUnit(associate.CustomProductUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);

                        switch (associate.CustomRateType)
                        {
                            case "ByBag":
                                item.DisplayCustomUnit = new CompositeUnit(customUnit.AbbreviatedDisplay, 1, Strings.Unit_Bag_Text.ToLower());
                                break;
                            case "ByCWT":
                                item.DisplayCustomUnit = new CompositeUnit(customUnit.AbbreviatedDisplay, 100, Strings.Unit_Weight_Text.ToLower());
                                break;
                            case "ByRow":
                                item.DisplayCustomUnit = new CompositeUnit(customUnit.AbbreviatedDisplay, 1000, Strings.Unit_LengthRow_Text.ToLower());
                                break;
                            case "BySeed":
                                var parentProd = masterlist.GetProduct(prod.Id);
                                item.DisplayCustomUnit = new CompositeUnit(customUnit.AbbreviatedDisplay, 1, parentProd.StdUnit);
                                break;
                            default:

                                break;
                        }

                        if (associate.HasCost)
                        {
                            var associateInvProd = inventory.Products.SingleOrDefault(x => x.Key == associate.ProductId).Value;

                            var avgCost = associateInvProd != null ? associateInvProd.AveragePriceValue : 0;
                            var avgCostUnit = associateInvProd != null && !string.IsNullOrEmpty(associateInvProd.AveragePriceUnit) ? UnitFactory.GetPackageSafeUnit(associateInvProd.AveragePriceUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit) : null;
                            var totalAssociateProd = UnitFactory.GetPackageSafeUnit(associate.TotalProductUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit).GetMeasure((double)associate.TotalProductValue, mlp.Density);
                            var convertedValue = avgCostUnit != null && totalAssociateProd.Unit != avgCostUnit && totalAssociateProd.CanConvertTo(avgCostUnit) ? totalAssociateProd.GetValueAs(avgCostUnit) : totalAssociateProd.Value;
                            item.TotalCost = (decimal)convertedValue;
                            //TotalAvgCost += avgCost != 0 ? (decimal)(convertedValue * avgCost) : 0;


                            if (associate.SpecificCostPerUnitValue != 0 && !string.IsNullOrEmpty(associate.SpecificCostPerUnitUnit))
                            {
                                var specificUnit = UnitFactory.GetPackageSafeUnit(associate.SpecificCostPerUnitUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                                var convertedSpecificValue = totalAssociateProd.Unit != specificUnit && totalAssociateProd.CanConvertTo(specificUnit) ? totalAssociateProd.GetValueAs(specificUnit) : totalAssociateProd.Value;
                                var specificTotalCost = (decimal)convertedSpecificValue * associate.SpecificCostPerUnitValue;
                                item.TotalCost = specificTotalCost;
                                //TotalSpecificCost += specificTotalCost;
                            }
                        }

                        prodData.AssociatedProducts.Add(item);
                    }

                    data.Products.Add(prodData);
                    cnt++;
                }

                data.TotalCost = data.Products.Sum(x => x.TotalCost);
                List<string> filteredMapData = new List<string>();

                foreach (var cz in appData.CropZones)
                {
                    var czDetails = endpoint.GetView<CropZoneDetailsView>(cz.Id).GetValue(new CropZoneDetailsView());

                    var czTreeData = (from f in flattenedHierarchy.Items
                                      where f.CropZoneId == cz.Id
                                      select f).FirstOrDefault();

                    var reportedArea = ReportedArea(czDetails);

                    var fieldDetails = endpoint.GetView<FieldDetailsView>(czDetails.FieldId).Value;
                    var centerLatLon = string.IsNullOrEmpty(czDetails.CenterLatLong) ? fieldDetails.CenterLatLong : czDetails.CenterLatLong;

                    ApplicationData.FieldLineItemData field = new ApplicationData.FieldLineItemData()
                    {
                        Area = cz.Area.Value,
                        CenterLatLong = centerLatLon,
                        Crop = masterlist.GetCropDisplay(czDetails.CropId),
                        CropZone = czDetails.Name,
                        Field = czTreeData.FieldName,
                        Farm = czTreeData.FarmName,
                    };
                    data.Fields.Add(field);
                    var maps = endpoint.GetView<ItemMap>(cz.Id);
                    try
                    {
                        if (maps.HasValue)
                        {
                            if (maps.Value.MostRecentMapItem != null && maps.Value.MostRecentMapItem.MapData != string.Empty && maps.Value.MostRecentMapItem.MapData != "MULTIPOLYGON EMPTY")
                            {
                                filteredMapData.Add(maps.Value.MostRecentMapItem.MapData);
                            }
                        }
                    }
                    finally { }
                }
                decimal envvalue = 0;
                if (appData.EnvironmentalZoneDistanceValue.HasValue) {
                    envvalue = appData.EnvironmentalZoneDistanceValue.Value;
                }
                var converter = new ConvertToBitmap(filteredMapData, width, height, this.endpoint, envvalue, appData.EnvironmentalZoneDistanceUnit, appData.Severity);
                //var converter = new ConvertToBitmap(filteredMapData, width, height, this.endpoint);
                data.MapImage = converter.BitMap;

                foreach (var applicator in appData.Applicators)
                {
                    ApplicationData.ApplicatorLineItemData appLineItem = new ApplicationData.ApplicatorLineItemData()
                    {
                        Name = applicator.PersonName,
                        Company = applicator.CompanyName,
                        LicenseNumber = applicator.LicenseNumber,
                        ExpirationDate = applicator.Expires,
                        DisplayName = string.Format("{0} ({1})", applicator.PersonName, applicator.CompanyName),
                    };

                    data.Applicators.Add(appLineItem);
                }

                data.Authorizer = appData.Authorization != null ? appData.Authorization.PersonName : string.Empty;
                data.AuthorizerDate = appData.Authorization != null ? appData.Authorization.AuthorizationDate : DateTime.Now;

                data.CropYear = cropYear;
                var sourceArray = from source in appData.Sources
                                  select "Source :: " + source.DocumentType + "  #" + source.Name + "\n";
                var sourcesString = string.Join("  ", sourceArray);
                data.Notes = appData.Notes;
                if (appData.Sources.Any())
                {
                    data.Notes = data.Notes + "\n" + "******************************************************************** \n" + sourcesString;
                }
            }

            return data;
        }

        public ApplicationData ApplicationGenerator(ApplicationId appId)
        {
            var productUnits = endpoint.GetProductUnitResolver();
            var appData = endpoint.GetView<ApplicationView>(appId).Value;
            var inventory = endpoint.GetView<InventoryListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new InventoryListView());
            var flattenedHierarchy = endpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new FlattenedTreeHierarchyView());
            var masterlist = endpoint.GetMasterlistService();
            var defaultAreaUnit = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit);
            var data = new ApplicationData();
            if (appData != null)
            {

                data.DatasourceName = ApplicationEnvironment.CurrentDataSourceName;
                data.ApplicationName = appData.Name;
                data.ApplicationDate = appData.StartDateTime.ToLocalTime().ToShortDateString();
                data.ApplicationType = appData.ProductStrategy.ToString();
                data.StartDate = appData.StartDateTime.ToLocalTime();
                data.EndDate = appData.CustomEndDate != null ? ((DateTime)appData.CustomEndDate).ToLocalTime() : appData.StartDateTime.ToLocalTime().AddHours(1.0);
                data.TotalArea = appData.CropZones.Sum(x => x.Area.Value).ToString("N2") + " " + appData.CropZones.FirstOrDefault(x => x.Area.Unit != null).Area.Unit.AbbreviatedDisplay;

                if (appData.Conditions != null)
                {
                    data.Temperature = appData.Conditions.TemperatureValue;
                    data.Humidity = appData.Conditions.HumidityPercent;
                    data.WindDirection = appData.Conditions.WindDirection;
                    data.WindSpeed = appData.Conditions.WindSpeedValue;
                    data.SkyCondition = appData.Conditions.SkyCondition == "-1" || string.IsNullOrEmpty(appData.Conditions.SkyCondition) ? string.Empty : appData.Conditions.SkyCondition;
                    data.SoilCondition = appData.Conditions.SoilMoisture == "-1" ? string.Empty : appData.Conditions.SoilMoisture;
                }

                data.TankInformationVisibility = appData.TankInformation == null ? false : true;
                if (appData.TankInformation != null)
                {
                    data.TankSizeValue = appData.TankInformation.TankSizeValue;
                    data.TankSizeUnit = appData.TankInformation.TankSizeUnit;
                    data.CarrierPerAreaValue = appData.TankInformation.CarrierPerAreaValue;
                    data.CarrierPerAreaUnit = appData.TankInformation.CarrierPerAreaUnit;
                    data.TotalCarrierValue = appData.TankInformation.TotalCarrierValue;
                    data.TotalCarrierUnit = appData.TankInformation.TotalCarrierUnit;
                    data.TankCount = appData.TankInformation.TankCount;
                }

                var cnt = 0;
                foreach (var prod in appData.Products)
                {
                    var masterlistProduct = masterlist.GetProduct(prod.Id);
                    var czCropIds = from m in appData.CropZones
                                    select endpoint.GetView<CropZoneDetailsView>(m.Id).Value.CropId;
                    var productService = new ProductServices(endpoint);
                    ReiPhi reiPhi = productService.ReturnLargestReiPhi(prod.Id, czCropIds.ToList()); //new ReiPhi();

                    if (reiPhi != null)
                    {
                        reiPhi.Rei = reiPhi.Rei;
                        reiPhi.Phi = reiPhi.Phi;
                    }

                    var aiList = from i in masterlistProduct.ActiveIngredients
                                 select masterlist.GetActiveIngredientDisplay(new ActiveIngredientId(i.Id));
                    string ai = string.Join(",", aiList.ToArray());

                    var registrationNumber = masterlistProduct.RegistrationNumber;
                    string epaNoTrailingChar = string.Empty;
                    if (registrationNumber == null) { epaNoTrailingChar = string.Empty; }
                    else
                    {
                        Regex epaNumberRegex = new Regex("([1-9][0-9]*)");

                        var matches = from m in epaNumberRegex.Matches(registrationNumber).Cast<Match>()
                                      select m.Value;
                        epaNoTrailingChar = string.Join("-", matches.ToArray());
                    }

                    var displayRate = DisplayMeasureConverter.ConvertToRateDisplayMeasure(prod.Id, prod.RateValue, prod.RateUnit, masterlistProduct.Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);
                    var displayTotal = DisplayMeasureConverter.ConvertToTotalDisplayMeasure(prod.Id, (decimal)prod.TotalProductValue, prod.TotalProductUnit, masterlistProduct.Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);
                    var perTankMeasure = UnitFactory.GetUnitByName(prod.RatePerTankUnit).GetMeasure((double)prod.RatePerTankValue, masterlistProduct.Density);
                    var displayPerTank = DisplayMeasureConverter.ConvertToRatePerTankDisplayMeasure(prod.Id, perTankMeasure, endpoint, ApplicationEnvironment.CurrentDataSourceId);

                    Measure displayPer100 = null;
                    if (data.ApplicationType == "ByRatePerTank")
                    {
                        var ratePer100Value = data.ApplicationType == "ByRatePerTank" ? ((double)prod.RatePerTankValue / ((double)appData.TankInformation.TankSizeValue / 100)) : 0;
                        var ratePer100Unit = data.ApplicationType == "ByRatePerTank" ? UnitFactory.GetPackageSafeUnit(prod.RatePerTankUnit, masterlistProduct.StdUnit, masterlistProduct.StdFactor, masterlistProduct.StdPackageUnit) : null;
                        var ratePer100Measure = ratePer100Unit.GetMeasure(ratePer100Value, masterlistProduct.Density);
                        displayPer100 = DisplayMeasureConverter.ConvertToRatePer100DisplayMeasure(prod.Id, ratePer100Measure, endpoint, ApplicationEnvironment.CurrentDataSourceId);
                    }

                    var invProd = inventory.Products[prod.Id];
                    var totalUnit = UnitFactory.GetPackageSafeUnit(prod.TotalProductUnit, masterlistProduct.StdUnit, masterlistProduct.StdFactor, masterlistProduct.StdPackageUnit);
                    var totalProd = totalUnit.GetMeasure((double)prod.TotalProductValue, masterlist.GetProduct(prod.Id).Density);
                    var avgPriceUnit = !string.IsNullOrEmpty(invProd.AveragePriceUnit) ? UnitFactory.GetPackageSafeUnit(invProd.AveragePriceUnit, masterlistProduct.StdUnit, masterlistProduct.StdFactor, masterlistProduct.StdPackageUnit) : null;
                    var convertedProdValue = avgPriceUnit != null ? totalProd.GetValueAs(avgPriceUnit) : totalProd.Value;
                    var totalCost = convertedProdValue * invProd.AveragePriceValue;
                    var specificCostUnit = !string.IsNullOrEmpty(prod.SpecificCostUnit) ? UnitFactory.GetPackageSafeUnit(prod.SpecificCostUnit, masterlistProduct.StdUnit, masterlistProduct.StdFactor, masterlistProduct.StdPackageUnit) : null;
                    var convertedForSpecific = specificCostUnit != null && totalProd.Unit != specificCostUnit ? totalProd.GetValueAs(specificCostUnit) : totalProd.Value;
                    totalCost = prod.SpecificCostPerUnit.HasValue ? (double)prod.SpecificCostPerUnit.Value * convertedForSpecific : totalCost;


                    var totalProdUnit = UnitFactory.GetPackageSafeUnit(prod.TotalProductUnit, masterlistProduct.StdUnit, masterlistProduct.StdFactor, masterlistProduct.StdPackageUnit);
                    var totalProdMeasure = totalProdUnit.GetMeasure((double)prod.TotalProductValue, prod.Density);
                    //var convertedTotalProdMeasure = totalProdMeasure.CanConvertTo(avgPriceUnit) ? avgPriceUnit.GetMeasure(totalProdMeasure.GetValueAs(avgPriceUnit)) : totalProdMeasure;
                    //var totalCost = convertedTotalProdMeasure.Value * avgPriceValue;

                    ApplicationData.ProductLineItemData prodData = new ApplicationData.ProductLineItemData()
                    {
                        ApplicationMethod = string.IsNullOrEmpty(prod.ApplicationMethod) ? string.Empty : prod.ApplicationMethod,
                        EPANumber = epaNoTrailingChar,
                        Pest = prod.TargetPest == null ? string.Empty : prod.TargetPest.CommonName,
                        ProductName = masterlistProduct.Name,
                        RateUnit = displayRate.Unit.Name,
                        RateValue = displayRate.Value,
                        RatePerTankUnit = data.ApplicationType == "ByRatePerTank" ? displayPerTank.Unit.Name : prod.RateUnit,
                        RatePerTank = data.ApplicationType == "ByRatePerTank" ? displayPerTank.Value : 0.0,
                        TotalUnit = displayTotal.Unit.Name,
                        TotalValue = displayTotal.Value,
                        PHI = reiPhi != null && reiPhi.Phi != null ? reiPhi.Phi.ToString() : "*",
                        REI = reiPhi != null && reiPhi.Rei != null ? reiPhi.Rei.ToString() : "*",
                        Active = ai,
                        RateType = data.ApplicationType == "ByRatePerTank" ? "Rate/Tank" : "Rate",
                        RatePer100 = data.ApplicationType == "ByRatePerTank" ? displayPer100.Value : 0,
                        RatePer100Unit = data.ApplicationType == "ByRatePerTank" ? displayPer100.Unit.Name : string.Empty,
                        TotalArea = (decimal)appData.TotalArea.Value,
                        TotalAreaUnit = appData.TotalArea.Unit,
                        PercentApplied = prod.PercentApplied,
                        TotalCost = totalCost,
                        CostPerArea = totalCost > 0 ? totalCost / (appData.TotalArea.Value * (double)prod.PercentApplied) : 0.0,
                        RowCount = cnt,
                        DefaultAreaUnit = defaultAreaUnit.AbbreviatedDisplay,
                    };

                    List<ApplicationData.AssociatedProductItem> associatedProducts = new List<ApplicationData.AssociatedProductItem>();

                    foreach (var associate in prod.AssociatedProducts)
                    {
                        var mlp = masterlist.GetProduct(associate.ProductId);
                        var item = new ApplicationData.AssociatedProductItem();
                        item.CustomRateType = associate.CustomRateType;
                        item.CustomRateValue = associate.CustomProductValue;
                        item.HasCost = associate.HasCost;
                        item.ProductName = associate.ProductName;
                        item.RatePerAreaUnit = UnitFactory.GetPackageSafeUnit(associate.RatePerAreaUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                        item.RatePerAreaValue = associate.RatePerAreaValue;
                        item.TotalProductUnit = UnitFactory.GetPackageSafeUnit(associate.TotalProductUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                        item.TotalProductValue = associate.TotalProductValue;
                        IUnit customUnit = UnitFactory.GetPackageSafeUnit(associate.CustomProductUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);

                        switch (associate.CustomRateType)
                        {
                            case "ByBag":
                                item.DisplayCustomUnit = new CompositeUnit(customUnit.AbbreviatedDisplay, 1, Strings.Unit_Bag_Text.ToLower());
                                break;
                            case "ByCWT":
                                item.DisplayCustomUnit = new CompositeUnit(customUnit.AbbreviatedDisplay, 100, Strings.Unit_Weight_Text.ToLower());
                                break;
                            case "ByRow":
                                item.DisplayCustomUnit = new CompositeUnit(customUnit.AbbreviatedDisplay, 1000, Strings.Unit_LengthRow_Text.ToLower());
                                break;
                            case "BySeed":
                                var parentProd = masterlist.GetProduct(prod.Id);
                                item.DisplayCustomUnit = new CompositeUnit(customUnit.AbbreviatedDisplay, 1, parentProd.StdUnit);
                                break;
                            default:

                                break;
                        }

                        if (associate.HasCost)
                        {
                            var associateInvProd = inventory.Products.SingleOrDefault(x => x.Key == associate.ProductId).Value;

                            var avgCost = associateInvProd != null ? associateInvProd.AveragePriceValue : 0;
                            var avgCostUnit = associateInvProd != null && !string.IsNullOrEmpty(associateInvProd.AveragePriceUnit) ? UnitFactory.GetPackageSafeUnit(associateInvProd.AveragePriceUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit) : null;
                            var totalAssociateProd = UnitFactory.GetPackageSafeUnit(associate.TotalProductUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit).GetMeasure((double)associate.TotalProductValue, mlp.Density);
                            var convertedValue = avgCostUnit != null && totalAssociateProd.Unit != avgCostUnit && totalAssociateProd.CanConvertTo(avgCostUnit) ? totalAssociateProd.GetValueAs(avgCostUnit) : totalAssociateProd.Value;
                            item.TotalCost = (decimal)convertedValue;
                            //TotalAvgCost += avgCost != 0 ? (decimal)(convertedValue * avgCost) : 0;


                            if (associate.SpecificCostPerUnitValue != 0 && !string.IsNullOrEmpty(associate.SpecificCostPerUnitUnit))
                            {
                                var specificUnit = UnitFactory.GetPackageSafeUnit(associate.SpecificCostPerUnitUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                                var convertedSpecificValue = totalAssociateProd.Unit != specificUnit && totalAssociateProd.CanConvertTo(specificUnit) ? totalAssociateProd.GetValueAs(specificUnit) : totalAssociateProd.Value;
                                var specificTotalCost = (decimal)convertedSpecificValue * associate.SpecificCostPerUnitValue;
                                item.TotalCost = specificTotalCost;
                                //TotalSpecificCost += specificTotalCost;
                            }
                        }

                        prodData.AssociatedProducts.Add(item);
                    }

                    data.Products.Add(prodData);
                    cnt++;
                }

                data.TotalCost = data.Products.Sum(x => x.TotalCost);
                List<string> filteredMapData = new List<string>();

                foreach (var cz in appData.CropZones)
                {
                    var czDetails = endpoint.GetView<CropZoneDetailsView>(cz.Id).GetValue(new CropZoneDetailsView());

                    var czTreeData = (from f in flattenedHierarchy.Items
                                      where f.CropZoneId == cz.Id
                                      select f).FirstOrDefault();

                    var reportedArea = ReportedArea(czDetails);

                    var fieldDetails = endpoint.GetView<FieldDetailsView>(czDetails.FieldId).Value;
                    var centerLatLon = string.IsNullOrEmpty(czDetails.CenterLatLong) ? fieldDetails.CenterLatLong : czDetails.CenterLatLong;

                    ApplicationData.FieldLineItemData field = new ApplicationData.FieldLineItemData()
                    {
                        Area = cz.Area.Value,
                        CenterLatLong = centerLatLon,
                        Crop = masterlist.GetCropDisplay(czDetails.CropId),
                        CropZone = czDetails.Name,
                        Field = czTreeData.FieldName,
                        Farm = czTreeData.FarmName,
                    };
                    data.Fields.Add(field);
                    var maps = endpoint.GetView<ItemMap>(cz.Id);
                    try
                    {
                        if (maps.HasValue)
                        {
                            if (maps.Value.MostRecentMapItem != null && maps.Value.MostRecentMapItem.MapData != string.Empty && maps.Value.MostRecentMapItem.MapData != "MULTIPOLYGON EMPTY")
                            {
                                filteredMapData.Add(maps.Value.MostRecentMapItem.MapData);
                            }
                        }
                    }
                    finally { }
                }
                decimal envvalue = 0;
                if (appData.EnvironmentalZoneDistanceValue.HasValue)
                {
                    envvalue = appData.EnvironmentalZoneDistanceValue.Value;
                }

                //var converter = new ConvertToBitmap(filteredMapData, width, height, this.endpoint, envvalue, appData.EnvironmentalZoneDistanceUnit, appData.Severity);
                ////var converter = new ConvertToBitmap(filteredMapData, width, height, this.endpoint);
                //data.MapImage = converter.BitMap;

                foreach (var applicator in appData.Applicators)
                {
                    ApplicationData.ApplicatorLineItemData appLineItem = new ApplicationData.ApplicatorLineItemData()
                    {
                        Name = applicator.PersonName,
                        Company = applicator.CompanyName,
                        LicenseNumber = applicator.LicenseNumber,
                        ExpirationDate = applicator.Expires,
                        DisplayName = string.Format("{0} ({1})", applicator.PersonName, applicator.CompanyName),
                    };

                    data.Applicators.Add(appLineItem);
                }

                data.Authorizer = appData.Authorization != null ? appData.Authorization.PersonName : string.Empty;
                data.AuthorizerDate = appData.Authorization != null ? appData.Authorization.AuthorizationDate : DateTime.Now;

                data.CropYear = cropYear;
                var sourceArray = from source in appData.Sources
                                  select "Source :: " + source.DocumentType + "  #" + source.Name + "\n";
                var sourcesString = string.Join("  ", sourceArray);
                data.Notes = appData.Notes;
                if (appData.Sources.Any())
                {
                    data.Notes = data.Notes + "\n" + "******************************************************************** \n" + sourcesString;
                }
            }

            return data;
        }

        private double ReportedArea(CropZoneDetailsView czDetails)
        {
            if (czDetails.ReportedArea != null)
            {
                return (double)czDetails.ReportedArea;
            }
            else if (czDetails.ReportedArea == null && czDetails.BoundaryArea != null)
            {
                return (double)czDetails.BoundaryArea;
            }
            else if (czDetails.ReportedArea == null && czDetails.BoundaryArea == null)
            {
                return 0.0;
            }
            else
            {
                return 0.0;
            }
        }
    }
}
