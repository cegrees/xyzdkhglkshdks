﻿using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.ReportModels.Application;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Landdb.Infrastructure;
using Landdb.Resources;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.Application.Generators
{
    public class WPSGenerator : IApplicationReportGenerator
    {
        IClientEndpoint endpoint;
        int cropYear;

        public WPSGenerator(IClientEndpoint endPoint, int cropYear)
        {
            this.endpoint = endPoint;
            this.cropYear = cropYear;
        }

        public Telerik.Reporting.ReportSource Generate(ApplicationQueryCriteria crit)
        {
            ReportBook book = new ReportBook();
            var rs = new InstanceReportSource();

            var appData = endpoint.GetView<ApplicationView>(crit.SelectedApplication).Value;
            var masterlist = endpoint.GetMasterlistService();

            if (appData != null)
            {
                var data = new ApplicationData();


                foreach (var prod in appData.Products)
                {
                    
                    var mlp = masterlist.GetProduct(prod.Id);


                    try
                    {
                        string dataText = string.Empty;

                        //create task....and wait for it to complete...
                        Task getPPE = Task.Factory.StartNew(() =>
                        {
                            var requestUriFormat = @"{0}/workerprotections?format=json&c=en-US&u=Y29yZXkucGVya2lucw%3D%3D&p=Y29yZXlwMQ%3D%3D&ProductIds={1}";
                            var requestUri = string.Format(requestUriFormat, Infrastructure.ApplicationEnvironment.MasterlistRemoteUri, prod.Id.Id);

                            WebClient client = new WebClient();
                            client.Headers.Add("Authorization", "basic QWdDQVBJOlJhbmRvbU1vbmtleVN1cmZzT25MYXZhIQ==");
                            dataText = client.DownloadString(new Uri(requestUri));
                        });

                        getPPE.Wait();
                        JObject proddata = JObject.Parse(dataText);

                        var ppe = from d in JArray.Parse(proddata["ProductWorkerProtections"].ToString())
                                  select JArray.Parse(d["WorkerProtections"].ToString());

                        var workerProtectionList = from d in JArray.Parse(proddata["ProductWorkerProtections"].ToString())
                                                   from i in JArray.Parse(d["WorkerProtections"].ToString())
                                                   select new WorkerProtection { PPE = i["PPE"] != null ? i["PPE"].ToString() : string.Empty, PPEReentry = i["PPEReentry"] != null ? i["PPEReentry"].ToString() : string.Empty };

                        foreach (var wp in workerProtectionList)
                        {
                            var wpsItem = new Landdb.ReportModels.Application.ApplicationData.WorkerProtectionItem();
                            wpsItem.ProductId = prod.Id;
                            wpsItem.ProductName = mlp.Name;
                            wpsItem.SignalWord = mlp.SignalWord;
                            wpsItem.PPE = wp.PPE;
                            wpsItem.ReEntry = wp.PPEReentry;
                            wpsItem.RestrictedUse = mlp.RestrictedUse ? "True" : "False";
                            wpsItem.DataSourceName = ApplicationEnvironment.CurrentDataSourceName;
                            wpsItem.CropYear = cropYear.ToString();

                            data.WorkerProtectionItems.Add(wpsItem);
                        }
                    }
                    catch (Exception x)
                    {

                    }
                }

                var report = new Landdb.Views.Secondary.Reports.Application.WPS();
                report.DataSource = data.WorkerProtectionItems;
                report.Name = Strings.ReportName_WPS_Text.ToUpper();

                book.Reports.Add(report);
            }

            rs.ReportDocument = book;
            return rs;
        }

        public string DisplayName
        {
            get { return Strings.ReportName_WPS_Text.ToUpper(); }
        }



        //public string GetWorkerProtectionData(ProductId productId)
        //{
        //var requestUriFormat = @"{0}/workerprotections?format=json&c=en-US&u=Y29yZXkucGVya2lucw%3D%3D&p=Y29yZXlwMQ%3D%3D&ProductIds={1}";
        //var requestUri = string.Format(requestUriFormat, Infrastructure.ApplicationEnvironment.MasterlistRemoteUri, prod.Id.Id);

        //    WebClient client = new WebClient();
        //    client.Headers.Add("Authorization", "basic QWdDQVBJOlJhbmRvbU1vbmtleVN1cmZzT25MYXZhIQ==");
        //    string data = client.DownloadString(new Uri(requestUri));

        //    return data;
        //}

        public class ProductWorkerProtection
        {
            public ProductId ProductId { get; set; }
            public List<WorkerProtection> WorkerProtections { get; set; }
            //public string PPE { get; set; }
            //public string PPEReEntry { get; set; }
        }

        public class WorkerProtection
        {
            public string PPE { get; set; }
            public string PPEReentry { get; set; }
        }
    }
}
