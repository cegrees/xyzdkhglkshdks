﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Converters.Reports;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Fields;
using Landdb.ViewModel.Reports;
using Landdb.ViewModel.Secondary.Reports.AppliedProduct.Generators;
using Microsoft.Win32;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Threading;
using Telerik.Reporting;
using Telerik.Reporting.Processing;

namespace Landdb.ViewModel.Secondary.Reports.AppliedProduct {
    public class AppliedProductReportCreatorViewModel : ViewModelBase, Updatable
    {
        IAppliedProductReportGenerator selectedReportGenerator;
        ReportTemplateViewModel template;
        ReportTemplateViewModelToFile templateService = new ReportTemplateViewModelToFile();
        string reportType;
        AppliedQueryCriteria criteria;
        Dispatcher dispatcher;

        public AppliedProductReportCreatorViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, int cropYear, ReportTemplateViewModel template, string reportType) {
            CancelCommand = new RelayCommand(OnCancelReport);
            SaveCommand = new RelayCommand(SaveTemplate);
            this.dispatcher = dispatcher;

            FieldsPageModel = new AppliedProductReportFieldSelectionViewModel(clientEndpoint, dispatcher, cropYear);
            InfoPageModel = new AppliedProductReportInfoViewModel(clientEndpoint, dispatcher, cropYear);

            ReportGenerators = new ObservableCollection<IAppliedProductReportGenerator>();
            //Call to add all the Applied Product Reports
            AddAppliedProductReportGenerators(clientEndpoint, cropYear);

            //SelectedReportGenerator = ReportGenerators.First();
            PDFExport = new RelayCommand(this.PDF);

            Update();
            this.reportType = reportType;
            //take passed in template and set values on associated ViewModels along with Criteria page
            if (template != null) { InitializeFromTemplate(template); this.template = template; };

            SelectedPageIndex = 0;
        }

        public ICommand CancelCommand { get; private set; }
        public RelayCommand PDFExport { get; set; }
        public ICommand SaveCommand { get; set; }

        public ReportSource ReportSource { get; set; }

        public string TemplateName { get; set; }

        public AppliedProductReportFieldSelectionViewModel FieldsPageModel { get; private set; }
        public AppliedProductReportInfoViewModel InfoPageModel {get; private set;}

        public ObservableCollection<IAppliedProductReportGenerator> ReportGenerators { get; private set; }
        public IAppliedProductReportGenerator SelectedReportGenerator {
            get { return selectedReportGenerator; }
            set {
                selectedReportGenerator = value;
                Update();
                RaisePropertyChanged("SelectedReportGenerator");
            }
        }

        public int SelectedPageIndex { get; set; }
        public string CropYear { get { return ApplicationEnvironment.CurrentCropYear.ToString(); } }

        void OnCancelReport() {
            Messenger.Default.Send<ShowMainViewMessage>(new ShowMainViewMessage());
        }

        public void Update() {
            if (SelectedReportGenerator != null) {
                criteria = BuildCriteria();
                ReportSource = SelectedReportGenerator.Generate(criteria);
                RaisePropertyChanged("ReportSource");
            }
        }

        AppliedQueryCriteria BuildCriteria() {
            AppliedQueryCriteria criteria = new AppliedQueryCriteria();

            var czq = from cz in FieldsPageModel.SelectedCropZones
                      select cz.Id;
            criteria.SelectedCropZones.AddRange(czq);

            criteria.QueryStartDate = InfoPageModel.ReportStartDate.AddHours(InfoPageModel.ReportStartDate.Hour * -1);
            criteria.QueryEndDate = InfoPageModel.ReportEndDate.AddHours(23.999); //.AddDays(1);
            criteria.CostType = InfoPageModel.CostType;
            criteria.SelectedEntity = InfoPageModel.Entity;

            var includedProds = from p in InfoPageModel.Products
                                where p.Checked == true
                                select p.Id;
            criteria.SelectedProducts.AddRange(includedProds);

            return criteria;
        }

        void AddAppliedProductReportGenerators(IClientEndpoint clientEndpoint, int cropYear)
        {
            if (ApplicationEnvironment.CurrentDataSource.DatasourceCulture.ToLower() == "en-us")
            {
                ReportGenerators.Add(new ByFieldDetailGenerator(clientEndpoint, cropYear));
                ReportGenerators.Add(new AppliedProductDetailGenerator(clientEndpoint, cropYear));
                ReportGenerators.Add(new RegulatoryGenerator(clientEndpoint, cropYear));
                ReportGenerators.Add(new AppliedProductSummaryGenerator(clientEndpoint, cropYear));
                ReportGenerators.Add(new ByDayGenerator(clientEndpoint, cropYear));
                ReportGenerators.Add(new FertilzerReportGenerator(clientEndpoint, cropYear));
                ReportGenerators.Add(new NoticeOfApplicationGenerator(clientEndpoint, cropYear));
                ReportGenerators.Add(new ReEntryReportGenerator(clientEndpoint, cropYear));
                ReportGenerators.Add(new PreHarvestReportGenerator(clientEndpoint, cropYear));
                ReportGenerators.Add(new ShipperPackerGenerator(clientEndpoint, cropYear));
                ReportGenerators.Add(new ApplicationNotesGenerator(clientEndpoint, cropYear));
                ReportGenerators.Add(new LandOwnerShareGenerator(clientEndpoint, cropYear));
                //TO DO :: ADD THIS ONE IN LATER
                //ReportGenerators.Add(new DicambaGenerator(clientEndpoint, ApplicationEnvironment.CurrentCropYear));
                ReportGenerators.Add(new DicambaDataGenerator(clientEndpoint, dispatcher, cropYear));

            }
            else if (ApplicationEnvironment.CurrentDataSource.DatasourceCulture.ToLower() == "en-ca")
            {
                ReportGenerators.Add(new ByFieldDetailGenerator(clientEndpoint, cropYear));
                ReportGenerators.Add(new AppliedProductDetailGenerator(clientEndpoint, cropYear));
                ReportGenerators.Add(new RegulatoryGenerator(clientEndpoint, cropYear));
                ReportGenerators.Add(new AppliedProductSummaryGenerator(clientEndpoint, cropYear));
                ReportGenerators.Add(new ByDayGenerator(clientEndpoint, cropYear));
                ReportGenerators.Add(new FertilzerReportGenerator(clientEndpoint, cropYear));
                ReportGenerators.Add(new NoticeOfApplicationGenerator(clientEndpoint, cropYear));
                ReportGenerators.Add(new ReEntryReportGenerator(clientEndpoint, cropYear));
                ReportGenerators.Add(new PreHarvestReportGenerator(clientEndpoint, cropYear));
                ReportGenerators.Add(new ShipperPackerGenerator(clientEndpoint, cropYear));
                ReportGenerators.Add(new ApplicationNotesGenerator(clientEndpoint, cropYear));
                ReportGenerators.Add(new LandOwnerShareGenerator(clientEndpoint, cropYear));
            }
        }

        public void PDF()
        {
            try
            {
                if (ReportSource == null) return;
                if (ReportSource == null) return;
                
                ReportSource reportExport = ReportSource;

                ReportProcessor reportProcessor = new ReportProcessor();
                System.Collections.Hashtable deviceInfo =
                new System.Collections.Hashtable();
                RenderingResult result = reportProcessor.RenderReport("PDF", reportExport, deviceInfo);

                SaveFileDialog saveFileDialog1 = new SaveFileDialog();

                saveFileDialog1.Filter = "Adobe Reader|*.pdf";
                saveFileDialog1.FilterIndex = 2;
                saveFileDialog1.RestoreDirectory = true;

                Nullable<bool> resultSaved = saveFileDialog1.ShowDialog();

                // Process save file dialog box results 
                if (resultSaved == true)
                {
                    // Save document 
                    string filename = saveFileDialog1.FileName;

                    try
                    {
                        using (System.IO.FileStream fs = new System.IO.FileStream(filename, System.IO.FileMode.Create))
                        {
                            fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
                        }

                        System.Diagnostics.Process.Start(filename);
                    }
                    catch (Exception ex) { }

                }
            }catch(Exception ex)
            {
                //do nothing
            }
        }

        //public void RTF()
        //{
        //    ReportSource reportExport = ReportSource;

        //    ReportProcessor reportProcessor = new ReportProcessor();
        //    System.Collections.Hashtable deviceInfo =
        //    new System.Collections.Hashtable();
        //    RenderingResult result = reportProcessor.RenderReport("PDF", reportExport, deviceInfo);

        //    SaveFileDialog saveFileDialog1 = new SaveFileDialog();

        //    saveFileDialog1.Filter = "Adobe Reader|*.pdf";
        //    saveFileDialog1.FilterIndex = 2;
        //    saveFileDialog1.RestoreDirectory = true;

        //    Nullable<bool> resultSaved = saveFileDialog1.ShowDialog();

        //    // Process save file dialog box results 
        //    if (resultSaved == true)
        //    {
        //        // Save document 
        //        string filename = saveFileDialog1.FileName;

        //        try
        //        {
        //            using (System.IO.FileStream fs = new System.IO.FileStream(filename, System.IO.FileMode.Create))
        //            {
        //                fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
        //            }
        //        }
        //        catch (Exception ex) { }

        //    }
        //}

        void SaveTemplate()
        {
            criteria = BuildCriteria();
            if (template != null && template.TemplateName == TemplateName)
            {
                ReportTemplateViewModel editedTemplate = new ReportTemplateViewModel()
                {
                    TemplateName = TemplateName,
                    Created = template.Created,
                    LastUsed = DateTime.Now,
                    CriteriaJSON = JsonConvert.SerializeObject(criteria),
                    ReportType = template.ReportType,
                    DataSourceId = template.DataSourceId
                };

                //TODO :: CALL TO EDIT PREVIOUS TEMPLATE...
                templateService.EditTemplate(editedTemplate);
            }
            else
            {
                ReportTemplateViewModel newTemplate = new ReportTemplateViewModel()
                {
                    TemplateName = TemplateName,
                    Created = DateTime.Now,
                    LastUsed = DateTime.Now,
                    CriteriaJSON = JsonConvert.SerializeObject(criteria),
                    ReportType = reportType,
                    DataSourceId = ApplicationEnvironment.CurrentDataSourceId
                };

                //TODO :: call service to save the new report template
                templateService.SaveNewTemplate(newTemplate);
                template = newTemplate;
            }

            RaisePropertyChanged("NewTemplateCreated", null, new ReportTemplateViewModel(), true);
        }

        void InitializeFromTemplate(ReportTemplateViewModel temp)
        {
            TemplateName = temp.TemplateName;

            AppliedQueryCriteria crit = JsonConvert.DeserializeObject<AppliedQueryCriteria>(temp.CriteriaJSON);

            //InfoPageModel.Products = crit.SelectedProducts;
            InfoPageModel.ReportStartDate = crit.QueryStartDate;
            InfoPageModel.ReportEndDate = crit.QueryEndDate;
            InfoPageModel.CostType = crit.CostType;

            var dsID = new DataSourceId(ApplicationEnvironment.CurrentDataSourceId);

            var RootTreeItemModels = FieldsPageModel.RootTreeItemModels;
            if (RootTreeItemModels.Any())
            {
                if (RootTreeItemModels.First() is GrowerTreeItemViewModel)
                {
                    var toCheck = from farm in RootTreeItemModels.First().Children
                                  from field in farm.Children
                                  from cz in field.Children
                                  where crit.SelectedCropZones.Contains(cz.Id)
                                  select cz;
                    toCheck.ForEach(x => x.IsChecked = true);
                }
                else
                {
                    var toCheck = from crop in RootTreeItemModels.First().Children
                                  from farm in crop.Children
                                  from field in farm.Children
                                  from cz in field.Children
                                  where crit.SelectedCropZones.Contains(cz.Id)
                                  select cz;
                    toCheck.ForEach(x => x.IsChecked = true);
                }
            }

            var unSelectedProds = from infoProds in InfoPageModel.Products
                                where !crit.SelectedProducts.Contains(infoProds.Id)
                                select infoProds;
            unSelectedProds.ForEach(x => x.Checked = false);
        }
    }
}
