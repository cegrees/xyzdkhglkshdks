﻿using AgC.UnitConversion;
using AgC.UnitConversion.Area;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.ActiveIngredientSetting;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Company;
using Landdb.Domain.ReadModels.Contract;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Domain.ReadModels.Person;
using Landdb.Domain.ReadModels.ProductSetting;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.ReportModels.ShipperPacker;
using Landdb.ReportServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Threading;
using Landdb.Resources;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.AppliedProduct.Generators
{
    public class ShipperPackerGenerator : IAppliedProductReportGenerator
    {
        IClientEndpoint clientEndpoint;
        int cropYear;

        public ShipperPackerGenerator(IClientEndpoint endpoint, int cropyear)
        {
            this.clientEndpoint = endpoint;
            this.cropYear = cropyear;
        }

        public Telerik.Reporting.ReportSource Generate(AppliedQueryCriteria criteria)
        {
            ReportBook book = new ReportBook();
            var rs = new InstanceReportSource();

            var applicationData = clientEndpoint.GetView<CropZoneApplicationDataView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new CropZoneApplicationDataView());
            //var inventory = clientEndpoint.GetView<InventoryListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new InventoryListView());
            var flattenedHierarchy = clientEndpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new FlattenedTreeHierarchyView());
            var masterlist = clientEndpoint.GetMasterlistService();

            if (criteria.SelectedCropZones.Any())
            {
                foreach (var czId in criteria.SelectedCropZones)
                {

                    var czAppData = (from x in applicationData.Items
                                    where x.CropZoneId == czId &&
                                    x.EndDate >= criteria.QueryStartDate &&
                                    x.EndDate <= criteria.QueryEndDate && criteria.SelectedProducts.Contains(x.ProductId.Id) == true
                                    select x).OrderBy(date => date.EndDate);

                    if (czAppData.Count() > 0)
                    {
                        var czTreeData = (from f in flattenedHierarchy.Items
                                          where f.CropZoneId == czId
                                          select f).FirstOrDefault();

                        var czDetails = clientEndpoint.GetView<CropZoneDetailsView>(czId).GetValue(new CropZoneDetailsView());
                        var fieldDetails = clientEndpoint.GetView<FieldDetailsView>(czDetails.FieldId).GetValue(new FieldDetailsView());
                        var data = new ShipperPackerData();
                        data.ActiveItems = new List<ActiveIngredientItemData>();
                        var settings = clientEndpoint.GetUserSettings();
                        data.DataSourceName = !string.IsNullOrWhiteSpace(criteria.SelectedEntity.Name) ? criteria.SelectedEntity.Name : ApplicationEnvironment.CurrentDataSourceName;
                        if (criteria.SelectedEntity != null && criteria.SelectedEntity.Type == "Company")
                        {
                            var companyMaybe = clientEndpoint.GetView<CompanyDetailsView>(new CompanyId(ApplicationEnvironment.CurrentDataSourceId, criteria.SelectedEntity.Id));
                            if (companyMaybe.HasValue)
                            {
                                var company = companyMaybe.Value;
                                if (company.StreetAddress != null)
                                {
                                    data.Address = company.StreetAddress.AddressLine1;
                                    data.City = company.StreetAddress.City;
                                    data.State = company.StreetAddress.State;
                                    data.ZipCode = company.StreetAddress.ZipCode;
                                }
                            }
                        }
                        else if (criteria.SelectedEntity != null)
                        {
                            var personMaybe = clientEndpoint.GetView<PersonDetailsView>(new PersonId(ApplicationEnvironment.CurrentDataSourceId, criteria.SelectedEntity.Id));
                            if (personMaybe.HasValue)
                            {
                                var person = personMaybe.Value;
                                if (person.StreetAddress != null)
                                {
                                    data.Address = person.StreetAddress.AddressLine1;
                                    data.City = person.StreetAddress.City;
                                    data.State = person.StreetAddress.State;
                                    data.ZipCode = person.StreetAddress.ZipCode;
                                }
                            }
                        }

                        if (czDetails.ReportedArea != null && czDetails.ReportedArea > 0)
                        {
                            data.Area = UnitFactory.GetUnitByName(czDetails.ReportedAreaUnit).GetMeasure(czDetails.ReportedArea.Value).FullDisplay;
                        }
                        else if (czDetails.BoundaryArea != null)
                        {
                            data.Area = UnitFactory.GetUnitByName(czDetails.BoundaryAreaUnit).GetMeasure(czDetails.BoundaryArea.Value).FullDisplay;
                        }
                        else
                        {
                            data.Area = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(0).FullDisplay;
                        }

                        data.CropYear = cropYear;
	                    data.DateRangeDisplay =
		                    $"{criteria.QueryStartDate.ToShortDateString()} - {criteria.QueryEndDate.ToShortDateString()}";
                        data.Township = fieldDetails.Township + fieldDetails.TownshipDirection;
                        data.Range = fieldDetails.Range + fieldDetails.RangeDirection;
                        data.Section = fieldDetails.Section;
                        data.Merdian = fieldDetails.Meridian;
                        data.PermitId = string.IsNullOrEmpty(czDetails.PermitId) ? fieldDetails.PermitId : czDetails.PermitId;
                        var farmPermitId = clientEndpoint.GetView<FarmDetailsView>(flattenedHierarchy.Items.FirstOrDefault(x => x.FieldId == czDetails.FieldId).FarmId).Value.PermitId;
                        data.PermitId = string.IsNullOrEmpty(data.PermitId) ? farmPermitId : data.PermitId;
                        data.SiteId = czDetails.SiteId;
                        data.BlockId = czDetails.CropZonePermitId;
                        data.CropZone = czDetails.Name;
                        data.Field = fieldDetails.FieldNameByCropYear[cropYear]; //.Name;
                        data.Farm = czTreeData.FarmName;
                        data.CenterLatLon = czDetails.CenterLatLong;
                        data.Crop = masterlist.GetCropDisplay(czDetails.CropId);
                        data.County = fieldDetails.County;

                        if (czDetails.ProductionContracts.Any())
                        {
                            var contractId = czDetails.ProductionContracts.FirstOrDefault().Id;
                            var contractDetails = clientEndpoint.GetView<ProductionContractDetailsView>(contractId).Value;
                            data.ContractId = contractDetails.ContractNumber;
                            data.GrowerId = contractDetails.GrowerId;
                        }

                        int cnt = 0;
                        foreach (var czAppRecord in czAppData)
                        {      
                            var mlp = masterlist.GetProduct(czAppRecord.ProductId);

                            if (mlp.ProductType == GlobalStrings.ProductType_Seed)
                            {
                                data.VarietyDisplay += "(" + czAppRecord.StartDate.ToLocalTime().ToShortDateString() + " : " + mlp.Name + ")";
                            }

                            var ReiPhis = masterlist.GetProductReiPhis(czAppRecord.ProductId);
                            ReportLineItemData rlid = new ReportLineItemData();

                            var displayRate = DisplayMeasureConverter.ConvertToRateDisplayMeasure(czAppRecord.ProductId, czAppRecord.RateValue, czAppRecord.RateUnit, mlp.Density, clientEndpoint, ApplicationEnvironment.CurrentDataSourceId);
                            var displayTotal = DisplayMeasureConverter.ConvertToTotalDisplayMeasure(czAppRecord.ProductId, czAppRecord.TotalProductValue, czAppRecord.TotalProductUnit, mlp.Density, clientEndpoint, ApplicationEnvironment.CurrentDataSourceId);

                            var applicationDetail = clientEndpoint.GetView<ApplicationView>(czAppRecord.ApplicationId).Value;
                            var appliedProduct = applicationDetail.Products.SingleOrDefault(x => x.TrackingId == czAppRecord.ProductTrackingId);
                            rlid.AppMethod = appliedProduct.ApplicationMethod;
                            rlid.Pest = appliedProduct.TargetPest != null ? appliedProduct.TargetPest.ToString() : string.Empty;
                            rlid.StartDateTime = czAppRecord.StartDate.ToLocalTime();
                            rlid.StartDate = czAppRecord.StartDate.ToLocalTime().ToString("MM/dd/yy hh:mm tt");
                            rlid.EndDate = czAppRecord.EndDate.ToLocalTime().ToString("MM/dd//yy hh:mm tt");
                            rlid.RateDisplay = czAppRecord.RateValue.ToString("n2") + " " + UnitFactory.GetUnitByName(czAppRecord.RateUnit).AbbreviatedDisplay; // TODO: Make this a measure.
                            rlid.Rate = displayRate.Value;
                            rlid.RateUnit = displayRate.Unit.AbbreviatedDisplay;
                            rlid.AreaDisplay = UnitFactory.GetUnitByName(czAppRecord.AreaUnit).GetMeasure(czAppRecord.AreaValue * czAppRecord.CoveragePercent).AbbreviatedDisplay;
                            rlid.ProductDisplay = mlp.Name;
                            rlid.Manufacturer = mlp.Manufacturer;
                            rlid.TotalProductDisplay = string.Format("{0} {1}", displayTotal.Value.ToString("n2"), displayTotal.Unit.AbbreviatedDisplay); // TODO: Make this a measure.
                            rlid.TotalProduct = displayTotal.Value;
                            rlid.TotalUnit = displayTotal.Unit.AbbreviatedDisplay;
                            var tankInfo =applicationDetail.TankInformation != null ? applicationDetail.TankInformation : null;
                            rlid.CarrierPerAreaDisplay = tankInfo != null && tankInfo.CarrierPerAreaValue != null ? string.Format("{0} {1}", tankInfo.CarrierPerAreaValue.ToString("N2"), UnitFactory.GetUnitByName(tankInfo.CarrierPerAreaUnit).AbbreviatedDisplay) : string.Empty;

                            var registrationNumber = mlp.RegistrationNumber;

                            if (registrationNumber == null) { rlid.EPANumber = string.Empty; }
                            else
                            {
                                Regex epaNumberRegex = new Regex("([1-9][0-9]*)");

                                var matches = from m in epaNumberRegex.Matches(registrationNumber).Cast<Match>()
                                              select m.Value;
                                rlid.EPANumber = string.Join("-", matches.ToArray());
                            }

                            var ais = from a in mlp.ActiveIngredients
                                      select masterlist.GetActiveIngredientDisplay(new ActiveIngredientId(a.Id));

                            rlid.Active = string.Join(" : ", ais.ToArray());

                            var productService = new ProductServices(clientEndpoint);
                            ReiPhi reiPhi = productService.ReturnReiPhi(czAppRecord.ProductId, czDetails.CropId);

                            rlid.PHI = reiPhi != null && reiPhi.Phi.HasValue ? reiPhi.Phi.ToString() : "*";
                            rlid.PHIDate = reiPhi != null && reiPhi.Phi.HasValue ? czAppRecord.EndDate.ToLocalTime().AddDays((double)reiPhi.Phi).ToString("g") : string.Empty;
                            var phiStatus = reiPhi != null && reiPhi.Phi.HasValue && czAppRecord.EndDate.ToLocalTime().AddDays((double)reiPhi.Phi) > DateTime.Now ? false : true;
                            rlid.PHIStatus = phiStatus == true ? Strings.ReportVariable_Rei_Status_Allowed : Strings.ReportVariable_Rei_Status_Denied;

                            foreach (var ai in mlp.ActiveIngredients)
                            {
                                if (ai.Id != new Guid())
                                {
                                    var aiSettingsId = new ActiveIngredientSettingId( ApplicationEnvironment.CurrentDataSourceId, new ActiveIngredientId(ai.Id) );
                                    var aiDetails = clientEndpoint.GetView<ActiveIngredientSettingsView>( aiSettingsId );
                                    var itemData = new ActiveIngredientItemData();

                                    if (czDetails.ReportedArea != null && czDetails.ReportedArea > 0)
                                    {
                                        itemData.Acres = czDetails.ReportedArea.Value;
                                    }
                                    else if (czDetails.BoundaryArea != null)
                                    {
                                        itemData.Acres = czDetails.BoundaryArea.Value;
                                    }
                                    else
                                    {
                                        itemData.Acres = 0;
                                    }

                                    itemData.ActiveIngredientID = ai.Id;
                                    itemData.Name = clientEndpoint.GetMasterlistService().GetActiveIngredientDisplay(new ActiveIngredientId(ai.Id));
                                    itemData.TotalAllowedLoad = 0;
                                    
                                    var ProductMeasure = UnitFactory.GetUnitByName(czAppRecord.TotalProductUnit).GetMeasure(czAppRecord.TotalProductValue, mlp.Density);
                                    var pound = UnitFactory.GetUnitByName("pound");
                                    var poundsOfProductValue = ProductMeasure.CanConvertTo(pound) ? ProductMeasure.GetValueAs(pound) : ProductMeasure.Value;
                                    var totalLoad = poundsOfProductValue * (double)ai.Percent;

                                    var dataItem = data.ActiveItems.Where(x => x.ActiveIngredientID == ai.Id).FirstOrDefault();

                                    if (aiDetails.HasValue)
                                    {
                                        var value = aiDetails.Value;
                                        var item = value.ItemsByYear.Any(x=>x.Key == cropYear) ? value.ItemsByYear[cropYear] : null;

                                        if (item == null)
                                        {
                                            item = value.ItemsByYear.Any(x => x.Key < cropYear) ? (from m in value.ItemsByYear where m.Key < cropYear select m).Last().Value : null;
                                        }

                                        var czAiItem = item != null ? item.Where(crop => crop.CropId == czDetails.CropId).SingleOrDefault() : null;
                                        if (czAiItem != null)
                                        {
                                            var allowedLoad = czAiItem.MaximumValue;
                                            var allowedUnit = czAiItem.MaximumUnit;

                                            if (allowedLoad != null)
                                            {
                                                var allowedAIMeasure = UnitFactory.GetUnitByName(allowedUnit).GetMeasure((double)allowedLoad, 1);
                                                itemData.TotalAllowedLoad = allowedAIMeasure.Value;
                                                itemData.TotalKilogramsAllowedLoad = 1.12085116 * allowedAIMeasure.Value;
                                                
                                            }
                                        }
                                    }
                                    
                                    if (dataItem != null)
                                    {
                                        data.ActiveItems.Remove(dataItem);
                                        dataItem.TotalLoad += totalLoad;
                                        data.ActiveItems.Add(dataItem);
                
                                    }
                                    else
                                    {
                                        itemData.TotalLoad = totalLoad;
                                        data.ActiveItems.Add(itemData);
                                    }
                                }
                            }
                            rlid.ItemCount = cnt;
                            data.LineItems.Add(rlid);
                            cnt++;
                        }

                        var report = new Landdb.Views.Secondary.Reports.AppliedProduct.PHIDetailActiveSummary();
                        report.DataSource = data;
                        report.Name = czDetails.Name;
                        if (data.LineItems.Count() > 0)
                        {
                            book.Reports.Add(report);
                        }
                    }
                }
            }

            rs.ReportDocument = book;
            return rs;
        }

        public string DisplayName
        {
            get { return Strings.GeneratorName_PhiDetailActiveSummary_Text; }
        }
    }
}
