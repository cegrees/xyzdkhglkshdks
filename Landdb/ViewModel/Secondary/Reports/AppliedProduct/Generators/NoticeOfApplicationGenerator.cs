﻿using AgC.UnitConversion;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Company;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Domain.ReadModels.Person;
using Landdb.Domain.ReadModels.ProductSetting;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.ReportModels.Application;
using Landdb.ReportServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Landdb.Resources;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.AppliedProduct.Generators
{
    public class NoticeOfApplicationGenerator : IAppliedProductReportGenerator
    {
        IClientEndpoint clientEndpoint;
        int cropYear;

        public NoticeOfApplicationGenerator(IClientEndpoint clientEndpoint, int cropYear)
        {
            this.clientEndpoint = clientEndpoint;
            this.cropYear = cropYear;
        }

        public Telerik.Reporting.ReportSource Generate(AppliedQueryCriteria criteria)
        {
            ReportBook book = new ReportBook();
            var rs = new InstanceReportSource();

            var applicationData = clientEndpoint.GetView<CropZoneApplicationDataView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new CropZoneApplicationDataView());
            //var appListData = clientEndpoint.GetView<ApplicationList>((new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear))).Value;
            var inventory = clientEndpoint.GetView<InventoryListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new InventoryListView());
            var flattenedHierarchy = clientEndpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new FlattenedTreeHierarchyView());
            var masterlist = clientEndpoint.GetMasterlistService();

            //Group by applicationId and add up the product info associated with the selected cropzones.  
            //iterate over by application id count...unique ids....

            if (criteria.SelectedCropZones.Any())
            {
                var czapplicationList = (from a in applicationData.Items
                                        where criteria.SelectedCropZones.Contains(a.CropZoneId) &&
                                        a.EndDate >= criteria.QueryStartDate && a.EndDate <= criteria.QueryEndDate
                                         select a).OrderBy(x => x.StartDate);

                var applicationList = (from app in czapplicationList
                                       select app.ApplicationId).Distinct().ToList();

                foreach (var appId in applicationList)
                {
                    var data = new ApplicationData();

                    var applicationDetails = clientEndpoint.GetView<ApplicationView>(appId).Value;
                    double maxREI = 0.0;
                    double maxPHI = 0.0;
                    data.Notes = applicationDetails.Notes;
                    //TO DO :: Account for null values
                    if (applicationDetails.Conditions != null)
                    {
                        var weatherData = applicationDetails.Conditions;
                        data.Temperature = weatherData.TemperatureValue;
                        data.WindDirection = weatherData.WindDirection;
                        data.WindSpeed = weatherData.WindSpeedValue;
                        data.Humidity = weatherData.HumidityPercent;
                        data.SkyCondition = weatherData.SkyCondition == "-1" ? string.Empty : weatherData.SkyCondition;
                        data.SoilCondition = weatherData.SoilMoisture;
                    }

                    var tankInfo = applicationDetails.TankInformation != null ? applicationDetails.TankInformation : null;
                    if (tankInfo != null)
                    {
                        data.TotalCarrierValue = tankInfo.TotalCarrierValue;
                        data.TotalCarrierUnit = tankInfo.TotalCarrierUnit;
                        data.CarrierPerAreaValue = tankInfo.CarrierPerAreaValue;
                        data.CarrierPerAreaUnit = tankInfo.CarrierPerAreaUnit;
                    }

                    data.StartDate = applicationDetails.StartDateTime.ToLocalTime(); ;
                    data.EndDate = ((DateTime)applicationDetails.CustomEndDate).ToLocalTime();

                    string address1 = string.Empty;
                    string city = string.Empty;
                    string state = string.Empty;
                    string zip = string.Empty;

                    if (criteria.SelectedEntity != null && criteria.SelectedEntity.Type == "Person")
                    {
                        var personMaybe = clientEndpoint.GetView<PersonDetailsView>(new PersonId(ApplicationEnvironment.CurrentDataSourceId, criteria.SelectedEntity.Id));
                        var person = personMaybe.HasValue ? personMaybe.Value : null;
                        if (person.StreetAddress != null)
                        {
                            address1 = person != null ? person.StreetAddress.AddressLine1 : string.Empty;
                            city = person != null ? person.StreetAddress.City : string.Empty;
                            state = person != null ? person.StreetAddress.State : string.Empty;
                            zip = person != null ? person.StreetAddress.ZipCode : string.Empty;
                        }
                    }
                    else if (criteria.SelectedEntity != null && criteria.SelectedEntity.Type == "Company")
                    {
                        var companyMaybe = clientEndpoint.GetView<CompanyDetailsView>(new CompanyId(ApplicationEnvironment.CurrentDataSourceId, criteria.SelectedEntity.Id));
                        var company = companyMaybe.HasValue ? companyMaybe.Value : null;
                        if (company.StreetAddress != null)
                        {
                            address1 = company != null ? company.StreetAddress.AddressLine1 : string.Empty;
                            city = company != null ? company.StreetAddress.City : string.Empty;
                            state = company != null ? company.StreetAddress.State : string.Empty;
                            zip = company != null ? company.StreetAddress.ZipCode : string.Empty;
                        }
                    }
                    else
                    {

                    }

	                data.CropYear = cropYear;
                    data.DatasourceName = !string.IsNullOrWhiteSpace(criteria.SelectedEntity.Name) ? criteria.SelectedEntity.Name : ApplicationEnvironment.CurrentDataSourceName;
                    data.AddressDisplay = !string.IsNullOrEmpty(address1) ? string.Format("{0}\n{1}, {2}  {3}", address1, city, state, zip) : string.Empty;

                    data.ApplicationName = applicationDetails.Name;
                    var containedCZ = from includedCZ in applicationDetails.CropZones
                                      where criteria.SelectedCropZones.Where(x => x == includedCZ.Id).Count() > 0
                                      select includedCZ;

                    var containedAppliedProduct = from includedProd in applicationDetails.Products
                                                  where criteria.SelectedProducts.Where(x => x == includedProd.Id.Id).Count() > 0
                                                  select includedProd;

                    //applicator information
                    foreach (var applicator in applicationDetails.Applicators)
                    {
                        var personDetails = clientEndpoint.GetView<PersonDetailsView>(applicator.PersonId);
                        var applicatorItem = new ApplicationData.ApplicatorLineItemData()
                        {
                            Name = applicator.PersonName,
                            Company = applicator.CompanyName,

                            LicenseNumber = applicator.LicenseNumber,
                            ExpirationDate = applicator.Expires,

                            //LicenseNumber = personDetails.Value.ApplicatorLicenseNumber,
                            //ExpirationDate = personDetails.Value.ApplicatorLicenseExpiration,
                            DisplayName = string.Format("{0} ({1})", applicator.PersonName, applicator.CompanyName),
                        };

                        data.Applicators.Add(applicatorItem);
                    }

                    //each cropzone included in the criteria
                    foreach (var cz in containedCZ)
                    {
                        var cropZoneLineItem = new ApplicationData.FieldLineItemData();
                        var czDetails = clientEndpoint.GetView<CropZoneDetailsView>(cz.Id).Value;
                        var flattened = flattenedHierarchy.Items.SingleOrDefault(x => x.CropZoneId == cz.Id);
                        cropZoneLineItem.Farm = flattened.FarmName;
                        cropZoneLineItem.Field = flattened.FieldName;
                        cropZoneLineItem.CropZone = flattened.CropZoneName;
                        cropZoneLineItem.Crop = masterlist.GetCrop(czDetails.CropId).Name;
                        cropZoneLineItem.BoundaryArea = czDetails.BoundaryArea.HasValue ? czDetails.BoundaryArea.Value : 0.0;
                        cropZoneLineItem.Area = applicationDetails.CropZones.SingleOrDefault(x => x.Id == cz.Id).Area.Value;

                        double rei = 0.0;
                        double phi = 0.0;
                        foreach (var prod in containedAppliedProduct)
                        {
                            var productService = new ProductServices(clientEndpoint);
                            ReiPhi reiPhi = productService.ReturnReiPhi(prod.Id, czDetails.CropId);

                            if (reiPhi != null)
                            {
                                rei = reiPhi.Rei.HasValue && reiPhi.Rei.Value > rei ? reiPhi.Rei.Value : rei;
                                phi = reiPhi.Phi.HasValue && reiPhi.Phi.Value > phi ? reiPhi.Phi.Value : phi;
                            }
                        }

                        cropZoneLineItem.REI = (decimal)rei;
                        cropZoneLineItem.PHI = (decimal)phi;

                        data.Fields.Add(cropZoneLineItem);
                    }

                    //PROCESS ALL THE SELECTED PRODUCTS
                    foreach (var prod in containedAppliedProduct)
                    {
                        var masterlistProd = masterlist.GetProduct(prod.Id);
                        var prodLineItem = new ApplicationData.ProductLineItemData();

                        var displayRate = DisplayMeasureConverter.ConvertToRateDisplayMeasure(prod.Id, prod.RateValue, prod.RateUnit, masterlistProd.Density, clientEndpoint, ApplicationEnvironment.CurrentDataSourceId);
                        var displayTotal = DisplayMeasureConverter.ConvertToTotalDisplayMeasure(prod.Id, (decimal)prod.TotalProductValue, prod.TotalProductUnit, masterlistProd.Density, clientEndpoint, ApplicationEnvironment.CurrentDataSourceId);

                        prodLineItem.RateValue = displayRate.Value; // (double)prod.RateValue;
                        prodLineItem.RateUnit = displayRate.Unit.AbbreviatedDisplay; // prod.RateUnit;
                        prodLineItem.ProductName = masterlistProd.Name;
                        prodLineItem.Manufacturer = masterlistProd.Manufacturer;
                        var aiArray = from i in masterlistProd.ActiveIngredients
                                      select masterlist.GetActiveIngredientDisplay(new ActiveIngredientId(i.Id));
                        
                        //TO DO :: PUT ACTIVE ON THIS REPORT....
                        prodLineItem.Active = string.Join(", ", aiArray);
                        var registrationNumber = masterlistProd.RegistrationNumber;

                        if (registrationNumber == null) { prodLineItem.EPANumber = string.Empty; }
                        else
                        {
                            Regex epaNumberRegex = new Regex("([1-9][0-9]*)");

                            var matches = from m in epaNumberRegex.Matches(registrationNumber).Cast<Match>()
                                          select m.Value;
                            prodLineItem.EPANumber = string.Join("-", matches.ToArray());
                        }

                        var TotalArea = ((from cz in containedCZ
                                         select cz.Area.Value).Sum()) * (double)prod.PercentApplied;
                        prodLineItem.TotalArea = (decimal)TotalArea;
                        prodLineItem.TotalAreaUnit = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit);
                        var multiplier = TotalArea / (applicationDetails.TotalArea.Value * (double)prod.PercentApplied);
                        prodLineItem.TotalValue = displayTotal.Value * multiplier; // (double)prod.TotalProductValue * multiplier;
                        prodLineItem.TotalUnit = displayTotal.Unit.AbbreviatedDisplay; // prod.TotalProductUnit;
                        prodLineItem.RUP = masterlistProd.RestrictedUse == true ? "Yes" : "No";
                        prodLineItem.ApplicationMethod = string.IsNullOrEmpty(prod.ApplicationMethod) ? string.Empty : prod.ApplicationMethod;
                        prodLineItem.Pest = prod.TargetPest == null ? string.Empty : prod.TargetPest.ToString();                      
                        data.Products.Add(prodLineItem);
                    }

                    maxREI = (double)data.Fields.Max(f => f.REI);
                    data.MaxREI = (decimal)maxREI;
                    maxPHI = (double)data.Fields.Max(z => z.PHI);
                    data.REIReEntry = applicationDetails.CustomEndDate.HasValue ? applicationDetails.CustomEndDate.Value.ToLocalTime().AddHours(maxREI) : applicationDetails.StartDateTime.ToLocalTime().AddHours(maxREI);
                    data.PhiExpiration = applicationDetails.CustomEndDate.HasValue ? applicationDetails.CustomEndDate.Value.ToLocalTime().AddDays(maxPHI) : applicationDetails.StartDateTime.ToLocalTime().AddDays(maxPHI);
                    var report = new Landdb.Views.Secondary.Reports.AppliedProduct.NoticeOfApplication();
                    report.DataSource = data;
                    report.Name = data.DatasourceName;

                    if (data.Products.Count() > 0)
                    {
                        book.Reports.Add(report);
                    }
                }
            }

            rs.ReportDocument = book;
            return rs;
        }

        public string DisplayName
        {
            get { return Strings.GeneratorName_NoticeOfApplication_Text; }
        }
    }
}
