﻿using AgC.UnitConversion;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Domain.ReadModels.Invoice;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.ReportModels.AppliedProductByFieldDetail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Landdb.Resources;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.AppliedProduct.Generators
{
    public class AppliedProductSummaryGenerator : IAppliedProductReportGenerator
    {

        IClientEndpoint clientEndpoint;
        int cropYear;

        public AppliedProductSummaryGenerator(IClientEndpoint clientEndpoint, int cropYear)
        {
            this.clientEndpoint = clientEndpoint;
            this.cropYear = cropYear;
        }

        public Telerik.Reporting.ReportSource Generate(AppliedQueryCriteria criteria)
        {
            ReportBook book = new ReportBook();
            var rs = new InstanceReportSource();

            var applicationData = clientEndpoint.GetView<CropZoneApplicationDataView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new CropZoneApplicationDataView());
            var inventory = clientEndpoint.GetView<InventoryListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new InventoryListView());
            var flattenedHierarchy = clientEndpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new FlattenedTreeHierarchyView());
            var masterlist = clientEndpoint.GetMasterlistService();

            if (criteria.SelectedCropZones.Any())
            {
                var czDetails = (from i in criteria.SelectedCropZones
                                select clientEndpoint.GetView<CropZoneDetailsView>(i).Value).ToList();
                double totalArea = 0.0;

                foreach (var det in czDetails)
                {
                    var area = det.BoundaryArea != null ? (double)det.BoundaryArea : 0.0;
                    totalArea += det.ReportedArea != null ? (double)det.ReportedArea : area;
                }

                var czapplicationList = (from a in applicationData.Items
                                        where criteria.SelectedCropZones.Contains(a.CropZoneId) &&
                                        a.EndDate >= criteria.QueryStartDate && a.EndDate <= criteria.QueryEndDate
                                        select a).ToList();
                //sum up the products
                var productIdList = (from p in czapplicationList
                                     where criteria.SelectedProducts.Contains(p.ProductId.Id)
                                     select p.ProductId).Distinct();
                var data = new ReportData();

                foreach(var cz in czDetails){
                    var czItem = new IncludedCropZoneItemData();
                    var flattened = (from f in flattenedHierarchy.Items
                                    where f.CropZoneId == cz.Id
                                    select f).FirstOrDefault();
                    czItem.FarmName = flattened.FarmName;
                    czItem.FieldName = flattened.FieldName;
                    czItem.CropZoneName = flattened.CropZoneName;
                    var area = cz.BoundaryArea != null ? (double)cz.BoundaryArea : 0.0;
                    czItem.Area = cz.ReportedArea != null ? (double)cz.ReportedArea : area;

                    data.CropZones.Add(czItem);
                }

                data.Area = totalArea;
                var areaUnit = string.IsNullOrEmpty(czDetails[0].BoundaryAreaUnit) ? czDetails[0].ReportedAreaUnit : czDetails[0].BoundaryAreaUnit;
                data.AreaDisplay = string.Format("{0} {1}", data.Area.ToString("N2"), areaUnit);
                data.DateRangeDisplay = string.Format("{0} - {1}", criteria.QueryStartDate.ToString("d"), criteria.QueryEndDate.ToString("d"));
	            data.CropYear = cropYear.ToString();
                data.DataSourceName = !string.IsNullOrWhiteSpace(criteria.SelectedEntity.Name) ? criteria.SelectedEntity.Name : ApplicationEnvironment.CurrentDataSourceName;
                foreach (var id in productIdList)
                {
                    var appliedProdList = (from applied in czapplicationList
                                          where applied.ProductId == id
                                          select applied).ToList();

                    var masterListProd = masterlist.GetProduct(id);

                    var prodLineItem = new ReportLineItemData();
                    prodLineItem.ApplicationArea = data.Area;
                    double total = 0.0;
					IUnit totalUnit = UnitFactory.GetPackageSafeUnit(appliedProdList[0].TotalProductUnit, masterListProd.StdUnit, masterListProd.StdFactor, masterListProd.StdPackageUnit);
                    double? totalCost = 0.0;
                    var productUnits = clientEndpoint.GetProductUnitResolver();
                    foreach (var item in appliedProdList)
                    {

						IUnit currentUnit = UnitFactory.GetPackageSafeUnit(item.TotalProductUnit, masterListProd.StdUnit, masterListProd.StdFactor, masterListProd.StdPackageUnit);
                        ApplicationView appDetails = clientEndpoint.GetView<ApplicationView>(item.ApplicationId).Value;
                        if (currentUnit == totalUnit)
                        {
                            total += Double.IsNaN(item.TotalProductValue) ? 0 : item.TotalProductValue;
                        }
                        else
                        {

							var measureProduct = UnitFactory.GetPackageSafeUnit(item.TotalProductUnit, masterListProd.StdUnit, masterListProd.StdFactor, masterListProd.StdPackageUnit).GetMeasure(item.TotalProductValue, masterListProd.Density);
                            var convertedProductVaue = measureProduct.CanConvertTo(totalUnit) ? measureProduct.GetValueAs(totalUnit) : measureProduct.Value;
                            total += Double.IsNaN(convertedProductVaue) ? 0 : convertedProductVaue;
                        }

                        //GET COST FOR SPECIFIC / INVOICE / AVG
                        switch (criteria.CostType)
                        {
                            case "Average":
                                prodLineItem.IncludeCost = true;
                                totalCost += GetAvgCost(item, inventory);
                                break;
                            case "Specific":
                                //get specific price
                                var appProd = appDetails.Products.SingleOrDefault(x => x.TrackingId == item.ProductTrackingId);
                                prodLineItem.IncludeCost = true;
                                var specificCostPerUnitUnit = string.IsNullOrEmpty( appProd.SpecificCostUnit ) ? null : UnitFactory.GetPackageSafeUnit(appProd.SpecificCostUnit, masterListProd.StdUnit, masterListProd.StdFactor, masterListProd.StdPackageUnit);
                                var totalProd = UnitFactory.GetPackageSafeUnit(item.TotalProductUnit, masterListProd.StdUnit, masterListProd.StdFactor, masterListProd.StdPackageUnit).GetMeasure(item.TotalProductValue, masterListProd.Density);
                                var convertedTotal = totalProd.CanConvertTo(specificCostPerUnitUnit) && totalProd.Unit != specificCostPerUnitUnit ? totalProd.GetValueAs(specificCostPerUnitUnit) : totalProd.Value;
                                var specificPrice = appProd.SpecificCostPerUnit.HasValue ? (decimal)convertedTotal * appProd.SpecificCostPerUnit.Value : 0m;
                                if (specificPrice != null && specificPrice != 0m)
                                {
                                    totalCost += (double)specificPrice;
                                }
                                else
                                {
                                    totalCost += GetAvgCost(item, inventory);
                                }
                                break;
                            case "Invoice":
                                //get invoice
                                prodLineItem.IncludeCost = true;
                                var source = appDetails.Sources.Where(x => x.DocumentType == "Invoice");
                                double invoiceCostPerUnit = 0.0;
                                int cnt = 0;
                                if (source.Count() > 0)
                                {
                                    foreach (var src in source)
                                    {
                                        var invoiceDetailMaybe = clientEndpoint.GetView<InvoiceView>(src.Identity as InvoiceId);
                                        if (invoiceDetailMaybe.HasValue)
                                        {
                                            var invoiceDetail = invoiceDetailMaybe.Value;
                                            var invoiceProducts = (from p in invoiceDetail.Products
                                                                   where p.ProductId == item.ProductId
                                                                   select p).ToList();
                                            IUnit unit = inventory.Products.ContainsKey(item.ProductId) ? UnitFactory.GetPackageSafeUnit(inventory.Products[item.ProductId].AveragePriceUnit, masterListProd.StdUnit, masterListProd.StdFactor, masterListProd.StdPackageUnit) : UnitFactory.GetPackageSafeUnit(invoiceProducts[0].TotalProductUnit, masterListProd.StdUnit, masterListProd.StdFactor, masterListProd.StdPackageUnit);

                                            foreach (var prod in invoiceProducts)
                                            {
                                                var measurePreConvert = UnitFactory.GetPackageSafeUnit(prod.TotalProductUnit, masterListProd.StdUnit, masterListProd.StdFactor, masterListProd.StdPackageUnit).GetMeasure((double)prod.TotalProductValue, masterListProd.Density);
                                                var convertBool = measurePreConvert.CanConvertTo(unit);
                                                var convertedMeasure = measurePreConvert.CanConvertTo(unit) ? unit.GetMeasure(measurePreConvert.GetValueAs(unit)) : measurePreConvert;
                                                invoiceCostPerUnit += convertedMeasure.Value != 0 ? (double)(prod.TotalCost / (decimal)convertedMeasure.Value) : (double)prod.TotalCost;
                                                cnt++;
                                            }
                                        }
                                    }

                                    if (inventory.Products.ContainsKey(item.ProductId))
                                    {
                                        try
                                        {
                                            if (invoiceCostPerUnit > 0)
                                            {
                                                var density = masterListProd.Density;
                                                var avgPriceUnit = string.IsNullOrEmpty(inventory.Products[item.ProductId].AveragePriceUnit) ? null : UnitFactory.GetPackageSafeUnit(inventory.Products[item.ProductId].AveragePriceUnit, masterListProd.StdUnit, masterListProd.StdFactor, masterListProd.StdPackageUnit); // .GetUnitByName(inventory.Products[item.ProductId].AveragePriceUnit);
                                                var totalProdUnit = item.TotalProductUnit != null ? productUnits.GetPackageSafeUnit(item.ProductId, item.TotalProductUnit) : null;
                                                var totalProdMeasure = totalProdUnit.GetMeasure(item.TotalProductValue, density);
                                                var convertedTotalProdMeasure = totalProdMeasure.CanConvertTo(avgPriceUnit) ? avgPriceUnit.GetMeasure(totalProdMeasure.GetValueAs(avgPriceUnit)) : totalProdMeasure;
                                                totalCost += invoiceCostPerUnit / cnt * convertedTotalProdMeasure.Value;
                                            }
                                            else
                                            {
                                                totalCost += GetAvgCost(item, inventory);
                                            }
                                        }
                                        catch
                                        {
                                            totalCost += 0.0;
                                        }
                                    }
                                }
                                else
                                {
                                    totalCost += GetAvgCost(item, inventory);
                                }

                                break;
                            case "None":
                                totalCost = null;
                                prodLineItem.IncludeCost = false;
                                break;
                            default:
                                if (inventory.Products.ContainsKey(item.ProductId))
                                {
                                    try
                                    {
                                        prodLineItem.IncludeCost = true;
                                        totalCost += GetAvgCost(item, inventory);
                                    }
                                    catch (Exception ex) { }
                                }
                                break;
                        }

                        //////////////////////////////////////////////////////////////////////


                    }

                    var displayTotal = DisplayMeasureConverter.ConvertToTotalDisplayMeasure(id, (decimal)total, totalUnit.Name, masterListProd.Density, clientEndpoint, ApplicationEnvironment.CurrentDataSourceId);

                    prodLineItem.EPANumber = masterListProd.RegistrationNumber;
                    prodLineItem.TotalProduct = displayTotal.Value;
                    prodLineItem.TotalUnit = displayTotal.Unit.AbbreviatedDisplay;
                    prodLineItem.TotalProductDisplay = string.Format("{0} {1}", displayTotal.Value.ToString("N2"), displayTotal.Unit.AbbreviatedDisplay);
                    prodLineItem.ProductDisplay = masterListProd.Name;
                    prodLineItem.RUP = masterListProd.RestrictedUse ? "YES" : "NO";
                    prodLineItem.Manufacturer = masterListProd.Manufacturer;
                    prodLineItem.TotalCost = totalCost;
                    data.LineItems.Add(prodLineItem);          
                }

                if (data.LineItems.Count() > 0)
                {
                    data.LineItems = data.LineItems.OrderBy(x => x.ProductDisplay).ToList();
                    var report = new Landdb.Views.Secondary.Reports.AppliedProduct.AppliedProductSummary();
                    report.DataSource = data;
                    report.Name = data.DataSourceName;

                    book.Reports.Add(report);
                }

            }
            
            rs.ReportDocument = book;
            return rs;
        }

        public string DisplayName
        {
            get { return Strings.GeneratorName_AppliedProductSummary_Text; }
        }

        public double GetAvgCost(CropZoneApplicationDataItem czAppRecord, InventoryListView inventory)
        {
            var masterlist = clientEndpoint.GetMasterlistService();
            if (inventory.Products.ContainsKey(czAppRecord.ProductId))
            {
                try
                {
					var mlProduct = masterlist.GetProduct(czAppRecord.ProductId);
                    var psu = UnitFactory.GetPackageSafeUnit(mlProduct.StdPackageUnit, mlProduct.StdUnit, mlProduct.StdFactor, mlProduct.StdPackageUnit);
                    var compatibleUnits = AgC.UnitConversion.CompatibilityFactory.GetCompatibleUnits(psu, mlProduct.Density);

					var totalMeasure = UnitFactory.GetPackageSafeUnit(czAppRecord.TotalProductUnit, mlProduct.StdUnit, mlProduct.StdFactor, mlProduct.StdPackageUnit).GetMeasure(czAppRecord.TotalProductValue, mlProduct.Density);
                    var avgPriceUnit = string.IsNullOrEmpty(inventory.Products[czAppRecord.ProductId].AveragePriceUnit) ? null : UnitFactory.GetUnitByName(inventory.Products[czAppRecord.ProductId].AveragePriceUnit);
                    var fromCompatable = compatibleUnits.SingleOrDefault(x => x.Name == avgPriceUnit.Name);
                    var convertedTotalMeasureValue = totalMeasure.CanConvertTo(fromCompatable) ? totalMeasure.GetValueAs(fromCompatable) : totalMeasure.Value;
                    return Math.Round(inventory.Products[czAppRecord.ProductId].AveragePriceValue * convertedTotalMeasureValue, 2); // TODO: This will only pan out if the units of these measures is the same. Need to take this into account.
                    //_total += rlid.TotalCost;
                }
                catch (Exception ex) { return 0.0; }
            }
            else
            {
                return 0.0;
            }
        }
    }
}
