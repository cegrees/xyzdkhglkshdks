﻿using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.ProductSetting;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.ReportModels.REIPHI;
using Landdb.ReportServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Landdb.Resources;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.AppliedProduct.Generators
{
    public class PreHarvestReportGenerator : IAppliedProductReportGenerator
    {
        IClientEndpoint clientEndpoint;
        int cropYear;

        public PreHarvestReportGenerator(IClientEndpoint endpoint, int cropYear)
        {
            this.clientEndpoint = endpoint;
            this.cropYear = cropYear;
        }

        public Telerik.Reporting.ReportSource Generate(AppliedQueryCriteria criteria)
        {
            ReportBook book = new ReportBook();
            var rs = new InstanceReportSource();
            var data = new REIPHIEntryData();

            var applicationData = clientEndpoint.GetView<CropZoneApplicationDataView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new CropZoneApplicationDataView());
            //var inventory = clientEndpoint.GetView<InventoryListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new InventoryListView());
            var flattenedHierarchy = clientEndpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new FlattenedTreeHierarchyView());
            var masterlist = clientEndpoint.GetMasterlistService();

            if (criteria.SelectedCropZones.Any())
            {
                foreach (var czId in criteria.SelectedCropZones)
                {
                    var czItem = new CropZoneItem();
                    czItem.IsAllowed = true;
                    var czAppData = from x in applicationData.Items
                                    where x.CropZoneId == czId &&
                                    x.EndDate >= criteria.QueryStartDate &&
                                    x.EndDate <= criteria.QueryEndDate && criteria.SelectedProducts.Contains(x.ProductId.Id) == true
                                    select x;

                    if (czAppData.Count() > 0)
                    {

	                    czItem.CropYear = cropYear.ToString();
                        czItem.DataSourceName = !string.IsNullOrWhiteSpace(criteria.SelectedEntity.Name) ? criteria.SelectedEntity.Name : ApplicationEnvironment.CurrentDataSourceName;
                        czItem.DateRangeDisplay = string.Format("{0} - {1}", criteria.QueryStartDate.ToString("d"), criteria.QueryEndDate.ToString("d"));

                        var czTreeData = (from f in flattenedHierarchy.Items
                                          where f.CropZoneId == czId
                                          select f).FirstOrDefault();
                        if (czTreeData != null)
                        {
                            czItem.Farm = czTreeData.FarmName;
                            czItem.Field = czTreeData.FieldName;
                            czItem.CropZone = czTreeData.CropZoneName;
                            czItem.Crop = masterlist.GetCropDisplay(czTreeData.CropId);
                        }

                        var czDetails = clientEndpoint.GetView<CropZoneDetailsView>(czId).GetValue(new CropZoneDetailsView());

                        foreach (var czAppRecord in czAppData)
                        {
                            var mlp = masterlist.GetProduct(czAppRecord.ProductId);

                            var productService = new ProductServices(clientEndpoint);
                            ReiPhi reiPhi = productService.ReturnReiPhi(czAppRecord.ProductId, czDetails.CropId);

                            if (reiPhi != null)
                            {
                                var expire = reiPhi.Phi.HasValue ? czAppRecord.EndDate.ToLocalTime().AddDays((double)reiPhi.Phi) : czAppRecord.EndDate.ToLocalTime();
                                if (expire > czItem.PhiExpiration)
                                {
                                    var phi = (decimal)reiPhi.Phi;
                                    czItem.Phi = phi;
                                    czItem.PhiExpiration = expire;
                                }
                            }

                            var applicationDetail = clientEndpoint.GetView<ApplicationView>(czAppRecord.ApplicationId).Value;
                        }

                        data.CropZones.Add(czItem);
                    }
                }

                data.CropZones.ForEach(x => x.IsAllowed = x.PhiExpiration > DateTime.Now ? false : true);
                data.CropZones.ForEach(s => s.Status = s.Status = s.IsAllowed == true ? "Allowed" : "No Harvest");
                data.CropZones = data.CropZones.OrderBy(l => l.Farm).ThenBy(f => f.Field).ThenBy(c => c.CropZone).ToList();

                var report = new Landdb.Views.Secondary.Reports.AppliedProduct.PreHarvestSummaryReport();
                report.DataSource = data.CropZones;
                report.Name = Strings.ReportName_Phi_Text;

                if (data.CropZones.Count() > 0)
                {
                    book.Reports.Add(report);
                }
            }

            rs.ReportDocument = book;
            return rs;
        }

        public string DisplayName
        {
            get { return Strings.ReportName_Phi_Text; }
        }
    }
}
