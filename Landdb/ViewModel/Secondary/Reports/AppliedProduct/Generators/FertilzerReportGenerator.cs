﻿using AgC.UnitConversion;
using AgC.UnitConversion.Area;
using Landdb.Client.Infrastructure;
using Landdb.Client.Services.Fertilizer;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.ReportModels.AppliedProductByFieldDetail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Landdb.Resources;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.AppliedProduct.Generators
{
    public class FertilzerReportGenerator: IAppliedProductReportGenerator {
        IClientEndpoint clientEndpoint;
        int cropYear;

        public FertilzerReportGenerator(IClientEndpoint clientEndpoint, int cropYear)
        {
            this.clientEndpoint = clientEndpoint;
            this.cropYear = cropYear;
        }

        public ReportSource Generate(AppliedQueryCriteria criteria) {
            ReportBook book = new ReportBook();
            var rs = new InstanceReportSource();

            var applicationData = clientEndpoint.GetView<CropZoneApplicationDataView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new CropZoneApplicationDataView());
            var inventory = clientEndpoint.GetView<InventoryListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new InventoryListView());
            var flattenedHierarchy = clientEndpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new FlattenedTreeHierarchyView()); 
            var masterlist = clientEndpoint.GetMasterlistService();
            var fertService = new FertilizerAccumulator(clientEndpoint, ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear);
            if (criteria.SelectedCropZones.Any()) {
                foreach (var czId in criteria.SelectedCropZones) {
                    var data = new ReportData();

	                data.CropYear = cropYear.ToString();
                    data.DataSourceName = !string.IsNullOrWhiteSpace(criteria.SelectedEntity.Name) ? criteria.SelectedEntity.Name : ApplicationEnvironment.CurrentDataSourceName;
                    data.DateRangeDisplay = string.Format("{0} - {1}", criteria.QueryStartDate.ToString("d"), criteria.QueryEndDate.ToString("d"));

                    var czTreeData = (from f in flattenedHierarchy.Items
                                      where f.CropZoneId == czId
                                      select f).FirstOrDefault();
                    if (czTreeData != null) {
                        data.FieldDisplay = string.Format(@"{0} \ {1} \ {2}", czTreeData.FarmName, czTreeData.FieldName, czTreeData.CropZoneName);
                        data.CropDisplay = masterlist.GetCropDisplay(czTreeData.CropId);
                    }

                    var czDetails = clientEndpoint.GetView<CropZoneDetailsView>(czId).GetValue(new CropZoneDetailsView());
                    Measure area;
                    if (czDetails.ReportedArea != null && czDetails.ReportedArea > 0) {
                        area = UnitFactory.GetUnitByName(czDetails.ReportedAreaUnit).GetMeasure(czDetails.ReportedArea.Value);
                        data.AreaDisplay = area.FullDisplay;
                        data.Area = czDetails.ReportedArea.Value;
                    } else if (czDetails.BoundaryArea != null) {
                        area = UnitFactory.GetUnitByName(czDetails.BoundaryAreaUnit).GetMeasure(czDetails.BoundaryArea.Value);
                        data.AreaDisplay = area.FullDisplay;
                        data.Area = czDetails.BoundaryArea.Value;
                    } else {
                        area = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(0);
                        data.AreaDisplay = area.FullDisplay;
                        data.Area = 0;
                    }

                    var czArea = data.Area; 
                    var czAppData = from x in applicationData.Items
                                    where x.CropZoneId == czId &&
                                    x.EndDate >= criteria.QueryStartDate &&
                                    x.EndDate <= criteria.QueryEndDate  && criteria.SelectedProducts.Contains(x.ProductId.Id) == true
                                    select x;

                    double? _total = 0.0;

                    FertilizerFormulationModel totalFertModel = new FertilizerFormulationModel();
                    Task totalFertTask = Task.Factory.StartNew(async () =>
                        {
                            totalFertModel = await fertService.CalculateFertilizerUsage(czAppData.ToList(), area);
                        });

                    foreach (var czAppRecord in czAppData) {
                        var mlp = masterlist.GetProduct(czAppRecord.ProductId);
                        ReportLineItemData rlid = new ReportLineItemData();

                        var displayRate = DisplayMeasureConverter.ConvertToRateDisplayMeasure(czAppRecord.ProductId, czAppRecord.RateValue, czAppRecord.RateUnit, mlp.Density, clientEndpoint, ApplicationEnvironment.CurrentDataSourceId);
                        var displayTotal = DisplayMeasureConverter.ConvertToTotalDisplayMeasure(czAppRecord.ProductId, (decimal)czAppRecord.TotalProductValue, czAppRecord.TotalProductUnit, mlp.Density, clientEndpoint, ApplicationEnvironment.CurrentDataSourceId);


                        rlid.StartDate = czAppRecord.StartDate.ToLocalTime().ToString("M/d");
                        rlid.StartDateTime = czAppRecord.StartDate.ToLocalTime();
                        rlid.RateDisplay = displayRate.Value.ToString("n2") + " " + displayRate.Unit.AbbreviatedDisplay; // TODO: Make this a measure.
                        var areaMeasure = UnitFactory.GetUnitByName(czAppRecord.AreaUnit).GetMeasure(czAppRecord.AreaValue);
                        rlid.AreaDisplay = areaMeasure.FullDisplay;
                        rlid.ProductDisplay = mlp.Name;
                        rlid.TotalProductDisplay = displayTotal.Value.ToString("n2") + " " + displayTotal.Unit.AbbreviatedDisplay; // TODO: Make this a measure.
                        rlid.TotalProduct = displayTotal.Value;
                        rlid.CropZoneArea = czArea;
                        string fertString = string.Empty;
                        var coverageAreaMeasure = areaMeasure.Unit.GetMeasure(areaMeasure.Value * czAppRecord.CoveragePercent, null);
                        Task fert = Task.Factory.StartNew(async () => { var fertObj = await fertService.CalculateFertilizerAnalysis(czAppRecord.ProductId, coverageAreaMeasure, (double)czAppRecord.RateValue, czAppRecord.RateUnit); fertString = fertObj.ToString(); });
                        fert.Wait();
                        rlid.FertilizerAnalysis = fertString;
                        if(inventory.Products.ContainsKey(czAppRecord.ProductId)) {
                            rlid.TotalCost = Math.Round(inventory.Products[czAppRecord.ProductId].AveragePriceValue * czAppRecord.TotalGrowerProductValue, 2); // TODO: This will only pan out if the units of these measures is the same. Need to take this into account.
                            _total += rlid.TotalCost;
                        }


                        data.LineItems.Add(rlid);
                        //if (mlp.ProductType == GlobalStrings.ProductType_CropProtection) { data.CropProtectionLineItems.Add(rlid); }
                        if (mlp.ProductType == GlobalStrings.ProductType_Fertilizer) { data.FertilizerLineItems.Add(rlid); }
                        if (mlp.ProductType == GlobalStrings.ProductType_Seed) { data.SeedLineItems.Add(rlid); }
                        //if (mlp.ProductType == GlobalStrings.ProductType_Service) { data.ServiceLineItems.Add(rlid); }
                    }

                    totalFertTask.Wait();
                    data.FertilizerLineItems = data.FertilizerLineItems.OrderBy(f => f.StartDateTime.ToLocalTime()).ToList();
                    data.SeedLineItems = data.SeedLineItems.OrderBy(s => s.StartDateTime.ToLocalTime()).ToList();
                    data.TotalFertilizer = totalFertModel;
                    data.TotalCost = (double)_total;
                    var report = new Landdb.Views.Secondary.Reports.AppliedProduct.FertilizerDetailReport();
                    report.DataSource = data;
                    report.Name = data.FieldDisplay;

                    if (data.LineItems.Count() > 0)
                    {
                        book.Reports.Add(report);
                    }
                }
            }

            rs.ReportDocument = book;
            return rs;
        }

        public string DisplayName {
            get { return Strings.GeneratorName_FertilizerReport_Text; }
        }
    }
}