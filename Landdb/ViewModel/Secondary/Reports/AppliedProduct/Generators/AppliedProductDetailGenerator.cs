﻿using AgC.UnitConversion;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.ReportModels.AppliedProduct;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Landdb.Resources;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.AppliedProduct.Generators
{
    public class AppliedProductDetailGenerator : IAppliedProductReportGenerator
    {
        IClientEndpoint endpoint;
        int cropYear;
        public AppliedProductDetailGenerator(IClientEndpoint endpoint, int cropYear)
        {
            this.endpoint = endpoint;
            this.cropYear = cropYear;
            DetailsList = new List<AppliedProductDetail>();
        }

        public List<AppliedProductDetail> DetailsList { get; set; }

        public Telerik.Reporting.ReportSource Generate(AppliedQueryCriteria criteria)
        {
            DetailsList = new List<AppliedProductDetail>();
            ReportBook book = new ReportBook();
            var rs = new InstanceReportSource();

            var applicationData = endpoint.GetView<CropZoneApplicationDataView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new CropZoneApplicationDataView());
            //var inventory = endpoint.GetView<InventoryListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new InventoryListView());
            var flattenedHierarchy = endpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new FlattenedTreeHierarchyView());
            var masterlist = endpoint.GetMasterlistService();

            if (criteria.SelectedCropZones.Any())
            {
                foreach (var czId in criteria.SelectedCropZones)
                {
                    var czDetails = endpoint.GetView<CropZoneDetailsView>(czId).GetValue(new CropZoneDetailsView());
                    var czAppData = from x in applicationData.Items
                                    where x.CropZoneId == czId &&
                                    x.EndDate >= criteria.QueryStartDate &&
                                    x.EndDate <= criteria.QueryEndDate && criteria.SelectedProducts.Contains(x.ProductId.Id) == true
                                    select x;

                    double? _total = 0.0;

                    foreach (var czAppRecord in czAppData)
                    {
                        var data = new AppliedProductDetail();
                        data.DatasourceName = !string.IsNullOrWhiteSpace(criteria.SelectedEntity?.Name) ? criteria.SelectedEntity.Name : ApplicationEnvironment.CurrentDataSourceName;
                        data.DateRangeDisplay = string.Format("{0} - {1}", criteria.QueryStartDate.ToString("d"), criteria.QueryEndDate.ToString("d"));
                        data.FarmName = flattenedHierarchy.Items.FirstOrDefault(c => c.CropZoneId == czAppRecord.CropZoneId).FarmName;
                        var czTreeData = (from f in flattenedHierarchy.Items
                                          where f.CropZoneId == czId
                                          select f).FirstOrDefault();
                        if (czTreeData != null)
                        {
                            data.FieldCropZone = string.Format(@"{0} \ {1}", czTreeData.FieldName, czTreeData.CropZoneName);
                        }

                        data.CropYear = cropYear.ToString();

                        var mlp = masterlist.GetProduct(czAppRecord.ProductId);

                        data.ProductId = czAppRecord.ProductId;
                        data.ProductName = mlp.Name;
                        data.Manufacturer = mlp.Manufacturer;
                        data.ApplicationDate = czAppRecord.StartDate.ToLocalTime();
                        //TO Do :: alter rate and total to user defined units....
                        var displayRate = DisplayMeasureConverter.ConvertToRateDisplayMeasure(czAppRecord.ProductId, czAppRecord.RateValue, czAppRecord.RateUnit, mlp.Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);
                        var displayTotal = DisplayMeasureConverter.ConvertToTotalDisplayMeasure(czAppRecord.ProductId, (decimal)czAppRecord.TotalProductValue, czAppRecord.TotalProductUnit, mlp.Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);

                        data.RateValue = displayRate.Value;
                        data.RateUnit = displayRate.Unit;

                        data.TotalProductValue = displayTotal.Value;
                        data.TotalUnit = displayTotal.Unit;

                        ApplicationView appDetails = endpoint.GetView<ApplicationView>(czAppRecord.ApplicationId).Value;

                        data.AreaValue = appDetails.CropZones.SingleOrDefault(c => c.Id == czAppRecord.CropZoneId).Area.Value * czAppRecord.CoveragePercent;
                        data.AreaUnit = appDetails.CropZones.SingleOrDefault(c => c.Id == czAppRecord.CropZoneId).Area.Unit.AbbreviatedDisplay;
                        DetailsList.Add(data);
                    }
                }

            }

            //CONVERT UNITS TO THE SAME UNIT.
            var distinctProds = DetailsList.Distinct(x => x.ProductId).ToList();

            foreach (var prod in distinctProds)
            {
                var mlp = endpoint.GetMasterlistService().GetProduct(prod.ProductId);
                var allProds = DetailsList.Where(x => x.ProductId == prod.ProductId);
                var units = allProds.Distinct(y => y.TotalUnit);
                var unitDictionary = new Dictionary<IUnit, int>();
                foreach (var u in units)
                {
                    var totalForUnit = allProds.Where(x => x.TotalUnit == u.TotalUnit).Count();
                    unitDictionary.Add(u.TotalUnit, totalForUnit);
                }

                var theUnit = unitDictionary.OrderByDescending(y => y.Value).First();

                foreach (var p in allProds)
                {
                    if (p.TotalUnit == theUnit.Key)
                    {

                    }
                    else
                    {
                        var prodMeasure = p.TotalUnit.GetMeasure(p.TotalProductValue, mlp.Density);
                        var converted = prodMeasure.Unit != theUnit.Key && prodMeasure.CanConvertTo(theUnit.Key) ? prodMeasure.GetValueAs(theUnit.Key) : prodMeasure.Value;

                        p.TotalProductValue = converted;
                        p.TotalUnit = theUnit.Key;
                    }
                }
            }

            //This past section is suppose to convert all the units to the same.

            var report = new Landdb.Views.Secondary.Reports.AppliedProduct.AppliedProductDetailsReport();
            DetailsList = DetailsList.OrderBy(x => x.ProductName).ThenBy(j => j.ApplicationDate).ToList();
            report.DataSource = DetailsList;
            report.Name = Strings.ReportName_AppliedProductDetails_Text;

            if (DetailsList.Count() > 0)
            {
                book.Reports.Add(report);
            }
            rs.ReportDocument = book;
            return rs;
        }

        public string DisplayName
        {
            get { return Strings.ReportName_AppliedProductDetails_Text; }
        }
    }
}