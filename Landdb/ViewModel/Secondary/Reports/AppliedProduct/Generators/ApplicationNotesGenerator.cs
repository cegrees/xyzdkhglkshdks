﻿using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.ReportModels.AppliedProduct;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Landdb.Resources;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.AppliedProduct.Generators
{
    public class ApplicationNotesGenerator : IAppliedProductReportGenerator
    {
        IClientEndpoint clientEndpoint;
        int cropYear;

        public ApplicationNotesGenerator(IClientEndpoint clientEndpoint, int cropYear)
        {
            this.clientEndpoint = clientEndpoint;
            this.cropYear = cropYear;
        }

        public Telerik.Reporting.ReportSource Generate(AppliedQueryCriteria criteria)
        {
            ReportBook book = new ReportBook();
            var rs = new InstanceReportSource();
            List<ApplicationNotesData> Notes = new List<ApplicationNotesData>();
            var applicationData = clientEndpoint.GetView<CropZoneApplicationDataView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new CropZoneApplicationDataView());
            //var inventory = clientEndpoint.GetView<InventoryListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new InventoryListView());
            var flattenedHierarchy = clientEndpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new FlattenedTreeHierarchyView());
            //var masterlist = clientEndpoint.GetMasterlistService();

            if (criteria.SelectedCropZones.Any())
            {
                var czapplicationList = (from a in applicationData.Items
                                         where criteria.SelectedCropZones.Contains(a.CropZoneId) &&
                                         a.EndDate >= criteria.QueryStartDate && a.EndDate <= criteria.QueryEndDate &&
                                         criteria.SelectedProducts.Contains(a.ProductId.Id)
                                         select a).OrderBy(x => x.StartDate);

                var dsName = ApplicationEnvironment.CurrentDataSourceName;
                foreach (var czId in criteria.SelectedCropZones)
                {
                    if (czapplicationList.Any(app => app.CropZoneId == czId))
                    {
                        var flattenedItem = flattenedHierarchy.Items.FirstOrDefault(cz => cz.CropZoneId == czId);
                        var czDetails = clientEndpoint.GetView<CropZoneDetailsView>(czId).Value;
                        var czAppItems = czapplicationList.Where(cz => cz.CropZoneId == czId).Distinct(id => id.ApplicationId).ToList().OrderBy(app => app.StartDate);

                        foreach (var item in czAppItems)
                        {
                            var dataItem = new ReportModels.AppliedProduct.ApplicationNotesData();
                            var appDetails = clientEndpoint.GetView<ApplicationView>(item.ApplicationId);
                            if (!string.IsNullOrEmpty(appDetails.Value.Notes))
                            {
                                dataItem.Area = czDetails.ReportedArea.HasValue && czDetails.ReportedArea.Value > 0 ? czDetails.ReportedArea.Value : 0;
                                dataItem.Area = dataItem.Area == 0 && czDetails.BoundaryArea.HasValue ? czDetails.BoundaryArea.Value : dataItem.Area;
                                dataItem.FarmName = flattenedItem.FarmName;
                                dataItem.FieldName = flattenedItem.FieldName;
                                dataItem.CropZoneName = flattenedItem.CropZoneName;
                                dataItem.DataSourceName = dsName;
	                            dataItem.CropYear = cropYear.ToString();
								dataItem.DateRangeDisplay = string.Format("{0} - {1}", criteria.QueryStartDate.ToShortDateString(), criteria.QueryEndDate.ToShortDateString());
                                dataItem.Notes = appDetails.Value.Notes;
                                dataItem.StartDate = appDetails.Value.StartDateTime.ToLocalTime();
                                Notes.Add(dataItem);
                            }
                        }
                    }
                }

                if (Notes.Count() > 0)
                {
                    var report = new Landdb.Views.Secondary.Reports.AppliedProduct.NotesReport();
                    report.DataSource = Notes;
                    report.Name = Strings.ReportName_ApplicationNotesReport_Text;

                    book.Reports.Add(report);
                }

            }

            rs.ReportDocument = book;
            return rs;
        }

        public string DisplayName
        {
            get { return Strings.ReportName_ApplicationNotesReport_Text; }
        }
    }
}
