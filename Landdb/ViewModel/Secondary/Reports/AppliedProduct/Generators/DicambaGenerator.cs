﻿using Landdb.Client.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Reporting;
using ClosedXML.Excel;
using Landdb.Resources;
using Microsoft.Win32;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;

namespace Landdb.ViewModel.Secondary.Reports.AppliedProduct.Generators
{
    public class DicambaGenerator : IAppliedProductReportGenerator
    {
        IClientEndpoint endpoint;
        int cropYear;

        static string columnHeaders = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        string filename;

        public DicambaGenerator(IClientEndpoint endPoint, int cropyear)
        {
            endpoint = endPoint;
            cropYear = cropyear;
        }

        public string DisplayName
        {
            get
            {
                return "Dicamba Application Report";
            }
        }


        public ReportSource Generate(AppliedQueryCriteria crit)
        {
            var applicationData = endpoint.GetView<CropZoneApplicationDataView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new CropZoneApplicationDataView());
            var flattenedHierarchy = endpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new FlattenedTreeHierarchyView());
            var masterlist = endpoint.GetMasterlistService();
            var actives = masterlist.GetActiveIngredientList();
            var dicambaAI = actives.Where(x => x.Name.ToLower().Contains("dichloromethoxybenzoic") || x.Name.ToLower().Contains("dicamba")).Distinct(i => i.Id);
            //var byId = actives.Where(x => x.Id.ToString() == "646249d2-1c6e-46ab-b28c-b48e770c45f1" || x.Id.ToString() == "ee262e43-46a4-4406-ab37-a6d57a6d2fc2");

            if (crit.SelectedCropZones.Any())
            {
                var czapplicationList = (from a in applicationData.Items
                                         where crit.SelectedCropZones.Contains(a.CropZoneId) &&
                                         a.EndDate >= crit.QueryStartDate && a.EndDate <= crit.QueryEndDate &&
                                         crit.SelectedProducts.Contains(a.ProductId.Id)
                                         select a).OrderBy(x => x.StartDate);

                var appGroup = czapplicationList.GroupBy(x => x.ApplicationId);

                foreach (var app in czapplicationList)
                {
                    var mlp = masterlist.GetProduct(app.ProductId);
                    var hasDicamba = (from a in dicambaAI
                                      from p in mlp.ActiveIngredients
                                      where p.Id == a.Id
                                      select mlp).Count() > 0;

                    if (hasDicamba)
                    {
                        //to do :: get all products associated with this application
                        var associatedAppProducts = czapplicationList.Where(x => x.ApplicationId == app.ApplicationId);

                    }
                }
            }
                    var wb = new XLWorkbook();
            var ws = wb.AddWorksheet("Dicamba Form");
            ws.Protection.SetSelectLockedCells(true);
            ws.Style.Fill.PatternType = XLFillPatternValues.None;
            ws.ShowGridLines = false;
            //PLAN DATA TO A WORKSHEET/////////////////////////////////////////////////


            var dicambaHeader = ws.Range("A1:N2");
            dicambaHeader.Merge();
            dicambaHeader.Value = "Dicamba Application Form";
            dicambaHeader.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            dicambaHeader.Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
            dicambaHeader.Style.Font.SetFontSize(18.0);
            dicambaHeader.Style.Font.SetBold(true);
            dicambaHeader.Style.Fill.BackgroundColor = XLColor.Green;
            dicambaHeader.Style.Protection.SetLocked(true);

            var certLBL = ws.Range("A4:C4");
            certLBL.Value = "Certified Applicator Name:";
            certLBL.Merge();
            certLBL.Style.Protection.Locked = true; 

            var certName = ws.Range("D4:F4");
            certName.Merge();
            certName.Style.Fill.BackgroundColor = XLColor.Gainsboro;
            //TO DO :: SET THIS VALUE FROM DATA

            var stateCertLBL = ws.Range("G4:I4");
            stateCertLBL.Merge();
            stateCertLBL.Value = "State Certification # of Applicator:";
            stateCertLBL.Style.Protection.Locked = true;

            var stateCert = ws.Range("J4:N4");
            stateCert.Merge();
            stateCert.Style.Fill.BackgroundColor = XLColor.Gainsboro;
            //TO DO :: SET THIS VALUE FROM DATA

            var appNameLBL = ws.Range("A5:E5");
            appNameLBL.Merge();
            appNameLBL.Value = "Applicator Name (if different from Certified Applicator):";
            appNameLBL.Style.Protection.Locked = true;

            var appName = ws.Range("F5:N5");
            appName.Merge();
            appName.Style.Fill.BackgroundColor = XLColor.Gainsboro;
            appName.Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);
            //TO DO :: SET THIS VALUE FROM DATA

            var sectionBreak = ws.Range("A7:N7");
            sectionBreak.Style.Border.TopBorderColor = XLColor.Green;
            sectionBreak.Style.Border.SetTopBorder(XLBorderStyleValues.Thick);

            var trainingBlock = ws.Range("A8:B9");
            trainingBlock.Merge();
            //trainingBlock.Style.Font.SetFontSize(18.0);
            trainingBlock.Style.Font.SetFontColor(XLColor.White);
            trainingBlock.Style.Font.SetBold(true);
            trainingBlock.Style.Fill.BackgroundColor = XLColor.Green;
            trainingBlock.Style.Protection.SetLocked(true);
            trainingBlock.Value = "REQUIRED DICAMBA APPLICATOR TRAINING";
            trainingBlock.Style.Alignment.SetWrapText(true);

            var trainedApplicatorLBL = ws.Range("C8:g8");
            trainedApplicatorLBL.Merge();
            trainedApplicatorLBL.Value = "Applicator Name (if different from Certified Applicator):";
            trainedApplicatorLBL.Style.Protection.SetLocked(true);
            
            var trainedApplicator = ws.Range("H8:N8");
            trainedApplicator.Merge();
            trainedApplicator.Style.Fill.BackgroundColor = XLColor.Gainsboro;
            trainedApplicator.Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);

            var trainedDateLBL = ws.Range("C9:E9");
            trainedDateLBL.Merge();
            trainedDateLBL.Value = "Date Completed (MM/DD/YY):";
            trainedDateLBL.Style.Protection.SetLocked(true);

            var trainedDate = ws.Range("F9:H9");
            trainedDate.Merge();
            trainedDate.Style.Fill.BackgroundColor = XLColor.Gainsboro;
            trainedDate.Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);

            var providerLBL = ws.Range("I9:K9");
            providerLBL.Merge();
            providerLBL.Value = "Provider:";
            providerLBL.Style.Protection.SetLocked(true);

            var provider = ws.Range("L9:N9");
            provider.Merge();
            provider.Style.Fill.BackgroundColor = XLColor.Gainsboro;
            provider.Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);

            var sectionBreak2 = ws.Range("A11:N11");
            sectionBreak2.Style.Border.SetTopBorderColor(XLColor.Green);
            sectionBreak2.Style.Border.SetTopBorder(XLBorderStyleValues.Thick);

            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            var susceptibleCropBlock = ws.Range("A13:B15");
            susceptibleCropBlock.Merge();
            //trainingBlock.Style.Font.SetFontSize(18.0);
            susceptibleCropBlock.Style.Font.SetFontColor(XLColor.White);
            susceptibleCropBlock.Style.Font.SetBold(true);
            susceptibleCropBlock.Style.Fill.BackgroundColor = XLColor.Green;
            susceptibleCropBlock.Style.Protection.SetLocked(true);
            susceptibleCropBlock.Value = "SUSCEPTIBLE CROP";
            susceptibleCropBlock.Style.Alignment.SetWrapText(true);

            var susceptibleCropLBL = ws.Range("c13:g13");
            susceptibleCropLBL.Merge();
            susceptibleCropLBL.Value = "Name and Date of the Sensitive Crop Registry Consulted:";
            susceptibleCropLBL.Style.Protection.SetLocked(true);

            var susceptibleCropDate = ws.Range("h13:j13");
            susceptibleCropDate.Merge();
            susceptibleCropDate.Style.Fill.BackgroundColor = XLColor.Gainsboro;
            susceptibleCropDate.Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);
            susceptibleCropDate.Value = "    /    /    ";

            var susceptibleRegistry = ws.Range("k13:n13");
            susceptibleRegistry.Merge();
            susceptibleRegistry.Style.Fill.BackgroundColor = XLColor.Gainsboro;
            susceptibleRegistry.Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);

            var neighboringCropLBL = ws.Range("c14:g14");


            var neighboringCropDate = ws.Range("h14:j14");
            neighboringCropDate.Merge();
            neighboringCropDate.Style.Fill.BackgroundColor = XLColor.Gainsboro;
            neighboringCropDate.Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);
            neighboringCropDate.Value = "    /    /    ";


            var sectionBreak3 = ws.Range("A17:N17");
            sectionBreak3.Style.Border.SetTopBorderColor(XLColor.Green);
            sectionBreak3.Style.Border.SetTopBorder(XLBorderStyleValues.Thick);

            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            var preAppBlock = ws.Range("A19:B25");
            preAppBlock.Merge();
            //trainingBlock.Style.Font.SetFontSize(18.0);
            preAppBlock.Style.Font.SetFontColor(XLColor.White);
            preAppBlock.Style.Font.SetBold(true);
            preAppBlock.Style.Fill.BackgroundColor = XLColor.Green;
            preAppBlock.Style.Protection.SetLocked(true);
            preAppBlock.Value = "PRE - APPLICATION INFORMATION";
            preAppBlock.Style.Alignment.SetWrapText(true);

            var preAppWarningLBL = ws.Range("C19:N19");
            preAppWarningLBL.Merge();
            preAppWarningLBL.Value = "Retain receipt of each purchase for each application. Retain Copy of all product labels, including state labels where applicable.";
            preAppWarningLBL.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            preAppWarningLBL.Style.Protection.SetLocked(true);

            var prodNameLBL = ws.Range("C20:e20");
            prodNameLBL.Merge();
            prodNameLBL.Value = "Approved Dicamba Product Name & Reg. #:";
            prodNameLBL.Style.Protection.SetLocked(true);

            var prodName = ws.Range("f20:h20");
            prodName.Merge();
            prodName.Value = "Approved Dicamba Product Name & Reg. #:";
            prodName.Style.Protection.SetLocked(true);

            try
            {
                //open up file dialog to save file....
                //then call createexcelfile to create the excel...
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();

                saveFileDialog1.Filter = "Excel|*.xlsx";
                saveFileDialog1.FilterIndex = 2;
                saveFileDialog1.RestoreDirectory = true;

                Nullable<bool> resultSaved = saveFileDialog1.ShowDialog();

                // Process save file dialog box results 
                if (resultSaved == true)
                {
                    // Save document 
                    filename = saveFileDialog1.FileName;
                    wb.SaveAs(filename);
                    //CreateExcelFile.CreateExcelDocument(ds, filename);
                }

                //now open file....
                System.Diagnostics.Process.Start(filename);
            }
            catch (Exception ex)
            {

            }

            return null;
        }
    }
}
