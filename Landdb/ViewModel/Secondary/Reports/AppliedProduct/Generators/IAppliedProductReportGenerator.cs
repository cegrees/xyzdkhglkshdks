﻿using System;
using System.Windows.Documents;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.AppliedProduct.Generators {
    public interface IAppliedProductReportGenerator {
        ReportSource Generate(AppliedQueryCriteria criteria);
        string DisplayName { get; }
    }
}
