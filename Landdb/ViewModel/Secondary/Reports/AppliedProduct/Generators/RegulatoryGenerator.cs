﻿using AgC.UnitConversion;
using AgC.UnitConversion.Area;
using AgC.UnitConversion.Volume;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Company;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Domain.ReadModels.Person;
using Landdb.Domain.ReadModels.ProductSetting;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.ReportModels.AppliedProductByFieldDetail;
using Landdb.ReportServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Landdb.Resources;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.AppliedProduct.Generators
{
    public class RegulatoryGenerator : IAppliedProductReportGenerator
    {
        IClientEndpoint clientEndpoint;
        int cropYear;

        public RegulatoryGenerator(IClientEndpoint endpoint, int cropYear)
        {
            this.clientEndpoint = endpoint;
            this.cropYear = cropYear;
        }

        public ReportSource Generate(AppliedQueryCriteria criteria)
        {
            ReportBook book = new ReportBook();
            var rs = new InstanceReportSource();

            var applicationData = clientEndpoint.GetView<CropZoneApplicationDataView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new CropZoneApplicationDataView());
            var inventory = clientEndpoint.GetView<InventoryListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new InventoryListView());
            var flattenedHierarchy = clientEndpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new FlattenedTreeHierarchyView());
            var masterlist = clientEndpoint.GetMasterlistService();

            if (criteria.SelectedCropZones.Any())
            {
                foreach (var czId in criteria.SelectedCropZones)
                {

                    var czAppData = from x in applicationData.Items
                                    where x.CropZoneId == czId &&
                                    x.EndDate >= criteria.QueryStartDate &&
                                    x.EndDate <= criteria.QueryEndDate && criteria.SelectedProducts.Contains(x.ProductId.Id) == true
                                    select x;

                    if (czAppData.Count() > 0)
                    {

                        var data = new ReportData();

                        string address1 = string.Empty;
                        string city = string.Empty;
                        string state = string.Empty;
                        string zip = string.Empty;

                        if (criteria.SelectedEntity != null && criteria.SelectedEntity.Type == "Person")
                        {
                            var personMaybe = clientEndpoint.GetView<PersonDetailsView>(new PersonId(ApplicationEnvironment.CurrentDataSourceId, criteria.SelectedEntity.Id));
                            var person = personMaybe.HasValue ? personMaybe.Value : null;
                            if (person.StreetAddress != null)
                            {
                                address1 = person != null ? person.StreetAddress.AddressLine1 : string.Empty;
                                city = person != null ? person.StreetAddress.City : string.Empty;
                                state = person != null ? person.StreetAddress.State : string.Empty;
                                zip = person != null ? person.StreetAddress.ZipCode : string.Empty;
                            }
                        }
                        else if (criteria.SelectedEntity != null && criteria.SelectedEntity.Type == "Company")
                        {
                            var companyMaybe = clientEndpoint.GetView<CompanyDetailsView>(new CompanyId(ApplicationEnvironment.CurrentDataSourceId, criteria.SelectedEntity.Id));
                            var company = companyMaybe.HasValue ? companyMaybe.Value : null;
                            if (company.StreetAddress != null)
                            {
                                address1 = company != null ? company.StreetAddress.AddressLine1 : string.Empty;
                                city = company != null ? company.StreetAddress.City : string.Empty;
                                state = company != null ? company.StreetAddress.State : string.Empty;
                                zip = company != null ? company.StreetAddress.ZipCode : string.Empty;
                            }
                        }
                        else
                        {
                            
                        }

	                    data.CropYear = ApplicationEnvironment.CurrentCropYear.ToString();
						data.DataSourceName = !string.IsNullOrWhiteSpace(criteria.SelectedEntity.Name) ? criteria.SelectedEntity.Name : ApplicationEnvironment.CurrentDataSourceName;
                        data.AddressDisplay = !string.IsNullOrEmpty(address1) ? string.Format("{0}\n{1}, {2}  {3}", address1, city, state, zip) : string.Empty;

                        data.DateRangeDisplay = string.Format("{0} - {1}", criteria.QueryStartDate.ToString("d"), criteria.QueryEndDate.ToString("d"));

                        var czTreeData = (from f in flattenedHierarchy.Items
                                          where f.CropZoneId == czId
                                          select f).FirstOrDefault();
                        if (czTreeData != null)
                        {
                            data.FieldDisplay = string.Format(@"{0} \ {1} \ {2}", czTreeData.FarmName, czTreeData.FieldName, czTreeData.CropZoneName);
                            data.CropDisplay = masterlist.GetCropDisplay(czTreeData.CropId);
                        }

                        var czDetails = clientEndpoint.GetView<CropZoneDetailsView>(czId).GetValue(new CropZoneDetailsView());
                        if (czDetails.ReportedArea != null && czDetails.ReportedArea > 0)
                        {
                            data.AreaDisplay = UnitFactory.GetUnitByName(czDetails.ReportedAreaUnit).GetMeasure(czDetails.ReportedArea.Value).FullDisplay;
                            data.Area = czDetails.ReportedArea.Value;
                        }
                        else if (czDetails.BoundaryArea != null)
                        {
                            data.AreaDisplay = UnitFactory.GetUnitByName(czDetails.BoundaryAreaUnit).GetMeasure(czDetails.BoundaryArea.Value).FullDisplay;
                            data.Area = czDetails.BoundaryArea.Value;
                        }
                        else
                        {
                            data.AreaDisplay = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(0).FullDisplay;
                            data.Area = 0;
                        }

                        foreach (var czAppRecord in czAppData)
                        {
                            var mlp = masterlist.GetProduct(czAppRecord.ProductId);
                            var ReiPhis = masterlist.GetProductReiPhis(czAppRecord.ProductId);
                            ReportLineItemData rlid = new ReportLineItemData();

                            var displayRate = DisplayMeasureConverter.ConvertToRateDisplayMeasure(czAppRecord.ProductId, czAppRecord.RateValue, czAppRecord.RateUnit, mlp.Density, clientEndpoint, ApplicationEnvironment.CurrentDataSourceId);
                            var displayTotal = DisplayMeasureConverter.ConvertToTotalDisplayMeasure(czAppRecord.ProductId, (decimal)czAppRecord.TotalProductValue, czAppRecord.TotalProductUnit, mlp.Density, clientEndpoint, ApplicationEnvironment.CurrentDataSourceId);

                            var applicationDetail = clientEndpoint.GetView<ApplicationView>(czAppRecord.ApplicationId).Value;
                            var applicators = applicationDetail.Applicators;
                            string applicatorDisplay = string.Join(" / ", applicators.Select(s => s.ToFullString()));

                            //List<string> applicatorStrings = new List<string>();
                            //foreach (var applicator in applicators)
                            //{
                            //    var personDetails = clientEndpoint.GetView<PersonDetailsView>(applicator.PersonId);
                            //    var applicatorString = string.Format("{0} :: {1} :: {2}", applicator.CompanyName, personDetails.Value.Name, personDetails.Value.ApplicatorLicenseNumber);
                            //    applicatorStrings.Add(applicatorString);
                            //}
                            //string applicatorDisplay = string.Join(" / ", applicatorStrings.ToArray());
                            rlid.ApplicatorDisplay = applicatorDisplay;
                            rlid.StartDateTime = czAppRecord.StartDate.ToLocalTime();
                            rlid.StartDate = czAppRecord.StartDate.ToLocalTime().ToString("MM/dd/yy hh:mm tt");
                            rlid.EndDate = applicationDetail.CustomEndDate.HasValue ? ((DateTime)applicationDetail.CustomEndDate).ToLocalTime().ToString("MM/dd/yy hh:mm tt") : czAppRecord.EndDate.ToLocalTime().ToString("MM/dd/yy hh:mm tt");
                            rlid.RateDisplay = czAppRecord.RateValue.ToString("n2") + " " + UnitFactory.GetUnitByName(czAppRecord.RateUnit).AbbreviatedDisplay; // TODO: Make this a measure.
                            rlid.Rate = displayRate.Value;
                            rlid.RateUnit = displayRate.Unit.AbbreviatedDisplay;
                            rlid.AreaDisplay = UnitFactory.GetUnitByName(czAppRecord.AreaUnit).GetMeasure(czAppRecord.AreaValue * czAppRecord.CoveragePercent).AbbreviatedDisplay;
                            rlid.ProductDisplay = mlp.Name;
                            rlid.TotalProductDisplay = displayTotal.Value.ToString("n2") + " " + displayTotal.Unit.AbbreviatedDisplay; // TODO: Make this a measure.
                            rlid.TotalProduct = displayTotal.Value;
                            var prodDetail = applicationDetail.Products.Any(x => x.TrackingId == czAppRecord.ProductTrackingId) ? applicationDetail.Products.FirstOrDefault(x => x.TrackingId == czAppRecord.ProductTrackingId) : null;
                            rlid.TotalUnit = displayTotal.Unit.AbbreviatedDisplay;
                            rlid.AppMethod = prodDetail != null ? prodDetail.ApplicationMethod : string.Empty;
                            rlid.Pest = prodDetail != null && prodDetail.TargetPest != null ? prodDetail.TargetPest.ToString() : string.Empty;
                            ///////////////////////////////////////////////

                            var registrationNumber = mlp.RegistrationNumber;

                            if (registrationNumber == null) { rlid.EPANumber = string.Empty; }
                            else
                            {
                                Regex epaNumberRegex = new Regex("([1-9][0-9]*)");

                                var matches = from m in epaNumberRegex.Matches(registrationNumber).Cast<Match>()
                                              select m.Value;
                                rlid.EPANumber = string.Join("-", matches.ToArray());
                            }
                            /////////////////////////////////////////////////

                            rlid.RUP = mlp.RestrictedUse == true ? "YES" : "NO";

                            var productService = new ProductServices(clientEndpoint);
                            ReiPhi reiPhi = productService.ReturnReiPhi(czAppRecord.ProductId, czDetails.CropId);

                            rlid.REI = reiPhi != null && reiPhi.Rei != null ? reiPhi.Rei.ToString() : "*";
                            rlid.PHI = reiPhi != null && reiPhi.Phi != null ? reiPhi.Phi.ToString() : "*";
                            rlid.REIDate = reiPhi != null && reiPhi.Rei.HasValue ? czAppRecord.EndDate.ToLocalTime().AddHours((double)reiPhi.Rei).ToString("g") : string.Empty;
                            rlid.PHIDate = reiPhi != null && reiPhi.Phi.HasValue ? czAppRecord.EndDate.ToLocalTime().AddDays((double)reiPhi.Phi).ToString("g") : string.Empty;

                            if (inventory.Products.ContainsKey(czAppRecord.ProductId))
                            {
                                rlid.TotalCost = inventory.Products[czAppRecord.ProductId].AveragePriceValue * czAppRecord.TotalGrowerProductValue; // TODO: This will only pan out if the units of these measures is the same. Need to take this into account.
                            }

                            foreach (var ai in mlp.ActiveIngredients)
                            {
                                if (ai.Id != new Guid()){
                                    var itemData = new ActiveIngredientItemData();

                                    itemData.ActiveIngredientID = ai.Id;
                                    itemData.Name = clientEndpoint.GetMasterlistService().GetActiveIngredientDisplay(new ActiveIngredientId(ai.Id));

                                    var ProductMeasure = UnitFactory.GetUnitByName(czAppRecord.TotalProductUnit).GetMeasure(czAppRecord.TotalProductValue, mlp.Density);
                                    var pound = UnitFactory.GetUnitByName("pound");
                                    var poundsOfProductValue = ProductMeasure.CanConvertTo(pound) ? ProductMeasure.GetValueAs(pound) : ProductMeasure.Value;
                                    var totalLoad = poundsOfProductValue * (double)ai.Percent;

                                    var dataItem = data.ActiveItems.Where(x => x.ActiveIngredientID == ai.Id).FirstOrDefault();

                                    if (dataItem != null)
                                    {
                                        data.ActiveItems.Remove(dataItem);

                                        dataItem.TotalLoad += totalLoad;
                                        data.ActiveItems.Add(dataItem);
                                    }
                                    else
                                    {
                                        itemData.TotalLoad = totalLoad;
                                        data.ActiveItems.Add(itemData);
                                    }
                                }
                            }
                            data.LineItems.Add(rlid);
                        }

                        data.LineItems = data.LineItems.OrderBy(l => l.StartDateTime).ToList();
                        int i = 0;
                        foreach (var item in data.LineItems)
                        {
                            item.RowCount = i;
                            i++;
                        }
                        var report = new Landdb.Views.Secondary.Reports.AppliedProduct.AppliedProductRegulatory();
                        report.DataSource = data;
                        report.Name = data.FieldDisplay;

                        if (data.LineItems.Count() > 0)
                        {
                            book.Reports.Add(report);
                        }
                    }
                }
            }

            rs.ReportDocument = book;
            return rs;
        }

        public string DisplayName
        {
            get { return Strings.GeneratorName_AppliedProductsRegulatory_Text; }
        }

    }
}
