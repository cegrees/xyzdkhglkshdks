﻿using AgC.UnitConversion;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Contract;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Domain.ReadModels.Invoice;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.ReportModels.AppliedProductByFieldDetail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Landdb.Resources;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.AppliedProduct.Generators
{
    public class LandOwnerShareGenerator : IAppliedProductReportGenerator
    {
        IClientEndpoint clientEndpoint;
        int cropYear;

        public LandOwnerShareGenerator(IClientEndpoint clientEndpoint, int cropYear)
        {
            this.clientEndpoint = clientEndpoint;
            this.cropYear = cropYear;
        }

        public Telerik.Reporting.ReportSource Generate(AppliedQueryCriteria criteria)
        {
            ReportBook book = new ReportBook();
            var rs = new InstanceReportSource();

            var applicationData = clientEndpoint.GetView<CropZoneApplicationDataView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new CropZoneApplicationDataView());
            var inventory = clientEndpoint.GetView<InventoryListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new InventoryListView());
            var flattenedHierarchy = clientEndpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new FlattenedTreeHierarchyView());
            var masterlist = clientEndpoint.GetMasterlistService();

            if (criteria.SelectedCropZones.Any())
            {
                var contractList = clientEndpoint.GetView<RentContractListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear));
                foreach (var czId in criteria.SelectedCropZones)
                {
                    var czDetails = clientEndpoint.GetView<CropZoneDetailsView>(czId).GetValue(new CropZoneDetailsView());

                    var rentContracts = from c in czDetails.Contracts
                                        where contractList.Value.RentContracts.Any(x => x.Id == c.Id)
                                        select c;

                    var rentContract = rentContracts.FirstOrDefault();
                    RentContractDetailsView contract = new RentContractDetailsView();
                    
                    if (rentContract != null)
                    {
                        var data = new ReportData();
                        
                        contract = clientEndpoint.GetView<RentContractDetailsView>(rentContract.Id).Value;
                        data.CostType = criteria.CostType;
	                    data.CropYear = cropYear.ToString();
                        data.DataSourceName = !string.IsNullOrWhiteSpace(criteria.SelectedEntity.Name) ? criteria.SelectedEntity.Name : ApplicationEnvironment.CurrentDataSourceName;
                        data.DateRangeDisplay = string.Format("{0} - {1}", criteria.QueryStartDate.ToString("d"), criteria.QueryEndDate.ToString("d"));

                        var czTreeData = (from f in flattenedHierarchy.Items
                                          where f.CropZoneId == czId
                                          select f).FirstOrDefault();
                        if (czTreeData != null)
                        {
                            data.FieldDisplay = string.Format(@"{0} \ {1} \ {2}", czTreeData.FarmName, czTreeData.FieldName, czTreeData.CropZoneName);
                            data.CropDisplay = masterlist.GetCropDisplay(czTreeData.CropId);
                        }

                        if (czDetails.ReportedArea != null && czDetails.ReportedArea > 0)
                        {
                            data.AreaDisplay = UnitFactory.GetUnitByName(czDetails.ReportedAreaUnit).GetMeasure(czDetails.ReportedArea.Value).FullDisplay;
                            data.Area = czDetails.ReportedArea.Value;
                        }
                        else if (czDetails.BoundaryArea != null)
                        {
                            data.AreaDisplay = UnitFactory.GetUnitByName(czDetails.BoundaryAreaUnit).GetMeasure(czDetails.BoundaryArea.Value).FullDisplay;
                            data.Area = czDetails.BoundaryArea.Value;
                        }
                        else
                        {
                            data.AreaDisplay = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(0).FullDisplay;
                            data.Area = 0;
                        }

                        var czArea = data.Area;
                        var czAppData = from x in applicationData.Items
                                        where x.CropZoneId == czId &&
                                        x.EndDate >= criteria.QueryStartDate &&
                                        x.EndDate <= criteria.QueryEndDate && criteria.SelectedProducts.Contains(x.ProductId.Id) == true
                                        select x;

                        double? _total = 0.0;

                        foreach (var czAppRecord in czAppData)
                        {
                            var mlp = masterlist.GetProduct(czAppRecord.ProductId);
                            ReportLineItemData rlid = new ReportLineItemData();
                            rlid.StartDate = czAppRecord.StartDate.ToLocalTime().ToShortDateString();
                            rlid.StartDateTime = czAppRecord.StartDate.ToLocalTime();
                            //TO Do :: alter rate and total to user defined units....
                            var displayRate = DisplayMeasureConverter.ConvertToRateDisplayMeasure(czAppRecord.ProductId, czAppRecord.RateValue, czAppRecord.RateUnit, mlp.Density, clientEndpoint, ApplicationEnvironment.CurrentDataSourceId);
                            var displayTotal = DisplayMeasureConverter.ConvertToTotalDisplayMeasure(czAppRecord.ProductId, (decimal)czAppRecord.TotalProductValue, czAppRecord.TotalProductUnit, mlp.Density, clientEndpoint, ApplicationEnvironment.CurrentDataSourceId);

                            ApplicationView appDetails = clientEndpoint.GetView<ApplicationView>(czAppRecord.ApplicationId).Value;
                            var coverage = appDetails.Products.FirstOrDefault(x => x.TrackingId == czAppRecord.ProductTrackingId).PercentApplied;
                            coverage = coverage == 0 ? 1 : coverage;
                            rlid.RateDisplay = displayRate.Value.ToString("n2") + " " + displayRate.Unit.AbbreviatedDisplay; // TODO: Make this a measure.
                            rlid.AreaDisplay = UnitFactory.GetUnitByName(czAppRecord.AreaUnit).GetMeasure(czAppRecord.AreaValue * (double)coverage).FullDisplay;
                            rlid.ProductDisplay = mlp.Name;
                            rlid.TotalProductDisplay = displayTotal.Value.ToString("n2") + " " + displayTotal.Unit.AbbreviatedDisplay; // TODO: Make this a measure.
                            rlid.TotalProduct = displayTotal.Value;



                            rlid.CropZoneArea = data.Area; // appDetails.CropZones.SingleOrDefault(c => c.Id == czAppRecord.CropZoneId).Area.Value;
                            rlid.ApplicationArea = appDetails.CropZones.SingleOrDefault(c => c.Id == czAppRecord.CropZoneId).Area.Value;
                            var UniqueCompanyNames = (from i in appDetails.Applicators
                                                      select i.CompanyName).Distinct().ToList();
                            string applicatorDisplay = string.Join(" / ", UniqueCompanyNames);
                            rlid.ApplicatorDisplay = applicatorDisplay;

                            double margin = 0.0;

                            switch (mlp.ProductType.ToLower())
                            {
                                case "cropprotection":
                                    margin = contract.IncludedInventoryInfo.CropProtectionMargin;
                                    break;
                                case "fertilizer":
                                    margin = contract.IncludedInventoryInfo.FertilizerMargin;
                                    break;
                                case "service":
                                    margin = contract.IncludedInventoryInfo.ServiceMargin;
                                    break;
                                case "seed":
                                    margin = contract.IncludedInventoryInfo.SeedMargin;
                                    break;
                            }

                            rlid.IsAverage = string.Empty;

                            switch (criteria.CostType)
                            {
                                case "Average":
                                    rlid.IncludeCost = true;
                                    rlid.TotalCost = GetAvgLandOwnerCost(czAppRecord, inventory);
                                    rlid.TotalCost = rlid.TotalCost * (1 - czAppRecord.GrowerShare) * (1 + margin);
                                    _total += rlid.TotalCost;
                                    break;
                                case "Specific":
                                    rlid.IncludeCost = true;
                                    //get specific price
                                    var totalSpecific = GetSpecificCost(czAppRecord, appDetails);
                                    var appProduct = appDetails.Products.SingleOrDefault(x => x.TrackingId == czAppRecord.ProductTrackingId);
                                    if (appProduct != null && !string.IsNullOrEmpty(appProduct.SpecificCostUnit))
                                    {
                                        rlid.TotalCost = totalSpecific;
                                        rlid.TotalCost = rlid.TotalCost * (1 - czAppRecord.GrowerShare) * (1 + margin);
                                        _total += rlid.TotalCost;
                                    }
                                    else
                                    {
                                        rlid.IsAverage = "*";
                                        rlid.TotalCost = GetAvgLandOwnerCost(czAppRecord, inventory);
                                        rlid.TotalCost = rlid.TotalCost * (1 - czAppRecord.GrowerShare) * (1 + margin);
                                        _total += rlid.TotalCost;
                                    }
                                    break;
                                case "Invoice":
                                    rlid.IncludeCost = true;
                                    //get invoice

                                    var source = appDetails.Sources.Where(x => x.DocumentType == "Invoice");
                                    double invoiceCostPerUnit = 0.0;
                                    int cnt = 0;
                                    if (source.Count() > 0)
                                    {
                                        foreach (var src in source)
                                        {
                                            var invoiceDetailmaybe = clientEndpoint.GetView<InvoiceView>(src.Identity as InvoiceId);
                                            if (invoiceDetailmaybe.HasValue)
                                            {
                                                var invoiceDetail = invoiceDetailmaybe.Value;
                                                var invoiceProducts = (from p in invoiceDetail.Products
                                                                       where p.ProductId == czAppRecord.ProductId
                                                                       select p).ToList();
                                                var containsKey = inventory.Products.ContainsKey(czAppRecord.ProductId);

                                                if (containsKey)
                                                {
                                                    var product = inventory.Products[czAppRecord.ProductId];
                                                    var fallbackUnit = invoiceProducts.Any() ? UnitFactory.GetUnitByName(invoiceProducts[0].TotalProductUnit) : UnitFactory.GetPackageSafeUnit(czAppRecord.TotalProductUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                                                    IUnit unit = !string.IsNullOrEmpty(product.AveragePriceUnit) ? UnitFactory.GetPackageSafeUnit(inventory.Products[czAppRecord.ProductId].AveragePriceUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit) : fallbackUnit;  //.GetUnitByName(inventory.Products[czAppRecord.ProductId].AveragePriceUnit) : fallbackUnit;

                                                    foreach (var prod in invoiceProducts)
                                                    {
                                                        var measurePreConvertUnit = UnitFactory.GetPackageSafeUnit(prod.TotalProductUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                                                        var measurePreConvert = measurePreConvertUnit.GetMeasure((double)prod.TotalProductValue, mlp.Density);
                                                        var canConvert = measurePreConvert.CanConvertTo(unit);
                                                        var convertedMeasure = measurePreConvert.CanConvertTo(unit) && measurePreConvert.Unit != unit ? unit.GetMeasure(measurePreConvert.GetValueAs(unit)) : measurePreConvert;
                                                        invoiceCostPerUnit += (double)(prod.TotalCost / (decimal)convertedMeasure.Value);
                                                        cnt++;
                                                    }
                                                }
                                            }
                                        }

                                        if (inventory.Products.ContainsKey(czAppRecord.ProductId))
                                        {
                                            try
                                            {
                                                var totalMeasure = UnitFactory.GetUnitByName(czAppRecord.TotalProductUnit).GetMeasure(czAppRecord.TotalProductValue, masterlist.GetProduct(czAppRecord.ProductId).Density);
                                                var avgPriceUnit = string.IsNullOrEmpty(inventory.Products[czAppRecord.ProductId].AveragePriceUnit) ? null : UnitFactory.GetUnitByName(inventory.Products[czAppRecord.ProductId].AveragePriceUnit);
                                                var convertedTotalMeasureValue = totalMeasure.CanConvertTo(avgPriceUnit) ? totalMeasure.GetValueAs(avgPriceUnit) : totalMeasure.Value;
                                                rlid.TotalCost = invoiceCostPerUnit != 0.0 ? invoiceCostPerUnit / cnt * convertedTotalMeasureValue : GetAvgLandOwnerCost(czAppRecord, inventory);
                                                rlid.TotalCost = rlid.TotalCost * (1 - czAppRecord.GrowerShare) * (1 + margin);
                                                rlid.IsAverage = invoiceCostPerUnit != 0.0 ? string.Empty : "*";
                                                _total += rlid.TotalCost;
                                            }
                                            catch
                                            {
                                                rlid.TotalCost = 0.0;
                                                _total += rlid.TotalCost;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        rlid.IsAverage = "*";
                                        rlid.TotalCost = GetAvgLandOwnerCost(czAppRecord, inventory);
                                        rlid.TotalCost = rlid.TotalCost * (1 - czAppRecord.GrowerShare) * (1 + margin);
                                        _total += rlid.TotalCost;
                                    }

                                    break;
                                case "None":
                                    _total = null;
                                    rlid.IncludeCost = false;
                                    break;
                                default:
                                    rlid.IncludeCost = true;
                                    if (inventory.Products.ContainsKey(czAppRecord.ProductId))
                                    {
                                        try
                                        {
                                            var totalMeasure = UnitFactory.GetUnitByName(czAppRecord.TotalProductUnit).GetMeasure(czAppRecord.TotalProductValue, masterlist.GetProduct(czAppRecord.ProductId).Density);
                                            var avgPriceUnit = string.IsNullOrEmpty(inventory.Products[czAppRecord.ProductId].AveragePriceUnit) ? null : UnitFactory.GetUnitByName(inventory.Products[czAppRecord.ProductId].AveragePriceUnit);

                                            double convertedTotalMeasureValue = 0;
                                            if (totalMeasure.CanConvertTo(avgPriceUnit))
                                            {
                                                convertedTotalMeasureValue = totalMeasure.GetValueAs(avgPriceUnit);
                                            }
                                            else if (czAppRecord.TotalProductUnit == mlp.StdPackageUnit)
                                            {
                                                var packageMeasure = UnitFactory.GetPackageSafeUnit(mlp.StdPackageUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit).GetMeasure(czAppRecord.TotalProductValue, mlp.Density);
                                                convertedTotalMeasureValue = packageMeasure.CanConvertTo(avgPriceUnit) ? packageMeasure.GetValueAs(avgPriceUnit) : totalMeasure.Value;
                                            }
                                            else
                                            {
                                                convertedTotalMeasureValue = totalMeasure.Value;
                                            }

                                            // TODO: This will only pan out if the units of these measures is the same. Need to take this into account.
                                            // this may be okay now with the above if statement
                                            rlid.IsAverage = "*";
                                            rlid.TotalCost = Math.Round(inventory.Products[czAppRecord.ProductId].AveragePriceValue * convertedTotalMeasureValue, 2);
                                            rlid.TotalCost = rlid.TotalCost * (1 - czAppRecord.GrowerShare) * (1 + margin);
                                            _total += rlid.TotalCost;
                                        }
                                        catch (Exception ex) { }
                                    }
                                    break;
                            }


                            data.LineItems.Add(rlid);
                            if (mlp.ProductType == GlobalStrings.ProductType_CropProtection) { data.CropProtectionLineItems.Add(rlid); }
                            if (mlp.ProductType == GlobalStrings.ProductType_Fertilizer) { data.FertilizerLineItems.Add(rlid); }
                            if (mlp.ProductType == GlobalStrings.ProductType_Seed) { data.SeedLineItems.Add(rlid); }
                            if (mlp.ProductType == GlobalStrings.ProductType_Service) { data.ServiceLineItems.Add(rlid); }
                        }

                        data.CropProtectionLineItems = data.CropProtectionLineItems.OrderBy(c => c.StartDateTime.ToLocalTime()).ToList();
                        data.FertilizerLineItems = data.FertilizerLineItems.OrderBy(f => f.StartDateTime.ToLocalTime()).ToList();
                        data.SeedLineItems = data.SeedLineItems.OrderBy(x => x.StartDateTime.ToLocalTime()).ToList();
                        data.ServiceLineItems = data.ServiceLineItems.OrderBy(s => s.StartDateTime.ToLocalTime()).ToList();

                        data.TotalCost = _total;
                        data.TotalCostPerArea = _total != null ? _total / data.Area : null;
                        var report = new Landdb.Views.Secondary.Reports.AppliedProduct.LandOwnerShare();
                        report.DataSource = data;
                        report.Name = data.FieldDisplay;

                        if (data.LineItems.Count() > 0)
                        {
                            book.Reports.Add(report);
                        }
                    }
                }
            }

            rs.ReportDocument = book;
            return rs;
        }

        public string DisplayName
        {
            get { return Strings.GeneratorName_LandOwnerShare_Text; }
        }

        public double GetAvgLandOwnerCost(CropZoneApplicationDataItem czAppRecord, InventoryListView inventory)
        {
            var masterlist = clientEndpoint.GetMasterlistService();
            if (inventory.Products.ContainsKey(czAppRecord.ProductId))
            {
                try
                {
                    var mlp = masterlist.GetProduct(czAppRecord.ProductId);
                    var totalMeasure = UnitFactory.GetPackageSafeUnit(czAppRecord.TotalProductUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit).GetMeasure(czAppRecord.TotalProductValue, mlp.Density);
                    var avgPriceUnit = string.IsNullOrEmpty(inventory.Products[czAppRecord.ProductId].AveragePriceUnit) ? null : UnitFactory.GetPackageSafeUnit(inventory.Products[czAppRecord.ProductId].AveragePriceUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                    var convertedTotalMeasureValue = totalMeasure.CanConvertTo(avgPriceUnit) ? totalMeasure.GetValueAs(avgPriceUnit) : totalMeasure.Value;
                    return Math.Round(inventory.Products[czAppRecord.ProductId].AveragePriceValue * convertedTotalMeasureValue, 2); // TODO: This will only pan out if the units of these measures is the same. Need to take this into account.

                }
                catch (Exception ex) { return 0.0; }
            }
            else
            {
                return 0.0;
            }
        }

        public double GetSpecificCost(CropZoneApplicationDataItem czAppRecord, ApplicationView view)
        {
            var appProduct = view.Products.SingleOrDefault(x => x.TrackingId == czAppRecord.ProductTrackingId);
            var totalSpecific = 0.0;

            if (appProduct.SpecificCostPerUnit.HasValue && !string.IsNullOrEmpty(appProduct.SpecificCostUnit))
            {
                var masterlist = clientEndpoint.GetMasterlistService();
                var mlp = masterlist.GetProduct(czAppRecord.ProductId);
                var avgPriceUnit = UnitFactory.GetPackageSafeUnit(appProduct.SpecificCostUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                var totalProductUnit = UnitFactory.GetPackageSafeUnit(czAppRecord.TotalProductUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                var totalMeasure = totalProductUnit.GetMeasure(czAppRecord.TotalProductValue, mlp.Density);
                var converted = totalMeasure.Unit != avgPriceUnit && totalMeasure.CanConvertTo(avgPriceUnit) ? totalMeasure.CreateAs(avgPriceUnit) : totalMeasure;
                totalSpecific = converted.Value * (double)appProduct.SpecificCostPerUnit.Value;
            }

            return totalSpecific;
        }
    }
}
