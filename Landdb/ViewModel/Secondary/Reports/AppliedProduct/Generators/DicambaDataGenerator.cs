﻿using AgC.UnitConversion;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Map;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.ReportModels.Application;
using Landdb.ReportServices;
using Landdb.ViewModel.Secondary.Application;
using Landdb.ViewModel.Secondary.Reports.Application.Generators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Threading;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.AppliedProduct.Generators
{
    public class DicambaDataGenerator : IAppliedProductReportGenerator
    {
        IClientEndpoint clientEndpoint;
        Dispatcher dispatcher;
        int cropYear;
        CropZoneApplicationDataView czApplications;
        public DicambaDataGenerator(IClientEndpoint clientEndpoint, Dispatcher dispatcher, int cropYear)
        {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            this.cropYear = cropYear;

            czApplications = clientEndpoint.GetView<Domain.ReadModels.Application.CropZoneApplicationDataView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new CropZoneApplicationDataView());
            //getInitialData();
            masterlistService = clientEndpoint.GetMasterlistService();
        }

        private IMasterlistService masterlistService { get; set; }
        private FlattenedTreeHierarchyView flattenedHierarchy { get; set; }

        public string DisplayName => "Dicamba Report";

        private ApplicationData getApplicationData(ApplicationId appId)
        {
            var productUnits = clientEndpoint.GetProductUnitResolver();
            var appData = clientEndpoint.GetView<ApplicationView>(appId).Value;
            //var inventory = clientEndpoint.GetView<InventoryListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new InventoryListView());
            var flattenedHierarchy = clientEndpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new FlattenedTreeHierarchyView());
            var masterlist = masterlistService;
            var defaultAreaUnit = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit);
            var data = new ApplicationData();
            if (appData != null)
            {

                data.DatasourceName = ApplicationEnvironment.CurrentDataSourceName;
                data.ApplicationName = appData.Name;
                data.ApplicationDate = appData.StartDateTime.ToLocalTime().ToShortDateString();
                data.ApplicationType = appData.ProductStrategy.ToString();
                data.StartDate = appData.StartDateTime.ToLocalTime();
                data.EndDate = appData.CustomEndDate != null ? ((DateTime)appData.CustomEndDate).ToLocalTime() : appData.StartDateTime.ToLocalTime().AddHours(1.0);
                data.TotalArea = appData.CropZones.Sum(x => x.Area.Value).ToString("N2") + " " + appData.CropZones.FirstOrDefault(x => x.Area.Unit != null).Area.Unit.AbbreviatedDisplay;
                var buffer = appData.EnvironmentalZoneDistanceValue.HasValue ? appData.EnvironmentalZoneDistanceValue.Value : 0m;
                var bufferUnit = !string.IsNullOrEmpty( appData.EnvironmentalZoneDistanceUnit ) ? UnitFactory.GetUnitByName(appData.EnvironmentalZoneDistanceUnit) : null;
                var bufferDisplay = bufferUnit != null ? bufferUnit.GetMeasure((double)buffer) : new NullMeasure();
                data.TimingEvent = appData.TimingEvent;

                if (appData.Conditions != null)
                {
                    data.Temperature = appData.Conditions.TemperatureValue;
                    data.Humidity = appData.Conditions.HumidityPercent;
                    data.WindDirection = appData.Conditions.WindDirection;
                    data.WindSpeed = appData.Conditions.WindSpeedValue;
                    data.SkyCondition = appData.Conditions.SkyCondition == "-1" || string.IsNullOrEmpty(appData.Conditions.SkyCondition) ? string.Empty : appData.Conditions.SkyCondition;
                    data.SoilCondition = appData.Conditions.SoilMoisture == "-1" ? string.Empty : appData.Conditions.SoilMoisture;
                }

                var endEnvironmentalConditions = getWeatherConditions(appData.CropZones, data.EndDate);

                data.EndHumidity = endEnvironmentalConditions.RelativeHumidity;
                data.EndTemperature = endEnvironmentalConditions.Temperature;
                data.EndWindDirection = endEnvironmentalConditions.WindDirection.DisplayText;
                data.EndWindSpeed = endEnvironmentalConditions.WindSpeed;

                data.TankInformationVisibility = appData.TankInformation == null ? false : true;
                if (appData.TankInformation != null)
                {
                    data.TankSizeValue = appData.TankInformation.TankSizeValue;
                    data.TankSizeUnit = appData.TankInformation.TankSizeUnit;
                    data.CarrierPerAreaValue = appData.TankInformation.CarrierPerAreaValue;
                    data.CarrierPerAreaUnit = appData.TankInformation.CarrierPerAreaUnit;
                    data.TotalCarrierValue = appData.TankInformation.TotalCarrierValue;
                    data.TotalCarrierUnit = appData.TankInformation.TotalCarrierUnit;
                    data.TankCount = appData.TankInformation.TankCount;
                }

                var cnt = 0;
                foreach (var prod in appData.Products)
                {
                    var masterlistProduct = masterlist.GetProduct(prod.Id);
                    var czCropIds = from m in appData.CropZones
                                    select clientEndpoint.GetView<CropZoneDetailsView>(m.Id).Value.CropId;
                    var productService = new ProductServices(clientEndpoint);
                    ReiPhi reiPhi = productService.ReturnLargestReiPhi(prod.Id, czCropIds.ToList()); //new ReiPhi();

                    if (reiPhi != null)
                    {
                        reiPhi.Rei = reiPhi.Rei;
                        reiPhi.Phi = reiPhi.Phi;
                    }

                    var aiList = from i in masterlistProduct.ActiveIngredients
                                 select masterlist.GetActiveIngredientDisplay(new ActiveIngredientId(i.Id));
                    string ai = string.Join(",", aiList.ToArray());

                    var registrationNumber = masterlistProduct.RegistrationNumber;
                    string epaNoTrailingChar = string.Empty;
                    if (registrationNumber == null) { epaNoTrailingChar = string.Empty; }
                    else
                    {
                        Regex epaNumberRegex = new Regex("([1-9][0-9]*)");

                        var matches = from m in epaNumberRegex.Matches(registrationNumber).Cast<Match>()
                                      select m.Value;
                        epaNoTrailingChar = string.Join("-", matches.ToArray());
                    }

                    var displayRate = DisplayMeasureConverter.ConvertToRateDisplayMeasure(prod.Id, prod.RateValue, prod.RateUnit, masterlistProduct.Density, clientEndpoint, ApplicationEnvironment.CurrentDataSourceId);
                    var displayTotal = DisplayMeasureConverter.ConvertToTotalDisplayMeasure(prod.Id, (decimal)prod.TotalProductValue, prod.TotalProductUnit, masterlistProduct.Density, clientEndpoint, ApplicationEnvironment.CurrentDataSourceId);
                    var perTankMeasure = UnitFactory.GetUnitByName(prod.RatePerTankUnit).GetMeasure((double)prod.RatePerTankValue, masterlistProduct.Density);
                    var displayPerTank = DisplayMeasureConverter.ConvertToRatePerTankDisplayMeasure(prod.Id, perTankMeasure, clientEndpoint, ApplicationEnvironment.CurrentDataSourceId);

                    Measure displayPer100 = null;
                    if (data.ApplicationType == "ByRatePerTank")
                    {
                        var ratePer100Value = data.ApplicationType == "ByRatePerTank" ? ((double)prod.RatePerTankValue / ((double)appData.TankInformation.TankSizeValue / 100)) : 0;
                        var ratePer100Unit = data.ApplicationType == "ByRatePerTank" ? UnitFactory.GetPackageSafeUnit(prod.RatePerTankUnit, masterlistProduct.StdUnit, masterlistProduct.StdFactor, masterlistProduct.StdPackageUnit) : null;
                        var ratePer100Measure = ratePer100Unit.GetMeasure(ratePer100Value, masterlistProduct.Density);
                        displayPer100 = DisplayMeasureConverter.ConvertToRatePer100DisplayMeasure(prod.Id, ratePer100Measure, clientEndpoint, ApplicationEnvironment.CurrentDataSourceId);
                    }

                    //var invProd = inventory.Products[prod.Id];
                    var totalUnit = UnitFactory.GetPackageSafeUnit(prod.TotalProductUnit, masterlistProduct.StdUnit, masterlistProduct.StdFactor, masterlistProduct.StdPackageUnit);
                    var totalProd = totalUnit.GetMeasure((double)prod.TotalProductValue, masterlist.GetProduct(prod.Id).Density);
                    //var avgPriceUnit = !string.IsNullOrEmpty(invProd.AveragePriceUnit) ? UnitFactory.GetPackageSafeUnit(invProd.AveragePriceUnit, masterlistProduct.StdUnit, masterlistProduct.StdFactor, masterlistProduct.StdPackageUnit) : null;
                    //var convertedProdValue = avgPriceUnit != null ? totalProd.GetValueAs(avgPriceUnit) : totalProd.Value;
                    //var totalCost = convertedProdValue * invProd.AveragePriceValue;
                    var specificCostUnit = !string.IsNullOrEmpty(prod.SpecificCostUnit) ? UnitFactory.GetPackageSafeUnit(prod.SpecificCostUnit, masterlistProduct.StdUnit, masterlistProduct.StdFactor, masterlistProduct.StdPackageUnit) : null;
                    var convertedForSpecific = specificCostUnit != null && totalProd.Unit != specificCostUnit ? totalProd.GetValueAs(specificCostUnit) : totalProd.Value;
                    //totalCost = prod.SpecificCostPerUnit.HasValue ? (double)prod.SpecificCostPerUnit.Value * convertedForSpecific : totalCost;


                    var totalProdUnit = UnitFactory.GetPackageSafeUnit(prod.TotalProductUnit, masterlistProduct.StdUnit, masterlistProduct.StdFactor, masterlistProduct.StdPackageUnit);
                    var totalProdMeasure = totalProdUnit.GetMeasure((double)prod.TotalProductValue, prod.Density);
                    //var convertedTotalProdMeasure = totalProdMeasure.CanConvertTo(avgPriceUnit) ? avgPriceUnit.GetMeasure(totalProdMeasure.GetValueAs(avgPriceUnit)) : totalProdMeasure;
                    //var totalCost = convertedTotalProdMeasure.Value * avgPriceValue;

                    ApplicationData.ProductLineItemData prodData = new ApplicationData.ProductLineItemData()
                    {
                        ApplicationMethod = string.IsNullOrEmpty(prod.ApplicationMethod) ? string.Empty : prod.ApplicationMethod,
                        EPANumber = epaNoTrailingChar,
                        Pest = prod.TargetPest == null ? string.Empty : prod.TargetPest.CommonName,
                        ProductName = masterlistProduct.Name,
                        RateUnit = displayRate.Unit.Name,
                        RateValue = displayRate.Value,
                        RatePerTankUnit = data.ApplicationType == "ByRatePerTank" ? displayPerTank.Unit.Name : prod.RateUnit,
                        RatePerTank = data.ApplicationType == "ByRatePerTank" ? displayPerTank.Value : 0.0,
                        TotalUnit = displayTotal.Unit.Name,
                        TotalValue = displayTotal.Value,
                        PHI = reiPhi != null && reiPhi.Phi != null ? reiPhi.Phi.ToString() : "*",
                        REI = reiPhi != null && reiPhi.Rei != null ? reiPhi.Rei.ToString() : "*",
                        Active = ai,
                        RateType = data.ApplicationType == "ByRatePerTank" ? "Rate/Tank" : "Rate",
                        RatePer100 = data.ApplicationType == "ByRatePerTank" ? displayPer100.Value : 0,
                        RatePer100Unit = data.ApplicationType == "ByRatePerTank" ? displayPer100.Unit.Name : string.Empty,
                        TotalArea = (decimal)appData.TotalArea.Value,
                        TotalAreaUnit = appData.TotalArea.Unit,
                        PercentApplied = prod.PercentApplied,
                        RowCount = cnt,
                        DefaultAreaUnit = defaultAreaUnit.AbbreviatedDisplay,
                    };

                    //List<ApplicationData.AssociatedProductItem> associatedProducts = new List<ApplicationData.AssociatedProductItem>();

                    //foreach (var associate in prod.AssociatedProducts)
                    //{
                    //    var mlp = masterlist.GetProduct(associate.ProductId);
                    //    var item = new ApplicationData.AssociatedProductItem();
                    //    item.CustomRateType = associate.CustomRateType;
                    //    item.CustomRateValue = associate.CustomProductValue;
                    //    item.HasCost = associate.HasCost;
                    //    item.ProductName = associate.ProductName;
                    //    item.RatePerAreaUnit = UnitFactory.GetPackageSafeUnit(associate.RatePerAreaUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                    //    item.RatePerAreaValue = associate.RatePerAreaValue;
                    //    item.TotalProductUnit = UnitFactory.GetPackageSafeUnit(associate.TotalProductUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                    //    item.TotalProductValue = associate.TotalProductValue;
                    //    IUnit customUnit = UnitFactory.GetPackageSafeUnit(associate.CustomProductUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);

                    //    switch (associate.CustomRateType)
                    //    {
                    //        case "ByBag":
                    //            item.DisplayCustomUnit = new CompositeUnit(customUnit.AbbreviatedDisplay, 1, Strings.Unit_Bag_Text.ToLower());
                    //            break;
                    //        case "ByCWT":
                    //            item.DisplayCustomUnit = new CompositeUnit(customUnit.AbbreviatedDisplay, 100, Strings.Unit_Weight_Text.ToLower());
                    //            break;
                    //        case "ByRow":
                    //            item.DisplayCustomUnit = new CompositeUnit(customUnit.AbbreviatedDisplay, 1000, Strings.Unit_LengthRow_Text.ToLower());
                    //            break;
                    //        case "BySeed":
                    //            var parentProd = masterlist.GetProduct(prod.Id);
                    //            item.DisplayCustomUnit = new CompositeUnit(customUnit.AbbreviatedDisplay, 1, parentProd.StdUnit);
                    //            break;
                    //        default:

                    //            break;
                    //    }

                    //    prodData.AssociatedProducts.Add(item);
                    //}

                    data.Products.Add(prodData);
                    cnt++;
                }

                data.TotalCost = data.Products.Sum(x => x.TotalCost);
                List<string> filteredMapData = new List<string>();


                foreach (var cz in appData.CropZones)
                {
                    var czDetails = clientEndpoint.GetView<CropZoneDetailsView>(cz.Id).GetValue(new CropZoneDetailsView());

                    var plantingApplications = PlantingDates(czDetails);

                    var czTreeData = (from f in flattenedHierarchy.Items
                                      where f.CropZoneId == cz.Id
                                      select f).FirstOrDefault();

                    var reportedArea = ReportedArea(czDetails);

                    var fieldDetails = clientEndpoint.GetView<FieldDetailsView>(czDetails.FieldId).Value;
                    var centerLatLon = string.IsNullOrEmpty(czDetails.CenterLatLong) ? fieldDetails.CenterLatLong : czDetails.CenterLatLong;

                    ApplicationData.FieldLineItemData field = new ApplicationData.FieldLineItemData()
                    {
                        Area = cz.Area.Value,
                        CenterLatLong = centerLatLon,
                        Crop = masterlist.GetCropDisplay(czDetails.CropId),
                        CropZone = czDetails.Name,
                        Field = czTreeData.FieldName,
                        Farm = czTreeData.FarmName,
                        Buffer = bufferDisplay != new NullMeasure() ? bufferDisplay.ToString() : "",
                        PlantingDate = plantingApplications.Any() && plantingApplications.Count > 0 ? plantingApplications.FirstOrDefault().StartDate.ToShortDateString() : "",
                    };
                    data.Fields.Add(field);

                }

                foreach (var applicator in appData.Applicators)
                {
                    ApplicationData.ApplicatorLineItemData appLineItem = new ApplicationData.ApplicatorLineItemData()
                    {
                        Name = applicator.PersonName,
                        Company = applicator.CompanyName,
                        LicenseNumber = applicator.LicenseNumber,
                        ExpirationDate = applicator.Expires,
                        DisplayName = string.Format("{0} ({1})", applicator.PersonName, applicator.CompanyName),
                    };

                    data.Applicators.Add(appLineItem);
                }

                data.Authorizer = appData.Authorization != null ? appData.Authorization.PersonName : string.Empty;
                data.AuthorizerDate = appData.Authorization != null ? appData.Authorization.AuthorizationDate : DateTime.Now;

                data.CropYear = cropYear;
                var sourceArray = from source in appData.Sources
                                  select "Source :: " + source.DocumentType + "  #" + source.Name + "\n";
                var sourcesString = string.Join("  ", sourceArray);
                data.Notes = appData.Notes;
                if (appData.Sources.Any())
                {
                    data.Notes = data.Notes + "\n" + "******************************************************************** \n" + sourcesString;
                }
            }

            return data;
        }

        private EnvironmentConditions getWeatherConditions(List<Domain.ReadModels.Shared.IncludedCropZone> cropZones, DateTime endDateTime )
        {
            var conditionsModel = new ClearAgWeatherConditionsViewModel(clientEndpoint, dispatcher, ApplicationEnvironment.CurrentCropYear);
            if (cropZones.Any())
            {
                var czs = cropZones;

                //get center lat lon of the included fields then set conditions lookuppoint
                //string mapData = null;
                List<string> mapShapeStrings = new List<string>();
                foreach (var cz in czs)
                {
                    var mapItem = clientEndpoint.GetView<ItemMap>(cz.Id);

                    if (mapItem.HasValue && mapItem.Value.MostRecentMapItem != null && mapItem.Value.MostRecentMapItem.MapData != "MULTIPOLYGON EMPTY")
                    {
                        var mapString = mapItem.Value.MostRecentMapItem.MapData;
                        mapString = mapString.Substring(13, (mapString.Length - 14));
                        mapShapeStrings.Add(mapString);
                        //mapData += mapItem.Value.MostRecentMapItem.MapData;
                    }
                }


                if (mapShapeStrings.Any())
                {
                    try
                    {
                        var multiString = string.Join(",", mapShapeStrings.ToArray());
                        var multiPolygonString = string.Format("Multipolygon({0})", multiString);
                        var shape = ThinkGeo.MapSuite.Core.PolygonShape.CreateShapeFromWellKnownData(multiPolygonString);
                        var center = shape.GetCenterPoint();
                        conditionsModel.GeoLookupPoint = center;
                        //conditionsModel.GeoLookupPoint = center; // string.Format("{0},{1}", center.Y.ToString("n1"), center.X.ToString("n1"));
                    }catch(Exception x)
                    {

                    }
                    
                }

                conditionsModel.ApplicationStartDate = endDateTime;

                return conditionsModel.EnvironmentConditions;
                //EnvironmentConditions.Digest(conditionsModel);
            }
            else
            {
                return new EnvironmentConditions();
            }
        }
        private double ReportedArea(CropZoneDetailsView czDetails)
        {
            if (czDetails.ReportedArea != null)
            {
                return (double)czDetails.ReportedArea;
            }
            else if (czDetails.ReportedArea == null && czDetails.BoundaryArea != null)
            {
                return (double)czDetails.BoundaryArea;
            }
            else if (czDetails.ReportedArea == null && czDetails.BoundaryArea == null)
            {
                return 0.0;
            }
            else
            {
                return 0.0;
            }
        }

        public List<CropZoneApplicationDataItem> PlantingDates(CropZoneDetailsView czDetails)
        {
            var plantingApplications = new List<CropZoneApplicationDataItem>();
            var czApps = czApplications.Items.Where(x => x.CropZoneId == czDetails.Id);

            foreach (var prod in czApps)
            {
                var mlp = masterlistService.GetProduct(prod.ProductId);
                if (mlp.ProductType == "Seed")
                {
                    plantingApplications.Add(prod);
                }
            }
            
            return plantingApplications;
        }

        public ReportSource Generate(AppliedQueryCriteria criteria)
        {
            ReportBook book = new ReportBook();
            var rs = new InstanceReportSource();

            //return null;
            var applicationData = clientEndpoint.GetView<CropZoneApplicationDataView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new CropZoneApplicationDataView());
            var flattenedHierarchy = clientEndpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new FlattenedTreeHierarchyView());
            var masterlist = clientEndpoint.GetMasterlistService();
            var actives = masterlist.GetActiveIngredientList();
            var dicambaAI = actives.Where(x => x.Name.ToLower().Contains("dichloromethoxybenzoic") || x.Name.ToLower().Contains("dicamba") || x.Name.ToLower().Contains("Diglycolamine salt of 3.6-dichloro-o-anisic acid")).Distinct(i => i.Id);
            //var byId = actives.Where(x => x.Id.ToString() == "646249d2-1c6e-46ab-b28c-b48e770c45f1" || x.Id.ToString() == "ee262e43-46a4-4406-ab37-a6d57a6d2fc2");
            var applicationIdList = new List<ApplicationId>();

            if (criteria.SelectedCropZones.Any())
            {
                var czapplicationList = (from a in applicationData.Items
                                         where criteria.SelectedCropZones.Contains(a.CropZoneId) &&
                                         a.EndDate >= criteria.QueryStartDate && a.EndDate <= criteria.QueryEndDate &&
                                         criteria.SelectedProducts.Contains(a.ProductId.Id)
                                         select a).OrderBy(x => x.StartDate);

                foreach (var app in czapplicationList)
                {
                    var mlp = masterlist.GetProduct(app.ProductId);
                    var hasDicamba = (from a in dicambaAI
                                        from p in mlp.ActiveIngredients
                                        where p.Id == a.Id || mlp.Name.ToLower().Contains("dicamba")
                                        select mlp).Count() > 0;

                    if (hasDicamba)
                    {
                        //to do :: get all products associated with this application
                        if (!applicationIdList.Contains(app.ApplicationId))
                        {
                            applicationIdList.Add(app.ApplicationId);
                        }
                    }
                    else
                    {
                        //DO NOTHING
                    }
                }
            }

            var appList = new List<ApplicationData>();
            ApplicationService appService = new ApplicationService(clientEndpoint, null);
            foreach (var appId in applicationIdList)
            {
                appList.Add(getApplicationData(appId));
                //PUT REPORT TOGETHER FROM HERe
            }

            var report = new Landdb.Views.Secondary.Reports.AppliedProduct.DicambaForm();
            appList = appList.OrderBy(x => x.ApplicationDate).ToList();
            report.DataSource = appList;
            report.Name = "Dicamba Form";

            if (appList.Count() > 0)
            {
                book.Reports.Add(report);
            }
            rs.ReportDocument = book;
            return rs;

            //return null;
        }
    }
}
