﻿using AgC.UnitConversion;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Company;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Domain.ReadModels.Person;
using Landdb.Domain.ReadModels.ProductSetting;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.ReportModels.ByDay;
using Landdb.ReportServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Landdb.Resources;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.AppliedProduct.Generators
{
    public class ByDayGenerator : IAppliedProductReportGenerator
    {
        IClientEndpoint clientEndpoint;
        int cropYear;

        public ByDayGenerator(IClientEndpoint clientEndpoint, int cropYear)
        {
            this.clientEndpoint = clientEndpoint;
            this.cropYear = cropYear;
        }

        public Telerik.Reporting.ReportSource Generate(AppliedQueryCriteria criteria)
        {
            ReportBook book = new ReportBook();
            var rs = new InstanceReportSource();

            var applicationData = clientEndpoint.GetView<CropZoneApplicationDataView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new CropZoneApplicationDataView());
            var inventory = clientEndpoint.GetView<InventoryListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new InventoryListView());
            var flattenedHierarchy = clientEndpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new FlattenedTreeHierarchyView());
            var masterlist = clientEndpoint.GetMasterlistService();

            //Group by applicationId and add up the product info associated with the selected cropzones.  
            //iterate over by application id count...unique ids....

            if (criteria.SelectedCropZones.Any())
            {
                var czapplicationList = (from a in applicationData.Items
                                      where criteria.SelectedCropZones.Contains(a.CropZoneId) && 
                                      a.EndDate >= criteria.QueryStartDate && a.EndDate <= criteria.QueryEndDate
                                      select a).OrderBy(x=>x.StartDate);

                var applicationList = (from app in czapplicationList
                                      select app.ApplicationId).Distinct().ToList();

                foreach (var appId in applicationList)
                {
                    var data = new ByDayModel();

                    var applicationDetails = clientEndpoint.GetView<ApplicationView>(appId).Value;

                    data.AuthorizedBy = applicationDetails.Authorization != null ? applicationDetails.Authorization.PersonName : string.Empty;
                    data.AuthorizedOn = applicationDetails.Authorization != null ? applicationDetails.Authorization.AuthorizationDate : new Nullable<DateTime>();
                    data.AuthorizeDisplay = applicationDetails.Authorization != null ? string.Format(Strings.Format_On_Text, data.AuthorizedBy, applicationDetails.Authorization.AuthorizationDate.ToString("d")) : string.Empty;
                    data.ApplicationId = applicationDetails.Name;
                    if (applicationDetails.Conditions != null)
                    {
                        var weatherData = applicationDetails.Conditions;
                        data.Temp = weatherData.TemperatureValue;
                        data.WindDirection = weatherData.WindDirection;
                        data.WindSpeed = weatherData.WindSpeedValue;
                        data.Humidity = weatherData.HumidityPercent;
                        data.SkyCondition = weatherData.SkyCondition;
                        data.SoilCondition = weatherData.SoilMoisture == "-1" ? string.Empty : weatherData.SoilMoisture;
                    }

                    switch (applicationDetails.ProductStrategy.ToString())
                    {
                        case "ByRatePerArea":
                            data.ApplicationType = Strings.ApplicationType_ByRate_Text;
                            break;
                        case "ByRatePerTank":
                            data.ApplicationType = Strings.ApplicationType_ByTank_Text;
                            break;
                        default:
                            data.ApplicationType = Strings.ApplicationType_ByTotal_Text;
                            break;
                    }

                    data.DateRangeDisplay = string.Format(Strings.Format_AtToAt_Text, applicationDetails.StartDateTime.ToLocalTime().ToString("d"), applicationDetails.StartDateTime.ToLocalTime().ToString("h:mm tt"), ((DateTime)applicationDetails.CustomEndDate.Value.ToLocalTime()).ToString("d"), ((DateTime)applicationDetails.CustomEndDate.Value.ToLocalTime()).ToString("h:mm tt"));

                    string address1 = string.Empty;
                    string city = string.Empty;
                    string state = string.Empty;
                    string zip = string.Empty;

                    if (criteria.SelectedEntity != null && criteria.SelectedEntity.Type == "Person")
                    {
                        var personMaybe = clientEndpoint.GetView<PersonDetailsView>(new PersonId(ApplicationEnvironment.CurrentDataSourceId, criteria.SelectedEntity.Id));
                        var person = personMaybe.HasValue ? personMaybe.Value : null;
                        if (person.StreetAddress != null)
                        {
                            address1 = person != null ? person.StreetAddress.AddressLine1 : string.Empty;
                            city = person != null ? person.StreetAddress.City : string.Empty;
                            state = person != null ? person.StreetAddress.State : string.Empty;
                            zip = person != null ? person.StreetAddress.ZipCode : string.Empty;
                        }
                    }
                    else if (criteria.SelectedEntity != null && criteria.SelectedEntity.Type == "Company")
                    {
                        var companyMaybe = clientEndpoint.GetView<CompanyDetailsView>(new CompanyId(ApplicationEnvironment.CurrentDataSourceId, criteria.SelectedEntity.Id));
                        var company = companyMaybe.HasValue ? companyMaybe.Value : null;
                        if (company.StreetAddress != null)
                        {
                            address1 = company != null ? company.StreetAddress.AddressLine1 : string.Empty;
                            city = company != null ? company.StreetAddress.City : string.Empty;
                            state = company != null ? company.StreetAddress.State : string.Empty;
                            zip = company != null ? company.StreetAddress.ZipCode : string.Empty;
                        }
                    }
                    else
                    {

                    }

	                data.CropYear = cropYear.ToString();
                    data.DataSourceName = !string.IsNullOrWhiteSpace(criteria.SelectedEntity.Name) ? criteria.SelectedEntity.Name : ApplicationEnvironment.CurrentDataSourceName;
                    data.AddressDisplay = !string.IsNullOrEmpty(address1) ? string.Format("{0}\n{1}, {2}  {3}", address1, city, state, zip) : string.Empty;


                    var containedCZ = from includedCZ in applicationDetails.CropZones
                                        where criteria.SelectedCropZones.Where(x => x == includedCZ.Id).Count() > 0
                                        select includedCZ;

                    var containedAppliedProduct = from includedProd in applicationDetails.Products
                                                  where criteria.SelectedProducts.Where(x => x == includedProd.Id.Id).Count() > 0
                                                  select includedProd;

                    //applicator information
                    foreach (var applicator in applicationDetails.Applicators)
                    {
                        var personDetails = clientEndpoint.GetView<PersonDetailsView>(applicator.PersonId);

                        var applicatorItem = new ApplicatorLineItemData()
                        {
                            
                            Name = applicator.PersonName,
                            Company = applicator.CompanyName,

                            LicenseNumber = applicator.LicenseNumber,
                            ExpirationDate = applicator.Expires,

                            //LicenseNumber = personDetails.Value.ApplicatorLicenseNumber,
                            //ExpirationDate = personDetails.Value.ApplicatorLicenseExpiration,
                            DisplayName = string.Format("{0} ({1})", applicator.PersonName, applicator.CompanyName),
                        };

                        data.ApplicatorItems.Add(applicatorItem);
                    }

                    //each cropzone included in the criteria
                    foreach (var cz in containedCZ)
                    {
                        var cropZoneLineItem = new CropZoneLineItemData();
                        var czDetails = clientEndpoint.GetView<CropZoneDetailsView>(cz.Id).Value;
                        var flattened = flattenedHierarchy.Items.SingleOrDefault(x => x.CropZoneId == cz.Id);
                        cropZoneLineItem.Farm = flattened.FarmName;
                        cropZoneLineItem.Field = flattened.FieldName;
                        cropZoneLineItem.CropZone = flattened.CropZoneName;
                        cropZoneLineItem.Crop = masterlist.GetCrop(czDetails.CropId).Name;
                        cropZoneLineItem.Area = (decimal)cz.Area.Value;

                        double rei = 0.0;
                        double phi = 0.0;
                        foreach (var prod in containedAppliedProduct)
                        {
                            var productService = new ProductServices(clientEndpoint);
                            ReiPhi reiPhi = productService.ReturnReiPhi(prod.Id, czDetails.CropId);

                            if (reiPhi != null)
                            {
                                rei = reiPhi.Rei.HasValue && reiPhi.Rei.Value > rei ? reiPhi.Rei.Value : rei;
                                phi = reiPhi.Phi.HasValue && reiPhi.Phi.Value > phi ? reiPhi.Phi.Value : phi;
                            }
                        }

                        cropZoneLineItem.REI = (decimal)rei;
                        cropZoneLineItem.PHI = (decimal)phi;
                        cropZoneLineItem.REIExpiration = applicationDetails.CustomEndDate != null ? applicationDetails.CustomEndDate.Value.ToLocalTime().AddHours(rei) : DateTime.Now;
                        cropZoneLineItem.PHIExpiration = applicationDetails.CustomEndDate != null ? applicationDetails.CustomEndDate.Value.ToLocalTime().AddDays(phi) : DateTime.Now;

                        data.CropZoneItems.Add(cropZoneLineItem);
                    }

                    //PROCESS ALL THE SELECTED PRODUCTS
                    foreach (var prod in containedAppliedProduct)
                    {
                        var masterlistProd = masterlist.GetProduct(prod.Id);
                        var displayRate = DisplayMeasureConverter.ConvertToRateDisplayMeasure(prod.Id, prod.RateValue, prod.RateUnit, masterlistProd.Density, clientEndpoint, ApplicationEnvironment.CurrentDataSourceId);
                        var displayTotal = DisplayMeasureConverter.ConvertToTotalDisplayMeasure(prod.Id, (decimal)prod.TotalProductValue, prod.TotalProductUnit, masterlistProd.Density, clientEndpoint, ApplicationEnvironment.CurrentDataSourceId);
                        var prodLineItem = new ReportLineItemData();
                        prodLineItem.Rate = displayRate.Value;
                        prodLineItem.RateUnit = displayRate.Unit.Name;
                        prodLineItem.ProductDisplay = masterlistProd.Name;
                        var aiArray = from i in masterlistProd.ActiveIngredients
                                      select masterlist.GetActiveIngredientDisplay(new ActiveIngredientId(i.Id));
                        prodLineItem.Active = string.Join(", ", aiArray.ToArray());

                        var registrationNumber = masterlistProd.RegistrationNumber;

                        if (registrationNumber == null) { prodLineItem.EPANumber = string.Empty; }
                        else
                        {
                            Regex epaNumberRegex = new Regex("([1-9][0-9]*)");

                            var matches = from m in epaNumberRegex.Matches(registrationNumber).Cast<Match>()
                                          select m.Value;
                            prodLineItem.EPANumber = string.Join("-", matches.ToArray());
                        }

                        var TotalArea = ((from cz in containedCZ
                                          select cz.Area.Value).Sum()) * (double)prod.PercentApplied;
                        prodLineItem.AreaDisplay = TotalArea.ToString("N2");
                        var multiplier = TotalArea / (applicationDetails.TotalArea.Value * (double)prod.PercentApplied);
                        prodLineItem.TotalProduct = displayTotal.Value * multiplier;
                        prodLineItem.TotalUnit = displayTotal.Unit.Name;
                        prodLineItem.RUP = masterlistProd.RestrictedUse == true ? "Yes" : "No";
                        prodLineItem.ApplicationMethod = string.IsNullOrEmpty(prod.ApplicationMethod) ? string.Empty : prod.ApplicationMethod;
                        prodLineItem.TargetPest = prod.TargetPest == null ? string.Empty : prod.TargetPest.CommonName;

                        var czCropIds = from m in applicationDetails.CropZones
                                        select clientEndpoint.GetView<CropZoneDetailsView>(m.Id).Value.CropId;
                        var productService = new ProductServices(clientEndpoint);
                        ReiPhi reiPhi = productService.ReturnLargestReiPhi(prod.Id, czCropIds.ToList());

                        prodLineItem.REI = reiPhi == null ? "*" : reiPhi.Rei.ToString();
                        prodLineItem.PHI = reiPhi == null ? "*" : reiPhi.Phi.ToString();

                        prodLineItem.REIDate = applicationDetails.CustomEndDate != null && reiPhi != null && reiPhi.Rei.HasValue ? (applicationDetails.CustomEndDate.Value.AddHours((double)reiPhi.Rei)).ToString() : string.Empty;
                        prodLineItem.PHIDate = applicationDetails.CustomEndDate != null && reiPhi != null && reiPhi.Phi.HasValue ? (applicationDetails.CustomEndDate.Value.AddDays((double)reiPhi.Phi)).ToString() : string.Empty;


                        //CALCULATE AI LOADS PER APPLICATION
                        foreach (var ai in masterlistProd.ActiveIngredients)
                        {
                            if (ai.Id != new Guid())
                            {
                                var itemData = new ActiveIngredientItemData();

                                itemData.ActiveIngredientID = ai.Id;
                                itemData.Name = clientEndpoint.GetMasterlistService().GetActiveIngredientDisplay(new ActiveIngredientId(ai.Id));

                                var psu = UnitFactory.GetPackageSafeUnit(prod.TotalProductUnit, masterlistProd.StdUnit, masterlistProd.StdFactor, masterlistProd.StdPackageUnit);
                                var ProductMeasure = psu.GetMeasure(prodLineItem.TotalProduct, masterlistProd.Density);
                                var pound = UnitFactory.GetUnitByName("pound");
                                var poundsOfProductValue = ProductMeasure.CanConvertTo(pound) ? ProductMeasure.GetValueAs(pound) : 0;
                                var totalLoad = poundsOfProductValue * (double)ai.Percent;

                                var dataItem = data.ActiveItems.Where(x => x.ActiveIngredientID == ai.Id).FirstOrDefault();

                                if (dataItem != null)
                                {
                                    data.ActiveItems.Remove(dataItem);

                                    dataItem.TotalLoad += totalLoad;
                                    data.ActiveItems.Add(dataItem);
                                }
                                else
                                {
                                    itemData.TotalLoad = totalLoad;
                                    data.ActiveItems.Add(itemData);
                                }
                            }
                        }

                        data.LineItems.Add(prodLineItem);
                    }

                    if (data.LineItems.Count() > 0)
                    {
                        var report = new Landdb.Views.Secondary.Reports.AppliedProduct.ByDayApplicationReport();
                        report.DataSource = data;
                        report.Name = data.DataSourceName;

                        book.Reports.Add(report);

                    }
                }
            }
            rs.ReportDocument = book;
            return rs;
        }

        public string DisplayName
        {
            get { return Strings.GeneratorName_ByDayApplication_Text; }
        }
    }
}
