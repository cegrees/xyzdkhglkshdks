﻿using Landdb.Client.Infrastructure;
using Landdb.ViewModel.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Landdb.ViewModel.Secondary.Reports.AppliedProduct {
    public class AppliedProductReportFieldSelectionViewModel : BaseFieldSelectionViewModel {
        public AppliedProductReportFieldSelectionViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, int cropYear) : base(clientEndpoint, dispatcher, cropYear) { Enable = false; }

        protected override void OnFieldCheckedChanged(Fields.CropZoneTreeItemViewModel treeItem) {
            // TODO: Something, right?
        }

        public bool Enable { get; set; }

    }
}
