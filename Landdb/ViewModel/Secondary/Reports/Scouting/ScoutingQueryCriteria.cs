﻿using Landdb.ViewModel.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Secondary.Reports.Scouting
{
    public class ScoutingQueryCriteria
    {
        public ScoutingQueryCriteria()
        {

        }

        public SiteDataListItemViewModel SelectedItem { get; set; }
    }
}
