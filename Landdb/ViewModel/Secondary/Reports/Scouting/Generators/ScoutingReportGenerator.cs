﻿using AgC.UnitConversion;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Map;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.ReportModels.Scouting;
using Landdb.ViewModel.Secondary.Map;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using Landdb.Resources;
using Telerik.Reporting;
using ThinkGeo.MapSuite.Core;

namespace Landdb.ViewModel.Secondary.Reports.Scouting.Generators
{
    public class ScoutingReportGenerator : IScoutingGenerator
    {
        IClientEndpoint endpoint;
        Dispatcher dispatcher;
        int areadecimals = 2;

        public ScoutingReportGenerator(IClientEndpoint endPoint, Dispatcher dispatcher)
        {
            this.endpoint = endPoint;
            this.dispatcher = dispatcher;
        }

        public string DisplayName
        {
            get
            {
                return Strings.GeneratorName_Scouting_Text;
            }
        }

        public ReportSource Generate(ScoutingQueryCriteria criteria)
        {
            ReportBook book = new ReportBook();
            var rs = new InstanceReportSource();

            //WorkOrderService woService = new WorkOrderService(endpoint, criteria);
            //WorkOrderData data = woService.GenerateWorkOrderData();
            ScoutingData data = new ScoutingData();
            var scoutingItem = criteria.SelectedItem;

            data.DocumentName = scoutingItem.Name;
            data.DatasourceName = ApplicationEnvironment.CurrentDataSourceName; 
            data.CropYear = ApplicationEnvironment.CurrentCropYear;
            data.Humidity = scoutingItem.Conditions.HumidityPercent;
            data.SkyCondition = scoutingItem.Conditions.SkyCondition;
            data.SoilCondition = scoutingItem.Conditions.SoilMoisture;
            data.Temperature = scoutingItem.Conditions.TemperatureValue.HasValue ? string.Format("{0} {1}", scoutingItem.Conditions.TemperatureValue.Value, scoutingItem.Conditions.TemperatureUnit) : string.Empty;
            data.WindDirection = scoutingItem.Conditions.WindDirection;
            data.WindSpeed = scoutingItem.Conditions.WindSpeedValue.HasValue ? string.Format("{0} {1}", scoutingItem.Conditions.WindSpeedValue.Value, scoutingItem.Conditions.WindSpeedUnit) : string.Empty;
            data.Notes = scoutingItem.Notes;
            data.ScoutingDate = DateTime.Now;
            data.StartDate = scoutingItem.StartDateTime.HasValue ? scoutingItem.StartDateTime.Value : DateTime.Now;
            data.EndDate = scoutingItem.EndDateTime.HasValue ? scoutingItem.EndDateTime.Value : DateTime.Now;

            var set = scoutingItem.ReportedSets;
            List<Observation> observations = new List<Observation>();
            int observationCnt = 1;
            foreach (var item in set)
            {
                if(item.GeoPosition == null) { continue; }
                var observation = new Observation();
                observation.GeoPosition = new GeoPositionDetails() { Accuracy = item.GeoPosition.Accuracy, GeoTimestamp = item.GeoPosition.GeoTimestamp, Latitude = item.GeoPosition.Latitude, Longitude = item.GeoPosition.Longitude, SysTimestamp = item.GeoPosition.SysTimestamp };
                observation.Notes = item.Notes;
                observation.ObservationName = item.ReportableSetSpec.Name;
                observation.ObservationNumber = observationCnt;
                observation.ObservationProperties = new List<ObservationProperty>();

                foreach (var property in item.ReportedProperties)
                {
                    ObservationProperty itemDetails = new ObservationProperty();
                    itemDetails.Name = property.ReportablePropertySpec.Label;
                    itemDetails.FullName = property.ReportablePropertySpec.Name;
                    
                    if (property.ReportedListItem != null)
                    {
                        itemDetails.DisplayValue = property.ReportedListItem.Label;
                        itemDetails.Value = property.ReportedListItem.Value;
                    }
                    else
                    {
                        itemDetails.Unit = property.ReportedUnit != null ? property.ReportedUnit.FullDisplay : string.Empty;

                        if (!string.IsNullOrEmpty(itemDetails.Unit))
                        {
                            if (property.ReportedUnit.PackageUnitName == "%")
                            {
                                double theValue = 0.0;
                                if (Double.TryParse(property.ReportedValue, out theValue))
                                {
                                    itemDetails.Value = (theValue * 100).ToString("N0");
                                    itemDetails.DisplayValue = string.Format("{0} {1}", itemDetails.Value, itemDetails.Unit);
                                }
                            }
                            else
                            {
                                itemDetails.Value = !string.IsNullOrEmpty(property.ReportedValue) ? property.ReportedValue : string.Empty;
                                itemDetails.DisplayValue = string.Format("{0} {1}", itemDetails.Value, itemDetails.Unit);
                            }
                        }
                        else
                        {
                            var reportedValueString = !string.IsNullOrEmpty(property.ReportedValue) ? property.ReportedValue : string.Empty;
                            if (!string.IsNullOrEmpty(reportedValueString))
                            {
                                double theValue = 0.0;
                                if (Double.TryParse(reportedValueString, out theValue))
                                {
                                    itemDetails.Value = (theValue).ToString("P0");
                                    itemDetails.DisplayValue = (theValue).ToString("P0");
                                }
                                else
                                {
                                    if (reportedValueString.ToLower() == "true")
                                    {
                                        itemDetails.Value = Strings.Yes_Text.ToUpper();
                                        itemDetails.DisplayValue = Strings.Yes_Text.ToUpper();
                                    }
                                    else if (reportedValueString.ToLower() == "false")
                                    {
                                        itemDetails.Value = Strings.No_Text.ToUpper();
                                        itemDetails.DisplayValue = Strings.No_Text.ToUpper();
                                    }
                                    else
                                    {
                                        itemDetails.Value = reportedValueString;
                                        itemDetails.DisplayValue = reportedValueString;
                                    }
                                }
                            }
                            else
                            {
                                itemDetails.Value = Strings.No_Text.ToUpper();
                                itemDetails.DisplayValue = Strings.No_Text.ToUpper();
                            }
                        }
                    }
                    observation.ObservationProperties.Add(itemDetails);
                }

                observations.Add(observation);
                observationCnt++;
            }

            data.Observations = new List<Observation>( observations );

            //TO DO :: GET MAP
            FlattenedTreeHierarchyView flattenedHierarchy = endpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, data.CropYear)).GetValue(new FlattenedTreeHierarchyView());
            List<Feature> filteredLargeMapData = new List<Feature>();
            foreach (var cz in scoutingItem.CropZones)
            {
                var maps = endpoint.GetView<ItemMap>(cz.CropZoneId);
                try
                {
                    if (maps.HasValue)
                    {
                        if (maps.Value.MostRecentMapItem != null && maps.Value.MostRecentMapItem.MapData != string.Empty && maps.Value.MostRecentMapItem.MapData != "MULTIPOLYGON EMPTY")
                        {
                            var fieldName = flattenedHierarchy.Items.FirstOrDefault(c => c.CropZoneId == cz.CropZoneId).FieldName;
                            //string customarealabel = GetCustomCropZoneName(cz.Id.Id, fieldName);
                            string customareaonlylabel = GetCustomAreaLabel(cz.CropZoneId.Id, false);
                            string customarealabel = DisplayLabelWithArea(fieldName, customareaonlylabel);
                            IDictionary<string, string> columnvalues = new Dictionary<string, string>();
                            columnvalues.Add(@"LdbLabel", fieldName);
                            columnvalues.Add(@"AreaLabel", customarealabel);
                            columnvalues.Add(@"AreaOnlyLabel", customareaonlylabel);
                            Feature newfeature = new Feature(maps.Value.MostRecentMapItem.MapData, cz.CropZoneId.Id.ToString(), columnvalues);
                            filteredLargeMapData.Add(newfeature);
                        }
                    }
                }
                finally { }
            }
            var clusterValue = Math.Sqrt((scoutingItem.CropZones.Sum(x => x.AreaValue) / 81) * 43560) / 2;
            var largeMapConverted = new ConvertToBitmap(filteredLargeMapData, true, endpoint, 20.0, data.Observations, clusterValue);
            System.Drawing.Bitmap largeMap = largeMapConverted.BitMap;

            data.MapImage = largeMap;

            var report = new Landdb.Views.Secondary.Reports.Scouting.ScoutingReport(); //.StandardWorkOrder();
            report.DataSource = data;
            report.Name = Strings.ReportName_Scouting_Text;

            ////EXAMPLE OF HOW TO BIND TO A SUB REPORT
            var mainReport = report;
            var itemDetail = (Telerik.Reporting.SubReport)mainReport.Items.Find("subReport1", true)[0];
            var repSource = (InstanceReportSource)itemDetail.ReportSource;
            var subReport = (Telerik.Reporting.Report)repSource.ReportDocument;
            var odsItems = (Telerik.Reporting.ObjectDataSource)subReport.DataSource;
            odsItems.DataSource = data.Observations;

            book.Reports.Add(report);

            rs.ReportDocument = book;
            return rs;
        }

        private string DisplayLabelWithArea(string name, string customarealabel)
        {
            return name + "\r\n" + customarealabel;
        }

        private string GetCustomAreaLabel(Guid Id, bool isfieldlabel)
        {
            string stringid = Id.ToString();
            string labelname = string.Empty;
            if (isfieldlabel)
            {
                var fieldMaybe = endpoint.GetView<FieldDetailsView>(new FieldId(ApplicationEnvironment.CurrentDataSourceId, Id));
                fieldMaybe.IfValue(field => {
                    labelname = string.Empty;
                    if (field.ReportedArea != null && !string.IsNullOrWhiteSpace(field.ReportedAreaUnit))
                    {
                        labelname = DisplaySelectedAreaOnlyDecimals(field.ReportedArea.Value, field.ReportedAreaUnit);
                    }
                    else if (field.BoundaryArea != null && !string.IsNullOrWhiteSpace(field.BoundaryAreaUnit))
                    {
                        labelname = DisplaySelectedAreaOnlyDecimals(field.BoundaryArea.Value, field.BoundaryAreaUnit);
                    }
                });
            }
            else
            {
                var fieldMaybe = endpoint.GetView<CropZoneDetailsView>(new CropZoneId(ApplicationEnvironment.CurrentDataSourceId, Id));
                fieldMaybe.IfValue(field => {
                    labelname = string.Empty;
                    if (field.ReportedArea != null && !string.IsNullOrWhiteSpace(field.ReportedAreaUnit))
                    {
                        labelname = DisplaySelectedAreaOnlyDecimals(field.ReportedArea.Value, field.ReportedAreaUnit);
                    }
                    else if (field.BoundaryArea != null && !string.IsNullOrWhiteSpace(field.BoundaryAreaUnit))
                    {
                        labelname = DisplaySelectedAreaOnlyDecimals(field.BoundaryArea.Value, field.BoundaryAreaUnit);
                    }
                });
            }
            return labelname;
        }

        private string DisplaySelectedAreaOnlyDecimals(double unitvalue, string unitname)
        {
            IUnit _unit = UnitFactory.GetUnitByName(unitname);
            string decimalstring = "N" + areadecimals.ToString("N0");
            return unitvalue.ToString(decimalstring);
        }
    }
}
