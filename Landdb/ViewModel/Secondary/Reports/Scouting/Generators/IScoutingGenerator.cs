﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.Scouting.Generators
{
    public interface IScoutingGenerator
    {
        ReportSource Generate(ScoutingQueryCriteria criteria);
        string DisplayName { get; }
    }
}
