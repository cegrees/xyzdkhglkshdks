﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Production;
using Landdb.ViewModel.Secondary.Reports.Scouting.Generators;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using Telerik.Reporting;
using Telerik.Reporting.Processing;

namespace Landdb.ViewModel.Secondary.Reports.Scouting
{
    public class SingleScoutingViewModel : ViewModelBase
    {
        IClientEndpoint endpoint;
        Dispatcher dispatcher;
        SiteDataListItemViewModel _selectedScoutingApplicaiton;
        IScoutingGenerator scoutGenerator;

        public SingleScoutingViewModel(IClientEndpoint endPoint, Dispatcher dispatcher, SiteDataListItemViewModel selectedScoutingApplication)
        {
            this.endpoint = endPoint;
            this.dispatcher = dispatcher;
            this._selectedScoutingApplicaiton = selectedScoutingApplication;

            ReportGenerators = new ObservableCollection<IScoutingGenerator>();

            UpdateReport();
            AddWorkOrderGenerators();

            HideReport = new RelayCommand(this.CloseReport);
            PDFExport = new RelayCommand(this.PDF);
            //PDFExport = new RelayCommand(this.RTF);
        }

        public RelayCommand HideReport { get; private set; }
        public RelayCommand PDFExport { get; set; }
        public void CloseReport()
        {
            Messenger.Default.Send(new HideOverlayMessage() { });
        }

        public ObservableCollection<IScoutingGenerator> ReportGenerators { get; private set; }
        public ReportSource ReportSource { get; set; }
        public IScoutingGenerator SelectedReportGenerator { get { return scoutGenerator; } set { scoutGenerator = value; UpdateReport(); } }

        void AddWorkOrderGenerators()
        {
            ReportGenerators.Add(new ScoutingReportGenerator(endpoint, dispatcher));
            SelectedReportGenerator = ReportGenerators[0];
        }

        public void UpdateReport()
        {
            if (SelectedReportGenerator != null)
            {
                ScoutingQueryCriteria criteria = BuildCriteria();
                ReportSource = SelectedReportGenerator.Generate(criteria);
                RaisePropertyChanged("BingMapLayer");
                RaisePropertyChanged("ReportSource");
            }
        }

        ScoutingQueryCriteria BuildCriteria()
        {
            ScoutingQueryCriteria criteria = new ScoutingQueryCriteria();

            if (_selectedScoutingApplicaiton != null)
            {
                criteria.SelectedItem = _selectedScoutingApplicaiton;
            }

            return criteria;
        }

        public void PDF()
        {
            try { 
            if (ReportSource == null) return;
            ReportSource reportExport = ReportSource;

            ReportProcessor reportProcessor = new ReportProcessor();
            System.Collections.Hashtable deviceInfo =
            new System.Collections.Hashtable();
            RenderingResult result = reportProcessor.RenderReport("PDF", reportExport, deviceInfo);

            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "Adobe Reader|*.pdf";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;

            Nullable<bool> resultSaved = saveFileDialog1.ShowDialog();

            // Process save file dialog box results 
            if (resultSaved == true)
            {
                // Save document 
                string filename = saveFileDialog1.FileName;

                try
                {
                    using (System.IO.FileStream fs = new System.IO.FileStream(filename, System.IO.FileMode.Create))
                    {
                        fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
                    }

                    System.Diagnostics.Process.Start(filename);
                }
                catch (Exception ex)
                {
                    var exception = ex;
                }

            }
            }
            catch (Exception ex)
            {
                //do nothing
            }
        }
    }
}
