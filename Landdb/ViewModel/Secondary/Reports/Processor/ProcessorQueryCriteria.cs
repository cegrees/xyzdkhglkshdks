﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Secondary.Reports.Processor
{
    public class ProcessorQueryCriteria
    {
        public ProcessorQueryCriteria()
        {
            SelectedCropZones = new List<CropZoneId>();
            SelectedProducts = new List<Guid>();
        }
        public List<CropZoneId> SelectedCropZones { get; set; }
        public DateTime QueryStartDate { get; set; }
        public DateTime QueryEndDate { get; set; }
        public List<Guid> SelectedProducts { get; set; }
        public EntityItem SelectedEntity { get; set; }
        //public string CostType { get; set; }
    }
}
