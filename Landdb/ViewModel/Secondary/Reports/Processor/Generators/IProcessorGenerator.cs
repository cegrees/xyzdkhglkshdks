﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.Processor.Generators
{
    public interface IProcessorGenerator
    {
        ReportSource Generate(ProcessorQueryCriteria criteria);
        string DisplayName { get; }
    }
}
