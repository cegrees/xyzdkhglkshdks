﻿using AgC.UnitConversion;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.ReportModels.Processor;
using Landdb.ReportServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Landdb.Resources;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.Processor.Generators
{
    public class RedGoldReportGenerator: IProcessorGenerator
    {
        IClientEndpoint endpoint;
        int cropYear;

        public RedGoldReportGenerator(IClientEndpoint endpoint, int cropYear)
        {
            this.endpoint = endpoint;
            this.cropYear = cropYear;
        }



        public Telerik.Reporting.ReportSource Generate(ProcessorQueryCriteria criteria)
        {
            ReportBook book = new ReportBook();
            var rs = new InstanceReportSource();

            var applicationData = endpoint.GetView<CropZoneApplicationDataView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new CropZoneApplicationDataView());
            var inventory = endpoint.GetView<InventoryListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new InventoryListView());
            var flattenedHierarchy = endpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new FlattenedTreeHierarchyView());
            var masterlist = endpoint.GetMasterlistService();
            

            if (criteria.SelectedCropZones.Any())
            {
                foreach (var czId in criteria.SelectedCropZones)
                {
                    var uniqueLicList = new List<string>();
                    var data = new ProcessorData();
                    data.DataSourceName = criteria.SelectedEntity != null && criteria.SelectedEntity.Name != null ? criteria.SelectedEntity.Name : ApplicationEnvironment.CurrentDataSourceName;
                    //data.AddressDisplay = criteria.SelectedEntity != null && criteria.SelectedEntity.Name != null ? criteria.SelectedEntity.Id
                    data.Products = new List<ProcessorProduct>();
                    data.DateRangeDisplay = string.Format("{0} - {1}", criteria.QueryStartDate.ToString("d"), criteria.QueryEndDate.ToString("d"));
                    var czDetails = endpoint.GetView<CropZoneDetailsView>(czId).GetValue(new CropZoneDetailsView());
                    data.CropYear = cropYear.ToString();
                    if (czDetails.ReportedArea != null && czDetails.ReportedArea > 0)
                    {
                        data.AreaUnit = UnitFactory.GetUnitByName(czDetails.ReportedAreaUnit);
                        data.AreaValue = (decimal)czDetails.ReportedArea.Value;
                    }
                    else if (czDetails.BoundaryArea != null)
                    {
                        data.AreaUnit = UnitFactory.GetUnitByName(czDetails.BoundaryAreaUnit);
                        data.AreaValue = (decimal)czDetails.BoundaryArea.Value;
                    }
                    else
                    {
                        data.AreaUnit = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit);
                        data.AreaValue = 0;
                    }

                    var czTreeData = (from f in flattenedHierarchy.Items
                                      where f.CropZoneId == czId
                                      select f).FirstOrDefault();

                    if (czTreeData != null)
                    {
                        data.CropDisplay = masterlist.GetCropDisplay(czTreeData.CropId);
                        data.FieldDisplay = string.Format(@"{0} \ {1}", czTreeData.FieldName, czTreeData.CropZoneName);
                    }

                    var czAppData = from x in applicationData.Items
                                    where x.CropZoneId == czId &&
                                    x.EndDate >= criteria.QueryStartDate &&
                                    x.EndDate <= criteria.QueryEndDate && criteria.SelectedProducts.Contains(x.ProductId.Id) == true
                                    select x;


                    foreach (var czAppRecord in czAppData)
                    {
                        var mlp = masterlist.GetProduct(czAppRecord.ProductId);

                        if (mlp.ProductType.ToLower() == "cropprotection")
                        {
                            ProcessorProduct rlid = new ProcessorProduct();
                            rlid.StartDate = czAppRecord.StartDate.ToLocalTime();

                            var displayRate = DisplayMeasureConverter.ConvertToRateDisplayMeasure(czAppRecord.ProductId, czAppRecord.RateValue, czAppRecord.RateUnit, mlp.Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);
                            var displayTotal = DisplayMeasureConverter.ConvertToTotalDisplayMeasure(czAppRecord.ProductId, (decimal)czAppRecord.TotalProductValue, czAppRecord.TotalProductUnit, mlp.Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);

                            ApplicationView appDetails = endpoint.GetView<ApplicationView>(czAppRecord.ApplicationId).Value;
                            var coverage = appDetails.Products.FirstOrDefault(x => x.TrackingId == czAppRecord.ProductTrackingId).PercentApplied;
                            coverage = coverage == 0 ? 1 : coverage;

                            rlid.WindDirection = appDetails.Conditions != null ? appDetails.Conditions.WindDirection : string.Empty;
                            rlid.WindSpeed = appDetails.Conditions != null ? appDetails.Conditions.WindSpeedValue : null;
                            rlid.Temp = appDetails.Conditions != null ? appDetails.Conditions.TemperatureValue : null;
                            
                            rlid.RateValue = (decimal)displayRate.Value;
                            rlid.RateUnit = displayRate.Unit;
                            rlid.AppliedArea = czAppRecord.AreaValue;
                            rlid.Product = mlp.Name;
                            rlid.Manufacturer = mlp.Manufacturer;
                            rlid.TotalValue = (decimal)displayTotal.Value;
                            rlid.TotalUnit = displayTotal.Unit;
                            ProductServices service = new ProductServices(endpoint);
                            var reiPhi = service.ReturnReiPhi(czAppRecord.ProductId, czDetails.CropId);
                            rlid.Rei = reiPhi != null && reiPhi.Rei.HasValue ? reiPhi.Rei.Value.ToString() : "*";

                            ///////////////////////////////////////////////

                            var registrationNumber = mlp.RegistrationNumber;

                            if (registrationNumber == null) { rlid.EpaNumber = string.Empty; }
                            else
                            {
                                Regex epaNumberRegex = new Regex("([1-9][0-9]*)");

                                var matches = from m in epaNumberRegex.Matches(registrationNumber).Cast<Match>()
                                              select m.Value;
                                rlid.EpaNumber = string.Join("-", matches.ToArray());
                            }
                            /////////////////////////////////////////////////

                            if (mlp.ProductType.ToLower() == "cropprotection")
                            {
                                var ais = from i in mlp.ActiveIngredients
                                          select masterlist.GetActiveIngredientDisplay(new ActiveIngredientId(i.Id));

                                rlid.Analysis = string.Join(" :: ", ais);
                            }

                            var appliedProduct = appDetails.Products.FirstOrDefault(x => x.TrackingId == czAppRecord.ProductTrackingId);
                            rlid.Pests = appliedProduct.TargetPest != null ? appliedProduct.TargetPest.CommonName : string.Empty;

                            var UniqueCompanyNames = (from i in appDetails.Applicators
                                                      select i.CompanyName).Distinct().ToList();
                            var UniqueLicNumber = (from i in appDetails.Applicators
                                                      select i.LicenseNumber).Distinct().ToList();

                            uniqueLicList.AddRange(UniqueLicNumber);

                            string applicatorDisplay = string.Join(" / ", UniqueCompanyNames);
                            rlid.Applicators = applicatorDisplay;
                            
                            data.Products.Add(rlid);
                        }
                    }

                    var report = new Landdb.Views.Secondary.Reports.Processor.RedGold();
                    report.DataSource = data;
                    report.Name = data.FieldDisplay;
                    report.PageNumberingStyle = PageNumberingStyle.ResetNumberingAndCount;
                    if (data.Products.Count() > 0)
                    {
                        book.Reports.Add(report);
                        
                    }
                }
            }

            rs.ReportDocument = book;
            return rs;
        }

        public string DisplayName
        {
            get { return Strings.GeneratorName_RedGold_Text; }
        }
    }
}
