﻿using AgC.UnitConversion;
using Landdb.Client.Infrastructure;
using Landdb.Client.Services.Fertilizer;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.ReportModels.Processor;
using Landdb.ReportServices;
using Landdb.Views.Resources.Contract.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Landdb.Resources;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.Processor.Generators
{
    public class TobaccoProcessorReportGenerator : IProcessorGenerator
    {
        IClientEndpoint endpoint;
        int cropYear;

        public TobaccoProcessorReportGenerator(IClientEndpoint endpoint, int cropYear)
        {
            this.endpoint = endpoint;
            this.cropYear = cropYear;
        }

        public Telerik.Reporting.ReportSource Generate(ProcessorQueryCriteria criteria)
        {

            var fertService = new FertilizerAccumulator(endpoint, ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear);
            ReportBook book = new ReportBook();
            var rs = new InstanceReportSource();

            var applicationData = endpoint.GetView<CropZoneApplicationDataView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new CropZoneApplicationDataView());
            var inventory = endpoint.GetView<InventoryListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new InventoryListView());
            var flattenedHierarchy = endpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new FlattenedTreeHierarchyView());
            var masterlist = endpoint.GetMasterlistService();


            if (criteria.SelectedCropZones.Any())
            {
                foreach (var czId in criteria.SelectedCropZones)
                {
                    var data = new TobaccoProductionData();
                    data.DataSourceName = !string.IsNullOrWhiteSpace(criteria.SelectedEntity.Name) ? criteria.SelectedEntity.Name : ApplicationEnvironment.CurrentDataSourceName;
                    data.Products = new List<ProcessorProduct>();
                    data.DateRangeDisplay = string.Format("{0} - {1}", criteria.QueryStartDate.ToString("d"), criteria.QueryEndDate.ToString("d"));
                    data.CropYear = cropYear.ToString();

                    var czTreeData = (from f in flattenedHierarchy.Items
                                      where f.CropZoneId == czId
                                      select f).FirstOrDefault();
                    
                    if (czTreeData != null)
                    {
                        data.FarmDisplay = czTreeData.FarmName;
                        data.FieldDisplay = czTreeData.FieldName;
                        //data.FieldDisplay = string.Format(@"{0} \ {1} \ {2}", czTreeData.FarmName, czTreeData.FieldName, czTreeData.CropZoneName);
                        data.CropDisplay = masterlist.GetCropDisplay(czTreeData.CropId);
                        data.FarmFieldCz = string.Format(@"{0} \ {1} \ {2}", czTreeData.FarmName, czTreeData.FieldName, czTreeData.CropZoneName);
                    }

                    var czDetails = endpoint.GetView<CropZoneDetailsView>(czId).GetValue(new CropZoneDetailsView());
                    if (czDetails.ReportedArea != null && czDetails.ReportedArea > 0)
                    {
                        data.AreaUnit = UnitFactory.GetUnitByName(czDetails.ReportedAreaUnit);
                        data.AreaValue = (decimal)czDetails.ReportedArea.Value;
                    }
                    else if (czDetails.BoundaryArea != null)
                    {
                        data.AreaUnit = UnitFactory.GetUnitByName(czDetails.BoundaryAreaUnit);
                        data.AreaValue = (decimal)czDetails.BoundaryArea.Value;
                    }
                    else
                    {
                        data.AreaUnit = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit);
                        data.AreaValue = 0;
                    }

                    //double? _total = 0.0;

                    var fieldDetails = endpoint.GetView<FieldDetailsView>(czDetails.FieldId).GetValue(new FieldDetailsView());
                    data.CountyState = string.Format("{0} :: {1}", fieldDetails.County, fieldDetails.State);

                    if (czDetails.ProductionContracts.Count() > 0)
                    {
                        var productionContract = endpoint.GetView<Landdb.Domain.ReadModels.Contract.ProductionContractDetailsView>(czDetails.ProductionContracts[0].Id);

                        if (productionContract.HasValue)
                        {
                            data.ContractName = productionContract.Value.Name;
                            data.ContractNumber = productionContract.Value.ContractNumber;
                            data.ContractWeight = productionContract.Value.IncludedCommodityInfo.ContractedAmount + " " + productionContract.Value.IncludedCommodityInfo.ContractedAmountUnit;
                        }
                    }

                    data.FSAFarm = fieldDetails.FsaFarmNumber;
                    data.LotNumber = czDetails.SeedLotId;

                    
                    if (czDetails.AppliedVarieties != null && czDetails.AppliedVarieties.Count() > 0)
                    {
                        data.Variety = string.Join(" :: ", czDetails.AppliedVarieties.Select(x => x.ProductName));
                    }

                    var czArea = data.AreaValue;
                    var czAppData = from x in applicationData.Items
                                    where x.CropZoneId == czId &&
                                    x.EndDate >= criteria.QueryStartDate &&
                                    x.EndDate <= criteria.QueryEndDate && criteria.SelectedProducts.Contains(x.ProductId.Id) == true
                                    select x;


                    List<MiniProduct> varieties = new List<MiniProduct>(); 
                    

                    foreach (var czAppRecord in czAppData)
                    {
                        var mlp = masterlist.GetProduct(czAppRecord.ProductId);

                        if (mlp.ProductType == "Seed")
                        {
                            varieties.Add(mlp);
                        }

                        var productId = mlp.Id.ToString();

                        switch (productId)
                        {
                            case "ab1b25b1-8f19-4935-a2c6-0aa2aecc0d6f":
                                data.Harvest = czAppRecord.StartDate.ToLocalTime().ToShortDateString();
                                break;
                            case "b882e645-5ee8-4bc2-a07f-5f77407e9a46":
                                data.SeedingDate = czAppRecord.StartDate.ToLocalTime().ToShortDateString();
                                break;
                            case "3610f697-1941-4bc5-b808-e01f9d333b62":
                                data.Topping = czAppRecord.StartDate.ToLocalTime().ToShortDateString();
                                break;
                            case "5ffd36c3-86dc-4dce-895f-ee4e2cb8fb92":
                                data.Transplant = czAppRecord.StartDate.ToLocalTime().ToShortDateString();
                                break;
                            default:
                                break;
                        }

                        if (mlp.ProductType.ToLower() == "fertilizer" || mlp.ProductType.ToLower() == "cropprotection")
                        {
                            ProcessorProduct rlid = new ProcessorProduct();
                            rlid.StartDate = czAppRecord.StartDate.ToLocalTime();

                            var displayRate = DisplayMeasureConverter.ConvertToRateDisplayMeasure(czAppRecord.ProductId, czAppRecord.RateValue, czAppRecord.RateUnit, mlp.Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);
                            var displayTotal = DisplayMeasureConverter.ConvertToTotalDisplayMeasure(czAppRecord.ProductId, (decimal)czAppRecord.TotalProductValue, czAppRecord.TotalProductUnit, mlp.Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);

                            ApplicationView appDetails = endpoint.GetView<ApplicationView>(czAppRecord.ApplicationId).Value;
                            var coverage = appDetails.Products.FirstOrDefault(x => x.TrackingId == czAppRecord.ProductTrackingId).PercentApplied;
                            coverage = coverage == 0 ? 1 : coverage;
                            rlid.RateValue = (decimal)displayRate.Value;
                            rlid.RateUnit = displayRate.Unit;
                            rlid.AppliedArea = czAppRecord.AreaValue;
                            rlid.Product = mlp.Name;
                            rlid.TotalValue = (decimal)displayTotal.Value;
                            rlid.TotalUnit = displayTotal.Unit;
                            ProductServices service = new ProductServices(endpoint);
                            var reiPhi = service.ReturnReiPhi(czAppRecord.ProductId, czDetails.CropId);
                            rlid.Rei = reiPhi != null && reiPhi.Rei.HasValue ? reiPhi.Rei.Value.ToString() : "*";

                            ///////////////////////////////////////////////

                            var registrationNumber = mlp.RegistrationNumber;

                            if (registrationNumber == null) { rlid.EpaNumber = string.Empty; }
                            else
                            {
                                Regex epaNumberRegex = new Regex("([1-9][0-9]*)");

                                var matches = from m in epaNumberRegex.Matches(registrationNumber).Cast<Match>()
                                              select m.Value;
                                rlid.EpaNumber = string.Join("-", matches.ToArray());
                            }
                            /////////////////////////////////////////////////

                            if (mlp.ProductType.ToLower() == "cropprotection")
                            {
                                var ais = from i in mlp.ActiveIngredients
                                          select masterlist.GetActiveIngredientDisplay(new ActiveIngredientId(i.Id));

                                rlid.Analysis = string.Join(" :: ", ais);
                            }
                            else if (mlp.ProductType.ToLower() == "fertilizer")
                            {
                                var areaUnit = UnitFactory.GetUnitByName(czAppRecord.AreaUnit);
                                var areaMeasure = areaUnit.GetMeasure(czAppRecord.AreaValue, null);
                                string fertString = string.Empty;
                                Task fert = Task.Factory.StartNew(async () => { var fertObj = await fertService.CalculateFertilizerAnalysis(czAppRecord.ProductId, areaMeasure, (double)czAppRecord.RateValue, czAppRecord.RateUnit); fertString = fertObj.ToString(); });
                                fert.Wait();
                                rlid.Analysis = fertString;
                            }
                            var appliedProduct = appDetails.Products.FirstOrDefault(x => x.TrackingId == czAppRecord.ProductTrackingId);
                            rlid.Pests = appliedProduct.TargetPest != null ? appliedProduct.TargetPest.CommonName : string.Empty;

                            var UniqueCompanyNames = (from i in appDetails.Applicators
                                                      select i.CompanyName).Distinct().ToList();
                            string applicatorDisplay = string.Join(" / ", UniqueCompanyNames);
                            rlid.Applicators = applicatorDisplay;

                            data.Products.Add(rlid);
                        }
                    }

                    data.Variety = string.Join(" :: ", varieties.Select(x => x.Name));

                    var report = new Landdb.Views.Secondary.Reports.Processor.TobaccoProduction();
                    report.DataSource = data;
                    report.Name = data.FieldDisplay;

                    if (data.Products.Count() > 0)
                    {
                        book.Reports.Add(report);
                    }
                }
            }

            rs.ReportDocument = book;
            return rs;
        }

        public string DisplayName
        {
            get { return Strings.GeneratorName_TobaccoProduction_Text; }
        }
    }
}
