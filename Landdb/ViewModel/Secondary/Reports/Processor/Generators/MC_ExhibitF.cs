﻿using AgC.UnitConversion;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using ExportToExcel;
using Landdb.Client.Infrastructure;
using Landdb.Client.Services.Fertilizer;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.ReportModels.Processor;
using Landdb.ViewModel.Secondary.Map;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Landdb.Resources;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.Processor.Generators
{
    public class MC_ExhibitF : IProcessorGenerator
    {
        private string newFilePath;

        IClientEndpoint endPoint;
        int cropYear;

        public MC_ExhibitF(IClientEndpoint endpoint, int cropYear)
        {
            this.endPoint = endpoint;
            this.cropYear = cropYear;
        }

        public void Generate(ProcessorQueryCriteria criteria)
        {
            var data = new List<MillerCoorsData>();
            var fertService = new FertilizerAccumulator(endPoint, ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear);
            //ReportBook book = new ReportBook();
            //var rs = new InstanceReportSource();

            var applicationData = endPoint.GetView<CropZoneApplicationDataView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new CropZoneApplicationDataView());
            var flattenedHierarchy = endPoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new FlattenedTreeHierarchyView());
            var masterlist = endPoint.GetMasterlistService();

            if (criteria.SelectedCropZones.Any())
            {
                foreach (var czId in criteria.SelectedCropZones)
                {
                    var flat = flattenedHierarchy.Items.FirstOrDefault(x => x.CropZoneId == czId);
                    var mcData = new MillerCoorsData();
                    ////////////////////////////////////////////////////////////////////////
                    //Gather Data foreach 
                    ////////////////////////////////////////////////////////////////////////
                    var czDetails = endPoint.GetView<CropZoneDetailsView>(czId).Value;
                    var fertilizers = new List<CropZoneApplicationDataItem>();
                    var herbicides = new List<CropZoneApplicationDataItem>();
                    var insecticides = new List<CropZoneApplicationDataItem>();
                    var plantGrowthRegulators = new List<CropZoneApplicationDataItem>();
                    var fungicides = new List<CropZoneApplicationDataItem>();

                    var czApps = from i in applicationData.Items
                                 where i.CropZoneId == czId && i.EndDate >= criteria.QueryStartDate 
                                 && i.EndDate <= criteria.QueryEndDate 
                                 && criteria.SelectedProducts.Contains(i.ProductId.Id)
                                 select i;

                    foreach (var item in czApps)
                    {
                        var mlp = masterlist.GetProduct(item.ProductId);

                        if (!string.IsNullOrEmpty(mlp.Category))
                        {
                            var prodString = mlp.Category.ToLower();

                            if (prodString.Contains("herbicide"))
                            {
                                herbicides.Add(item);
                            }
                            if (prodString.Contains("fungicide"))
                            {
                                fungicides.Add(item);
                            }
                            if (prodString.Contains("insecticide"))
                            {
                                insecticides.Add(item);
                            }
                            if (prodString.Contains("growth regulator"))
                            {
                                plantGrowthRegulators.Add(item);
                            }
                            if (prodString.Contains("fertilizer"))
                            {
                                fertilizers.Add(item);
                            }
                        }
                        //if
                        //    var slippingProduct = mlp.Name + " :: " + mlp.Category;
                        //    break;

                        if (mlp.ProductType.ToString() == "fertilizer")
                        {
                            fertilizers.Add(item);
                        }

                    }
                    ////////////////////////////////////////////////////////////////////////
                    var area = czDetails.ReportedArea.HasValue ? czDetails.ReportedArea.Value : 0.0;
                    area = area == 0.0 && czDetails.BoundaryArea.HasValue ? czDetails.BoundaryArea.Value : area;
                    var variety = (from f in czApps
                                  where masterlist.GetProduct(f.ProductId).ProductType.ToLower() == "seed"
                                  select f).FirstOrDefault();
                    var areaMeasure = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(area, null);

                    FertilizerFormulationModel totalFertModel = new FertilizerFormulationModel();

                    Task totalFertTask = Task.Factory.StartNew(async () =>
                    {
                        totalFertModel = await fertService.CalculateFertilizerUsage(czApps.ToList(), areaMeasure);
                    });

                    totalFertTask.Wait();

                    mcData.GrowerName = string.Format("{0}::{1}::{2}::{3}", ApplicationEnvironment.CurrentDataSourceName, flat.FarmName, flat.FieldName, flat.CropZoneName);
                    var fieldDetails = endPoint.GetView<FieldDetailsView>(flat.FieldId).Value;

                    var centerLatLon = string.IsNullOrEmpty(czDetails.CenterLatLong) ? fieldDetails.CenterLatLong : czDetails.CenterLatLong;
                    var mapsettings = endPoint.GetMapSettings();
                    if (!string.IsNullOrEmpty(centerLatLon))
                    {
                        centerLatLon = DecimalDegrees.ddstring_To_DMs(centerLatLon, mapsettings.DisplayCoordinateFormat);
                    }

                    mcData.FieldLocation = centerLatLon;
                    mcData.PreviousCrop = string.Empty;  //TO DO :: GET CROP HISTORY FOR FIELD AND GET CROP
                    mcData.Acres = area;
                    mcData.Variety = variety != null ? masterlist.GetProductDisplay(variety.ProductId) : string.Empty;
                    mcData.SeedingRate = variety != null ? string.Format("{0} {1}", variety.RateValue.ToString("N2"), UnitFactory.GetUnitByName(variety.RateUnit).AbbreviatedDisplay) : string.Empty;

                    mcData.NitrogenApplied = totalFertModel.N;
                    mcData.PhosphorusApplied = totalFertModel.P;
                    mcData.PotassiumApplied = totalFertModel.K;
                    
                    var herb1 = herbicides.Any() ? herbicides[0] : null;
                    mcData.Section3HerbicideApplied = herbicides.Any() ? masterlist.GetProductDisplay(herbicides[0].ProductId) : Strings.NothingApplied_Text.ToUpper();
                    mcData.Section3HerbicideRateApplied = herb1 != null ? string.Format("{0} {1}", herb1.RateValue.ToString("N2"), UnitFactory.GetUnitByName(herb1.RateUnit).AbbreviatedDisplay) : string.Empty;
                    mcData.Section3HerbicideDateApplied = herb1 != null ? herb1.StartDate.ToShortDateString() : string.Empty;

                    var herb2 = herbicides.Count() > 1 ? herbicides[1] : null;
                    mcData.Section3_1HerbicideApplied = herbicides.Count() > 1 ? masterlist.GetProductDisplay(herbicides[1].ProductId) : Strings.NothingApplied_Text.ToUpper();
                    mcData.Section3_1HerbicideRateApplied = herb2 != null ? string.Format("{0} {1}", herb2.RateValue.ToString("N2"), UnitFactory.GetUnitByName(herb2.RateUnit).AbbreviatedDisplay) : string.Empty;
                    mcData.Section3_1HerbicideDateApplied = herb2 != null ? herb2.StartDate.ToShortDateString() : string.Empty;

                    var herb3 = herbicides.Count() > 2 ? herbicides[2] : null;
                    mcData.Section3_2HerbicideApplied = herbicides.Count() > 2 ? masterlist.GetProductDisplay(herbicides[2].ProductId) : Strings.NothingApplied_Text.ToUpper();
                    mcData.Section3_2HerbicideRateApplied = herb3 != null ? string.Format("{0} {1}", herb3.RateValue.ToString("N2"), UnitFactory.GetUnitByName(herb3.RateUnit).AbbreviatedDisplay) : string.Empty;
                    mcData.Section3_2HerbicideDateApplied = herb3 != null ? herb3.StartDate.ToShortDateString() : string.Empty;

                    var herb4 = herbicides.Count() > 3 ? herbicides[3] : null;
                    mcData.Section3_3HerbicideApplied = herb4 != null ? masterlist.GetProductDisplay(herb4.ProductId) : Strings.NothingApplied_Text.ToUpper();
                    mcData.Section3_3HerbicideRateApplied = herb4 != null ? string.Format("{0} {1}", herb4.RateValue.ToString("N2"), UnitFactory.GetUnitByName(herb4.RateUnit).AbbreviatedDisplay) : string.Empty;
                    mcData.Section3_3HerbicideDateApplied = herb4 != null ? herb4.StartDate.ToShortDateString() : string.Empty;

                    var insect1 = insecticides.Any() ? insecticides[0] : null;
                    mcData.Section4InsecticideApplied = insect1 != null ? masterlist.GetProductDisplay(insect1.ProductId) : Strings.NothingApplied_Text.ToUpper();
                    mcData.Section4InsecticideRateApplied = insect1 != null ? string.Format("{0} {1}", insect1.RateValue.ToString("N2"), UnitFactory.GetUnitByName(insect1.RateUnit).AbbreviatedDisplay) : string.Empty;
                    mcData.Section4InsecticideDateApplied = insect1 != null ? insect1.StartDate.ToShortDateString() : string.Empty;

                    var insect2 = insecticides.Count() > 1 ? insecticides[1] : null;
                    mcData.Section4_1InsecticideApplied = insect2 != null ? masterlist.GetProductDisplay(insect2.ProductId) : Strings.NothingApplied_Text.ToUpper();
                    mcData.Section4_1InsecticideRateApplied = insect2 != null ? string.Format("{0} {1}", insect2.RateValue.ToString("N2"), UnitFactory.GetUnitByName(insect2.RateUnit).AbbreviatedDisplay) : string.Empty;
                    mcData.Section4_1InsecticideDateApplied = insect2 != null ? insect2.StartDate.ToShortDateString() : string.Empty;

                    //plant growth reg
                    var growthReg = plantGrowthRegulators.Count() > 0 ? plantGrowthRegulators[0] : null;
                    mcData.PlantGrowthRegulatorApplied = growthReg != null ? masterlist.GetProductDisplay(growthReg.ProductId) : Strings.NothingApplied_Text.ToUpper();
                    mcData.PlantGrowthRegulatorRateApplied = growthReg != null ? string.Format("{0} {1}", growthReg.RateValue.ToString("N2"), UnitFactory.GetUnitByName(growthReg.RateUnit).AbbreviatedDisplay) : string.Empty;
                    mcData.PlantGrowthRegulatorDateApplied = growthReg != null ? growthReg.StartDate.ToShortDateString() : string.Empty;

                    var fungicide1 = fungicides.Any() ? fungicides[0] : null;
                    mcData.Section6FungicideApplied = fungicide1 != null ? masterlist.GetProductDisplay(fungicide1.ProductId) : Strings.NothingApplied_Text.ToUpper();
                    mcData.Section6FungicideRateApplied = fungicide1 != null ? string.Format("{0} {1}", fungicide1.RateValue.ToString("N2"), UnitFactory.GetUnitByName(fungicide1.RateUnit).AbbreviatedDisplay) : string.Empty;
                    mcData.Section6FungicideDateApplied = fungicide1 != null ? fungicide1.StartDate.ToShortDateString() : string.Empty;

                    var fungicide2 = fungicides.Count() > 1 ? fungicides[1] : null;
                    mcData.Section6_1FungicideApplied = fungicide2 != null ? masterlist.GetProductDisplay(fungicide2.ProductId) : Strings.NothingApplied_Text.ToUpper();
                    mcData.Section6_1FungicideRateApplied = fungicide2 != null ? string.Format("{0} {1}", fungicide2.RateValue.ToString("N2"), UnitFactory.GetUnitByName(fungicide2.RateUnit).AbbreviatedDisplay) : string.Empty;
                    mcData.Section6_1FungicideDateApplied = fungicide2 != null ? fungicide2.StartDate.ToShortDateString() : string.Empty;

                    mcData.TillageOperation = czDetails.Tillage;

                    if (czDetails.IrrigationInformation != null)
                    {
                        mcData.IrrigationSystemType = czDetails.IrrigationInformation.IrrigationSystem;

                    }
                    else
                    {
                            mcData.IrrigationSystemType = fieldDetails.IrrigationSystem;
                    }
                    data.Add(mcData);
                }

                System.Data.DataTable dt1 = new System.Data.DataTable();

                dt1 = ExportToExcel.CreateExcelFile.ListToDataTable(data, true);

                dt1.TableName = Strings.TableName_Exhibit1_Text;

                DataSet ds = new DataSet("excelDS");
                ds.Tables.Add(dt1);


                try
                {
                    //open up file dialog to save file....
                    //then call createexcelfile to create the excel...
                    SaveFileDialog saveFileDialog1 = new SaveFileDialog();

                    saveFileDialog1.Filter = "Excel|*.xlsx";
                    saveFileDialog1.FilterIndex = 2;
                    saveFileDialog1.RestoreDirectory = true;

                    Nullable<bool> resultSaved = saveFileDialog1.ShowDialog();

                    // Process save file dialog box results 
                    if (resultSaved == true)
                    {
                        // Save document 
                        newFilePath = saveFileDialog1.FileName;
                        CreateExcelFile.CreateExcelDocument(ds, newFilePath);
                    }

                    //now open file....
                    System.Diagnostics.Process.Start(newFilePath);
                }
                catch (Exception ex)
                {
                    return;
                }
            }
        }

        public string DisplayName
        {
            get { return Strings.ExhibitF_Name_Text; }
        }

        ReportSource IProcessorGenerator.Generate(ProcessorQueryCriteria criteria)
        {
            Generate(criteria);

            return null;
            //throw new NotImplementedException();
        }
    }
}
