﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Company;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Domain.ReadModels.Person;
using Landdb.Infrastructure;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Secondary.Reports.Processor
{
    public class ProcessorReportInfoViewModel: ViewModelBase {
        DateTime reportStartDate;
        DateTime reportEndDate;

        bool selectAll;
        bool includeCP;
        bool includeFert;
        bool includeSeed;
        bool includeService;
        bool filter;
        int cropYear;
        EntityItem selectedEntity;
        UserSettings settings;
        IClientEndpoint endPoint;
        IMasterlistService service;
        Dictionary<ProductId, InventoryListItem> usedProducts;

        public ProcessorReportInfoViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, int cropYear) {
            this.endPoint = clientEndpoint;
            this.cropYear = cropYear;

            Products = new ObservableCollection<ProductTemplateViewModel>();
            service = endPoint.GetMasterlistService();
			var inventoryListView = endPoint.GetView<InventoryListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new InventoryListView());
            usedProducts = inventoryListView.Products;

            DataSourceId dsID = new DataSourceId(ApplicationEnvironment.CurrentDataSourceId);
            var companiesMaybe = endPoint.GetView<CompanyListView>(dsID);
            var peopleMaybe = endPoint.GetView<PersonListView>(dsID);
            Entities = new List<EntityItem>();
            if (companiesMaybe.HasValue)
            {
                var entities = from c in companiesMaybe.Value.Companies
                               where !c.RemovedCropYear.HasValue || !(c.RemovedCropYear.Value <= ApplicationEnvironment.CurrentCropYear)
                               select new EntityItem() { Name = c.Name, Id = c.Id.Id, Type = "Company" };
                Entities.AddRange(entities);
            }

            if (peopleMaybe.HasValue)
            {
                var peopleEntities = from p in peopleMaybe.Value.Persons
                                     where !p.RemovedCropYear.HasValue || !(p.RemovedCropYear.Value <= ApplicationEnvironment.CurrentCropYear)
                                     select new EntityItem() { Name = p.Name, Id = p.Id.Id, Type = "Person" };
                Entities.AddRange(peopleEntities);
            }

            Entities = Entities.OrderBy(n => n.Name).ToList();
            Entities.Insert(0, new EntityItem());
            //initialize to previously used entity and date...
            settings = endPoint.GetUserSettings();
            if (settings.LastReportHeaderEntityId != null)
            {
                selectedEntity = Entities.SingleOrDefault(x => x.Id == settings.LastReportHeaderEntityId);
            }
            //////////////////////////////////////////
            Messenger.Default.Register<PropertyChangedMessage<bool>>(this, UpdateFilterHeader);

            InitializeProperties();
        }

        public List<EntityItem> Entities { get; set; }
        public EntityItem Entity
        {
            get { return selectedEntity; }
            set
            {
                selectedEntity = value;

                //save selected item to user settings
                settings.LastReportHeaderEntityId = selectedEntity.Id;
                endPoint.SaveUserSettings();
            }
        }

        public ObservableCollection<ProductTemplateViewModel> Products { get; set; }
        public string FilterHeader { get; set; }
        public bool IsFilterVisible
        {
            get
            {
                return filter;
            }
            set
            {
                filter = value;
                RaisePropertyChanged("IsFilterVisible");
            }
        }
        public bool SelectAll
        {
            get { return selectAll; }
            set
            {
                selectAll = value;
                if (!selectAll)
                {
                    foreach (var p in Products)
                    {
                        p.Checked = false;
                    }
                }
                else {
                    foreach (var p in Products)
                    {
                        p.Checked = true;
                    }
                }

                FilterHeader = $"{Strings.FilterHeader_IncludedProducts_Text}: " + Products.Where(x => x.Checked == true).Count();
                RaisePropertyChanged("FilterHeader");
                RaisePropertyChanged("Products");
            }
        }
        public bool IncludeCP 
        { 
            get 
            { 
                return includeCP; 
            }
            set
            {
                includeCP = value;
                ProductsOfType("CropProtection", includeCP);
            }
        }
        public bool IncludeFert
        {
            get
            {
                return includeFert;
            }
            set
            {
                includeFert = value;
                ProductsOfType("Fertilizer", includeFert);
            }
        }
        public bool IncludeSeed
        {
            get
            {
                return includeSeed;
            }
            set
            {
                includeSeed = value;
                ProductsOfType("Seed", includeSeed);
            }
        }

        public bool IncludeService
        {
            get
            {
                return includeService;
            }
            set
            {
                includeService = value;
                ProductsOfType("Service", includeService);
            }
        }

        public DateTime ReportStartDate {
            get { return reportStartDate; }
            set
            {
                reportStartDate = value;

                if (reportStartDate != settings.LastUsedReportStartDate)
                {
                    settings.LastUsedReportStartDate = reportStartDate;
                    endPoint.SaveUserSettings();
                }
                RaisePropertyChanged("ReportStartDate");
            }
        }

        public DateTime ReportEndDate {
            get { return reportEndDate; }
            set {
                reportEndDate = value;

                RaisePropertyChanged("ReportEndDate");
            }
        }

        public void InitializeProperties()
        {
            if (settings.LastUsedReportStartDate != null && settings.LastUsedReportStartDate != new DateTime())
            {
                ReportStartDate = settings.LastUsedReportStartDate;
            }
            else 
            { 
                ReportStartDate = new DateTime(cropYear, 1, 1); 
            }

            ReportEndDate = DateTime.Now;


            Products = new ObservableCollection<ProductTemplateViewModel>();
            IncludeCP = true;
            IncludeFert = true;
            IncludeSeed = true;
            IncludeService = true;
            SelectAll = true;
            IsFilterVisible = false;
        }

        public void ProductsOfType(string prodType, bool add)
        {
            if (!add)
            {
                var products = Products.ToList();
                var cps = from p in products
                          where p.Type == prodType
                          select p;

                foreach (var c in cps)
                {
                    Products.Remove(c);
                }

                RaisePropertyChanged("Products");
            }
            else
            {
                var prodId = new ProductId(new Guid("dbcd09b0-1d7c-43fc-baf3-d8c7e9f5e024"));
                var prod = service.GetProduct(prodId);
                var productsDetails = (from p in usedProducts
                                       where service.GetProduct(p.Key) != null && service.GetProduct(p.Key).ProductType == prodType
                                       select service.GetProduct(p.Key)).ToList();

                var productsToInclude = (from i in productsDetails
                                         where !Products.Any(x => x.Id == i.Id)
                                         select new ProductTemplateViewModel() { Name = i.Name, Manufacturer = i.Manufacturer, Checked = SelectAll, Id = i.Id, Type = i.ProductType }).ToList();

                foreach (var p in productsToInclude)
                {
                    Products.Add(p);
                }

                Products = new ObservableCollection<ProductTemplateViewModel>(Products.OrderBy(x => x.Name));
                RaisePropertyChanged("Products");
            }

            FilterHeader = $"{Strings.FilterHeader_IncludedProducts_Text}: " + Products.Where(x => x.Checked == true).Count();
            RaisePropertyChanged("FilterHeader");
        }

        void UpdateFilterHeader(PropertyChangedMessage<bool> _checked)
        {
            if (_checked.PropertyName == "Checked")
            {
                FilterHeader = $"{Strings.FilterHeader_IncludedProducts_Text}: " + Products.Where(x => x.Checked == true).Count();
                RaisePropertyChanged("FilterHeader");
             }
        }

    }

    public class ProductTemplateViewModel : ViewModelBase
    {
        bool _checked;
        public ProductTemplateViewModel()
        {

        }

        public bool Checked {
            get { return _checked; } 
            set 
            { 
                
                _checked = value;
                RaisePropertyChanged("Checked", true, _checked, true); 
            } 
        }
        public string Name { get; set; }
        public string Manufacturer { get; set; }
        public Guid Id { get; set; }
        public string Type { get; set; }
    }

    public class EntityItem
    {
        public string Name { get; set; }
        public Guid Id { get; set; }
        public string Type { get; set; }
        public override string ToString()
        {
            return this.Name;
        }
    }
}