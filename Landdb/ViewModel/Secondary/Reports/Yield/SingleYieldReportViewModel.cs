﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Secondary.Reports.Yield.Generators;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using Telerik.Reporting;
using Telerik.Reporting.Processing;

namespace Landdb.ViewModel.Secondary.Reports.Yield
{
    public class SingleYieldReportViewModel : ViewModelBase
    {
        IClientEndpoint endpoint;

        IYieldReportGenerator yieldGenerator;

        public SingleYieldReportViewModel(IClientEndpoint endPoint, Dispatcher dispatcher, IYieldReportGenerator selectedYieldReport)
        {
            ReportGenerators = new ObservableCollection<IYieldReportGenerator>();
            endpoint = endPoint;

            AddYieldGenerators(selectedYieldReport);

			//UpdateReport();

			HideReport = new RelayCommand(this.CloseReport);
            PDFExport = new RelayCommand(this.PDF);
            //PDFExport = new RelayCommand(this.RTF);
        }

        public RelayCommand HideReport { get; private set; }
        public RelayCommand PDFExport { get; set; }
        public void CloseReport()
        {
            Messenger.Default.Send(new HideOverlayMessage() { });
        }

        public ObservableCollection<IYieldReportGenerator> ReportGenerators { get; private set; }
        public ReportSource ReportSource { get; set; }
        public IYieldReportGenerator SelectedReportGenerator { get { return yieldGenerator; } set { yieldGenerator = value; UpdateReport(); } }

        void AddYieldGenerators(IYieldReportGenerator selectedYieldReport)
        {
            ReportGenerators.Add(selectedYieldReport);
            SelectedReportGenerator = ReportGenerators[0];
            //ReportGenerators.Add(new WorkOrderWithMapGeneratorAndLabels(endpoint, dispatcher, ApplicationEnvironment.CurrentCropYear));
        }

        public void UpdateReport()
        {
            if (SelectedReportGenerator != null)
            {
                YieldQueryCriteria criteria = BuildCriteria();
                ReportSource = SelectedReportGenerator.Generate(criteria);
                RaisePropertyChanged("ReportSource");
            }
        }

        YieldQueryCriteria BuildCriteria()
        {
            YieldQueryCriteria criteria = new YieldQueryCriteria();

            //if (workOrderId != null)
            //{
            //    criteria.SelectedWorkOrderId = workOrderId;
            //}

            return criteria;
        }

        public void PDF()
        {
            try { 
            if (ReportSource == null) return;
            ReportSource reportExport = ReportSource;

            ReportProcessor reportProcessor = new ReportProcessor();
            System.Collections.Hashtable deviceInfo =
            new System.Collections.Hashtable();
            RenderingResult result = reportProcessor.RenderReport("PDF", reportExport, deviceInfo);

            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "Adobe Reader|*.pdf";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;

            Nullable<bool> resultSaved = saveFileDialog1.ShowDialog();

            // Process save file dialog box results 
            if (resultSaved == true)
            {
                // Save document 
                string filename = saveFileDialog1.FileName;

                try
                {
                    using (System.IO.FileStream fs = new System.IO.FileStream(filename, System.IO.FileMode.Create))
                    {
                        fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
                    }

                    System.Diagnostics.Process.Start(filename);
                }
                catch (Exception ex)
                {
                    var exception = ex;
                }

            }
            }
            catch (Exception ex)
            {
                //do nothing
            }
        }
    }
}
