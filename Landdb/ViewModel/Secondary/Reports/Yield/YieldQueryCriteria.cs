﻿using Landdb.ViewModel.Secondary.Reports.AppliedProduct;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Secondary.Reports.Yield
{
    public class YieldQueryCriteria
    {
        public YieldQueryCriteria()
        {
            SelectedCropZones = new List<CropZoneId>();
        }

        public List<CropZoneId> SelectedCropZones { get; set; }
        public DateTime ReportStartDate { get; set; }
        public DateTime ReportEndDate { get; set; }
        public EntityItem SelectedEntity { get; set; }
        public string CostType { get; set; }
    }
}
