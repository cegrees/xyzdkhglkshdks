﻿using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Domain.ReadModels.Yield;
using Landdb.Infrastructure;
using Landdb.ReportModels.Yield;
using Landdb.ViewModel.Yield;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using Landdb.Resources;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.Yield.Generators
{
    class CommoditySummaryReportGenerator : IYieldReportGenerator
    {

        readonly IClientEndpoint clientEndpoint;
        readonly Dispatcher dispatcher;

        readonly int cropYear;

        public CommoditySummaryReportGenerator(IClientEndpoint clientEndpoint, Dispatcher dispatcher, int cropYear)
        {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            this.cropYear = cropYear;
        }

        public Telerik.Reporting.ReportSource Generate(YieldQueryCriteria criteria)
        {
            ReportBook book = new ReportBook();
            var rs = new InstanceReportSource();

            var dsId = new DataSourceId(ApplicationEnvironment.CurrentDataSourceId);
            var currentCropYearId = new CropYearId(dsId.Id, cropYear);

            var dataSourceName = ApplicationEnvironment.CurrentDataSourceName;
            var flattenedHierarchyView = clientEndpoint.GetView<FlattenedTreeHierarchyView>(currentCropYearId).GetValue(new FlattenedTreeHierarchyView());
            var CommodityList = clientEndpoint.GetView<CommodityListView>(currentCropYearId).GetValue(new CommodityListView());

            List<CommoditySummaryData> CommoditySummaryData = new List<CommoditySummaryData>();

            foreach (var commodity in CommodityList.Commodities)
            {
                var detailsViewKey = new CommodityDetailsViewKey(currentCropYearId, commodity.CropId.Id, commodity.CommodityDescription);
                var commodityDetails = clientEndpoint.GetView<CommodityDetailsView>(detailsViewKey);

                if (commodityDetails.HasValue)
                {
                    BuildDetailsFromProjection(commodityDetails.Value);

                    foreach (var calcUnit in CommoditySummary.UnitSummaries)
                    {
                        List<Guid> loadIDs = new List<Guid>();
                        HarvestedLoads.ForEach(l => {
                            if (!loadIDs.Contains(l.LoadId.Id) && l.QuantityUnit == calcUnit.Key)
                            {
                                loadIDs.Add(l.LoadId.Id);
                            }
                        });
                        SoldLoads.ForEach(s =>
                        {
                            if (!loadIDs.Contains(s.LoadId.Id) && s.QuantityUnit == calcUnit.Key)
                            {
                                loadIDs.Add(s.LoadId.Id);
                            }
                        });

                        var summaryData = new CommoditySummaryData();
                        summaryData.CropYear = cropYear;
                        summaryData.DataSourceName = dataSourceName;
                        summaryData.Commodity = commodity.CropName;
                        summaryData.CommodityID = commodity.CropId.Id;
                        var loadCount = loadIDs.Count();
                        summaryData.LoadsDisplay = string.Format("{0} {1}", loadCount, "LOADS");
                        summaryData.HarvestedValue = calcUnit.Value.HarvestedGrossQuantity;
                        summaryData.HarvestedUnit = calcUnit.Key.AbbreviatedDisplay;
                        summaryData.SoldValue = calcUnit.Value.TotalSoldQuantity;
                        summaryData.SoldUnit = calcUnit.Key.AbbreviatedDisplay;
                        summaryData.StoredValue = calcUnit.Value.TotalUnsoldQuantity;
                        summaryData.StoredUnit = calcUnit.Key.AbbreviatedDisplay;

                        CommoditySummaryData.Add(summaryData);
                    }
                }
            }

            if (CommoditySummaryData.Count() > 0)
            {
                CommoditySummaryData = CommoditySummaryData.OrderBy(x => x.Commodity).ToList();

                var report = new Landdb.Views.Secondary.Reports.Yield.CommoditySummaryReport();
                report.DataSource = CommoditySummaryData;
                report.Name = Strings.ReportName_CommoditySummary_Text;

                book.Reports.Add(report);
            }

            rs.ReportDocument = book;

            return rs;
        }

        public string DisplayName
        {
            get { return Strings.GeneratorName_CommoditySummary_Text; }
        }

        public string CropName { get; set; }
        public string CommodityDescription { get; set; }

        public List<IncludedLoadViewModel> HarvestedLoads { get; set; }
        public List<IncludedLoadViewModel> StoredLoads { get; set; }
        public List<IncludedLoadViewModel> SoldLoads { get; set; }

        public CommoditySummaryViewModel CommoditySummary { get; set; }

        private void BuildDetailsFromProjection(CommodityDetailsView commodityDetailsView)
        {
            CropName = commodityDetailsView.CropName;
            CommodityDescription = commodityDetailsView.CommodityDescription;

            HarvestedLoads = new List<IncludedLoadViewModel>();
            StoredLoads = new List<IncludedLoadViewModel>();
            SoldLoads = new List<IncludedLoadViewModel>();

            var dsId = new DataSourceId(ApplicationEnvironment.CurrentDataSourceId);
            var currentCropYearId = new CropYearId(dsId.Id, cropYear);

            var flattenedTreeView = clientEndpoint.GetView<FlattenedTreeHierarchyView>(currentCropYearId).GetValue(() => null);
			var locationDictionary = YieldLocationListItemViewModel.GetFlattenedListItemsForCropYear(clientEndpoint, currentCropYearId, false)
				.ToDictionary(x => x.Id, x => x.FullDisplay);

			CommoditySummary = new CommoditySummaryViewModel(clientEndpoint, dispatcher, currentCropYearId, commodityDetailsView.CropId, commodityDetailsView.CommodityDescription);

            if (CommoditySummary.UnitSummaries.Any())
            {
                commodityDetailsView.HarvestedLoads.OrderByDescending(x => x.StartTime).ThenByDescending(x => x.LoadNumber).ForEach(load =>
                {
                    HarvestedLoads.AddRange(IncludedLoadViewModel.GetListFromIncludedLoad(flattenedTreeView, locationDictionary, load));
                });

                commodityDetailsView.SoldLoads.OrderByDescending(x => x.StartTime).ThenByDescending(x => x.LoadNumber).ForEach(load =>
                {
                    SoldLoads.AddRange(IncludedLoadViewModel.GetListFromIncludedLoad(flattenedTreeView, locationDictionary, load));
                });

                commodityDetailsView.StoredLoads.OrderByDescending(x => x.StartTime).ThenByDescending(x => x.LoadNumber).ForEach(load =>
                {
                    StoredLoads.AddRange(IncludedLoadViewModel.GetListFromIncludedLoad(flattenedTreeView, locationDictionary, load));
                });
            }
        }
    }
}
