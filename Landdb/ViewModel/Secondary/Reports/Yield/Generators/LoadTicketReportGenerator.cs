﻿using AgC.UnitConversion;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.ReportModels.Yield;
using Landdb.ViewModel.Secondary.YieldOperation;
using Landdb.Views.Secondary.Reports.Yield;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Landdb.Resources;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.Yield.Generators
{
    public class LoadTicketReportGenerator : IYieldReportGenerator
    {
        LoadViewModel _model;
        List<CropZoneId> _cropZones = new List<CropZoneId>();
        CropId _cropId;
        IClientEndpoint _endPoint;

		public LoadTicketReportGenerator(LoadViewModel model, List<CropZoneId> cropZones, CropId cropId, IClientEndpoint endPoint) {
			this._model = model;
			this._cropId = cropId;
			this._endPoint = endPoint;

			if (cropZones != null) { this._cropZones.AddRange(cropZones); }
		}

		public string DisplayName
        {
            get
            {
                return Strings.GeneratorName_LoadTicket_Text;
            }
        }

        public ReportSource Generate(YieldQueryCriteria criteria)
        {
            ReportBook book = new ReportBook();
            var rs = new InstanceReportSource();

            if (_model != null)
            {
                var reportModel = new LoadData();
                reportModel.DataSourceName = ApplicationEnvironment.CurrentDataSourceName;
                reportModel.LoadNumber = _model.LoadNumber;
                reportModel.LoadDate = _model.StartDateTime;
                reportModel.Commodity = !string.IsNullOrEmpty(_model.CommodityDescription) ? _model.CommodityDescription : string.Empty;
                reportModel.Destination = _model.SelectedDestinationLocation?.FullDisplay;
                reportModel.Truck = _model.SelectedTruck?.BasicInfoViewModel.Name;
                reportModel.Driver = _model.SelectedDriver?.Name;
                reportModel.MoisturePercent = _model.QuantityInfo.InitialMoisturePercentage;
                reportModel.MoistureShrinkDisplay = _model.QuantityInfo.StandardMoistureShrinkQuantityDisplayText;
                reportModel.AdditionalShrinkDisplay = _model.QuantityInfo.AdjustmentShrinkQuantityDisplayText;
                reportModel.GrossWeightDisplay = _model.QuantityInfo.TruckGrossWeightDisplayText;
                reportModel.TareWeightDisplay = _model.QuantityInfo.TruckTareWeightDisplayText;
                reportModel.NetWeightDisplay = _model.QuantityInfo.TruckNetWeightDisplayText;
                reportModel.FinalWeightDisplay = _model.QuantityInfo.FinalQuantityDisplayText;
                reportModel.Notes = _model.Notes;
                reportModel.TotalShrinkDisplay = _model.QuantityInfo.TotalShrinkQuantityDisplayText;
                
                foreach(var prop in _model.LoadPropertySets)
                {
                    foreach(var innerProperty in prop.LoadProperties)
                    {
                        var newProperty = new LoadPropertyItem();
                        newProperty.Name = innerProperty.Name;
                        newProperty.Value = innerProperty.DisplayText;
                        reportModel.LoadProperties.Add(newProperty);
                        
                    }
                }


                var flattenedTree = _endPoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear)).GetValue(new FlattenedTreeHierarchyView()); ;
                foreach (var czId in _cropZones)
                {
                    var czItem = flattenedTree.Items.FirstOrDefault(x => x.CropZoneId == czId);
                    var includedFieldItem = new LoadFieldItem();
                    var czDetailsMaybe = _endPoint.GetView<CropZoneDetailsView>(czId);

                    if (czItem.FieldId != null)
                    {
                        var fieldDetails = _endPoint.GetView<FieldDetailsView>(czItem.FieldId).Value;
                        includedFieldItem.FSAFarm = fieldDetails.FsaFarmNumber;
                        includedFieldItem.FSAField = fieldDetails.FsaFieldNumber;
                        includedFieldItem.FSATract = fieldDetails.FsaTractNumber;
                        includedFieldItem.Field = fieldDetails.FieldNameByCropYear[ApplicationEnvironment.CurrentCropYear]; //.Name;
                        includedFieldItem.Farm = czItem.FarmName;
                        includedFieldItem.Field = czItem.FieldName;
                        includedFieldItem.Crop = czItem.CropName;
                        var area = czDetailsMaybe.Value.ReportedArea != null ? czDetailsMaybe.Value.ReportedArea.Value : czDetailsMaybe.Value.BoundaryArea != null ? czDetailsMaybe.Value.BoundaryArea.Value : 0.0;
                        var areaUnit = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit);
                        includedFieldItem.Area = string.Format("{0} {1}", area.ToString("N2"), areaUnit.AbbreviatedDisplay);
                        reportModel.IncludedFields.Add(includedFieldItem);
                    }
                }

                LoadTicket report = new LoadTicket();
                report.DataSource = reportModel;

                book.Reports.Add(report);
            }

            rs.ReportDocument = book;
            return rs;
        }
    }
}
