﻿using AgC.UnitConversion;
using ExportToExcel;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Contract;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Domain.ReadModels.Invoice;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Domain.ReadModels.Yield;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ReportModels.Yield;
using Landdb.ViewModel.Fields.FieldDetails;
using Landdb.ViewModel.Yield;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using Telerik.Reporting;
using ClosedXML;
using ClosedXML.Excel;
using Landdb.Domain.ReadModels.YieldLocation;
using Landdb.Resources;

namespace Landdb.ViewModel.Secondary.Reports.Yield.Generators
{
    public class ContributionMarginReportGenerator : IYieldReportGenerator
    {
        readonly IClientEndpoint clientEndpoint;
        readonly Dispatcher dispatcher;

        readonly int cropYear;
        bool isTransposed;

        public ContributionMarginReportGenerator(IClientEndpoint clientEndpoint, Dispatcher dispatcher, int cropYear, bool isTransposed)
        {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            this.cropYear = cropYear;
            this.isTransposed = isTransposed;
        }

        public string DisplayName
        {
            get
            {
                return isTransposed ? Strings.GeneratorName_ContributionMarginVertical_Text : Strings.GeneratorName_ContributionMarginHorizontal_Text;
            }
        }

        public ReportSource Generate(YieldQueryCriteria criteria)
        {
            var currentCropYearId = new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear);

            var masterlist = clientEndpoint.GetMasterlistService();
            var cropZoneApplications = clientEndpoint.GetView<CropZoneApplicationDataView>(currentCropYearId).GetValue(new CropZoneApplicationDataView()).Items;
            var inventoryList = clientEndpoint.GetView<InventoryListView>(currentCropYearId).GetValue(new InventoryListView()).Products;
            var flattenedHierarchyView = clientEndpoint.GetView<FlattenedTreeHierarchyView>(currentCropYearId).GetValue(new FlattenedTreeHierarchyView());
            var loadList = clientEndpoint.GetView<CropZoneLoadDataView>(currentCropYearId).GetValue(new CropZoneLoadDataView()).Items;
            var commodityList = clientEndpoint.GetView<CommodityListView>(currentCropYearId).GetValue(new CommodityListView()).Commodities;
			var yieldLocations = clientEndpoint.GetView<YieldLocationListView>(new DataSourceId(currentCropYearId.DataSourceId)).GetValue(() => new YieldLocationListView()).YieldLocations;

            //get unique list of crops from selected crop zones and then roll this stuff into CommoditySummaryViewModel....
            if (criteria.SelectedCropZones.Any())
            {
                var wb = new XLWorkbook();
                var ws = wb.AddWorksheet("Contribution Margin");
                /////////////////////////////////////////////////////////////////////////////
                //HEADERS
                ////////////////////////////////////////////////////////////////////////////
                ws.Cell("A1").Value = Strings.ExcelWorksheet_Header_Farm_Text;
                ws.Cell("B1").Value = Strings.ExcelWorksheet_Header_Field_Text;
                ws.Cell("C1").Value = Strings.ExcelWorksheet_Header_CropZone_Text;
                ws.Cell("D1").Value = Strings.ExcelWorksheet_Header_Crop_Text;
                ws.Cell("E1").Value = Strings.ExcelWorksheet_Header_CropYear_Text;
                ws.Cell("F1").Value = Strings.ExcelWorksheet_Header_CropZoneArea_Text;
                ws.Cell("G1").Value = Strings.ExcelWorksheet_Header_ContributionMarginTotal_Text;
                ws.Cell("H1").Value = Strings.ExcelWorksheet_Header_ContributionMarginPerArea_Text;
                ws.Cell("I1").Value = Strings.ExcelWorksheet_Header_TotalYield_Text;
                ws.Cell("J1").Value = Strings.ExcelWorksheet_Header_YieldPerArea_Text;
                ws.Cell("K1").Value = Strings.ExcelWorksheet_Header_AverageSalePrice_Text;
                ws.Cell("L1").Value = Strings.ExcelWorksheet_Header_GrowerShares_Text;
                ws.Cell("M1").Value = Strings.ExcelWorksheet_Header_GrowerShare_Text;
                ws.Cell("N1").Value = Strings.ExcelWorksheet_Header_GrowerRevenue_Text;
                ws.Cell("O1").Value = Strings.ExcelWorksheet_Header_GrowerRevenuePerArea_Text;
                ws.Cell("P1").Value = Strings.ExcelWorksheet_Header_GrowerSeedCosts_Text;
                ws.Cell("Q1").Value = Strings.ExcelWorksheet_Header_GrowerSeedCostPerArea_Text;

                //new
                ws.Cell("R1").Value = Strings.ExcelWorksheet_Header_BreakevenCostPerUnit_Text;
                ws.Cell("S1").Value = Strings.ExcelWorksheet_Header_BreakevenYieldPerArea_Text;
                //increment
                ws.Cell("T1").Value = Strings.ExcelWorksheet_Header_GrowerFertilizerCosts_Text;
                ws.Cell("U1").Value = Strings.ExcelWorksheet_Header_GrowerFertilizerCostPerArea_Text;
                ws.Cell("V1").Value = Strings.ExcelWorksheet_Header_GrowerCropProtectionCosts_Text;
                ws.Cell("W1").Value = Strings.ExcelWorksheet_Header_GrowerCropProtectionCostPerArea_Text;
                ws.Cell("X1").Value = Strings.ExcelWorksheet_Header_GrowerServicesCosts_Text;
                ws.Cell("Y1").Value = Strings.ExcelWorksheet_Header_GrowerServicesCostsPerArea_TExt;
                ws.Cell("Z1").Value = Strings.ExcelWorksheet_Header_GrowerTotalInputCosts_Text;
                ws.Cell("AA1").Value = Strings.ExcelWorksheet_Header_GrowerTotalInputCostPerArea_Text;
                ws.Cell("AB1").Value = Strings.ExcelWorksheet_Header_CashRentTotal_Text;
                ws.Cell("AC1").Value = Strings.ExcelWorksheet_Header_CashRentPerArea_Text;
                ws.Cell("AD1").Value = Strings.ExcelWorksheet_Header_SeedVarietyPlantingDate_Text;
                var planHeaders = ws.Range("A1:AD1");
                planHeaders.Style.Font.Bold = true;

                ////////////////////////////////////////////////////////////////////////////


                List<CropPrice> priceContainer = new List<CropPrice>();
                foreach (var commodity in commodityList)
                {
                    CommoditySummaryViewModel csViewModel = new CommoditySummaryViewModel(clientEndpoint, dispatcher, currentCropYearId, commodity.CropId, commodity.CommodityDescription);

                    foreach (var summary in csViewModel.UnitSummaries)
                    {
                        priceContainer.Add(new CropPrice()
                        {
                            CropID = commodity.CropId,
                            CommodityDescription = commodity.CommodityDescription,
                            AvgPrice = summary.Value.GrowerAverageRevenue,
                            AvgPriceUnit = summary.Value.QuantityUnit
                        });
                    }
                }

                var czCounter = 2;
                foreach (var czId in criteria.SelectedCropZones)
                {
                    //var contributionMargin = new ContributionMarginData();
                    var czDetails = clientEndpoint.GetView<CropZoneDetailsView>(czId).Value;
                    var czItem = flattenedHierarchyView.Items.FirstOrDefault(p => p.CropZoneId == czId);
                    var contract = clientEndpoint.GetView<CropZoneRentContractView>(currentCropYearId);

                    ws.Cell("A" + czCounter.ToString()).Value = czItem.FarmName;
                    ws.Cell("B" + czCounter.ToString()).Value = czItem.FieldName;
                    ws.Cell("C" + czCounter.ToString()).Value = czDetails.Name;
                    ws.Cell("D" + czCounter.ToString()).Value = masterlist.GetCropDisplay(czItem.CropId);
                    var czArea = (decimal)(czDetails.ReportedArea != null ? czDetails.ReportedArea : czDetails.BoundaryArea != null ? czDetails.BoundaryArea : 0.0);
                    ws.Cell("E" + czCounter.ToString()).Value = cropYear;
                    ws.Cell("F" + czCounter.ToString()).Value = czArea;

                    var productUnits = clientEndpoint.GetProductUnitResolver();
                    var czAppItems = cropZoneApplications.Where(x => x.CropZoneId == czId);

                    decimal totalInputs = 0m;
                    decimal cpCosts = 0m;
                    decimal fertCosts = 0m;
                    decimal seedCosts = 0m;
                    decimal serviceCosts = 0m;
                    List<string> varieties = new List<string>();
                    foreach (var item in czAppItems)
                    {
                        var mlp = masterlist.GetProduct(item.ProductId);
                        var appDetails = clientEndpoint.GetView<ApplicationView>(item.ApplicationId).Value;
                        SharedYieldMethods methods = new SharedYieldMethods();
                        double cost = 0.0;

                        switch (criteria.CostType)
                        {
                            case "Average":
                                cost = methods.GetAvg(item, inventoryList, mlp) * item.GrowerShare; ;
                                break;
                            case "Invoice":

                                //get invoice
                                var source = appDetails.Sources.Where(x => x.DocumentType == "Invoice");
                                double invoiceCostPerUnit = 0.0;
                                int cnt = 0;
                                if (source.Count() > 0)
                                {
                                    foreach (var src in source)
                                    {
                                        var invoiceDetailmaybe = clientEndpoint.GetView<InvoiceView>(src.Identity as InvoiceId);
                                        if (invoiceDetailmaybe.HasValue)
                                        {
                                            var invoiceDetail = invoiceDetailmaybe.Value;
                                            var invoiceProducts = (from p in invoiceDetail.Products
                                                                   where p.ProductId == item.ProductId
                                                                   select p).ToList();
                                            var containsKey = inventoryList.ContainsKey(item.ProductId);

                                            if (containsKey)
                                            {
                                                var product = inventoryList[item.ProductId];
                                                var fallbackUnit = invoiceProducts.Any() ? UnitFactory.GetUnitByName(invoiceProducts[0].TotalProductUnit) : UnitFactory.GetPackageSafeUnit(item.TotalProductUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                                                IUnit unit = !string.IsNullOrEmpty(product.AveragePriceUnit) ? UnitFactory.GetPackageSafeUnit(inventoryList[item.ProductId].AveragePriceUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit) : fallbackUnit;  //.GetUnitByName(inventory.Products[czAppRecord.ProductId].AveragePriceUnit) : fallbackUnit;

                                                foreach (var prod in invoiceProducts)
                                                {
                                                    var measurePreConvertUnit = UnitFactory.GetPackageSafeUnit(prod.TotalProductUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                                                    var measurePreConvert = measurePreConvertUnit.GetMeasure((double)prod.TotalProductValue, mlp.Density);
                                                    var canConvert = measurePreConvert.CanConvertTo(unit);
                                                    var convertedMeasure = measurePreConvert.CanConvertTo(unit) && measurePreConvert.Unit != unit ? unit.GetMeasure(measurePreConvert.GetValueAs(unit)) : measurePreConvert;
                                                    invoiceCostPerUnit += convertedMeasure.Value != 0 ? (double)(prod.TotalCost / (decimal)convertedMeasure.Value) : (double)prod.TotalCost;
                                                    cnt++;
                                                }
                                            }
                                        }
                                    }

                                    if (inventoryList.ContainsKey(item.ProductId))
                                    {
                                        try
                                        {
                                            var totalMeasure = UnitFactory.GetUnitByName(item.TotalProductUnit).GetMeasure(item.TotalProductValue, masterlist.GetProduct(item.ProductId).Density);
                                            var avgPriceUnit = string.IsNullOrEmpty(inventoryList[item.ProductId].AveragePriceUnit) ? null : UnitFactory.GetUnitByName(inventoryList[item.ProductId].AveragePriceUnit);
                                            var convertedTotalMeasureValue = totalMeasure.CanConvertTo(avgPriceUnit) ? totalMeasure.GetValueAs(avgPriceUnit) : totalMeasure.Value;
                                            var totalInvCost = invoiceCostPerUnit != 0.0 ? invoiceCostPerUnit / cnt * convertedTotalMeasureValue : methods.GetAvg(item, inventoryList, mlp);
                                            cost = (totalInvCost * item.GrowerShare);
                                        }
                                        catch
                                        {
                                            //do nothing
                                        }
                                    }
                                }
                                else
                                {
                                    var avgCost = methods.GetAvg(item, inventoryList, mlp);
                                    cost = (avgCost * item.GrowerShare);
                                }
                                break;
                            case "Specific":
                                var totalSpecific = methods.GetSpecificCost(item, appDetails, mlp);
                                var appProduct = appDetails.Products.SingleOrDefault(x => x.TrackingId == item.ProductTrackingId);
                                if (appProduct != null && !string.IsNullOrEmpty(appProduct.SpecificCostUnit))
                                {
                                    cost = (totalSpecific * item.GrowerShare);
                                }
                                else
                                {
                                    cost += (methods.GetAvg(item, inventoryList, mlp) * item.GrowerShare);
                                }
                                break;
                            default:
                                break;

                        }


                        totalInputs += double.IsNaN(cost) ? 0m : (decimal)cost;

                        switch (mlp.ProductType)
                        {
                            case "Seed":
                                seedCosts += double.IsNaN(cost) ? 0m : (decimal)cost;
                                varieties.Add(string.Format("{0} ({1})", mlp.Name, item.StartDate.ToShortDateString()));
                                break;
                            case "CropProtection":
                                cpCosts += double.IsNaN(cost) ? 0m : (decimal)cost;
                                break;
                            case "Fertilizer":
                                fertCosts += double.IsNaN(cost) ? 0m : (decimal)cost;
                                break;
                            case "Service":
                                serviceCosts += double.IsNaN(cost) ? 0m : (decimal)cost;
                                break;
                        }

                    }
                    ws.Cell("P" + czCounter.ToString()).Value = seedCosts;
                    ws.Cell("Q" + czCounter.ToString()).Value = czArea != 0 ? seedCosts / czArea : 0;

                    //add in breakeven stuff


                    ws.Cell("T" + czCounter.ToString()).Value = fertCosts;
                    ws.Cell("U" + czCounter.ToString()).Value = czArea != 0 ? fertCosts / czArea : 0;

                    ws.Cell("V" + czCounter.ToString()).Value = cpCosts;
                    ws.Cell("W" + czCounter.ToString()).Value = czArea != 0 ? cpCosts / czArea : 0;
                    ws.Cell("X" + czCounter.ToString()).Value = serviceCosts;
                    ws.Cell("Y" + czCounter.ToString()).Value = czArea != 0 ? serviceCosts / czArea : 0;
                    ws.Cell("Z" + czCounter.ToString()).Value = totalInputs;
                    ws.Cell("AA" + czCounter.ToString()).Value = czArea != 0 ? totalInputs / czArea : 0;


                    ws.Cell("AD" + czCounter.ToString()).Value = string.Join(" + ", varieties.ToArray());

                    //Cash Rent
                    var growerShare = 1m;
                    var cashRent = 0m;
                    if (contract.HasValue)
                    {
                        var contractDetails = contract.Value;
                        if (contractDetails.CropZoneRentContracts.Where(x => x.Value.CropZoneId == czId).Any())
                        {
                            var theContract = contractDetails.CropZoneRentContracts.FirstOrDefault(x => x.Value.CropZoneId == czId);
                            growerShare = theContract.Value.GrowerCropShare;
                            cashRent = theContract.Value.CashRentPerArea * czArea;
                            ws.Cell("AC" + czCounter.ToString()).Value = theContract.Value.CashRentPerArea;
                            ws.Cell("AB" + czCounter.ToString()).Value = theContract.Value.CashRentPerArea * czArea;
                        }
                    }

                    var locationsExcludedFromInventory = from loc in yieldLocations
                                                         where loc.IsActiveInCropYear(currentCropYearId.Id) && loc.IsQuantityExcludedFromInventory
                                                         select loc.Id.Id;

                    var czLoads = from load in loadList
								  from loc in load.DestinationLocations
                                  where load.CropZoneId == czId
                                    && !locationsExcludedFromInventory.Contains(loc.Id.Id)
                                  select load;

                    decimal revenue = 0m;
                    var avgSalePrice = 0m;
                    Dictionary<CompositeUnit, decimal> totalPerUnit = new Dictionary<CompositeUnit, decimal>();
                    foreach (var load in czLoads)
                    {
                        var priceItems = priceContainer.Where(x => x.CropID == load.Commodity.CropId);
                        var item = priceItems.Any() && priceItems.Count() > 1 ? priceItems.FirstOrDefault(y => y.AvgPriceUnit == load.FinalQuantityUnit) : priceItems.First();
                        //var commodity = Commodityl
                        if (totalPerUnit.ContainsKey(load.FinalQuantityUnit))
                        {
                            totalPerUnit[load.FinalQuantityUnit] += load.AreaWeightedFinalQuantity; // .FirstOrDefault(load.FinalQuantityUnit);
                            revenue += item != null ? item.AvgPrice * load.AreaWeightedFinalQuantity * growerShare : 0m;
                            ws.Cell("K" + czCounter.ToString()).Value = item != null ? item.AvgPrice : 0m;
                            avgSalePrice = item != null ? item.AvgPrice : 0m;
                        }
                        else
                        {
                            totalPerUnit.Add(load.FinalQuantityUnit, load.AreaWeightedFinalQuantity);
                            revenue += item != null ? item.AvgPrice * load.AreaWeightedFinalQuantity * growerShare : 0m;
                            ws.Cell("K" + czCounter.ToString()).Value = item != null ? item.AvgPrice : 0m;
                            avgSalePrice = item != null ? item.AvgPrice : 0m;
                        }
                    }

                    ws.Cell("S" + czCounter.ToString()).Value = avgSalePrice != 0 && czArea != 0 ? ((cashRent + totalInputs) / avgSalePrice) / czArea : 0m;

                    var loadUnits = (from loads in czLoads
                                     select loads.FinalQuantityUnit).Distinct();

                    if (loadUnits.Count() == 1)
                    {
                        var totalYield = czLoads.Sum(x => x.AreaWeightedFinalQuantity);
                        ws.Cell("I" + czCounter.ToString()).Value = totalYield;
                        ws.Cell("J" + czCounter.ToString()).Value = czArea != 0 ? totalYield / czArea : 0;
                        ws.Cell("L" + czCounter.ToString()).Value = totalYield * growerShare;

                        ws.Cell("R" + czCounter.ToString()).Value = totalYield != 0 && czArea != 0 ? ((cashRent + totalInputs) / totalYield) : 0m;
                    }
                    else if (loadUnits.Count() > 1)
                    {

                        Tuple<CompositeUnit, decimal, string> commodityItem;
                        List<Tuple<CompositeUnit, decimal, string>> commodityItems = new List<Tuple<CompositeUnit, decimal, string>>();
                        foreach (var unit in loadUnits)
                        {
                            var unitTotal = czLoads.Where(t => t.FinalQuantityUnit == unit).Sum(i => i.AreaWeightedFinalQuantity);
                            commodityItem = new Tuple<CompositeUnit, decimal, string>(unit, unitTotal, string.Empty);
                            commodityItems.Add(commodityItem);
                        }

                        var greatest = commodityItems.OrderBy(x => x.Item2).Last();
                        ws.Cell("I" + czCounter.ToString()).Value = greatest.Item2;
                        ws.Cell("J" + czCounter.ToString()).Value = greatest.Item2 / czArea;
                        ws.Cell("L" + czCounter.ToString()).Value = greatest.Item2 * growerShare;

                        ws.Cell("R" + czCounter.ToString()).Value = greatest.Item2 != 0 && czArea != 0 ? ((cashRent + totalInputs) / greatest.Item2) : 0m;
                    }

                    ws.Cell("N" + czCounter.ToString()).Value = revenue;
                    ws.Cell("O" + czCounter.ToString()).Value = czArea != 0 ? revenue / czArea : 0;
                    ws.Cell("M" + czCounter.ToString()).Value = growerShare;

                    ws.Cell("G" + czCounter.ToString()).Value = revenue - totalInputs - cashRent;
                    ws.Cell("H" + czCounter.ToString()).Value = czArea != 0 ? (revenue - totalInputs - cashRent) / czArea : 0;

                    czCounter++;
                }


                /////////////////////////////////////////////////////////////////////////////////////////////////////////
                //STYLE COLUMNS THAT NEED FORMATS
                ////////////////////////////////////////////////////////////////////////////////////////////////////////
                var dollarRange = ws.Range("N2:R" + czCounter.ToString());
                dollarRange.Style.NumberFormat.Format = "$ #,##0.00";

                var dollarRange2 = ws.Range("T2:AC" + czCounter.ToString());
                dollarRange2.Style.NumberFormat.Format = "$ #,##0.00";

                var avgPriceRange = ws.Range("K2:K" + czCounter.ToString());
                avgPriceRange.Style.NumberFormat.Format = "$ #,##0.00";

                var contMarginRange = ws.Range("G2:H" + czCounter.ToString());
                contMarginRange.Style.NumberFormat.Format = "$ #,##0.00";

                var numberRange = ws.Range("S2:S" + czCounter.ToString());
                numberRange.Style.NumberFormat.Format = "#,##0.00";
                ///////////////////////////////////////////////////////////////////////////////////////////////////////


                //////////////////////////////////////////////////////////////////////////////////////////////////////
                //TRANSPOSED REPORT FORMAT
                //////////////////////////////////////////////////////////////////////////////////////////////////////
                if (isTransposed)
                {
                    var rngTable = ws.Range("A1:AD" + czCounter.ToString());
                    rngTable.Transpose(XLTransposeOptions.MoveCells);
                    ws.Columns().AdjustToContents();

                    ws.SheetView.FreezeRows(4);
                    ws.SheetView.FreezeColumns(1);

                    var wrapTable = ws.Range("A1:A" + czCounter.ToString());
                    wrapTable.Style.Alignment.WrapText = true;
                }
                //////////////////////////////////////////////////////////////////////////////////////////////////////

                string filename = string.Empty;
                try
                {
                    //open up file dialog to save file....
                    //then call createexcelfile to create the excel...
                    SaveFileDialog saveFileDialog1 = new SaveFileDialog();

                    saveFileDialog1.Filter = "Excel|*.xlsx";
                    saveFileDialog1.FilterIndex = 2;
                    saveFileDialog1.RestoreDirectory = true;

                    Nullable<bool> resultSaved = saveFileDialog1.ShowDialog();

                    // Process save file dialog box results 
                    if (resultSaved == true)
                    {
                        // Save document 
                        filename = saveFileDialog1.FileName;
                        wb.SaveAs(filename);
                    }

                    //now open file....
                    System.Diagnostics.Process.Start(filename);

                    Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
                }
                catch (Exception ex)
                {
                    Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
                    return null;
                }

            }

            return null;

        }
    }
}

