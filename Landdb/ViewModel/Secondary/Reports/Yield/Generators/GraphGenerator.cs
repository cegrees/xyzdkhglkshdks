﻿using AgC.UnitConversion;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Contract;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Domain.ReadModels.Yield;
using Landdb.Domain.ReadModels.YieldLocation;
using Landdb.Infrastructure;
using Landdb.ReportModels.Yield;
using Landdb.ViewModel.Yield;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using Landdb.Resources;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.Yield.Generators
{
    public class GraphGenerator : IYieldReportGenerator
    {

        readonly IClientEndpoint clientEndpoint;
        readonly Dispatcher dispatcher;

        readonly int cropYear;
        public GraphGenerator(IClientEndpoint clientEndpoint, Dispatcher dispatcher, int cropYear)
        {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            this.cropYear = cropYear;
        }

        public Telerik.Reporting.ReportSource Generate(YieldQueryCriteria criteria)
        {
            ReportBook book = new ReportBook();
            var rs = new InstanceReportSource();

            var currentCropYearId = new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear);

            var masterlist = clientEndpoint.GetMasterlistService();
            var cropZoneApplications = clientEndpoint.GetView<CropZoneApplicationDataView>(currentCropYearId).GetValue(new CropZoneApplicationDataView()).Items;
            var inventoryList = clientEndpoint.GetView<InventoryListView>(currentCropYearId).GetValue(new InventoryListView()).Products;
            var flattenedHierarchyView = clientEndpoint.GetView<FlattenedTreeHierarchyView>(currentCropYearId).GetValue(new FlattenedTreeHierarchyView());
            var loadList = clientEndpoint.GetView<CropZoneLoadDataView>(currentCropYearId).GetValue(new CropZoneLoadDataView()).Items;
            var commodityList = clientEndpoint.GetView<CommodityListView>(currentCropYearId).GetValue(new CommodityListView()).Commodities;
			var yieldLocations = clientEndpoint.GetView<YieldLocationListView>(new DataSourceId(currentCropYearId.DataSourceId)).GetValue(() => new YieldLocationListView()).YieldLocations;

			//get unique list of crops from selected crop zones and then roll this stuff into CommoditySummaryViewModel....
			if (criteria.SelectedCropZones.Any())
            {
                var data = new ProfitabilityData();

                List<CropPrice> priceContainer = new List<CropPrice>();
                foreach (var commodity in commodityList)
                {
                    CommoditySummaryViewModel csViewModel = new CommoditySummaryViewModel(clientEndpoint, dispatcher, currentCropYearId, commodity.CropId, commodity.CommodityDescription);

                    foreach (var summary in csViewModel.UnitSummaries)
                    {
                        priceContainer.Add(new CropPrice()
                        {
                            CropID = commodity.CropId,
                            CommodityDescription = commodity.CommodityDescription,
                            AvgPrice = summary.Value.GrowerAverageRevenue,
                            AvgPriceUnit = summary.Value.QuantityUnit
                        });
                    }
                }

                foreach (var czId in criteria.SelectedCropZones)
                {
                    var profItem = new ProfitabilityItem();
                    List<ProfitabilityItem> multipleCommodities = new List<ProfitabilityItem>();
                    var czDetails = clientEndpoint.GetView<CropZoneDetailsView>(czId).Value;
                    var czItem = flattenedHierarchyView.Items.FirstOrDefault(p => p.CropZoneId == czId);
                    var contract = clientEndpoint.GetView<CropZoneRentContractView>(currentCropYearId);

                    profItem.Farm = czItem.FarmName;
                    profItem.Field = czItem.FieldName + " : " + czItem.CropZoneName;
                    profItem.Crop = masterlist.GetCropDisplay(czItem.CropId);
                    profItem.Area = (decimal)(czDetails.ReportedArea != null ? czDetails.ReportedArea : czDetails.BoundaryArea != null ? czDetails.BoundaryArea : 0.0);
                    profItem.CropYear = cropYear.ToString();
                    var productUnits = clientEndpoint.GetProductUnitResolver();
                    var czAppItems = cropZoneApplications.Where(x => x.CropZoneId == czId);
                    List<string> varieties = new List<string>();

                    foreach (var item in czAppItems)
                    {
                        var invItem = inventoryList.FirstOrDefault(x => x.Key == item.ProductId);
                        var mp = masterlist.GetProduct(item.ProductId);
                        if (mp.ProductType == GlobalStrings.ProductType_Seed)
                        {
                            varieties.Add(mp.Name);
                        }
                        var totalUnit = UnitFactory.GetUnitByName(item.TotalProductUnit);
                        var totalMeasure = totalUnit.GetMeasure(item.TotalProductValue, mp.Density);

                        //see if avg price unit is equal to total prod measure unit.
                        if (!string.IsNullOrEmpty(invItem.Value.AveragePriceUnit) && totalMeasure.Unit == UnitFactory.GetUnitByName(invItem.Value.AveragePriceUnit))
                        {
                            var totalCost = totalMeasure.Value * invItem.Value.AveragePriceValue;
                            profItem.Cost += (decimal)totalCost * (decimal)item.GrowerShare;
                        }
                        else if (!string.IsNullOrEmpty(invItem.Value.AveragePriceUnit))
                        {
                            var totalProdUnit = item.TotalProductUnit != null ? productUnits.GetPackageSafeUnit(item.ProductId, item.TotalProductUnit) : null;
                            var totalProdMeasure = totalProdUnit.GetMeasure((double)item.TotalProductValue, mp.Density);
                            var value = invItem.Value != null ? invItem.Value : null;
                            var avgPriceUnit = value != null && value.AveragePriceUnit != null ? productUnits.GetPackageSafeUnit(value.ProductId, value.AveragePriceUnit) : null;
                            var convertedTotalProdMeasure = totalProdMeasure.CanConvertTo(avgPriceUnit) ? avgPriceUnit.GetMeasure(totalProdMeasure.GetValueAs(avgPriceUnit)) : totalProdMeasure;
                            var convertedCost = convertedTotalProdMeasure.Value * invItem.Value.AveragePriceValue;
                            profItem.Cost += (decimal)convertedCost * (decimal)item.GrowerShare;
                        }
                    }

                    profItem.Variety = string.Join(" : ", varieties.ToArray());

					var locationsExcludedFromInventory = from loc in yieldLocations
														 where loc.IsActiveInCropYear(currentCropYearId.Id) && loc.IsQuantityExcludedFromInventory
														 select loc.Id.Id;

					var czLoads = from load in loadList
								  from loc in load.DestinationLocations
								  where load.CropZoneId == czId
									&& !locationsExcludedFromInventory.Contains(loc.Id.Id)
								  select load;

                    foreach (var load in czLoads)
                    {
                        ProfitabilityItem item = new ProfitabilityItem();
                        item.Area = profItem.Area;
                        item.Cost = profItem.Cost;
                        item.Crop = profItem.Crop;
                        item.CropYear = profItem.CropYear;
                        item.DataSourceName = profItem.DataSourceName;
                        item.Farm = profItem.Farm;
                        item.Field = profItem.Field;
                        item.Variety = profItem.Variety;
                        item.TotalYieldValue = load.AreaWeightedFinalQuantity;
                        item.TotalYieldUnit = load.FinalQuantityUnit;

                        var prices = priceContainer.Where(x => x.CropID == czItem.CropId).ToList();
                        if (prices.Any())
                        {
                            var specificPrice = from u in prices
                                                where u.AvgPriceUnit == item.TotalYieldUnit
                                                select u;
                            if (specificPrice.Any())
                            {
                                var priceVal = specificPrice.First().AvgPrice;
                                item.Income += item.TotalYieldValue * priceVal;
                            }
                            else if (prices.First().AvgPriceUnit != null && CompositeUnitConverter.CanConvert(item.TotalYieldUnit, prices.First().AvgPriceUnit))
                            {
                                var priceUnit = prices.First().AvgPriceUnit;
                                var priceVal = prices.First().AvgPrice;

                                var newValue = CompositeUnitConverter.ConvertValue(item.TotalYieldValue, item.TotalYieldUnit, priceUnit);
                                item.Income += newValue * priceVal;
                            }
                        }

                        if (multipleCommodities.Count() > 0)
                        {
                            bool canConvert = false;
                            foreach (var selectedCommodity in multipleCommodities)
                            {
                                if (CompositeUnitConverter.CanConvert(item.TotalYieldUnit, selectedCommodity.TotalYieldUnit))
                                {
                                    var convertedValue = CompositeUnitConverter.ConvertValue(item.TotalYieldValue, item.TotalYieldUnit, selectedCommodity.TotalYieldUnit);
                                    selectedCommodity.TotalYieldValue += convertedValue;
                                    selectedCommodity.Income += item.Income;
                                    canConvert = true;
                                }
                            }

                            if (canConvert == false)
                            {
                                multipleCommodities.Add(item);
                            }
                        }
                        else
                        {
                            multipleCommodities.Add(item);
                        }
                    }

                    decimal cropMultiplier = 1m;
                    if (contract.HasValue)
                    {
                        //gotta ask about how to deal with multiple rent contracts on a cropzone
                        cropMultiplier = contract.Value.CropZoneRentContracts.First().Value.GrowerCropShare;
                        profItem.TotalYieldValue = profItem.TotalYieldValue * cropMultiplier;
                        profItem.Income = profItem.Income * cropMultiplier;
                    }

                    multipleCommodities.ForEach(i => i.Cost = i.Cost / (decimal)multipleCommodities.Count());
                    multipleCommodities.ForEach(p => p.Income = p.Income * cropMultiplier);
                    multipleCommodities.ForEach(s => s.TotalYieldValue = s.TotalYieldValue * cropMultiplier);
                    multipleCommodities.ForEach(c => c.NetIncome = c.Income - c.Cost);
                    multipleCommodities.ForEach(n => n.NetIncomePerArea = n.NetIncome / n.Area);
                    multipleCommodities.ForEach(a => a.AvgYieldValue = a.TotalYieldValue / a.Area);
                    multipleCommodities.ForEach(u => u.AvgYieldUnit = u.TotalYieldUnit);

                    data.ProfitabilityItems.AddRange(multipleCommodities);
                }

                data.ProfitabilityItems = data.ProfitabilityItems.OrderBy(f => f.Farm).ThenBy(c => c.Crop).ToList();

                var report = new Landdb.Views.Secondary.Reports.Yield.GraphedYield();
                report.DataSource = data.ProfitabilityItems;
                report.Name = Strings.ReportName_Graph_Text;

                if (data.ProfitabilityItems.Count() > 0)
                {
                    book.Reports.Add(report);
                }
            }

            rs.ReportDocument = book;

            return rs;
        }

        public string DisplayName
        {
            get { return Strings.GeneratorName_ProfitabilityGraphedReport_Text; }
        }
    }

}