﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.Yield.Generators
{
    public interface IYieldReportGenerator
    {
        ReportSource Generate(YieldQueryCriteria criteria);
        string DisplayName { get; }
    }
}
