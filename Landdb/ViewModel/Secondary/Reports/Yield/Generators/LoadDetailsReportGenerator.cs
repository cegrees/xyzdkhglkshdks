﻿using AgC.UnitConversion;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Domain.ReadModels.Yield;
using Landdb.Domain.ReadModels.YieldLocation;
using Landdb.Infrastructure;
using Landdb.ReportModels.Yield;
using Landdb.ViewModel.Yield;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using Landdb.Resources;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.Yield.Generators
{
    public class LoadDetailsReportGenerator: IYieldReportGenerator
    {

        readonly IClientEndpoint clientEndpoint;
        readonly Dispatcher dispatcher;

        readonly int cropYear;

        public LoadDetailsReportGenerator(IClientEndpoint clientEndpoint, Dispatcher dispatcher, int cropYear)
        {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            this.cropYear = cropYear;
        }


        public Telerik.Reporting.ReportSource Generate(YieldQueryCriteria criteria)
        {
            ReportBook book = new ReportBook();
			var rs = new InstanceReportSource();

			var currentCropYearId = new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear);

			var masterlist = clientEndpoint.GetMasterlistService();
			var cropZoneApplications = clientEndpoint.GetView<CropZoneApplicationDataView>(currentCropYearId).GetValue(new CropZoneApplicationDataView()).Items;
			var flattenedHierarchyView = clientEndpoint.GetView<FlattenedTreeHierarchyView>(currentCropYearId).GetValue(new FlattenedTreeHierarchyView());
			var cropZoneLoads = clientEndpoint.GetView<CropZoneLoadDataView>(currentCropYearId).GetValue(new CropZoneLoadDataView()).Items;
			var yieldLocations = clientEndpoint.GetView<YieldLocationListView>(new DataSourceId(currentCropYearId.DataSourceId)).GetValue(() => new YieldLocationListView()).YieldLocations;

			var dsName = criteria.SelectedEntity != null ? criteria.SelectedEntity.Name : ApplicationEnvironment.CurrentDataSourceName;
            var area = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit);
			//get unique list of crops from selected crop zones and then roll this stuff into CommoditySummaryViewModel....
            if (criteria.SelectedCropZones.Any())
            {
                var data = new List<YieldLoadData>();

                var locationsExcludedFromInventory = from loc in yieldLocations
													 where loc.IsActiveInCropYear(currentCropYearId.Id) && loc.IsQuantityExcludedFromInventory
													 select loc.Id.Id;

				foreach (var czId in criteria.SelectedCropZones)
                {
                    var czDetails = clientEndpoint.GetView<CropZoneDetailsView>(czId).Value;

					//FIX :: MOVE REPORT END DATE TO END OF DAY TIME.
					var czLoads = (from load in cropZoneLoads
								   from loc in load.DestinationLocations
								   where load.CropZoneId == czId
									 && criteria.ReportStartDate.Date <= load.StartDateTime.Date
									 && criteria.ReportEndDate.Date >= load.StartDateTime.Date
									 && !locationsExcludedFromInventory.Contains(loc.Id.Id)
								   select load).Distinct(x => x.LoadId);

                    var flattenedItem = flattenedHierarchyView.Items.FirstOrDefault(x => x.CropZoneId == czId);
                    foreach (var load in czLoads)
                    {
						foreach (var destination in load.DestinationLocations) {
							var loadDetail = new YieldLoadData();

                            loadDetail.DateRange = string.Format("{0} - {1}", criteria.ReportStartDate.ToShortDateString(), criteria.ReportEndDate.ToShortDateString());
                            loadDetail.DataSourceName = dsName;
							loadDetail.CropYear = cropYear;
							loadDetail.LoadDate = load.StartDateTime;
							loadDetail.LoadName = load.LoadNumber;
							var areaValue = (czDetails.ReportedArea != null && czDetails.ReportedArea > 0) ? czDetails.ReportedArea : null;
							areaValue = areaValue == null && czDetails.BoundaryArea != null ? czDetails.BoundaryArea : areaValue;
							areaValue = areaValue == null ? 0 : areaValue;
							loadDetail.CropZoneArea = string.Format("{0} {1}", areaValue.Value.ToString("N2"), area.AbbreviatedDisplay);
							loadDetail.Area = (decimal)areaValue;
							loadDetail.CropZoneId = load.CropZoneId;
							loadDetail.FarmFieldCZ = string.Format("{0} :: {1} :: {2}", flattenedItem.FarmName, flattenedItem.FieldName, flattenedItem.CropZoneName);
							loadDetail.Commodity = load.Commodity.FullCommodityDisplayText;
							loadDetail.DryQty = load.AreaWeightedFinalQuantity / load.DestinationLocations.Count;

							loadDetail.LocationName = destination.Name;
							var loadMaybe = clientEndpoint.GetView<LoadDetailsView>(load.LoadId);
							var yieldUnit = load.FinalQuantityUnit;
							loadDetail.PerAreaDisplay = string.Format("{0} / {1}", yieldUnit.PackageUnitName, area.Name);
							loadDetail.DryQtyUnit = yieldUnit.PackageUnitName;

							if (loadMaybe.HasValue) {
								var loadValue = loadMaybe.Value;
								var mulitplier = load.AreaValue / (decimal)loadValue.CropZones.Sum(x => x.Area.Value);
								//MAY HAVE TO MULTIPLY THESE BY THE MULTIPLIER TO HAVE A WEIGHTED QTY

								loadDetail.GrossWeight = loadValue.QuantityInformation.TruckGrossWeight / load.DestinationLocations.Count;
								loadDetail.TareWeight = loadValue.QuantityInformation.TruckTareWeight / load.DestinationLocations.Count;
								loadDetail.NetWeight = loadValue.QuantityInformation.TruckNetWeight / load.DestinationLocations.Count;

								loadDetail.DeliveredQty = ((loadValue.QuantityInformation.FinalQuantityValue + loadValue.QuantityInformation.StandardMoistureShrinkQuantity + loadValue.QuantityInformation.HandlingShrinkQuantity) * (load.AreaValue / (decimal)loadValue.CropZones.Sum(x => x.Area.Value))) / load.DestinationLocations.Count;
								loadDetail.Moisture = loadValue.QuantityInformation.InitialMoisturePercentage;
							}

							data.Add(loadDetail); 
						}
                    }
                }

                data = data.OrderBy(x => x.FarmFieldCZ).ThenBy(y => y.Commodity).ThenBy(d => d.LoadDate).ToList();

                var report = new Landdb.Views.Secondary.Reports.Yield.LoadDetailsReport();
                report.DataSource = data;
                report.Name = Strings.ReportName_LoadDetails_Text;

                book.Reports.Add(report);
            }

            rs.ReportDocument = book;

            return rs;
        }

        public string DisplayName
        {
            get { return Strings.GeneratorName_LoadDetails_Text; }
        }
    }
}
