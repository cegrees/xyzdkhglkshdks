﻿using ExportToExcel;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Yield;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Shared;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Landdb.Resources;

namespace Landdb.ViewModel.Secondary.Reports.Yield.Generators
{
    public class ECottonExcelReportGenerator : IYieldReportGenerator
    {
        string filename;
        IClientEndpoint endpoint;
        int cropYear;

        public ECottonExcelReportGenerator(IClientEndpoint endpoint, int cropYear)
        {
            this.endpoint = endpoint;
            this.cropYear = cropYear;
            BaleList = new List<ECottonBale>();
        }

        public List<ECottonBale> BaleList { get; set; }

        public void GenerateExcel(YieldQueryCriteria crit)
        {

            var eCottonDocs = endpoint.GetView<ECottonDocumentsListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear));
            if (eCottonDocs.HasValue)
            {
                var docsList = eCottonDocs.Value.ECottonDocuments;

                foreach (var doc in docsList)
                {
                    var docsModel = new ECottonDocumentViewModel(endpoint, doc);
                    BaleList.AddRange(docsModel.CottonBaleList);
                }

                BaleList = BaleList.OrderBy(x => x.BaleNumber).ToList();

                System.Data.DataTable dt1 = new System.Data.DataTable();
                dt1 = ExportToExcel.CreateExcelFile.ListToDataTable(BaleList, true);
                dt1.TableName = Strings.TableName_ECottonData_Text;
                DataSet ds = new DataSet("excelDS");
                ds.Tables.Add(dt1);


                try
                {
                    //open up file dialog to save file....
                    //then call createexcelfile to create the excel...
                    SaveFileDialog saveFileDialog1 = new SaveFileDialog();

                    saveFileDialog1.Filter = "Excel|*.xlsx";
                    saveFileDialog1.FilterIndex = 2;
                    saveFileDialog1.RestoreDirectory = true;

                    Nullable<bool> resultSaved = saveFileDialog1.ShowDialog();

                    // Process save file dialog box results 
                    if (resultSaved == true)
                    {
                        // Save document 
                        filename = saveFileDialog1.FileName;
                        CreateExcelFile.CreateExcelDocument(ds, filename);
                    }

                    //now open file....
                    System.Diagnostics.Process.Start(filename);

                    Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
                }
                catch (Exception ex)
                {
                    Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
                    return;
                }

            }
        }

        public Telerik.Reporting.ReportSource Generate(YieldQueryCriteria criteria)
        {
            GenerateExcel(criteria);
            return null;
        }

        public string DisplayName
        {
            get { return Strings.GeneratorName_ECottonYieldReport_Text; }
        }
    }
}
