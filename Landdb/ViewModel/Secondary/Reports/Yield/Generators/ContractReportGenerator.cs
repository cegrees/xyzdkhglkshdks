﻿using AgC.UnitConversion;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.ReportModels.Yield;
using Landdb.ViewModel.Secondary.YieldOperation;
using Landdb.Views.Secondary.Reports.Yield;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

using Landdb.Resources;
using Telerik.Reporting;
using Landdb.ReportModels.Contracts;
using Landdb.Domain.ReadModels.Contract;
using Landdb.ViewModel.Resources;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.ViewModel.Resources.Contract.Rent;
using Landdb.Domain.ReadModels.YieldLocation;

namespace Landdb.ViewModel.Secondary.Reports.Yield.Generators
{
    public class ContractReportGenerator : IYieldReportGenerator
    {

        IClientEndpoint endpoint;
        Dispatcher dispatcher;
        int cropYear;

        public ContractReportGenerator(IClientEndpoint endPoint, Dispatcher dispatcher, int cropYear)
        {
            this.endpoint = endPoint;
            this.dispatcher = dispatcher;
            this.cropYear = cropYear;
        }

        private IEnumerable<ContractListItemViewModel> GetListItemModels(Guid datasourceid, int cropyear)
        {
            var cropYearId = new CropYearId(datasourceid, cropyear);

            var rentContractListView = endpoint.GetView<RentContractListView>(cropYearId).GetValue(new RentContractListView());
            var rentContractListItems = rentContractListView.RentContracts.Select(x => new ContractListItemViewModel(x));

            var productionContractListView = endpoint.GetView<ProductionContractListView>(cropYearId).GetValue(new ProductionContractListView());
            var productionContractListItems = productionContractListView.ProductionContracts.Select(x => new ContractListItemViewModel(x));

            return rentContractListItems.Union(productionContractListItems).OrderBy(x => x.Name);
        }

        public Telerik.Reporting.ReportSource Generate(YieldQueryCriteria criteria)
        {
            ReportBook book = new ReportBook();
            var rs = new InstanceReportSource();

            var listofcontracts = GetListItemModels(ApplicationEnvironment.CurrentDataSourceId, this.cropYear);
            foreach (ContractListItemViewModel contractlistitem in listofcontracts)
            {
                if(contractlistitem.Date < criteria.ReportStartDate || contractlistitem.Date > criteria.ReportEndDate) { continue; }
                var rentcontractData = endpoint.GetView<RentContractDetailsView>(contractlistitem.Id);
                var productionContractData = endpoint.GetView<ProductionContractDetailsView>(contractlistitem.Id);
                //var inventory = endpoint.GetView<InventoryListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new InventoryListView());
                //var masterlist = endpoint.GetMasterlistService();
                if (rentcontractData.HasValue)
                {
                    var doesContainCropzones = rentcontractData.Value.CropZones.Any(x => criteria.SelectedCropZones.Contains(x.Id));
                    if (doesContainCropzones)
                    {
                        var reportData = RentContractData(rentcontractData.Value);
                        var report = new Landdb.Views.Secondary.Reports.Contracts.RentContractSummary();
                        report.DataSource = reportData;
                        report.Name = Strings.ReportName_RentContract_Text;
                        book.Reports.Add(report);
                    }
                }
                else if (productionContractData.HasValue)
                {
                    var doesContainCropzones = productionContractData.Value.CropZones.Any(x => criteria.SelectedCropZones.Contains(x.Id));
                    if (doesContainCropzones)
                    {
                        var reportData = ProductionContractData(productionContractData.Value);
                        var report = new Landdb.Views.Secondary.Reports.Contracts.ProductionContractSummary();
                        report.DataSource = reportData;
                        report.Name = Strings.ReportName_ProductionContract_Text;
                        book.Reports.Add(report);
                    }
                }
            }

            rs.ReportDocument = book;
            return rs;
        }

        public RentContractData RentContractData(RentContractDetailsView view)
        {
            var flattenedHierarchy = endpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new FlattenedTreeHierarchyView());

            var data = new RentContractData();
            data.DataSourceName = ApplicationEnvironment.CurrentDataSourceName;
            data.ContractName = view.Name;
            var czId = view.CropZones.FirstOrDefault().Id;
            var cropId = flattenedHierarchy.Items.FirstOrDefault(x => x.CropZoneId == czId).CropId;
            var masterlist = endpoint.GetMasterlistService();
            data.Crop = masterlist.GetCropDisplay(cropId);
            var area = view.CropZones.Sum(x => x.Area.Value);
            data.TotalArea = area;
            data.AreaDisplay = string.Format("{0} {1}", area.ToString("N2"), view.CropZones.FirstOrDefault().Area.Unit.AbbreviatedDisplay);
            data.CropYear = cropYear.ToString();

            data.TotalRent = view.PerAcre * area;
            data.CPPercent = 1.0 - view.IncludedShareInfo.GrowerCropProtectionShare;
            data.FertPercent = 1.0 - view.IncludedShareInfo.GrowerFertilizerShare;
            data.SeedPercent = 1.0 - view.IncludedShareInfo.GrowerSeedShare;
            data.ServicePercent = 1.0 - view.IncludedShareInfo.GrowerServiceShare;
            data.CropShare = 1.0 - view.IncludedShareInfo.GrowerCropShare;

            foreach (var item in view.FlexDetails)
            {
                var flexItem = new RentContractData.FlexInformationItem();
                flexItem.BonusPercent = item.BonusPercent;
                flexItem.PerAcreUnit = item.LevelUnit != null ? item.LevelUnit.AbbreviatedDisplay : string.Empty;
                flexItem.PerAcreValue = item.LevelValue;
                data.FlexItems.Add(flexItem);
            }

            data.DueDate = view.RentDue;
            data.StartDate = view.StartDate;

            foreach (var cz in view.CropZones)
            {
                var field = new RentContractData.FieldLineItem();
                var czItem = flattenedHierarchy.Items.FirstOrDefault(x => x.CropZoneId == cz.Id);
                field.AreaDisplay = cz.Area.Value;
                field.CropZone = czItem.CropZoneName;
                field.Field = czItem.FieldName;
                field.Farm = czItem.FarmName;
                data.Fields.Add(field);
            }

            data.FlexPercent = view.FlexPercentage;

            var desc = string.Empty;

            switch (view.FlexPercentageSource)
            {
                case RentContractFlexPercentageSources.TotalRevenue:
                    desc = Strings.GeneratorVariable_SingleContract_OfTotalRevenue_Text;
                    break;
                case RentContractFlexPercentageSources.ProductionRevenue:
                    desc = Strings.GeneratorVariable_SingleContract_OfProductionRevenue_Text;
                    break;
                case RentContractFlexPercentageSources.TotalQuantityAtFlatPrice:
                    desc = Strings.GeneratorVariable_SingleContract_OfTotalQuantityFlatCropPrice_Text;
                    break;
                case RentContractFlexPercentageSources.None:
                    desc = string.Empty;
                    break;
                default:
                    desc = string.Empty;
                    break;
            }

            data.FlexPercentSource = desc.ToLower();

            data.LandOwner = view.LandOwner != null ? view.LandOwner.Name : string.Empty;
            data.PerAcreRent = view.PerAcre;

            var shareAndInfoViewModel = new IncludedShareInfoViewModel(view.IncludedShareInfo);
            var cropYearId = new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear);
            shareAndInfoViewModel.CalculateShareDollars(endpoint, cropYearId, view.CropYear, view.CropZones.Select(x => x.Id.Id), view.Id);

            data.TotalCPShare = shareAndInfoViewModel.CropProtectionLandOwnerShareDollars * (1.0 + view.IncludedInventoryInfo.CropProtectionMargin);
            data.TotalFertShare = shareAndInfoViewModel.FertilizerLandOwnerShareDollars * (1.0 + view.IncludedInventoryInfo.FertilizerMargin);
            data.TotalSeedShare = shareAndInfoViewModel.SeedLandOwnerShareDollars * (1.0 + view.IncludedInventoryInfo.SeedMargin);
            data.TotalServiceShare = shareAndInfoViewModel.ServiceLandOwnerShareDollars * (1.0 + view.IncludedInventoryInfo.ServiceMargin);
            data.TotalShare = data.TotalCPShare + data.TotalFertShare + data.TotalSeedShare + data.TotalServiceShare;

            var czrcView = endpoint.GetView<CropZoneRentContractView>(cropYearId).GetValue(new CropZoneRentContractView());
            var czIds = view.CropZones.Select(x => x.Id.Id);
            var associatedCropZones = czrcView.CropZoneRentContracts.Where(x => czIds.Contains(x.Key)).Select(x => x.Value);

            var unitList = new List<CompositeUnit>();

            // determine the most frequently used unit
            if (view.AssociatedSaleLoads.Any())
            {
                unitList = (from load in view.AssociatedSaleLoads
                            group load by load.FinalQuantityUnit into g
                            where g.Key != null
                            orderby g.Count() descending
                            select g.Key).ToList();
            }
            else if (associatedCropZones.Any())
            {
                unitList = (from dic in associatedCropZones.Select(x => x.LoadQuantities)
                            from load in dic.Values
                            group load by load.QuantityUnit into g
                            where g.Key != null
                            orderby g.Count() descending
                            select g.Key).ToList();
            }

            var yieldLocationList = endpoint.GetView<YieldLocationListView>(new DataSourceId(cropYearId.DataSourceId)).GetValue(() => new YieldLocationListView());
            var excludedYieldLocations = from loc in yieldLocationList.YieldLocations
                                         where loc.IsActiveInCropYear(cropYearId.Id) && loc.IsQuantityExcludedFromInventory
                                         select loc.Id;

            foreach (var unit in unitList)
            {
                var saleInfo = new IncludedSaleInfoViewModel(unit, Convert.ToDecimal(view.IncludedShareInfo.GrowerCropShare), view.AssociatedSaleLoads, associatedCropZones, excludedYieldLocations);

                data.YieldShares.Add(new RentContractData.YieldShare()
                {
                    CropShare = 1 - view.IncludedShareInfo.GrowerCropShare,
                    TotalHarvested = (double)saleInfo.TotalQuantity,
                    HarvestedUnit = saleInfo.DisplayUnit != null ? saleInfo.DisplayUnit.AbbreviatedDisplay : string.Empty,
                    LandOwnerShares = (double)saleInfo.LandOwnerShares,
                    LandOwnerSales = (double)saleInfo.LandOwnerSales,
                    LandOwnerSharesSold = (double)saleInfo.LandOwnerSoldShares,
                    AverageSalePrice = saleInfo.LandOwnerSoldShares > 0 ? (double)(saleInfo.LandOwnerSales / saleInfo.LandOwnerSoldShares) : 0,
                });
            }

            return data;
        }

        public ProductionContractData ProductionContractData(ProductionContractDetailsView view)
        {
            var flattenedHierarchy = endpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear)).GetValue(new FlattenedTreeHierarchyView());

            var data = new ProductionContractData();
            data.DataSourceName = ApplicationEnvironment.CurrentDataSourceName;
            data.ContractName = view.Name;
            var masterlist = endpoint.GetMasterlistService();
            data.Crop = masterlist.GetCropDisplay(new CropId(view.IncludedCommodityInfo.CropId));
            var area = view.CropZones.Sum(x => x.Area.Value);
            data.Area = area;
            data.AreaDisplay = string.Format("{0} {1}", area.ToString("N2"), view.CropZones.FirstOrDefault().Area.Unit.AbbreviatedDisplay);
            data.Buyer = view.Buyer != null ? view.Buyer.Name : string.Empty;
            data.CropYear = cropYear.ToString();

            //data.TotalRent = view.PerAcre * area;
            //data.CPPercent = 1.0 - view.IncludedShareInfo.GrowerCropProtectionShare;
            //data.FertPercent = 1.0 - view.IncludedShareInfo.GrowerFertilizerShare;
            //data.SeedPercent = 1.0 - view.IncludedShareInfo.GrowerSeedShare;
            //data.ServicePercent = 1.0 - view.IncludedShareInfo.GrowerServiceShare;
            //data.CropShare = 1.0 - view.IncludedShareInfo.GrowerCropShare;

            data.DueDate = view.DeliveryDate;
            data.StartDate = view.StartDate;

            foreach (var cz in view.CropZones)
            {
                var field = new CropZoneItems();
                var czItem = flattenedHierarchy.Items.FirstOrDefault(x => x.CropZoneId == cz.Id);
                field.Area = cz.Area.Value;
                field.CropZone = czItem.CropZoneName;
                field.Field = czItem.FieldName;
                field.Farm = czItem.FarmName;
                data.IncludedCropZones.Add(field);
            }


            var desc = string.Empty;

            var commodityInfo = new IncludedProductionContractCommodityInfo(view.IncludedCommodityInfo.CropId, view.IncludedCommodityInfo.ContractedAmount, view.IncludedCommodityInfo.ContractedAmountUnit, view.IncludedCommodityInfo.ContractPrice, view.IncludedCommodityInfo.FuturesPrice); // IncludedSaleInfoViewModel(endpoint, dispatcher, view);
            var dsId = ApplicationEnvironment.CurrentDataSourceId;
            calculateFulfilledAmount(new CropYearId(dsId, cropYear), new ContractId(dsId, view.Id.Id), new CropId(commodityInfo.CropId), commodityInfo.ContractedAmountUnit);
            data.Fulfillment.Add(new FulfilmentItem() { Name = Strings.GeneratorVariable_SingleContract_OfTotalQuantityFlatCropPrice_Text, Value = FulfilledAmount });
            data.Fulfillment.Add(new FulfilmentItem() { Name = Strings.GeneratorVariable_SingleContract_Remaining_Text, Value = commodityInfo.ContractedAmount - FulfilledAmount });
            data.ContractedAmount = commodityInfo.ContractedAmount;
            data.ContractedAmountDisplay = string.Format("{0} {1}", commodityInfo.ContractedAmount.ToString("N2"), commodityInfo.ContractedAmountUnit.AbbreviatedDisplay);

            data.FulfilledAmount = FulfilledAmount;
            data.FulfilledDisplay = string.Format("{0:N2} {1}", FulfilledAmount, commodityInfo.ContractedAmountUnit.AbbreviatedDisplay);
            data.PercentFulfilled = Convert.ToDecimal(commodityInfo.ContractedAmount != 0 ? FulfilledAmount / commodityInfo.ContractedAmount : 0);

            data.ContractedPrice = commodityInfo.ContractPrice;
            data.FuturesPrice = commodityInfo.FuturesPrice;

            return data;
        }
        private double FulfilledAmount { get; set; }

        private void calculateFulfilledAmount(CropYearId currentCropYearId, ContractId contractId, CropId cropId, CompositeUnit contractedAmountUnit)
        {
            if (contractedAmountUnit != null)
            {
                var loads = endpoint.GetView<ProductionContractLoadsView>(currentCropYearId)
                    .GetValue(new ProductionContractLoadsView())
                    .ProductionContractLoads
                    .Values
                    .Where(x => x.GrowerProductionContractId == contractId || x.LandOwnerProductionContractId == contractId);

                var shareDic = new Dictionary<ContractId, decimal>();
                decimal shareQuantity = 0;

                foreach (var load in loads)
                {
                    if (load.IsSplitLoad && load.GrowerProductionContractId != load.LandOwnerProductionContractId)
                    {
                        decimal growerSharePercentage;

                        if (shareDic.ContainsKey(load.RentContractId))
                        {
                            growerSharePercentage = shareDic[load.RentContractId];
                        }
                        else
                        {
                            var rentContractDetails = endpoint.GetView<RentContractDetailsView>(load.RentContractId).GetValue(new RentContractDetailsView());
                            growerSharePercentage = Convert.ToDecimal(rentContractDetails.IncludedShareInfo.GrowerCropShare);
                            shareDic.Add(load.RentContractId, growerSharePercentage);
                        }

                        if (load.GrowerProductionContractId == contractId)
                        {
                            shareQuantity = load.FinalQuantityValue * growerSharePercentage;
                        }
                        else if (load.LandOwnerProductionContractId == contractId)
                        {
                            shareQuantity = load.FinalQuantityValue * (1 - growerSharePercentage);
                        }
                    }
                    else
                    {
                        shareQuantity = load.FinalQuantityValue;
                    }

                    var convertedQuantity = CompositeUnitConverter.ConvertValue(shareQuantity, load.FinalQuantityUnit, contractedAmountUnit);
                    FulfilledAmount += Convert.ToDouble(convertedQuantity);
                }
            }
        }

        public string DisplayName
        {
            get { return Strings.GeneratorName_ContractReport_Text; }
        }
    }
}
