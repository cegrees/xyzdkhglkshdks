﻿using AgC.UnitConversion;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Domain.ReadModels.Yield;
using Landdb.Domain.ReadModels.YieldLocation;
using Landdb.Infrastructure;
using Landdb.ReportModels.Yield;
using Landdb.ViewModel.Yield;
using Landdb.Views.Secondary.Reports.Yield;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Landdb.Domain.ReadModels.Equipment;
using Landdb.Resources;
using Lokad.Cqrs;
using Telerik.Reporting;

namespace Landdb.ViewModel.Secondary.Reports.Yield.Generators
{
    public class CompletedLoadTicketReportGenerator : IYieldReportGenerator
    {
        LoadListItemViewModel _model;
        IClientEndpoint _endPoint;

        public CompletedLoadTicketReportGenerator(LoadListItemViewModel load, IClientEndpoint endPoint)
        {
            _model = load;
            _endPoint = endPoint;
        }

        public string DisplayName
        {
            get
            {
                return Strings.GeneratorName_LoadTicket_Text;
            }
        }

        public ReportSource Generate(YieldQueryCriteria criteria)
        {
            ReportBook book = new ReportBook();
            var rs = new InstanceReportSource();

            if (_model != null)
            {
                var loadDetailsMaybe = _endPoint.GetView<LoadDetailsView>(_model.Id);
                var loadDetails = loadDetailsMaybe.HasValue ? loadDetailsMaybe.Value : null;

                var reportModel = new LoadData();
                reportModel.DataSourceName = ApplicationEnvironment.CurrentDataSourceName;
                reportModel.CropYear = ApplicationEnvironment.CurrentCropYear.ToString();
                reportModel.LoadNumber = _model.LoadNumber;
                reportModel.LoadDate = _model.StartDateTime;
                reportModel.Commodity = !string.IsNullOrEmpty(_model.CommodityDescription) ? _model.CommodityDescription : string.Empty;

                var locationName = loadDetails.DestinationLocations.Any() && loadDetails.DestinationLocations.Count > 1 ? _endPoint.GetView<YieldLocationDetailsView>(loadDetails.DestinationLocations.First().ParentLocationId).Value.Name : loadDetails.DestinationLocations.First().Name;
                reportModel.Destination = loadDetails.DestinationLocations.Count > 1 ? string.Format("{0} - {1}", locationName, string.Join(", ", loadDetails.DestinationLocations)) : locationName;
                Maybe<EquipmentDetailsView> possibleEquipment = loadDetails.TruckId != null ? _endPoint.GetView<EquipmentDetailsView>(loadDetails.TruckId) : Maybe<EquipmentDetailsView>.Empty;
                reportModel.Truck = possibleEquipment.HasValue ? possibleEquipment.Value.BasicInfo.Name : string.Empty;
                reportModel.Driver = loadDetails.DriverName;
                reportModel.MoisturePercent = loadDetails.QuantityInformation.InitialMoisturePercentage;
                reportModel.MoistureShrinkDisplay = string.Format("{0} {1}", loadDetails.QuantityInformation.StandardMoistureShrinkQuantity.ToString("N2"), loadDetails.QuantityInformation.FinalQuantityUnit.AbbreviatedDisplay);
                reportModel.AdditionalShrinkDisplay = string.Format("{0} {1}", loadDetails.QuantityInformation.AdjustmentShrinkQuantity.ToString("N2"), loadDetails.QuantityInformation.FinalQuantityUnit.AbbreviatedDisplay);
                reportModel.GrossWeightDisplay = string.Format("{0} {1}", loadDetails.QuantityInformation.TruckGrossWeight.ToString("N2"), loadDetails.QuantityInformation.TruckWeightUnit.AbbreviatedDisplay);
                reportModel.TareWeightDisplay = string.Format("{0} {1}", loadDetails.QuantityInformation.TruckTareWeight.ToString("N2"), loadDetails.QuantityInformation.TruckWeightUnit.AbbreviatedDisplay);
                reportModel.NetWeightDisplay = string.Format("{0} {1}", loadDetails.QuantityInformation.TruckNetWeight.ToString("N2"), loadDetails.QuantityInformation.TruckWeightUnit.AbbreviatedDisplay);
                reportModel.FinalWeightDisplay = string.Format("{0} {1}", loadDetails.QuantityInformation.FinalQuantityValue.ToString("N2"), loadDetails.QuantityInformation.FinalQuantityUnit.AbbreviatedDisplay);
                reportModel.Notes = loadDetails.Notes; // _model.Notes;
                reportModel.TotalShrinkDisplay = string.Format("{0} {1}", (loadDetails.QuantityInformation.AdjustmentShrinkQuantity + loadDetails.QuantityInformation.HandlingShrinkQuantity + loadDetails.QuantityInformation.StandardMoistureShrinkQuantity).ToString("N2"), loadDetails.QuantityInformation.FinalQuantityUnit.AbbreviatedDisplay);

                var _loadProperties = _endPoint.GetMasterlistService().GetLoadPropertyDefinitions();

                foreach (var prop in loadDetails.ReportedPropertySets)
                {
                    foreach (var innerProperty in prop.ReportedPropertyValues)
                    {
                        var newProperty = new LoadPropertyItem();
                        newProperty.Name = _loadProperties.LoadProperties.FirstOrDefault(x => x.Id == innerProperty.LoadPropertyId).Name;
                        newProperty.Value = innerProperty.ReportedUnit != null ? string.Format("{0} {1}", innerProperty.ReportedValue, innerProperty.ReportedUnit.AbbreviatedDisplay) : innerProperty.ReportedValue;
                        reportModel.LoadProperties.Add(newProperty);
                    }
                }

                var flattenedTree = _endPoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear)).GetValue(new FlattenedTreeHierarchyView()); ;
                var _cropZones = from c in loadDetails.CropZones select c.Id;
                foreach (var czId in _cropZones)
                {
                    var czItem = flattenedTree.Items.FirstOrDefault(x => x.CropZoneId == czId);
                    var includedFieldItem = new LoadFieldItem();
                    var czDetailsMaybe = _endPoint.GetView<CropZoneDetailsView>(czId);

                    if (czItem.FieldId != null)
                    {
                        var fieldDetails = _endPoint.GetView<FieldDetailsView>(czItem.FieldId).Value;
                        includedFieldItem.FSAFarm = fieldDetails.FsaFarmNumber;
                        includedFieldItem.FSAField = fieldDetails.FsaFieldNumber;
                        includedFieldItem.FSATract = fieldDetails.FsaTractNumber;
                        includedFieldItem.Field = fieldDetails.FieldNameByCropYear[ApplicationEnvironment.CurrentCropYear];//.Name;
                        includedFieldItem.Farm = czItem.FarmName;
                        includedFieldItem.Field = czItem.FieldName;
                        includedFieldItem.Crop = czItem.CropName;
                        var area = czDetailsMaybe.Value.ReportedArea != null ? czDetailsMaybe.Value.ReportedArea.Value : czDetailsMaybe.Value.BoundaryArea != null ? czDetailsMaybe.Value.BoundaryArea.Value : 0.0;
                        var areaUnit = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit);
                        includedFieldItem.Area = string.Format("{0} {1}", area.ToString("N2"), areaUnit.AbbreviatedDisplay);
                        reportModel.IncludedFields.Add(includedFieldItem);
                    }
                }

                LoadTicket report = new LoadTicket();
                report.DataSource = reportModel;

                book.Reports.Add(report);
            }

            rs.ReportDocument = book;
            return rs;
        }
    }
}
