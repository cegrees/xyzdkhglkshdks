﻿using AgC.UnitConversion;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Inventory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Secondary.Reports.Yield.Generators
{
    public class SharedYieldMethods
    {

        public double GetAvg(CropZoneApplicationDataItem item, Dictionary<ProductId, InventoryListItem> inventoryList, MiniProduct prod)
        {
            //var masterlist = clientEndpoint.GetMasterlistService();
            if (inventoryList.ContainsKey(item.ProductId))
            {
                try
                {
                    var mlp = prod;
                    var totalMeasure = UnitFactory.GetPackageSafeUnit(item.TotalProductUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit).GetMeasure(item.TotalProductValue, mlp.Density);
                    var avgPriceUnit = string.IsNullOrEmpty(inventoryList[item.ProductId].AveragePriceUnit) ? null : UnitFactory.GetPackageSafeUnit(inventoryList[item.ProductId].AveragePriceUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                    var convertedTotalMeasureValue = totalMeasure.CanConvertTo(avgPriceUnit) ? totalMeasure.GetValueAs(avgPriceUnit) : totalMeasure.Value;
                    return Math.Round(inventoryList[item.ProductId].AveragePriceValue * convertedTotalMeasureValue, 2); // TODO: This will only pan out if the units of these measures is the same. Need to take this into account.
                }
                catch (Exception ex) { return 0.0; }
            }
            else
            {
                return 0.0;
            }
        }

        public double GetSpecificCost(CropZoneApplicationDataItem czAppRecord, ApplicationView view, MiniProduct prod)
        {
            var appProduct = view.Products.SingleOrDefault(x => x.TrackingId == czAppRecord.ProductTrackingId);
            var totalSpecific = 0.0;

            if (appProduct.SpecificCostPerUnit.HasValue && !string.IsNullOrEmpty(appProduct.SpecificCostUnit))
            {
                var mlp = prod;
                var avgPriceUnit = UnitFactory.GetPackageSafeUnit(appProduct.SpecificCostUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                var totalProductUnit = UnitFactory.GetPackageSafeUnit(czAppRecord.TotalProductUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                var totalMeasure = totalProductUnit.GetMeasure(czAppRecord.TotalProductValue, mlp.Density);
                var converted = totalMeasure.Unit != avgPriceUnit && totalMeasure.CanConvertTo(avgPriceUnit) ? totalMeasure.CreateAs(avgPriceUnit) : totalMeasure;
                totalSpecific = converted.Value * (double)appProduct.SpecificCostPerUnit.Value;
            }

            return totalSpecific;
        }
	}
}
