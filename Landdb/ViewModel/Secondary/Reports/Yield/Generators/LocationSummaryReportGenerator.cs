﻿using Landdb.Client.Infrastructure;
using Landdb.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using Telerik.Reporting;
using Landdb.Domain.ReadModels.YieldLocation;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Domain.ReadModels.Yield;
using Landdb.ViewModel.Yield;
using Landdb.ReportModels.Yield;
using Landdb.Resources;

namespace Landdb.ViewModel.Secondary.Reports.Yield.Generators
{
    public class LocationSummaryReportGenerator : IYieldReportGenerator
    {
        readonly IClientEndpoint clientEndpoint;
        readonly Dispatcher dispatcher;

        readonly int cropYear;

        public LocationSummaryReportGenerator(IClientEndpoint clientEndpoint, Dispatcher dispatcher, int cropYear)
        {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            this.cropYear = cropYear;
        }

        public Telerik.Reporting.ReportSource Generate( YieldQueryCriteria criteria)
        {
            ReportBook book = new ReportBook();
            var rs = new InstanceReportSource();

            var dsId = new DataSourceId(ApplicationEnvironment.CurrentDataSourceId);
            var currentCropYearId = new CropYearId(dsId.Id, cropYear);

            var dataSourceName = ApplicationEnvironment.CurrentDataSourceName;
            var flattenedHierarchyView = clientEndpoint.GetView<FlattenedTreeHierarchyView>(currentCropYearId).GetValue(new FlattenedTreeHierarchyView());
            var LocationList = clientEndpoint.GetView<YieldLocationListView>(dsId).GetValue(new YieldLocationListView());

            List<LocationSummaryData> LocationSummaryData = new List<LocationSummaryData>();

            foreach (var location in LocationList.YieldLocations)
            {
                CommodityDetailList = new List<IncludedCommodityDetails>();
                //GET LOCATION DETAILS VIEW MODEL
                var localDetials = clientEndpoint.GetView<YieldLocationDetailsView>(location.Id).Value;

				IncomingLoads = localDetials.IncomingLoadsByCropYear.ContainsKey(cropYear) ? localDetials.IncomingLoadsByCropYear[cropYear] : new List<IncludedLoad>();
				OutgoingLoads = localDetials.OutgoingLoadsByCropYear.ContainsKey(cropYear) ? localDetials.OutgoingLoadsByCropYear[cropYear] : new List<IncludedLoad>();

                calculateCommodityBalances(localDetials);

                var hasParent = true;
                YieldLocationId parentLocationID = location.ParentLocationId;
                string fullName = location.Name;
                while(hasParent == true)
                {
                    if (parentLocationID != null)
                    {
                        var currentItem = LocationList.YieldLocations.FirstOrDefault(x => x.Id == parentLocationID);
                        fullName = string.Format("{0} / {1}", currentItem.Name, fullName);
                        parentLocationID = currentItem.ParentLocationId;
                    }
                    hasParent = parentLocationID != null ? true : false;
                }

                if (CommodityDetailList.Count() > 0)
                {
                    foreach (var commodity in CommodityDetailList)
                    {
                        var summaryData = new LocationSummaryData();
                        summaryData.Commodity = commodity.CommodityDisplay;
                        summaryData.CropYear = cropYear;
                        summaryData.DataSourceName = dataSourceName;
                        summaryData.LocationID = localDetials.Id.Id;
                        summaryData.LocationName = fullName;//localDetials.Name;
                        summaryData.LocationType = localDetials.YieldLocationType.ToString();
                        summaryData.RemainingUnit = commodity.QuantityUnit.AbbreviatedDisplay;
                        summaryData.RemainingValue = commodity.QuantityValue;
                        LocationSummaryData.Add(summaryData);
                    }
                }
            }

            if (LocationSummaryData.Count() > 0)
            {
                LocationSummaryData = LocationSummaryData.OrderBy(x => x.LocationName).ThenBy(y => y.Commodity).ToList();

                var report = new Landdb.Views.Secondary.Reports.Yield.LocationSummaryReport();
                report.DataSource = LocationSummaryData;
                report.Name = Strings.ReportName_LocationSummary_Text;

                book.Reports.Add(report);
            }

            rs.ReportDocument = book;

            return rs;
        }

        public string DisplayName
        {
            get { return Strings.GeneratorName_LocationSummary_Text; }
        }

        public IEnumerable<IncludedLoad> IncomingLoads { get; private set; }
        public IEnumerable<IncludedLoad> OutgoingLoads { get; private set; }

        public List<IncludedCommodityDetails> CommodityDetailList { get; private set; }

        private void calculateCommodityBalances(YieldLocationDetailsView locationDetails)
        {
			var currentCropYearId = new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear);

			var flattenedTreeView = clientEndpoint.GetView<FlattenedTreeHierarchyView>(currentCropYearId).GetValue(() => null);
			var locationDictionary = YieldLocationListItemViewModel.GetFlattenedListItemsForCropYear(clientEndpoint, currentCropYearId, false)
				.ToDictionary(x => x.Id, x => x.FullDisplay);

			var loadsForThisYear = IncomingLoads.Union(OutgoingLoads).Where(x => x.CropYear == cropYear);

            // build a list of all commodities across all loads in this cropyear
            var commodityDisplayList = from load in loadsForThisYear
                                       group load by load.Commodity into g
                                       orderby g.Count() descending
                                       select g.Key;

            foreach (var commodity in commodityDisplayList)
            {
                var loadsForThisCommodity = loadsForThisYear.Where(x => x.Commodity == commodity);

                // determine the target unit by finding the most commonly used unit across all loads of this crop
                var targetUnit = (from load in loadsForThisCommodity
                                  group load by load.QuantityUnit into g
                                  orderby g.Count() descending
                                  select g.Key).FirstOrDefault();

                decimal balance = 0;

                // add incoming loads of this crop to the balance
                var incomingLoadsForThisCrop = IncomingLoads.Where(x => loadsForThisCommodity.Contains(x));
                foreach (var load in incomingLoadsForThisCrop)
                {
                    if (load.QuantityUnit == targetUnit)
                    {
                        balance += load.LocationWeightedFinalQuantity; //.FinalQuantityValue;
                    }
                    else if (CompositeUnitConverter.CanConvert(load.QuantityUnit, targetUnit))
                    {
                        balance += CompositeUnitConverter.ConvertValue(load.LocationWeightedFinalQuantity, load.QuantityUnit, targetUnit);
                        //balance += CompositeUnitConverter.ConvertValue(load.FinalQuantityValue, load.QuantityUnit, targetUnit);
                    }
                }

                // subtract outgoing loads of this crop from the balance
                var outgoingLoadsForThisCrop = OutgoingLoads.Where(x => loadsForThisCommodity.Contains(x));
                foreach (var load in outgoingLoadsForThisCrop)
                {
                    if (load.QuantityUnit == targetUnit)
                    {
                        balance -= load.DeliveredQuantityValue;
                    }
                    else if (CompositeUnitConverter.CanConvert(load.QuantityUnit, targetUnit))
                    {
                        balance -= CompositeUnitConverter.ConvertValue(load.DeliveredQuantityValue, load.QuantityUnit, targetUnit);
                    }
                }

                var saleAmount = locationDetails.YieldLocationType == YieldLocationTypes.Sale 
					? incomingLoadsForThisCrop.Where(x => x.IsLoadSold).Sum(x => x.GrowerSalePrice + x.LandOwnerSalePrice) 
					: (decimal?)null;

				CommodityDetailList.Add(new IncludedCommodityDetails(commodity.FullCommodityDisplayText, balance, targetUnit, saleAmount) {
					// delay loading these somehow? 45s to load on miles farms loc with ~1500 loads

					IncomingLoads = incomingLoadsForThisCrop
						.OrderBy(x => x.StartTime)
						.ThenBy(x => x.LoadNumber)
						.SelectMany(x => IncludedLoadViewModel.GetListFromIncludedLoad(flattenedTreeView, locationDictionary, x))
						.ToList(),

					OutgoingLoads = outgoingLoadsForThisCrop
						.OrderBy(x => x.StartTime)
						.ThenBy(x => x.LoadNumber)
						.SelectMany(x => IncludedLoadViewModel.GetListFromIncludedLoad(flattenedTreeView, locationDictionary, x))
						.ToList(),
                });
            }
        }
    }
}
