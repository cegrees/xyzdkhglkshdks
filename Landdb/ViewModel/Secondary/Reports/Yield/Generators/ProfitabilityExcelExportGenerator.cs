﻿using AgC.UnitConversion;
using ExportToExcel;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Contract;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Domain.ReadModels.Invoice;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Domain.ReadModels.Yield;
using Landdb.Domain.ReadModels.YieldLocation;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ReportModels.Yield;
using Landdb.ViewModel.Yield;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Secondary.Reports.Yield.Generators
{
    public class ProfitabilityExcelExportGenerator: IYieldReportGenerator
    {

        readonly IClientEndpoint clientEndpoint;
        readonly Dispatcher dispatcher;

        readonly int cropYear;

        public ProfitabilityExcelExportGenerator(IClientEndpoint clientEndpoint, Dispatcher dispatcher, int cropYear)
        {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            this.cropYear = cropYear;
        }

        public Telerik.Reporting.ReportSource Generate(YieldQueryCriteria criteria)
        {
            var currentCropYearId = new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear);

			var masterlist = clientEndpoint.GetMasterlistService();
			var cropZoneApplications = clientEndpoint.GetView<CropZoneApplicationDataView>(currentCropYearId).GetValue(new CropZoneApplicationDataView()).Items;
			var inventoryList = clientEndpoint.GetView<InventoryListView>(currentCropYearId).GetValue(new InventoryListView()).Products;
			var flattenedHierarchyView = clientEndpoint.GetView<FlattenedTreeHierarchyView>(currentCropYearId).GetValue(new FlattenedTreeHierarchyView());
			var loadList = clientEndpoint.GetView<CropZoneLoadDataView>(currentCropYearId).GetValue(new CropZoneLoadDataView()).Items;
			var commodityList = clientEndpoint.GetView<CommodityListView>(currentCropYearId).GetValue(new CommodityListView()).Commodities;
			var yieldLocations = clientEndpoint.GetView<YieldLocationListView>(new DataSourceId(currentCropYearId.DataSourceId)).GetValue(() => new YieldLocationListView()).YieldLocations;
			var contractedCropZoneList = clientEndpoint.GetView<CropZoneRentContractView>(currentCropYearId).GetValue(() => new CropZoneRentContractView()).CropZoneRentContracts;

			//get unique list of crops from selected crop zones and then roll this stuff into CommoditySummaryViewModel....
			if (criteria.SelectedCropZones.Any())
            {
                var data = new List<ProfitabilityExcelItem>();

                List<CropPrice> priceContainer = new List<CropPrice>();
                foreach (var commodity in commodityList)
                {
                    CommoditySummaryViewModel csViewModel = new CommoditySummaryViewModel(clientEndpoint, dispatcher, currentCropYearId, commodity.CropId, commodity.CommodityDescription);

                    foreach (var summary in csViewModel.UnitSummaries)
                    {
                        priceContainer.Add(new CropPrice()
                        {
                            CropID = commodity.CropId,
                            CommodityDescription = commodity.CommodityDescription,
                            AvgPrice = summary.Value.GrowerAverageRevenue,
                            AvgPriceUnit = summary.Value.QuantityUnit
                        });
                    }
                }

                foreach (var czId in criteria.SelectedCropZones)
                {
                    var profItem = new ProfitabilityExcelItem();
                    List<ProfitabilityExcelItem> multipleCommodities = new List<ProfitabilityExcelItem>();
                    var czDetails = clientEndpoint.GetView<CropZoneDetailsView>(czId).Value;
                    var czItem = flattenedHierarchyView.Items.FirstOrDefault(p => p.CropZoneId == czId);
                    var contract = clientEndpoint.GetView<CropZoneRentContractView>(currentCropYearId);

                    profItem.DataSourceName = criteria.SelectedEntity != null ? criteria.SelectedEntity.Name : ApplicationEnvironment.CurrentDataSourceName;
                    //profItem.DateRange = string.Format("{0} - {1}", criteria.ReportStartDate.ToShortDateString(), criteria.ReportEndDate.ToShortDateString());
                    profItem.Farm = czItem.FarmName;
                    profItem.Field = czItem.FieldName;
                    profItem.Crop = masterlist.GetCropDisplay(czItem.CropId);
                    profItem.Area = (decimal)(czDetails.ReportedArea != null ? czDetails.ReportedArea : czDetails.BoundaryArea != null ? czDetails.BoundaryArea : 0.0);
                    profItem.CropYear = cropYear.ToString();
                    profItem.CropZone = czDetails.Name;
                    profItem.Tags = string.Join(" + ", czDetails.Tags.ToArray());
                    profItem.DataSourceName = ApplicationEnvironment.CurrentDataSourceName;
                    var productUnits = clientEndpoint.GetProductUnitResolver();
                    var czAppItems = cropZoneApplications.Where(x => x.CropZoneId == czId);
                    List<string> varieties = new List<string>();

                    decimal _rentCost = 0m;
                    foreach (var contractitem in czDetails.Contracts)
                    {
                        //var rentContract = czDetails.Contracts.Any() ? czDetails.Contracts[0].GetType() : null;
                        var contractMaybe = clientEndpoint.GetView<RentContractDetailsView>(contractitem.Id);
                        if (contractMaybe.HasValue)
                        {
                            var contractValue = contractMaybe.Value;
                            var rentPerArea = contractValue.PerAcre;
                            var totalRent = (decimal)rentPerArea * profItem.Area;
                            profItem.Cost += totalRent;
                        }
                    }

                    foreach (var item in czAppItems)
                    {
                        var mlp = masterlist.GetProduct(item.ProductId);
                        var appDetails = clientEndpoint.GetView<ApplicationView>(item.ApplicationId).Value;

                        if (mlp.ProductType == GlobalStrings.ProductType_Seed)
                        {
                            varieties.Add(mlp.Name);
                        }

                        switch (criteria.CostType){
                            case "Average":
                                var mp = mlp;
                                var totalCost = GetAvg(item, inventoryList);
                                profItem.Cost += (decimal)totalCost * (decimal)item.GrowerShare;
                                break;
                            case "Invoice":
                                
                                //get invoice
                                var source = appDetails.Sources.Where(x => x.DocumentType == "Invoice");
                                double invoiceCostPerUnit = 0.0;
                                int cnt = 0;
                                if (source.Count() > 0)
                                {
                                    foreach (var src in source)
                                    {
                                        var invoiceDetailmaybe = clientEndpoint.GetView<InvoiceView>(src.Identity as InvoiceId);
                                        if (invoiceDetailmaybe.HasValue)
                                        {
                                            var invoiceDetail = invoiceDetailmaybe.Value;
                                            var invoiceProducts = (from p in invoiceDetail.Products
                                                                   where p.ProductId == item.ProductId
                                                                   select p).ToList();
                                            var containsKey = inventoryList.ContainsKey(item.ProductId);

                                            if (containsKey)
                                            {
                                                var product = inventoryList[item.ProductId];
                                                var fallbackUnit = invoiceProducts.Any() ? UnitFactory.GetUnitByName(invoiceProducts[0].TotalProductUnit) : UnitFactory.GetPackageSafeUnit(item.TotalProductUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                                                IUnit unit = !string.IsNullOrEmpty(product.AveragePriceUnit) ? UnitFactory.GetPackageSafeUnit(inventoryList[item.ProductId].AveragePriceUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit) : fallbackUnit;  //.GetUnitByName(inventory.Products[czAppRecord.ProductId].AveragePriceUnit) : fallbackUnit;

                                                foreach (var prod in invoiceProducts)
                                                {
                                                    var measurePreConvertUnit = UnitFactory.GetPackageSafeUnit(prod.TotalProductUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                                                    var measurePreConvert = measurePreConvertUnit.GetMeasure((double)prod.TotalProductValue, mlp.Density);
                                                    var canConvert = measurePreConvert.CanConvertTo(unit);
                                                    var convertedMeasure = measurePreConvert.CanConvertTo(unit) && measurePreConvert.Unit != unit ? unit.GetMeasure(measurePreConvert.GetValueAs(unit)) : measurePreConvert;
                                                    invoiceCostPerUnit += convertedMeasure.Value != 0 ? (double)(prod.TotalCost / (decimal)convertedMeasure.Value) : (double)prod.TotalCost;
                                                    cnt++;
                                                }
                                            }
                                        }
                                    }

                                    if (inventoryList.ContainsKey(item.ProductId))
                                    {
                                        try
                                        {
                                            var totalMeasure = UnitFactory.GetUnitByName(item.TotalProductUnit).GetMeasure(item.TotalProductValue, masterlist.GetProduct(item.ProductId).Density);
                                            var avgPriceUnit = string.IsNullOrEmpty(inventoryList[item.ProductId].AveragePriceUnit) ? null : UnitFactory.GetUnitByName(inventoryList[item.ProductId].AveragePriceUnit);
                                            var convertedTotalMeasureValue = totalMeasure.CanConvertTo(avgPriceUnit) ? totalMeasure.GetValueAs(avgPriceUnit) : totalMeasure.Value;
                                            var totalInvCost = invoiceCostPerUnit != 0.0 ? invoiceCostPerUnit / cnt * convertedTotalMeasureValue : GetAvg(item, inventoryList);
                                            profItem.Cost += (decimal)(totalInvCost * item.GrowerShare);
                                        }
                                        catch
                                        {
                                            //do nothing
                                        }
                                    }
                                }
                                else
                                {
                                    var avgCost = GetAvg(item, inventoryList);
                                    profItem.Cost += (decimal)(avgCost * item.GrowerShare);
                                }
                                break;
                            case "Specific":
                                var totalSpecific = GetSpecificCost(item, appDetails);
                                var appProduct = appDetails.Products.SingleOrDefault(x => x.TrackingId == item.ProductTrackingId);
                                if (appProduct != null && !string.IsNullOrEmpty(appProduct.SpecificCostUnit))
                                {
                                    profItem.Cost += (decimal)(totalSpecific * item.GrowerShare);
                                }
                                else
                                {
                                    profItem.Cost += (decimal)(GetAvg(item, inventoryList) * item.GrowerShare);
                                }
                                break;
                            default:
                                break;

                    }
                }

                    profItem.Variety = string.Join(" : ", varieties.ToArray());

					var locationsExcludedFromInventory = from loc in yieldLocations
														 where loc.IsActiveInCropYear(currentCropYearId.Id) && loc.IsQuantityExcludedFromInventory
														 select loc.Id.Id;

					var czLoads = from load in loadList
								  from loc in load.DestinationLocations
                                  where load.CropZoneId == czId 
									&& criteria.ReportStartDate <= load.StartDateTime 
									&& criteria.ReportEndDate.AddHours(23 - criteria.ReportEndDate.Hour) >= load.StartDateTime
									&& !locationsExcludedFromInventory.Contains(loc.Id.Id)
                                  select load;

                    foreach (var load in czLoads)
                    {
                        ProfitabilityExcelItem item = new ProfitabilityExcelItem();
                        item.Area = profItem.Area;
                        item.Cost = profItem.Cost;
                        item.Crop = profItem.Crop;
                        item.Tags = profItem.Tags;
                        item.CropZone = profItem.CropZone;
                        item.CropYear = profItem.CropYear;
                        item.DataSourceName = profItem.DataSourceName;
                        item.Farm = profItem.Farm;
                        item.Field = profItem.Field;
                        item.Variety = profItem.Variety;
                        item.TotalYieldValue = load.AreaWeightedFinalQuantity;
                        item.TotalYieldValue = (load.AreaWeightedFinalQuantity / load.DestinationLocations.Count);
                        item.TotalYieldUnit = load.FinalQuantityUnit;

                        var prices = priceContainer.Where(x => x.CropID == czItem.CropId).ToList();
                        if (prices.Any())
                        {
                            if (load.AreaWeightedGrowerPrice > 0)
                            {
                                item.Income += load.AreaWeightedGrowerPrice;
                            }
                            else
                            {
                                var commodityPrices = from u in prices
                                                      where u.AvgPriceUnit == item.TotalYieldUnit && u.CommodityDescription == load.Commodity.CommodityDescription
                                                      select u;
                                if (commodityPrices.Any() && commodityPrices.FirstOrDefault().AvgPriceUnit != null && CompositeUnitConverter.CanConvert(item.TotalYieldUnit, commodityPrices.First().AvgPriceUnit))
                                {
                                    var priceUnit = commodityPrices.First().AvgPriceUnit;
                                    var priceVal = commodityPrices.First().AvgPrice;

                                    var newValue = CompositeUnitConverter.ConvertValue((load.AreaWeightedFinalQuantity / load.DestinationLocations.Count), item.TotalYieldUnit, priceUnit);
                                    item.Income += newValue * priceVal;
                                }
                            }
                        }

                        if (multipleCommodities.Count() > 0)
                        {
                            bool canConvert = false;
                            foreach (var selectedCommodity in multipleCommodities)
                            {
                                if (CompositeUnitConverter.CanConvert(item.TotalYieldUnit, selectedCommodity.TotalYieldUnit))
                                {
                                    var convertedValue = CompositeUnitConverter.ConvertValue(item.TotalYieldValue, item.TotalYieldUnit, selectedCommodity.TotalYieldUnit);
                                    selectedCommodity.TotalYieldValue += convertedValue;
                                    selectedCommodity.Income += item.Income;
                                    canConvert = true;
                                }
                            }

                            if (canConvert == false)
                            {
                                multipleCommodities.Add(item);
                            }
                        }
                        else
                        {
                            multipleCommodities.Add(item);
                        }
                    }

                    decimal cropMultiplier = 1m;
                    if (contractedCropZoneList.ContainsKey(czId.Id))
                    {
                        //gotta ask about how to deal with multiple rent contracts on a cropzone
                        // answer: by this point, the cropzonerentcontract readmodel has already dealt with this
                        // current logic mandates that the contract with the most recent start date is what takes priority.
                        cropMultiplier = contractedCropZoneList[czId.Id].GrowerCropShare;
                        profItem.TotalYieldValue = profItem.TotalYieldValue * cropMultiplier;
                        profItem.Income = profItem.Income * cropMultiplier;
                        //profItem.Cost += contractedCropZoneList[czId.Id].CashRentPerArea * profItem.Area;
                    }

                    multipleCommodities.ForEach(i => i.Cost = i.Cost / (decimal)multipleCommodities.Count());
                    multipleCommodities.ForEach(p => p.Income = p.Income);
                    multipleCommodities.ForEach(s => s.TotalYieldValue = s.TotalYieldValue);
                    multipleCommodities.ForEach(c => c.NetIncome = c.Income - c.Cost);
                    multipleCommodities.ForEach(n => n.NetIncomePerArea = n.NetIncome / n.Area);
                    multipleCommodities.ForEach(a => a.AvgYieldValue = a.TotalYieldValue / a.Area);
                    multipleCommodities.ForEach(u => u.AvgYieldUnit = u.TotalYieldUnit);

                    data.AddRange(multipleCommodities);
                }

                data = data.OrderBy(f => f.Farm).ThenBy(c => c.Crop).ToList();

                //send data to excel
                System.Data.DataTable dt1 = new System.Data.DataTable();

                dt1 = ExportToExcel.CreateExcelFile.ListToDataTable(data, true);

                dt1.TableName = "Profitability";

                DataSet ds = new DataSet("excelDS");
                ds.Tables.Add(dt1);

                string filename = string.Empty;
                try
                {
                    //open up file dialog to save file....
                    //then call createexcelfile to create the excel...
                    SaveFileDialog saveFileDialog1 = new SaveFileDialog();

                    saveFileDialog1.Filter = "Excel|*.xlsx";
                    saveFileDialog1.FilterIndex = 2;
                    saveFileDialog1.RestoreDirectory = true;

                    Nullable<bool> resultSaved = saveFileDialog1.ShowDialog();

                    // Process save file dialog box results 
                    if (resultSaved == true)
                    {
                        // Save document 
                        filename = saveFileDialog1.FileName;
                        CreateExcelFile.CreateExcelDocument(ds, filename);
                    }

                    //now open file....
                    System.Diagnostics.Process.Start(filename);

                    Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
                }
                catch (Exception ex)
                {
                    Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
                    return null;
                }
            }
            return null;
        }


		public double GetAvg(CropZoneApplicationDataItem item, Dictionary<ProductId, InventoryListItem> inventoryList)
        {
            var masterlist = clientEndpoint.GetMasterlistService();
            if (inventoryList.ContainsKey(item.ProductId))
            {
                try
                {
                    var mlp = masterlist.GetProduct(item.ProductId);
                    var totalMeasure = UnitFactory.GetPackageSafeUnit(item.TotalProductUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit).GetMeasure(item.TotalProductValue, mlp.Density);
                    var avgPriceUnit = string.IsNullOrEmpty(inventoryList[item.ProductId].AveragePriceUnit) ? null : UnitFactory.GetPackageSafeUnit(inventoryList[item.ProductId].AveragePriceUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                    var convertedTotalMeasureValue = totalMeasure.CanConvertTo(avgPriceUnit) ? totalMeasure.GetValueAs(avgPriceUnit) : totalMeasure.Value;
                    return Math.Round(inventoryList[item.ProductId].AveragePriceValue * convertedTotalMeasureValue, 2); // TODO: This will only pan out if the units of these measures is the same. Need to take this into account.
                }
                catch (Exception ex) { return 0.0; }
            }
            else
            {
                return 0.0;
            }
        }

        public double GetSpecificCost(CropZoneApplicationDataItem czAppRecord, ApplicationView view)
        {
            var appProduct = view.Products.SingleOrDefault(x => x.TrackingId == czAppRecord.ProductTrackingId);
            var totalSpecific = 0.0;

            if (appProduct.SpecificCostPerUnit.HasValue && !string.IsNullOrEmpty(appProduct.SpecificCostUnit))
            {
                var masterlist = clientEndpoint.GetMasterlistService();
                var mlp = masterlist.GetProduct(czAppRecord.ProductId);
                var avgPriceUnit = UnitFactory.GetPackageSafeUnit(appProduct.SpecificCostUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                var totalProductUnit = UnitFactory.GetPackageSafeUnit(czAppRecord.TotalProductUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                var totalMeasure = totalProductUnit.GetMeasure(czAppRecord.TotalProductValue, mlp.Density);
                var converted = totalMeasure.Unit != avgPriceUnit && totalMeasure.CanConvertTo(avgPriceUnit) ? totalMeasure.CreateAs(avgPriceUnit) : totalMeasure;
                totalSpecific = converted.Value * (double)appProduct.SpecificCostPerUnit.Value;
            }

            return totalSpecific;
        }

        public string DisplayName
        {
            get { return Strings.GeneratorName_ProfitabilityExcel_Text; }
        }
    }
}
