﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Company;
using Landdb.Domain.ReadModels.Person;
using Landdb.Infrastructure;
using Landdb.ViewModel.Secondary.Reports.AppliedProduct;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Secondary.Reports.Yield
{
    public class YieldReportInfoViewModel : ViewModelBase
    {
        DateTime reportStartDate;
        DateTime reportEndDate;

        
        int cropYear;
        EntityItem selectedEntity;
        UserSettings settings;
        IClientEndpoint endPoint;

        public YieldReportInfoViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, int cropYear)
        {
            this.endPoint = clientEndpoint;
            this.cropYear = cropYear;

            DataSourceId dsID = new DataSourceId(ApplicationEnvironment.CurrentDataSourceId);
            var companiesMaybe = endPoint.GetView<CompanyListView>(dsID);
            var peopleMaybe = endPoint.GetView<PersonListView>(dsID);
            Entities = new List<EntityItem>();
            if (companiesMaybe.HasValue)
            {
                var entities = from c in companiesMaybe.Value.Companies
                               where !c.RemovedCropYear.HasValue || !(c.RemovedCropYear.Value <= ApplicationEnvironment.CurrentCropYear)
                               select new EntityItem() { Name = c.Name, Id = c.Id.Id, Type = "Company" };
                Entities.AddRange(entities);
            }

            if (peopleMaybe.HasValue)
            {
                var peopleEntities = from p in peopleMaybe.Value.Persons
                                     where !p.RemovedCropYear.HasValue || !(p.RemovedCropYear.Value <= ApplicationEnvironment.CurrentCropYear)
                                     select new EntityItem() { Name = p.Name, Id = p.Id.Id, Type = "Person" };
                Entities.AddRange(peopleEntities);
            }

            Entities = Entities.OrderBy(n => n.Name).ToList();
            Entities.Add(new EntityItem() { Id = new Guid(), Name = string.Empty, Type = "Empty" });
            //initialize to previously used entity and date...
            settings = endPoint.GetUserSettings();
            if (settings.LastReportHeaderEntityId != null)
            {
                selectedEntity = Entities.SingleOrDefault(x => x.Id == settings.LastReportHeaderEntityId);
            }
            //////////////////////////////////////////

            InitializeProperties();

            CostCommand = new RelayCommand<string>(this.Cost);
        }

        
        public ICommand CostCommand { get; set; }

        public List<EntityItem> Entities { get; set; }
        public EntityItem Entity
        {
            get { return selectedEntity; }
            set
            {
                selectedEntity = value;

                //save selected item to user settings
                settings.LastReportHeaderEntityId = selectedEntity.Id;
                endPoint.SaveUserSettings();
            }
        }

        public bool IsAvgChecked { get; set; }
        public bool IsSpecificChecked { get; set; }
        public bool IsInvoiceChecked { get; set; }
        public string CostType { get; set; }
        public void Cost(string type)
        {
            switch (type)
            {
                case "Average":
                    //set pricing to Average
                    CostType = Strings.CostType_Average_Text;
                    IsAvgChecked = true;
                    IsSpecificChecked = false;
                    IsInvoiceChecked = false;

                    break;
                case "Specific":
                    //setpricing to specific
                    CostType = Strings.CostType_Specific_Text;
                    IsAvgChecked = false;
                    IsSpecificChecked = true;
                    IsInvoiceChecked = false;

                    break;
                case "Invoice":
                    //set pricing to invoice
                    CostType = Strings.CostType_Invoice_Text;
                    IsAvgChecked = false;
                    IsSpecificChecked = false;
                    IsInvoiceChecked = true;

                    break;
                case "NoPrice":
                    CostType = Strings.CostType_None_Text;
                    break;
                default:
                    //set to average
                    CostType = Strings.CostType_Average_Text;
                    IsAvgChecked = true;
                    IsSpecificChecked = false;
                    IsInvoiceChecked = false;
                    break;
            }
            RaisePropertyChanged("CostType");
            RaisePropertyChanged("IsAvgChecked");
            RaisePropertyChanged("IsSpecificChecked");
            RaisePropertyChanged("IsInvoiceChecked");
        }

        public DateTime ReportStartDate
        {
            get { return reportStartDate; }
            set
            {
                reportStartDate = value;

                if (reportStartDate != settings.LastUsedReportStartDate)
                {
                    settings.LastUsedReportStartDate = reportStartDate;
                    endPoint.SaveUserSettings();
                }

                RaisePropertyChanged("ReportStartDate");
            }
        }

        public DateTime ReportEndDate
        {
            get { return reportEndDate; }
            set
            {
                reportEndDate = value;

                //if (reportEndDate != settings.LastUsedReportEndDate)
                //{
                //    settings.LastUsedReportEndDate = reportEndDate;
                //    endPoint.SaveUserSettings();
                //}

                RaisePropertyChanged("ReportEndDate");
            }
        }

        public void InitializeProperties()
        {
            //set date times
            if (settings.LastUsedReportStartDate != null && settings.LastUsedReportStartDate != new DateTime())
            {
                ReportStartDate = settings.LastUsedReportStartDate;
            }
            else
            {
                ReportStartDate = new DateTime(cropYear, 1, 1);
            }

            //if (settings.LastUsedReportEndDate != null && settings.LastUsedReportEndDate != new DateTime())
            //{
            //    ReportEndDate = settings.LastUsedReportEndDate;
            //}
            //else
            //{
            //    if (DateTime.Today.Year == cropYear)
            //    {
            //        ReportEndDate = DateTime.Today;
            //    }
            //    else
            //    {
            //        ReportEndDate = new DateTime(cropYear, 12, 31);
            //    }
            //}

            //BUG 1134 REQUEST TO SET END DATE TO NOW
            ReportEndDate = DateTime.Now;

            Cost("Average");
        }
    }
}
