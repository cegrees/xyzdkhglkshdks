﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Converters.Reports;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Fields;
using Landdb.ViewModel.Reports;
using Landdb.ViewModel.Secondary.Reports.AppliedProduct;
using Landdb.ViewModel.Secondary.Reports.Yield.Generators;
using Microsoft.Win32;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using Telerik.Reporting;
using Telerik.Reporting.Processing;

namespace Landdb.ViewModel.Secondary.Reports.Yield {
    public class YieldReportCreatorViewModel : ViewModelBase {
        IYieldReportGenerator selectedReportGenerator;
        ReportTemplateViewModel template;
        ReportTemplateViewModelToFile templateService = new ReportTemplateViewModelToFile();
        string reportType;
        YieldQueryCriteria criteria;
        Dispatcher dispatch;

        public YieldReportCreatorViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, int cropYear, ReportTemplateViewModel template, string reportType) {
            this.dispatch = dispatcher;
            this.dispatch = dispatcher;
            CancelCommand = new RelayCommand(OnCancelReport);
            SaveCommand = new RelayCommand(SaveTemplate);

            FieldsPageModel = new AppliedProductReportFieldSelectionViewModel(clientEndpoint, dispatcher, cropYear);
            InfoPageModel = new YieldReportInfoViewModel(clientEndpoint, dispatcher, cropYear);

            ReportGenerators = new ObservableCollection<IYieldReportGenerator>();
            //Call to add all the Applied Product Reports
            AddYieldReportGenerators(clientEndpoint, cropYear);

            //SelectedReportGenerator = ReportGenerators.First();
            PDFExport = new RelayCommand(this.PDF);

            UpdateReport();
            this.reportType = reportType;
            //take passed in template and set values on associated ViewModels along with Criteria page
            if (template != null) { InitializeFromTemplate(template); this.template = template; };

            SelectedPageIndex = 0;
        }
        public ICommand CancelCommand { get; private set; }
        public RelayCommand PDFExport { get; set; }
        public ICommand SaveCommand { get; set; }

        public int CropYear { get { return ApplicationEnvironment.CurrentCropYear; } }
        public int SelectedPageIndex { get; set; }
        public ReportSource ReportSource { get; set; }

        public string TemplateName { get; set; }

        public AppliedProductReportFieldSelectionViewModel FieldsPageModel { get; private set; }
        public YieldReportInfoViewModel InfoPageModel { get; private set; }

        public ObservableCollection<IYieldReportGenerator> ReportGenerators { get; private set; }
        public IYieldReportGenerator SelectedReportGenerator {
            get { return selectedReportGenerator; }
            set {
                selectedReportGenerator = value;
                UpdateReport();
                RaisePropertyChanged("SelectedReportGenerator");
            }
        }

        void OnCancelReport() {
            Messenger.Default.Send<ShowMainViewMessage>(new ShowMainViewMessage());
        }

        public void UpdateReport() {
            if (SelectedReportGenerator != null) {
                criteria = BuildCriteria();
                ReportSource = SelectedReportGenerator.Generate(criteria);
                RaisePropertyChanged("ReportSource");
            }
        }

        public void PDF() {
            try { 
            if (ReportSource == null) return;
            ReportSource reportExport = ReportSource;

            ReportProcessor reportProcessor = new ReportProcessor();
            System.Collections.Hashtable deviceInfo =
            new System.Collections.Hashtable();
            RenderingResult result = reportProcessor.RenderReport("PDF", reportExport, deviceInfo);

            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "Adobe Reader|*.pdf";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;

            Nullable<bool> resultSaved = saveFileDialog1.ShowDialog();

            // Process save file dialog box results 
            if (resultSaved == true) {
                // Save document 
                string filename = saveFileDialog1.FileName;

                try
                {
                    using (System.IO.FileStream fs = new System.IO.FileStream(filename, System.IO.FileMode.Create))
                    {
                        fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
                    }

                    System.Diagnostics.Process.Start(filename);
                }
                catch (Exception ex)
                {

                }
            }
            }
            catch (Exception ex)
            {
                //do nothing
            }
        }

        void AddYieldReportGenerators(IClientEndpoint clientEndpoint, int cropYear)
        {
            ReportGenerators.Add(new LoadDetailsReportGenerator(clientEndpoint, dispatch, cropYear));
            ReportGenerators.Add(new ProfitabilityReportGenerator(clientEndpoint, dispatch, cropYear));
            ReportGenerators.Add(new ProductionReportGenerator(clientEndpoint, dispatch, cropYear));
            ReportGenerators.Add(new ECottonExcelReportGenerator(clientEndpoint, cropYear));
            ReportGenerators.Add(new ProfitabilityExcelExportGenerator(clientEndpoint, dispatch, cropYear));
            ReportGenerators.Add(new ContributionMarginReportGenerator(clientEndpoint, dispatch, cropYear, false));
            ReportGenerators.Add(new ContributionMarginReportGenerator(clientEndpoint, dispatch, cropYear, true));
            ReportGenerators.Add(new ContractReportGenerator(clientEndpoint, dispatch, cropYear));
            //ReportGenerators.Add(new CropZoneSummaryReportGenerator(clientEndpoint, dispatch, cropYear));
            //ReportGenerators.Add(new GraphGenerator(clientEndpoint, dispatch, cropYear));
        }

        void SaveTemplate() {
            criteria = BuildCriteria();
            if (template != null && template.TemplateName == TemplateName) {
                ReportTemplateViewModel editedTemplate = new ReportTemplateViewModel() {
                    TemplateName = TemplateName,
                    Created = template.Created,
                    LastUsed = DateTime.Now,
                    CriteriaJSON = JsonConvert.SerializeObject(criteria),
                    ReportType = template.ReportType,
                    DataSourceId = template.DataSourceId
                };

                //TODO :: CALL TO EDIT PREVIOUS TEMPLATE...
                templateService.EditTemplate(editedTemplate);
            } else {
                ReportTemplateViewModel newTemplate = new ReportTemplateViewModel() {
                    TemplateName = TemplateName,
                    Created = DateTime.Now,
                    LastUsed = DateTime.Now,
                    CriteriaJSON = JsonConvert.SerializeObject(criteria),
                    ReportType = reportType,
                    DataSourceId = ApplicationEnvironment.CurrentDataSourceId
                };

                //TODO :: call service to save the new report template
                templateService.SaveNewTemplate(newTemplate);
                template = newTemplate;
            }

            RaisePropertyChanged("NewTemplateCreated", null, new ReportTemplateViewModel(), true);
        }

        void InitializeFromTemplate(ReportTemplateViewModel temp) {
            TemplateName = temp.TemplateName;

            YieldQueryCriteria crit = JsonConvert.DeserializeObject<YieldQueryCriteria>(temp.CriteriaJSON);

            var dsID = new DataSourceId(ApplicationEnvironment.CurrentDataSourceId);
            InfoPageModel.ReportStartDate = crit.ReportStartDate;
            InfoPageModel.ReportEndDate = crit.ReportEndDate;
            InfoPageModel.Entity = crit.SelectedEntity;
            InfoPageModel.CostType = crit.CostType;

            var RootTreeItemModels = FieldsPageModel.RootTreeItemModels;
            if (RootTreeItemModels.Any()) {
                if (RootTreeItemModels.First() is GrowerTreeItemViewModel) {
                    var toCheck = from farm in RootTreeItemModels.First().Children
                                  from field in farm.Children
                                  from cz in field.Children
                                  where crit.SelectedCropZones.Contains(cz.Id)
                                  select cz;
                    toCheck.ForEach(x => x.IsChecked = true);
                } else {
                    var toCheck = from crop in RootTreeItemModels.First().Children
                                  from farm in crop.Children
                                  from field in farm.Children
                                  from cz in field.Children
                                  where crit.SelectedCropZones.Contains(cz.Id)
                                  select cz;
                    toCheck.ForEach(x => x.IsChecked = true);
                }
            }
        }

        YieldQueryCriteria BuildCriteria() {
            YieldQueryCriteria criteria = new YieldQueryCriteria();

            var czq = from cz in FieldsPageModel.SelectedCropZones
                      select cz.Id;
            criteria.SelectedCropZones.AddRange(czq);
            criteria.ReportStartDate = InfoPageModel.ReportStartDate;
            criteria.ReportEndDate = InfoPageModel.ReportEndDate;
            criteria.CostType = InfoPageModel.CostType;
            criteria.SelectedEntity = InfoPageModel.Entity;

            return criteria;
        }
    }
}
