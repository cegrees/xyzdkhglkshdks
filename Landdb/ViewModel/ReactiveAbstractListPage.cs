﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;
using System.Reactive.Linq;
using System.Reactive;
using NLog;

namespace Landdb.ViewModel {
    

    public abstract class ReactiveAbstractListPage<TListItemModel, TDetailsModel> : ReactiveObject, ISupportsActivation {

        protected readonly IClientEndpoint clientEndpoint;
        protected readonly Dispatcher dispatcher;
        Logger log = LogManager.GetCurrentClassLogger();
        
        TDetailsModel detailsViewModel;

        bool isCharmPopupOpen;
        bool isFilterVisible;
        string filterHeader = string.Empty;
        readonly ViewModelActivator activator;

        public ReactiveAbstractListPage(IClientEndpoint clientEndpoint, Dispatcher dispatcher) {
            activator = new ViewModelActivator();
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;

            cropYearContext = new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear);
            Messenger.Default.Register<DataSourceChangedMessage>(this, OnDataSourceChanged);

            RefreshListCommand = ReactiveCommand.CreateFromTask(_ => RefreshItemsListAsync(dispatcher));
            this.WhenAnyValue(x => x.CropYearContext)
                .Select(x => Unit.Default)
                .InvokeCommand(RefreshListCommand);
            isLoadingItems = RefreshListCommand.IsExecuting.ToProperty(this, x => x.IsLoadingItems, false);

            ListItems = new ReactiveList<TListItemModel>();
            SetupListItemsView();

            var canCreateDetails = this.WhenAny(x => x.SelectedListItem, x => x.Value != null);
            CreateDetailsCommand = ReactiveCommand.CreateFromTask(_ => CreateDetails_InternalAsync(), canCreateDetails);
            this.WhenAnyValue(x => x.SelectedListItem)
                .Select(x => Unit.Default)
                .InvokeCommand(CreateDetailsCommand);
            isLoadingDetails = CreateDetailsCommand.IsExecuting.ToProperty(this, x => x.IsLoadingDetails, false);

            //////////////// old
            CreateNewItemCommand = ReactiveCommand.Create(CreateNewItem);
            CreateNewPopupItemCommand = ReactiveCommand.Create(CreateNewPopupItem);
            CreateNewOverlayItemCommand = ReactiveCommand.Create(CreateNewOverlayItem);
            RemoveSelectedItemCommand = ReactiveCommand.CreateFromTask(RemoveSelectedItemAsync, this.WhenAny(x => x.SelectedListItem, li => li != null && CanDelete(li.Value)));
            SetCharmCommand = new RelayCommand<string>(SetCharmOnSelectedItem);
            ApplyFilterCommand = ReactiveCommand.Create(ApplyFilter);
            ClearFilterCommand = ReactiveCommand.Create(ClearFilter);
            ShowRestorableItemsCommand = ReactiveCommand.Create(onShowRestorableItemList);
            HideRemovalPopupCommand = ReactiveCommand.Create(HideRemovalPopup);
        }

        protected virtual void SetupListItemsView() {
            ListItemsView = ListItems.CreateDerivedCollection(x => x, li => {
                if (FilterModel != null) { return FilterModel.FilterItem(li); } else { return true; }
            });
            ListItemsView.Changed.Subscribe(_ => {
                GenerateFilterHeader();
                FilterModel?.RefreshLists();
                if (SelectedListItem == null) { SelectedListItem = ListItemsView.FirstOrDefault(); }
            });
        }

        void OnDataSourceChanged(DataSourceChangedMessage message) {
            CropYearContext = new CropYearId(message.DataSourceId, message.CropYear);
        }

        public ReactiveCommand RefreshListCommand { get; protected set; }
        public ReactiveCommand CreateDetailsCommand { get; protected set; }

        protected ReactiveList<TListItemModel> ListItems { get; private set; }
        public IReactiveDerivedList<TListItemModel> ListItemsView { get; protected set; }

        internal async Task RefreshItemsListAsync(Dispatcher dispatcher) {
            if(cropYearContext == null || cropYearContext == EmptyCropYearId) {
                Console.WriteLine($"No crop year context to refresh data for.");
                return;
            }
            Console.WriteLine($"Refreshing list items for {this.GetType().Name}.");
            var listItems = await GetListItemModelsAsync();
            await dispatcher.InvokeAsync(() => {
                using (ListItems.SuppressChangeNotifications()) {
                    ListItems.Clear();
                    ListItems.AddRange(listItems ?? new List<TListItemModel>());
                }
            });

            DefaultPostRefreshAction();
        }

        public ReactiveCommand CreateNewItemCommand { get; }
        public ReactiveCommand CreateNewPopupItemCommand { get; }
        public ReactiveCommand CreateNewOverlayItemCommand { get; }
        public ReactiveCommand RemoveSelectedItemCommand { get; }
        public RelayCommand<string> SetCharmCommand { get; }
        public ReactiveCommand ApplyFilterCommand { get; }
        public ReactiveCommand ClearFilterCommand { get; }
        public ReactiveCommand ShowRestorableItemsCommand { get; }
        public ReactiveCommand HideRemovalPopupCommand { get; }

        IPageFilterViewModel filterModel;
        public IPageFilterViewModel FilterModel {
            get { return filterModel; }
            set { this.RaiseAndSetIfChanged(ref filterModel, value); }
        }

        protected virtual Action DefaultPostRefreshAction() => () => { };

        protected abstract Task<IEnumerable<TListItemModel>> GetListItemModelsAsync();
        protected abstract Task<ScreenDescriptor> CreateScreenDescriptorForNewItemCreationAsync();
        protected abstract IDomainCommand CreateRemovalDomainCommand(TListItemModel selectedItem);
        protected abstract IDomainCommand CreateSetCharmCommand(TListItemModel selectedItem, string charmName);
        protected abstract Task<TDetailsModel> CreateDetailsModelAsync(TListItemModel selectedItem);

        protected virtual IEnumerable<RestorableItem> GetRestorableItems() => new List<RestorableItem>();
        protected virtual void RestoreDeletedItem(Guid id) { }

        internal Task LoadingTask { get; private set; }

        TListItemModel selectedListItem;
        public TListItemModel SelectedListItem {
            get { return selectedListItem; }
            set { this.RaiseAndSetIfChanged(ref selectedListItem, value); }
        }

        CropYearId cropYearContext;
        protected CropYearId CropYearContext {
            get { return cropYearContext; }
            set { this.RaiseAndSetIfChanged(ref cropYearContext, value); }
        }
        async Task CreateDetails_InternalAsync() {
            var m = await CreateDetailsModelAsync(SelectedListItem);
            if (m != null) {
                DetailsModel = m;
            }
        }

        static CropYearId EmptyCropYearId = new CropYearId();

        #region Properties
        public TDetailsModel DetailsModel {
            get { return detailsViewModel; }
            set { this.RaiseAndSetIfChanged(ref detailsViewModel, value); }
        }

        public bool IsCharmPopupOpen {
            get { return isCharmPopupOpen; }
            set { this.RaiseAndSetIfChanged(ref isCharmPopupOpen, value); }
        }

        ObservableAsPropertyHelper<bool> isLoadingItems;
        public bool IsLoadingItems => isLoadingItems.Value;

        ObservableAsPropertyHelper<bool> isLoadingDetails;
        public bool IsLoadingDetails => isLoadingDetails.Value;

        public bool IsFilterVisible {
            get { return isFilterVisible; }
            set { this.RaiseAndSetIfChanged(ref isFilterVisible, value); }
        }

        public string FilterHeader {
            get { return filterHeader; }
            set { this.RaiseAndSetIfChanged(ref filterHeader, value); }
        }

        private bool isRemovalPopupOpen;
        public bool IsRemovalPopupOpen {
            get { return isRemovalPopupOpen; }
            set { this.RaiseAndSetIfChanged(ref isRemovalPopupOpen, value); }
        }

        private string removalPopupText;
        public string RemovalPopupText {
            get { return removalPopupText; }
            set { this.RaiseAndSetIfChanged(ref removalPopupText, value); }
        }

        public ViewModelActivator Activator => this.activator;
        #endregion

        public virtual string GetEntityName() => "item";
        public virtual string GetPluralEntityName() => "items";

        public virtual bool CanDelete(TListItemModel toDelete) => true;

        protected virtual async Task CreateNewItem() {
            var descriptor = await CreateScreenDescriptorForNewItemCreationAsync();
            Messenger.Default.Send(new ChangeScreenDescriptorMessage() { ScreenDescriptor = descriptor });
        }

        protected virtual async Task CreateNewPopupItem() {
            Messenger.Default.Send(new ShowPopupMessage() {
                ScreenDescriptor = await CreateScreenDescriptorForNewItemCreationAsync()
            });
        }

        protected virtual async Task CreateNewOverlayItem() {
            Messenger.Default.Send(new ShowOverlayMessage() {
                ScreenDescriptor = await CreateScreenDescriptorForNewItemCreationAsync()
            });
        }

        private void HideRemovalPopup() {
            IsRemovalPopupOpen = false;
        }

        protected virtual Task RemoveSelectedItemAsync() {
            if (SelectedListItem == null) { return Task.FromResult(false); }

            if (!CanDelete(selectedListItem)) {
                if (string.IsNullOrWhiteSpace(RemovalPopupText)) {
                    RemovalPopupText = $"Selected {GetEntityName()} cannot be removed.";
                }

                IsRemovalPopupOpen = true;

                return Task.FromResult(false);
            }

            var removeCommand = CreateRemovalDomainCommand(selectedListItem);
            if (removeCommand == null) { return Task.FromResult(true); } // If the implementing class doesn't provide a remove command, we can't do much.

            DialogFactory.ShowYesNoDialog($"remove {GetEntityName()}", $"Are you sure you want to remove this {GetEntityName()}", () => {
                clientEndpoint.SendOne(removeCommand);

                int index = ListItems.IndexOf(SelectedListItem) - 1;
                if (index < 0) { index = 0; }

                dispatcher.BeginInvoke(new Action(() => {
                    ListItems.Remove(SelectedListItem);
                    if (ListItems.Any()) {
                        SelectedListItem = ListItems[index];
                    } else {
                        SelectedListItem = default(TListItemModel);
                    }
                }));
            }, () => { });

            return Task.FromResult(true);
        }

        protected virtual void SetCharmOnSelectedItem(string charmName) {
            if (SelectedListItem == null) { return; }

            var flagCommand = CreateSetCharmCommand(selectedListItem, charmName);

            if (flagCommand == null) { return; }
            clientEndpoint.SendOne(flagCommand);

            IsCharmPopupOpen = false;
            //RaisePropertyChanged(() => ListItems);
        }

        protected virtual void ApplyFilter() {
            if (FilterModel != null && ListItemsView != null) {
                FilterModel.BeforeFilter();
                //ListItemsView.Refresh();
                GenerateFilterHeader();
                IsFilterVisible = false;
            }
        }

        protected virtual void ClearFilter() {
            if (FilterModel != null && ListItemsView != null) {
                FilterModel.ClearFilter();
                FilterModel.BeforeFilter();
                //ListItemsView.Refresh();
                GenerateFilterHeader();
                IsFilterVisible = false;
            }
        }

        protected virtual void GenerateFilterHeader() {
            var originalCount = ListItems.Count;
            var filteredCount = ListItemsView.Count;

            string entityPluralityAwareName = originalCount == 1 ? GetEntityName() : GetPluralEntityName();

            if (originalCount != filteredCount) {
                FilterHeader = $"{filteredCount} (of {originalCount}) {entityPluralityAwareName}";
            } else {
                FilterHeader = $"{filteredCount} {entityPluralityAwareName}";
            }
        }

        private void onShowRestorableItemList() {
            var restorableItems = GetRestorableItems();

            if (restorableItems.Any()) {
                var vm = new Shared.Popups.RestoreDeletedItemsViewModel(clientEndpoint, dispatcher, GetEntityName(), restorableItems, restoreVm => {
                    if (restoreVm != null && restoreVm.SelectedItem != null) {
                        RestoreDeletedItem(restoreVm.SelectedItem.Id);
                    }

                    Messenger.Default.Send(new HideOverlayMessage());
                });

                Messenger.Default.Send(new ShowOverlayMessage() {
                    ScreenDescriptor = new ScreenDescriptor(typeof(Views.Shared.Popups.RestoreDeletedItemsView), "restore deleted items", vm)
                });
            }
        }
    }
}
