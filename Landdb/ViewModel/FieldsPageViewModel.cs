﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GalaSoft.MvvmLight;
using System.Collections.ObjectModel;
using Landdb.Infrastructure;
using System.Reactive.Linq;
using WPFLocalizeExtension.Extensions;

namespace Landdb.ViewModel {
    public class FieldsPageViewModel : ViewModelBase {
        public FieldsPageViewModel() {
			Screens = new ObservableCollection<ScreenDescriptor>();

			ScreenDescriptor[] screenDescriptors = new ScreenDescriptor[] {
                new ScreenDescriptor(typeof(Views.FieldDetailsPageView), LocExtension.GetLocalizedValue<string>("Landdb:Strings:PageHeader_Land_Farms").ToUpper()),
                //new ScreenDescriptor(typeof(Landdb.Views.CropDetailsPageView), LocExtension.GetLocalizedValue<string>("Landdb:Strings:PageHeader_Land_Crops").ToUpper()),
                new ScreenDescriptor(typeof(Views.Maps.MapsPageView), LocExtension.GetLocalizedValue<string>("Landdb:Strings:PageHeader_Land_Maps").ToUpper()),
            };

            screenDescriptors.ToObservable()
                .OnTimeline(TimeSpan.FromSeconds(0.2))
                .ObserveOn(System.Threading.SynchronizationContext.Current)
                .Subscribe(AddScreen);
        }

        public ObservableCollection<ScreenDescriptor> Screens { get; }

        void AddScreen(ScreenDescriptor screen) {
            Screens.Add(screen);
        }
    }
}