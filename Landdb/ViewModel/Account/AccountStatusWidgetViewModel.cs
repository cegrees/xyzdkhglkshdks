﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;

namespace Landdb.ViewModel.Account {
	public class AccountStatusWidgetViewModel : ViewModelBase {
		string userDisplayName;
		bool isLoggedIn;
		string loginStatusText;
		bool showAccountPopup;
		string emailAddress;

		public AccountStatusWidgetViewModel() {
			UserDisplayName = string.Empty;
			Messenger.Default.Register<LoggedInMessage>(this, OnLoggedIn);

			SignOutCommand = new RelayCommand(SignOut);
			OptionsCommand = new RelayCommand(ShowOptions);
		}

		public ICommand SignOutCommand { get; }
		public RelayCommand OptionsCommand { get; }

		public string UserDisplayName {
			get { return userDisplayName; }
			set {
				userDisplayName = value;
				RaisePropertyChanged(() => UserDisplayName);
			}
		}

		public bool IsLoggedIn {
			get { return isLoggedIn; }
			set {
				isLoggedIn = value;
				RaisePropertyChanged(() => IsLoggedIn);
			}
		}

		public string LoginStatusText {
			get { return loginStatusText; }
			set {
				loginStatusText = value;
				RaisePropertyChanged(() => LoginStatusText);
			}
		}

		public bool ShowAccountPopup {
			get { return showAccountPopup; }
			set {
				showAccountPopup = value;
				RaisePropertyChanged(() => ShowAccountPopup);
			}
		}

		public string EmailAddress {
			get { return emailAddress; }
			set {
				emailAddress = value;
				RaisePropertyChanged(() => EmailAddress);
				RaisePropertyChanged(() => GravatarImageUriSmall);
				RaisePropertyChanged(() => GravatarImageUriLarge);
			}
		}

		public string GravatarImageUriSmall => GravatarGrabber.GetGravatarUri(EmailAddress, 30);
		public string GravatarImageUriLarge => GravatarGrabber.GetGravatarUri(EmailAddress, 50);

		void ShowOptions() {
			var clientEndpoint = ServiceLocator.Get<IClientEndpoint>(); // TODO: Factor this better

			OptionsPageViewModel vm = new OptionsPageViewModel(clientEndpoint);

			Messenger.Default.Send(new ChangeScreenDescriptorMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.OptionsPageView), "options", vm)
			});

			ShowAccountPopup = false;
		}

		// TODO: Once working, the SignOut process should be handled in a service class
		public void SignOut() {
			_DeleteEveryCookie(new Uri("https://login.live.com"));

			ShowAccountPopup = false;
		}

		void OnLoggedIn(LoggedInMessage message) {
			var name = string.Format("{0} {1}", message.UserAccountInformation.GivenName, message.UserAccountInformation.Surname);
			UserDisplayName = name;
			EmailAddress = message.UserAccountInformation.Email;
			IsLoggedIn = true;

			if (message.WorkingOffline) {
				LoginStatusText = Landdb.Resources.Strings.AccountLoggedInOfflineStatus;
			} else {
				LoginStatusText = Landdb.Resources.Strings.AccountLoggedInOnlineStatus;
			}
		}

		private static void _DeleteEveryCookie(Uri url) {
			string cookie = string.Empty;
			try {
				// Get every cookie (Expiration will not be in this response)
				cookie = Application.GetCookie(url);
			} catch (Win32Exception) {
				// "no more data is available" ... happens randomly so ignore it.
			}
			if (!string.IsNullOrEmpty(cookie)) {
				// This may change eventually, but seems quite consistent for Facebook.com.
				// ... they split all values w/ ';' and put everything in foo=bar format.
				string[] values = cookie.Split(';');

				foreach (string s in values) {
					if (s.IndexOf('=') > 0) {
						// Sets value to null with expiration date of yesterday.
						_DeleteSingleCookie(s.Substring(0, s.IndexOf('=')).Trim(), url);
					}
				}
			}
		}

		private static void _DeleteSingleCookie(string name, Uri url) {
			try {
				// Calculate "one day ago"
				DateTime expiration = DateTime.UtcNow - TimeSpan.FromDays(1);
				// Format the cookie as seen on FB.com.  Path and domain name are important factors here.
				string cookie = String.Format("{0}=; expires={1}; path=/; domain=.live.com", name, expiration.ToString("R"));
				// Set a single value from this cookie (doesnt work if you try to do all at once, for some reason)
				Application.SetCookie(url, cookie);
			} catch (Exception exc) {
				Console.WriteLine(exc + " seen deleting a cookie.  If this is reasonable, add it to the list.");
				//Assert.Fail(exc + " seen deleting a cookie.  If this is reasonable, add it to the list.");
			}
		}
	}
}