﻿using System;
using System.Linq;
using System.Collections.ObjectModel;
using GalaSoft.MvvmLight;
using System.Reactive.Linq;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Account;
using Landdb.Client.Services;
using Landdb.ViewModel.DataSources;
using System.Windows.Threading;
using Landdb.Client.Infrastructure;
using WPFLocalizeExtension.Extensions;

namespace Landdb.ViewModel {
    public class MainPageViewModel : ViewModelBase, IHandleViewRouting {
        private readonly ICloudClientFactory cloudClientFactory;
        private readonly Dispatcher dispatcher; 
        DataSourceSelectionViewModel dataSourceSelectionViewModel;
        int selectedTabIndex;
        bool addelluminatepage = true;
        bool addplanetlabspage = true;

        public MainPageViewModel(IAccountStatus accountStatus, ISynchronizationManager synchronizationManager, ICloudClientFactory cloudClientFactory, Dispatcher dispatcher) {
			Screens = new ObservableCollection<ScreenDescriptor>();

			Messenger.Default.Register<LoggedInMessage>(this, OnLoggedIn);
            Messenger.Default.Register<RouteRequestMessage>(this, HandleRouteRequest);

            this.cloudClientFactory = cloudClientFactory;
            this.dispatcher = dispatcher;

            DataSourceSelectionViewModel = new DataSourceSelectionViewModel(accountStatus, synchronizationManager, ServiceLocator.Get<IClientEndpoint>(), cloudClientFactory, dispatcher); // TODO: Get rid of ServiceLocator call
            
            CreateScreenDescriptors();
        }

        public DataSourceSelectionViewModel DataSourceSelectionViewModel {
            get { return dataSourceSelectionViewModel; }
            private set {
                dataSourceSelectionViewModel = value;
                RaisePropertyChanged(() => DataSourceSelectionViewModel);
            }
        }

        public ObservableCollection<ScreenDescriptor> Screens { get; }

        public void CreateScreenDescriptors() {
            var landText = LocExtension.GetLocalizedValue<string>("Landdb:Strings:PageHeader_Land").ToLower();
            var resourcesText = LocExtension.GetLocalizedValue<string>("Landdb:Strings:PageHeader_Resources").ToLower();
            var productionText = LocExtension.GetLocalizedValue<string>("Landdb:Strings:PageHeader_Production").ToLower();
            var yieldText = LocExtension.GetLocalizedValue<string>("Landdb:Strings:PageHeader_Yield").ToLower();
            var analysisText = LocExtension.GetLocalizedValue<string>("Landdb:Strings:PageHeader_Analysis").ToLower();
            var reportsText = LocExtension.GetLocalizedValue<string>("Landdb:Strings:PageHeader_Reports").ToLower();
            
            ScreenDescriptor[] screenDescriptors = new ScreenDescriptor[] {
                new ScreenDescriptor(typeof(Views.FieldsPageView), landText, new FieldsPageViewModel()),
                new ScreenDescriptor(typeof(Views.ResourcesPageView), resourcesText, new ResourcesPageViewModel()),
                new ScreenDescriptor(typeof(Views.ProductionPageView), productionText, new ProductionPageViewModel()),
                new ScreenDescriptor(typeof(Views.YieldPageView), yieldText, new YieldPageViewModel()),
                new ScreenDescriptor(typeof(Views.AnalysisPageView), analysisText, new AnalysisPageViewModel()),
                new ScreenDescriptor(typeof(Views.ReportsPageView), reportsText)
            };

            screenDescriptors.ToObservable()
                .OnTimeline(TimeSpan.FromSeconds(0.2))
                .ObserveOn(System.Threading.SynchronizationContext.Current)
                .Subscribe(AddScreen);
        }

        void OnLoggedIn(LoggedInMessage message) {
            CreateScreenDescriptors();
        }

        void AddScreen(ScreenDescriptor screen) {
            Screens.Add(screen);
        }

        public int SelectedTabIndex {
            get { return selectedTabIndex; }
            set {
                selectedTabIndex = value;
                AddSubscriptionPages(selectedTabIndex);
                RaisePropertyChanged(() => SelectedTabIndex);
            }
        }

        public void HandleRouteRequest(RouteRequestMessage message) {
            if (message.Route is string) {
                var route = (string)message.Route;
                var q = from s in Screens
                        where s.Key == route
                        select s;
                if (q.Any()) {
                    var sd = q.FirstOrDefault();
                    var index = Screens.IndexOf(sd);
                    SelectedTabIndex = index;
                    if (sd.ViewModel != null && message.Next != null && sd.ViewModel is IHandleViewRouting) {
                        ((IHandleViewRouting)sd.ViewModel).HandleRouteRequest(message.Next);
                    }
                }
            }
        }

        public void AddSubscriptionPages(int selectedtab)
        {
            if(Screens[selectedtab].ViewModel is AnalysisPageViewModel && !ApplicationEnvironment.PlanetPageLoaded)
            {
                if (ApplicationEnvironment.ShowPlanetLabsImagery || ApplicationEnvironment.ShowElluminateTab)
                {
                    ((AnalysisPageViewModel)Screens[selectedtab].ViewModel).AddTheImageryPage(true);
                } else if (!ApplicationEnvironment.ShowPlanetLabsImagery)
                {
                    ((AnalysisPageViewModel)Screens[selectedtab].ViewModel).AddTheImageryPage(false);
                }
            }
        }
    }
}
