﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.Resources;

namespace Landdb.ViewModel.Fields {
    public class CropTreeItemViewModel : AbstractTreeItemViewModel {
        CropId id;
        string name;
        IClientEndpoint client;

        public CropTreeItemViewModel(TreeViewCropItem crop, AbstractTreeItemViewModel parent, bool sortChildrenAlphabetically, Action<AbstractTreeItemViewModel> onCheckedChanged = null, Action<List<CropZoneId>> onParentCompleted = null, Predicate<object> filter = null)
            : base(parent, onCheckedChanged, onParentCompleted, filter) {
            id = crop.CropId;
            client = ServiceLocator.Get<IClientEndpoint>(); // TODO: Refactor ServiceLocator out. Pass dependency in via ctor.

            if (client != null) {
                name = client.GetMasterlistService().GetCropDisplay(id);
            } else {
                name = Strings.UnknownCrop_Text;
            }

            var q = from f in crop.Farms
                    select new FarmTreeItemViewModel(f, this, sortChildrenAlphabetically, onCheckedChanged, onParentCompleted, filter);

            if (sortChildrenAlphabetically) {
                q = q.OrderBy(x => x.Name);
            }

            Children = new ObservableCollection<AbstractTreeItemViewModel>(q);
        }

        public override IIdentity Id { get { return id; } }
        public CropId CropId { get { return id; } }
        public string Name { get { return name; } }

        public override string ToString() {
            return Name;
        }
    }
}
