﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using ThinkGeo.MapSuite.WpfDesktopEdition;
using ThinkGeo.MapSuite.Core;
using System.IO;
using Microsoft.Win32;
using NLog;
using Landdb.Client.Infrastructure;
using System.Windows.Threading;
using System.Collections.ObjectModel;
using Landdb.Domain.ReadModels.Tree;
using Npgsql;
using System.Data;
using System.Windows;
using Landdb.Resources;

namespace Landdb.ViewModel.Fields.FieldDetails.MapModels {
    public class ImportToolModel : ViewModelBase {
        WpfMap map;
        MapViewModel parent;
        IClientEndpoint clientEndpoint;
        Dispatcher dispatcher;
        Cursor previousCursor;
        Logger log = LogManager.GetCurrentClassLogger();
        string filename = string.Empty;
        string newfilenamewithpath = string.Empty;
        string[] newfilenameswithpathlist;
        string newprojectionfilenamewithpath = string.Empty;
        string overrideprojection = string.Empty;
        Proj4Projection projection;
        Proj4Projection wfsprojection;
        Proj4Projection wfs4326projection;
        Proj4Projection mouseprojection;
        int projInt = 4326;
        MultipolygonShape importpoly = new MultipolygonShape();
        bool hasunsavedchanges = false;
        
        bool toggledrawandedit = false;
        bool isdrawing = false;
        bool isediting = false;
        AreaStyle areaStyleBrush;
        MapStyles.CustomAreaPolygonStyle customAreaPolygonStyleText;

        bool importingfeatures = false;
        bool importingapexfeatures = false;
        bool importingclufeatures = false;
        bool currentextentchanged = false;
        bool mousedown = false;
        bool mousemove = false;
        //NOTE:  externalcludata  chooses the method for handling CLU data.  To display all shapes or only selected shape.
        //bool externalcludata = true;
        bool externalcludata = false;
        LayerOverlay importingOverlay = new LayerOverlay();
        ShapeFileFeatureLayer shapeLayer = new ShapeFileFeatureLayer();
        GpxFeatureLayer gpxLayer = new GpxFeatureLayer();
        KmlFeatureLayer kmlLayer = new KmlFeatureLayer();
        InMemoryFeatureLayer importingLayer = new InMemoryFeatureLayer();
        Dictionary<string, Feature> cludictionary = new Dictionary<string, Feature>();
        WfsFeatureLayer wfsFeatureLayer;
        PostgreSqlFeatureLayer postFeatureLayer;
        MultipolygonShape drawingpolygonshape = new MultipolygonShape();
        MultipolygonShape editingpolygonshape = new MultipolygonShape();
        LineShape drawinglineshape = new LineShape();
        PointShape controlpoint = new PointShape();
        bool isdrawingpolygon = false;
        bool isdrawingmeasure = false;
        string polystring = string.Empty;
        MapSettings mapsettings;
        string areaUnitString = string.Empty;
        string coordinateformat = string.Empty;
        AgC.UnitConversion.Area.AreaUnit agcAreaUnit;
        ThinkGeo.MapSuite.Core.AreaUnit mapAreaUnit;
        ThinkGeo.MapSuite.Core.DistanceUnit mapDistanceUnit;

        public ImportToolModel(WpfMap map, MapViewModel parent, IClientEndpoint clientEndpoint, Dispatcher dispatcher) {
            this.map = map;
            this.parent = parent;
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            InitializeSettings();

            projection = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetGoogleMapParametersString());
            wfsprojection = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetGoogleMapParametersString());
            wfs4326projection = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetGoogleMapParametersString());
            mouseprojection = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetGoogleMapParametersString());


            map.MapClick += (sender, e) => {
                currentextentchanged = false;
                if (map.TrackOverlay.TrackMode != TrackMode.None) { return; }
                if (importingapexfeatures) { return; }
                try {
                    if (importingclufeatures) {
                        if (externalcludata) {
                            PullSelectedFromExternalLayer(e);
                        }
                        else {
                            PullSelectedFromInternalLayer(e);
                        }
                    }
                    if (importingfeatures) {
                        PullSelectedFromInternalLayer(e);
                    }
                }
                catch (Exception ex) {
                    log.ErrorException("Caught an exception while importing a shape", ex);
                    System.Windows.MessageBox.Show(Strings.ImportToolsCaughtException_Text);
                }
            };

            map.CurrentExtentChanged += (sender, e) => {
                if (importingclufeatures && mousedown && mousemove) {
                    currentextentchanged = true;
                }
            };

            map.MouseDown += (sender, e) => {
                if (importingclufeatures) {
                    mousedown = true;
                }
            };

            map.MouseMove += (sender, e) => {
                if (importingclufeatures) {
                    mousemove = true;
                }
            };

            map.MouseUp += (sender, e) => {
                if (importingclufeatures && currentextentchanged && !externalcludata) {
                    parent.IsProcessingCommands = true;
                    EnsureImportingLayerIsOpen();
                    LoadSpatialFeatures();
                    if (map.Overlays.Contains("mportingOverlay")) {
                        if (((LayerOverlay)map.Overlays["mportingOverlay"]).Layers.Contains("importingLayer")) {
                            map.Overlays["mportingOverlay"].Refresh();
                        }
                    }
                    parent.IsProcessingCommands = false;
                }
                currentextentchanged = false;
                mousedown = false;
                mousemove = false;
            };
        }

        private void InitializeSettings()
        {
            this.mapsettings = clientEndpoint.GetMapSettings();
            areaUnitString = mapsettings.MapAreaUnit;
            coordinateformat = mapsettings.DisplayCoordinateFormat;
            agcAreaUnit = AgC.UnitConversion.UnitFactory.GetUnitByName(areaUnitString) as AgC.UnitConversion.Area.AreaUnit;
            mapAreaUnit = MapUnitFactory.AreaMeasureConversion(agcAreaUnit);
            mapDistanceUnit = MapUnitFactory.AreaDistanceMeasureConversion(agcAreaUnit);
        }

        private void PullSelectedFromInternalLayer(MapClickWpfMapEventArgs e) {
            PointShape worldlocation = e.WorldLocation as PointShape;
            PointShape latlonlocation = wfsprojection.ConvertToInternalProjection(worldlocation) as PointShape;
            PointShape latlonmouse = mouseprojection.ConvertToInternalProjection(worldlocation) as PointShape;
            try
            {
                EnsureImportingLayerIsOpen();
                Collection<Feature> selectedFeatures = importingLayer.QueryTools.GetFeaturesContaining(e.WorldLocation, ReturningColumnsType.AllColumns);
                if (selectedFeatures.Count > 0) {
                    for (int i = 0; i < importingLayer.InternalFeatures.Count; i++) {
                        Feature feat = importingLayer.InternalFeatures[i];
                        BaseShape baseshape = feat.GetShape();
                        AreaBaseShape areabaseshape = baseshape as AreaBaseShape;
                        if (areabaseshape.Contains(latlonlocation)) {
                            log.Info("ImportTools - Import shape selected.");
                            BaseShape baseshape2 = wfsprojection.ConvertToExternalProjection(baseshape);
                            parent.SaveMassImport(baseshape2);
                            importingLayer.InternalFeatures.RemoveAt(i);
                            break;
                        }
                        if (areabaseshape.Contains(latlonmouse))
                        {
                            log.Info("ImportTools - Import shape selected.");
                            BaseShape baseshape2 = mouseprojection.ConvertToExternalProjection(baseshape);
                            parent.SaveMassImport(baseshape2);
                            importingLayer.InternalFeatures.RemoveAt(i);
                            break;
                        }
                    }
                }
            } catch (Exception ex) {
                int xxx = 0;
                //if(importingLayer.InternalFeatures.Count == 1) {
                //    log.Info("ImportTools - Import shape selected.");
                //    Feature basefeature = importingLayer.InternalFeatures[0];
                //    BaseShape baseshape1 = basefeature.GetShape();
                //    BaseShape baseshape2 = wfsprojection.ConvertToExternalProjection(baseshape1);
                //    parent.SaveMassImport(baseshape2);
                //    importingLayer.InternalFeatures.RemoveAt(0);
                //} else {
                    try {
                        for (int i = 0; i < importingLayer.InternalFeatures.Count; i++) {
                            Feature feat = importingLayer.InternalFeatures[i];
                            BaseShape baseshape = feat.GetShape();
                            AreaBaseShape areabaseshape = baseshape as AreaBaseShape;
                            MultipolygonShape mps = areabaseshape.Buffer(0, map.MapUnit, MapUnitFactory.AreaDistanceMeasureConversion(map.MapUnit));
                            if (mps.Contains(latlonlocation)) {
                                log.Info("ImportTools - Import shape selected.");
                                BaseShape mpsbase = mps as BaseShape;
                                BaseShape baseshape2 = wfsprojection.ConvertToExternalProjection(mpsbase);
                                parent.SaveMassImport(baseshape2);
                                importingLayer.InternalFeatures.RemoveAt(i);
                                break;
                            }
                        }

                    }
                    catch (Exception ex11) {

                    }
                //}
            }
        }

        private void PullSelectedFromExternalLayer(MapClickWpfMapEventArgs e) {
            PointShape worldlocation = e.WorldLocation as PointShape;
            PointShape latlonlocation = wfsprojection.ConvertToInternalProjection(worldlocation) as PointShape;
            try {
                EnsureImportingLayerIsOpen();
                Collection<Feature> selectedFeatures = postFeatureLayer.QueryTools.GetFeaturesContaining(latlonlocation, ReturningColumnsType.AllColumns);
                string wkt1 = string.Empty;
                if (selectedFeatures.Count > 0) {
                    for (int i = 0; i < selectedFeatures.Count; i++) {
                        Feature feat = selectedFeatures[i];
                        BaseShape baseshape = feat.GetShape();
                        AreaBaseShape areabaseshape = baseshape as AreaBaseShape;
                        wkt1 = areabaseshape.GetWellKnownText();
                        Feature newFeature = new Feature(wkt1, feat.Id);
                        if (areabaseshape.Contains(latlonlocation)) {
                            log.Info("ImportTools - Import shape selected.");
                            BaseShape baseshape2 = wfsprojection.ConvertToExternalProjection(baseshape);
                            if (!cludictionary.ContainsKey(feat.Id)) {
                                cludictionary.Add(feat.Id, newFeature);
                                parent.SaveMassImport(baseshape2);
                                break;
                            }
                        }
                    }
                }
            } catch (Exception ex) {
                int xxx = 0;
                //if(importingLayer.InternalFeatures.Count == 1) {
                //    log.Info("ImportTools - Import shape selected.");
                //    Feature basefeature = importingLayer.InternalFeatures[0];
                //    BaseShape baseshape1 = basefeature.GetShape();
                //    BaseShape baseshape2 = wfsprojection.ConvertToExternalProjection(baseshape1);
                //    parent.SaveMassImport(baseshape2);
                //    importingLayer.InternalFeatures.RemoveAt(0);
                //} else {
                try {
                    Collection<Feature> selectedFeatures = postFeatureLayer.QueryTools.GetFeaturesContaining(latlonlocation, ReturningColumnsType.AllColumns);
                    for (int i = 0; i < selectedFeatures.Count; i++) {
                        Feature feat = selectedFeatures[i];
                        BaseShape baseshape = feat.GetShape();
                        AreaBaseShape areabaseshape = baseshape as AreaBaseShape;
                        MultipolygonShape mps = areabaseshape.Buffer(0, map.MapUnit, MapUnitFactory.AreaDistanceMeasureConversion(map.MapUnit));
                        if (mps.Contains(latlonlocation)) {
                            log.Info("ImportTools - Import shape selected.");
                            BaseShape mpsbase = mps as BaseShape;
                            BaseShape baseshape2 = wfsprojection.ConvertToExternalProjection(mpsbase);
                            parent.SaveMassImport(baseshape2);
                            importingLayer.InternalFeatures.RemoveAt(i);
                            break;
                        }
                    }

                }
                catch (Exception ex11) {

                }
                //}
            }
        }

        public string FileName {
            get { return filename; }
            set {
                if (filename == value) { return; }
                filename = value;
                RaisePropertyChanged("FileName");
            }
        }

        public string OverrideProjection {
            get { return overrideprojection; }
            set {
                if (overrideprojection == value) { return; }
                overrideprojection = value;
                RaisePropertyChanged("OverrideProjection");
            }
        }

        public Proj4Projection Projection {
            get { return projection; }
            set {
                if (projection == value) { return; }
                Proj4Projection test = value;
                if (projection.InternalProjectionParametersString == test.InternalProjectionParametersString) { return; }
                projection = value;
                RaisePropertyChanged("Projection");
            }
        }

        public bool HasUnsavedChanges {
            get { return hasunsavedchanges; }
            set {
                if (hasunsavedchanges == value) { return; }
                hasunsavedchanges = value;
                RaisePropertyChanged("HasUnsavedChanges");
            }
        }

        public void HandleShapesMassImport() {
            OpenAndLoadaShapefile();
        }

        public void HandleShapesApexImport() {
            OpenAndLoadaApexShapefile();
        }

        public void HandleShapesCLUImport() {
            //OpenAndLoadaCLULayer();
            OpenAndLoadaPostgreLayer();
        }

        public void HandleShapesPLSSImport()
        {
            //OpenAndLoadaCLULayer();
            OpenAndLoadaPLSSPostgreLayer();
        }
        private void OpenAndLoadaShapefile() {
            cludictionary = new Dictionary<string, Feature>(); 
            bool goodfile = AddLayersByOpenFileDialog(false);
            if (goodfile) {
                InitializeImportingOverlay(false, new Collection<FeatureSourceColumn>());
                bool foundthefile = CreateShapeFileLayerAndOverlay();
                if (foundthefile) {
                    LoadShapeFileOverlayOntoMap();

                    if (map.Overlays.Contains("mportingOverlay")) {
                        if (((LayerOverlay)map.Overlays["mportingOverlay"]).Layers.Contains("importingLayer")) {
                            map.Refresh();
                            importingfeatures = true;
                            importingapexfeatures = false;
                            importingclufeatures = false;
                        }
                    }
                }
            }
            else {
                log.Info("ImportTools - Not a good shape file to import.");
                shapeLayer = new ShapeFileFeatureLayer();
                gpxLayer = new GpxFeatureLayer();
                kmlLayer = new KmlFeatureLayer();
                newfilenamewithpath = string.Empty;
                CloseAndCancelImport();
            }
        }

        private void OpenAndLoadaPostgreLayer() {
            cludictionary = new Dictionary<string, Feature>();
            InitializeImportingOverlay(false, new Collection<FeatureSourceColumn>());
            //string postconnectString = "Server=clu.landdb.com;User Id=gis;Password=TGBnhyui;DataBase=gisdb;Port=5432;";
            //string postconnectString = "Server=maps.landdb.com;User Id=gis;Password=TGBnhyui;DataBase=gisdb;Port=15432;";

            string postconnectString = "Server=maps.landdb.com;User Id=ROgis;Password=fgh^YHNjklo987;DataBase=gisdb;Port=15432;";
            //string postconnectString = "Server=maps2.landdb.com;User Id=ROgis;Password=fgh^YHNjklo987;DataBase=gisdb;Port=15432;";

            wfsprojection = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetGoogleMapParametersString());
            wfsprojection.Open();
            mouseprojection = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetGoogleMapParametersString());
            mouseprojection.Open();
            //postFeatureLayer = new PostgreSqlFeatureLayer(postconnectString, "fsa_clu_table", "ogc_fid", 4326);

            postFeatureLayer = new PostgreSqlFeatureLayer(postconnectString, "fsa_clu_table", "ogc_fid", 4326);
            //postFeatureLayer = new PostgreSqlFeatureLayer(postconnectString, "enogen_zone", "ogc_fid", 4326);
            //postFeatureLayer.GeometryColumnName = "wkb_geometry";

            //Enogen Awareness Zones
            //Server:  maps.landdb.com
            //User Id: ROgis
            //Password:  fgh ^ YHNjklo987
            //DataBase: gisdb
            //Port:  15432
            //Table Name:  enogen_zone
            //Feature Id Column Name: ogc_fid
            //Geometry Column Name:  wkb_geometry
            //Projection:  wgs84
            //epsg code: 4326

            postFeatureLayer.DrawingExceptionMode = DrawingExceptionMode.DrawException;
            postFeatureLayer.Open();
            postFeatureLayer.FeatureSource.Open();

            importingfeatures = false;
            importingapexfeatures = false;
            importingclufeatures = true;

            if (importingclufeatures && !externalcludata) {
                EnsureImportingLayerIsOpen();
                importingLayer.FeatureSource.Projection = wfsprojection;

                parent.IsProcessingCommands = true;
                LoadSpatialFeatures();

                if (map.Overlays.Contains("mportingOverlay")) {
                    map.Overlays.Remove("mportingOverlay");
                }
                if (importingOverlay.Layers.Contains("importingLayer")) {
                    map.Overlays.Add("mportingOverlay", importingOverlay);
                }
                if (!importingLayer.IsOpen) {
                    importingLayer.Open();
                }

                EnsureImportingLayerIsOpen();
                try {
                    var shapelayerfeatures = importingLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);
                    if (shapelayerfeatures.Count > 0) {
                        map.CurrentExtent = importingLayer.GetBoundingBox();
                    }
                }
                catch {
                    log.Info("ImportTools - Problem calculating extent.");
                }

                if (map.Overlays.Contains("mportingOverlay")) {
                    if (((LayerOverlay)map.Overlays["mportingOverlay"]).Layers.Contains("importingLayer")) {
                        map.Refresh();
                    }
                }
                parent.IsProcessingCommands = false;
                return;
            }
        }

        private void OpenAndLoadaPLSSPostgreLayer()
        {
            try
            {
                //string postconnectString = "Server=maps.landdb.com;User Id=ROgis;Password=fgh^YHNjklo987;DataBase=gisdb;Port=15432;";

                //-host:  system1.centralus.cloudapp.azure.com
                //- database:  gisdb
                //- un:  gis
                //- pw: Th3D0wnwardSp1ral
                //- tables:  plu_table, plu_point_table

                cludictionary = new Dictionary<string, Feature>();
                InitializeImportingOverlay(false, new Collection<FeatureSourceColumn>());
                //string postconnectString = "Server=clu.landdb.com;User Id=gis;Password=TGBnhyui;DataBase=gisdb;Port=5432;";
                //string postconnectString = "Server=maps.landdb.com;User Id=gis;Password=TGBnhyui;DataBase=gisdb;Port=15432;";

                //string postconnectString = "Server=maps.landdb.com;User Id=gis;Password=Th3D0wnwardSp1ral;DataBase=gisdb;Port=15432;";
                //string postconnectString = "Server=system1.centralus.cloudapp.azure.com;User Id=gis;Password=Th3D0wnwardSp1ral;DataBase=gisdb;Port=15432;";
                //string postconnectString = "Server=maps.landdb.com;User Id=ROgis;Password=fgh^YHNjklo987;DataBase=gisdb;Port=15432;";
                //string postconnectString = "Server=plss.landdb.com;User Id=ROgis;Password=Th3D0wnwardSp1ral;DataBase=gisdb;Port=15432;";
                //string postconnectString = "Server=plss.landdb.com;User Id=ROgis;Password=fgh^YHNjklo987;DataBase=gisdb;Port=15432;";
                //string postconnectString = "Server=system1.centralus.cloudapp.azure.com;User Id=gis;Password=Th3D0wnwardSp1ral;DataBase=gisdb;Port=15432;";
                string postconnectString = "Server=system1.centralus.cloudapp.azure.com;User Id=gis;Password=Th3D0wnwardSp1ral;DataBase=gisdb;Port=5432;";

                wfsprojection = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetGoogleMapParametersString());
                wfsprojection.Open();
                mouseprojection = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetGoogleMapParametersString());
                mouseprojection.Open();
                //postFeatureLayer = new PostgreSqlFeatureLayer(postconnectString, "fsa_clu_table", "ogc_fid", 4326);
                postFeatureLayer = new PostgreSqlFeatureLayer(postconnectString, "plu_table", "ogc_fid", 4326);
                //postFeatureLayer = new PostgreSqlFeatureLayer(postconnectString, "plu_point_table", "ogc_fid", 4326);
                postFeatureLayer.DrawingExceptionMode = DrawingExceptionMode.DrawException;
                postFeatureLayer.Open();
                postFeatureLayer.FeatureSource.Open();

                importingfeatures = false;
                importingapexfeatures = false;
                importingclufeatures = true;

                if (importingclufeatures && !externalcludata)
                {
                    EnsureImportingLayerIsOpen();
                    importingLayer.FeatureSource.Projection = wfsprojection;

                    parent.IsProcessingCommands = true;
                    LoadSpatialFeatures();

                    if (map.Overlays.Contains("mportingOverlay"))
                    {
                        map.Overlays.Remove("mportingOverlay");
                    }
                    if (importingOverlay.Layers.Contains("importingLayer"))
                    {
                        map.Overlays.Add("mportingOverlay", importingOverlay);
                    }
                    if (!importingLayer.IsOpen)
                    {
                        importingLayer.Open();
                    }

                    EnsureImportingLayerIsOpen();
                    try
                    {
                        var shapelayerfeatures = importingLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);
                        if (shapelayerfeatures.Count > 0)
                        {
                            map.CurrentExtent = importingLayer.GetBoundingBox();
                        }
                    }
                    catch
                    {
                        log.Info("ImportTools - Problem calculating extent.");
                    }

                    if (map.Overlays.Contains("mportingOverlay"))
                    {
                        if (((LayerOverlay)map.Overlays["mportingOverlay"]).Layers.Contains("importingLayer"))
                        {
                            map.Refresh();
                        }
                    }
                    parent.IsProcessingCommands = false;
                    return;
                }
            } catch (Exception ex)
            {
                MessageBox.Show(Strings.ProblemLoadingPLSS_Text);
            }
        }

        private void LoadSpatialFeatures() {
            try {
                RectangleShape wfsextent = map.CurrentExtent;
                double extentarea = wfsextent.GetArea(GeographyUnit.Meter, mapAreaUnit);
                if (extentarea > 100000) {
                    wfsextent = map_ScaleExtent(wfsextent);
                }
                RectangleShape wfsextentinternal = wfsprojection.ConvertToInternalProjection(wfsextent) as RectangleShape;
                System.Collections.ObjectModel.Collection<Feature> featuressss = postFeatureLayer.FeatureSource.GetFeaturesInsideBoundingBox(wfsextentinternal, ReturningColumnsType.AllColumns);
                string wkt1 = string.Empty;
                foreach (Feature feat in featuressss) {
                    BaseShape baseshape = feat.GetShape();
                    AreaBaseShape areabaseshape = baseshape as AreaBaseShape;
                    wkt1 = areabaseshape.GetWellKnownText();
                    Feature newFeature = new Feature(wkt1, feat.Id);
                    cludictionary = new Dictionary<string, Feature>();
                    try {
                        if (!cludictionary.ContainsKey(feat.Id)) {
                            cludictionary.Add(feat.Id, newFeature);
                            if (!importingLayer.InternalFeatures.Contains(newFeature)) {
                                importingLayer.InternalFeatures.Add(feat.Id, newFeature);

                            }
                        }
                    }
                    catch (Exception ex) {

                    }
                }
            }
            catch (Exception ex) {
                log.ErrorException("Caught an exception while loading CLU shapes", ex);
                System.Windows.MessageBox.Show(Strings.ImportToolsCaughtExceptionClu_Text);
                CloseAndCancelImport();
            }
        }

        private void OpenAndLoadaCLULayer() {
            InitializeImportingOverlay(false, new Collection<FeatureSourceColumn>());
            wfsFeatureLayer = new WfsFeatureLayer(@"http://192.168.11.199:8080/geoserver/clu_public_a_ky035/ows", @"clu_public_a_ky035:clu_public_a_ky035");
            wfsFeatureLayer.Open();
            wfsFeatureLayer.FeatureSource.Open();

            Proj4Projection tempproj4 = new Proj4Projection(Proj4Projection.GetEpsgParametersString(26916), Proj4Projection.GetGoogleMapParametersString());
            tempproj4.Open();
            wfsprojection = new Proj4Projection(Proj4Projection.GetEpsgParametersString(26916), Proj4Projection.GetGoogleMapParametersString());
            wfsprojection.Open();
            wfs4326projection = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetGoogleMapParametersString());
            wfs4326projection.Open();
            mouseprojection = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetGoogleMapParametersString());
            mouseprojection.Open();

            importingfeatures = false;
            importingapexfeatures = false;
            importingclufeatures = true;
            currentextentchanged = false;
            mousedown = false;
            mousemove = false;

            if (importingclufeatures) {
                RectangleShape wfsextent = map.CurrentExtent;
                double extentarea = wfsextent.GetArea(GeographyUnit.Meter, mapAreaUnit);
                if (extentarea > 100000) {
                    wfsextent = map_ScaleExtent(wfsextent);
                }
                RectangleShape wfsextentinternal = wfsprojection.ConvertToInternalProjection(wfsextent) as RectangleShape;
                System.Collections.ObjectModel.Collection<Feature> featuressss = wfsFeatureLayer.FeatureSource.GetFeaturesInsideBoundingBox(wfsextentinternal, ReturningColumnsType.AllColumns);
                string wkt1 = string.Empty;
                string wkt2 = string.Empty;
                string wkt3 = string.Empty;
                EnsureImportingLayerIsOpen();
                importingLayer.FeatureSource.Projection = wfsprojection;
                foreach (Feature feat in featuressss) {
                    BaseShape baseshape = feat.GetShape();
                    AreaBaseShape areabaseshape = baseshape as AreaBaseShape;
                    wkt1 = areabaseshape.GetWellKnownText();
                    importingLayer.InternalFeatures.Add(feat);

                }

                if (map.Overlays.Contains("mportingOverlay")) {
                    map.Overlays.Remove("mportingOverlay");
                }
                if (importingOverlay.Layers.Contains("importingLayer")) {
                    map.Overlays.Add("mportingOverlay", importingOverlay);
                }
                if (!importingLayer.IsOpen) {
                    importingLayer.Open();
                }

                EnsureImportingLayerIsOpen();
                try {
                    var shapelayerfeatures = importingLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);
                    if (shapelayerfeatures.Count > 0) {
                        map.CurrentExtent = importingLayer.GetBoundingBox();
                    }
                }
                catch {
                    log.Info("ImportTools - Problem calculating extent.");
                }

                if (map.Overlays.Contains("mportingOverlay")) {
                    if (((LayerOverlay)map.Overlays["mportingOverlay"]).Layers.Contains("importingLayer")) {
                        map.Refresh();
                    }
                }

                return;
            }
        }

        private RectangleShape map_ScaleExtent(RectangleShape shape4) {
            double shapearea101 = 0;
            double DesiredArea = 100000;
            PolygonShape poly = shape4.ToPolygon();
            PolygonShape shape = new PolygonShape(poly.GetWellKnownText());
            PolygonShape shape2 = new PolygonShape(poly.GetWellKnownText());
            MultipolygonShape shape3 = new MultipolygonShape();
            if (shape != null) {
                shapearea101 = shape2.GetArea(GeographyUnit.Meter, mapAreaUnit);
                double OldArea = shapearea101;

                double ndiff = -.0001;
                double pdiff = .0001;
                double counter = 0;
                double areadifference = 99900 - shapearea101;
                if (areadifference < 0) {
                    areadifference = areadifference * -1;
                }
                double ScalePercent = (areadifference / OldArea) * 2;

                while (areadifference < ndiff || areadifference > pdiff) {
                    shapearea101 = shape.GetArea(GeographyUnit.Meter, mapAreaUnit);

                    if (DesiredArea > shapearea101) {
                        shape.ScaleUp(System.Convert.ToSingle(ScalePercent));
                    }
                    else {
                        shape.ScaleDown(System.Convert.ToSingle(ScalePercent));
                    }

                    shapearea101 = shape.GetArea(GeographyUnit.Meter, mapAreaUnit);
                    areadifference = DesiredArea - shapearea101;
                    if (areadifference < 0) {
                        areadifference = areadifference * -1;
                    }
                    ScalePercent = (areadifference / OldArea) * 2;

                    counter++;
                    if (counter > 1000) {
                        break;
                    }
                }
                return shape.GetBoundingBox();
            }
            return shape4;
        }

        private void OpenAndLoadaApexShapefile() {
            bool goodfile = AddLayersByOpenFileDialog(true);
            if (goodfile) {
                //InitializeImportingOverlay(true, new Collection<FeatureSourceColumn>());
                bool foundthefile = CreateApexShapeFileLayerAndOverlay();
                if (foundthefile) {
                    LoadShapeFileOverlayOntoMap();

                    if (map.Overlays.Contains("mportingOverlay")) {
                        if (((LayerOverlay)map.Overlays["mportingOverlay"]).Layers.Contains("importingLayer")) {
                            map.Refresh();
                            importingfeatures = false;
                            importingapexfeatures = true;
                        }
                    }
                }
            }
            else {
                log.Info("ImportTools - Not a good shape file to import.");
                shapeLayer = new ShapeFileFeatureLayer();
                gpxLayer = new GpxFeatureLayer();
                kmlLayer = new KmlFeatureLayer();
                newfilenamewithpath = string.Empty;
                CloseAndCancelImport();
            }
        }

        private bool AddLayersByOpenFileDialog(bool multiplefiles) {
            newfilenamewithpath = string.Empty;
            filename = string.Empty;
            importpoly = new MultipolygonShape();
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Multiselect = multiplefiles;
            openFileDialog.AddExtension = true;
            openFileDialog.CheckFileExists = true;
            openFileDialog.CheckPathExists = true;
            openFileDialog.Filter = "Shape files (*.shp)|*.shp|KML files (*.kml)|*.kml";
            //openFileDialog.Filter = "Shape files (*.shp)|*.shp";
            bool? dialogresult = openFileDialog.ShowDialog();
            if (dialogresult.HasValue && dialogresult.Value) {
                if (multiplefiles) {
                    newfilenameswithpathlist = openFileDialog.FileNames;
                    return true;
                }
                else {
                    newfilenamewithpath = openFileDialog.FileName;
                    log.Info("ImportTools - Shape import file selected: {0}.", newfilenamewithpath);
                    if (File.Exists(newfilenamewithpath)) {
                        string tempFileBase = Path.Combine(Path.GetDirectoryName(newfilenamewithpath), Path.GetFileNameWithoutExtension(newfilenamewithpath));
                        string result = Path.GetExtension(newfilenamewithpath).Replace(".", "");
                        if (result == "kml" && File.Exists(newfilenamewithpath))
                        {
                            FileName = openFileDialog.FileName;
                            return true;
                        }
                        if (result == "gpx" && File.Exists(newfilenamewithpath)) {
                            FileName = openFileDialog.FileName;
                            return true;
                        }
                        newprojectionfilenamewithpath = string.Format("{0}.dbf", tempFileBase);
                        if (result == "shp" && !File.Exists(newprojectionfilenamewithpath)) {
                            return false;
                        }
                        newprojectionfilenamewithpath = string.Format("{0}.shx", tempFileBase);
                        if (result == "shp" && !File.Exists(newprojectionfilenamewithpath)) {
                            return false;
                        }
                        FileName = openFileDialog.FileName;
                        return true;
                    }
                    else {
                        return false;
                    }
                }
            }
            else {
                return false;
            }
        }

        private void InitializeImportingOverlay(bool multiplefiles, Collection<FeatureSourceColumn> columnlist) {
            importingfeatures = false;
            importingapexfeatures = false;
            importingclufeatures = false;
            currentextentchanged = false;
            mousedown = false;
            mousemove = false;
            cludictionary = new Dictionary<string, Feature>();
            importingOverlay = new LayerOverlay();
            importingOverlay.DrawingExceptionMode = DrawingExceptionMode.DrawException;
            importingOverlay.TileType = TileType.SingleTile;
            importingLayer = new InMemoryFeatureLayer();
            drawingpolygonshape = new MultipolygonShape();
            editingpolygonshape = new MultipolygonShape();
            drawinglineshape = new LineShape();
            polystring = string.Empty;
            if (multiplefiles) {
                importingLayer.FeatureSource.Open();
                foreach (FeatureSourceColumn fsc in columnlist) {
                    importingLayer.Columns.Add(fsc);
                }
                //FeatureSourceColumn column1 = new FeatureSourceColumn(@"CLIENT", "string", 254);
                //FeatureSourceColumn column2 = new FeatureSourceColumn(@"FARM", "string", 254);
                //FeatureSourceColumn column3 = new FeatureSourceColumn(@"FIELD", "string", 254);
                //importingLayer.Columns.Add(column1);
                //importingLayer.Columns.Add(column2);
                //importingLayer.Columns.Add(column3);
                importingLayer.FeatureSource.Close();
            }
            importingLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = AreaStyles.CreateSimpleAreaStyle(GeoColor.FromArgb(80, GeoColor.SimpleColors.Red), GeoColor.FromArgb(100, GeoColor.SimpleColors.Red));
            importingLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = LineStyles.CreateSimpleLineStyle(GeoColor.FromArgb(100, GeoColor.SimpleColors.Red), 2, true);
            importingLayer.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = PointStyles.CreateSimplePointStyle(PointSymbolType.Circle, GeoColor.FromArgb(100, GeoColor.SimpleColors.Red), 8);
            importingLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            importingLayer.FeatureSource.Projection = projection;
            importingLayer.IsVisible = true;
            importingOverlay.Layers.Add("importingLayer", importingLayer);
        }

        private bool CreateShapeFileLayerAndOverlay() {

            log.Info("ImportTools - Load import file: {0}.", newfilenamewithpath);
            if (File.Exists(newfilenamewithpath)) {
                string result = Path.GetExtension(newfilenamewithpath).Replace(".", "");
                if (result == "shp") {
                    shapeLayer = new ShapeFileFeatureLayer(newfilenamewithpath, ShapeFileReadWriteMode.ReadOnly);
                    ProjectToPRJFile(newfilenamewithpath);
                    shapeLayer.RequireIndex = false;
                    EnsureImportingLayerIsOpen();
                    Collection<Feature> allfeatures = shapeLayer.FeatureSource.GetAllFeatures(ReturningColumnsType.AllColumns);
                    foreach (Feature feat in allfeatures) {
                        importingLayer.InternalFeatures.Add(feat);
                        //BaseShape bs = feat.GetShape();
                        //if (bs is PolygonShape) {
                        //    PolygonShape poly = bs as PolygonShape;
                        //}
                        //if (bs is MultipolygonShape) {
                        //    MultipolygonShape mpoly = bs as MultipolygonShape;
                        //}

                    }
                }
                if (result == "gpx")
                {
                    gpxLayer = new GpxFeatureLayer(newfilenamewithpath);
                    ProjectToPRJFile(newfilenamewithpath);
                    EnsureImportingLayerIsOpen();
                    Collection<Feature> allfeatures = gpxLayer.FeatureSource.GetAllFeatures(ReturningColumnsType.AllColumns);
                    foreach (Feature feat in allfeatures)
                    {
                        BaseShape bs = feat.GetShape();
                        if (bs is MultilineShape)
                        {
                            MultilineShape mline = bs as MultilineShape;
                            PolygonShape poly = new PolygonShape(new RingShape(mline.Lines[0].Vertices));
                            MultipolygonShape mpoly = new MultipolygonShape();
                            mpoly.Polygons.Add(poly);
                            importingLayer.InternalFeatures.Add(new Feature(mpoly, feat.ColumnValues));
                        }
                        if (bs is PolygonShape)
                        {
                            PolygonShape poly = bs as PolygonShape;
                            importingLayer.InternalFeatures.Add(feat);
                        }
                        if (bs is MultipolygonShape)
                        {
                            MultipolygonShape mpoly = bs as MultipolygonShape;
                            importingLayer.InternalFeatures.Add(feat);
                        }
                    }
                }
                if (result == "kml")
                {
                    kmlLayer = new KmlFeatureLayer(newfilenamewithpath);
                    kmlLayer.RequireIndex = false;
                    //KmlFeatureLayer.BuildIndexFile(newfilenamewithpath, BuildIndexMode.DoNotRebuild);
                    ProjectToPRJFile(newfilenamewithpath);
                    EnsureImportingLayerIsOpen();
                    if (!kmlLayer.FeatureSource.IsOpen)
                    {
                        kmlLayer.FeatureSource.Open();
                    }
                    //if (!kmlLayer.IsOpen)
                    //{
                    //    kmlLayer.Open();
                    //}
                    Collection<Feature> allfeatures = kmlLayer.FeatureSource.GetAllFeatures(ReturningColumnsType.AllColumns);
                    foreach (Feature feat in allfeatures)
                    {
                        BaseShape bs = feat.GetShape();
                        if (bs is MultilineShape)
                        {
                            MultilineShape mline = bs as MultilineShape;
                            PolygonShape poly = new PolygonShape(new RingShape(mline.Lines[0].Vertices));
                            MultipolygonShape mpoly = new MultipolygonShape();
                            mpoly.Polygons.Add(poly);
                            importingLayer.InternalFeatures.Add(new Feature(mpoly, feat.ColumnValues));
                        }
                        if (bs is PolygonShape)
                        {
                            PolygonShape poly = bs as PolygonShape;
                            importingLayer.InternalFeatures.Add(feat);
                        }
                        if (bs is MultipolygonShape)
                        {
                            MultipolygonShape mpoly = bs as MultipolygonShape;
                            importingLayer.InternalFeatures.Add(feat);
                        }
                    }
                }

            }
            else {
                log.Info("ImportTools - File cannot be imported.");
                CloseAndCancelImport();
                return false;
            }
            return true;
        }

        private bool CreateApexShapeFileLayerAndOverlay() {
            bool firstrecord = true;
            foreach (string shapefilename in newfilenameswithpathlist) {
                log.Info("ImportTools - Load import file: {0}.", shapefilename);
                if (File.Exists(shapefilename)) {
                    string result = Path.GetExtension(shapefilename).Replace(".", "");
                    if (result == "shp") {
                        shapeLayer = new ShapeFileFeatureLayer(shapefilename, ShapeFileReadWriteMode.ReadOnly);
                        ProjectToPRJFile(shapefilename);
                        shapeLayer.RequireIndex = false;
                        EnsureImportingLayerIsOpen();
                        Collection<Feature> allfeatures = shapeLayer.FeatureSource.GetAllFeatures(ReturningColumnsType.AllColumns);
                        if (firstrecord) {
                            Collection<FeatureSourceColumn> columnlist = shapeLayer.FeatureSource.GetColumns();
                            InitializeImportingOverlay(false, columnlist);
                            EnsureImportingLayerIsOpen();
                            firstrecord = false;
                        }
                        foreach (Feature feat in allfeatures) {
                            importingLayer.InternalFeatures.Add(feat);

                            //farmId = new FarmId(this.CurrentDataSourceId, Guid.NewGuid());
                            //CreateFarm createFarmCommand = new CreateFarm(farmId, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), NewItemName, CurrentCropYear);
                            //clientEndpoint.SendOne(createFarmCommand);


                            //fieldId = new FieldId(this.CurrentDataSourceId, Guid.NewGuid());
                            //CreateField createFieldCommand = new CreateField(fieldId, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), NewItemName, farmVm.FarmId, currentCropYear, ReportedArea, ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit);
                            //clientEndpoint.SendOne(createFieldCommand);



                        }
                    }
                }
                else {
                    log.Info("ImportTools - File cannot be imported:  " + shapefilename);
                    CloseAndCancelImport();
                    return false;
                }
            }
            return true;
        }

        private void LoadShapeFileOverlayOntoMap() {
            log.Info("ImportTools - Load the import overlay and set extents.");
            if (map.Overlays.Contains("mportingOverlay")) {
                map.Overlays.Remove("mportingOverlay");
            }
            if (importingOverlay.Layers.Contains("importingLayer")) {
                if (!string.IsNullOrEmpty(FileName) || newfilenameswithpathlist != null) {
                    map.Overlays.Add("mportingOverlay", importingOverlay);
                    //map.Overlays.MoveToTop(importingOverlay.Name);
                }
            }
            if (!importingLayer.IsOpen) {
                importingLayer.Open();
            }

            EnsureImportingLayerIsOpen();
            var extent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
            try {
                var shapelayerfeatures = importingLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);
                if (shapelayerfeatures.Count > 0) {
                    map.CurrentExtent = importingLayer.GetBoundingBox();
                }
            }
            catch {
                log.Info("ImportTools - Problem calculating extent.");
            }
        }

        private void EnsureImportingLayerIsOpen() {
            if (importingLayer == null) {
                importingLayer = new InMemoryFeatureLayer();
            }

            if (!importingLayer.IsOpen) {
                importingLayer.Open();
            }

            if (shapeLayer == null) {
                shapeLayer = new ShapeFileFeatureLayer();
            }

            if (!string.IsNullOrWhiteSpace(shapeLayer.ShapePathFileName)) {
                if (!shapeLayer.IsOpen) {
                    shapeLayer.Open();
                }
            }

            if (gpxLayer == null)
            {
                gpxLayer = new GpxFeatureLayer();
            }

            if (!string.IsNullOrWhiteSpace(gpxLayer.GpxPathFilename))
            {
                if (!gpxLayer.IsOpen)
                {
                    gpxLayer.Open();
                }
            }

            if (kmlLayer == null)
            {
                kmlLayer = new KmlFeatureLayer();
            }

            if (projection != null) {
                if (!projection.IsOpen) {
                    projection.Open();
                }
            }
            if (wfsprojection != null) {
                if (!wfsprojection.IsOpen) {
                    wfsprojection.Open();
                }
            }
            if (wfs4326projection != null) {
                if (!wfs4326projection.IsOpen) {
                    wfs4326projection.Open();
                }
            }
            if (mouseprojection != null)
            {
                if (!mouseprojection.IsOpen)
                {
                    mouseprojection.Open();
                }
            }

        }

        private void ProjectToPRJFile(string shapefilename) {
            projection = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetGoogleMapParametersString());
            mouseprojection = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetGoogleMapParametersString());
            importingLayer.FeatureSource.Projection = projection;
            string tempFileBase = Path.Combine(Path.GetDirectoryName(shapefilename), Path.GetFileNameWithoutExtension(shapefilename));
            if (string.IsNullOrWhiteSpace(overrideprojection)) {
                newprojectionfilenamewithpath = string.Format("{0}.prj", tempFileBase);
                if (File.Exists(newprojectionfilenamewithpath)) {
                    Proj4Projection proj4 = new Proj4Projection();
                    string prjFileText = System.IO.File.ReadAllText(newprojectionfilenamewithpath);
                    proj4.InternalProjectionParametersString = Proj4Projection.ConvertPrjToProj4(prjFileText);
                    if (string.IsNullOrEmpty(proj4.InternalProjectionParametersString)) { return; }
                    proj4.ExternalProjectionParametersString = Proj4Projection.GetGoogleMapParametersString();
                    projection = new Proj4Projection(proj4.InternalProjectionParametersString, proj4.ExternalProjectionParametersString);
                    importingLayer.FeatureSource.Projection = projection;
                    mouseprojection = projection;
                }
            }
            else {
                if (overrideprojection.Length < 7) {
                    Proj4Projection proj4 = new Proj4Projection();
                    proj4.InternalProjectionParametersString = Proj4Projection.GetEpsgParametersString(System.Convert.ToInt32(overrideprojection));
                    if (string.IsNullOrEmpty(proj4.InternalProjectionParametersString)) { return; }
                    proj4.ExternalProjectionParametersString = Proj4Projection.GetGoogleMapParametersString();
                    projection = new Proj4Projection(proj4.InternalProjectionParametersString, proj4.ExternalProjectionParametersString);
                    importingLayer.FeatureSource.Projection = projection;
                    mouseprojection = projection;
                }
                else {
                    Proj4Projection proj4 = new Proj4Projection();
                    proj4.InternalProjectionParametersString = overrideprojection;
                    if (string.IsNullOrEmpty(proj4.InternalProjectionParametersString)) { return; }
                    proj4.ExternalProjectionParametersString = Proj4Projection.GetGoogleMapParametersString();
                    projection = new Proj4Projection(proj4.InternalProjectionParametersString, proj4.ExternalProjectionParametersString);
                    importingLayer.FeatureSource.Projection = projection;
                    mouseprojection = projection;
                }
            }
        }

        public IList<FarmTreeItemViewModel> AssignApexShapes(AbstractTreeItemViewModel selectedTreeItem) {

            UserSettings settings = clientEndpoint.GetUserSettings();
            bool sortAlphabetically = settings.AreFieldsSortedAlphabetically;
            //MapSettings mapsettings = clientEndpoint.GetMapSettings();
            //string areaUnitString = mapsettings.MapAreaUnit;
            AgC.UnitConversion.Area.AreaUnit agcAreaUnit = AgC.UnitConversion.UnitFactory.GetUnitByName(areaUnitString) as AgC.UnitConversion.Area.AreaUnit;
            AreaUnit mapAreaUnit = MapUnitFactory.AreaMeasureConversion(agcAreaUnit);
            bool hasfarm = false;
            bool hasfield = false;
            bool hasnotes = false;
            //Collection<FeatureSourceColumn> featColums = importingLayer.Columns;
            string stringfarmcolumnname = @"Farm";
            string stringfieldcolumnname = @"Field";
            string stringnotescolumnname1 = @"Notes1";
            string stringnotescolumnname2 = @"Notes2";
            if (importingLayer.InternalFeatures.Count > 0) {
                Feature featcol = importingLayer.InternalFeatures[0];
                if (!hasfarm) {
                    try {
                        if (!string.IsNullOrEmpty(featcol.ColumnValues["Farm"])) {
                            stringfarmcolumnname = "Farm";
                            hasfarm = true;
                        }
                    }
                    catch (Exception ex1) {
                        int i1 = 0;
                    }
                }
                if (!hasfarm) {
                    try {
                        if (!string.IsNullOrEmpty(featcol.ColumnValues["Farmname"])) {
                            stringfarmcolumnname = "Farmname";
                            hasfarm = true;
                        }
                    }
                    catch (Exception ex1) {
                        int i1 = 0;
                    }
                }
                if (!hasfarm) {
                    try {
                        if (!string.IsNullOrEmpty(featcol.ColumnValues["FARM_NAME"])) {
                            stringfarmcolumnname = "FARM_NAME";
                            hasfarm = true;
                        }
                    }
                    catch (Exception ex1) {
                        int i1 = 0;
                    }
                }
                if (!hasfarm)
                {
                    try
                    {
                        if (!string.IsNullOrEmpty(featcol.ColumnValues["Farm Name"]))
                        {
                            stringfarmcolumnname = "Farm Name";
                            hasfarm = true;
                        }
                    }
                    catch (Exception ex1)
                    {
                        int i1 = 0;
                    }
                }
                if (!hasfield) {
                    try {
                        if (!string.IsNullOrEmpty(featcol.ColumnValues["Field"])) {
                            stringfieldcolumnname = "Field";
                            hasfield = true;
                        }
                    }
                    catch (Exception ex1) {
                        int i1 = 0;
                    }
                }
                if (!hasfield) {
                    try {
                        if (!string.IsNullOrEmpty(featcol.ColumnValues["Fieldname"])) {
                            stringfieldcolumnname = "Fieldname";
                            hasfield = true;
                        }
                    }
                    catch (Exception ex1) {
                        int i1 = 0;
                    }
                }
                if (!hasfield) {
                    try {
                        if (!string.IsNullOrEmpty(featcol.ColumnValues["FIELD_NAME"])) {
                            stringfieldcolumnname = "FIELD_NAME";
                            hasfield = true;
                        }
                    }
                    catch (Exception ex1) {
                        int i1 = 0;
                    }
                }
                if (!hasfield)
                {
                    try
                    {
                        if (!string.IsNullOrEmpty(featcol.ColumnValues["Field Name"]))
                        {
                            stringfieldcolumnname = "Field Name";
                            hasfield = true;
                        }
                    }
                    catch (Exception ex1)
                    {
                        int i1 = 0;
                    }
                }
                if (!hasnotes)
                {
                    try
                    {
                        if (!string.IsNullOrEmpty(featcol.ColumnValues["Grower SAP"]))
                        {
                            stringnotescolumnname1 = "Grower SAP";
                        }
                        if (!string.IsNullOrEmpty(featcol.ColumnValues["Contract C"]))
                        {
                            stringnotescolumnname2 = "Contract C";
                        }
                        if(stringnotescolumnname1 == "Grower SAP" && stringnotescolumnname2 == "Contract C")
                        {
                            hasnotes = true;
                        }
                    }
                    catch (Exception ex1)
                    {
                        int i1 = 0;
                    }
                }
            }
            if (!hasfarm) {
                System.Windows.MessageBox.Show(Strings.ShapefileToBeImportedNotHaveRecognizedCol_Text);
                log.Info("ImportTools - File cannot be imported.");
                CloseAndCancelImport();
                return new List< FarmTreeItemViewModel > ();
            }
            if (!hasfield) {
                System.Windows.MessageBox.Show(Strings.ShapefileToBeImportedNotHaveRecognizedCol_Text);
                log.Info("ImportTools - File cannot be imported.");
                CloseAndCancelImport();
                return new List<FarmTreeItemViewModel>();
            }
            IList<FarmTreeItemViewModel> farmtreelist = new List<FarmTreeItemViewModel>();
            Dictionary<string, FarmId> farmlist = new Dictionary<string, FarmId>();
            foreach (Feature feature in importingLayer.InternalFeatures) {
                string farmName = feature.ColumnValues[stringfarmcolumnname];
                FarmId farmId = new FarmId(Landdb.Infrastructure.ApplicationEnvironment.CurrentDataSource.DataSourceId, Guid.NewGuid());
                if (!farmlist.ContainsKey(farmName)) {
                    farmlist.Add(farmName, farmId);
                    CreateFarm createFarmCommand = new CreateFarm(farmId, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), farmName, Landdb.Infrastructure.ApplicationEnvironment.CurrentCropYear);
                    clientEndpoint.SendOne(createFarmCommand);
                    System.Threading.Thread.Sleep(50); // This is stupid, right?
                    TreeViewFarmItem tvfi = new TreeViewFarmItem() { FarmId = farmId, Name = farmName, Fields = new List<TreeViewFieldItem>() };
                    FarmTreeItemViewModel newItem = new FarmTreeItemViewModel(tvfi, selectedTreeItem, sortAlphabetically);
                    farmtreelist.Add(newItem);

                    if(hasnotes)
                    {
                        string note1 = feature.ColumnValues[stringnotescolumnname1];
                        string note2 = feature.ColumnValues[stringnotescolumnname2];
                        string notes = $"Grower SAP#:  {note1},  Contract#:  {note2}";
                        //UpdateFarmNotes
                        UpdateFarmNotes createNotesCommand = new UpdateFarmNotes(farmId, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), notes);
                        clientEndpoint.SendOne(createFarmCommand);
                    }
                }
            }
            System.Threading.Thread.Sleep(1000); // This is stupid, right?
            foreach (Feature feature in importingLayer.InternalFeatures) {
                string fieldName = feature.ColumnValues[stringfieldcolumnname];
                string farmName = feature.ColumnValues[stringfarmcolumnname];
                int i = -999;
                for (int j = 0; j < farmtreelist.Count; j++) {
                    if (farmtreelist[j].Name == farmName) {
                        i = j;
                        break;
                    }
                }
                if (i < 0) { continue; }
                FieldId fieldId = new FieldId(Landdb.Infrastructure.ApplicationEnvironment.CurrentDataSource.DataSourceId, Guid.NewGuid());
                FarmId farmId = new FarmId();
                if (farmlist.ContainsKey(farmName)) {
                    farmId = farmlist[farmName];
                }
                else {
                    continue;
                }
                CreateField createFieldCommand = new CreateField(fieldId, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), fieldName, farmId, Landdb.Infrastructure.ApplicationEnvironment.CurrentCropYear, null, Landdb.Infrastructure.ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit);
                clientEndpoint.SendOne(createFieldCommand);

                BaseShape baseshape = feature.GetShape();
                AreaBaseShape areabaseshape = baseshape as AreaBaseShape;
                string spatialData = feature.GetShape().GetWellKnownText();
                spatialData = spatialData.Replace(@"\n", "");
                spatialData = spatialData.Replace(@"\r", "");
                spatialData = spatialData.Replace(@"\t", "");

                double area = 0;
                string cenlatlon = string.Empty;
                try {
                    area = areabaseshape.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
                    PointShape centerpoint = areabaseshape.GetCenterPoint();
                    Vertex geoVertex1 = new Vertex(centerpoint.X, centerpoint.Y);
                    string _DecimalDegreeString = Math.Round(geoVertex1.Y, 5) + ", " + Math.Round(geoVertex1.X, 5);
                    cenlatlon = _DecimalDegreeString;

                }
                catch {
                    area = 0;
                }

                var saveBoundaryCommand = new ChangeFieldBoundary(fieldId, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), spatialData, "WKT", area, areaUnitString, Landdb.Infrastructure.ApplicationEnvironment.CurrentCropYear);
                clientEndpoint.SendOne(saveBoundaryCommand);

                if (cenlatlon != string.Empty) {
                    var saveUpdateFieldLocationCommand = new UpdateFieldLocation(fieldId, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), cenlatlon, null, null, null);
                    clientEndpoint.SendOne(saveUpdateFieldLocationCommand);
                }
                TreeViewFieldItem tvfi = new TreeViewFieldItem() { FieldId = fieldId, Name = fieldName, CropZones = new List<TreeViewCropZoneItem>() };
                FieldTreeItemViewModel newItem = new FieldTreeItemViewModel(tvfi, farmtreelist[i], sortAlphabetically);
                farmtreelist[i].Children.Add(newItem);
                farmtreelist[i].IsExpanded = true;
            }
            log.Info("ImportTools - Apex import is finished");
            return farmtreelist;
        }

        public void CloseAndCancelImport() {
            importingfeatures = false;
            importingapexfeatures = false;
            importingclufeatures = false;
            currentextentchanged = false;
            mousedown = false;
            mousemove = false;
            polystring = string.Empty;
            newfilenamewithpath = string.Empty;
            //importingOverlay.Layers.Add("importingLayer", importingLayer);
            if (map.Overlays.Contains("mportingOverlay")) {
                map.Overlays.Remove("mportingOverlay");
            }
            map.Refresh();
        }

        public void FinishMassImport() {
            importingfeatures = false;
            importingapexfeatures = false;
            importingclufeatures = false;
            currentextentchanged = false;
            mousedown = false;
            mousemove = false;
            polystring = string.Empty;
            newfilenamewithpath = string.Empty;
            //importingOverlay.Layers.Add("importingLayer", importingLayer);
            if (map.Overlays.Contains("mportingOverlay")) {
                map.Overlays.Remove("mportingOverlay");
            }
            map.Refresh();
        }

        private Vertex GetMousePoint(MouseEventArgs e) {
            try {
                PointShape worldlocation2 = this.map.ToWorldCoordinate(e.GetPosition(map));
                PointShape centerpoint = projection.ConvertToInternalProjection(worldlocation2) as PointShape;
                Vertex geoVertex1 = new Vertex(centerpoint.X, centerpoint.Y);
                return geoVertex1;
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        public void HandleDrawPolygon() {
            map.TrackOverlay.TrackEnded += new EventHandler<TrackEndedTrackInteractiveOverlayEventArgs>(TrackOverlay_TrackEnded);
            map.TrackOverlay.TrackMode = TrackMode.Polygon;
            map.MapClick += new EventHandler<MapClickWpfMapEventArgs>(map_MapClick);
            map.KeyDown += new KeyEventHandler(keyEvent_Esc);
            previousCursor = map.Cursor;
            map.Cursor = Cursors.Pen;
            parent.IsMeasureLine = false;
        }

        public void HandleDrawPoint() {
            map.TrackOverlay.TrackEnded += new EventHandler<TrackEndedTrackInteractiveOverlayEventArgs>(TrackOverlay_TrackEnded);
            map.TrackOverlay.TrackMode = TrackMode.Point;
            map.MapClick += new EventHandler<MapClickWpfMapEventArgs>(map_MapClick);
            map.KeyDown += new KeyEventHandler(keyEvent_Esc);
            previousCursor = map.Cursor;
            map.Cursor = Cursors.Pen;
            parent.IsMeasureLine = false;
        }

        private void TrackOverlay_TrackEnded(object sender, TrackEndedTrackInteractiveOverlayEventArgs e) {
            map.TrackOverlay.TrackEnded -= new EventHandler<TrackEndedTrackInteractiveOverlayEventArgs>(TrackOverlay_TrackEnded);
            map.TrackOverlay.TrackMode = TrackMode.None;
            map.MapClick -= new EventHandler<MapClickWpfMapEventArgs>(map_MapClick);
            map.KeyDown -= new KeyEventHandler(keyEvent_Esc);
            map.Cursor = previousCursor;
            parent.HasUnsavedChanges = true;
        }

        private MultipolygonShape AddShapesAndCutOuts(MultipolygonShape multiPolygonShape, MultipolygonShape multipoly) {
            try {
                if (multiPolygonShape.Polygons.Count > 0 && multipoly.Polygons.Count > 0 && multiPolygonShape.Contains(multipoly)) {
                    MultipolygonShape resultingmulti = multiPolygonShape.GetDifference(multipoly);
                    multiPolygonShape = resultingmulti;
                }
                else if (multiPolygonShape.Polygons.Count > 0 && multipoly.Polygons.Count > 0 && multiPolygonShape.Overlaps(multipoly)) {
                    MultipolygonShape resultingmulti = multiPolygonShape.GetDifference(multipoly);
                    multiPolygonShape = resultingmulti;
                }
                else {
                    foreach (PolygonShape poly in multipoly.Polygons) {
                        multiPolygonShape.Polygons.Add(poly);
                    }
                }
            }
            catch (Exception ex) {
                log.ErrorException("There was a problem adding shapes and cutouts.", ex);
            }
            return multiPolygonShape;
        }

        public void HandleShapeEdit() {
            LayerOverlay fieldOverlay = (LayerOverlay)map.Overlays["fieldOverlay"];
            FeatureLayer currentLayer = (FeatureLayer)fieldOverlay.Layers["currentLayer"];
            System.Collections.ObjectModel.Collection<ThinkGeo.MapSuite.Core.Feature> selectedFeatures = currentLayer.FeatureSource.GetAllFeatures(ThinkGeo.MapSuite.Core.ReturningColumnsType.AllColumns);
            map.EditOverlay.EditShapesLayer.InternalFeatures.Clear();
            if (selectedFeatures.Count > 0) {

                map.EditOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = null;
                map.EditOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = null;
                map.EditOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = null;
                map.EditOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.DefaultTextStyle = null;
                map.EditOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Clear();
                map.EditOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customAreaPolygonStyleText);
                map.EditOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(areaStyleBrush);
                map.EditOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;

                map.EditOverlay.EditShapesLayer.InternalFeatures.Add(selectedFeatures[0]);
                map.EditOverlay.CalculateAllControlPoints();
                map.EditOverlay.Refresh();
                map.Cursor = Cursors.Hand;
                parent.HasUnsavedChanges = true;
            }
            parent.IsMeasureLine = false;
        }

        private void keyEvent_Esc(object sender, KeyEventArgs e) {
            if (e.Key == System.Windows.Input.Key.Escape) {
                if (map.TrackOverlay.TrackMode != TrackMode.None) {
                    map.TrackOverlay.TrackEnded -= new EventHandler<TrackEndedTrackInteractiveOverlayEventArgs>(TrackOverlay_TrackEnded);
                    map.KeyDown -= new KeyEventHandler(keyEvent_Esc);

                    if (map.TrackOverlay.TrackMode == TrackMode.Polygon || map.TrackOverlay.TrackMode == TrackMode.Line || map.TrackOverlay.TrackMode == TrackMode.Circle) {
                        map.TrackOverlay.MouseDoubleClick(new InteractionArguments());
                    }
                    else {
                        map.TrackOverlay.MouseUp(new InteractionArguments());
                    }

                    map.TrackOverlay.TrackMode = TrackMode.None;
                    map.Cursor = previousCursor;
                    parent.HasUnsavedChanges = false;

                    int count = map.TrackOverlay.TrackShapeLayer.InternalFeatures.Count;
                    if (count > 0) {
                        map.TrackOverlay.TrackShapeLayer.InternalFeatures.Remove("InTrackingFeature");
                    }
                    map.TrackOverlay.TrackShapeLayer.InternalFeatures.Clear();
                    try {
                        map.TrackOverlay.Refresh();
                    }
                    catch {}
                    map.TrackOverlay = new TrackInteractiveOverlay();
                }
            }
        }

        private void map_MapClick(object sender, MapClickWpfMapEventArgs e) {
            if (!map.IsFocused) {
                map.Focus();
            }
        }

        private void ResetAndRefreshLayers() {
            map.TrackOverlay.TrackShapeLayer.Open();
            map.TrackOverlay.TrackShapeLayer.Clear();
            map.TrackOverlay.TrackShapeLayer.Close();
            map.EditOverlay.EditShapesLayer.Open();
            map.EditOverlay.EditShapesLayer.Clear();
            map.EditOverlay.CalculateAllControlPoints();
            map.EditOverlay.EditShapesLayer.Close();
            map.Overlays["fieldOverlay"].Refresh();
            map.TrackOverlay.Refresh();
            map.EditOverlay.Refresh();
        }
    }
}
