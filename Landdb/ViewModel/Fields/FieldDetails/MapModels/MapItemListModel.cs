﻿using AgC.UnitConversion;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Map;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Fields.FieldDetails.MapModels {
    public class MapItemListModel {
        private static MapItemListModel _self;
        IClientEndpoint clientEndpoint;
        MapSettings mapsettings;
        int areadecimals = 2;

        public MapItemListModel() {
        }

        public static MapItemListModel Self
        {
            get
            {
                if (_self == null) {
                    lock (typeof(MapItemListModel)) {
                        if (_self == null) {
                            _self = new MapItemListModel();
                        }
                    }
                }
                return _self;
            }
        }
        public IList<MapItem> AllCropItems { get; private set; }
        public IList<MapItem> AllFieldItems { get; private set; }
        public IList<MapItem> AllMapItems { get; private set; }
        public IList<MapItem> filteredItems { get; private set; }

        public void SetEndPoint(IClientEndpoint clientEndpoint) {
            this.clientEndpoint = clientEndpoint;
            this.mapsettings = clientEndpoint.GetMapSettings();
            areadecimals = mapsettings.AreaDecimals;
            RebuildList();
        }

        public void RebuildList() {
            var years = clientEndpoint.GetView<FieldTreeView>(new DataSourceId(ApplicationEnvironment.CurrentDataSourceId));
            var yearItem = (years.HasValue && years.Value.CropYearList.ContainsKey(ApplicationEnvironment.CurrentCropYear)) ? years.Value.CropYearList[ApplicationEnvironment.CurrentCropYear] : new TreeViewYearItem();
            AllMapItems = yearItem.Maps.Where(x => x.IsVisible).ToList();

            var tree = clientEndpoint.GetView<CropTreeView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear)).GetValue(new CropTreeView());
            var q = from c in tree.Crops
                    from f in c.Farms
                    from fi in f.Fields
                    from cz in fi.CropZones
                    let fieldname = fi.Name
                    select new { f.FarmId, fi.FieldId, fieldname, cz.CropZoneId, cz.Name };

            IList<MapItem> czmaps = new List<MapItem>();
            foreach (var item in q) {
                var maps = clientEndpoint.GetView<ItemMap>(item.CropZoneId);
                try {
                    if (maps.HasValue) {
                        MapItem mi = new MapItem();
                        mi.FarmId = item.FarmId.Id;
                        mi.FieldId = item.FieldId.Id;
                        mi.CropZoneId = item.CropZoneId.Id;
                        mi.Name = item.Name;
                        if (maps.Value.MostRecentMapItem != null) {
                            mi.MapData = maps.Value.MostRecentMapItem.MapData;
                            mi.MapDataType = maps.Value.MostRecentMapItem.DataType;
                        }
                        czmaps.Add(mi);
                    }
                }
                finally { }
            }
            AllCropItems = czmaps;

            var ftree = clientEndpoint.GetView<CropTreeView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear)).GetValue(new CropTreeView());
            var fq = from c in ftree.Crops
                     from f in c.Farms
                     from fi in f.Fields
                     let fieldname = fi.Name
                     select new { f.FarmId, fi.FieldId, fieldname };

            IList<MapItem> fdmaps = new List<MapItem>();
            foreach (var item in fq) {
                var maps = clientEndpoint.GetView<ItemMap>(item.FieldId);
                try {
                    if (maps.HasValue) {
                        MapItem mi = new MapItem();
                        mi.FarmId = item.FarmId.Id;
                        mi.FieldId = item.FieldId.Id;
                        mi.Name = item.fieldname;
                        ItemMapItem mostrecentitem = GetMostRecentShapeByYear(maps);
                        if (mostrecentitem != null) {
                            mi.MapData = mostrecentitem.MapData;
                            mi.MapDataType = mostrecentitem.DataType;
                        }
                        fdmaps.Add(mi);
                    }
                }
                finally { }
            }
            AllFieldItems = fdmaps;
        }

        private static ItemMapItem GetMostRecentShapeByYear(Lokad.Cqrs.Maybe<ItemMap> maps_a)
        {
            ItemMapItem item_map_item = null;
            if (maps_a.HasValue && maps_a.Value.MostRecentMapItem != null && maps_a.Value.MostRecentMapItem.CropYearChangeOccurred <= Infrastructure.ApplicationEnvironment.CurrentCropYear)
            {
                item_map_item = maps_a.Value.MostRecentMapItem;
            }
            else
            {
                ItemMapItem currentyearmostrecentboundarychange = (from md in maps_a.Value.MapItems where (md.CropYearChangeOccurred <= Infrastructure.ApplicationEnvironment.CurrentCropYear) select md).LastOrDefault();
                if (currentyearmostrecentboundarychange != null && !string.IsNullOrEmpty(currentyearmostrecentboundarychange.MapData))
                {
                    item_map_item = currentyearmostrecentboundarychange;
                }
                if (item_map_item == null)
                {
                    item_map_item = maps_a.Value.MostRecentMapItem;
                }
            }
            return item_map_item;
        }
    }
}
            
        



    

