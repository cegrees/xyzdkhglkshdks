﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThinkGeo.MapSuite.Core;

namespace Landdb.ViewModel.Fields.FieldDetails.MapModels {
    public class ZoomLevelFactory {
        IList<double> elevations = new List<double>();
        ZoomLevelSet zoomLevelSET = new ZoomLevelSet();
        private static System.Collections.ObjectModel.Collection<ZoomLevel> googlezoomlevellist;

        public ZoomLevelFactory() {
            //            IList<double> elevationlist = InitializeListSurvey40();
            IList<double> elevationlist = new List<double>();
            SetZoomLevels(elevationlist);
        }

        public ZoomLevelFactory(string zoomlevel) {
            IList<double> elevationlist = new List<double>();
            elevationlist = InitializeListStandardPlus40();
            if (zoomlevel == "Google40") {
                elevationlist = InitializeListGooglePlus40();
            }
            //if (zoomlevel == "Custom20") {
            //    elevationlist = InitializeList20();
            //}
            //else if (zoomlevel == "Custom40") {
            //    elevationlist = InitializeListSurvey40();
            //}
            //else if (zoomlevel == "Custom60") {
            //    elevationlist = InitializeList60();
            //}
            //else if (zoomlevel == "Survey40") {
            //    elevationlist = InitializeListSurvey40();
            //}
            //else if (zoomlevel == "StandardPlus20") {
            //    elevationlist = InitializeListStandardPlus20();
            //}
            //else if (zoomlevel == "StandardPlus40") {
            //    elevationlist = InitializeListStandardPlus40();
            //}
            //else if (zoomlevel == "StandardPlus60") {
            //    elevationlist = InitializeListStandardPlus60();
            //}
            SetZoomLevels(elevationlist);
        }

        public ZoomLevelSet SetZoomLevels(IList<double> elevationlist) {
            zoomLevelSET = new ZoomLevelSet();
            if (elevationlist.Count > 0) {
                foreach (double item in elevationlist) {
                    zoomLevelSET.CustomZoomLevels.Add(new ZoomLevel(item));
                }
            }
            else {
                zoomLevelSET = new ZoomLevelSet();
            }
            return zoomLevelSET;
        }

        public IList<double> Elevations {
            get { return elevations; }
            set {
                elevations = value;

            }
        }

        public ZoomLevelSet ZoomLevelSET {
            get { return zoomLevelSET; }
            set {
                zoomLevelSET = value;

            }
        }

        private IList<double> InitializeList() {
            elevations = new List<double>();
            elevations.Add(9244649.2269999981);
            elevations.Add(2311162.3070000061);
            elevations.Add(577790.57670000347);
            elevations.Add(288895.2884000064);
            elevations.Add(144447.64419999777);
            elevations.Add(72223.822090002068);
            elevations.Add(54167.8665600033);
            elevations.Add(36111.911040002204);
            elevations.Add(30093.25920000183);
            elevations.Add(24074.60736000147);
            elevations.Add(18055.955520001102);
            elevations.Add(15046.62960167041);
            elevations.Add(12037.30368133633);
            elevations.Add(9027.9777610022466);
            elevations.Add(4513.98887999707);
            elevations.Add(2256.9944400034765);
            elevations.Add(1128.4972199972628);
            elevations.Add(564.248609998631);
            elevations.Add(282.1243049993155);
            elevations.Add(105.7966143747434);
            return elevations;
        }

        private IList<double> InitializeList2() {
            elevations = new List<double>();
            elevations.Add(9244649.2269999981);
            elevations.Add(2311162.3070000061);
            elevations.Add(577790.57670000347);
            elevations.Add(288895.2884000064);
            elevations.Add(144447.64419999777);
            elevations.Add(72223.822090002068);
            elevations.Add(54167.8665600033);
            elevations.Add(36111.911040002204);
            elevations.Add(34983.41381996003);
            elevations.Add(33854.91659996277);
            elevations.Add(32726.41937996551);
            elevations.Add(31597.92215996824);
            elevations.Add(30469.42493997098);
            elevations.Add(29340.92771997372);
            elevations.Add(28212.43049997646);
            elevations.Add(27083.9332799792);
            elevations.Add(25955.43605998193);
            elevations.Add(24826.93883998467);
            elevations.Add(23698.44161998741);
            elevations.Add(22569.94439999015);
            elevations.Add(21441.447179992897);
            elevations.Add(20312.94995999562);
            elevations.Add(19184.45273999836);
            elevations.Add(18055.955520001102);
            elevations.Add(16927.458300983081);
            elevations.Add(15798.96108098582);
            elevations.Add(14670.46386098856);
            elevations.Add(13541.96664099129);
            elevations.Add(12413.46942099403);
            elevations.Add(11284.97220099677);
            elevations.Add(10156.47497998431);
            elevations.Add(9027.9777610022466);
            elevations.Add(8463.729149988417);
            elevations.Add(7899.480539989786);
            elevations.Add(7335.231929991155);
            elevations.Add(6770.983319992524);
            elevations.Add(6206.734709993893);
            elevations.Add(5642.486099995262);
            elevations.Add(5078.237489996631);
            elevations.Add(4513.98887999707);
            elevations.Add(3949.740269999369);
            elevations.Add(3385.491660000738);
            elevations.Add(2821.243050002107);
            elevations.Add(2256.9944400034765);
            elevations.Add(1692.745829995893);
            elevations.Add(1128.4972199972628);
            elevations.Add(564.248609998631);
            elevations.Add(282.1243049993155);
            elevations.Add(105.7966143747434);
            return elevations;
        }

        //public ZoomLevelSet GetCustom1ZoomLevelSet() {
        //    ZoomLevelSet zoomLevelSet = new ZoomLevelSet();
        //    zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(18489298.45));
        //    zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(9244649.2269999981));
        //    zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(4622324.614));
        //    zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(2311162.3070000061));
        //    zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(577790.57670000347));
        //    zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(288895.2884000064));
        //    zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(144447.64419999777));
        //    zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(72223.822090002068));
        //    zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(60186.51840000366));
        //    zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(48149.21472000293));
        //    zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(36111.911040002204));
        //    zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(30093.25920000183));
        //    zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(24074.60736000147));
        //    zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(18055.955520001102));
        //    zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(15046.62960167041));
        //    zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(12037.30368133633));
        //    zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(9027.9777610022466));
        //    zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(4513.98887999707));
        //    zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(2256.9944400034765));
        //    zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(1128.4972199972628));
        //    return zoomLevelSet;
        //}

        private IList<double> InitializeListStandard() {
            elevations = new List<double>();
            elevations.Add(591657550.4);
            elevations.Add(295828775.2);
            elevations.Add(147914387.6);
            elevations.Add(73957193.8);
            elevations.Add(36978596.9);
            elevations.Add(18489298.45);
            elevations.Add(9244649.2269999981);
            elevations.Add(4622324.614);
            elevations.Add(2311162.3070000061);
            elevations.Add(1155581.153400007);
            elevations.Add(577790.57670000347);
            elevations.Add(288895.2884000064);
            elevations.Add(144447.64419999777);
            elevations.Add(72223.822090002068);
            elevations.Add(36111.911040002204);
            elevations.Add(18055.955520001102);
            elevations.Add(9027.9777610022466);
            elevations.Add(4513.98887999707);
            elevations.Add(2256.9944400034765);
            elevations.Add(1128.4972199972628);
            return elevations;
        }

        private IList<double> InitializeListStandardPlus20() {
            elevations = new List<double>();
            elevations.Add(591657550.4);
            elevations.Add(295828775.2);
            elevations.Add(147914387.6);
            elevations.Add(73957193.8);
            elevations.Add(36978596.9);
            elevations.Add(18489298.45);
            elevations.Add(9244649.2269999981);
            elevations.Add(4622324.614);
            elevations.Add(2311162.3070000061);
            elevations.Add(1155581.153400007);

            elevations.Add(577790.57670000347);
            elevations.Add(288895.2884000064);
            elevations.Add(144447.64419999777);
            elevations.Add(72223.822090002068);
            elevations.Add(54167.8665600033);
            elevations.Add(36111.911040002204);
            elevations.Add(30093.25920000183);
            elevations.Add(24074.60736000147);
            elevations.Add(18055.955520001102);
            elevations.Add(15046.62960167041);

            elevations.Add(12037.30368133633);
            elevations.Add(9027.9777610022466);
            elevations.Add(6770.983319992524);
            elevations.Add(4513.98887999707);
            elevations.Add(3385.491660000738);
            elevations.Add(2256.9944400034765);
            elevations.Add(1692.745829995893);
            elevations.Add(1128.4972199972628);
            elevations.Add(564.248609998631);
            elevations.Add(282.1243049993155);
            elevations.Add(105.7966143747434);
            return elevations;
        }

        private IList<double> InitializeListStandardPlus40() {
            elevations = new List<double>();
            elevations.Add(591657550.4);
            elevations.Add(295828775.2);
            elevations.Add(147914387.6);
            elevations.Add(73957193.8);
            elevations.Add(36978596.9);
            elevations.Add(18489298.45);
            elevations.Add(9244649.2269999981);
            elevations.Add(4622324.614);
            elevations.Add(2311162.3070000061);
            elevations.Add(1155581.153400007);

            elevations.Add(577790.57670000347);
            elevations.Add(288895.2884000064);
            elevations.Add(144447.64419999777);
            elevations.Add(72223.822090002068);
            elevations.Add(54167.8665600033);

            elevations.Add(44011.39157999183);
            elevations.Add(41754.39713998836);
            elevations.Add(39497.40269998488);
            elevations.Add(37240.40825998141);
            elevations.Add(34983.41381997793);
            elevations.Add(32726.41937997446);
            //elevations.Add(36111.911040002204);
            //elevations.Add(30469.42493997098);
            elevations.Add(28212.43049997646);
            elevations.Add(25955.43605998193);
            elevations.Add(23698.44161998741);

            elevations.Add(21441.447179992897);
            elevations.Add(19184.45273999836);
            elevations.Add(18055.955520001102);
            elevations.Add(16927.458300983081);
            elevations.Add(15798.96108098582);
            elevations.Add(14670.46386098856);
            elevations.Add(13541.96664099129);
            elevations.Add(12413.46942099403);
            elevations.Add(11284.97220099677);
            elevations.Add(10156.47497998431);

            elevations.Add(9027.9777610022466);
            elevations.Add(8463.729149988417);
            elevations.Add(7899.480539989786);
            elevations.Add(7335.231929991155);
            elevations.Add(6770.983319992524);
            elevations.Add(6206.734709993893);
            elevations.Add(5642.486099995262);
            elevations.Add(5078.237489996631);
            elevations.Add(4513.98887999707);
            elevations.Add(3949.740269999369);

            elevations.Add(3385.491660000738);
            elevations.Add(2821.243050002107);
            elevations.Add(2256.9944400034765);
            elevations.Add(1692.745829995893);
            elevations.Add(1128.4972199972628);
            elevations.Add(564.248609998631);
            elevations.Add(282.1243049993155);
            elevations.Add(105.7966143747434);
            return elevations;
        }

        //private IList<double> InitializeListStandardPlus40() {
        //    elevations = new List<double>();
        //    //elevations.Add(591657550.4);
        //    //elevations.Add(295828775.2);
        //    //elevations.Add(147914387.6);
        //    elevations.Add(73957193.8);
        //    elevations.Add(36978596.9);

        //    elevations.Add(18489298.45);
        //    elevations.Add(9244649.2269999981);
        //    elevations.Add(4622324.614);
        //    elevations.Add(2311162.3070000061);
        //    elevations.Add(1155581.153400007);


        //    elevations.Add(577790.57670000347);
        //    elevations.Add(288895.2884000064);
        //    elevations.Add(144447.64419999777);
        //    elevations.Add(72223.822090002068);
        //    elevations.Add(54167.8665600033);

        //    elevations.Add(44011.39157999183);
        //    elevations.Add(41754.39713998836);
        //    elevations.Add(39497.40269998488);
        //    elevations.Add(37240.40825998141);
        //    elevations.Add(34983.41381997793);
        //    elevations.Add(32726.41937997446);
        //    //elevations.Add(36111.911040002204);
        //    elevations.Add(30469.42493997098);
        //    elevations.Add(28212.43049997646);
        //    elevations.Add(25955.43605998193);
        //    elevations.Add(23698.44161998741);


        //    elevations.Add(21441.447179992897);
        //    elevations.Add(19184.45273999836);
        //    elevations.Add(18055.955520001102);
        //    elevations.Add(16927.458300983081);
        //    elevations.Add(15798.96108098582);

        //    elevations.Add(14670.46386098856);
        //    elevations.Add(13541.96664099129);
        //    elevations.Add(12413.46942099403);
        //    elevations.Add(11284.97220099677);
        //    elevations.Add(10156.47497998431);


        //    elevations.Add(9027.9777610022466);
        //    elevations.Add(8463.729149988417);
        //    elevations.Add(7899.480539989786);
        //    elevations.Add(7335.231929991155);
        //    elevations.Add(6770.983319992524);

        //    elevations.Add(6206.734709993893);
        //    elevations.Add(5642.486099995262);
        //    elevations.Add(5078.237489996631);
        //    elevations.Add(4513.98887999707);
        //    elevations.Add(3949.740269999369);


        //    elevations.Add(3385.491660000738);
        //    elevations.Add(2821.243050002107);
        //    elevations.Add(2256.9944400034765);
        //    elevations.Add(1692.745829995893);
        //    elevations.Add(1128.4972199972628);

        //    elevations.Add(564.248609998631);
        //    elevations.Add(282.1243049993155);
        //    elevations.Add(105.7966143747434);
        //    return elevations;
        //}



        private IList<double> InitializeListStandardPlus60() {
            elevations = new List<double>();
            //elevations.Add(591657550.4);
            //elevations.Add(295828775.2);
            //elevations.Add(147914387.6);
            elevations.Add(73957193.8);
            elevations.Add(36978596.9);

            elevations.Add(18489298.45);
            elevations.Add(9244649.2269999981);
            elevations.Add(4622324.614);
            elevations.Add(2311162.3070000061);
            elevations.Add(1155581.153400007);

            elevations.Add(577790.57670000347);
            elevations.Add(288895.2884000064);
            elevations.Add(144447.64419999777);
            elevations.Add(72223.822090002068);
            elevations.Add(66581.33597997718);
            elevations.Add(62067.34709998011);
            elevations.Add(57553.35821998304);
            elevations.Add(53039.36933998597);
            elevations.Add(48525.3804599889);
            elevations.Add(44011.39157999183);
            elevations.Add(41754.39713998836);
            elevations.Add(39497.40269998488);
            elevations.Add(37240.40825998141);
            elevations.Add(34983.41381997793);


            elevations.Add(32726.41937997446);
            elevations.Add(30469.42493997098);
            elevations.Add(28212.43049997646);
            elevations.Add(25955.43605998193);
            elevations.Add(23698.44161998741);

            elevations.Add(21441.447179992897);
            elevations.Add(19184.45273999836);
            elevations.Add(18055.955520001102);
            elevations.Add(16927.458300983081);
            elevations.Add(15798.96108098582);


            elevations.Add(14670.46386098856);
            elevations.Add(13541.96664099129);
            elevations.Add(12413.46942099403);
            elevations.Add(11284.97220099677);
            elevations.Add(10156.47497998431);

            elevations.Add(9027.9777610022466);
            elevations.Add(8463.729149988417);
            elevations.Add(7899.480539989786);
            elevations.Add(7335.231929991155);
            elevations.Add(6770.983319992524);


            elevations.Add(6206.734709993893);
            elevations.Add(5642.486099995262);
            elevations.Add(5078.237489996631);
            elevations.Add(4513.98887999707);
            elevations.Add(3949.740269999369);

            elevations.Add(3385.491660000738);
            elevations.Add(2821.243050002107);
            elevations.Add(2256.9944400034765);
            elevations.Add(1692.745829995893);
            elevations.Add(1128.4972199972628);


            elevations.Add(564.248609998631);
            elevations.Add(282.1243049993155);
            elevations.Add(105.7966143747434);
            return elevations;
        }

        private IList<double> InitializeList20() {
            elevations = new List<double>();
            elevations.Add(9244649.2269999981);
            elevations.Add(2311162.3070000061);
            elevations.Add(577790.57670000347);
            elevations.Add(288895.2884000064);
            elevations.Add(144447.64419999777);
            elevations.Add(72223.822090002068);
            elevations.Add(54167.8665600033);
            elevations.Add(36111.911040002204);
            elevations.Add(30093.25920000183);
            elevations.Add(24074.60736000147);
            elevations.Add(18055.955520001102);
            elevations.Add(15046.62960167041);
            elevations.Add(12037.30368133633);
            elevations.Add(9027.9777610022466);
            elevations.Add(4513.98887999707);
            elevations.Add(2256.9944400034765);
            elevations.Add(1128.4972199972628);
            elevations.Add(564.248609998631);
            elevations.Add(282.1243049993155);
            elevations.Add(105.7966143747434);
            return elevations;
        }

        private IList<double> InitializeList40() {
            elevations = new List<double>();
            elevations.Add(9244649.2269999981);
            elevations.Add(2311162.3070000061);
            elevations.Add(577790.57670000347);
            elevations.Add(288895.2884000064);
            elevations.Add(144447.64419999777);
            elevations.Add(72223.822090002068);
            elevations.Add(54167.8665600033);
            elevations.Add(36111.911040002204);
            elevations.Add(30469.42493997098);
            elevations.Add(28212.43049997646);

            elevations.Add(25955.43605998193);
            elevations.Add(23698.44161998741);
            elevations.Add(21441.447179992897);
            elevations.Add(19184.45273999836);
            elevations.Add(18055.955520001102);
            elevations.Add(16927.458300983081);
            elevations.Add(15798.96108098582);
            elevations.Add(14670.46386098856);
            elevations.Add(13541.96664099129);
            elevations.Add(12413.46942099403);

            elevations.Add(11284.97220099677);
            elevations.Add(10156.47497998431);
            elevations.Add(9027.9777610022466);
            elevations.Add(8463.729149988417);
            elevations.Add(7899.480539989786);
            elevations.Add(7335.231929991155);
            elevations.Add(6770.983319992524);
            elevations.Add(6206.734709993893);
            elevations.Add(5642.486099995262);
            elevations.Add(5078.237489996631);

            elevations.Add(4513.98887999707);
            elevations.Add(3949.740269999369);
            elevations.Add(3385.491660000738);
            elevations.Add(2821.243050002107);
            elevations.Add(2256.9944400034765);
            elevations.Add(1692.745829995893);
            elevations.Add(1128.4972199972628);
            elevations.Add(564.248609998631);
            elevations.Add(282.1243049993155);
            elevations.Add(105.7966143747434);
            return elevations;
        }

        private IList<double> InitializeList60() {
            elevations = new List<double>();
            elevations.Add(9244649.2269999981);
            elevations.Add(2311162.3070000061);
            elevations.Add(577790.57670000347);
            elevations.Add(288895.2884000064);
            elevations.Add(252783.3773200043);
            elevations.Add(216671.4662800021);
            elevations.Add(180559.5552399999);
            elevations.Add(144447.64419999777);
            elevations.Add(126391.6886500054);
            elevations.Add(108335.7331300043);

            elevations.Add(90279.77761000316);
            elevations.Add(72223.822090002068);
            elevations.Add(67709.83319998169);
            elevations.Add(63195.84431998462);
            elevations.Add(58681.85543998755);
            elevations.Add(49653.87767999341);
            elevations.Add(45139.88879999634);
            elevations.Add(40625.89991999927);
            elevations.Add(36111.911040002204);
            elevations.Add(34983.41381996003);

            elevations.Add(33854.91659996277);
            elevations.Add(32726.41937996551);
            elevations.Add(31597.92215996824);
            elevations.Add(30469.42493997098);
            elevations.Add(29340.92771997372);
            elevations.Add(28212.43049997646);
            elevations.Add(27083.9332799792);
            elevations.Add(25955.43605998193);
            elevations.Add(24826.93883998467);
            elevations.Add(23698.44161998741);

            elevations.Add(22569.94439999015);
            elevations.Add(21441.447179992897);
            elevations.Add(20312.94995999562);
            elevations.Add(19184.45273999836);
            elevations.Add(18055.955520001102);
            elevations.Add(16927.458300983081);
            elevations.Add(15798.96108098582);
            elevations.Add(14670.46386098856);
            elevations.Add(13541.96664099129);
            elevations.Add(12413.46942099403);

            elevations.Add(11284.97220099677);
            elevations.Add(10156.47497998431);
            elevations.Add(9027.9777610022466);
            elevations.Add(8463.729149988417);
            elevations.Add(7899.480539989786);
            elevations.Add(7335.231929991155);
            elevations.Add(6770.983319992524);
            elevations.Add(6206.734709993893);
            elevations.Add(5642.486099995262);
            elevations.Add(5078.237489996631);

            elevations.Add(4513.98887999707);
            elevations.Add(3949.740269999369);
            elevations.Add(3385.491660000738);
            elevations.Add(2821.243050002107);
            elevations.Add(2256.9944400034765);
            elevations.Add(1692.745829995893);
            elevations.Add(1128.4972199972628);
            elevations.Add(564.248609998631);
            elevations.Add(282.1243049993155);
            elevations.Add(105.7966143747434);
            return elevations;
        }

        private IList<double> InitializeListSurvey40() {
            elevations = new List<double>();
            elevations.Add(11765160);
            elevations.Add(6485160);
            elevations.Add(1205160);
            elevations.Add(677160);
            elevations.Add(149160);
            elevations.Add(96360);
            elevations.Add(43560);
            elevations.Add(42240);
            elevations.Add(40920);
            elevations.Add(39600);

            elevations.Add(38280);
            elevations.Add(36960);
            elevations.Add(35640);
            elevations.Add(34320);
            elevations.Add(33000);
            elevations.Add(31680);
            elevations.Add(30360);
            elevations.Add(29040);
            elevations.Add(27720);
            elevations.Add(26400);

            elevations.Add(25080);
            elevations.Add(23760);
            elevations.Add(22440);
            elevations.Add(21120);
            elevations.Add(19800);
            elevations.Add(18480);
            elevations.Add(17160);
            elevations.Add(15840);
            elevations.Add(14520);
            elevations.Add(13200);

            elevations.Add(11880);
            elevations.Add(10560);
            elevations.Add(9240);
            elevations.Add(7920);
            elevations.Add(6600);
            elevations.Add(5280);
            elevations.Add(3960);
            elevations.Add(2640);
            elevations.Add(1320);
            elevations.Add(500);
            return elevations;
        }

        public ZoomLevelSet SetZoomLevels(ThinkGeo.MapSuite.WpfDesktopEdition.WpfMap map, RectangleShape boundingbox) {
            if (map.ActualWidth <= 0) { return zoomLevelSET; }
            bool scalefound = false;
            bool scaleupdated = false;
            double newScale = ExtentHelper.GetScale(boundingbox, (float)map.ActualWidth, map.MapUnit);
            newScale += 500;
            zoomLevelSET = new ZoomLevelSet();
            IList<double> elevationlist = InitializeList();
            foreach (double item in elevationlist) {
                if (newScale >= item) { scalefound = true; }
                if (scalefound && !scaleupdated) {
                    zoomLevelSET.CustomZoomLevels.Add(new ZoomLevel(newScale));
                    scaleupdated = true;
                }
                else {
                    zoomLevelSET.CustomZoomLevels.Add(new ZoomLevel(item));
                }
            }
            return zoomLevelSET;
        }

        //double maxScale = ExtentHelper.GetScale(currentExtent, mapActualWidth, mapUnit);
        //private void buttonZoomTo_Click(object sender, EventArgs e) {
        //    RectangleShape rect = new RectangleShape(-20, 20, 20, -20);

        //    double targetScale = ExtentHelper.GetScale(rect, winformsMap1.Width, GeographyUnit.DecimalDegree);
        //    double currentScale = targetScale;

        //    ZoomLevelSet set = new ZoomLevelSet();
        //    for (int i = 0; i < 20; i++) {
        //        set.CustomZoomLevels.Add(new ZoomLevel(currentScale));
        //        currentScale = currentScale / 2;
        //    }

        //    winformsMap1.ZoomLevelSet = set;

        //    winformsMap1.CurrentExtent = rect;
        //    winformsMap1.Refresh();
        //}




        public ZoomLevelSet GetBingMapsZoomLevelSet() {
            ZoomLevelSet zoomLevelSet = new BingMapsZoomLevelSet();
            return zoomLevelSet;
        }

        public ZoomLevelSet GetGoogleMapsZoomLevelSet() {
            ZoomLevelSet zoomLevelSet = new GoogleMapsZoomLevelSet();
            //ZoomLevelSet zoomLevelSet = SetZoomLevels(InitializeListGooglePlus40());
            return zoomLevelSet;
        }

        public ZoomLevelSet GetSphericalMercatorZoomLevelSet() {
            ZoomLevelSet zoomLevelSet = new SphericalMercatorZoomLevelSet();
            return zoomLevelSet;
        }

        public ZoomLevelSet GetOpenStreetMapZoomLevelSet() {
            ZoomLevelSet zoomLevelSet = new OpenStreetMapsZoomLevelSet();
            return zoomLevelSet;
        }

        public ZoomLevelSet GetCustom1ZoomLevelSet() {
            ZoomLevelSet zoomLevelSet = new ZoomLevelSet();
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(18489298.45));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(9244649.2269999981));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(4622324.614));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(2311162.3070000061));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(577790.57670000347));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(288895.2884000064));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(144447.64419999777));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(72223.822090002068));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(60186.51840000366));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(48149.21472000293));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(36111.911040002204));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(30093.25920000183));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(24074.60736000147));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(18055.955520001102));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(15046.62960167041));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(12037.30368133633));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(9027.9777610022466));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(4513.98887999707));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(2256.9944400034765));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(1128.4972199972628));
            return zoomLevelSet;
        }

        public ZoomLevelSet GetCustom2ZoomLevelSet() {
            ZoomLevelSet zoomLevelSet = new ZoomLevelSet();
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(18489298.45));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(9244649.2269999981));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(4622324.614));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(2311162.3070000061));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(577790.57670000347));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(288895.2884000064));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(144447.64419999777));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(72223.822090002068));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(54167.8665600033));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(36111.911040002204));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(30093.25920000183));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(24074.60736000147));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(18055.955520001102));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(15046.62960167041));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(12037.30368133633));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(9027.9777610022466));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(4513.98887999707));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(2256.9944400034765));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(1128.4972199972628));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(564.248609998631));
            return zoomLevelSet;
        }

        public ZoomLevelSet GetCustom3ZoomLevelSet() {
            ZoomLevelSet zoomLevelSet = new ZoomLevelSet();
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(9244649.2269999981));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(4622324.614));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(2311162.3070000061));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(577790.57670000347));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(288895.2884000064));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(144447.64419999777));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(72223.822090002068));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(36111.911040002204));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(30093.25920000183));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(24074.60736000147));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(18055.955520001102));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(15046.62960167041));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(12037.30368133633));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(9027.9777610022466));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(4513.98887999707));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(2256.9944400034765));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(1128.4972199972628));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(564.248609998631));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(282.1243049993155));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(141.0621524996578));
            return zoomLevelSet;
        }

        public ZoomLevelSet GetCustomZoomLevelSet() {
            ZoomLevelSet zoomLevelSet = new ZoomLevelSet();
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(9244649.2269999981));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(2311162.3070000061));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(577790.57670000347));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(288895.2884000064));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(144447.64419999777));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(72223.822090002068));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(54167.8665600033));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(36111.911040002204));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(30093.25920000183));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(24074.60736000147));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(18055.955520001102));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(15046.62960167041));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(12037.30368133633));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(9027.9777610022466));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(4513.98887999707));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(2256.9944400034765));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(1128.4972199972628));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(564.248609998631));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(282.1243049993155));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(105.7966143747434));
            return zoomLevelSet;
        }

        public ZoomLevelSet GetCustomScale1ZoomLevelSet(double initialScale) {
            ZoomLevelSet zoomLevelSet = new ZoomLevelSet();
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(initialScale));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(initialScale / 2));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(initialScale / 4));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(initialScale / 8));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(initialScale / 16));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(initialScale / 32));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(initialScale / 64));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(initialScale / 128));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(initialScale / 256));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(initialScale / 512));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(initialScale / 1024));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(initialScale / 2048));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(initialScale / 4096));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(initialScale / 8192));
            return zoomLevelSet;
        }

        public ZoomLevelSet GetCustomScale2ZoomLevelSet(ThinkGeo.MapSuite.WpfDesktopEdition.WpfMap map) {
            ZoomLevelSet zoomLevelSet = new ZoomLevelSet();
            zoomLevelSet.CustomZoomLevels.Add(map.ZoomLevelSet.ZoomLevel02);
            zoomLevelSet.CustomZoomLevels.Add(map.ZoomLevelSet.ZoomLevel03);
            zoomLevelSet.CustomZoomLevels.Add(map.ZoomLevelSet.ZoomLevel04);
            zoomLevelSet.CustomZoomLevels.Add(map.ZoomLevelSet.ZoomLevel01);
            zoomLevelSet.CustomZoomLevels.Add(map.ZoomLevelSet.ZoomLevel05);
            zoomLevelSet.CustomZoomLevels.Add(map.ZoomLevelSet.ZoomLevel06);
            zoomLevelSet.CustomZoomLevels.Add(map.ZoomLevelSet.ZoomLevel07);
            zoomLevelSet.CustomZoomLevels.Add(map.ZoomLevelSet.ZoomLevel08);
            zoomLevelSet.CustomZoomLevels.Add(map.ZoomLevelSet.ZoomLevel09);
            zoomLevelSet.CustomZoomLevels.Add(map.ZoomLevelSet.ZoomLevel10);
            zoomLevelSet.CustomZoomLevels.Add(map.ZoomLevelSet.ZoomLevel11);
            zoomLevelSet.CustomZoomLevels.Add(map.ZoomLevelSet.ZoomLevel12);
            zoomLevelSet.CustomZoomLevels.Add(map.ZoomLevelSet.ZoomLevel13);
            zoomLevelSet.CustomZoomLevels.Add(map.ZoomLevelSet.ZoomLevel14);
            zoomLevelSet.CustomZoomLevels.Add(map.ZoomLevelSet.ZoomLevel15);
            zoomLevelSet.CustomZoomLevels.Add(map.ZoomLevelSet.ZoomLevel16);
            zoomLevelSet.CustomZoomLevels.Add(map.ZoomLevelSet.ZoomLevel17);
            zoomLevelSet.CustomZoomLevels.Add(map.ZoomLevelSet.ZoomLevel18);
            zoomLevelSet.CustomZoomLevels.Add(map.ZoomLevelSet.ZoomLevel19);
            zoomLevelSet.CustomZoomLevels.Add(map.ZoomLevelSet.ZoomLevel20);
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(map.ZoomLevelSet.ZoomLevel20.Scale / 2));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(map.ZoomLevelSet.ZoomLevel20.Scale / 4));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(map.ZoomLevelSet.ZoomLevel20.Scale / 8));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(map.ZoomLevelSet.ZoomLevel20.Scale / 16));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(map.ZoomLevelSet.ZoomLevel20.Scale / 32));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(map.ZoomLevelSet.ZoomLevel20.Scale / 64));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(map.ZoomLevelSet.ZoomLevel20.Scale / 128));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(map.ZoomLevelSet.ZoomLevel20.Scale / 256));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(map.ZoomLevelSet.ZoomLevel20.Scale / 512));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(map.ZoomLevelSet.ZoomLevel20.Scale / 1024));
            return zoomLevelSet;
        }

        public ZoomLevelSet GetTopoZoomLevelSet() {
            ZoomLevelSet zoomLevelSet = new ZoomLevelSet();
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(591657527.591555));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(295828763.795777));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(147914381.897889));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(73957190.948944));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(36978595.474472));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(18489297.737236));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(9244648.868618));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(4622324.434309));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(2311162.217155));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(1155581.108577));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(577790.554289));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(288895.277144));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(144447.638572));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(72223.819286));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(36111.909643));
            zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(18055.954822));
            //zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(9027.977411));
            //zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(4513.988705));
            //zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(2256.994353));
            //zoomLevelSet.CustomZoomLevels.Add(new ZoomLevel(1128.497176));
            return zoomLevelSet;
        }

        private IList<double> InitializeListGooglePlus40() {
            elevations = new List<double>();
            elevations.Add(591657550.5);
            elevations.Add(295828775.3);
            elevations.Add(147914387.6);
            elevations.Add(73957193.82);
            elevations.Add(36978596.91);
            elevations.Add(18489298.45);
            elevations.Add(9244649.227);
            elevations.Add(4622324.614);
            elevations.Add(2311162.307);
            elevations.Add(1155581.153);

            elevations.Add(577790.5767);
            elevations.Add(288895.2884);
            elevations.Add(144447.6442);

            elevations.Add(72223.82209);
            elevations.Add(54167.866541);
            elevations.Add(45139.8888);
            elevations.Add(40625.89992);

            elevations.Add(36111.91104);
            elevations.Add(33854.9166);
            elevations.Add(31597.92216);
            elevations.Add(29340.92772);
            elevations.Add(27083.93328);
            elevations.Add(24826.93884);
            elevations.Add(22569.9444);
            elevations.Add(20312.94996);

            elevations.Add(18055.95552);
            elevations.Add(16927.458301);
            elevations.Add(15798.961081);
            elevations.Add(14670.463861);
            elevations.Add(13541.966641);
            elevations.Add(12413.469421);
            elevations.Add(11284.972201);
            elevations.Add(10156.474981);

            elevations.Add(9027.977761);
            elevations.Add(8463.72915);
            elevations.Add(7899.48054);
            elevations.Add(7335.23193);
            elevations.Add(6770.98332);
            elevations.Add(6206.73471);
            elevations.Add(5642.4861);
            elevations.Add(5078.23749);

            elevations.Add(4513.98888);
            elevations.Add(3949.74026);
            elevations.Add(3385.49165);
            elevations.Add(2821.24305);

            elevations.Add(2256.99444);
            elevations.Add(1692.74583);

            elevations.Add(1128.49722);
            elevations.Add(564.24861);
            elevations.Add(282.124305);
            elevations.Add(141.0621525);
            return elevations;
        }

        private IList<double> InitializeListGoogleStandard() {
            elevations = new List<double>();
            elevations.Add(591657550.5);
            elevations.Add(295828775.3);
            elevations.Add(147914387.6);
            elevations.Add(73957193.82);
            elevations.Add(36978596.91);
            elevations.Add(18489298.45);
            elevations.Add(9244649.227);
            elevations.Add(4622324.614);
            elevations.Add(2311162.307);
            elevations.Add(1155581.153);
            elevations.Add(577790.5767);
            elevations.Add(288895.2884);
            elevations.Add(144447.6442);
            elevations.Add(72223.82209);
            elevations.Add(36111.91104);
            elevations.Add(18055.95552);
            elevations.Add(9027.977761);
            elevations.Add(4513.98888);
            elevations.Add(2256.99444);
            elevations.Add(1128.49722);
            return elevations;
        }

        public int GetZoomLevelFromScale(double scale) {
            int newzoomlevel = 21;
            System.Collections.ObjectModel.Collection<ZoomLevel> googlelist = GoogleZoomLevels();
            for (int i = 0; i < googlelist.Count(); i++) {
                try {
                    ZoomLevel zoomlevel = googlelist[i];
                    if (zoomlevel.Scale >= scale) {
                        newzoomlevel = i;
                    }
                }
                catch (Exception ex) {
                    return newzoomlevel;
                }
            }
            return newzoomlevel;
        }

        public bool VerifyGoogleZoomLevelFromScale(double scale) {
            int newzoomlevel = 21;
            System.Collections.ObjectModel.Collection<ZoomLevel> googlelist = GoogleZoomLevels();
            for (int i = 0; i < googlelist.Count(); i++) {
                try {
                    ZoomLevel zoomlevel = googlelist[i];
                    if (zoomlevel.Scale == scale) {
                        return true;
                    }
                }
                catch (Exception ex) {
                    return false;
                }
            }
            return false;
        }

        public double SnapToGoogleZoomLevelScale(double scale) {
            double savedscale = double.MaxValue;
            System.Collections.ObjectModel.Collection<ZoomLevel> googlelist = GoogleZoomLevels();
            for (int i = 0; i < googlelist.Count(); i++) {
                try {
                    ZoomLevel zoomlevel = googlelist[i];
                    if (zoomlevel.Scale >= scale) {
                        savedscale = zoomlevel.Scale;
                    }
                    else {
                        return savedscale;
                    }
                }
                catch (Exception ex) {
                    return scale;
                }
            }
            return savedscale;
        }

        //public int GetScaleFromZoomLevel(int zoomlevel) {
        //    int newzoomlevel = 21;
        //    System.Collections.ObjectModel.Collection<ZoomLevel> googlelist = GoogleZoomLevels();
        //    double scale = new GoogleMapsZoomLevelSet().GetZoomLevel()
        //    for (int i = 0; i <= googlelist.Count(); i++) {
        //        try {
        //            ZoomLevel zoomlevel = googlelist[i];
        //            if (zoomlevel.Scale >= scale) {
        //                newzoomlevel = i;
        //            }
        //        }
        //        catch (Exception ex) {
        //            return newzoomlevel;
        //        }
        //    }
        //    return newzoomlevel;
        //}

        private static System.Collections.ObjectModel.Collection<ZoomLevel> GoogleZoomLevels() {
            //zoomlevellist = (zoomlevellist ?? new SphericalMercatorZoomLevelSet().GetZoomLevels());
            googlezoomlevellist = (googlezoomlevellist ?? new GoogleMapsZoomLevelSet().GetZoomLevels());
            return googlezoomlevellist;
        }

        //private static Dictionary<string, int> UtmZoneList() {
        //    if(utmzonelist != null && utmzonelist.Count > 0) {
        //        return utmzonelist;
        //    }
        //    else {
        //        utmzonelist = new Dictionary<string, int>();
        //        utmzonelist.Add("1N", 26901);
        //        utmzonelist.Add("2N", 26902);
        //        utmzonelist.Add("3N", 26903);
        //        utmzonelist.Add("4N", 26904);
        //        utmzonelist.Add("5N", 26905);
        //        utmzonelist.Add("6N", 26906);
        //        utmzonelist.Add("7N", 26907);
        //        utmzonelist.Add("8N", 26908);
        //        utmzonelist.Add("9N", 26909);
        //        utmzonelist.Add("10N", 26910);
        //        utmzonelist.Add("11N", 26911);
        //        utmzonelist.Add("12N", 26912);
        //        utmzonelist.Add("13N", 26913);
        //        utmzonelist.Add("14N", 26914);
        //        utmzonelist.Add("15N", 26915);
        //        utmzonelist.Add("16N", 26916);
        //        utmzonelist.Add("17N", 26917);
        //        utmzonelist.Add("18N", 26918);
        //        utmzonelist.Add("19N", 26919);
        //        utmzonelist.Add("20N", 26920);
        //        utmzonelist.Add("21N", 26921);
        //        utmzonelist.Add("22N", 26922);
        //        utmzonelist.Add("23N", 26923);
        //    }
        //    return utmzonelist;
        //}


        public UTMConstruction GetUTMZone(PointShape pointshape) {
            bool iscurrentutmzone = false;
            bool isinsidecurrentutmzone = false;
            string utmzone = string.Empty;
            int EPSG = 32600;

            int zone = (int)(Math.Floor((pointshape.X + 180.0) / 6) + 1);
            string hemisphere = string.Empty;
            if (pointshape.Y >= 0) {
                utmzone = zone.ToString("N0") + "N";
                hemisphere = "N";
            }
            else {
                utmzone = zone.ToString("N0") + "S";
                hemisphere = "S";
                EPSG = 32700;
            }
            EPSG += zone;
            UTMConstruction utmc = new UTMConstruction();
            utmc.worldpoint = pointshape;
            utmc.zone = zone;
            utmc.hemisphereString = hemisphere;
            utmc.utmzoneString = utmzone;
            utmc.epsgcode = EPSG;
            return utmc;
        }
    }

    

    public class UTMConstruction {
        public PointShape worldpoint;
        public int zone;
        public string hemisphereString;
        public string utmzoneString;
        public int epsgcode;
    }

}
