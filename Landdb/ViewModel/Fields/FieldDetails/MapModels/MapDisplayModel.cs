﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Input;
using AgC.UnitConversion;
using AgC.UnitConversion.Area;
using GalaSoft.MvvmLight;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Map;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.MapStyles;
using Microsoft.Win32;
using NLog;
using ThinkGeo.MapSuite.Core;
using ThinkGeo.MapSuite.WpfDesktopEdition;
using Landdb.ViewModel.Maps.ImageryOverlay;
using Landdb.ViewModel.Secondary.Map;
using Landdb.Domain.ReadModels.MapAnnotation;
using System.Windows.Threading;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Drawing;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure.Messages;
using System.Globalization;
using Landdb.Resources;

namespace Landdb.ViewModel.Fields.FieldDetails.MapModels {
    //--r=uri:landdb_demo --uri=https://demo.landdb.com/
    //--r=uri:landdb_test --uri=https://test.landdb.com/
    //--r=uri:landdb --uri=https://www.landdb.com/

    public class MapDisplayModel : ViewModelBase {
        IClientEndpoint clientEndpoint;
        MapViewModel parent;
        Logger log = LogManager.GetCurrentClassLogger();
        static bool checknullshapes = false;
        bool maploaded = false;
        bool allowupdates = true;
        bool draggingfeatures = false;
        bool removingvertex = false;
        bool addingvertex = false;
        bool trackingfeatures = false;
        bool startedOutOnline = false;
        bool repaintthescreen = false;
        bool calculatingdimensions = false;

        IList<MapItem> clientLayerItems;
        IMapOverlay selectedoverlay;
        object detailsViewModel;
        string selectedid = string.Empty;
        BaseShape importshape;
        BaseShape pivotshape;
        bool drawingpivot = false;
        bool ismeasureline = false;
        BaseShape replaceshape;
        PointShape deleteshapepointer;
        Feature selectedfeature;
        IIdentity selectedIdentity;
        Feature selectedfield;
        GeoCollection<Feature> selectedFeatures;
        long newticks = 0;
        long oldticks = 0;
        RectangleShape mapextent = new RectangleShape();
        private Dictionary<string, string> customLabels = new Dictionary<string, string>();
        Dictionary<string, string> shapevalues = new Dictionary<string, string>();
        Dictionary<string, string> labelvalues = new Dictionary<string, string>();
        List<AnnotationStyleItem> changedstyles = new List<AnnotationStyleItem>();
        Dictionary<string, MapAnnotationCategory> userstylenames = new Dictionary<string, MapAnnotationCategory>();

        ScalingTextStyle scalingTextStyle = new ScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, 24, 2);
        bool displayareainfieldlabel = false;
        double fittingpolygonfactor = 2;
        bool allowlabeloverlapping = true;
        bool fittingpolygon = true;
        bool displayUnselectedFields = false;
        bool labelallpolygonparts = false;
        bool displayLabels = false;
        private Dictionary<string, double> rotationAngles = new Dictionary<string, double>();
        private Dictionary<string, PointShape> labelPositions = new Dictionary<string, PointShape>();

        Dispatcher dispatcher;

        // Layers
        InMemoryFeatureLayer fieldsLayer = new InMemoryFeatureLayer();
        InMemoryFeatureLayer currentLayer = new InMemoryFeatureLayer();
        InMemoryFeatureLayer cropzoneLayer = new InMemoryFeatureLayer();
        InMemoryFeatureLayer cropzoneLayerCZ = new InMemoryFeatureLayer();
        InMemoryFeatureLayer cropzoneLayerDraw = new InMemoryFeatureLayer();
        InMemoryFeatureLayer measureLayer = new InMemoryFeatureLayer();
        InMemoryFeatureLayer dimensionLayer = new InMemoryFeatureLayer();
        InMemoryFeatureLayer annotationLayer = new InMemoryFeatureLayer();
        InMemoryFeatureLayer importLayer = new InMemoryFeatureLayer();
        LayerOverlay layerOverlay = new LayerOverlay();
        PopupOverlay popupOverlay = new PopupOverlay();
        LayerOverlay dimensionOverlay = new LayerOverlay();
        MultipolygonShape savededitoverlaypoly = new MultipolygonShape();
        Proj4Projection projection;
        ClientLayerAreaStyle fieldsLayerAreaStyle = null;
        ClientLayerAreaStyle cropzonesLayerAreaStyle = null;
        Cursor defaultcursor = Cursors.Arrow;
        ScalingTextStyle scalingTextStyleCZ = new ScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, 24, 2);
        AnnotationScalingTextStyle scalingTextStyle2 = new AnnotationScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, 24, 2);
        MapStyles.CustomDistanceLineStyle dimensionDistanceLineStyleText = new MapStyles.CustomDistanceLineStyle(DistanceUnit.Feet, new GeoFont("Arial", 12, DrawingFontStyles.Regular), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.DarkRed, 2), new GeoPen(GeoColor.StandardColors.DarkBlue, 2));
        MapStyles.CustomDistanceLineStyle customDistanceLineStyleText = new MapStyles.CustomDistanceLineStyle(DistanceUnit.Feet, new GeoFont("Arial", 12, DrawingFontStyles.Regular), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.DarkRed, 2), new GeoPen(GeoColor.StandardColors.DarkBlue, 2));
        //MapStyles.CustomDistancePolygonStyle customDistancePolygonStyleText = new MapStyles.CustomDistancePolygonStyle(ThinkGeo.MapSuite.Core.AreaUnit.Acres, ThinkGeo.MapSuite.Core.DistanceUnit.Feet, new GeoFont("Arial", 16, DrawingFontStyles.Regular), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.White), new GeoSolidBrush(GeoColor.StandardColors.Red), new GeoPen(GeoColor.StandardColors.Red));
        MapStyles.CustomAreaPolygonStyle customDistancePolygonStyleText = new MapStyles.CustomAreaPolygonStyle(ThinkGeo.MapSuite.Core.AreaUnit.Acres, new GeoFont("Arial", 16, DrawingFontStyles.Regular), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.White), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.DarkRed));
    
        LineStyle lineStylePen = new LineStyle(new GeoPen(GeoColor.FromArgb(150, GeoColor.StandardColors.Orange), 4));
        MapStyles.AnnotationMultiStyle customMultiStyles = new MapStyles.AnnotationMultiStyle();
        string areaUnitString = string.Empty;
        AgC.UnitConversion.Area.AreaUnit agcAreaUnit;
        ThinkGeo.MapSuite.Core.AreaUnit mapAreaUnit;
        ThinkGeo.MapSuite.Core.DistanceUnit mapDistanceUnit;
        bool bingmapvisible = true;
        int fieldopacity = 125;
        int fieldopacityfill = 125;
        int annotationopacity = 125;
        int areadecimals = 2;
        int bordersize = 2;
        bool annotationvisibility = false;
        bool displayareaonlylabel = false;
        bool mapannotationlabelsvisible = false;
        bool displayFillColor = false;
        bool displaymapzoombar = false;
        MapSettings mapsettings;
        IStyleFactory styleFactory;
        string fieldsselectedfillcolor;
        string fieldsunselectedfillcolor;
        string fieldsselectedbordercolor;
        string fieldsunselectedbordercolor;
        string coordinateformat = string.Empty;
        GeoColor cropzoneGeoColor = GeoColor.StandardColors.Red;
        GeoColor fieldsselectedGeoColor = GeoColor.StandardColors.Yellow;
        GeoColor fieldsunselectedGeoColor = GeoColor.StandardColors.Gray;
        GeoColor fieldsselectedborderGeoColor = new GeoColor();
        GeoColor fieldsunselectedborderGeoColor = new GeoColor();
        string updateddisplayarea = string.Empty;
        ZoomLevelFactory zoomlevelfactory = new ZoomLevelFactory();
        bool allowzropzoneoutlines = false;
        bool notcenteredyet = false;
        private Projection projectionFromSphericalMercator;
        private Projection projectionFromWGS84;
        private Projection projectionFromSM;

        public MapDisplayModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, MapViewModel mapViewModel) {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            this.parent = mapViewModel;
            this.mapsettings = clientEndpoint.GetMapSettings();

            if (!checknullshapes && ApplicationEnvironment.SearchFieldHistory)
            {
                checknullshapes = ApplicationEnvironment.SearchFieldHistory;
            }

            //System.Drawing.Text.InstalledFontCollection fonts = new System.Drawing.Text.InstalledFontCollection();
            ////FontFamily[] fontFamilies = fonts.Families;
            //Collection<string> fontNames = new Collection<string>();
            //StringBuilder fontnamestring = new StringBuilder(30);
            //foreach (var item in fonts.Families) {
            //    fontNames.Add(item.Name);
            //    fontnamestring.Append(String.Format("{0}, ", item.Name));
            //}

            updateddisplayarea = string.Empty;
            bingmapvisible = mapsettings.BingMapVisible;
            fieldopacity = mapsettings.FieldsOpacity;
            fieldopacityfill = mapsettings.FieldsOpacity;
            annotationopacity = mapsettings.MapAnnotationOpacity;
            annotationvisibility = mapsettings.MapAnnotationsVisible;
            displayLabels = mapsettings.ScalingTextVisible;
            displayUnselectedFields = mapsettings.FieldsVisible;
            string zlevel = mapsettings.ZoomLevel;
            if (mapsettings.DisplayFillColor.HasValue) {
                displayFillColor = mapsettings.DisplayFillColor.Value;
                if (displayFillColor) {
                    fieldopacityfill = mapsettings.FieldsOpacity;
                }
                else {
                    fieldopacityfill = 0;
                }
            }
            if (mapsettings.DisplayAreaInFieldLabel.HasValue) {
                displayareainfieldlabel = mapsettings.DisplayAreaInFieldLabel.Value;
            }
            if (mapsettings.DisplayAreaOnlyLabel.HasValue) {
                displayareaonlylabel = mapsettings.DisplayAreaOnlyLabel.Value;
            }
            if (mapsettings.MapAnnotationLabelsVisible.HasValue) {
                mapannotationlabelsvisible = mapsettings.MapAnnotationLabelsVisible.Value;
            }
            displaymapzoombar = false;
            if (mapsettings.DisplayMapZoomBar.HasValue) {
                displaymapzoombar = mapsettings.DisplayMapZoomBar.Value;
            }

            styleFactory = new StyleFactory();
            changedstyles = new List<AnnotationStyleItem>();
            userstylenames = new Dictionary<string, MapAnnotationCategory>();

            bordersize = mapsettings.BorderSize;
            areadecimals = mapsettings.AreaDecimals;
            areaUnitString = mapsettings.MapAreaUnit;
            coordinateformat = mapsettings.DisplayCoordinateFormat;

            agcAreaUnit = UnitFactory.GetUnitByName(areaUnitString) as AgC.UnitConversion.Area.AreaUnit;
            mapAreaUnit = MapUnitFactory.AreaMeasureConversion(agcAreaUnit);
            mapDistanceUnit = MapUnitFactory.AreaDistanceMeasureConversion(agcAreaUnit);

            dimensionDistanceLineStyleText = new MapStyles.CustomDistanceLineStyle(mapDistanceUnit, new GeoFont("Arial", 12, DrawingFontStyles.Regular), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.DarkRed, 2), new GeoPen(GeoColor.StandardColors.DarkBlue, 2));
            dimensionDistanceLineStyleText.DisplayAngles = false;
            dimensionDistanceLineStyleText.DisplayTotalLength = false;
            customDistanceLineStyleText = new MapStyles.CustomDistanceLineStyle(mapDistanceUnit, new GeoFont("Arial", 12, DrawingFontStyles.Regular), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.DarkRed, 2), new GeoPen(GeoColor.StandardColors.DarkBlue, 2));
            //customDistancePolygonStyleText = new MapStyles.CustomDistancePolygonStyle(mapAreaUnit, mapDistanceUnit, new GeoFont("Arial", 16, DrawingFontStyles.Regular), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.White), new GeoSolidBrush(GeoColor.StandardColors.Red), new GeoPen(GeoColor.StandardColors.Red));
            customDistancePolygonStyleText = new MapStyles.CustomAreaPolygonStyle(mapAreaUnit, new GeoFont("Arial", 16, DrawingFontStyles.Regular), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.White), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.DarkRed));
            scalingTextStyle = new ScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, mapsettings.ScalingTextMaximumSize, this.mapsettings.ScalingTextMinimumSize);
            scalingTextStyleCZ = new ScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, mapsettings.ScalingTextMaximumSize, this.mapsettings.ScalingTextMinimumSize);
            if (displayareainfieldlabel) {
                if (displayareaonlylabel) {
                    scalingTextStyle.SetTexTColumnName(@"AreaOnlyLabel");
                    scalingTextStyleCZ.SetTexTColumnName(@"AreaOnlyLabel");
                }
                else {
                    scalingTextStyle.SetTexTColumnName(@"AreaLabel");
                    scalingTextStyleCZ.SetTexTColumnName(@"AreaLabel");
                }
            }
            else {
                scalingTextStyle.SetTexTColumnName(@"LdbLabel");
                scalingTextStyleCZ.SetTexTColumnName(@"LdbLabel");
            }
            //scalingTextStyle.SetTexTColumnName(@"AreaOnlyLabel");
            //scalingTextStyleCZ.SetTexTColumnName(@"AreaOnlyLabel");
            //customDistancePolygonStyleText.YOffsetInPixel = -20;
            fittingpolygonfactor = mapsettings.FittingPolygonFactor;
            if (mapsettings.AllowLabelOverlapping.HasValue) {
                allowlabeloverlapping = mapsettings.AllowLabelOverlapping.Value;
            }
            if (mapsettings.FittingPolygon.HasValue) {
                fittingpolygon = mapsettings.FittingPolygon.Value;
            }
            scalingTextStyle.UnselectedFieldVisibility = displayUnselectedFields;
            scalingTextStyle.LabelAllPolygonParts = labelallpolygonparts;
            scalingTextStyleCZ.LabelAllPolygonParts = labelallpolygonparts;
            scalingTextStyle.PolygonLabelingLocationMode = PolygonLabelingLocationMode.BoundingBoxCenter;
            scalingTextStyleCZ.PolygonLabelingLocationMode = PolygonLabelingLocationMode.BoundingBoxCenter;
            scalingTextStyle.FittingPolygon = fittingpolygon;
            scalingTextStyleCZ.FittingPolygon = fittingpolygon;
            scalingTextStyle.FittingPolygonFactor = fittingpolygonfactor;
            scalingTextStyleCZ.FittingPolygonFactor = fittingpolygonfactor;
            if (allowlabeloverlapping) {
                scalingTextStyle.OverlappingRule = LabelOverlappingRule.AllowOverlapping;
                scalingTextStyleCZ.OverlappingRule = LabelOverlappingRule.AllowOverlapping;
            }
            else {
                scalingTextStyle.OverlappingRule = LabelOverlappingRule.NoOverlapping;
                scalingTextStyleCZ.OverlappingRule = LabelOverlappingRule.NoOverlapping;
            }
            fieldsselectedfillcolor = mapsettings.FieldsSelectedFillColor;
            fieldsunselectedfillcolor = mapsettings.FieldsUnSelectedFillColor;
            if (!string.IsNullOrEmpty(fieldsselectedfillcolor)) {
                fieldsselectedGeoColor = GeoColor.FromHtml(fieldsselectedfillcolor);
            }
            if (!string.IsNullOrEmpty(fieldsunselectedfillcolor)) {
                fieldsunselectedGeoColor = GeoColor.FromHtml(fieldsunselectedfillcolor);
            }

            fieldsselectedbordercolor = mapsettings.FieldsSelectedBorderColor;
            fieldsunselectedbordercolor = mapsettings.FieldsUnSelectedBorderColor;
            if (!string.IsNullOrEmpty(fieldsselectedbordercolor)) {
                fieldsselectedborderGeoColor = GeoColor.FromHtml(fieldsselectedbordercolor);
            } else {
                fieldsselectedborderGeoColor = fieldsselectedGeoColor;
            }
            if (!string.IsNullOrEmpty(fieldsunselectedbordercolor)) {
                fieldsunselectedborderGeoColor = GeoColor.FromHtml(fieldsunselectedbordercolor);
            } else {
                fieldsunselectedborderGeoColor = fieldsunselectedGeoColor;
            }

            //selectedoverlay = parent.MAPOptionsViewModel.MapFactory(mapsettings.MapInfoSelectorItem);
            selectedoverlay = parent.MAPOptionsViewModel.MapFactory(mapsettings.MapInfoSelectorItem, mapsettings.AllowOfflineMap);

            repaintthescreen = false;
            if (maploaded) { return; }
            InitializeMap();
        }


        public bool SelectedOverlayMapStatusIsGoogle() {
            return selectedoverlay.MapInfoSelectorItem.IsGoogle();
        }

        public bool MapSettingsMapStatusIsGoogle() {
            return mapsettings.MapInfoSelectorItem.IsGoogle();
        }
        private bool SelectedOverlayMapStatusEquals(MapInfoSelectorItem mapInfoSelectorItem) {
            return selectedoverlay.MapInfoSelectorItem.Equals(mapInfoSelectorItem);
        }

        public bool MapSettingsMapStatusEquals(MapInfoSelectorItem mapInfoSelectorItem) {
            return mapsettings.MapInfoSelectorItem.Equals(mapInfoSelectorItem);
        }

        public WpfMap Map { get; set; }

        static object clientLayerLock = new object();
        public IList<MapItem> ClientLayer {
            get { return clientLayerItems; }
            set {
                lock (clientLayerLock) {
                    clientLayerItems = value;
                    updateddisplayarea = string.Empty;
                    EnsureFieldsLayerIsOpen();
                    fieldsLayer.InternalFeatures.Clear();

                    foreach (var m in clientLayerItems) {
                        string stringid = m.FieldId.ToString(); ;
                        if (shapevalues.ContainsKey(stringid)) {
                            m.MapData = shapevalues[stringid];
                        }
                        if (m.MapData != null && !string.IsNullOrEmpty(m.MapData) && m.MapData != "MULTIPOLYGON EMPTY") {
                            try {
                                var labels = clientEndpoint.GetView<ItemMap>(new FieldId(ApplicationEnvironment.CurrentDataSourceId, m.FieldId));
                                if (labels.HasValue && labels.Value.MostRecentLabelItem != null) {
                                    ItemLabelItem mrli = labels.Value.MostRecentLabelItem;
                                    if (mrli != null && !mrli.OptionalVisibility) {
                                        continue;
                                    }
                                }

                                //string customarealabel = GetCustomFieldName(m.FieldId, m.Name);
                                string customareaonlylabel = GetCustomAreaLabel(m.FieldId, true);
                                string customarealabel = DisplayLabelWithArea(m.Name, customareaonlylabel);
                                IDictionary<string, string> columnvalues = new Dictionary<string, string>();
                                columnvalues.Add(@"LdbLabel", m.Name);
                                columnvalues.Add(@"AreaLabel", customarealabel);
                                columnvalues.Add(@"AreaOnlyLabel", customareaonlylabel);
                                //MapItem
                                var testfeat = new Feature(m.MapData);
                                BaseShape testbase = testfeat.GetShape();
                                var f = new Feature(m.MapData, m.FieldId.ToString(), columnvalues);
                                if (testbase is PolygonShape) {
                                    MultipolygonShape testconvert = new MultipolygonShape();
                                    testconvert.Polygons.Add(testbase as PolygonShape);
                                    f = new Feature(testconvert.GetWellKnownText(), m.FieldId.ToString(), columnvalues);
                                }
                                if (fieldsLayer.InternalFeatures.Contains(f)) { continue; }
                                fieldsLayer.InternalFeatures.Add(m.FieldId.ToString(), f);

                                if (labels.HasValue && labels.Value.MostRecentLabelItem != null) {
                                    ItemLabelItem mrli = labels.Value.MostRecentLabelItem;
                                    if (mrli != null && !mrli.OptionalVisibility) {
                                        continue;
                                    }
                                    if (!string.IsNullOrWhiteSpace(mrli.SpatialLabelPosition)) {
                                        PointShape ps = new PointShape(mrli.SpatialLabelPosition);
                                        if (labelPositions.ContainsKey(m.FieldId.ToString())) {
                                            labelPositions[m.FieldId.ToString()] = ps;
                                        }
                                        else {
                                            labelPositions.Add(m.FieldId.ToString(), ps);
                                        }
                                    }
                                    if (rotationAngles.ContainsKey(m.FieldId.ToString())) {
                                        rotationAngles[m.FieldId.ToString()] = (double)mrli.HorizontalLabelRotation;
                                    }
                                    else {
                                        if (mrli.HorizontalLabelRotation != 0)
                                        {
                                            rotationAngles.Add(m.FieldId.ToString(), (double)mrli.HorizontalLabelRotation);
                                        }
                                    }
                                }
                            }
                            catch (Exception ex) {
                                log.ErrorException("Couldn't load the client layer map item.", ex);
                            }
                        }
                    }

                    //var fieldlayerfeatures = fieldsLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);
                    //if (fieldlayerfeatures.Count > 0) {
                    //    RectangleShape fieldlayerextent = ExtentHelper.GetBoundingBoxOfItems(fieldlayerfeatures);
                    //    Map.ZoomLevelSet = zoomlevelfactory.SetZoomLevels(Map, fieldlayerextent);
                    //}


                    scalingTextStyle.RotationAngles = rotationAngles;
                    scalingTextStyle.LabelPositions = labelPositions;
                    scalingTextStyle.MapScale = Map.CurrentScale;
                    scalingTextStyleCZ.RotationAngles = rotationAngles;
                    scalingTextStyleCZ.LabelPositions = labelPositions;
                    scalingTextStyleCZ.MapScale = Map.CurrentScale;
                    scalingTextStyle2.MapScale = Map.CurrentScale;
                    customMultiStyles.MapScale = Map.CurrentScale;
                    if (Map.Overlays.Contains("fieldOverlay")) {
                        Map.Overlays["fieldOverlay"].Refresh();
                    }

                    RaisePropertyChanged("ClientLayer");
                }
            }
        }

        public IList<MapItem> FieldCropzonelist { get; set; }

        public IList<MapItem> SelectedFieldlist { get; set; }

        public IMapOverlay SelectedOverlay {
            get { return selectedoverlay; }
            set {
                if (selectedoverlay == value) { return; }
                if (selectedoverlay == null) { return; }
                if (string.IsNullOrWhiteSpace(value.Name)) { return; }
                selectedoverlay = value;
                RaisePropertyChanged("SelectedOverlay");
            }
        }

        public object DetailsViewModel {
            get { return detailsViewModel; }
            set {
                detailsViewModel = value;
                LoadMapForItem(value, dispatcher);
                RaisePropertyChanged("DetailsViewModel");
            }
        }

        public BaseShape ReplaceShape {
            get { return replaceshape; }
            set {
                replaceshape = value;
                //SaveReplaceShape();
                RaisePropertyChanged("ReplaceShape");
            }
        }

        public BaseShape ImportShape {
            get { return importshape; }
            set {
                importshape = value;
                SaveImportShape(importshape, false);
                RaisePropertyChanged("ImportShape");
            }
        }

        public BaseShape PivotShape {
            get { return pivotshape; }
            set {
                pivotshape = value;
                SavePivotShape();
                RaisePropertyChanged("PivotShape");
            }
        }

        public bool DrawingPivot {
            get { return drawingpivot; }
            set {
                drawingpivot = value;
                RaisePropertyChanged("DrawingPivot");
            }
        }

        public bool IsMeasureLine {
            get { return ismeasureline; }
            set {
                ismeasureline = value;
                RaisePropertyChanged("IsMeasureLine");
            }
        }

        public PointShape DeleteShapePointer {
            get { return deleteshapepointer; }
            set {
                deleteshapepointer = value;
                DeleteShape();
                RaisePropertyChanged("DeleteShapePointer");
            }
        }

        public IIdentity SelectedIdentity {
            get { return selectedIdentity; }
            set {
                selectedIdentity = value;
                RaisePropertyChanged("SelectedIdentity");
            }
        }

        public Feature SelectedField {
            get { return selectedfield; }
            set {
                selectedfield = value;
                RaisePropertyChanged("SelectedField");
            }
        }

        public Feature SelectedFeature {
            get { return selectedfeature; }
            set {
                selectedfeature = value;
                RaisePropertyChanged("SelectedFeature");
            }
        }

        public GeoCollection<Feature> SelectedFeatures {
            get { return selectedFeatures; }
            set {
                selectedFeatures = value;
                RaisePropertyChanged("SelectedFeatures");
            }
        }

        public Dictionary<string, double> RotationAngles {
            get { return rotationAngles; }
            set {
                if (rotationAngles == value) { return; }
                rotationAngles = value;
                scalingTextStyle.RotationAngles = rotationAngles;
                scalingTextStyleCZ.RotationAngles = rotationAngles;
            }
        }

        public Dictionary<string, PointShape> LabelPositions {
            get { return labelPositions; }
            set {
                if (labelPositions == value) { return; }
                labelPositions = value;
                scalingTextStyle.LabelPositions = labelPositions;
                scalingTextStyleCZ.LabelPositions = labelPositions;
            }
        }

        public string UpdatedDisplayArea
        {
            get { return updateddisplayarea; }
            set
            {
                if (updateddisplayarea == value) { return; }
                updateddisplayarea = value;
                RaisePropertyChanged("UpdatedDisplayArea");
            }
        }

        public RectangleShape MapExtent {
            get { return mapextent; }
            set { mapextent = value; }
        }

        public Bitmap MapImage { get; set; }

        public Collection<Feature> GetSpatialQueryResults() {
            try {
                Collection<Feature> spatialQueryResults = fieldsLayer.QueryTools.GetFeaturesIntersecting(mapextent, ReturningColumnsType.AllColumns);
                for (int i = spatialQueryResults.Count - 1; i >= 0; i--) {
                    Feature feat1 = spatialQueryResults[i];
                    Feature feat2 = currentLayer.InternalFeatures[0];
                    if(feat1.ColumnValues["AreaLabel"] == feat2.ColumnValues["AreaLabel"]) {
                        spatialQueryResults.RemoveAt(i);
                    }
                }
                return spatialQueryResults;
            }
            catch (Exception ex) {
                log.ErrorException("Caught an exception while getting spatial query results", ex);
                //System.Windows.MessageBox.Show("ImportTools - Caught an exception while working with a simple draw shape.");
            }
            return new Collection<Feature>();
        }

        public Collection<Feature> GetCropzoneResults() {
            try {
                Collection<Feature> spatialQueryResults = cropzoneLayerCZ.QueryTools.GetFeaturesIntersecting(mapextent, ReturningColumnsType.AllColumns);
                return spatialQueryResults;
            }
            catch (Exception ex) {
                log.ErrorException("Caught an exception while getting cropzone results", ex);
                //System.Windows.MessageBox.Show("ImportTools - Caught an exception while working with a simple draw shape.");
            }
            return new Collection<Feature>();
        }

        public void SaveMapImage()
        {
            //MapContainer Create Image and Save
            var width = Map.ActualWidth;
            var height = Map.ActualHeight;
            RenderTargetBitmap bmp = new RenderTargetBitmap((int)width, (int)height, 96, 96, PixelFormats.Pbgra32);
            bmp.Render(Map);
            PngBitmapEncoder png = new PngBitmapEncoder();
            png.Frames.Add(BitmapFrame.Create(bmp));

            try
            {
                //open up file dialog to save file....
                //then call createexcelfile to create the excel...
                string filename = string.Empty;
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();

                saveFileDialog1.Filter = "JPEG|*.jpg";
                saveFileDialog1.FilterIndex = 2;
                saveFileDialog1.RestoreDirectory = true;

                Nullable<bool> resultSaved = saveFileDialog1.ShowDialog();

                // Process save file dialog box results 
                if (resultSaved == true)
                {
                    // Save document 
                    filename = saveFileDialog1.FileName;

                    using (Stream stm = File.Create(filename))
                    {
                        png.Save(stm);
                    }
                }

                //now open file....
                System.Diagnostics.Process.Start(filename);
            }
            catch (Exception ex)
            {
                return;
            }
        }

        public void MapPan(PanDirection direction) {
            //Map.Pan(direction, 5);
            RectangleShape rs = ExtentHelper.Pan(Map.CurrentExtent, direction, 20);
            Map.CurrentExtent = rs;
            Map.Refresh();
        }

        public void PrintMapView(string headerName) {
            var width = Map.ActualWidth;
            var height = Map.ActualHeight;
            RenderTargetBitmap bmp = new RenderTargetBitmap((int)width, (int)height, 96, 96, PixelFormats.Pbgra32);
            bmp.Render(Map);
            PngBitmapEncoder png = new PngBitmapEncoder();
            png.Frames.Add(BitmapFrame.Create(bmp));

            try {

                MemoryStream fs = new MemoryStream();
                png.Save(fs);
                MapImage = new Bitmap(fs);
            }
            catch (Exception ex) {
                return;
            }

            var czs = from c in ClientLayer
                      where c.CropZoneId != null
                      select new Guid(c.CropZoneId.ToString());

            //Now generate Report
            var vm = new Secondary.Reports.Map.PrintMapViewModel(clientEndpoint, dispatcher, MapImage, czs.ToList(), headerName, selectedIdentity);
            var sd = new ScreenDescriptor("Landdb.Views.Secondary.Reports.Work_Order.SingleWorkOrderView", "viewReport", vm);
            Messenger.Default.Send(new ShowOverlayMessage() { ScreenDescriptor = sd });
        }

        public void ZoomToFieldsExtent() {
            try {
                var fieldlayerfeatures = fieldsLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);
                if (fieldlayerfeatures.Count > 0) {
                    notcenteredyet = true;
                    RectangleShape fieldlayerextent = ExtentHelper.GetBoundingBoxOfItems(fieldlayerfeatures);
                    Map.CurrentExtent = fieldlayerextent;
                    notcenteredyet = false;
                    PointShape center = fieldlayerextent.GetCenterPoint();
                    Map.CenterAt(center);
                }
            }
            catch (Exception ex) {
                log.ErrorException("Caught an exception while zoom to field extent", ex);
                //System.Windows.MessageBox.Show("ImportTools - Caught an exception while working with a simple draw shape.");
            }

        }

        public void FindLatLonLocation(PointShape LatLonPoint)
        {
            try
            {
                var fieldlayerfeatures = fieldsLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);
                if (fieldlayerfeatures.Count > 0)
                {
                    PointShape center = projection.ConvertToExternalProjection(LatLonPoint) as PointShape;
                    Map.CenterAt(center);
                }
            }
            catch (Exception ex)
            {
                log.ErrorException("Caught an exception while zoom to lat lon", ex);
                //System.Windows.MessageBox.Show("ImportTools - Caught an exception while working with a simple draw shape.");
            }

        }

        public void ZoomToSelectedExtent() {
            try {
                var fieldlayerfeatures = cropzoneLayerCZ.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);
                if (!cropzoneLayerCZ.IsVisible) {
                    fieldlayerfeatures = currentLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);
                }
                if (fieldlayerfeatures.Count > 0) {
                    notcenteredyet = true;
                    RectangleShape fieldlayerextent = ExtentHelper.GetBoundingBoxOfItems(fieldlayerfeatures);
                    Map.CurrentExtent = fieldlayerextent;
                    notcenteredyet = false;
                    PointShape center = fieldlayerextent.GetCenterPoint();
                    Map.CenterAt(center);
                }
            }
            catch (Exception ex) {
                log.ErrorException("Caught an exception while zoom to selected extent", ex);
                //System.Windows.MessageBox.Show("ImportTools - Caught an exception while working with a simple draw shape.");
            }

        }

        public void SnapToGoogleImagery() {
            if (SelectedOverlay != null && SelectedOverlayMapStatusIsGoogle()) {
                double googlescale = zoomlevelfactory.SnapToGoogleZoomLevelScale(Map.CurrentScale);
                Map.ZoomToScale(googlescale);
            }
        }
        public void RefreshFieldsLayer() {
            scalingTextStyle.RotationAngles = RotationAngles;
            scalingTextStyle.LabelPositions = LabelPositions;
            scalingTextStyle.MapScale = Map.CurrentScale;
            scalingTextStyleCZ.RotationAngles = RotationAngles;
            scalingTextStyleCZ.LabelPositions = LabelPositions;
            scalingTextStyleCZ.MapScale = Map.CurrentScale;
            scalingTextStyle2.MapScale = Map.CurrentScale;
            customMultiStyles.MapScale = Map.CurrentScale;
            Map.Refresh();
            repaintthescreen = true;
        }

        public void QuickMapRefresh() {
            Map.Refresh();
        }

        public void RefreshImageryLayer(IMapOverlay selOverlay) {
            if (selOverlay == null) {
                mapsettings = clientEndpoint.GetMapSettings();
                //var a = mapsettings.BingMapVisible;
                //var d = mapsettings.DisplayCoordinateFormat;
                //var f = mapsettings.AreaDecimals;
                //var h = mapsettings.FieldsVisible;
                //var i = mapsettings.UnselectedOpacity;
                //var j = mapsettings.UnselectedVisible;
                //var k = mapsettings.MapAnnotationsVisible;
                //var kk = mapsettings.CMGMapAnnotationsVisible;
                //var l = mapsettings.ScalingTextVisible;
                //var o = mapsettings.FieldsSelectedFillColor;
                //var p = mapsettings.FieldsUnSelectedFillColor;

                //DisplayAreaInFieldLabel = false;
                //FieldsOpacity = 100;
                //MapAnnotationOpacity = 100;
                //MapAnnotationLabelsVisible = false;

                bingmapvisible = mapsettings.BingMapVisible;
                fieldopacity = mapsettings.FieldsOpacity;
                fieldopacityfill = mapsettings.FieldsOpacity;
                annotationopacity = mapsettings.MapAnnotationOpacity;
                annotationvisibility = mapsettings.MapAnnotationsVisible;
                displayLabels = mapsettings.ScalingTextVisible;
                displayUnselectedFields = mapsettings.FieldsVisible;
                if (mapsettings.DisplayFillColor.HasValue) {
                    displayFillColor = mapsettings.DisplayFillColor.Value;
                    if (displayFillColor) {
                        fieldopacityfill = mapsettings.FieldsOpacity;
                    }
                    else {
                        fieldopacityfill = 0;
                    }
                }
                if (mapsettings.DisplayAreaInFieldLabel.HasValue) {
                    displayareainfieldlabel = mapsettings.DisplayAreaInFieldLabel.Value;
                }
                if (mapsettings.DisplayAreaOnlyLabel.HasValue) {
                    displayareaonlylabel = mapsettings.DisplayAreaOnlyLabel.Value;
                }
                if (mapsettings.MapAnnotationLabelsVisible.HasValue) {
                    mapannotationlabelsvisible = mapsettings.MapAnnotationLabelsVisible.Value;
                }
                displaymapzoombar = false;
                if (mapsettings.DisplayMapZoomBar.HasValue) {
                    displaymapzoombar = mapsettings.DisplayMapZoomBar.Value;
                }
                if (!displaymapzoombar) {
                    Map.MapTools.PanZoomBar.Visibility = System.Windows.Visibility.Collapsed;
                }

                //StyleData.Self.UserStyleNames = this.mapsettings.MapAnnotationCategories;
                bordersize = mapsettings.BorderSize;
                areadecimals = mapsettings.AreaDecimals;
                areaUnitString = mapsettings.MapAreaUnit;
                coordinateformat = mapsettings.DisplayCoordinateFormat;
                if (coordinateformat == "Decimal Degree") {
                    Map.MapTools.MouseCoordinate.MouseCoordinateType = MouseCoordinateType.DegreesMinutesSeconds;
                }
                else {
                    Map.MapTools.MouseCoordinate.MouseCoordinateType = MouseCoordinateType.LatitudeLongitude;
                }
                //zoomlevelfactory = new ZoomLevelFactory(mapsettings.ZoomLevel);
                //Map.ZoomLevelSet = zoomlevelfactory.ZoomLevelSET;

                agcAreaUnit = UnitFactory.GetUnitByName(areaUnitString) as AgC.UnitConversion.Area.AreaUnit;
                mapAreaUnit = MapUnitFactory.AreaMeasureConversion(agcAreaUnit);
                mapDistanceUnit = MapUnitFactory.AreaDistanceMeasureConversion(agcAreaUnit);
                //SelectedOverlay = parent.MAPOptionsViewModel.MapFactory(mapsettings.MapInfoSelectorItem);
                SelectedOverlay = parent.MAPOptionsViewModel.MapFactory(mapsettings.MapInfoSelectorItem, mapsettings.AllowOfflineMap);
                fieldsselectedfillcolor = mapsettings.FieldsSelectedFillColor;
                fieldsunselectedfillcolor = mapsettings.FieldsUnSelectedFillColor;
                if (!string.IsNullOrEmpty(fieldsselectedfillcolor)) {
                    fieldsselectedGeoColor = GeoColor.FromHtml(fieldsselectedfillcolor);
                }
                if (!string.IsNullOrEmpty(fieldsunselectedfillcolor)) {
                    fieldsunselectedGeoColor = GeoColor.FromHtml(fieldsunselectedfillcolor);
                }

                fieldsselectedbordercolor = mapsettings.FieldsSelectedBorderColor;
                fieldsunselectedbordercolor = mapsettings.FieldsUnSelectedBorderColor;
                if (!string.IsNullOrEmpty(fieldsselectedbordercolor)) {
                    fieldsselectedborderGeoColor = GeoColor.FromHtml(fieldsselectedbordercolor);
                }
                else {
                    fieldsselectedborderGeoColor = fieldsselectedGeoColor;
                }
                if (!string.IsNullOrEmpty(fieldsunselectedbordercolor)) {
                    fieldsunselectedborderGeoColor = GeoColor.FromHtml(fieldsunselectedbordercolor);
                }
                else {
                    fieldsunselectedborderGeoColor = fieldsunselectedGeoColor;
                }

                scalingTextStyle = new ScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, mapsettings.ScalingTextMaximumSize, this.mapsettings.ScalingTextMinimumSize);
                scalingTextStyleCZ = new ScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, mapsettings.ScalingTextMaximumSize, this.mapsettings.ScalingTextMinimumSize);
                if (displayareainfieldlabel) {
                    if (displayareaonlylabel) {
                        scalingTextStyle.SetTexTColumnName(@"AreaOnlyLabel");
                        scalingTextStyleCZ.SetTexTColumnName(@"AreaOnlyLabel");
                    }
                    else {
                        scalingTextStyle.SetTexTColumnName(@"AreaLabel");
                        scalingTextStyleCZ.SetTexTColumnName(@"AreaLabel");
                    }
                }
                else {
                    scalingTextStyle.SetTexTColumnName(@"LdbLabel");
                    scalingTextStyleCZ.SetTexTColumnName(@"LdbLabel");
                }
                //scalingTextStyle.SetTexTColumnName(@"AreaOnlyLabel");
                //scalingTextStyleCZ.SetTexTColumnName(@"AreaOnlyLabel");

                fittingpolygonfactor = mapsettings.FittingPolygonFactor;
                if (mapsettings.AllowLabelOverlapping.HasValue) {
                    allowlabeloverlapping = mapsettings.AllowLabelOverlapping.Value;
                }
                if (mapsettings.FittingPolygon.HasValue) {
                    fittingpolygon = mapsettings.FittingPolygon.Value;
                }
                scalingTextStyle.UnselectedFieldVisibility = displayUnselectedFields;
                scalingTextStyle.LabelAllPolygonParts = labelallpolygonparts;
                scalingTextStyleCZ.LabelAllPolygonParts = labelallpolygonparts;
                scalingTextStyle.PolygonLabelingLocationMode = PolygonLabelingLocationMode.BoundingBoxCenter;
                scalingTextStyleCZ.PolygonLabelingLocationMode = PolygonLabelingLocationMode.BoundingBoxCenter;
                scalingTextStyle.FittingPolygon = fittingpolygon;
                scalingTextStyleCZ.FittingPolygon = fittingpolygon;
                scalingTextStyle.FittingPolygonFactor = fittingpolygonfactor;
                scalingTextStyleCZ.FittingPolygonFactor = fittingpolygonfactor;
                if (allowlabeloverlapping) {
                    scalingTextStyle.OverlappingRule = LabelOverlappingRule.AllowOverlapping;
                    scalingTextStyleCZ.OverlappingRule = LabelOverlappingRule.AllowOverlapping;
                }
                else {
                    scalingTextStyle.OverlappingRule = LabelOverlappingRule.NoOverlapping;
                    scalingTextStyleCZ.OverlappingRule = LabelOverlappingRule.NoOverlapping;
                }
                fieldsLayerAreaStyle.SelectedAreaStyle = new AreaStyle(new GeoPen(GeoColor.StandardColors.Transparent, bordersize), new GeoSolidBrush(new GeoColor(0, GeoColor.StandardColors.Transparent)));
                fieldsLayerAreaStyle.UnselectedAreaStyle = new AreaStyle(new GeoPen(new GeoColor(fieldopacity, fieldsunselectedborderGeoColor), bordersize), new GeoSolidBrush(new GeoColor(fieldopacityfill, fieldsunselectedGeoColor)));
                fieldsLayerAreaStyle.UnselectedFieldVisibility = displayUnselectedFields;
                fieldsLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Clear();
                fieldsLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(fieldsLayerAreaStyle);
                if (displayLabels) {
                    fieldsLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(scalingTextStyle);
                }
                fieldsLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;

                cropzonesLayerAreaStyle.SelectedAreaStyle = new AreaStyle(new GeoPen(GeoColor.StandardColors.Transparent, bordersize), new GeoSolidBrush(new GeoColor(0, GeoColor.StandardColors.Transparent)));
                cropzonesLayerAreaStyle.UnselectedAreaStyle = new AreaStyle(new GeoPen(new GeoColor(fieldopacity, fieldsunselectedborderGeoColor), bordersize), new GeoSolidBrush(new GeoColor(fieldopacityfill, fieldsunselectedGeoColor)));
                //cropzonesLayerAreaStyle.UnselectedFieldVisibility = displayUnselectedFields;
                cropzoneLayerCZ.ZoomLevelSet.ZoomLevel01.CustomStyles.Clear();
                cropzoneLayerCZ.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(cropzonesLayerAreaStyle);
                if (displayLabels) {
                    cropzoneLayerCZ.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(scalingTextStyleCZ);
                }
                cropzoneLayerCZ.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;

                AreaStyle currentLayerStyle = AreaStyles.CreateSimpleAreaStyle(new GeoColor(fieldopacityfill, fieldsselectedGeoColor), new GeoColor(fieldopacity, fieldsselectedborderGeoColor), bordersize);
                currentLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = currentLayerStyle;
                currentLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;

                AreaStyle cropzoneLayerStyle = AreaStyles.CreateSimpleAreaStyle(new GeoColor(fieldopacityfill, fieldsselectedGeoColor), new GeoColor(fieldopacity, fieldsunselectedborderGeoColor), bordersize);
                cropzoneLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = cropzoneLayerStyle;
                cropzoneLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;

                AreaStyle cropzoneLayerStyleDraw = new AreaStyle(new GeoPen(GeoColor.StandardColors.Black, 1), new GeoSolidBrush(new GeoColor(0, GeoColor.StandardColors.Transparent)));
                cropzoneLayerDraw.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = cropzoneLayerStyleDraw;
                cropzoneLayerDraw.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;


                LoadBingMapsOverlay();
                RebuildMapAnnotations();
                IList<MapItem> mapshapes = clientLayerItems;
                ClientLayer = mapshapes;
                measureLayer.InternalFeatures.Clear();
                dimensionLayer.InternalFeatures.Clear();
                popupOverlay.Popups.Clear();
                //RefreshFieldsLayer();
                LoadMapForItem(detailsViewModel, dispatcher);
            }
            else {
                SelectedOverlay = selOverlay;
                LoadBingMapsOverlay();
                RebuildMapAnnotations();
                RefreshFieldsLayer();
                mapsettings = clientEndpoint.GetMapSettings();
                mapsettings.MapInfoSelectorItem = selectedoverlay.MapInfoSelectorItem;
                clientEndpoint.SaveMapSettings(mapsettings);
            }
        }
        public void SetCropzoneOutlines() {
            if (!allowzropzoneoutlines) {
                allowzropzoneoutlines = true;
                cropzoneLayerDraw.IsVisible = allowzropzoneoutlines;
                Map.Overlays["fieldOverlay"].Refresh();
            }
        }
        public void TurnOffCropzoneOutlines() {
            if (allowzropzoneoutlines) {
                allowzropzoneoutlines = false;
                cropzoneLayerDraw.IsVisible = allowzropzoneoutlines;
                Map.Overlays["fieldOverlay"].Refresh();
            }
        }

        void InitializeMap() {
            if (maploaded) { return; }
            Map = new WpfMap();
            startedOutOnline = Landdb.Client.Infrastructure.NetworkStatus.IsInternetAvailableFast();
            if (SelectedOverlayMapStatusIsGoogle()) {
                Map.MaxHeight = 2048;
                Map.MaxWidth = 2048;
                //Map.Height = 640;
                //Map.Width = 640;
            }
            Map.MapUnit = GeographyUnit.Meter;
            Map.MapTools.PanZoomBar.Visibility = System.Windows.Visibility.Visible;
            Map.MapTools.PanZoomBar.GlobeButtonVisibility = System.Windows.Visibility.Hidden;
            Map.MapTools.PanZoomBar.VerticalAlignment = System.Windows.VerticalAlignment.Center;
            Map.MapTools.PanZoomBar.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            displaymapzoombar = false;
            if (mapsettings.DisplayMapZoomBar.HasValue) {
                displaymapzoombar = mapsettings.DisplayMapZoomBar.Value;
            }
            if (!displaymapzoombar) {
                Map.MapTools.PanZoomBar.Visibility = System.Windows.Visibility.Collapsed;
            }

            Map.Background = System.Windows.Media.Brushes.White;
            //zoomlevelfactory = new ZoomLevelFactory(mapsettings.ZoomLevel);
            //Map.ZoomLevelSet = zoomlevelfactory.ZoomLevelSET;
            Map.MouseLeftButtonDown += (sender, e) => {
                ((MainWindow)System.Windows.Application.Current.MainWindow).Draggable = false;
            };
            Map.ExtentOverlay.DoubleLeftClickMode = MapDoubleLeftClickMode.Disabled;

            shapevalues = new Dictionary<string, string>();
            labelvalues = new Dictionary<string, string>();

            projection = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetGoogleMapParametersString());

            //WmsRasterLayer wmsImageLayer = new WmsRasterLayer(new Uri("http://www.geocommunicator.gov/wmsconnector/com.esri.wms.Esrimap?ServiceName=BLM_MAP_PLSS&"));
            
            scalingTextStyle = new ScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, mapsettings.ScalingTextMaximumSize, this.mapsettings.ScalingTextMinimumSize);
            scalingTextStyleCZ = new ScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, mapsettings.ScalingTextMaximumSize, this.mapsettings.ScalingTextMinimumSize);



            if (displayareainfieldlabel) {
                if (displayareaonlylabel) {
                    scalingTextStyle.SetTexTColumnName(@"AreaOnlyLabel");
                    scalingTextStyleCZ.SetTexTColumnName(@"AreaOnlyLabel");
                }
                else {
                    scalingTextStyle.SetTexTColumnName(@"AreaLabel");
                    scalingTextStyleCZ.SetTexTColumnName(@"AreaLabel");
                }
            }
            else {
                scalingTextStyle.SetTexTColumnName(@"LdbLabel");
                scalingTextStyleCZ.SetTexTColumnName(@"LdbLabel");
            }
            //scalingTextStyle.SetTexTColumnName(@"AreaOnlyLabel");
            //scalingTextStyleCZ.SetTexTColumnName(@"AreaOnlyLabel");
            fittingpolygonfactor = mapsettings.FittingPolygonFactor;
            if (mapsettings.AllowLabelOverlapping.HasValue) {
                allowlabeloverlapping = mapsettings.AllowLabelOverlapping.Value;
            }
            if (mapsettings.FittingPolygon.HasValue) {
                fittingpolygon = mapsettings.FittingPolygon.Value;
            }
            scalingTextStyle.UnselectedFieldVisibility = displayUnselectedFields;
            scalingTextStyle.LabelAllPolygonParts = labelallpolygonparts;
            scalingTextStyleCZ.LabelAllPolygonParts = labelallpolygonparts;
            scalingTextStyle.PolygonLabelingLocationMode = PolygonLabelingLocationMode.BoundingBoxCenter;
            scalingTextStyleCZ.PolygonLabelingLocationMode = PolygonLabelingLocationMode.BoundingBoxCenter;
            scalingTextStyle.FittingPolygon = fittingpolygon;
            scalingTextStyleCZ.FittingPolygon = fittingpolygon;
            scalingTextStyle.FittingPolygonFactor = fittingpolygonfactor;
            scalingTextStyleCZ.FittingPolygonFactor = fittingpolygonfactor;
            if (allowlabeloverlapping) {
                scalingTextStyle.OverlappingRule = LabelOverlappingRule.AllowOverlapping;
                scalingTextStyleCZ.OverlappingRule = LabelOverlappingRule.AllowOverlapping;
            }
            else {
                scalingTextStyle.OverlappingRule = LabelOverlappingRule.NoOverlapping;
                scalingTextStyleCZ.OverlappingRule = LabelOverlappingRule.NoOverlapping;
            }
            scalingTextStyle.RotationAngles = new Dictionary<string, double>();
            scalingTextStyle.LabelPositions = new Dictionary<string, PointShape>();
            scalingTextStyle.CustomLabels = new Dictionary<string, string>();
            scalingTextStyleCZ.RotationAngles = new Dictionary<string, double>();
            scalingTextStyleCZ.LabelPositions = new Dictionary<string, PointShape>();
            scalingTextStyleCZ.CustomLabels = new Dictionary<string, string>();
            //fieldsLayer
            fieldsLayer.FeatureSource.Open();
            FeatureSourceColumn column = new FeatureSourceColumn(@"LdbLabel", DbfColumnType.Character.ToString(), 64);
            FeatureSourceColumn column1 = new FeatureSourceColumn(@"AreaLabel", DbfColumnType.Character.ToString(), 64);
            FeatureSourceColumn column2 = new FeatureSourceColumn(@"AreaOnlyLabel", DbfColumnType.Character.ToString(), 64);
            fieldsLayer.Columns.Add(column);
            fieldsLayer.Columns.Add(column1);
            fieldsLayer.Columns.Add(column2);
            fieldsLayer.FeatureSource.Close();
            fieldsLayerAreaStyle = new ClientLayerAreaStyle();
            fieldsLayerAreaStyle.SelectedAreaStyle = new AreaStyle(new GeoPen(GeoColor.StandardColors.Transparent, bordersize), new GeoSolidBrush(new GeoColor(0, GeoColor.StandardColors.Transparent)));
            fieldsLayerAreaStyle.UnselectedAreaStyle = new AreaStyle(new GeoPen(new GeoColor(fieldopacity, fieldsunselectedborderGeoColor), bordersize), new GeoSolidBrush(new GeoColor(fieldopacityfill, fieldsunselectedGeoColor)));
            fieldsLayerAreaStyle.UnselectedFieldVisibility = displayUnselectedFields;
            fieldsLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(fieldsLayerAreaStyle);
            if (displayLabels) {
                fieldsLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(scalingTextStyle);
            }
            fieldsLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            fieldsLayer.FeatureSource.Projection = projection;
            fieldsLayer.IsVisible = true;
            scalingTextStyle.IsActive = true;
            fieldsLayerAreaStyle.SelectedFeatures = new List<Feature>();
            fieldsLayerAreaStyle.HiddenFeatures = new List<Feature>();
            scalingTextStyle.SelectedFeatures = new List<Feature>();
            //currentLayer
            currentLayer.FeatureSource.Open();
            currentLayer.Columns.Add(column);
            currentLayer.Columns.Add(column1);
            currentLayer.FeatureSource.Close();
            AreaStyle currentLayerStyle = AreaStyles.CreateSimpleAreaStyle(new GeoColor(fieldopacityfill, fieldsselectedGeoColor), new GeoColor(fieldopacity, fieldsselectedborderGeoColor), bordersize);
            currentLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = currentLayerStyle;
            currentLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            currentLayer.FeatureSource.Projection = projection;
            currentLayer.IsVisible = true;

            //cropzoneLayer
            cropzoneLayer.FeatureSource.Open();
            cropzoneLayer.Columns.Add(column);
            cropzoneLayer.Columns.Add(column1);
            cropzoneLayer.FeatureSource.Close();
            AreaStyle cropzoneLayerStyle = AreaStyles.CreateSimpleAreaStyle(new GeoColor(fieldopacityfill, fieldsselectedGeoColor), new GeoColor(fieldopacity, fieldsunselectedborderGeoColor), bordersize);
            cropzoneLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = cropzoneLayerStyle;
            cropzoneLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            cropzoneLayer.FeatureSource.Projection = projection;
            cropzoneLayer.IsVisible = false;

            //cropzoneLayerCZ
            cropzoneLayerCZ.FeatureSource.Open();
            cropzoneLayerCZ.Columns.Add(column);
            cropzoneLayerCZ.Columns.Add(column1);
            cropzoneLayerCZ.FeatureSource.Close();
            cropzonesLayerAreaStyle = new ClientLayerAreaStyle();
            cropzonesLayerAreaStyle.SelectedAreaStyle = new AreaStyle(new GeoPen(GeoColor.StandardColors.Transparent, bordersize), new GeoSolidBrush(GeoColor.StandardColors.Transparent));
            cropzonesLayerAreaStyle.UnselectedAreaStyle = new AreaStyle(new GeoPen(new GeoColor(fieldopacity, fieldsunselectedborderGeoColor), bordersize), new GeoSolidBrush(new GeoColor(fieldopacityfill, fieldsunselectedGeoColor)));
            //cropzonesLayerAreaStyle.UnselectedFieldVisibility = displayUnselectedFields;
            cropzoneLayerCZ.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(cropzonesLayerAreaStyle);
            if (displayLabels) {
                cropzoneLayerCZ.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(scalingTextStyleCZ);
            }
            cropzoneLayerCZ.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            cropzoneLayerCZ.FeatureSource.Projection = projection;
            cropzoneLayerCZ.IsVisible = false;
            cropzonesLayerAreaStyle.SelectedFeatures = new List<Feature>();
            cropzonesLayerAreaStyle.HiddenFeatures = new List<Feature>();

            //cropzoneLayerDraw
            AreaStyle cropzoneLayerStyleDraw = new AreaStyle(new GeoPen(GeoColor.StandardColors.Black, 1), new GeoSolidBrush(new GeoColor(0, GeoColor.StandardColors.Transparent)));
            cropzoneLayerDraw.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = cropzoneLayerStyleDraw;
            cropzoneLayerDraw.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            cropzoneLayerDraw.FeatureSource.Projection = projection;
            cropzoneLayerDraw.IsVisible = allowzropzoneoutlines;

            //measureLayer
            AreaStyle measureLayerStyle = AreaStyles.CreateSimpleAreaStyle(new GeoColor(50, GeoColor.StandardColors.Orange), new GeoColor(200, GeoColor.StandardColors.Orange), 2);
            measureLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(measureLayerStyle);
            measureLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(lineStylePen);
            measureLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            measureLayer.FeatureSource.Projection = projection;

            //dimensionLayer
            dimensionLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customDistancePolygonStyleText);
            dimensionLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(dimensionDistanceLineStyleText);
            dimensionLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            dimensionLayer.FeatureSource.Projection = projection;

            CreateAnnotationMemoryLayer();

            //shapes overlay
            layerOverlay = new LayerOverlay();
            layerOverlay.DrawingExceptionMode = DrawingExceptionMode.DrawException;
            layerOverlay.Layers.Add("fieldLayer", fieldsLayer);
            layerOverlay.Layers.Add("currentLayer", currentLayer);
            layerOverlay.Layers.Add("cropzoneLayer", cropzoneLayer);
            layerOverlay.Layers.Add("cropzoneLayerCZ", cropzoneLayerCZ);
            layerOverlay.Layers.Add("annotationLayer", annotationLayer);
            layerOverlay.Layers.Add("cropzoneLayerDraw", cropzoneLayerDraw);

            popupOverlay = new PopupOverlay();
            popupOverlay.DrawingExceptionMode = DrawingExceptionMode.DrawException;

            dimensionOverlay = new LayerOverlay();
            dimensionOverlay.DrawingExceptionMode = DrawingExceptionMode.DrawException;
            dimensionOverlay.Layers.Add("measureLayer", measureLayer);
            dimensionOverlay.Layers.Add("dimensionLayer", dimensionLayer);


            Map.Loaded += (sender, e) => {
                if (maploaded) { return; }
                // TODO: Be nice to get extent data based on current user location, if possible...
                var extent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
                var projectionstring = Proj4Projection.GetGoogleMapParametersString();

                LoadBingMapsOverlay();
                RebuildMapAnnotations();

                Map.Overlays.Add("fieldOverlay", layerOverlay);
                Map.Overlays.Add("popupOverlay", popupOverlay);
                Map.Overlays.Add("dimensionOverlay", dimensionOverlay);

                EnsureCurrentLayerIsOpen();
                EnsureCropzoneLayerIsOpen();
                EnsureMeasureLayerIsOpen();
                EnsureDimensionLayerIsOpen();
                EnsureAnnotationLayerIsOpen();
                if (currentLayer.InternalFeatures.Count > 0) {
                    try {
                        Map.CurrentExtent = currentLayer.GetBoundingBox();
                        PointShape center = currentLayer.GetBoundingBox().GetCenterPoint();
                        SnapToGoogleImagery();
                    }
                    catch (Exception exception) {
                        log.ErrorException("Couldn't load the current item's map.", exception);
                    }
                }
                else {
                    try {
                        Map.CurrentExtent = extent;
                        PointShape center = extent.GetCenterPoint();
                        //Map.CenterAt(center);
                        //mapextent = Map.CurrentExtent;
                    }
                    catch (Exception exception) {
                        log.ErrorException("Couldn't load the current item's map.", exception);
                    }
                }
                maploaded = true;
            };

            //Map.OverlayDrawing += (sender, e) => {
            //    if (Map.Overlays.Contains("Google")) {
            //        int zoomLevelsNumber = ((GoogleMapLayerControl)((LayerOverlay)Map.Overlays["Google"]).Layers["GoogleMapsLayer"]).ZoomLevelsNumber;
            //        ((LayerOverlay)Map.Overlays["Google"]).Attribution = SelectedOverlay.LoadAttribution(zoomLevelsNumber);
            //    }
            //};

            Map.CurrentScaleChanged += (sender, e) => {
                scalingTextStyle.MapScale = e.CurrentScale;
                scalingTextStyleCZ.MapScale = e.CurrentScale;
                scalingTextStyle2.MapScale = e.CurrentScale;
                customMultiStyles.MapScale = Map.CurrentScale;

                //if (Map.Overlays.Contains("fieldOverlay")) {
                //    Map.Overlays["fieldOverlay"].Refresh();
                //}
                //bingOverlay.Drawing += (sender, e) => {
                //    zoomLevelsNumber = ((GoogleMapLayerControl)bingOverlay.Layers["GoogleMapsLayer"]).ZoomLevelsNumber;
                //    bingOverlay.Attribution = LoadAttribution(((GoogleMapLayerControl)bingOverlay.Layers["GoogleMapsLayer"]).ZoomLevelsNumber);
                //};

                //bingOverlay.DrawingAttribution += (sender, e) => {
                //    zoomLevelsNumber = ((GoogleMapLayerControl)bingOverlay.Layers["GoogleMapsLayer"]).ZoomLevelsNumber;
                //    e.Attribution = LoadAttribution(((GoogleMapLayerControl)bingOverlay.Layers["GoogleMapsLayer"]).ZoomLevelsNumber);
                //};
                if (repaintthescreen) {
                    repaintthescreen = false;
                }
            };

            Map.CurrentExtentChanged += (sender, e) => {
                mapextent = e.CurrentExtent;
                if (!maploaded) { return; }
                if (!ApplicationEnvironment.ShowPremiumMap) { return; }
                if (notcenteredyet) {
                    notcenteredyet = false;
                    //return;
                }
                if (SelectedOverlayMapStatusIsGoogle()) {

                    oldticks = newticks;
                    newticks = DateTime.Now.Ticks;
                    long diffticks = newticks - oldticks;
                    //if(diffticks < 5000000) { return; }

                    PointShape centerpoint = mapextent.GetCenterPoint();
                    PointShape wgscenterpoint = projection.ConvertToInternalProjection(centerpoint) as PointShape;
                    UTMConstruction utmclass = zoomlevelfactory.GetUTMZone(wgscenterpoint);
                    int epsgcode = utmclass.epsgcode;
                    projection.ExternalProjectionParametersString = Proj4Projection.GetEpsgParametersString(epsgcode);
                    Proj4Projection projectionutm = new Proj4Projection(Proj4Projection.GetEpsgParametersString(epsgcode), Proj4Projection.GetGoogleMapParametersString());
                    projectionutm.Open();
                    PointShape gglcenterpoint = projection.ConvertToExternalProjection(centerpoint) as PointShape;
                    RectangleShape gglextent = projection.ConvertToExternalProjection(mapextent) as RectangleShape;
                    RectangleShape wgsextent = projection.ConvertToInternalProjection(mapextent) as RectangleShape;
                    RectangleShape utmextent = projectionutm.ConvertToInternalProjection(mapextent) as RectangleShape;
                    int currentzoomlevel = zoomlevelfactory.GetZoomLevelFromScale(Map.CurrentScale);
                    bool isthisagooglezoomlevel = zoomlevelfactory.VerifyGoogleZoomLevelFromScale(Map.CurrentScale);
                    if(!isthisagooglezoomlevel) { return; }

                    if (this.projectionFromSphericalMercator == null) {
                        string googleMapParametersString = ManagedProj4Projection.GetEpsgParametersString(3857);
                        string googleMapParametersString2 = ManagedProj4Projection.GetEpsgParametersString(3857);
                        this.projectionFromSphericalMercator = new ManagedProj4Projection(googleMapParametersString, googleMapParametersString2);
                        this.projectionFromSphericalMercator.Open();
                    }
                    RectangleShape gglextent2 = (RectangleShape)this.projectionFromSphericalMercator.ConvertToInternalProjection(mapextent);
                    if (this.projectionFromWGS84 == null) {
                        string wgsParametersString = ManagedProj4Projection.GetWgs84ParametersString();
                        string wgspParametersString2 = ManagedProj4Projection.GetWgs84ParametersString();
                        this.projectionFromWGS84 = new ManagedProj4Projection(wgsParametersString, wgspParametersString2);
                        this.projectionFromWGS84.Open();
                    }
                    RectangleShape wgsextent2 = (RectangleShape)this.projectionFromWGS84.ConvertToInternalProjection(mapextent);
                    PointShape wgscenterpoint2 = wgsextent2.GetCenterPoint();
                    UTMConstruction utmclass2 = zoomlevelfactory.GetUTMZone(wgscenterpoint2);
                    int epsgcode2 = utmclass2.epsgcode;
                    PointShape wgscenterpoint3 = (PointShape)this.projectionFromWGS84.ConvertToInternalProjection(centerpoint);
                    UTMConstruction utmclass3 = zoomlevelfactory.GetUTMZone(wgscenterpoint3);
                    int epsgcode3 = utmclass3.epsgcode;

                    if (this.projectionFromSM == null) {
                        string smParametersString = ManagedProj4Projection.GetWgs84ParametersString();
                        string smParametersString2 = ManagedProj4Projection.GetWgs84ParametersString();
                        this.projectionFromSM = new ManagedProj4Projection(smParametersString, smParametersString2);
                        this.projectionFromSM.Open();
                    }
                    RectangleShape wgsextent4 = (RectangleShape)this.projectionFromSM.ConvertToInternalProjection(mapextent);
                    log.Info("MapDisplay - Get Google imagery.");

                    //Map.Height = 640;
                    //Map.Width = 640;
                    int tileHeight = System.Convert.ToInt32(Map.ActualHeight);
                    int tileWidth = System.Convert.ToInt32(Map.ActualWidth);
                    Overlay googloverlay = SelectedOverlay.ExtentHasChanged(tileHeight, tileWidth, utmextent, gglextent2, wgscenterpoint, currentzoomlevel, epsgcode);
                    if (Map.Overlays.Contains("Bing")) {
                        Map.Overlays.Remove("Bing");
                    }
                    if (Map.Overlays.Contains("Bing Online")) {
                        Map.Overlays.Remove("Bing Online");
                    }
                    if (Map.Overlays.Contains("Roads Only")) {
                        Map.Overlays.Remove("Roads Only");
                    }
                    if (Map.Overlays.Contains("Road Online")) {
                        Map.Overlays.Remove("Road Online");
                    }
                    if (Map.Overlays.Contains("Google")) {
                        Map.Overlays.Remove("Google");
                    }
                    if (Map.Overlays.Contains("Google1")) {
                        Map.Overlays.Remove("Google1");
                    }
                    if (Map.Overlays.Contains("None")) {
                        Map.Overlays.Remove("None");
                    }
                    if (googloverlay != null) {
                        Map.Overlays.Add(SelectedOverlay.Name, googloverlay);
                        Map.Overlays.MoveToBottom(SelectedOverlay.Name);
                        Map.Overlays[SelectedOverlay.Name].Refresh();
                    }
                }
            };

            Map.MouseMove += (sender, e) => {
                PointShape worldlocation2 = Map.ToWorldCoordinate(e.GetPosition(Map));
                PointShape centerpoint = projection.ConvertToInternalProjection(worldlocation2) as PointShape;
                Vertex geoVertex1 = new Vertex(centerpoint.X, centerpoint.Y);
                string _DecimalDegreeString = Math.Round(geoVertex1.Y, 5) + ", " + Math.Round(geoVertex1.X, 5);
                string displaycoordinates1 = DecimalDegrees.ddstring_To_DMs(_DecimalDegreeString, mapsettings.DisplayCoordinateFormat);
                parent.DisplayCoordinates = displaycoordinates1;
            };

            Map.EditOverlay.MapMouseUp += (sender, e) => {
                if (calculatingdimensions) { return; }
                if (draggingfeatures) {
                    draggingfeatures = false;
                    try {
                        double worldx = e.InteractionArguments.WorldX;
                        double worldy = e.InteractionArguments.WorldY;
                        PointShape projectedpoint = projection.ConvertToInternalProjection(new PointShape(worldx, worldy)) as PointShape;
                        CalculateDimensionLayer(null, projectedpoint);
                    }
                    catch (Exception ex) {
                        int z = 0;
                    }
                }
            };

            Map.EditOverlay.FeatureDragging += (sender, e) => {
                draggingfeatures = true;
            };

            Map.EditOverlay.VertexMoving += (sender, e) => {
                draggingfeatures = true;
            };

            Map.EditOverlay.VertexMoved += (sender, e) => {
                if (calculatingdimensions) { return; }
                try {
                    Vertex worldvertex = e.MovedVertex;
                    PointShape projectedpoint = projection.ConvertToInternalProjection(new PointShape(worldvertex)) as PointShape;
                    CalculateDimensionLayer(projectedpoint, null);
                }
                catch (Exception ex) {
                    int z = 0;
                }
            };

            //Map.EditOverlay.VertexRemoved += (sender, e) => {
            //        removingvertex = false;
            //        if (calculatingdimensions) { return; }
            //        try {
            //            CalculateDimensionLayer();
            //            Map.Refresh();
            //        }
            //        catch (Exception ex) {
            //            int z = 0;
            //        }
            //};

            //Map.TrackOverlay.TrackStarted += (sender, e) => {
            //    trackingfeatures = true;
            //};

            //Map.TrackOverlay.TrackEnded += (sender, e) => {
            //    trackingfeatures = false;
            //};

            //Map.TrackOverlay.VertexAdded += (sender, e) => {
            //    addingvertex = false;
            //    if (calculatingdimensions) { return; }
            //    try {
            //        CalculateDimensionLayer();
            //        //Map.Refresh();
            //    }
            //    catch (Exception ex) {
            //        int z = 0;
            //    }
            //};

            //Map.TrackOverlay.MouseMoved += (sender, e) => {
            //    if (calculatingdimensions) { return; }
            //    if (trackingfeatures) {
            //        if (draggingfeatures) { return; }
            //        if (removingvertex) { return; }
            //        if (addingvertex) { return; }
            //        try {
            //            //double worldx = e.InteractionArguments.WorldX;
            //            //double worldy = e.InteractionArguments.WorldY;
            //            //PointShape projectedpoint = projection.ConvertToInternalProjection(new PointShape(worldx, worldy)) as PointShape;

            //            //Vertex worldvertex = e.MovedVertex;
            //            //PointShape projectedpoint = projection.ConvertToInternalProjection(new PointShape(worldvertex)) as PointShape;

            //            //CalculateDimensionLayer();
            //        }
            //        catch (Exception ex) {
            //            int z = 0;
            //        }
            //    }
            //};

        }

        internal void LoadMapForItem(object item, Dispatcher dispatcher) {
            IIdentity itemId = null;
            IIdentity frmitemId = null;
            IIdentity czitemId = null;
            selectedid = string.Empty;
            UpdatedDisplayArea = string.Empty;
            bool isdoublecrop = false;
            HomeDetailsViewModel homitem = null;
            FarmDetailsViewModel frmitem = null;
            FieldDetailsViewModel fvitem = null;
            CropZoneDetailsViewModel czitem = null;
            selectedIdentity = null;
            FieldCropzonelist = new List<MapItem>();
            SelectedFieldlist = new List<MapItem>();
            fieldsLayerAreaStyle.SelectedFeatures = new List<Feature>();
            fieldsLayerAreaStyle.HiddenFeatures = new List<Feature>();
            scalingTextStyle.SelectedFeatures = new List<Feature>();
            cropzonesLayerAreaStyle.SelectedFeatures = new List<Feature>();
            cropzonesLayerAreaStyle.HiddenFeatures = new List<Feature>();
            scalingTextStyle.CustomLabels = new Dictionary<string, string>();
            string stringid = string.Empty;
            if (item is HomeDetailsViewModel) {
                homitem = (HomeDetailsViewModel)item;
                selectedfield = new Feature();
            }
            if (item is FarmDetailsViewModel) {
                itemId = ((FarmDetailsViewModel)item).FarmDetails.Id;
                frmitem = (FarmDetailsViewModel)item;
                selectedIdentity = itemId;
                selectedfield = new Feature();
            }
            if (item is FieldDetailsViewModel) {
                itemId = ((FieldDetailsViewModel)item).FieldView.Id;
                stringid = ((FieldDetailsViewModel)item).FieldView.Id.Id.ToString();
                fvitem = (FieldDetailsViewModel)item;
                selectedIdentity = itemId;
                if (!labelvalues.ContainsKey(stringid)) {
                    labelvalues.Add(stringid, fvitem.Name);
                }
                else {
                    labelvalues[stringid] = fvitem.Name;
                }
                selectedfield = new Feature();
                log.Info("MapDisplay - Change selected field to " + fvitem.Name + "  " + stringid);
            }
            if (item is CropZoneDetailsViewModel) {

                itemId = ((CropZoneDetailsViewModel)item).CropZoneView.Id;
                stringid = ((CropZoneDetailsViewModel)item).CropZoneView.Id.Id.ToString();
                czitem = (CropZoneDetailsViewModel)item;
                frmitemId = czitem.CropZoneView.FieldId;
                czitemId = ((CropZoneDetailsViewModel)item).CropZoneView.Id;

                selectedIdentity = itemId;
                if (!labelvalues.ContainsKey(stringid)) {
                    labelvalues.Add(stringid, czitem.Name);
                }
                else {
                    labelvalues[stringid] = czitem.Name;
                }
                log.Info("MapDisplay - Change selected cropzone to " + czitem.Name + "  " + stringid);
            }

            try {
                EnsureCurrentLayerIsOpen();
                EnsureCropzoneLayerIsOpen();
                cropzoneLayer.IsVisible = false;
                cropzoneLayerCZ.IsVisible = false;
                cropzoneLayerDraw.IsVisible = allowzropzoneoutlines;
                currentLayer.IsVisible = true;
                fieldsLayer.IsVisible = true;
                scalingTextStyle.IsActive = true;
                currentLayer.InternalFeatures.Clear();
                cropzoneLayer.InternalFeatures.Clear();
                cropzoneLayerCZ.InternalFeatures.Clear();
                cropzoneLayerDraw.InternalFeatures.Clear();
                selectedfeature = new Feature();
            }
            catch { }
            if (itemId == null) {
                if (homitem != null) {
                    foreach (var m in clientLayerItems) {
                        stringid = m.FieldId.ToString(); ;
                        if (shapevalues.ContainsKey(stringid)) {
                            m.MapData = shapevalues[stringid];
                        }
                        if (m.MapData != null && !string.IsNullOrEmpty(m.MapData) && m.MapData != "MULTIPOLYGON EMPTY") {
                            IDictionary<string, string> columnvalues = new Dictionary<string, string>();
                            //GetCustomFieldName(m.FieldId, m.Name);
                            columnvalues.Add(@"LdbLabel", m.Name);
                            var testfeat = new Feature(m.MapData);
                            BaseShape testbase = testfeat.GetShape();
                            var f = new Feature(m.MapData, m.FieldId.ToString(), columnvalues);
                            MultipolygonShape mdataconvert = new MultipolygonShape();
                            if (testbase is PolygonShape) {
                                mdataconvert.Polygons.Add(testbase as PolygonShape);
                                f = new Feature(mdataconvert.GetWellKnownText(), m.FieldId.ToString(), columnvalues);
                            }
                            if (testbase is MultipolygonShape) {
                                mdataconvert = new MultipolygonShape(m.MapData);
                                f = new Feature(mdataconvert.GetWellKnownText(), m.FieldId.ToString(), columnvalues);
                            }

                            if (!currentLayer.InternalFeatures.Contains(m.FieldId.ToString())) {
                                currentLayer.InternalFeatures.Add(m.FieldId.ToString(), f);
                            }
                        }
                    }
                    SelectedFeatures = currentLayer.InternalFeatures;
                    fieldsLayerAreaStyle.SelectedFeatures = currentLayer.InternalFeatures;
                    scalingTextStyle.SelectedFeatures = currentLayer.InternalFeatures;

                    if (Map.Overlays.Contains("fieldOverlay")) {
                        try {
                            if (currentLayer.InternalFeatures.Count > 0) {
                                notcenteredyet = true;
                                Map.CurrentExtent = currentLayer.GetBoundingBox();
                                notcenteredyet = false;
                                PointShape center = currentLayer.GetBoundingBox().GetCenterPoint();
                                Map.CenterAt(center);
                            }
                            SnapToGoogleImagery();
                            scalingTextStyle.MapScale = Map.CurrentScale;
                            scalingTextStyleCZ.MapScale = Map.CurrentScale;
                            scalingTextStyle2.MapScale = Map.CurrentScale;
                            customMultiStyles.MapScale = Map.CurrentScale;
                            Map.Overlays["fieldOverlay"].Refresh();
                        }
                        catch (Exception exception) {
                            log.ErrorException("Couldn't load the current item's map.", exception);
                        }
                    }
                }
                if (Map.Overlays.Contains("fieldOverlay")) {
                    try {
                    scalingTextStyle.MapScale = Map.CurrentScale;
                    scalingTextStyleCZ.MapScale = Map.CurrentScale;
                    scalingTextStyle2.MapScale = Map.CurrentScale;
                    customMultiStyles.MapScale = Map.CurrentScale;
                    Map.Overlays["fieldOverlay"].Refresh();
                    }
                    catch (Exception exception) {
                        log.ErrorException("Couldn't load the current item's map.", exception);
                    }

                }
                return;
            }


            var maps = clientEndpoint.GetView<ItemMap>(itemId);
            try {
                if (maps.HasValue) {
                    ItemMapItem mri = GetMostRecentShapeByYear(maps);
                    if(checknullshapes && mri.MapData == null && mri.DataType == null && frmitemId != null && czitemId != null)
                    {
                        System.Windows.MessageBox.Show("A current cropzone shape was not found, retrieving a parent field shape");
                        //System.Windows.MessageBox.Show(Strings.ProblemMergingImportShapeWithExisting_Text);
                        maps = clientEndpoint.GetView<ItemMap>(frmitemId);
                        mri = GetMostRecentShapeByYear(maps);
                    }
                    if (mri == null) {
                        fieldsLayerAreaStyle.SelectedFeatures = new List<Feature>();
                        fieldsLayerAreaStyle.HiddenFeatures = new List<Feature>();
                        scalingTextStyle.SelectedFeatures = new List<Feature>();
                        cropzonesLayerAreaStyle.SelectedFeatures = new List<Feature>();
                        cropzonesLayerAreaStyle.HiddenFeatures = new List<Feature>();
                        scalingTextStyle.CustomLabels = new Dictionary<string, string>();
                        currentLayer.InternalFeatures.Clear();
                        cropzoneLayer.InternalFeatures.Clear();
                        cropzoneLayer.InternalFeatures.Clear();
                        cropzoneLayerCZ.InternalFeatures.Clear();
                        if (Map.Overlays.Contains("fieldOverlay"))
                        {
                            Map.Overlays["fieldOverlay"].Refresh();
                        }
                        return;
                    }
                    if (shapevalues.ContainsKey(stringid)) {
                        mri.MapData = shapevalues[stringid];
                    }

                    var labels = clientEndpoint.GetView<ItemMap>(itemId);
                    if (labels.HasValue) {
                        ItemLabelItem mrli = labels.Value.MostRecentLabelItem;
                        if (mrli != null && !mrli.OptionalVisibility) {
                            parent.ShowDrawCommands = false;
                            parent.ShowEditCommands = false;
                            parent.ShowOptionCommands = false;
                            if (Map.Overlays.Contains("fieldOverlay")) {
                                Map.Overlays["fieldOverlay"].Refresh();
                            }
                            return;
                        }
                        if (mrli != null && !string.IsNullOrWhiteSpace(mrli.SpatialLabelPosition)) {
                            PointShape ps = new PointShape(mrli.SpatialLabelPosition);
                            if (labelPositions.ContainsKey(stringid)) {
                                labelPositions[stringid] = ps;
                            }
                            else {
                                labelPositions.Add(stringid, ps);
                            }
                        }

                        if (mrli != null)
                        {
                            if (rotationAngles.ContainsKey(stringid))
                            {
                                rotationAngles[stringid] = (double)mrli.HorizontalLabelRotation;
                            }
                            else
                            {
                                if (mrli.HorizontalLabelRotation != 0)
                                {
                                    rotationAngles.Add(stringid, (double)mrli.HorizontalLabelRotation);
                                }
                            }
                        }

                    }
                    scalingTextStyle.RotationAngles = rotationAngles;
                                scalingTextStyle.LabelPositions = labelPositions;
                                scalingTextStyle.MapScale = Map.CurrentScale;
                                scalingTextStyleCZ.RotationAngles = rotationAngles;
                                scalingTextStyleCZ.LabelPositions = labelPositions;
                                scalingTextStyleCZ.MapScale = Map.CurrentScale;
                                scalingTextStyle2.MapScale = Map.CurrentScale;
                                customMultiStyles.MapScale = Map.CurrentScale;

                    //if (string.IsNullOrEmpty(mri.MapData) || mri.MapData == "MULTIPOLYGON EMPTY") {
                    //    if (Map.Overlays.Contains("fieldOverlay")) {
                    //        Map.Overlays["fieldOverlay"].Refresh();
                    //    }
                    //    return;
                    //}
                    if (!string.IsNullOrEmpty(mri.MapData) && mri.MapData != "MULTIPOLYGON EMPTY") {
                        var feature = new Feature(mri.MapData, stringid);
                        BaseShape testbase = feature.GetShape();
                        MultipolygonShape mdataconvert = new MultipolygonShape();
                        if (testbase is PolygonShape) {
                            mdataconvert.Polygons.Add(testbase as PolygonShape);
                            feature = new Feature(mdataconvert.GetWellKnownText(), stringid);
                        }


                        selectedfeature = feature;
                        BaseShape bs1 = feature.GetShape();
                        MultipolygonShape abs100 = feature.GetShape() as MultipolygonShape;
                        double abs100area = abs100.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
                        string featureid = feature.Id;
                        MultipolygonShape mps1 = new MultipolygonShape();
                        PointShape center = new PointShape();
                        PointShape selectedcenter = new PointShape();
                        if (bs1 is MultipolygonShape) {
                            mps1 = bs1 as MultipolygonShape;
                            center = mps1.GetCenterPoint();
                            selectedcenter = mps1.GetCenterPoint();
                            center = projection.ConvertToExternalProjection(center) as PointShape;
                        }

                        if (fvitem != null) {
                            selectedid = fvitem.FieldView.Id.Id.ToString();
                            //string customarealabel = GetCustomFieldName(fvitem.FieldTreeItemVM.FieldId.Id, fvitem.FieldView.Name);
                            string customareaonlylabel = GetCustomAreaLabel(fvitem.FieldTreeItemVM.FieldId.Id, true);
                            string customarealabel = DisplayLabelWithArea(fvitem.FieldView.FieldNameByCropYear[ApplicationEnvironment.CurrentCropYear], customareaonlylabel);
                            feature.ColumnValues.Add(@"LdbLabel", fvitem.FieldView.FieldNameByCropYear[ApplicationEnvironment.CurrentCropYear]);
                            feature.ColumnValues.Add(@"AreaLabel", customarealabel);
                            feature.ColumnValues.Add(@"AreaOnlyLabel", customareaonlylabel);
                            currentLayer.InternalFeatures.Add(selectedid, feature);
                            SelectedField = feature;
                            SelectedFeatures = currentLayer.InternalFeatures;
                            fieldsLayerAreaStyle.SelectedFeatures = currentLayer.InternalFeatures;
                            scalingTextStyle.SelectedFeatures = currentLayer.InternalFeatures;


                            MapItem mi = new MapItem();
                            mi.FarmId = ((FarmId)fvitem.FieldTreeItemVM.Parent.Id).Id;
                            mi.FieldId = fvitem.FieldView.Id.Id;
                            mi.Name = fvitem.FieldView.Name;
                            ItemMapItem mostrecentitem = GetMostRecentShapeByYear(maps);
                            if (mostrecentitem != null) {
                                mi.MapData = mostrecentitem.MapData;
                                mi.MapDataType = mostrecentitem.DataType;
                            }
                            SelectedFieldlist.Add(mi);


                            if (fvitem.FieldTreeItemVM.Children.Count > 0) {
                                foreach (object czobject in fvitem.FieldTreeItemVM.Children) {
                                    IIdentity itemIdcz = null;
                                    if (czobject is CropZoneTreeItemViewModel) {
                                        CropZoneTreeItemViewModel fieldczitem = czobject as CropZoneTreeItemViewModel;
                                        string stringid1 = fieldczitem.CropZoneId.Id.ToString();
                                        itemIdcz = fieldczitem.CropZoneId;
                                        if (!labelvalues.ContainsKey(stringid1)) {
                                            labelvalues.Add(stringid1, fieldczitem.Name);
                                        }
                                        else {
                                            labelvalues[stringid1] = fieldczitem.Name;
                                        }
                                        var czmaps = clientEndpoint.GetView<ItemMap>(itemIdcz);
                                        if (czmaps.HasValue) {
                                            ItemMapItem mri1 = GetMostRecentShapeByYear(czmaps);
                                            //if (mri1 == null) { return; }
                                            if (mri1 != null) {
                                                if (string.IsNullOrWhiteSpace(mri1.MapData)) { continue; }
                                                ItemLabelItem mrli = czmaps.Value.MostRecentLabelItem;
                                                // cropzone visibility
                                                if (mrli != null && !mrli.OptionalVisibility) {
                                                    continue;
                                                }
                                                //string customarealabel1 = GetCustomCropZoneName(fieldczitem.CropZoneId.Id, fieldczitem.Name);
                                                string customareaonlylabel1 = GetCustomAreaLabel(fieldczitem.CropZoneId.Id, false);
                                                string customarealabel1 = DisplayLabelWithArea(fieldczitem.Name, customareaonlylabel1);
                                                IDictionary<string, string> columnvalues = new Dictionary<string, string>();
                                                columnvalues.Add(@"LdbLabel", fieldczitem.Name);
                                                columnvalues.Add(@"AreaLabel", customarealabel1);
                                                columnvalues.Add(@"AreaOnlyLabel", customareaonlylabel1);
                                                var czfeature = new Feature(mri1.MapData, stringid1, columnvalues);
                                                BaseShape testbase1 = feature.GetShape();
                                                MultipolygonShape mdataconvert1 = new MultipolygonShape();
                                                if (testbase1 is PolygonShape) {
                                                    mdataconvert1.Polygons.Add(testbase1 as PolygonShape);
                                                    czfeature = new Feature(mdataconvert1.GetWellKnownText(), stringid1, columnvalues);
                                                }
                                                cropzoneLayer.InternalFeatures.Add(stringid1, czfeature);
                                                cropzoneLayerCZ.InternalFeatures.Add(stringid1, czfeature);
                                                cropzoneLayer.IsVisible = true;
                                                cropzoneLayerCZ.IsVisible = true;
                                                currentLayer.IsVisible = true;
                                                fieldsLayer.IsVisible = true;
                                                scalingTextStyle.IsActive = false;
                                                //if (fvitem.FieldTreeItemVM.Children.Count == 1) {
                                                //    scalingTextStyle.IsActive = false;
                                                //}


                                                try {
                                                    MultipolygonShape overlappoly = abs100.GetIntersection(czfeature);
                                                    double overlaparea = overlappoly.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
                                                    AreaBaseShape caabs90 = czfeature.GetShape() as AreaBaseShape;
                                                    double czabs90area = caabs90.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
                                                    double featurepercent = (overlaparea / czabs90area) * 100;
                                                    double abs100percent = (overlaparea / abs100area) * 100;
                                                    if (featurepercent > 98 && featurepercent < 120 && abs100percent > 96) {
                                                        isdoublecrop = true;
                                                        cropzoneLayer.IsVisible = false;
                                                        cropzoneLayerCZ.IsVisible = false;
                                                        currentLayer.IsVisible = true;
                                                        fieldsLayer.IsVisible = true;
                                                        scalingTextStyle.IsActive = true;
                                                        cropzoneLayer.InternalFeatures.Clear();
                                                        cropzoneLayerCZ.InternalFeatures.Clear();
                                                        break;
                                                    }
                                                }
                                                catch (Exception ex8) {
                                                    int ppp = 0;
                                                }

                                                MapItem mi1 = new MapItem();
                                                mi1.FarmId = ((FarmId)fvitem.FieldTreeItemVM.Parent.Id).Id;
                                                mi1.FieldId = fvitem.FieldView.Id.Id;
                                                mi1.CropZoneId = fieldczitem.CropZoneId.Id;
                                                mi1.Name = fieldczitem.Name;
                                                if (czmaps.Value.MostRecentMapItem != null) {
                                                    mi1.MapData = czmaps.Value.MostRecentMapItem.MapData;
                                                    mi1.MapDataType = czmaps.Value.MostRecentMapItem.DataType;
                                                }
                                                FieldCropzonelist.Add(mi1);

                                                //ItemLabelItem mrli = czmaps.Value.MostRecentLabelItem;
                                                if (mrli != null && !string.IsNullOrWhiteSpace(mrli.SpatialLabelPosition)) {
                                                    PointShape ps = new PointShape(mrli.SpatialLabelPosition);
                                                    if (labelPositions.ContainsKey(stringid1)) {
                                                        labelPositions[stringid1] = ps;
                                                    }
                                                    else {
                                                        labelPositions.Add(stringid1, ps);
                                                    }
                                                }

                                                if (mrli != null)
                                                {
                                                    if (rotationAngles.ContainsKey(stringid1))
                                                    {
                                                        rotationAngles[stringid1] = (double)mrli.HorizontalLabelRotation;
                                                    }
                                                    else
                                                    {
                                                        if (mrli.HorizontalLabelRotation != 0)
                                                        {
                                                            rotationAngles.Add(stringid1, (double)mrli.HorizontalLabelRotation);
                                                        }
                                                    }
                                                }
                                                scalingTextStyleCZ.RotationAngles = rotationAngles;
                                                scalingTextStyleCZ.LabelPositions = labelPositions;
                                                scalingTextStyleCZ.MapScale = Map.CurrentScale;
                                            }
                                        }
                                    }
                                }
                            }
                            if (cropzoneLayerCZ.InternalFeatures.Count > 0) {
                                fieldsLayerAreaStyle.HiddenFeatures = currentLayer.InternalFeatures;
                                cropzonesLayerAreaStyle.SelectedFeatures = cropzoneLayerCZ.InternalFeatures;
                            }
                        }
                        if (czitem != null) {
                            selectedid = czitem.CropZoneView.FieldId.Id.ToString();
                            string selectedczparent = czitem.CropZoneView.Id.Id.ToString();
                            string selectedfieldparent = czitem.CropZoneView.FieldId.Id.ToString();
                            //string customarealabel = GetCustomCropZoneName(czitem.CropZoneView.Id.Id, czitem.CropZoneView.Name);
                            string customareaonlylabel = GetCustomAreaLabel(czitem.CropZoneView.Id.Id, false);
                            string customarealabel = DisplayLabelWithArea(czitem.CropZoneView.Name, customareaonlylabel);
                            feature.ColumnValues.Add(@"LdbLabel", czitem.CropZoneView.Name);
                            feature.ColumnValues.Add(@"AreaLabel", customarealabel);
                            feature.ColumnValues.Add(@"AreaOnlyLabel", customareaonlylabel);
                            currentLayer.InternalFeatures.Add(selectedid, feature);

                            cropzonesLayerAreaStyle.SelectedFeatures = currentLayer.InternalFeatures;

                            if (fieldsLayer.InternalFeatures.Contains(czitem.CropZoneView.FieldId.Id.ToString())) {
                                Feature geofeature = fieldsLayer.InternalFeatures[czitem.CropZoneView.FieldId.Id.ToString()];
                                var fieldfeature = new Feature(geofeature.GetShape());
                                fieldfeature.ColumnValues.Add(@"LdbLabel", geofeature.ColumnValues[@"LdbLabel"]);
                                fieldfeature.ColumnValues.Add(@"AreaLabel", geofeature.ColumnValues[@"AreaLabel"]);
                                fieldfeature.ColumnValues.Add(@"AreaOnlyLabel", geofeature.ColumnValues[@"AreaOnlyLabel"]);
                                SelectedField = fieldfeature;
                                
                                BaseShape fieldshape = geofeature.GetShape();
                                MultipolygonShape mps = fieldshape as MultipolygonShape;
                                var fldfeature = new Feature(mps.GetWellKnownText(), czitem.CropZoneView.FieldId.Id.ToString());
                                cropzoneLayerDraw.InternalFeatures.Add(czitem.CropZoneView.FieldId.Id.ToString(), fldfeature);
                            }

                            Feature firstfeature = feature;
                            MultipolygonShape firstshape = feature.GetShape() as MultipolygonShape;
                            //MultipolygonShape unionshape = firstshape;
                            MultipolygonShape unionshape = new MultipolygonShape();

                            var years = clientEndpoint.GetView<FieldTreeView>(new DataSourceId(ApplicationEnvironment.CurrentDataSourceId));
                            var yearItem = (years.HasValue && years.Value.CropYearList.ContainsKey(ApplicationEnvironment.CurrentCropYear)) ? years.Value.CropYearList[ApplicationEnvironment.CurrentCropYear] : new TreeViewYearItem();
                            IList<TreeViewFarmItem> farms = yearItem.Farms != null ? yearItem.Farms : new List<TreeViewFarmItem>();

                            var q = from f in yearItem.Farms
                                    from fi in f.Fields
                                    from cz in fi.CropZones
                                    let itemMap = clientEndpoint.GetView<ItemMap>(cz.CropZoneId)
                                    where fi.FieldId == czitem.CropZoneView.FieldId && itemMap.HasValue && itemMap.Value.MostRecentMapItem != null
                                    select new { f.FarmId, fi.FieldId, cz, itemMap.Value.MostRecentMapItem.MapData, itemMap.Value.MostRecentMapItem.DataType, itemMap.Value.MostRecentLabelItem };


                            foreach (var cropZ in q) {
                                IIdentity itemIdcz = null;
                                if (cropZ.cz.CropZoneId == null) { continue; }
                                string stringid1 = cropZ.cz.CropZoneId.Id.ToString();
                                //if(stringid1 == selectedczparent)  { continue; }
                                itemIdcz = cropZ.cz.CropZoneId;

                                ItemLabelItem mrli = cropZ.MostRecentLabelItem;
                                // cropzone visibility
                                if (mrli != null && !mrli.OptionalVisibility) {
                                    continue;
                                }


                                if (!labelvalues.ContainsKey(stringid1)) {
                                    labelvalues.Add(stringid1, cropZ.cz.Name);
                                }
                                else {
                                    labelvalues[stringid1] = cropZ.cz.Name;
                                }

                                if (string.IsNullOrWhiteSpace(cropZ.MapData)) { continue; }
                                var czonefeature = new Feature(cropZ.MapData, stringid1);
                                cropzoneLayerDraw.InternalFeatures.Add(stringid1, czonefeature);
                                cropzoneLayerDraw.IsVisible = allowzropzoneoutlines;

                                //string customarealabel1 = GetCustomCropZoneName(cropZ.cz.CropZoneId.Id, cropZ.cz.Name);
                                string customareaonlylabel1 = GetCustomAreaLabel(cropZ.cz.CropZoneId.Id, false);
                                string customarealabel1 = DisplayLabelWithArea(cropZ.cz.Name, customareaonlylabel1);
                                IDictionary<string, string> columnvalues = new Dictionary<string, string>();
                                columnvalues.Add(@"LdbLabel", cropZ.cz.Name);
                                columnvalues.Add(@"AreaLabel", customarealabel1);
                                columnvalues.Add(@"AreaOnlyLabel", customareaonlylabel1);
                                var czfeature = new Feature(cropZ.MapData, stringid1, columnvalues);
                                BaseShape testbase1 = feature.GetShape();
                                MultipolygonShape mdataconvert1 = new MultipolygonShape();
                                if (testbase1 is PolygonShape) {
                                    mdataconvert1.Polygons.Add(testbase1 as PolygonShape);
                                    czfeature = new Feature(mdataconvert1.GetWellKnownText(), stringid1, columnvalues);
                                }
                                cropzoneLayer.InternalFeatures.Add(stringid1, czfeature);
                                cropzoneLayerCZ.InternalFeatures.Add(stringid1, czfeature);
                                cropzoneLayer.IsVisible = false;
                                cropzoneLayerCZ.IsVisible = true;
                                currentLayer.IsVisible = true;
                                fieldsLayer.IsVisible = true;
                                scalingTextStyle.IsActive = false;

                                if (stringid1 != selectedczparent) {
                                    try {
                                        if (unionshape.Polygons.Count > 0) {
                                            AreaBaseShape abstest = czfeature.GetShape() as AreaBaseShape;
                                            unionshape = unionshape.Union(abstest);
                                        }
                                        else {
                                            unionshape = czfeature.GetShape() as MultipolygonShape;
                                        }
                                    }
                                    catch (Exception ex7) {
                                        int lll = 0;
                                    }
                                }

                                MapItem mi = new MapItem();
                                mi.FarmId = cropZ.FarmId.Id;
                                mi.FieldId = czitem.CropZoneView.FieldId.Id;
                                mi.CropZoneId = cropZ.cz.CropZoneId.Id;
                                mi.Name = cropZ.cz.Name;
                                if (cropZ.MapData != null) {
                                    mi.MapData = cropZ.MapData;
                                    mi.MapDataType = cropZ.DataType;
                                }
                                FieldCropzonelist.Add(mi);

                                if (mrli != null && !string.IsNullOrWhiteSpace(mrli.SpatialLabelPosition)) {
                                    PointShape ps = new PointShape(mrli.SpatialLabelPosition);
                                    if (labelPositions.ContainsKey(stringid1)) {
                                        labelPositions[stringid1] = ps;
                                    }
                                    else {
                                        labelPositions.Add(stringid1, ps);
                                    }
                                }
                                if (mrli != null)
                                {
                                    if (rotationAngles.ContainsKey(stringid1))
                                    {
                                        rotationAngles[stringid1] = (double)mrli.HorizontalLabelRotation;
                                    }
                                    else
                                    {
                                        if (mrli.HorizontalLabelRotation != 0)
                                        {
                                            rotationAngles.Add(stringid1, (double)mrli.HorizontalLabelRotation);
                                        }
                                    }
                                }

                                scalingTextStyleCZ.RotationAngles = rotationAngles;
                                scalingTextStyleCZ.LabelPositions = labelPositions;
                                scalingTextStyleCZ.MapScale = Map.CurrentScale;
                            }

                            try {
                                if (unionshape.Polygons.Count > 0 && firstshape.Polygons.Count > 0) {
                                    MultipolygonShape union100 = new MultipolygonShape(unionshape.Polygons);
                                    MultipolygonShape overlappoly = union100.GetIntersection(firstfeature);
                                    MultipolygonShape firstfeaturepoly = firstshape;
                                    double overlaparea = overlappoly.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
                                    double union100area = union100.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
                                    double firstfeaturearea = firstshape.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
                                    double abs100percent = (overlaparea / union100area) * 100;
                                    double first100percent = (union100area / firstfeaturearea) * 100;
                                    if (abs100percent > 98 && abs100percent < 102 && first100percent > 98 && first100percent < 102 && displayFillColor) {
                                        isdoublecrop = true;
                                        cropzoneLayer.InternalFeatures.Clear();
                                        cropzoneLayerCZ.InternalFeatures.Clear();
                                        cropzoneLayer.InternalFeatures.Add(firstfeature.Id.ToString(), firstfeature);
                                        cropzoneLayerCZ.InternalFeatures.Add(firstfeature.Id.ToString(), firstfeature);
                                        cropzoneLayer.IsVisible = true;
                                        cropzoneLayerCZ.IsVisible = true;
                                        currentLayer.IsVisible = false;
                                        fieldsLayer.IsVisible = true;
                                        scalingTextStyle.IsActive = false;
                                    }
                                }
                            }
                            catch (Exception ex555) {
                                int yyy = 0;
                            }

                            if (cropzoneLayerCZ.InternalFeatures.Count > 0) {
                                if (fieldsLayer.InternalFeatures.Contains(selectedfieldparent)) {
                                    var geoFeature = fieldsLayer.InternalFeatures[selectedfieldparent];
                                    fieldsLayerAreaStyle.HiddenFeatures.Add(geoFeature);
                                }

                                if (cropzoneLayerCZ.InternalFeatures.Contains(selectedczparent)) {
                                    var geoFeature2 = cropzoneLayerCZ.InternalFeatures[selectedczparent];
                                    cropzonesLayerAreaStyle.SelectedFeatures.Add(geoFeature2);
                                }
                            }
                        }

                        if (!string.IsNullOrEmpty(selectedid) && cropzoneLayerCZ.InternalFeatures.Contains(selectedid) && cropzoneLayerCZ.IsVisible) {
                            var geoFeature = cropzoneLayerCZ.InternalFeatures[selectedid];
                            cropzonesLayerAreaStyle.SelectedFeatures = new List<Feature>() { geoFeature };
                        }

                        if (!string.IsNullOrEmpty(selectedid) && fieldsLayer.InternalFeatures.Contains(selectedid) && fieldsLayer.IsVisible) {
                            var geoFeature = fieldsLayer.InternalFeatures[selectedid];
                            fieldsLayerAreaStyle.SelectedFeatures = new List<Feature>() { geoFeature };
                            scalingTextStyle.SelectedFeatures = new List<Feature>() { geoFeature };
                        }

                        if (Map.Overlays.Contains("fieldOverlay")) {
                            try {
                                if (currentLayer.InternalFeatures.Count > 0) {
                                    notcenteredyet = true;
                                    Map.CurrentExtent = currentLayer.GetBoundingBox();
                                    notcenteredyet = false;
                                    Map.CenterAt(center);
                                }
                                else if (cropzoneLayer.InternalFeatures.Count > 0) {
                                    notcenteredyet = true;
                                    Map.CurrentExtent = cropzoneLayer.GetBoundingBox();
                                    notcenteredyet = false;
                                    Map.CenterAt(center);
                                }
                                SnapToGoogleImagery();
                                scalingTextStyle.MapScale = Map.CurrentScale;
                                scalingTextStyleCZ.MapScale = Map.CurrentScale;
                                scalingTextStyle2.MapScale = Map.CurrentScale;
                                customMultiStyles.MapScale = Map.CurrentScale;
                                Map.Overlays["fieldOverlay"].Refresh();
                            }
                            catch (Exception exception) {
                                log.ErrorException("Couldn't load the current item's map.", exception);
                            }
                        }
                    }
                    else {

                        if (fvitem != null) {
                            SelectedField = new Feature();
                            SelectedFeatures = new GeoCollection<Feature>();
                            Feature firstfeature = new Feature();
                            MultipolygonShape firstshape = new MultipolygonShape();
                            MultipolygonShape unionshape = new MultipolygonShape();

                            if (fvitem.FieldTreeItemVM.Children.Count > 0) {
                                foreach (object czobject in fvitem.FieldTreeItemVM.Children) {
                                    IIdentity itemIdcz = null;
                                    if (czobject is CropZoneTreeItemViewModel) {
                                        CropZoneTreeItemViewModel fieldczitem = czobject as CropZoneTreeItemViewModel;
                                        string stringid1 = fieldczitem.CropZoneId.Id.ToString();
                                        itemIdcz = fieldczitem.CropZoneId;
                                        if (!labelvalues.ContainsKey(stringid1)) {
                                            labelvalues.Add(stringid1, fieldczitem.Name);
                                        }
                                        else {
                                            labelvalues[stringid1] = fieldczitem.Name;
                                        }
                                        var czmaps = clientEndpoint.GetView<ItemMap>(itemIdcz);
                                        if (czmaps.HasValue) {
                                            ItemMapItem mri1 = czmaps.Value.MostRecentMapItem;
                                            if (mri1 == null) {
                                                continue;
                                                //return;
                                            }
                                            if (string.IsNullOrWhiteSpace(mri1.MapData)) { continue; }
                                            ItemLabelItem mrli = czmaps.Value.MostRecentLabelItem;
                                            if (mrli != null && !mrli.OptionalVisibility) {
                                                continue;
                                            }
                                            //string customarealabel1 = GetCustomCropZoneName(fieldczitem.CropZoneId.Id, fieldczitem.Name);
                                            string customareaonlylabel1 = GetCustomAreaLabel(fieldczitem.CropZoneId.Id, false);
                                            string customarealabel1 = DisplayLabelWithArea(fieldczitem.Name, customareaonlylabel1);
                                            IDictionary<string, string> columnvalues = new Dictionary<string, string>();
                                            columnvalues.Add(@"LdbLabel", fieldczitem.Name);
                                            columnvalues.Add(@"AreaLabel", customarealabel1);
                                            columnvalues.Add(@"AreaOnlyLabel", customareaonlylabel1);
                                            var czfeature = new Feature(mri1.MapData, stringid1, columnvalues);
                                            BaseShape testbase1 = czfeature.GetShape();
                                            MultipolygonShape mdataconvert1 = new MultipolygonShape();
                                            if (testbase1 is PolygonShape) {
                                                mdataconvert1.Polygons.Add(testbase1 as PolygonShape);
                                                czfeature = new Feature(mdataconvert1.GetWellKnownText(), stringid1, columnvalues);
                                            }
                                            cropzoneLayer.InternalFeatures.Add(stringid1, czfeature);
                                            cropzoneLayerCZ.InternalFeatures.Add(stringid1, czfeature);
                                            cropzoneLayer.IsVisible = true;
                                            cropzoneLayerCZ.IsVisible = true;
                                            currentLayer.IsVisible = false;
                                            fieldsLayer.IsVisible = true;
                                            scalingTextStyle.IsActive = false;

                                            if (firstshape.Polygons.Count == 0) {
                                                firstshape = czfeature.GetShape() as MultipolygonShape;
                                                firstfeature = new Feature(((AreaBaseShape)firstshape).GetWellKnownText(), stringid1, columnvalues);
                                            }

                                            try {
                                                if (unionshape.Polygons.Count > 0) {
                                                    AreaBaseShape abstest = czfeature.GetShape() as AreaBaseShape;
                                                    unionshape = unionshape.Union(abstest);
                                                }
                                                else {
                                                    unionshape = czfeature.GetShape() as MultipolygonShape;
                                                }
                                            }
                                            catch (Exception ex333) {
                                                int aaa = 0;
                                            }

                                            MapItem mi = new MapItem();
                                            mi.FarmId = ((FarmId)fvitem.FieldTreeItemVM.Parent.Id).Id;
                                            mi.FieldId = fvitem.FieldView.Id.Id;
                                            mi.CropZoneId = fieldczitem.CropZoneId.Id;
                                            mi.Name = fieldczitem.Name;
                                            if (czmaps.Value.MostRecentMapItem != null) {
                                                mi.MapData = czmaps.Value.MostRecentMapItem.MapData;
                                                mi.MapDataType = czmaps.Value.MostRecentMapItem.DataType;
                                            }
                                            FieldCropzonelist.Add(mi);

                                            if (mrli != null && !string.IsNullOrWhiteSpace(mrli.SpatialLabelPosition)) {
                                                PointShape ps = new PointShape(mrli.SpatialLabelPosition);
                                                if (labelPositions.ContainsKey(stringid1)) {
                                                    labelPositions[stringid1] = ps;
                                                }
                                                else {
                                                    labelPositions.Add(stringid1, ps);
                                                }
                                            }
                                            if (mrli != null)
                                            {
                                                if (rotationAngles.ContainsKey(stringid1))
                                                {
                                                    rotationAngles[stringid1] = (double)mrli.HorizontalLabelRotation;
                                                }
                                                else
                                                {
                                                    if (mrli.HorizontalLabelRotation != 0)
                                                    {
                                                        rotationAngles.Add(stringid1, (double)mrli.HorizontalLabelRotation);
                                                    }
                                                }
                                            }

                                            scalingTextStyleCZ.RotationAngles = rotationAngles;
                                            scalingTextStyleCZ.LabelPositions = labelPositions;
                                            scalingTextStyleCZ.MapScale = Map.CurrentScale;
                                        }
                                    }
                                }
                                try {
                                    if (unionshape.Polygons.Count > 0) {
                                        MultipolygonShape union100 = new MultipolygonShape(unionshape.Polygons);
                                        MultipolygonShape overlappoly = union100.GetIntersection(firstfeature);
                                        double overlaparea = overlappoly.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
                                        double union100area = union100.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
                                        double abs100percent = (overlaparea / union100area) * 100;
                                        if (abs100percent > 98) {
                                            isdoublecrop = true;
                                            cropzoneLayer.InternalFeatures.Clear();
                                            cropzoneLayerCZ.InternalFeatures.Clear();
                                            cropzoneLayer.InternalFeatures.Add(firstfeature.Id.ToString(), firstfeature);
                                            cropzoneLayerCZ.InternalFeatures.Add(firstfeature.Id.ToString(), firstfeature);
                                            cropzoneLayer.IsVisible = true;
                                            cropzoneLayerCZ.IsVisible = true;
                                            currentLayer.IsVisible = false;
                                            fieldsLayer.IsVisible = true;
                                            scalingTextStyle.IsActive = false;
                                        }
                                    }
                                }
                                catch (Exception ex000) {
                                    int www = 0;
                                }

                                cropzonesLayerAreaStyle.SelectedFeatures = cropzoneLayerCZ.InternalFeatures;

                                if (Map.Overlays.Contains("fieldOverlay")) {
                                    try {
                                        if (cropzoneLayerCZ.InternalFeatures.Count > 0) {
                                            notcenteredyet = true;
                                            Map.CurrentExtent = cropzoneLayerCZ.GetBoundingBox();
                                            notcenteredyet = false;
                                            PointShape center1 = cropzoneLayerCZ.GetBoundingBox().GetCenterPoint();
                                            Map.CenterAt(center1);
                                        }
                                        SnapToGoogleImagery();
                                        scalingTextStyle.MapScale = Map.CurrentScale;
                                        scalingTextStyleCZ.MapScale = Map.CurrentScale;
                                        scalingTextStyle2.MapScale = Map.CurrentScale;
                                        customMultiStyles.MapScale = Map.CurrentScale;
                                        Map.Overlays["fieldOverlay"].Refresh();
                                    }
                                    catch (Exception exception) {
                                        log.ErrorException("Couldn't load the current item's map.", exception);
                                    }

                                }

                            }
                        }

                        if (czitem != null) {
                            if (fieldsLayer.InternalFeatures.Contains(czitem.CropZoneView.FieldId.Id.ToString())) {
                                Feature geofeature = fieldsLayer.InternalFeatures[czitem.CropZoneView.FieldId.Id.ToString()];
                                BaseShape fieldshape = geofeature.GetShape();
                                MultipolygonShape mps = fieldshape as MultipolygonShape;
                                var fldfeature = new Feature(mps.GetWellKnownText(), czitem.CropZoneView.FieldId.Id.ToString());
                                cropzoneLayerDraw.InternalFeatures.Add(czitem.CropZoneView.FieldId.Id.ToString(), fldfeature);
                            }

                            var years = clientEndpoint.GetView<FieldTreeView>(new DataSourceId(ApplicationEnvironment.CurrentDataSourceId));
                            var yearItem = (years.HasValue && years.Value.CropYearList.ContainsKey(ApplicationEnvironment.CurrentCropYear)) ? years.Value.CropYearList[ApplicationEnvironment.CurrentCropYear] : new TreeViewYearItem();
                            IList<TreeViewFarmItem> farms = yearItem.Farms != null ? yearItem.Farms : new List<TreeViewFarmItem>();

                            var q = from f in yearItem.Farms
                                    from fi in f.Fields
                                    from cz in fi.CropZones
                                    let itemMap = clientEndpoint.GetView<ItemMap>(cz.CropZoneId)
                                    where fi.FieldId == czitem.CropZoneView.FieldId && itemMap.HasValue && itemMap.Value.MostRecentMapItem != null
                                    select new { f.FarmId, fi.FieldId, cz, itemMap.Value.MostRecentMapItem.MapData, itemMap.Value.MostRecentMapItem.DataType, itemMap.Value.MostRecentLabelItem };

                            foreach (var cropZ in q) {
                                IIdentity itemIdcz = null;
                                if (cropZ.cz.CropZoneId == null) { continue; }
                                string stringid1 = cropZ.cz.CropZoneId.Id.ToString();
                                if (string.IsNullOrWhiteSpace(cropZ.MapData) || cropZ.MapData == "MULTIPOLYGON EMPTY") { continue; }
                                var czfeature = new Feature(cropZ.MapData, stringid1);
                                cropzoneLayerDraw.InternalFeatures.Add(stringid1, czfeature);
                                cropzoneLayerDraw.IsVisible = allowzropzoneoutlines;
                            }
                        }

                        if (Map.Overlays.Contains("fieldOverlay")) {
                            scalingTextStyle.MapScale = Map.CurrentScale;
                            scalingTextStyleCZ.MapScale = Map.CurrentScale;
                            scalingTextStyle2.MapScale = Map.CurrentScale;
                            customMultiStyles.MapScale = Map.CurrentScale;
                            Map.Overlays["fieldOverlay"].Refresh();
                        }

                    }
                }
                else {
                    if (frmitem != null) {
                        MultipolygonShape mps2 = new MultipolygonShape();
                        selectedid = frmitem.FarmDetails.Id.Id.ToString();
                        foreach (var m in clientLayerItems) {
                            stringid = m.FieldId.ToString(); ;
                            if (shapevalues.ContainsKey(stringid)) {
                                m.MapData = shapevalues[stringid];
                            }
                            if (m.FarmId.ToString() == selectedid) {

                                if (m.MapData != null && !string.IsNullOrEmpty(m.MapData) && m.MapData != @"MULTIPOLYGON EMPTY") {
                                    if (m.FarmId.ToString() == selectedid) {
                                        var labels = clientEndpoint.GetView<ItemMap>(new FieldId(ApplicationEnvironment.CurrentDataSourceId, m.FieldId));
                                        if (labels.HasValue && labels.Value.MostRecentLabelItem != null) {
                                            ItemLabelItem mrli = labels.Value.MostRecentLabelItem;
                                            if (mrli != null && !mrli.OptionalVisibility) {
                                                continue;
                                            }
                                        }
                                        IDictionary<string, string> columnvalues = new Dictionary<string, string>();
                                        //GetCustomFieldName(m.FieldId, m.Name);
                                        columnvalues.Add(@"LdbLabel", m.Name);
                                        var testfeat = new Feature(m.MapData);
                                        BaseShape testbase = testfeat.GetShape();
                                        var f = new Feature(m.MapData, m.FieldId.ToString(), columnvalues);
                                        MultipolygonShape mdataconvert = new MultipolygonShape();
                                        if (testbase is PolygonShape) {
                                            mdataconvert.Polygons.Add(testbase as PolygonShape);
                                            f = new Feature(mdataconvert.GetWellKnownText(), m.FieldId.ToString(), columnvalues);
                                        }
                                        if (testbase is MultipolygonShape) {
                                            mdataconvert = new MultipolygonShape(m.MapData);
                                            f = new Feature(mdataconvert.GetWellKnownText(), m.FieldId.ToString(), columnvalues);
                                        }
                                        //string geojsonstring = f.GetGeoJson();
                                        //Feature geojsonfeature = Feature.CreateFeatureFromGeoJson(geojsonstring);
                                                    
                                        if (!currentLayer.InternalFeatures.Contains(m.FieldId.ToString())) {
                                            currentLayer.InternalFeatures.Add(m.FieldId.ToString(), f);
                                        }
                                    }
                                }
                            }
                        }
                        SelectedFeatures = currentLayer.InternalFeatures;
                        fieldsLayerAreaStyle.SelectedFeatures = currentLayer.InternalFeatures;
                        scalingTextStyle.SelectedFeatures = currentLayer.InternalFeatures;

                        if (Map.Overlays.Contains("fieldOverlay")) {
                            try {
                                if (currentLayer.InternalFeatures.Count > 0) {
                                    notcenteredyet = true;
                                    Map.CurrentExtent = currentLayer.GetBoundingBox();
                                    notcenteredyet = false;
                                    PointShape center1 = currentLayer.GetBoundingBox().GetCenterPoint();
                                    Map.CenterAt(center1);



                                    RectangleShape rs = currentLayer.GetBoundingBox();
                                    RectangleShape rs2 = projection.ConvertToInternalProjection(rs) as RectangleShape;
                                    PolygonShape ps = rs2.ToPolygon();
                                    string psstring = ps.GetGeoJson();
                                    DateTime datenow = DateTime.Now;
                                    DateTime datepast = datenow - new TimeSpan(90, 0, 0, 0);
                                    string datenowstring = string.Format("{0:s}.000Z", datenow);
                                    string datepaststring = string.Format("{0:s}.000Z", datepast);
                                    String[] data = new String[] { "PSScene4Band", "PSScene3Band", "PSOrthoTile", "REOrthoTile", "Sentinel2L1C", "Landsat8L1G" };
                                    var result = string.Join(",", data.Select(o => string.Concat("\"", o, "\"")));
                                    StringBuilder stringbuilder = new StringBuilder("{\"item_types\":[");
                                    stringbuilder.Append(result);
                                    stringbuilder.Append("], \"filter\":{\"type\":\"AndFilter\",\"config\":");
                                    stringbuilder.Append("[{\"type\":\"GeometryFilter\",\"field_name\":\"geometry\",\"config\":");
                                    stringbuilder.Append(psstring);
                                    stringbuilder.Append(", {\"type\":\"RangeFilter\",\"field_name\":\"sun_elevation\",\"config\":");
                                    stringbuilder.Append("{\"gte\":0,\"lte\":90}}");
                                    stringbuilder.Append(", {\"type\":\"DateRangeFilter\",\"field_name\":\"acquired\",\"config\":");
                                    stringbuilder.Append("{\"gte\":\"");
                                    stringbuilder.Append(datepaststring);
                                    stringbuilder.Append("\",\"lte\":\"");
                                    stringbuilder.Append(datenowstring);
                                    stringbuilder.Append("\"}}]}]}}");
                                    string string1 = stringbuilder.ToString();
                                    int lkjlkj = 0;
                                }
                                SnapToGoogleImagery();
                                scalingTextStyle.MapScale = Map.CurrentScale;
                                scalingTextStyleCZ.MapScale = Map.CurrentScale;
                                scalingTextStyle2.MapScale = Map.CurrentScale;
                                customMultiStyles.MapScale = Map.CurrentScale;
                                Map.Overlays["fieldOverlay"].Refresh();
                            }
                            catch (Exception exception) {
                                log.ErrorException("Couldn't load the current item's map.", exception);
                            }
                            
                        }
                    }

                    if (fvitem != null) {
                        SelectedField = new Feature();
                        SelectedFeatures = new GeoCollection<Feature>();
                        Feature firstfeature = new Feature();
                        MultipolygonShape firstshape = new MultipolygonShape();
                        MultipolygonShape unionshape = new MultipolygonShape();

                        if (fvitem.FieldTreeItemVM.Children.Count > 0) {
                            foreach (object czobject in fvitem.FieldTreeItemVM.Children) {
                                IIdentity itemIdcz = null;
                                if (czobject is CropZoneTreeItemViewModel) {
                                    CropZoneTreeItemViewModel fieldczitem = czobject as CropZoneTreeItemViewModel;
                                    string stringid1 = fieldczitem.CropZoneId.Id.ToString();
                                    itemIdcz = fieldczitem.CropZoneId;
                                    if (!labelvalues.ContainsKey(stringid1)) {
                                        labelvalues.Add(stringid1, fieldczitem.Name);
                                    }
                                    else {
                                        labelvalues[stringid1] = fieldczitem.Name;
                                    }
                                    var czmaps = clientEndpoint.GetView<ItemMap>(itemIdcz);
                                    if (czmaps.HasValue) {
                                        ItemMapItem mri1 = czmaps.Value.MostRecentMapItem;
                                        if (mri1 == null) {
                                            continue;
                                            //return;
                                        }
                                        if (string.IsNullOrWhiteSpace(mri1.MapData)) { continue; }
                                        ItemLabelItem mrli = czmaps.Value.MostRecentLabelItem;
                                        if (mrli != null && !mrli.OptionalVisibility) {
                                            continue;
                                        }
                                        //string customarealabel1 = GetCustomCropZoneName(fieldczitem.CropZoneId.Id, fieldczitem.Name);
                                        string customareaonlylabel1 = GetCustomAreaLabel(fieldczitem.CropZoneId.Id, false);
                                        string customarealabel1 = DisplayLabelWithArea(fieldczitem.Name, customareaonlylabel1);
                                        IDictionary<string, string> columnvalues = new Dictionary<string, string>();
                                        columnvalues.Add(@"LdbLabel", fieldczitem.Name);
                                        columnvalues.Add(@"AreaLabel", customarealabel1);
                                        columnvalues.Add(@"AreaOnlyLabel", customareaonlylabel1);
                                        var czfeature = new Feature(mri1.MapData, stringid1, columnvalues);
                                        BaseShape testbase1 = czfeature.GetShape();
                                        MultipolygonShape mdataconvert1 = new MultipolygonShape();
                                        if (testbase1 is PolygonShape) {
                                            mdataconvert1.Polygons.Add(testbase1 as PolygonShape);
                                            czfeature = new Feature(mdataconvert1.GetWellKnownText(), stringid1, columnvalues);
                                        }
                                        cropzoneLayer.InternalFeatures.Add(stringid1, czfeature);
                                        cropzoneLayerCZ.InternalFeatures.Add(stringid1, czfeature);
                                        cropzoneLayer.IsVisible = true;
                                        cropzoneLayerCZ.IsVisible = true;
                                        currentLayer.IsVisible = false;
                                        fieldsLayer.IsVisible = true;
                                        scalingTextStyle.IsActive = false;

                                        if (firstshape.Polygons.Count == 0) {
                                            firstshape = czfeature.GetShape() as MultipolygonShape;
                                            firstfeature = new Feature(((AreaBaseShape)firstshape).GetWellKnownText(), stringid1, columnvalues);
                                        }

                                        try {
                                            if (unionshape.Polygons.Count > 0) {
                                                AreaBaseShape abstest = czfeature.GetShape() as AreaBaseShape;
                                                unionshape = unionshape.Union(abstest);
                                            }
                                            else {
                                                unionshape = czfeature.GetShape() as MultipolygonShape;
                                            }
                                        }
                                        catch (Exception ex333) {
                                            int aaa = 0;
                                        }

                                        MapItem mi = new MapItem();
                                        mi.FarmId = ((FarmId)fvitem.FieldTreeItemVM.Parent.Id).Id;
                                        mi.FieldId = fvitem.FieldView.Id.Id;
                                        mi.CropZoneId = fieldczitem.CropZoneId.Id;
                                        mi.Name = fieldczitem.Name;
                                        if (czmaps.Value.MostRecentMapItem != null) {
                                            mi.MapData = czmaps.Value.MostRecentMapItem.MapData;
                                            mi.MapDataType = czmaps.Value.MostRecentMapItem.DataType;
                                        }
                                        FieldCropzonelist.Add(mi);

                                        if (mrli != null && !string.IsNullOrWhiteSpace(mrli.SpatialLabelPosition)) {
                                            PointShape ps = new PointShape(mrli.SpatialLabelPosition);
                                            if (labelPositions.ContainsKey(stringid1)) {
                                                labelPositions[stringid1] = ps;
                                            }
                                            else {
                                                labelPositions.Add(stringid1, ps);
                                            }
                                        }
                                        if (mrli != null)
                                        {
                                            if (rotationAngles.ContainsKey(stringid1))
                                            {
                                                rotationAngles[stringid1] = (double)mrli.HorizontalLabelRotation;
                                            }
                                            else
                                            {
                                                if (mrli.HorizontalLabelRotation != 0)
                                                {
                                                    rotationAngles.Add(stringid1, (double)mrli.HorizontalLabelRotation);
                                                }
                                            }
                                        }

                                        scalingTextStyleCZ.RotationAngles = rotationAngles;
                                        scalingTextStyleCZ.LabelPositions = labelPositions;
                                        scalingTextStyleCZ.MapScale = Map.CurrentScale;
                                    }
                                }
                            }
                            try {
                                if (unionshape.Polygons.Count > 0) {
                                    MultipolygonShape union100 = new MultipolygonShape(unionshape.Polygons);
                                    MultipolygonShape overlappoly = union100.GetIntersection(firstfeature);
                                    double overlaparea = overlappoly.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
                                    double union100area = union100.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
                                    double abs100percent = (overlaparea / union100area) * 100;
                                    if (abs100percent > 98) {
                                        isdoublecrop = true;
                                        cropzoneLayer.InternalFeatures.Clear();
                                        cropzoneLayerCZ.InternalFeatures.Clear();
                                        cropzoneLayer.InternalFeatures.Add(firstfeature.Id.ToString(), firstfeature);
                                        cropzoneLayerCZ.InternalFeatures.Add(firstfeature.Id.ToString(), firstfeature);
                                        cropzoneLayer.IsVisible = true;
                                        cropzoneLayerCZ.IsVisible = true;
                                        currentLayer.IsVisible = false;
                                        fieldsLayer.IsVisible = true;
                                        scalingTextStyle.IsActive = false;
                                    }
                                }
                            }
                            catch (Exception ex000) {
                                int www = 0;
                            }

                            cropzonesLayerAreaStyle.SelectedFeatures = cropzoneLayerCZ.InternalFeatures;

                            if (Map.Overlays.Contains("fieldOverlay")) {
                                try {
                                    if (cropzoneLayerCZ.InternalFeatures.Count > 0) {
                                        notcenteredyet = true;
                                        Map.CurrentExtent = cropzoneLayerCZ.GetBoundingBox();
                                        notcenteredyet = false;
                                        PointShape center1 = cropzoneLayerCZ.GetBoundingBox().GetCenterPoint();
                                        Map.CenterAt(center1);
                                    }
                                    SnapToGoogleImagery();
                                    scalingTextStyle.MapScale = Map.CurrentScale;
                                    scalingTextStyleCZ.MapScale = Map.CurrentScale;
                                    scalingTextStyle2.MapScale = Map.CurrentScale;
                                    customMultiStyles.MapScale = Map.CurrentScale;
                                    Map.Overlays["fieldOverlay"].Refresh();
                                }
                                catch (Exception exception) {
                                    log.ErrorException("Couldn't load the current item's map.", exception);
                                }

                            }

                        }
                    }

                    //
                    //
                    //

                    if (Map.Overlays.Contains("fieldOverlay")) {
                        scalingTextStyle.MapScale = Map.CurrentScale;
                        scalingTextStyleCZ.MapScale = Map.CurrentScale;
                        scalingTextStyle2.MapScale = Map.CurrentScale;
                        customMultiStyles.MapScale = Map.CurrentScale;
                        Map.Overlays["fieldOverlay"].Refresh();
                    }
                }
            } catch (Exception exception) {
                log.ErrorException("Couldn't load the current item's map.", exception);
            }
        }

        //private string GetCustomFieldName(Guid Id, string name) {
        //    string stringid = Id.ToString();
        //    string labelname = name;
        //    var fieldMaybe = clientEndpoint.GetView<FieldDetailsView>(new FieldId(ApplicationEnvironment.CurrentDataSourceId, Id));
        //    fieldMaybe.IfValue(field => {
        //        if (field.ReportedArea != null && !string.IsNullOrWhiteSpace(field.ReportedAreaUnit)) {
        //            labelname = DisplaySelectedAreaDecimals(name, field.ReportedArea.Value, field.ReportedAreaUnit);
        //        }
        //        else if (field.BoundaryArea != null && !string.IsNullOrWhiteSpace(field.BoundaryAreaUnit)) {
        //            labelname = DisplaySelectedAreaDecimals(name, field.BoundaryArea.Value, field.BoundaryAreaUnit);
        //        }
        //        else {
        //            labelname = name + " " + Acre.Self.GetMeasure(0);
        //        }

        //    });
        //    return labelname;
        //}

        //private string GetCustomCropZoneName(Guid Id, string name) {
        //    string stringid = Id.ToString();
        //    string labelname = name;
        //    var fieldMaybe = clientEndpoint.GetView<CropZoneDetailsView>(new CropZoneId(ApplicationEnvironment.CurrentDataSourceId, Id));
        //    fieldMaybe.IfValue(field => {
        //        if (field.ReportedArea != null && !string.IsNullOrWhiteSpace(field.ReportedAreaUnit)) {
        //            labelname = DisplaySelectedAreaDecimals(name, field.ReportedArea.Value, field.ReportedAreaUnit);
        //        }
        //        else if (field.BoundaryArea != null && !string.IsNullOrWhiteSpace(field.BoundaryAreaUnit)) {
        //            labelname = DisplaySelectedAreaDecimals(name, field.BoundaryArea.Value, field.BoundaryAreaUnit);
        //        }
        //        else {
        //            labelname = name + " " + Acre.Self.GetMeasure(0);
        //        }

        //    });
        //    return labelname;
        //}

        private string DisplaySelectedAreaDecimals(string name, double unitvalue, string unitname) {
            IUnit _unit = UnitFactory.GetUnitByName(unitname);
            string decimalstring = "N" + areadecimals.ToString("N0");
            //  + "\r\n" + 
            return name + "\r\n" + unitvalue.ToString(decimalstring);
            //return name + ": " + unitvalue.ToString(decimalstring) + " " + _unit.AbbreviatedDisplay;
        }

        private string DisplayLabelWithArea(string name, string customarealabel) {
            return name + "\r\n" + customarealabel;
        }

        private string GetCustomAreaLabel(Guid Id, bool isfieldlabel) {
            string stringid = Id.ToString();
            string labelname = string.Empty;
            if (isfieldlabel) {
                var fieldMaybe = clientEndpoint.GetView<FieldDetailsView>(new FieldId(ApplicationEnvironment.CurrentDataSourceId, Id));
                fieldMaybe.IfValue(field => {
                    labelname = string.Empty;
                    if (field.ReportedArea != null && !string.IsNullOrWhiteSpace(field.ReportedAreaUnit)) {
                        labelname = DisplaySelectedAreaOnlyDecimals(field.ReportedArea.Value, field.ReportedAreaUnit);
                    }
                    else if (field.BoundaryArea != null && !string.IsNullOrWhiteSpace(field.BoundaryAreaUnit)) {
                        labelname = DisplaySelectedAreaOnlyDecimals(field.BoundaryArea.Value, field.BoundaryAreaUnit);
                    }
                });
            }
            else {
                var fieldMaybe = clientEndpoint.GetView<CropZoneDetailsView>(new CropZoneId(ApplicationEnvironment.CurrentDataSourceId, Id));
                fieldMaybe.IfValue(field => {
                    labelname = string.Empty;
                    if (field.ReportedArea != null && !string.IsNullOrWhiteSpace(field.ReportedAreaUnit)) {
                        labelname = DisplaySelectedAreaOnlyDecimals(field.ReportedArea.Value, field.ReportedAreaUnit);
                    }
                    else if (field.BoundaryArea != null && !string.IsNullOrWhiteSpace(field.BoundaryAreaUnit)) {
                        labelname = DisplaySelectedAreaOnlyDecimals(field.BoundaryArea.Value, field.BoundaryAreaUnit);
                    }
                });
            }
            return labelname;
        }

        private string DisplaySelectedAreaOnlyDecimals(double unitvalue, string unitname) {
            IUnit _unit = UnitFactory.GetUnitByName(unitname);
            string decimalstring = "N" + areadecimals.ToString("N0");
            return unitvalue.ToString(decimalstring);
        }

        private void GetSelectedFeature() {
        }

        private void LoadBingMapsOverlay() {
            if (!startedOutOnline) { return; }
            //IMapOverlay mapoverlay = new BingMapOverlay();
            //SelectedOverlay.NewOverlay();
            IMapOverlay mapoverlay = SelectedOverlay;
            Overlay bingOverlay = mapoverlay.Overlay;
            if (bingOverlay == null) { return; }
            if (Map.Overlays.Contains("Bing")) {
                Map.Overlays.Remove("Bing");
            }
            if (Map.Overlays.Contains("Bing Online")) {
                Map.Overlays.Remove("Bing Online");
            }
            if (Map.Overlays.Contains("Roads Only")) {
                Map.Overlays.Remove("Roads Only");
            }
            if (Map.Overlays.Contains("Road Online")) {
                Map.Overlays.Remove("Road Online");
            }
            if (Map.Overlays.Contains("Google")) {
                Map.Overlays.Remove("Google");
            }
            if (Map.Overlays.Contains("Google1")) {
                Map.Overlays.Remove("Google1");
            }
            if (Map.Overlays.Contains("None")) {
                Map.Overlays.Remove("None");
            }
            //bingOverlay.IsVisible = mapoverlay.Visibility;
            //((TileOverlay)bingOverlay).TileType = TileType.SingleTile;

                //Map.OverlayDrawing += (sender, e) => {
                //    if (Map.Overlays.Contains("Google")) {
                //        int zoomLevelsNumber = ((GoogleMapLayerControl)((LayerOverlay)Map.Overlays["Google"]).Layers["GoogleMapsLayer"]).ZoomLevelsNumber;
                //        ((LayerOverlay)Map.Overlays["Google"]).Attribution = SelectedOverlay.LoadAttribution(zoomLevelsNumber);
                //    }
                //};



            Map.Overlays.Add(mapoverlay.Name, bingOverlay);
            Map.Overlays.MoveToBottom(mapoverlay.Name);
            //zoomlevelfactory = new ZoomLevelFactory(mapsettings.ZoomLevel);
            ZoomLevelSet zls = SelectedOverlay.GetZoomLevelSet();
            if (zls == null) {
                zoomlevelfactory = new ZoomLevelFactory("Google40");
                //zoomlevelfactory = new ZoomLevelFactory(mapsettings.ZoomLevel);
                Map.ZoomLevelSet = zoomlevelfactory.ZoomLevelSET;
            }
            else {
                Map.ZoomLevelSet = SelectedOverlay.GetZoomLevelSet();
            }
        }

        internal void EnsureCurrentLayerIsOpen() {
            if (currentLayer == null) {
                currentLayer = new InMemoryFeatureLayer();
            }

            if (!currentLayer.IsOpen) {
                currentLayer.Open();
            }

            if (!projection.IsOpen) {
                projection.Open();
            }
        }

        internal void EnsureCropzoneLayerIsOpen() {
            if (cropzoneLayer == null) {
                cropzoneLayer = new InMemoryFeatureLayer();
            }

            if (!cropzoneLayer.IsOpen) {
                cropzoneLayer.Open();
            }

            if (cropzoneLayerCZ == null) {
                cropzoneLayerCZ = new InMemoryFeatureLayer();
            }

            if (!cropzoneLayerCZ.IsOpen) {
                cropzoneLayerCZ.Open();
            }

            if (cropzoneLayerDraw == null) {
                cropzoneLayerDraw = new InMemoryFeatureLayer();
            }

            if (!cropzoneLayerDraw.IsOpen) {
                cropzoneLayerDraw.Open();
            }

            if (!projection.IsOpen) {
                projection.Open();
            }
        }

        internal void EnsureFieldsLayerIsOpen() {
            if (fieldsLayer == null) {
                fieldsLayer = new InMemoryFeatureLayer();
            }

            if (!fieldsLayer.IsOpen) {
                fieldsLayer.Open();
            }

            if (!projection.IsOpen) {
                projection.Open();
            }
        }

        internal void EnsureMeasureLayerIsOpen() {
            if (measureLayer == null) {
                measureLayer = new InMemoryFeatureLayer();
            }

            if (!measureLayer.IsOpen) {
                measureLayer.Open();
            }

            if (!projection.IsOpen) {
                projection.Open();
            }
        }

        internal void EnsureDimensionLayerIsOpen() {
            if (dimensionLayer == null) {
                dimensionLayer = new InMemoryFeatureLayer();
            }

            if (!dimensionLayer.IsOpen) {
                dimensionLayer.Open();
            }

            if (!projection.IsOpen) {
                projection.Open();
            }
        }

        internal void EnsureAnnotationLayerIsOpen() {
            if (annotationLayer == null) {
                annotationLayer = new InMemoryFeatureLayer();
            }

            if (!annotationLayer.IsOpen) {
                annotationLayer.Open();
            }

            if (!projection.IsOpen) {
                projection.Open();
            }
        }

        private void CreateAnnotationMemoryLayer() {
            annotationLayer = new InMemoryFeatureLayer();
            annotationLayer.FeatureSource.Open();
            FeatureSourceColumn column1 = new FeatureSourceColumn(@"Id", "string", 64);
            FeatureSourceColumn column2 = new FeatureSourceColumn(@"LdbLabel", "string", 64);
            FeatureSourceColumn column3 = new FeatureSourceColumn(@"TypeName", "string", 64);
            FeatureSourceColumn column4 = new FeatureSourceColumn(@"ShapeType", "string", 64);
            annotationLayer.Columns.Add(column1);
            annotationLayer.Columns.Add(column2);
            annotationLayer.Columns.Add(column3);
            annotationLayer.Columns.Add(column4);
            annotationLayer.FeatureSource.Close();
            annotationLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = null;
            annotationLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = null;
            annotationLayer.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = null;
            annotationLayer.ZoomLevelSet.ZoomLevel01.DefaultTextStyle = null;
            annotationLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Clear();
            AreaStyle areastyle = new AreaStyle(new GeoPen(new GeoColor(100, GeoColor.StandardColors.Transparent), 2), new GeoSolidBrush(new GeoColor(60, GeoColor.StandardColors.Transparent)));
            LineStyle linestyle = new LineStyle(new GeoPen(new GeoColor(100, GeoColor.StandardColors.Transparent), 2));
            PointStyle pointstyle = new PointStyle(PointSymbolType.Circle, new GeoSolidBrush(new GeoColor(100, GeoColor.StandardColors.Transparent)), 10);
            customMultiStyles = new MapStyles.AnnotationMultiStyle(areastyle, linestyle, pointstyle, annotationopacity);
            customMultiStyles.ColumnsList = new List<string>() { @"Id", @"LdbLabel", @"TypeName", @"ShapeType" };
            customMultiStyles.Opacity = annotationopacity;
            customMultiStyles.MaximumLabelSize = this.mapsettings.ScalingTextMaximumSize;
            customMultiStyles.MinimumLabelSize = this.mapsettings.ScalingTextMinimumSize;
            customMultiStyles.MaximumScale = 19000;
            customMultiStyles.MinimumScale = 1200;
            annotationLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customMultiStyles);
            scalingTextStyle2 = new AnnotationScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, mapsettings.ScalingTextMaximumSize, this.mapsettings.ScalingTextMinimumSize);
            scalingTextStyle2.ColumnsList = new List<string>() { @"Id", @"LdbLabel", @"TypeName", @"ShapeType" };
            //scalingTextStyle2.YOffsetInPixel = 20;
            if (mapannotationlabelsvisible) {
                annotationLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(scalingTextStyle2);
            }
            annotationLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            annotationLayer.FeatureSource.Projection = projection;
        }

        private void RebuildMapAnnotations() {
            var begin = DateTime.UtcNow;
            userstylenames = this.mapsettings.MapAnnotationCategories;
            annotationopacity = this.mapsettings.MapAnnotationOpacity;
            customMultiStyles.Opacity = annotationopacity;
            if (userstylenames.Count > 0) {
                StyleData.Self.UserStyleNames = userstylenames;
                changedstyles = new List<AnnotationStyleItem>();
                foreach (KeyValuePair<string, MapAnnotationCategory> asi in userstylenames) {
                    AnnotationStyleItem annotatonstyle = styleFactory.GetStyle(asi.Value.Name, asi.Value.ShapeType);
                    annotatonstyle.Visible = asi.Value.Visible;
                    changedstyles.Add(annotatonstyle);
                }
                StyleData.Self.CustomStyles = changedstyles;
            }
            EnsureAnnotationLayerIsOpen();
            annotationLayer.InternalFeatures.Clear();
            if (!annotationvisibility) { return; }

            annotationLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = null;
            annotationLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = null;
            annotationLayer.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = null;
            annotationLayer.ZoomLevelSet.ZoomLevel01.DefaultTextStyle = null;
            annotationLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Clear();
            AreaStyle areastyle = new AreaStyle(new GeoPen(new GeoColor(100, GeoColor.StandardColors.Transparent), 2), new GeoSolidBrush(new GeoColor(60, GeoColor.StandardColors.Transparent)));
            LineStyle linestyle = new LineStyle(new GeoPen(new GeoColor(100, GeoColor.StandardColors.Transparent), 2));
            PointStyle pointstyle = new PointStyle(PointSymbolType.Circle, new GeoSolidBrush(new GeoColor(100, GeoColor.StandardColors.Transparent)), 10);
            customMultiStyles = new MapStyles.AnnotationMultiStyle(areastyle, linestyle, pointstyle, annotationopacity);
            customMultiStyles.ColumnsList = new List<string>() { @"Id", @"LdbLabel", @"TypeName", @"ShapeType" };
            customMultiStyles.Opacity = annotationopacity;
            customMultiStyles.MaximumLabelSize = this.mapsettings.ScalingTextMaximumSize;
            customMultiStyles.MinimumLabelSize = this.mapsettings.ScalingTextMinimumSize;
            customMultiStyles.MaximumScale = 19000;
            customMultiStyles.MinimumScale = 1200;
            annotationLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customMultiStyles);
            scalingTextStyle2 = new AnnotationScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, mapsettings.ScalingTextMaximumSize, this.mapsettings.ScalingTextMinimumSize);
            scalingTextStyle2.ColumnsList = new List<string>() { @"Id", @"LdbLabel", @"TypeName", @"ShapeType" };
            //scalingTextStyle2.YOffsetInPixel = 20;
            if (mapannotationlabelsvisible) {
                annotationLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(scalingTextStyle2);
            }
            annotationLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;

            var annotations = clientEndpoint.GetView<MapAnnotationListView>(new DataSourceId(ApplicationEnvironment.CurrentDataSourceId));
            List<MapAnnotationListItem> anlist = annotations.HasValue && annotations.Value.Annotations != null ? annotations.Value.Annotations : new List<MapAnnotationListItem>();
            IList<MapAnnotationDetailView> detaillist = new List<MapAnnotationDetailView>();
            IList<AnnotationStyleItem> stylelist = new List<AnnotationStyleItem>();
            foreach (var m in anlist) {
                if (m.Name != null && !string.IsNullOrEmpty(m.Name)) {
                    try {
                        var shapeItemMaybe = clientEndpoint.GetView<MapAnnotationDetailView>(m.Id);
                        MapAnnotationId mapAnnotationId = new MapAnnotationId();
                        string annotationName = string.Empty;
                        string annotationWktData = string.Empty;
                        string annotationShapeType = string.Empty;
                        string annotationType = string.Empty;
                        int? annotationCropYear = null;
                        shapeItemMaybe.IfValue(annotation => {
                            detaillist.Add(annotation);
                            string anid = annotation.Id.Id.ToString();
                            mapAnnotationId = annotation.Id;
                            annotationName = annotation.Name;
                            annotationWktData = annotation.WktData;
                            annotationShapeType = annotation.ShapeType;
                            annotationType = annotation.AnnotationType;
                            annotationCropYear = annotation.CropYear;
                            IDictionary<string, string> columnvalues = new Dictionary<string, string>();
                            columnvalues.Add(@"Id", anid);
                            columnvalues.Add(@"LdbLabel", annotationName);
                            columnvalues.Add(@"TypeName", annotationType);
                            columnvalues.Add(@"ShapeType", annotationShapeType);
                            Feature newfeature = new Feature(annotationWktData, anid, columnvalues);
                            annotationLayer.InternalFeatures.Add(anid, newfeature);
                            var f = new Feature(annotationWktData, mapAnnotationId.Id.ToString(), columnvalues);
                            if (annotationLayer.InternalFeatures.Contains(anid)) {
                                annotationLayer.InternalFeatures.Remove(anid);
                            }
                            annotationLayer.InternalFeatures.Add(anid, newfeature);
                        });

                    }
                    catch { }
                }
            }
            var end = DateTime.UtcNow;
            log.Debug("Annotation Shapes load took " + (end - begin).TotalMilliseconds + "ms");
        }

        public void SaveCustomLabels(IList<MapItem> mapshapelist, bool isCropzone) {
            PointShape pointshapetest = new PointShape();
            if (mapshapelist != null) {
                foreach (var m in mapshapelist) {
                    if (m.MapData != null && !string.IsNullOrEmpty(m.MapData) && m.MapData != @"MULTIPOLYGON EMPTY") {
                        string itemId = m.FieldId.ToString();
                        if (isCropzone) {
                            itemId = m.CropZoneId.ToString();
                        }
                        PointShape pointshape = new PointShape();
                        string pointwkt = string.Empty;
                        double rotation = 0;
                        if (LabelPositions.ContainsKey(itemId)) {
                            pointshape = LabelPositions[itemId];
                            pointwkt = pointshape.GetWellKnownText();
                        }
                        if (RotationAngles.ContainsKey(itemId)) {
                            rotation = RotationAngles[itemId];
                        }
                        if (pointshape.X == 0 && pointshape.Y == 0 && rotation == 0) { continue; }
                        //var maps = clientEndpoint.GetView<ItemMap>(itemId);

                        bool optionalvisibility = true;
                        if (isCropzone) {
                            var czMaybe = clientEndpoint.GetView<Domain.ReadModels.Tree.CropZoneDetailsView>(new CropZoneId(ApplicationEnvironment.CurrentDataSourceId, new Guid(m.CropZoneId.ToString())));
                            czMaybe.IfValue(cropzone => {
                                //var cropzonelabelinfo = new UpdateCropZoneLabelLocation(cropzone.Id, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), pointwkt, (float)rotation, ApplicationEnvironment.CurrentCropYear);
                                var cropzonelabelinfo = new UpdateCropZoneLabelLocation(cropzone.Id, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), pointwkt, (float)rotation, ApplicationEnvironment.CurrentCropYear, optionalvisibility);
                                if (allowupdates) {
                                    clientEndpoint.SendOne(cropzonelabelinfo);
                                }
                            });
                        }
                        else {
                            var fieldMaybe = clientEndpoint.GetView<Domain.ReadModels.Tree.FieldDetailsView>(new FieldId(ApplicationEnvironment.CurrentDataSourceId, m.FieldId));
                            fieldMaybe.IfValue(field => {
                                //var fieldlabelinfo = new UpdateFieldLabelLocation(field.Id, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), pointwkt, (float)rotation, ApplicationEnvironment.CurrentCropYear);
                                var fieldlabelinfo = new UpdateFieldLabelLocation(field.Id, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), pointwkt, (float)rotation, ApplicationEnvironment.CurrentCropYear, optionalvisibility);
                                if (allowupdates) {
                                    clientEndpoint.SendOne(fieldlabelinfo);
                                }
                            });

                            var czMaybe = clientEndpoint.GetView<Domain.ReadModels.Tree.CropZoneDetailsView>(new CropZoneId(ApplicationEnvironment.CurrentDataSourceId, m.FieldId));
                            czMaybe.IfValue(cropzone => {
                                //var cropzonelabelinfo = new UpdateCropZoneLabelLocation(cropzone.Id, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), pointwkt, (float)rotation, ApplicationEnvironment.CurrentCropYear);
                                var cropzonelabelinfo = new UpdateCropZoneLabelLocation(cropzone.Id, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), pointwkt, (float)rotation, ApplicationEnvironment.CurrentCropYear, optionalvisibility);
                                if (allowupdates) {
                                    clientEndpoint.SendOne(cropzonelabelinfo);
                                }
                            });
                        }
                    }
                }
            }
        }

        internal void SaveTrackingLayer() {
            var spatialDataType = "WKT"; // TODO: Make an enum?
            var features = Map.TrackOverlay.TrackShapeLayer.InternalFeatures;
            var edfeatures = Map.EditOverlay.EditShapesLayer.InternalFeatures;
            var curfeatures = currentLayer.InternalFeatures;
            MultipolygonShape multiPolygonShape = new MultipolygonShape();
            MultipolygonShape currentPolygonShape = new MultipolygonShape();
            foreach (var cfeat in curfeatures) {
                var shape = cfeat.GetShape();
                if (shape is MultipolygonShape) {
                    currentPolygonShape = shape as MultipolygonShape;
                    break;
                }
            }
            if (currentPolygonShape.Polygons.Count > 0) {
                multiPolygonShape = currentPolygonShape;
            }

            if (Map.EditOverlay.EditShapesLayer.InternalFeatures.Count > 0) {
                multiPolygonShape = new MultipolygonShape();
                MultipolygonShape editPolygonShape = new MultipolygonShape();
                foreach (var cfeat in edfeatures) {
                    var shape = cfeat.GetShape();
                    if (shape is MultipolygonShape) {
                        MultipolygonShape editPolygonShape2 = shape as MultipolygonShape;
                        editPolygonShape = projection.ConvertToInternalProjection(editPolygonShape2) as MultipolygonShape;
                        if (editPolygonShape.Polygons.Count > 0) {
                            foreach (PolygonShape ps11 in editPolygonShape.Polygons) {
                                if (ps11.OuterRing.Vertices.Count > 3) {
                                    multiPolygonShape.Polygons.Add(ps11);
                                }
                            }
                        }
                    }
                }
            }

            foreach (var feature in features) {
                var shape = feature.GetShape();
                if (shape is AreaBaseShape) {
                    MultipolygonShape multipoly = new MultipolygonShape();
                    if (shape is MultipolygonShape) {
                        multipoly = shape as MultipolygonShape;
                    }
                    PolygonShape poly = new PolygonShape();
                    if (shape is PolygonShape) {
                        poly = shape as PolygonShape;
                    }
                    if (shape is EllipseShape) {
                        EllipseShape ellipse = shape as EllipseShape;
                        poly = ellipse.ToPolygon();
                    }
                    if (poly.OuterRing.Vertices.Count > 3) {
                        poly = projection.ConvertToInternalProjection(poly) as PolygonShape;
                        multipoly = new MultipolygonShape();
                        multipoly.Polygons.Add(poly);
                        multiPolygonShape = AddShapesAndCutOuts(multiPolygonShape, multipoly);
                        break;
                    }
                }
            }
            UpdateDataAndMap(spatialDataType, multiPolygonShape, true);
        }

        public void SaveImportShape(BaseShape iimportShape, bool shouldzoomtoshapeextent) {
            PolygonShape testpoly = new PolygonShape();
            if (iimportShape == null) { return; }
            if (iimportShape == testpoly) { return; }
            MultipolygonShape testpoly2 = new MultipolygonShape();
            if (iimportShape == testpoly2) { return; }
            if (iimportShape is PolygonShape) {
                if (((PolygonShape)iimportShape).OuterRing.Vertices.Count == 0) {
                    Map.Refresh();
                    return;
                }
            }
            if (iimportShape is MultipolygonShape) {
                if (((MultipolygonShape)iimportShape).Polygons.Count == 0) {
                    Map.Refresh();
                    return;
                }
            }

            var spatialDataType = "WKT"; // TODO: Make an enum?
            var efeatures = currentLayer.InternalFeatures;
            MultipolygonShape multiPolygonShape = new MultipolygonShape();
            MultipolygonShape currentPolygonShape = new MultipolygonShape();
            foreach (var cfeat in efeatures) {
                var shape = cfeat.GetShape();
                if (shape is MultipolygonShape) {
                    currentPolygonShape = shape as MultipolygonShape;
                    break;
                }
            }
            multiPolygonShape = currentPolygonShape;
            MultipolygonShape multipoly = new MultipolygonShape();
            if (iimportShape is MultipolygonShape) {
                multipoly = iimportShape as MultipolygonShape;
            }
            if (iimportShape is PolygonShape) {
                PolygonShape poly = iimportShape as PolygonShape;
                multipoly.Polygons.Add(poly);
            }
            MultipolygonShape multipoly2 = new MultipolygonShape();
            if (multipoly.Polygons.Count > 0) {
                multipoly2 = projection.ConvertToInternalProjection(multipoly) as MultipolygonShape;
            }

            if (multiPolygonShape.Polygons.Count > 0) {
                bool notahole = true;
                try {
                    for (int i = 0; i < multiPolygonShape.Polygons.Count; i++) {
                        foreach (PolygonShape polyx in multipoly2.Polygons) {
                            if (multiPolygonShape.Polygons[i].Contains(polyx)) {
                                notahole = false;
                            }
                        }
                    }
                }
                catch (Exception ex) {
                    System.Windows.MessageBox.Show(Strings.ProblemMergingImportShapeWithExisting_Text);
                    return;
                }


                if (notahole) {
                    try {
                        multiPolygonShape = multiPolygonShape.Union(multipoly2);
                    }
                    catch (Exception ex) {
                        System.Windows.MessageBox.Show(Strings.ProblemMergingImportShapeWithExisting_Text);
                        return;
                    }
                }
                else {
                    multiPolygonShape = AddShapesAndCutOuts(multiPolygonShape, multipoly2);
                }
            }
            else {
                multiPolygonShape = multipoly2;
            }

            UpdateDataAndMap(spatialDataType, multiPolygonShape, shouldzoomtoshapeextent);
        }

        void SavePivotShape() {
            PolygonShape testpoly = new PolygonShape();
            if (PivotShape == null) { return; }
            if (PivotShape == testpoly) { return; }
            MultipolygonShape testpoly2 = new MultipolygonShape();
            if (PivotShape == testpoly2) { return; }
            if (PivotShape is PolygonShape) {
                if (((PolygonShape)PivotShape).OuterRing.Vertices.Count == 0) {
                    Map.Refresh();
                    return;
                }
            }
            if (PivotShape is MultipolygonShape) {
                if (((MultipolygonShape)PivotShape).Polygons.Count == 0) {
                    Map.Refresh();
                    return;
                }
            }

            var spatialDataType = "WKT"; // TODO: Make an enum?
            MultipolygonShape multiPolygonShape = new MultipolygonShape();
            MultipolygonShape currentPolygonShape = new MultipolygonShape();

            try {
                var shape1 = SelectedField.GetShape();
                if (shape1 != null) {
                    if (shape1 is PolygonShape) {
                        PolygonShape poly222 = shape1 as PolygonShape;
                        currentPolygonShape.Polygons.Add(poly222);
                    }
                    if (shape1 is MultipolygonShape) {
                        currentPolygonShape = shape1 as MultipolygonShape;
                    }
                    multiPolygonShape = currentPolygonShape;
                }
            }
            catch { }

            MultipolygonShape multipoly = new MultipolygonShape();
            if (PivotShape is MultipolygonShape) {
                multipoly = PivotShape as MultipolygonShape;
            }
            if (PivotShape is PolygonShape) {
                PolygonShape poly = PivotShape as PolygonShape;
                multipoly.Polygons.Add(poly);
            }
            MultipolygonShape multipoly2 = new MultipolygonShape();
            if (multipoly.Polygons.Count > 0) {
                multipoly2 = projection.ConvertToInternalProjection(multipoly) as MultipolygonShape;
            }

            if (drawingpivot) {
                multiPolygonShape = multipoly2;
            }
            else {
                multiPolygonShape = AddShapesAndCutOuts(multiPolygonShape, multipoly2);
            }
            UpdateDataAndMap(spatialDataType, multiPolygonShape, true);
            Map.Refresh();
        }

        public void SaveReplaceShape() {
            PolygonShape testpoly = new PolygonShape();
            if (ReplaceShape == null) { return; }
            if (ReplaceShape == testpoly) { return; }
            MultipolygonShape testpoly2 = new MultipolygonShape();
            if (ReplaceShape == testpoly2) { return; }
            if (ReplaceShape is PolygonShape) {
                if (((PolygonShape)ReplaceShape).OuterRing.Vertices.Count == 0) {
                    return;
                }
            }
            if (ReplaceShape is MultipolygonShape) {
                if (((MultipolygonShape)ReplaceShape).Polygons.Count == 0) {
                    return;
                }
            }

            var spatialDataType = "WKT"; // TODO: Make an enum?
            MultipolygonShape multiPolygonShape = new MultipolygonShape();
            MultipolygonShape multipoly = new MultipolygonShape();
            if (ReplaceShape is MultipolygonShape) {
                multipoly = ReplaceShape as MultipolygonShape;
            }
            if (ReplaceShape is PolygonShape) {
                PolygonShape poly = ReplaceShape as PolygonShape;
                multipoly.Polygons.Add(poly);
            }
            MultipolygonShape multipoly2 = new MultipolygonShape();
            if (multipoly.Polygons.Count > 0) {
                //multipoly2 = projection.ConvertToInternalProjection(multipoly) as MultipolygonShape;
                multiPolygonShape = AddShapesAndCutOuts(multiPolygonShape, multipoly);
                UpdateDataAndMap(spatialDataType, multiPolygonShape, true);
            }

            //multiPolygonShape = AddShapesAndCutOuts(multiPolygonShape, multipoly);
            //UpdateDataAndMap(spatialDataType, multiPolygonShape);
        }

        public void SaveAdvancedShape() {
            //PolygonShape testpoly = new PolygonShape();
            if (ReplaceShape == null) { return; }
            //if (ReplaceShape == testpoly) { return; }

            var spatialDataType = "WKT"; // TODO: Make an enum?

            var efeatures = currentLayer.InternalFeatures;
            MultipolygonShape currentPolygonShape = new MultipolygonShape();
            foreach (var cfeat in efeatures) {
                var shape = cfeat.GetShape();
                if (shape is MultipolygonShape) {
                    currentPolygonShape = shape as MultipolygonShape;
                    break;
                }
            }


            MultipolygonShape multiPolygonShape = new MultipolygonShape();
            MultipolygonShape multipoly = new MultipolygonShape();
            if (ReplaceShape is MultipolygonShape) {
                multipoly = ReplaceShape as MultipolygonShape;
            }
            if (ReplaceShape is PolygonShape) {
                PolygonShape poly = ReplaceShape as PolygonShape;
                multipoly.Polygons.Add(poly);
            }
            MultipolygonShape multipoly2 = new MultipolygonShape();

            if (multipoly.Polygons.Count > 0) {
                multipoly2 = projection.ConvertToInternalProjection(multipoly) as MultipolygonShape;
            }

            multiPolygonShape = multipoly2;
            if (currentPolygonShape.Polygons.Count == 0 && multiPolygonShape.Polygons.Count == 0) { return; }

            UpdateDataAndMap(spatialDataType, multiPolygonShape, true);

            if (currentPolygonShape.Polygons.Count > 0 && multiPolygonShape.Polygons.Count == 0) {
                currentLayer.InternalFeatures.Clear();
                Map.Refresh();
            }
        }

        internal void ClearCurrentLayer() {
            currentLayer.InternalFeatures.Clear();
            try {
                Map.Overlays["fieldOverlay"].Refresh();
            }
            catch { }
        }
        public void DiffShape() {
            IIdentity itemId = null;
            IIdentity fielditemId = null;
            CropZoneDetailsViewModel czitem = null;
            string stringid = string.Empty;

            if (detailsViewModel is CropZoneDetailsViewModel) {
                itemId = ((CropZoneDetailsViewModel)detailsViewModel).CropZoneView.Id;
                stringid = ((CropZoneDetailsViewModel)detailsViewModel).CropZoneView.Id.Id.ToString();
                czitem = (CropZoneDetailsViewModel)detailsViewModel;
                selectedIdentity = itemId;
            } else {
                return;
            }

            string selectedfieldid = czitem.CropZoneView.FieldId.Id.ToString();
            string selectedczparent = czitem.CropZoneView.Id.Id.ToString();
            string selectedfieldparent = czitem.CropZoneView.FieldId.Id.ToString();

            fielditemId = czitem.CropZoneView.FieldId;
            MultipolygonShape fieldshape = null;
            Feature fldfeature = null;
            var maps = clientEndpoint.GetView<ItemMap>(fielditemId);
            try {
                if (maps.HasValue) {
                    ItemMapItem mri = GetMostRecentShapeByYear(maps);
                    if (mri == null) { return; }
                    if (string.IsNullOrWhiteSpace(mri.MapData)) { return; }
                    if (mri.MapData == "MULTIPOLYGON EMPTY") { return; }
                    fldfeature = new Feature(mri.MapData);
                    fieldshape = fldfeature.GetShape() as MultipolygonShape;

                }
            }
            catch (Exception ex555) {
                int yyy = 0;
            }

            if(fldfeature == null) { return; }
            if (fieldshape == null) { return; }



            MultipolygonShape unionshape = new MultipolygonShape();
            var years = clientEndpoint.GetView<FieldTreeView>(new DataSourceId(ApplicationEnvironment.CurrentDataSourceId));
            var yearItem = (years.HasValue && years.Value.CropYearList.ContainsKey(ApplicationEnvironment.CurrentCropYear)) ? years.Value.CropYearList[ApplicationEnvironment.CurrentCropYear] : new TreeViewYearItem();
            IList<TreeViewFarmItem> farms = yearItem.Farms != null ? yearItem.Farms : new List<TreeViewFarmItem>();

            var q = from f in yearItem.Farms
                    from fi in f.Fields
                    from cz in fi.CropZones
                    let itemMap = clientEndpoint.GetView<ItemMap>(cz.CropZoneId)
                    where fi.FieldId == czitem.CropZoneView.FieldId && itemMap.HasValue && itemMap.Value.MostRecentMapItem != null
                    select new { f.FarmId, fi.FieldId, cz, itemMap.Value.MostRecentMapItem.MapData, itemMap.Value.MostRecentMapItem.DataType, itemMap.Value.MostRecentLabelItem };


            foreach (var cropZ in q) {
                IIdentity itemIdcz = null;
                if (cropZ.cz.CropZoneId == null) { continue; }
                string stringid1 = cropZ.cz.CropZoneId.Id.ToString();
                itemIdcz = cropZ.cz.CropZoneId;

                if (string.IsNullOrWhiteSpace(cropZ.MapData)) { continue; }
                var czfeature = new Feature(cropZ.MapData);

                if (stringid1 != selectedczparent) {
                    try {
                        if (unionshape.Polygons.Count > 0) {
                            AreaBaseShape abstest = czfeature.GetShape() as AreaBaseShape;
                            unionshape = unionshape.Union(abstest);
                        }
                        else {
                            unionshape = czfeature.GetShape() as MultipolygonShape;
                        }
                    }
                    catch (Exception ex7) {
                        int lll = 0;
                    }
                }
            }

            var spatialDataType = "WKT"; // TODO: Make an enum?
            MultipolygonShape multiPolygonShape = AddShapesAndCutOuts(fieldshape, unionshape);
            UpdateDataAndMap(spatialDataType, multiPolygonShape, true);
        }

        void DeleteShape() {
            PointShape testpoint = new PointShape();
            if (DeleteShapePointer == null) { return; }
            if (DeleteShapePointer == testpoint) { return; }
            var spatialDataType = "WKT"; // TODO: Make an enum?
            var efeatures = currentLayer.InternalFeatures;
            MultipolygonShape currentPolygonShape = new MultipolygonShape();
            foreach (var cfeat in efeatures) {
                var shape = cfeat.GetShape();
                if (shape is MultipolygonShape) {
                    currentPolygonShape = shape as MultipolygonShape;
                    break;
                }
            }
            MultipolygonShape multiPolygonShape = currentPolygonShape;
            PointShape thispoint = projection.ConvertToInternalProjection(DeleteShapePointer) as PointShape;
            try {
                //for (int i = 0; i < multiPolygonShape.Polygons.Count; i++) {
                //    if (multiPolygonShape.Polygons[i].Contains(thispoint)) {

                //        System.Windows.MessageBoxResult result = System.Windows.MessageBox.Show("Are your sure you want to delete this shape?", "Confirm Delete Shape", System.Windows.MessageBoxButton.OKCancel, System.Windows.MessageBoxImage.Information, System.Windows.MessageBoxResult.Cancel, (System.Windows.MessageBoxOptions)0);
                //        if (result == System.Windows.MessageBoxResult.Cancel) { return; }

                //        multiPolygonShape.Polygons.RemoveAt(i);
                //        break;
                //    }
                //}

                bool foundashape = false;
                try {
                    for (int i = 0; i < multiPolygonShape.Polygons.Count; i++) {
                        if (foundashape) { break; }
                        for (int j = 0; j < multiPolygonShape.Polygons[i].InnerRings.Count; j++) {
                            if (foundashape) {
                                break;
                            }
                            if (multiPolygonShape.Polygons[i].InnerRings[j].Contains(thispoint)) {
                                System.Windows.MessageBoxResult result = System.Windows.MessageBox.Show(Strings.AreYouSureYouWantToDeleteShape_Text, Strings.ConfirmDeleteShape_Text, System.Windows.MessageBoxButton.OKCancel, System.Windows.MessageBoxImage.Information, System.Windows.MessageBoxResult.Cancel, (System.Windows.MessageBoxOptions)0);
                                if (result == System.Windows.MessageBoxResult.Cancel) { return; }

                                multiPolygonShape.Polygons[i].InnerRings.RemoveAt(j);
                                foundashape = true;
                                break;
                            }
                        }
                        if (foundashape) {
                            break;
                        }
                        if (multiPolygonShape.Polygons[i].OuterRing.Contains(thispoint)) {
                            System.Windows.MessageBoxResult result = System.Windows.MessageBox.Show(Strings.AreYouSureYouWantToDeleteShape_Text, Strings.ConfirmDeleteShape_Text, System.Windows.MessageBoxButton.OKCancel, System.Windows.MessageBoxImage.Information, System.Windows.MessageBoxResult.Cancel, (System.Windows.MessageBoxOptions)0);
                            if (result == System.Windows.MessageBoxResult.Cancel) { return; }

                            multiPolygonShape.Polygons.RemoveAt(i);
                            foundashape = true;
                            break;
                        }
                    }
                }
                catch (Exception ex) {
                    log.ErrorException("There was a problem deleting a shape.", ex);
                }

                for (int j = multiPolygonShape.Polygons.Count - 1; j >= 0; j--) {
                    try {
                        PolygonShape poly1 = multiPolygonShape.Polygons[j];
                        for (int k = multiPolygonShape.Polygons[j].InnerRings.Count - 1; k >= 0; k--) {
                            try {
                                RingShape poly2 = multiPolygonShape.Polygons[j].InnerRings[k];
                                double polyarea2 = poly2.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
                                if (polyarea2 < .0001) {
                                    multiPolygonShape.Polygons[j].InnerRings.RemoveAt(k);
                                }
                            }
                            catch { }
                        }
                        double polyarea = poly1.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
                        if (polyarea < .0001) {
                            multiPolygonShape.Polygons.RemoveAt(j);
                        }
                    }
                    catch { }
                }
                UpdateDataAndMap(spatialDataType, multiPolygonShape, true);
            }
            catch (Exception ex) {
                log.Info("MapDisplay - Selected Feature: " + selectedfeature.ColumnValues[@"LdbLabel"] + "  ID: " + selectedfeature.Id.ToString());
                log.ErrorException("There was a problem deleting a shape.", ex);
                System.Windows.MessageBox.Show(Strings.ThereWasAProblemDeletingShape_Text);
            }

        }

        private void UpdateDataAndMap(string spatialDataType, MultipolygonShape multiPolygonShape, bool shouldzoomtoshapeextent) {
            parent.ShowDeleteCommand = false;
            updateddisplayarea = string.Empty;
            string spatialData = string.Empty;
            if (multiPolygonShape == null) {
                ResetAndRefreshLayers();
                return; 
            }

            for (int y = multiPolygonShape.Polygons.Count - 1; y >= 0; y--) {
                try {
                    PolygonShape poly1 = multiPolygonShape.Polygons[y];
                    for (int z = multiPolygonShape.Polygons[y].InnerRings.Count - 1; z >= 0; z--) {
                        try {
                            RingShape poly2 = multiPolygonShape.Polygons[y].InnerRings[z];
                            double polyarea2 = poly2.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
                            if (polyarea2 < .0001) {
                                multiPolygonShape.Polygons[y].InnerRings.RemoveAt(z);
                            }
                        }
                        catch { }
                    }
                    double polyarea3 = poly1.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
                    if (polyarea3 < .0001) {
                        multiPolygonShape.Polygons.RemoveAt(y);
                    }
                }
                catch { }
            }

            if (multiPolygonShape.Polygons.Count > 0) {
                //MultipolygonShape mps = multiPolygonShape.Buffer(0, Map.MapUnit, MapUnitFactory.AreaDistanceMeasureConversion(Map.MapUnit));
                //spatialData = mps.GetWellKnownText();
                spatialData = multiPolygonShape.GetWellKnownText();
                spatialData = spatialData.Replace(@"\n", "");
                spatialData = spatialData.Replace(@"\r", "");
                spatialData = spatialData.Replace(@"\t", "");
            }

            bool thiswasacropzoneupdate = false;
            double area = 0;
            string cenlatlon = string.Empty;
            if (multiPolygonShape.Polygons.Count > 0) {
                try {
                    area = multiPolygonShape.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
                    PointShape centerpoint = multiPolygonShape.GetCenterPoint();
                    Vertex geoVertex1 = new Vertex(centerpoint.X, centerpoint.Y);
                    string _DecimalDegreeString = Math.Round(geoVertex1.Y, 5) + ", " + Math.Round(geoVertex1.X, 5);
                    string longlat = DecimalDegrees.d_To_DMs(geoVertex1.X, 2, DecimalDegrees.Type.Longitude) + "  " + DecimalDegrees.d_To_DMs(geoVertex1.Y, 2, DecimalDegrees.Type.Latitude);
                    string longlatAG = DecimalDegrees.d_To_Dm(geoVertex1.X, 4, DecimalDegrees.Type.Longitude) + "  " + DecimalDegrees.d_To_Dm(geoVertex1.Y, 4, DecimalDegrees.Type.Latitude);
                    cenlatlon = _DecimalDegreeString;
                    //char[] delimiterChars = { ',' };
                    //string[] words = cenlatlon.Split(delimiterChars);

                }
                catch {
                    area = 0;
                }
            }

            if (detailsViewModel is FieldDetailsViewModel) {
                var fieldVm = detailsViewModel as FieldDetailsViewModel;
                log.Info("MapDisplay - Save field polygon " + fieldVm.Name + "  " + fieldVm.FieldView.Id.ToString());
                var saveBoundaryCommand = new ChangeFieldBoundary(fieldVm.FieldView.Id, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), spatialData, spatialDataType, area, areaUnitString, ApplicationEnvironment.CurrentCropYear);
                if (allowupdates) {
                    clientEndpoint.SendOne(saveBoundaryCommand);
                }
                if (displayareainfieldlabel) {
                    if (fieldVm.AreaInfo.ReportedAreaValue == null || (fieldVm.AreaInfo.ReportedAreaValue.HasValue && fieldVm.AreaInfo.ReportedAreaValue.Value == 0)) {
                        UpdatedDisplayArea = DisplaySelectedAreaDecimals(fieldVm.Name, area, areaUnitString);
                        this.parent.Name = UpdatedDisplayArea;
                    }
                }
                fieldVm.FieldLocation.CenterLatLong = cenlatlon;
                string statename = fieldVm.FieldLocation.State != null ? fieldVm.FieldLocation.State.Name : null;
                string countyname = fieldVm.FieldLocation.County != null ? fieldVm.FieldLocation.County.Name : null;
                LandOwner landowner = fieldVm.FieldLocation.LandOwner != null ? fieldVm.FieldLocation.LandOwner : null;
                if (cenlatlon != string.Empty) {
                    var saveUpdateFieldLocationCommand = new UpdateFieldLocation(fieldVm.FieldView.Id, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), cenlatlon, statename, countyname, landowner);
                    if (allowupdates) {
                        clientEndpoint.SendOne(saveUpdateFieldLocationCommand);
                    }
                }
                string stringid = fieldVm.FieldView.Id.Id.ToString();
                if (string.IsNullOrEmpty(selectedid)) {
                    selectedid = stringid;
                }
                if (shapevalues.ContainsKey(stringid)) {
                    shapevalues[stringid] = spatialData;
                }
                else {
                    shapevalues.Add(stringid, spatialData);
                }
                if (labelvalues.ContainsKey(stringid)) {
                    labelvalues[stringid] = ((FieldDetailsViewModel)fieldVm).Name;
                }
                else {
                    labelvalues.Add(stringid, ((FieldDetailsViewModel)fieldVm).Name);
                }
            }
            else if (detailsViewModel is CropZoneDetailsViewModel) {
                var czVm = detailsViewModel as CropZoneDetailsViewModel;
                log.Info("MapDisplay - Save cropzone polygon " + czVm.Name + "  " + czVm.CropZoneView.Id.ToString());
                var saveBoundaryCommand = new ChangeCropZoneBoundary(czVm.CropZoneView.Id, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), spatialData, spatialDataType, area, areaUnitString, ApplicationEnvironment.CurrentCropYear);
                clientEndpoint.SendOne(saveBoundaryCommand);
                if (displayareainfieldlabel) {
                    if (czVm.AreaInfo.ReportedAreaValue == null || (czVm.AreaInfo.ReportedAreaValue.HasValue && czVm.AreaInfo.ReportedAreaValue.Value == 0)) {
                        UpdatedDisplayArea = DisplaySelectedAreaDecimals(czVm.Name, area, areaUnitString);
                        this.parent.Name = UpdatedDisplayArea;
                    }
                }
                if (cenlatlon != string.Empty) {
                    var saveUpdateCropZoneLocationCommand = new UpdateCropZoneLocation(czVm.CropZoneView.Id, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), cenlatlon);
                    clientEndpoint.SendOne(saveUpdateCropZoneLocationCommand);
                }
                string stringid = czVm.CropZoneView.Id.Id.ToString();
                if (string.IsNullOrEmpty(selectedid)) {
                    selectedid = czVm.CropZoneView.FieldId.Id.ToString();
                }
                if (shapevalues.ContainsKey(stringid)) {
                    shapevalues[stringid] = spatialData;
                }
                else {
                    shapevalues.Add(stringid, spatialData);
                }
                if (labelvalues.ContainsKey(stringid)) {
                    labelvalues[stringid] = ((CropZoneDetailsViewModel)czVm).Name;
                }
                else {
                    labelvalues.Add(stringid, ((CropZoneDetailsViewModel)czVm).Name);
                }
            }

            currentLayer.InternalFeatures.Clear();
            if (multiPolygonShape.Polygons.Count > 0) {
                currentLayer.InternalFeatures.Add(new Feature(multiPolygonShape));
                if (detailsViewModel is FieldDetailsViewModel) {
                    try {
                        if (!string.IsNullOrEmpty(selectedid)) {
                            var fieldVm = detailsViewModel as FieldDetailsViewModel;
                            if (fieldsLayer.InternalFeatures.Contains(selectedid)) {
                                Feature geofeature = fieldsLayer.InternalFeatures[selectedid];
                                fieldsLayer.InternalFeatures.Remove(selectedid);
                                IDictionary<string, string> columnvalues = new Dictionary<string, string>();
                                string labelname = ((FieldDetailsViewModel)detailsViewModel).Name;
                                //string customarealabel = GetCustomFieldName(new Guid(selectedid), labelname);
                                string customareaonlylabel = GetCustomAreaLabel(new Guid(selectedid), true);
                                string customarealabel = DisplayLabelWithArea(labelname, customareaonlylabel);
                                if (!fieldVm.AreaInfo.ReportedAreaValue.HasValue || fieldVm.AreaInfo.ReportedAreaValue.Value == 0) {
                                    customarealabel = DisplaySelectedAreaDecimals(labelvalues[selectedid], area, areaUnitString);
                                }
                                columnvalues.Add(@"LdbLabel", labelvalues[selectedid]);
                                columnvalues.Add(@"AreaLabel", customarealabel);
                                columnvalues.Add(@"AreaOnlyLabel", customareaonlylabel);
                                var f = new Feature(multiPolygonShape.GetWellKnownText(), selectedid, columnvalues);
                                fieldsLayer.InternalFeatures.Add(selectedid, f);
                                currentLayer.InternalFeatures.Clear();
                                currentLayer.InternalFeatures.Add(selectedid, f);
                                selectedfield = f;
                                selectedfeature = f;
                            }
                            else {
                                IDictionary<string, string> columnvalues = new Dictionary<string, string>();
                                //string customarealabel = GetCustomFieldName(new Guid(selectedid), labelvalues[selectedid]);
                                string customareaonlylabel = GetCustomAreaLabel(new Guid(selectedid), false);
                                string customarealabel = DisplayLabelWithArea(labelvalues[selectedid], customareaonlylabel);
                                if (!fieldVm.AreaInfo.ReportedAreaValue.HasValue || fieldVm.AreaInfo.ReportedAreaValue.Value == 0) {
                                    customarealabel = DisplaySelectedAreaDecimals(labelvalues[selectedid], area, areaUnitString);
                                }
                                columnvalues.Add(@"LdbLabel", labelvalues[selectedid]);
                                columnvalues.Add(@"AreaLabel", customarealabel);
                                columnvalues.Add(@"AreaOnlyLabel", customareaonlylabel);
                                var f = new Feature(multiPolygonShape.GetWellKnownText(), selectedid, columnvalues);
                                fieldsLayer.InternalFeatures.Add(selectedid, f);
                                currentLayer.InternalFeatures.Clear();
                                currentLayer.InternalFeatures.Add(selectedid, f);
                                selectedfield = f;
                                selectedfeature = f;
                            }
                        }
                        else {
                            fieldsLayer.InternalFeatures.Add(new Feature(multiPolygonShape));
                        }
                    }
                    catch (Exception exfd) {
                        IDictionary<string, string> columnvalues = new Dictionary<string, string>();
                        if (fieldsLayer.InternalFeatures.Contains(selectedid)) {
                            fieldsLayer.InternalFeatures.Remove(selectedid);
                        }
                        //string customarealabel = GetCustomFieldName(new Guid(selectedid), labelvalues[selectedid]);
                        string customareaonlylabel = GetCustomAreaLabel(new Guid(selectedid), true);
                        string customarealabel = DisplayLabelWithArea(labelvalues[selectedid], customareaonlylabel);
                        columnvalues.Add(@"LdbLabel", labelvalues[selectedid]);
                        columnvalues.Add(@"AreaLabel", customarealabel);
                        columnvalues.Add(@"AreaOnlyLabel", customareaonlylabel);
                        var f = new Feature(multiPolygonShape.GetWellKnownText(), selectedid, columnvalues);
                        fieldsLayer.InternalFeatures.Add(selectedid, f);
                        currentLayer.InternalFeatures.Clear();
                        currentLayer.InternalFeatures.Add(selectedid, f);
                        selectedfield = f;
                        selectedfeature = f;
                    }
                }
                else if (detailsViewModel is CropZoneDetailsViewModel) {
                    var czVm = detailsViewModel as CropZoneDetailsViewModel;
                    string selectedczid = czVm.CropZoneView.Id.Id.ToString();
                    selectedfeature = new Feature(multiPolygonShape.GetWellKnownText(), selectedczid);
                    try {
                        if (!string.IsNullOrEmpty(selectedid)) {
                            if (cropzoneLayer.InternalFeatures.Contains(selectedczid)) {
                                Feature geofeature = cropzoneLayer.InternalFeatures[selectedczid];
                                cropzoneLayer.InternalFeatures.Remove(selectedczid);
                                IDictionary<string, string> columnvalues = new Dictionary<string, string>();
                                string labelname = ((CropZoneDetailsViewModel)detailsViewModel).Name;
                                //string customarealabel = GetCustomCropZoneName(new Guid(selectedczid), labelname);
                                string customareaonlylabel = GetCustomAreaLabel(new Guid(selectedczid), false);
                                string customarealabel = DisplayLabelWithArea(labelvalues[selectedczid], customareaonlylabel);
                                if (!czVm.AreaInfo.ReportedAreaValue.HasValue || czVm.AreaInfo.ReportedAreaValue.Value == 0) {
                                    customarealabel = DisplaySelectedAreaDecimals(labelvalues[selectedczid], area, areaUnitString);
                                }
                                columnvalues.Add(@"LdbLabel", labelvalues[selectedczid]);
                                columnvalues.Add(@"AreaLabel", customarealabel);
                                columnvalues.Add(@"AreaOnlyLabel", customareaonlylabel);
                                var f = new Feature(multiPolygonShape.GetWellKnownText(), selectedczid, columnvalues);
                                cropzoneLayer.InternalFeatures.Add(selectedczid, f);
                                currentLayer.InternalFeatures.Clear();
                                currentLayer.InternalFeatures.Add(selectedczid, f);
                                selectedfeature = f;
                                if (cropzoneLayerCZ.InternalFeatures.Contains(selectedczid)) {
                                    cropzoneLayerCZ.InternalFeatures.Remove(selectedczid);
                                    cropzoneLayerCZ.InternalFeatures.Add(selectedczid, f);
                                }
                                else {
                                    cropzoneLayerCZ.InternalFeatures.Add(selectedczid, f);
                                }
                                if (cropzoneLayerDraw.InternalFeatures.Contains(selectedczid)) {
                                    cropzoneLayerDraw.InternalFeatures.Remove(selectedczid);
                                    cropzoneLayerDraw.InternalFeatures.Add(selectedczid, f);
                                }
                                else {
                                    cropzoneLayerDraw.InternalFeatures.Add(selectedczid, f);
                                }
                                var geoFeature = cropzoneLayerCZ.InternalFeatures[selectedczid];
                                cropzonesLayerAreaStyle.SelectedFeatures = new List<Feature>() { geoFeature };
                                cropzoneLayer.IsVisible = false;
                                cropzoneLayerCZ.IsVisible = true;
                                currentLayer.IsVisible = true;
                                fieldsLayer.IsVisible = true;
                                scalingTextStyle.IsActive = false;
                                thiswasacropzoneupdate = true;
                            }
                            else {
                                IDictionary<string, string> columnvalues = new Dictionary<string, string>();
                                //string customarealabel = GetCustomCropZoneName(new Guid(selectedczid), labelvalues[selectedczid]);
                                string customareaonlylabel = GetCustomAreaLabel(new Guid(selectedczid), false);
                                string customarealabel = DisplayLabelWithArea(labelvalues[selectedczid], customareaonlylabel);
                                if (!czVm.AreaInfo.ReportedAreaValue.HasValue || czVm.AreaInfo.ReportedAreaValue.Value == 0) {
                                    customarealabel = DisplaySelectedAreaDecimals(labelvalues[selectedczid], area, areaUnitString);
                                }
                                columnvalues.Add(@"LdbLabel", labelvalues[selectedczid]);
                                columnvalues.Add(@"AreaLabel", customarealabel);
                                columnvalues.Add(@"AreaOnlyLabel", customareaonlylabel);
                                var f = new Feature(multiPolygonShape.GetWellKnownText(), selectedczid, columnvalues);
                                cropzoneLayer.InternalFeatures.Add(selectedczid, f);
                                currentLayer.InternalFeatures.Clear();
                                currentLayer.InternalFeatures.Add(selectedczid, f);
                                selectedfeature = f;
                                if (cropzoneLayerCZ.InternalFeatures.Contains(selectedczid)) {
                                    cropzoneLayerCZ.InternalFeatures.Remove(selectedczid);
                                    cropzoneLayerCZ.InternalFeatures.Add(selectedczid, f);
                                }
                                else {
                                    cropzoneLayerCZ.InternalFeatures.Add(selectedczid, f);
                                }
                                if (cropzoneLayerDraw.InternalFeatures.Contains(selectedczid)) {
                                    cropzoneLayerDraw.InternalFeatures.Remove(selectedczid);
                                    cropzoneLayerDraw.InternalFeatures.Add(selectedczid, f);
                                }
                                else {
                                    cropzoneLayerDraw.InternalFeatures.Add(selectedczid, f);
                                }
                                var geoFeature = cropzoneLayerCZ.InternalFeatures[selectedid];
                                cropzonesLayerAreaStyle.SelectedFeatures = new List<Feature>() { geoFeature };
                                cropzoneLayer.IsVisible = false;
                                cropzoneLayerCZ.IsVisible = true;
                                currentLayer.IsVisible = true;
                                fieldsLayer.IsVisible = true;
                                scalingTextStyle.IsActive = false;
                                thiswasacropzoneupdate = true;
                            }
                        }
                        else {
                            cropzoneLayer.InternalFeatures.Add(new Feature(multiPolygonShape));
                            cropzoneLayerCZ.InternalFeatures.Add(new Feature(multiPolygonShape));
                        }
                    }
                    catch (Exception excz) {
                        IDictionary<string, string> columnvalues = new Dictionary<string, string>();
                        if (cropzoneLayer.InternalFeatures.Contains(selectedczid)) {
                            cropzoneLayer.InternalFeatures.Remove(selectedczid);
                        }
                        if (cropzoneLayerCZ.InternalFeatures.Contains(selectedczid)) {
                            cropzoneLayerCZ.InternalFeatures.Remove(selectedczid);
                        }
                        //string customarealabel = GetCustomCropZoneName(new Guid(selectedczid), labelvalues[selectedczid]);
                        string customareaonlylabel = GetCustomAreaLabel(new Guid(selectedczid), false);
                        string customarealabel = DisplayLabelWithArea(labelvalues[selectedczid], customareaonlylabel);
                        if (!czVm.AreaInfo.ReportedAreaValue.HasValue || czVm.AreaInfo.ReportedAreaValue.Value == 0) {
                            customarealabel = DisplaySelectedAreaDecimals(labelvalues[selectedczid], area, areaUnitString);
                        }
                        columnvalues.Add(@"LdbLabel", labelvalues[selectedczid]);
                        columnvalues.Add(@"AreaLabel", customarealabel);
                        columnvalues.Add(@"AreaOnlyLabel", customareaonlylabel);
                        var f = new Feature(multiPolygonShape.GetWellKnownText(), selectedczid, columnvalues);
                        cropzoneLayer.InternalFeatures.Add(selectedczid, f);
                        cropzoneLayerCZ.InternalFeatures.Add(selectedczid, f);
                        currentLayer.InternalFeatures.Clear();
                        currentLayer.InternalFeatures.Add(selectedczid, f);
                        selectedfeature = f;
                        cropzoneLayer.IsVisible = false;
                        cropzoneLayerCZ.IsVisible = true;
                        currentLayer.IsVisible = true;
                        fieldsLayer.IsVisible = true;
                        scalingTextStyle.IsActive = false;
                        thiswasacropzoneupdate = true;
                    }
                }
            }
            ResetAndRefreshLayers();
            //if (thiswasacropzoneupdate) {
            //    Map.Refresh();
            //}
        }

        internal void CancelDrawingAndEditing() {
            if (Map.TrackOverlay.TrackMode == TrackMode.Polygon) {
                Map.TrackOverlay.TrackMode = TrackMode.None;
            }
            ResetAndRefreshLayers();
        }

        private void ResetAndRefreshLayers() {
            //parent.ShowDeleteCommand = false;

            Map.TrackOverlay.TrackShapeLayer.Open();
            Map.TrackOverlay.TrackShapeLayer.Clear();
            Map.TrackOverlay.TrackShapeLayer.Close();
            Map.EditOverlay.EditShapesLayer.Open();
            Map.EditOverlay.EditShapesLayer.Clear();
            Map.EditOverlay.CalculateAllControlPoints();
            Map.EditOverlay.EditShapesLayer.Close();
            dimensionLayer.InternalFeatures.Clear();
            if (Map.Overlays.Contains("dimensionOverlay")) {
                Map.Overlays["dimensionOverlay"].Refresh();
            }

            try {
                scalingTextStyle.MapScale = Map.CurrentScale;
                scalingTextStyleCZ.MapScale = Map.CurrentScale;
                scalingTextStyle2.MapScale = Map.CurrentScale;
                customMultiStyles.MapScale = Map.CurrentScale;
                Map.Overlays["fieldOverlay"].Refresh();
            }
            catch { }
            try {
                Map.TrackOverlay.Refresh();
            }
            catch { }
            try {
                Map.EditOverlay.Refresh();
            }
            catch { }
            parent.HasUnsavedChanges = false;
            parent.IsMeasureLine = false;
            parent.SetButtonsAndMenus();
            Map.Cursor = defaultcursor;
        }

        private MultipolygonShape AddShapesAndCutOuts(MultipolygonShape multiPolygonShape, MultipolygonShape multipoly) {
            try {
                if (multiPolygonShape.Polygons.Count > 0 && multipoly.Polygons.Count > 0 && multiPolygonShape.Contains(multipoly)) {
                    try {
                        MultipolygonShape resultingmulti = multiPolygonShape.GetDifference(multipoly);
                        multiPolygonShape = resultingmulti;
                    }
                    catch {
                        for (int i = 0; i < multiPolygonShape.Polygons.Count; i++) {
                            foreach (PolygonShape poly in multipoly.Polygons) {
                                if (multiPolygonShape.Polygons[i].Contains(poly)) {
                                    multiPolygonShape.Polygons[i].InnerRings.Add(new RingShape(poly.OuterRing.Vertices));
                                }
                            }
                        }
                    }
                }
                else if (multiPolygonShape.Polygons.Count > 0 && multipoly.Polygons.Count > 0 && multiPolygonShape.Overlaps(multipoly)) {
                    try {
                        MultipolygonShape resultingmulti = multiPolygonShape.GetDifference(multipoly);
                        multiPolygonShape = resultingmulti;
                    }
                    catch (Exception ex) {
                        log.Info("MapDisplay - Selected Feature: " + selectedfeature.ColumnValues[@"LdbLabel"] + "  ID: " + selectedfeature.Id.ToString());
                        log.ErrorException("There was a problem cutting a hole.", ex);
                        System.Windows.MessageBox.Show(Strings.ThereWasAProblemCuttingHole_Text);
                    }
                }
                else {
                    foreach (PolygonShape poly in multipoly.Polygons) {
                        multiPolygonShape.Polygons.Add(poly);
                    }
                }

                //for (int j = multiPolygonShape.Polygons.Count - 1; j >= 0; j--) {
                //    try {
                //        PolygonShape poly1 = multiPolygonShape.Polygons[j];
                //        for (int k = multiPolygonShape.Polygons[j].InnerRings.Count - 1; k >= 0; k--) {
                //            try {
                //                RingShape poly2 = multiPolygonShape.Polygons[j].InnerRings[k];
                //                double polyarea2 = poly2.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
                //                if (polyarea2 < .0001) {
                //                    multiPolygonShape.Polygons[j].InnerRings.RemoveAt(k);
                //                }
                //            }
                //            catch { }
                //        }
                //        double polyarea = poly1.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
                //        if (polyarea < .0001) {
                //            multiPolygonShape.Polygons.RemoveAt(j);
                //        }
                //    }
                //    catch { }
                //}
            }
            catch (Exception ex) {
                //log.Info("Selected Field: " + selectedfield.ColumnValues[@"LdbLabel"]);
                log.Info("MapDisplay - Selected Feature: " + selectedfeature.ColumnValues[@"LdbLabel"] + "  ID: " + selectedfeature.Id.ToString());
                log.ErrorException("There was a problem adding a shape or cutting a hole.", ex);
                System.Windows.MessageBox.Show(Strings.ThereWasAProblemAddingShapeOrCuttingHole_Text);
                return null;

            }
            return multiPolygonShape;
        }

        internal void ExportShapeFile(ShapeFileType shapefiletype, bool exportallfeatures) {
            string filename = AddLayersBySaveFileDialog();
            //string flatfilename = Path.GetFileNameWithoutExtension(filename);
            //string directoryname = Path.GetDirectoryName(filename);
            //string jsonfilename = $"{flatfilename}.geojson";
            //string geojsonfilename = Path.Combine(directoryname, jsonfilename);
            //filename = geojsonfilename;
            if (string.IsNullOrEmpty(filename)) { return; }
            if (filename.EndsWith(".shp")) { 

                Collection<FeatureSourceColumn> layercolumns = currentLayer.GetColumns();
                List<DbfColumn> columns = new List<DbfColumn>();
                log.Info("MapDisplay - Export: build columns");
                foreach (FeatureSourceColumn fsc in layercolumns) {
                    DbfColumnType coltype = DbfColumnType.Character;
                    if (fsc.TypeName == "Interger") { coltype = DbfColumnType.IntegerInBinary; }
                    if (fsc.TypeName == "Double") { coltype = DbfColumnType.DoubleInBinary; }
                    if (fsc.TypeName == "Date") { coltype = DbfColumnType.Date; }
                    if (fsc.TypeName == "Logical") { coltype = DbfColumnType.Logical; }
                    if (fsc.TypeName == "Memo") { coltype = DbfColumnType.Memo; }
                    columns.Add(new DbfColumn(fsc.ColumnName, coltype, fsc.MaxLength, 0));
                }
                ShapeFileFeatureLayer shapeFileFeatureLayer1 = new ShapeFileFeatureLayer();
                shapeFileFeatureLayer1.RequireIndex = false;
                shapeFileFeatureLayer1.FeatureSource.Projection = projection;
                log.Info("MapDisplay - Export: create shapefile");
                if (File.Exists(filename))
                {
                    shapeFileFeatureLayer1 = new ShapeFileFeatureLayer(filename, ShapeFileReadWriteMode.ReadWrite);
                }
                else
                {
                    shapeFileFeatureLayer1 = CreateNewFeaturesShapeFile(filename, columns, shapefiletype);
                    shapeFileFeatureLayer1.RequireIndex = false;
                }

                log.Info("MapDisplay - Export: load shapes");
                shapeFileFeatureLayer1.Open();
                int nrecords = 0;
                shapeFileFeatureLayer1.EditTools.BeginTransaction();

                GeoCollection<Feature> exportfeatures = new GeoCollection<Feature>();
                if (exportallfeatures)
                {
                    exportfeatures = fieldsLayer.InternalFeatures;
                }
                else
                {
                    exportfeatures = currentLayer.InternalFeatures;
                }

                foreach (Feature feature in exportfeatures)
                {
                    nrecords++;
                    BaseShape baseshape = feature.GetShape();
                    if (baseshape == null) { continue; }
                    try
                    {
                        if (shapefiletype == ShapeFileType.Point)
                        {
                            PointShape centerpoint = baseshape.GetCenterPoint();
                            Feature newfeat = new Feature(centerpoint, feature.ColumnValues);
                            shapeFileFeatureLayer1.FeatureSource.AddFeature(newfeat);
                        }
                        else
                        {
                            Feature newfeat = new Feature(baseshape, feature.ColumnValues);
                            shapeFileFeatureLayer1.FeatureSource.AddFeature(newfeat);
                        }
                    }
                    catch { }
                }

                log.Info("MapDisplay - Export: commit records");
                shapeFileFeatureLayer1.EditTools.CommitTransaction();
                shapeFileFeatureLayer1.Close();
                return;
            }
            if (filename.EndsWith(".kml"))
            {
                SharpKml.Dom.Document document = new SharpKml.Dom.Document();
                document.Name = filename;
                document.Open = false;

                GeoCollection<Feature> exportfeatures = new GeoCollection<Feature>();
                if (exportallfeatures)
                {
                    exportfeatures = fieldsLayer.InternalFeatures;
                }
                else
                {
                    exportfeatures = currentLayer.InternalFeatures;
                }

                foreach (Feature feature in exportfeatures)
                {
                    BaseShape baseshape = feature.GetShape();
                    if (baseshape == null) { continue; }
                    try
                    {
                        if (shapefiletype == ShapeFileType.Point)
                        {
                            PointShape centerpoint = baseshape.GetCenterPoint();
                            Feature newfeat = new Feature(centerpoint, feature.ColumnValues);
                            SharpKml.Dom.Placemark placemark = TranslatePointFeatureToKML(newfeat);
                            document.AddFeature(placemark);
                        }
                        else 
                        {
                            Feature newfeat = new Feature(baseshape, feature.ColumnValues);
                            SharpKml.Dom.Placemark placemark = TranslateMultipolygonFeatureToKML(newfeat);
                            document.AddFeature(placemark);
                        }
                    }
                    catch { }
                }
                var kml = new SharpKml.Dom.Kml();
                kml.Feature = document;
                SharpKml.Engine.KmlFile kmlfile = SharpKml.Engine.KmlFile.Create(kml, false);
                using (var stream = System.IO.File.OpenWrite(filename))
                {
                    kmlfile.Save(stream);
                }
                return;
            }
            if (filename.EndsWith(".geojson"))
            {
                int nrecords = 0;
                GeoCollection<Feature> exportfeatures = new GeoCollection<Feature>();
                if (exportallfeatures)
                {
                    exportfeatures = fieldsLayer.InternalFeatures;
                }
                else
                {
                    exportfeatures = currentLayer.InternalFeatures;
                }

                StringBuilder geojsonfeaturelist = new StringBuilder();
                foreach (Feature feature in exportfeatures)
                {
                    nrecords++;
                    BaseShape baseshape = feature.GetShape();
                    if (baseshape == null) { continue; }
                    try
                    {
                        string geojsonfeature = feature.GetGeoJson();
                        if(geojsonfeaturelist.Length == 0)
                        {
                            geojsonfeaturelist = new StringBuilder(geojsonfeature);
                        }
                        else
                        {
                            geojsonfeaturelist.Append(",");
                            geojsonfeaturelist.Append(geojsonfeature);

                        }
                    }
                    catch (Exception ex) {
                        int xxxx = 0;
                    }
                }
                string ob = "{";
                string cb = "}";
                string jsoncontent = $"{ob}\"type\":\"FeatureCollection\",\"features\":[{geojsonfeaturelist}]{cb}";
                File.WriteAllText(filename, jsoncontent);
                return;
            }
        }

        public SharpKml.Dom.Placemark TranslateMultipolygonFeatureToKML(ThinkGeo.MapSuite.Core.Feature feature)
        {
            string name = feature.ColumnValues[@"LdbLabel"];
            MultipolygonShape mpoly = feature.GetShape() as MultipolygonShape;
            if(mpoly == null) { return null; }
            byte byte_Color_R = 150, byte_Color_G = 150, byte_Color_B = 150, byte_Color_A = 100; //you may get your own color by other method
            var style = new SharpKml.Dom.Style();
            style.Polygon = new SharpKml.Dom.PolygonStyle();
            style.Polygon.ColorMode = SharpKml.Dom.ColorMode.Normal;
            style.Polygon.Color = new SharpKml.Base.Color32(byte_Color_A, byte_Color_B, byte_Color_G, byte_Color_R);
            SharpKml.Dom.MultipleGeometry multiplegeometry = new SharpKml.Dom.MultipleGeometry();
            int i = 0;
            foreach (var poly in mpoly.Polygons)
            {
                i++;
                SharpKml.Dom.Polygon polygon = new SharpKml.Dom.Polygon();
                polygon.Extrude = true;
                polygon.AltitudeMode = SharpKml.Dom.AltitudeMode.ClampToGround;
                //Color Style Setting:
                SharpKml.Dom.OuterBoundary outerBoundary = new SharpKml.Dom.OuterBoundary();
                outerBoundary.LinearRing = new SharpKml.Dom.LinearRing();
                outerBoundary.LinearRing.Coordinates = new SharpKml.Dom.CoordinateCollection();
                foreach (Vertex vertex in poly.OuterRing.Vertices)
                {
                    outerBoundary.LinearRing.Coordinates.Add(new SharpKml.Base.Vector(vertex.Y, vertex.X));
                }
                polygon.OuterBoundary = outerBoundary;
                foreach (RingShape ringshape in poly.InnerRings)
                {
                    SharpKml.Dom.InnerBoundary innerboundary = new SharpKml.Dom.InnerBoundary();
                    innerboundary.LinearRing = new SharpKml.Dom.LinearRing();
                    innerboundary.LinearRing.Coordinates = new SharpKml.Dom.CoordinateCollection();
                    foreach (Vertex vertex in ringshape.Vertices)
                    {
                        innerboundary.LinearRing.Coordinates.Add(new SharpKml.Base.Vector(vertex.Y, vertex.X));
                    }
                    polygon.AddInnerBoundary(innerboundary);
                }
                multiplegeometry.AddGeometry(polygon);
            }
            SharpKml.Dom.Placemark placemark = new SharpKml.Dom.Placemark();
            placemark.Name = name;
            placemark.Geometry = multiplegeometry;
            placemark.AddStyle(style);
            return placemark;
        }

        public SharpKml.Dom.Placemark TranslatePointFeatureToKML(ThinkGeo.MapSuite.Core.Feature feature)
        {
            string name = feature.ColumnValues[@"LdbLabel"];
            PointShape thispoint = feature.GetShape() as PointShape;
            if(thispoint == null) { return null; }
            SharpKml.Dom.Point point = new SharpKml.Dom.Point();
            point.Coordinate = new SharpKml.Base.Vector(thispoint.Y, thispoint.X);
            point.AltitudeMode = SharpKml.Dom.AltitudeMode.RelativeToGround;

            var iconstyle = new SharpKml.Dom.Style();
            iconstyle.Id = "randomColorIcon";
            iconstyle.Icon = new SharpKml.Dom.IconStyle();
            iconstyle.Icon.Color = new SharpKml.Base.Color32(255, 0, 255, 0);
            iconstyle.Icon.ColorMode = SharpKml.Dom.ColorMode.Random;
            iconstyle.Icon.Icon = new SharpKml.Dom.IconStyle.IconLink(new Uri("http://maps.google.com/mapfiles/kml/pal3/icon21.png"));
            iconstyle.Icon.Scale = 1.1;

            SharpKml.Dom.Placemark placemark = new SharpKml.Dom.Placemark();
            placemark.Name = name;
            placemark.Geometry = point;
            placemark.AddStyle(iconstyle);
            return placemark;
        }

        internal string AddLayersBySaveFileDialog() {
            SaveFileDialog savefiledialog = new SaveFileDialog();
            savefiledialog.OverwritePrompt = true;
            savefiledialog.Title = Strings.ExportShapesFile_Text;
            savefiledialog.DefaultExt = "shp";
            savefiledialog.Filter = "shp files (*.shp)|*.shp|kml files (*.kml)|*.kml|geojson files (*.geojson)|*.geojson";
            bool? dialogresult = savefiledialog.ShowDialog();
            if (dialogresult.HasValue && dialogresult.Value) {
                return savefiledialog.FileName;
            }
            return string.Empty;
        }

        public ShapeFileFeatureLayer CreateNewFeaturesShapeFile(string newfilenamewithpath, List<DbfColumn> columns, ShapeFileType shapefiletype) {
            ShapeFileFeatureSource.CreateShapeFile(shapefiletype, newfilenamewithpath, columns);
            ShapeFileFeatureLayer shapeFileFeatureLayer1 = new ShapeFileFeatureLayer(newfilenamewithpath, ShapeFileReadWriteMode.ReadWrite);
            return shapeFileFeatureLayer1;
        }

        internal void SaveMeasureLayer() {
            try {
                var features = Map.TrackOverlay.TrackShapeLayer.InternalFeatures;
                foreach (Feature feat in features) {
                    var shape = feat.GetShape();
                    PointShape centerpoint = shape.GetCenterPoint();
                    BaseShape bs1 = DisplayMeasureBalloon(shape);
                    BaseShape bs2 = projection.ConvertToInternalProjection(bs1);
                    measureLayer.InternalFeatures.Add(new Feature(bs2));
                    double areaTotal = 0;
                    double perimeterTotal = 0;
                    string displayerText = string.Empty;
                    if (bs2 is PolygonShape) {
                        PolygonShape poly1 = bs2 as PolygonShape;
                        areaTotal = poly1.GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
                        perimeterTotal = poly1.GetPerimeter(GeographyUnit.DecimalDegree, mapDistanceUnit);
                        Measure perimetermeasure = MapUnitFactory.LengthConversion(mapDistanceUnit.ToString(), perimeterTotal);
                        Measure areameasure = MapUnitFactory.AreaConversion(mapAreaUnit.ToString(), areaTotal);
                        displayerText = Strings.AreaColon_Text  + areameasure.FullDisplay + "\r\n" + Strings.Perimeter_Text + perimetermeasure.FullDisplay;
                    }
                    if (bs2 is LineShape) {
                        LineShape line1 = bs2 as LineShape;
                        perimeterTotal = line1.GetLength(GeographyUnit.DecimalDegree, mapDistanceUnit);
                        Measure perimetermeasure = MapUnitFactory.LengthConversion(mapDistanceUnit.ToString(), perimeterTotal);
                        displayerText = Strings.Length_Text + perimetermeasure.FullDisplay;
                        LineShape lineshape2 = shape as LineShape;
                        for (int xyz = 0; xyz <= (lineshape2.Vertices.Count / 2); xyz++) {
                            centerpoint = new PointShape(lineshape2.Vertices[xyz]);
                        }
                    }
                    Popup popup = new Popup(centerpoint);
                    popup.Loaded += new System.Windows.RoutedEventHandler(popup_Loaded);
                    popup.MouseDoubleClick += new MouseButtonEventHandler(mouse_DoubleClick);
                    System.Windows.Controls.TextBox displayer = new System.Windows.Controls.TextBox();
                    displayer.Text = displayerText;
                    popup.Content = displayer;
                    popupOverlay.Popups.Add(popup);
                    popupOverlay.Refresh();
                }
                Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Clear();
                Map.Refresh();
            }
            catch (Exception ex) {
                log.Error("Error saving measure line - {0}", ex);
                System.Windows.MessageBox.Show(Strings.ThereWasAProblemSavingAMeasureLine_Text);
            }
        }

        void popup_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            Popup p = sender as Popup;
        }

        internal void CalculateDimensionLayer(PointShape movingpoint, PointShape movedpoint) {
            if (calculatingdimensions) { return; }
            if (ismeasureline) { return; }
            calculatingdimensions = true;
            if (Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Count > 0) {
                BaseShape baseShape1 = Map.TrackOverlay.TrackShapeLayer.InternalFeatures[0].GetShape();
                BaseShape baseShape = projection.ConvertToInternalProjection(baseShape1);
                if (baseShape is PolygonShape) {
                    PolygonShape polyShape = baseShape as PolygonShape;
                    if (polyShape.OuterRing.Vertices.Count > 1) {
                        LineShape lineShape = new LineShape();
                        foreach (Vertex vert in polyShape.OuterRing.Vertices) {
                            lineShape.Vertices.Add(vert);
                        }
                        dimensionLayer.InternalFeatures.Clear();
                        dimensionLayer.InternalFeatures.Add(new Feature(polyShape));
                        //dimensionLayer.InternalFeatures.Add(new Feature(lineShape));
                    }
                }
            }
            if (Map.EditOverlay.EditShapesLayer.InternalFeatures.Count > 0) {
                BaseShape baseShape1 = Map.EditOverlay.EditShapesLayer.InternalFeatures[0].GetShape();
                BaseShape baseShape = projection.ConvertToInternalProjection(baseShape1);
                MultipolygonShape multipoly = new MultipolygonShape();
                if (baseShape is MultipolygonShape) {
                    multipoly = baseShape as MultipolygonShape;
                }
                if (baseShape is PolygonShape) {
                    PolygonShape poly = baseShape as PolygonShape;
                    multipoly.Polygons.Add(poly);
                }
                dimensionLayer.InternalFeatures.Clear();
                dimensionLayer.InternalFeatures.Add(new Feature(multipoly));
                LineShape lineShape = new LineShape();
                if (movingpoint != null || movedpoint != null) {
                    try {
                        Vertex foundvertex = new Vertex();
                        if (movingpoint != null && movedpoint == null) {
                            try {
                                PointShape foundpoint = multipoly.GetClosestPointTo(movingpoint, Map.MapUnit);
                                if (foundpoint == null) {
                                    foundpoint = movingpoint;
                                }
                                foundvertex = new Vertex(foundpoint.X, foundpoint.Y);
                            }
                            catch (Exception ex1) {
                                int aaa = 0;
                                calculatingdimensions = false;
                                return;
                            }
                        }
                        if (movedpoint != null && movingpoint == null) {
                            try {
                                foundvertex = new Vertex(movedpoint.X, movedpoint.Y);
                            }
                            catch (Exception ex2) {
                                int bbb = 0;
                                calculatingdimensions = false;
                                return;
                            }
                        }
                        foreach (PolygonShape polyShape in multipoly.Polygons) {
                            if (polyShape.OuterRing.Vertices.Count > 2) {
                                for (int i = 0; i < polyShape.OuterRing.Vertices.Count; i++) {
                                    if (polyShape.OuterRing.Vertices[i] == foundvertex) {
                                        if (i == 0) {
                                            lineShape = new LineShape(); 
                                            lineShape.Vertices.Add(polyShape.OuterRing.Vertices[polyShape.OuterRing.Vertices.Count - 1]);
                                            lineShape.Vertices.Add(polyShape.OuterRing.Vertices[i]);
                                            lineShape.Vertices.Add(polyShape.OuterRing.Vertices[i + 1]);
                                            break;
                                        }
                                        if (i == polyShape.OuterRing.Vertices.Count - 1) {
                                            lineShape = new LineShape();
                                            lineShape.Vertices.Add(polyShape.OuterRing.Vertices[i - 1]);
                                            lineShape.Vertices.Add(polyShape.OuterRing.Vertices[i]);
                                            lineShape.Vertices.Add(polyShape.OuterRing.Vertices[0]);
                                            break;
                                        }
                                        lineShape = new LineShape();
                                        lineShape.Vertices.Add(polyShape.OuterRing.Vertices[i - 1]);
                                        lineShape.Vertices.Add(polyShape.OuterRing.Vertices[i]);
                                        lineShape.Vertices.Add(polyShape.OuterRing.Vertices[i + 1]);
                                        break;

                                    }
                                }
                            }
                        }
                        if (lineShape.Vertices.Count == 0 && savededitoverlaypoly != null && savededitoverlaypoly.Polygons.Count > 0 && savededitoverlaypoly.Polygons.Count == multipoly.Polygons.Count) {
                            for (int x = 0; x < multipoly.Polygons.Count; x++) {
                                for (int y = 0; y < multipoly.Polygons[x].OuterRing.Vertices.Count; y++) {
                                    if (multipoly.Polygons[x].OuterRing.Vertices[y] != savededitoverlaypoly.Polygons[x].OuterRing.Vertices[y]) {
                                        if (y == 0) {
                                            lineShape = new LineShape();
                                            lineShape.Vertices.Add(multipoly.Polygons[x].OuterRing.Vertices[multipoly.Polygons[x].OuterRing.Vertices.Count - 1]);
                                            lineShape.Vertices.Add(multipoly.Polygons[x].OuterRing.Vertices[0]);
                                            lineShape.Vertices.Add(multipoly.Polygons[x].OuterRing.Vertices[1]);
                                            break;
                                        }
                                        if (y == multipoly.Polygons[x].OuterRing.Vertices.Count - 1) {
                                            lineShape = new LineShape();
                                            lineShape.Vertices.Add(multipoly.Polygons[x].OuterRing.Vertices[multipoly.Polygons[x].OuterRing.Vertices.Count - 2]);
                                            lineShape.Vertices.Add(multipoly.Polygons[x].OuterRing.Vertices[multipoly.Polygons[x].OuterRing.Vertices.Count - 1]);
                                            lineShape.Vertices.Add(multipoly.Polygons[x].OuterRing.Vertices[0]);
                                            break;
                                        }
                                        lineShape = new LineShape();
                                        lineShape.Vertices.Add(multipoly.Polygons[x].OuterRing.Vertices[y - 1]);
                                        lineShape.Vertices.Add(multipoly.Polygons[x].OuterRing.Vertices[y]);
                                        lineShape.Vertices.Add(multipoly.Polygons[x].OuterRing.Vertices[y + 1]);
                                        break;

                                    }
                                }
                            }
                        }
                        dimensionLayer.InternalFeatures.Add(new Feature(lineShape));
                        savededitoverlaypoly = multipoly;
                    }
                    catch (Exception ex) {
                        int x = 0;
                        calculatingdimensions = false;
                        return;
                    }

                }
            }

            if (Map.Overlays.Contains("dimensionOverlay")) {
                Map.Overlays["dimensionOverlay"].Refresh();
            }
            calculatingdimensions = false;
        }

        void mouse_DoubleClick(object sender, MouseButtonEventArgs e) {
            Popup p = sender as Popup;
            if (popupOverlay.Popups.Contains(p)) {
                popupOverlay.Popups.Remove(p);
                popupOverlay.Refresh();
            }
        }

        private BaseShape DisplayMeasureBalloon(BaseShape shape) {
            if (shape is LineBaseShape) {
                if (shape is LineShape) {
                    LineShape lineshape = shape as LineShape;
                    var firstvert = lineshape.Vertices.First();
                    var lastvert = lineshape.Vertices.Last();
                    var allverts = from vt in lineshape.Vertices
                                   select vt;

                    PointShape point1 = new PointShape(firstvert);
                    PointShape point2 = new PointShape(lastvert);
                    if (lineshape.Vertices.Count > 2) {
                        ScreenPointF screenPoint1 = ExtentHelper.ToScreenCoordinate(Map.CurrentExtent, point1, System.Convert.ToSingle(Map.ActualWidth), System.Convert.ToSingle(Map.ActualHeight));
                        ScreenPointF screenPoint2 = ExtentHelper.ToScreenCoordinate(Map.CurrentExtent, point2, System.Convert.ToSingle(Map.ActualWidth), System.Convert.ToSingle(Map.ActualHeight));
                        double distance = Math.Sqrt(((screenPoint1.X - screenPoint2.X) * (screenPoint1.X - screenPoint2.X)) + ((screenPoint1.Y - screenPoint2.Y) * (screenPoint1.Y - screenPoint2.Y)));
                        if (distance >= -10 && distance <= 10) {
                            PolygonShape polygon = new PolygonShape();
                            Collection<Vertex> collectvertex = lineshape.Vertices;
                            collectvertex.Add(lineshape.Vertices[0]);
                            RingShape rs = new RingShape(collectvertex);
                            polygon.OuterRing = rs;
                            return polygon;
                        }
                    }
                    return lineshape;
                }
                if (shape is MultilineShape) {
                    MultilineShape mlshape = shape as MultilineShape;
                    var firstvert = mlshape.Lines.First().Vertices.First();
                    var lastvert = mlshape.Lines.Last().Vertices.Last();
                    var allverts = from ml in mlshape.Lines
                                   from vt in ml.Vertices
                                   select vt;

                    PointShape point1 = new PointShape(firstvert);
                    PointShape point2 = new PointShape(lastvert);
                    if (allverts.Count() > 2) {
                        ScreenPointF screenPoint1 = ExtentHelper.ToScreenCoordinate(Map.CurrentExtent, point1, System.Convert.ToSingle(Map.ActualWidth), System.Convert.ToSingle(Map.ActualHeight));
                        ScreenPointF screenPoint2 = ExtentHelper.ToScreenCoordinate(Map.CurrentExtent, point2, System.Convert.ToSingle(Map.ActualWidth), System.Convert.ToSingle(Map.ActualHeight));
                        double distance = Math.Sqrt(((screenPoint1.X - screenPoint2.X) * (screenPoint1.X - screenPoint2.X)) + ((screenPoint1.Y - screenPoint2.Y) * (screenPoint1.Y - screenPoint2.Y)));
                        if (distance >= -10 && distance <= 10) {
                            PolygonShape polygon = new PolygonShape();
                            polygon.OuterRing = new RingShape(allverts);
                            return polygon;
                        }
                    }
                    return mlshape;
                }
            }
            return null;
        }

        private static ItemMapItem GetMostRecentShapeByYear(Lokad.Cqrs.Maybe<ItemMap> maps_a)
        {
            ItemMapItem item_map_item = null;
            if (maps_a.HasValue && maps_a.Value.MostRecentMapItem != null && maps_a.Value.MostRecentMapItem.CropYearChangeOccurred <= Infrastructure.ApplicationEnvironment.CurrentCropYear)
            {
                item_map_item = maps_a.Value.MostRecentMapItem;
            }
            else
            {
                ItemMapItem currentyearmostrecentboundarychange = (from md in maps_a.Value.MapItems where (md.CropYearChangeOccurred <= Infrastructure.ApplicationEnvironment.CurrentCropYear) select md).LastOrDefault();
                if (currentyearmostrecentboundarychange != null && !string.IsNullOrEmpty(currentyearmostrecentboundarychange.MapData))
                {
                    item_map_item = currentyearmostrecentboundarychange;
                }
                if (item_map_item == null)
                {
                    item_map_item = maps_a.Value.MostRecentMapItem;
                }
            }
            if(checknullshapes && string.IsNullOrEmpty(item_map_item.DataType) && string.IsNullOrEmpty(item_map_item.MapData))
            {
                for (int i = maps_a.Value.MapItems.Count - 1; i >= 0; i--)
                {
                    if (maps_a.Value.MapItems[i].DataType == "WKT" && !string.IsNullOrEmpty(maps_a.Value.MapItems[i].MapData))
                    {
                        System.Windows.MessageBox.Show("A current shape was not found, retrieving an older one");
                        //System.Windows.MessageBox.Show(Strings.ProblemMergingImportShapeWithExisting_Text);
                        item_map_item = maps_a.Value.MapItems[i];
                        break;
                    }
                }
            }
            return item_map_item;
        }
    }
}

