﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using ThinkGeo.MapSuite.WpfDesktopEdition;
using ThinkGeo.MapSuite.Core;
using Landdb.Client.Infrastructure;
using AgC.UnitConversion;
using System.Collections.ObjectModel;
using NLog;
using System.Diagnostics;
using Landdb.MapStyles.Overlays;
using Landdb.Resources;

namespace Landdb.ViewModel.Fields.FieldDetails.MapModels {
    public class DrawToolModel : ViewModelBase {
        WpfMap map;
        MapViewModel parent;
        Logger log = LogManager.GetCurrentClassLogger();
        Cursor previousCursor;
        bool toggledrawandedit = false;
        Proj4Projection projection;
        AreaStyle areaStyleBrush;
        LineStyle lineStylePen;

        MapStyles.CustomDistancePolygonStyle customDistancePolygonStyleText;
        MapStyles.CustomDistanceLineStyle customDistanceLineStyleText;
        MapStyles.CustomAreaPolygonStyle customAreaPolygonStyleText;

        IClientEndpoint clientEndpoint;
        MapSettings mapsettings;
        string areaUnitString = string.Empty;
        AgC.UnitConversion.Area.AreaUnit agcAreaUnit;
        ThinkGeo.MapSuite.Core.AreaUnit mapAreaUnit;
        ThinkGeo.MapSuite.Core.DistanceUnit mapDistanceUnit;
        int areadecimals = 2;

        LineStyle defaultdrawingLineStylePen;
        AreaStyle defaultdrawingAreaStyleBrush;
        PointStyle defaultdrawingPointStyle;
        MapStyles.FastEditPolygonLineStyle fastEditPolygonLineStyle;
        bool drawingfeatures = false;
        bool editingfeatures = false;
        LayerOverlay drawingOverlay = new LayerOverlay();
        InMemoryFeatureLayer drawingLayer = new InMemoryFeatureLayer();
        PolygonShape drawingpolygonshape = new PolygonShape();
        MultipolygonShape drawingmultipolygonshape = new MultipolygonShape();
        MultipolygonShape editingmultipolygonshape = new MultipolygonShape();
        LineShape drawinglineshape = new LineShape();
        PointShape controlpoint = new PointShape();
        Collection<Vertex> drawingVertices = new Collection<Vertex>();
        bool isdrawingpolygon = false;
        bool iseditingpolygon = false;
        bool isdrawingmeasure = false;
        string polystring = string.Empty;
        MapStyles.Overlays.FastEditPolygonOverlay dragInteractiveOverlay = new MapStyles.Overlays.FastEditPolygonOverlay();

        public DrawToolModel(WpfMap map, MapViewModel parent, IClientEndpoint clientEndpoint) {
            this.map = map;
            this.parent = parent;
            this.clientEndpoint = clientEndpoint;
            this.mapsettings = clientEndpoint.GetMapSettings();
            areadecimals = mapsettings.AreaDecimals;
            areaUnitString = mapsettings.MapAreaUnit;
            agcAreaUnit = UnitFactory.GetUnitByName(areaUnitString) as AgC.UnitConversion.Area.AreaUnit;
            mapAreaUnit = MapUnitFactory.AreaMeasureConversion(agcAreaUnit);
            mapDistanceUnit = MapUnitFactory.AreaDistanceMeasureConversion(agcAreaUnit);
            projection = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetGoogleMapParametersString());
            areaStyleBrush = new AreaStyle(new GeoPen(GeoColor.FromArgb(150, GeoColor.StandardColors.Blue), 2), new GeoSolidBrush(GeoColor.FromArgb(75, GeoColor.StandardColors.Blue)));
            lineStylePen = new LineStyle(new GeoPen(GeoColor.FromArgb(150, GeoColor.StandardColors.Blue), 2));
            customDistancePolygonStyleText = new MapStyles.CustomDistancePolygonStyle(mapAreaUnit, mapDistanceUnit, new GeoFont("Arial", 12, DrawingFontStyles.Regular), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.White), new GeoSolidBrush(GeoColor.StandardColors.Red), new GeoPen(GeoColor.StandardColors.Red));
            //customDistancePolygonStyleText.YOffsetInPixel = -20;
            customDistanceLineStyleText = new MapStyles.CustomDistanceLineStyle(mapDistanceUnit, new GeoFont("Arial", 12, DrawingFontStyles.Regular), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.DarkRed, 2), new GeoPen(GeoColor.StandardColors.DarkBlue, 2));
            customDistanceLineStyleText.DisplayAngles = false;
            customDistanceLineStyleText.DisplayTotalLength = false;
            customAreaPolygonStyleText = new MapStyles.CustomAreaPolygonStyle(mapAreaUnit, new GeoFont("Arial", 24, DrawingFontStyles.Regular), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.White), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.DarkRed));
            //customAreaPolygonStyleText.YOffsetInPixel = -20;

            InitializeNewDrawingOverlay(map);
            //InitializeNewEditingOverlay(map);
            LoadCustomEditOverlay(map);

            map.MouseMove += (sender, e) => {
                try {
                    if (drawingfeatures) {
                        if (drawingVertices.Count > 0) {
                            Vertex geoVertex1 = GetMousePoint(e);
                            ProcessTrackPolygon(drawingVertices[0], geoVertex1);

                            //if (stopwatch.ElapsedMilliseconds > 5) {
                            //    Vertex geoVertex1 = GetMousePoint(e);
                            //    ProcessTrackPolygon(drawingVertices[0], geoVertex1);
                            //    Console.WriteLine("MouseMove {0} {1}", stopwatch.ElapsedMilliseconds, "ms");
                            //    stopwatch.Stop();
                            //    stopwatch.Reset();
                            //    stopwatch.Start();
                            //}
                        }
                    }
                    if (editingfeatures) {
                        if (e.LeftButton == MouseButtonState.Pressed) {
                            if (map.EditOverlay.EditShapesLayer.InternalFeatures.Count > 0) {
                                Vertex geoVertex1 = GetMousePoint(e);
                                ProcessEditPolygon(geoVertex1);
                            }
                        }
                        if (e.LeftButton == MouseButtonState.Released) {
                            isdrawingpolygon = false;
                        }
                        //end = DateTime.UtcNow;
                        //double lasttime = (end - last).TotalMilliseconds;
                        //Console.WriteLine("MapVM mouse took {0} {1}", lasttime, "ms");
                        //last = DateTime.UtcNow;
                    }
                    //if (editingfeatures) {
                    //    if (isdragmode) {
                    //        //Vertex point = new Vertex(e.WorldX, e.WorldY);
                    //        //PolygonShape polygon = new PolygonShape(wpfMap1.EditOverlay.EditShapesLayer.InternalFeatures[0].WellKnownBinary);
                    //        //PolygonShape newPolygon = polygon.CloneDeep() as PolygonShape;
                    //        //newPolygon.OuterRing.Vertices[2] = point;

                    //        //wpfMap1.EditOverlay.EditShapesLayer.InternalFeatures[0].WellKnownBinary = newPolygon.GetWellKnownBinary();
                    //        //wpfMap1.EditOverlay.ExistingControlPointsLayer.InternalFeatures[2].WellKnownBinary = new PointShape(point).GetWellKnownBinary();
                    //        //wpfMap1.Refresh(wpfMap1.EditOverlay);

                    //    }
                    //}
                }
                catch (Exception ex) {
                    //throw ex;
                }
            };

            map.MouseLeftButtonDown += (sender, e) => {
                if (drawingfeatures) {
                    Vertex geoVertex1 = GetMousePoint(e);
                    //if (!stopwatch.IsRunning) {
                    //    stopwatch.Reset();
                    //    stopwatch.Start();
                    //}
                    //else {
                    //    stopwatch.Stop();
                    //    stopwatch.Reset();
                    //    stopwatch.Start();
                    //}
                }
                //if (editingfeatures) {
                //    Vertex geoVertex1 = GetMousePoint(e);
                //    PointShape point1 = new PointShape(geoVertex1);
                //    if (!isdragmode) {
                //        Collection<Feature> selectedFeatures = editingPointLayer.QueryTools.GetFeaturesNearestTo(point1, GeographyUnit.DecimalDegree, 1, ReturningColumnsType.AllColumns);
                //        if (selectedFeatures.Count > 0) {
                //            PointShape point2 = selectedFeatures[0].GetShape() as PointShape;
                //            ScreenPointF screenPoint1 = ExtentHelper.ToScreenCoordinate(map.CurrentExtent, point1, System.Convert.ToSingle(map.ActualWidth), System.Convert.ToSingle(map.ActualHeight));
                //            ScreenPointF screenPoint2 = ExtentHelper.ToScreenCoordinate(map.CurrentExtent, point2, System.Convert.ToSingle(map.ActualWidth), System.Convert.ToSingle(map.ActualHeight));
                //            double distance = Math.Sqrt(((screenPoint1.X - screenPoint2.X) * (screenPoint1.X - screenPoint2.X)) + ((screenPoint1.Y - screenPoint2.Y) * (screenPoint1.Y - screenPoint2.Y)));
                //            if (distance >= -10 && distance <= 10) {
                //                isdragmode = true;
                //                //Feature editfeature = editingPolygonLayer
                //                //editingpolygonshape 

                //            }
                //        }
                //    }
                //}

            };

            map.MouseLeftButtonUp += (sender, e) => {
                try {
                    if (drawingfeatures) {
                        Vertex geoVertex1 = GetMousePoint(e);
                        drawingVertices.Add(geoVertex1);
                        ProcessTrackPolygon(drawingVertices[0], geoVertex1);
                    }
                    if (editingfeatures && !string.IsNullOrEmpty(polystring)) {
                        map.EditOverlay.EditShapesLayer.InternalFeatures.Clear();
                        map.EditOverlay.EditShapesLayer.InternalFeatures.Add(new Feature(polystring));
                        map.EditOverlay.CalculateAllControlPoints();
                        map.EditOverlay.Refresh();
                        polystring = string.Empty;
                    }
                }
                catch (Exception ex) {
                    //throw ex;
                }
            };

            map.MouseDoubleClick += (sender, e) => {
                if (drawingfeatures) {
                    map.Cursor = previousCursor;
                    //stopwatch.Stop();
                    //stopwatch.Reset();
                    DrawingPolygonEnded();
                    drawingfeatures = false;
                    //map.ExtentOverlay.DoubleLeftClickMode = MapDoubleLeftClickMode.Default;
                }
            };

        }

        //private void InitializeNewDrawingOverlay(WpfMap map) {
        //    drawingfeatures = false;
        //    drawingmeasure = false;
        //    drawingOverlay = new LayerOverlay();
        //    drawingOverlay.DrawingExceptionMode = DrawingExceptionMode.DrawException;
        //    drawingOverlay.TileType = TileType.SingleTile;
        //    drawingLayer = new InMemoryFeatureLayer();
        //    drawingpolygonshape = new PolygonShape();
        //    drawinglineshape = new LineShape();
        //    drawingVertices = new Collection<Vertex>();
        //    //stopwatch = new Stopwatch();
        //    defaultdrawingLineStylePen = new LineStyle(new GeoPen(GeoColor.FromArgb(150, GeoColor.StandardColors.Blue), 2));
        //    defaultdrawingAreaStyleBrush = new AreaStyle(new GeoPen(GeoColor.FromArgb(255, GeoColor.StandardColors.Blue), 2), new GeoSolidBrush(GeoColor.FromArgb(50, GeoColor.StandardColors.Blue)));
        //    defaultdrawingPointStyle = new PointStyle(PointSymbolType.Circle, new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.Blue), 10);
        //    drawingLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = defaultdrawingAreaStyleBrush;
        //    drawingLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = defaultdrawingLineStylePen;
        //    drawingLayer.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = defaultdrawingPointStyle;
        //    drawingLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
        //    drawingLayer.FeatureSource.Projection = projection;
        //    drawingLayer.IsVisible = true;
        //    drawingOverlay.Layers.Add("drawingLayer", drawingLayer);
        //    map.Overlays.Add("drawingOverlay", drawingOverlay);
        //    if (!drawingLayer.IsOpen) {
        //        drawingLayer.Open();
        //    }
        //}

        private void InitializeNewDrawingOverlay(WpfMap map) {
            drawingfeatures = false;
            editingfeatures = false;
            drawingOverlay = new LayerOverlay();
            drawingOverlay.DrawingExceptionMode = DrawingExceptionMode.DrawException;
            drawingOverlay.TileType = TileType.SingleTile;
            drawingLayer = new InMemoryFeatureLayer();
            drawingpolygonshape = new PolygonShape();
            drawingmultipolygonshape = new MultipolygonShape();
            editingmultipolygonshape = new MultipolygonShape();
            drawinglineshape = new LineShape();
            drawingVertices = new Collection<Vertex>();
            polystring = string.Empty;
            //defaultdrawingLineStylePen = new LineStyle(new GeoPen(GeoColor.FromArgb(150, GeoColor.StandardColors.Red), 2));
            defaultdrawingLineStylePen = new LineStyle(new GeoPen(GeoColor.FromArgb(0, GeoColor.StandardColors.Transparent), 2));
            defaultdrawingAreaStyleBrush = new AreaStyle(new GeoPen(GeoColor.FromArgb(255, GeoColor.StandardColors.Red), 2), new GeoSolidBrush(GeoColor.FromArgb(50, GeoColor.StandardColors.Red)));
            defaultdrawingPointStyle = new PointStyle(PointSymbolType.Circle, new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.Red), 10);
            fastEditPolygonLineStyle = new MapStyles.FastEditPolygonLineStyle(mapDistanceUnit, new GeoFont("Arial", 14, DrawingFontStyles.Bold), new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.Black, 3));
            //drawingLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = defaultdrawingAreaStyleBrush;
            //drawingLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = defaultdrawingLineStylePen;
            //drawingLayer.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = defaultdrawingPointStyle;
            drawingLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = null;
            drawingLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = null;
            drawingLayer.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = null;
            drawingLayer.ZoomLevelSet.ZoomLevel01.DefaultTextStyle = null;
            drawingLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Clear();
            drawingLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(defaultdrawingAreaStyleBrush);
            drawingLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(defaultdrawingLineStylePen);
            drawingLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(defaultdrawingPointStyle);
            drawingLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customAreaPolygonStyleText);
            drawingLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(fastEditPolygonLineStyle);
            drawingLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            drawingLayer.FeatureSource.Projection = projection;
            drawingLayer.IsVisible = true;
            drawingOverlay.Layers.Add("drawingLayer", drawingLayer);
            map.Overlays.Add("drawingOverlay", drawingOverlay);
            if (!drawingLayer.IsOpen) {
                drawingLayer.Open();
            }

        }

        private void LoadCustomEditOverlay(WpfMap map) {

            //MapStyles.Overlays.FastEditPolygonOverlay dragInteractiveOverlay = new MapStyles.Overlays.FastEditPolygonOverlay();
            dragInteractiveOverlay = new MapStyles.Overlays.FastEditPolygonOverlay();
            dragInteractiveOverlay.MapDistanceUnit = mapDistanceUnit;
            dragInteractiveOverlay.ControlPointStyle = new PointStyle(PointSymbolType.Circle, new GeoSolidBrush(GeoColor.StandardColors.PaleGoldenrod), new GeoPen(GeoColor.StandardColors.Black), 8);
            dragInteractiveOverlay.DraggedControlPointStyle = new PointStyle(PointSymbolType.Circle, new GeoSolidBrush(GeoColor.StandardColors.Red), new GeoPen(GeoColor.StandardColors.Orange, 2), 10);
            dragInteractiveOverlay.CanDrag = true;
            dragInteractiveOverlay.CanReshape = true;
            dragInteractiveOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = null;
            dragInteractiveOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = null;
            dragInteractiveOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = null;
            dragInteractiveOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.DefaultTextStyle = null;
            dragInteractiveOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Clear();
            dragInteractiveOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(areaStyleBrush);
            dragInteractiveOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            map.EditOverlay = dragInteractiveOverlay;
        }

        private void ProcessTrackPolygon(Vertex lastvertex, Vertex currentvertex) {
            try {
                if (isdrawingpolygon) { return; }
                isdrawingpolygon = true;
                drawingpolygonshape.OuterRing.Vertices.Clear();
                Vertex firstvertex = new Vertex();
                foreach (Vertex vert in drawingVertices) {
                    drawingpolygonshape.OuterRing.Vertices.Add(vert);
                    firstvertex = vert;
                }
                drawingpolygonshape.OuterRing.Vertices.Add(currentvertex);
                drawingpolygonshape.OuterRing.Vertices.Add(lastvertex);
                if (drawingpolygonshape.OuterRing.Vertices.Count < 4) {
                    drawingpolygonshape.OuterRing.Vertices.Add(lastvertex);
                }


                Vertex vertexshape0 = firstvertex;
                Vertex vertexshape1 = currentvertex;
                Vertex vertexshape2 = lastvertex;
                LineShape lineShape1 = new LineShape();
                lineShape1.Vertices.Add(vertexshape0);
                lineShape1.Vertices.Add(vertexshape1);

                //LineShape lineShape2 = new LineShape();
                //lineShape2.Vertices.Add(vertexshape2);
                //lineShape2.Vertices.Add(vertexshape1);

                drawingLayer.InternalFeatures.Clear();
                drawingLayer.InternalFeatures.Add(new Feature(drawingpolygonshape));
                //if (drawingpolygonshape.OuterRing.Vertices.Count > 3) {
                    drawingLayer.InternalFeatures.Add(new Feature(lineShape1));
                    //drawingLayer.InternalFeatures.Add(new Feature(lineShape2));
                //}
                if (map.Overlays.Contains("drawingOverlay")) {
                    if (((LayerOverlay)map.Overlays["drawingOverlay"]).Layers.Contains("drawingLayer")) {
                        map.Overlays["drawingOverlay"].Refresh();
                    }
                }
                isdrawingpolygon = false;
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        private void ProcessEditPolygon(Vertex currentvertex) {
            try {
                if (iseditingpolygon) { return; }
                iseditingpolygon = true;

                //Vertex movingVertex = ((EditPolygonOverlay)Map.EditOverlay).MovingVertex;
                PointShape targetPoint = ((FastEditPolygonOverlay)map.EditOverlay).TargetPoint;
                PointShape targetPoint1 = ((FastEditPolygonOverlay)map.EditOverlay).TargetPoint1;
                PointShape targetPoint2 = ((FastEditPolygonOverlay)map.EditOverlay).TargetPoint2;
                //PointShape targetPoint = projection.ConvertToExternalProjection(targetPoint1) as PointShape;
                //controlpoint = e.TargetVertex;

                if (targetPoint == null) {
                    iseditingpolygon = false;
                    return;
                }
                if (targetPoint == new PointShape()) {
                    iseditingpolygon = false;
                    return;
                }

                BaseShape areabaseshape = map.EditOverlay.EditShapesLayer.InternalFeatures[0].GetShape();
                if (areabaseshape is MultipolygonShape) {
                    editingmultipolygonshape = areabaseshape as MultipolygonShape;
                }

                polystring = editingmultipolygonshape.GetWellKnownText();

                string pointstring = targetPoint.GetWellKnownText();
                pointstring = pointstring.Replace(@"\n", "");
                pointstring = pointstring.Replace(@"\r", "");
                pointstring = pointstring.Replace(@"\t", "");
                pointstring = pointstring.Replace(@"POINT(", "");
                pointstring = pointstring.Replace(@")", "");
                //string pointstring2 = new PointShape(movingVertex).GetWellKnownText();

                PointShape currentpoint1 = new PointShape(currentvertex);
                PointShape currentpoint = projection.ConvertToExternalProjection(currentpoint1) as PointShape;
                string currentstring = currentpoint.GetWellKnownText();
                currentstring = currentstring.Replace(@"\n", "");
                currentstring = currentstring.Replace(@"\r", "");
                currentstring = currentstring.Replace(@"\t", "");
                currentstring = currentstring.Replace(@"POINT(", "");
                currentstring = currentstring.Replace(@")", "");

                //if (!polystring.Contains(pointstring)) { return; }

                polystring = polystring.Replace(pointstring, currentstring);

                MultipolygonShape mps = new MultipolygonShape(polystring);
                MultipolygonShape mps2 = projection.ConvertToInternalProjection(mps) as MultipolygonShape;
                PointShape tpoint = projection.ConvertToInternalProjection(targetPoint) as PointShape;
                PointShape tpoint1 = projection.ConvertToInternalProjection(targetPoint1) as PointShape;
                PointShape tpoint2 = projection.ConvertToInternalProjection(targetPoint2) as PointShape;

                LineShape lineShape1 = new LineShape();
                lineShape1.Vertices.Add(new Vertex(tpoint1));
                lineShape1.Vertices.Add(new Vertex(currentpoint1));

                LineShape lineShape2 = new LineShape();
                lineShape2.Vertices.Add(new Vertex(tpoint2));
                lineShape2.Vertices.Add(new Vertex(currentpoint1));


                //if (!stopwatch.IsRunning) {
                //    stopwatch.Reset();
                //    stopwatch.Start();
                //}

                drawingLayer.InternalFeatures.Clear();
                drawingLayer.InternalFeatures.Add(new Feature(mps2));
                if (mps.Contains(currentpoint)) {
                    drawingLayer.InternalFeatures.Add(new Feature(lineShape1));
                    drawingLayer.InternalFeatures.Add(new Feature(lineShape2));
                }
                if (map.Overlays.Contains("drawingOverlay")) {
                    if (((LayerOverlay)map.Overlays["drawingOverlay"]).Layers.Contains("drawingLayer")) {
                        map.Overlays["drawingOverlay"].Refresh();
                    }
                }
                //CalculateDimensionLayer(mps2, currentpoint1, tpoint1, tpoint2);
                iseditingpolygon = false;
            }
            catch (Exception ex) {
                iseditingpolygon = false;
                log.Error("EditShape - process polygon - {0}", ex);
                throw ex;
            }
        }

        public void ClearDrawingAndEditingPolygon() {
            isdrawingpolygon = false;
            iseditingpolygon = false;
            drawingfeatures = false;
            editingfeatures = false;
            drawingLayer.InternalFeatures.Clear();
            dragInteractiveOverlay.ExistingControlPointsLayer.InternalFeatures.Clear();
            dragInteractiveOverlay.CalculateAllControlPoints();
            //dragInteractiveOverlay.Refresh();
            map.TrackOverlay.TrackShapeLayer.InternalFeatures.Clear();
            //map.TrackOverlay.Refresh();
            map.EditOverlay.EditShapesLayer.InternalFeatures.Clear();
            map.EditOverlay.ExistingControlPointsLayer.InternalFeatures.Clear();
            map.EditOverlay.CalculateAllControlPoints();
            //map.EditOverlay.Refresh();
            //if (map.Overlays.Contains("drawingOverlay")) {
            //    if (((LayerOverlay)map.Overlays["drawingOverlay"]).Layers.Contains("drawingLayer")) {
            //        map.Overlays["drawingOverlay"].Refresh();
            //    }
            //}
            //if (map.Overlays.Contains("fieldOverlay")) {
            //    if (((LayerOverlay)map.Overlays["fieldOverlay"]).Layers.Contains("cropzoneLayerDraw")) {
            //        //((LayerOverlay)map.Overlays["fieldOverlay"]).Layers["cropzoneLayerDraw"].IsVisible = false;
            //    }
            //}

            map.Refresh();
        }

        private void ProcessTrackMeasure(Vertex currentvertex) {
            try {
                if (isdrawingmeasure) { return; }
                isdrawingmeasure = true;
                drawinglineshape.Vertices.Clear();
                foreach (Vertex vert in drawingVertices) {
                    drawinglineshape.Vertices.Add(vert);
                }
                drawinglineshape.Vertices.Add(currentvertex);
                drawingLayer.InternalFeatures.Clear();
                drawingLayer.InternalFeatures.Add(new Feature(drawinglineshape));
                if (map.Overlays.Contains("drawingOverlay")) {
                    if (((LayerOverlay)map.Overlays["drawingOverlay"]).Layers.Contains("drawingLayer")) {
                        map.Overlays["drawingOverlay"].Refresh();
                    }
                }
                isdrawingmeasure = false;
            }
            catch (Exception ex) {
                //throw ex;
            }
        }

        private Vertex GetMousePoint(MouseEventArgs e) {
            try {
                PointShape worldlocation2 = this.map.ToWorldCoordinate(e.GetPosition(map));
                PointShape centerpoint = projection.ConvertToInternalProjection(worldlocation2) as PointShape;
                Vertex geoVertex1 = new Vertex(centerpoint.X, centerpoint.Y);
                return geoVertex1;
            }
            catch (Exception ex) {
                throw ex;
            }
            //return new Vertex();
        }

        private void DrawingPolygonEnded() {
            map.Cursor = previousCursor;
            map.MapClick -= new EventHandler<MapClickWpfMapEventArgs>(map_MapClick);
            map.KeyDown -= new KeyEventHandler(keyEvent_Esc);

            if (map.EditOverlay.EditShapesLayer.InternalFeatures.Count == 0) {
                HandleShapeEdit();
            }

            if (drawingLayer.InternalFeatures.Count > 0) {
                MultipolygonShape editmps = new MultipolygonShape();
                foreach (Feature feat in map.EditOverlay.EditShapesLayer.InternalFeatures) {
                    BaseShape bs = feat.GetShape();
                    if (bs is PolygonShape) {
                        editmps.Polygons.Add(bs as PolygonShape);
                    }
                    if (bs is MultipolygonShape) {
                        MultipolygonShape mps2 = new MultipolygonShape();
                        mps2 = bs as MultipolygonShape;
                        foreach (PolygonShape ps in mps2.Polygons) {
                            editmps.Polygons.Add(ps);
                        }
                    }
                }

                Feature drawingfeature = drawingLayer.InternalFeatures.FirstOrDefault();
                BaseShape baseShape = drawingfeature.GetShape();
                if (baseShape == null) { return; }
                BaseShape baseShape2 = projection.ConvertToExternalProjection(baseShape);
                PolygonShape drawingpoly = baseShape2 as PolygonShape;
                MultipolygonShape mps = new MultipolygonShape();
                mps.Polygons.Add(drawingpoly);

                MultipolygonShape multipoly = AddShapesAndCutOuts(editmps, mps);
                if (multipoly == null) {
                    try {
                        if (map.Overlays.Contains("drawingOverlay")) {
                            if (((LayerOverlay)map.Overlays["drawingOverlay"]).Layers.Contains("drawingLayer")) {
                                drawingLayer.InternalFeatures.Clear();
                                drawingpolygonshape = new PolygonShape();
                                drawingVertices = new Collection<Vertex>();
                                map.Overlays["drawingOverlay"].Refresh();
                            }
                        }

                        map.TrackOverlay.TrackShapeLayer.InternalFeatures.Clear();
                        map.EditOverlay.ExistingControlPointsLayer.InternalFeatures.Clear();
                        map.EditOverlay.EditShapesLayer.InternalFeatures.Clear();
                        map.EditOverlay.CalculateAllControlPoints();
                        map.EditOverlay.Refresh();
                        map.Cursor = previousCursor;
                    }
                    catch { }
                    return;
                }
                map.EditOverlay.EditShapesLayer.InternalFeatures.Clear();
                map.EditOverlay.ExistingControlPointsLayer.InternalFeatures.Clear();
                map.EditOverlay.EditShapesLayer.InternalFeatures.Add(new Feature(multipoly));
                map.EditOverlay.CalculateAllControlPoints();
                map.EditOverlay.Refresh();
                map.TrackOverlay.TrackShapeLayer.InternalFeatures.Clear();
                if (map.Overlays.Contains("drawingOverlay")) {
                    if (((LayerOverlay)map.Overlays["drawingOverlay"]).Layers.Contains("drawingLayer")) {
                        drawingLayer.InternalFeatures.Clear();
                        drawingpolygonshape = new PolygonShape();
                        drawingVertices = new Collection<Vertex>();
                        map.Overlays["drawingOverlay"].Refresh();
                    }
                }

                map.Cursor = Cursors.Hand;
                parent.HasUnsavedChanges = true;
                parent.IsMeasureLine = false;
                editingfeatures = true;

                try {
                    var shapelayerfeatures = map.EditOverlay.EditShapesLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);
                    if (shapelayerfeatures.Count > 0) {
                        RectangleShape extent = ExtentHelper.GetBoundingBoxOfItems(shapelayerfeatures);
                        map.CurrentExtent = extent;
                        PointShape center = extent.GetCenterPoint();
                        map.CenterAt(center);
                    }
                }
                catch (Exception ex) {
                    log.ErrorException("Caught an exception while working with a drawing tool shape", ex);
                    //System.Windows.MessageBox.Show("ImportTools - Caught an exception while working with a simple draw shape.");
                }
            }
        }

        public void HandleFastDrawAndEditPolygon() {
            parent.IsMeasureLine = false;
            if (drawingfeatures) {
                CancelTrackingModeAndEdit();
                return;
            }

            drawingfeatures = true;
            drawingLayer.InternalFeatures.Clear();
            drawingpolygonshape = new PolygonShape();
            drawingVertices = new Collection<Vertex>();
            //stopwatch = new Stopwatch();

            map.MapClick += new EventHandler<MapClickWpfMapEventArgs>(map_MapClick);
            map.KeyDown += new KeyEventHandler(keyEvent_Esc);
            //map.ExtentOverlay.DoubleLeftClickMode = MapDoubleLeftClickMode.Disabled;

            //if (map.Overlays.Contains("fieldOverlay")) {
            //    if (((LayerOverlay)map.Overlays["fieldOverlay"]).Layers.Contains("cropzoneLayerDraw")) {
            //        ((LayerOverlay)map.Overlays["fieldOverlay"]).Layers["cropzoneLayerDraw"].IsVisible = true;
            //    }
            //}

            previousCursor = map.Cursor;
            map.Cursor = Cursors.Pen;
            parent.IsMeasureLine = false;
        }

        //private void InitializeNewEditingOverlay(WpfMap map) {
        //    editingfeatures = false;
        //    isdragmode = false;
        //    editingOverlay = new LayerOverlay();
        //    editingOverlay.DrawingExceptionMode = DrawingExceptionMode.DrawException;
        //    editingOverlay.TileType = TileType.SingleTile;
        //    editingPolygonLayer = new InMemoryFeatureLayer();
        //    editingLineLayer = new InMemoryFeatureLayer();
        //    editingPointLayer = new InMemoryFeatureLayer();
        //    editingpolygonshape = new PolygonShape();
        //    editinglineshape = new LineShape();
        //    editingVertices = new Collection<Vertex>();
        //    //stopwatch = new Stopwatch();
        //    defaulteditingLineStylePen = new LineStyle(new GeoPen(GeoColor.FromArgb(150, GeoColor.StandardColors.Blue), 2));
        //    defaulteditingAreaStyleBrush = new AreaStyle(new GeoPen(GeoColor.FromArgb(255, GeoColor.StandardColors.Blue), 2), new GeoSolidBrush(GeoColor.FromArgb(50, GeoColor.StandardColors.Blue)));
        //    defaulteditingPointStyle = new PointStyle(PointSymbolType.Circle, new GeoSolidBrush(GeoColor.StandardColors.White), new GeoPen(GeoColor.StandardColors.Blue), 10);
        //    editingPolygonLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = defaulteditingAreaStyleBrush;
        //    editingPolygonLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
        //    editingPolygonLayer.FeatureSource.Projection = projection;
        //    editingPolygonLayer.IsVisible = true;
        //    editingOverlay.Layers.Add("editingPolygonLayer", editingPolygonLayer);
        //    editingLineLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = defaulteditingLineStylePen;
        //    editingLineLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
        //    editingLineLayer.FeatureSource.Projection = projection;
        //    editingLineLayer.IsVisible = true;
        //    editingOverlay.Layers.Add("editingLineLayer", editingLineLayer);
        //    editingPointLayer.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = defaulteditingPointStyle;
        //    editingPointLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
        //    editingPointLayer.FeatureSource.Projection = projection;
        //    editingPointLayer.IsVisible = true;
        //    editingOverlay.Layers.Add("editingPointLayer", editingPointLayer);
        //    map.Overlays.Add("editingOverlay", editingOverlay);
        //    if (!editingPolygonLayer.IsOpen) {
        //        editingPolygonLayer.Open();
        //    }
        //    if (!editingLineLayer.IsOpen) {
        //        editingLineLayer.Open();
        //    }
        //    if (!editingPointLayer.IsOpen) {
        //        editingPointLayer.Open();
        //    }
        //}

        public void HandleDrawPolygon() {
            if (map.TrackOverlay.TrackMode == TrackMode.Polygon) {
                CancelTrackingModeAndEdit();
                return;
            }
            map.TrackOverlay.TrackEnded += new EventHandler<TrackEndedTrackInteractiveOverlayEventArgs>(TrackOverlay_TrackEnded);
            map.TrackOverlay.TrackMode = TrackMode.Polygon;
            map.MapClick += new EventHandler<MapClickWpfMapEventArgs>(map_MapClick);
            map.KeyDown += new KeyEventHandler(keyEvent_Esc);
            previousCursor = map.Cursor;
            map.Cursor = Cursors.Pen;
            parent.IsMeasureLine = false;
        }

        public void HandleDrawAndEditPolygon() {
            parent.IsMeasureLine = false;
            if (map.TrackOverlay.TrackMode == TrackMode.Polygon) {
                CancelTrackingModeAndEdit();
                return;
            }
            //map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = null;
            //map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = null;
            //map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = null;
            //map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.DefaultTextStyle = null;
            //map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Clear();
            //map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customAreaPolygonStyleText);
            //map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(areaStyleBrush);
            //map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;

            map.TrackOverlay.TrackEnded += new EventHandler<TrackEndedTrackInteractiveOverlayEventArgs>(TrackAndEditOverlay_TrackEnded);
            map.TrackOverlay.TrackMode = TrackMode.Polygon;
            map.MapClick += new EventHandler<MapClickWpfMapEventArgs>(map_MapClick);
            map.KeyDown += new KeyEventHandler(keyEvent_Esc);
            previousCursor = map.Cursor;
            map.Cursor = Cursors.Pen;
            parent.IsMeasureLine = false;
        }

        public void HandleDrawPivot() {
            if (map.TrackOverlay.TrackMode == TrackMode.Circle) {
                CancelTrackingModeAndEdit();
                return;
            }
            map.TrackOverlay.TrackEnded += new EventHandler<TrackEndedTrackInteractiveOverlayEventArgs>(TrackOverlay_TrackEnded);
            map.TrackOverlay.TrackMode = TrackMode.Circle;
            map.MapClick += new EventHandler<MapClickWpfMapEventArgs>(map_MapClick);
            map.KeyDown += new KeyEventHandler(keyEvent_Esc);
            previousCursor = map.Cursor;
            map.Cursor = Cursors.Pen;
            parent.IsMeasureLine = false;
        }

        public void HandleMeasureLine() {
            if (map.TrackOverlay.TrackMode == TrackMode.Line) {
                CancelTrackingModeAndEdit();
                return;
            }
            map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = null;
            map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = null;
            map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = null;
            map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.DefaultTextStyle = null;
            map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Clear();
            map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customDistancePolygonStyleText);
            map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customDistanceLineStyleText);
            map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(lineStylePen);
            map.TrackOverlay.TrackShapeLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            map.TrackOverlay.TrackEnded += new EventHandler<TrackEndedTrackInteractiveOverlayEventArgs>(TrackOverlay_TrackEnded);
            map.TrackOverlay.TrackMode = TrackMode.Line;
            map.MapClick += new EventHandler<MapClickWpfMapEventArgs>(map_MapClick);
            map.KeyDown += new KeyEventHandler(keyEvent_Esc);
            previousCursor = map.Cursor;
            map.Cursor = Cursors.Pen;
            parent.IsMeasureLine = true;
        }

        //private void MouseMoved(object sender, MouseMovedTrackInteractiveOverlayEventArgs e) {
        //    //Gets the shape as it is being tracked and gets its properties to be displayed in labels og groupbox
        //    LineBaseShape areaBaseShape = (LineBaseShape)map.TrackOverlay.TrackShapeLayer.InternalFeatures[0].GetShape();
        //    double Perimeter = Math.Round(areaBaseShape.GetPerimeter(map.MapUnit, DistanceUnit.Kilometer));
        //    double Area = Math.Round(areaBaseShape.GetArea(map.MapUnit, AreaUnit.SquareKilometers));
        //    lblPerimeter.Text = String.Format("{0:0,0}", Perimeter) + " km";
        //    lblArea.Text = String.Format("{0:0,0}", Area) + " km2";

        //    //Sets the location (in screen coordinates) of the groupbox according to last moved vertex of the tracked shape (in world coordinates).
        //    ScreenPointF screenPointF = ThinkGeo.MapSuite.Core.ExtentHelper.ToScreenCoordinate(map.CurrentExtent, e.MovedVertex.X, e.MovedVertex.Y, map.Width, map.Height);
        //    groupBoxInfo.Location = new Point((int)screenPointF.X + 30, (int)screenPointF.Y + 10);
        //}

        private void TrackOverlay_TrackEnded(object sender, TrackEndedTrackInteractiveOverlayEventArgs e) {
            map.TrackOverlay.TrackEnded -= new EventHandler<TrackEndedTrackInteractiveOverlayEventArgs>(TrackOverlay_TrackEnded);
            map.TrackOverlay.TrackMode = TrackMode.None;
            map.MapClick -= new EventHandler<MapClickWpfMapEventArgs>(map_MapClick);
            map.KeyDown -= new KeyEventHandler(keyEvent_Esc);
            map.Cursor = previousCursor;
            parent.HasUnsavedChanges = true;
            editingfeatures = true;

            //map.TrackOverlay = new TrackInteractiveOverlay();
        }

        private void TrackAndEditOverlay_TrackEnded(object sender, TrackEndedTrackInteractiveOverlayEventArgs e) {
            map.TrackOverlay.TrackEnded -= new EventHandler<TrackEndedTrackInteractiveOverlayEventArgs>(TrackAndEditOverlay_TrackEnded);
            map.TrackOverlay.TrackMode = TrackMode.None;
            map.MapClick -= new EventHandler<MapClickWpfMapEventArgs>(map_MapClick);
            map.KeyDown -= new KeyEventHandler(keyEvent_Esc);
            map.Cursor = previousCursor;

            if (map.EditOverlay.EditShapesLayer.InternalFeatures.Count == 0) {
                HandleShapeEdit();
            }

            if (map.TrackOverlay.TrackShapeLayer.InternalFeatures.Count > 0) {
                MultipolygonShape editmps = new MultipolygonShape();
                foreach (Feature feat in map.EditOverlay.EditShapesLayer.InternalFeatures) {
                    BaseShape bs = feat.GetShape();
                    if (bs is PolygonShape) {
                        editmps.Polygons.Add(bs as PolygonShape);
                    }
                    if (bs is MultipolygonShape) {
                        MultipolygonShape mps2 = new MultipolygonShape();
                        mps2 = bs as MultipolygonShape;
                        foreach (PolygonShape ps in mps2.Polygons) {
                            editmps.Polygons.Add(ps);
                        }
                    }
                }
                MultipolygonShape mps = new MultipolygonShape();
                foreach (Feature feat in map.TrackOverlay.TrackShapeLayer.InternalFeatures) {
                    BaseShape bs = feat.GetShape();
                    if (bs is PolygonShape) {
                        mps.Polygons.Add(bs as PolygonShape);
                    }
                    if (bs is MultipolygonShape) {
                        MultipolygonShape mps2 = new MultipolygonShape();
                        mps2 = bs as MultipolygonShape;
                        foreach (PolygonShape ps in mps2.Polygons) {
                            mps.Polygons.Add(ps);
                        }
                    }
                }
                MultipolygonShape multipoly = AddShapesAndCutOuts(editmps, mps);
                if (multipoly == null) {
                    try {
                        map.TrackOverlay.TrackShapeLayer.InternalFeatures.Clear();
                        map.EditOverlay.EditShapesLayer.InternalFeatures.Clear();
                        map.EditOverlay.CalculateAllControlPoints();
                        map.EditOverlay.Refresh();
                        map.Cursor = previousCursor;
                    }
                    catch { }
                    return;
                }

                //map.EditOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = null;
                //map.EditOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = null;
                //map.EditOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = null;
                //map.EditOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.DefaultTextStyle = null;
                //map.EditOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Clear();
                //map.EditOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customAreaPolygonStyleText);
                //map.EditOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(areaStyleBrush);
                //map.EditOverlay.EditShapesLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;

                map.EditOverlay.ExistingControlPointsLayer.InternalFeatures.Clear();
                map.EditOverlay.EditShapesLayer.InternalFeatures.Clear();
                map.EditOverlay.EditShapesLayer.InternalFeatures.Add(new Feature(multipoly));
                map.EditOverlay.CalculateAllControlPoints();
                map.EditOverlay.Refresh();
                map.TrackOverlay.TrackShapeLayer.InternalFeatures.Clear();
                map.Cursor = Cursors.Hand;
                parent.HasUnsavedChanges = true;
                parent.IsMeasureLine = false;
                editingfeatures = true;

                try {
                    var shapelayerfeatures = map.EditOverlay.EditShapesLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);
                    if (shapelayerfeatures.Count > 0) {
                        RectangleShape extent = ExtentHelper.GetBoundingBoxOfItems(shapelayerfeatures);
                        map.CurrentExtent = extent;
                        PointShape center = extent.GetCenterPoint();
                        map.CenterAt(center);
                    }


                    map.Refresh();
                    //LoadDimensionLayer();
                    //map.TrackOverlay = new TrackInteractiveOverlay();
                }
                catch (Exception ex) {
                    log.ErrorException("Caught an exception while tracking ended", ex);
                    //System.Windows.MessageBox.Show("ImportTools - Caught an exception while working with a simple draw shape.");
                }

            }
        }

        internal void LoadDimensionLayer() {
            try {
                if (map.Overlays.Contains("dimensionOverlay")) {
                    if (((LayerOverlay)map.Overlays["dimensionOverlay"]).Layers.Contains("dimensionLayer")) {
                        if (!projection.IsOpen) { projection.Open(); }
                        if (map.TrackOverlay.TrackShapeLayer.InternalFeatures.Count > 0) {
                            BaseShape baseShape1 = map.TrackOverlay.TrackShapeLayer.InternalFeatures[0].GetShape();
                            BaseShape baseShape = projection.ConvertToInternalProjection(baseShape1);
                            if (baseShape is PolygonShape) {
                                PolygonShape polyShape = baseShape as PolygonShape;
                                if (polyShape.OuterRing.Vertices.Count > 1) {
                                    LineShape lineShape = new LineShape();
                                    foreach (Vertex vert in polyShape.OuterRing.Vertices) {
                                        lineShape.Vertices.Add(vert);
                                    }
                                    ((InMemoryFeatureLayer)((LayerOverlay)map.Overlays["dimensionOverlay"]).Layers["dimensionLayer"]).InternalFeatures.Clear();
                                    ((InMemoryFeatureLayer)((LayerOverlay)map.Overlays["dimensionOverlay"]).Layers["dimensionLayer"]).InternalFeatures.Add(new Feature(polyShape));
                                    ((InMemoryFeatureLayer)((LayerOverlay)map.Overlays["dimensionOverlay"]).Layers["dimensionLayer"]).InternalFeatures.Add(new Feature(lineShape));
                                }
                            }
                        }
                        //if (drawingLayer.InternalFeatures.Count > 0) {
                        //    BaseShape baseShape1 = drawingLayer.InternalFeatures[0].GetShape();
                        //    BaseShape baseShape = projection.ConvertToInternalProjection(baseShape1);
                        //    if (baseShape is PolygonShape) {
                        //        PolygonShape polyShape = baseShape as PolygonShape;
                        //        if (polyShape.OuterRing.Vertices.Count > 1) {
                        //            LineShape lineShape = new LineShape();
                        //            foreach (Vertex vert in polyShape.OuterRing.Vertices) {
                        //                lineShape.Vertices.Add(vert);
                        //            }
                        //            ((InMemoryFeatureLayer)((LayerOverlay)map.Overlays["dimensionOverlay"]).Layers["dimensionLayer"]).InternalFeatures.Clear();
                        //            ((InMemoryFeatureLayer)((LayerOverlay)map.Overlays["dimensionOverlay"]).Layers["dimensionLayer"]).InternalFeatures.Add(new Feature(polyShape));
                        //            ((InMemoryFeatureLayer)((LayerOverlay)map.Overlays["dimensionOverlay"]).Layers["dimensionLayer"]).InternalFeatures.Add(new Feature(lineShape));
                        //        }
                        //    }
                        //}
                        if (map.EditOverlay.EditShapesLayer.InternalFeatures.Count > 0) {
                            BaseShape baseShape1 = map.EditOverlay.EditShapesLayer.InternalFeatures[0].GetShape();
                            BaseShape baseShape = projection.ConvertToInternalProjection(baseShape1);
                            MultipolygonShape multipoly = new MultipolygonShape();
                            if (baseShape is MultipolygonShape) {
                                multipoly = baseShape as MultipolygonShape;
                            }
                            if (baseShape is PolygonShape) {
                                PolygonShape poly = baseShape as PolygonShape;
                                multipoly.Polygons.Add(poly);
                            }
                            ((InMemoryFeatureLayer)((LayerOverlay)map.Overlays["dimensionOverlay"]).Layers["dimensionLayer"]).InternalFeatures.Clear();
                            ((InMemoryFeatureLayer)((LayerOverlay)map.Overlays["dimensionOverlay"]).Layers["dimensionLayer"]).InternalFeatures.Add(new Feature(multipoly));

                            foreach (PolygonShape polyShape in multipoly.Polygons) {
                                if (polyShape.OuterRing.Vertices.Count > 1) {
                                    LineShape lineShape = new LineShape();
                                    foreach (Vertex vert in polyShape.OuterRing.Vertices) {
                                        lineShape.Vertices.Add(vert);
                                    }
                                    ((InMemoryFeatureLayer)((LayerOverlay)map.Overlays["dimensionOverlay"]).Layers["dimensionLayer"]).InternalFeatures.Add(new Feature(lineShape));
                                }
                            }
                        }

                        map.Overlays["dimensionOverlay"].Refresh();
                    }
                }
            }
            catch (Exception ex) {
                log.Error("DrawTool - Error Loaddiminsion - {0}", ex);
            }
        }

        internal void UnLoadDimensionLayer() {
            if (map.Overlays.Contains("dimensionOverlay")) {
                if (((LayerOverlay)map.Overlays["dimensionOverlay"]).Layers.Contains("dimensionLayer")) {
                    ((InMemoryFeatureLayer)((LayerOverlay)map.Overlays["dimensionOverlay"]).Layers["dimensionLayer"]).InternalFeatures.Clear();
                    map.Overlays["dimensionOverlay"].Refresh();
                }
            }
        }

        private MultipolygonShape AddShapesAndCutOuts(MultipolygonShape multiPolygonShape, MultipolygonShape multipoly) {
            try {
                if (multiPolygonShape.Polygons.Count > 0 && multipoly.Polygons.Count > 0 && multiPolygonShape.Contains(multipoly)) {
                    try {
                        MultipolygonShape resultingmulti = multiPolygonShape.GetDifference(multipoly);
                        multiPolygonShape = resultingmulti;
                    }
                    catch {
                        for (int i = 0; i < multiPolygonShape.Polygons.Count; i++) {
                            foreach (PolygonShape poly in multipoly.Polygons) {
                                if (multiPolygonShape.Polygons[i].Contains(poly)) {
                                    multiPolygonShape.Polygons[i].InnerRings.Add(new RingShape(poly.OuterRing.Vertices));
                                }
                            }
                        }
                    }
                }
                else if (multiPolygonShape.Polygons.Count > 0 && multipoly.Polygons.Count > 0 && multiPolygonShape.Overlaps(multipoly)) {
                    try {
                        MultipolygonShape resultingmulti = multiPolygonShape.GetDifference(multipoly);
                        multiPolygonShape = resultingmulti;
                    }
                    catch { }
                }
                else {
                    foreach (PolygonShape poly in multipoly.Polygons) {
                        multiPolygonShape.Polygons.Add(poly);
                    }
                }
            }
            catch (Exception ex) {
                log.ErrorException("There was a problem adding a shape or cutting a hole.", ex);
                System.Windows.MessageBox.Show(Strings.ThereWasAProblemAddingShape_Text);
                return null;
            }

            return multiPolygonShape;
        }

        public void HandleDeleteShape() {
            map.MapClick += new EventHandler<MapClickWpfMapEventArgs>(Map_DeleteShapeDoubleClick);
            map.Cursor = Cursors.Hand;
            parent.IsMeasureLine = false;
        }

        private void Map_DeleteShapeDoubleClick(object sender, MapClickWpfMapEventArgs e) {
            map.MapClick -= new EventHandler<MapClickWpfMapEventArgs>(Map_DeleteShapeDoubleClick);
            map.TrackOverlay.TrackMode = TrackMode.None;
            map.Cursor = previousCursor;
            parent.HasUnsavedChanges = true;
            parent.DeleteShape(e.WorldLocation);
            parent.IsMeasureLine = false;
            editingfeatures = true;

        }

        public void HandleShapeEdit() {
            //LoadCustomEditOverlay(map);
            LayerOverlay fieldOverlay = (LayerOverlay)map.Overlays["fieldOverlay"];
            FeatureLayer currentLayer = (FeatureLayer)fieldOverlay.Layers["currentLayer"];
            System.Collections.ObjectModel.Collection<ThinkGeo.MapSuite.Core.Feature> selectedFeatures = currentLayer.FeatureSource.GetAllFeatures(ThinkGeo.MapSuite.Core.ReturningColumnsType.AllColumns);
            map.EditOverlay.EditShapesLayer.InternalFeatures.Clear();
            map.EditOverlay.ExistingControlPointsLayer.InternalFeatures.Clear();
            //map.EditOverlay.ExistingControlPointsLayer.Clear();
            if (selectedFeatures.Count > 0) {
                map.EditOverlay.EditShapesLayer.InternalFeatures.Add(selectedFeatures[0]);
                map.EditOverlay.CalculateAllControlPoints();
                map.EditOverlay.Refresh();
                map.Cursor = Cursors.Hand;
                parent.HasUnsavedChanges = true;
                editingfeatures = true;
            }
            parent.IsMeasureLine = false;
            //LoadDimensionLayer();

        }

        public void HandleShapeControl() {
            //LoadCustomEditOverlay(map);
            LayerOverlay fieldOverlay = (LayerOverlay)map.Overlays["fieldOverlay"];
            FeatureLayer currentLayer = (FeatureLayer)fieldOverlay.Layers["currentLayer"];
            System.Collections.ObjectModel.Collection<ThinkGeo.MapSuite.Core.Feature> selectedFeatures = currentLayer.FeatureSource.GetAllFeatures(ThinkGeo.MapSuite.Core.ReturningColumnsType.AllColumns);
            map.EditOverlay.EditShapesLayer.InternalFeatures.Clear();
            map.EditOverlay.ExistingControlPointsLayer.InternalFeatures.Clear();
            //map.EditOverlay.ExistingControlPointsLayer.Clear();
            if (selectedFeatures.Count > 0) {
                map.EditOverlay.EditShapesLayer.InternalFeatures.Add(selectedFeatures[0]);
                map.EditOverlay.CalculateAllControlPoints();
                map.EditOverlay.Refresh();
                map.Cursor = Cursors.Hand;
                parent.HasUnsavedChanges = true;
                editingfeatures = false;
            }
            parent.IsMeasureLine = false;
            //LoadDimensionLayer();

        }

        private void keyEvent_Esc(object sender, KeyEventArgs e) {
            if (e.Key == System.Windows.Input.Key.Escape) {
                CancelTrackingModeAndEdit();
            }
        }

        private void CancelTrackingModeAndEdit() {
            if (map.TrackOverlay.TrackMode != TrackMode.None) {
                map.TrackOverlay.TrackEnded -= new EventHandler<TrackEndedTrackInteractiveOverlayEventArgs>(TrackOverlay_TrackEnded);
                map.KeyDown -= new KeyEventHandler(keyEvent_Esc);

                if (map.TrackOverlay.TrackMode == TrackMode.Polygon || map.TrackOverlay.TrackMode == TrackMode.Line || map.TrackOverlay.TrackMode == TrackMode.Circle) {
                    map.TrackOverlay.MouseDoubleClick(new InteractionArguments());
                }
                else {
                    map.TrackOverlay.MouseUp(new InteractionArguments());
                }

                map.TrackOverlay.TrackMode = TrackMode.None;
                map.Cursor = previousCursor;
                parent.HasUnsavedChanges = false;
                editingfeatures = false;
                //parent.ShowDeleteCommand = false;

                int count = map.TrackOverlay.TrackShapeLayer.InternalFeatures.Count;
                if (count > 0) {
                    map.TrackOverlay.TrackShapeLayer.InternalFeatures.Remove("InTrackingFeature");
                }
                map.TrackOverlay.TrackShapeLayer.InternalFeatures.Clear();
                try {
                    map.TrackOverlay.Refresh();
                }
                catch { }
                //map.TrackOverlay = new TrackInteractiveOverlay();

                try {
                    map.EditOverlay.ExistingControlPointsLayer.InternalFeatures.Clear();
                    map.EditOverlay.EditShapesLayer.InternalFeatures.Clear();
                    map.EditOverlay.CalculateAllControlPoints();
                    map.EditOverlay.Refresh();
                }
                catch { }
            }
            if (drawingfeatures) {
                if (map.Overlays.Contains("drawingOverlay")) {
                    if (((LayerOverlay)map.Overlays["drawingOverlay"]).Layers.Contains("drawingLayer")) {
                        drawingLayer.InternalFeatures.Clear();
                        drawingpolygonshape = new PolygonShape();
                        drawingVertices = new Collection<Vertex>();
                        map.Overlays["drawingOverlay"].Refresh();
                        map.Cursor = previousCursor;
                        drawingfeatures = false;
                    }
                }
                //if (map.Overlays.Contains("fieldOverlay")) {
                //    if (((LayerOverlay)map.Overlays["fieldOverlay"]).Layers.Contains("cropzoneLayerDraw")) {
                //        ((LayerOverlay)map.Overlays["fieldOverlay"]).Layers["cropzoneLayerDraw"].IsVisible = false;
                //    }
                //}
            }

            if (map.TrackOverlay.TrackMode == TrackMode.None) {
                try {
                    //LoadCustomEditOverlay(map);
                    map.EditOverlay.ExistingControlPointsLayer.InternalFeatures.Clear();
                    map.EditOverlay.EditShapesLayer.InternalFeatures.Clear();
                    map.EditOverlay.CalculateAllControlPoints();
                    map.EditOverlay.Refresh();
                    map.Cursor = previousCursor;
                }
                catch { }
            }
            parent.SetButtonsAndMenus();
            parent.TurnOffCropzoneOutlines();
            UnLoadDimensionLayer();
            map.ZoomOut(10);
            map.Refresh();
        }

        //private void CancelFastTrackingModeAndEdit() {
        //    if (map.TrackOverlay.TrackMode != TrackMode.None) {
        //        map.TrackOverlay.TrackEnded -= new EventHandler<TrackEndedTrackInteractiveOverlayEventArgs>(TrackOverlay_TrackEnded);
        //        map.KeyDown -= new KeyEventHandler(keyEvent_Esc);

        //        if (map.TrackOverlay.TrackMode == TrackMode.Polygon || map.TrackOverlay.TrackMode == TrackMode.Line || map.TrackOverlay.TrackMode == TrackMode.Circle) {
        //            map.TrackOverlay.MouseDoubleClick(new InteractionArguments());
        //        }
        //        else {
        //            map.TrackOverlay.MouseUp(new InteractionArguments());
        //        }

        //        map.TrackOverlay.TrackMode = TrackMode.None;
        //        map.Cursor = previousCursor;
        //        parent.HasUnsavedChanges = false;
        //        parent.ShowDeleteCommand = false;

        //        int count = map.TrackOverlay.TrackShapeLayer.InternalFeatures.Count;
        //        if (count > 0) {
        //            map.TrackOverlay.TrackShapeLayer.InternalFeatures.Remove("InTrackingFeature");
        //        }
        //        map.TrackOverlay.TrackShapeLayer.InternalFeatures.Clear();
        //        try {
        //            map.TrackOverlay.Refresh();
        //        }
        //        catch { }
        //        //map.TrackOverlay = new TrackInteractiveOverlay();

        //        try {
        //            map.EditOverlay.EditShapesLayer.InternalFeatures.Clear();
        //            map.EditOverlay.CalculateAllControlPoints();
        //            map.EditOverlay.Refresh();
        //        }
        //        catch { }
        //    }
        //    if (map.TrackOverlay.TrackMode == TrackMode.None) {
        //        try {
        //            map.EditOverlay.EditShapesLayer.InternalFeatures.Clear();
        //            map.EditOverlay.CalculateAllControlPoints();
        //            map.EditOverlay.Refresh();
        //            map.Cursor = previousCursor;
        //        }
        //        catch { }
        //    }
        //    UnLoadDimensionLayer();
        //    map.ZoomOut(10);
        //    map.Refresh();
        //}

        private void map_MapClick(object sender, MapClickWpfMapEventArgs e) {
            if (!map.IsFocused) {
                map.Focus();
            }
        }

        private void ResetAndRefreshLayers() {
            map.TrackOverlay.TrackShapeLayer.Open();
            map.TrackOverlay.TrackShapeLayer.Clear();
            map.TrackOverlay.TrackShapeLayer.Close();
            map.EditOverlay.EditShapesLayer.Open();
            map.EditOverlay.EditShapesLayer.Clear();
            map.EditOverlay.CalculateAllControlPoints();
            map.EditOverlay.EditShapesLayer.Close();
            map.Overlays["fieldOverlay"].Refresh();
            map.TrackOverlay.Refresh();
            map.EditOverlay.Refresh();
            UnLoadDimensionLayer();
        }

        public void HandleClearEditOverlay() {
            map.TrackOverlay.Refresh();
            //if (map.EditOverlay.EditShapesLayer.InternalFeatures.Count > 1) {
            //    map.EditOverlay.EditShapesLayer.InternalFeatures.RemoveAt(map.EditOverlay.EditShapesLayer.InternalFeatures.Count - 1);
            //}
            //else {
            map.EditOverlay.ExistingControlPointsLayer.InternalFeatures.Clear();
            map.EditOverlay.EditShapesLayer.InternalFeatures.Clear();
            //}
            map.EditOverlay.CalculateAllControlPoints();
            map.EditOverlay.Refresh();
        }

        public void HandleSubractShape() {
            map.TrackOverlay.Refresh();
            //if (map.EditOverlay.EditShapesLayer.InternalFeatures.Count > 1) {
            HandleDeleteAShape();
            //}
            //else {
            //    map.EditOverlay.EditShapesLayer.InternalFeatures.Clear();
            //}
            //map.EditOverlay.CalculateAllControlPoints();
            //map.EditOverlay.Refresh();
        }

        public void HandleDeleteAShape() {
            map.MapClick += new EventHandler<MapClickWpfMapEventArgs>(Map_DeleteAShape);
            map.Cursor = Cursors.Hand;
            parent.IsMeasureLine = false;
        }

        private void Map_DeleteAShape(object sender, MapClickWpfMapEventArgs e) {
            map.MapClick -= new EventHandler<MapClickWpfMapEventArgs>(Map_DeleteAShape);
            map.TrackOverlay.TrackMode = TrackMode.None;
            map.Cursor = previousCursor;
            parent.HasUnsavedChanges = true;
            DeleteAShape(e.WorldLocation);
            parent.IsMeasureLine = false;
            //editingfeatures = false;

        }

        void DeleteAShape(PointShape worldlocation) {
            bool foundashape = false;
            MultipolygonShape currentPolygonShape = new MultipolygonShape();
            Feature newfeature = new Feature();
            for (int x = 0; x < map.EditOverlay.EditShapesLayer.InternalFeatures.Count; x++) {
                if (foundashape) {
                    break;
                }
                currentPolygonShape = new MultipolygonShape();
                Feature feature = map.EditOverlay.EditShapesLayer.InternalFeatures[x];
                var shape = map.EditOverlay.EditShapesLayer.InternalFeatures[x].GetShape();
                if (shape is MultipolygonShape) {
                    currentPolygonShape = shape as MultipolygonShape;
                }
                else if (shape is PolygonShape) {
                    currentPolygonShape.Polygons.Add(shape as PolygonShape);
                }
                try {
                    for (int i = 0; i < currentPolygonShape.Polygons.Count; i++) {
                        if (foundashape) { break; }
                        for (int j = 0; j < currentPolygonShape.Polygons[i].InnerRings.Count; j++) {
                            if (foundashape) {
                                break;
                            }
                            if (currentPolygonShape.Polygons[i].InnerRings[j].Contains(worldlocation)) {
                                currentPolygonShape.Polygons[i].InnerRings.RemoveAt(j);
                                foundashape = true;
                                break;
                            }
                        }
                        if (foundashape) {
                            break;
                        }
                        if (currentPolygonShape.Polygons[i].OuterRing.Contains(worldlocation)) {
                            currentPolygonShape.Polygons.RemoveAt(i);
                            foundashape = true;
                            break;
                        }
                    }
                    if (foundashape) {
                        break;
                    }
                }
                catch (Exception ex) {
                    log.ErrorException("There was a problem deleting a shape.", ex);
                }

            }
            if (foundashape) {
                map.EditOverlay.EditShapesLayer.InternalFeatures.Clear();
                if (currentPolygonShape.Polygons.Count == 0) {
                    //currentLayer.InternalFeatures.Clear();
                }
                else {
                    map.EditOverlay.EditShapesLayer.InternalFeatures.Add(new Feature(currentPolygonShape));
                }
                map.EditOverlay.CalculateAllControlPoints();
                map.EditOverlay.Refresh();
            }
        }

        public void HandleDrawAndEditShape() {
            if (toggledrawandedit) {
                toggledrawandedit = false;

            }
            else {
                LayerOverlay fieldOverlay = (LayerOverlay)map.Overlays["fieldOverlay"];
                FeatureLayer currentLayer = (FeatureLayer)fieldOverlay.Layers["currentLayer"];
                System.Collections.ObjectModel.Collection<ThinkGeo.MapSuite.Core.Feature> selectedFeatures = currentLayer.FeatureSource.GetAllFeatures(ThinkGeo.MapSuite.Core.ReturningColumnsType.AllColumns);
                //LoadCustomEditOverlay(map);
                map.EditOverlay.EditShapesLayer.InternalFeatures.Clear();
                if (selectedFeatures.Count > 0) {
                    map.EditOverlay.EditShapesLayer.InternalFeatures.Add(selectedFeatures[0]);
                    map.EditOverlay.CalculateAllControlPoints();
                    map.EditOverlay.Refresh();
                    map.Cursor = Cursors.Hand;
                    parent.HasUnsavedChanges = true;
                    editingfeatures = true;
                }
                parent.IsMeasureLine = false;
                toggledrawandedit = true;
                //map.EditOverlay.MouseMove -= new MouseEventHandler(EditMouseMove);
                //map.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Map_MouseMove);
                map.MouseMove += new MouseEventHandler(Map_MouseMove);
            }
        }


        private void Map_MouseMove(object sender, MouseEventArgs e) {
            try {
                System.Windows.Point ee = e.GetPosition(map);
                PointShape worldpoint = ExtentHelper.ToWorldCoordinate(map.CurrentExtent, new ScreenPointF((float)ee.X, (float)ee.Y), (float)map.Width, (float)map.Height);
                PointShape projectedpoint = projection.ConvertToInternalProjection(worldpoint) as PointShape;
            }
            catch (Exception ex) {
                log.Error("DrawTool - map mouse move - {0}", ex);
            }
        }

        //public void EditMouseMove(object sender, MapMouseMoveInteractiveOverlayEventArgs e) {

        //}


        //public void EditMouseMove(object sender, MouseEventArgs e) {

        //}

    }
}
