﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Fields.FieldDetails.MapModels {
    public class MapUnitFactory {

        public static string AreaMeasureConversionString(ThinkGeo.MapSuite.Core.AreaUnit areaunit) {
            if (areaunit == ThinkGeo.MapSuite.Core.AreaUnit.Acres) return AgC.UnitConversion.Area.Acre.Self.Name;
            if (areaunit == ThinkGeo.MapSuite.Core.AreaUnit.Hectares) return AgC.UnitConversion.Area.Hectare.Self.Name;
            if (areaunit == ThinkGeo.MapSuite.Core.AreaUnit.SquareMeters) return AgC.UnitConversion.Area.SquareMeter.Self.Name;
            if (areaunit == ThinkGeo.MapSuite.Core.AreaUnit.SquareFeet) return AgC.UnitConversion.Area.SquareFoot.Self.Name;
            if (areaunit == ThinkGeo.MapSuite.Core.AreaUnit.SquareMiles) return AgC.UnitConversion.Area.SquareMile.Self.Name;
            return AgC.UnitConversion.Area.Acre.Self.Name;
        }

        public static AgC.UnitConversion.Area.AreaUnit MapUnitConversion(ThinkGeo.MapSuite.Core.AreaUnit areaunit) {
            if (areaunit == ThinkGeo.MapSuite.Core.AreaUnit.Acres) return AgC.UnitConversion.Area.Acre.Self;
            if (areaunit == ThinkGeo.MapSuite.Core.AreaUnit.Hectares) return AgC.UnitConversion.Area.Hectare.Self;
            if (areaunit == ThinkGeo.MapSuite.Core.AreaUnit.SquareMeters) return AgC.UnitConversion.Area.SquareMeter.Self;
            if (areaunit == ThinkGeo.MapSuite.Core.AreaUnit.SquareFeet) return AgC.UnitConversion.Area.SquareFoot.Self;
            if (areaunit == ThinkGeo.MapSuite.Core.AreaUnit.SquareMiles) return AgC.UnitConversion.Area.SquareMile.Self;
            return AgC.UnitConversion.Area.Acre.Self;
        }

        public static ThinkGeo.MapSuite.Core.AreaUnit AreaMeasureConversion(AgC.UnitConversion.Area.AreaUnit areaunit) {
            if (areaunit == AgC.UnitConversion.Area.Acre.Self) return ThinkGeo.MapSuite.Core.AreaUnit.Acres;
            if (areaunit == AgC.UnitConversion.Area.Hectare.Self) return ThinkGeo.MapSuite.Core.AreaUnit.Hectares;
            if (areaunit == AgC.UnitConversion.Area.SquareMeter.Self) return ThinkGeo.MapSuite.Core.AreaUnit.SquareMeters;
            if (areaunit == AgC.UnitConversion.Area.SquareFoot.Self) return ThinkGeo.MapSuite.Core.AreaUnit.SquareFeet;
            if (areaunit == AgC.UnitConversion.Area.SquareMile.Self) return ThinkGeo.MapSuite.Core.AreaUnit.SquareMiles;
            return ThinkGeo.MapSuite.Core.AreaUnit.Acres;
        }

        public static AgC.UnitConversion.Measure AreaConversion(string areaunitname, double areaunitvalue) {
            if (areaunitname == AgC.UnitConversion.Area.Acre.Self.Name) return AgC.UnitConversion.Area.Acre.Self.GetMeasure(areaunitvalue);
            if (areaunitname == AgC.UnitConversion.Area.Hectare.Self.Name) return AgC.UnitConversion.Area.Hectare.Self.GetMeasure(areaunitvalue);
            if (areaunitname == AgC.UnitConversion.Area.SquareMeter.Self.Name) return AgC.UnitConversion.Area.SquareMeter.Self.GetMeasure(areaunitvalue);
            if (areaunitname == AgC.UnitConversion.Area.SquareFoot.Self.Name) return AgC.UnitConversion.Area.SquareFoot.Self.GetMeasure(areaunitvalue);
            if (areaunitname == AgC.UnitConversion.Area.SquareMile.Self.Name) return AgC.UnitConversion.Area.SquareMile.Self.GetMeasure(areaunitvalue);

            if (areaunitname == ThinkGeo.MapSuite.Core.AreaUnit.Acres.ToString()) return AgC.UnitConversion.Area.Acre.Self.GetMeasure(areaunitvalue);
            if (areaunitname == ThinkGeo.MapSuite.Core.AreaUnit.Hectares.ToString()) return AgC.UnitConversion.Area.Hectare.Self.GetMeasure(areaunitvalue);
            if (areaunitname == ThinkGeo.MapSuite.Core.AreaUnit.SquareMeters.ToString()) return AgC.UnitConversion.Area.SquareMeter.Self.GetMeasure(areaunitvalue);
            if (areaunitname == ThinkGeo.MapSuite.Core.AreaUnit.SquareFeet.ToString()) return AgC.UnitConversion.Area.SquareFoot.Self.GetMeasure(areaunitvalue);
            if (areaunitname == ThinkGeo.MapSuite.Core.AreaUnit.SquareMiles.ToString()) return AgC.UnitConversion.Area.SquareMile.Self.GetMeasure(areaunitvalue);
            
            return AgC.UnitConversion.Area.Acre.Self.GetMeasure(areaunitvalue);
        }

        public static string LengthMeasureConversionString(ThinkGeo.MapSuite.Core.DistanceUnit distanceunit) {
            if (distanceunit == ThinkGeo.MapSuite.Core.DistanceUnit.Feet) return AgC.UnitConversion.Length.Foot.Self.Name;
            if (distanceunit == ThinkGeo.MapSuite.Core.DistanceUnit.Kilometer) return AgC.UnitConversion.Length.Kilometer.Self.Name;
            if (distanceunit == ThinkGeo.MapSuite.Core.DistanceUnit.Meter) return AgC.UnitConversion.Length.Meter.Self.Name;
            if (distanceunit == ThinkGeo.MapSuite.Core.DistanceUnit.Mile) return AgC.UnitConversion.Length.Mile.Self.Name;
            return AgC.UnitConversion.Length.Foot.Self.Name;
        }

        public static AgC.UnitConversion.Length.LengthUnit MapLengthUnitConversion(ThinkGeo.MapSuite.Core.DistanceUnit distanceunit) {
            if (distanceunit == ThinkGeo.MapSuite.Core.DistanceUnit.Feet) return AgC.UnitConversion.Length.Foot.Self;
            if (distanceunit == ThinkGeo.MapSuite.Core.DistanceUnit.Kilometer) return AgC.UnitConversion.Length.Kilometer.Self;
            if (distanceunit == ThinkGeo.MapSuite.Core.DistanceUnit.Meter) return AgC.UnitConversion.Length.Meter.Self;
            if (distanceunit == ThinkGeo.MapSuite.Core.DistanceUnit.Mile) return AgC.UnitConversion.Length.Mile.Self;
            return AgC.UnitConversion.Length.Foot.Self;
        }

        public static ThinkGeo.MapSuite.Core.DistanceUnit LengthMeasureConversion(AgC.UnitConversion.Length.LengthUnit distanceunit) {
            if (distanceunit == AgC.UnitConversion.Length.Foot.Self) return ThinkGeo.MapSuite.Core.DistanceUnit.Feet;
            if (distanceunit == AgC.UnitConversion.Length.Kilometer.Self) return ThinkGeo.MapSuite.Core.DistanceUnit.Kilometer;
            if (distanceunit == AgC.UnitConversion.Length.Meter.Self) return ThinkGeo.MapSuite.Core.DistanceUnit.Meter;
            if (distanceunit == AgC.UnitConversion.Length.Foot.Self) return ThinkGeo.MapSuite.Core.DistanceUnit.Mile;
            return ThinkGeo.MapSuite.Core.DistanceUnit.Feet; 
        }

        public static AgC.UnitConversion.Measure LengthConversion(string lengthunitname, double lengthunitvalue) {
            if (lengthunitname == AgC.UnitConversion.Length.Foot.Self.Name) return AgC.UnitConversion.Length.Foot.Self.GetMeasure(lengthunitvalue);
            if (lengthunitname == AgC.UnitConversion.Length.Kilometer.Self.Name) return AgC.UnitConversion.Length.Kilometer.Self.GetMeasure(lengthunitvalue);
            if (lengthunitname == AgC.UnitConversion.Length.Meter.Self.Name) return AgC.UnitConversion.Length.Meter.Self.GetMeasure(lengthunitvalue);
            if (lengthunitname == AgC.UnitConversion.Length.Foot.Self.Name) return AgC.UnitConversion.Length.Mile.Self.GetMeasure(lengthunitvalue);

            if (lengthunitname == ThinkGeo.MapSuite.Core.DistanceUnit.Feet.ToString()) return AgC.UnitConversion.Length.Foot.Self.GetMeasure(lengthunitvalue);
            if (lengthunitname == ThinkGeo.MapSuite.Core.DistanceUnit.Kilometer.ToString()) return AgC.UnitConversion.Length.Kilometer.Self.GetMeasure(lengthunitvalue);
            if (lengthunitname == ThinkGeo.MapSuite.Core.DistanceUnit.Meter.ToString()) return AgC.UnitConversion.Length.Meter.Self.GetMeasure(lengthunitvalue);
            if (lengthunitname == ThinkGeo.MapSuite.Core.DistanceUnit.Mile.ToString()) return AgC.UnitConversion.Length.Mile.Self.GetMeasure(lengthunitvalue);

            return AgC.UnitConversion.Length.Foot.Self.GetMeasure(lengthunitvalue);
        }

        public static ThinkGeo.MapSuite.Core.DistanceUnit AreaDistanceMeasureConversion(AgC.UnitConversion.Area.AreaUnit areaunit) {
            if (areaunit == AgC.UnitConversion.Area.Acre.Self) return ThinkGeo.MapSuite.Core.DistanceUnit.Feet;
            if (areaunit == AgC.UnitConversion.Area.Hectare.Self) return ThinkGeo.MapSuite.Core.DistanceUnit.Meter;
            if (areaunit == AgC.UnitConversion.Area.SquareMeter.Self) return ThinkGeo.MapSuite.Core.DistanceUnit.Meter;
            if (areaunit == AgC.UnitConversion.Area.SquareFoot.Self) return ThinkGeo.MapSuite.Core.DistanceUnit.Feet;
            if (areaunit == AgC.UnitConversion.Area.SquareMile.Self) return ThinkGeo.MapSuite.Core.DistanceUnit.Feet;
            if (areaunit == AgC.UnitConversion.Area.Are.Self) return ThinkGeo.MapSuite.Core.DistanceUnit.Meter;
            return ThinkGeo.MapSuite.Core.DistanceUnit.Feet;
        }

        public static ThinkGeo.MapSuite.Core.DistanceUnit AreaDistanceMeasureConversion(ThinkGeo.MapSuite.Core.GeographyUnit areaunit) {
            if (areaunit == ThinkGeo.MapSuite.Core.GeographyUnit.DecimalDegree) return ThinkGeo.MapSuite.Core.DistanceUnit.Feet;
            if (areaunit == ThinkGeo.MapSuite.Core.GeographyUnit.Feet) return ThinkGeo.MapSuite.Core.DistanceUnit.Feet;
            if (areaunit == ThinkGeo.MapSuite.Core.GeographyUnit.Meter) return ThinkGeo.MapSuite.Core.DistanceUnit.Meter;
            return ThinkGeo.MapSuite.Core.DistanceUnit.Feet;
        }
    }
}
