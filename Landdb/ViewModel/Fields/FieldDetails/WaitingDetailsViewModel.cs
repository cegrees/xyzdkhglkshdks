﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using Landdb.Domain.ReadModels.Tree;

namespace Landdb.ViewModel.Fields.FieldDetails {
    public class WaitingDetailsViewModel {
        readonly IIdentity itemId;
        readonly IList<MapItem> mapItems;
        readonly AbstractTreeItemViewModel treeItem;
        readonly Action<object> OnViewModelCreated;
        readonly Timer timer;
        readonly Action<IIdentity, IList<MapItem>, AbstractTreeItemViewModel, Action<object>> RefreshAction;
        static bool isAlreadyWaiting;

        public WaitingDetailsViewModel(IIdentity itemId, IList<MapItem> mapItems, AbstractTreeItemViewModel treeItem, Action<object> OnViewModelCreated, 
                                        Action<IIdentity, IList<MapItem>, AbstractTreeItemViewModel, Action<object>> RefreshAction) {
            if (!isAlreadyWaiting) {
                this.itemId = itemId;
                this.mapItems = mapItems;
                this.treeItem = treeItem;
                this.OnViewModelCreated = OnViewModelCreated;
                this.RefreshAction = RefreshAction;
                isAlreadyWaiting = true;

                timer = new Timer(400);
                timer.Elapsed += timer_Elapsed;
                timer.Start();
            }
        }

        void timer_Elapsed(object sender, ElapsedEventArgs e) {
            timer.Stop();

            RefreshAction(itemId, mapItems, treeItem, vm => {
                if (vm is WaitingDetailsViewModel) {
                    timer.Start(); // keep waiting...
                } else {
                    isAlreadyWaiting = false;
                    OnViewModelCreated(vm);
                }
            });
        }
    }
}