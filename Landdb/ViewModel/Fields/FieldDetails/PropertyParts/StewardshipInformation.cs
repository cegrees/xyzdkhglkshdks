﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Fields.FieldDetails.PropertyParts
{
    public class StewardshipInformation : ViewModelBase
    {
        IClientEndpoint clientEndpoint;
        Dispatcher dispatcher;
        Domain.ReadModels.Tree.CropZoneDetailsView czView;
        bool thisisaborderrow = false;
        //string borderrowrelationship = string.Empty;
        //string borderrowcropzonename = string.Empty;
        //string borderrowcropzonerelationship = string.Empty;
        BufferZone selectedborderrow;

        public StewardshipInformation(IClientEndpoint clientEndpoint, Dispatcher dispatcher, CropZoneDetailsView czView)
        {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            this.czView = czView;
            selectedborderrow = new BufferZone();
            thisisaborderrow = false;

            if (czView != null && czView.FieldId != null) {
                var fieldMaybe = clientEndpoint.GetView<FieldDetailsView>(new FieldId(ApplicationEnvironment.CurrentDataSourceId, czView.FieldId.Id));
                bool hasbordervalues = false;
                CanToggleBorderRows = true;
                fieldMaybe.IfValue(field => {
                    if (field.BufferZones.Count > 0) {
                        hasbordervalues = true;
                    }
                    foreach (BufferZone bufferzone in field.BufferZones) {
                        if(czView.Id == bufferzone.Id) {
                            thisisaborderrow = true;
                            //BufferzoneRelationship = bufferzone.Relationship;
                            BufferzoneRelationship = string.Format(Strings.ThisIsABufferzoneRelationshipForOtherCropzones_Text, bufferzone.Relationship);
                            break;
                        }
                    }
                });
                if (hasbordervalues) {
                    if (czView.BorderZoneId != null) {
                        CanToggleBorderRows = false;
                        var czMaybe = clientEndpoint.GetView<CropZoneDetailsView>(new CropZoneId(ApplicationEnvironment.CurrentDataSourceId, czView.BorderZoneId.Id));
                        czMaybe.IfValue(cropzone => {
                            BufferzoneRelationship = string.Format("{0}: {1}", cropzone.BorderZoneRelationship, cropzone.Name);
                        });
                    }
                }
            }


            ShowEditorCommand = new RelayCommand(ShowEditor);
            CancelCommand = new RelayCommand(Cancel);
            UpdateCommand = new RelayCommand(Update);
        }

        public ICommand ShowEditorCommand { get; private set; }
        public ICommand UpdateCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }

        public string BufferzoneRelationship { get; set; }

        public bool HasData
        {
            get
            {
                return string.IsNullOrEmpty(BufferzoneRelationship);
            }
        }

        private bool canToggleBorderRows = false;
        public bool CanToggleBorderRows
        {
            get
            {
                return canToggleBorderRows;
            }
            set
            {
                canToggleBorderRows = value;
                RaisePropertyChanged("CanToggleBorderRows");
            }
        }

        void Cancel()
        {
            //UpdateFromProjection(fieldView);
            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
        }

        void Update()
        {
            var years = clientEndpoint.GetView<FieldTreeView>(new DataSourceId(ApplicationEnvironment.CurrentDataSourceId));
            var yearItem = (years.HasValue && years.Value.CropYearList.ContainsKey(ApplicationEnvironment.CurrentCropYear)) ? years.Value.CropYearList[ApplicationEnvironment.CurrentCropYear] : new TreeViewYearItem();
            IList<TreeViewFarmItem> farms = yearItem.Farms != null ? yearItem.Farms : new List<TreeViewFarmItem>();

            var q = from f in yearItem.Farms
                    from fi in f.Fields
                    from cz in fi.CropZones
                    where fi.FieldId == czView.FieldId && cz.CropZoneId != czView.Id
                    select new { f.FarmId, fi.FieldId, cz };


            foreach (var cropZ in q) {
                IIdentity itemIdcz = null;
                if (cropZ.cz.CropZoneId == null) { continue; }
                string stringid1 = cropZ.cz.CropZoneId.Id.ToString();
                itemIdcz = cropZ.cz.CropZoneId;
                if (thisisaborderrow) {
                    BufferzoneRelationship = string.Empty;
                    RemoveRelatedZone removerelatedzonecommand = new RemoveRelatedZone(cropZ.cz.CropZoneId, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), czView.Id);
                    clientEndpoint.SendOne(removerelatedzonecommand);
                }
                else {
                    BufferzoneRelationship = Strings.BorderRow_Text;
                    AddRelatedZone addrelatedzonecommand = new AddRelatedZone(cropZ.cz.CropZoneId, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), czView.Id, BufferzoneRelationship);
                    clientEndpoint.SendOne(addrelatedzonecommand);
                }
            }

            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
            RaisePropertyChanged("HasData");
        }

        void ShowEditor()
        {
            Messenger.Default.Send<ShowPopupMessage>(new ShowPopupMessage() { ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Fields.Popups.EditStewardshipView", "editStewardship", this) });
        }
    }
}
