﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;

namespace Landdb.ViewModel.Fields.FieldDetails.PropertyParts
{
    public class FsaAreaInformation : ViewModelBase
    {
        IClientEndpoint clientEndpoint;
        Dispatcher dispatcher;
        Domain.ReadModels.Tree.CropZoneDetailsView czView;

        public FsaAreaInformation(IClientEndpoint clientEndpoint, Dispatcher dispatcher, CropZoneDetailsView czView)
        {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            this.czView = czView;
            FsaArea = czView.FsaArea;

            ShowEditorCommand = new RelayCommand(ShowEditor);
            CancelCommand = new RelayCommand(Cancel);
            UpdateCommand = new RelayCommand(Update);
        }

        public ICommand ShowEditorCommand { get; private set; }
        public ICommand UpdateCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }

        public double? FsaArea { get; set; }

        public bool HasData
        {
            get
            {
                return FsaArea.HasValue;
            }
        }
        void Cancel()
        {
            //UpdateFromProjection(fieldView);
            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
        }

        void Update()
        {
            if (FsaArea != czView.FsaArea)
            {
                EditCropZoneFsaArea infoCommand = new EditCropZoneFsaArea(czView.Id, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), FsaArea);
                clientEndpoint.SendOne(infoCommand);
            }

            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
            RaisePropertyChanged("HasData");
        }

        void ShowEditor()
        {
            Messenger.Default.Send<ShowPopupMessage>(new ShowPopupMessage() { ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Fields.Popups.EditFsaAreaView", "editFsaArea", this) });
        }
    }
}
