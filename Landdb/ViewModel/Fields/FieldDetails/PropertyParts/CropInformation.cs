﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.Infrastructure;
using Landdb.Domain.ReadModels.Company;
using System.Collections.ObjectModel;
using Landdb.Resources;

namespace Landdb.ViewModel.Fields.FieldDetails.PropertyParts {
    public class CropInformation : ViewModelBase {
        IClientEndpoint clientEndpoint;
        Dispatcher dispatcher;

        Landdb.Domain.ReadModels.Tree.CropZoneDetailsView czReadModel;
        CropZoneDetailsViewModel czViewModel;

        string seedLotId;
        string source;
        string crop;
        string plannedVariety;
        string actualVariety;
        string insuranceId;
        MiniCrop newItemCropSelection;

        public CropInformation(IClientEndpoint clientEndpoint, Dispatcher dispatcher, CropZoneDetailsViewModel czViewModel) {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            this.czReadModel = czViewModel.CropZoneView;
            this.czViewModel = czViewModel;

            ShowEditorCommand = new RelayCommand(ShowEditor);
            CancelCommand = new RelayCommand(Cancel);
            UpdateCommand = new RelayCommand(Update);

            UpdateFromProjection(czReadModel);
        }

        public ICommand ShowEditorCommand { get; private set; }
        public ICommand UpdateCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }

        public string SeedLotId {
            get { return seedLotId; }
            set {
                seedLotId = value;
                RaisePropertyChanged("SeedLotId");
            }
        }

        public string InsuranceId {
            get { return insuranceId; }
            set {
                insuranceId = value;
                RaisePropertyChanged("InsuranceId");
            }
        }

        public List<CompanyListItem> Companies { get; set; }
        public CompanyListItem Company { get; set; }

        public string Source {
            get { return source; }
            set {
                source = value;
                RaisePropertyChanged("Source");
            }
        }

        public IEnumerable<MiniCrop> CropList {
            get { return clientEndpoint.GetMasterlistService().GetCropList().OrderBy(x => x.Name); }
        }

        public MiniCrop NewItemCropSelection {
            get { return newItemCropSelection; }
            set {

                var oldName = newItemCropSelection != null ? newItemCropSelection.Name : string.Empty;

                newItemCropSelection = value;

                if (value != null && (string.IsNullOrWhiteSpace(Crop) || Crop == oldName)) {
                    Crop = value.Name;
                    RaisePropertyChanged("Crop");
                }
            }
        }

        public string Crop {
            get { return crop; }
            set {
                crop = value;
                RaisePropertyChanged("Crop");
            }
        }

        public string PlannedVariety {
            //Get Planned Variety from Crop Plan
            get { return plannedVariety; }
        }

        public string ActualVariety {
            //Get Actual Variety From Applied Product
            get { return actualVariety; }
        }

        public string WarningMessage { get; set; }

        public bool HasData {
            get {
                return SeedLotId != string.Empty || Crop != string.Empty || PlannedVariety != string.Empty || ActualVariety != string.Empty;
            }
        }

        void UpdateFromProjection(Domain.ReadModels.Tree.CropZoneDetailsView czView) {
            DataSourceId dsID = new DataSourceId(ApplicationEnvironment.CurrentDataSourceId);
			var currentCropYear = ApplicationEnvironment.CurrentCropYear;

            // Get companies
			var companyListView = clientEndpoint.GetView<CompanyListView>(dsID).GetValue(new CompanyListView());
			var sortedFilteredCompanies = from c in companyListView.Companies
										  where c.IsActiveInCropYear(currentCropYear)
										  orderby c.Name
										  select c;

			Companies = sortedFilteredCompanies.ToList();

            SeedLotId = czView.SeedLotId;
            Source = czView.SeedSource;
            Company = czView.SeedSource != null && Companies != null ? Companies.Where(x => x.Name == czView.SeedSource).FirstOrDefault() : null;
            Crop = clientEndpoint.GetMasterlistService().GetCropDisplay(czView.CropId);
            InsuranceId = czView.InsuranceId;
            if (czView.CropId != null)
                NewItemCropSelection = clientEndpoint.GetMasterlistService().GetCropList().Where(x => x.Id == czView.CropId.Id).SingleOrDefault();
        }

        bool IsInfoChanged() {
            return InsuranceId != czReadModel.InsuranceId
                || SeedLotId != czReadModel.SeedLotId
                || (Company != null && Company.Name != czReadModel.SeedSource)
                || (Company == null && czReadModel.SeedSource != null)
                || (czReadModel.CropId != null && NewItemCropSelection.Id != czReadModel.CropId.Id)
                || (czReadModel.CropId == null && NewItemCropSelection != null);
        }


        void Cancel() {
            UpdateFromProjection(czReadModel);
            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
        }

        async void Update() {
            if (newItemCropSelection == null)
            {
                WarningMessage = $"{Strings.ExclamationPointTwo_Text}{Strings.ExclamationPointOne_Text} {Strings.InvalidCrop_Text} {Strings.ExclamationPointOne_Text}{Strings.ExclamationPointTwo_Text}";
                RaisePropertyChanged(() => WarningMessage);
            }
            else
            {
                if (IsInfoChanged())
                {

                    var companyName = Company != null ? Company.Name : string.Empty;
                    RenameCropZone renameCommand = null;

                    if (czReadModel.Name == clientEndpoint.GetMasterlistService().GetCropDisplay(czReadModel.CropId) && czReadModel.CropId != new CropId(NewItemCropSelection.Id))
                    {
                        // ask if we should also rename the field.
                        await DialogFactory.ShowTwoOptionDialog(Strings.UpdateName_Text, Strings.CropzoneNamedAfterCropShouldWeRename_Text,
                            Strings.Rename_Text, () =>
                            {
                                renameCommand = new RenameCropZone(czReadModel.Id, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), NewItemCropSelection.Name, ApplicationEnvironment.CurrentCropYear, string.IsNullOrEmpty(czReadModel.CustomIntegrationId) ? string.Empty : czReadModel.CustomIntegrationId);
                            },
                            Strings.KeepExistingName_Text, () => { /* do nothing */ });
                    }

                    UpdateCropZoneCropInformation infoCommand = new UpdateCropZoneCropInformation(czReadModel.Id, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), ApplicationEnvironment.CurrentCropYear, SeedLotId, companyName, new CropId(NewItemCropSelection.Id), InsuranceId);
                    clientEndpoint.SendOne(infoCommand);

                    if (renameCommand != null)
                    {
                        clientEndpoint.SendOne(renameCommand);
                        czViewModel.Name = renameCommand.NewName;
                    }
                }

                Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
                RaisePropertyChanged("HasData");
            }
        }
        void ShowEditor() {
            Messenger.Default.Send<ShowPopupMessage>(new ShowPopupMessage() { ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Fields.Popups.UpdateCropInformationView", "editCropInfo", this) });
        }
    }
}
