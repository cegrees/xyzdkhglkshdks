﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using System.Windows.Threading;

namespace Landdb.ViewModel.Fields.FieldDetails.PropertyParts {
	public class LegalInformation : ViewModelBase {

		readonly IClientEndpoint clientEndpoint;
		readonly Dispatcher dispatcher;

		Meridian meridian;
		string township;
		string range;
		string section;
		string shortDesc;
		string legalDesc;
		string parcelNumber;
		string taxID;
		string townshipDirection;
		string rangeDirection;

		FieldDetailsView fieldView;
		int currentCropYear;

		public LegalInformation(IClientEndpoint clientEndpoint, Dispatcher dispatcher, FieldDetailsView fieldView, int currentCropYear) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;
			this.fieldView = fieldView;
			this.currentCropYear = currentCropYear;

            Meridians = clientEndpoint.GetMasterlistService().GetDominionList().ToList();

            if (ApplicationEnvironment.CurrentDataSource.DatasourceCulture.ToLower() == "en-us")
            {
                Meridians = clientEndpoint.GetMasterlistService().GetMeridianList().ToList();
            }
            else
            {
                var dominionsList = clientEndpoint.GetMasterlistService().GetDominionList();
            }
			Meridians.Insert(0, new Landdb.Meridian() { Name = string.Empty });

			RangeDirections = new List<string>() { string.Empty, "E", "W", };
			TownshipDirections = new List<string>() { string.Empty, "N", "S", };

			updateFromProjection(fieldView);

			ShowEditorCommand = new RelayCommand(onShowEditor);
			CancelCommand = new RelayCommand(onCancel);
			UpdateCommand = new RelayCommand(onUpdate);
		}

		public ICommand ShowEditorCommand { get; private set; }
		public ICommand UpdateCommand { get; private set; }
		public ICommand CancelCommand { get; private set; }

		public List<Meridian> Meridians { get; private set; }

		public IEnumerable<string> RangeDirections { get; private set; }
		public IEnumerable<string> TownshipDirections { get; private set; }

		public Meridian Meridian {
			get { return meridian; }
			set {
				meridian = value;
				RaisePropertyChanged("Meridian");
			}
		}

		public string Section {
			get { return section; }
			set {
				section = value;
				RaisePropertyChanged("Section");
			}
		}

		public string Township {
			get { return township; }
			set {
				township = value;
				RaisePropertyChanged("Township");
			}
		}

		public string TownshipDirection {
			get { return townshipDirection; }
			set {
				townshipDirection = value;
				RaisePropertyChanged("TownshipDirection");
			}
		}

		public string TownshipDisplay {
			get { return Township + " " + TownshipDirection; }
		}

		public string Range {
			get { return range; }
			set {
				range = value;
				RaisePropertyChanged("Range");
			}
		}

		public string RangeDirection {
			get { return rangeDirection; }
			set {
				rangeDirection = value;
				RaisePropertyChanged("RangeDirection");
			}
		}

		public string RangeDisplay {
			get { return Range + " " + RangeDirection; }
		}

		public string LegalDescription {
			get { return legalDesc; }
			set {
				legalDesc = value;
				RaisePropertyChanged("LegalDescription");
			}
		}

		public string ShortDescription {
			get { return shortDesc; }
			set {
				shortDesc = value;
				RaisePropertyChanged("ShortDescription");
			}
		}

		public string PropertyTaxId {
			get { return taxID; }
			set {
				taxID = value;
				RaisePropertyChanged("PropertyTaxId");
			}
		}

		public string ParcelNumber {
			get { return parcelNumber; }
			set {
				parcelNumber = value;
				RaisePropertyChanged("ParcelNumber");
			}
		}

		public bool HasData {
			get {
				return !string.IsNullOrWhiteSpace(Township)
					|| !string.IsNullOrWhiteSpace(Range)
					|| !string.IsNullOrWhiteSpace(Section)
					|| !string.IsNullOrWhiteSpace(GetMeridianName())
					|| !string.IsNullOrWhiteSpace(PropertyTaxId)
					|| !string.IsNullOrWhiteSpace(ParcelNumber)
					|| !string.IsNullOrWhiteSpace(TownshipDirection)
					|| !string.IsNullOrWhiteSpace(RangeDirection)
					|| !string.IsNullOrWhiteSpace(ShortDescription)
					|| !string.IsNullOrWhiteSpace(LegalDescription);
			}
		}

		public bool HasChanges() {
			return GetMeridianName() != fieldView.Meridian
				|| Township != fieldView.Township
				|| TownshipDirection != fieldView.TownshipDirection
				|| Range != fieldView.Range
				|| RangeDirection != fieldView.RangeDirection
				|| Section != fieldView.Section
				|| PropertyTaxId != fieldView.PropertyTaxId
				|| ParcelNumber != fieldView.ParcelNumber
				|| ShortDescription != fieldView.ShortDescription
				|| LegalDescription != fieldView.LegalDescription;
		}

		private void updateFromProjection(Domain.ReadModels.Tree.FieldDetailsView fieldView) {
            if (ApplicationEnvironment.CurrentDataSource.DatasourceCulture.ToLower() == "en-us")
            {
                Meridian = clientEndpoint.GetMasterlistService().GetMeridianByName(fieldView.Meridian);
            }
            else
            {
                Meridian = clientEndpoint.GetMasterlistService().GetDominionByName(fieldView.Meridian);
            }
			Township = fieldView.Township;
			TownshipDirection = fieldView.TownshipDirection;
			Range = fieldView.Range;
			RangeDirection = fieldView.RangeDirection;
			Section = fieldView.Section;
			PropertyTaxId = fieldView.PropertyTaxId;
			ParcelNumber = fieldView.ParcelNumber;
			ShortDescription = fieldView.ShortDescription;
			LegalDescription = fieldView.LegalDescription;

			RaisePropertyChanged("Meridian");
		}

		private bool isShortDescriptionChanged() {
			return ShortDescription != fieldView.ShortDescription;
		}

		private bool isLegalDescriptionChanged() {
			return LegalDescription != fieldView.LegalDescription;
		}

		private string GetMeridianName() {
			string name = null;

			if (Meridian != null && !string.IsNullOrWhiteSpace(Meridian.Name)) {
				name = Meridian.ToString();
			}

			return name;
		}

		private void onUpdate() {
			if (HasChanges()) {
				var infoCommand = new UpdateFieldLegalInformation(
					fieldView.Id,
					clientEndpoint.GenerateNewMetadata(),
					GetMeridianName(),
					Township,
					TownshipDirection,
					Range,
					RangeDirection,
					Section,
					PropertyTaxId,
					ParcelNumber,
					currentCropYear
				);
				clientEndpoint.SendOne(infoCommand);
			}

			if (isShortDescriptionChanged()) {
				var shortCommand = new UpdateFieldShortDescription(fieldView.Id, clientEndpoint.GenerateNewMetadata(), ShortDescription, currentCropYear);
				clientEndpoint.SendOne(shortCommand);
			}

			if (isLegalDescriptionChanged()) {
				var legalCommand = new UpdateFieldLegalDescription(fieldView.Id, clientEndpoint.GenerateNewMetadata(), LegalDescription, currentCropYear);
				clientEndpoint.SendOne(legalCommand);
			}

			Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
			RaisePropertyChanged("HasData");
		}

		private void onCancel() {
			updateFromProjection(fieldView);
			Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
		}

		private void onShowEditor() {
			Messenger.Default.Send<ShowPopupMessage>(new ShowPopupMessage() {
				ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Fields.Popups.UpdateLegalLandInformationView", "editFsa", this)
			});
		}
	}
}