﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Resources;
using Landdb.ViewModel.Secondary.Map;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Fields.FieldDetails.PropertyParts {
	public class FieldLocation : ViewModelBase {

		readonly IClientEndpoint clientEndpoint;
		readonly Dispatcher dispatcher;
		readonly FieldDetailsView fieldView;
		MapSettings mapsettings;

		public FieldLocation(IClientEndpoint clientEndpoint, Dispatcher dispatcher, FieldDetailsView fieldView) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;
			this.fieldView = fieldView;
			this.mapsettings = clientEndpoint.GetMapSettings();

			var currentCropYearId = new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear);
			LandOwnerList = getLandOwnerList(currentCropYearId);

			updateFromProjection(fieldView);

			ShowEditorCommand = new RelayCommand(onShowEditor);
			CancelCommand = new RelayCommand(onCancel);
			UpdateCommand = new RelayCommand(onUpdate);
		}

		public ICommand ShowEditorCommand { get; }
		public ICommand UpdateCommand { get; }
		public ICommand CancelCommand { get; }

		public ICollectionView LandOwnerList { get; }

		public IEnumerable<State> StateList {
			get { return clientEndpoint.GetMasterlistService().GetStateList(); }
		}

		public IEnumerable<County> CountyList {
			get {
				if (State == null) {
					return new List<County>();
				} else {
					return State.Counties;
				}
			}
		}

		private State state;
		public State State {
			get { return state; }
			set {
				state = value;
				RaisePropertyChanged(() => State);
				RaisePropertyChanged(() => CountyList);
			}
		}

		private County county;
		public County County {
			get { return county; }
			set {
				county = value;
				RaisePropertyChanged(() => County);
			}
		}

		private string center;
		public string CenterLatLong {
			get { return center; }
			set {
				center = value;
				RaisePropertyChanged(() => CenterLatLong);
				RaisePropertyChanged(() => FormatedCenterLatLong);
			}
		}

		public string FormatedCenterLatLong {
			get {
				return DecimalDegrees.ddstring_To_DMs(center, mapsettings.DisplayCoordinateFormat);
			}
		}

		private ContractorListItemViewModel selectedLandOwner;
		public ContractorListItemViewModel SelectedLandOwnerListItem {
			get { return selectedLandOwner; }
			set {
				selectedLandOwner = value;
				RaisePropertyChanged(() => SelectedLandOwnerListItem);
			}
		}

		public LandOwner LandOwner {
			get {
				if (SelectedLandOwnerListItem == null) { return null; }

				if (SelectedLandOwnerListItem.Group == Strings.People_Text) {
					return new LandOwner(SelectedLandOwnerListItem.PersonId, SelectedLandOwnerListItem.Name, SelectedLandOwnerListItem.PhoneNumber);
				} else if (SelectedLandOwnerListItem.Group == Strings.Companies_Text) {
					return new LandOwner(SelectedLandOwnerListItem.CompanyId, SelectedLandOwnerListItem.Name, SelectedLandOwnerListItem.PhoneNumber);
				} else {
					return null;
				}
			}
		}

		public Visibility IsSelected {
			get { return Visibility.Hidden; }
		}

		public bool HasData {
			get {
				return !string.IsNullOrWhiteSpace(CenterLatLong)
					|| State != null
					|| County != null
					|| SelectedLandOwnerListItem != null;
			}
		}

		public bool HasChanges {
			get {
				var stateName = State != null ? State.Name : null;
				var countyName = County != null ? County.Name : null;

				return stateName != fieldView.State
					|| countyName != fieldView.County
					|| CenterLatLong != fieldView.CenterLatLong
					|| LandOwner != fieldView.LandOwner;
			}
		}

		private void updateFromProjection(FieldDetailsView fieldView) {
			State = clientEndpoint.GetMasterlistService().GetStateByName(fieldView.State);
			County = clientEndpoint.GetMasterlistService().GetCountyByStateAndName(fieldView.County, fieldView.State);
			CenterLatLong = fieldView.CenterLatLong;

			if (fieldView.LandOwner != null) {
				if (fieldView.LandOwner.PersonId != null) {
					SelectedLandOwnerListItem = LandOwnerList.Cast<ContractorListItemViewModel>().FirstOrDefault(x => x.PersonId == fieldView.LandOwner.PersonId);
				} else if (fieldView.LandOwner.CompanyId != null) {
					SelectedLandOwnerListItem = LandOwnerList.Cast<ContractorListItemViewModel>().FirstOrDefault(x => x.CompanyId == fieldView.LandOwner.CompanyId);
				}
			}
		}

		private ICollectionView getLandOwnerList(CropYearId currentCropYearId) {
			var landOwnerList = ContractorListItemViewModel.GetListItemsForCropYear(clientEndpoint, currentCropYearId, false);
			var collectionView = CollectionViewSource.GetDefaultView(landOwnerList.OrderBy(x => x.Group).ThenBy(x => x.Name));
			collectionView.GroupDescriptions.Add(new PropertyGroupDescription("Group"));
			return collectionView;
		}

		private void onCancel() {
			updateFromProjection(fieldView);
			Messenger.Default.Send(new HidePopupMessage());
		}

		private void onUpdate() {
			if (HasChanges) {
				var stateName = State != null ? State.Name : null;
				var countyName = County != null ? County.Name : null;

				UpdateFieldLocation cmd = new UpdateFieldLocation(
					fieldView.Id,
					clientEndpoint.GenerateNewMetadata(),
					CenterLatLong,
					stateName,
					countyName,
					LandOwner
				);

				clientEndpoint.SendOne(cmd);
			}

			Messenger.Default.Send(new HidePopupMessage());
			RaisePropertyChanged(() => HasData);
		}

		private void onShowEditor() {
			Messenger.Default.Send(new ShowPopupMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Fields.Popups.UpdateFieldLocationView), "editFsa", this)
			});
		}
	}
}