﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.Infrastructure;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Resources;

namespace Landdb.ViewModel.Fields.FieldDetails.PropertyParts {
    public class FsaInformation : ViewModelBase {
        IClientEndpoint clientEndpoint;
        Dispatcher dispatcher;

        string farmNumber;
        string fieldNumber;
        string tractNumber;
        string cluID;
        double area;
        double landlordPercent;
        double tenantPercent;
        List<string> rentType = new List<string>();
        string type;
        private Domain.ReadModels.Tree.FieldDetailsView fieldView;
        int currentCropYear;

        public FsaInformation(IClientEndpoint clientEndpoint, Dispatcher dispatcher, Domain.ReadModels.Tree.FieldDetailsView fieldView, int currentCropYear) {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            this.fieldView = fieldView;
            this.currentCropYear = currentCropYear;

            rentType.Add("");
            rentType.Add(Strings.CashRent_Text);
            rentType.Add(Strings.CropShare_Text);

            UpdateFromProjection(fieldView);
            InheritDataFromParent(fieldView.Id);

            ShowEditorCommand = new RelayCommand(ShowEditor);
            CancelCommand = new RelayCommand(Cancel);
            UpdateCommand = new RelayCommand(Update);
        }

        public ICommand ShowEditorCommand { get; private set; }
        public ICommand UpdateCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }

        public List<string> Type {
            get { return rentType; }
        }
        public string FieldName {
            get { return fieldView.FieldNameByCropYear[currentCropYear]; } //.Name; }
        }

        public string FarmNumber {
            get { return farmNumber; }
            set {
                farmNumber = value;
                RaisePropertyChanged(() => FarmNumber);
            }
        }

        bool isFarmNumberInherited;
        public bool IsFarmNumberInherited {
            get { return isFarmNumberInherited; }
            set {
                isFarmNumberInherited = value;
                RaisePropertyChanged(() => IsFarmNumberInherited);
            }
        }

        public string FieldNumber {
            get { return fieldNumber; }
            set {
                fieldNumber = value;
                RaisePropertyChanged(() => FieldNumber);
            }
        }

        public string TractNumber {
            get { return tractNumber; }
            set {
                tractNumber = value;
                RaisePropertyChanged(() => TractNumber);
            }
        }

        public string CluID {
            get { return cluID; }
            set {
                cluID = value;
                RaisePropertyChanged(() => CluID);
            }
        }

        public double Area {
            get { return area; }
            set {
                area = value;
                RaisePropertyChanged(() => Area);
            }
        }

        public string RentType {
            get { return type; }
            set {
                if (value != null) {
                    type = value.ToString();
                    RaisePropertyChanged(() => RentType);
                }
            }
        }

        public double LandlordPercent {
            get { return landlordPercent; }
            set {
                landlordPercent = value;
                RaisePropertyChanged(() => LandlordPercent);
            }
        }

        public double TenantPercent {
            get { return tenantPercent; }
            set {
                tenantPercent = value;
                RaisePropertyChanged(() => TenantPercent);
            }
        }

        public bool HasData {
            get {
                return !string.IsNullOrWhiteSpace(FarmNumber) || !string.IsNullOrWhiteSpace(FieldNumber) || !string.IsNullOrWhiteSpace(TractNumber) || !string.IsNullOrWhiteSpace(RentType) || TenantPercent > 0 || LandlordPercent > 0;
            }
        }

        void UpdateFromProjection(Domain.ReadModels.Tree.FieldDetailsView fieldView) {
            FarmNumber = fieldView.FsaFarmNumber;
            FieldNumber = fieldView.FsaFieldNumber;
            TractNumber = fieldView.FsaTractNumber;
            Area = fieldView.FsaArea.HasValue ? fieldView.FsaArea.Value : 0.0;
            CluID = fieldView.CluID;
            RentType = fieldView.RentType;
            LandlordPercent = fieldView.LandLordPercent;
            TenantPercent = fieldView.TenantPercent;
        }

        private void InheritDataFromParent(FieldId fieldId) {
            if (!string.IsNullOrWhiteSpace(FarmNumber)) { return; } // prevent lookup if we already have a value.

            var flattened = clientEndpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear)).Value;
            var hierarchyItem = flattened.Items.FirstOrDefault(x => x.FieldId == fieldId);
            if (hierarchyItem == null) { return; }
            var farmId = hierarchyItem.FarmId;
            var farmView = clientEndpoint.GetView<Landdb.Domain.ReadModels.Tree.FarmDetailsView>(farmId);

            if (farmView.HasValue) {
                FarmNumber = farmView.Value.FsaFarmNumber;
                IsFarmNumberInherited = true;
            }
        }

        bool IsInfoChanged() {
            return FarmNumber != fieldView.FsaFarmNumber || FieldNumber != fieldView.FsaFieldNumber || TractNumber != fieldView.FsaTractNumber || Area != fieldView.FsaArea || CluID != fieldView.CluID || RentType != fieldView.RentType || LandlordPercent != fieldView.LandLordPercent || TenantPercent != fieldView.TenantPercent;
        }

        bool IsAreaChanged() {
            return Area != fieldView.FsaArea;
        }

        void Cancel() {
            UpdateFromProjection(fieldView);
            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
        }

        void Update() {
            if (IsInfoChanged()) {
                UpdateFieldFsaIdentification infoCommand = new UpdateFieldFsaIdentification(fieldView.Id, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), FarmNumber, TractNumber, FieldNumber, CluID, RentType, TenantPercent, LandlordPercent, currentCropYear);
                clientEndpoint.SendOne(infoCommand);

                if (!string.IsNullOrWhiteSpace(FarmNumber)) {
                    IsFarmNumberInherited = false;
                }
            }

            //TO DO :: Actually set the correct Unit for Area
            if (IsAreaChanged()) {
                UpdateFieldFsaArea areaCommand = new UpdateFieldFsaArea(fieldView.Id, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), Area, ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit, currentCropYear);
                clientEndpoint.SendOne(areaCommand);
            }

            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
            RaisePropertyChanged("HasData");
        }

        void ShowEditor() {
            Messenger.Default.Send<ShowPopupMessage>(new ShowPopupMessage() { ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Fields.Popups.UpdateFsaInformationView", "editFsa", this) });
        }
    }
}