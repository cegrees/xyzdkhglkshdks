﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using Landdb.Domain.ReadModels.Tree;

namespace Landdb.ViewModel.Fields.FieldDetails.PropertyParts
{
    public class CropHistoryListItemViewModel : ViewModelBase
    {
        FieldId fieldId;
        CropId cropId;
        string crop;
        int cropYear;
        string tillage;
        double area;
        string areaUnit;
        bool created;
        Guid trackingId;

        public CropHistoryListItemViewModel(FieldCropHistory fch)
        {
            FieldId = fch.FieldId;
            CropId = fch.CropId;
            Crop = fch.Crop;
            Area = fch.AreaValue;
            AreaUnit = fch.AreaUnit;
            Tillage = fch.Tillage;
            CropYear = fch.CropYear;
            Created = fch.Created;
            TrackingId = fch.TrackingId;
        }

        public CropHistoryListItemViewModel(CropHistoryListItem li, string licrop, double liareavalue, string liareaunit, CropZoneId czId) {
            FieldId = li.FieldID;
            CropId = li.CropID;
            Crop = licrop;
            Area = liareavalue;
            AreaUnit = liareaunit;
            Tillage = li.TillagePractice;
            CropYear = li.CropYear;
            Created = true;
            TrackingId = Guid.Empty;
            CropZoneID = czId;
        }

        public CropZoneId CropZoneID { get; set; }

        public FieldId FieldId
        {
            get { return fieldId; }
            set
            {
                fieldId = value;
                RaisePropertyChanged("FieldId");
            }
        }

        public Guid TrackingId
        {
            get { return trackingId; }
            set
            {
                trackingId = value;
            }
        }

        public CropId CropId
        {
            get { return cropId; }
            set {
                cropId = value;
                RaisePropertyChanged("CropId");
            }
        }

        public string Crop
        {
            get { return crop; }
            set {
                crop = value;
                RaisePropertyChanged("Crop");
            }
        }

        public double Area
        {
            get { return area; }
            set {
                area = value;
                RaisePropertyChanged("Area");
            }
        }

        public string AreaUnit
        {
            get { return areaUnit; }
            set {
                areaUnit = value;
                RaisePropertyChanged("AreaUnit");
            }
        }

        public string Tillage
        {
            get { return tillage; }
            set {
                tillage = value;
                RaisePropertyChanged("Tillage");
            }
        }

        public int CropYear
        {
            get { return cropYear; }
            set {
                cropYear = value;
                RaisePropertyChanged("CropYear");
            }
        }

        public System.Windows.Media.Brush TextColor
        {
            get
            {
                if (Created)
                {
                    return System.Windows.Media.Brushes.Green;
                }
                else
                {
                    return System.Windows.Media.Brushes.Black;
                }
            }
        }

        public bool Created
        {
            get
            {
                return created;
            }
            set
            {
                created = value;
            }
        }
    }
}
