﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.Views.Fields.FieldDetails;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;

namespace Landdb.ViewModel.Fields.FieldDetails.PropertyParts
{
    public class PermitInformation : ViewModelBase
    {
        IClientEndpoint clientEndpoint;
        Dispatcher dispatcher;

        string permitID;
        string farmPermitId;
        string siteID;
        string czPermitID;
        private Domain.ReadModels.Tree.FieldDetailsView fieldView;
        private Domain.ReadModels.Tree.CropZoneDetailsView czView;
        bool isCZ;
        int currentCropYear;

        public PermitInformation(IClientEndpoint clientEndpoint, Dispatcher dispatcher, Domain.ReadModels.Tree.FieldDetailsView fieldView, int currentCropYear)
        {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            this.fieldView = fieldView;
            this.currentCropYear = currentCropYear;

            IsCropZone = false;

            
            var flattened = clientEndpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear)).Value;
            var farmId = flattened.Items.Any(x => x.FieldId == fieldView.Id) ? flattened.Items.FirstOrDefault(x => x.FieldId == fieldView.Id).FarmId : null;
            if (farmId != null)
            {
                var farmView = clientEndpoint.GetView<Landdb.Domain.ReadModels.Tree.FarmDetailsView>(farmId);

                if (farmView.HasValue)
                {
                    farmPermitId = farmView.Value.PermitId;
                }
            }

            UpdateFromProjection(fieldView);

            ShowEditorCommand = new RelayCommand(ShowEditor);
            CancelCommand = new RelayCommand(Cancel);
            UpdateCommand = new RelayCommand(Update);
        }

        public PermitInformation(IClientEndpoint clientEndpoint, Dispatcher dispatcher, Domain.ReadModels.Tree.CropZoneDetailsView czView)
        {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            this.czView = czView;

            IsCropZone = true;

            var flattened = clientEndpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear)).Value;
            var farmId = flattened.Items.FirstOrDefault(x => x.CropZoneId == czView.Id).FarmId;
            var farmView = clientEndpoint.GetView<Landdb.Domain.ReadModels.Tree.FarmDetailsView>(farmId);

            if (farmView.HasValue)
            {
                farmPermitId = farmView.Value.PermitId;
            }

            UpdateFromProjection(czView);

            var fieldMaybe = clientEndpoint.GetView<Domain.ReadModels.Tree.FieldDetailsView>(czView.FieldId);
            fieldMaybe.IfValue(field =>
            {
                if (czView.PermitId == null) { PermitID = field.PermitId; }
                if (czView.SiteId == null) { SiteID = field.SiteIdsByCropYear.ContainsKey(currentCropYear) ? field.SiteIdsByCropYear[currentCropYear] : null; }
            });

            PermitID = farmPermitId != null && string.IsNullOrEmpty(PermitID) ? farmPermitId : PermitID;

            ShowEditorCommand = new RelayCommand(ShowEditor);
            CancelCommand = new RelayCommand(Cancel);
            UpdateCommand = new RelayCommand(Update);

        }

        public ICommand ShowEditorCommand { get; private set; }
        public ICommand UpdateCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }

        public bool IsCropZone
        {
            get { return isCZ; }
            set
            {
                isCZ = value;
                RaisePropertyChanged("IsCropZone");
            }
        }

        public string PermitID
        {
            get { return permitID; }
            set 
            { 
                permitID = value;
                RaisePropertyChanged("PermitID");
            }
        }

        public string SiteID
        {
            get { return siteID; }
            set
            {
                siteID = value;
                RaisePropertyChanged("SiteID");
            }
        }

        public string CropZonePermitID
        {
            get { return czPermitID; }
            set
            {
                czPermitID = value;
                RaisePropertyChanged("CropZonePermitID");
            }
        }

        void UpdateFromProjection(Domain.ReadModels.Tree.FieldDetailsView fieldView)
        {
            PermitID = farmPermitId != null && string.IsNullOrEmpty(fieldView.PermitId) ? farmPermitId : fieldView.PermitId;
			SiteID = fieldView.SiteIdsByCropYear.ContainsKey(currentCropYear) ? fieldView.SiteIdsByCropYear[currentCropYear] : string.Empty;
            CropZonePermitID = fieldView.CropZonePermitId;
        }

        void UpdateFromProjection(Domain.ReadModels.Tree.CropZoneDetailsView czView)
        {
            PermitID = farmPermitId != null && string.IsNullOrEmpty(czView.PermitId) ? farmPermitId : czView.PermitId;
            SiteID = czView.SiteId;
            CropZonePermitID = czView.CropZonePermitId;
        }

        public bool HasData
        {
            get
            {
                return !string.IsNullOrWhiteSpace(PermitID) || !string.IsNullOrWhiteSpace(SiteID) || !string.IsNullOrWhiteSpace(CropZonePermitID);
            }
        }

        bool IsInfoChanged()
        {
            if (IsCropZone)
            {
                return PermitID != czView.PermitId || SiteID != czView.SiteId || CropZonePermitID != czView.CropZonePermitId;
            }
            else
            {
                return PermitID != fieldView.PermitId || SiteID != (fieldView.SiteIdsByCropYear.ContainsKey(currentCropYear) ? fieldView.SiteIdsByCropYear[currentCropYear] : null);
            }
        }

        void Cancel()
        {
            if (IsCropZone)
            {
                UpdateFromProjection(czView);
            }
            else
            {
                UpdateFromProjection(fieldView);
            }
            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
        }

        void Update()
        {
            if (IsInfoChanged())
            {
                if (IsCropZone)
                {
                    UpdateCropZoneCaliforniaPermitInformation czCommand = new UpdateCropZoneCaliforniaPermitInformation(czView.Id, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), PermitID, SiteID, CropZonePermitID);
                    clientEndpoint.SendOne(czCommand);
                }
                else
                {
                    UpdateFieldCaliforniaPermitInformation infoCommand = new UpdateFieldCaliforniaPermitInformation(fieldView.Id, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), PermitID, SiteID, CropZonePermitID, currentCropYear);
                    clientEndpoint.SendOne(infoCommand);
                }
            }

            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
            RaisePropertyChanged("HasData");
        }
        void ShowEditor()
        {
            Messenger.Default.Send<ShowPopupMessage>(new ShowPopupMessage() { ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Fields.Popups.UpdateCaliforniaPermitInformationView", "editFsa", this) });
        }
    }
}
