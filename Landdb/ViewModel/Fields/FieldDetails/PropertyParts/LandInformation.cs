﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using AgC.UnitConversion;
using NLog;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.Resources;

namespace Landdb.ViewModel.Fields.FieldDetails.PropertyParts {
    public class LandInformation : ViewModelBase {
        IClientEndpoint clientEndpoint;
        Dispatcher dispatcher;
        Domain.ReadModels.Tree.FieldDetailsView fieldView;
        int currentCropYear;
        Logger log = LogManager.GetCurrentClassLogger();

        decimal? slopePercent;
        decimal? slopeLength;
        string slopeLengthUnit = Strings.SlopeLengthUnit_Text.ToLower();
        SoilTexture texture;
        IList<Landdb.ViewModel.Secondary.Map.SSurgoViewModel> soilfeatures;
        Landdb.ViewModel.Secondary.Map.SSurgoViewModel soilfeature;

        public LandInformation(IClientEndpoint clientEndpoint, Dispatcher dispatcher, Domain.ReadModels.Tree.FieldDetailsView fieldView, int currentCropYear) {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            this.fieldView = fieldView;
            this.currentCropYear = currentCropYear;

            SoilTypeString = string.Empty;
            SlopeLengthString = string.Empty;
            SlopePercentString = string.Empty;
            SandTotal_rString = string.Empty;
            SiltTotal_rString = string.Empty;
            ClayTotal_rString = string.Empty;
            HydrologyGroupList = new List<string>() { "A", "B", "C", "D", "A / D", "B / D", "C / D" };
            
            HydrologyString = string.Empty;

            UpdateFromProjection(fieldView);

            CalculateSSurgoDataCommand = new RelayCommand(CalculateSSurgoData);
            UseResultsCommand = new RelayCommand(UpdateSoilProperties);
            ShowEditorCommand = new RelayCommand(ShowEditor);
            AssistCommand = new RelayCommand(ShowEditorAssist);
            CancelCommand = new RelayCommand(Cancel);
            CancelAssistCommand = new RelayCommand(CancelAssist);
            UpdateCommand = new RelayCommand(Update);
            SlopePercentCommand = new RelayCommand(UpdateSoilProperties);
            SlopeLengthCommand = new RelayCommand(UpdateSoilProperties);
            SoilTypeCommand = new RelayCommand(UpdateSoilProperties);
            HydrologyCommand = new RelayCommand(UpdateSoilProperties);

        }
        public ICommand CalculateSSurgoDataCommand { get; private set; }
        public ICommand UseResultsCommand { get; private set; }
        public ICommand ShowEditorCommand { get; private set; }
        public ICommand AssistCommand { get; private set; }
        public ICommand UpdateCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }
        public ICommand CancelAssistCommand { get; private set; }
        public ICommand SlopePercentCommand { get; private set; }
        public ICommand SlopeLengthCommand { get; private set; }
        public ICommand SoilTypeCommand { get; private set; }
        public ICommand HydrologyCommand { get; private set; }

        public decimal? SlopePercent {
            get { return slopePercent; }
            set {
                slopePercent = value;
                RaisePropertyChanged("SlopePercent");
            }
        }

        void UpdateSoilProperties() {
            decimal? slopper = System.Convert.ToDecimal(SlopePercentString);
            decimal? sloplen = System.Convert.ToDecimal(SlopeLengthString);
            if (slopper > 0) {
                SlopePercent = slopper / 100;

            }
            SlopeLength = sloplen;
            SoilTexture soiltex = clientEndpoint.GetMasterlistService().GetSoilTextureByName(SoilTypeString);
            SoilTexture = soiltex;
            HydrologyGroup = hydrologyString;

            CancelAssist();
        }

        public decimal? SlopeLength {
            get { return slopeLength; }
            set {
                slopeLength = value;
                RaisePropertyChanged("SlopeLength");
                RaisePropertyChanged("SlopeLengthDisplay");
            }
        }

        public IUnit SlopeLengthUnit {
            get { 
                return UnitFactory.GetUnitByName(slopeLengthUnit); 
            }
            set {
                if (value != null) {
                    slopeLengthUnit = value.Name;
                }
                RaisePropertyChanged("SlopeLengthUnit");
                RaisePropertyChanged("SlopeLengthDisplay");
            }
        }

        public string SlopeLengthDisplay {
            get {
                double val = SlopeLength.HasValue ? (double)SlopeLength : 0;
                return SlopeLengthUnit.GetMeasure(val).FullDisplay;
            }
        }

        public List<SoilTexture> SoilTextures
        {
            get
            {
                return clientEndpoint.GetMasterlistService().GetSoilTextureList().ToList();
            }
        }

        public SoilTexture SoilTexture
        {
            get { return texture; }
            set {
                //if (texture == value) { return; }
                texture = value;
                RaisePropertyChanged("SoilTexture");
            }
        }

        public IList<Landdb.ViewModel.Secondary.Map.SSurgoViewModel> SoilFeatures {
            get { return soilfeatures; }
            set { 
                soilfeatures = value; 
                RaisePropertyChanged("SoilFeatures"); 
            }
        }

        public Landdb.ViewModel.Secondary.Map.SSurgoViewModel SoilFeature {
            get { return soilfeature; }
            set { 
                soilfeature = value; 
                RaisePropertyChanged("SoilFeature"); }
        }


        string soilTypeString = string.Empty;
        public string SoilTypeString {
            get { return soilTypeString; }
            set {
                if (soilTypeString == value) { return; }
                soilTypeString = value;
                RaisePropertyChanged("SoilTypeString");
            }
        }

        string slopeLengthString = string.Empty;
        public string SlopeLengthString {
            get { return slopeLengthString; }
            set {
                if (slopeLengthString == value) { return; }
                slopeLengthString = value;
                RaisePropertyChanged("SlopeLengthString");
            }
        }

        string slopePercentString = string.Empty;
        public string SlopePercentString {
            get { return slopePercentString; }
            set {
                if (slopePercentString == value) { return; }
                slopePercentString = value;
                RaisePropertyChanged("SlopePercentString");
            }
        }

        string sandTotal_rString = string.Empty;
        public string SandTotal_rString {
            get { return sandTotal_rString; }
            set {
                if (sandTotal_rString == value) { return; }
                sandTotal_rString = value;
                RaisePropertyChanged("SandTotal_rString");
            }
        }

        string siltTotal_rString = string.Empty;
        public string SiltTotal_rString {
            get { return siltTotal_rString; }
            set {
                if (siltTotal_rString == value) { return; }
                siltTotal_rString = value;
                RaisePropertyChanged("SiltTotal_rString");
            }
        }

        string clayTotal_rString = string.Empty;
        public string ClayTotal_rString {
            get { return clayTotal_rString; }
            set { 
                if (clayTotal_rString == value) { return; }
                clayTotal_rString = value;
                RaisePropertyChanged("ClayTotal_rString");
            }
        }

        private IList<string> hydrologygrouplist = new List<string>();
        public IList<string> HydrologyGroupList {
            get { return hydrologygrouplist; }
            private set {
                hydrologygrouplist = value;
                RaisePropertyChanged("HydrologyGroupList");
            }
        }

        string hydrologygroup = string.Empty;
        public string HydrologyGroup {
            get { return hydrologygroup; }
            set {
                if (hydrologygroup == value) { return; }
                hydrologygroup = value;
                RaisePropertyChanged("HydrologyGroup");
            }
        }

        string hydrologyString = string.Empty;
        public string HydrologyString {
            get { return hydrologyString; }
            set {
                if (hydrologyString == value) { return; }
                hydrologyString = value;
                RaisePropertyChanged("HydrologyString");
            }
        }

        public bool HasData {
            get {
                return SlopePercent.HasValue || SlopeLength.HasValue || HydrologyGroup != string.Empty;
            }
        }

        void UpdateFromProjection(Domain.ReadModels.Tree.FieldDetailsView fieldView) {
            SlopePercent = fieldView.SlopePercent;
            SlopeLength = fieldView.SlopeLength;
            SoilTexture = clientEndpoint.GetMasterlistService().GetSoilTextureByName(fieldView.SoilTexture);
            HydrologyGroup = fieldView.HydrologyGroup;
            if (!string.IsNullOrEmpty(fieldView.SlopeLengthUnit)) {
                SlopeLengthUnit = UnitFactory.GetUnitByName(fieldView.SlopeLengthUnit);
            }
        }

        bool IsSlopeChanged() {
            return SlopePercent != fieldView.SlopePercent || SlopeLength != fieldView.SlopeLength || SlopeLengthUnit.Name != fieldView.SlopeLengthUnit || SoilTexture.Name != fieldView.SoilTexture || HydrologyGroup != fieldView.HydrologyGroup;
        }

        void Cancel() {
            UpdateFromProjection(fieldView);
            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
        }

        void CancelAssist() {
            //UpdateFromProjection(fieldView);
            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
        }

        void Update() {
            if (IsSlopeChanged()) {
                var slopeLengthUnit = SlopeLengthUnit == null || string.IsNullOrEmpty(SlopeLengthUnit.Name) ? string.Empty : SlopeLengthUnit.Name;
                var soilTexture = SoilTexture == null || string.IsNullOrEmpty(SoilTexture.Name) ? string.Empty : SoilTexture.Name;
                UpdateFieldSlopeInformation slopeCommand = new UpdateFieldSlopeInformation(fieldView.Id, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), SlopePercent, SlopeLength, slopeLengthUnit, soilTexture, currentCropYear, hydrologygroup);
                clientEndpoint.SendOne(slopeCommand);
            }

            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
            RaisePropertyChanged("HasData");
        }

        void ShowEditor() {
            Messenger.Default.Send<ShowPopupMessage>(new ShowPopupMessage() { ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Fields.Popups.UpdateLandInformationView", "editFsa", this) });
        }

        void ShowEditorAssist() {
            Messenger.Default.Send<ShowPopupMessage>(new ShowPopupMessage() { ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Fields.Popups.UpdateLandInformationAssistView", "editSSurgp", this) });
        }

        void CalculateSSurgoData() {
            ThinkGeo.MapSuite.Core.GeoCollection<ThinkGeo.MapSuite.Core.Feature> selectedFeatures = new ThinkGeo.MapSuite.Core.GeoCollection<ThinkGeo.MapSuite.Core.Feature>();
            FieldId fieldid = fieldView.Id;
            IIdentity itemId = fieldView.Id;
            var maps = clientEndpoint.GetView<Landdb.Domain.ReadModels.Map.ItemMap>(itemId);
            try {
                if (maps.HasValue) {
                    Landdb.Domain.ReadModels.Map.ItemMapItem mri = maps.Value.MostRecentMapItem;
                    if (mri == null) { return; }
                    if (!string.IsNullOrEmpty(mri.MapData) && mri.MapData != "MULTIPOLYGON EMPTY") {
                        var feature = new ThinkGeo.MapSuite.Core.Feature(mri.MapData);
                        ThinkGeo.MapSuite.Core.BaseShape testbase = feature.GetShape();
                        ThinkGeo.MapSuite.Core.MultipolygonShape mdataconvert = new ThinkGeo.MapSuite.Core.MultipolygonShape();
                        if (testbase is ThinkGeo.MapSuite.Core.PolygonShape) {
                            mdataconvert.Polygons.Add(testbase as ThinkGeo.MapSuite.Core.PolygonShape);
                            feature = new ThinkGeo.MapSuite.Core.Feature(mdataconvert.GetWellKnownText());
                        }
                        selectedFeatures.Add(feature);
                        Landdb.ViewModel.Secondary.Map.SSurgoNrcsInfoWfsViewModel nrcsinfo = new Landdb.ViewModel.Secondary.Map.SSurgoNrcsInfoWfsViewModel(clientEndpoint, selectedFeatures);
                        SoilTypeString = nrcsinfo.SlopeLengthString;
                        SlopeLengthString = nrcsinfo.SlopeLengthString;
                        SlopePercentString = nrcsinfo.SlopePercentString;
                        SandTotal_rString = nrcsinfo.SandTotal_rString;
                        SiltTotal_rString = nrcsinfo.SiltTotal_rString;
                        ClayTotal_rString = nrcsinfo.ClayTotal_rString;
                        SoilTypeString = nrcsinfo.SoilType;
                        HydrologyString = ParseNrcsHydrologyGroup(nrcsinfo.HydrologyGroup);
                        SoilFeatures = nrcsinfo.SoilInfoList;
                    }
                }
            }
            catch (Exception ex) {
                log.ErrorException("LandInformation - Couldn't load the field shape.", ex);
                System.Windows.MessageBox.Show(Strings.ThereWasProblemLoadingFieldShape_Text);
            }

            //Messenger.Default.Send<ShowPopupMessage>(new ShowPopupMessage() { ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Fields.Popups.UpdateLandInformationView", "editFsa", this) });
        }

        string ParseNrcsHydrologyGroup(string nrcsValue) {
            if (!string.IsNullOrWhiteSpace(nrcsValue) && nrcsValue.Contains('/') && !nrcsValue.Contains(" / ")) {
                // ensures that there are spaces surrounding the forward slash...
                return nrcsValue.Replace("/", " / ");
            } else { return nrcsValue; }
        }
    }
}
