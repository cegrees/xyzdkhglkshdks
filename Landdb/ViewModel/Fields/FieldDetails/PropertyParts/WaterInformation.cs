﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Resources;

namespace Landdb.ViewModel.Fields.FieldDetails.PropertyParts {
    public class WaterInformation : ViewModelBase {
        WaterSource waterSource;
        WaterOrigin waterOrigin;
        EnergySource energySource;
        double? pumpPressure;
        double? pumpLift;
        string irrigated;
        IrrigationSystem irrigationSystem;
        List<string> irr = new List<string>();

        IClientEndpoint clientEndpoint;
        Dispatcher dispatcher;
        int currentCropYear;
        Domain.ReadModels.Tree.FieldDetailsView fieldView;
        Domain.ReadModels.Tree.CropZoneDetailsView czView;

        public WaterInformation(IClientEndpoint clientEndpoint, Dispatcher dispatcher, Domain.ReadModels.Tree.FieldDetailsView fieldView, int currentCropYear) {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            this.fieldView = fieldView;
            this.currentCropYear = currentCropYear;

            irr.Add(Strings.Yes_Text);
            irr.Add(Strings.No_Text);

            IrrigationSystemName = string.Empty;
            WaterOriginName = string.Empty;
            EnergySourceName = string.Empty;
            WaterSourceName = string.Empty;

            czView = null;

            UpdateFromProjection(fieldView);

            ShowEditorCommand = new RelayCommand(ShowEditor);
            CancelCommand = new RelayCommand(Cancel);
            UpdateCommand = new RelayCommand(Update);
        }

        public WaterInformation(IClientEndpoint clientEndpoint, Dispatcher dispatcher, Domain.ReadModels.Tree.CropZoneDetailsView czView, int currentCropYear)
        {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            this.czView = czView;
            this.currentCropYear = currentCropYear;

            irr.Add(Strings.Yes_Text);
            irr.Add(Strings.No_Text);

            IrrigationSystemName = string.Empty;
            WaterOriginName = string.Empty;
            EnergySourceName = string.Empty;
            WaterSourceName = string.Empty;

            var fieldId = czView.FieldId;
            var fieldDetailsMaybe = clientEndpoint.GetView<FieldDetailsView>(fieldId);
            fieldView = fieldDetailsMaybe.HasValue ? fieldDetailsMaybe.Value : null;

            UpdateFromProjection(czView);

            ShowEditorCommand = new RelayCommand(ShowEditor);
            CancelCommand = new RelayCommand(Cancel);
            UpdateCommand = new RelayCommand(Update);
        }

        public ICommand ShowEditorCommand { get; private set; }
        public ICommand UpdateCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }

        public List<string> Irr {
            get { return irr; }
        }
        public string Irrigated {
            get { return irrigated; }
            set {
                if (value != null) {
                    irrigated = value.ToString();
                    RaisePropertyChanged("Irrigated");
                }
            }
        }

        public List<WaterSource> WaterSources {
            get { return clientEndpoint.GetMasterlistService().GetWaterSourceList().ToList(); }
        }

        public WaterSource WaterSource {
            get { return waterSource; }
            set {
                waterSource = value;
                if (waterSource != null) WaterSourceName = waterSource.Name;
                RaisePropertyChanged("WaterSource");
            }
        }

        public string WaterSourceName { get; set; }

        public List<WaterOrigin> WaterOrigins {
            get { return clientEndpoint.GetMasterlistService().GetWaterOriginList().ToList(); }
        }

        public WaterOrigin WaterOrigin {
            get { return waterOrigin; }
            set {
                waterOrigin = value;
                if (waterOrigin != null) WaterOriginName = waterOrigin.Name;
                RaisePropertyChanged("WaterOrigin");
            }
        }

        public string WaterOriginName { get; set; }

        public List<EnergySource> EnergySources {
            get { return clientEndpoint.GetMasterlistService().GetEnergySourceList().ToList(); }
        }

        public EnergySource EnergySource {
            get { return energySource; }
            set {
                energySource = value;
                if (energySource != null) EnergySourceName = energySource.Name;
                RaisePropertyChanged("EnergySource");
            }
        }

        public string EnergySourceName { get; set; }

        public double? PumpPressure {
            get { return pumpPressure; }
            set {
                pumpPressure = value;
                RaisePropertyChanged("PumpPressure");
            }
        }

        public double? PumpLift {
            get { return pumpLift; }
            set {
                pumpLift = value;
                RaisePropertyChanged("PumpLift");
            }
        }

        public List<IrrigationSystem> IrrigationSystems {
            get { return clientEndpoint.GetMasterlistService().GetIrrigationSystemList().ToList(); }
        }

        public IrrigationSystem IrrigationSystem {
            get { return irrigationSystem; }
            set {
                irrigationSystem = value;
                if (irrigationSystem != null) IrrigationSystemName = irrigationSystem.Name;
                RaisePropertyChanged("IrrigationSystem");
            }
        }

        public string IrrigationSystemName { get; set; }

        public bool FromField { get; set; }

        public bool HasData {
            get {

                return !string.IsNullOrWhiteSpace(Irrigated) || IrrigationSystem != null || WaterOrigin != null || WaterSource != null || PumpPressure.HasValue && PumpPressure != 0 || PumpLift.HasValue && PumpLift != 0;
            }
        }

        void UpdateFromProjection(Domain.ReadModels.Tree.FieldDetailsView fieldView) {
            Irrigated = fieldView.Irrigated == null ? string.Empty : fieldView.Irrigated.GetValueOrDefault() ? Strings.Yes_Text : Strings.No_Text;
            IrrigationSystem = clientEndpoint.GetMasterlistService().GetIrrigationSystemByName(fieldView.IrrigationSystem);
            WaterSource = clientEndpoint.GetMasterlistService().GetWaterSourceByName(fieldView.WaterSource);
            WaterOrigin = clientEndpoint.GetMasterlistService().GetWaterOriginByName(fieldView.WaterOrigin);
            PumpPressure = fieldView.PumpPressure;
            PumpLift = fieldView.PumpLift;
            EnergySource = clientEndpoint.GetMasterlistService().GetEnergySourceByName(fieldView.EnergySource);
            FromField = false;
        }

        void UpdateFromProjection(Domain.ReadModels.Tree.CropZoneDetailsView czView)
        {
            if (czView.IrrigationInformation != null)
            {
                Irrigated = czView.IrrigationInformation.Irrigated == null ? string.Empty : czView.IrrigationInformation.Irrigated.GetValueOrDefault() ? Strings.Yes_Text : Strings.No_Text;
                IrrigationSystem = clientEndpoint.GetMasterlistService().GetIrrigationSystemByName(czView.IrrigationInformation.IrrigationSystem);
                WaterSource = clientEndpoint.GetMasterlistService().GetWaterSourceByName(czView.IrrigationInformation.WaterSource);
                WaterOrigin = clientEndpoint.GetMasterlistService().GetWaterOriginByName(czView.IrrigationInformation.WaterOrigin);
                PumpPressure = czView.IrrigationInformation.PumpPressure;
                PumpLift = czView.IrrigationInformation.PumpLift;
                EnergySource = clientEndpoint.GetMasterlistService().GetEnergySourceByName(czView.IrrigationInformation.EnergySource);
                FromField = false;
            }
            else
            {
                Irrigated = fieldView.Irrigated == null ? string.Empty : fieldView.Irrigated.GetValueOrDefault() ? Strings.Yes_Text : Strings.No_Text;
                IrrigationSystem = clientEndpoint.GetMasterlistService().GetIrrigationSystemByName(fieldView.IrrigationSystem);
                WaterSource = clientEndpoint.GetMasterlistService().GetWaterSourceByName(fieldView.WaterSource);
                WaterOrigin = clientEndpoint.GetMasterlistService().GetWaterOriginByName(fieldView.WaterOrigin);
                PumpPressure = fieldView.PumpPressure;
                PumpLift = fieldView.PumpLift;
                EnergySource = clientEndpoint.GetMasterlistService().GetEnergySourceByName(fieldView.EnergySource);
                FromField = HasData == true ? true : false;
            }

            RaisePropertyChanged("HasData");
        }

        bool IsInfoChanged() {
            if (czView != null)
            {
                if (czView.IrrigationInformation != null)
                {
                    return czView.IrrigationInformation.Irrigated != (Irrigated == Strings.Yes_Text ? true : false) || czView.IrrigationInformation.WaterOrigin != WaterOriginName || czView.IrrigationInformation.WaterSource != WaterSourceName || czView.IrrigationInformation.PumpLift != PumpLift || czView.IrrigationInformation.PumpPressure != PumpPressure || czView.IrrigationInformation.IrrigationSystem != IrrigationSystemName || czView.IrrigationInformation.EnergySource != EnergySourceName;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return fieldView.Irrigated != (Irrigated == Strings.Yes_Text ? true : false) || fieldView.WaterOrigin != WaterOriginName || fieldView.WaterSource != WaterSourceName || fieldView.PumpLift != PumpLift || fieldView.PumpPressure != PumpPressure || fieldView.IrrigationSystem != IrrigationSystemName || fieldView.EnergySource != EnergySourceName;
            }
        }

        void Cancel() {
            if (czView != null) { UpdateFromProjection(czView); }
            else { UpdateFromProjection(fieldView); }

            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
        }

        void Update() {
            if (IsInfoChanged()) {
                if (czView != null)
                {
                    UpdateCropZoneWaterInformation czWaterCommand = new UpdateCropZoneWaterInformation(czView.Id, clientEndpoint.GenerateNewMetadata(), Irrigated == Strings.Yes_Text, WaterSourceName, WaterOriginName, PumpPressure, PumpLift, IrrigationSystemName, EnergySourceName, currentCropYear);
                    clientEndpoint.SendOne(czWaterCommand);
                }
                else
                {
                    UpdateFieldWaterInformation waterCommand = new UpdateFieldWaterInformation(fieldView.Id, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), Irrigated == Strings.Yes_Text, WaterSourceName, WaterOriginName, PumpPressure, PumpLift, IrrigationSystemName, EnergySourceName, currentCropYear);
                    clientEndpoint.SendOne(waterCommand);
                }
            }

            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
            RaisePropertyChanged("HasData");
        }

        void ShowEditor() {
            Messenger.Default.Send<ShowPopupMessage>(new ShowPopupMessage() { ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Fields.Popups.UpdateWaterInformationView", "editFsa", this) });
        }
    }
}
