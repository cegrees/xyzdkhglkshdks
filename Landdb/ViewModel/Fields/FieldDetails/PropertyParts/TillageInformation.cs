﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;

namespace Landdb.ViewModel.Fields.FieldDetails.PropertyParts
{
    public class TillageInformation : ViewModelBase
    {
        IClientEndpoint clientEndpoint;
        Dispatcher dispatcher;
        IMasterlistService service;

        Landdb.Domain.ReadModels.Tree.CropZoneDetailsView czView;
        int currentCropYear;
        TillageSystem tillage;

        public TillageInformation(IClientEndpoint clientEndpoint, Dispatcher dispatcher, Landdb.Domain.ReadModels.Tree.CropZoneDetailsView czView, int currentCropYear)
        {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            this.czView = czView;
            this.currentCropYear = currentCropYear;

            ShowEditorCommand = new RelayCommand(ShowEditor);
            CancelCommand = new RelayCommand(Cancel);
            UpdateCommand = new RelayCommand(Update);
            //set townshipDirection and rangeDirection from Display

            service = clientEndpoint.GetMasterlistService();
            TillageSystems = service.GetTillageSystemList().ToList();

            UpdateFromProjection(czView);
            RaisePropertyChanged("TillageSystems");
            
        }

        public ICommand ShowEditorCommand { get; private set; }
        public ICommand UpdateCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }

        public List<TillageSystem> TillageSystems { get; set; }
        public TillageSystem TillageSystem { 
            get { return tillage; } 
            set 
            { 
                tillage = value; 
                TillageSystemText = tillage != null ? tillage.Name : string.Empty; 
                RaisePropertyChanged("TillageSystem");
                RaisePropertyChanged("TillageSystemText"); 
            } 
        }
        public string TillageSystemText { get; set; }



        public bool HasData
        {
            get
            {
                return !string.IsNullOrWhiteSpace(TillageSystemText);
            }
        }

        void Cancel()
        {
            UpdateFromProjection(czView);
            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
        }

        void Update()
        {
            if (InfoChanged())
            {
                RaisePropertyChanged("TillageSystemText");
                SetCropZoneTillagePractice command = new SetCropZoneTillagePractice(czView.Id, clientEndpoint.GenerateNewMetadata(), TillageSystem.Name, currentCropYear);
                clientEndpoint.SendOne(command);
            }
            
            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
            RaisePropertyChanged("HasData");
        }

        bool InfoChanged()
        {
            return TillageSystemText != czView.Tillage;
        }

        void ShowEditor()
        {
            Messenger.Default.Send<ShowPopupMessage>(new ShowPopupMessage() { ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Fields.Popups.UpdateTillageSystemView", "editFsa", this) });
        }

        void UpdateFromProjection(Domain.ReadModels.Tree.CropZoneDetailsView czView)
        {
            TillageSystem = TillageSystems.Where(x => x.Name == czView.Tillage).SingleOrDefault();
            TillageSystemText = TillageSystem != null ? TillageSystem.Name : string.Empty;

            RaisePropertyChanged("TillageSystem");
            RaisePropertyChanged("TillageSystemText");
        }
    }
}
