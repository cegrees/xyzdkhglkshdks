﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels;
using Landdb.Infrastructure.Messages;
using System.Windows;
using Landdb.Models;

namespace Landdb.ViewModel.Fields.FieldDetails.PropertyParts {
    public class NameViewModel : ViewModelBase, IEditNameViewModel {
        IClientEndpoint clientEndpoint;
        Dispatcher dispatcher;

        Landdb.Domain.ReadModels.Tree.FieldDetailsView fieldView;
        Landdb.Domain.ReadModels.Tree.CropZoneDetailsView czReadModel;
        Landdb.Domain.ReadModels.Tree.FarmDetailsView farmView;

        IHasName namedModel;
        int currentCropYear;

        string name;
        NameType type;

        public NameViewModel(IClientEndpoint endPoint, Dispatcher dispatcher, FieldDetailsViewModel fieldViewModel, int currentCropYear) {
            this.clientEndpoint = endPoint;
            this.dispatcher = dispatcher;
            this.fieldView = fieldViewModel.FieldView;
            this.currentCropYear = currentCropYear;
            this.namedModel = fieldViewModel;

            Type = NameType.Field;

            UpdateFromProjection();

            ShowEditorCommand = new RelayCommand(ShowEditor);
            CancelCommand = new RelayCommand(Cancel);
            UpdateCommand = new RelayCommand(Update);
        }

        public NameViewModel(IClientEndpoint endPoint, Dispatcher dispatcher, FarmDetailsViewModel farmViewModel, int currentCropYear) {
            this.clientEndpoint = endPoint;
            this.dispatcher = dispatcher;
            this.farmView = farmViewModel.FarmDetails;
            this.currentCropYear = currentCropYear;
            this.namedModel = farmViewModel;

            Type = NameType.Farm;

            UpdateFromProjection();

            ShowEditorCommand = new RelayCommand(ShowEditor);
            CancelCommand = new RelayCommand(Cancel);
            UpdateCommand = new RelayCommand(Update);
        }

        public NameViewModel(IClientEndpoint endPoint, Dispatcher dispatcher, CropZoneDetailsViewModel czViewModel, int currentCropYear) {
            this.clientEndpoint = endPoint;
            this.dispatcher = dispatcher;
            this.czReadModel = czViewModel.CropZoneView;
            this.currentCropYear = currentCropYear;
            this.namedModel = czViewModel;

            Type = NameType.CropZone;

            UpdateFromProjection();

            ShowEditorCommand = new RelayCommand(ShowEditor);
            CancelCommand = new RelayCommand(Cancel);
            UpdateCommand = new RelayCommand(Update);
        }

        public ICommand ShowEditorCommand { get; private set; }
        public ICommand UpdateCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }

        public string Name {
            get { return name; }
            set {
                name = value;
                RaisePropertyChanged("Name");

                if (namedModel != null) {
                    namedModel.Name = value;
                }
            }
        }

        public string FieldID { get; set; }
        public Visibility IsCollapsed { get; set; }

        public enum NameType {
            Farm,
            Field,
            CropZone
        }

        public NameType Type {
            get { return type; }
            set {
                type = value;
            }
        }

        bool IsInfoChanged() {
            switch (Type) {
                case NameType.Farm:
                    return Name != farmView.FarmNameByCropYear[currentCropYear]; //.Name; ;
                case NameType.Field:
                    return Name != fieldView.FieldNameByCropYear[currentCropYear] || FieldID != fieldView.FieldID; ;
                case NameType.CropZone:
                    return Name != czReadModel.Name || FieldID != czReadModel.CustomIntegrationId; ;
                default:
                    return false;
            }
        }

        void UpdateFromProjection() {
            switch (Type) {
                case NameType.Farm:
                    Name = farmView.FarmNameByCropYear.ContainsKey(currentCropYear) ? farmView.FarmNameByCropYear[currentCropYear] : farmView.Name; //.Name;
                    IsCollapsed = Visibility.Collapsed;
                    return;
                case NameType.Field:
                    Name = fieldView.FieldNameByCropYear.ContainsKey(currentCropYear) ? fieldView.FieldNameByCropYear[currentCropYear] : fieldView.Name; //.Name;
                    FieldID = fieldView.FieldID;
                    IsCollapsed = Visibility.Visible;
                    return;
                case NameType.CropZone:
                    Name = czReadModel.Name;
                    FieldID = czReadModel.CustomIntegrationId;
                    IsCollapsed = Visibility.Visible;
                    return;
                default:
                    return;
            }
        }

        void Cancel() {
            UpdateFromProjection();
            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
        }

        void Update() {
            if (IsInfoChanged()) {
                switch (Type) {
                    case NameType.Farm:
                        //update farm name
                        RenameFarm farmCommand = new RenameFarm(farmView.Id, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), Name, currentCropYear);
                        clientEndpoint.SendOne(farmCommand);
                        break;
                    case NameType.Field:
                        //update field name
                        RenameField_v2 fieldCommand = new RenameField_v2(fieldView.Id, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), Name, FieldID, currentCropYear);
                        clientEndpoint.SendOne(fieldCommand);
                        break;
                    case NameType.CropZone:
                        //update crop zone name
                        RenameCropZone czCommand = new RenameCropZone(czReadModel.Id, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), Name, currentCropYear, FieldID);

                        clientEndpoint.SendOne(czCommand);
                        break;
                    default:
                        break;
                }

            }

            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
        }

        void ShowEditor() {
            Messenger.Default.Send<ShowPopupMessage>(new ShowPopupMessage() { ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Shared.Popups.EditNameView", "editName", this) });
        }
    }
}
