﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure.Messages;
using AgC.UnitConversion;
using AgC.UnitConversion.Area;
using Landdb.Infrastructure;

namespace Landdb.ViewModel.Fields.FieldDetails.PropertyParts {
    public class AreaInformation : ViewModelBase {
        IClientEndpoint clientEndpoint;
        Dispatcher dispatcher;

        Landdb.Domain.ReadModels.Tree.CropZoneDetailsView czView;
        Landdb.Domain.ReadModels.Tree.FieldDetailsView fieldView;

        double? boundaryArea;
        double? reportedArea;
        NameType type;
        string reportedUnit;
        string boundaryUnit;
        int currentCropYear;


        public AreaInformation(IClientEndpoint endpoint, Dispatcher dispatcher, Landdb.Domain.ReadModels.Tree.CropZoneDetailsView czView, int currentCropYear) {
            this.clientEndpoint = endpoint;
            this.dispatcher = dispatcher;
            this.currentCropYear = currentCropYear;

            this.czView = czView;

            type = NameType.CropZone;

            ShowEditorCommand = new RelayCommand(ShowEditor);
            CancelCommand = new RelayCommand(Cancel);
            UpdateCommand = new RelayCommand(Update);

            UpdateFromProjection();
        }

        public AreaInformation(IClientEndpoint endpoint, Dispatcher dispatcher, Landdb.Domain.ReadModels.Tree.FieldDetailsView fieldView, int currentCropYear) {
            this.clientEndpoint = endpoint;
            this.dispatcher = dispatcher;
            this.currentCropYear = currentCropYear;

            this.fieldView = fieldView;

            type = NameType.Field;

            ShowEditorCommand = new RelayCommand(ShowEditor);
            CancelCommand = new RelayCommand(Cancel);
            UpdateCommand = new RelayCommand(Update);

            UpdateFromProjection();
        }

        public ICommand ShowEditorCommand { get; private set; }
        public ICommand UpdateCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }

        public double? BoundaryAreaValue {
            get { return boundaryArea; }
            set {
                boundaryArea = value;
                RaisePropertyChanged("BoundaryAreaValue");
                RaisePropertyChanged("DisplayArea");
            }
        }

        public string BoundaryAreaUnit {
            get { return boundaryUnit; }
            set {
                boundaryUnit = value;
                RaisePropertyChanged("BoundaryAreaUnit");
                RaisePropertyChanged("DisplayArea");
            }
        }

        public double? ReportedAreaValue {
            get { return reportedArea; }
            set {
                reportedArea = value;
                RaisePropertyChanged("ReportedAreaValue");
                RaisePropertyChanged("DisplayArea");
            }
        }

        public string ReportedAreaUnit {
            get { return reportedUnit; }
            set {
                reportedUnit = value;
                RaisePropertyChanged("ReportedAreaUnit");
                RaisePropertyChanged("DisplayArea");
            }
        }

        public Measure DisplayArea {
            get {
                if (reportedArea != null && !string.IsNullOrWhiteSpace(reportedUnit)) {
                    return UnitFactory.GetUnitByName(reportedUnit).GetMeasure(reportedArea.Value);
                } else if (boundaryArea != null && !string.IsNullOrWhiteSpace(boundaryUnit)) {
                    return UnitFactory.GetUnitByName(boundaryUnit).GetMeasure(boundaryArea.Value);
                } else {
                    return UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(0);
                }
            }
        }

        public string BoundaryArea {
            get {
                if (boundaryArea != null && !string.IsNullOrWhiteSpace(boundaryUnit)) {
                    return UnitFactory.GetUnitByName(boundaryUnit).GetMeasure(boundaryArea.Value).ToLongDisplay();
                } else {
                    return UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(0).ToLongDisplay();
                }
            }
        }

        public bool IsReported {
            get { return (reportedArea != null && !string.IsNullOrWhiteSpace(reportedUnit)); }
        }

        public enum NameType {
            Field,
            CropZone
        }

        public NameType Type {
            get { return type; }
            set {
                type = value;
            }
        }

        bool IsInfoChanged() {
            switch (Type) {
                case NameType.Field:
                    return ReportedAreaValue != fieldView.ReportedArea;
                case NameType.CropZone:
                    return ReportedAreaValue != czView.ReportedArea;
                default:
                    return false;
            }
        }

        void UpdateFromProjection() {
            switch (Type) {
                case NameType.Field:
                    ReportedAreaValue = fieldView.ReportedArea;
                    ReportedAreaUnit = fieldView.ReportedAreaUnit;
                    BoundaryAreaValue = fieldView.BoundaryArea;
                    BoundaryAreaUnit = fieldView.BoundaryAreaUnit;
                    break;
                case NameType.CropZone:
                    ReportedAreaValue = czView.ReportedArea;
                    ReportedAreaUnit = czView.ReportedAreaUnit;
                    BoundaryAreaValue = czView.BoundaryArea;
                    BoundaryAreaUnit = czView.BoundaryAreaUnit;
                    break;
                default:
                    break;
            }
            RaisePropertyChanged("DisplayArea");
        }

        void Cancel() {
            UpdateFromProjection();
            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
        }

        void Update() {
            if (IsInfoChanged()) {
                string actualReportedAreaUnit = ReportedAreaValue != null ? ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit : null;

                switch (Type) {
                    case NameType.Field:
                        //update field area
                        UpdateFieldReportedArea fieldCommand = new UpdateFieldReportedArea(fieldView.Id, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), ReportedAreaValue, actualReportedAreaUnit, currentCropYear);
                        clientEndpoint.SendOne(fieldCommand);
                        break;
                    case NameType.CropZone:
                        //update crop zone area
                        UpdateCropZoneReportedArea czCommand = new UpdateCropZoneReportedArea(czView.Id, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), ReportedAreaValue, actualReportedAreaUnit, currentCropYear);
                        clientEndpoint.SendOne(czCommand);
                        break;
                    default:
                        break;
                }
                RaisePropertyChanged("DisplayArea");
            }

            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
        }

        void ShowEditor() {
            Messenger.Default.Send<ShowPopupMessage>(new ShowPopupMessage() { ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Fields.Popups.EditAreaView", "editArea", this) });
        }
    }
}