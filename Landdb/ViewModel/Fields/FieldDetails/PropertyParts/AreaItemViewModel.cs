﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;

namespace Landdb.ViewModel.Fields.FieldDetails.PropertyParts
{
    public class AreaItemViewModel : ViewModelBase
    {
        string choice;
        bool isChecked;

        public AreaItemViewModel(string choice, bool isChecked)
        {
            this.choice = choice;
            this.isChecked = isChecked;
        }

        public string AreaChoice { get { return choice; } }
        public bool IsChecked { get { return isChecked; } }

    }
}
