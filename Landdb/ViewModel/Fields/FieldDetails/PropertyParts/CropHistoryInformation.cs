﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;

namespace Landdb.ViewModel.Fields.FieldDetails.PropertyParts
{
    public class CropHistoryInformation : ViewModelBase
    {
        IClientEndpoint clientEndpoint;
        Dispatcher dispatcher;

        Landdb.Domain.ReadModels.Tree.FieldDetailsView fieldDetails;

        CropId cropId;
        int cropYear;
        TillageSystem tillage;
        double area;
        List<FieldCropHistory> fch = new List<FieldCropHistory>();
        CropHistoryListItemViewModel cropHistoryVM;
        MiniCrop newItemCropSelection;
        string newItemName;


        public CropHistoryInformation(IClientEndpoint endPoint, Dispatcher dispatcher, Landdb.Domain.ReadModels.Tree.FieldDetailsView fieldDetails)
        {
            this.clientEndpoint = endPoint;
            this.dispatcher = dispatcher;
            this.fieldDetails = fieldDetails;

            ShowEditorCommand = new RelayCommand(ShowEditor);
            CancelCommand = new RelayCommand(Cancel);
            FinishCommand = new RelayCommand(Finish);
            CreateCommand = new RelayCommand(Create);
            DeleteCommand = new RelayCommand(Delete);

            //var chView = clientEndpoint.GetView<Landdb.Domain.ReadModels.Tree.CropHistoryView>(fieldDetails.FieldID);


            CropHistoryList = new ObservableCollection<CropHistoryListItemViewModel>();

            UpdateFromProjection(fieldDetails);
        }

        public ICommand ShowEditorCommand { get; private set; }
        public ICommand FinishCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }
        public ICommand CreateCommand { get; private set; }
        public ICommand DeleteCommand { get; private set; }

        public List<FieldCropHistory> CropHistorys
        {
            get
            {
                return fch;
            }
            set
            {
                fch = value;
                RaisePropertyChanged("CropHistorys");
            }
        }

        public ObservableCollection<CropHistoryListItemViewModel> CropHistoryList { get; set; }

        public CropHistoryListItemViewModel SelectedCropHistory
        {
            get { return cropHistoryVM; }
            set
            {
                cropHistoryVM = value;
                RaisePropertyChanged("SelectedCropHistory");
            }
        }

        public IEnumerable<MiniCrop> CropList
        {
            get { return clientEndpoint.GetMasterlistService().GetCropList().OrderBy(x => x.Name); }
        }

        public MiniCrop NewItemCropSelection
        {
            get { return newItemCropSelection; }
            set
            {
                // Remember the old name so that we can adjust default naming below, if it was defaulted before.
                var oldName = newItemCropSelection != null ? newItemCropSelection.Name : string.Empty;

                newItemCropSelection = value;
                CropId = new Landdb.CropId(newItemCropSelection.Id);
                RaisePropertyChanged("CropId");

                if (value != null && (string.IsNullOrWhiteSpace(NewItemName) || NewItemName == oldName))
                {
                    NewItemName = value.Name;
                    RaisePropertyChanged("NewItemName");
                }
            }
        }

        public string NewItemName
        {
            get { return newItemName; }
            set
            {
                newItemName = value;
                RaisePropertyChanged("NewItemName");
            }
        }

        public CropId CropId
        {
            get { return cropId; }
            set {
                cropId = value;
                RaisePropertyChanged("CropId");
            }
        }

        public double Area
        {
            get { return area; }
            set {
                area = value;
                RaisePropertyChanged("Area");
            }
        }

        public string AreaUnit
        {
            get { return ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit; }
        }

        public IEnumerable<TillageSystem> TillageSystems
        {
            get { return clientEndpoint.GetMasterlistService().GetTillageSystemList(); }
        }

        public TillageSystem Tillage
        {
            get { return tillage; }
            set {
                tillage = value;
                RaisePropertyChanged("Tillage");
            }
        }

        public int CropYear
        {
            get { return cropYear; }
            set {
                cropYear = value;
                RaisePropertyChanged("CropYear");
            }
        }

        void UpdateFromProjection(Domain.ReadModels.Tree.FieldDetailsView fieldView)
        {
            //updatet the CropHistoryList from fieldView
            //if (fieldView.CropHistorys != null) {
            //    foreach (FieldCropHistory fch in fieldView.CropHistorys) {
            //        CropHistoryListItemViewModel chlVM = new CropHistoryListItemViewModel(fch);
            //        CropHistoryList.Add(chlVM);
            //    }

            //    CropHistoryList.OrderBy(x => x.CropYear);
            //}

            Lokad.Cqrs.Maybe<CropHistoryView> chistoryMaybe = clientEndpoint.GetView<CropHistoryView>(new DataSourceId(Landdb.Infrastructure.ApplicationEnvironment.CurrentDataSourceId));

            chistoryMaybe.IfValue(chview => {
                if (chview.CropHistories != null && chview.CropHistories.Count > 0) {
                    var q = from x in chview.CropHistories
                            where x.FieldID == fieldView.Id && x.CropYear <= Landdb.Infrastructure.ApplicationEnvironment.CurrentCropYear
                            orderby x.CropYear descending, x.TimeStamp descending
                            select x;

                    var userSettings = clientEndpoint.GetUserSettings();
                    string areaunit = userSettings.UserAreaUnitName;
                    if (string.IsNullOrWhiteSpace(areaunit)) {
                        areaunit = ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit;
                    }

                    foreach (CropHistoryListItem chv in q) {
                        string cropName = clientEndpoint.GetMasterlistService().GetCropDisplay(chv.CropID);
                        var czView = clientEndpoint.GetView<Landdb.Domain.ReadModels.Tree.CropZoneDetailsView>(chv.CropZoneID);
                        double area = czView.Value.ReportedArea != null ? System.Convert.ToDouble(czView.Value.ReportedArea) : System.Convert.ToDouble(czView.Value.BoundaryArea);
                        CropHistoryListItemViewModel chlVM = new CropHistoryListItemViewModel(chv, cropName, area, areaunit, chv.CropZoneID);
                        CropHistoryList.Add(chlVM);
                    }
                    //CropHistoryList.OrderByDescending(x => x.CropYear);
                }

            });
        }

        public bool HasData
        {
            get
            {
                return (CropHistoryList.Count > 0);
            }
        }

        bool IsInfoChanged()
        {
            return true;//SeedLotId != czView.SeedLotId;
        }


        void Cancel()
        {
            //UpdateFromProjection(fieldDetails);
            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
        }

        void Create()
        {
            Guid trackingId = Guid.NewGuid();
            MessageMetadata newMeta = new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId);

            FieldCropHistory fch = new FieldCropHistory(fieldDetails.Id, trackingId, Area, AreaUnit, CropId, Tillage.Name, CropYear, NewItemName, true);
            CropHistorys.Add(fch);
            CropHistoryList.Add(new CropHistoryListItemViewModel(fch));

            CropHistoryList.OrderBy(x => x.CropYear);

            //call command to add a fieldcrophistory to the field
            //CreateCropHistory cropCommand = new CreateCropHistory(fieldDetails.Id, newMeta, trackingId, Area, AreaUnit, CropId, NewItemName, Tillage.Name, CropYear);
            //clientEndpoint.SendOne(cropCommand);
        }

        void Delete()
        {
            // NOTE: Not sure that delete should ever happen from here...
            if (SelectedCropHistory.Created == true)
            {
                MessageMetadata newMeta = new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId);

                //call command to remove the selectedCropHistory
                //RemoveCropHistory removeCommand = new RemoveCropHistory(SelectedCropHistory.FieldId, newMeta, SelectedCropHistory.TrackingId);
                //CropHistoryList.Remove(SelectedCropHistory);
                //clientEndpoint.SendOne(removeCommand);
            }
            else
            {
                //do nothing... you cannot delete crop history returned from crop zone level.
            }
        }

        void Edit()
        {
            this.CropYear = SelectedCropHistory.CropYear;
            this.Area = SelectedCropHistory.Area;
            this.Tillage = clientEndpoint.GetMasterlistService().GetTillageSystemByName(SelectedCropHistory.Tillage);

        }

        void Finish()
        {
            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
        }

        void ShowEditor()
        {
            Messenger.Default.Send<ShowPopupMessage>(new ShowPopupMessage() { ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Fields.Popups.CropHistoryEditorView", "editCropHistory", this) });
        }
    }
}
