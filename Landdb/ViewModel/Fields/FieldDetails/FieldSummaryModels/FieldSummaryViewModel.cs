﻿using AgC.UnitConversion;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.ActiveIngredientSetting;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Contract;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Domain.ReadModels.Map;
using Landdb.Domain.ReadModels.Product;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ReportModels.CropZone;
using Landdb.Services;
using Landdb.ViewModel.Fields.FieldDetails.PropertyParts;
using Landdb.ViewModel.Secondary.Map;
using Landdb.ViewModel.Secondary.Reports.CropZone.Generators;
using NLog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Client.Infrastructure.DisplayItems;

namespace Landdb.ViewModel.Fields.FieldDetails {
	public class FieldSummaryViewModel : ViewModelBase {

		readonly IClientEndpoint clientEndpoint;
		readonly Dispatcher dispatcher;

		readonly YieldSummaryViewModel yieldSummaryModel;

		PricingViewStrategy selectedPricingViewStrategy = PricingViewStrategy.AverageCost;

		decimal totalProductionCostByAverage = 0m;
		decimal productionCostPerAreaByAverage = 0m;

		Logger log = LogManager.GetCurrentClassLogger();

		public FieldSummaryViewModel(YieldSummaryViewModel yieldSummaryModel, IClientEndpoint clientEndpoint, Dispatcher dispatcher) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			this.yieldSummaryModel = yieldSummaryModel;

			ApplicationLineItems = new ObservableCollection<ApplicationLineItemViewModel>();
			Varieties = new ObservableCollection<VarietyItemViewModel>();

            Print = new RelayCommand(PrintReport);

            ShowAverageCost = true;
			PricingViewStrategies = new ObservableCollection<PricingViewStrategy>(Enum.GetValues(typeof(PricingViewStrategy)).Cast<PricingViewStrategy>());
			YieldPricingViewStrategies = new ObservableCollection<YieldPricingViewStrategy>(Enum.GetValues(typeof(YieldPricingViewStrategy)).Cast<YieldPricingViewStrategy>());
		}

        public ICommand Print { get; }

        public CropId CropId { get; private set; }
        public string CropZoneName { get; private set; }
        public string FieldName { get; private set; }
        public string AreaDisplay { get; private set; }
        public CropZoneId CropZoneID { get; set; }
        public FieldId FieldID { get; set; }

        public List<ActiveIngredientLineItemViewModel> ActiveIngredients { get; private set; }

		public ObservableCollection<PricingViewStrategy> PricingViewStrategies { get; private set; }
		public ObservableCollection<YieldPricingViewStrategy> YieldPricingViewStrategies { get; private set; }
		public ObservableCollection<ApplicationLineItemViewModel> ApplicationLineItems { get; private set; }
		public ObservableCollection<VarietyItemViewModel> Varieties { get; private set; }

		public bool ShowAverageCost { get; private set; }
		public bool ShowInvoiceCost { get; private set; }
		public bool ShowSpecificCost { get; private set; }

	    public static string AreaUnitDisplayText => $"/ {AreaUnitDisplayItem.GetAreaUnitDisplayTextFor(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit)}";

        public PricingViewStrategy SelectedPricingViewStrategy {
			get { return selectedPricingViewStrategy; }
			set {
				selectedPricingViewStrategy = value;
				RaisePropertyChanged(() => SelectedPricingViewStrategy);

				ShowAverageCost = false;
				ShowInvoiceCost = false;
				ShowSpecificCost = false;

				switch (value) {
					case PricingViewStrategy.AverageCost:
					default:
						RecalculateCostSummaryByAverage();
						ShowAverageCost = true;
						break;
					case PricingViewStrategy.InvoiceMatching:
						RecalculateCostSummaryByInvoice();
						ShowInvoiceCost = true;
						break;
					case PricingViewStrategy.SpecificCost:
						RecalculateCostSummaryBySpecific();
						ShowSpecificCost = true;
						break;
				}

				RaisePropertyChanged(() => ShowAverageCost);
				RaisePropertyChanged(() => ShowInvoiceCost);
				RaisePropertyChanged(() => ShowSpecificCost);
				RaisePropertyChanged(() => ShowDeviationLine);
			}
		}

        public bool IsCropZone { get; private set; }

		private FertilizerFormulationViewModel fertilizerInfo;
		public FertilizerFormulationViewModel FertilizerInformation {
			get { return fertilizerInfo; }
			set {
				fertilizerInfo = value;
				RaisePropertyChanged(() => FertilizerInformation);
			}
		}

		private RevenueSummaryViewModel revenueSummary;
		public RevenueSummaryViewModel RevenueSummary {
			get { return revenueSummary ?? new RevenueSummaryViewModel(0, null, 0); }
			private set {
				revenueSummary = value;
				RaisePropertyChanged(() => RevenueSummary);
			}
		}

		public decimal AlternativeProductionTotalCostDeviation {
			get {
				if (RevenueSummary != null) {
					return RevenueSummary.TotalCost - totalProductionCostByAverage;
				} else {
					return 0m;
				}
			}
		}

		public decimal AlternativeProductionCostPerAreaDeviation {
			get {
				if (RevenueSummary != null) {
					return RevenueSummary.CostPerArea - productionCostPerAreaByAverage;
				} else {
					return 0m;
				}
			}
		}

		public bool HasInvoices { get; private set; }
		public bool HasSpecificCost { get; private set; }

		public bool ShowDeviationLine {
			get {
				return (SelectedPricingViewStrategy == PricingViewStrategy.InvoiceMatching && HasInvoices) ||
					   (SelectedPricingViewStrategy == PricingViewStrategy.SpecificCost && HasSpecificCost);
			}
		}

		private YieldPricingViewStrategy _selectedYieldPricingViewStrategy;
		public YieldPricingViewStrategy SelectedYieldPricingViewStrategy {
			get { return _selectedYieldPricingViewStrategy; }
			set {
				_selectedYieldPricingViewStrategy = value;
				RaisePropertyChanged(() => SelectedYieldPricingViewStrategy);

				RevenueSummary.SetYieldPricingViewStrategy(value);
			}
		}

        private string FertilizerDetailsString(FertilizerFormulationViewModel fertInfo)
        {
            StringBuilder formulation = new StringBuilder(30);
            formulation.Append(String.Format("{0:0#.##}", fertInfo.N)); formulation.Append("N - ");
            formulation.Append(String.Format("{0:0#.##}", fertInfo.P)); formulation.Append("P - ");
            formulation.Append(String.Format("{0:0#.##}", fertInfo.K)); formulation.Append("K");

            if (fertInfo.Ca != 0)
            {
                formulation.Append(" - "); formulation.Append(String.Format("{0:0#.##}", fertInfo.Ca)); formulation.Append("Ca");
            }
            if (fertInfo.Mg != 0)
            {
                formulation.Append(" - "); formulation.Append(String.Format("{0:0#.##}", fertInfo.Mg)); formulation.Append("Mg");
            }
            if (fertInfo.S != 0)
            {
                formulation.Append(" - "); formulation.Append(String.Format("{0:0#.##}", fertInfo.S)); formulation.Append("S");
            }
            if (fertInfo.B != 0)
            {
                formulation.Append(" - "); formulation.Append(String.Format("{0:0#.##}", fertInfo.B)); formulation.Append("B");
            }
            if (fertInfo.Cl != 0)
            {
                formulation.Append(" - "); formulation.Append(String.Format("{0:0#.##}", fertInfo.Cl)); formulation.Append("Cl");
            }
            if (fertInfo.Cu != 0)
            {
                formulation.Append(" - "); formulation.Append(String.Format("{0:0#.##}", fertInfo.Cu)); formulation.Append("Cu");
            }
            if (fertInfo.Fe != 0)
            {
                formulation.Append(" - "); formulation.Append(String.Format("{0:0#.##}", fertInfo.Fe)); formulation.Append("Fe");
            }
            if (fertInfo.Mn != 0)
            {
                formulation.Append(" - "); formulation.Append(String.Format("{0:0#.##}", fertInfo.Mn)); formulation.Append("Mn");
            }
            if (fertInfo.Mo != 0)
            {
                formulation.Append(" - "); formulation.Append(String.Format("{0:0#.##}", fertInfo.Mo)); formulation.Append("Mo");
            }
            if (fertInfo.Zn != 0)
            {
                formulation.Append(" - "); formulation.Append(String.Format("{0:0#.##}", fertInfo.Zn)); formulation.Append("Zn");
            }

            return formulation.ToString();
        }
        private void PrintReport()
        {
            //TO DO :: PRINT SUMMARY REPORT
            var data = new SummaryData();
            data.AreaDisplay = AreaDisplay;

            if (IsCropZone) { data.CropDisplay = FieldName + " :: " + CropZoneName; }
            else if (FieldName != null) { data.CropDisplay = FieldName; }
            else { return; }

            data.CropYear = ApplicationEnvironment.CurrentCropYear;
            data.DataSourceName = ApplicationEnvironment.CurrentDataSourceName;
            data.FertilizerAnalysis = FertilizerInformation != null ? FertilizerDetailsString(FertilizerInformation) : string.Empty;
            //var micros = 

            foreach(var v in Varieties)
            {
                data.Varieties.Add(new Variety(v.Name, v.PlantingDateText));
            }

            var areaUnit = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).AbbreviatedDisplay;
            foreach (var y in RevenueSummary.YieldSummaryItems)
            {
                var yieldUnit = y.YieldQuantityUnit != null ? y.YieldQuantityUnit.AbbreviatedDisplay : string.Empty;
                data.YieldItems.Add(new GenericListItem(y.Description, string.Format("{0} {1}", y.TotalYieldQuantity.ToString("N2"), yieldUnit), string.Format("{0} / {1}", y.TotalYieldQuantityPerArea.ToString("N2"), areaUnit)));
                data.YieldItems.Add(new GenericListItem(Landdb.Resources.Strings.AverageSale_Text.ToLower(), y.AverageSalePrice.ToString("C2"), string.Empty));
                data.YieldItems.Add(new GenericListItem(Landdb.Resources.Strings.GrowerShares_Text.ToLower(), string.Format("{0} {1}", y.GrowerQuantity.ToString("N2"), yieldUnit), string.Format("{0} / {1}", y.GrowerQuantityPerArea.ToString("N2"), areaUnit)));
                data.YieldItems.Add(new GenericListItem(Landdb.Resources.Strings.GrowerRevenue_Text.ToLower(), y.GrowerRevenue.ToString("C2"), string.Empty));
                data.YieldItems.Add(new GenericListItem(string.Empty, string.Empty, string.Empty));
            }

            data.InputItems.Add(new GenericListItem(Landdb.Resources.Strings.Seed_Text.ToLower(), RevenueSummary.SeedTotalCost.ToString("C2"), string.Format("{0} / {1}", RevenueSummary.SeedCostPerArea.ToString("C2"), areaUnit)));
            data.InputItems.Add(new GenericListItem(Landdb.Resources.Strings.Fertilizer_Text.ToLower(), RevenueSummary.FertilizerTotalCost.ToString("C2"), string.Format("{0} / {1}", RevenueSummary.FertilizerCostPerArea.ToString("C2"), areaUnit)));
            data.InputItems.Add(new GenericListItem(Landdb.Resources.Strings.CropProtection_Text.ToLower(), RevenueSummary.CropProtectionTotalCost.ToString("C2"), string.Format("{0} / {1}", RevenueSummary.CropProtectionCostPerArea.ToString("C2"), areaUnit)));
            data.InputItems.Add(new GenericListItem(Landdb.Resources.Strings.FilterType_Services.ToLower(), RevenueSummary.ServicesTotalCost.ToString("C2"), string.Format("{0} / {1}", RevenueSummary.ServicesCostPerArea.ToString("C2"), areaUnit)));
            data.InputItems.Add(new GenericListItem(Landdb.Resources.Strings.Total_Text.ToLower(), RevenueSummary.TotalCost.ToString("C2"), string.Format("{0} / {1}", RevenueSummary.CostPerArea.ToString("C2"), areaUnit)));
            if ( !ShowAverageCost ) { data.InputItems.Add(new GenericListItem(Landdb.Resources.Strings.AppAnalysis_DeviationFromAvg_Text.ToLower(), AlternativeProductionTotalCostDeviation.ToString("C2"), string.Format("{0} / {1}", AlternativeProductionCostPerAreaDeviation.ToString("C2"), areaUnit))); }
            data.InputItems.Add(new GenericListItem(string.Empty, string.Empty, string.Empty));
            if (RevenueSummary.TotalCashRent != 0) { data.InputItems.Add(new GenericListItem(Landdb.Resources.Strings.CashRent_Text.ToLower(), RevenueSummary.TotalCashRent.ToString("C2"), string.Format("{0} / {1}", RevenueSummary.CashRentPerArea.ToString("C2"), areaUnit))); }
            data.InputItems.Add(new GenericListItem(Landdb.Resources.Strings.AppAnalysis_ContributionMargin_Text.ToLower(), RevenueSummary.TotalContributionMargin.ToString("C2"), string.Format("{0} / {1}", RevenueSummary.ContributionMarginPerArea.ToString("C2"), areaUnit)));

            if (RevenueSummary.BreakEvenYieldVisible)
            {
                data.InputItems.Add(new GenericListItem(RevenueSummary.BreakEvenYieldDescription, string.Empty, RevenueSummary.BreakEvenYield.ToString("N2")));
            }
            if (RevenueSummary.BreakEvenCostVisible)
            {
                data.InputItems.Add(new GenericListItem(RevenueSummary.BreakEvenCostDescription, string.Empty, RevenueSummary.BreakEvenCost.ToString("C2")));
            }

            var fieldView = clientEndpoint.GetView<FieldDetailsView>(FieldID).Value;
            CropHistoryInformation cropHistory = new CropHistoryInformation(clientEndpoint, dispatcher, fieldView);
            foreach (var history in cropHistory.CropHistoryList)
            {
                FieldCropHistoryItem fch = new FieldCropHistoryItem();
                fch.Area = (decimal)history.Area;
                fch.AreaUnit = UnitFactory.GetUnitByName( history.AreaUnit );
                fch.Crop = history.Crop;
                fch.CropID = history.CropId;
                fch.CropYear = history.CropYear;
                fch.CropZoneID = history.CropZoneID;

                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                //yieldsummaryitemviewmodel
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                var yieldSummaryItems = new List<YieldSummaryItemViewModel>();

                var currentCropYearId = new CropYearId(ApplicationEnvironment.CurrentDataSourceId, fch.CropYear);
                YieldSummaryViewModel yieldVM = new YieldSummaryViewModel(clientEndpoint, dispatcher);
                var loads = new List<LoadLineItemViewModel>();
                var yieldLoads = clientEndpoint.GetView<Domain.ReadModels.Yield.CropZoneLoadDataView>(currentCropYearId);
                if (yieldLoads.HasValue)
                {
                    var czLoads = yieldLoads.Value.Items.Where(x => x.CropZoneId == fch.CropZoneID);
                    foreach (var item in czLoads)
                    {
                        //TO DO :: FIND THE CORRECT GROWER SHARE SPLIT
                        loads.Add(new LoadLineItemViewModel(item, 1m));
                    }
                }

                var commodityList = from item in loads
                                    group item by item.Commodity into g
                                    orderby g.Count() descending
                                    select g.Key;

                foreach (var commodity in commodityList)
                {
                    var itemsWithThisCommodity = loads.Where(x => x.Commodity == commodity);

                    var sortedUnitFrequencyList = from item in itemsWithThisCommodity
                                                  where item.FinalQuantityUnit != null
                                                  group item by item.FinalQuantityUnit into g
                                                  orderby g.Count() descending
                                                  select g.Key;

                    foreach (var unit in sortedUnitFrequencyList)
                    {
                        var itemsWithThisUnit = itemsWithThisCommodity.Where(x => x.FinalQuantityUnit == unit);

                        var totalQuantity = itemsWithThisUnit.Sum(x => x.WeightedFinalQuantity);
                        var growerShare = itemsWithThisUnit.Sum(x => x.GrowerShareQuantity);

                        decimal commodityAverageSalePrice = 0;
                        decimal fieldToPosAverageSalePrice = 0;

                        var commoditySummary = new Yield.CommoditySummaryViewModel(clientEndpoint, dispatcher, currentCropYearId, commodity.CropId, commodity.CommodityDescription);
                        if (commoditySummary.UnitSummaries.ContainsKey(unit))
                        {
                            commodityAverageSalePrice = commoditySummary.UnitSummaries[unit].GrowerAverageRevenue;
                        }

                        var growerSale = itemsWithThisUnit.Sum(x => x.GrowerPrice);
                        fieldToPosAverageSalePrice = growerShare != 0 ? growerSale / growerShare : 0;
                        

                        yieldSummaryItems.Add(new YieldSummaryItemViewModel(fch.Area)
                        {
                            Description = !string.IsNullOrWhiteSpace(commodity.CommodityDescription) ? commodity.CommodityDescription : commodity.CropName,
                            TotalYieldQuantity = totalQuantity,
                            GrowerQuantity = growerShare,
                            YieldQuantityUnit = unit,
                            CommodityAverageSalePrice = commodityAverageSalePrice,
                            FieldToPosAverageSalePrice = fieldToPosAverageSalePrice,
                        });
                    }
                }

                // add a blank item if there aren't any
                if (!yieldSummaryItems.Any())
                {
                    yieldSummaryItems.Add(new YieldSummaryItemViewModel(fch.Area));
                }

                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                foreach (var item in yieldSummaryItems)
                {
                    fch.AverageYieldPerArea = item.TotalYieldQuantityPerArea;
                    fch.YieldUnit = item.YieldQuantityUnit;
                    fch.Crop = string.IsNullOrEmpty(item.Description) ? fch.Crop : item.Description;
                    data.CropHistorys.Add(fch);
                }
            }

            if (data.CropHistorys.Count > 12)
            {
                var subList = data.CropHistorys.GetRange(0, 12);
                data.CropHistorys = subList;
            }

            if (CropZoneID != null)
            {
                var maps = clientEndpoint.GetView<ItemMap>(CropZoneID);
                string mapString = string.Empty;
                try
                {
                    if (maps.HasValue)
                    {
                        if (maps.Value.MostRecentMapItem != null && maps.Value.MostRecentMapItem.MapData != string.Empty && maps.Value.MostRecentMapItem.MapData != "MULTIPOLYGON EMPTY")
                        {
                            mapString = maps.Value.MostRecentMapItem.MapData;
                        }
                    }
                }
                finally { }


                var converter = new ConvertToBitmap(mapString, clientEndpoint);
                data.MapImage = converter.BitMap;
            }
            else
            {
                var maps = clientEndpoint.GetView<ItemMap>(FieldID);
                string mapString = string.Empty;
                try
                {
                    if (maps.HasValue)
                    {
                        ItemMapItem mostrecentitem = GetMostRecentShapeByYear(maps);

                        if (mostrecentitem != null && !string.IsNullOrEmpty(mostrecentitem.MapData) && mostrecentitem.MapData != "MULTIPOLYGON EMPTY")
                        {
                            mapString = mostrecentitem.MapData;
                        }
                    }
                }
                finally { }


                var converter = new ConvertToBitmap(mapString, clientEndpoint);
                data.MapImage = converter.BitMap;
            }
            

            //to do :: pop up report....
            SummaryGenerator vm = new SummaryGenerator(clientEndpoint, data);
            Messenger.Default.Send<ShowOverlayMessage>(new ShowOverlayMessage()
            {
                ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Secondary.Reports.Work_Order.SingleWorkOrderView", "viewReport", vm)
            });

        }

        #region Public Refresh Methods
        public async void RefreshSummaryItems(AbstractTreeItemViewModel treeModel) {
			ApplicationLineItems.Clear();
			Varieties.Clear();
			FertilizerInformation = null;

			var currentCropYearId = new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear);
			var data = clientEndpoint.GetView<CropZoneApplicationDataView>(currentCropYearId).GetValue(new CropZoneApplicationDataView());
			var inventory = clientEndpoint.GetView<InventoryListView>(currentCropYearId).GetValue(new InventoryListView());

			IEnumerable<CropZoneApplicationDataItem> filtered = null;
			double area = 0;
			IUnit areaUnit = null;
			decimal cashRentPerArea = 0;

			if (treeModel is CropZoneTreeItemViewModel) {
				var cz = (CropZoneTreeItemViewModel)treeModel;
				var czId = (CropZoneId)cz.Id;
                CropZoneName = cz.Name;
                var field = ((FieldTreeItemViewModel)cz.Parent);
                FieldName = field.Name;
                FieldID = field.Id as FieldId;
                AreaDisplay = cz.Area.ToString();
                CropId = cz.CropId;
				filtered = data.Items.Where(x => x.CropZoneId == czId).OrderByDescending(x => x.StartDate);
				area = cz.Area.Value;
				areaUnit = cz.Area.Unit;
                CropZoneID = cz.CropZoneId;
  
				var contractedCropZones = clientEndpoint.GetView<CropZoneRentContractView>(currentCropYearId).GetValue(new CropZoneRentContractView());
				if (contractedCropZones.CropZoneRentContracts.ContainsKey(czId.Id)) {
					cashRentPerArea = contractedCropZones.CropZoneRentContracts[czId.Id].CashRentPerArea;
				}
			}

			if (treeModel is FieldTreeItemViewModel) {
				filtered = data.Items.Where(x => x.FieldId == (FieldId)((FieldTreeItemViewModel)treeModel).Id).OrderByDescending(x => x.StartDate);
                FieldName = ((FieldTreeItemViewModel)treeModel).Name;
                AreaDisplay = ((FieldTreeItemViewModel)treeModel).Area.ToString();
                FieldID = ((FieldTreeItemViewModel)treeModel).FieldId;
            }

			RevenueSummary = new RevenueSummaryViewModel(area, areaUnit, cashRentPerArea);
			IInventoryCalculationService inventoryService = new InventoryCalculationService();

			if (filtered != null) {
				filtered.ForEach(x => {
					var appSummaryItem = new ApplicationLineItemViewModel(clientEndpoint, inventoryService, inventory, x);
					ApplicationLineItems.Add(appSummaryItem);
				});

				RecalculateCostSummaryByAverage();

				totalProductionCostByAverage = RevenueSummary.TotalCost;
				productionCostPerAreaByAverage = RevenueSummary.CostPerArea;

				if (SelectedPricingViewStrategy == PricingViewStrategy.InvoiceMatching) {
					// yes, this is a bit silly. Potential refactor idea: calculate both cost summaries up front, and switch them out based on selected view strategy.
					RecalculateCostSummaryByInvoice();
				} else if (SelectedPricingViewStrategy == PricingViewStrategy.SpecificCost) {
					RecalculateCostSummaryBySpecific();
				}

				HasInvoices = ApplicationLineItems.Any(x => x.HasInvoiceCost);
				HasSpecificCost = ApplicationLineItems.Any(x => x.HasSpecificCost);
				RaisePropertyChanged(() => HasInvoices);
				RaisePropertyChanged(() => HasSpecificCost);
				RaisePropertyChanged(() => ShowDeviationLine);
			}

			RaisePropertyChanged(() => RevenueSummary);

			if (treeModel is CropZoneTreeItemViewModel) {
				CropId cropId = null;
				var czDetails = clientEndpoint.GetView<Domain.ReadModels.Tree.CropZoneDetailsView>(((CropZoneTreeItemViewModel)treeModel).Id).GetValue(() => null);

				if (czDetails != null) {
					cropId = czDetails.CropId;
				}

				var czModel = (CropZoneTreeItemViewModel)treeModel;

				// TODO: need cancellability
				var analysis = await CalculateFertilizerUsage(filtered, ((CropZoneTreeItemViewModel)treeModel).Area, cropId);
				FertilizerInformation = analysis.FertilizerFormulation;
				analysis.ActiveIngredients.Sort(delegate(ActiveIngredientLineItemViewModel g1, ActiveIngredientLineItemViewModel g2) {
					return g1.PercentRemaining.CompareTo(g2.PercentRemaining);
				});

				ActiveIngredients = analysis.ActiveIngredients;
				RaisePropertyChanged(() => ActiveIngredients);
                IsCropZone = true;
                RaisePropertyChanged(() => IsCropZone);
            }
            else
            {
                //turn visibility off on fertilizer and active ingredient
                FertilizerInformation = null;
                RaisePropertyChanged(() => FertilizerInformation);
                IsCropZone = false;
                RaisePropertyChanged(() => IsCropZone);
            }
            
			RefreshYieldSummary(treeModel);
		}

		async void RefreshYieldSummary(AbstractTreeItemViewModel treeModel) {
			RevenueSummary.YieldSummaryItems.Clear();

			var currentCropYearId = new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear);

			var commodityList = from item in yieldSummaryModel.LoadLineItems
								group item by item.Commodity into g
								orderby g.Count() descending
								select g.Key;

			foreach (var commodity in commodityList) {
				var itemsWithThisCommodity = yieldSummaryModel.LoadLineItems.Where(x => x.Commodity == commodity);

				var sortedUnitFrequencyList = from item in itemsWithThisCommodity
											  where item.FinalQuantityUnit != null
											  group item by item.FinalQuantityUnit into g
											  orderby g.Count() descending
											  select g.Key;

				foreach (var unit in sortedUnitFrequencyList) {
					var itemsWithThisUnit = itemsWithThisCommodity.Where(x => x.FinalQuantityUnit == unit);

					var totalQuantity = itemsWithThisUnit.Sum(x => x.WeightedFinalQuantity);
					var growerShare = itemsWithThisUnit.Sum(x => x.GrowerShareQuantity);

					decimal commodityAverageSalePrice = 0;
					decimal fieldToPosAverageSalePrice = 0;

					if (treeModel is CropZoneTreeItemViewModel) {
						var commoditySummary = new Yield.CommoditySummaryViewModel(clientEndpoint, dispatcher, currentCropYearId, commodity.CropId, commodity.CommodityDescription);
						if (commoditySummary.UnitSummaries.ContainsKey(unit)) {
							commodityAverageSalePrice = commoditySummary.UnitSummaries[unit].GrowerAverageRevenue;
						}

						var growerSale = itemsWithThisUnit.Sum(x => x.GrowerPrice);
						fieldToPosAverageSalePrice = growerShare != 0 ? growerSale / growerShare : 0;
					}

					RevenueSummary.YieldSummaryItems.Add(new YieldSummaryItemViewModel(RevenueSummary.Area) {
						Description = !string.IsNullOrWhiteSpace(commodity.CommodityDescription) ? commodity.CommodityDescription : commodity.CropName,
						TotalYieldQuantity = totalQuantity,
						GrowerQuantity = growerShare,
						YieldQuantityUnit = unit,
						CommodityAverageSalePrice = commodityAverageSalePrice,
						FieldToPosAverageSalePrice = fieldToPosAverageSalePrice,
					});
				}
			}

			// add a blank item if there aren't any
			if (!RevenueSummary.YieldSummaryItems.Any()) {
				RevenueSummary.YieldSummaryItems.Add(new YieldSummaryItemViewModel(RevenueSummary.Area));
			}

			RevenueSummary.SetYieldPricingViewStrategy(SelectedYieldPricingViewStrategy);
		}
		#endregion

		#region Cost Summary Recalc
		async void RecalculateCostSummaryByAverage() {
			RevenueSummary.ClearProductionCosts();
			Varieties.Clear();

			foreach (var x in ApplicationLineItems) {
				var p = clientEndpoint.GetMasterlistService().GetProduct(x.ProductId);
				if (p != null) {
					// TODO: Calculating cost summaries like this will have perf implications for large lists. 
					// Look to offload this to another thread in the future. -bs

					if (p.ProductType == GlobalStrings.ProductType_Seed) {
						Varieties.Add(new VarietyItemViewModel(string.Format("{0} ({1})", p.Name, p.Manufacturer), x.StartDate));
						RevenueSummary.SeedTotalCost += (decimal)x.TotalCost;
					}
					if (p.ProductType == GlobalStrings.ProductType_Fertilizer) {
						RevenueSummary.FertilizerTotalCost += (decimal)x.TotalCost;
					}
					if (p.ProductType == GlobalStrings.ProductType_CropProtection) {
						RevenueSummary.CropProtectionTotalCost += (decimal)x.TotalCost;
					}
					if (p.ProductType == GlobalStrings.ProductType_Service) {
						RevenueSummary.ServicesTotalCost += (decimal)x.TotalCost;
					}
				}
			}

			RaisePropertyChanged(() => AlternativeProductionTotalCostDeviation);
			RaisePropertyChanged(() => AlternativeProductionCostPerAreaDeviation);
		}

		async void RecalculateCostSummaryByInvoice() {
			RevenueSummary.ClearProductionCosts();
			Varieties.Clear();
			foreach (var x in ApplicationLineItems) {
				var p = clientEndpoint.GetMasterlistService().GetProduct(x.ProductId);
				if (p != null) {
					// TODO: Calculating cost summaries like this will have perf implications for large lists. 
					// Look to offload this to another thread in the future. -bs

					if (p.ProductType == GlobalStrings.ProductType_Seed) {
						Varieties.Add(new VarietyItemViewModel(string.Format("{0} ({1})", p.Name, p.Manufacturer), x.StartDate));
						RevenueSummary.SeedTotalCost += (decimal)x.TotalInvoicedCost;
					}
					if (p.ProductType == GlobalStrings.ProductType_Fertilizer) {
						RevenueSummary.FertilizerTotalCost += (decimal)x.TotalInvoicedCost;
					}
					if (p.ProductType == GlobalStrings.ProductType_CropProtection) {
						RevenueSummary.CropProtectionTotalCost += (decimal)x.TotalInvoicedCost;
					}
					if (p.ProductType == GlobalStrings.ProductType_Service) {
						RevenueSummary.ServicesTotalCost += (decimal)x.TotalInvoicedCost;
					}
				}
			}
			RaisePropertyChanged(() => AlternativeProductionTotalCostDeviation);
			RaisePropertyChanged(() => AlternativeProductionCostPerAreaDeviation);
		}

		async void RecalculateCostSummaryBySpecific() {
			RevenueSummary.ClearProductionCosts();
			Varieties.Clear();
			foreach (var x in ApplicationLineItems) {
				var p = clientEndpoint.GetMasterlistService().GetProduct(x.ProductId);
				if (p != null) {
					// TODO: Calculating cost summaries like this will have perf implications for large lists. 
					// Look to offload this to another thread in the future. -bs

					if (p.ProductType == GlobalStrings.ProductType_Seed) {
						Varieties.Add(new VarietyItemViewModel(string.Format("{0} ({1})", p.Name, p.Manufacturer), x.StartDate));
						RevenueSummary.SeedTotalCost += (decimal)x.TotalSpecificCost;
					}
					if (p.ProductType == GlobalStrings.ProductType_Fertilizer) {
						RevenueSummary.FertilizerTotalCost += (decimal)x.TotalSpecificCost;
					}
					if (p.ProductType == GlobalStrings.ProductType_CropProtection) {
						RevenueSummary.CropProtectionTotalCost += (decimal)x.TotalSpecificCost;
					}
					if (p.ProductType == GlobalStrings.ProductType_Service) {
						RevenueSummary.ServicesTotalCost += (decimal)x.TotalSpecificCost;
					}
				}
			}
			RaisePropertyChanged(() => AlternativeProductionTotalCostDeviation);
			RaisePropertyChanged(() => AlternativeProductionCostPerAreaDeviation);
		}
		#endregion

		async Task<AnalysisModel> CalculateFertilizerUsage(IEnumerable<CropZoneApplicationDataItem> items, Measure czArea, CropId cropId) {
			var currentCropYearId = new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear);

			FertilizerFormulationViewModel fertModel = new FertilizerFormulationViewModel();
			AnalysisModel model = new AnalysisModel() { FertilizerFormulation = fertModel };

			if (czArea.Value == 0) { return model; }

			var ml = clientEndpoint.GetMasterlistService();
			var userSettings = clientEndpoint.GetUserSettings();
			double divisor = 1;
			double prodRate = 0;
			double areaRatio = 1;

			Dictionary<Guid, double> aiQuantitiesUsed = new Dictionary<Guid, double>();

			foreach (var item in items) {
				var product = ml.GetProduct(item.ProductId);

				if (product == null) {
					var userCreatedList = clientEndpoint.GetView<UserCreatedProductList>(new DataSourceId(currentCropYearId.DataSourceId)).GetValue(new UserCreatedProductList());
					var userCreated = userCreatedList.UserCreatedProducts.Where(x => x.Id == item.ProductId.Id).FirstOrDefault();
					product = userCreated;
				}

                List<ActiveIngredientContent> aiContent = new List<ActiveIngredientContent>();
                foreach (var associate in item.AssociatedProducts)
                {
                    var associateMlp = ml.GetProduct(associate.ProductId);
                    var associateRatePerArea = UnitFactory.GetPackageSafeUnit(associate.RatePerAreaUnit, associateMlp.StdUnit, associateMlp.StdFactor, associateMlp.StdPackageUnit).GetMeasure((double)associate.RatePerAreaValue, associateMlp.Density);
                    if (associateRatePerArea is AgC.UnitConversion.MassAndWeight.MassAndWeightMeasure || (associateRatePerArea is AgC.UnitConversion.Volume.VolumeMeasure && associateRatePerArea.CanConvertTo(userSettings.UserMassUnit)))
                    {
                        divisor = 100;
                        var associateRate = associateRatePerArea.CreateAs(userSettings.UserMassUnit).Value;

                        foreach (var ai in associateMlp.ActiveIngredients ?? new List<ActiveIngredientContent>())
                        {
                            if (ai.Id == Guid.Empty) { continue; }
                            double massOfAiApplied = associateRate * areaRatio * (double)ai.Percent;

                            if (Landdb.Domain.ReadModels.Services.ActiveIngredientXRef.Self.IsGoodActiveIngredient(ai.Id))
                            {
                                var _id = Landdb.Domain.ReadModels.Services.ActiveIngredientXRef.Self.CrossReferenceActiveIngredient(ai.Id);
                                if (aiQuantitiesUsed.ContainsKey(_id))
                                {
                                    aiQuantitiesUsed[_id] += massOfAiApplied;
                                }
                                else
                                {
                                    aiQuantitiesUsed.Add(_id, massOfAiApplied);
                                }
                            }
                        }
                    }
                }

                IUnit rateUnit = UnitFactory.GetUnitByName(item.RateUnit);
				Measure rateMeasure;
				rateMeasure = rateUnit.GetMeasure((double)item.RateValue, product.Density);

				if (item.AreaUnit == czArea.Unit.Name) {
					areaRatio = (item.AreaValue * item.CoveragePercent) / czArea.Value;
				} else {
					var itemAreaUnit = UnitFactory.GetUnitByName(item.AreaUnit);
					var itemArea = itemAreaUnit.GetMeasure(item.AreaValue);
					var consistentItemArea = itemArea.GetValueAs(czArea.Unit);
					areaRatio = (consistentItemArea * item.CoveragePercent) / czArea.Value;
				}

				if (rateMeasure is AgC.UnitConversion.Area.AreaMeasure) {
					divisor = 1;
					prodRate = rateMeasure.CreateAs(userSettings.UserAreaUnit).Value;
				} else if (rateMeasure is AgC.UnitConversion.MassAndWeight.MassAndWeightMeasure || (rateMeasure is AgC.UnitConversion.Volume.VolumeMeasure && rateMeasure.CanConvertTo(userSettings.UserMassUnit))) {
					divisor = 100;
					prodRate = rateMeasure.CreateAs(userSettings.UserMassUnit).Value;

					foreach (var ai in product.ActiveIngredients ?? new List<ActiveIngredientContent>()) {
						if (ai.Id == Guid.Empty) { continue; }
						double massOfAiApplied = prodRate * areaRatio * (double)ai.Percent;

                        if (Landdb.Domain.ReadModels.Services.ActiveIngredientXRef.Self.IsGoodActiveIngredient(ai.Id)) {
                            var _id = Landdb.Domain.ReadModels.Services.ActiveIngredientXRef.Self.CrossReferenceActiveIngredient(ai.Id);
                            if (aiQuantitiesUsed.ContainsKey(_id)) {
                                aiQuantitiesUsed[_id] += massOfAiApplied;
                            }
                            else {
                                aiQuantitiesUsed.Add(_id, massOfAiApplied);
                            }
                        }
					}
				} else {
					continue;
				}

				decimal multiplicand = (decimal)prodRate * (decimal)areaRatio / (decimal)divisor;

				if (product.Formulation != null) {
					var f = product.Formulation;

					if (f.N.HasValue) { fertModel.N += f.N.Value * multiplicand; }
					if (f.P.HasValue) { fertModel.P += f.P.Value * multiplicand; }
					if (f.K.HasValue) { fertModel.K += f.K.Value * multiplicand; }
					if (f.Ca.HasValue) { fertModel.Ca += f.Ca.Value * multiplicand; }
					if (f.Mg.HasValue) { fertModel.Mg += f.Mg.Value * multiplicand; }
					if (f.S.HasValue) { fertModel.S += f.S.Value * multiplicand; }
					if (f.B.HasValue) { fertModel.B += f.B.Value * multiplicand; }
					if (f.Cl.HasValue) { fertModel.Cl += f.Cl.Value * multiplicand; }
					if (f.Cu.HasValue) { fertModel.Cu += f.Cu.Value * multiplicand; }
					if (f.Fe.HasValue) { fertModel.Fe += f.Fe.Value * multiplicand; }
					if (f.Mn.HasValue) { fertModel.Mn += f.Mn.Value * multiplicand; }
					if (f.Mo.HasValue) { fertModel.Mo += f.Mo.Value * multiplicand; }
					if (f.Zn.HasValue) { fertModel.Zn += f.Zn.Value * multiplicand; }
				}
			}

			foreach (var activeIngredientId in aiQuantitiesUsed.Keys) {
				var aiId = new ActiveIngredientId(activeIngredientId);
				var aiName = ml.GetActiveIngredientDisplay(aiId);
				var usedQuantity = aiQuantitiesUsed[activeIngredientId];
				double? maxQuantity = null;

				var aiSetting = clientEndpoint.GetView<ActiveIngredientSettingsView>(new ActiveIngredientSettingId(currentCropYearId.DataSourceId, aiId));
				aiSetting.IfValue(x => {
					//added in an else statement to grab previous years value of max ai ~~ Shea
					var yearSettings = new List<ActiveIngredientSettingItem>();

					if (x.ItemsByYear.ContainsKey(currentCropYearId.Id)) {
						var item = x.ItemsByYear[currentCropYearId.Id].Where(y => y.CropId == cropId).FirstOrDefault();
						if (item != null) { maxQuantity = (double?)item.MaximumValue; }
					} else {
						yearSettings = (from m in x.ItemsByYear where m.Key < currentCropYearId.Id select m).Any()
							? (from m in x.ItemsByYear where m.Key < currentCropYearId.Id select m).Last().Value : new List<ActiveIngredientSettingItem>();

						var item = yearSettings.Where(c => c.CropId == cropId).Any() ? yearSettings.Where(c => c.CropId == cropId).Last() : null;
						if (item != null) { maxQuantity = (double?)item.MaximumValue; }
					}
				});

				model.ActiveIngredients.Add(new ActiveIngredientLineItemViewModel(clientEndpoint, userSettings, aiId, aiName, usedQuantity, maxQuantity, cropId));
			}

			return model;
		}

        private static ItemMapItem GetMostRecentShapeByYear(Lokad.Cqrs.Maybe<ItemMap> maps_a)
        {
            ItemMapItem item_map_item = null;
            if (maps_a.HasValue && maps_a.Value.MostRecentMapItem != null && maps_a.Value.MostRecentMapItem.CropYearChangeOccurred <= Infrastructure.ApplicationEnvironment.CurrentCropYear)
            {
                item_map_item = maps_a.Value.MostRecentMapItem;
            }
            else
            {
                ItemMapItem currentyearmostrecentboundarychange = (from md in maps_a.Value.MapItems where (md.CropYearChangeOccurred <= Infrastructure.ApplicationEnvironment.CurrentCropYear) select md).LastOrDefault();
                if (currentyearmostrecentboundarychange != null && !string.IsNullOrEmpty(currentyearmostrecentboundarychange.MapData))
                {
                    item_map_item = currentyearmostrecentboundarychange;
                }
                if (item_map_item == null)
                {
                    item_map_item = maps_a.Value.MostRecentMapItem;
                }
            }
            return item_map_item;
        }
    }



    public enum PricingViewStrategy {
		AverageCost = 0,
		InvoiceMatching = 1,
		SpecificCost = 2
	}

	public enum YieldPricingViewStrategy {
		AverageSalePrice,
		FieldDirectToPointOfSale
	}
}