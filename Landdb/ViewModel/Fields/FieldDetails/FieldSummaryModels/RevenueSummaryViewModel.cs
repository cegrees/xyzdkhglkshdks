﻿using AgC.UnitConversion;
using GalaSoft.MvvmLight;
using System.Collections.ObjectModel;
using System.Linq;
using Landdb.Resources;

namespace Landdb.ViewModel.Fields.FieldDetails {
    public class RevenueSummaryViewModel : ViewModelBase {

        public RevenueSummaryViewModel(double area, IUnit areaUnit, decimal cashRentPerArea) {
            Area = (decimal)area;
            if (Area <= 0) {
                Area = 1;
                ShowPerAreaValues = false;
            } else {
                ShowPerAreaValues = true;
            }

            AreaUnit = areaUnit;

            CashRentPerArea = cashRentPerArea;

            YieldSummaryItems = new ObservableCollection<YieldSummaryItemViewModel>();
            YieldSummaryItems.CollectionChanged += YieldSummaryItems_CollectionChanged;
        }

        private void YieldSummaryItems_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            RaisePropertyChanged(() => BreakEvenYieldVisible);
            RaisePropertyChanged(() => BreakEvenCostVisible);
        }

        public ObservableCollection<YieldSummaryItemViewModel> YieldSummaryItems { get; private set; }

        public decimal Area { get; private set; }
        public IUnit AreaUnit { get; private set; }
        public decimal CashRentPerArea { get; private set; }

        public void ClearProductionCosts() {
            CropProtectionTotalCost = 0;
            FertilizerTotalCost = 0;
            SeedTotalCost = 0;
            ServicesTotalCost = 0;
        }

        private bool _showPerAreaValues;
        public bool ShowPerAreaValues {
            get { return _showPerAreaValues; }
            set {
                _showPerAreaValues = value;
                RaisePropertyChanged(() => ShowPerAreaValues);
            }
        }

        private decimal _growerRevenue;
        public decimal GrowerRevenue {
            get { return _growerRevenue; }
            set {
                _growerRevenue = value;
                RaisePropertyChanged(() => GrowerRevenue);
                costChanged();
            }
        }

        private decimal _seedTotalCost;
        public decimal SeedTotalCost {
            get { return _seedTotalCost; }
            set {
                _seedTotalCost = value;
                RaisePropertyChanged(() => SeedTotalCost);
                RaisePropertyChanged(() => SeedCostPerArea);
                costChanged();
            }
        }

        private decimal _fertilizerTotalCost;
        public decimal FertilizerTotalCost {
            get { return _fertilizerTotalCost; }
            set {
                _fertilizerTotalCost = value;
                RaisePropertyChanged(() => FertilizerTotalCost);
                RaisePropertyChanged(() => FertilizerCostPerArea);
                costChanged();
            }
        }

        private decimal _cropProtectionTotalCost;
        public decimal CropProtectionTotalCost {
            get { return _cropProtectionTotalCost; }
            set {
                _cropProtectionTotalCost = value;
                RaisePropertyChanged(() => CropProtectionTotalCost);
                RaisePropertyChanged(() => CropProtectionCostPerArea);
                costChanged();
            }
        }

        private decimal _servicesTotalCost;
        public decimal ServicesTotalCost {
            get { return _servicesTotalCost; }
            set {
                _servicesTotalCost = value;
                RaisePropertyChanged(() => ServicesTotalCost);
                RaisePropertyChanged(() => ServicesCostPerArea);
                costChanged();
            }
        }

        public decimal TotalCost {
            get { return SeedTotalCost + FertilizerTotalCost + CropProtectionTotalCost + ServicesTotalCost; }
        }

        public decimal SeedCostPerArea {
            get { return SeedTotalCost / Area; }
        }

        public decimal FertilizerCostPerArea {
            get { return FertilizerTotalCost / Area; }
        }

        public decimal CropProtectionCostPerArea {
            get { return CropProtectionTotalCost / Area; }
        }

        public decimal ServicesCostPerArea {
            get { return ServicesTotalCost / Area; }
        }

        public decimal CostPerArea {
            get { return TotalCost / Area; }
        }

        public decimal ProductionRevenue {
            get { return GrowerRevenue - TotalCost; }
        }

        public decimal ProductionRevenuePerArea {
            get { return ProductionRevenue / Area; }
        }

        public bool IsCashRentVisible {
            get { return CashRentPerArea > 0; }
        }

        public decimal TotalCashRent {
            get { return CashRentPerArea * Area; }
        }

        public decimal TotalContributionMargin {
            get { return ProductionRevenue - TotalCashRent; }
        }

        public decimal ContributionMarginPerArea {
            get { return TotalContributionMargin / Area; }
        }

        public YieldSummaryItemViewModel FirstItem {
            get { return YieldSummaryItems.FirstOrDefault(); }
        }

        public CompositeUnit FirstItemUnit {
            get { return FirstItem?.YieldQuantityUnit; }
        }

        public decimal FirstItemSalePrice {
            get {
                var price = 0M;

                if (FirstItem != null) {
                    switch (FirstItem.PriceStrategy) {
                        case YieldPricingViewStrategy.AverageSalePrice:
                            price = FirstItem.CommodityAverageSalePrice;
                            break;
                        case YieldPricingViewStrategy.FieldDirectToPointOfSale:
                            price = FirstItem.FieldToPosAverageSalePrice;
                            break;
                        default:
                            break;
                    }
                }

                return price;
            }
        }

        public decimal FirstItemYieldPerArea {
            get { return FirstItem != null ? FirstItem.GrowerQuantityPerArea : 0; }
        }

        public bool BreakEvenYieldVisible
        {
            get
            {
                return YieldSummaryItems.Count < 2 ? true : false;
            }
        }
		public string BreakEvenYieldDescription {
			get { return string.Format(Strings.BreakEvenYield_Format_Text, FirstItemSalePrice, FirstItemUnit != null ? FirstItemUnit.AbbreviatedDisplay : Strings.Unit_Text.ToLower()); }
		}

		public decimal BreakEvenYield {
			get { return FirstItemSalePrice != 0 ? (CostPerArea + CashRentPerArea) / FirstItemSalePrice : 0; }
		}

        public bool BreakEvenCostVisible { get { return YieldSummaryItems.Count < 2 ? true : false; } }
        public string BreakEvenCostDescription {
			get { return string.Format(Strings.BreakEvenCost_Format_Text, FirstItemYieldPerArea, FirstItemUnit != null ? FirstItemUnit.AbbreviatedDisplay : Strings.Unit_Text.ToLower(), AreaUnit != null ? AreaUnit.AbbreviatedDisplay : Strings.Area_Text.ToLower()); }
		}

		public decimal BreakEvenCost {
			get { return FirstItemYieldPerArea != 0 ? (CostPerArea + CashRentPerArea) / FirstItemYieldPerArea : 0; }
		}

		public void SetYieldPricingViewStrategy(YieldPricingViewStrategy strategy) {
			decimal newGrowerRevenue = 0;

			foreach (var item in YieldSummaryItems) {
				item.SetPricingViewStrategy(strategy);
				newGrowerRevenue += item.GrowerRevenue;
			}

			GrowerRevenue = newGrowerRevenue;

			RaisePropertyChanged(() => BreakEvenYieldDescription);
			RaisePropertyChanged(() => BreakEvenYield);
			RaisePropertyChanged(() => BreakEvenCostDescription);
			RaisePropertyChanged(() => BreakEvenCost);
		}

		private void costChanged() {
				RaisePropertyChanged(() => TotalCost);
				RaisePropertyChanged(() => CostPerArea);
				RaisePropertyChanged(() => ProductionRevenue);
				RaisePropertyChanged(() => ProductionRevenuePerArea);
				RaisePropertyChanged(() => TotalContributionMargin);
				RaisePropertyChanged(() => ContributionMarginPerArea);
		}
	}
}