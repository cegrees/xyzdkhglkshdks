﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Yield;
using Landdb.Infrastructure;
using Landdb.ViewModel.Yield;
using System;
using System.Linq;
using System.Windows.Input;
using Landdb.Resources;

namespace Landdb.ViewModel.Fields.FieldDetails {
	public class LoadLineItemViewModel : ViewModelBase {

		readonly CropZoneLoadDataItem item;
		readonly decimal growerSharePercentage;

		public LoadLineItemViewModel(CropZoneLoadDataItem item, decimal growerSharePercentage) {
			this.item = item;
			this.growerSharePercentage = growerSharePercentage;

			JumpToYieldLoadCommand = new RelayCommand(onJumpToYieldLoad);
		}

		public ICommand JumpToYieldLoadCommand { get; }

		public DateTime StartDate => item.StartDateTime;
		public string LoadNumber => item.LoadNumber;
		public string DestinationLocationName {
			get {
				var retString = string.Empty;

				if (item.DestinationLocations.Count == 1) {
					retString = item.DestinationLocations.Single().Name;
				} else {
					var distinctParents = item.DestinationLocations.Select(x => x.ParentLocationId).Distinct();

					if (distinctParents.Count() == 1) {
						var yieldLocationList = YieldLocationListItemViewModel.GetFlattenedListItemsForCropYear(
							ServiceLocator.Get<IClientEndpoint>(),
							new CropYearId(item.LoadId.DataSourceId, ApplicationEnvironment.CurrentCropYear),
							false
						);

						retString = yieldLocationList.SingleOrDefault(x => x.Id == distinctParents.Single())?.FullDisplay ?? Strings.UnknownLocation_Text;
					} else {
						retString = String.Format(Strings.Locations_Format_Text, item.DestinationLocations.Count);
					}
				}

				return retString;
			}
		}

		public string LocationRangeDisplayText => item.DestinationLocations.Count > 1 ? string.Join(", ", item.DestinationLocations.Select(x => x.Name)) : string.Empty;
		public string TruckName => item.TruckName;
		public string DriverName => item.DriverName;
		public decimal GrowerShareQuantity => growerSharePercentage * item.AreaWeightedFinalQuantity;
		public decimal GrowerPrice => item.AreaWeightedGrowerPrice;

		public string GrowerShareQuantityDisplayText => $"{GrowerShareQuantity:N2} {item.FinalQuantityUnit.FullDisplay}";
		public string FinalQuantityDisplayText => $"{item.AreaWeightedFinalQuantity:N2} {item.FinalQuantityUnit.AbbreviatedDisplay}";

		public IncludedCommodity Commodity => item.Commodity;
		public CompositeUnit FinalQuantityUnit => item.FinalQuantityUnit;
		public decimal WeightedFinalQuantity => item.AreaWeightedFinalQuantity;
		public decimal WeightedFinalPrice => item.AreaWeightedGrowerPrice + item.AreaWeightedLandOwnerPrice;

		public string ShareDescription {
			get {
				if (growerSharePercentage == 1) {
					return Strings.OneHundredPercentGrowerShare_Text;
				} else {
					return $"{growerSharePercentage:P} / {FinalQuantityDisplayText}";
				}
			}
		}

		public string GrowerPriceDisplayText {
			get {
				var retDisplayText = string.Empty;

				if (item.IsLoadSold) {
					retDisplayText = GrowerPrice.ToString("C");
				}

				return retDisplayText;
			}
		}

		public string GrowerUnitPriceDisplayText {
			get {
				var retDisplayText = string.Empty;

				if (item.IsLoadSold) {
					decimal unitPrice = 0;

					if (GrowerShareQuantity != 0) {
						unitPrice = GrowerPrice / GrowerShareQuantity;
					}

					retDisplayText = $"{unitPrice:C} / {item.FinalQuantityUnit.AbbreviatedDisplay}";
				}

				return retDisplayText;
			}
		}

		private void onJumpToYieldLoad() {
			ViewRouter.JumpToYieldLoad(item.LoadId, true);
		}
	}
}
