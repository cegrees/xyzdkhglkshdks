﻿using AgC.UnitConversion;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Infrastructure;
using Landdb.Services;
using Landdb.ViewModel.Secondary.Application;
using Landdb.ViewModel.Shared;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Landdb.Resources;

namespace Landdb.ViewModel.Fields.FieldDetails {
	public class ApplicationLineItemViewModel : ViewModelBase {

		readonly IClientEndpoint clientEndpoint;

		readonly CropZoneApplicationDataItem item;
		readonly InventoryListView inventory;
		readonly IInventoryCalculationService inventoryServices;

		Measure displayRate = null;
		Measure displayTotal = null;

		double? costPerArea;
		double? totalCost;
		double? totalInvoicedCost;
		double? invoicedCostPerArea;
		double? totalSpecificCost;
		double? specificCostPerArea;
		double? density;

		public ApplicationLineItemViewModel(IClientEndpoint clientEndpoint, IInventoryCalculationService inventoryServices, InventoryListView inventory, CropZoneApplicationDataItem item) {
			this.clientEndpoint = clientEndpoint;
			this.inventoryServices = inventoryServices;

			this.inventory = inventory;
			this.item = item;

			if (inventory != null && inventory.Products.ContainsKey(item.ProductId)) {
				var ip = inventory.Products[item.ProductId];
				var costPerUnit = (decimal)ip.AveragePriceValue;
				var costUnit = ip.AveragePriceUnit ?? item.TotalProductUnit;

				CalculateCosts();
				totalInvoicedCost = CalculateInvoicedCost();
				invoicedCostPerArea = totalInvoicedCost.HasValue ? (double?)(totalInvoicedCost.GetValueOrDefault() / (item.AreaValue * item.CoveragePercent)) : null;

				totalSpecificCost = CalculateSpecificCost();
				specificCostPerArea = totalSpecificCost.HasValue ? (double?)(totalSpecificCost.GetValueOrDefault() / (item.AreaValue * item.CoveragePercent)) : null;
			} else {
				costPerArea = item.CostPerArea;
				totalCost = item.TotalCost;
			}

			this.density = clientEndpoint.GetProductUnitResolver().GetDensity(item.ProductId);
			displayRate = DisplayMeasureConverter.ConvertToRateDisplayMeasure(item.ProductId, item.RateValue, item.RateUnit, density, clientEndpoint, ApplicationEnvironment.CurrentDataSourceId);

			//TODO  The following code related to NaN is a bandaid just so the grower/support can fix their own data  -mh
			if (item.TotalProductValue.ToString() == "NaN") { item.TotalProductValue = 0; }
			displayTotal = DisplayMeasureConverter.ConvertToTotalDisplayMeasure(item.ProductId, (decimal)item.TotalProductValue, item.TotalProductUnit, density, clientEndpoint, ApplicationEnvironment.CurrentDataSourceId);

            AssociatedProducts = new ObservableCollection<AssociatedProductItem>();
            foreach (var associate in item.AssociatedProducts)
            {
                var associateProd = new AssociatedProductItem(clientEndpoint, clientEndpoint.GetMasterlistService().GetProduct(item.ProductId), null);
                var associateMLP = clientEndpoint.GetMasterlistService().GetProduct(associate.ProductId);
                associateProd.CustomUnit = UnitFactory.GetPackageSafeUnit(associate.CustomProductUnit, associateMLP.StdUnit, associateMLP.StdFactor, associateMLP.StdPackageUnit);
                associateProd.CustomRateValue = associate.CustomProductValue;
                associateProd.HasCost = associate.HasCost;
                associateProd.CostPerUnitValue = associate.HasCost ? associate.SpecificCostPerUnitValue : 0;
                associateProd.CostPerUnitUnit = associate.HasCost && !string.IsNullOrEmpty(associate.SpecificCostPerUnitUnit) ? UnitFactory.GetPackageSafeUnit(associate.SpecificCostPerUnitUnit, associateMLP.StdUnit, associateMLP.StdFactor, associateMLP.StdPackageUnit) : null;
                associateProd.ProductID = associate.ProductId;
                associateProd.ProductName = associate.ProductName;
                associateProd.RatePerAreaUnit = UnitFactory.GetPackageSafeUnit(associate.RatePerAreaUnit, associateMLP.StdUnit, associateMLP.StdFactor, associateMLP.StdPackageUnit);
                associateProd.RatePerAreaValue = associate.RatePerAreaValue;
                //associateProd.CostPerUnitUnit = UnitFactory.GetPackageSafeUnit(associate.SpecificCostPerUnitUnit, associateMLP.StdUnit, associateMLP.StdFactor, associateMLP.StdPackageUnit);
                //associateProd.CostPerUnitValue = associate.SpecificCostPerUnitValue;
                associateProd.TotalProductUnit = UnitFactory.GetPackageSafeUnit(associate.TotalProductUnit, associateMLP.StdUnit, associateMLP.StdFactor, associateMLP.StdPackageUnit);
                associateProd.TotalProductValue = associate.TotalProductValue;
                associateProd.TrackingID = associate.TrackingId;

                switch (associate.CustomRateType)
                {
                    case "ByBag":
                        associateProd.CustomRateType = AssociatedProductRateType.ByBag;
                        break;
                    case "ByCWT":
                        associateProd.CustomRateType = AssociatedProductRateType.ByCWT;
                        break;
                    case "ByRow":
                        associateProd.CustomRateType = AssociatedProductRateType.ByRow;
                        break;
                    case "BySeed":
                        associateProd.CustomRateType = AssociatedProductRateType.BySeed;
                        break;
                    default:
                        associateProd.CustomRateType = AssociatedProductRateType.BySeed;
                        break;
                }

                HasAssociatedProducts = true;
                AssociatedProducts.Add(associateProd);
            }

            RaisePropertyChanged(() => AssociatedProducts);
            RaisePropertyChanged(() => HasAssociatedProducts);
            RaisePropertyChanged(() => HasAnyCost);


            JumpToApplicationCommand = new RelayCommand(JumpToApplication);
		}

		#region Properties
		public ICommand JumpToApplicationCommand { get; private set; }

		public ApplicationId ApplicationId {
			get { return item.ApplicationId; }
		}
		public DateTime StartDate {
			get { return item.StartDate.ToLocalTime(); }
		}
		public ProductId ProductId {
			get { return item.ProductId; }
		}

		public string ProductDisplay {
			get {
				var display = Strings.UnknownProduct_Text;
				var ml = clientEndpoint.GetMasterlistService();

				if (ml != null) { display = ml.GetProductDisplay(item.ProductId); }

				return display;
			}
		}

		public string RateDisplay {
			get {
				var unit = displayRate.Unit;
				var areaUnit = UnitFactory.GetUnitByName(item.AreaUnit);
				return string.Format("{0:n2} {1} / {2} ({3} {4})", displayRate.Value, unit.FullDisplay, areaUnit.AbbreviatedDisplay, AppliedArea.ToString("n2"), areaUnit.AbbreviatedDisplay);
			}
		}

	    public string PerAreaDisplay => $"{Strings.Per_Text.ToLower()} {UnitFactory.GetUnitByName(item.AreaUnit).AbbreviatedDisplay}";

		public decimal RateValue {
			get { return item.RateValue; }
		}

		public string RateUnit {
			get { return item.RateUnit; }
		}

		public double AreaValue {
			get { return item.AreaValue; }
		}

		public string AreaUnit {
			get { return item.AreaUnit; }
		}

		public double AppliedArea {
			get { return item.AreaValue * item.CoveragePercent; }
		}

		public bool HasInvoiceCost {
			get { return totalInvoicedCost != null; }
		}

		public bool HasSpecificCost {
			get { return totalSpecificCost != null; }
		}

		public double CostPerArea {
			get {
				// Lazy-init this for perf
				if (costPerArea == null) { CalculateCosts(); }
				return costPerArea.GetValueOrDefault();
			}
		}

		public string TotalProductDisplay {
			get {
				var unit = displayTotal.Unit;
				return string.Format("{0:n2} {1}", displayTotal.Value, unit.FullDisplay);
			}
		}

		public double TotalProductValue {
			get { return item.TotalProductValue; }
		}

		public string TotalProductUnit {
			get { return item.TotalProductUnit; }
		}

		public double TotalCost {
			get {
				// Lazy-init this for perf
				if (totalCost == null) { CalculateCosts(); }
				return totalCost.GetValueOrDefault();
			}
		}

		public double TotalInvoicedCost {
			get {
				if (totalInvoicedCost == null) {
					return TotalCost;
				} else {
					return totalInvoicedCost.GetValueOrDefault();
				}
			}
		}

		public double InvoicedCostPerArea {
			get {
				if (invoicedCostPerArea == null) {
					return CostPerArea;
				} else {
					return invoicedCostPerArea.GetValueOrDefault();
				}
			}
		}

		public double CostPerUnitValue { get; set; }
		public string CostPerUnit { get; set; }

		public double TotalSpecificCost {
			get {
				if (totalSpecificCost == null) {
					return TotalCost;
				} else {
					return totalSpecificCost.GetValueOrDefault();
				}
			}
		}

		public double SpecificCostPerArea {
			get {
				if (specificCostPerArea == null) {
					return CostPerArea;
				} else {
					return specificCostPerArea.GetValueOrDefault();
				}
			}
		}

		public double TotalShareOwnerQuantity {
			get { return item.TotalProductValue - item.TotalGrowerProductValue; }
		}

		public double TotalGrowerQuantity {
			get { return item.TotalGrowerProductValue; }
		}

		public double GrowerShare {
			get { return item.GrowerShare; }
		}

        private ObservableCollection<AssociatedProductItem> _associatedProducts { get; set; }
        public ObservableCollection<AssociatedProductItem> AssociatedProducts
        {
            get
            {
                return _associatedProducts;
            }
            set
            {
                _associatedProducts = value;
                RaisePropertyChanged(() => AssociatedProducts);
            }
        }

        private bool _hasAssociatedProducts { get; set; }
        public bool HasAssociatedProducts { get { return _hasAssociatedProducts; } set { _hasAssociatedProducts = value; RaisePropertyChanged(() => HasAssociatedProducts); } }

        public bool HasAnyCost { get { return AssociatedProducts.Any() && AssociatedProducts.Any(x => x.HasCost == true) ? true : false; } }

        #endregion

        #region Cost Calculations
        void CalculateCosts() {
			if (inventory != null && inventory.Products.ContainsKey(item.ProductId)) {
				var ip = inventory.Products[item.ProductId];
				var costPerUnit = Convert.ToDecimal(ip.AveragePriceValue * item.GrowerShare);
				var costUnit = ip.AveragePriceUnit ?? clientEndpoint.GetProductUnitResolver().GetProductPackageUnit(item.ProductId).Name;
				CostPerUnitValue = (double)costPerUnit;
				CostPerUnit = costUnit;

				try {
					MiniProduct miniprod = clientEndpoint.GetMasterlistService().GetProduct(item.ProductId);
					var totalMeasure = clientEndpoint.GetProductUnitResolver().GetPackageSafeUnit(item.ProductId, item.TotalProductUnit).GetMeasure((double)item.TotalProductValue, miniprod.Density);
					var convertedTotalMeasure = totalMeasure.GetValueAs(clientEndpoint.GetProductUnitResolver().GetPackageSafeUnit(item.ProductId, costUnit));
					totalCost = convertedTotalMeasure * (double)costPerUnit;

					//ISSUES FOUND IN FEWEL FARMS DATASOURCE CUSTOM FERT BOUGHT BY THE TON APPLIED BY THE GALLON...TOTAL PRICE WAS OFF
					//var costMeasure = clientEndpoint.GetProductUnitResolver().GetPackageSafeUnit(item.ProductId, costUnit).GetMeasure((double)costPerUnit, miniprod.Density); // TODO: Account for density?
					//var totalCostMeasure = costMeasure.GetValueAs(clientEndpoint.GetProductUnitResolver().GetPackageSafeUnit(item.ProductId, item.TotalProductUnit));
					//totalCost = totalCostMeasure * item.TotalGrowerProductValue;
					costPerArea = totalCost / (item.AreaValue * item.CoveragePercent);
				} catch {
					// TODO FIX HACK - this bunch of code is a hack to get around incompatible measues - MH
					MiniProduct miniprod = clientEndpoint.GetMasterlistService().GetProduct(item.ProductId);
					double? proddensity = miniprod.Density;
					var costMeasure = clientEndpoint.GetProductUnitResolver().GetPackageSafeUnit(item.ProductId, costUnit).GetMeasure((double)costPerUnit, proddensity);
					var psu = clientEndpoint.GetProductUnitResolver().GetProductPackageUnit(item.ProductId);
					var totalCostMeasure = costMeasure.GetValueAs(psu);
					totalCost = totalCostMeasure * item.TotalGrowerProductValue;
                    costPerArea = totalCost / (item.AreaValue * item.CoveragePercent);
					//
				}
			} else {
				costPerArea = item.CostPerArea;
				totalCost = item.TotalCost;
			}
		}

		double? CalculateInvoicedCost() {
			var application = clientEndpoint.GetView<ApplicationView>(this.ApplicationId);
			if (application.HasValue && inventory != null && inventory.Products.ContainsKey(item.ProductId)) {
				var ip = inventory.Products[item.ProductId];
				var costUnit = item.TotalProductUnit ?? clientEndpoint.GetProductUnitResolver().GetProductPackageUnit(item.ProductId).Name;

				var invoices = (from d in application.Value.Sources
								where d.Identity is InvoiceId
								select clientEndpoint.GetView<Landdb.Domain.ReadModels.Invoice.InvoiceView>((InvoiceId)d.Identity).GetValue(() => null)).ToList();

				invoices.RemoveAll(x => x == null);

				if (!invoices.Any()) { return null; }

				var lineItems = from i in invoices
								from li in i.Products
								where !i.IsMarkedAsDeleted && li.ProductId == item.ProductId
								select li;

				if (!lineItems.Any()) { return null; }

				string unit = null;
				double value = 0;
				decimal cost = 0;
				MiniProduct miniprod = clientEndpoint.GetMasterlistService().GetProduct(item.ProductId);
				double? proddensity = miniprod.Density;

				foreach (var li in lineItems) {
					var newTotal = inventoryServices.SumTotalMeasure(item.ProductId, (double)li.TotalProductValue, li.TotalProductUnit, value, unit, clientEndpoint.GetProductUnitResolver(), proddensity);
					value = newTotal.Value;
					unit = newTotal.Unit.Name;
					cost += li.TotalCost;
				}

				var invoiceQuantityMeasure = clientEndpoint.GetProductUnitResolver().GetPackageSafeUnit(item.ProductId, unit).GetMeasure(value, proddensity);
				var costMeasureUnit = clientEndpoint.GetProductUnitResolver().GetPackageSafeUnit(item.ProductId, costUnit);
				var avgInvoiceQuantityInInventoryUnits = invoiceQuantityMeasure.CreateAs(costMeasureUnit);
				var costPerInventoryUnit = avgInvoiceQuantityInInventoryUnits.Value != 0 ? (double)cost / avgInvoiceQuantityInInventoryUnits.Value : 0;

				return costPerInventoryUnit * item.TotalGrowerProductValue;
			} else {
				return null;
			}
		}

		double? CalculateSpecificCost() {
			var application = clientEndpoint.GetView<ApplicationView>(this.ApplicationId);
			if (application.HasValue && inventory != null && inventory.Products.ContainsKey(item.ProductId)) {
				var ip = inventory.Products[item.ProductId];
				var costUnit = item.TotalProductUnit ?? clientEndpoint.GetProductUnitResolver().GetProductPackageUnit(item.ProductId).Name;

				string unit = null;
				decimal cost = 0;
				MiniProduct miniprod = clientEndpoint.GetMasterlistService().GetProduct(item.ProductId);
				double? proddensity = miniprod.Density;

				var p = application.Value.Products.Where(x => x.TrackingId == item.ProductTrackingId).FirstOrDefault();
				if (p != null && p.SpecificCostPerUnit.HasValue) {
					cost = p.SpecificCostPerUnit.Value;
					unit = p.SpecificCostUnit;


					var specificQuantityMeasure = clientEndpoint.GetProductUnitResolver().GetPackageSafeUnit(item.ProductId, unit).GetMeasure(1.0, proddensity);
					var costMeasureUnit = clientEndpoint.GetProductUnitResolver().GetPackageSafeUnit(item.ProductId, costUnit);
					var avgSpecificQuantityInInventoryUnits = specificQuantityMeasure.CreateAs(costMeasureUnit);
					var costPerInventoryUnit = (double)cost / avgSpecificQuantityInInventoryUnits.Value;

					return costPerInventoryUnit * item.TotalGrowerProductValue;
				} else {
					return null;
				}
			} else {
				return null;
			}
		}
		#endregion

		void JumpToApplication() {
			ViewRouter.JumpToApplication(item.ApplicationId);
		}
	}
}