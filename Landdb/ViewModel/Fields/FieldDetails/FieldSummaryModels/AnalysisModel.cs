﻿using AgC.UnitConversion.MassAndWeight;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using System;
using System.Collections.Generic;
using System.Windows.Input;
using Landdb.Resources;

namespace Landdb.ViewModel.Fields.FieldDetails {
	public class AnalysisModel {
		public AnalysisModel() {
			ActiveIngredients = new List<ActiveIngredientLineItemViewModel>();
		}

		public FertilizerFormulationViewModel FertilizerFormulation { get; set; }
		public List<ActiveIngredientLineItemViewModel> ActiveIngredients { get; set; }
	}

	public class FertilizerFormulationViewModel : ViewModelBase {
		public decimal N { get; set; }
		public decimal P { get; set; }
		public decimal K { get; set; }
		public decimal Ca { get; set; }
		public decimal Mg { get; set; }
		public decimal S { get; set; }
		public decimal B { get; set; }
		public decimal Cl { get; set; }
		public decimal Cu { get; set; }
		public decimal Fe { get; set; }
		public decimal Mn { get; set; }
		public decimal Mo { get; set; }
		public decimal Zn { get; set; }

		public override string ToString() {
			return string.Format("{0} - {1} - {2}", N.ToString("n2"), P.ToString("n2"), K.ToString("n2"));			
		}
	}

	public class ActiveIngredientLineItemViewModel : ViewModelBase {

		readonly IClientEndpoint clientEndpoint;
		readonly UserSettings settings;
		readonly CropId CropId;

		public ActiveIngredientLineItemViewModel(IClientEndpoint clientEndpoint, UserSettings settings, ActiveIngredientId activeIngredientId, string name, double usedQuantity, double? maxQuantity, CropId cropId) {
			this.clientEndpoint = clientEndpoint;
			this.settings = settings;
			this.ActiveIngredientId = activeIngredientId;
			this.Name = name;
			this.UsedQuantity = usedQuantity;
			this.MaxQuantity = maxQuantity;
			this.CropId = cropId;

			UpdatePercentAndText();

			UpdateMaximumUseCommand = new RelayCommand(UpdateMaximumUse);
		}

		private void UpdatePercentAndText() {
			if (MaxQuantity.HasValue && MaxQuantity > 0) {
				this.PercentRemaining = ((MaxQuantity.Value - UsedQuantity) / MaxQuantity.Value) * 100;
				if (this.PercentRemaining < 0) { this.PercentRemaining = 0; }
				this.PercentRemainingText = string.Format(Strings.PercentRemaining_Text, this.PercentRemaining);
				this.QuantityText = string.Format(Strings.AnalysisQuantity_Text, UsedQuantity, MaxQuantity.Value, "lb", ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit);
			} else {
				this.PercentRemaining = 0;
				this.PercentRemainingText = string.Empty;
				this.QuantityText = string.Format(Strings.AnalysisQuantityPercentRemainingZero_Text, UsedQuantity, "lb", ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit);
			}

			RaisePropertyChanged("PercentRemaining");
			RaisePropertyChanged("PercentRemainingText");
			RaisePropertyChanged("QuantityText");
		}

		public ICommand UpdateMaximumUseCommand { get; private set; }

		public ActiveIngredientId ActiveIngredientId { get; set; }
		public string Name { get; set; }
		public double UsedQuantity { get; set; }
		public double? MaxQuantity { get; set; }
		public string QuantityText { get; set; }
		public double PercentRemaining { get; set; }
		public string PercentRemainingText { get; set; }

		public double Percentage {
			get {
				if (MaxQuantity == null || MaxQuantity == 0) {
					return double.NaN;
				} else {
					return UsedQuantity / MaxQuantity.Value;
				}
			}
		}

		public string RatioText {
			get {
				//string rateText = string.Format(Landdb.Properties.Resources.RatePerAreaDisplayText, DataConfiguration.Instance.UserMassUnit.AbbreviatedDisplay, DataConfiguration.Instance.UserAreaUnit.FullDisplay);
				string rateText = string.Format(Strings.Per_Format_Text, settings.UserMassUnit.AbbreviatedDisplay, settings.UserAreaUnit.FullDisplay);
				return string.Format("({0} / {1} {2})", UsedQuantity.ToString("n2"), MaxQuantity.GetValueOrDefault().ToString("n2"), rateText);
			}
		}

		public override string ToString() {
			return ActiveIngredientId.ToString() + " | " + UsedQuantity.ToString() + " | " + MaxQuantity.ToString() + " | " + Percentage.ToString() + "%";
		}

		void UpdateMaximumUse() {
			var vm = new Shared.Popups.EditActiveIngredientLoadViewModel((decimal)MaxQuantity.GetValueOrDefault(), x => {
				var domainCmd = new UpdateMaximumActiveIngredientUsePerCropYear(
					new ActiveIngredientSettingId(ApplicationEnvironment.CurrentDataSourceId, ActiveIngredientId),
					clientEndpoint.GenerateNewMetadata(), 
					x.MaximumValue,
					Pound.Self.Name, 
					CropId, 
					ApplicationEnvironment.CurrentCropYear,
					string.Empty
				);

				clientEndpoint.SendOne(domainCmd);

				if (x.MaximumValue <= 0) {
					MaxQuantity = null;
				} else {
					MaxQuantity = (double)x.MaximumValue;
				}

				UpdatePercentAndText();
			});

			var descriptor = new ScreenDescriptor("Landdb.Views.Shared.Popups.EditActiveIngredientLoadView", "EditAiMax", vm);
			Messenger.Default.Send<ShowPopupMessage>(new ShowPopupMessage() { ScreenDescriptor = descriptor });
		}
	}
}