﻿using Landdb.Client.Infrastructure.DisplayItems;
using GalaSoft.MvvmLight;
using Landdb.Infrastructure;

namespace Landdb.ViewModel.Fields.FieldDetails {
	public class YieldSummaryItemViewModel : ViewModelBase {

		readonly decimal area;

		public YieldSummaryItemViewModel(decimal area) {
			this.area = area;

			PriceStrategy = YieldPricingViewStrategy.AverageSalePrice;
		}

	    public static string AreaUnitDisplayText => $"/ {AreaUnitDisplayItem.GetAreaUnitDisplayTextFor(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit)}";
		public YieldPricingViewStrategy PriceStrategy { get; private set; }

		public decimal FieldToPosAverageSalePrice { get; set; }
		public decimal CommodityAverageSalePrice { get; set; }

		public decimal AverageSalePrice {
			get {
				switch (PriceStrategy) {
					case YieldPricingViewStrategy.FieldDirectToPointOfSale:
						return FieldToPosAverageSalePrice;
					case YieldPricingViewStrategy.AverageSalePrice:
					default:
						return CommodityAverageSalePrice;

				}
			}
		}

        private string _description;
		public string Description {
			get { return _description; }
			set {
				_description = value;
				RaisePropertyChanged(() => Description);
			}
		}

		private CompositeUnit _yieldQuantityUnit;
		public CompositeUnit YieldQuantityUnit {
			get => _yieldQuantityUnit;
		    set {
				_yieldQuantityUnit = value;
				RaisePropertyChanged(() => YieldQuantityUnit);
			}
		}

		private decimal _growerQuantity;
		public decimal GrowerQuantity {
			get { return _growerQuantity; }
			set {
				_growerQuantity = value;
				RaisePropertyChanged(() => GrowerQuantity);
				RaisePropertyChanged(() => GrowerQuantityPerArea);
				RaisePropertyChanged(() => GrowerRevenue);
				RaisePropertyChanged(() => GrowerRevenuePerArea);
			}
		}

		public decimal GrowerRevenue {
			get { return GrowerQuantity * AverageSalePrice; }
		}

		private decimal _totalYieldQuantity;
		public decimal TotalYieldQuantity {
			get { return _totalYieldQuantity; }
			set {
				_totalYieldQuantity = value;
				RaisePropertyChanged(() => TotalYieldQuantity);
				RaisePropertyChanged(() => TotalYieldQuantityPerArea);
			}
		}

		public decimal GrowerRevenuePerArea => area > 0 ? GrowerRevenue / area : 0;
		public decimal GrowerQuantityPerArea => area > 0 ? GrowerQuantity / area : 0;
		public decimal TotalYieldQuantityPerArea => area > 0 ? TotalYieldQuantity / area : 0;

		public void SetPricingViewStrategy(YieldPricingViewStrategy strategy) {
			PriceStrategy = strategy;

			RaisePropertyChanged(() => AverageSalePrice);
			RaisePropertyChanged(() => GrowerRevenue);
			RaisePropertyChanged(() => GrowerRevenuePerArea);
		}
	}
}