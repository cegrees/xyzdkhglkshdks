﻿using GalaSoft.MvvmLight;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Contract;
using Landdb.Domain.ReadModels.Yield;
using Landdb.Domain.ReadModels.YieldLocation;
using Landdb.Infrastructure;
using NLog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Threading;

namespace Landdb.ViewModel.Fields.FieldDetails {
	public class YieldSummaryViewModel : ViewModelBase {

		private readonly IClientEndpoint clientEndpoint;
		private readonly Dispatcher dispatcher;

		private readonly Logger log = LogManager.GetCurrentClassLogger();

		public YieldSummaryViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			LoadLineItems = new ObservableCollection<LoadLineItemViewModel>();
		}

		public ObservableCollection<LoadLineItemViewModel> LoadLineItems { get; }

		public void RefreshSummaryItems(AbstractTreeItemViewModel treeModel) {
			LoadLineItems.Clear();

			// resist the temptation to set this in the constructor.
			// at that point, no datasource or cropyear has been set yet.
			var currentCropYearId = new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear);
			var data = clientEndpoint.GetView<CropZoneLoadDataView>(currentCropYearId).GetValue(new CropZoneLoadDataView());
			var yieldLocations = clientEndpoint.GetView<YieldLocationListView>(new DataSourceId(currentCropYearId.DataSourceId)).GetValue(() => new YieldLocationListView()).YieldLocations;

			var locationsExcludedFromInventory = from loc in yieldLocations
												 where loc.IsActiveInCropYear(currentCropYearId.Id) && loc.IsQuantityExcludedFromInventory
												 select loc.Id.Id;

			IEnumerable<CropZoneLoadDataItem> filtered = null;

			if (treeModel is CropZoneTreeItemViewModel) {
				filtered = (from i in data.Items
							from loc in i.DestinationLocations
							where i.CropZoneId == (CropZoneId)((CropZoneTreeItemViewModel)treeModel).Id
								 && !locationsExcludedFromInventory.Contains(loc.Id.Id) && i.AreaWeightedFinalQuantity != 0
							orderby i.StartDateTime descending
							select i).Distinct(x => x.LoadId);
            } else if (treeModel is FieldTreeItemViewModel) {
                filtered = (from i in data.Items
                            from loc in i.DestinationLocations
                            where i.FieldId == (FieldId)((FieldTreeItemViewModel)treeModel).Id
                                 && !locationsExcludedFromInventory.Contains(loc.Id.Id)
                            orderby i.StartDateTime descending
                            select i).Distinct(x => x);
            }

            if (filtered != null && filtered.Any()) {
				decimal? defaultSharePercentage = null;
				var firstItem = filtered.First();

				// grab the default grower share percentage from the first associated rent contract we can find.
				// TODO: maybe warn if we find more than one?
				var contractedCropZones = clientEndpoint.GetView<CropZoneRentContractView>(currentCropYearId).GetValue(new CropZoneRentContractView());
				if (contractedCropZones.CropZoneRentContracts.ContainsKey(firstItem.CropZoneId.Id)) {
					defaultSharePercentage = contractedCropZones.CropZoneRentContracts[firstItem.CropZoneId.Id].GrowerCropShare;
				}

				foreach (var item in filtered) {
					decimal growerSharePercentage = 1;

					if (item.IsLoadSold && item.RentContractId != null) {
						if (item.IsShareSplitLoad) {
							var rentContractDetails = clientEndpoint.GetView<RentContractDetailsView>(item.RentContractId).GetValue(new RentContractDetailsView());
							growerSharePercentage = Convert.ToDecimal(rentContractDetails.IncludedShareInfo.GrowerCropShare);
						} else {
                            // load is 100% landowner shares. skip it.
                            growerSharePercentage = 0;
							//continue;
						}
					} else if (item.IsLoadStored && defaultSharePercentage.HasValue) {
						// load needs to be split according to the default percentage determined by 
						// the first rent contract associated with this cropzone.
						growerSharePercentage = defaultSharePercentage.Value;
					} else if (item.IsShareSplitLoad && item.RentContractId == null) {
						// bad condition. log it.
						log.Warn("Load id {0} dated {1} named \"{2}\" detected with split pricing but no rent contract id", item.LoadId, item.StartDateTime, item.LoadNumber);
					} else {
						// load is 100% grower shares. nothing to do.
					}

                    if (growerSharePercentage != 0)
                    {
                        LoadLineItems.Add(new LoadLineItemViewModel(item, growerSharePercentage));
                    }
				}
			}
		}
	}
}