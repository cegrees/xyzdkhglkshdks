﻿using GalaSoft.MvvmLight;
using System;
using Landdb.Resources;

namespace Landdb.ViewModel.Fields.FieldDetails {
	public class VarietyItemViewModel : ViewModelBase {
		public VarietyItemViewModel(string name, DateTime plantingDate) {
			Name = name;
			PlantingDate = plantingDate;
		}

		public string Name { get; set; }
		public DateTime PlantingDate { get; set; }

		public string PlantingDateText {
			get { return string.Format(Strings.PlantedOnDate_Format_Text.ToLower(), PlantingDate.ToString("d")); }
		}
	}
}
