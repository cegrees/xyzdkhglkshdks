﻿using System;
using System.Globalization;
using System.Windows.Data;
using Landdb.Resources;

namespace Landdb.ViewModel.Fields.FieldDetails {
	public class PricingViewStrategyConverter : IValueConverter {
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
			if (!(value is PricingViewStrategy)) { return null; }
			var e = (PricingViewStrategy)value;

			switch (e) {
				case PricingViewStrategy.AverageCost:
					return Strings.ByAverageCost_Text;
				case PricingViewStrategy.InvoiceMatching:
					return Strings.ByAssociatedInvoices_Text;
				case PricingViewStrategy.SpecificCost:
					return Strings.BySpecificCost_Text;
				default:
					return "";
			}
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
			throw new NotImplementedException();
		}
	}

	public class IsInvoiceColorConverter : IValueConverter {
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
			if (!(value is bool)) { return new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(0xff, 0x1a, 0x1a, 0x1a)); }

			if ((bool)value) {
				return new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(0xff, 0x00, 0x6f, 0xca));
			} else {
				return new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(0xff, 0x1a, 0x1a, 0x1a));
			}
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
			throw new NotImplementedException();
		}
	}

	public class YieldPricingViewStrategyConverter : IValueConverter {
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
			if (!(value is YieldPricingViewStrategy)) { return null; }
			var e = (YieldPricingViewStrategy)value;

			switch (e) {
				case YieldPricingViewStrategy.AverageSalePrice:
					return Strings.ByAverageSalePrice_Text;
				case YieldPricingViewStrategy.FieldDirectToPointOfSale:
					return Strings.ByFieldToPosPrice_Text;
				default:
					return "";
			}
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
			throw new NotImplementedException();
		}
	}
}
