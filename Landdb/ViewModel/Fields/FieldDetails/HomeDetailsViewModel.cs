﻿using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Legal;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Windows.Input;

namespace Landdb.ViewModel.Fields.FieldDetails
{
    public class HomeDetailsViewModel {
        public EulaDescriptionViewModel EulaViewModel { get; }
        private IClientEndpoint clientEndpoint;
        public HomeDetailsViewModel(IClientEndpoint clientEndpoint) {
            ShowPrereleasePricingDocumentationCommand = new RelayCommand(PricingDocumentation);
            ShowAccountingIntegrationDocumentationCommand = new RelayCommand(AccountingDocumentation);
            ShowReleaseNotesCommand = new RelayCommand(ReleaseNotes);
            ShowEULACommand = new RelayCommand(EULA);
            AcceptCommand = new RelayCommand(HideEula);
            ShowNewsItemCommand = new RelayCommand(ShowNewsItem);
            FarmShotsCommand = new RelayCommand(FarmShots);
            FarmShotsLinkCommand = new RelayCommand(FarmShotsLink);
            EulaViewModel = new EulaDescriptionViewModel();
            this.clientEndpoint = clientEndpoint;
        }

        public ICommand ShowPrereleasePricingDocumentationCommand { get; private set; }
        public ICommand ShowAccountingIntegrationDocumentationCommand { get; private set; }
        public ICommand ShowReleaseNotesCommand { get; private set; }
        public ICommand ShowNewsItemCommand { get; private set; }
        public ICommand ShowEULACommand { get; private set; }
        public ICommand AcceptCommand { get; private set; }
        public ICommand FarmShotsCommand { get; set;  }
        public ICommand FarmShotsLinkCommand { get; set; }
        void PricingDocumentation()
        {
            var vm = new Landdb.ViewModel.Prerelease.PricingChangeDescriptionViewModel();
            ScreenDescriptor descriptor = new ScreenDescriptor("Landdb.Views.Prerelease.PricingChangeDescription", "prerelease pricing", vm);
            Messenger.Default.Send<ChangeScreenDescriptorMessage>(new ChangeScreenDescriptorMessage() { ScreenDescriptor = descriptor });
        }

        void AccountingDocumentation() {
            var vm = new Landdb.ViewModel.Prerelease.AccountingIntegrationDescriptionViewModel();
            ScreenDescriptor descriptor = new ScreenDescriptor("Landdb.Views.Prerelease.AccountingIntegrationDescriptionView", "accounting integration", vm);
            Messenger.Default.Send<ChangeScreenDescriptorMessage>(new ChangeScreenDescriptorMessage() { ScreenDescriptor = descriptor });
        }

        void ReleaseNotes() {
            System.Diagnostics.Process.Start(@"https://landdb.blob.core.windows.net/public-dist/Land.db%20Release%20Notes%20-%20Latest.docx");
        }

        private void EULA()
        {
            Messenger.Default.Send(new ShowPopupMessage{
                ScreenDescriptor = new ScreenDescriptor(typeof(Views.Prerelease.EulaDescriptionView), "eula", this)
            });
        }

        public string LanddbWebIntegrationUri { get; set; }
        public string IntegrationName { get; set; }

        private async void FarmShots()
        {
            IntegrationName = "FarmShots";
            LanddbWebIntegrationUri = string.Format("{0}/", RemoteConfigurationSettings.GetBaseUri());
            //call to see if user is properly set up on integration party
            //api / integrationparty / GetByUser
            var integrationPartiesUri = string.Format("{0}api/integrationparty/GetByUser", RemoteConfigurationSettings.GetBaseUri());
            var client = new HttpClient
            {
                BaseAddress = new Uri(integrationPartiesUri),
            };

            client.DefaultRequestHeaders.Add("Accept", "application / json");
            GetData<bool>(client);

            //call farmshots integration...
        }

        private void FarmShotsLink()
        {
            var link = "https://app.farmshots.com/#/login";
            System.Diagnostics.Process.Start(@link);
        }

        private async void GetData<T>(HttpClient client)
        {
            try
            {
                var cloudClientFactory = new Landdb.Infrastructure.CloudClientFactory();
                var token = cloudClientFactory.ResolveStorageCredentials();
                await token.SetBearerTokenAsync(client);

                var response = await client.GetAsync("");

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var json = await response.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<List<IntegrationItem>>(json);
                    var hasFarmShots = result.Where(x => x.Name == "FarmShots");

                    if (hasFarmShots.Count() > 0)
                    {
                        //get user id
                        var userId = clientEndpoint.UserId;
                        var dataSourceId = Landdb.Infrastructure.ApplicationEnvironment.CurrentDataSourceId;
                        var currentYear = Landdb.Infrastructure.ApplicationEnvironment.CurrentCropYear;

                        var link = string.Format("https://enrollment.farmshots.com/#/enrollment?user={0}&data={1}&crop_year={2}", userId, dataSourceId, currentYear);
                        System.Diagnostics.Process.Start(@link);
                    }
                    else
                    {
                        //popup
                        Messenger.Default.Send(new ShowPopupMessage
                        {
                            ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Production.Popups.IntegrationsPageInfo", "integrations", this)
                        });

                    }
                }
                else
                {
                    //open popup with link to go sign up for the integration
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Scouting Service Info Retrieval Failed.");
            }
        }

        void HideEula()
        {
            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
        }

        void ShowNewsItem() {
            System.Diagnostics.Process.Start(@"http://landdb.blob.core.windows.net/public-dist/RecentNews.zip");
        }
    }
}

public class IntegrationItem
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public Guid ApiKey { get; set; }
    public DateTime CreatedOn { get; set; }
    public DateTime ModifiedOn { get; set; }
}