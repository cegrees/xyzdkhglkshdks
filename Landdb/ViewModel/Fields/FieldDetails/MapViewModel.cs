﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ThinkGeo.MapSuite.WpfDesktopEdition;
using ThinkGeo.MapSuite.Core;
using GalaSoft.MvvmLight;
using Landdb.Infrastructure;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Map;
using NLog;
using System.Collections.ObjectModel;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure.Messages;
using Landdb.Domain.ReadModels.Tree;
using Landdb.MapStyles;
using Landdb.ViewModel.Fields.FieldDetails.MapModels;
using ReflectionMagic;
using Landdb.ViewModel.Secondary.Map;
using Landdb.ViewModel.Maps.ImageryOverlay;
using System.Windows;
using Landdb.ViewModel.Secondary.Options;
using Landdb.Client.Account;
using System.Windows.Threading;
using Landdb.Client.Localization;
using Landdb.Resources;

namespace Landdb.ViewModel.Fields.FieldDetails {
    public class MapViewModel : ViewModelBase
    {
        private readonly IClientEndpoint clientEndpoint;
        private readonly Logger log = LogManager.GetCurrentClassLogger();

        private string name;
        private object detailsViewModel;

        private Visibility mapDisplayOptionsVisibility = Visibility.Collapsed;
        private MapOptionsViewModel mapOptionsViewModel;
        private readonly IAccountStatus accountStatus;
        private UserSettings userSettings;
        private readonly MapSettings mapsettings;
        private bool showDeleteCommand = false;
        private bool showDrawCommands = false;
        private bool showEditCommands = false;
        private bool canFindLatLon = false;
        private bool showOptionCommands = false;
        private bool showImportCommands = false;
        private bool showApexImportCommands = false;
        private bool showGoogleRadarCommands = false;
        private bool showCLUImportCommands = false;
        private bool showDeleteInstructionCommands = false;
        private bool hasUnsavedChanges = false;
        private bool isProcessingCommands = false;
        private bool isApexProcessing = false;
        private bool ismeasureline = false;
        private bool isMapToolsPopupOpen = false;
        private bool isSearchMapPopupOpen = false;
        private bool isOptionsMapPopupOpen = false;
        private bool isImportToolsPopupOpen = false;
        private bool isApexImportToolsPopupOpen = false;
        private bool isImportCLUToolsPopupOpen = false;
        private bool isDeleteInstructionToolsPopupOpen = false;
        private string bingMapsImageryType;
        private string displaycoordinates = string.Empty;
        private string pointLocation = string.Empty;

        private readonly MapDisplayModel displayModel;
        private readonly DrawToolModel drawTool;
        private readonly ImportToolModel importTool;
        private readonly ConstructPivotViewModel constructpivotvm;
        private readonly AnnotationsViewModel annotationsvm;
        private readonly CustomLabelViewModel customlabelvm;
        private readonly ScalePolygonViewModel scalepolygonvm;
        private readonly SSurgoNrcsWfsViewModel ssurgonrcswfsvm;
        private readonly NOAAViewModel noaavm;
        private PLSSViewModel plssvm;
        private readonly ShapeImportViewModel shapeimportvm;
        private CreateCropzoneViewModel createcropzonevm;
        private DrawEditDeleteViewModel draweditdeletevm;
        private SimpleDrawViewModel simpledrawvm;
        private Dispatcher dispatcher;

        #region ViewModel Construct

        public MapViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher)
        {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            this.accountStatus = ServiceLocator.Get<IAccountStatus>(); // TODO: Bad factoring here. Redo.
            var settingsId = new UserSettingsId(accountStatus.UserId);
            this.userSettings = clientEndpoint.GetUserSettings();
            this.mapsettings = clientEndpoint.GetMapSettings();
            mapOptionsViewModel = new MapOptionsViewModel(clientEndpoint, mapsettings, settingsId, ApplicationEnvironment.ShowPremiumMap);

            DataSourceId = ApplicationEnvironment.CurrentDataSourceId;
            CropYear = ApplicationEnvironment.CurrentCropYear;
            bingMapsImageryType = "Cached";
            mapDisplayOptionsVisibility = Visibility.Collapsed;

            this.displayModel = new MapDisplayModel(this.clientEndpoint, dispatcher, this);
            this.drawTool = new DrawToolModel(displayModel.Map, this, clientEndpoint);
            this.importTool = new ImportToolModel(displayModel.Map, this, clientEndpoint, System.Windows.Application.Current.Dispatcher);
            this.constructpivotvm = new ConstructPivotViewModel(this.clientEndpoint);
            this.annotationsvm = new AnnotationsViewModel(this.clientEndpoint, this, System.Windows.Application.Current.Dispatcher);
            this.customlabelvm = new CustomLabelViewModel();
            this.scalepolygonvm = new ScalePolygonViewModel(this.clientEndpoint);
            this.ssurgonrcswfsvm = new SSurgoNrcsWfsViewModel(this.clientEndpoint);
            this.noaavm = new NOAAViewModel();
            this.plssvm = new PLSSViewModel(this.clientEndpoint,"PLSS");
            this.shapeimportvm = new ShapeImportViewModel(this.clientEndpoint, System.Windows.Application.Current.Dispatcher);
            this.createcropzonevm = new CreateCropzoneViewModel(this.clientEndpoint, System.Windows.Application.Current.Dispatcher);

            showOptionCommands = true;
            ShowGoogleRadarCommands = false;

            Messenger.Default.Register<DataSourceChangedMessage>(this, OnDataSourceChanged);
            Messenger.Default.Register<ShapeImportMessage>(this, OnShapeImportReturn);
            Messenger.Default.Register<PivotShapeMessage>(this, OnPivotShapeReturn);
            Messenger.Default.Register<ScalePolygonMessage>(this, OnShapeScaleReturn);
            Messenger.Default.Register<CustomLabelMessage>(this, OnCustomLabelReturn);
            Messenger.Default.Register<DrawEditDeleteMessage>(this, OnDrawEditDeleteReturn);

            AdvancedDrawEditCommand = new RelayCommand(AdvancedDrawEdit);
            SimpleDrawCommand = new RelayCommand(SimpleDrawEdit);
            DrawCommand = new RelayCommand(Draw);
            EditCommand = new RelayCommand(Edit);
            ControlCommand = new RelayCommand(ControlEdit);
            SubtractShapeCommand = new RelayCommand(SubtractShape);
            ResetCommand = new RelayCommand(ClearShape);
            ImportCommand = new RelayCommand(Import);
            MassImportCommand = new RelayCommand(MassImport);
            CancelMassImportCommand = new RelayCommand(CancelMassImport);
            FinishMassImportCommand = new RelayCommand(FinishMassImport);
            
            //CLUImportCommand = new RelayCommand(PLSSImport);
            CLUImportCommand = new RelayCommand(CLUImport);
            CancelCLUImportCommand = new RelayCommand(CancelCLUImport);
            FinishCLUImportCommand = new RelayCommand(FinishCLUImport);
            ApexImportCommand = new RelayCommand(ApexImport);
            ProcessApexImportCommand = new RelayCommand(AssignAShapeApexImport);
            CancelApexImportCommand = new RelayCommand(CancelApexImport);
            FinishApexImportCommand = new RelayCommand(FinishApexImport);
            ImportFSACommand = new RelayCommand(ImportFSA);
            ExportPolygonCommand = new RelayCommand(ExportPolygon);
            ExportPointCommand = new RelayCommand(ExportPoint);
            CropzoneBoundaryCommand = new RelayCommand(CropzoneBoundaryCmd);
            BorderRowCommand = new RelayCommand(BorderRowCmd);
            CustomLabelCommand = new RelayCommand(CustomLabel);
            CustomCropZoneLabelCommand = new RelayCommand(CustomCropZoneLabel);
            ScalePolygonCommand = new RelayCommand(ScalePolygon);
            SSurgoCommand = new RelayCommand(SSurgo);
            NOAACommand = new RelayCommand(NOAA);
            PLSSCommand = new RelayCommand(PLSS);
            PLSSfastCommand = new RelayCommand(PLSSfast);
            TOPOfastCommand = new RelayCommand(TOPOfast);
            HydrologyfastCommand = new RelayCommand(Hydrologyfast);
            PrintMapViewCommand = new RelayCommand(PrintMapViewTool);
            SaveMapImage = new RelayCommand(SaveMapImageViewTool);
            DeleteShapeCommand = new RelayCommand(DeleteShape);
            DrawPivotCommand = new RelayCommand(DrawPivot);
            DrawDryCornersCommand = new RelayCommand(DrawDryCorners);
            SaveCommand = new RelayCommand(Save);
            CancelCommand = new RelayCommand(Cancel);
            MeasureCommand = new RelayCommand(Measure);
            RefreshMapCommand = new RelayCommand(RefreshDisplayMap);
            SetMapTypeCommand = new RelayCommand(SetMapType);
            ToggleMapAnnotationsCommand = new RelayCommand(SetMapAnnotation);
            MapAnnotationsCommand = new RelayCommand(DrawMapAnnotations);
            MAPOptionsVisibilityCommand = new RelayCommand(ToggleOptionsVisibility);
            ZoomToFieldsExtentCommand = new RelayCommand(ZoomToFieldsExtent);
            ZoomToLatLonCommand = new RelayCommand(ZoomToLatLonMethod);
            DisplayFindLatLonCommand = new RelayCommand(DisplayFindLatLonMethod);
            ZoomToSelectedExtentCommand = new RelayCommand(ZoomToSelectedExtent);
            GoogleRadarCommand = new RelayCommand(SnapToGoogleImagery);
            WestCommand = new RelayCommand(West);
            NorthCommand = new RelayCommand(North);
            EastCommand = new RelayCommand(East);
            SouthCommand = new RelayCommand(South);
        }

        public ICommand WestCommand { get; private set; }
        public ICommand NorthCommand { get; private set; }
        public ICommand EastCommand { get; private set; }
        public ICommand SouthCommand { get; private set; }
        public ICommand AdvancedDrawEditCommand { get; private set; }
        public ICommand SimpleDrawCommand { get; private set; }
        public ICommand DrawCommand { get; private set; }
        public ICommand EditCommand { get; private set; }
        public ICommand ControlCommand { get; private set; }
        public ICommand SubtractShapeCommand { get; private set; }
        public ICommand ResetCommand { get; private set; }
        public ICommand ImportCommand { get; private set; }
        public ICommand MassImportCommand { get; private set; }
        public ICommand CancelMassImportCommand { get; private set; }
        public ICommand FinishMassImportCommand { get; private set; }
        public ICommand CLUImportCommand { get; private set; }
        public ICommand CancelCLUImportCommand { get; private set; }
        public ICommand FinishCLUImportCommand { get; private set; }
        public ICommand ApexImportCommand { get; private set; }
        public ICommand ProcessApexImportCommand { get; private set; }
        public ICommand CancelApexImportCommand { get; private set; }
        public ICommand FinishApexImportCommand { get; private set; }
        public ICommand ImportFSACommand { get; private set; }
        public ICommand ExportPolygonCommand { get; private set; }
        public ICommand ExportPointCommand { get; private set; }
        public ICommand CropzoneBoundaryCommand { get; private set; }
        public ICommand BorderRowCommand { get; private set; }
        public ICommand DeleteShapeCommand { get; private set; }
        public ICommand DrawPivotCommand { get; private set; }
        public ICommand DrawDryCornersCommand { get; private set; }
        public ICommand CustomLabelCommand { get; private set; }
        public ICommand CustomCropZoneLabelCommand { get; private set; }
        public ICommand SaveCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }
        public ICommand MeasureCommand { get; private set; }
        public ICommand ScalePolygonCommand { get; private set; }
        public ICommand SSurgoCommand { get; private set; }
        public ICommand NOAACommand { get; private set; }
        public ICommand PLSSCommand { get; private set; }
        public ICommand PLSSfastCommand { get; private set; }
        public ICommand TOPOfastCommand { get; private set; }
        public ICommand HydrologyfastCommand { get; private set; }
        public ICommand PrintMapViewCommand { get; private set; }
        public ICommand SaveMapImage { get; private set; }
        public ICommand RefreshMapCommand { get; private set; }
        public ICommand SetMapTypeCommand { get; private set; }
        public ICommand ToggleMapAnnotationsCommand { get; private set; }
        public ICommand MapAnnotationsCommand { get; private set; }
        public ICommand MAPOptionsVisibilityCommand { get; private set; }
        public ICommand ZoomToFieldsExtentCommand { get; private set; }
        public ICommand ZoomToLatLonCommand { get; private set; }
        public ICommand DisplayFindLatLonCommand { get; private set; }
        public ICommand ZoomToSelectedExtentCommand { get; private set; }
        public ICommand GoogleRadarCommand { get; private set; }
        public int CropYear { get; set; }
        public Guid DataSourceId { get; set; }

        #endregion ViewModel Construct

        #region Public Properties

        public WpfMap Map { get { return displayModel.Map; } }
        public bool IsField { get; set; }
        
        // TODO: Probably encapsulate this in a custom layer type for ThinkGeo
        public IList<MapItem> ClientLayer
        {
            get { return displayModel.ClientLayer; }
            set
            {
                displayModel.ClientLayer = value;
                mapDisplayOptionsVisibility = Visibility.Collapsed; 
                //annotationsvm.ClientLayer = value;
                RaisePropertyChanged("ClientLayer");
            }
        }

        public object DetailsViewModel
        {
            get { return detailsViewModel; }
            set
            {
                detailsViewModel = value;
                try
                {
                    if (detailsViewModel is HomeDetailsViewModel)
                    {
                        this.Name = " ";
                    }
                    else if (detailsViewModel is WaitingDetailsViewModel)
                    {
                        this.Name = Strings.UnknownItem_Text;
                    }
                    else if (detailsViewModel is CropDetailsViewModel)
                    {
                        this.Name = Strings.UnknownItem_Text;
                    }
                    else if (detailsViewModel is FarmDetailsViewModel) {
                        this.Name = detailsViewModel.AsDynamic().Name;
                        try {
                            if (this.mapsettings.DisplayAreaInFieldLabel.HasValue && this.mapsettings.DisplayAreaInFieldLabel.Value) {
                                string areaname = detailsViewModel.AsDynamic().Name;
                                string displayarea = detailsViewModel.AsDynamic().TotalArea.AbbreviatedDisplay;
                                this.Name = string.Format("{0} {1}", areaname, displayarea);
                            }
                        }
                        catch { }
                    }
                    else
                    {
                       if (this.mapsettings.DisplayAreaInFieldLabel.HasValue && this.mapsettings.DisplayAreaInFieldLabel.Value) {
                           string areaname = detailsViewModel.AsDynamic().Name;
                           Landdb.ViewModel.Fields.FieldDetails.PropertyParts.AreaInformation areaInfo = detailsViewModel.AsDynamic().AreaInfo;
                           if (areaInfo.DisplayArea.Value != 0) {
                               this.Name = string.Format("{0} {1}", areaname, areaInfo.DisplayArea);
                           }
                           else {
                               this.Name = detailsViewModel.AsDynamic().Name;
                           }
                       }
                       else {
                           this.Name = detailsViewModel.AsDynamic().Name;
                       }

                        //Hide CLU Importer IF it is a Crop Zone....
                        IsField = detailsViewModel is FieldDetailsViewModel ? true : false;
                        RaisePropertyChanged(() => IsField);
                    }
                }
                catch (ArgumentException aex)
                {
                    this.Name = Strings.UnknownItem_Text;
                }

                if (ApplicationEnvironment.CurrentDataSource != null) {
                    if (this.mapsettings.MapAreaUnit != ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit) {
                        var vm = new Landdb.ViewModel.Overlays.DataSourceMapUnitChangeConfirmationViewModel(clientEndpoint);
                        ScreenDescriptor sd = new ScreenDescriptor("Landdb.Views.Overlays.DatasourceMapUnitChangeConfirmationView", "mapunitChangeConfirm", vm);
                        Messenger.Default.Send<ShowOverlayMessage>(new ShowOverlayMessage() { ScreenDescriptor = sd });

                        //System.Windows.MessageBox.Show("The change in data source requires the synchronization of map settings and initializing satellite imagery.");
                        //this.mapsettings.MapAreaUnit = ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit;
                        //clientEndpoint.SaveMapSettings(this.mapsettings);
                        //this.displayModel.RefreshImageryLayer(null);
                        //ShowDrawCommands = false;
                        return;
                    }
                }

                SetButtonsAndMenus();
                displayModel.DetailsViewModel = value;
                mapDisplayOptionsVisibility = Visibility.Collapsed;

                RaisePropertyChanged("DetailsViewModel");
            }
        }

        private AbstractTreeItemViewModel selectedTreeItem;
        public AbstractTreeItemViewModel SelectedTreeItem {
            get { return selectedTreeItem; }
            set {
                selectedTreeItem = value;
            }
        }

        public void TurnOffCropzoneOutlines() {
            displayModel.TurnOffCropzoneOutlines();
        }

        private bool AnImportOrDeleteCommandShouldBeShown() {
            return ShowImportCommands || ShowApexImportCommands || ShowCLUImportCommands || ShowDeleteInstructionCommands;
        }

        private void SetCommandsBasedOnViewModelDetails() {
            if (HomeOrFarmDetailsViewModelIsSelected())
                SetCommandsForFarmOrHome();
            if (CropOrFieldDetailsViewModelIsSelected())
                SetCommandsForCropOrField();
            else
                SetCommandsForEverythingElse();
        }
        private bool HomeOrFarmDetailsViewModelIsSelected() {
            return detailsViewModel is HomeDetailsViewModel || detailsViewModel is FarmDetailsViewModel;
        }
        private bool CropOrFieldDetailsViewModelIsSelected() {
            return detailsViewModel is FieldDetailsViewModel || detailsViewModel is CropZoneDetailsViewModel;
        }

        private void SetCommandsForFarmOrHome() {
            if (AnImportOrDeleteCommandShouldBeShown()) {
                SetCommands_DeleteDrawEditOption_To(false);
            } else {
                ShowOptionCommands = true;
                ShowDeleteCommand = false;
            }
        }
        private void SetCommandsForCropOrField() {
            if (AnImportOrDeleteCommandShouldBeShown()) {
                SetCommands_DeleteDrawEditOption_To(false);
            } else {
                SetCommands_DrawEditOption_To(true);
                ShowDeleteCommand = false;
            }
        }
        private void SetCommandsForEverythingElse() {
            if (AnImportOrDeleteCommandShouldBeShown()) {
                SetCommands_DeleteDrawEditOption_To(false);
            } else {
                SetCommands_DeleteDrawEdit_To(false);
            }
        }
        private void SetCommands_DeleteDrawEdit_To(bool toSet) {
            ShowDeleteCommand = toSet;
            ShowDrawCommands = toSet;
            ShowEditCommands = toSet;
        }
        private void SetCommands_DeleteDrawEditOption_To(bool toSet) {
            ShowDeleteCommand = toSet;
            ShowDrawCommands = toSet;
            ShowEditCommands = toSet;
            ShowOptionCommands = toSet;
        }
        private void SetCommands_DrawEditOption_To(bool toSet) {
            ShowDrawCommands = toSet;
            ShowEditCommands = toSet;
            ShowOptionCommands = toSet;
        }

        private void SetToolsAndAndOptionPopupsTo(bool toSet) {
            IsMapToolsPopupOpen = false;
            IsOptionsMapPopupOpen = false;
        }

        private void SetGoogleRadarCommands() {
            if (displayModel.SelectedOverlay != null && displayModel.SelectedOverlayMapStatusIsGoogle()) {
                ShowGoogleRadarCommands = true;
            }
            else {
                ShowGoogleRadarCommands = false;
            }
        }

        public void SetButtonsAndMenus() {
            SetCommandsBasedOnViewModelDetails();
            SetToolsAndAndOptionPopupsTo(false);
            SetGoogleRadarCommands();
        }

        public bool ShowDrawCommands {
            get { return showDrawCommands; }
            set {
                showDrawCommands = value;
                RaisePropertyChanged("ShowDrawCommands");
            }
        }

        public bool ShowDeleteCommand
        {
            get { return showDeleteCommand; }
            set
            {
                showDeleteCommand = value;
                RaisePropertyChanged("ShowDeleteCommand");
            }
        }

        public bool ShowEditCommands
        {
            get { return showEditCommands; }
            set
            {
                showEditCommands = value;
                RaisePropertyChanged("ShowEditCommands");
            }
        }

        public bool CanFindLatLon
        {
            get { return canFindLatLon; }
            set
            {
                canFindLatLon = value;
                RaisePropertyChanged("CanFindLatLon");
            }
        }

        public bool ShowOptionCommands {
            get { return showOptionCommands; }
            set {
                showOptionCommands = value;
                RaisePropertyChanged("ShowOptionCommands");
            }
        }

        public bool ShowImportCommands {
            get { return showImportCommands; }
            set {
                showImportCommands = value;
                RaisePropertyChanged("ShowImportCommands");
            }
        }

        public bool ShowCLUImportCommands
        {
            get { return showCLUImportCommands; }
            set
            {
                showCLUImportCommands = value;
                RaisePropertyChanged("ShowCLUImportCommands");
            }
        }

        public bool ShowDeleteInstructionCommands {
            get { return showDeleteInstructionCommands; }
            set {
                showDeleteInstructionCommands = value;
                RaisePropertyChanged("ShowDeleteInstructionCommands");
            }
        }
        public bool ShowApexImportCommands {
            get { return showApexImportCommands; }
            set {
                showApexImportCommands = value;
                RaisePropertyChanged("ShowApexImportCommands");
            }
        }

        public bool ShowGoogleRadarCommands {
            get { return showGoogleRadarCommands; }
            set {
                showGoogleRadarCommands = value;
                RaisePropertyChanged("ShowGoogleRadarCommands");
            }
        }

        public bool HasUnsavedChanges
        {
            get { return hasUnsavedChanges; }
            set
            {
                hasUnsavedChanges = value;
                RaisePropertyChanged("HasUnsavedChanges");
            }
        }

        public Visibility MAPDisplayOptionsVisibility {
            get { return mapDisplayOptionsVisibility; }
            set {
                mapDisplayOptionsVisibility = value;
                RaisePropertyChanged("MAPDisplayOptionsVisibility");
            }
        }
        public MapOptionsViewModel MAPOptionsViewModel {
            get { return mapOptionsViewModel; }
            set {
                mapOptionsViewModel = value;
                RaisePropertyChanged("MAPOptionsViewModel");
            }
        }


        public bool IsMeasureLine
        {
            get { return ismeasureline; }
            set
            {
                ismeasureline = value;
                this.displayModel.IsMeasureLine = ismeasureline;
                RaisePropertyChanged("IsMeasureLine");
            }
        }

        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                RaisePropertyChanged("Name");
            }
        }

        public bool IsMapToolsPopupOpen
        {
            get { return isMapToolsPopupOpen; }
            set
            {
                isMapToolsPopupOpen = value;
                SetProcessingIndicator();
                RaisePropertyChanged("IsMapToolsPopupOpen");
            }
        }

        public bool IsSearchMapPopupOpen
        {
            get { return isSearchMapPopupOpen; }
            set
            {
                isSearchMapPopupOpen = value;
                RaisePropertyChanged("IsSearchMapPopupOpen");
            }
        }

        public bool IsOptionsMapPopupOpen
        {
            get { return isOptionsMapPopupOpen; }
            set
            {
                isOptionsMapPopupOpen = value;
                SetProcessingIndicator();
                RaisePropertyChanged("IsOptionsMapPopupOpen");
            }
        }

        public bool IsImportToolsPopupOpen {
            get { return isImportToolsPopupOpen; }
            set {
                isImportToolsPopupOpen = value;
                RaisePropertyChanged("IsImportToolsPopupOpen");
            }
        }

        public bool IsApexImportToolsPopupOpen {
            get { return isApexImportToolsPopupOpen; }
            set {
                isApexImportToolsPopupOpen = value;
                RaisePropertyChanged("IsApexImportToolsPopupOpen");
            }
        }

        public bool IsImportCLUToolsPopupOpen {
            get { return isImportCLUToolsPopupOpen; }
            set {
                isImportCLUToolsPopupOpen = value;
                RaisePropertyChanged("IsImportCLUToolsPopupOpen");
            }
        }

        public bool IsApexProcessing {
            get { return isApexProcessing; }
            set {
                isApexProcessing = value;
                SetApexProcessingIndicator();
                RaisePropertyChanged("IsApexProcessing");
            }
        }

        public bool IsProcessingCommands {
            get { return isProcessingCommands; }
            set {
                isProcessingCommands = value;
                RaisePropertyChanged("IsProcessingCommands");
            }
        }

        private void SetProcessingIndicator() {
            if (isMapToolsPopupOpen && !isOptionsMapPopupOpen) {
                IsProcessingCommands = true;
            }
            else if (!isMapToolsPopupOpen && isOptionsMapPopupOpen) {
                IsProcessingCommands = true;
            }
            else {
                IsProcessingCommands = false;
            }
            RaisePropertyChanged("IsProcessingCommands");
        }

        private void SetApexProcessingIndicator() {
            if (IsApexImportToolsPopupOpen) {
                IsProcessingCommands = true;
            }
            else {
                IsProcessingCommands = false;
            }
            RaisePropertyChanged("IsProcessingCommands");
        }

        public string BingMapsImageryType
        {
            get { return bingMapsImageryType; }
            set
            {
                bingMapsImageryType = value;
                RaisePropertyChanged("BingMapsImageryType");
            }
        }

        public string DisplayCoordinates
        {
            get { return displaycoordinates; }
            set
            {
                displaycoordinates = value;
                RaisePropertyChanged("DisplayCoordinates");
            }
        }

        public string PointLocation
        {
            get { return pointLocation; }
            set
            {
                if (pointLocation == value) { return; }
                pointLocation = value;
                RaisePropertyChanged("PointLocation");
            }
        }

        #endregion Public Properties

        #region Shape Modification

        internal void DeleteShape(PointShape eshape)
        {
            displayModel.DeleteShapePointer = eshape;
            ShowDeleteInstructionCommands = false;
        }

        #endregion Shape Modification

        #region Events and Messaging

        private void CropzoneBoundary()
        {
            if (DetailsViewModel is FieldDetailsViewModel)
            {
                if (string.IsNullOrWhiteSpace(displayModel.SelectedField.GetWellKnownText()))
                {
                    System.Windows.MessageBox.Show(Strings.PleaseSelectFieldWIthBoundaryBeforeCropzoneCreator_Text);
                    return;
                }
                createcropzonevm = new CreateCropzoneViewModel(clientEndpoint, System.Windows.Application.Current.Dispatcher);
                createcropzonevm.FieldName = Name;
                createcropzonevm.SpatialQueryResults = displayModel.GetCropzoneResults();
                createcropzonevm.SelectedField = displayModel.SelectedField;
                createcropzonevm.PreviousMapExtent = displayModel.MapExtent;
                createcropzonevm.DisplayBorderRow = false;
                FieldDetailsViewModel fdvm = DetailsViewModel as FieldDetailsViewModel;
                FieldTreeItemViewModel ftivm = fdvm.FieldTreeItemVM;
                createcropzonevm.SetFieldTreeItemVM(ftivm);
                var boundarymessage = new ChangeScreenDescriptorMessage() { ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Secondary.Map.CreateCropzoneView", "cropzoneBoundary", createcropzonevm) };
                Messenger.Default.Send<ChangeScreenDescriptorMessage>(boundarymessage);
            }
            else
            {
                System.Windows.MessageBox.Show(Strings.PleaseSelectDesiredFieldBeforeCropzoneCreator_Text);
            }
        }

        private void BorderRowTool()
        {
            if (DetailsViewModel is FieldDetailsViewModel)
            {
                if (string.IsNullOrWhiteSpace(displayModel.SelectedField.GetWellKnownText()))
                {
                    System.Windows.MessageBox.Show(Strings.PleaseSelectFieldWIthBoundaryBeforeCropzoneCreator_Text);
                    return;
                }
                createcropzonevm = new CreateCropzoneViewModel(clientEndpoint, System.Windows.Application.Current.Dispatcher);
                createcropzonevm.FieldName = Name;
                createcropzonevm.SpatialQueryResults = displayModel.GetCropzoneResults();
                createcropzonevm.SelectedField = displayModel.SelectedField;
                createcropzonevm.PreviousMapExtent = displayModel.MapExtent;
                createcropzonevm.DisplayBorderRow = true;
                FieldDetailsViewModel fdvm = DetailsViewModel as FieldDetailsViewModel;
                FieldTreeItemViewModel ftivm = fdvm.FieldTreeItemVM;
                createcropzonevm.SetFieldTreeItemVM(ftivm);
                var boundarymessage = new ChangeScreenDescriptorMessage() { ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Secondary.Map.CreateCropzoneView", "cropzoneBoundary", createcropzonevm) };
                Messenger.Default.Send<ChangeScreenDescriptorMessage>(boundarymessage);
            }
            else
            {
                System.Windows.MessageBox.Show(Strings.PleaseSelectDesiredFieldBeforeCropzoneCreator_Text);
            }
        }

        private void DrawEditDelete() {
            draweditdeletevm = new DrawEditDeleteViewModel(clientEndpoint, System.Windows.Application.Current.Dispatcher);
            draweditdeletevm.SelectedIdentity = displayModel.SelectedIdentity;
            draweditdeletevm.SpatialQueryResults = displayModel.GetSpatialQueryResults();
            draweditdeletevm.SelectedField = displayModel.SelectedFeature;
            draweditdeletevm.CurrentMapExtent = displayModel.Map.CurrentExtent;
            var boundarymessage = new ChangeScreenDescriptorMessage() { ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Secondary.Map.DrawEditDeleteView", "drawBoundary", draweditdeletevm) };
            Messenger.Default.Send<ChangeScreenDescriptorMessage>(boundarymessage);
        }

        private void SimpleDraw() {
            simpledrawvm = new SimpleDrawViewModel(clientEndpoint, System.Windows.Application.Current.Dispatcher);
            simpledrawvm.SelectedField = displayModel.SelectedFeature;
            simpledrawvm.CurrentMapExtent = displayModel.Map.CurrentExtent;
            var boundarymessage = new ChangeScreenDescriptorMessage() { ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Secondary.Map.SimpleDrawView", "drawBoundary", simpledrawvm) };
            Messenger.Default.Send<ChangeScreenDescriptorMessage>(boundarymessage);
        }

        private void CustomLabelTool()
        {
            customlabelvm.ResetLabels();
            customlabelvm.LabelPositions = new Dictionary<string, PointShape>(displayModel.LabelPositions);
            customlabelvm.RotationAngles = new Dictionary<string, double>(displayModel.RotationAngles);
            customlabelvm.IsCropZone = false;
            //customlabelvm.ClientLayer = displayModel.ClientLayer;
            customlabelvm.ClientLayer = displayModel.SelectedFieldlist;
            customlabelvm.SelectedFeature = displayModel.SelectedField;

            var labelmessage = new ChangeScreenDescriptorMessage() { ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Secondary.Map.CustomLabelView", "customLabelVM", customlabelvm) };
            Messenger.Default.Send<ChangeScreenDescriptorMessage>(labelmessage);
        }

        private void CustomCropZoneLabelTool() {
            customlabelvm.ResetLabels();
            customlabelvm.WorldLocation = new PointShape();
            customlabelvm.IsCropZone = true;
            customlabelvm.LabelPositions = new Dictionary<string, PointShape>(displayModel.LabelPositions);
            customlabelvm.RotationAngles = new Dictionary<string, double>(displayModel.RotationAngles);
            customlabelvm.SelectedField = null;
            customlabelvm.ClientLayer = displayModel.FieldCropzonelist;
            customlabelvm.SelectedFeature = displayModel.SelectedFeature;
            var labelmessage = new ChangeScreenDescriptorMessage() { ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Secondary.Map.CustomLabelView", "customLabelVM", customlabelvm) };
            Messenger.Default.Send<ChangeScreenDescriptorMessage>(labelmessage);
        }

        private void ScalePolygonTool()
        {
            scalepolygonvm.FieldName = Name;
            scalepolygonvm.SelectedField = displayModel.SelectedFeature;
            scalepolygonvm.PreviousMapExtent = displayModel.MapExtent;
            var scalemessage = new ChangeScreenDescriptorMessage() { ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Secondary.Map.ScalePolygonView", "scalePolygonVM", scalepolygonvm) };
            Messenger.Default.Send<ChangeScreenDescriptorMessage>(scalemessage);
        }

        private void SSurgoNrcsWfsTool() {
            this.ssurgonrcswfsvm.IsPopupScreen = false;
            if (displayModel.SelectedFeatures.Count > 0)
            {
                ssurgonrcswfsvm.FieldName = Name;
                ssurgonrcswfsvm.SelectedField = displayModel.SelectedFeature;
                ssurgonrcswfsvm.SelectedFeatures = displayModel.SelectedFeatures;
                ssurgonrcswfsvm.PreviousMapExtent = displayModel.MapExtent;
                var scalemessage = new ChangeScreenDescriptorMessage() { ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Secondary.Map.SSurgoNrcsWfsView", "SSurgoNrcsWfsVM", ssurgonrcswfsvm) };
                Messenger.Default.Send<ChangeScreenDescriptorMessage>(scalemessage);
            }
            else
            {
                System.Windows.MessageBox.Show(Strings.MessageBox_MapViewModel_PleaseSelectTreeItems_Text);
            }
        }

        private void NOAATool() {
            noaavm.ClientLayer = displayModel.ClientLayer;
            noaavm.FieldName = Name;
            var scalemessage = new ChangeScreenDescriptorMessage() { ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Secondary.Map.NOAAView", "NOAAVM", noaavm) };
            Messenger.Default.Send<ChangeScreenDescriptorMessage>(scalemessage);
        }

        private void PLSSTool() {
            this.plssvm = new PLSSViewModel(this.clientEndpoint, "PLSSBing");
            plssvm.ClientLayer = displayModel.ClientLayer;
            plssvm.FieldName = Name;
            var scalemessage = new ChangeScreenDescriptorMessage() { ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Secondary.Map.PLSSView", "PLSSVM", plssvm) };
            Messenger.Default.Send<ChangeScreenDescriptorMessage>(scalemessage);
        }

        private void PLSSToolFast() {
            this.plssvm = new PLSSViewModel(this.clientEndpoint, "PLSS");
            plssvm.ClientLayer = displayModel.ClientLayer;
            plssvm.FieldName = Name;
            var scalemessage = new ChangeScreenDescriptorMessage() { ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Secondary.Map.PLSSView", "PLSSVM", plssvm) };
            Messenger.Default.Send<ChangeScreenDescriptorMessage>(scalemessage);
        }

        private void TOPOToolFast() {
            this.plssvm = new PLSSViewModel(this.clientEndpoint, "TOPO");
            plssvm.ClientLayer = displayModel.ClientLayer;
            plssvm.FieldName = Name;
            var scalemessage = new ChangeScreenDescriptorMessage() { ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Secondary.Map.PLSSView", "PLSSVM", plssvm) };
            Messenger.Default.Send<ChangeScreenDescriptorMessage>(scalemessage);
        }

        private void HydrologyToolFast() {
            this.plssvm = new PLSSViewModel(this.clientEndpoint, "Hydrology");
            plssvm.ClientLayer = displayModel.ClientLayer;
            plssvm.FieldName = Name;
            var scalemessage = new ChangeScreenDescriptorMessage() { ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Secondary.Map.PLSSView", "PLSSVM", plssvm) };
            Messenger.Default.Send<ChangeScreenDescriptorMessage>(scalemessage);
        }

        private void SaveMapImageViewTool()
        {
            IsOptionsMapPopupOpen = false;
            displayModel.SaveMapImage();
        }

        private void PrintMapViewTool() {
            IsOptionsMapPopupOpen = false;
            string name = string.Empty;
            string name1 = string.Empty;
            string name2 = string.Empty;
            string name3 = string.Empty;
            string name4 = string.Empty;
            string name5 = string.Empty;

            var years = clientEndpoint.GetView<FieldTreeView>(new DataSourceId(ApplicationEnvironment.CurrentDataSourceId));
            var yearItem = (years.HasValue && years.Value.CropYearList.ContainsKey(ApplicationEnvironment.CurrentCropYear)) ? years.Value.CropYearList[ApplicationEnvironment.CurrentCropYear] : new TreeViewYearItem();
            IList<TreeViewFarmItem> farms = yearItem.Farms != null ? yearItem.Farms : new List<TreeViewFarmItem>();

            if (DetailsViewModel is HomeDetailsViewModel) {
                name = ApplicationEnvironment.CurrentDataSourceName;
            }
            if (DetailsViewModel is FarmDetailsViewModel) {
                name = (DetailsViewModel as FarmDetailsViewModel).Name;
                name3 = (DetailsViewModel as FarmDetailsViewModel).Name;
                name4 = ApplicationEnvironment.CurrentDataSourceName;
                name = string.Format(@"{0}\{1}", name4, name3);
            }
            if (DetailsViewModel is FieldDetailsViewModel) {
                name = (DetailsViewModel as FieldDetailsViewModel).Name;
                name2 = (DetailsViewModel as FieldDetailsViewModel).Name;

                FieldId fldid = new FieldId(ApplicationEnvironment.CurrentDataSourceId, (DetailsViewModel as FieldDetailsViewModel).FieldView.Id.Id);

                var q = from f in yearItem.Farms
                        from fi in f.Fields
                        where fi.FieldId == fldid
                        select new { f.FarmId, fi.FieldId };

                foreach (var cropZ in q) {
                    IIdentity itemIdcz = null;
                    var farmMaybe = clientEndpoint.GetView<FarmDetailsView>(cropZ.FarmId);
                    if (farmMaybe.HasValue) {
                        name3 = (farmMaybe.Value as FarmDetailsView).Name;
                        name4 = ApplicationEnvironment.CurrentDataSourceName;
                        name = string.Format(@"{0}\{1}\{2}", name4, name3, name2);
                        break;
                    }
                }
            }
            if (DetailsViewModel is CropZoneDetailsViewModel) {
                name = (DetailsViewModel as CropZoneDetailsViewModel).Name;
                name1 = (DetailsViewModel as CropZoneDetailsViewModel).Name;
                var czMaybe = clientEndpoint.GetView<CropZoneDetailsView>((DetailsViewModel as CropZoneDetailsViewModel).CropZoneView.Id);
                var fieldMaybe = clientEndpoint.GetView<FieldDetailsView>(new FieldId(ApplicationEnvironment.CurrentDataSourceId, czMaybe.Value.FieldId.Id));

                if (fieldMaybe.HasValue) {
                    name2 = fieldMaybe.Value.FieldNameByCropYear[ApplicationEnvironment.CurrentCropYear];// .Name;
                    FieldId fldid = new FieldId(ApplicationEnvironment.CurrentDataSourceId, czMaybe.Value.FieldId.Id);

                    var q = from f in yearItem.Farms
                            from fi in f.Fields
                            where fi.FieldId == fldid
                            select new { f.FarmId, fi.FieldId };

                    foreach (var cropZ in q) {
                        IIdentity itemIdcz = null;
                        FarmId frmid = new FarmId(ApplicationEnvironment.CurrentDataSourceId, cropZ.FarmId.Id);
                        var farmMaybe = clientEndpoint.GetView<FarmDetailsView>(frmid);
                        if (farmMaybe.HasValue) {
                            name3 = (farmMaybe.Value as FarmDetailsView).Name;
                            name4 =  ApplicationEnvironment.CurrentDataSourceName;
                            name = string.Format(@"{0}\{1}\{2}\{3}", name4, name3, name2, name1);
                            break;
                        }
                    }
                }
            }

            displayModel.PrintMapView(name);
        }

        private void ConstructPivot()
        {
            IsMeasureLine = false;
            //constructpivotvm = new ConstructPivotViewModel(clientEndpoint);
            constructpivotvm.DrawingPivot = true;
            constructpivotvm.SelectedOverlay = displayModel.SelectedOverlay;
            constructpivotvm.SelectedField = displayModel.SelectedField;
            constructpivotvm.CurrentMapExtent = displayModel.MapExtent;
            var importmessage = new ChangeScreenDescriptorMessage() { ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Secondary.Map.ConstructPivotView", "constructPivotVM", constructpivotvm) };
            Messenger.Default.Send<ChangeScreenDescriptorMessage>(importmessage);
        }

        private void ConstructDryCorners()
        {
            IsMeasureLine = false;
            //constructpivotvm = new ConstructPivotViewModel(clientEndpoint);
            constructpivotvm.DrawingPivot = false;
            constructpivotvm.SelectedOverlay = displayModel.SelectedOverlay;
            constructpivotvm.SelectedField = displayModel.SelectedField;
            constructpivotvm.CurrentMapExtent = displayModel.MapExtent;
            var importmessage = new ChangeScreenDescriptorMessage() { ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Secondary.Map.ConstructPivotView", "constructPivotVM", constructpivotvm) };
            Messenger.Default.Send<ChangeScreenDescriptorMessage>(importmessage);
        }

        private void Annotations() {
            annotationsvm.FieldTreeShapes = displayModel.ClientLayer;
            annotationsvm.SelectedField = displayModel.SelectedField;
            annotationsvm.CurrentMapExtent = displayModel.MapExtent;
            annotationsvm.LoadAnnotationLists();
            if (annotationsvm.MapLoaded) {
                annotationsvm.SetCanLoadAnnotations();
                annotationsvm.ReLoadAnnotationImageryLayer();
            }
            var importmessage = new ChangeScreenDescriptorMessage() { ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Secondary.Map.AnnotationsView", "annotationsVM", annotationsvm) };
            Messenger.Default.Send<ChangeScreenDescriptorMessage>(importmessage);
        }

        private void ShapeImport()
        {
            shapeimportvm.Projection = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetGoogleMapParametersString());
            shapeimportvm.ResetLayers();
            var importmessage = new ChangeScreenDescriptorMessage() { ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Secondary.Map.ShapeImportView", "importShapeVM", shapeimportvm) };
            Messenger.Default.Send<ChangeScreenDescriptorMessage>(importmessage);
        }

        private void MassShapeImport() {
            log.Info("DrawingTools - Open mass import.");
            ShowDrawCommands = false;
            ShowOptionCommands = false;
            ShowEditCommands = false;
            ShowDeleteCommand = false;
            ShowImportCommands = true;
            ShowApexImportCommands = false;
            importTool.HandleShapesMassImport();
        }

        private void CancelMassShapeImport() {
            log.Info("DrawingTools - Cancel mass import.");
            ShowDrawCommands = false;
            ShowOptionCommands = false;
            ShowEditCommands = false;
            ShowDeleteCommand = false;
            ShowImportCommands = false;
            ShowApexImportCommands = false;
            importTool.CloseAndCancelImport();
            SetButtonsAndMenus();
        }

        private void FinishMassShapeImport() {
            log.Info("DrawingTools - Finish mass import.");
            ShowDrawCommands = false;
            ShowOptionCommands = false;
            ShowEditCommands = false;
            ShowDeleteCommand = false;
            ShowImportCommands = false;
            ShowApexImportCommands = false;
            importTool.FinishMassImport();
            SetButtonsAndMenus();
        }

        private void CLUShapeImport() {
            log.Info("DrawingTools - Open clu import.");
            ShowDrawCommands = false;
            ShowOptionCommands = false;
            ShowEditCommands = false;
            ShowDeleteCommand = false;
            ShowImportCommands = false;
            ShowCLUImportCommands = true;
            ShowDeleteInstructionCommands = false;
            ShowApexImportCommands = false;
            importTool.HandleShapesCLUImport();
        }

        private void PLSSShapeImport()
        {
            log.Info("DrawingTools - Open clu import.");
            ShowDrawCommands = false;
            ShowOptionCommands = false;
            ShowEditCommands = false;
            ShowDeleteCommand = false;
            ShowImportCommands = false;
            ShowCLUImportCommands = true;
            ShowDeleteInstructionCommands = false;
            ShowApexImportCommands = false;
            importTool.HandleShapesPLSSImport();
        }
        private void CancelCLUShapeImport() {
            log.Info("DrawingTools - Cancel clu import.");
            ShowDrawCommands = false;
            ShowOptionCommands = false;
            ShowEditCommands = false;
            ShowDeleteCommand = false;
            ShowImportCommands = false;
            ShowCLUImportCommands = false;
            ShowDeleteInstructionCommands = false;
            ShowApexImportCommands = false;
            importTool.CloseAndCancelImport();
            SetButtonsAndMenus();
        }

        private void FinishCLUShapeImport() {
            log.Info("DrawingTools - Finish clu import.");
            ShowDrawCommands = false;
            ShowOptionCommands = false;
            ShowEditCommands = false;
            ShowDeleteCommand = false;
            ShowImportCommands = false;
            ShowCLUImportCommands = false;
            ShowDeleteInstructionCommands = false;
            ShowApexImportCommands = false;
            importTool.FinishMassImport();
            SetButtonsAndMenus();
        }

        private void ApexShapeImport() {
            log.Info("DrawingTools - Open apex import.");
            ShowDrawCommands = false;
            ShowOptionCommands = false;
            ShowEditCommands = false;
            ShowDeleteCommand = false;
            ShowImportCommands = false;
            ShowCLUImportCommands = false;
            ShowDeleteInstructionCommands = false;
            ShowApexImportCommands = true;
            importTool.HandleShapesApexImport();
            SetButtonsAndMenus();
        }

        private bool AssignApexShapes() {
            log.Info("DrawingTools - import apex shapes and tree.");
            if (selectedTreeItem is GrowerTreeItemViewModel) {
                ShowDrawCommands = false;
                ShowOptionCommands = false;
                ShowEditCommands = false;
                ShowDeleteCommand = false;
                ShowImportCommands = false;
                ShowCLUImportCommands = false;
                ShowDeleteInstructionCommands = false;
                ShowApexImportCommands = true;

                IList<FarmTreeItemViewModel> farmtreelist = new List<FarmTreeItemViewModel>();
                farmtreelist = importTool.AssignApexShapes(SelectedTreeItem);
                System.Threading.Thread.Sleep(5000);
                if (farmtreelist.Count == 0) {
                    System.Windows.MessageBox.Show(Strings.NoFarmsAndFieldsCompletedImportProcess_Text);
                    return false;
                }
                foreach (FarmTreeItemViewModel ftivm in farmtreelist) {
                    SelectedTreeItem.Children.Add(ftivm);
                }
                SelectedTreeItem.IsExpanded = true;
                return true;
            }
            else {
                System.Windows.MessageBox.Show(Strings.MustSelectHomeTreeItem_Text);
                return false;
            }
        }

        private void CancelApexShapeImport() {
            log.Info("DrawingTools - Cancel apex import.");
            ShowDrawCommands = false;
            ShowOptionCommands = false;
            ShowEditCommands = false;
            ShowDeleteCommand = false;
            ShowImportCommands = false;
            ShowCLUImportCommands = false;
            ShowDeleteInstructionCommands = false;
            ShowApexImportCommands = false;
            importTool.CloseAndCancelImport();
            SetButtonsAndMenus();
        }

        private void FinishApexShapeImport() {
            log.Info("DrawingTools - Finish apex import.");
            ShowDrawCommands = false;
            ShowOptionCommands = false;
            ShowEditCommands = false;
            ShowDeleteCommand = false;
            ShowImportCommands = false;
            ShowCLUImportCommands = false;
            ShowDeleteInstructionCommands = false;
            ShowApexImportCommands = false;
            importTool.FinishMassImport();
            SetButtonsAndMenus();
        }

        private void ShapeImportFSA()
        {
            shapeimportvm.Projection = new Proj4Projection(Proj4Projection.GetEpsgParametersString(3857), Proj4Projection.GetGoogleMapParametersString());
            shapeimportvm.ResetLayers();
            var importmessage = new ChangeScreenDescriptorMessage() { ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Secondary.Map.ShapeImportView", "importShapeVM", shapeimportvm) };
            Messenger.Default.Send<ChangeScreenDescriptorMessage>(importmessage);
        }

        private void HideImport()
        {
            var importmessage = new HideOverlayMessage() { };
            Messenger.Default.Send<HideOverlayMessage>(importmessage);
        }

        private void OnShapeImportReturn(ShapeImportMessage message)
        {
            displayModel.ImportShape = message.ShapeBase;
            displayModel.ImportShape = null;
            Messenger.Default.Send<ShowMainViewMessage>(new ShowMainViewMessage());
        }

        private void OnPivotShapeReturn(PivotShapeMessage message)
        {
            displayModel.DrawingPivot = message.DrawingPivot;
            displayModel.PivotShape = message.ShapeBase;
            displayModel.PivotShape = null;
            Messenger.Default.Send<ShowMainViewMessage>(new ShowMainViewMessage());
        }

        private void OnDrawEditDeleteReturn(DrawEditDeleteMessage message) {
            displayModel.ReplaceShape = message.ShapeBase;
            displayModel.SaveAdvancedShape();
            displayModel.ReplaceShape = null;
            Messenger.Default.Send<ShowMainViewMessage>(new ShowMainViewMessage());
        }

        private void OnShapeScaleReturn(ScalePolygonMessage message)
        {
            displayModel.ReplaceShape = message.ShapeBase;
            displayModel.SaveReplaceShape();
            displayModel.ReplaceShape = null;
            Messenger.Default.Send<ShowMainViewMessage>(new ShowMainViewMessage());
        }

        private void OnCustomLabelReturn(CustomLabelMessage message)
        {
            if (customlabelvm.CanSaveChanges) {
                displayModel.RotationAngles = message.RotationAngles;
                displayModel.LabelPositions = message.LabelPositions;
                displayModel.SaveCustomLabels(customlabelvm.ClientLayer, customlabelvm.IsCropZone);
                customlabelvm.CanSaveChanges = false;
                displayModel.RefreshFieldsLayer();
            }
            Messenger.Default.Send<ShowMainViewMessage>(new ShowMainViewMessage());
        }

        private void OnDataSourceChanged(DataSourceChangedMessage message)
        {
        }

        private void AdvancedDrawEdit() {
            IsMapToolsPopupOpen = false;
            DrawEditDelete();
        }

        private void SimpleDrawEdit() {
            IsMapToolsPopupOpen = false;
            SimpleDraw();
        }

        private void Draw() {
            log.Info("DrawingTools - Draw a new shape.");
            ShowOptionCommands = false;
            ShowEditCommands = false;
            ShowDeleteCommand = false;
            displayModel.SetCropzoneOutlines();

            drawTool.HandleFastDrawAndEditPolygon();
        }

        private void Edit()
        {
            log.Info("DrawingTools - Edit an existing shape.");
            IsMapToolsPopupOpen = false;
            ShowDrawCommands = false;
            ShowOptionCommands = false;
            ShowDeleteCommand = true;
            displayModel.EnsureCurrentLayerIsOpen();
            displayModel.SetCropzoneOutlines();
            drawTool.HandleShapeEdit();
        }

        private void ControlEdit() {
            log.Info("DrawingTools - Control an existing shape.");
            IsMapToolsPopupOpen = false;
            ShowDrawCommands = false;
            ShowOptionCommands = false;
            ShowDeleteCommand = true;
            displayModel.EnsureCurrentLayerIsOpen();
            drawTool.HandleShapeControl();
        }

        private void ClearShape() {
            log.Info("DrawingTools - Delete a shape.");
            IsMapToolsPopupOpen = false;
            ShowDrawCommands = false;
            ShowOptionCommands = false;
            displayModel.EnsureCurrentLayerIsOpen();
            System.Windows.MessageBoxResult result = System.Windows.MessageBox.Show(Strings.AreYouSureYouWantToDeleteShape_Text, Strings.ConfirmDeleteShape_Text, System.Windows.MessageBoxButton.OKCancel, System.Windows.MessageBoxImage.Information, System.Windows.MessageBoxResult.Cancel, (System.Windows.MessageBoxOptions)0);
            if (result == System.Windows.MessageBoxResult.OK || result == System.Windows.MessageBoxResult.Yes) {
                drawTool.HandleClearEditOverlay();
            }
        }

        private void SubtractShape() {
            log.Info("DrawingTools - Subtract a shape.");
            IsMapToolsPopupOpen = false;
            ShowDrawCommands = false;
            ShowOptionCommands = false;
            displayModel.EnsureCurrentLayerIsOpen();
            drawTool.HandleSubractShape();
        }

        private void Import() {
            log.Info("DrawingTools - Import a shape.");
            IsMapToolsPopupOpen = false;
            ShapeImport();
        }

        private void MassImport()
        {
            log.Info("MassImportTools - Import shapes.");
            IsMapToolsPopupOpen = false;
            IsImportToolsPopupOpen = true;
            MassShapeImport();
        }

        private void AssignAShapeMassImport() {
            log.Info("MassImportTools - Assign.");
            IsImportToolsPopupOpen = false;
            CancelMassShapeImport();
        }

        private void CancelMassImport() {
            log.Info("MassImportTools - Cancel.");
            IsImportToolsPopupOpen = false;
            CancelMassShapeImport();
        }

        private void FinishMassImport() {
            log.Info("MassImportTools - Finish.");
            IsImportToolsPopupOpen = false;
            FinishMassShapeImport();
        }

        public void SaveMassImport(BaseShape importpolygon) {
            if (detailsViewModel is FieldDetailsViewModel || detailsViewModel is CropZoneDetailsViewModel) {
                log.Info("MassImportTools - Save Shape.");
                displayModel.SaveImportShape(importpolygon, true);
            }
            else {
                System.Windows.MessageBox.Show(Strings.MustHaveTreeOrCropzoneSelectedInTree_Text);
            }
        }

        private void PLSSImport()
        {
            if (detailsViewModel is FieldDetailsViewModel)
            {
                log.Info("CLUImportTools - Import shapes.");
                IsMapToolsPopupOpen = false;
                IsImportCLUToolsPopupOpen = false;
                PLSSShapeImport();
            }
            else
            {
                System.Windows.MessageBox.Show(Strings.ThisToolOnlyAvailableForFieldTreeSelection_Text);
            }
        }

        private void CLUImport() {
            if (detailsViewModel is FieldDetailsViewModel) {
                log.Info("CLUImportTools - Import shapes.");
                IsMapToolsPopupOpen = false;
                IsImportCLUToolsPopupOpen = false;
                CLUShapeImport();
            }
            else {
                System.Windows.MessageBox.Show(Strings.ThisToolOnlyAvailableForFieldTreeSelection_Text);
            }
        }

        private void AssignAShapeCLUImport() {
            log.Info("CLUImportTools - Assign.");
            IsImportCLUToolsPopupOpen = false;
            CancelCLUShapeImport();
        }

        private void CancelCLUImport() {
            log.Info("CLUImportTools - Cancel.");
            IsImportCLUToolsPopupOpen = false;
            CancelCLUShapeImport();
        }

        private void FinishCLUImport() {
            log.Info("CLUImportTools - Finish.");
            IsImportCLUToolsPopupOpen = false;
            FinishCLUShapeImport();
        }

        private void ApexImport() {
            log.Info("ApexImportTools - Import Apex shapes.");
            IsOptionsMapPopupOpen = false;
            IsApexImportToolsPopupOpen = true;
            ApexShapeImport();
        }

        private void AssignAShapeApexImport() {
            IsApexProcessing = true;
            log.Info("ApexImportTools - Assign.");
            bool completedsuccessfully = AssignApexShapes();
            if (completedsuccessfully) {
                CancelApexImport();
            }
            IsApexProcessing = false;
        }

        private void CancelApexImport() {
            log.Info("ApexImportTools - Cancel.");
            IsApexImportToolsPopupOpen = false;
            CancelApexShapeImport();
        }

        private void FinishApexImport() {
            log.Info("ApexImportTools - Finish.");
            IsApexImportToolsPopupOpen = false;
            FinishApexShapeImport();
        }

        private void ImportFSA()
        {
            log.Info("DrawingTools - Import an FSA shape.");
            IsMapToolsPopupOpen = false;
            ShapeImportFSA();
        }

        private void ExportPolygon()
        {
            log.Info("DrawingTools - Export a polygon.");
            IsOptionsMapPopupOpen = false;
            displayModel.ExportShapeFile(ShapeFileType.Polygon, false);
        }

        private void ExportPoint()
        {
            log.Info("DrawingTools - Export a centerpoint.");
            IsOptionsMapPopupOpen = false;
            displayModel.ExportShapeFile(ShapeFileType.Point, false);
        }

        private void CropzoneBoundaryCmd()
        {
            log.Info("DrawingTools - Split cropzones.");
            IsMapToolsPopupOpen = false;
            CropzoneBoundary();
        }

        private void BorderRowCmd()
        {
            log.Info("DrawingTools - Draw Border Rows.");
            IsMapToolsPopupOpen = false;
            BorderRowTool();
        }

        private void CustomLabel()
        {
            log.Info("DrawingTools - Custom field labels.");
            IsMapToolsPopupOpen = false;
            CustomLabelTool();
        }

        private void CustomCropZoneLabel() {
            log.Info("DrawingTools - Custom cropzone labels.");
            IsMapToolsPopupOpen = false;
            CustomCropZoneLabelTool();
        }

        private void ScalePolygon()
        {
            log.Info("DrawingTools - Scale or weed a polygon.");
            IsMapToolsPopupOpen = false;
            ScalePolygonTool();
        }

        private void SSurgo() {
            log.Info("DrawingTools - View ssurgo data.");
            IsMapToolsPopupOpen = false;
            IsOptionsMapPopupOpen = false;
            SSurgoNrcsWfsTool();
        }

        private void NOAA() {
            log.Info("DrawingTools - View weather maps.");
            IsOptionsMapPopupOpen = false;
            NOAATool();
        }

        private void PLSS() {
            log.Info("DrawingTools - View PLSS maps.");
            IsOptionsMapPopupOpen = false;
            PLSSTool();
        }

        private void PLSSfast() {
            log.Info("DrawingTools - View PLSS maps.");
            IsOptionsMapPopupOpen = false;
            PLSSToolFast();
        }

        private void TOPOfast() {
            log.Info("DrawingTools - View TOPO maps.");
            IsOptionsMapPopupOpen = false;
            TOPOToolFast();
        }

        private void Hydrologyfast() {
            log.Info("DrawingTools - View Hydrology maps.");
            IsOptionsMapPopupOpen = false;
            HydrologyToolFast();
        }

        private void DeleteShape()
        {
            log.Info("DrawingTools - Handle deleting a shape .");
            IsMapToolsPopupOpen = false;
            ShowDeleteInstructionCommands = true;
            drawTool.HandleDeleteShape();
        }

        private void DrawPivot()
        {
            log.Info("DrawingTools - Draw a pivot.");
            IsMapToolsPopupOpen = false;
            ConstructPivot();
        }

        private void DrawDryCorners()
        {
            log.Info("DrawingTools - Draw dry corners.");
            IsMapToolsPopupOpen = false;
            displayModel.DiffShape();
        }

        private void West() {
            displayModel.MapPan(PanDirection.Left);
        }

        private void North() {
            displayModel.MapPan(PanDirection.Up);
        }

        private void East() {
            displayModel.MapPan(PanDirection.Right);
        }

        private void South() {
            displayModel.MapPan(PanDirection.Down);
        }

        private void Save()
        {
            log.Info("DrawingTools - Perform shape save.");
            if (IsMeasureLine)
            {
                displayModel.SaveMeasureLayer();
                IsMeasureLine = false;
                HasUnsavedChanges = false;
                return;
            }
            SetButtonsAndMenus();
            displayModel.TurnOffCropzoneOutlines();
            displayModel.SaveTrackingLayer();
            drawTool.ClearDrawingAndEditingPolygon();
    
            if (!string.IsNullOrWhiteSpace(this.displayModel.UpdatedDisplayArea)) {
                this.Name = this.displayModel.UpdatedDisplayArea;
            }
        }

        private void Cancel() {
            log.Info("DrawingTools - Cancel drawing and editing.");
            SetButtonsAndMenus();
            displayModel.TurnOffCropzoneOutlines();
            displayModel.CancelDrawingAndEditing();
            drawTool.ClearDrawingAndEditingPolygon();
        }

        private void Measure()
        {
            log.Info("DrawingTools - Draw a measure line.");
            IsMapToolsPopupOpen = false;
            IsOptionsMapPopupOpen = false;
            drawTool.HandleMeasureLine();
        }

        private void RefreshDisplayMap() {
            log.Info("DrawingTools - Manually refresh the map.");
            IsOptionsMapPopupOpen = false;
            IsMeasureLine = false;
            this.displayModel.RefreshImageryLayer(null);
        }

        private void SetMapType() {
            log.Info("DrawingTools - Toggle map imagery.");
            IsOptionsMapPopupOpen = false;
            IsMeasureLine = false;
            this.displayModel.RefreshImageryLayer(MAPOptionsViewModel.MapToggle(this.displayModel.SelectedOverlay.MapInfoSelectorItem, !this.displayModel.SelectedOverlay.OnlineOnly));
        }

        private void SetMapAnnotation() {
            log.Info("DrawingTools - Toggle annotations.");
            IsOptionsMapPopupOpen = false;
            IsMeasureLine = false;
            MapSettings mapsettings = this.clientEndpoint.GetMapSettings();
            mapsettings.MapAnnotationsVisible = !mapsettings.MapAnnotationsVisible;
            this.clientEndpoint.SaveMapSettings(mapsettings);
            this.displayModel.RefreshImageryLayer(null);
        }

        private void DrawMapAnnotations() {
            log.Info("DrawingTools - Draw annotations.");
            IsOptionsMapPopupOpen = false;
            IsMeasureLine = false;
            Annotations();
        }

        private void ZoomToFieldsExtent()
        {
            log.Info("DrawingTools - Zoom to fields extent.");
            IsOptionsMapPopupOpen = false;
            this.displayModel.ZoomToFieldsExtent();
        }

        private void ZoomToLatLonMethod()
        {
            log.Info("DrawingTools - Zoom to point extent.");
            CanFindLatLon = false;
            string newpointlocation = pointLocation.Replace(" ", "");
            char[] delimiterChars = { ',',' ' };
            string[] words = newpointlocation.Split(delimiterChars);
            try
            {
                if (words.Count() == 2 || words.Count() == 4) {
                    PointShape pointlatlon = null;
                    if (words.Count() == 2) 
                    {
                        pointlatlon = new PointShape(System.Convert.ToDouble(words[1]), System.Convert.ToDouble(words[0]));
                    }
                    else if (words.Count() == 4) 
                    {
                        pointlatlon = new PointShape(System.Convert.ToDouble($"{words[2]}.{words[3]}"), System.Convert.ToDouble($"{words[0]}.{words[1]}"));
                    }                   
                    this.displayModel.FindLatLonLocation(pointlatlon);
                }                   
            }
            catch
            {
                log.Debug($"Invalid point entered for {nameof(PointLocation)}. Must be in format \"integer.mantissa, integer.mantissa\" or \"integer,mantissa, integer,mantissa\".");
                return;
            }
        }

        private void DisplayFindLatLonMethod()
        {
            log.Info("DrawingTools - DisplayFindLatLonMethod.");
            CanFindLatLon = !CanFindLatLon;
        }

        private void ZoomToSelectedExtent() {
            log.Info("DrawingTools - Zoom to selected extent.");
            IsOptionsMapPopupOpen = false;
            this.displayModel.ZoomToSelectedExtent();
        }

        private void SnapToGoogleImagery() {
            log.Info("DrawingTools - Snap to Google imagery.");
            this.displayModel.SnapToGoogleImagery();
        }

        private bool DisplayModelOverlayIsGoogle() {
            return displayModel.SelectedOverlayMapStatusIsGoogle();
        }

        private bool MapSettingsOverlayIsGoogle() {
            return mapsettings.MapInfoSelectorItem.IsGoogle();
        }

        private void SaveMapSettings() {
            clientEndpoint.SaveMapSettings(mapsettings);
        }

        private void SetMapSettingsCMGSelectorItem() {
            mapsettings.CMGMapInfoSelectorItem = mapsettings.MapInfoSelectorItem;
        }

        private void ChangeZoomLevel() {
            var vm = new Overlays.DataSourceMapZoomLevelChangeConfirmationViewModel(clientEndpoint);
            ScreenDescriptor sd = new ScreenDescriptor("Landdb.Views.Overlays.DataSourceMapZoomLevelChangeConfirmationView", "zoomlevelChangeConfirm", vm);
            Messenger.Default.Send(new ShowOverlayMessage { ScreenDescriptor = sd });
        }

        private bool MAPDisplayOptionsIsCollapsed() {
            return MAPDisplayOptionsVisibility == Visibility.Collapsed;
        }

        private void SetCMGSelectorItemThenSaveAndChangeZoom() {
            SetMapSettingsCMGSelectorItem();
            SaveMapSettings();
            ChangeZoomLevel();
        }

        private bool DisplayIsNotGoogleAndSettingsIsGoogle() {
            return !DisplayModelOverlayIsGoogle() && MapSettingsOverlayIsGoogle();
        }
        private bool DisplayIsGoogleAndSettingsIsNotGoogle() {
            return DisplayModelOverlayIsGoogle() && !MapSettingsOverlayIsGoogle();
        }

        private bool GoogleIsSelectedInSettingsXorDisplayModel() {
            return DisplayIsNotGoogleAndSettingsIsGoogle() || DisplayIsGoogleAndSettingsIsNotGoogle();
        }

        private void RefreshMapSettingsAndMakeOptionsVisible() {
            MAPOptionsViewModel.RefreshMapSettings(ApplicationEnvironment.ShowPremiumMap);
            MAPDisplayOptionsVisibility = Visibility.Visible;
        }

        private void RefreshImageryLayerAndCollapseOptions() {
            MAPDisplayOptionsVisibility = Visibility.Collapsed;
            displayModel.RefreshImageryLayer(null);
        }

        private void ChangeMapDisplayOptionsAndRefresh() {
            if (MAPDisplayOptionsIsCollapsed()) {
                RefreshMapSettingsAndMakeOptionsVisible();
            } else {
                RefreshImageryLayerAndCollapseOptions();
            }
        }

        private void ToggleOptionsVisibility() {
            log.Info("DrawingTools - Show map options.");
            IsOptionsMapPopupOpen = false;
            IsMeasureLine = false;
            if (GoogleIsSelectedInSettingsXorDisplayModel()) {
                SetCMGSelectorItemThenSaveAndChangeZoom();
            } else {
                ChangeMapDisplayOptionsAndRefresh();
            }            
        }

        #endregion Events and Messaging

    }
}
