﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Landdb.Domain.ReadModels.Tree;
using GalaSoft.MvvmLight;
using System.Windows.Shapes;
using Landdb.Client.Spatial;
using System.Windows.Threading;
using System.Windows;
using Microsoft.Maps.MapControl.WPF;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Infrastructure;
using Landdb.ViewModel.Fields.CropZoneDetails.PropertyParts;
using Landdb.ViewModel.Fields.FieldDetails.PropertyParts;
using System.Windows.Input;
using Landdb.Controls;
using GalaSoft.MvvmLight.Command;
using Landdb.Domain.ReadModels.Tags;
using Landdb.Models;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net;
using Newtonsoft.Json;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure.Messages;

namespace Landdb.ViewModel.Fields.FieldDetails {
    public class CropZoneDetailsViewModel : ViewModelBase, IHasName {
        readonly CropZoneDetailsView czView;
        string name;
        Path cropzonePreview;
        MapLayer layer;
        bool shouldShowBingMaps;
        bool shouldShowWpfMock;
        bool hasNoMapData;
        readonly Dispatcher dispatcher;
        readonly IClientEndpoint clientEndpoint;
        int cropYear;

        CropZoneLocation cropZoneLocation;
        CropZoneNotes notes;
        CropInformation cropInfo;
        PermitInformation permitInfo;
        NameViewModel nameInfo;
        AreaInformation areaInfo;
        TillageInformation tillageInfo;
        CropZoneVisibility cropzoneVis;
        WaterInformation waterInfo;
        FsaAreaInformation fsaInfo;
        StewardshipInformation stewardshipInfo;

        List<TagItem> selectedTags;
        AbstractTreeItemViewModel treeItem;

        public CropZoneDetailsViewModel(CropZoneDetailsView czView, IEnumerable<string> mapData, IClientEndpoint clientEndpoint, AbstractTreeItemViewModel treeItem) : this(czView, mapData, clientEndpoint, Application.Current.Dispatcher, treeItem) { }

        public CropZoneDetailsViewModel(CropZoneDetailsView czView, IEnumerable<string> mapData, IClientEndpoint clientEndpoint, Dispatcher dispatcher, AbstractTreeItemViewModel treeItem) {
            this.czView = czView;
            this.name = czView.Name;
            this.dispatcher = dispatcher;
            this.clientEndpoint = clientEndpoint;
            this.treeItem = treeItem;
            this.cropYear = ApplicationEnvironment.CurrentCropYear; // TODO: Really would like to pass this in on creation.

            if (!mapData.Any()) {
                HasNoMapData = true;
            } else {
                ShouldShowBingMaps = NetworkStatus.IsInternetAvailableFast();
                ShouldShowWpfMock = !ShouldShowBingMaps;

                dispatcher.BeginInvoke(new Action(() => {
                    if (shouldShowBingMaps) {
                        layer = BingMapsUtility.GetLayerForMapData(mapData);

                    } else {
                        cropzonePreview = WpfTransforms.CreatePathFromMapData(mapData);
                    }
                }));
            }

            cropZoneLocation = new CropZoneLocation(clientEndpoint, dispatcher, czView);
            notes = new CropZoneNotes(clientEndpoint, dispatcher, czView);
            cropInfo = new CropInformation(clientEndpoint, dispatcher, this);
            permitInfo = new PermitInformation(clientEndpoint, dispatcher, czView);
            waterInfo = new WaterInformation(clientEndpoint, dispatcher, czView, ApplicationEnvironment.CurrentCropYear);
            nameInfo = new NameViewModel(clientEndpoint, dispatcher, this, ApplicationEnvironment.CurrentCropYear);
            areaInfo = new AreaInformation(clientEndpoint, dispatcher, czView, ApplicationEnvironment.CurrentCropYear);
            tillageInfo = new TillageInformation(clientEndpoint, dispatcher, czView, ApplicationEnvironment.CurrentCropYear);
            cropzoneVis = new CropZoneVisibility(clientEndpoint, dispatcher, czView, ApplicationEnvironment.CurrentCropYear);
            fsaInfo = new FsaAreaInformation(clientEndpoint, dispatcher, czView);
            stewardshipInfo = new StewardshipInformation(clientEndpoint, dispatcher, czView);

            ShowIntegrationCommand = new RelayCommand(ShowIntegration);
            Close = new RelayCommand(CloseMessage);
            TagAddedCommand = new RelayCommand<TagEventArgs>(AddTag);
            TagRemovedCommand = new RelayCommand<TagEventArgs>(RemoveTag);

            var tagList = czView.Tags.Select(x => new TagItem(x));
            SelectedTags = tagList.ToList();
        }

        public RelayCommand<TagEventArgs> TagAddedCommand { get; }
        public RelayCommand<TagEventArgs> TagRemovedCommand { get; }
        public RelayCommand ShowIntegrationCommand { get; set; }
        public RelayCommand Close { get; set; }

        public string Name {
            get { return name; }
            set {
                name = value;
                RaisePropertyChanged("Name");

                if (nameInfo != null && nameInfo.Name != value) {
                    // Then this name change didn't come from the nameInfo model. Need to update it to match.
                    nameInfo.Name = value;
                }

                if (treeItem is CropZoneTreeItemViewModel) {
                    ((CropZoneTreeItemViewModel)treeItem).UpdateName(value);
                }
            }
        }

        public CropZoneLocation Location {
            get { return cropZoneLocation; }
        }

        public FsaAreaInformation FsaInfo
        {
            get { return fsaInfo; }
        }

        public StewardshipInformation StewardshipInfo
        {
            get { return stewardshipInfo; }
        }

        public NameViewModel NameInfo {
            get { return nameInfo; }
        }

        public AreaInformation AreaInfo {
            get { return areaInfo; }
        }

        public CropZoneNotes Notes {
            get { return notes; }
        }

        public CropZoneVisibility CropZoneVis {
            get { return cropzoneVis; }
        }

        public CropInformation CropInfo {
            get { return cropInfo; }
        }

        public PermitInformation PermitInfo {
            get { return permitInfo; }
        }

        public TillageInformation TillageInfo
        {
            get { return tillageInfo; }
        }

        public WaterInformation WaterInfo
        {
            get { return waterInfo; }
        }

        public Path CropZonePreview {
            get { return cropzonePreview; }
        }

        public MapLayer BingMapsLayer {
            get { return layer; }
        }

        public string MapCulture
        {
            get { return Infrastructure.ApplicationEnvironment.BingWpfMapCulture; }
        }

        public List<string> AvailableTags {
            get {
                var tagsMaybe = clientEndpoint.GetView<TagListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear));
                if (tagsMaybe.HasValue) {
                    var currentTags = SelectedTags.Select<TagItem, string>(x => x.Text);
                    List<string> tags = tagsMaybe.Value.UsedTagsList.Except(currentTags).ToList();
                    return tags;
                } else {
                    return new List<string>();
                }
            }
        }

        internal Landdb.Domain.ReadModels.Tree.CropZoneDetailsView CropZoneView {
            get { return czView; }
        }

        public bool ShouldShowBingMaps {
            get { return shouldShowBingMaps; }
            private set {
                shouldShowBingMaps = value;
                RaisePropertyChanged("ShouldShowBingMaps");
            }
        }

        public bool ShouldShowWpfMock {
            get { return shouldShowWpfMock; }
            set {
                shouldShowWpfMock = value;
                RaisePropertyChanged("ShouldShowWpfMock");

            }
        }

        public bool HasNoMapData {
            get { return hasNoMapData; }
            set {
                hasNoMapData = value;
                RaisePropertyChanged("HasNoMapData");

                ShouldShowBingMaps = false;
                ShouldShowWpfMock = false;
            }
        }

        public List<TagItem> SelectedTags {
            get { return selectedTags; }
            set {
                selectedTags = value;
                RaisePropertyChanged("SelectedTags");
            }
        }

        void RemoveTag(TagEventArgs tag) {
            var removeTagCommand = new UntagCropZone(czView.Id, clientEndpoint.GenerateNewMetadata(), new string[] { tag.Item.Text }, cropYear);
            clientEndpoint.SendOne(removeTagCommand);
            RaisePropertyChanged("AvailableTags");
        }

        void AddTag(TagEventArgs tag) {
            var addTagCommand = new TagCropZone(czView.Id, clientEndpoint.GenerateNewMetadata(), new string[] { tag.Item.Text }, cropYear);
            clientEndpoint.SendOne(addTagCommand);
            RaisePropertyChanged("AvailableTags");
        }

        public void OverrideBoundaryValue(double updateboundarytemporarily)
        {
            areaInfo.BoundaryAreaValue = updateboundarytemporarily;
        }

        public void ShowIntegration()
        {
            //TO DO :: CALL API TO GET PAGE
            //List<DataSource> List = new List<DataSource>();
            Task<string> getMLZone = Task.Factory.StartNew<string>(() =>
            {

                var dataSource = new DataSourceId(ApplicationEnvironment.CurrentDataSourceId);
                var cropYear = ApplicationEnvironment.CurrentCropYear;

                if (NetworkStatus.IsInternetAvailable())
                {
                    var baseDocumentUri = string.Format("{0}api/treeviewitem/getTokenUID/", RemoteConfigurationSettings.GetBaseUri());
                    var action = string.Format("?dataSourceId={0}&cropYear={2}&nodeId={1}&nodeType=field", dataSource.Id, czView.Id.Id, cropYear);
                    var client = new HttpClient
                    {
                        BaseAddress = new Uri(baseDocumentUri),
                    };

                    client.DefaultRequestHeaders.Add("Accept", "application / json");
                    //client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    var holder = GetData<string>(action, client);

                    holder.Wait();
                    //Guid mzId = holder != null ? holder.Result : new Guid();

                    var formattedLink = holder.Result;
                    if (formattedLink != null)
                    {
                        System.Diagnostics.Process.Start(formattedLink);
                    }
                    else
                    {
                        //TO DO :: SHOW POPUP
                        Messenger.Default.Send<ShowPopupMessage>(new ShowPopupMessage() { ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Fields.Popups.PremierIntegrationNotification", "messagePop", this) });
                    }

                    return holder.Result;
                }
                else
                {
                    return string.Empty;
                }
            });
        }

        private void CloseMessage()
        {
            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
        }

        private async Task<T> GetData<T>(string baseDocumentUri, HttpClient client)
        {
            try
            {
                var cloudClientFactory = new Landdb.Infrastructure.CloudClientFactory();
                var token = cloudClientFactory.ResolveStorageCredentials();
                await token.SetBearerTokenAsync(client);

                var response = await client.GetAsync(baseDocumentUri);

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var json = await response.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<T>(json);
                    return result;
                }
                else
                {
                    var fake = string.Empty;
                    var result = JsonConvert.DeserializeObject<T>(fake);
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Scouting Service Info Retrieval Failed.");
            }
        }

    }
}
