﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using GalaSoft.MvvmLight;
using Landdb.Client.Infrastructure;
using Landdb.Client.Spatial;
using Landdb.Infrastructure.Mapping;
using Landdb.ViewModel.Fields.FieldDetails.PropertyParts;
using Microsoft.Maps.MapControl.WPF;
using ThinkGeo.MapSuite.Core;
using Landdb.Infrastructure;
using Landdb.Models;
using GalaSoft.MvvmLight.Command;
using System.Net.Http;
using System.Threading.Tasks;
using System.Net;
using Newtonsoft.Json;

namespace Landdb.ViewModel.Fields.FieldDetails {
    public class FieldDetailsViewModel : ViewModelBase, IHasName {
        readonly Domain.ReadModels.Tree.FieldDetailsView fieldView;
        string name;
        Path fieldPreview;
        MapLayer layer; 
        bool shouldShowBingMaps;
        bool shouldShowWpfMock;
        bool hasNoMapData;
        Dispatcher dispatcher;

        readonly FieldTreeItemViewModel fieldTreeItem;

        FsaInformation fsaInfo;
        LegalInformation legalInfo;
        LandInformation landInfo;
        WaterInformation waterInfo;
        PermitInformation permitInfo;
        FieldNotes noteSection;
        FieldLocation fieldLocation;
        CropHistoryInformation cropHistory;
        NameViewModel nameInfo;
        AreaInformation areaInfo;

        public FieldDetailsViewModel(IClientEndpoint clientEndpoint, Domain.ReadModels.Tree.FieldDetailsView fieldView, IEnumerable<string> mapData, FieldTreeItemViewModel fieldTreeItem) : this(clientEndpoint, fieldView, mapData, fieldTreeItem, Application.Current.Dispatcher) { }

        public FieldDetailsViewModel(IClientEndpoint clientEndpoint, Domain.ReadModels.Tree.FieldDetailsView fieldView, IEnumerable<string> mapData, FieldTreeItemViewModel fieldTreeItem, Dispatcher dispatcher) {
            this.fieldView = fieldView;
            this.name = fieldView.FieldNameByCropYear.ContainsKey(ApplicationEnvironment.CurrentCropYear) ? fieldView.FieldNameByCropYear[ApplicationEnvironment.CurrentCropYear] : fieldView.Name; // .Name;
            this.dispatcher = dispatcher;
            this.fieldTreeItem = fieldTreeItem;

            if (!mapData.Any()) {
                HasNoMapData = true;
            } else {
                ShouldShowBingMaps = NetworkStatus.IsInternetAvailableFast();
                ShouldShowWpfMock = !ShouldShowBingMaps;

                dispatcher.BeginInvoke(new Action(() => {
                    if (ShouldShowBingMaps) {
                        layer = BingMapsUtility.GetLayerForMapData(mapData);
                    } else {
                        fieldPreview = WpfTransforms.CreatePathFromMapData(mapData);
                    }
                }));
            }

            ShowIntegrationCommand = new RelayCommand(ShowIntegration);

            fsaInfo = new FsaInformation(clientEndpoint, dispatcher, fieldView, ApplicationEnvironment.CurrentCropYear);
            legalInfo = new LegalInformation(clientEndpoint, dispatcher, fieldView, ApplicationEnvironment.CurrentCropYear);
            landInfo = new LandInformation(clientEndpoint, dispatcher, fieldView, ApplicationEnvironment.CurrentCropYear);
            waterInfo = new WaterInformation(clientEndpoint, dispatcher, fieldView, ApplicationEnvironment.CurrentCropYear);
            permitInfo = new PermitInformation(clientEndpoint, dispatcher, fieldView, ApplicationEnvironment.CurrentCropYear);
            noteSection = new FieldNotes(clientEndpoint, dispatcher, fieldView);
            fieldLocation = new FieldLocation(clientEndpoint, dispatcher, fieldView);
            cropHistory = new CropHistoryInformation(clientEndpoint, dispatcher, fieldView);
            nameInfo = new NameViewModel(clientEndpoint, dispatcher, this, ApplicationEnvironment.CurrentCropYear);
            areaInfo = new AreaInformation(clientEndpoint, dispatcher, fieldView, ApplicationEnvironment.CurrentCropYear);
        }

        public RelayCommand ShowIntegrationCommand { get; set; }

        public string Name {
            get { return name; }
            set {
                name = value;
                RaisePropertyChanged("Name");

                if (fieldTreeItem != null) {
                    fieldTreeItem.UpdateName(value);
                }
            }
        }

        public CropHistoryInformation CropHistory
        {
            get { return cropHistory; }
        }

        public NameViewModel NameInfo
        {
            get { return nameInfo; }
        }

        public AreaInformation AreaInfo
        {
            get { return areaInfo; }
        }

        public FieldLocation FieldLocation
        {
            get { return fieldLocation; }
        }

        public FsaInformation FsaInfo {
            get { return fsaInfo; }
        }

        public LegalInformation LegalInfo
        {
            get { return legalInfo; }
        }

        public LandInformation LandInfo {
            get { return landInfo; }
        }

        public WaterInformation WaterInfo
        {
            get { return waterInfo; }
        }

        public PermitInformation PermitInfo
        {
            get { return permitInfo; }
        }

        public FieldNotes NoteSection
        {
            get { return noteSection; }
        }

        public Path FieldPreview {
            get { return fieldPreview; }
        }

        public MapLayer BingMapsLayer {
            get { return layer; }
        }

        public string MapCulture
        {
            get { return Infrastructure.ApplicationEnvironment.BingWpfMapCulture; }
        }

        internal Domain.ReadModels.Tree.FieldDetailsView FieldView {
            get { return fieldView; }
        }

        public bool ShouldShowBingMaps {
            get { return shouldShowBingMaps; }
            private set {
                shouldShowBingMaps = value;
                RaisePropertyChanged("ShouldShowBingMaps");
            }
        }

        public bool ShouldShowWpfMock {
            get { return shouldShowWpfMock; }
            set {
                shouldShowWpfMock = value;
                RaisePropertyChanged("ShouldShowWpfMock");

            }
        }

        public bool HasNoMapData {
            get { return hasNoMapData; }
            set {
                hasNoMapData = value;
                RaisePropertyChanged("HasNoMapData");

                ShouldShowBingMaps = false;
                ShouldShowWpfMock = false;
            }
        }

        public IList<CropZoneTreeItemViewModel> CropZoneTreeModels {
            get {
                if (fieldTreeItem == null) { return new List<CropZoneTreeItemViewModel>(); }

                return (from f in fieldTreeItem.Children
                         where f is CropZoneTreeItemViewModel
                         select (CropZoneTreeItemViewModel)f).ToList();
            }
        }

        public void ShowIntegration()
        {
            //TO DO :: CALL API TO GET PAGE
            //List<DataSource> List = new List<DataSource>();
            Task<string> getMLZone = Task.Factory.StartNew<string>(() =>
            {

                var dataSource = new DataSourceId(ApplicationEnvironment.CurrentDataSourceId);
                var cropYear = ApplicationEnvironment.CurrentCropYear;

                if (NetworkStatus.IsInternetAvailable())
                {
                    var baseDocumentUri = string.Format("{0}api/treeviewitem/getTokenUID/", RemoteConfigurationSettings.GetBaseUri());
                    var action = string.Format("?dataSourceId={0}&cropYear={2}&nodeId={1}&nodeType=field", dataSource.Id, fieldView.Id.Id, cropYear);
                    var client = new HttpClient
                    {
                        BaseAddress = new Uri(baseDocumentUri),
                    };

                    client.DefaultRequestHeaders.Add("Accept", "application / json");
                    //client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    var holder = GetData<string>(action, client);

                    holder.Wait();
                    //Guid mzId = holder != null ? holder.Result : new Guid();

                    var formattedLink = holder.Result;
                    System.Diagnostics.Process.Start(formattedLink);

                    return holder.Result;
                }
                else
                {
                    return string.Empty;
                }
            });
        }

        private async Task<T> GetData<T>(string baseDocumentUri, HttpClient client)
        {
            try
            {
                var cloudClientFactory = new Landdb.Infrastructure.CloudClientFactory();
                var token = cloudClientFactory.ResolveStorageCredentials();
                await token.SetBearerTokenAsync(client);

                var response = await client.GetAsync(baseDocumentUri);

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var json = await response.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<T>(json);
                    return result;
                }
                else
                {
                    var fake = string.Empty;
                    var result = JsonConvert.DeserializeObject<T>(fake);
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Scouting Service Info Retrieval Failed.");
            }
        }

        public FieldTreeItemViewModel FieldTreeItemVM {
            get {
                if (fieldTreeItem == null) { return null; }
                return fieldTreeItem;
            }
        }

        public void OverrideBoundaryValue(double updateboundarytemporarily)
        {
            areaInfo.BoundaryAreaValue = updateboundarytemporarily;
        }
    }
}