﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Landdb.Domain.ReadModels.Tree;
using GalaSoft.MvvmLight;
using System.Windows.Shapes;
using Landdb.Client.Spatial;
using System.Windows.Threading;
using System.Windows;
using Microsoft.Maps.MapControl.WPF;
using Landdb.Client.Infrastructure;
using Landdb.ViewModel.Farms.PropertyParts;
using Landdb.ViewModel.Fields.FieldDetails.PropertyParts;
using Landdb.Infrastructure;
using System.Windows.Input;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure.Messages;
using GalaSoft.MvvmLight.Command;
using Landdb.Models;
using AgC.UnitConversion;

namespace Landdb.ViewModel.Fields.FieldDetails {
    public class FarmDetailsViewModel : ViewModelBase, IHasName {
        readonly FarmDetailsView farmDetails;
        string name;
        Path farmPreview;
        MapLayer layer; 
        bool shouldShowBingMaps;
        bool shouldShowWpfMock;
        bool hasNoMapData;
        Dispatcher dispatcher;
        IClientEndpoint clientEndpoint;

        Farms.PropertyParts.FsaInformation fsaInfo;
        Farms.PropertyParts.RegulatoryInformation permitInfo;
        FarmNotes farmNotes;
        NameViewModel nameInfo;

        FarmTreeItemViewModel treeItem;

        public FarmDetailsViewModel(IClientEndpoint clientEndpoint, FarmDetailsView farmDetails, IEnumerable<string> mapData, FarmTreeItemViewModel treeItem) : this(clientEndpoint, farmDetails, mapData, Application.Current.Dispatcher, treeItem) { }
        public FarmDetailsViewModel(IClientEndpoint clientEndpoint, FarmDetailsView farmDetails, IEnumerable<string> mapData, Dispatcher dispatcher, FarmTreeItemViewModel treeItem) {
            this.farmDetails = farmDetails;

            var closestYears = from year in farmDetails.FarmNameByCropYear.Keys
                               where year < ApplicationEnvironment.CurrentCropYear
                               select year;
            var closestYear = closestYears.Any() ? closestYears.Max() : (farmDetails.FarmNameByCropYear.Keys.Where(x => x > ApplicationEnvironment.CurrentCropYear).Any() ? farmDetails.FarmNameByCropYear.Keys.Where(x => x > ApplicationEnvironment.CurrentCropYear).Min() : ApplicationEnvironment.CurrentCropYear);
            var farmName = farmDetails.FarmNameByCropYear.ContainsKey(closestYear) ? farmDetails.FarmNameByCropYear[closestYear] : farmDetails.Name;

            this.name = farmDetails.FarmNameByCropYear.ContainsKey(ApplicationEnvironment.CurrentCropYear) ? farmDetails.FarmNameByCropYear[ApplicationEnvironment.CurrentCropYear] : farmName;//.Name;
            this.dispatcher = dispatcher;
            this.clientEndpoint = clientEndpoint;
            this.treeItem = treeItem;

			var state = string.Empty;
			if (!string.IsNullOrWhiteSpace(farmDetails.State)) {
				var stateObj = clientEndpoint.GetMasterlistService().GetStateByName(farmDetails.State);
				if (stateObj != null) { state = stateObj.Name; }
			}

            Location = farmDetails.County == string.Empty || farmDetails.County == null ? state : farmDetails.County + ", " + state;

            if (!mapData.Any()) {
                HasNoMapData = true;
            } else {
                ShouldShowBingMaps = NetworkStatus.IsInternetAvailableFast();
                ShouldShowWpfMock = !ShouldShowBingMaps;

                dispatcher.BeginInvoke(new Action(() => {
                    if (shouldShowBingMaps) {
                        layer = BingMapsUtility.GetLayerForMapData(mapData);
                    } else {
                        farmPreview = WpfTransforms.CreatePathFromMapData(mapData);
                    }
                }));
            }

            fsaInfo = new Farms.PropertyParts.FsaInformation(clientEndpoint, dispatcher, farmDetails);
            permitInfo = new Farms.PropertyParts.RegulatoryInformation(clientEndpoint, dispatcher, farmDetails);
            farmNotes = new FarmNotes(clientEndpoint, dispatcher, farmDetails);
            nameInfo = new NameViewModel(clientEndpoint, dispatcher, this, ApplicationEnvironment.CurrentCropYear);
            
            UpdateLocation = new RelayCommand(UpdateFarmLocation);

            double totalArea = 0.0;
            string areaUnit = string.Empty;
            foreach (var c in treeItem.Children)
            {
                var fieldDetails = clientEndpoint.GetView<FieldDetailsView>(c.Id as FieldId).Value;
                var fieldArea = fieldDetails.ReportedArea != null ? fieldDetails.ReportedArea : fieldDetails.BoundaryArea;
                totalArea += fieldArea != null ? (double)fieldArea : 0.0;
                if (string.IsNullOrEmpty(areaUnit)) {
                    areaUnit = fieldDetails.ReportedArea != null ? fieldDetails.ReportedAreaUnit : fieldDetails.BoundaryAreaUnit;
                }
            }

            if (!string.IsNullOrEmpty(areaUnit)) {
                TotalArea = UnitFactory.GetUnitByName(areaUnit).GetMeasure(totalArea);
            }
        }

        public ICommand UpdateLocation { get; set; }

        public string Name {
            get { return name; }
            set {
                name = value;
                RaisePropertyChanged("Name");

                if (treeItem != null) {
                    treeItem.UpdateName(value);
                }
            }
        }

        public Measure TotalArea { get; set; }

        public string Location
        {
            get;
            set;
        }

        public NameViewModel NameInfo
        {
            get { return nameInfo; }

        }

        public Farms.PropertyParts.FsaInformation FsaInfo
        {
            get { return fsaInfo; }
        }

        public Farms.PropertyParts.RegulatoryInformation PermitInfo
        {
            get { return permitInfo; }
        }

        public FarmNotes NoteSection
        {
            get { return farmNotes; }
        }

        public Path FarmPreview {
            get { return farmPreview; }
        }

        public MapLayer BingMapsLayer {
            get { return layer; }
        }

        public string MapCulture
        {
            get { return Infrastructure.ApplicationEnvironment.BingWpfMapCulture; }
        }

        internal FarmDetailsView FarmDetails {
            get { return farmDetails; }
        }

        public bool ShouldShowBingMaps {
            get { return shouldShowBingMaps; }
            private set {
                shouldShowBingMaps = value;
                RaisePropertyChanged("ShouldShowBingMaps");
            }
        }

        public bool ShouldShowWpfMock {
            get { return shouldShowWpfMock; }
            set {
                shouldShowWpfMock = value;
                RaisePropertyChanged("ShouldShowWpfMock");

            }
        }

        public bool HasNoMapData {
            get { return hasNoMapData; }
            set {
                hasNoMapData = value;
                RaisePropertyChanged("HasNoMapData");

                ShouldShowBingMaps = false;
                ShouldShowWpfMock = false;
            }
        }

        void UpdateFarmLocation()
        {
            var vm = new UpdateFarmLocationInfo(clientEndpoint, farmDetails);
            var sd = new ScreenDescriptor("Landdb.Views.Farms.Popups.UpdateFarmLocationView", "editLocation", vm);
            Messenger.Default.Send(new ShowPopupMessage() { ScreenDescriptor = sd });
        }

    }
}
