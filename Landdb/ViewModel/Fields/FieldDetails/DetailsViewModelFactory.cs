﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Map;
using Landdb.Domain.ReadModels.Tree;
using ThinkGeo.MapSuite.Core;

namespace Landdb.ViewModel.Fields.FieldDetails {
    
    public class DetailsViewModelFactory : IDetailsViewModelFactory {
        readonly IClientEndpoint clientEndpoint;

        public DetailsViewModelFactory(IClientEndpoint clientEndpoint) {
            this.clientEndpoint = clientEndpoint;
        }

        public void CreateDetailsViewModel(IIdentity itemId, IList<MapItem> mapItems, AbstractTreeItemViewModel treeItem, Action<object> OnViewModelCreated) {
            object vm = null;

            if (itemId is FarmId) {
                var farmView = clientEndpoint.GetView<FarmDetailsView>(itemId);
                if (farmView.HasValue) {
                    var filteredMapData = from mi in mapItems
                                          where mi.FarmId.Equals(((FarmId)itemId).Id) && !string.IsNullOrWhiteSpace(mi.MapData)
                                          select mi.MapData;

                    vm = new FarmDetailsViewModel(clientEndpoint ,farmView.Value, filteredMapData, treeItem as FarmTreeItemViewModel);
                } else {
                    vm = new WaitingDetailsViewModel(itemId, mapItems, treeItem, OnViewModelCreated, CreateDetailsViewModel);
                }
            }
            if (itemId is FieldId) {
                var fieldView = clientEndpoint.GetView<Landdb.Domain.ReadModels.Tree.FieldDetailsView>(itemId);
                if (fieldView.HasValue) {
                    //var filteredMapData = from mi in mapItems
                    //                       where mi.FieldId.Equals(((FieldId) itemId).Id) && !string.IsNullOrWhiteSpace(mi.MapData)
                    //                       select mi.MapData;
                    var maps = clientEndpoint.GetView<ItemMap>(fieldView.Value.Id);
                    List<string> filteredMapData = new List<string>();
                    if (maps.HasValue)
                    {
                        ItemMapItem item_map_item = GetMostRecentShapeByYear(maps);
                        if (item_map_item != null)
                        {
                            filteredMapData.Add(item_map_item.MapData);
                        }
                    }


                    vm = new FieldDetailsViewModel(clientEndpoint, fieldView.Value, filteredMapData, treeItem as FieldTreeItemViewModel);
                    if(maps.HasValue && maps.Value.MostRecentMapItem.CropYearChangeOccurred > Infrastructure.ApplicationEnvironment.CurrentCropYear)
                    {
                        Proj4Projection projection = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetGoogleMapParametersString());
                        Feature feature = new Feature(filteredMapData.FirstOrDefault());
                        MapSettings mapsettings = this.clientEndpoint.GetMapSettings();
                        string areaUnitString = mapsettings.MapAreaUnit;
                        AgC.UnitConversion.Area.AreaUnit agcAreaUnit = AgC.UnitConversion.UnitFactory.GetUnitByName(areaUnitString) as AgC.UnitConversion.Area.AreaUnit;
                        ThinkGeo.MapSuite.Core.AreaUnit mapAreaUnit = MapModels.MapUnitFactory.AreaMeasureConversion(agcAreaUnit);
                        double areaTotal = ((AreaBaseShape)feature.GetShape()).GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
                        ((FieldDetailsViewModel)vm).OverrideBoundaryValue(areaTotal);
                    }
                } else {
                    vm = new WaitingDetailsViewModel(itemId, mapItems, treeItem, OnViewModelCreated, CreateDetailsViewModel);
                }
            }
            if (itemId is CropZoneId) {
                var czView = clientEndpoint.GetView<Landdb.Domain.ReadModels.Tree.CropZoneDetailsView>(itemId);
                if (czView.HasValue) {
                    var mi = clientEndpoint.GetView<ItemMap>(itemId);
                    List<string> filteredMapData = new List<string>();
                    if (mi.HasValue) {
                        //var filteredData = from x in mi.Value.MapItems
                        //                   select x.MapData;
                        //filteredMapData.AddRange(filteredData);
                        //if (mi.Value.MostRecentMapItem != null)
                        //{
                        //    filteredMapData.Add(mi.Value.MostRecentMapItem.MapData);
                        //}
                        ItemMapItem item_map_item = GetMostRecentShapeByYear(mi);
                        if (item_map_item != null)
                        {
                            filteredMapData.Add(item_map_item.MapData);
                        }
                    }

                    vm = new CropZoneDetailsViewModel(czView.Value, filteredMapData, clientEndpoint, treeItem);
                    if (mi.HasValue && mi.Value.MostRecentMapItem != null && mi.Value.MostRecentMapItem.CropYearChangeOccurred > Infrastructure.ApplicationEnvironment.CurrentCropYear && mi.Value.MostRecentMapItem.MapData != null)
                    {
                        Proj4Projection projection = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetGoogleMapParametersString());
                        Feature feature = new Feature(filteredMapData.FirstOrDefault());
                        MapSettings mapsettings = this.clientEndpoint.GetMapSettings();
                        string areaUnitString = mapsettings.MapAreaUnit;
                        AgC.UnitConversion.Area.AreaUnit agcAreaUnit = AgC.UnitConversion.UnitFactory.GetUnitByName(areaUnitString) as AgC.UnitConversion.Area.AreaUnit;
                        ThinkGeo.MapSuite.Core.AreaUnit mapAreaUnit = MapModels.MapUnitFactory.AreaMeasureConversion(agcAreaUnit);
                        double areaTotal = ((AreaBaseShape)feature.GetShape()).GetArea(GeographyUnit.DecimalDegree, mapAreaUnit);
                        ((CropZoneDetailsViewModel)vm).OverrideBoundaryValue(areaTotal);
                    }
                }
                else {
                    vm = new WaitingDetailsViewModel(itemId, mapItems, treeItem, OnViewModelCreated, CreateDetailsViewModel);
                }
            }
            if (itemId is CropId) {
                //var cropView = clientEndpoint.GetView<Landdb.Domain.ReadModels.Tree.CropDetails>(itemId);
                vm = new CropDetailsViewModel(clientEndpoint, new List<string>(), System.Windows.Application.Current.Dispatcher);
            }

            if (vm != null) {
                OnViewModelCreated(vm);
            }  else {
                OnViewModelCreated(new HomeDetailsViewModel(clientEndpoint));
            }
        }

        private static ItemMapItem GetMostRecentShapeByYear(Lokad.Cqrs.Maybe<ItemMap> maps_a)
        {
            ItemMapItem item_map_item = null;
            if (maps_a.HasValue && maps_a.Value.MostRecentMapItem != null && maps_a.Value.MostRecentMapItem.CropYearChangeOccurred <= Infrastructure.ApplicationEnvironment.CurrentCropYear)
            {
                item_map_item = maps_a.Value.MostRecentMapItem;
            }
            else
            {
                ItemMapItem currentyearmostrecentboundarychange = (from md in maps_a.Value.MapItems where (md.CropYearChangeOccurred <= Infrastructure.ApplicationEnvironment.CurrentCropYear) select md).LastOrDefault();
                if(currentyearmostrecentboundarychange != null && !string.IsNullOrEmpty(currentyearmostrecentboundarychange.MapData))
                {
                    item_map_item = currentyearmostrecentboundarychange;
                }
                if (item_map_item == null)
                {
                    item_map_item = maps_a.Value.MostRecentMapItem;
                }
            }
            return item_map_item;
        }
    }


    public interface IDetailsViewModelFactory {
        void CreateDetailsViewModel(IIdentity itemId, IList<MapItem> mapItems, AbstractTreeItemViewModel treeItem, Action<object> OnViewModelCreated);
    }

}
