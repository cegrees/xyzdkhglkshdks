﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Fields.MassEditorAssigner {
	public class TagSelectionItem : ViewModelBase {

		private readonly ObservableCollection<TagSelectionItem> listRef;

		private readonly Action<TagSelectionItem> checkChanged;

		public TagSelectionItem(string tag, bool isSaved, ObservableCollection<TagSelectionItem> listRef, Action<TagSelectionItem> onCheckChanged) {
			IsSaved = isSaved;
			Tag = tag;

			this.listRef = listRef;

			checkChanged += onCheckChanged;

			// updates the index when items in the middle are removed.
			// e.g., in list [a, b, c], when c is removed, the index of c becomes 2.
			listRef.CollectionChanged += (sender, args) => {
				RaisePropertyChanged(() => Index);
			};
		}

		public bool IsSaved { get; }

		public int Index => listRef.IndexOf(this) + 1;

		private string _tag;
		public string Tag {
			get { return _tag; }
			set {
				_tag = value;

				IsCheckable = !string.IsNullOrWhiteSpace(_tag);
				IsChecked = IsCheckable && !IsSaved;

				RaisePropertyChanged(() => Tag);
			}
		}

		private bool _isCheckable;
		public bool IsCheckable {
			get { return _isCheckable; }
			set {
				_isCheckable = value;
				if (!_isCheckable) { IsChecked = false; }
				RaisePropertyChanged(() => IsCheckable);
			}
		}

		private bool _isChecked;
		public bool IsChecked {
			get { return _isChecked; }
			set {
				_isChecked = value;

				checkChanged?.Invoke(this);

				RaisePropertyChanged(() => IsChecked);
				RaisePropertyChanged(() => Index);
			}
		}

		public override string ToString() => Tag;
	}
}