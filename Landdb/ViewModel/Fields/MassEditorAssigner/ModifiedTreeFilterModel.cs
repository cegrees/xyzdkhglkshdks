﻿using GalaSoft.MvvmLight;
using Landdb.Client.Infrastructure;

namespace Landdb.ViewModel.Fields.MassEditorAssigner {
	public class ModifiedTreeFilterModel : ViewModelBase, IPageFilterViewModel {

		private readonly IClientEndpoint clientEndpoint;

		public ModifiedTreeFilterModel(IClientEndpoint clientEndpoint) {
			this.clientEndpoint = clientEndpoint;
		}

		string nameFilter = string.Empty;
		public string NameFilter {
			get { return nameFilter; }
			set {
				nameFilter = value;
				RaisePropertyChanged(() => NameFilter);
			}
		}

		private bool _showOriginalItems = false;
		public bool ShowOriginalItems {
			get { return _showOriginalItems; }
			set {
				_showOriginalItems = value;
				RaisePropertyChanged(() => ShowOriginalItems);
			}
		}

		private bool _showNewItems = false;
		public bool ShowNewItems {
			get { return _showNewItems; }
			set {
				_showNewItems = value;
				RaisePropertyChanged(() => ShowNewItems);
			}
		}

		private bool _showChangedItems = false;
		public bool ShowChangedItems {
			get { return _showChangedItems; }
			set {
				_showChangedItems = value;
				RaisePropertyChanged(() => ShowChangedItems);
			}
		}

		private bool _showRemovedItems = false;
		public bool ShowRemovedItems {
			get { return _showRemovedItems; }
			set {
				_showRemovedItems = value;
				RaisePropertyChanged(() => ShowRemovedItems);
			}
		}

		public bool FilterItem(object item) {
			AbstractTreeItemViewModel treeItem = item as AbstractTreeItemViewModel;
			if (treeItem.FilteredChildren != null) {
				treeItem.FilteredChildren.Refresh();
			}

			if (treeItem is FieldTreeItemViewModel) {
				var field = treeItem as FieldTreeItemViewModel;

				bool? retBool = null;

				if (!string.IsNullOrWhiteSpace(NameFilter)) {
					var match = field.Name.ToLower().Contains(NameFilter.ToLower());
					retBool = retBool.HasValue ? retBool.Value && match : match;
				}

				if (!ShowOriginalItems && field.FilteredChildren != null) {
					retBool = (retBool.HasValue && retBool.Value) || field.FilteredChildren.Count > 0;
				}

				return retBool.HasValue ? retBool.Value : true;
			} else if (treeItem is CropZoneTreeItemViewModel) {
				var cropZone = treeItem as CropZoneTreeItemViewModel;

				bool? retBool = null;

				if (!string.IsNullOrWhiteSpace(NameFilter)) {
					var match = cropZone.Name.ToLower().Contains(NameFilter.ToLower());
					retBool = retBool.HasValue ? retBool.Value && match : match;
				}

				retBool = filterOnStatus(cropZone.Status, retBool);

				if (!ShowOriginalItems && cropZone.FilteredChildren != null) {
					retBool = (retBool.HasValue && retBool.Value) || cropZone.FilteredChildren.Count > 0;
				}

				return retBool.HasValue ? retBool.Value : true;
			} else if (treeItem is TagTreeItemViewModel) {
				var tag = treeItem as TagTreeItemViewModel;

				var retBool = filterOnStatus(tag.Status, null);

				return retBool.HasValue ? retBool.Value : true;
			} else if (ShowOriginalItems) {
				return true;
			} else {
				if (treeItem.FilteredChildren == null) {
					return false;
				} else {
					return treeItem.FilteredChildren.Count > 0;
				}
			}
		}

		public void ClearFilter() {
			// not really a clear. more of a reset
			NameFilter = string.Empty;
			ShowOriginalItems = false;
			ShowNewItems = true;
			ShowChangedItems = true;
			ShowRemovedItems = true;
		}

		public void BeforeFilter() {
			// do nothing
		}

		public void RefreshLists() { }

		private bool? filterOnStatus(MassAssignerStatus status, bool? currentBool) {
			switch (status) {
				case MassAssignerStatus.Unchanged:
					currentBool = currentBool.HasValue ? currentBool.Value && ShowOriginalItems : ShowOriginalItems;
					break;
				case MassAssignerStatus.Added:
					currentBool = currentBool.HasValue ? currentBool.Value && ShowNewItems : ShowNewItems;
					break;
				case MassAssignerStatus.Changed:
					currentBool = currentBool.HasValue ? currentBool.Value && ShowChangedItems : ShowChangedItems;
					break;
				case MassAssignerStatus.Removed:
					currentBool = currentBool.HasValue ? currentBool.Value && ShowRemovedItems : ShowRemovedItems;
					break;
				default:
					break;
			}

			return currentBool;
		}
	}
}
