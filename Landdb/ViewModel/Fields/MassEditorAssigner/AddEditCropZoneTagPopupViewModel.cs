﻿using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Tags;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Fields.MassEditorAssigner {
	public class AddEditCropZoneTagPopupViewModel : ValidationViewModelBase {

		private readonly IClientEndpoint clientEndpoint;
		private readonly Dispatcher dispatcher;

		private readonly CropYearId currentCropYearId;

		private readonly Action<AddEditCropZoneTagPopupViewModel> addEditComplete;

		public AddEditCropZoneTagPopupViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, CropYearId currentCropYearId, SelectionMode selectionMode, IEnumerable<CropZoneItemViewModel> selectedCropZones, IEnumerable<string> unsavedTags, Action<AddEditCropZoneTagPopupViewModel> onAddEditComplete) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			this.currentCropYearId = currentCropYearId;

			SelectedTags = new ObservableCollection<TagSelectionItem>();

			// validate the selected tags when its elements change
			SelectedTags.CollectionChanged += (sender, args) => {
				ValidateProperty(() => SelectedTags);
			};

			// add the first blank new tag, ready for the user to fill out if desired
			NewTags = new ObservableCollection<TagSelectionItem>() {
				new TagSelectionItem(string.Empty, false, SelectedTags, newTag => { onNewTagCheckedChanged(newTag); })
			};

			switch (selectionMode) {
				case SelectionMode.AddCropZones:
					initiateAddCropZonesMode(selectedCropZones);
					break;
				case SelectionMode.ChangeCropZones:
					initiateChangeCropZonesMode();
					break;
				case SelectionMode.AddTags:
					initiateAddTagsMode();
					break;
				case SelectionMode.ChangeTags:
					initiateChangeTagsMode(selectedCropZones);
					break;
				default:
					PopupTitle = Strings.BadSelectionMode_Text.ToLower();

					CropSelectionVisibility = Visibility.Collapsed;
					TagSelectionVisibility = Visibility.Collapsed;
					break;
			}

			ValidateViewModel();

			addEditComplete += onAddEditComplete;

			UncheckAllTagsCommand = new RelayCommand(onUncheckAllTags);

			CompleteCommand = new RelayCommand(onComplete);
			CancelCommand = new RelayCommand(onCancel);
		}

		public ICommand CompleteCommand { get; }
		public ICommand CancelCommand { get; }

		public ICommand UncheckAllTagsCommand { get; }

		public ObservableCollection<TagSelectionItem> NewTags { get; }

		[CustomValidation(typeof(AddEditCropZoneTagPopupViewModel), nameof(ValidateSelectedTags))]
		public ObservableCollection<TagSelectionItem> SelectedTags { get; }

		public string PopupTitle { get; private set; }

		public Visibility CropSelectionVisibility { get; private set; }
		public Visibility TagSelectionVisibility { get; private set; }

		public bool IsCropSelectionRequired { get; private set; }
		public bool IsTagSelectionRequired { get; private set; }

		public IEnumerable<CropZoneItemViewModel> ZeroAreaFields { get; private set; }

		private bool _isTagPopupOpen;
		public bool IsTagPopupOpen {
			get { return _isTagPopupOpen; }
			set {
				_isTagPopupOpen = value;
				RaisePropertyChanged(() => IsTagPopupOpen);
			}
		}

		// passively load crop list since it's not always needed.
		private List<MiniCrop> _cropList;
		public List<MiniCrop> CropList {
			get {
				if (_cropList == null) {
					_cropList = clientEndpoint.GetMasterlistService().GetCropList().ToList();
					_cropList.Insert(0, new MiniCrop());
				}

				return _cropList;
			}
		}

		public IEnumerable<string> GetTrulyNewTags() {
			return NewTags.Where(x => !string.IsNullOrWhiteSpace(x.Tag)).Except(ExistingTags).Select(x => x.Tag);
		}

		// passively load tag list since it's not always needed.
		private List<TagSelectionItem> _existingTags;
		public List<TagSelectionItem> ExistingTags {
			get {
				if (_existingTags == null) {
					_existingTags = clientEndpoint.GetView<TagListView>(currentCropYearId)
					.GetValue(new TagListView())
					.UsedTagsList
					.Select(x => new TagSelectionItem(x, true, SelectedTags, existingTag => { onExistingTagCheckedChanged(existingTag); }))
					.OrderBy(x => x.Tag)
					.ToList();
				}

				return _existingTags;
			}
		}

		private MiniCrop _selectedCrop;
		[CustomValidation(typeof(AddEditCropZoneTagPopupViewModel), nameof(ValidateSelectedCrop))]
		public MiniCrop SelectedCrop {
			get { return _selectedCrop; }
			set {
				_selectedCrop = value;
				ValidateAndRaisePropertyChanged(() => SelectedCrop);
			}
		}

		private void onExistingTagCheckedChanged(TagSelectionItem existingTag) {
			if (existingTag.IsChecked) {
				SelectedTags.Add(existingTag);
			} else {
				SelectedTags.Remove(existingTag);
			}
		}

		private void onNewTagCheckedChanged(TagSelectionItem newTag) {
			if (newTag.IsChecked) {
				// if this is truly a new tag, it will have an index of 0 because it is not yet in the list.
				// otherwise, we need to replace the existing tag
				if (newTag.Index == 0) {
					SelectedTags.Add(newTag);
				} else {
					var existingIndex = newTag.Index - 1;
					SelectedTags.RemoveAt(existingIndex);
					SelectedTags.Insert(existingIndex, newTag);
				}
			} else {
				SelectedTags.Remove(newTag);
			}

			// add a new blank tag, ready to be filled in.
			if (newTag.IsChecked && !string.IsNullOrWhiteSpace(NewTags.Last().Tag)) {
				NewTags.Add(new TagSelectionItem(string.Empty, false, SelectedTags, nt => onNewTagCheckedChanged(nt)));
			}
		}

		private void initiateAddCropZonesMode(IEnumerable<CropZoneItemViewModel> selectedCropZones) {
			PopupTitle = Strings.AddCropzones_Text.ToLower();

			CropSelectionVisibility = Visibility.Visible;
			TagSelectionVisibility = Visibility.Visible;

			IsCropSelectionRequired = true;
			IsTagSelectionRequired = false;

			if (selectedCropZones != null) {
				ZeroAreaFields = from cz in selectedCropZones
								 where cz.BoundaryArea == null
									 || cz.BoundaryArea.Value == 0
								 orderby cz.FarmName, cz.FieldName
								 select cz;

				RaisePropertyChanged(() => ZeroAreaFields);
			}
		}

		private void initiateChangeCropZonesMode() {
			PopupTitle = Strings.ChangeCropzones_Text.ToLower();

			CropSelectionVisibility = Visibility.Visible;
			TagSelectionVisibility = Visibility.Collapsed;

			IsCropSelectionRequired = true;
			IsTagSelectionRequired = false;
		}

		private void initiateAddTagsMode() {
			PopupTitle = Strings.AddTags_Text.ToLower();

			CropSelectionVisibility = Visibility.Collapsed;
			TagSelectionVisibility = Visibility.Visible;

			IsCropSelectionRequired = false;
			IsTagSelectionRequired = true;
		}

		private void initiateChangeTagsMode(IEnumerable<CropZoneItemViewModel> selectedCropZones) {
			PopupTitle = Strings.ChangeTags_Text.ToLower();

			CropSelectionVisibility = Visibility.Collapsed;
			TagSelectionVisibility = Visibility.Visible;

			IsCropSelectionRequired = false;
			IsTagSelectionRequired = true;

			var tagGroups = from cz in selectedCropZones
							group cz by string.Join(", ", cz.Tags) into g
							select g.Key;

			if (tagGroups.Count() == 1) {
				var cz = selectedCropZones.First();

				foreach (var tag in cz.Tags) {
					var existingTag = ExistingTags.SingleOrDefault(x => x.Tag == tag);

					if (existingTag != null) {
						existingTag.IsChecked = true;
					}
				}
			}
		}

		private void onUncheckAllTags() {
			ExistingTags.ForEach(x => x.IsChecked = false);
			NewTags.ForEach(x => x.IsChecked = false);
		}

		private void onComplete() {
			if (ValidateViewModel()) {
				addEditComplete(this);
			}
		}

		private void onCancel() {
			addEditComplete(null);
		}

		public static ValidationResult ValidateSelectedCrop(MiniCrop selectedCrop, ValidationContext context) {
			ValidationResult result = null;

			var vm = (AddEditCropZoneTagPopupViewModel)context.ObjectInstance;

			if (vm.IsCropSelectionRequired && selectedCrop == null) {
				result = new ValidationResult(Strings.CropIsRequired_Text);
			}

			return result;
		}

		public static ValidationResult ValidateSelectedTags(IEnumerable<TagSelectionItem> selectedTags, ValidationContext context) {
			ValidationResult result = null;

			var vm = (AddEditCropZoneTagPopupViewModel)context.ObjectInstance;

			if (vm.IsTagSelectionRequired && !selectedTags.Any()) {
				result = new ValidationResult(Strings.PleaseSelectAtLeastOneTag_text);
			}

			return result;
		}

		public enum SelectionMode {
			AddCropZones,
			ChangeCropZones,
			AddTags,
			ChangeTags
		}
	}
}
