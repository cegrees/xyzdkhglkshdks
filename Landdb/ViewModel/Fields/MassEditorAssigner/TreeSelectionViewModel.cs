﻿using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Client.Spatial;
using Landdb.Domain.ReadModels.Map;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Microsoft.Maps.MapControl.WPF;
using NLog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Fields.MassEditorAssigner {

	public class TreeSelectionViewModel : ValidationViewModelBase {

		public event EventHandler MapUpdated;
		public EventArgs e = null;

		private readonly IClientEndpoint clientEndpoint;
		private readonly Dispatcher dispatcher;

		private readonly CropYearId currentCropYearId;
		private readonly bool sortAlphabetically;
		private readonly Logger log = LogManager.GetCurrentClassLogger();

		private CollectionView originalTreeItemModelView;
		private CollectionView modifiedTreeItemModelView;

		public TreeSelectionViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, CropYearId currentCropYearId, MassEditorAssignerViewModel parent) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			this.currentCropYearId = currentCropYearId;

			sortAlphabetically = clientEndpoint.GetUserSettings().AreFieldsSortedAlphabetically;

			OriginalTreeFilterModel = new TreeFilterModel(clientEndpoint);

			ModifiedTreeFilterModel = new ModifiedTreeFilterModel(clientEndpoint) {
				ShowOriginalItems = false,
				ShowNewItems = true,
				ShowChangedItems = true,
				ShowRemovedItems = true,
			};

			buildTreeList();

			CheckedCropZonesToAdd = new ObservableCollection<CropZoneItemViewModel>();
			CheckedCropZonesToChange = new ObservableCollection<CropZoneItemViewModel>();

			CheckedCropZonesToAdd.CollectionChanged += (sender, args) => {
				RaisePropertyChanged(() => CheckedFieldCountDisplayText);
			};

			CheckedCropZonesToChange.CollectionChanged += (sender, args) => {
				RaisePropertyChanged(() => CheckedCropZoneCountDisplayText);
			};

			ApplyOriginalTreeFilterCommand = new RelayCommand(onApplyOriginalTreeFilter);
			ClearOriginalTreeFilterCommand = new RelayCommand(onClearOriginalTreeFilter);

			ApplyModifiedTreeFilterCommand = new RelayCommand(onApplyModifiedTreeFilter);
			ClearModifiedTreeFilterCommand = new RelayCommand(onClearModifiedTreeFilter);
		}

		#region Properties
		public ICommand ApplyOriginalTreeFilterCommand { get; }
		public ICommand ClearOriginalTreeFilterCommand { get; }

		public ICommand ApplyModifiedTreeFilterCommand { get; }
		public ICommand ClearModifiedTreeFilterCommand { get; }

		public IPageFilterViewModel OriginalTreeFilterModel { get; }
		public IPageFilterViewModel ModifiedTreeFilterModel { get; }

		public ObservableCollection<CropZoneItemViewModel> CheckedCropZonesToAdd { get; }
		public ObservableCollection<CropZoneItemViewModel> CheckedCropZonesToChange { get; }

		public ObservableCollection<AbstractTreeItemViewModel> OriginalTreeItemModels { get; private set; }
		public ObservableCollection<AbstractTreeItemViewModel> ModifiedTreeItemModels { get; private set; }

		public MapLayer BingMapsLayer { get; private set; }
		public MapLayer LabelsLayer { get; private set; }

        public string MapCulture
        {
            get { return Infrastructure.ApplicationEnvironment.BingWpfMapCulture; }
        }

        // used by view
        public List<TreeExpansionDepth> TreeExpansionOptions {
			get { return Enum.GetValues(typeof(TreeExpansionDepth)).Cast<TreeExpansionDepth>().ToList(); }
		}

		public string CheckedFieldCountDisplayText {
			get { return string.Format(Strings.Fields_Format_Text, CheckedCropZonesToAdd.Count, CheckedCropZonesToAdd.Count != 1 ? "s" : string.Empty); }
		}

		public string CheckedCropZoneCountDisplayText {
			get { return string.Format(Strings.Cropzone_Format_Text, CheckedCropZonesToChange.Count, CheckedCropZonesToChange.Count != 1 ? "s" : string.Empty); }
		}

		public string CheckedFieldAreaDisplayText {
			get { return string.Format("{0:N2} {1}s", CheckedFieldArea, ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit); }
		}

		public string CheckedCropZoneAreaDisplayText {
			get { return string.Format("{0:N2} {1}s", CheckedCropZoneArea, ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit); }
		}

		private double _checkedCzArea;
		public double CheckedCropZoneArea {
			get { return _checkedCzArea; }
			private set {
				_checkedCzArea = value;
				RaisePropertyChanged(() => CheckedCropZoneArea);
				RaisePropertyChanged(() => CheckedCropZoneAreaDisplayText);
			}
		}

		private double _checkedFldArea;
		public double CheckedFieldArea {
			get { return _checkedFldArea; }
			private set {
				_checkedFldArea = value;
				RaisePropertyChanged(() => CheckedFieldArea);
				RaisePropertyChanged(() => CheckedFieldAreaDisplayText);
			}
		}

		// both trees are bound to this. effectively disables non-checkbox selection within the tree
		private AbstractTreeItemViewModel _selectedTreeItem;
		public AbstractTreeItemViewModel SelectedTreeItem {
			get { return _selectedTreeItem; }
			set {
				if (value != null) { value.IsSelected = false; }

				_selectedTreeItem = null;
				RaisePropertyChanged(() => SelectedTreeItem);
			}
		}

		private TreeExpansionDepth _selectedTreeDepth;
		public TreeExpansionDepth SelectedTreeDepth {
			get { return _selectedTreeDepth; }
			set {
				_selectedTreeDepth = value;

				var depth = (int)value;

				OriginalTreeItemModels[0].IsExpanded = true;

				foreach (FarmTreeItemViewModel farm in OriginalTreeItemModels[0].Children) {
					farm.IsExpanded = depth > 0;

					foreach (FieldTreeItemViewModel field in farm.Children) {
						field.IsExpanded = depth > 1;

						foreach (CropZoneTreeItemViewModel cz in field.Children) {
							cz.IsExpanded = depth > 2;

							foreach (TagTreeItemViewModel tag in cz.Children) {
								tag.IsExpanded = depth > 3;
							}
						}
					}
				}

				ModifiedTreeItemModels[0].IsExpanded = true;

				foreach (FarmTreeItemViewModel farm in ModifiedTreeItemModels[0].Children) {
					farm.IsExpanded = depth > 0;

					foreach (FieldTreeItemViewModel field in farm.Children) {
						field.IsExpanded = depth > 1;

						foreach (CropZoneTreeItemViewModel cz in field.Children) {
							cz.IsExpanded = depth > 2;

							foreach (TagTreeItemViewModel tag in cz.Children) {
								tag.IsExpanded = depth > 3;
							}
						}
					}
				}

				refreshMap();

				RaisePropertyChanged(() => SelectedTreeDepth);
			}
		}

		private string _originalTreeFilterHeader;
		public string OriginalTreeFilterHeader {
			get { return _originalTreeFilterHeader; }
			set {
				_originalTreeFilterHeader = value;
				RaisePropertyChanged(() => OriginalTreeFilterHeader);
			}
		}

		private string _modifiedTreeFilterHeader;
		public string ModifiedTreeFilterHeader {
			get { return _modifiedTreeFilterHeader; }
			set {
				_modifiedTreeFilterHeader = value;
				RaisePropertyChanged(() => ModifiedTreeFilterHeader);
			}
		}

		private bool _isOriginalTreeFilterPopupOpen;
		public bool IsOriginalTreeFilterPopupOpen {
			get { return _isOriginalTreeFilterPopupOpen; }
			set {
				_isOriginalTreeFilterPopupOpen = value;
				RaisePropertyChanged(() => IsOriginalTreeFilterPopupOpen);
			}
		}

		private bool _isModifiedTreeFilterPopupOpen;
		public bool IsModifiedTreeFilterPopupOpen {
			get { return _isModifiedTreeFilterPopupOpen; }
			set {
				_isModifiedTreeFilterPopupOpen = value;
				RaisePropertyChanged(() => IsModifiedTreeFilterPopupOpen);
			}
		}
		#endregion

		#region Methods
		#region Modified Tree Manipulation Methods
		public void AddCropZoneToModifiedTree(FieldId parentFieldId, TreeViewCropZoneItem czTreeItem) {
			var destField = (from FarmTreeItemViewModel farm in ModifiedTreeItemModels[0].Children
							 from FieldTreeItemViewModel field in farm.Children
							 where field.FieldId == parentFieldId
							 select field).SingleOrDefault();

			var newCZ = new CropZoneTreeItemViewModel(czTreeItem, destField, sortAlphabetically, null, null, ModifiedTreeFilterModel.FilterItem, false);
			newCZ.UpdateStatus(MassAssignerStatus.Added);

			// hoooooly crap. just learned how to use Func<>  :D
			newCZ.IsThisTreeItemValid += cz => { return isNewCropZoneValid(cz as CropZoneTreeItemViewModel); };
			newCZ.ValidateViewModel();

			destField.Children.Add(newCZ);
			destField.IsExpanded = true;

			// expand labels, if any
			destField.Children.ForEach(x => x.IsExpanded = true);

			refreshModifiedTree();
		}

		public void UpdateCropZoneInModifiedTree(CropZoneId cropZoneId, string newName, CropId newCropId, string newCropName) {
			var czToUpdate = (from FarmTreeItemViewModel farm in ModifiedTreeItemModels[0].Children
							  from FieldTreeItemViewModel field in farm.Children
							  from CropZoneTreeItemViewModel cz in field.Children
							  where cz.CropZoneId == cropZoneId
							  select cz).SingleOrDefault();

			czToUpdate.UpdateCrop(newCropId, newCropName);
			czToUpdate.UpdateName(newName);
			czToUpdate.UpdateStatus(MassAssignerStatus.Changed);
			czToUpdate.IsExpanded = true;

			// expand labels, if any
			czToUpdate.Children.ForEach(x => x.IsExpanded = true);

			refreshModifiedTree();
		}

		public void FlagCropZoneRemovedInModifiedTree(CropZoneId cropZoneId) {
			var czToRemove = (from FarmTreeItemViewModel farm in ModifiedTreeItemModels[0].Children
							  from FieldTreeItemViewModel field in farm.Children
							  from CropZoneTreeItemViewModel cz in field.Children
							  where cz.CropZoneId == cropZoneId
							  select cz).SingleOrDefault();

			if (czToRemove != null) {
				czToRemove.UpdateStatus(MassAssignerStatus.Removed);
				czToRemove.IsExpanded = true;
			}

			refreshModifiedTree();
		}

		public void UpdateCropZoneTagsInTree(CropZoneId cropZoneId, List<string> tags) {
			var czToUpdate = (from FarmTreeItemViewModel farm in ModifiedTreeItemModels[0].Children
							  from FieldTreeItemViewModel field in farm.Children
							  from CropZoneTreeItemViewModel cz in field.Children
							  where cz.CropZoneId == cropZoneId
							  select cz).SingleOrDefault();

			if (czToUpdate != null) {
				czToUpdate.UpdateTags(tags);
				czToUpdate.IsExpanded = true;
				czToUpdate.Children.ForEach(x => x.IsExpanded = true);
			}

			refreshModifiedTree();
		}

		public void RevertCropZoneModification(CropZoneTreeItemViewModel cropZone) {
			switch (cropZone.Status) {
				case MassAssignerStatus.Unchanged:
					log.Error("Attempted to revert an unchanged cropzone with id: [{0}]", cropZone.CropZoneId);
					break;
				case MassAssignerStatus.Added:
					var parentField = (from FarmTreeItemViewModel farm in ModifiedTreeItemModels[0].Children
									   from FieldTreeItemViewModel field in farm.Children
									   where field.FieldId == cropZone.Parent.Id as FieldId
									   select field).SingleOrDefault();

					parentField.Children.Remove(cropZone);
					break;
				case MassAssignerStatus.Changed:
					var originalCropZone = (from FarmTreeItemViewModel farm in OriginalTreeItemModels[0].Children
											from FieldTreeItemViewModel field in farm.Children
											from CropZoneTreeItemViewModel cz in field.Children
											where cz.CropZoneId == cropZone.CropZoneId
											select cz).SingleOrDefault();

					cropZone.UpdateName(originalCropZone.Name);
					cropZone.UpdateCrop(originalCropZone.CropId, originalCropZone.CropName);
					cropZone.UpdateArea(originalCropZone.Area);
					cropZone.UpdateStatus(MassAssignerStatus.Unchanged);
					break;
				case MassAssignerStatus.Removed:
					cropZone.UpdateStatus(MassAssignerStatus.Unchanged);
					break;
				default:
					log.Error("Unhandled cropzone undo status: [{0}]", cropZone.Status);
					break;
			}

			refreshModifiedTree();

			Messenger.Default.Send(new HidePopupMessage());
		}

		public void RevertTagModification(TagTreeItemViewModel tag) {
			var parentCropZone = tag.Parent as CropZoneTreeItemViewModel;

			switch (tag.Status) {
				case MassAssignerStatus.Unchanged:
					log.Error("Attempted to revert an unchanged cropzone with id: [{0}]", parentCropZone.CropZoneId);
					break;
				case MassAssignerStatus.Added:					
					parentCropZone.UpdateTags(new List<string>());
					refreshModifiedTree();
					break;
				case MassAssignerStatus.Changed:
				case MassAssignerStatus.Removed:
					parentCropZone.UpdateTags(parentCropZone.OriginalTags);
					refreshModifiedTree();
					break;
				default:
					log.Error("Unhandled tag undo status: [{0}]", tag.Status);
					break;
			}
		}
		#endregion

		public void ClearSelections() {
			// clear selection lists
			CheckedCropZonesToAdd.Clear();
			CheckedCropZonesToChange.Clear();

			// uncheck everything
			OriginalTreeItemModels[0].Children.ForEach(x => x.IsChecked = false);
			ModifiedTreeItemModels[0].Children.ForEach(x => x.IsChecked = false);

			CheckedFieldArea = 0;
			CheckedCropZoneArea = 0;
		}

		public override void Cleanup() {
			base.Cleanup();
			OriginalTreeFilterModel.Cleanup();
			ModifiedTreeFilterModel.Cleanup();
		}

		private void buildTreeList() {
			var stopWatch = Stopwatch.StartNew();

			AbstractTreeItemViewModel rootNode = null;

			// TODO: When maps are separate, move this back to the Farms case in the switch below.
			var years = clientEndpoint.GetView<FieldTreeView>(new DataSourceId(currentCropYearId.DataSourceId));
			var yearItem = (years.HasValue && years.Value.CropYearList.ContainsKey(currentCropYearId.Id)) ? years.Value.CropYearList[currentCropYearId.Id] : new TreeViewYearItem();

			var farms = yearItem.Farms != null ? yearItem.Farms : new List<TreeViewFarmItem>();
			rootNode = new GrowerTreeItemViewModel("Original Tree", farms, sortAlphabetically, onTreeItemCheckedChanged, null, OriginalTreeFilterModel.FilterItem);

			dispatcher.BeginInvoke(new Action(() => {
				// initialize map
				var mapQ = from m in yearItem.Maps
						   where m.IsVisible
						   select m.MapData;

				BingMapsLayer = BingMapsUtility.GetLayerForMapData(mapQ);
				RaisePropertyChanged(() => BingMapsLayer);
			}));

			if (rootNode != null) {
				OriginalTreeItemModels = new ObservableCollection<AbstractTreeItemViewModel>() { rootNode };
				RaisePropertyChanged(() => OriginalTreeItemModels);

				ModifiedTreeItemModels = new ObservableCollection<AbstractTreeItemViewModel>() {
					new GrowerTreeItemViewModel("Modified Tree", farms, sortAlphabetically, null, null, ModifiedTreeFilterModel.FilterItem, false)
				};

				RaisePropertyChanged(() => ModifiedTreeItemModels);

				dispatcher.BeginInvoke(new Action(() => {
					originalTreeItemModelView = (CollectionView)CollectionViewSource.GetDefaultView(OriginalTreeItemModels);
					originalTreeItemModelView.Filter = OriginalTreeFilterModel.FilterItem;
					generateOriginalTreeFilterHeader();

					modifiedTreeItemModelView = (CollectionView)CollectionViewSource.GetDefaultView(ModifiedTreeItemModels);
					modifiedTreeItemModelView.Filter = ModifiedTreeFilterModel.FilterItem;
					generateModifiedTreeFilterHeader();
				}));
			}

			log.Debug("Mass Editor / Assigner tree load took {0} ms", stopWatch.ElapsedMilliseconds);

			SelectedTreeDepth = TreeExpansionDepth.Fields;
		}

		private ValidationResult isNewCropZoneValid(CropZoneTreeItemViewModel cropZone) {
			ValidationResult result = null;

			if (cropZone.Status == MassAssignerStatus.Added && cropZone.Area.Value <= 0) {
				result = new ValidationResult(Strings.AreaMustBeGreaterThanZero_Text);
			}

			return result;
		}

		#region Checked Changed Handling
		private void onTreeItemCheckedChanged(AbstractTreeItemViewModel treeItem) {
			if (treeItem is FieldTreeItemViewModel) {
				var field = treeItem as FieldTreeItemViewModel;
				addNewCropZoneToChecked(field);
				field.Children.ForEach(x => addExistingCropZoneToChecked(x as CropZoneTreeItemViewModel));
			} else if (treeItem is CropZoneTreeItemViewModel) {
				addExistingCropZoneToChecked(treeItem as CropZoneTreeItemViewModel);
			}
		}

		private void addNewCropZoneToChecked(FieldTreeItemViewModel field) {
			// create a cropzone for the selected field and add it to CheckedCropZones
			if (field.IsChecked.HasValue && field.IsChecked.Value) {
				var mapItem = clientEndpoint.GetView<ItemMap>(field.Id);
				string mapData = null;

				if (mapItem.HasValue && mapItem.Value.MostRecentMapItem != null) {
					mapData = mapItem.Value.MostRecentMapItem.MapData;
				}

				var fieldDetails = clientEndpoint.GetView<FieldDetailsView>(field.Id).GetValue(() => null);

				double? boundaryArea = null;
				double? reportedArea = null;

				if (fieldDetails != null) {
					boundaryArea = fieldDetails.BoundaryArea;
					reportedArea = fieldDetails.ReportedArea;
				}

				var newCropZoneItem = new CropZoneItemViewModel(clientEndpoint, field, boundaryArea, reportedArea, mapData);

				CheckedCropZonesToAdd.Add(newCropZoneItem);
				CheckedFieldArea += newCropZoneItem.SelectedArea != null ? newCropZoneItem.SelectedArea.Value : 0;

				refreshMap();
			}

			if (field.IsChecked.HasValue && !field.IsChecked.Value) {
				var czsRemoveList = CheckedCropZonesToAdd.Where(x => x.FieldId == field.FieldId).ToList();

				foreach (var cz in czsRemoveList) {
					CheckedCropZonesToAdd.Remove(cz);
					CheckedFieldArea -= cz.SelectedArea != null ? cz.SelectedArea.Value : 0;
				}

				refreshMap();
			}
		}

		private void addExistingCropZoneToChecked(CropZoneTreeItemViewModel cropZone) {
			if (cropZone.IsChecked.HasValue && cropZone.IsChecked.Value && !CheckedCropZonesToChange.Any(x => x.Id == cropZone.CropZoneId)) {
				var mapItem = clientEndpoint.GetView<ItemMap>(cropZone.Id);
				string mapData = null;

				if (mapItem.HasValue && mapItem.Value.MostRecentMapItem != null) {
					mapData = mapItem.Value.MostRecentMapItem.MapData;
				}

				var czDetails = clientEndpoint.GetView<CropZoneDetailsView>(cropZone.CropZoneId).GetValue(() => null);

				var newCropZoneItem = new CropZoneItemViewModel(clientEndpoint, cropZone, czDetails.BoundaryArea, czDetails.ReportedArea, mapData);

				CheckedCropZonesToChange.Add(newCropZoneItem);
				CheckedCropZoneArea += newCropZoneItem.SelectedArea != null ? newCropZoneItem.SelectedArea.Value : 0;

				refreshMap();
			}

			if (cropZone.IsChecked.HasValue && !cropZone.IsChecked.Value) {
				CheckedCropZonesToChange.Where(x => x.Id == (CropZoneId)cropZone.Id).ToList().ForEach(x => {
					CheckedCropZonesToChange.Remove(x);
					CheckedCropZoneArea -= x.SelectedArea != null ? x.SelectedArea.Value : 0;

					refreshMap();
				});
			}
		}
		#endregion

		#region Refresh Methods
		private void refreshModifiedTree() {
			dispatcher.BeginInvoke(new Action(() => {
				modifiedTreeItemModelView = (CollectionView)CollectionViewSource.GetDefaultView(ModifiedTreeItemModels);
				modifiedTreeItemModelView.Filter = ModifiedTreeFilterModel.FilterItem;
				generateModifiedTreeFilterHeader();
			}));
		}

		private void refreshMap() {
			dispatcher.BeginInvoke(new Action(() => {
				if (MapUpdated == null) { return; }

				var selectedMapData = new List<string>();
				LabelsLayer = new MapLayer();

				IEnumerable<CropZoneItemViewModel> checkedCropZones = null;

				switch (SelectedTreeDepth) {
					case TreeExpansionDepth.Farms:
					case TreeExpansionDepth.Fields:
					default:
						checkedCropZones = CheckedCropZonesToAdd;
						break;
					case TreeExpansionDepth.CropZones:
					case TreeExpansionDepth.Tags:
						checkedCropZones = CheckedCropZonesToChange;
						break;
				}

				foreach (var sCZ in checkedCropZones) {
					if (sCZ.MapData != null) {
						selectedMapData.Add(sCZ.MapData);

						// get bounding box for each shape...find center and add label
						var holder = new List<string>();
						holder.Add(sCZ.MapData);
						var shapeLayer = BingMapsUtility.GetLayerForMapData(holder);
						var bounding = BingMapsUtility.GetBoundingBox(shapeLayer);
						var customLabel = new System.Windows.Controls.Label();
						customLabel.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Center;
						customLabel.VerticalContentAlignment = System.Windows.VerticalAlignment.Center;
						customLabel.FontWeight = System.Windows.FontWeights.DemiBold;

						customLabel.Content = sCZ.FieldName;
						LabelsLayer.AddChild(customLabel, bounding);
					}
				}

				BingMapsLayer = BingMapsUtility.GetLayerForMapData(selectedMapData.ToArray());
				RaisePropertyChanged(() => BingMapsLayer);

				MapUpdated(this, e);
			}));
		}
		#endregion

		#region Filter Logic
		private void onApplyOriginalTreeFilter() {
			if (OriginalTreeFilterModel != null && originalTreeItemModelView != null) {
				OriginalTreeFilterModel.BeforeFilter();
				originalTreeItemModelView.Refresh();
				generateOriginalTreeFilterHeader();
				IsOriginalTreeFilterPopupOpen = false;
			}
		}

		private void onClearOriginalTreeFilter() {
			if (OriginalTreeFilterModel != null && originalTreeItemModelView != null) {
				OriginalTreeFilterModel.ClearFilter();
				OriginalTreeFilterModel.BeforeFilter();
				originalTreeItemModelView.Refresh();
				generateOriginalTreeFilterHeader();
				IsOriginalTreeFilterPopupOpen = false;
			}
		}

		private void onApplyModifiedTreeFilter() {
			if (ModifiedTreeFilterModel != null && modifiedTreeItemModelView != null) {
				ModifiedTreeFilterModel.BeforeFilter();
				modifiedTreeItemModelView.Refresh();
				generateModifiedTreeFilterHeader();
				IsModifiedTreeFilterPopupOpen = false;
			}
		}

		private void onClearModifiedTreeFilter() {
			if (ModifiedTreeFilterModel != null && modifiedTreeItemModelView != null) {
				ModifiedTreeFilterModel.ClearFilter();
				ModifiedTreeFilterModel.BeforeFilter();
				modifiedTreeItemModelView.Refresh();
				generateModifiedTreeFilterHeader();
				IsModifiedTreeFilterPopupOpen = false;
			}
		}

		private void generateOriginalTreeFilterHeader() {
			var originalCount = (from r in OriginalTreeItemModels
								 from fa in r.Children
								 select fa.Children.Count).Sum();

			var filteredCount = (from r in OriginalTreeItemModels
								 from fa in r.Children
								 select fa.FilteredChildren.Count).Sum();

			string entityPluralityAwareName = originalCount == 1 ? Strings.Field_Text : Strings.Fields_Text;

			if (originalCount != filteredCount) {
				OriginalTreeFilterHeader = String.Format(Strings.FilteredCountOfOriginalCount_Text, filteredCount, originalCount, entityPluralityAwareName);
			} else {
				OriginalTreeFilterHeader = Strings.AllFields_Text;
			}
		}

		private void generateModifiedTreeFilterHeader() {
			var originalCount = (from r in ModifiedTreeItemModels
								 from fa in r.Children
								 select fa.Children.Count).Sum();

			var filteredCount = (from r in ModifiedTreeItemModels
								 from fa in r.Children
								 select fa.FilteredChildren.Count).Sum();

			string entityPluralityAwareName = originalCount == 1 ? Strings.Field_Text : Strings.Fields_Text;

			if (originalCount != filteredCount) {
				ModifiedTreeFilterHeader = String.Format(Strings.FilteredCountOfOriginalCount_Text, filteredCount, originalCount, entityPluralityAwareName);
			} else {
				ModifiedTreeFilterHeader = Strings.AllFields_Text;
			}
		}
		#endregion

		public enum TreeExpansionDepth {
			Farms,
			Fields,
			CropZones,
			Tags
		}
		#endregion
	}
}
