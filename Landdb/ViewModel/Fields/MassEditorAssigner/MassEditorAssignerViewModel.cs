﻿using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Map;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Fields.MassEditorAssigner {
	public class MassEditorAssignerViewModel : ValidationViewModelBase {

		private readonly IClientEndpoint clientEndpoint;
		private readonly Dispatcher dispatcher;

		private readonly CropYearId currentCropYearId;

		private readonly Action<MassEditorAssignerViewModel> finalize;

		private readonly Logger log = LogManager.GetCurrentClassLogger();

		public MassEditorAssignerViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, CropYearId currentCropYearId, Action<MassEditorAssignerViewModel> onFinalize) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			this.currentCropYearId = currentCropYearId;

			finalize += onFinalize;

			TreeModel = new TreeSelectionViewModel(clientEndpoint, dispatcher, currentCropYearId, this);

			UnsavedTags = new List<string>();

			ShowCropZoneOpsCommand = new RelayCommand(() => {
				if (IsTagOpsPopupOpen) { IsTagOpsPopupOpen = false; }
				IsCropZoneOpsPopupOpen = !IsCropZoneOpsPopupOpen;
			});

			ShowTagOpsCommand = new RelayCommand(() => {
				if (IsCropZoneOpsPopupOpen) { IsCropZoneOpsPopupOpen = false; }
				IsTagOpsPopupOpen = !IsTagOpsPopupOpen;
			});

			AddCropZoneCommand = new RelayCommand(onAddCrop);
			ChangeCropZoneCommand = new RelayCommand(onChangeCrop);
			RemoveCropZoneCommand = new RelayCommand(onRemoveCrop);

			AddTagsCommand = new RelayCommand(onAddTags);
			ChangeTagsCommand = new RelayCommand(onChangeTags);
			RemoveTagsCommand = new RelayCommand(onRemoveTags);

			EditTreeItemDetailsCommand = new RelayCommand<AbstractTreeItemViewModel>(onEditTreeItemDetails);
			RevertModificationCommand = new RelayCommand<AbstractTreeItemViewModel>(onRevertModification);

			ShowSummaryCommand = new RelayCommand(onShowSummary);

			CompleteCommand = new RelayCommand(onComplete);
			CancelCommand = new RelayCommand(onCancel);
		}

		#region Properties
		public ICommand ShowCropZoneOpsCommand { get; }
		public ICommand ShowTagOpsCommand { get; }

		public ICommand AddCropZoneCommand { get; }
		public ICommand ChangeCropZoneCommand { get; }
		public ICommand RemoveCropZoneCommand { get; }

		public ICommand AddTagsCommand { get; }
		public ICommand ChangeTagsCommand { get; }
		public ICommand RemoveTagsCommand { get; }

		public ICommand EditTreeItemDetailsCommand { get; }
		public ICommand RevertModificationCommand { get; }

		public ICommand ShowSummaryCommand { get; }

		public ICommand CompleteCommand { get; }
		public ICommand CancelCommand { get; }

		[CustomValidation(typeof(MassEditorAssignerViewModel), nameof(ValidateTreeModel))]
		public TreeSelectionViewModel TreeModel { get; }

		public List<string> UnsavedTags { get; }

		public bool IsFinalizationAllowed => ChangedCropZones.Any() && !HasErrors;

		public List<CropZoneTreeItemViewModel> ChangedCropZones {
			get {
				return (from FarmTreeItemViewModel farm in TreeModel.ModifiedTreeItemModels[0].Children
						from FieldTreeItemViewModel field in farm.Children
						from CropZoneTreeItemViewModel cz in field.Children
						let tagItem = cz.Children.FirstOrDefault() as TagTreeItemViewModel
						where cz.Status != MassAssignerStatus.Unchanged || (tagItem != null && tagItem.Status != MassAssignerStatus.Unchanged)
						select cz).ToList();
			}
		}

		// for display in view
		public int CropYear => currentCropYearId.Id;

		private bool _isCropZoneOpsPopupOpen;
		public bool IsCropZoneOpsPopupOpen {
			get { return _isCropZoneOpsPopupOpen; }
			set {
				_isCropZoneOpsPopupOpen = value;
				RaisePropertyChanged(() => IsCropZoneOpsPopupOpen);
			}
		}

		private bool _isTagOpsPopupOpen;
		public bool IsTagOpsPopupOpen {
			get { return _isTagOpsPopupOpen; }
			set {
				_isTagOpsPopupOpen = value;
				RaisePropertyChanged(() => IsTagOpsPopupOpen);
			}
		}

		private string _flyoutText;
		public string FlyoutText {
			get { return _flyoutText; }
			set {
				_flyoutText = value;
				RaisePropertyChanged(() => FlyoutText);
			}
		}

		private bool _isFlyoutTextVisible;
		public bool IsFlyoutTextVisible {
			get { return _isFlyoutTextVisible; }
			set {
				_isFlyoutTextVisible = value;
				RaisePropertyChanged(() => IsFlyoutTextVisible);
			}
		}
		#endregion

		public List<IDomainCommand> GetDomainCommands() {
			var cmdList = new List<IDomainCommand>();

			foreach (var cz in ChangedCropZones) {
				switch (cz.Status) {
					case MassAssignerStatus.Unchanged:
						if (!cz.Tags.SequenceEqual(cz.OriginalTags)) {
							cmdList.Add(new UntagCropZone(
								cz.CropZoneId,
								clientEndpoint.GenerateNewMetadata(),
								cz.OriginalTags.ToArray(),
								currentCropYearId.Id
							));

							if (cz.Tags.Any()) {
								cmdList.Add(new TagCropZone(
									cz.CropZoneId,
									clientEndpoint.GenerateNewMetadata(),
									cz.Tags.ToArray(),
									currentCropYearId.Id
								));
							}
						}
						break;
					case MassAssignerStatus.Added:
						cmdList.Add(new CreateAnnualCropZone(
							cz.CropZoneId,
							clientEndpoint.GenerateNewMetadata(),
							cz.Name,
							(cz.Parent as FieldTreeItemViewModel).FieldId,
							cz.CropId,
							currentCropYearId.Id,
							cz.Area.Value,
							cz.Area.Unit.Name
						));

						if (cz.Tags.Any()) {
							cmdList.Add(new TagCropZone(
								cz.CropZoneId,
								clientEndpoint.GenerateNewMetadata(),
								cz.Tags.ToArray(),
								currentCropYearId.Id
							));
						}
						break;
					case MassAssignerStatus.Changed:
						var details = clientEndpoint.GetView<CropZoneDetailsView>(cz.Id).GetValue(() => null);
						if (details != null) {
							cmdList.Add(new UpdateCropZoneCropInformation(
								cz.CropZoneId,
								clientEndpoint.GenerateNewMetadata(),
								currentCropYearId.Id,
								details.SeedLotId,
								details.SeedSource,
								cz.CropId,
								details.InsuranceId
							));

							if (cz.Name != details.Name) {
								cmdList.Add(new RenameCropZone(
									cz.CropZoneId,
									clientEndpoint.GenerateNewMetadata(),
									cz.Name,
									currentCropYearId.Id,
									details.CustomIntegrationId
								));
							}

							if (cz.Area.Value != details.ReportedArea && cz.Area.Value != details.BoundaryArea) {
								cmdList.Add(new UpdateCropZoneReportedArea(
									cz.CropZoneId,
									clientEndpoint.GenerateNewMetadata(),
									cz.Area.Value,
									cz.Area.Unit.Name,
									currentCropYearId.Id
								));
							}

							if (!cz.Tags.SequenceEqual(cz.OriginalTags)) {
								cmdList.Add(new UntagCropZone(
									cz.CropZoneId,
									clientEndpoint.GenerateNewMetadata(),
									cz.OriginalTags.ToArray(),
									currentCropYearId.Id
								));

								if (cz.Tags.Any()) {
									cmdList.Add(new TagCropZone(
										cz.CropZoneId,
										clientEndpoint.GenerateNewMetadata(),
										cz.Tags.ToArray(),
										currentCropYearId.Id
									));
								}
							}
						}
						break;
					case MassAssignerStatus.Removed:
						cmdList.Add(new RemoveAnnualCropZone(cz.CropZoneId, clientEndpoint.GenerateNewMetadata(), currentCropYearId.Id));
						break;
					default:
						break;
				}
			}

			return cmdList;
		}

		private async void showFlyoutText(string text, int durationInSeconds = 5) {
			if (string.IsNullOrWhiteSpace(FlyoutText)) {
				await System.Threading.Tasks.Task.Run(new Action(() => {
					FlyoutText = text;
					IsFlyoutTextVisible = true;
					System.Threading.Thread.Sleep(durationInSeconds * 1000);
					IsFlyoutTextVisible = false;
					FlyoutText = null;
				}));
			}
		}

		private void onShowSummary() {
			var vm = new AssigmentSummaryViewModel(clientEndpoint, TreeModel.ModifiedTreeItemModels[0], onDone => {
				Messenger.Default.Send(new HidePopupMessage());
			});

			Messenger.Default.Send(new ShowPopupMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Fields.MassEditorAssigner.AssignmentSummaryView), "summary", vm)
			});
		}

		#region Crop Operations
		private void onAddCrop() {
			IsCropZoneOpsPopupOpen = false;

			if (!TreeModel.CheckedCropZonesToAdd.Any()) {
				showFlyoutText(Strings.PleaseSelectFieldsBeforeAddingCropzones_Text);
				return;
			}

			var vm = new AddEditCropZoneTagPopupViewModel(clientEndpoint, dispatcher, currentCropYearId, AddEditCropZoneTagPopupViewModel.SelectionMode.AddCropZones, TreeModel.CheckedCropZonesToAdd, UnsavedTags, popupVm => {
				if (popupVm != null && popupVm.SelectedCrop != null) {
					foreach (var cz in TreeModel.CheckedCropZonesToAdd) {
						// uncomment if we ever want to restrict the creation of cropzones under fields with no boundaries
						//var hasBoundaryArea = cz.BoundaryArea.HasValue && cz.BoundaryArea >= 0;

						if (cz.CropId == null /* && hasBoundaryArea */) {
							cz.Name = popupVm.SelectedCrop.Name;
							cz.UpdateCrop(new CropId(popupVm.SelectedCrop.Id), popupVm.SelectedCrop.Name);
							cz.AddTags(popupVm.SelectedTags.Select(x => x.Tag));

							var czTreeItem = new TreeViewCropZoneItem() {
								CropZoneId = cz.Id,
								CropId = new CropId(popupVm.SelectedCrop.Id),
								Name = cz.Name,
								IsPerennial = false,
								ReportedArea = cz.ReportedArea,
								ReportedAreaUnit = cz.AreaUnit.Name,
								BoundaryArea = cz.SelectedArea,
								BoundaryAreaUnit = cz.AreaUnit.Name,
								Tags = cz.Tags,
							};

							TreeModel.AddCropZoneToModifiedTree(cz.FieldId, czTreeItem);
						}
					}

					TreeModel.ClearSelections();

					UnsavedTags.AddRange(popupVm.GetTrulyNewTags());

					ValidateProperty(() => TreeModel);

					RaisePropertyChanged(() => IsFinalizationAllowed);
				}

				Messenger.Default.Send(new HidePopupMessage());
			});

			Messenger.Default.Send(new ShowPopupMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Fields.MassEditorAssigner.AddEditCropZoneTagPopupView), "add cropzones", vm)
			});
		}

		private void onChangeCrop() {
			IsCropZoneOpsPopupOpen = false;

			if (!TreeModel.CheckedCropZonesToChange.Any()) {
				showFlyoutText(Strings.PleaseSelectCropzonesToChange_Text);
				return;
			}

			var vm = new AddEditCropZoneTagPopupViewModel(clientEndpoint, dispatcher, currentCropYearId, AddEditCropZoneTagPopupViewModel.SelectionMode.ChangeCropZones, TreeModel.CheckedCropZonesToChange, UnsavedTags, popupVm => {
				if (popupVm != null && popupVm.SelectedCrop != null) {
					foreach (var cz in TreeModel.CheckedCropZonesToChange) {
						var newCropId = popupVm.SelectedCrop.Id != Guid.Empty ? new CropId(popupVm.SelectedCrop.Id) : null;

						// skip if new crop is not different
						if (cz.CropId == newCropId) { continue; }

						// only change the name if it isn't a custom name
						if (cz.Name == cz.CropName) {
							cz.Name = popupVm.SelectedCrop.Name;
						}

						cz.UpdateCrop(newCropId, popupVm.SelectedCrop.Name);

						TreeModel.UpdateCropZoneInModifiedTree(cz.Id, cz.Name, cz.CropId, cz.CropName);

						base.RaisePropertyChanged(() => IsFinalizationAllowed);
					}

					TreeModel.ClearSelections();
				}

				Messenger.Default.Send(new HidePopupMessage());
			});

			Messenger.Default.Send(new ShowPopupMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Fields.MassEditorAssigner.AddEditCropZoneTagPopupView), "edit cropzones", vm)
			});
		}

		private void onRemoveCrop() {
			IsCropZoneOpsPopupOpen = false;

			if (!TreeModel.CheckedCropZonesToChange.Any()) {
				showFlyoutText(Strings.PleaseSelectCropzonesToRemove_Text);
				return;
			}

			// tolist because we need to clone so that removals do not throw "collection was modified" exception
			var vm = new RemoveCropZonesPopupViewModel(TreeModel.CheckedCropZonesToChange.ToList(), popupVm => {
				if (popupVm != null && popupVm.CropZonesThatCanBeRemoved != null && popupVm.CropZonesThatCanBeRemoved.Any()) {
					foreach (var cz in popupVm.CropZonesThatCanBeRemoved) {
						if (!cz.CanBeDeleted) { continue; } // throw error msg? shouldn't be any at this point

						if (cz.IsSaved) {
							TreeModel.FlagCropZoneRemovedInModifiedTree(cz.Id);
						}
					}

					TreeModel.ClearSelections();

					RaisePropertyChanged(() => IsFinalizationAllowed);
				}

				Messenger.Default.Send(new HidePopupMessage());
			});

			Messenger.Default.Send(new ShowPopupMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Fields.MassEditorAssigner.RemoveCropZonesPopupView), "remove cropzones", vm)
			});
		}

		private void editCropZoneTreeItemDetails(CropZoneTreeItemViewModel cropZone) {
			string mapData = null;

			clientEndpoint.GetView<Domain.ReadModels.Map.ItemMap>(cropZone.Parent.Id as FieldId).IfValue(mapItem => {
                ItemMapItem mostrecentitem = GetMostRecentShapeByYear(mapItem);

                if (mostrecentitem != null) {
					mapData = mostrecentitem.MapData;
				}
			});

			var vm = new EditCropZoneDetailViewModel(clientEndpoint, dispatcher, currentCropYearId, cropZone, mapData, UnsavedTags, changeVm => {
				if (changeVm != null && changeVm.HasChanges) {
					cropZone.UpdateName(changeVm.Name);

					if (changeVm.HasAreaChanged) {
						cropZone.UpdateArea(cropZone.Area.Unit.GetMeasure(changeVm.SelectedArea));
					}

					if (changeVm.HaveTagsChanged) {
						cropZone.UpdateTags(changeVm.SelectedTags.Select(x => x.Tag));
						UnsavedTags.AddRange(changeVm.GetTrulyNewTags());
					}

					cropZone.ValidateViewModel();

					ValidateProperty(() => TreeModel);

					RaisePropertyChanged(() => IsFinalizationAllowed);
				}

				Messenger.Default.Send(new HidePopupMessage());
			});

			Messenger.Default.Send(new ShowPopupMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Fields.MassAssigner.EditCropZoneDetailPopupView), "editInfo", vm)
			});
		}
		#endregion

		#region Tag Operations
		private void onAddTags() {
			IsTagOpsPopupOpen = false;

			if (!TreeModel.CheckedCropZonesToChange.Any()) {
				showFlyoutText(Strings.PleaseSelectCropzonesToTag_Text);
				return;
			}

			var vm = new AddEditCropZoneTagPopupViewModel(clientEndpoint, dispatcher, currentCropYearId, AddEditCropZoneTagPopupViewModel.SelectionMode.AddTags, TreeModel.CheckedCropZonesToChange, UnsavedTags, popupVm => {
				if (popupVm != null && popupVm.SelectedTags.Any()) {
					foreach (var cz in TreeModel.CheckedCropZonesToChange) {
						cz.AddTags(popupVm.SelectedTags.Select(x => x.Tag));
						TreeModel.UpdateCropZoneTagsInTree(cz.Id, cz.Tags);
					}

					UnsavedTags.AddRange(popupVm.GetTrulyNewTags());

					TreeModel.ClearSelections();

					RaisePropertyChanged(() => IsFinalizationAllowed);
				}

				Messenger.Default.Send(new HidePopupMessage());
			});

			Messenger.Default.Send(new ShowPopupMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Fields.MassEditorAssigner.AddEditCropZoneTagPopupView), "add tags", vm)
			});
		}

		private void onChangeTags() {
			IsTagOpsPopupOpen = false;

			if (!TreeModel.CheckedCropZonesToChange.Any()) {
				showFlyoutText(Strings.PleaseSelectCropzonesToChange_Text);
				return;
			}

			var vm = new AddEditCropZoneTagPopupViewModel(clientEndpoint, dispatcher, currentCropYearId, AddEditCropZoneTagPopupViewModel.SelectionMode.ChangeTags, TreeModel.CheckedCropZonesToChange, UnsavedTags, popupVm => {
				if (popupVm != null && popupVm.SelectedTags.Any()) {
					foreach (var cz in TreeModel.CheckedCropZonesToChange) {
						cz.ChangeTags(popupVm.SelectedTags.Select(x => x.Tag));
						TreeModel.UpdateCropZoneTagsInTree(cz.Id, cz.Tags);
					}

					UnsavedTags.AddRange(popupVm.GetTrulyNewTags());

					TreeModel.ClearSelections();

					RaisePropertyChanged(() => IsFinalizationAllowed);
				}

				Messenger.Default.Send(new HidePopupMessage());
			});

			Messenger.Default.Send(new ShowPopupMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Fields.MassEditorAssigner.AddEditCropZoneTagPopupView), "change tags", vm)
			});
		}

		private void onRemoveTags() {
			IsTagOpsPopupOpen = false;

			if (!TreeModel.CheckedCropZonesToChange.Any()) {
				showFlyoutText(Strings.PleaseSelectCropzonesToChange_Text);
				return;
			}

			DialogFactory.ShowYesNoDialog(
				Strings.RemoveTagsQuestion_Text.ToLower(),
				string.Format(Strings.DoYouWishToRemoveAllTagsFromSelectedCropzones_Question_Text, TreeModel.CheckedCropZonesToChange.Count),
				() => { removeSelectedTags(); },
				() => { /* do nothing */ }
			);
		}

		private void removeSelectedTags() {
			foreach (var cz in TreeModel.CheckedCropZonesToChange) {
				cz.Tags.Clear();
				TreeModel.UpdateCropZoneTagsInTree(cz.Id, cz.Tags);
			}

			RaisePropertyChanged(() => IsFinalizationAllowed);

			TreeModel.ClearSelections();
		}

		private void editTagTreeItemDetails(TagTreeItemViewModel tag) {
			var parentCropZone = tag.Parent as CropZoneTreeItemViewModel;

			var czList = new List<CropZoneItemViewModel>() {
				new CropZoneItemViewModel(clientEndpoint, parentCropZone, null, null, null)
			};

			var vm = new AddEditCropZoneTagPopupViewModel(clientEndpoint, dispatcher, currentCropYearId, AddEditCropZoneTagPopupViewModel.SelectionMode.ChangeTags, czList, UnsavedTags, changeVm => {
				if (changeVm != null) {
					parentCropZone.UpdateTags(changeVm.SelectedTags.Select(x => x.Tag));

					UnsavedTags.AddRange(changeVm.GetTrulyNewTags());
				}

				Messenger.Default.Send(new HidePopupMessage());
			});

			Messenger.Default.Send(new ShowPopupMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Fields.MassEditorAssigner.AddEditCropZoneTagPopupView), "edit tag modification", vm)
			});
		}

		#endregion

		private void onEditTreeItemDetails(AbstractTreeItemViewModel treeItem) {
			if (treeItem is CropZoneTreeItemViewModel) {
				editCropZoneTreeItemDetails(treeItem as CropZoneTreeItemViewModel);
			} else if (treeItem is TagTreeItemViewModel) {
				editTagTreeItemDetails(treeItem as TagTreeItemViewModel);
			}
		}

		private void onRevertModification(AbstractTreeItemViewModel treeItem) {
			if (treeItem is CropZoneTreeItemViewModel) {
				TreeModel.RevertCropZoneModification(treeItem as CropZoneTreeItemViewModel);
			} else if (treeItem is TagTreeItemViewModel) {
				TreeModel.RevertTagModification(treeItem as TagTreeItemViewModel);
			}

			ValidateProperty(() => TreeModel);

			RaisePropertyChanged(() => IsFinalizationAllowed);
		}

		private void onComplete() {
			if (IsFinalizationAllowed) {
				finalize(this);
			}
		}

		private void onCancel() {
			if (IsFinalizationAllowed) {
				DialogFactory.ShowYesNoDialog(
					Strings.ConfirmExit_Text.ToLower(),
					Strings.YouHaveChangesThatHaveNotYetBeenCompletedExitAnyway_Question_Text,
					() => { finalize(null); },
					() => { /* do nothing */ }
				);
			} else {
				finalize(null);
			}
		}

		#region CustomValidation
		public static ValidationResult ValidateTreeModel(TreeSelectionViewModel treeModel, ValidationContext context) {
			ValidationResult result = null;

			var cropZonesWithErrors = from FarmTreeItemViewModel farm in treeModel.ModifiedTreeItemModels[0].Children
									  from FieldTreeItemViewModel field in farm.Children
									  from CropZoneTreeItemViewModel cz in field.Children
									  where cz.HasErrors
									  select cz;

			if (cropZonesWithErrors.Any()) {
				result = new ValidationResult(Strings.OneOrMoreCropzonesHaveProblemsThatNeedToBeCorrected_Text);
			}

			return result;
		}
        #endregion

        private static ItemMapItem GetMostRecentShapeByYear(Lokad.Cqrs.Maybe<ItemMap> maps_a)
        {
            ItemMapItem item_map_item = null;
            if (maps_a.HasValue && maps_a.Value.MostRecentMapItem != null && maps_a.Value.MostRecentMapItem.CropYearChangeOccurred <= Infrastructure.ApplicationEnvironment.CurrentCropYear)
            {
                item_map_item = maps_a.Value.MostRecentMapItem;
            }
            else
            {
                ItemMapItem currentyearmostrecentboundarychange = (from md in maps_a.Value.MapItems where (md.CropYearChangeOccurred <= Infrastructure.ApplicationEnvironment.CurrentCropYear) select md).LastOrDefault();
                if (currentyearmostrecentboundarychange != null && !string.IsNullOrEmpty(currentyearmostrecentboundarychange.MapData))
                {
                    item_map_item = currentyearmostrecentboundarychange;
                }
                if (item_map_item == null)
                {
                    item_map_item = maps_a.Value.MostRecentMapItem;
                }
            }
            return item_map_item;
        }
    }
}