﻿using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Landdb.ViewModel.Fields.MassEditorAssigner {
	public class RemoveCropZonesPopupViewModel {

		private readonly Action<RemoveCropZonesPopupViewModel> done;

		public RemoveCropZonesPopupViewModel(IEnumerable<CropZoneItemViewModel> selectedCropZones, Action<RemoveCropZonesPopupViewModel> onDone) {
			done += onDone;

			CropZonesThatCanBeRemoved = new List<CropZoneItemViewModel>();
			CropZonesThatCannotBeRemoved = new List<CropZoneItemViewModel>();

			foreach(var cz in selectedCropZones) {
				if (cz.CanBeDeleted) {
					CropZonesThatCanBeRemoved.Add(cz);
				} else {
					CropZonesThatCannotBeRemoved.Add(cz);
				}
			}

			CompleteCommand = new RelayCommand(onComplete);
			CancelCommand = new RelayCommand(onCancel);
		}

		public ICommand CompleteCommand { get; }
		public ICommand CancelCommand { get; }

		public List<CropZoneItemViewModel> CropZonesThatCanBeRemoved { get; }
		public List<CropZoneItemViewModel> CropZonesThatCannotBeRemoved { get; }

		public bool HasCropZonesThatCanBeRemoved => CropZonesThatCanBeRemoved != null && CropZonesThatCanBeRemoved.Any();
		public bool HasCropZonesThatCannotBeRemoved => CropZonesThatCannotBeRemoved != null && CropZonesThatCannotBeRemoved.Any();

		private void onComplete() {
			done(this);
		}

		private void onCancel() {
			done(null);
		}
	}
}