﻿using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using Landdb.Client.Spatial;
using Landdb.Domain.ReadModels.Tags;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows.Input;
using System.Windows.Shapes;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Fields.MassEditorAssigner {
	public class EditCropZoneDetailViewModel : ValidationViewModelBase {

		private readonly IClientEndpoint clientEndpoint;
		private readonly Dispatcher dispatcher;

		private readonly Action<EditCropZoneDetailViewModel> detailsChanged;

		private readonly string originalName;
		private readonly double originalArea;
		private readonly List<string> originalTags;

		public EditCropZoneDetailViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, CropYearId currentCropYearId, CropZoneTreeItemViewModel cropZone, string mapData, IEnumerable<string> unsavedTags, Action<EditCropZoneDetailViewModel> onDetailsChanged) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			if (!string.IsNullOrWhiteSpace(mapData)) {
				ShapePreview = WpfTransforms.CreatePathFromMapData(new[] { mapData }, 55);
			}

			originalName = cropZone.Name;
			originalArea = cropZone.Area.Value;
			originalTags = cropZone.Tags.ToList();			

			Name = originalName;
			SelectedArea = cropZone.Area.Value;
			AreaUnit = cropZone.Area.Unit.FullDisplay;

			IsAreaAllowedToChange = cropZone.Status == MassAssignerStatus.Added;

			SelectedTags = new ObservableCollection<TagSelectionItem>();

			// add the first blank new tag, ready for the user to fill out if desired
			NewTags = new ObservableCollection<TagSelectionItem>() {
				new TagSelectionItem(string.Empty, false, SelectedTags, newTag => { onNewTagCheckedChanged(newTag); })
			};

			ExistingTags = clientEndpoint.GetView<TagListView>(currentCropYearId)
				.GetValue(new TagListView())
				.UsedTagsList
				.Union(unsavedTags)
				.Select(x => new TagSelectionItem(x, true, SelectedTags, existingTag => { onExistingTagCheckedChanged(existingTag); }))
				.OrderBy(x => x.Tag)
				.ToList();

			// precheck tags that are already assigned
			foreach (var tag in cropZone.Tags) {
				var existingTag = ExistingTags.SingleOrDefault(x => x.Tag == tag);
				if (existingTag != null) { existingTag.IsChecked = true; }
			}

			detailsChanged += onDetailsChanged;

			UncheckAllTagsCommand = new RelayCommand(onUncheckAllTags);

			CompleteCommand = new RelayCommand(onComplete);
			CancelCommand = new RelayCommand(onCancel);
		}

		public ICommand CompleteCommand { get; }
		public ICommand CancelCommand { get; }

		public ICommand UncheckAllTagsCommand { get; }

		public List<TagSelectionItem> ExistingTags { get; }
		public ObservableCollection<TagSelectionItem> NewTags { get; }
		public ObservableCollection<TagSelectionItem> SelectedTags { get; }

		public string AreaUnit { get; }
		public Path ShapePreview { get; }
		public bool IsAreaAllowedToChange { get; }

		public bool HasChanges => Name != originalName || HasAreaChanged || HaveTagsChanged;
		public bool HasAreaChanged => SelectedArea != originalArea;
		public bool HaveTagsChanged => !SelectedTags.Select(x => x.Tag).SequenceEqual(originalTags);

		private bool _isTagPopupOpen;
		public bool IsTagPopupOpen {
			get { return _isTagPopupOpen; }
			set {
				_isTagPopupOpen = value;
				RaisePropertyChanged(() => IsTagPopupOpen);
			}
		}

		private string _name;
	    [Required(AllowEmptyStrings = false, ErrorMessageResourceName = "ErrorMessage_NameIsRequired_Text", ErrorMessageResourceType = typeof(Strings))]
        public string Name {
			get { return _name; }
			set {
				_name = value;
				ValidateAndRaisePropertyChanged(() => Name);
			}
		}

		private double _selectedArea;
		[CustomValidation(typeof(EditCropZoneDetailViewModel), nameof(ValidateSelectedArea))]
		public double SelectedArea {
			get { return _selectedArea; }
			set {
				_selectedArea = value;
				ValidateAndRaisePropertyChanged(() => SelectedArea);
			}
		}

		public IEnumerable<string> GetTrulyNewTags() {
			return NewTags.Where(x => !string.IsNullOrWhiteSpace(x.Tag)).Except(ExistingTags).Select(x => x.Tag);
		}

		private void onExistingTagCheckedChanged(TagSelectionItem existingTag) {
			if (existingTag.IsChecked) {
				SelectedTags.Add(existingTag);
			} else {
				SelectedTags.Remove(existingTag);
			}
		}

		private void onNewTagCheckedChanged(TagSelectionItem newTag) {
			if (newTag.IsChecked) {
				// if this is truly a new tag, it will have an index of 0 because it is not yet in the list.
				// otherwise, we need to replace the existing tag
				if (newTag.Index == 0) {
					SelectedTags.Add(newTag);
				} else {
					var existingIndex = newTag.Index - 1;
					SelectedTags.RemoveAt(existingIndex);
					SelectedTags.Insert(existingIndex, newTag);
				}
			} else {
				SelectedTags.Remove(newTag);
			}

			// add a new blank tag, ready to be filled in.
			if (newTag.IsChecked && !string.IsNullOrWhiteSpace(NewTags.Last().Tag)) {
				NewTags.Add(new TagSelectionItem(string.Empty, false, SelectedTags, nt => onNewTagCheckedChanged(nt)));
			}
		}

		private void onUncheckAllTags() {
			ExistingTags.ForEach(x => x.IsChecked = false);
			NewTags.ForEach(x => x.IsChecked = false);
		}

		private void onComplete() {
			if (ValidateViewModel()) {
				detailsChanged(this);
			}
		}

		private void onCancel() {
			detailsChanged(null);
		}

		#region Custom Validation
		public static ValidationResult ValidateSelectedArea(double? selectedArea, ValidationContext context) {
			ValidationResult result = null;

			if (selectedArea == null || selectedArea <= 0) {
				result = new ValidationResult(Strings.AreaIsRequired_Text);
			}

			return result;
		}
		#endregion
	}
}
