﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using Landdb.Resources;

namespace Landdb.ViewModel.Fields.MassEditorAssigner {
	public class AssigmentSummaryViewModel {

		private readonly IMasterlistService masterListService;

		private readonly Action<AssigmentSummaryViewModel> done;

		public AssigmentSummaryViewModel(IClientEndpoint clientEndpoint, AbstractTreeItemViewModel modifiedTree, Action<AssigmentSummaryViewModel> onDone) {
			var areaUnit = $"{ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit}s";

			masterListService = clientEndpoint.GetMasterlistService();

			buildSummariesByCrop(modifiedTree, areaUnit);
			buildSummariesByTag(modifiedTree, areaUnit);

			done += onDone;

			CompleteCommand = new RelayCommand(onComplete);
		}

		public ICommand CompleteCommand { get; }

		public List<SummaryTreeItem> CropSummaryItemRoot { get; private set; }
		public List<SummaryTreeItem> TagSummaryItemRoot { get; private set; }

		private void buildSummariesByCrop(AbstractTreeItemViewModel modifiedTree, string areaUnit) {
			CropSummaryItemRoot = new List<SummaryTreeItem> {
				new SummaryTreeItem(Strings.AllCrops_Text, 0, areaUnit, true)
			};

			var rootNode = CropSummaryItemRoot[0];

			var cropZonesGroupedByCropId = from FarmTreeItemViewModel farm in modifiedTree.Children
										   from FieldTreeItemViewModel field in farm.Children
										   from CropZoneTreeItemViewModel cz in field.Children
										   where cz.Status != MassAssignerStatus.Removed
										   orderby farm.Name, field.Name, cz.Name, cz.CropName
										   group cz by cz.CropId into g
										   select g;

			foreach (var cropGroup in cropZonesGroupedByCropId) {
				var miniCrop = masterListService.GetCrop(cropGroup.Key);

				SummaryTreeItem cropSummary = null;

				if (miniCrop.ParentId == null) {
					cropSummary = rootNode.Children.SingleOrDefault(x => x.Name == miniCrop.Name);

					if (cropSummary == null) {
						cropSummary = new SummaryTreeItem(miniCrop.Name, 0, areaUnit);
						rootNode.Children.Add(cropSummary);
					}
				} else {
					var parentCropId = new CropId(miniCrop.ParentId.Value);
					var parentMiniCrop = masterListService.GetCrop(parentCropId);

					var parentCropSummary = rootNode.Children.SingleOrDefault(x => x.Name == parentMiniCrop.Name);

					if (parentCropSummary == null) {
						parentCropSummary = new SummaryTreeItem(parentMiniCrop.Name, 0, areaUnit);
						rootNode.Children.Add(parentCropSummary);
					}

					cropSummary = parentCropSummary.Children.SingleOrDefault(x => x.Name == parentMiniCrop.Name);
					if (cropSummary == null) {
						cropSummary = new SummaryTreeItem(miniCrop.Name, 0, areaUnit);
						parentCropSummary.Children.Add(cropSummary);
					}
				}

				foreach (var czTreeItem in cropGroup) {
					var parentField = czTreeItem.Parent as FieldTreeItemViewModel;
					var parentFarm = parentField.Parent as FarmTreeItemViewModel;
					var fullName = $"{parentFarm.Name} : {parentField.Name} : {czTreeItem.Name}";

					cropSummary.Children.Add(
						new SummaryTreeItem(fullName, czTreeItem.Area != null ? czTreeItem.Area.Value : 0, areaUnit)
					);
				}
			}
		}

		private void buildSummariesByTag(AbstractTreeItemViewModel modifiedTree, string areaUnit) {
			TagSummaryItemRoot = new List<SummaryTreeItem>() {
				new SummaryTreeItem(Strings.AllTags_Text, 0, areaUnit, true)
			};

			var rootNode = TagSummaryItemRoot[0];

			var cropZonesGroupedByTag = from FarmTreeItemViewModel farm in modifiedTree.Children
										from FieldTreeItemViewModel field in farm.Children
										from CropZoneTreeItemViewModel cz in field.Children
										from tag in cz.Tags
										where cz.Status != MassAssignerStatus.Removed
										group cz by tag into g
										orderby g.Key
										select g;

			foreach (var tagGroup in cropZonesGroupedByTag) {
				var tagSummary = rootNode.Children.SingleOrDefault(x => x.Name == tagGroup.Key);

				if (tagSummary == null) {
					tagSummary = new SummaryTreeItem(tagGroup.Key, 0, areaUnit);
					rootNode.Children.Add(tagSummary);
				}

				foreach (var czTreeItem in tagGroup) {
					var parentField = czTreeItem.Parent as FieldTreeItemViewModel;
					var parentFarm = parentField.Parent;
					var fullName = $"{parentFarm} : {parentField} : {czTreeItem.Name}";

					tagSummary.Children.Add(
						new SummaryTreeItem(fullName, czTreeItem.Area != null ? czTreeItem.Area.Value : 0, areaUnit)
					);
				}
			}
		}

		private void onComplete() {
			done(this);
		}

		public class SummaryTreeItem : ViewModelBase {

			private readonly double area;

			public SummaryTreeItem(string name, double area, string areaUnit, bool isExpanded = false) {
				Name = name;
				AreaUnit = areaUnit;
				this.area = area;

				IsExpanded = isExpanded;

				Children = new List<SummaryTreeItem>();
			}

			public string Name { get; }
			public string AreaUnit { get; }
			public List<SummaryTreeItem> Children { get; }

			public double Area => Children.Any() ? Children.Sum(x => x.Area) : area;

			private bool _isSelected;
			public bool IsSelected {
				get { return _isSelected; }
				set {
					_isSelected = value;
					RaisePropertyChanged(() => IsSelected);
				}
			}

			private bool _isExpanded;
			public bool IsExpanded {
				get { return _isExpanded; }
				set {
					_isExpanded = value;
					RaisePropertyChanged(() => IsExpanded);
				}
			}

			public override string ToString() => $"{Name} : {Area} {AreaUnit}";
		}
	}
}