﻿using AgC.UnitConversion;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Client.Spatial;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows.Shapes;
using Landdb.Resources;

namespace Landdb.ViewModel.Fields.MassEditorAssigner {

	// TODO: possible to factor out this vm?

	public class CropZoneItemViewModel : ValidationViewModelBase {

		readonly IClientEndpoint clientEndpoint;

		public CropZoneItemViewModel(IClientEndpoint clientEndpoint, AbstractTreeItemViewModel treeItem, double? boundaryArea, double? reportedArea, string mapData) {
			this.clientEndpoint = clientEndpoint;

			FieldTreeItemViewModel field = null;

			if (treeItem is FieldTreeItemViewModel) {
				// new cropzone
				field = treeItem as FieldTreeItemViewModel;

				Id = new CropZoneId(field.FieldId.DataSourceId, Guid.NewGuid());
				Name = Strings.UnknownCrop_Text;

				OriginalTags = new List<string>();
				Tags = new List<string>();
			} else if (treeItem is CropZoneTreeItemViewModel) {
				// pre-existing cropzone
				var cropZone = treeItem as CropZoneTreeItemViewModel;

				field = cropZone.Parent as FieldTreeItemViewModel;

				Id = cropZone.CropZoneId;
				Name = cropZone.Name;
				CropId = cropZone.CropId;
				CropName = cropZone.CropName;
				CanBeDeleted = cropZone.CanBeDeleted();
				IsSaved = true;

				OriginalTags = cropZone.Tags.ToList();
				Tags = cropZone.Tags.ToList();
			}

			AreaUnit = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit);

			BoundaryArea = boundaryArea;
			ReportedArea = reportedArea;

			FieldId = field.FieldId;
			FieldName = field.Name;

			var farm = field.Parent as FarmTreeItemViewModel;
			FarmId = farm.FarmId;
			FarmName = farm.Name;

			MapData = mapData;

			if (mapData != null) {
				ShapePreview = WpfTransforms.CreatePathFromMapData(new[] { mapData }, 55);
			}

			ValidateViewModel();
		}

		public CropZoneId Id { get; }
		public FarmId FarmId { get; }
		public string FarmName { get; }
		public FieldId FieldId { get; }
		public string FieldName { get; }
		public IUnit AreaUnit { get; }
		public bool IsSaved { get; }
		public bool CanBeDeleted { get; }
		public string MapData { get; }
		public Path ShapePreview { get; }
		public List<string> OriginalTags { get; }

		public CropId CropId { get; private set; }
		public string CropName { get; private set; }

		public List<string> Tags { get; private set; }

		// lazy load
		private CropZoneDataUsageStatistics _usageStatistics;
		public CropZoneDataUsageStatistics UsageStatistics {
			get {
				if (_usageStatistics == null) {
					_usageStatistics = clientEndpoint.GetView<CropZoneDataUsageStatistics>(Id).GetValue(() => null);
				}

				return _usageStatistics;
			}
		}

		[CustomValidation(typeof(CropZoneItemViewModel), nameof(ValidateSelectedArea))]
		public double? SelectedArea => ReportedArea.HasValue ? ReportedArea : BoundaryArea;

		private string _name;
	    [Required(AllowEmptyStrings = false, ErrorMessageResourceName = "ErrorMessage_NameIsRequired_Text", ErrorMessageResourceType = typeof(Strings))]
        public string Name {
			get { return _name; }
			set {
				_name = value;
				ValidateAndRaisePropertyChanged(() => Name);
			}
		}

		private double? _reportedArea;
		public double? ReportedArea {
			get { return _reportedArea; }
			set {
				_reportedArea = value;
				RaisePropertyChanged(() => ReportedArea);
				ValidateAndRaisePropertyChanged(() => SelectedArea);
			}
		}

		private double? _boundaryArea;
		public double? BoundaryArea {
			get { return _boundaryArea; }
			set {
				_boundaryArea = value;
				RaisePropertyChanged(() => BoundaryArea);
				ValidateAndRaisePropertyChanged(() => SelectedArea);
			}
		}

		public void UpdateCrop(CropId cropId, string name) {
			CropId = cropId;
			CropName = name;
			RaisePropertyChanged(() => CropName);
		}

		public void AddTags(IEnumerable<string> newTags) {
			Tags = Tags.Union(newTags).ToList();
			RaisePropertyChanged(() => Tags);
		}

		public void ChangeTags(IEnumerable<string> newTags) {
			Tags = newTags.ToList();
			RaisePropertyChanged(() => Tags);
		}

		public override string ToString() => $"{FarmName} : {FieldName} : {Name} : ({(!string.IsNullOrWhiteSpace(CropName) ? CropName : $"<{Strings.NoCrop_Text.ToLower()}>")})";

		private void onComplete() {
			Messenger.Default.Send(new HidePopupMessage());
		}

		private void onCancel() {
			Messenger.Default.Send(new HidePopupMessage());
		}

		#region Custom Validation
		public static ValidationResult ValidateSelectedArea(double? selectedArea, ValidationContext context) {
			ValidationResult result = null;

			var vm = (CropZoneItemViewModel)context.ObjectInstance;

			if (!vm.IsSaved && (selectedArea == null || selectedArea <= 0)) {
				result = new ValidationResult(Strings.AreaIsRequired_Text);
			}

			return result;
		}
		#endregion
	}
}