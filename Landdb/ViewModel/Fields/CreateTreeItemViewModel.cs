﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure.Messages;
using Landdb.Infrastructure;
using System.ComponentModel.DataAnnotations;
using Landdb.Resources;

namespace Landdb.ViewModel.Fields {
    public class CreateTreeItemViewModel : ValidationViewModelBase {
        readonly IClientEndpoint clientEndpoint;
        AbstractTreeItemViewModel rootNode;
        AbstractTreeItemViewModel selectedParentNode;
        MiniCrop newItemCropSelection;
        PreviousTreeItemViewModel previousItemsViewModel;
        bool isContinuouslyAdding;
        bool useparentboundary = false;
        Visibility useparentboundaryvisibility = Visibility.Visible;
        Visibility iscontinuouslyaddingvisibility = Visibility.Visible;
        bool isAddItemPopupOpen;
        Action<AbstractTreeItemViewModel> onNodeCreated;

        int currentCropYear;
        Guid currentDataSourceId;

        string creatorName = string.Empty;
        string newItemName;
        bool isUseReportedAreaChecked;
        bool isPerennialCropChecked;
        double? reportedArea;
        object newItemFromPreviousSelection;

        string areaUnitString = string.Empty;

        public CreateTreeItemViewModel(IClientEndpoint clientEndpoint, Action<AbstractTreeItemViewModel> onNodeCreated) {
            this.clientEndpoint = clientEndpoint;
            this.onNodeCreated = onNodeCreated;

            this.previousItemsViewModel = new PreviousTreeItemViewModel(clientEndpoint);

            CreateNewItemCommand = new RelayCommand(CreateNewItem);
            CancelCreateCommand = new RelayCommand(CancelCreate);
        }

        public ICommand CreateNewItemCommand { get; private set; }
        public ICommand CancelCreateCommand { get; private set; }

		// checked when creating a cropzone
		public bool HasFieldArea {
			get {
				var hasBoundaryArea = false;
				var hasReportedArea = false;

				if (selectedParentNode is FieldTreeItemViewModel) {
					var fieldDetails = clientEndpoint.GetView<FieldDetailsView>(((FieldTreeItemViewModel)selectedParentNode).FieldId).GetValue(() => null);
					if (fieldDetails != null) { 
						hasBoundaryArea = fieldDetails.BoundaryArea.HasValue && fieldDetails.BoundaryArea.Value > 0;
						hasReportedArea = fieldDetails.ReportedArea.HasValue && fieldDetails.ReportedArea.Value > 0;
					}
				}

				return hasBoundaryArea || hasReportedArea;
			}
		}

		[Required]
        public string NewItemName {
            get { return newItemName; }
            set {
                newItemName = value;
				ValidateAndRaisePropertyChanged(() => NewItemName);
            }
        }

        public bool IsUseReportedAreaChecked {
            get { return isUseReportedAreaChecked; }
            set {
                isUseReportedAreaChecked = value;
                RaisePropertyChanged(() => IsUseReportedAreaChecked);
            }
        }

        public bool IsPerennialCropChecked {
            get { return isPerennialCropChecked; }
            set {
                isPerennialCropChecked = value;
                RaisePropertyChanged(() => IsPerennialCropChecked);
            }
        }

        bool displayReportedAreaUI = false;
        public bool DisplayReportedAreaUI {
            get { return displayReportedAreaUI; }
            set {
                displayReportedAreaUI = value;
                RaisePropertyChanged(() => DisplayReportedAreaUI);
            }
        }

        public double? ReportedArea {
            get { return reportedArea; }
            set {
                reportedArea = value;
                RaisePropertyChanged(() => ReportedArea);

                // TODO: Just need to get rid of this entirely in the domain, in favor of preferring reported area simply if it exists.
                if (reportedArea.HasValue && reportedArea > 0) {
                    IsUseReportedAreaChecked = true;
                } else {
                    IsUseReportedAreaChecked = false;
                }
            }
        }

        public object NewItemFromPreviousSelection {
            get { return newItemFromPreviousSelection; }
            set {
                newItemFromPreviousSelection = value;
                RaisePropertyChanged(() => NewItemFromPreviousSelection);
            }
        }

        public int CurrentCropYear {
            get { return currentCropYear; }
            set {
                currentCropYear = value;
                previousItemsViewModel.CurrentYear = value;
            }
        }
        public Guid CurrentDataSourceId {
            get { return currentDataSourceId; }
            set {
                currentDataSourceId = value;
                previousItemsViewModel.DataSourceId = value;
            }
        }

        public AbstractTreeItemViewModel RootNode {
            get { return rootNode; }
            set {
                rootNode = value;
                RaisePropertyChanged(() => RootNode);
            }
        }

        public AbstractTreeItemViewModel SelectedParentNode {
            get { return selectedParentNode; }
            set {
                selectedParentNode = value;
                RaisePropertyChanged(() => SelectedParentNode);
            }
        }

        public MiniCrop NewItemCropSelection {
            get { return newItemCropSelection; }
            set {
                // Remember the old name so that we can adjust default naming below, if it was defaulted before.
                var oldName = newItemCropSelection != null ? newItemCropSelection.Name : string.Empty;

                newItemCropSelection = value;

                if (value != null && (string.IsNullOrWhiteSpace(NewItemName) || NewItemName == oldName)) {
                    NewItemName = value.Name;
                    RaisePropertyChanged(() => NewItemName);
                }
            }
        }

        public PreviousTreeItemViewModel PreviousItemsViewModel {
            get { return previousItemsViewModel; }
            private set {
                previousItemsViewModel = value;
                RaisePropertyChanged(() => PreviousItemsViewModel);
            }
        }

        public IEnumerable<MiniCrop> CropList {
            get { return clientEndpoint.GetMasterlistService().GetCropList().OrderBy(x => x.Name); }
        }

        public bool IsAddItemPopupOpen {
            get { return isAddItemPopupOpen; }
            set {
                isAddItemPopupOpen = value;
                RaisePropertyChanged(() => IsAddItemPopupOpen);

                if (!IsAddItemPopupOpen) {
                    IsContinuouslyAdding = false;
                }
            }
        }

        public bool IsContinuouslyAdding {
            get { return isContinuouslyAdding; }
            set {
                isContinuouslyAdding = value;
                RaisePropertyChanged(() => IsContinuouslyAdding);
            }
        }

        public bool UseParentBoundary {
            get { return useparentboundary; }
            set {
                useparentboundary = value;
                RaisePropertyChanged(() => UseParentBoundary);
            }
        }

        public Visibility UseParentBoundaryVisibility {
            get { return useparentboundaryvisibility; }
            set {
                useparentboundaryvisibility = value;
                RaisePropertyChanged(() => UseParentBoundaryVisibility);
            }
        }

        public Visibility IsContinuouslyAddingVisibility {
            get { return iscontinuouslyaddingvisibility; }
            set {
                iscontinuouslyaddingvisibility = value;
                RaisePropertyChanged(() => IsContinuouslyAddingVisibility);
            }
        }

        public string CreatorName {
            get { return creatorName; }
            set {
                creatorName = value;
                RaisePropertyChanged(() => CreatorName);
            }
        }

        async void CreateNewItem() {
			if (!ValidateViewModel()) { return; }

            AbstractTreeItemViewModel newItem = null;
            string name = NewItemName;
            var settings = clientEndpoint.GetUserSettings();
            bool sortAlphabetically = settings.AreFieldsSortedAlphabetically;

            if (SelectedParentNode is GrowerTreeItemViewModel)
            {
                FarmId farmId = null;

                if (NewItemFromPreviousSelection != null && NewItemFromPreviousSelection is FarmYearUseItem) {
                    await DialogFactory.ShowTwoOptionDialog(Strings.UsePreviousItem_Text, Strings.AFarmWithSameNameUsedPreviously_Text, 
                        Strings.UseThePreviousFarm_Text, () => { 
                            var fyui = NewItemFromPreviousSelection as FarmYearUseItem;
                            farmId = fyui.Id;
                            name = fyui.Name;
                            IncludeFarmInCropYear includeFarmComand = new IncludeFarmInCropYear(farmId, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), CurrentCropYear);
                            clientEndpoint.SendOne(includeFarmComand);
                        },
                        Strings.CreateANewOne_Text, () => { farmId = CreateNewFarm(farmId); });
                } else {
                    farmId = CreateNewFarm(farmId);
                }

                // Create the item to update the UI
                newItem = new FarmTreeItemViewModel(new TreeViewFarmItem() { FarmId = farmId, Name = name, Fields = new List<TreeViewFieldItem>() }, SelectedParentNode, sortAlphabetically);
                RaisePropertyChanged(() => PreviousItemsViewModel.PreviousFarms);
            }

            if (SelectedParentNode is FarmTreeItemViewModel)
            {
                FieldId fieldId = null;
                FarmTreeItemViewModel farmVm = SelectedParentNode as FarmTreeItemViewModel;

                if (NewItemFromPreviousSelection != null && NewItemFromPreviousSelection is FieldYearUseItem) {
                    await DialogFactory.ShowTwoOptionDialog(Strings.UsePreviousItem_Text, Strings.AFieldWithSameNameUsedPreviously_Text,
                        Strings.UseThePreviousField_Text, () => {
                            var fyui = NewItemFromPreviousSelection as FieldYearUseItem;
                            fieldId = fyui.Id;
                            name = fyui.Name;
                            IncludeFieldInCropYear includeFieldCommand = new IncludeFieldInCropYear(fieldId, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), farmVm.FarmId, currentCropYear);
                            clientEndpoint.SendOne(includeFieldCommand);
                        },
                        Strings.CreateANewOne_Text, () => { fieldId = CreateNewField(fieldId, farmVm); });
                } else {
                    fieldId = CreateNewField(fieldId, farmVm);
                }

                // Create the item to update the UI
                newItem = new FieldTreeItemViewModel(new TreeViewFieldItem() { FieldId = fieldId, Name = name, CropZones = new List<TreeViewCropZoneItem>() }, SelectedParentNode, sortAlphabetically);
                RaisePropertyChanged(() => PreviousItemsViewModel.PreviousFields);
            }

            if (SelectedParentNode is FieldTreeItemViewModel)
            {
                if (NewItemCropSelection == null) { return; } // prevent a null ref from happening below.				

                FieldTreeItemViewModel fieldVm = SelectedParentNode as FieldTreeItemViewModel;

				if (!HasFieldArea) { return; }

                var czId = new CropZoneId(this.CurrentDataSourceId, Guid.NewGuid());
                double? area = null;
                if (ReportedArea.HasValue && ReportedArea > 0) {
                    area = ReportedArea;
                }

                if (string.IsNullOrWhiteSpace(name) && this.NewItemCropSelection != null) { // Force name to equal crop if they've removed it.
                    name = this.NewItemCropSelection.Name;
                }

                if (IsPerennialCropChecked) {
                    CreatePerennialCropZone createPerennialCropZoneCommand = new CreatePerennialCropZone(czId, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), name, fieldVm.FieldId, new CropId(NewItemCropSelection.Id), currentCropYear, area, ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit);
                    clientEndpoint.SendOne(createPerennialCropZoneCommand);
                } else {
                    CreateAnnualCropZone createAnnualCropZoneCommand = new CreateAnnualCropZone(czId, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), name, fieldVm.FieldId, new CropId(NewItemCropSelection.Id), currentCropYear, area, ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit);
                    clientEndpoint.SendOne(createAnnualCropZoneCommand);
                }

                var fieldDetails = clientEndpoint.GetView<FieldDetailsView>(fieldVm.FieldId).Value;
                double? bdyArea = null;
                if (fieldDetails.BoundaryArea.HasValue)
                {
                    bdyArea = fieldDetails.BoundaryArea.Value;
                }
                var areaUnit = ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit;
                newItem = new CropZoneTreeItemViewModel(new TreeViewCropZoneItem() { CropZoneId = czId, CropId = new CropId(NewItemCropSelection.Id), Name = NewItemName, IsPerennial = IsPerennialCropChecked, ReportedArea = area, ReportedAreaUnit = areaUnit, BoundaryArea = bdyArea, BoundaryAreaUnit = areaUnit }, SelectedParentNode, sortAlphabetically);
                
            }

            // Update the UI
            SelectedParentNode.Children.Add(newItem);
            SelectedParentNode.IsExpanded = true;
            onNodeCreated(newItem); // notify the parent

            if (!IsContinuouslyAdding) {
                IsAddItemPopupOpen = false;
                Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
                NewItemName = string.Empty;
                NewItemFromPreviousSelection = null;
                NewItemCropSelection = null;
            }

            // Clear item inputs
            //NewItemName = string.Empty;
            //NewItemFromPreviousSelection = null;
            //NewItemCropSelection = null;
            ReportedArea = null;
        }

        private FieldId CreateNewField(FieldId fieldId, FarmTreeItemViewModel farmVm) {
            fieldId = new FieldId(this.CurrentDataSourceId, Guid.NewGuid());
            CreateField createFieldCommand = new CreateField(fieldId, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), NewItemName, farmVm.FarmId, currentCropYear, ReportedArea, ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit);
            clientEndpoint.SendOne(createFieldCommand);
            return fieldId;
        }

        private FarmId CreateNewFarm(FarmId farmId) {
            farmId = new FarmId(this.CurrentDataSourceId, Guid.NewGuid());
            CreateFarm createFarmCommand = new CreateFarm(farmId, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), NewItemName, CurrentCropYear);
            clientEndpoint.SendOne(createFarmCommand);
            return farmId;
        }

        void CancelCreate() {
            IsAddItemPopupOpen = false;
            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
            NewItemName = string.Empty;
            NewItemFromPreviousSelection = null;
            NewItemCropSelection = null;
            ReportedArea = null;
        }
    }
}