﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure.Messages;
using Landdb.Infrastructure;

namespace Landdb.ViewModel.Fields {
    public class CreateTreeItemMapCZViewModel : ViewModelBase {
        readonly IClientEndpoint clientEndpoint;
        AbstractTreeItemViewModel rootNode;
        AbstractTreeItemViewModel selectedParentNode;
        AbstractTreeItemViewModel creatednode;
        MiniCrop newItemCropSelection;
        PreviousTreeItemViewModel previousItemsViewModel;
        bool isContinuouslyAdding = false;
        bool useparentboundary = false;
        Visibility useparentboundaryvisibility = Visibility.Collapsed;
        Visibility iscontinuouslyaddingvisibility = Visibility.Collapsed;
        bool isAddItemPopupOpen;
        Action<AbstractTreeItemViewModel> onNodeCreated;
        IList<IDomainCommand> commandlist = new List<IDomainCommand>();

        int currentCropYear;
        Guid currentDataSourceId;

        string newItemName;
        bool isUseReportedAreaChecked;
        bool isPerennialCropChecked;
        double? reportedArea;
        object newItemFromPreviousSelection;

        public CreateTreeItemMapCZViewModel(IClientEndpoint clientEndpoint, Action<AbstractTreeItemViewModel> onNodeCreated, Action onNodeIgnored) {
            this.clientEndpoint = clientEndpoint;
            this.onNodeCreated = onNodeCreated;

            this.previousItemsViewModel = new PreviousTreeItemViewModel(clientEndpoint);
            commandlist = new List<IDomainCommand>();

            CreateNewItemCommand = new RelayCommand(CreateNewItem);
            CancelCreateCommand = new RelayCommand(CancelCreate);
        }

        public ICommand CreateNewItemCommand { get; private set; }
        public ICommand CancelCreateCommand { get; private set; }

        public string NewItemName {
            get { return newItemName; }
            set {
                newItemName = value;
                RaisePropertyChanged("NewItemName");
            }
        }

        public bool IsUseReportedAreaChecked {
            get { return isUseReportedAreaChecked; }
            set {
                isUseReportedAreaChecked = value;
                RaisePropertyChanged("IsUseReportedAreaChecked");
            }
        }

        public bool IsPerennialCropChecked {
            get { return isPerennialCropChecked; }
            set {
                isPerennialCropChecked = value;
                RaisePropertyChanged("IsPerennialCropChecked");
            }
        }

        public double? ReportedArea {
            get { return reportedArea; }
            set {
                reportedArea = value;
                RaisePropertyChanged("ReportedArea");
            }
        }

        public object NewItemFromPreviousSelection {
            get { return newItemFromPreviousSelection; }
            set {
                newItemFromPreviousSelection = value;
                RaisePropertyChanged("NewItemFromPreviousSelection");
            }
        }

        public int CurrentCropYear {
            get { return currentCropYear; }
            set {
                currentCropYear = value;
                previousItemsViewModel.CurrentYear = value;
            }
        }
        public Guid CurrentDataSourceId {
            get { return currentDataSourceId; }
            set {
                currentDataSourceId = value;
                previousItemsViewModel.DataSourceId = value;
            }
        }

        public AbstractTreeItemViewModel RootNode {
            get { return rootNode; }
            set {
                rootNode = value;
                RaisePropertyChanged("RootNode");
            }
        }

        public AbstractTreeItemViewModel SelectedParentNode {
            get { return selectedParentNode; }
            set {
                selectedParentNode = value;
                RaisePropertyChanged("SelectedParentNode");
            }
        }

        public AbstractTreeItemViewModel CreatedNode {
            get { return creatednode; }
            set {
                creatednode = value;
                RaisePropertyChanged("CreatedNode");
            }
        }

        public MiniCrop NewItemCropSelection {
            get { return newItemCropSelection; }
            set {
                // Remember the old name so that we can adjust default naming below, if it was defaulted before.
                var oldName = newItemCropSelection != null ? newItemCropSelection.Name : string.Empty;

                newItemCropSelection = value;

                if (value != null && (string.IsNullOrWhiteSpace(NewItemName) || NewItemName == oldName)) {
                    NewItemName = value.Name;
                    RaisePropertyChanged("NewItemName");
                }
            }
        }

        public PreviousTreeItemViewModel PreviousItemsViewModel {
            get { return previousItemsViewModel; }
            private set {
                previousItemsViewModel = value;
                RaisePropertyChanged("PreviousTreeItemViewModel");
            }
        }

        public IEnumerable<MiniCrop> CropList {
            get { return clientEndpoint.GetMasterlistService().GetCropList().OrderBy(x => x.Name); }
        }

        public bool IsAddItemPopupOpen {
            get { return isAddItemPopupOpen; }
            set {
                isAddItemPopupOpen = value;
                RaisePropertyChanged("IsAddItemPopupOpen");

                if (!IsAddItemPopupOpen) {
                    IsContinuouslyAdding = false;
                }
            }
        }

        public bool IsContinuouslyAdding {
            get { return isContinuouslyAdding; }
            set {
                isContinuouslyAdding = value;
                RaisePropertyChanged("IsContinuouslyAdding");
            }
        }

        public bool UseParentBoundary {
            get { return useparentboundary; }
            set {
                useparentboundary = value;
                RaisePropertyChanged("UseParentBoundary");
            }
        }

        public Visibility UseParentBoundaryVisibility {
            get { return useparentboundaryvisibility; }
            set {
                useparentboundaryvisibility = value;
                RaisePropertyChanged("UseParentBoundaryVisibility");
            }
        }

        public Visibility IsContinuouslyAddingVisibility {
            get { return iscontinuouslyaddingvisibility; }
            set {
                iscontinuouslyaddingvisibility = value;
                RaisePropertyChanged("IsContinuouslyAddingVisibility");
            }
        }

        public IList<IDomainCommand> CommandList {
            get { return commandlist; }
            set { commandlist = value; }
        }

        void CreateNewItem() {
            AbstractTreeItemViewModel newItem = null;
            string name = NewItemName;
            var settings = clientEndpoint.GetUserSettings();
            bool sortAlphabetically = settings.AreFieldsSortedAlphabetically;

            if (SelectedParentNode is FieldTreeItemViewModel)
            {
                if (NewItemCropSelection == null) { return; } // prevent a null ref from happening below.

                FieldTreeItemViewModel fieldVm = SelectedParentNode as FieldTreeItemViewModel;
                var czId = new CropZoneId(this.CurrentDataSourceId, Guid.NewGuid());
                double? area = null;
                if (ReportedArea.HasValue && ReportedArea > 0) {
                    area = ReportedArea;
                }
                if (IsPerennialCropChecked) {
                    CreatePerennialCropZone createPerennialCropZoneCommand = new CreatePerennialCropZone(czId, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), name, fieldVm.FieldId, new CropId(NewItemCropSelection.Id), ApplicationEnvironment.CurrentCropYear, area, ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit);
                    //clientEndpoint.SendOne(createPerennialCropZoneCommand);
                    commandlist.Add(createPerennialCropZoneCommand);
                } else {
                    CreateAnnualCropZone createAnnualCropZoneCommand = new CreateAnnualCropZone(czId, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), name, fieldVm.FieldId, new CropId(NewItemCropSelection.Id), ApplicationEnvironment.CurrentCropYear, area, ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit);
                    //clientEndpoint.SendOne(createAnnualCropZoneCommand);
                    commandlist.Add(createAnnualCropZoneCommand);
                }

                // Create the item to update the UI
                var areaUnit = ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit;
                newItem = new CropZoneTreeItemViewModel(new TreeViewCropZoneItem() { CropZoneId = czId, CropId = new CropId(NewItemCropSelection.Id), Name = NewItemName, IsPerennial = IsPerennialCropChecked, ReportedArea = area, ReportedAreaUnit = areaUnit }, SelectedParentNode, sortAlphabetically);
                CreatedNode = newItem;
            }

            // Update the UI
            //selectedParentVm.Children.Add(newItem);
            //selectedParentVm.IsExpanded = true;
            onNodeCreated(newItem); // notify the parent

            if (!IsContinuouslyAdding) {
                IsAddItemPopupOpen = false;
                Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
            }

            // Clear item inputs
            NewItemName = string.Empty;
            NewItemFromPreviousSelection = null;
            NewItemCropSelection = null;
            ReportedArea = null;
        }

        void CancelCreate() {
            IsAddItemPopupOpen = false;
            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
        }

    }
}
