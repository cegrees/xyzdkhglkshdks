﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;

namespace Landdb.ViewModel.Fields {
    public class PreviousTreeItemViewModel {
        IClientEndpoint clientEndpoint;

        public PreviousTreeItemViewModel(IClientEndpoint clientEndpoint) {
            this.clientEndpoint = clientEndpoint;
        }

        internal Guid DataSourceId { get; set; }
        internal int CurrentYear { get; set; }

        public IEnumerable<FarmYearUseItem> PreviousFarms {
            get {
                var uses = clientEndpoint.GetView<TreeItemYearUse>(new DataSourceId(DataSourceId));
                if (!uses.HasValue) { return new List<FarmYearUseItem>(); }
                var q = from f in uses.Value.Farms
                        where !f.CropYears.Contains(CurrentYear)
                        orderby f.Name
                        select f;
                return q;
            }
        }

        public IEnumerable<FieldYearUseItem> PreviousFields {
            get {
                var uses = clientEndpoint.GetView<TreeItemYearUse>(new DataSourceId(DataSourceId));
                if (!uses.HasValue) { return new List<FieldYearUseItem>(); }
                var q = from f in uses.Value.Fields
                        where !f.CropYears.Contains(CurrentYear)
                        orderby f.Name
                        select f;
                return q;
            }
        }

    }
}
