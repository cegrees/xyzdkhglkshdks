﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Tags;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Landdb.ViewModel.Fields {
	public class TreeFilterModel : ViewModelBase, IPageFilterViewModel {

		readonly IClientEndpoint clientEndpoint;

		public TreeFilterModel(IClientEndpoint clientEndpoint) {
			this.clientEndpoint = clientEndpoint;

			Messenger.Default.Register<DataSourceChangedMessage>(this, onDataSourceChanged);

			updateAvailableTags();
		}

		public IList<TagFilterItem> AvailableTags { get; private set; }

		public IList<TagFilterItem> SelectedTags {
			get { return AvailableTags.Where(x => x.IsChecked).ToList(); }
		}

		// load passively. not all models need this
		private IEnumerable<CropListItem> cropList;
		public IEnumerable<CropListItem> CropList {
			get {
				if (cropList == null) {
					// cropyearid cannot be instantiated in the constructor due to the order in which the viewmodels 
					// are loadded when the app is first started
					var currentCropYearId = new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear);

					var flattenedTreeView = clientEndpoint.GetView<FlattenedTreeHierarchyView>(currentCropYearId).GetValue(() => null);
					if (flattenedTreeView != null) {
						cropList = from treeItem in flattenedTreeView.Items
								   group treeItem by new Tuple<CropId, string>(treeItem.CropId, treeItem.CropName) into g
								   select new CropListItem(g.Key.Item1, g.Key.Item2);
					}
				}

				return cropList;
			}
		}

		string nameFilter = string.Empty;
		public string NameFilter {
			get { return nameFilter; }
			set {
				nameFilter = value;
				RaisePropertyChanged(() => NameFilter);
			}
		}

		private CropListItem selectedCrop;
		public CropListItem SelectedCrop {
			get { return selectedCrop; }
			set {
				selectedCrop = value;
				RaisePropertyChanged(() => SelectedCrop);
			}
		}

		private bool _isTagFilterPopupOpen;
		public bool IsTagFilterPopupOpen {
			get { return _isTagFilterPopupOpen; }
			set {
				_isTagFilterPopupOpen = value;
				RaisePropertyChanged(() => IsTagFilterPopupOpen);
			}
		}

		private bool _showUnassignedOnly;
		public bool ShowUnassignedOnly {
			get { return _showUnassignedOnly; }
			set {
				_showUnassignedOnly = value;

				if (_showUnassignedOnly && ShowAssignedOnly) { ShowAssignedOnly = false; }

				RaisePropertyChanged(() => ShowUnassignedOnly);
			}
		}

		private bool _showAssignedOnly;
		public bool ShowAssignedOnly {
			get { return _showAssignedOnly; }
			set {
				_showAssignedOnly = value;

				if (_showAssignedOnly && ShowUnassignedOnly) { ShowUnassignedOnly = false; }

				RaisePropertyChanged(() => ShowAssignedOnly);
			}
		}

		public void ClearFilter() {
			NameFilter = string.Empty;
			SelectedCrop = null;
			ShowUnassignedOnly = false;
			ShowAssignedOnly = false;

			AvailableTags.ForEach(x => x.IsChecked = false);
			onTagsChanged();
		}

		public void BeforeFilter() {
			// TODO: Make a nice filter lookup model based on tag filter input, rather than getting view each time.
		}

		public void RefreshLists() { }

		public bool FilterItem(object item) {
			AbstractTreeItemViewModel treeItem = item as AbstractTreeItemViewModel;
			if (treeItem.FilteredChildren != null) {
				treeItem.FilteredChildren.Refresh();
			}

			var filterOnName = !string.IsNullOrWhiteSpace(NameFilter);
			var filterOnTags = SelectedTags.Any();
			var filterOnCrop = SelectedCrop != null && SelectedCrop.CropId != null && SelectedCrop.CropId.Id != Guid.Empty;

			if (treeItem is CropZoneTreeItemViewModel && !ShowUnassignedOnly) {
				var cropZone = treeItem as CropZoneTreeItemViewModel;

				bool? retBool = null;

				if (filterOnName) {
					var parentField = treeItem.Parent as FieldTreeItemViewModel;
					var lowerName = NameFilter.ToLower();
					var match = cropZone.Name.ToLower().Contains(lowerName) || parentField.Name.ToLower().Contains(lowerName);
					retBool = retBool.HasValue ? retBool.Value && match : match;
				}

				if (filterOnTags) {
					var tagMatches = from czTag in cropZone.Tags
									 where AvailableTags.Any(x => x.IsChecked == true && x.Tag == czTag)
									 select czTag;

					var hasTagMatches = tagMatches.Count() == SelectedTags.Count();

					retBool = retBool.HasValue ? retBool.Value && hasTagMatches : hasTagMatches;
				}

				if (filterOnCrop) {
					var match = SelectedCrop.CropId == cropZone.CropId;
					retBool = retBool.HasValue ? retBool.Value && match : match;
				}

				return retBool.HasValue ? retBool.Value : true;
			} else if (treeItem is FieldTreeItemViewModel) {
				var field = treeItem as FieldTreeItemViewModel;

				bool? retBool = null;

				if (filterOnName) {
					var match = field.Name.ToLower().Contains(NameFilter.ToLower());
					retBool = retBool.HasValue ? retBool.Value && match : match;
				}

				var cropOrTagsSelected = filterOnTags || filterOnCrop;
				if (cropOrTagsSelected && field.FilteredChildren != null) {
					var match = field.FilteredChildren.Count > 0;
					retBool = retBool.HasValue ? retBool.Value && match : match;
				}

				if (ShowUnassignedOnly) {
					var match = !field.Children.Any();
					retBool = retBool.HasValue ? retBool.Value && match : match;
				}

				if (ShowAssignedOnly) {
					var match = field.Children.Any();
					retBool = retBool.HasValue ? retBool.Value && match : match;
				}

				return retBool.HasValue ? retBool.Value : true;
			} else if (treeItem is TagTreeItemViewModel) {
				return true;
			} else {
				if (treeItem.FilteredChildren == null) {
					return false;
				} else {
					var filterOn = filterOnName || filterOnCrop || filterOnTags || ShowAssignedOnly || ShowUnassignedOnly;

					if (filterOn) {
						return treeItem.FilteredChildren.Count > 0;
					} else {
						return true;
					}
				}
			}
		}

		public override void Cleanup() {
			base.Cleanup();
			Messenger.Default.Unregister<DataSourceChangedMessage>(this, onDataSourceChanged);
		}

		private void onTagsChanged() {
			RaisePropertyChanged(() => AvailableTags);
			RaisePropertyChanged(() => SelectedTags);
		}

		// TODO: Implement boolean methods to make filter checking easier.
		// Return true if (filter exists && item matches) || (filter is nonexistent)
		//public bool TagChecks(Abstra

		private void onDataSourceChanged(DataSourceChangedMessage message) {
			updateAvailableTags();
		}

		private void updateAvailableTags() {
			// cropyearid cannot be instantiated in the constructor due to the order in which the viewmodels 
			// are loadded when the app is first started
			var currentCropYearId = new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear);
			AvailableTags = clientEndpoint.GetView<TagListView>(currentCropYearId)
				.GetValue(new TagListView())
				.UsedTagsList
				.OrderBy(x => x)
				.Select(x => new TagFilterItem(x, onTagsChanged))
				.ToList();

			onTagsChanged();
		}
	}

	public class TagFilterItem : ViewModelBase {

		readonly Action checkChanged;

		public TagFilterItem(string tag, Action onCheckChanged) {
			Tag = tag;

			checkChanged += onCheckChanged;
		}

		public string Tag { get; private set; }

		private bool _isChecked;
		public bool IsChecked {
			get { return _isChecked; }
			set {
				_isChecked = value;

				if (checkChanged != null) { checkChanged(); }

				RaisePropertyChanged(() => IsChecked);
			}
		}

		public override string ToString() {
			return Tag;
		}
	}

	public class CropListItem {
		public CropListItem(CropId cropId, string cropName) {
			CropId = cropId;
			CropName = cropName;
		}

		public CropId CropId { get; private set; }
		public string CropName { get; private set; }

		public override string ToString() {
			return CropName;
		}
	}
}
