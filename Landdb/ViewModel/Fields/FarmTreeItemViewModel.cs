﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GalaSoft.MvvmLight;
using Landdb.Domain.ReadModels.Tree;
using System.Collections.ObjectModel;

namespace Landdb.ViewModel.Fields {
    public class FarmTreeItemViewModel : AbstractTreeItemViewModel {

        FarmId id;
        string name;
		bool isCheckboxVisible;

        public FarmTreeItemViewModel(TreeViewFarmItem farm, AbstractTreeItemViewModel parent, bool sortChildrenAlphabetically, Action<AbstractTreeItemViewModel> onCheckedChanged = null, Action<List<CropZoneId>> onParentCompleted = null, Predicate<object> filter = null, bool showCheckboxes = true)
            : base(parent, onCheckedChanged, onParentCompleted, filter) {
            id = farm.FarmId;
            name = farm.Name;
			isCheckboxVisible = showCheckboxes;

            var q = from f in farm.Fields
                    select new FieldTreeItemViewModel(f, this, sortChildrenAlphabetically, onCheckedChanged, onParentCompleted, filter, showCheckboxes); // and the donkey too...

            if (sortChildrenAlphabetically) {
                q = q.OrderBy(x => x.Name);
            }


            Children = new ObservableCollection<AbstractTreeItemViewModel>(q);
        }

        public override IIdentity Id { get { return id; } }
        public FarmId FarmId { get { return id; } }

        public string Name { get { return name; } }
		public bool IsCheckboxVisible { get { return isCheckboxVisible; } }

		internal void UpdateName(string newName) {
            name = newName;
            RaisePropertyChanged("Name");
        }

        public override string ToString() {
            return Name;
        }
    }
}
