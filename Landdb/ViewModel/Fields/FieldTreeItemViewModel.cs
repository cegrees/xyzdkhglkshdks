﻿using AgC.UnitConversion;
using Landdb.Domain.ReadModels.Tree;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Landdb.ViewModel.Fields {
	public class FieldTreeItemViewModel : AbstractTreeItemViewModel {

		FieldId id;
		string name;
		Measure area;
		bool isCheckboxVisible;

		public FieldTreeItemViewModel(TreeViewFieldItem field, AbstractTreeItemViewModel parent, bool sortChildrenAlphabetically, Action<AbstractTreeItemViewModel> onCheckedChanged = null, Action<List<CropZoneId>> onParentCompleted = null, Predicate<object> filter = null, bool showCheckboxes = true)
			: base(parent, onCheckedChanged, onParentCompleted, filter) {
			id = field.FieldId;
			name = field.Name;
			area = field.GetArea();
			isCheckboxVisible = showCheckboxes;

			var q = from cz in field.CropZones
					select new CropZoneTreeItemViewModel(cz, this, sortChildrenAlphabetically, onCheckedChanged, onParentCompleted, filter, showCheckboxes);

			if (sortChildrenAlphabetically) {
				q = q.OrderBy(x => x.Name);
			}

			Children = new ObservableCollection<AbstractTreeItemViewModel>(q);
		}

		public override IIdentity Id { get { return id; } }
		public FieldId FieldId { get { return id; } }
		public string Name { get { return name; } }
		public Measure Area { get { return area; } }
		public bool IsCheckboxVisible { get { return isCheckboxVisible; } }

		internal void UpdateName(string newName) {
			name = newName;
			RaisePropertyChanged(() => Name);
		}

		public override string ToString() {
			return Name;
		}
	}
}
