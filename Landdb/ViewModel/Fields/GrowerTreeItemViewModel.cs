﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GalaSoft.MvvmLight;
using Landdb.Domain.ReadModels.Tree;
using System.Collections.ObjectModel;

namespace Landdb.ViewModel.Fields {
    public class GrowerTreeItemViewModel : AbstractTreeItemViewModel {

        private readonly string name;
		bool isCheckboxVisible;

        public GrowerTreeItemViewModel(string name, IList<TreeViewFarmItem> farms, bool sortChildrenAlphabetically, Action<AbstractTreeItemViewModel> onCheckedChanged = null, Action<List<CropZoneId>> onParentCompleted = null, Predicate<object> filter = null, bool showCheckboxes = true)
            : base(null, onCheckedChanged, onParentCompleted, filter)
        {
            this.name = name;
			this.isCheckboxVisible = showCheckboxes;

            var q = from f in farms
                    select new FarmTreeItemViewModel(f, this, sortChildrenAlphabetically, onCheckedChanged, onParentCompleted, filter, showCheckboxes); // and the horse it rode in on...

            if (sortChildrenAlphabetically) {
                q = q.OrderBy(x => x.Name);
            }

            Children = new ObservableCollection<AbstractTreeItemViewModel>(q);
        }
            
        public string Name { get { return name; } }
		public bool IsCheckboxVisible { get { return isCheckboxVisible; } }

		public override IIdentity Id {
            get { return null; } // TODO: Maybe return DataSourceId?
        }

        public override string ToString() {
            return Name;
        }
    }
}
