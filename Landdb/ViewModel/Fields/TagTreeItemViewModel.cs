﻿using System.Collections.Generic;
using System.Linq;
using Landdb.Resources;

namespace Landdb.ViewModel.Fields {
	public class TagTreeItemViewModel : AbstractTreeItemViewModel {

		private readonly CropZoneTreeItemViewModel parentCropZone;

		public TagTreeItemViewModel(AbstractTreeItemViewModel parent) : base(parent, null, null) {
			parentCropZone = parent as CropZoneTreeItemViewModel;

			// propagates tag property changes from parent field
			// TODO: nameof when available
			parent.PropertyChanged += (sender, args) => {
				if (args.PropertyName == "Tags") {
					RaisePropertyChanged(() => Tags);
					RaisePropertyChanged(() => Status);
					RaisePropertyChanged(() => IsChanged);
				}
			};
		}

		public override IIdentity Id => null;
		public List<string> Tags => Status != MassAssignerStatus.Removed ? parentCropZone.Tags : parentCropZone.OriginalTags;
		public bool IsChanged => Status != MassAssignerStatus.Unchanged;

		public MassAssignerStatus Status {
			get {
				// avoid references to this.Tags() here in order to avoid stack overflow exceptions 

				var originallyHadTags = parentCropZone.OriginalTags != null && parentCropZone.OriginalTags.Any();

				if (parentCropZone.Status == MassAssignerStatus.Added || (!originallyHadTags && parentCropZone.Tags.Any())) {
					return MassAssignerStatus.Added;
				} else if (originallyHadTags && !parentCropZone.Tags.Any()) {
					return MassAssignerStatus.Removed;
				} else if (!parentCropZone.OriginalTags.SequenceEqual(parentCropZone.Tags)) {
					return MassAssignerStatus.Changed;
				} else {
					return MassAssignerStatus.Unchanged;
				}
			}
		}

		public override string ToString() => Tags != null && Tags.Any() ? string.Join(" ", Tags.Select(x => $"<{x}>")) : $"<{Strings.None_Text}>";
	}
}