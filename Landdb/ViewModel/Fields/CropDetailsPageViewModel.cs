﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Threading;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.Resources;
using Landdb.ViewModel.Fields.FieldDetails;
using NLog;

namespace Landdb.ViewModel.Fields {
    public class CropDetailsPageViewModel : ViewModelBase {
        readonly IClientEndpoint clientEndpoint;
        readonly Dispatcher dispatcher;
        readonly Logger log = LogManager.GetCurrentClassLogger();

        readonly IDetailsViewModelFactory detailsFactory;

        Guid currentDataSourceId;
        int currentCropYear;
        bool stopcropmapfromdoubleloading = false;

        object detailsViewModel = new HomeDetailsViewModel(null);
        AbstractTreeItemViewModel selectedTreeItem;
        MapViewModel mapViewModel;

        public CropDetailsPageViewModel(IClientEndpoint clientEndpoint, IDetailsViewModelFactory detailsFactory, Dispatcher dispatcher) {
            this.dispatcher = dispatcher;
            this.clientEndpoint = clientEndpoint;
            this.detailsFactory = detailsFactory;

            this.currentDataSourceId = ApplicationEnvironment.CurrentDataSourceId;
            this.currentCropYear = ApplicationEnvironment.CurrentCropYear;

            //mapViewModel = new MapViewModel(clientEndpoint, dispatcher);

            Messenger.Default.Register<DataSourceChangedMessage>(this, OnDataSourceChanged);

            if (currentDataSourceId != Guid.Empty && currentCropYear > 1980) {
                RebuildTree();
            }
        }

        public ObservableCollection<GrowerCropTreeItemViewModel> RootTreeItemModels { get; private set; }

        public object DetailsViewModel {
            get { return detailsViewModel; }
            set {
                detailsViewModel = value;
                RaisePropertyChanged("DetailsViewModel");
                //RaisePropertyChanged("ApplicationSummaryItems");
            }
        }

        void OnDataSourceChanged(DataSourceChangedMessage message) {
            this.currentDataSourceId = message.DataSourceId;
            this.currentCropYear = message.CropYear;

            RebuildTree();
        }

        private void RebuildTree() {
            dispatcher.BeginInvoke(new Action(() => {
                var begin = DateTime.UtcNow;
                var tree = clientEndpoint.GetView<CropTreeView>(new CropYearId(this.currentDataSourceId, this.currentCropYear)).GetValue(new CropTreeView());

                IList<TreeViewCropItem> crops = tree.Crops != null ? tree.Crops : new List<TreeViewCropItem>();

                var userSettings = clientEndpoint.GetUserSettings();
                bool sortAlphabetically = userSettings.AreFieldsSortedAlphabetically;

                //if (!stopcropmapfromdoubleloading) {
                //    //TODO need to MapViewModel and MapDisplayModel for Crop Tree
                //    mapViewModel.ClientLayer = new List<MapItem>(); // TODO: Implement map
                //    stopcropmapfromdoubleloading = true;
                //}

                RootTreeItemModels = new ObservableCollection<GrowerCropTreeItemViewModel>() { new GrowerCropTreeItemViewModel(Strings.Home_Text, crops, sortAlphabetically) };
                RootTreeItemModels[0].IsExpanded = true;
                RaisePropertyChanged("RootTreeItemModels");

                var end = DateTime.UtcNow;

                log.Debug("Tree load took " + (end - begin).TotalMilliseconds + "ms");
            }));
        }

        public AbstractTreeItemViewModel SelectedTreeItem {
            get { return selectedTreeItem; }
            set {
                selectedTreeItem = value;
                RaisePropertyChanged("SelectedTreeItem");
                if (value == null) { return; }

                detailsFactory.CreateDetailsViewModel(SelectedTreeItem.Id, mapViewModel.ClientLayer, SelectedTreeItem,
                    viewModel => {
                        dispatcher.BeginInvoke(new Action(() => { // if waiting, this could come through a worker thread, so use the dispatcher to route it appropriately.
                            DetailsViewModel = viewModel;
                            //TODO need to MapViewModel and MapDisplayModel for Crop Tree
                            //mapViewModel.DetailsViewModel = viewModel;
                        }));
                    });

                //ApplicationSummary.RefreshSummaryItems(value);
            }
        }


    }
}
