﻿using AgC.UnitConversion;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Landdb.Resources;

namespace Landdb.ViewModel.Fields {
	public class CropZoneTreeItemViewModel : AbstractTreeItemViewModel {

		readonly IClientEndpoint clientEndpoint;

		readonly List<string> originalTags;

		CropZoneId id;
		string name;
		string cropName;
		bool isPerennial;
		CropId crop;
		bool isCheckboxVisible;
		MassAssignerStatus status;
		List<string> tags;

		public CropZoneTreeItemViewModel(TreeViewCropZoneItem cropZone, AbstractTreeItemViewModel parent, bool sortChildrenAlphabetically, Action<AbstractTreeItemViewModel> onCheckedChanged = null, Action<List<CropZoneId>> onParentCompleted = null, Predicate<object> filter = null, bool showCheckboxes = true)
			: base(parent, onCheckedChanged, onParentCompleted, filter) {
			this.id = cropZone.CropZoneId;
			this.name = cropZone.Name;
			this.isPerennial = cropZone.IsPerennial;
			this.crop = cropZone.CropId;
			this.isCheckboxVisible = showCheckboxes;

			// force copies with tolist
			tags = cropZone.Tags.ToList();
			originalTags = cropZone.Tags.ToList();

			clientEndpoint = ServiceLocator.Get<IClientEndpoint>();
			if (clientEndpoint != null) {
				cropName = clientEndpoint.GetMasterlistService().GetCropDisplay(cropZone.CropId);
			} else {
				cropName = Strings.UnknownCrop_Text;
			}

			if (string.IsNullOrWhiteSpace(this.name)) {
				this.name = cropName;
			}

			updateChildren();

			this.Area = cropZone.GetArea();
		}

		private void updateChildren() {
			if (tags.Any()) {
				// only one child... the tag list
				Children = new ObservableCollection<AbstractTreeItemViewModel>(
					new List<AbstractTreeItemViewModel>() { new TagTreeItemViewModel(this) }
				);

				RaisePropertyChanged(() => Children);
			} else if (Children != null && Status == MassAssignerStatus.Added) {
				Children.Clear();
			}

			RaisePropertyChanged(() => Tags);
		}

		public override IIdentity Id { get { return id; } }
		public CropZoneId CropZoneId { get { return id; } }
		public string Name { get { return name; } }
		public CropId CropId { get { return crop; } }
		public string CropName { get { return cropName; } }
		public bool IsCheckboxVisible { get { return isCheckboxVisible; } }
		public MassAssignerStatus Status { get { return status; } }
		public List<string> OriginalTags { get { return originalTags; } }
		public List<string> Tags { get { return tags; } }
		public bool IsPerennial { get { return isPerennial; } }
		public Measure Area { get; private set; }
		public bool IsEditor { get; set; }

		public string DetailsText {
			get {
				if (name == cropName) {
					return Area.ToString();
				} else {
					return string.Format("{0}  {1}", cropName, Area.ToString());
				}
			}
		}

		public bool IsChanged {
			get { return Status != MassAssignerStatus.Unchanged; }
		}

		public CropZoneDataUsageStatistics UsageStatistics {
			get { return clientEndpoint.GetView<CropZoneDataUsageStatistics>(this.id).GetValue(() => null); }
		}

		public override bool CanBeDeleted() {
			return UsageStatistics == null || !UsageStatistics.HasStats();
		}

		internal void UpdateName(string newName) {
			name = newName;
			RaisePropertyChanged(() => Name);
		}

		internal void UpdateArea(Measure newArea) {
			Area = newArea;
			RaisePropertyChanged(() => Area);
		}

		internal void UpdateCrop(CropId newCropId, string newCropName) {
			crop = newCropId;
			cropName = newCropName;
			RaisePropertyChanged(() => CropId);
			RaisePropertyChanged(() => CropName);
		}

		internal void UpdateStatus(MassAssignerStatus newStatus) {
			status = newStatus;
			RaisePropertyChanged(() => Status);
			RaisePropertyChanged(() => IsChanged);
		}

		internal void UpdateTags(IEnumerable<string> newTags) {
			tags = newTags.ToList();
			RaisePropertyChanged(() => Tags);

			updateChildren();
		}

		public override string ToString() {
			return Name;
		}
	}
}