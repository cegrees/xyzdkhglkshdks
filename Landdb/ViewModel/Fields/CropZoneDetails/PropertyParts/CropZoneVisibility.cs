﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.Resources;

namespace Landdb.ViewModel.Fields.CropZoneDetails.PropertyParts
{
    public class CropZoneVisibility : ViewModelBase
    {
        IClientEndpoint clientEndpoint;
        Dispatcher dispatcher;
        Landdb.Domain.ReadModels.Tree.CropZoneDetailsView czView;

        string spatialLabelPosition = string.Empty;
        float horizontalLabelRotation = 0;
        bool optionalVisibility = true;
        int currentCropYear;
        List<string> irr = new List<string>();
        string optionalVisibilityText = string.Empty;

        public CropZoneVisibility(IClientEndpoint clientEndpoint, Dispatcher dispatcher, Landdb.Domain.ReadModels.Tree.CropZoneDetailsView czView, int currentCropYear)
        {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            this.czView = czView;
            this.currentCropYear = currentCropYear;

            irr.Add(Strings.Yes_Text);
            irr.Add(Strings.No_Text);

            OptionalVisibility = true;
            HorizontalLabelRotation = 0;
            SpatialLabelPosition = string.Empty;

            UpdateFromProjection(czView);

            ShowEditorCommand = new RelayCommand(ShowEditor);
            CancelCommand = new RelayCommand(Cancel);
            UpdateCommand = new RelayCommand(Update);
        }

        public ICommand ShowEditorCommand { get; private set; }
        public ICommand UpdateCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }


        public List<string> Irr {
            get { return irr; }
        }
        public string OptionalVisibilityText {
            get { return optionalVisibilityText; }
            set {
                if (value != null) {
                    optionalVisibilityText = value.ToString();
                    RaisePropertyChanged("OptionalVisibilityText");
                }
            }
        }
        public bool OptionalVisibility {
            get {
                if (optionalVisibilityText == "No") {
                    return false;
                }
                return true;
            }
            set {
                optionalVisibility = value;
                RaisePropertyChanged("OptionalVisibility");
            }
        }

        public float HorizontalLabelRotation {
            get { return horizontalLabelRotation; }
            set {
                horizontalLabelRotation = value;
                RaisePropertyChanged("HorizontalLabelRotation");
            }
        }

        public string SpatialLabelPosition
        {
            get { return spatialLabelPosition; }
            set
            {
                spatialLabelPosition = value;
                RaisePropertyChanged("SpatialLabelPosition");
            }
        }

        public bool HasData
        {
            get
            {
                return true;
            }
        }

        void UpdateFromProjection(Domain.ReadModels.Tree.CropZoneDetailsView czView)
        {
            SpatialLabelPosition = czView.SpatialLabelPosition;
            HorizontalLabelRotation = czView.HorizontalLabelRotation;
            OptionalVisibility = czView.OptionalVisibility;
            if (!czView.OptionalVisibility) {
                OptionalVisibilityText = Strings.No_Text;
            }
            else {
                OptionalVisibilityText = Strings.Yes_Text;
            }
            RaisePropertyChanged("OptionalVisibility");
            RaisePropertyChanged("OptionalVisibilityText");
        }

        bool IsInfoChanged()
        {
            return OptionalVisibility != czView.OptionalVisibility;
        }

        void Cancel()
        {
            UpdateFromProjection(czView);
            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
        }

        void Update()
        {
            if (IsInfoChanged())
            {
                var cropzonelabelinfo = new UpdateCropZoneLabelLocation(czView.Id, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), SpatialLabelPosition, (float)HorizontalLabelRotation, currentCropYear, OptionalVisibility);
                clientEndpoint.SendOne(cropzonelabelinfo);
            }

            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
            RaisePropertyChanged("HasData");
        }
        void ShowEditor()
        {
            Messenger.Default.Send<ShowPopupMessage>(new ShowPopupMessage() { ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Fields.Popups.UpdateFieldVisibilityView", "editFsa", this) });
        }
    }
}
