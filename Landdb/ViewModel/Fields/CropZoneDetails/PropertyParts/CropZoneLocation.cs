﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Person;
using Landdb.Infrastructure.Messages;
using ThinkGeo.MapSuite.Core;
using Landdb.ViewModel.Secondary.Map;

namespace Landdb.ViewModel.Fields.CropZoneDetails.PropertyParts
{
    public class CropZoneLocation : ViewModelBase
    {
        IClientEndpoint clientEndpoint;
        Dispatcher dispatcher;
        Landdb.Domain.ReadModels.Tree.CropZoneDetailsView czView;
        MapSettings mapsettings;
        string center;


        public CropZoneLocation(IClientEndpoint clientEndpoint, Dispatcher dispatcher, Landdb.Domain.ReadModels.Tree.CropZoneDetailsView czView)
        {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            this.czView = czView;
            this.mapsettings = clientEndpoint.GetMapSettings();

            UpdateFromProjection(czView);

            ShowEditorCommand = new RelayCommand(ShowEditor);
            CancelCommand = new RelayCommand(Cancel);
            UpdateCommand = new RelayCommand(Update);
        }

        public ICommand ShowEditorCommand { get; private set; }
        public ICommand UpdateCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }

        public string CenterLatLong {
            get { return center; }
            set {
                center = value;
                RaisePropertyChanged("CenterLatLong");
                RaisePropertyChanged("FormatedCenterLatLong");
            }
        }

        public string FormatedCenterLatLong {
            get {
                return DecimalDegrees.ddstring_To_DMs(center, mapsettings.DisplayCoordinateFormat);
            }
        }

        public bool HasData
        {
            get
            {
                return !string.IsNullOrWhiteSpace(CenterLatLong);
            }
        }

        void UpdateFromProjection(Domain.ReadModels.Tree.CropZoneDetailsView czView)
        {
            CenterLatLong = czView.CenterLatLong;
        }

        void GetPeople()
        {
            //var people = clientEndpoint.GetView<PersonList>();
        }

        bool IsInfoChanged()
        {
            return CenterLatLong != czView.CenterLatLong;
        }

        void Cancel()
        {
            UpdateFromProjection(czView);
            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
        }

        void Update()
        {
            if (IsInfoChanged())
            {
                UpdateCropZoneLocation infoCommand = new UpdateCropZoneLocation(czView.Id, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), CenterLatLong);
                clientEndpoint.SendOne(infoCommand);
            }

            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
            RaisePropertyChanged("HasData");
        }
        void ShowEditor()
        {
            RaisePropertyChanged("State");
            Messenger.Default.Send<ShowPopupMessage>(new ShowPopupMessage() { ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Fields.Popups.UpdateCropZoneLocationView", "editFsa", this) });
        }
    }
}
