﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Fields {
	public enum MassAssignerStatus {
		Unchanged,
		Added,
		Changed,
		Removed
	}
}
