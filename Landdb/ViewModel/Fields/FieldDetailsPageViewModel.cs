﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Map;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ReportModels.CropZone;
using Landdb.ViewModel.Fields.FieldDetails;
using Landdb.ViewModel.Fields.FieldDetails.MapModels;
using Landdb.ViewModel.Fields.MassEditorAssigner;
using Landdb.ViewModel.Secondary.Map;
using NLog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Fields {
	public class FieldDetailsPageViewModel : ViewModelBase {

		readonly IClientEndpoint clientEndpoint;
		readonly Dispatcher dispatcher;
		readonly Logger log = LogManager.GetCurrentClassLogger();

		readonly IDetailsViewModelFactory detailsFactory;

		CropYearId currentCropYearId;

		object detailsViewModel = new HomeDetailsViewModel(null);
		AbstractTreeItemViewModel selectedTreeItem;
		MapViewModel mapViewModel;
		CreateTreeItemViewModel createItemViewModel;

		bool isMassCropPopupOpen;
		bool isLoadingTree;
		bool isFilterVisible = false;
		string filterHeader = string.Empty;
		MapSettings mapsettings;
	    private AgC.UnitConversion.Area.AreaUnit agcAreaUnit { get; }
        private ThinkGeo.MapSuite.Core.AreaUnit mapAreaUnit { get; }
	    ThinkGeo.MapSuite.Core.DistanceUnit mapDistanceUnit { get; }
        string areaUnitString = string.Empty;
        bool displayareainfieldlabel = false;
		bool displayareaonlylabel = false;

        // TODO: Obsolete this constructor?
        public FieldDetailsPageViewModel(IClientEndpoint clientEndpoint) : this(clientEndpoint, new DetailsViewModelFactory(clientEndpoint), Application.Current.Dispatcher) { }

		public FieldDetailsPageViewModel(IClientEndpoint clientEndpoint, IDetailsViewModelFactory detailsFactory, Dispatcher dispatcher) {
			this.dispatcher = dispatcher;
			this.clientEndpoint = clientEndpoint;
			this.detailsFactory = detailsFactory;
            this.detailsViewModel = new HomeDetailsViewModel(this.clientEndpoint);
			currentCropYearId = new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear);

			mapsettings = clientEndpoint.GetMapSettings();
			areaUnitString = mapsettings.MapAreaUnit;
			agcAreaUnit = AgC.UnitConversion.UnitFactory.GetUnitByName(areaUnitString) as AgC.UnitConversion.Area.AreaUnit;
			mapAreaUnit = MapUnitFactory.AreaMeasureConversion(agcAreaUnit);
			mapDistanceUnit = MapUnitFactory.AreaDistanceMeasureConversion(agcAreaUnit);
			if (mapsettings.DisplayAreaInFieldLabel.HasValue) {
				displayareainfieldlabel = mapsettings.DisplayAreaInFieldLabel.Value;
			}
            if (mapsettings.DisplayAreaOnlyLabel.HasValue) {
                displayareaonlylabel = mapsettings.DisplayAreaOnlyLabel.Value;
            }

            mapViewModel = new MapViewModel(clientEndpoint, dispatcher);
			createItemViewModel = new CreateTreeItemViewModel(clientEndpoint, OnNewItemCreated) { CurrentCropYear = currentCropYearId.Id, CurrentDataSourceId = currentCropYearId.DataSourceId };
			YieldSummary = new YieldSummaryViewModel(clientEndpoint, dispatcher);
			ApplicationSummary = new FieldSummaryViewModel(YieldSummary, clientEndpoint, dispatcher);

			Messenger.Default.Register<DataSourceChangedMessage>(this, OnDataSourceChanged);
			Messenger.Default.Register<RemoteEventReceived>(this, OnRemoteEventReceived);

			CreateNewItemCommand = new RelayCommand(ShowCreateNewItemPopup);
			RemoveCommand = new RelayCommand(RemoveSelectedItem);

			ApplyFilterCommand = new RelayCommand(ApplyFilter);
			ClearFilterCommand = new RelayCommand(ClearFilter);

			FilterModel = new TreeFilterModel(clientEndpoint);

			ShowMassAssigner = new RelayCommand(ShowMassAssignScreen);
			ShowSplitField = new RelayCommand(SplitField);
			Cancel = new RelayCommand(CancelMassAssign);

			HideRemovalPopupCommand = new RelayCommand(HideRemovalPopup);

			if (currentCropYearId.DataSourceId != Guid.Empty && currentCropYearId.Id > 1980) {
				RebuildFarms();
			}

			Messenger.Default.Register<UpdateTreeMessage>(this, OnTreeUpdated);
		}

		private void HideRemovalPopup() {
			IsRemovalPopupOpen = false;
		}

		public ICommand CreateNewItemCommand { get; }
		public ICommand RemoveCommand { get; }
		public ICommand ApplyFilterCommand { get; }
		public ICommand ClearFilterCommand { get; }
		public ICommand ShowMassAssigner { get; }
		public ICommand Cancel { get; set; }
		public ICommand ShowSplitField { get; set; }
		public ICommand HideRemovalPopupCommand { get; }
		public IPageFilterViewModel FilterModel { get; protected set; }

		public bool IsMassCropPopupOpen {
			get { return isMassCropPopupOpen; }
			set {
				isMassCropPopupOpen = value;
				RaisePropertyChanged(() => IsMassCropPopupOpen);
				RaisePropertyChanged(() => IsField);
			}
		}

		public bool IsField {
			get {
				if (RootTreeItemModelsView == null) {
					return false;
				} else {
					AbstractTreeItemViewModel parentNode = SelectedTreeItem;

					if (SelectedTreeItem == null) {
						parentNode = RootTreeItemModels.FirstOrDefault();
					}

					if (parentNode is FieldTreeItemViewModel) {
						return true;
					} else {
						//no cz spliter
						return false;
					}
				}
			}
		}

		public bool IsLoadingTree {
			get { return isLoadingTree; }
			set {
				isLoadingTree = value;
				RaisePropertyChanged(() => IsLoadingTree);
			}
		}

		public ObservableCollection<GrowerTreeItemViewModel> RootTreeItemModels { get; private set; }
		public CollectionView RootTreeItemModelsView { get; private set; }

		public object DetailsViewModel {
			get { return detailsViewModel; }
			set {
				detailsViewModel = value;
				RaisePropertyChanged(() => DetailsViewModel);
				RaisePropertyChanged(() => ApplicationSummary);
			}
		}

		public FieldSummaryViewModel ApplicationSummary { get; }
		public YieldSummaryViewModel YieldSummary { get; }

		public MapViewModel MapViewModel {
			get { return mapViewModel; }
			private set {
				mapViewModel = value;
				RaisePropertyChanged(() => MapViewModel);
			}
		}

		public AbstractTreeItemViewModel SelectedTreeItem {
			get { return selectedTreeItem; }
			set {
				selectedTreeItem = value;
				IsMassCropPopupOpen = false;
				IsRemovalPopupOpen = false;
				RaisePropertyChanged(() => IsMassCropPopupOpen);
				RaisePropertyChanged(() => SelectedTreeItem);
				if (value == null) { return; }

				detailsFactory.CreateDetailsViewModel(SelectedTreeItem.Id, mapViewModel.ClientLayer, SelectedTreeItem,
					viewModel => {
						dispatcher.BeginInvoke(new Action(() => { // if waiting, this could come through a worker thread, so use the dispatcher to route it appropriately.
							DetailsViewModel = viewModel;
							mapViewModel.DetailsViewModel = viewModel;
							mapViewModel.SelectedTreeItem = selectedTreeItem;
						}));
					});

				// NOTE: For perf, it's currently important that YieldSummary be refreshed before ApplicationSummary.
				// This is because ApplicationSummary's refresh depends on the yield model's items being present.
				// In the future, we really should seek to decouple these so that ordering doesn't matter.
				YieldSummary.RefreshSummaryItems(value);
				ApplicationSummary.RefreshSummaryItems(value);

				((RelayCommand)RemoveCommand).RaiseCanExecuteChanged();
			}
		}

		void OnTreeUpdated(UpdateTreeMessage message) {
			RebuildFarms();
			RaisePropertyChanged(() => RootTreeItemModels);
		}

		void OnDataSourceChanged(DataSourceChangedMessage message) {
			currentCropYearId = new CropYearId(message.DataSourceId, message.CropYear);
			mapViewModel.CropYear = message.CropYear;

			if (createItemViewModel != null) {
				createItemViewModel.CurrentCropYear = message.CropYear;
				createItemViewModel.CurrentDataSourceId = message.DataSourceId;
			}

			RebuildFarms();
		}

		void OnRemoteEventReceived(RemoteEventReceived message) {
			RebuildFarms();
		}

		private void RebuildFarms() {
			dispatcher.BeginInvoke(new Action(() => {
				IsLoadingTree = true;
				var begin = DateTime.UtcNow;
				var years = clientEndpoint.GetView<FieldTreeView>(new DataSourceId(currentCropYearId.DataSourceId));

				var yearItem = (years.HasValue && years.Value.CropYearList.ContainsKey(currentCropYearId.Id)) ? years.Value.CropYearList[currentCropYearId.Id] : new TreeViewYearItem();
				IList<TreeViewFarmItem> farms = yearItem.Farms != null ? yearItem.Farms : new List<TreeViewFarmItem>();

				mapViewModel.ClientLayer = yearItem.Maps.Where(m => m.IsVisible).ToList();

				var userSettings = clientEndpoint.GetUserSettings();
				bool sortAlphabetically = userSettings.AreFieldsSortedAlphabetically;

				RootTreeItemModels = new ObservableCollection<GrowerTreeItemViewModel>() { new GrowerTreeItemViewModel(Strings.Home_Text, farms, sortAlphabetically, filter: FilterModel.FilterItem) };
				RootTreeItemModels[0].IsExpanded = true;

				dispatcher.BeginInvoke(new Action(() => {
					RootTreeItemModelsView = (CollectionView)CollectionViewSource.GetDefaultView(RootTreeItemModels);
					RootTreeItemModelsView.Filter = FilterModel.FilterItem;
					GenerateFilterHeader();
					RaisePropertyChanged(() => RootTreeItemModelsView);
				}));

				RaisePropertyChanged(() => RootTreeItemModels);

				var end = DateTime.UtcNow;
				IsLoadingTree = false;

				log.Debug($"Tree load took {(end - begin).TotalMilliseconds}ms");
			}));
		}

		void RemoveItem(AbstractTreeItemViewModel vm) {
			if (vm is GrowerTreeItemViewModel) {
				return;
			}

			if (vm is FarmTreeItemViewModel) {
				clientEndpoint.SendOne(new RemoveFarmFromCropYear(((FarmTreeItemViewModel)vm).FarmId, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), currentCropYearId.Id));
			}

			if (vm is FieldTreeItemViewModel) {
				clientEndpoint.SendOne(new RemoveFieldFromCropYear(((FieldTreeItemViewModel)vm).FieldId, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), currentCropYearId.Id));
			}

			if (vm is CropZoneTreeItemViewModel) {
				if (((CropZoneTreeItemViewModel)vm).IsPerennial) {
					clientEndpoint.SendOne(new RemovePerennialCropZoneFromYear(((CropZoneTreeItemViewModel)vm).CropZoneId, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), currentCropYearId.Id));
				} else {
					clientEndpoint.SendOne(new RemoveAnnualCropZone(((CropZoneTreeItemViewModel)vm).CropZoneId, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), currentCropYearId.Id));
				}
			}

			vm.Parent.Children.Remove(vm);
		}

		void ShowMassAssignScreen() {
			IsMassCropPopupOpen = false;

			var vm = new MassEditorAssignerViewModel(clientEndpoint, dispatcher, currentCropYearId, finalizedVm => {
				if (finalizedVm != null) {
					var commandsToExecute = finalizedVm.GetDomainCommands();
					foreach (var cmd in commandsToExecute) {
						clientEndpoint.SendOne(cmd);
					}

					// hack: institute a delay and then force a tree refresh with a datasource changed msg
					// TODO: manually modify tree with "dummy" changes
					Thread.Sleep(2000);

					Messenger.Default.Send(new DataSourceChangedMessage() {
						DataSourceId = currentCropYearId.DataSourceId,
						CropYear = currentCropYearId.Id,
					});
				}

				Messenger.Default.Send(new ShowMainViewMessage());
			});

			Messenger.Default.Send(new ChangeScreenDescriptorMessage() {
				ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Fields.MassEditorAssigner.MassEditorAssignerView", "MassAssigner", vm)
			});
		}

		void CancelMassAssign() {
			Messenger.Default.Send(new ShowMainViewMessage());
		}

		void SplitField() {
			IsMassCropPopupOpen = false;

			MapViewModel.CropzoneBoundaryCommand.Execute(null);
		}

		void ShowCreateNewItemPopup() {
			AbstractTreeItemViewModel parentNode = SelectedTreeItem;

			if (RootTreeItemModels == null) { return; } // prevents an ArgumentNullException when RootTreeItemModels hasn't been initialized yet.

			if (SelectedTreeItem == null) {
				parentNode = RootTreeItemModels.FirstOrDefault();
			}

			if (SelectedTreeItem is CropZoneTreeItemViewModel) {
				parentNode = SelectedTreeItem.Parent;
				createItemViewModel.CreatorName = Strings.CreatedFromCropZone_Text;
			} else if (SelectedTreeItem is FieldTreeItemViewModel) {
				createItemViewModel.CreatorName = Strings.CreatedFromField_Text;
			}

			createItemViewModel.SelectedParentNode = parentNode;

			ScreenDescriptor descriptor = null;

			// Toggle Reported Area UI(ReportedArea)
			createItemViewModel.DisplayReportedAreaUI = false;
			if (parentNode is GrowerTreeItemViewModel) {
				descriptor = new ScreenDescriptor("Landdb.Views.Fields.Popups.AddFarmPopupView", "AddFarm", createItemViewModel);
			} else if (parentNode is FarmTreeItemViewModel) {
				descriptor = new ScreenDescriptor("Landdb.Views.Fields.Popups.AddFieldPopupView", "AddField", createItemViewModel);
			} else if (parentNode is FieldTreeItemViewModel) {
				descriptor = new ScreenDescriptor("Landdb.Views.Fields.Popups.AddCropZonePopupView", "AddCz", createItemViewModel);
			}

			if (IsFiltered()) {
				ClearFilterCommand.Execute(null);
			}

			Messenger.Default.Send(new ShowPopupMessage() { ScreenDescriptor = descriptor });
		}

		void OnNewItemCreated(AbstractTreeItemViewModel vm) {
			if (SelectedTreeItem is CropZoneTreeItemViewModel && vm is CropZoneTreeItemViewModel) {
				var maps = clientEndpoint.GetView<Landdb.Domain.ReadModels.Map.ItemMap>(SelectedTreeItem.Id);
				if (maps.HasValue) {
                    ItemMapItem mri = GetMostRecentShapeByYear(maps);
                    //Landdb.Domain.ReadModels.Map.ItemMapItem mri = maps.Value.MostRecentMapItem;
					if (mri != null) {
						try {
							string spatialData = mri.MapData;
							var feature = new ThinkGeo.MapSuite.Core.Feature(mri.MapData);
							ThinkGeo.MapSuite.Core.AreaBaseShape testbase = feature.GetShape() as ThinkGeo.MapSuite.Core.AreaBaseShape;
							string cenlatlon = string.Empty;
							double area = testbase.GetArea(ThinkGeo.MapSuite.Core.GeographyUnit.DecimalDegree, mapAreaUnit);
							ThinkGeo.MapSuite.Core.PointShape centerpoint = testbase.GetCenterPoint();
							ThinkGeo.MapSuite.Core.Vertex geoVertex1 = new ThinkGeo.MapSuite.Core.Vertex(centerpoint.X, centerpoint.Y);
							string _DecimalDegreeString = Math.Round(geoVertex1.Y, 5) + ", " + Math.Round(geoVertex1.X, 5);
							string longlat = DecimalDegrees.d_To_DMs(geoVertex1.X, 2, DecimalDegrees.Type.Longitude) + "  " + DecimalDegrees.d_To_DMs(geoVertex1.Y, 2, DecimalDegrees.Type.Latitude);
							string longlatAG = DecimalDegrees.d_To_Dm(geoVertex1.X, 4, DecimalDegrees.Type.Longitude) + "  " + DecimalDegrees.d_To_Dm(geoVertex1.Y, 4, DecimalDegrees.Type.Latitude);
							cenlatlon = _DecimalDegreeString;
							var czVM2 = vm as CropZoneTreeItemViewModel;
							var saveBoundaryCommand = new ChangeCropZoneBoundary(czVM2.CropZoneId, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), spatialData, "WKT", area, areaUnitString, ApplicationEnvironment.CurrentCropYear);
							clientEndpoint.SendOne(saveBoundaryCommand);
							if (cenlatlon != string.Empty) {
								var saveUpdateCropZoneLocationCommand = new UpdateCropZoneLocation(czVM2.CropZoneId, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), cenlatlon);
								clientEndpoint.SendOne(saveUpdateCropZoneLocationCommand);
							}
							((CropZoneTreeItemViewModel)vm).UpdateArea(((CropZoneTreeItemViewModel)SelectedTreeItem).Area);
						} catch { }
					}
				}
			}

			if (!createItemViewModel.IsContinuouslyAdding) {
				vm.IsSelected = true;
				SelectedTreeItem = vm;
			}
		}

		//void Measure GetArea() {
		//    if (ReportedArea != null && !string.IsNullOrWhiteSpace(ReportedAreaUnit)) {
		//        return UnitFactory.GetUnitByName(ReportedAreaUnit).GetMeasure(ReportedArea.Value);
		//    }
		//    else if (BoundaryArea != null && !string.IsNullOrWhiteSpace(BoundaryAreaUnit)) {
		//        return AgC.UnitConversion.UnitFactory.GetUnitByName(BoundaryAreaUnit).GetMeasure(BoundaryArea.GetValueOrDefault(0));
		//    }
		//    else {
		//        return AgC.UnitConversion.Area.Acre.Self.GetMeasure(0);
		//    }
		//}


		public bool IsFilterVisible {
			get { return isFilterVisible; }
			set {
				isFilterVisible = value;
				RaisePropertyChanged(() => IsFilterVisible);
			}
		}

		private string removalPopupText;
		public string RemovalPopupText {
			get { return removalPopupText; }
			set {
				removalPopupText = value;
				RaisePropertyChanged(() => RemovalPopupText);
			}
		}

		private bool isRemovalPopupOpen;
		public bool IsRemovalPopupOpen {
			get { return isRemovalPopupOpen; }
			set {
				isRemovalPopupOpen = value;
				RaisePropertyChanged(() => IsRemovalPopupOpen);
			}
		}

		public string FilterHeader {
			get { return filterHeader; }
			set {
				filterHeader = value;
				RaisePropertyChanged(() => FilterHeader);
			}
		}

		void ClearFilter() {
			if (FilterModel != null && RootTreeItemModelsView != null) {
				FilterModel.ClearFilter();
				FilterModel.BeforeFilter();

				if (IsFiltered()) {					
					RootTreeItemModelsView.Refresh();
					GenerateFilterHeader();
					IsFilterVisible = false;
				}
			}
		}

		void ApplyFilter() {
			if (FilterModel != null && RootTreeItemModelsView != null) {
				FilterModel.BeforeFilter();
				RootTreeItemModelsView.Refresh();
				GenerateFilterHeader();
				IsFilterVisible = false;
			}
		}

		bool IsFiltered() {
			var originalCount = (from r in RootTreeItemModels
								 from fa in r.Children
								 select fa.Children.Count).Sum();
			var filteredCount = (from r in RootTreeItemModels
								 from fa in r.Children
								 select fa.FilteredChildren.Count).Sum();
			return originalCount != filteredCount;
		}

		void GenerateFilterHeader() {
			var originalCount = (from r in RootTreeItemModels
								 from fa in r.Children
								 select fa.Children.Count).Sum();
			var filteredCount = (from r in RootTreeItemModels
								 from fa in r.Children
								 select fa.FilteredChildren.Count).Sum();

			string entityPluralityAwareName = originalCount == 1 ? Strings.Field_Text.ToLower() : Strings.Fields_Text.ToLower();

			if (originalCount != filteredCount) {
				FilterHeader = String.Format(Strings.FilteredCountOfOriginalCount_Text, filteredCount, originalCount, entityPluralityAwareName);
			} else {
				//FilterHeader = string.Format("{0} {1}", filteredCount, entityPluralityAwareName);
				FilterHeader = Strings.AllFields_Text;
			}
		}

		private void RemoveSelectedItem() {
			// store the item to remove, as it can get changed before the user confirms removal
			var itemToRemove = SelectedTreeItem;

			if (itemToRemove != null && itemToRemove.CanBeDeleted()) {
				DialogFactory.ShowYesNoDialog(Strings.RemoveItem_Text.ToLower(), String.Format(Strings.AreYouSureYouWantToRemoveThis_Text, itemToRemove.ToString()), () => {
					RemoveItem(itemToRemove);
				}, () => { });
			} else {
				if (itemToRemove is GrowerTreeItemViewModel) {
					RemovalPopupText = Strings.GrowerMayNotBeRemoved_Text;
				} else if (itemToRemove is FarmTreeItemViewModel) {
					RemovalPopupText = String.Format(Strings.ItemMayNotBeRemovedBecauseContainsFields_Format_Text, ((FarmTreeItemViewModel)itemToRemove).Name);
				} else if (itemToRemove is FieldTreeItemViewModel) {
					RemovalPopupText = String.Format(Strings.ItemMayNotBeRemovedBecauseContainsCropzones_Format_Text, ((FieldTreeItemViewModel)itemToRemove).Name);
				} else if (itemToRemove is CropZoneTreeItemViewModel) {
					var cz = (CropZoneTreeItemViewModel)itemToRemove;
					RemovalPopupText = String.Format(Strings.ItemMayNotBeRemovedBecauseItHasBeenUsedIn_Format_Text, cz.Name) + "\n\n";

					var recordDescriptions = new List<string>();

					if (cz.UsageStatistics.ApplicationCount > 0) { recordDescriptions.Add(String.Format(Strings.CountOfApplications_Text, cz.UsageStatistics.ApplicationCount)); }
					if (cz.UsageStatistics.WorkOrderCount > 0) { recordDescriptions.Add(String.Format(Strings.CountOfWorkorders_Text, cz.UsageStatistics.WorkOrderCount)); }
					if (cz.UsageStatistics.PlanCount > 0) { recordDescriptions.Add(String.Format(Strings.CountOfPlans_Text, cz.UsageStatistics.PlanCount)); }
					if (cz.UsageStatistics.RentContractIds.Any()) { recordDescriptions.Add(String.Format(Strings.CountOfRentContracts_Text, cz.UsageStatistics.RentContractIds.Count)); }
					if (cz.UsageStatistics.ProductionContractIds.Any()) { recordDescriptions.Add(String.Format(Strings.CountOfProductionContracts_Text, cz.UsageStatistics.ProductionContractIds.Count)); }
					if (cz.UsageStatistics.HarvestLoadCount > 0) { recordDescriptions.Add(String.Format(Strings.CountOfHarvestLoads_Text, cz.UsageStatistics.HarvestLoadCount)); }

					RemovalPopupText += string.Join("\n", recordDescriptions);
				}

				IsRemovalPopupOpen = true;
			}
		}

        private static ItemMapItem GetMostRecentShapeByYear(Lokad.Cqrs.Maybe<ItemMap> maps_a)
        {
            ItemMapItem item_map_item = null;
            if (maps_a.HasValue && maps_a.Value.MostRecentMapItem != null && maps_a.Value.MostRecentMapItem.CropYearChangeOccurred <= Infrastructure.ApplicationEnvironment.CurrentCropYear)
            {
                item_map_item = maps_a.Value.MostRecentMapItem;
            }
            else
            {
                ItemMapItem currentyearmostrecentboundarychange = (from md in maps_a.Value.MapItems where (md.CropYearChangeOccurred <= Infrastructure.ApplicationEnvironment.CurrentCropYear) select md).LastOrDefault();
                if (currentyearmostrecentboundarychange != null && !string.IsNullOrEmpty(currentyearmostrecentboundarychange.MapData))
                {
                    item_map_item = currentyearmostrecentboundarychange;
                }
                if (item_map_item == null)
                {
                    item_map_item = maps_a.Value.MostRecentMapItem;
                }
            }
            return item_map_item;
        }
    }
}