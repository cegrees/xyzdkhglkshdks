﻿using System;

namespace Landdb.ViewModel {
	public class ScreenDescriptor {
		public ScreenDescriptor(string viewType, string title, object viewModel = null) {
			Key = viewType;
			Title = title;
			ViewModel = viewModel;
		}

		public ScreenDescriptor(Type viewType, string title, object viewModel = null) : this(viewType.FullName, title, viewModel) { }

		public string Key { get; set; }
		public string Title { get; set; }
		public object ViewModel { get; set; }
	}
}
