﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Landdb.ViewModel.Farms.PropertyParts
{
    public class UpdateFarmLocationInfo : ViewModelBase
    {
        IClientEndpoint endpoint;
        FarmDetailsView details;
        State state;


        public UpdateFarmLocationInfo(IClientEndpoint endpoint, FarmDetailsView details)
        {
            this.endpoint = endpoint;
            this.details = details;

            UpdateLocation = new RelayCommand(Update);
            CancelLocation = new RelayCommand(Cancel);

            UpdateFromProjection();
        }

        public ICommand UpdateLocation { get; set; }
        public ICommand CancelLocation { get; set; } 

        public County County { get; set; }

        public State State {
            get
            {
                return state;
            }
            set
            {
                state = value;
                if (state != null)
                {
                    CountyList = state.Counties.ToList();
                    RaisePropertyChanged("CountyList");
                }
                RaisePropertyChanged("State");
            }
        }

        public List<State> StateList { get; set; }
        public List<County> CountyList { get; set; }

        void Update() 
        {
            var state = State != null ? State.Name : string.Empty;
            var county = County != null ? County.Name : string.Empty;
            UpdateFarmLocation farmCommand = new UpdateFarmLocation(details.Id, endpoint.GenerateNewMetadata(), state, county);
            endpoint.SendOne(farmCommand);

            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
        }

        void Cancel()
        {
            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
        }

        void UpdateFromProjection()
        {
            StateList = endpoint.GetMasterlistService().GetStateList().ToList();
            RaisePropertyChanged("StateList");
            County = endpoint.GetMasterlistService().GetCountyByStateAndName(details.County, details.State);
            State = endpoint.GetMasterlistService().GetStateByName(details.State);

            if (State != null)
            {
                CountyList = State.Counties.ToList();
            }

            RaisePropertyChanged("County");
        }
    }
}
