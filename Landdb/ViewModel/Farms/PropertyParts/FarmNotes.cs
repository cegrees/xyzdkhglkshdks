﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure.Messages;

namespace Landdb.ViewModel.Farms.PropertyParts
{
    public class FarmNotes : ViewModelBase
    {
        IClientEndpoint clientEndpoint;
        Dispatcher dispatcher;
        Landdb.Domain.ReadModels.Tree.FarmDetailsView farmView;

        string notes;

        public FarmNotes(IClientEndpoint clientEndpoint, Dispatcher dispatcher, Landdb.Domain.ReadModels.Tree.FarmDetailsView farmView)
        {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            this.farmView = farmView;

            UpdateFromProjection(farmView);

            ShowEditorCommand = new RelayCommand(ShowEditor);
            CancelCommand = new RelayCommand(Cancel);
            UpdateCommand = new RelayCommand(Update);
        }

        public ICommand ShowEditorCommand { get; private set; }
        public ICommand UpdateCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }

        public string Notes
        {
            get { return notes; }
            set
            {
                notes = value;
                RaisePropertyChanged("Notes");
            }
        }

        public bool HasData
        {
            get
            {
                return !string.IsNullOrWhiteSpace(Notes);
            }
        }

        void UpdateFromProjection(Domain.ReadModels.Tree.FarmDetailsView farmView)
        {
            Notes = farmView.Notes;

            RaisePropertyChanged("Notes");
        }

        bool IsInfoChanged()
        {
            return Notes != farmView.Notes;
        }

        void Cancel()
        {
            UpdateFromProjection(farmView);
            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
        }

        void Update()
        {
            if (IsInfoChanged())
            {
                UpdateFarmNotes infoCommand = new UpdateFarmNotes(farmView.Id, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), Notes);
                clientEndpoint.SendOne(infoCommand);
            }

            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
            RaisePropertyChanged("HasData");
        }

        void ShowEditor()
        {
            Messenger.Default.Send<ShowPopupMessage>(new ShowPopupMessage() { ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Fields.Popups.UpdateFieldNotesView", "editFsa", this) });
        }

    }
}
