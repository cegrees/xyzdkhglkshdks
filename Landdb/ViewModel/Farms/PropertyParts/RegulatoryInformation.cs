﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;

namespace Landdb.ViewModel.Farms.PropertyParts
{
    public class RegulatoryInformation : ViewModelBase
    {
        IClientEndpoint clientEndpoint;
        Dispatcher dispatcher;
        Landdb.Domain.ReadModels.Tree.FarmDetailsView farmView;
        string permitId = string.Empty;

        public RegulatoryInformation(IClientEndpoint clientEndpoint, Dispatcher dispatcher, Landdb.Domain.ReadModels.Tree.FarmDetailsView farmView)
        {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            this.farmView = farmView;

            UpdateFromProjection(farmView);

            ShowEditorCommand = new RelayCommand(ShowEditor);
            CancelCommand = new RelayCommand(Cancel);
            UpdateCommand = new RelayCommand(Update);
        }
        
        public ICommand ShowEditorCommand { get; private set; }
        public ICommand UpdateCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }

        public string PermitId
        {
            get { return permitId; }
            set
            {
                permitId = value;
                RaisePropertyChanged("PermitId");
            }
        }

        public bool HasData
        {
            get
            {
                return !string.IsNullOrWhiteSpace(PermitId);
            }
        }

        void UpdateFromProjection(Domain.ReadModels.Tree.FarmDetailsView farmView)
        {

            //TO DO :: FIX THIS....................
            PermitId = farmView.PermitId;

            RaisePropertyChanged("PermitId");
        }

        bool IsInfoChanged()
        {
            //TO DO :: FIX THIS...........
            return PermitId != farmView.PermitId;
        }

        void Cancel()
        {
            UpdateFromProjection(farmView);
            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
        }

        void Update()
        {
            if (IsInfoChanged())
            {
                UpdateFarmPermitIdentification_V2 infoCommand = new UpdateFarmPermitIdentification_V2(farmView.Id, clientEndpoint.GenerateNewMetadata(), PermitId, ApplicationEnvironment.CurrentCropYear);
                clientEndpoint.SendOne(infoCommand);
                //UpdateFarmNotes infoCommand = new UpdateFarmNotes(farmView.Id, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), Notes);
                //clientEndpoint.SendOne(infoCommand);
            }

            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
            RaisePropertyChanged("HasData");
        }
        void ShowEditor()
        {
            Messenger.Default.Send<ShowPopupMessage>(new ShowPopupMessage() { ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Farms.Popups.UpdateRegulatoryInformationView", "editPermit", this) });
        }

    }
}