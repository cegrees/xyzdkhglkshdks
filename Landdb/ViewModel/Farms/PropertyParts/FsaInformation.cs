﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure.Messages;

namespace Landdb.ViewModel.Farms.PropertyParts
{
    public class FsaInformation : ViewModelBase
    {
        IClientEndpoint clientEndpoint;
        Dispatcher dispatcher;

        string farmNumber;
        double area = 212.4;
        private Domain.ReadModels.Tree.FarmDetailsView farmView;

        public FsaInformation(IClientEndpoint clientEndpoint, Dispatcher dispatcher, Domain.ReadModels.Tree.FarmDetailsView farmView)
        {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            this.farmView = farmView;

            UpdateFromProjection(farmView);

            ShowEditorCommand = new RelayCommand(ShowEditor);
            CancelCommand = new RelayCommand(Cancel);
            UpdateCommand = new RelayCommand(Update);
        }

        public ICommand ShowEditorCommand { get; private set; }
        public ICommand UpdateCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }

        public string FarmNumber
        {
            get { return farmNumber; }
            set
            {
                farmNumber = value;
                RaisePropertyChanged("FarmNumber");
            }
        }

        public double Area
        {
            get
            {
                //TO DO :: Calculate the FSA Area of all fields under this Farm
                return area;
            }
        }

        public bool HasData
        {
            get
            {
                return !string.IsNullOrWhiteSpace(FarmNumber) || Area > 0;
            }
        }

        void UpdateFromProjection(Domain.ReadModels.Tree.FarmDetailsView farmView)
        {
            farmNumber = farmView.FsaFarmNumber;
        }

        bool IsInfoChanged() {
            return FarmNumber != farmView.FsaFarmNumber;
        }

        void Cancel() {
            UpdateFromProjection(farmView);
            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
        }

        void Update()
        {
            if (IsInfoChanged())
            {
                UpdateFarmFsaIdentification infoCommand = new UpdateFarmFsaIdentification(farmView.Id, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), FarmNumber);
                clientEndpoint.SendOne(infoCommand);
            }

            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
            RaisePropertyChanged("HasData");
        }

        void ShowEditor()
        {
            Messenger.Default.Send<ShowPopupMessage>(new ShowPopupMessage() { ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Farms.Popups.UpdateFsaInformationView", "editFsa", this) });
        }
    }
}
