using GalaSoft.MvvmLight;
using Landdb.Infrastructure;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure.Messages;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Landdb.ViewModel {
    public class MainViewModel : ViewModelBase {
        ScreenDescriptor currentScreenDescriptor;
        ScreenDescriptor currentOverlayDescriptor;
        ScreenDescriptor currentPopupDescriptor;

        Stack<ScreenDescriptor> overlayDescriptorStack = new Stack<ScreenDescriptor>();
        Stack<ScreenDescriptor> popupDescriptorStack = new Stack<ScreenDescriptor>();

        ScreenDescriptor loginViewDescriptor;
        ScreenDescriptor mainPageDescriptor;
        ScreenDescriptor startupViewDescriptor;

        ScreenDescriptor secondaryViewDescriptor;

        IApplicationUpdater appUpdater;
        //Stopwatch secondaryViewStopwatch = new Stopwatch();

        public MainViewModel() {
            Messenger.Default.Register<LoggedInMessage>(this, OnLoggedIn);
            Messenger.Default.Register<ShowOverlayMessage>(this, OnShowOverlay);
            Messenger.Default.Register<HideOverlayMessage>(this, OnHideOverlay);
            Messenger.Default.Register<ShowPopupMessage>(this, OnShowPopup);
            Messenger.Default.Register<HidePopupMessage>(this, OnHidePopup);
            Messenger.Default.Register<ShowMainViewMessage>(this, OnShowMainView);
            Messenger.Default.Register<ChangeScreenDescriptorMessage>(this, OnChangeScreenDescriptor);

            mainPageDescriptor = new ScreenDescriptor(typeof(Views.MainPage), "main");
            loginViewDescriptor = new ScreenDescriptor(typeof(Views.Account.LoginPageView), "login");
            startupViewDescriptor = new ScreenDescriptor(typeof(Landdb.Views.StartupView), "startup");

            CurrentScreenDescriptor = startupViewDescriptor;
            SecondaryScreenDescriptor = null;
        }

        public async Task OnStartupComplete() {
            this.appUpdater = ServiceLocator.Get<IApplicationUpdater>();
            var isUpdateAvailable = await appUpdater.CheckForUpdatesAsync();
            var isPrereqUpdateAvailable = await appUpdater.CheckForPrerequisiteUpdatesAsync();
            if (isUpdateAvailable) {
                if (appUpdater is ApplicationUpdater) {
                    CurrentScreenDescriptor = new ScreenDescriptor(typeof(Views.UpdatePageView), "update", ((ApplicationUpdater)appUpdater).UpdateStatusModel);
                }
                await appUpdater.PerformUpdateAsync();
            } else if(isPrereqUpdateAvailable) {
                if (appUpdater is ApplicationUpdater) {
                    CurrentScreenDescriptor = new ScreenDescriptor(typeof(Views.PrereqUpdatePageView), "prereqUpdate", ((ApplicationUpdater)appUpdater).PreRequisiteStatusModel);
                }
            } else {
                CurrentScreenDescriptor = loginViewDescriptor;
            }
        }

        void OnShowMainView(ShowMainViewMessage message) {
            CurrentOverlayDescriptor = null;

            //if (SecondaryScreenDescriptor != null) {
            //    secondaryViewStopwatch.Stop();
            //    if (!string.IsNullOrWhiteSpace(SecondaryScreenDescriptor.Key)) {
            //        var pageViewTelem = new Microsoft.ApplicationInsights.DataContracts.PageViewTelemetry(SecondaryScreenDescriptor.Key) {
            //            Duration = secondaryViewStopwatch.Elapsed
            //        };
            //        App.CurrentApp.Telemetry.TrackPageView(pageViewTelem);
            //    }
            //    secondaryViewStopwatch.Reset();
            //}

            SecondaryScreenDescriptor = null;
            if (CurrentScreenDescriptor != mainPageDescriptor) {
                CurrentScreenDescriptor = mainPageDescriptor;
            }
        }

        void OnLoggedIn(LoggedInMessage message) {
            CurrentOverlayDescriptor = null;
            SecondaryScreenDescriptor = null;
            CurrentScreenDescriptor = mainPageDescriptor;

            App.CurrentApp.Telemetry.Context.User.Id = message.UserAccountInformation.Id.ToString();
        }

        void OnChangeScreenDescriptor(ChangeScreenDescriptorMessage message) {
            //CurrentScreenDescriptor = message.ScreenDescriptor;
            SecondaryScreenDescriptor = message.ScreenDescriptor;
            //secondaryViewStopwatch.Start();

            if (ShouldTrackPageView(message.ScreenDescriptor)) {
                App.CurrentApp.Telemetry.TrackPageView(message.ScreenDescriptor.Key);
            }
        }

        void OnShowOverlay(ShowOverlayMessage message) {
            if (CurrentOverlayDescriptor != null) {
                overlayDescriptorStack.Push(CurrentOverlayDescriptor);
            }
            CurrentOverlayDescriptor = message.ScreenDescriptor;

            if (ShouldTrackPageView(message.ScreenDescriptor)) {
                App.CurrentApp.Telemetry.TrackPageView(message.ScreenDescriptor.Key);
            }
        }

        void OnHideOverlay(HideOverlayMessage message) {
            if (overlayDescriptorStack.Count > 0) {
                CurrentOverlayDescriptor = overlayDescriptorStack.Pop();
            } else {
                CurrentOverlayDescriptor = null;
            }
        }

        void OnShowPopup(ShowPopupMessage message) {
            if (CurrentPopupDescriptor != null) {
                popupDescriptorStack.Push(CurrentPopupDescriptor);
            }
            CurrentPopupDescriptor = message.ScreenDescriptor;

            if (ShouldTrackPageView(message.ScreenDescriptor)) {
                App.CurrentApp.Telemetry.TrackPageView(message.ScreenDescriptor.Key);
            }
        }

        void OnHidePopup(HidePopupMessage message) {
            if (popupDescriptorStack.Count > 0) {
                CurrentPopupDescriptor = popupDescriptorStack.Pop();
            } else {
                CurrentPopupDescriptor = null;
            }
        }

        bool ShouldTrackPageView(ScreenDescriptor screenDescriptor) {
            if (screenDescriptor != null && !string.IsNullOrWhiteSpace(screenDescriptor.Key)) {
                #region Return false for the following common UI dialogs that we don't need to bother tracking
                if (screenDescriptor.Key == typeof(Views.Overlays.FullSynchronizationProgressOverlayView).FullName) {
                    return false;
                } else if (screenDescriptor.Key == typeof(Views.Overlays.ApplicationCloseConfirmation).FullName) {
                    return false;
                }
                #endregion

                return true;
            } else {
                return false;
            }
        }

        public ScreenDescriptor CurrentScreenDescriptor {
            get { return currentScreenDescriptor; }
            set { 
                currentScreenDescriptor = value;
                RaisePropertyChanged(() => CurrentScreenDescriptor);
            }
        }

        public ScreenDescriptor SecondaryScreenDescriptor {
            get { return secondaryViewDescriptor; }
            set {
                secondaryViewDescriptor = value;
                RaisePropertyChanged(() => SecondaryScreenDescriptor);
                RaisePropertyChanged(() => ShowSecondary);
            }
        }

        public ScreenDescriptor CurrentOverlayDescriptor {
            get { return currentOverlayDescriptor; }
            set {
                currentOverlayDescriptor = value;
                RaisePropertyChanged(() => CurrentOverlayDescriptor);
                RaisePropertyChanged(() => ShowOverlay);
            }
        }

        public ScreenDescriptor CurrentPopupDescriptor {
            get { return currentPopupDescriptor; }
            set {
                currentPopupDescriptor = value;
                RaisePropertyChanged(() => CurrentPopupDescriptor);
                RaisePropertyChanged(() => ShowPopup);
            }
        }

		public bool ShowOverlay => currentOverlayDescriptor != null;
		public bool ShowPopup => currentPopupDescriptor != null;
		public bool ShowSecondary => secondaryViewDescriptor != null;

		// TODO: Copied from StartupViewModel. Find a more common way of including this.
		public string ApplicationVersionText => $"v. {System.Reflection.Assembly.GetEntryAssembly().GetName().Version.ToString()}";
	}
}