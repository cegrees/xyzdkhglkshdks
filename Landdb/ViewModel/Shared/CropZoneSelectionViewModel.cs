﻿using AgC.UnitConversion;
using GalaSoft.MvvmLight;
using Landdb.Client.Infrastructure;
using Landdb.Client.Spatial;
using Landdb.ViewModel.Fields;
using System;
using System.ComponentModel.DataAnnotations;
using System.Windows.Shapes;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Shared {
	public class CropZoneSelectionViewModel : ValidationViewModelBase {

		readonly IClientEndpoint clientEndpoint;
		readonly Dispatcher dispatcher;

		string fieldName;
		string name;
		Measure selectedArea;
		Measure baseArea;
		Path shapePreview;
		Action<double> changeAreaByDelta;

		public CropZoneSelectionViewModel(CropZoneTreeItemViewModel treeModel, string mapData, IClientEndpoint clientEndpoint, Action hideEditAction, Action<double> changeAreaByDelta, Dispatcher dispatcher = null) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher ?? App.Current.Dispatcher;

			Id = treeModel.Id as CropZoneId;
			fieldName = ((FieldTreeItemViewModel)treeModel.Parent).Name;
			name = treeModel.Name;
			baseArea = treeModel.Area;
			selectedArea = treeModel.Area;
            InitialArea = treeModel.Area;
			HideEditAction = hideEditAction;
			this.changeAreaByDelta = changeAreaByDelta;
			Crop = treeModel.CropId;

			MapData = mapData;
			//shapePreview = WpfTransforms.CreatePathFromMapData(new[] { mapData }, 55);

			ValidateViewModel();
		}

		public Action HideEditAction { get; }
		public CropZoneId Id { get; }
		public CropId Crop { get; }
		public string MapData { get; }

		public Measure InitialArea { get; private set; }

		public string FieldName {
			get { return fieldName; }
			set {
				fieldName = value;
				RaisePropertyChanged(() => FieldName);
			}
		}

		public string Name {
			get { return name; }
			set {
				name = value;
				RaisePropertyChanged(() => Name);
			}
		}

		[CustomValidation(typeof(CropZoneSelectionViewModel), nameof(ValidateSelectedArea))]
		public Measure SelectedArea {
			get { return selectedArea; }
			set {
				selectedArea = value;
				RaisePropertyChanged(() => SelectedArea);
			}
		}

		public Path ShapePreview {
			get {
				if (shapePreview == null) {
					WpfTransforms.CreateGeometryGroupFromMapDataAsync(new[] { MapData }, 55).ContinueWith(task => {
						if (task.Status == System.Threading.Tasks.TaskStatus.RanToCompletion) {
							dispatcher.BeginInvoke(new Action(() => {
								shapePreview = WpfTransforms.CreatePathFromGeometryGroup(task.Result);
								RaisePropertyChanged(() => ShapePreview);
							}));
						}
					});
					return new Path();
				} else {
					return shapePreview;
				}
			}
		}

		public CropZoneSelectionEditViewModel CreateEditModel() {
			return new CropZoneSelectionEditViewModel(Id, SelectedArea, ShapePreview, FieldName, Name, clientEndpoint, ChangeSelectedArea, HideEditAction);
		}

		internal void ChangeSelectedArea(Measure newArea) {
			if (newArea.Unit == SelectedArea.Unit && changeAreaByDelta != null) { // should always be, but no harm in being paranoid, right?
				var delta = newArea.Value - SelectedArea.Value;
				changeAreaByDelta(delta);
			}
			SelectedArea = newArea;
            InitialArea = newArea;
		}

		#region Custom Validation
		public static ValidationResult ValidateSelectedArea(Measure area, ValidationContext context) {
			ValidationResult result = null;

			if (area == null || area.Value <= 0) {
				result = new ValidationResult(Strings.ValidationResult_AreaIsRequired_Text);
			}

			return result;
		}
		#endregion
	}
}