﻿using AgC.UnitConversion;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using System;
using System.Windows.Input;
using System.Windows.Shapes;

namespace Landdb.ViewModel.Shared {
	public class CropZoneSelectionEditViewModel : ViewModelBase {

		double percent;
		double customArea;
		IUnit customUnit;
		string customId;
		IClientEndpoint endPoint;
		CropZoneId czId;
		string initialId;
		Action<Measure> changeAreaAction;
		Action hideEditAction;
        bool usePercentCoverageCropzone = true;

        public CropZoneSelectionEditViewModel(CropZoneId cropZoneId, Measure currentArea, Path shapePreview, string fieldName, string name, IClientEndpoint clientEndpoint, Action<Measure> changeAreaAction, Action hideEditAction) {
			Name = $"{fieldName} : {name}";
			ShapePreview = shapePreview;
			endPoint = clientEndpoint;
			czId = cropZoneId;
			this.changeAreaAction = changeAreaAction;
			this.hideEditAction = hideEditAction;
			var model = clientEndpoint.GetView<CropZoneDetailsView>(cropZoneId).GetValue(new CropZoneDetailsView());

			if (model.BoundaryArea != null && !string.IsNullOrWhiteSpace(model.BoundaryAreaUnit)) {
				BoundaryArea = UnitFactory.GetUnitByName(model.BoundaryAreaUnit).GetMeasure((double)model.BoundaryArea.Value);
			}

			if (model.ReportedArea != null && !string.IsNullOrWhiteSpace(model.ReportedAreaUnit)) {
				ReportedArea = UnitFactory.GetUnitByName(model.ReportedAreaUnit).GetMeasure((double)model.ReportedArea.Value);
			}

			if (model.FsaArea != null && !string.IsNullOrWhiteSpace(model.FsaAreaUnit)) {
				FsaArea = UnitFactory.GetUnitByName(model.FsaAreaUnit).GetMeasure((double)model.FsaArea.Value);
			}

			if (model.CustomId != null) {
				CustomCropZoneId = model.CustomId;
				initialId = model.CustomId;
			}

			var customMeasure = currentArea ?? ReportedArea ?? BoundaryArea ?? FsaArea ?? UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(0);
			CustomUnit = customMeasure.Unit;
			CustomArea = customMeasure.Value;
			var area = ReportedArea != null ? ReportedArea : BoundaryArea;
			Percent = (CustomArea / area.Value);
            usePercentCoverageCropzone = true;

            CancelCommand = new RelayCommand(hideEditAction);

			UseBoundaryAreaCommand = new RelayCommand(UseBoundaryArea);
			UseReportedAreaCommand = new RelayCommand(UseReportedArea);
			UseFsaAreaCommand = new RelayCommand(UseFsaArea);
			UseCustomAreaCommand = new RelayCommand(UseCustomArea);

			SetFieldID = new RelayCommand(setID);
			Cancel = new RelayCommand(cancel);
		}

		public RelayCommand CancelCommand { get; }
		public RelayCommand UseBoundaryAreaCommand { get; }
		public RelayCommand UseReportedAreaCommand { get; }
		public RelayCommand UseFsaAreaCommand { get; }
		public RelayCommand UseCustomAreaCommand { get; }
		public ICommand SetFieldID { get; }
		public ICommand Cancel { get; }

		public string Name { get; }

		public Path ShapePreview { get; set; }

		public Measure BoundaryArea { get; set; }
		public Measure ReportedArea { get; set; }
		public Measure FsaArea { get; set; }

		public bool HasBoundaryArea => BoundaryArea != null;
		public bool HasReportedArea => ReportedArea != null;
		public bool HasFsaArea => FsaArea != null;

		public double Percent {
			get { return percent; }
			set {
				percent = value;
				RaisePropertyChanged(() => Percent);

				var area = ReportedArea != null ? ReportedArea : BoundaryArea;
				if ((area.Value * percent) != customArea) {
					customArea = area.Value * percent;
					RaisePropertyChanged(() => CustomArea);
				}
			}
		}

		public double CustomArea {
			get { return customArea; }
			set {
				customArea = value;
				RaisePropertyChanged(() => CustomArea);

				var area = ReportedArea != null ? ReportedArea : BoundaryArea;
				if (percent != (customArea / area.Value)) {
					percent = (customArea / area.Value);
					RaisePropertyChanged(() => Percent);
				}
			}
		}

		public IUnit CustomUnit {
			get { return customUnit; }
			set {
				customUnit = value;
				RaisePropertyChanged(() => CustomUnit);
			}
		}

		public string CustomCropZoneId {
			get { return customId; }
			set {
				customId = value;
				RaisePropertyChanged(() => CustomCropZoneId);
			}
		}

        public bool UsePercentCoverageCropzone {
            get { return usePercentCoverageCropzone; }
            set {
                usePercentCoverageCropzone = value;
                RaisePropertyChanged(() => UsePercentCoverageCropzone);
            }
        }

        void setID() {
			if (initialId != CustomCropZoneId) {
				var infoUpdate = new SetCropZoneCustomId(czId, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, endPoint.DeviceId, endPoint.UserId), CustomCropZoneId);
				endPoint.SendOne(infoUpdate);
			}

			Messenger.Default.Send(new HidePopupMessage());
		}

		void cancel() {
			Messenger.Default.Send(new HidePopupMessage());
		}

		void UseBoundaryArea() {
			if (BoundaryArea != null) {
				Percent = 1.0;
				changeAreaAction(BoundaryArea);
				hideEditAction();
				cancel();
			}
		}

		void UseReportedArea() {
			if (ReportedArea != null) {
				Percent = 1.0;
				changeAreaAction(ReportedArea);
				hideEditAction();
				cancel();
			}
		}

		void UseFsaArea() {
			if (FsaArea != null) {
				var area = ReportedArea != null ? ReportedArea : BoundaryArea;
				Percent = (FsaArea.Value / area.Value);
				changeAreaAction(FsaArea);
				hideEditAction();
				cancel();
			}
		}

		void UseCustomArea() {
			var area = ReportedArea != null ? ReportedArea : BoundaryArea;
			Percent = (CustomArea / area.Value);
			var newArea = CustomUnit.GetMeasure(CustomArea);
			changeAreaAction(newArea);
			hideEditAction();
			cancel();
		}
	}
}