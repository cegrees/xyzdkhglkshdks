﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Production;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Client.Infrastructure.DisplayItems;
using Landdb.Resources;

namespace Landdb.ViewModel.Shared {
    public class DocumentDescriptorViewModel : ViewModelBase {

        private readonly DocumentDescriptor document;

		private bool isDeleted = false;

        public DocumentDescriptorViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, DocumentDescriptor document) {
            this.document = document;
            Task.Run(() => {
				if (document.Identity != null) {
					isDeleted = new Infrastructure.DocumentStatusCheck(clientEndpoint).DocumentHasBeenDeleted(document.Identity);
				}
            });
        }

		public DocumentDescriptor Document => document;
		public DocumentTypeDisplayItem DocumentType => new DocumentTypeDisplayItem(document.DocumentType);
		public AbstractDataSourceIdentity<Guid, Guid> Identity => document.Identity;
		public string Location => document.Location;
		public string Name => document.Name;
		public bool IsDeleted => isDeleted;
	}

    public class DocumentListPageViewModel : AbstractListPage<DocumentDescriptorViewModel, object> {

        int cropYear;
        ProductionDocumentDetailsModelCreator productionModelCreator;
        ObservableCollection<DocumentDescriptorViewModel> sourceDocuments;
        List<DocumentDescriptor> addedDocuments = new List<DocumentDescriptor>();
        List<DocumentDescriptor> removedDocuments = new List<DocumentDescriptor>();

        public DocumentListPageViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, int cropYear, IEnumerable<DocumentDescriptor> initialSourceDocuments, bool isAddingAllowed = true) : base(clientEndpoint, dispatcher) {
            this.cropYear = cropYear;
            var p = from d in initialSourceDocuments
                    select new DocumentDescriptorViewModel(clientEndpoint, dispatcher, d);
            sourceDocuments = new ObservableCollection<DocumentDescriptorViewModel>(p);
            productionModelCreator = new ProductionDocumentDetailsModelCreator(clientEndpoint, dispatcher);

			IsAddingAllowed = isAddingAllowed;

            AddDocumentCommand = new RelayCommand(AddDocument);
            RemoveDocumentCommand = new RelayCommand(RemoveDocument);
            RefreshItemsList();
        }

        public ICommand AddDocumentCommand { get; }
		public ICommand RemoveDocumentCommand { get; }

		// property to disable the add button for yield until we have the ability to attach something
		public bool IsAddingAllowed { get; }

		public Action OnDocumentInfoChanged { get; set; }

		protected override IEnumerable<DocumentDescriptorViewModel> GetListItemModels() => sourceDocuments;

		protected override object CreateDetailsModel(DocumentDescriptorViewModel selectedItem) {
			if (selectedItem.Identity != null) {
				return productionModelCreator.CreateDetailsModel(selectedItem.Identity);
			} else {
				return new ECottonDocumentViewModel(clientEndpoint, selectedItem.Document);
			}
        }

        void AddDocument() {
            var vm = new Overlays.DocumentFinderOverlayViewModel(this.clientEndpoint, this.dispatcher, (doc) => {
                if (doc != null) {
                    if (!sourceDocuments.Any(x => x.DocumentType.KeyText.Equals(doc.DocumentType) && x.Identity == doc.Identity)) {
                        sourceDocuments.Add(new DocumentDescriptorViewModel(clientEndpoint, dispatcher, doc));
                        if (removedDocuments.Any(x => x.DocumentType == doc.DocumentType && x.Identity == doc.Identity)) {
                            removedDocuments.RemoveAll(x => x.DocumentType == doc.DocumentType && x.Identity == doc.Identity);
                        } else {
                            addedDocuments.Add(doc);
                        }
                        RefreshItemsList();
                        RaisePropertyChanged(() => DocumentCount);
                        OnDocumentInfoChanged?.Invoke();
                    }
                }

                Messenger.Default.Send<HideOverlayMessage>(new HideOverlayMessage() { });
            });

			Messenger.Default.Send(new ShowOverlayMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Overlays.DocumentFinderOverlayView), Strings.ScreenDescriptor_ChooseDocument_Text.ToLower(), vm)
			});
        }

        void RemoveDocument() {
            if (SelectedListItem != null) {
                sourceDocuments.Remove(SelectedListItem);
                if (addedDocuments.Contains(SelectedListItem.Document)) {
                    addedDocuments.Remove(SelectedListItem.Document);
                } else {
                    removedDocuments.Add(SelectedListItem.Document);
                }
                RefreshItemsList();
                RaisePropertyChanged(() => DocumentCount);
                OnDocumentInfoChanged?.Invoke();
            }
        }

		public int DocumentCount => sourceDocuments.Count;
		public List<DocumentDescriptor> AddedDocuments => addedDocuments;
		public List<DocumentDescriptor> RemovedDocuments => removedDocuments;

		internal void ResetData() {
            sourceDocuments.Clear();
        }

        // TODO: This is possibly obsolete/redundant
        public DocumentDescriptor[] GetDocuments() {
            var sd = from d in sourceDocuments
                     select d.Document;
            return sd.ToArray();
        }

        #region Unnecessary Overrides
        protected override async Task<ScreenDescriptor> CreateScreenDescriptorForNewItemCreation() {
            throw new NotImplementedException();
        }

        protected override bool PerformItemRemoval(DocumentDescriptorViewModel selectedItem) {
            throw new NotImplementedException();
        }

        protected override IDomainCommand CreateSetCharmCommand(DocumentDescriptorViewModel selectedItem, string charmName) {
            throw new NotImplementedException();
        }
        #endregion
    }
}
