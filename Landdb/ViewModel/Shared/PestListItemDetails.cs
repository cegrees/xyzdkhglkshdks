﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Landdb.Infrastructure;
using System.Diagnostics;
using System.Windows.Input;

namespace Landdb.ViewModel.Shared {
	public class PestListItemDetails : ViewModelBase {

		public PestListItemDetails(Pest pest, short groupSortOrder) {
			PestId = pest.Id;
			CommonName = pest.Name;
			LatinName = pest.LatinName;
			GroupSortOrder = groupSortOrder;
			SearchText = string.Format("{0}_{1}", pest.Name, pest.LatinName);
			SearchWikipediaCommand = new RelayCommand(onSearchWikipedia);
		}

		public ICommand SearchWikipediaCommand { get; }

		public uint PestId { get; }
		public string CommonName { get; }
		public string LatinName { get; }
		public string SearchText { get; }
		public short GroupSortOrder { get; }

        public override string ToString()
        {
            //Pest Search did not work properly when only common name was returned
            //   => CommonName;

            var returnedString = CommonName == string.Empty ? LatinName : (LatinName == string.Empty ? CommonName : string.Format("{0} ({1})", CommonName, LatinName));
            return returnedString;
        }

		private void onSearchWikipedia() {
			var searchTerm = string.Empty;

			if (!string.IsNullOrWhiteSpace(LatinName)) {
				searchTerm = LatinName;
			} else {
				searchTerm = CommonName;
			}

			string browserPath = ApplicationEnvironment.GetSystemDefaultBrowserPath();

			if (!string.IsNullOrWhiteSpace(browserPath)) {
				Process.Start(browserPath, $"\"? {searchTerm}\"");
			} else {
				searchTerm = searchTerm.Replace(' ', '_');
				var url = $@"http://en.wikipedia.org/wiki/Special:Search/{searchTerm}"; // TODO: Allow for culture to drive the URL as well
				Process.Start(url);
			}
		}
	}
}