﻿using GalaSoft.MvvmLight;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Tree;
using System;
using System.Collections.Generic;
using System.Linq;
using Landdb.Resources;

namespace Landdb.ViewModel.Shared {
	public class CropListItemViewModel : ViewModelBase {
		public CropListItemViewModel(CropId id, string name, string groupName, short groupSortOrder) {
			CropId = id;
			CropName = name;
			GroupName = groupName;
			GroupSortOrder = groupSortOrder;
		}

		public CropId CropId { get; }
		public string CropName { get; }
		public string GroupName { get; }
		public short GroupSortOrder { get; }

		public static List<CropListItemViewModel> GetListItemsForCropYear(IClientEndpoint clientEndpoint, CropYearId cropYearId) {
			var miniCrops = clientEndpoint.GetMasterlistService().GetCropList();

			IEnumerable<Guid> cropIdsAssignedThisYear = new List<Guid>();

			var flattenedTreeView = clientEndpoint.GetView<FlattenedTreeHierarchyView>(cropYearId).GetValue(() => null);
			if (flattenedTreeView != null) {
				cropIdsAssignedThisYear = (from treeItem in flattenedTreeView.Items
										   where treeItem.CropId != null
										   select treeItem.CropId.Id).Distinct();
			}

			var cropList = from mc in miniCrops
						   let cont = cropIdsAssignedThisYear.Contains(mc.Id)
						   select new CropListItemViewModel(new CropId(mc.Id), mc.Name, cont ? Strings.AssignedCrops_Text : Strings.UnassignedCrops_Text, cont ? (short)0 : (short)1);

			return cropList.OrderBy(x => x.GroupSortOrder).ThenBy(x => x.CropName).ToList();
		}

		public override string ToString() => CropName;
	}
}
