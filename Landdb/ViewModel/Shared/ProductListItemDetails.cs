﻿using GalaSoft.MvvmLight;
using System;

namespace Landdb.ViewModel.Shared {
	public class ProductListItemDetails : ViewModelBase {
		public ProductListItemDetails(MiniProduct product, Guid dataSourceId, string groupName, short groupSortOrder) {
			Product = product;
			DataSourceId = dataSourceId;
			GroupName = groupName;
			GroupSortOrder = groupSortOrder;
            SearchText = string.Format("{0}_{1}_{2}", Product.Name, Product.Manufacturer, Product.RegistrationNumber);
        }

		public MiniProduct Product { get; set; }
		public Guid DataSourceId { get; set; }
		public string GroupName { get; set; }
		public short GroupSortOrder { get; set; }
        public string SearchText { get; }

        public ProductId ProductId => new ProductId(DataSourceId, Product.Id);

        public override string ToString()
        {
            if (Product == null) { return string.Empty; }
            if (string.IsNullOrEmpty(Product.RegistrationNumber))
            {
                return Product.ToString();
            }
            return string.Format("{0} ({1}) ({2})", Product.Name, Product.Manufacturer, Product.RegistrationNumber);
        }
    }
}
