﻿using Landdb.Client.Infrastructure;
using Landdb.Infrastructure;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Shared {
	public class ECottonDocumentViewModel {

		private readonly IClientEndpoint clientEndpoint;

		private readonly ECottonLoad eCottonLoadDetails;

		private readonly Logger log;

		public ECottonDocumentViewModel(IClientEndpoint clientEndpoint, DocumentDescriptor document) {
			this.clientEndpoint = clientEndpoint;

			log = LogManager.GetCurrentClassLogger();

			DocumentLocation = document.Location;

			eCottonLoadDetails = retrieveDocument();

			buildDetailsFromDocument();
		}

		// used for displaying an error message in the ui when the doc can't be retrieved.
		public string DocumentLocation { get; }

		public string DocumentTitle { get; private set; }
		public string BaleCount { get; private set; }
		public string TotalWeight { get; private set; }
		public string Status { get; private set; }
		public string DateDelivered { get; private set; }
		public string DateGinned { get; private set; }

		public List<ECottonBale> CottonBaleList { get; private set; }

		public bool HasDetails => eCottonLoadDetails != null;

		private ECottonLoad retrieveDocument() {
			ECottonLoad loadDoc = null;

			try {
				var baseUri = RemoteConfigurationSettings.GetBaseUri();
				var documentLocation = HttpUtility.UrlEncode(DocumentLocation);
				var eCottonDocumentUri = $"{baseUri}api/ecotton/?key={documentLocation}";
				var client = new WebClient();
				var cloudClientFactory = new CloudClientFactory();
				var token = cloudClientFactory.ResolveStorageCredentials();
				token.SignWebClientAsync(client);

				// TODO: make this async
				var response = client.DownloadString(eCottonDocumentUri);

				// http://stackoverflow.com/questions/13702657/deserializing-json-how-to-ignore-the-root-element
				loadDoc = JsonConvert.DeserializeAnonymousType(response, new { Results = new ECottonLoad() }).Results;
			} catch (Exception ex) {
				var errorMsg = $"Unable to retrieve eCotton document with key [{DocumentLocation}]";
				log.ErrorException(errorMsg, ex);

				return null;
			}

			return loadDoc;
		}

		private void buildDetailsFromDocument() {
			if (HasDetails) {
				DocumentTitle = $"{Strings.ECottonLoad_Text} {eCottonLoadDetails.LoadNumber}";
				BaleCount = $"{eCottonLoadDetails.BaleList.Count} {Strings.Unit_Bales_Text}";
				TotalWeight = $"{eCottonLoadDetails.BaleList.Sum(x => x.NetWeight):N2} {Strings.Unit_Weight_Text}";
				Status = $"{Strings.Status_Text}: {(!string.IsNullOrWhiteSpace(eCottonLoadDetails.Status) ? eCottonLoadDetails.Status : $"<{Strings.Unknown_Text}")}";
				DateDelivered = $"{Strings.Delivered_Text}: {eCottonLoadDetails.DateTimeDelivered.ToShortDateString()}";
				DateGinned = $"{Strings.Ginned_Text}: {eCottonLoadDetails.DateGinned.ToShortDateString()}";

				CottonBaleList = eCottonLoadDetails.BaleList;
			}
		}
	}
}