﻿using AgC.UnitConversion;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Client.Spatial;
using Landdb.Domain.ReadModels.Map;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Fields;
using Landdb.ViewModel.Secondary.Map;
using Microsoft.Maps.MapControl.WPF;
using NLog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Data;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Shared {
	public abstract class BaseFieldSelectionViewModel : ValidationViewModelBase {

		protected readonly Dispatcher dispatcher;
		protected readonly IClientEndpoint clientEndpoint;

		protected CropYearId currentCropYearId;

		protected Logger log = LogManager.GetCurrentClassLogger();
		protected Measure totalArea = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(0);

	    TreeDisplayModeDisplayItem selectedTreeDisplayMode;
		CropZoneSelectionEditMode editMode;
		bool isEditPopupOpen = false;
		CropZoneSelectionViewModel selected;
		string totalAreaText;
		bool isFilterVisible = false;
		string filterHeader = string.Empty;
        bool usePercentCoverage = true;
        bool usePercentCoverageCropzone = true;

        public event EventHandler MapUpdated;
		public EventArgs e = new MapUpdatedEventArgs();

		private FlattenedTreeHierarchyView flattenedHierarchy;
        private SSurgoNrcsWfsViewModel ssurgonrcswfsvm;

        public BaseFieldSelectionViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, int cropYear, CropZoneSelectionEditMode mode = CropZoneSelectionEditMode.Application) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			currentCropYearId = new CropYearId(ApplicationEnvironment.CurrentDataSourceId, cropYear);
			editMode = mode;
            usePercentCoverage = true;
            usePercentCoverageCropzone = true;

            SelectedCropZones = new ObservableCollection<CropZoneSelectionViewModel>();
            this.ssurgonrcswfsvm = new SSurgoNrcsWfsViewModel(this.clientEndpoint);

            ApplyFilterCommand = new RelayCommand(ApplyFilter);
			ClearFilterCommand = new RelayCommand(ClearFilter);
			EditAreaView = new RelayCommand(EditArea);
			ClosePopUpCommand = new RelayCommand(ClosePopUp);
			CancelPopUpCommand = new RelayCommand(CancelPopUp);
            SSurgoCommand = new RelayCommand(SSurgoNrcsWfsTool);


            FilterModel = new TreeFilterModel(clientEndpoint);

			SelectedTreeDisplayMode = new TreeDisplayModeDisplayItem("Farms");
			BuildTreeList();
            
            flattenedHierarchy = clientEndpoint.GetView<FlattenedTreeHierarchyView>(currentCropYearId).GetValue(new FlattenedTreeHierarchyView());
			Percent = 1m;
			SelectedCropZones.CollectionChanged += ((sender, args) => {
				ValidateProperty(() => SelectedCropZones);
			});
		}

		public RelayCommand ApplyFilterCommand { get; }
		public RelayCommand ClearFilterCommand { get; }
		public RelayCommand EditAreaView { get; }
		public RelayCommand ClosePopUpCommand { get; }
        public RelayCommand CancelPopUpCommand { get; }
        public RelayCommand SSurgoCommand { get; }

        public bool IsRentContract { get; set; }

        public IPageFilterViewModel FilterModel { get; protected set; }

		public ObservableCollection<AbstractTreeItemViewModel> RootTreeItemModels { get; private set; }
		public CollectionView RootTreeItemModelsView { get; private set; }

		public virtual ObservableCollection<CropZoneSelectionViewModel> SelectedCropZones { get; protected set; }

		public MapLayer BingMapsLayer { get; private set; }
		public MapLayer LabelsLayer { get; set; }

        public string MapCulture
        {
            get { return Infrastructure.ApplicationEnvironment.BingWpfMapCulture; }
        }

        public CropZoneSelectionViewModel SelectedCropZone {
			get { return selected; }
			set {
				selected = value;
				OpenCropZonePopUP();
			}
		}

		public bool IsEditPopupOpen {
			get { return isEditPopupOpen; }
			set {
				isEditPopupOpen = value;
				RaisePropertyChanged(() => IsEditPopupOpen);
			}
		}

		public Measure TotalArea {
			get { return totalArea; }
			set {
				var oldValue = totalArea;
				totalArea = value;
				//TotalAreaText = totalArea.Value.ToString();

				if (TotalAreaText == null || TotalAreaText == string.Empty || TotalAreaText != totalArea.Value.ToString("N2")) {
					totalAreaText = totalArea.Value.ToString("N2");
					RaisePropertyChanged(() => TotalAreaText);
				}

				RaisePropertyChanged(() => TotalArea);
			}
		}

		public string TotalAreaText {
			get { return totalAreaText; }
			set {
				if (string.IsNullOrWhiteSpace(value)) { return; }

				var oldArea = TotalArea.Value;

				if (totalAreaText != value && TotalArea != null) {
					totalAreaText = value;
					decimal area = 0m;

					if (decimal.TryParse(totalAreaText, out area)) {
						var areaUnit = TotalArea.Unit;
						var newTotalArea = areaUnit.GetMeasure((double)area);
						totalAreaText = newTotalArea.Value.ToString("N2");
                        oldArea = 0;
                        foreach (CropZoneSelectionViewModel cz in SelectedCropZones) {
                            oldArea += cz.InitialArea.Value;
                        }

                        if (newTotalArea != null && newTotalArea.Value > 0) {
							var percentChanged = newTotalArea.Value / oldArea;

							foreach (CropZoneSelectionViewModel cz in SelectedCropZones) {
								cz.SelectedArea = cz.SelectedArea.Unit.GetMeasure(cz.InitialArea.Value * percentChanged);
								//cz.ChangeSelectedArea(cz.SelectedArea.Unit.GetMeasure(cz.SelectedArea.Value * percentChanged));
							}
						}

						RaisePropertyChanged(() => TotalAreaText);
					}
				} else {
					totalAreaText = "0";
				}
			}
		}

		public int FieldCount => SelectedCropZones.Count;

		public static List<TreeDisplayModeDisplayItem> AvailableTreeDisplayModes => 
			new List<TreeDisplayModeDisplayItem>() {
				new TreeDisplayModeDisplayItem("Farms"),
			    new TreeDisplayModeDisplayItem("Crops")
            };

		public TreeDisplayModeDisplayItem SelectedTreeDisplayMode {
			get { return selectedTreeDisplayMode; }
			set {
                if (value != null)
                    if (!Equals(selectedTreeDisplayMode, value)) {
					selectedTreeDisplayMode = value;

					if (RootTreeItemModels != null) {
						RootTreeItemModels.Clear();
					}

					BuildTreeList();

					RaisePropertyChanged(() => SelectedTreeDisplayMode);
				}
			}
		}

        public bool UsePercentCoverage {
            get { return usePercentCoverage; }
            set {
                usePercentCoverage = value;
                if(!usePercentCoverage) {
                    UsePercentCoverageCropzone = false;
                }
                RaisePropertyChanged(() => UsePercentCoverage);
            }
        }
        public bool UsePercentCoverageCropzone {
            get { return usePercentCoverageCropzone; }
            set {
                usePercentCoverageCropzone = value;
                RaisePropertyChanged(() => UsePercentCoverageCropzone);
            }
        }

        void BuildTreeList() {
			//await Task.Run(new Action(() => {
			var begin = DateTime.UtcNow;
			AbstractTreeItemViewModel rootNode = null;
			var settings = clientEndpoint.GetUserSettings();
			bool sortAlphabetically = settings.AreFieldsSortedAlphabetically;

			// TODO: When maps are separate, move this back to the Farms case in the switch below.
			var years = clientEndpoint.GetView<FieldTreeView>(new DataSourceId(ApplicationEnvironment.CurrentDataSourceId));
			var yearItem = (years.HasValue && years.Value.CropYearList.ContainsKey(currentCropYearId.Id)) ? years.Value.CropYearList[currentCropYearId.Id] : new TreeViewYearItem();

            if (SelectedTreeDisplayMode.IsCrops) {
                var tree = clientEndpoint.GetView<CropTreeView>(currentCropYearId).GetValue(new CropTreeView());
                IList<TreeViewCropItem> crops = tree.Crops ?? new List<TreeViewCropItem>();
                rootNode = new GrowerCropTreeItemViewModel("Home", crops, sortAlphabetically, OnFieldCheckedChanged_internal, OnParentCheckedChanged_internal, FilterModel.FilterItem);
            } else { //if it's farms or anything else
                IList<TreeViewFarmItem> farms = yearItem.Farms ?? new List<TreeViewFarmItem>();
                rootNode = new GrowerTreeItemViewModel("Home", farms, sortAlphabetically, OnFieldCheckedChanged_internal, OnParentCheckedChanged_internal, FilterModel.FilterItem);
            }

            // initialize map
			var mapQ = from m in yearItem.Maps
					   where m.IsVisible
					   select m.MapData;

			BingMapsLayer = BingMapsUtility.GetLayerForMapData(mapQ);
			RaisePropertyChanged(() => BingMapsLayer);

			if (rootNode != null) {
				RootTreeItemModels = new ObservableCollection<AbstractTreeItemViewModel>() { rootNode };
				RootTreeItemModels[0].IsExpanded = true;

				App.Current.Dispatcher.BeginInvoke(new Action(() => {
					RootTreeItemModelsView = (CollectionView)CollectionViewSource.GetDefaultView(RootTreeItemModels);
					RootTreeItemModelsView.Filter = FilterModel.FilterItem;
					GenerateFilterHeader();
					RaisePropertyChanged(() => RootTreeItemModelsView);
				}));

				var selectedCzIds = SelectedCropZones.Select(x => x.Id);

				// recheck selected crop zones
				if (selectedCzIds.Any()) {
					IEnumerable<CropZoneTreeItemViewModel> cropZonesToRecheck = new List<CropZoneTreeItemViewModel>();

					if (SelectedTreeDisplayMode.IsFarms) { 
							cropZonesToRecheck = from farm in RootTreeItemModels[0].Children
												 from field in farm.Children
												 from cz in field.Children
												 where selectedCzIds.Contains(cz.Id)
												 select cz as CropZoneTreeItemViewModel;
					}
                    else if (SelectedTreeDisplayMode.IsCrops) {
				        cropZonesToRecheck = from crop in RootTreeItemModels[0].Children
				                             from farm in crop.Children
				                             from field in farm.Children
				                             from cz in field.Children
				                             where selectedCzIds.Contains(cz.Id)
				                             select cz as CropZoneTreeItemViewModel;
				    }
					cropZonesToRecheck.ForEach(x => x.IsChecked = true);
				}
			}

			RaisePropertyChanged(() => RootTreeItemModels);

			var end = DateTime.UtcNow;

			log.Debug($"Applicator tree load took {(end - begin).TotalMilliseconds}ms");
			//}));
		}

		private void OnParentCheckedChanged_internal(List<CropZoneId> czIds) {
			//GetMapLayer();
			List<string> mapDataList = new List<string>();
			MapLayer newLabelsLayer = new MapLayer();
			BingMapsLayer = new MapLayer();
			foreach (var id in czIds) {
				var mapItem = clientEndpoint.GetView<ItemMap>(id);
				string mapData = null;
				if (mapItem.HasValue && mapItem.Value.MostRecentMapItem != null) {
					mapDataList.Add(mapItem.Value.MostRecentMapItem.MapData);

					List<string> holder = new List<string>();
					holder.Add(mapItem.Value.MostRecentMapItem.MapData);
					var shapeLayer = BingMapsUtility.GetLayerForMapData(holder);
					var bounding = BingMapsUtility.GetBoundingBox(shapeLayer);
					System.Windows.Controls.Label customLabel = new System.Windows.Controls.Label();
					customLabel.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Center;
					customLabel.VerticalContentAlignment = System.Windows.VerticalAlignment.Center;
					customLabel.FontWeight = System.Windows.FontWeights.DemiBold;

					var flattenedItem = flattenedHierarchy.Items.FirstOrDefault(x => x.CropZoneId == id);
					var fieldName = flattenedItem != null ? flattenedItem.FieldName : string.Empty;
					customLabel.Content = fieldName;
					newLabelsLayer.AddChild(customLabel, bounding);
				}
			}

			BingMapsLayer = BingMapsUtility.GetLayerForMapData(mapDataList.ToArray());
			RaisePropertyChanged(() => BingMapsLayer);

			LabelsLayer = newLabelsLayer;
			RaisePropertyChanged(() => LabelsLayer);

			var updatedArg = new MapUpdatedEventArgs() { IsUpdated = true, TimeUpdated = DateTime.Now };
			if (updatedArg != null) {
				OnMapUpdated(updatedArg);
				//MapUpdated(this, updatedArg);
			}
		}

		protected virtual void OnMapUpdated(EventArgs e) {
			MapUpdated?.Invoke(this, e);
		}

		protected abstract void OnFieldCheckedChanged(CropZoneTreeItemViewModel treeItem);

		private void OnFieldCheckedChanged_internal(AbstractTreeItemViewModel treeItem) {
			if (treeItem is CropZoneTreeItemViewModel) {
				OnFieldCheckedChanged(treeItem as CropZoneTreeItemViewModel);

				if (treeItem.IsChecked.HasValue && treeItem.IsChecked.Value && !SelectedCropZones.Any(x => x.Id == (treeItem.Id as CropZoneId))) {
					var mapItem = clientEndpoint.GetView<ItemMap>(treeItem.Id);
					string mapData = null;

					if (mapItem.HasValue && mapItem.Value.MostRecentMapItem != null) {
						mapData = mapItem.Value.MostRecentMapItem.MapData;
					}

					var cztivm = new CropZoneSelectionViewModel(treeItem as CropZoneTreeItemViewModel, mapData, clientEndpoint, () => { IsEditPopupOpen = false; }, (delta) => {
						TotalArea = TotalArea.Unit.GetMeasure(TotalArea.Value + delta);
						RaisePropertyChanged(() => TotalArea);
					});

					SelectedCropZones.Add(cztivm);


					dispatcher.BeginInvoke(new Action(() => {
						var area = SelectedCropZones.Sum(x => x.SelectedArea.Value);
						TotalArea = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(area, null);
                        EditAreaValue = (decimal)area;
						RaisePropertyChanged(() => FieldCount);
					}));

				}

				if (treeItem.IsChecked.HasValue && !treeItem.IsChecked.Value) {
					var found = SelectedCropZones.Where(x => x.Id == (CropZoneId)treeItem.Id).ToList();
					found.ForEach(x => {
						SelectedCropZones.Remove(x);

					});
					dispatcher.BeginInvoke(new Action(() => {
						var area = SelectedCropZones.Sum(x => x.SelectedArea.Value);
						TotalArea = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(area, null);

					}));

					RaisePropertyChanged(() => FieldCount);
				}
			}
		}

		//private async Task CropZoneItemCreator(AbstractTreeItemViewModel treeItem, string mapData, Action<CropZoneSelectionViewModel> onListCompleted) {
		//    var cztivm = new CropZoneSelectionViewModel(treeItem as CropZoneTreeItemViewModel, mapData, clientEndpoint, () => { IsEditPopupOpen = false; }, (delta) => {
		//        TotalArea = TotalArea.Unit.GetMeasure(TotalArea.Value + delta);
		//        RaisePropertyChanged(() => TotalArea");
		//    });

		//    onListCompleted(cztivm);
		//}

		//private async Task RemoveCZItems(AbstractTreeItemViewModel item, ObservableCollection<CropZoneSelectionViewModel> czList, Action<ObservableCollection<CropZoneSelectionViewModel>> onListUpdated) {
		//    var collection = czList;
		//    var found = czList.Where(x => x.Id == (CropZoneId)item.Id).ToList();
		//    found.ForEach(x => {
		//        collection.Remove(x); //.Remove(x);
		//    });

		//    onListUpdated(collection);
		//}

		internal CropZoneArea[] GetCropZoneAreas() {
			List<CropZoneArea> areas = new List<CropZoneArea>();

			foreach (var cz in SelectedCropZones) {
				areas.Add(new CropZoneArea(cz.Id, cz.SelectedArea.Value, cz.SelectedArea.Unit.Name));
			}

			return areas.ToArray();
		}

		internal virtual void ResetData() {
			SelectedCropZones.Clear();
			TotalArea = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(0);
			RaisePropertyChanged(() => FieldCount);
			BuildTreeList();
		}

		public void ChangeCropYearForTree(int cropyear) {
			currentCropYearId = new CropYearId(currentCropYearId.DataSourceId, cropyear);
			ResetData();
		}

		internal void OpenCropZonePopUP() {
			if (SelectedCropZone != null) {
				var editVM = new CropZoneSelectionEditViewModel(SelectedCropZone.Id, SelectedCropZone.SelectedArea, SelectedCropZone.ShapePreview, SelectedCropZone.FieldName, SelectedCropZone.Name, clientEndpoint, SelectedCropZone.ChangeSelectedArea, SelectedCropZone.HideEditAction);
                editVM.UsePercentCoverageCropzone = UsePercentCoverageCropzone;
			    if (IsRentContract) return; //intended to prevent the rent contract popup from appearing in the FieldsPageView when a cropzone or w/e is selected. 
				if (editMode == CropZoneSelectionEditMode.Application) {
					Messenger.Default.Send(new ShowPopupMessage() {
						ScreenDescriptor = new ScreenDescriptor(typeof(Views.Secondary.Application.UpdateSelectedCropZoneAreaView), Strings.ScreenDescriptor_EditArea_Text.ToLower(), editVM)
					});
				} else if (editMode == CropZoneSelectionEditMode.Contract) {
					//open a different View for the pop up
					Messenger.Default.Send(new ShowPopupMessage() {
						ScreenDescriptor = new ScreenDescriptor(typeof(Views.Shared.Popups.UpdateSelectedCropZoneFieldIdView), Strings.ScreenDescriptor_EditFieldId_Text.ToLower(), editVM)
					});
				}
			}
		}

		public bool IsFilterVisible {
			get { return isFilterVisible; }
			set {
				isFilterVisible = value;
				RaisePropertyChanged(() => IsFilterVisible);
			}
		}

		public string FilterHeader {
			get { return filterHeader; }
			set {
				filterHeader = value;
				RaisePropertyChanged(() => FilterHeader);
			}
		}

		void ClearFilter() {
			if (FilterModel != null && RootTreeItemModelsView != null) {
				FilterModel.ClearFilter();
				FilterModel.BeforeFilter();
				RootTreeItemModelsView.Refresh();
				GenerateFilterHeader();
				IsFilterVisible = false;
			}
		}

		void EditArea() {
		    if (IsRentContract) return;
			if (SelectedCropZones.Any() && SelectedCropZones.Sum(x => x.SelectedArea.Value) > 0) {
                EditAreaValue = (decimal)SelectedCropZones.Sum(x => x.SelectedArea.Value);
				Messenger.Default.Send(new ShowPopupMessage() {
					ScreenDescriptor = new ScreenDescriptor(typeof(Views.Secondary.Application.EditApplicationAreaView), Strings.ScreenDescriptor_EditArea_Text.ToLower(), this)
				});
			}
		}

		void ClosePopUp() {
			//TotalAreaText = EditAreaValue.ToString();

            TotalAreaText = EditAreaValue.ToString();
            totalArea = TotalArea.Unit.GetMeasure((double)EditAreaValue, null);
            RaisePropertyChanged(() => TotalArea);
			Messenger.Default.Send(new HidePopupMessage());
		}

		void CancelPopUp() {
			Messenger.Default.Send(new HidePopupMessage());
		}

		decimal percent;
		public decimal Percent {
			get { return percent; }
			set {
				percent = value;
				RaisePropertyChanged(() => Percent);

                //should always get initial value of crop area
				var area = SelectedCropZones.Sum(x => x.InitialArea.Value);

				if (editAreaValue != ((decimal)area * percent)) {
					EditAreaValue = ((decimal)area * percent);
					RaisePropertyChanged(() => EditAreaValue);
				}

			}
		}

		decimal editAreaValue;
		public decimal EditAreaValue {
			get { return editAreaValue; }
			set {
				editAreaValue = value;

				var area = SelectedCropZones.Sum(x => x.InitialArea.Value);
                if (area > 0)
                {
                    if (Percent != (editAreaValue / (decimal)area))
                    {
                        Percent = editAreaValue / (decimal)area;
                        RaisePropertyChanged(() => Percent);
                    }
                }
			}
		}

		void ApplyFilter() {
			if (FilterModel != null && RootTreeItemModelsView != null) {
				FilterModel.BeforeFilter();
				RootTreeItemModelsView.Refresh();
				GenerateFilterHeader();
				IsFilterVisible = false;
			}
		}

		void GenerateFilterHeader() {
			var originalCount = (from r in RootTreeItemModels
								 from fa in r.Children
								 select fa.Children.Count).Sum();
			var filteredCount = (from r in RootTreeItemModels
								 from fa in r.Children
								 select fa.FilteredChildren.Count).Sum();

			string entityPluralityAwareName = originalCount == 1 ? Strings.Field_Text  : Strings.Fields_Text.ToLower();

			if (originalCount != filteredCount) {
				FilterHeader = $"{filteredCount} ({Strings.Of_Text.ToLower()} {originalCount}) {entityPluralityAwareName}";
			} else {
				//FilterHeader = string.Format("{0} {1}", filteredCount, entityPluralityAwareName);
				FilterHeader = Strings.FilterHeader_AllFields_Text;
			}
		}

		public override void Cleanup() {
			base.Cleanup();
			FilterModel.Cleanup();
		}

        void SSurgoNrcsWfsTool()
        {
            int x = 0;
            //ssurgonrcswfsvm.FieldName = Name;
            //ssurgonrcswfsvm.SelectedField = displayModel.SelectedFeature;
            this.ssurgonrcswfsvm = new SSurgoNrcsWfsViewModel(this.clientEndpoint);

            foreach (var item in SelectedCropZones)
            {
                if (item.MapData != null && !string.IsNullOrEmpty(item.MapData) && item.MapData != "MULTIPOLYGON EMPTY")
                {
                    this.ssurgonrcswfsvm.SelectedFeatures.Add(new ThinkGeo.MapSuite.Core.Feature(item.MapData));
                }
            }
            this.ssurgonrcswfsvm.IsPopupScreen = true;

            //this.ssurgonrcswfsvm.SelectedFeatures = displayModel.SelectedFeatures;
            //ssurgonrcswfsvm.PreviousMapExtent = displayModel.MapExtent;
            //var scalemessage = new ChangeScreenDescriptorMessage() { ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Secondary.Map.SSurgoNrcsWfsView", "SSurgoNrcsWfsVM", ssurgonrcswfsvm) };
            //Messenger.Default.Send<ChangeScreenDescriptorMessage>(scalemessage);

            ScreenDescriptor descriptor = null;
            descriptor = new ScreenDescriptor("Landdb.Views.Secondary.Map.SSurgoNrcsWfsView", "SSurgoNrcsWfsVM", ssurgonrcswfsvm);
            //Messenger.Default.Send<ShowPopupMessage>(new ShowPopupMessage() { ScreenDescriptor = descriptor });
            Messenger.Default.Send(new ShowOverlayMessage() { ScreenDescriptor = descriptor });



        }
    }

	public enum CropZoneSelectionEditMode {
		Application,
		Contract,
		Yield
	}

	public class MapUpdatedEventArgs : EventArgs {
		public bool IsUpdated { get; set; }
		public DateTime TimeUpdated { get; set; }
	}
}