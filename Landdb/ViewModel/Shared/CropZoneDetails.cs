﻿using AgC.UnitConversion.Area;
using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Shapes;

namespace Landdb.ViewModel.Shared {
	public class CropZoneDetails : ViewModelBase {
		public CropZoneDetails() { }

		public CropZoneDetails(CropZoneId id, string name, AreaMeasure area, string percent, Path shapePreview) {
			this.Id = id;
			this.Name = name;
			this.Area = area;
			this.Percent = percent;
			this.ShapePreview = shapePreview;
		}

		public CropZoneDetails(CropZoneId id, string name, AreaMeasure area, string percent) : this(id, name, area, percent, null) { }

		public CropZoneId Id { get; set; }
		public string Name { get; set; }
		public string FarmName { get; set; }
		public string FieldName { get; set; }
		public AreaMeasure Area { get; set; }
		public Path ShapePreview { get; set; }
		public string Percent { get; set; }

		public override string ToString() {
			return string.Format("{0} ({1})", Name, Area);
		}
	}
}