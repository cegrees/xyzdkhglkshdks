﻿using AgC.UnitConversion;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Infrastructure;
using Landdb.ValueObjects;
using Landdb.ViewModel.Secondary.Application;
using System;
using System.Collections.ObjectModel;
using Landdb.Resources;

namespace Landdb.ViewModel.Shared
{
    public class AssociatedProductItem : ViewModelBase
    {
        IClientEndpoint clientEndpoint;
        //private AppliedProductViewModel _parent;
        readonly MiniProduct _selectedProduct;
        Action<Guid> _removeProduct;
        private InventoryListItem currentInventoryItem;
        private InventoryListView inventory;

        public AssociatedProductItem(IClientEndpoint clientEndpoint, MiniProduct selectedProduct, Action<Guid> removeProduct)
        {
            this.clientEndpoint = clientEndpoint;
            this._selectedProduct = selectedProduct;
            this._removeProduct += removeProduct;

            inventory = clientEndpoint.GetView<InventoryListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear)).GetValue(new InventoryListView());
            Remove = new RelayCommand(RemoveProduct);
        }

        //public AssociatedProductItem(IClientEndpoint clientEndpoint)
        //{
        //    this.clientEndpoint = clientEndpoint;

        //    inventory = clientEndpoint.GetView<InventoryListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear)).GetValue(new InventoryListView());
        //    Remove = new RelayCommand(RemoveProduct);
        //}

        public RelayCommand Remove { get; set; }

        private ProductId _productId { get; set; }
        public ProductId ProductID
        {
            get
            {
                return _productId;
            }
            set
            {
                _productId = value;
                 mlp = clientEndpoint.GetMasterlistService().GetProduct(ProductID);
                UpdateInventoryListItem();
            }
        }
        private MiniProduct mlp { get; set; }
        public Guid TrackingID { get; set; }
        public string ProductName { get; set; }
        private decimal _ratePerAreaValue;
        public decimal RatePerAreaValue { get { return _ratePerAreaValue; } set { _ratePerAreaValue = value; RaisePropertyChanged(() => RatePerAreaValue); } }
        public IUnit RatePerAreaUnit { get; set; }
        private decimal _totalProductValue;
        public decimal TotalProductValue { get { return _totalProductValue; } set { _totalProductValue = value; RaisePropertyChanged(() => TotalProductValue); } }
        public IUnit TotalProductUnit { get; set; }
        public AssociatedProductRateType CustomRateType { get; set; }
        public decimal CustomRateValue { get; set; }
        public IUnit CustomUnit { get; set; }
        private bool _hasCost;
        public bool HasCost { get { return _hasCost; } set { _hasCost = value; RaisePropertyChanged(() => HasCost); } }
        public decimal CostPerUnitValue { get; set; }
        public IUnit CostPerUnitUnit { get; set; }
        private CompositeUnit DisplayCustomUnit { get { return GetCompositeUnit(); } }
        public string CustomRateDisplay { get { return string.Format("{0} {1}", CustomRateValue.ToString("N2"), DisplayCustomUnit.FullDisplay); } }
        public string RateDisplay { get { return string.Format("{0} {1} / {2}", RatePerAreaValue.ToString("N2"), RatePerAreaUnit.AbbreviatedDisplay, ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit); } }
        public string TotalDisplay { get { return string.Format("{0} {1}", TotalProductValue.ToString("N2"), TotalProductUnit.AbbreviatedDisplay); } }
        public decimal TotalCost {
            get
            {
                if (TotalProductUnit != null && HasCost)
                {
                    if (CostPerUnitUnit != null && !string.IsNullOrEmpty(CostPerUnitUnit.Name))
                    {
                        var totalMeasure = TotalProductUnit.GetMeasure((double)TotalProductValue, mlp.Density);
                        var convertedTotalMeasure = totalMeasure.Unit != CostPerUnitUnit && totalMeasure.CanConvertTo(CostPerUnitUnit) ? totalMeasure.GetValueAs(CostPerUnitUnit) : totalMeasure.Value;
                        var totalCost = CostPerUnitValue * (decimal)convertedTotalMeasure;

                        return totalCost;
                    }
                    else
                    {
                        var avgPriceUnit = UnitFactory.GetPackageSafeUnit(AveragePriceItem.PriceUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                        var totalMeasure = TotalProductUnit.GetMeasure((double)TotalProductValue, mlp.Density);
                        var convertedTotalMeasure = totalMeasure.Unit != avgPriceUnit && totalMeasure.CanConvertTo(avgPriceUnit) ? totalMeasure.GetValueAs(avgPriceUnit) : totalMeasure.Value;
                        var totalCost = AveragePriceItem.Price * (decimal)convertedTotalMeasure;

                        return totalCost;
                    }
                }
                else { return 0m; }
            }
        }

        private PriceItem _averagePriceItem;
        public PriceItem AveragePriceItem
        {
            get { return _averagePriceItem; }
            set
            {
                _averagePriceItem = value;
                RaisePropertyChanged(() => AveragePriceItem);
                //RecalculateItem();
            }
        }

        private string _selectedPriceText;
        public string SelectedPriceText
        {
            get { return _selectedPriceText; }
            set
            {
                if (string.IsNullOrWhiteSpace(value)) { return; }

                if (_selectedPriceText != value && (SelectedPriceItem == null || value != SelectedPriceItem.ToString()))
                {
                    _selectedPriceText = value;
                    decimal price = 0m;
                    var seeThis = decimal.TryParse(SelectedPriceText, out price);

                    if (decimal.TryParse(_selectedPriceText, out price))
                    {
                        var priceUnit = currentInventoryItem == null || currentInventoryItem.AveragePriceUnit == null ? mlp.StdPackageUnit : currentInventoryItem.AveragePriceUnit;
                        PriceItem priceItem = new PriceItem(price, priceUnit);
                        if (!PriceItems.Contains(priceItem)) { PriceItems.Add(priceItem); }
                        SelectedPriceItem = priceItem;
                        _selectedPriceText = priceItem.ToString();
                    }
                    else {
                        //INCASE OF PARSE FAILURE   
                        string parsedString = string.Empty;
                        for (int i = 0; i < _selectedPriceText.Length; i++)
                        {
                            if (Char.IsDigit(_selectedPriceText[i]) || _selectedPriceText[i] == '.')
                            {
                                parsedString += _selectedPriceText[i];
                            }
                        }

                        if (decimal.TryParse(parsedString, out price))
                        {
                            var priceUnit = currentInventoryItem == null || currentInventoryItem.AveragePriceUnit == null ? mlp.StdPackageUnit : currentInventoryItem.AveragePriceUnit;
                            PriceItem priceItem = new PriceItem(price, priceUnit);
                            if (!PriceItems.Contains(priceItem)) { PriceItems.Add(priceItem); }
                            SelectedPriceItem = priceItem;
                            _selectedPriceText = priceItem.ToString();
                        }
                    }
                }
                else {
                    _selectedPriceText = _selectedPriceItem.ToString();
                }
            }
        }

        private PriceItem _selectedPriceItem;
        public PriceItem SelectedPriceItem
        {
            get { return _selectedPriceItem; }
            set
            {
                _selectedPriceItem = value;

                RaisePropertyChanged(() => SelectedPriceItem);
            }
        }

        private CompositeUnit GetCompositeUnit()
        {
            CompositeUnit displayUnit = new CompositeUnit();
            switch (CustomRateType.ToString())
            {
                case "BySeed":
                    displayUnit = new CompositeUnit(CustomUnit.AbbreviatedDisplay, 1, _selectedProduct.StdUnit);
                    break;
                case "ByBag":
                    displayUnit = new CompositeUnit(CustomUnit.AbbreviatedDisplay, 1, Strings.Unit_Bag_Text);
                    break;
                case "ByCWT":
                    displayUnit = new CompositeUnit(CustomUnit.AbbreviatedDisplay, 100, _selectedProduct.StdUnit );
                    break;
                case "ByRow":
                    displayUnit = new CompositeUnit(CustomUnit.AbbreviatedDisplay, 1000, Strings.Unit_LengthRow_Text);
                    break;
            }

            return displayUnit;
        }

        public ObservableCollection<PriceItem> PriceItems { get; private set; }

        void RemoveProduct()
        {
            _removeProduct(TrackingID);
        }


        private void UpdateInventoryListItem()
        {
            currentInventoryItem = inventory.Products.ContainsKey(ProductID) ? inventory.Products[ProductID] : null;

            if (currentInventoryItem != null)
            {
                var priceUnit = currentInventoryItem.AveragePriceUnit != null ? currentInventoryItem.AveragePriceUnit : UnitFactory.GetPackageSafeUnit(mlp.StdPackageUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit).Name;
                var priceItem = new PriceItem((decimal)currentInventoryItem.AveragePriceValue, priceUnit, true);
                AveragePriceItem = priceItem;
            }
            else {
                var psu = UnitFactory.GetPackageSafeUnit(mlp.StdPackageUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                var priceItem = new PriceItem(0m, psu.Name, true);
                AveragePriceItem = priceItem;
            }

            UpdatePriceItems();
        }

        private void UpdatePriceItems()
        {
            PriceItems = new ObservableCollection<PriceItem>();

            if (currentInventoryItem != null)
            {
                var priceItem = new PriceItem((decimal)currentInventoryItem.AveragePriceValue, currentInventoryItem.AveragePriceUnit, true);
                PriceItems.Add(priceItem);
                SelectedPriceItem = priceItem;
            }
            else {
                if (ProductID == null)
                {
                    SelectedPriceItem = null;
                }
                else {
                    var priceItem = new PriceItem(0m, mlp.StdPackageUnit, false);
                    PriceItems.Add(priceItem);
                    SelectedPriceItem = null;
                }
            }

            var cachedPrices = clientEndpoint.GetView<Landdb.Domain.ReadModels.Product.SpecificPriceCacheView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear));
            if (cachedPrices.HasValue && mlp != null && cachedPrices.Value.Prices.ContainsKey(mlp.Id))
            {
                var priceList = cachedPrices.Value.Prices[mlp.Id];
                foreach (var p in priceList)
                {
                    PriceItems.Add(new PriceItem(p.Price, p.PriceUnit, false));
                }
            }
        }

    }
}
