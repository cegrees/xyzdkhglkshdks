﻿using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using System;
using System.ComponentModel.DataAnnotations;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Shared.Popups {
	public class UpdateEntityContactInfoViewModel : ValidationViewModelBase {

		private readonly string originalName;
		private readonly string originalPhoneNumber;
		private readonly string originalEmailAddress;

		readonly Action<UpdateEntityContactInfoViewModel> contactInfoUpdated;

		public UpdateEntityContactInfoViewModel(string originalName, string originalPhoneNumber, string originalEmailAddress, Action<UpdateEntityContactInfoViewModel> onContactInfoUpdated) {
			this.originalName = originalName;
			this.originalPhoneNumber = originalPhoneNumber;
			this.originalEmailAddress = originalEmailAddress;

			Name = originalName;
			PhoneNumber = originalPhoneNumber;
			EmailAddress = originalEmailAddress;

			contactInfoUpdated += onContactInfoUpdated;

			CompleteCommand = new RelayCommand(onComplete);
			CancelCommand = new RelayCommand(onCancel);
		}

		public ICommand CompleteCommand { get; }
		public ICommand CancelCommand { get; }

		private string _name;
		[Required(AllowEmptyStrings = false, ErrorMessageResourceName = "ErrorMessage_NameIsRequired_Text", ErrorMessageResourceType = typeof(Strings))]
        public string Name {
			get { return _name != null ? _name.Trim() : null; }
			set {
				_name = value;
				RaisePropertyChanged(() => Name);
			}
		}

		private string _phoneNumber;
		public string PhoneNumber {
			get { return _phoneNumber != null ? _phoneNumber.Trim() : null; }
			set {
				_phoneNumber = value;
				RaisePropertyChanged(() => PhoneNumber);
			}
		}

		private string _emailAddress;
		public string EmailAddress {
			get { return _emailAddress != null ? _emailAddress.Trim() : null; }
			set {
				_emailAddress = value;
				RaisePropertyChanged(() => EmailAddress);
			}
		}

		public PhoneNumber GetPhoneNumberValueObject() {
			PhoneNumber retPhoneNumber = null;

			if (!string.IsNullOrWhiteSpace(PhoneNumber)) {
				retPhoneNumber = new PhoneNumber(null, PhoneNumber);
			}

			return retPhoneNumber;
		}

		public Email GetEmailAddressValueObject() {
			Email retEmailAddress = null;

			if (!string.IsNullOrWhiteSpace(EmailAddress)) {
				retEmailAddress = new Email(EmailAddress, Name);
			}

			return retEmailAddress;
		}

		public bool HasChanges =>
			Name != originalName
				|| PhoneNumber != originalPhoneNumber
				|| EmailAddress != originalEmailAddress;

		void onComplete() {
			if (ValidateViewModel()) {
				contactInfoUpdated(this);
			}
		}

		void onCancel() {
			contactInfoUpdated(null);
		}
	}
}