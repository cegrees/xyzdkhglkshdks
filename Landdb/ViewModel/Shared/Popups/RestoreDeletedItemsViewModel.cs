﻿using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using System;
using System.Collections.Generic;
using System.Windows.Input;
using System.Windows.Threading;

namespace Landdb.ViewModel.Shared.Popups {
	public class RestoreDeletedItemsViewModel {

		private readonly IClientEndpoint clientEndpoint;
		private readonly Dispatcher dispatcher;

		private readonly Action<RestoreDeletedItemsViewModel> restoreItem;

		public RestoreDeletedItemsViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, string entityName, IEnumerable<RestorableItem> restorableItems, Action<RestoreDeletedItemsViewModel> onRestoreItem) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			RestorableItems = restorableItems;

			restoreItem += onRestoreItem;

			CompleteCommand = new RelayCommand(onComplete);
			CancelCommand = new RelayCommand(onCancel);
		}

		public ICommand CompleteCommand { get; }
		public ICommand CancelCommand { get; }

		public IEnumerable<RestorableItem> RestorableItems { get; }

		public RestorableItem SelectedItem { get; set; }

		private void onComplete() {
			restoreItem(this);
		}

		private void onCancel() {
			restoreItem(null);
		}
	}
}