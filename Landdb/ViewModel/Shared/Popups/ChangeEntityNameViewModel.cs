﻿using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using System;
using System.ComponentModel.DataAnnotations;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Shared.Popups {
	public class ChangeEntityNameViewModel : ValidationViewModelBase {

		private readonly string originalName;

		private readonly Action<ChangeEntityNameViewModel> nameChanged;

		public ChangeEntityNameViewModel(string entityDescription, string originalName, Action<ChangeEntityNameViewModel> onNameChanged) {
			this.originalName = originalName;
			EntityName = originalName;

			nameChanged += onNameChanged;

			if (!string.IsNullOrWhiteSpace(entityDescription)) {
				EntityDescription = entityDescription;
			} else {
				EntityDescription = Strings.Name_Text;
			}

			CompleteCommand = new RelayCommand(onComplete);
			CancelCommand = new RelayCommand(onCancel);
		}

		public RelayCommand CompleteCommand { get; }
		public RelayCommand CancelCommand { get; }

		public string EntityDescription { get; }

		private string _entityName = string.Empty;
        [Required(AllowEmptyStrings = false, ErrorMessageResourceName = "ErrorMessage_ValueIsRequired_Text", ErrorMessageResourceType = typeof(Strings))]
        public string EntityName {
			get { return _entityName != null ? _entityName.Trim() : null; }
			set {
				_entityName = value;
				RaisePropertyChanged(() => EntityName);
			}
		}

		public bool HasChanges => EntityName != originalName;

		private void onComplete() {
			if (ValidateViewModel()) {
				nameChanged(this);
			}
		}

		private void onCancel() {
			nameChanged(null);
		}
	}
}