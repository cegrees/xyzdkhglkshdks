﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure.Messages;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Landdb.ViewModel.Shared.Popups {
	public class MultiOptionConfirmationDialogViewModel : ViewModelBase {

		private readonly Task action1;
		private readonly Task action2;

		public MultiOptionConfirmationDialogViewModel(string title, string description, string action1Text, Task onAction1, string action2Text, Task onAction2) {
			Title = title;
			Description = description;
			Action1Text = action1Text;
			Action2Text = action2Text;

			action1 = onAction1;
			action2 = onAction2;

			Action1Command = new RelayCommand(OnAction1Command);
			Action2Command = new RelayCommand(OnAction2Command);
		}

		public ICommand Action1Command { get; }
		public ICommand Action2Command { get; }

		void OnAction1Command() {
			Messenger.Default.Send(new HidePopupMessage());
			action1.RunSynchronously();
		}

		void OnAction2Command() {
			Messenger.Default.Send(new HidePopupMessage());
			action2.RunSynchronously();
		}

		// TODO: do these need to be full properties or can we just make them get-only?

		private string _action1Text;
		public string Action1Text {
			get { return _action1Text; }
			set {
				_action1Text = value;
				RaisePropertyChanged(() => Action1Text);
			}
		}

		private string _action2Text;
		public string Action2Text {
			get { return _action2Text; }
			set {
				_action2Text = value;
				RaisePropertyChanged(() => Action2Text);
			}
		}

		private string _title;
		public string Title {
			get { return _title; }
			set {
				_title = value;
				RaisePropertyChanged(() => Title);
			}
		}

		private string _description;
		public string Description {
			get { return _description; }
			set {
				_description = value;
				RaisePropertyChanged(() => Description);
			}
		}
	}
}
