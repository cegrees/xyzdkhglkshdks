﻿using Landdb.Resources;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure.Messages;
using System;
using System.Windows.Input;
using Landdb.Client.Infrastructure.DisplayItems;
using Landdb.Infrastructure;

namespace Landdb.ViewModel.Shared.Popups {
	public class EditActiveIngredientLoadViewModel : ViewModelBase {

		private readonly Action<EditActiveIngredientLoadViewModel> update;

		public EditActiveIngredientLoadViewModel(decimal currentMaximum, Action<EditActiveIngredientLoadViewModel> onUpdated) {
			_maximumValue = currentMaximum;
			update = onUpdated;

			UpdateCommand = new RelayCommand(onUpdate);
			CancelCommand = new RelayCommand(onClose);
		}

        private static string AreaText => ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit.ToLower().Equals("acre") ?
	            AreaUnitDisplayItem.GetAreaUnitDisplayTextFor(AreaUnitDisplayType.Acre) :
	            AreaUnitDisplayItem.GetAreaUnitDisplayTextFor(AreaUnitDisplayType.Hectare);

	    private static string WeightText => ApplicationEnvironment.CurrentDataSource.DatasourceCulture.ToLower().Equals("en-us") ?
	        WeightUnitDisplayItem.GetWeightUnitDisplayTextFor(WeightUnitDisplayType.Pound) :
	        WeightUnitDisplayItem.GetWeightUnitDisplayTextFor(WeightUnitDisplayType.Kilogram);

        public static string WeightPerAreaDisplayText =>
	        String.Format(Strings.Format_WeightPerArea, WeightText, AreaText).ToLower();

        public ICommand UpdateCommand { get; }
		public ICommand CancelCommand { get; }

		private decimal _maximumValue;
		public decimal MaximumValue {
			get { return _maximumValue; }
			set {
				_maximumValue = value;
				RaisePropertyChanged(() => MaximumValue);
			}
		}

		void onClose() {
			Messenger.Default.Send(new HidePopupMessage());
		}

		void onUpdate() {
			update(this);
			onClose();
		}
	}
}
