﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure;
using System;
using System.Windows.Input;
using System.Windows.Threading;

namespace Landdb.ViewModel.Shared.Popups {
	public class UpdateEntityStreetAddressViewModel : ViewModelBase {

		private readonly IClientEndpoint clientEndpoint;
		private readonly Dispatcher dispatcher;

		private readonly StreetAddress originalStreetAddress;

		private readonly Action<UpdateEntityStreetAddressViewModel> addressUpdated;

		public UpdateEntityStreetAddressViewModel(StreetAddress originalStreetAddress, Action<UpdateEntityStreetAddressViewModel> onAddressUpdated) {
			clientEndpoint = ServiceLocator.Get<IClientEndpoint>();
			dispatcher = System.Windows.Application.Current.Dispatcher;

			this.originalStreetAddress = originalStreetAddress;

			StreetAddress = new StreetAddressViewModel(clientEndpoint, dispatcher, originalStreetAddress);

			addressUpdated += onAddressUpdated;

			CompleteCommand = new RelayCommand(onComplete);
			CancelCommand = new RelayCommand(onCancel);
		}

		public ICommand CompleteCommand { get; }
		public ICommand CancelCommand { get; }

		public StreetAddressViewModel StreetAddress { get; }

		public bool HasChanges => StreetAddress.GetValueObject() != originalStreetAddress;

		void onComplete() {
			addressUpdated(this);
		}

		void onCancel() {
			addressUpdated(null);
		}
	}
}
