﻿using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;

namespace Landdb.ViewModel.Shared.Popups {
	public class ChangeEntityComboBoxSelection<TEquatableListItem> where TEquatableListItem : IEquatable<TEquatableListItem> {

		private readonly TEquatableListItem originalSelectedItem;

		private readonly Action<ChangeEntityComboBoxSelection<TEquatableListItem>> selectionChanged;

		public ChangeEntityComboBoxSelection(TEquatableListItem currentSelectedItem, IEnumerable<TEquatableListItem> availableListItems, string entityDescription, Action<ChangeEntityComboBoxSelection<TEquatableListItem>> onSelectionChanged) {
			originalSelectedItem = currentSelectedItem;

			EntityDescription = entityDescription;

			AvailableListItems = availableListItems.ToList();

			SelectedListItem = availableListItems.FirstOrDefault(x => x.Equals(currentSelectedItem));

			selectionChanged += onSelectionChanged;

			CompleteCommand = new RelayCommand(onComplete);
			CancelCommand = new RelayCommand(onCancel);
		}

		public ICommand CompleteCommand { get; }
		public ICommand CancelCommand { get; }

		public List<TEquatableListItem> AvailableListItems { get; }

		public string EntityDescription { get; }

		public TEquatableListItem SelectedListItem { get; set; }

		public bool HasChanges => !SelectedListItem.Equals(originalSelectedItem);

		private void onComplete() {
			selectionChanged(this);
		}

		private void onCancel() {
			selectionChanged(null);
		}
	}
}
