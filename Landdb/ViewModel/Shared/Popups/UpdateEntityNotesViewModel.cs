﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using System;
using System.Windows.Input;
using System.Windows.Threading;

namespace Landdb.ViewModel.Shared.Popups {
	public class UpdateEntityNotesViewModel : ViewModelBase {

		private readonly string originalNotes;

		private readonly Action<UpdateEntityNotesViewModel> notesUpdated;

		public UpdateEntityNotesViewModel(string originalNotes, Action<UpdateEntityNotesViewModel> onNotesUpdated) {
			this.originalNotes = originalNotes;

			Notes = originalNotes;

			notesUpdated += onNotesUpdated;

			CompleteCommand = new RelayCommand(onComplete);
			CancelCommand = new RelayCommand(onCancel);
		}

		public ICommand CompleteCommand { get; }
		public ICommand CancelCommand { get; }

		private string _notes = string.Empty;
		public string Notes {
			get { return _notes != null ? _notes.Trim() : null; }
			set {
				_notes = value;
				RaisePropertyChanged(() => Notes);
			}
		}

		public bool HasChanges => Notes != originalNotes;

		private void onComplete() {
			notesUpdated(this);
		}

		private void onCancel() {
			notesUpdated(null);
		}
	}
}