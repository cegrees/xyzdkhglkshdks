﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Landdb.Resources;

namespace Landdb.ViewModel.Shared.Popups {
    public class MessageDialogViewModel : ViewModelBase {
        string title;
        string description;
        string dismissButtonName;
        Action onDismissAction;

        public MessageDialogViewModel(string title, string description, string dismissButtonName = null, Action onDismissAction = null) {
            Title = title;
            Description = description;
            DismissButtonName = dismissButtonName; 
            this.onDismissAction = onDismissAction;

            DismissCommand = new RelayCommand(Dismiss);
        }

        public ICommand DismissCommand { get; }

        public string Title {
            get { return title; }
            set {
                title = value;
                RaisePropertyChanged(() => Title);
            }
        }

        public string Description {
            get { return description; }
            set {
                description = value;
                RaisePropertyChanged(() => Description);
            }
        }

        public string DismissButtonName {
            get { return dismissButtonName ?? Strings.ButtonText_OK; }
            set {
                dismissButtonName = value;
                RaisePropertyChanged(() => DismissButtonName);
            }
        }

        void Dismiss() {
            Messenger.Default.Send(new HidePopupMessage());
			onDismissAction?.Invoke();
		}
    }
}
