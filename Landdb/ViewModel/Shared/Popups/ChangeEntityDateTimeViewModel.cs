﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Shared.Popups {
	public class ChangeEntityDateTimeViewModel : ViewModelBase {

		private readonly DateTime originalStartDateTime;

		private readonly Action<ChangeEntityDateTimeViewModel> startDateTimeChanged;

		public ChangeEntityDateTimeViewModel(DateTime originalStartDateTime, EntityDateTimeDisplayMode displayMode, Action<ChangeEntityDateTimeViewModel> onStartDateTimeChanged) {
			this.originalStartDateTime = originalStartDateTime;

			StartDate = originalStartDateTime;
			StartTime = originalStartDateTime;

			setDateTimeVisibility(displayMode);

			startDateTimeChanged += onStartDateTimeChanged;

			CompleteCommand = new RelayCommand(onComplete);
			CancelCommand = new RelayCommand(onCancel);
		}

		public ICommand CompleteCommand { get; }
		public ICommand CancelCommand { get; }

		public string FormTitle { get; private set; }

		public DateTime StartDate { get; set; }
		public DateTime StartTime { get; set; }

		public Visibility DateVisibility { get; private set; }
		public Visibility TimeVisibility { get; private set; }

		public bool HasChanges => GetStartDateTime() != originalStartDateTime;

		public DateTime GetStartDateTime() => StartDate.Date.Add(StartTime.TimeOfDay);

		private void onComplete() {
			startDateTimeChanged(this);
		}

		private void onCancel() {
			startDateTimeChanged(null);
		}

		private void setDateTimeVisibility(EntityDateTimeDisplayMode mode) {
			switch (mode) {
				case EntityDateTimeDisplayMode.DateTime:
					FormTitle = Strings.FormTitle_ChangeDateAndTime_Text;
					DateVisibility = Visibility.Visible;
					TimeVisibility = Visibility.Visible;
					break;
				case EntityDateTimeDisplayMode.DateOnly:
					FormTitle = Strings.FormTitle_ChangeDate_Text;
					DateVisibility = Visibility.Visible;
					TimeVisibility = Visibility.Collapsed;
					break;
				case EntityDateTimeDisplayMode.TimeOnly:
					FormTitle = Strings.FormTitle_ChangeTime_Text;
					DateVisibility = Visibility.Collapsed;
					TimeVisibility = Visibility.Visible;
					break;
				default:
					break;
			}
		}

		public enum EntityDateTimeDisplayMode {
			DateTime,
			DateOnly,
			TimeOnly,
		}
	}
}