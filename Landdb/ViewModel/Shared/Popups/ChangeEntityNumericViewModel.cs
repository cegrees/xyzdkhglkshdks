﻿using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using System;
using System.Windows.Input;
using System.Windows.Threading;

namespace Landdb.ViewModel.Shared.Popups {
	public class ChangeEntityNumericViewModel : ValidationViewModelBase {

		private readonly decimal originalValue;
		private readonly string stringFormat;

		private readonly Action<ChangeEntityNumericViewModel> valueChanged;

		public ChangeEntityNumericViewModel(string valueDescription, decimal originalValue, string stringFormat, string warningText, Action<ChangeEntityNumericViewModel> onValueChanged) {
			ValueDescription = valueDescription;
			WarningText = warningText;

			this.originalValue = originalValue;
			this.stringFormat = stringFormat;

			NewValue = originalValue;

			valueChanged += onValueChanged;

			CompleteCommand = new RelayCommand(onComplete);
			CancelCommand = new RelayCommand(onCancel);
		}

		public ICommand CompleteCommand { get; }
		public ICommand CancelCommand { get; }

		public string ValueDescription { get; }
		public string WarningText { get; }

		public decimal NewValue { get; set; }

		public string NewValueConverter {
			get { return NewValue.ToString(stringFormat); }
			set {
				decimal parsedValue = 0;
				decimal.TryParse(value, out parsedValue);
				NewValue = parsedValue;
				RaisePropertyChanged(() => NewValueConverter);
			}
		}

		public bool HasChanges => NewValue != originalValue;
		public bool IsWarningTextVisible => !string.IsNullOrWhiteSpace(WarningText);

		private void onComplete() {
			valueChanged(this);
		}

		private void onCancel() {
			valueChanged(null);
		}
	}
}
