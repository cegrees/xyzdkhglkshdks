﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure.Messages;
using System;
using System.Windows.Input;

namespace Landdb.ViewModel.Shared.Popups {
	public class YesNoConfirmationDialogViewModel : ViewModelBase {
		string title;
		string description;
		Action onYesAction;
		Action onNoAction;

		public YesNoConfirmationDialogViewModel(string title, string description, Action onYesAction, Action onNoAction) {
			Title = title;
			Description = description;
			this.onYesAction = onYesAction;
			this.onNoAction = onNoAction;

			YesCommand = new RelayCommand(Yes);
			NoCommand = new RelayCommand(No);
		}

		public ICommand YesCommand { get; }
		public ICommand NoCommand { get; }

		public string Title {
			get { return title; }
			set {
				title = value;
				RaisePropertyChanged(() => Title);
			}
		}

		public string Description {
			get { return description; }
			set {
				description = value;
				RaisePropertyChanged(() => Description);
			}
		}

		void Yes() {
			Messenger.Default.Send(new HidePopupMessage());
			onYesAction();
		}
		void No() {
			Messenger.Default.Send(new HidePopupMessage());
			onNoAction();
		}
	}
}
