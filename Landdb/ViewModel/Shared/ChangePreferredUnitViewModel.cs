﻿using AgC.UnitConversion;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Secondary.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Shared {
    public class ChangePreferredUnitViewModel : ViewModelBase {

        IUnit selectedUnit;
        string originalUnitName;
        Action<IUnit> onUnitSelected;

        public ChangePreferredUnitViewModel(IList<IUnit> allowedRateUnits, string originalUnitName, Action<IUnit> onUnitSelected) {
            this.AllowedRateUnits = allowedRateUnits;
            this.originalUnitName = originalUnitName;
            this.onUnitSelected = onUnitSelected;

            SelectedUnit = (from u in AllowedRateUnits
                             where u.Name == originalUnitName
                             select u).FirstOrDefault();

            AcceptCommand = new RelayCommand(Accept);
            CancelCommand = new RelayCommand(Cancel);
        }

        public RelayCommand AcceptCommand { get; private set; }
        public RelayCommand CancelCommand { get; private set; }

        public IList<IUnit> AllowedRateUnits { get; private set; }

        public IUnit SelectedUnit {
            get { return selectedUnit; }
            set {
                selectedUnit = value;
                RaisePropertyChanged("SelectedUnit");
            }
        }

        void Accept() {
            onUnitSelected(SelectedUnit);
            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
        }

        void Cancel() {
            onUnitSelected(null);
            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
        }
    }
}
