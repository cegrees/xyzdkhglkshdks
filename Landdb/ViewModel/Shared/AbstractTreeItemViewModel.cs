﻿using Landdb.ViewModel.Fields;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows.Data;

namespace Landdb.ViewModel {
	public abstract class AbstractTreeItemViewModel : ValidationViewModelBase {

		ObservableCollection<AbstractTreeItemViewModel> children;
		CollectionView filteredChildren;
		AbstractTreeItemViewModel parent;

		bool isExpanded;
		bool isSelected;
		bool? isChecked = false;
		Action<AbstractTreeItemViewModel> OnCheckedChanged;
		Action<List<CropZoneId>> OnParentComplete;
		Predicate<object> filter;

		protected AbstractTreeItemViewModel(AbstractTreeItemViewModel parent, Action<AbstractTreeItemViewModel> onCheckedChanged, Action<List<CropZoneId>> onParentComplete, Predicate<object> filter = null) {
			this.parent = parent;
			children = new ObservableCollection<AbstractTreeItemViewModel>();
			OnCheckedChanged = onCheckedChanged;
			OnParentComplete = onParentComplete;
			this.filter = filter;
		}

		public ObservableCollection<AbstractTreeItemViewModel> Children {
			get { return children; }
			protected set {
				children = value;
				filteredChildren = null;
				RaisePropertyChanged(() => Children);
				RaisePropertyChanged(() => FilteredChildren);
			}
		}

		public CollectionView FilteredChildren {
			get {
				if (filteredChildren == null) {
					filteredChildren = (CollectionView)CollectionViewSource.GetDefaultView(Children);
					filteredChildren.Filter = filter;
				}

				return filteredChildren;
			}
		}

		public abstract IIdentity Id { get; }

		public bool? IsChecked {
			get { return isChecked; }
			set { this.SetIsChecked(value, true, true); }
		}

		public bool IsExpanded {
			get { return isExpanded; }
			set {
				if (value != isExpanded) {
					isExpanded = value;
					RaisePropertyChanged(() => IsExpanded);
				}

				// Expand all the way up to the root
				if (isExpanded && parent != null) {
					parent.IsExpanded = true;
				}
			}
		}

		public bool IsSelected {
			get { return isSelected; }
			set {
				if (isSelected != value) {
					isSelected = value;
					RaisePropertyChanged(() => IsSelected);
				}
			}
		}

		public AbstractTreeItemViewModel Parent {
			get { return parent; }
		}

		public virtual bool CanBeDeleted() {
			return !Children.Any();
		}

		public void CheckMatches(Func<AbstractTreeItemViewModel, bool> matchLambda) {
			if (matchLambda(this)) {
				this.IsChecked = true;
			}

			foreach (var child in Children) {
				child.CheckMatches(matchLambda);
			}
		}

		void SetIsChecked(bool? value, bool updateChildren, bool updateParent) {
			if (value == isChecked) { return; }
			isChecked = value;

			if (updateChildren && isChecked.HasValue) {
				if (filteredChildren != null) {
					foreach (AbstractTreeItemViewModel c in filteredChildren) {
						c.SetIsChecked(isChecked, true, false);
					}
				} else {
					foreach (var c in Children) {
						c.SetIsChecked(isChecked, true, false);
					}
				}
			}

			if (updateParent && parent != null) {
				//BUILD LIST OF CHECKED CROP ZONES
				#region Builds Crop Zone List to Populate Map
				var czIDList = new List<CropZoneId>();
				List<CropZoneTreeItemViewModel> czList = new List<CropZoneTreeItemViewModel>();
				if (parent is GrowerCropTreeItemViewModel || parent is GrowerTreeItemViewModel) {
					if (parent is GrowerTreeItemViewModel) {
						czList = (from farm in parent.children
								  from field in farm.children
								  from cz in field.children
								  select cz as CropZoneTreeItemViewModel).ToList();
					} else {
						czList = (from crop in parent.children
								  from farm in crop.children
								  from field in farm.children
								  from cz in field.children
								  select cz as CropZoneTreeItemViewModel).ToList();
					}

					foreach (var cz in czList) {
						if (cz.isChecked.HasValue && cz.isChecked == true) {
							czIDList.Add(cz.Id as CropZoneId);
						}
					}

					if (OnParentComplete != null) {
						OnParentComplete(czIDList);
					}
				} else if (parent is CropTreeItemViewModel) {
					var master = parent.Parent;
					if (master.parent is GrowerCropTreeItemViewModel) { master = master.parent; }
					if (master is GrowerTreeItemViewModel) {
						czList = (from farm in master.children
								  from field in farm.children
								  from cz in field.children
								  select cz as CropZoneTreeItemViewModel).ToList();
					} else {
						czList = (from crop in master.children
								  from farm in crop.children
								  from field in farm.children
								  from cz in field.children
								  select cz as CropZoneTreeItemViewModel).ToList();
					}

					foreach (var cz in czList) {
						if (cz.isChecked.HasValue && cz.isChecked == true) {
							czIDList.Add(cz.Id as CropZoneId);
						}
					}

					if (OnParentComplete != null) {
						OnParentComplete(czIDList);
					}
				} else if (parent is FarmTreeItemViewModel) {
					var master = parent.Parent;
					if (master.parent is GrowerCropTreeItemViewModel) { master = master.parent; }
					if (master is GrowerTreeItemViewModel) {
						czList = (from farm in master.children
								  from field in farm.children
								  from cz in field.children
								  select cz as CropZoneTreeItemViewModel).ToList();
					} else {
						czList = (from crop in master.children
								  from farm in crop.children
								  from field in farm.children
								  from cz in field.children
								  select cz as CropZoneTreeItemViewModel).ToList();
					}

					foreach (var cz in czList) {
						if (cz.isChecked.HasValue && cz.isChecked == true) {
							czIDList.Add(cz.Id as CropZoneId);
						}
					}

					if (OnParentComplete != null) {
						OnParentComplete(czIDList);
					}
				} else if (parent is FieldTreeItemViewModel) {
					var master = parent.Parent.Parent;
					if (master.parent is GrowerCropTreeItemViewModel) { master = master.parent; }
					if (master is GrowerTreeItemViewModel) {
						czList = (from farm in master.children
								  from field in farm.children
								  from cz in field.children
								  select cz as CropZoneTreeItemViewModel).ToList();
					} else {
						czList = (from crop in master.children
								  from farm in crop.children
								  from field in farm.children
								  from cz in field.children
								  select cz as CropZoneTreeItemViewModel).ToList();
					}

					foreach (var cz in czList) {
						if (cz.isChecked.HasValue && cz.isChecked == true) {
							czIDList.Add(cz.Id as CropZoneId);
						}
					}

					if (OnParentComplete != null) {
						OnParentComplete(czIDList);
					}
				}

				parent.VerifyCheckState();
			} else if (parent == null && updateParent == true) {
				var czIDList = new List<CropZoneId>();
				List<CropZoneTreeItemViewModel> czList = new List<CropZoneTreeItemViewModel>();

				if (this is GrowerCropTreeItemViewModel || this is GrowerTreeItemViewModel) {
					if (this is GrowerTreeItemViewModel) {
						czList = (from farm in this.children
								  from field in farm.children
								  from cz in field.children
								  select cz as CropZoneTreeItemViewModel).ToList();
					} else {
						czList = (from crop in this.children
								  from farm in crop.children
								  from field in farm.children
								  from cz in field.children
								  select cz as CropZoneTreeItemViewModel).ToList();
					}

					foreach (var cz in czList) {
						if (cz.isChecked.HasValue && cz.isChecked == true) {
							czIDList.Add(cz.Id as CropZoneId);
						}
					}

					OnParentComplete?.Invoke(czIDList);
				}
			}
			#endregion

			OnCheckedChanged?.Invoke(this);

			RaisePropertyChanged(() => IsChecked);

		}

		void VerifyCheckState() {
			bool? state = null;
			for (int i = 0; i < Children.Count; ++i) {
				bool? current = Children[i].IsChecked;
				if (i == 0) {
					state = current;
				} else if (state != current) {
					state = null;
					break;
				}
			}

			SetIsChecked(state, false, true);
		}

		#region Validation
		public Func<AbstractTreeItemViewModel, ValidationResult> IsThisTreeItemValid { get; set; }

		[CustomValidation(typeof(AbstractTreeItemViewModel), nameof(ValidateThisTreeItem))]
		public AbstractTreeItemViewModel ValidationTarget => this;

		public static ValidationResult ValidateThisTreeItem(AbstractTreeItemViewModel treeItem, ValidationContext context) {
			ValidationResult result = null;

			var vm = (AbstractTreeItemViewModel)context.ObjectInstance;

			if (vm.IsThisTreeItemValid != null) {
				result = vm.IsThisTreeItemValid(treeItem);
			}

			return result;
		}
		#endregion
	}
}