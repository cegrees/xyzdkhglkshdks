﻿using GalaSoft.MvvmLight;
using Landdb.Client.Infrastructure;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Threading;

namespace Landdb.ViewModel.Shared {
	public class StreetAddressViewModel : ViewModelBase {

		public StreetAddressViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, StreetAddress streetAddress = null)
			: this(streetAddress) {
			StateList = clientEndpoint.GetMasterlistService().GetStateList();

			var matchingState = from state in StateList
								where state.Name == State
									|| state.Abbreviation == State
								select state;

			SelectedState = matchingState.FirstOrDefault();
		}

		public StreetAddressViewModel(StreetAddress streetAddress) {
			if (streetAddress != null) {
				AddressLine1 = streetAddress.AddressLine1;
				AddressLine2 = streetAddress.AddressLine2;
				City = streetAddress.City;
				State = streetAddress.State;
				ZipCode = streetAddress.ZipCode;
				Country = streetAddress.Country;
			}
		}

		public IEnumerable<State> StateList { get; }

		private string _addressLine1;
		public string AddressLine1 {
			get { return !string.IsNullOrWhiteSpace(_addressLine1) ? _addressLine1.Trim() : null; }
			set {
				_addressLine1 = value;
				RaisePropertyChanged(() => AddressLine1);
			}
		}

		private string _addressLine2;
		public string AddressLine2 {
			get { return !string.IsNullOrWhiteSpace(_addressLine2) ? _addressLine2.Trim() : null; }
			set {
				_addressLine2 = value;
				RaisePropertyChanged(() => AddressLine2);
			}
		}

		private string _city;
		public string City {
			get { return !string.IsNullOrWhiteSpace(_city) ? _city.Trim() : null; }
			set {
				_city = value;
				RaisePropertyChanged(() => City);
			}
		}

		private string _state;
		public string State {
			get { return !string.IsNullOrWhiteSpace(_state) ? _state.Trim() : null; }
			private set {
				_state = value;
				RaisePropertyChanged(() => State);
			}
		}

		private string _zipCode;
		public string ZipCode {
			get { return !string.IsNullOrWhiteSpace(_zipCode) ? _zipCode.Trim() : null; }
			set {
				_zipCode = value;
				RaisePropertyChanged(() => ZipCode);
			}
		}

		private string _country;
		public string Country {
			get { return !string.IsNullOrWhiteSpace(_country) ? _country.Trim() : null; }
			set {
				_country = value;
				RaisePropertyChanged(() => Country);
			}
		}

		private State _selectedState = null;
		public State SelectedState {
			get { return _selectedState; }
			set {
				_selectedState = value;

				if (_selectedState != null) {
					State = _selectedState.Abbreviation;
				} else {
					State = null;
				}

				RaisePropertyChanged(() => SelectedState);
			}
		}

		public string CityStateZip {
			get {
				if (!string.IsNullOrWhiteSpace(City) && !string.IsNullOrWhiteSpace(State)) {
					return $"{City}, {State} {ZipCode}".Trim();
				} else if (!string.IsNullOrWhiteSpace(State)) {
					return State;
				} else {
					return null;
				}
			}
		}

		public string MultiLineDisplay {
			get {
				var lines = new List<string>() {
					AddressLine1,
					AddressLine2,
					CityStateZip
				};

				return string.Join("\n", lines.Where(x => !string.IsNullOrWhiteSpace(x)));
			}
		}

		public StreetAddress GetValueObject() => 
			new StreetAddress(
				AddressLine1,
				AddressLine2,
				City,
				State,
				ZipCode,
				Country
			);

		public void Reset() {
			AddressLine1 = null;
			AddressLine2 = null;
			City = null;
			SelectedState = null;
			ZipCode = null;
			Country = null;
		}

		public override string ToString() {
			var lines = new List<string>() {
				AddressLine1,
				AddressLine2,
				CityStateZip
			};

			return string.Join(" | ", lines.Where(x => !string.IsNullOrWhiteSpace(x)));
		}
	}
}
