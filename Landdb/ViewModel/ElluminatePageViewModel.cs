﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GalaSoft.MvvmLight;
using System.Collections.ObjectModel;
using Landdb.Infrastructure;
using System.Reactive.Linq;
using WPFLocalizeExtension.Extensions;

namespace Landdb.ViewModel {
    public class ElluminatePageViewModel : ViewModelBase {
        ObservableCollection<ScreenDescriptor> screens = new ObservableCollection<ScreenDescriptor>();

        public ElluminatePageViewModel()
        {
            //ScreenDescriptor ftmPage = new ScreenDescriptor("Landdb.Views.BlankPage", LocExtension.GetLocalizedValue<string>("Landdb:Strings:PageHeader_Analysis_Fieldprint").ToUpper());
            //if (ApplicationEnvironment.ShowSustainability) {
            //    ftmPage = new ScreenDescriptor("Landdb.Views.Analysis.FieldToMarket.FtmPageView", LocExtension.GetLocalizedValue<string>("Landdb:Strings:PageHeader_Analysis_Fieldprint").ToUpper());
            //}

            //AddPageTabs(false);
        }

        private void AddPageTabs(bool AddSubscribedPages)
        {
            //ScreenDescriptor ftmPage = new ScreenDescriptor("Landdb.Views.Analysis.FieldToMarket.FtmPageView", LocExtension.GetLocalizedValue<string>("Landdb:Strings:PageHeader_Analysis_Fieldprint").ToUpper());
            //ScreenDescriptor budPage = new ScreenDescriptor("Landdb.Views.Analysis.BudgetVarianceView", LocExtension.GetLocalizedValue<string>("Landdb:Strings:PageHeader_Analysis_BudgetVariance").ToUpper());
            //ScreenDescriptor mapPage = new ScreenDescriptor("Landdb.Views.Analysis.Planet.PlanetMapPageView", LocExtension.GetLocalizedValue<string>("Landdb:Strings:PageHeader_Analysis_PlanetMap").ToUpper());

            //if (AddSubscribedPages)
            //{
            //    ScreenDescriptor[] screenDescriptors = new ScreenDescriptor[] {
            //        mapPage
            //    };

            //    screenDescriptors.ToObservable()
            //        .OnTimeline(TimeSpan.FromSeconds(0.2))
            //        .ObserveOn(System.Threading.SynchronizationContext.Current)
            //        .Subscribe(AddScreen);

            //} else
            //{
            //    ScreenDescriptor[] screenDescriptors = new ScreenDescriptor[] {
            //        budPage,
            //        ftmPage,
            //    };

            //    screenDescriptors.ToObservable()
            //        .OnTimeline(TimeSpan.FromSeconds(0.2))
            //        .ObserveOn(System.Threading.SynchronizationContext.Current)
            //        .Subscribe(AddScreen);

            //}
        }

        public ObservableCollection<ScreenDescriptor> Screens {
            get { return screens; }
        }

        void AddScreen(ScreenDescriptor screen) {
            Screens.Add(screen);
        }

        //public void AddTheImageryPage(bool AddSubscribedPages)
        //{
        //    if (AddSubscribedPages)
        //    {
        //        AddPageTabs(AddSubscribedPages);
        //    }
        //}
    }
}