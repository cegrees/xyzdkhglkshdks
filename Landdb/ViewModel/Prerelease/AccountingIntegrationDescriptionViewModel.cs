﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Landdb.ViewModel.Prerelease {
    public class AccountingIntegrationDescriptionViewModel : ViewModelBase {
        public AccountingIntegrationDescriptionViewModel() {
            CancelCommand = new RelayCommand(Cancel);
        }
        public ICommand CancelCommand { get; private set; }

        void Cancel() { Messenger.Default.Send<ShowMainViewMessage>(new ShowMainViewMessage()); }
    }
}
