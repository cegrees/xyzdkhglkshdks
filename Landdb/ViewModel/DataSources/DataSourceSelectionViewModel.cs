﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using Landdb.Client.Account;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Landdb.Web.ServiceContracts;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure.Messages;
using Landdb.Client.Services;
using System.Windows.Threading;
using Landdb.Infrastructure;
using NLog;
using Landdb.Client.Infrastructure;
using Landdb.ViewModel.Overlays;
using System.Diagnostics;
using Landdb.Web.ServiceContracts.Data;
using System.Threading.Tasks;
using Landdb.Resources;

namespace Landdb.ViewModel.DataSources {
    public class DataSourceSelectionViewModel : ViewModelBase {
        private readonly IAccountStatus accountStatus;
        private readonly ISynchronizationManager synchronizationManager;
        private readonly ICloudClientFactory cloudClientFactory;
        private readonly Dispatcher dispatcher;
        private readonly Logger log = LogManager.GetCurrentClassLogger();
        private readonly IClientEndpoint clientEndpoint;

        IDataSource selectedDataSource;

        bool isDataSourceListVisible = false;
        string title = Strings.Application_Name;

        public DataSourceSelectionViewModel(IAccountStatus accountStatus, ISynchronizationManager synchronizationManager, IClientEndpoint clientEndpoint, ICloudClientFactory cloudClientFactory, Dispatcher dispatcher) {
            this.accountStatus = accountStatus;
            this.synchronizationManager = synchronizationManager;
            this.clientEndpoint = clientEndpoint;
            this.cloudClientFactory = cloudClientFactory;
            this.dispatcher = dispatcher;

            CropYears = new ObservableCollection<DataSourceYearViewModel>();
            OtherDataSources = new ObservableCollection<DataSourceOtherViewModel>();

            this.accountStatus.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(accountStatus_PropertyChanged);
            RebuildViewModel();
        }

        public IDataSource SelectedDataSource {
            get { return selectedDataSource; }
            set {
                if (ValidateMasterlistCulture(value)) {
                    selectedDataSource = value;
                    RaisePropertyChanged("SelectedDataSource");
                } else {
                    var vm = new DatasourceRegionChangeConfirmationViewModel(clientEndpoint, value);
                    ScreenDescriptor sd = new ScreenDescriptor("Landdb.Views.Overlays.DatasourceRegionChangeConfirmationView", "regionChangeConfirm", vm);
                    Messenger.Default.Send<ShowOverlayMessage>(new ShowOverlayMessage() { ScreenDescriptor = sd });
                }
            }
        }

        public ObservableCollection<DataSourceYearViewModel> CropYears { get; private set; }
        public ObservableCollection<DataSourceOtherViewModel> OtherDataSources { get; private set; }

        public bool IsDataSourceListVisible {
            get { return isDataSourceListVisible; }
            set {
                isDataSourceListVisible = value;
                RaisePropertyChanged("IsDataSourceListVisible");
            }
        }

        public bool HasOtherDataSources {
            get { return OtherDataSources.Any(); }
        }

        public string Title {
            get { return title; }
            private set {
                title = value;
                RaisePropertyChanged("Title");
            }
        }

        void accountStatus_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e) {
            if (e.PropertyName == "AvailableDataSources") {
                RebuildViewModel();
            }
        }

        bool ValidateMasterlistCulture(IDataSource value) {
            if (value.MasterlistGroup == null && value.DatasourceCulture == null) { return true; } // This should be temporary, until the server code gets propogated. Take this out eventually.
            return Landdb.Client.Properties.Settings.Default.MasterlistGroup == value.MasterlistGroup && Landdb.Client.Properties.Settings.Default.DatasourceCulture == value.DatasourceCulture;
        }

        void RebuildViewModel() {
            if(accountStatus.AvailableDataSources.Any()) {
                IDataSource previousDataSource = null;

                var settings = clientEndpoint.GetUserSettings();
                if (settings != null && settings.LastDataSourceId != Guid.Empty) {
                    var q = from ds in accountStatus.AvailableDataSources
                                         where ds.DataSourceId == settings.LastDataSourceId
                                         select ds;
                    previousDataSource = q.FirstOrDefault();
                }

                if (ApplicationEnvironment.FakeLocalDataSource != null) {
                    previousDataSource = ApplicationEnvironment.FakeLocalDataSource;
                }

                SelectedDataSource = previousDataSource ?? accountStatus.AvailableDataSources.First();
                RebuildOtherDataSources();
                RebuildCropYears();

                if (ApplicationEnvironment.FakeLocalDataSource != null) {
                    SelectCropYear(CropYears.LastOrDefault());
                } else if (settings != null) {
                    var q2 = from c in CropYears
                             where c.CropYear == settings.LastCropYear
                             select c;
                    var lastUsedCropYear = q2.FirstOrDefault();
                    if (lastUsedCropYear != null) {
                        SelectCropYear(lastUsedCropYear);
                    }
                }
            }
        }

        private void RebuildOtherDataSources() {
            OtherDataSources.Clear();
            var otherList = from ds in accountStatus.AvailableDataSources
                            where ds != SelectedDataSource
                            orderby ds.Name
                            select ds;
            foreach (var ds in otherList) {
                OtherDataSources.Add(new DataSourceOtherViewModel(this, ds));
            }
            RaisePropertyChanged("HasOtherDataSources");
        }

        void RebuildCropYears() {
            CropYears.Clear();
            var orderedYears = SelectedDataSource.AvailableYears.OrderByDescending(x => x);
            foreach (var year in orderedYears) {
                CropYears.Add(new DataSourceYearViewModel(this, SelectedDataSource, year));
            }
        }

        internal async Task SelectCropYear(DataSourceYearViewModel dataSourceYearViewModel) {
            if (dataSourceYearViewModel == null || dataSourceYearViewModel.DataSource == null) { return; }

            var dataSourceId = dataSourceYearViewModel.DataSource.DataSourceId;
            var cropYear = dataSourceYearViewModel.CropYear;

            if (SelectedDataSource != ApplicationEnvironment.FakeLocalDataSource) {
                UpdateUserSettings(dataSourceId, cropYear);
            }

            var dsId = new DataSourceId(dataSourceId);

            if (synchronizationManager.CurrentDataSourceId == dsId) { // Don't need to recheck device sub if we're just swapping years.
                OnCropYearSelected(dsId, cropYear, SelectedDataSource);
            } else {
                if (NetworkStatus.IsInternetAvailableFast() && !accountStatus.IsWorkingOffline) {
                    ProgressOverlayViewModel vm = new ProgressOverlayViewModel() { StatusMessage = Strings.CheckingRegistration_Text };
                    ScreenDescriptor progressDescriptor = new ScreenDescriptor("Landdb.Views.Overlays.FullSynchronizationProgressOverlayView", "fullsyncprogress", vm);
                    Messenger.Default.Send<ShowOverlayMessage>(new ShowOverlayMessage() { ScreenDescriptor = progressDescriptor });

                    var subscriptionResults = await cloudClientFactory.ResolveRegistrationClient().ConfirmDeviceSubscription(dataSourceId, cloudClientFactory.DeviceId);

                    if (!subscriptionResults.HasError && subscriptionResults.Details != null) {
                        await synchronizationManager.BeginSynchronizationTasks(dataSourceId, accountStatus, vm,
                            () => {
                                Messenger.Default.Send<HideOverlayMessage>(new HideOverlayMessage());
                                OnCropYearSelected(dsId, cropYear, SelectedDataSource);
                            });
                        clientEndpoint.GetConnectEngine().StartSynchronization(dsId.Id);
                    } else {
                        // TODO: Show problem to user.
                        log.ErrorException("An error was encountered while confirming device subscription.", subscriptionResults.Exception);
                    }

                } else {
                    OnCropYearSelected(dsId, cropYear, SelectedDataSource);
                }
            }

            var t = dataSourceYearViewModel.DataSource.Name;
            ApplicationEnvironment.CurrentDataSourceName = t;
            if (string.IsNullOrWhiteSpace(t)) { Title = Strings.Application_Name; }

            Title = string.Format("{0} - {1}", cropYear, t);

            IsDataSourceListVisible = false;
        }

        void OnCropYearSelected(DataSourceId dsId, int cropYear, IDataSource dataSource) {
            ApplicationEnvironment.CurrentDataSourceId = dsId.Id;
            ApplicationEnvironment.CurrentCropYear = cropYear;
            ApplicationEnvironment.CurrentDataSource = dataSource;
            ApplicationEnvironment.ShowPremiumMap = true;
            //commandline feature activation --ShowPlanetImagery
            //ApplicationEnvironment.ShowPlanetLabsImagery = false;
            bool planetpermission = CheckForPlanetPermissions();
            //if (!ApplicationEnvironment.ShowPlanetLabsImagery && planetpermission && ApplicationEnvironment.PlanetPageLoaded)
            //{
            //    var vm = new Landdb.ViewModel.Overlays.DataSourcePlanetPermissionChangeConfirmationViewModel(clientEndpoint);
            //    ScreenDescriptor sd = new ScreenDescriptor("Landdb.Views.Overlays.DataSourcePlanetPermissionChangeConfirmationView", "planetpermissionsChangeConfirm", vm);
            //    Messenger.Default.Send<ShowOverlayMessage>(new ShowOverlayMessage() { ScreenDescriptor = sd });
            //    return;
            //}
            ApplicationEnvironment.ShowPlanetLabsImagery = planetpermission;
            ApplicationEnvironment.PlanetPageLoaded = false;
            //if (!ApplicationEnvironment.ShowPlanetLabsImagery)
            //{
            //    ApplicationEnvironment.ShowPlanetLabsImagery = false;
            //}
            ApplicationEnvironment.ShowElluminateTab = false;

            if (dataSource != null) {
                log.Info("Datasource selected: {0} ({1}) - {2}", dataSource.Name, dsId.Id, cropYear);
            } else {
                log.Warn("A null datasource was selected.");
            }

            ShowDataSourceMessageIfAvailable();

            Messenger.Default.Send<DataSourceChangedMessage>(new DataSourceChangedMessage() { DataSourceId = dsId.Id, CropYear = cropYear });
        }

        private void UpdateUserSettings(Guid dataSourceId, int cropYear) {
            var settings = clientEndpoint.GetUserSettings();

            if (settings.LastDataSourceId != dataSourceId || settings.LastCropYear != cropYear) { // don't save unnecessarily
                settings.LastDataSourceId = dataSourceId;
                settings.LastCropYear = cropYear;
                clientEndpoint.SaveUserSettings();
            }
        }

        internal void SelectDataSource(DataSourceOtherViewModel dataSourceOtherViewModel) {
            if (dataSourceOtherViewModel == null || dataSourceOtherViewModel.DataSource == null) { return; }

            SelectedDataSource = dataSourceOtherViewModel.DataSource;

            RebuildOtherDataSources();
            RebuildCropYears();
        }

        internal void ShowDataSourceMessageIfAvailable() {
            if (SelectedDataSource != null && SelectedDataSource.DataSourceMessage != null) {
                DataSourceMessageOverlayViewModel vm = new DataSourceMessageOverlayViewModel();

                vm.Message = SelectedDataSource.DataSourceMessage.Message;
                vm.Url = SelectedDataSource.DataSourceMessage.Url;

                ScreenDescriptor progressDescriptor = new ScreenDescriptor("Landdb.Views.Overlays.DataSourceMessageOverlayView", "datasourcemessage", vm);
                Messenger.Default.Send<ShowOverlayMessage>(new ShowOverlayMessage() { ScreenDescriptor = progressDescriptor });
            }
        }

        internal bool CheckForPlanetPermissions()
        {
            bool canuseplanetlabs = false;
            try
            {
                var planetLabBaseUri = string.Format("{0}/api/permission/getbydatasource?dataSourceId=", RemoteConfigurationSettings.GetBaseUri());
                var planetLabUri = string.Format("{0}{1}", planetLabBaseUri, ApplicationEnvironment.CurrentDataSourceId);
                var cloudClientFactory = new Landdb.Infrastructure.CloudClientFactory();
                var token = cloudClientFactory.ResolveStorageCredentials();
                var client1 = new System.Net.Http.HttpClient
                {
                    BaseAddress = new Uri(planetLabUri)
                };
                client1.DefaultRequestHeaders.Add("Accept", "application/json");
                token.SetBearerTokenAsync(client1);
                var response = client1.GetStringAsync(planetLabUri);
                response.Wait();
                string resultzones = response.Result;
                if (response.Result.Length > 10)
                {
                    log.Info("LOG PLANET PERMISSIONS:  " + response.Result);
                    if (response.Result.Contains("CanUsePlanetLabs"))
                    {
                        canuseplanetlabs = true;
                        log.Info("..........This datasource can use planet imagery.........");
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Info("Problem downloading images.");
                return false;
            }
            return false;
        }
    }
}
