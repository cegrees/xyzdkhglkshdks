﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using Landdb.Client.Account;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System.Threading.Tasks;

namespace Landdb.ViewModel.DataSources {
    public class DataSourceYearViewModel : ViewModelBase {
        readonly IDataSource dataSource;
        readonly int cropYear;
        readonly DataSourceSelectionViewModel parentVm;

        public DataSourceYearViewModel(DataSourceSelectionViewModel parentVm, IDataSource dataSource, int cropYear) {
            this.parentVm = parentVm;
            this.dataSource = dataSource;
            this.cropYear = cropYear;

            SelectCropYearCommand = new AwaitableRelayCommand(SelectCropYear);
        }

        public ICommand SelectCropYearCommand { get; private set; }

        public IDataSource DataSource { get { return dataSource; } }
        public int CropYear { get { return cropYear; } }

        async Task SelectCropYear() {
            await parentVm.SelectCropYear(this);
        }
    }
}
