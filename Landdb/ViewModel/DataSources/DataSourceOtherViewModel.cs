﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GalaSoft.MvvmLight;
using Landdb.Client.Account;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;

namespace Landdb.ViewModel.DataSources {
    public class DataSourceOtherViewModel : ViewModelBase {
        readonly IDataSource dataSource;
        readonly DataSourceSelectionViewModel parentVm;

        public DataSourceOtherViewModel(DataSourceSelectionViewModel parentVm, IDataSource dataSource) {
            this.parentVm = parentVm;
            this.dataSource = dataSource;

            SelectDataSourceCommand = new RelayCommand(SelectDataSource);
        }

        void SelectDataSource() {
            parentVm.SelectDataSource(this);
        }

        public RelayCommand SelectDataSourceCommand { get; private set; }

        public string Name { get { return dataSource.Name; } }
        public IDataSource DataSource { get { return dataSource; } }
    }
}
