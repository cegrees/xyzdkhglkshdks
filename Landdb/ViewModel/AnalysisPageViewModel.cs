﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GalaSoft.MvvmLight;
using System.Collections.ObjectModel;
using System.Globalization;
using Landdb.Infrastructure;
using System.Reactive.Linq;
using WPFLocalizeExtension.Extensions;

namespace Landdb.ViewModel {
    public class AnalysisPageViewModel : ViewModelBase {
        ObservableCollection<ScreenDescriptor> screens = new ObservableCollection<ScreenDescriptor>();

        public AnalysisPageViewModel()
        {
            //ScreenDescriptor ftmPage = new ScreenDescriptor("Landdb.Views.BlankPage", LocExtension.GetLocalizedValue<string>("Landdb:Strings:PageHeader_Analysis_Fieldprint").ToUpper());
            //if (ApplicationEnvironment.ShowSustainability) {
            //    ftmPage = new ScreenDescriptor("Landdb.Views.Analysis.FieldToMarket.FtmPageView", LocExtension.GetLocalizedValue<string>("Landdb:Strings:PageHeader_Analysis_Fieldprint").ToUpper());
            //}

            //AddPageTabs(false);
        }

        private void AddPageTabs(bool AddSubscribedPages)
        {
            ScreenDescriptor ftmPage = new ScreenDescriptor("Landdb.Views.Analysis.FieldToMarket.FtmPageView", LocExtension.GetLocalizedValue<string>("Landdb:Strings:PageHeader_Analysis_Fieldprint").ToUpper(), new Analysis.FieldToMarket.FtmPageViewModel(ServiceLocator.Get<Client.Infrastructure.IClientEndpoint>(), App.Current.Dispatcher));
            ScreenDescriptor mapPage = new ScreenDescriptor("Landdb.Views.Analysis.Planet.PlanetMapPageView", LocExtension.GetLocalizedValue<string>("Landdb:Strings:PageHeader_Analysis_PlanetMap").ToUpper(), new Analysis.Planet.PlanetMapPageViewModel(ServiceLocator.Get<Client.Infrastructure.IClientEndpoint>()));
            ScreenDescriptor budPage = new ScreenDescriptor("Landdb.Views.Analysis.BudgetVarianceView", LocExtension.GetLocalizedValue<string>("Landdb:Strings:PageHeader_Analysis_BudgetVariance").ToUpper(), new Analysis.BudgetVarianceViewModel(ServiceLocator.Get<Client.Infrastructure.IClientEndpoint>(), App.Current.Dispatcher));
            //ScreenDescriptor ellumPage = new ScreenDescriptor(typeof(Views.ElluminatePageView), "e-lluminate".ToUpper(), new ElluminatePageViewModel());
            CultureInfo thisCulture = Landdb.Resources.Strings.Culture == null ? CultureInfo.CurrentCulture : Landdb.Resources.Strings.Culture;
            bool cultureIsUS = thisCulture.Name == "en-US";

            if (AddSubscribedPages)
            {
                if (ApplicationEnvironment.ShowPlanetLabsImagery)
                {
                    screens.Clear();
                    ScreenDescriptor[] screenDescriptors = cultureIsUS ? new[] { budPage, ftmPage, mapPage } : new[] { budPage };

                    screenDescriptors.ToObservable()
                        .OnTimeline(TimeSpan.FromSeconds(0.2))
                        .ObserveOn(System.Threading.SynchronizationContext.Current)
                        .Subscribe(AddScreen);
                }
                //if (ApplicationEnvironment.ShowElluminateTab)
                //{
                //    ScreenDescriptor[] screenDescriptors = new ScreenDescriptor[] {
                //        ellumPage
                //    };

                //    screenDescriptors.ToObservable()
                //        .OnTimeline(TimeSpan.FromSeconds(0.2))
                //        .ObserveOn(System.Threading.SynchronizationContext.Current)
                //        .Subscribe(AddScreen);
                //}
            }
            else
            {
                screens.Clear();
                ScreenDescriptor[] screenDescriptors = cultureIsUS ? new[] { budPage, ftmPage } : new[] { budPage };

                screenDescriptors.ToObservable()
                    .OnTimeline(TimeSpan.FromSeconds(0.2))
                    .ObserveOn(System.Threading.SynchronizationContext.Current)
                    .Subscribe(AddScreen);

            }
            ApplicationEnvironment.PlanetPageLoaded = true;
        }

        public ObservableCollection<ScreenDescriptor> Screens {
            get { return screens; }
        }

        void AddScreen(ScreenDescriptor screen) {
            Screens.Add(screen);
        }

        public void AddTheImageryPage(bool AddSubscribedPages)
        {
            //if (AddSubscribedPages)
            //{
                AddPageTabs(AddSubscribedPages);
            //}
        }
    }
}