﻿using GalaSoft.MvvmLight;
using Landdb.Infrastructure;
using Landdb.Views.Production;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive.Linq;
using WPFLocalizeExtension.Extensions;

namespace Landdb.ViewModel {
	public class ProductionPageViewModel : ViewModelBase, IHandleViewRouting {

		static int selectedTabIndex;
		int savedTabIndex;
        readonly ViewModelLocator locator;

        public ProductionPageViewModel() {
			Screens = new ObservableCollection<ScreenDescriptor>();
            var locatorMaybe = System.Windows.Application.Current.TryFindResource("Locator");
            locator = locatorMaybe is ViewModelLocator ? (ViewModelLocator)locatorMaybe : null;

            ScreenDescriptor[] screenDescriptors = new ScreenDescriptor[] {
				new ScreenDescriptor(typeof(PlansPageView), LocExtension.GetLocalizedValue<string>("Landdb:Strings:PageHeader_Production_Plans").ToUpper()),
				new ScreenDescriptor(typeof(SiteDataPageView), LocExtension.GetLocalizedValue<string>("Landdb:Strings:PageHeader_Production_SiteData").ToUpper()),
				new ScreenDescriptor(typeof(RecommendationsPageView), LocExtension.GetLocalizedValue<string>("Landdb:Strings:PageHeader_Production_Recommendations").ToUpper()),
				new ScreenDescriptor(typeof(WorkOrdersPageView), LocExtension.GetLocalizedValue<string>("Landdb:Strings:PageHeader_Production_WorkOrders").ToUpper()),
				new ScreenDescriptor(typeof(ApplicationsPageView), LocExtension.GetLocalizedValue<string>("Landdb:Strings:PageHeader_Production_Applications").ToUpper()),
				new ScreenDescriptor(typeof(InvoicesPageView), LocExtension.GetLocalizedValue<string>("Landdb:Strings:PageHeader_Production_Invoices").ToUpper(), locator?.InvoicesPage),
			};

			savedTabIndex = selectedTabIndex;

			screenDescriptors.ToObservable()
				.OnTimeline(TimeSpan.FromSeconds(0.2))
				.ObserveOn(System.Threading.SynchronizationContext.Current)
				.Subscribe(AddScreen);
		}

		public ObservableCollection<ScreenDescriptor> Screens { get; }

		void AddScreen(ScreenDescriptor screen) {
			Screens.Add(screen);
			if (Screens.Count == savedTabIndex + 1) {
				SelectedTabIndex = savedTabIndex;
			}
		}

		public int SelectedTabIndex {
			get { return selectedTabIndex; }
			set {
				selectedTabIndex = value;
				RaisePropertyChanged(() => SelectedTabIndex);
			}
		}

		public void HandleRouteRequest(Infrastructure.Messages.RouteRequestMessage message) {
			if (message.Route is string) {
				var route = (string)message.Route;

				var q = from s in Screens
						where s.Key == route
						select Screens.IndexOf(s);

				if (q.Any()) {
					SelectedTabIndex = q.FirstOrDefault();
				}

				if (locator == null) { return; }

				if (route == typeof(ApplicationsPageView).FullName) {
					locator.ApplicationsPage.HandleRouteRequest(message.Next);
				}
				if (route == typeof(InvoicesPageView).FullName) {
					locator.InvoicesPage.HandleRouteRequest(message.Next);
				}
			}
		}

		public object SelectedViewModel { get; set; }
	}
}