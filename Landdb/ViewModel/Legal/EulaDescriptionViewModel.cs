﻿using System;
using System.Globalization;
using System.IO;
using System.Windows.Documents;
using System.Windows.Forms;
using Landdb.Resources;

namespace Landdb.ViewModel.Legal {
    public class EulaDescriptionViewModel {
        public static FlowDocument Eula => GetEula();

        private static FlowDocument GetEula() {
            var theEulaForThisCulture = $"{Strings.NoLegalEulaFound_Text} :( ";
            var markdownTransformer = new Markdown.Xaml.Markdown();
            var exceptionMessage = "";
            try {
                var thisCulture = Strings.Culture == null ? CultureInfo.CurrentCulture.Name : Strings.Culture.Name;
                var folderPath = Path.GetFullPath(@"Resources");
                var path = $"{folderPath}\\EULA\\{thisCulture}.EULA.txt";
                using (var reader = new StreamReader(path)) {
                    theEulaForThisCulture = reader.ReadToEnd();
                }
            } catch (Exception ex) {
                exceptionMessage = ex.Message;
            }

            return markdownTransformer.Transform($"{theEulaForThisCulture} {exceptionMessage}");
        }
    }
}