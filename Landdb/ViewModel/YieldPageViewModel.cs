﻿using GalaSoft.MvvmLight;
using Landdb.Infrastructure;
using Landdb.Views.Yield;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive.Linq;
using Landdb.Resources;

namespace Landdb.ViewModel {
	public class YieldPageViewModel : ViewModelBase, IHandleViewRouting {

		private int savedTabIndex;

		public YieldPageViewModel() {
			Screens = new ObservableCollection<ScreenDescriptor>();

			ScreenDescriptor[] screenDescriptors = new ScreenDescriptor[] {
				new ScreenDescriptor(typeof(HarvestLoadsPageView), Strings.ScreenDescriptor_FromFields_Text.ToUpper()),
                new ScreenDescriptor(typeof(StorageLoadsPageView), Strings.ScreenDescriptor_FromStorage_Text.ToUpper()),
				new ScreenDescriptor(typeof(YieldLocationsPageView), Strings.ScreenDescriptor_Locations_Text.ToUpper()),
				new ScreenDescriptor(typeof(CommoditiesPageView), Strings.ScreenDescriptor_CommoditySummary_Text.ToUpper()),
				new ScreenDescriptor(typeof(CropSettingsPageView), Strings.ScreenDescriptor_CropSettings_Text.ToUpper()),
            };

			savedTabIndex = selectedTabIndex;

			screenDescriptors.ToObservable()
				.OnTimeline(TimeSpan.FromSeconds(0.2))
				.ObserveOn(System.Threading.SynchronizationContext.Current)
				.Subscribe(addScreen);
		}

		public ObservableCollection<ScreenDescriptor> Screens { get; }

		private int selectedTabIndex;
		public int SelectedTabIndex {
			get { return selectedTabIndex; }
			set {
				selectedTabIndex = value;
				RaisePropertyChanged(() => SelectedTabIndex);
			}
		}

		public void HandleRouteRequest(Infrastructure.Messages.RouteRequestMessage message) {
			if (message.Route is string) {
				var route = (string)message.Route;

				var q = from s in Screens
						where s.Key == route
						select Screens.IndexOf(s);

				if (q.Any()) {
					SelectedTabIndex = q.FirstOrDefault();
				}

				var locatorMaybe = System.Windows.Application.Current.TryFindResource("Locator");
				var locator = locatorMaybe is ViewModelLocator ? (ViewModelLocator)locatorMaybe : null;

				if (locator == null) { return; }

				if (route == typeof(HarvestLoadsPageView).FullName) {
					locator.HarvestLoadsPage.HandleRouteRequest(message.Next);
				} else if (route == typeof(StorageLoadsPageView).FullName) {
					locator.StorageLoadsPage.HandleRouteRequest(message.Next);
				}
			}
		}

		private void addScreen(ScreenDescriptor screen) {
			Screens.Add(screen);

			if (Screens.Count == savedTabIndex + 1) {
				SelectedTabIndex = savedTabIndex;
			}
		}
	}
}