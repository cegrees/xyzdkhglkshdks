﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel {
    public interface IPageFilterViewModel : ICleanup {
        void ClearFilter();
        void BeforeFilter();
		void RefreshLists();
        bool FilterItem(object item);
    }
}
