﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using System.Windows.Data;
using Landdb.Resources;

namespace Landdb.ViewModel {
    public abstract class AbstractListPage<TListItemModel, TDetailsModel> : ViewModelBase {

        protected readonly IClientEndpoint clientEndpoint;
        protected readonly Dispatcher dispatcher;

        protected DataSourceId currentDataSourceId;
        protected int currentCropYear;

        TListItemModel selectedListItem;
        TDetailsModel detailsViewModel;

        bool isCharmPopupOpen = false;
        bool isLoadingItems;
        bool isLoadingDetails;
        bool isFilterVisible = false;
        string filterHeader = string.Empty;

        protected AbstractListPage(IClientEndpoint clientEndpoint, Dispatcher dispatcher) {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;

            currentDataSourceId = new DataSourceId(ApplicationEnvironment.CurrentDataSourceId);
            currentCropYear = ApplicationEnvironment.CurrentCropYear;

            CreateNewItemCommand = new RelayCommand(async () => await CreateNewItem());
            CreateNewPopupItemCommand = new RelayCommand(async () => await CreateNewPopupItem());
            CreateNewOverlayItemCommand = new RelayCommand(async () => await CreateNewOverlayItem());
            RemoveSelectedItemCommand = new RelayCommand(RemoveSelectedItem);
            SetCharmCommand = new RelayCommand<string>(SetCharmOnSelectedItem);
            ApplyFilterCommand = new RelayCommand(ApplyFilter);
            ClearFilterCommand = new RelayCommand(ClearFilter);
            ShowRestorableItemsCommand = new RelayCommand(onShowRestorableItemList);
            HideRemovalPopupCommand = new RelayCommand(HideRemovalPopup);

            Messenger.Default.Register<DataSourceChangedMessage>(this, OnDataSourceChanged);
            RefreshItemsList();
        }

        public ICommand CreateNewItemCommand { get; }
        public ICommand CreateNewPopupItemCommand { get; }
        public ICommand CreateNewOverlayItemCommand { get; }
        public ICommand RemoveSelectedItemCommand { get; }
        public ICommand SetCharmCommand { get; }
        public ICommand ApplyFilterCommand { get; }
        public ICommand ClearFilterCommand { get; }
        public ICommand ShowRestorableItemsCommand { get; }
        public ICommand HideRemovalPopupCommand { get; }

        public ObservableCollection<TListItemModel> ListItems { get; protected set; }
        public CollectionView ListItemsView { get; private set; }
        public IPageFilterViewModel FilterModel { get; protected set; }

        protected bool EnableAutoRefresh { get; set; }
        protected Action DefaultPostRefreshAction { get; set; }

        protected abstract IEnumerable<TListItemModel> GetListItemModels();
        protected abstract Task<ScreenDescriptor> CreateScreenDescriptorForNewItemCreation();
        protected abstract bool PerformItemRemoval(TListItemModel selectedItem);
        protected abstract IDomainCommand CreateSetCharmCommand(TListItemModel selectedItem, string charmName);
        protected abstract TDetailsModel CreateDetailsModel(TListItemModel selectedItem);

        protected virtual IEnumerable<RestorableItem> GetRestorableItems() => new List<RestorableItem>();
        protected virtual void RestoreDeletedItem(Guid id) { }

        internal Task LoadingTask { get; private set; }

        public TListItemModel SelectedListItem {
            get { return selectedListItem; }
            set {
                selectedListItem = value;
                RaisePropertyChanged(() => SelectedListItem);

                if (selectedListItem != null) {
                    CreateDetails_Internal(selectedListItem);
                } else {
                    DetailsModel = default(TDetailsModel);
                }
            }
        }

        async Task CreateDetails_Internal(TListItemModel selectedItem) {
            IsLoadingDetails = true;

            await Task.Run(new Action(() => {
                var m = CreateDetailsModel(selectedListItem);
                if (m != null) {
                    DetailsModel = m;
                }
            }));

            IsLoadingDetails = false;
        }

        public TDetailsModel DetailsModel {
            get { return detailsViewModel; }
            set {
                detailsViewModel = value;
                RaisePropertyChanged(() => DetailsModel);
            }
        }

        public bool IsCharmPopupOpen {
            get { return isCharmPopupOpen; }
            set {
                isCharmPopupOpen = value;
                RaisePropertyChanged(() => IsCharmPopupOpen);
            }
        }

        public bool IsLoadingItems {
            get { return isLoadingItems; }
            set {
                isLoadingItems = value;
                RaisePropertyChanged(() => IsLoadingItems);
            }
        }

        public bool IsLoadingDetails {
            get { return isLoadingDetails; }
            set {
                isLoadingDetails = value;
                RaisePropertyChanged(() => IsLoadingDetails);
            }
        }

        public bool IsFilterVisible {
            get { return isFilterVisible; }
            set {
                isFilterVisible = value;
                RaisePropertyChanged(() => IsFilterVisible);
            }
        }

        public string FilterHeader {
            get { return filterHeader; }
            set {
                filterHeader = value;
                RaisePropertyChanged(() => FilterHeader);
            }
        }

        private bool isRemovalPopupOpen;
        public bool IsRemovalPopupOpen {
            get { return isRemovalPopupOpen; }
            set {
                isRemovalPopupOpen = value;
                RaisePropertyChanged(() => IsRemovalPopupOpen);
            }
        }

        private string removalPopupText;
        public string RemovalPopupText {
            get { return removalPopupText; }
            set {
                removalPopupText = value;
                RaisePropertyChanged(() => RemovalPopupText);
            }
        }

        public virtual string GetEntityName() => Strings.Item_Text.ToLower();
        public virtual string GetPluralEntityName() => Strings.Items_Text.ToLower();

        public virtual bool CanDelete(TListItemModel toDelete) => true;

        protected virtual async Task CreateNewItem() {
            var descriptor = await CreateScreenDescriptorForNewItemCreation();
            Messenger.Default.Send(new ChangeScreenDescriptorMessage() { ScreenDescriptor = descriptor });

            if (EnableAutoRefresh) {
                Messenger.Default.Register<ShowMainViewMessage>(this, OnReturnFromCreation);
            }
        }

        protected virtual async void OnReturnFromCreation(ShowMainViewMessage msg) {
            RefreshItemsList(DefaultPostRefreshAction);
            Messenger.Default.Unregister<ShowMainViewMessage>(this, OnReturnFromCreation);
        }

        protected virtual async Task CreateNewPopupItem() {
            Messenger.Default.Send(new ShowPopupMessage() {
                ScreenDescriptor = await CreateScreenDescriptorForNewItemCreation()
            });
        }

        protected virtual async Task CreateNewOverlayItem() {
            Messenger.Default.Send(new ShowOverlayMessage() {
                ScreenDescriptor = await CreateScreenDescriptorForNewItemCreation()
            });
        }

        private void HideRemovalPopup() {
            IsRemovalPopupOpen = false;
        }

        protected virtual void RemoveSelectedItem() {
            RemoveSelectedItem(String.Format(Strings.AreYouSureYouWantToRemoveThis_Text, GetEntityName()));
        }

        protected virtual void RemoveSelectedItem(string YesNoDialogMessage) {
            if (SelectedListItem == null) { return; }

            if (!CanDelete(selectedListItem)) {
                if (string.IsNullOrWhiteSpace(RemovalPopupText)) {
                    RemovalPopupText = String.Format(Strings.SelectedCannotBeRemoved_Format_Text, GetEntityName());
                }

                IsRemovalPopupOpen = true;

                return;
            }

            DialogFactory.ShowYesNoDialog(
                String.Format(Strings.Remove_Format_Text, GetEntityName()),
                String.Format(YesNoDialogMessage),
                () => {
                    var removed = PerformItemRemoval(selectedListItem);

                    if (removed) {
                        int index = ListItems.IndexOf(SelectedListItem) - 1;
                        if (index < 0) { index = 0; }

                        ListItems.Remove(SelectedListItem);
                        SelectedListItem = ListItems.Any() ? ListItems[index] : default(TListItemModel);

                        ApplyFilter();
                    }
                },
                () => { }
            );
        }

        protected virtual void SetCharmOnSelectedItem(string charmName) {
            if (SelectedListItem == null) { return; }

            var flagCommand = CreateSetCharmCommand(selectedListItem, charmName);

            if (flagCommand == null) { return; }
            clientEndpoint.SendOne(flagCommand);

            IsCharmPopupOpen = false;
            RaisePropertyChanged(() => ListItems);
        }

        void OnDataSourceChanged(DataSourceChangedMessage message) {
            currentDataSourceId = new DataSourceId(message.DataSourceId);
            currentCropYear = message.CropYear;

            RefreshItemsList();
        }

        protected virtual void ApplyFilter() {
            if (FilterModel != null && ListItemsView != null) {
                FilterModel.BeforeFilter();
                ListItemsView.Refresh();
                generateFilterHeader();
                IsFilterVisible = false;
                RaisePropertyChanged(() => FilterHeader);
            }
        }

        protected virtual void ClearFilter() {
            if (FilterModel != null && ListItemsView != null) {
                FilterModel.ClearFilter();
                FilterModel.BeforeFilter();
                ListItemsView.Refresh();
                generateFilterHeader();
                IsFilterVisible = false;
            }
        }

        internal async Task RefreshItemsList(Action afterLoading = null) {
            IsLoadingItems = true;

            var listItems = await Task.Run(() => GetListItemModels());

            if (listItems == null) { listItems = new List<TListItemModel>(); }

            if (LoadingTask != null) {
                await LoadingTask; // TODO: Ideally cancel this in the future.
            }

            LoadingTask = Task.Run(() => {
                ListItems = new ObservableCollection<TListItemModel>(listItems);
                if (FilterModel != null) {
                    dispatcher.BeginInvoke(new Action(() => { // make sure the collection view is created on the dispatcher thread
                        ListItemsView = (CollectionView)CollectionViewSource.GetDefaultView(ListItems);
                        ListItemsView.Filter = FilterModel.FilterItem;
                        generateFilterHeader();
                        FilterModel.RefreshLists();
                        RaisePropertyChanged(() => ListItemsView);
                    }));
                }

                RaisePropertyChanged(() => ListItems);
            });

            await LoadingTask;

            afterLoading?.Invoke();

            if (ListItems.Count > 0 && SelectedListItem == null) {
                SelectedListItem = ListItems[0];
            } else if (ListItems.Count == 0) {
                SelectedListItem = default(TListItemModel);
            }

            IsLoadingItems = false;
        }

        protected virtual void generateFilterHeader() {
            int originalCount = ListItems.Count;
            int filteredCount = ListItemsView.Count;
            string entityPluralityAwareName = originalCount == 1 ? GetEntityName() : GetPluralEntityName();

            if (originalCount != filteredCount) {
                FilterHeader = String.Format(Strings.CountOfCountName_Format_Text, filteredCount, originalCount, entityPluralityAwareName);
            } else {
                FilterHeader = $"{filteredCount} {entityPluralityAwareName}";
            }
        }

        private void onShowRestorableItemList() {
            var restorableItems = GetRestorableItems();

            if (restorableItems.Any()) {
                var vm = new Shared.Popups.RestoreDeletedItemsViewModel(clientEndpoint, dispatcher, GetEntityName(), restorableItems, restoreVm => {
                    if (restoreVm != null && restoreVm.SelectedItem != null) {
                        RestoreDeletedItem(restoreVm.SelectedItem.Id);
                    }

                    Messenger.Default.Send(new HideOverlayMessage());
                });

                Messenger.Default.Send(new ShowOverlayMessage() {
                    ScreenDescriptor = new ScreenDescriptor(typeof(Views.Shared.Popups.RestoreDeletedItemsView), Strings.RestoreDeletedItems_Text.ToLower(), vm)
                });
            }
        }

        public override void Cleanup() {
            base.Cleanup();
            Messenger.Default.Unregister<DataSourceChangedMessage>(this, OnDataSourceChanged);
        }
    }

    public class RestorableItem {
        public RestorableItem(Guid id, string name, string itemDescription, int cropYearCreated, int cropYearRemoved) {
            Id = id;
            Name = name;
            ItemDescription = itemDescription;
            CropYearCreated = cropYearCreated;
            CropYearRemoved = cropYearRemoved;
        }

        public Guid Id { get; }
        public string Name { get; }
        public string ItemDescription { get; }
        public int CropYearCreated { get; }
        public int CropYearRemoved { get; }

        public string CropYearDisplay => CropYearCreated != CropYearRemoved ? $"{CropYearCreated} - {CropYearRemoved}" : $"{CropYearCreated}";

        public override string ToString() => $"{Name} [{CropYearDisplay}]";
    }
}