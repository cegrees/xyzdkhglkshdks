﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Landdb.ViewModel.Overlays {
    public class RemoteErrorsListViewModel : ViewModelBase {
        public RemoteErrorsListViewModel(List<ErrorDisplayModel> errorDisplayList) {
            this.ErrorDisplayList = errorDisplayList;
            ConfirmCommand = new RelayCommand(CloseOverlay);
        }

        public ICommand ConfirmCommand { get; private set; }

        public List<ErrorDisplayModel> ErrorDisplayList { get; private set; }

        void CloseOverlay() {
            Messenger.Default.Send<HideOverlayMessage>(new HideOverlayMessage());
        }
    }
}
