﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Account;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Landdb.ViewModel.Overlays {
    public class DataSourceMapZoomLevelChangeConfirmationViewModel : ViewModelBase {
        IClientEndpoint clientEndpoint;

        public DataSourceMapZoomLevelChangeConfirmationViewModel(IClientEndpoint clientEndpoint) {
            ConfirmCloseOperation = new RelayCommand(ConfirmClose);
            CancelCloseOperation = new RelayCommand(CancelClose);

            this.clientEndpoint = clientEndpoint;
        }

        public ICommand ConfirmCloseOperation { get; private set; }
        public ICommand CancelCloseOperation { get; private set; }

        void ConfirmClose() {
            if (ApplicationEnvironment.CurrentDataSource != null) {
                System.Windows.Forms.Application.Restart();
                System.Windows.Application.Current.Shutdown();
                //var mapsettings = clientEndpoint.GetMapSettings();
                //if (!string.IsNullOrWhiteSpace(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit) && mapsettings.MapAreaUnit != ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit) {
                //    mapsettings.MapAreaUnit = ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit;
                //    clientEndpoint.SaveMapSettings(mapsettings);
                //    System.Windows.Forms.Application.Restart();
                //    System.Windows.Application.Current.Shutdown();
                //}
            }
        }

        void CancelClose() {
            Messenger.Default.Send<HideOverlayMessage>(new HideOverlayMessage());
        }
    }
}
