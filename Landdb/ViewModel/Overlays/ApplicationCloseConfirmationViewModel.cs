﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using System;
using System.Windows.Input;

namespace Landdb.ViewModel.Overlays {
	public class ApplicationCloseConfirmationViewModel : ViewModelBase {
		MainWindow applicationWindow;

		public ApplicationCloseConfirmationViewModel(MainWindow applicationWindow) {
			this.applicationWindow = applicationWindow;

			ConfirmCloseOperation = new RelayCommand(applicationWindow.Close);
			CancelCloseOperation = new RelayCommand(CancelClose);

			// added this to handle the problem where the user has not logged in yet
			// and cannot click the confirm button because it is stuck behind the
			// login prompt (fb 20351). also has the side effect of not prompting
			// when the user has yet to select a datasource.

			// this will also allow the user to close the software during a long synchronization
			// without resorting to a force-close, as the values it checks are not set
			// until synchronization completes.
			SkipConfirmationIfNoDataSourceIsSelected();
		}

		public ICommand ConfirmCloseOperation { get; private set; }
		public ICommand CancelCloseOperation { get; private set; }

		void CancelClose() {
			Messenger.Default.Send<HideOverlayMessage>(new HideOverlayMessage());
		}

		private void SkipConfirmationIfNoDataSourceIsSelected() {
			var currentCropYear = ApplicationEnvironment.CurrentCropYear;
			var currentDataSourceId = ApplicationEnvironment.CurrentDataSourceId;

			if (currentCropYear == 0 && currentDataSourceId == Guid.Empty) {
				ConfirmCloseOperation.Execute(null);
			}
		}
	}
}