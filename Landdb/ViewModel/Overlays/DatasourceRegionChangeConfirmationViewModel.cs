﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Account;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Landdb.ViewModel.Overlays {
    public class DatasourceRegionChangeConfirmationViewModel : ViewModelBase {
        IClientEndpoint clientEndpoint;
        IDataSource desiredDatasource;

        public DatasourceRegionChangeConfirmationViewModel(IClientEndpoint clientEndpoint, IDataSource desiredDatasource) {
            ConfirmCloseOperation = new RelayCommand(ConfirmClose);
            CancelCloseOperation = new RelayCommand(CancelClose);

            this.clientEndpoint = clientEndpoint;
            this.desiredDatasource = desiredDatasource;
        }

        public ICommand ConfirmCloseOperation { get; private set; }
        public ICommand CancelCloseOperation { get; private set; }

        void ConfirmClose() {
            UpdateSettings();

            System.Windows.Forms.Application.Restart();
            System.Windows.Application.Current.Shutdown();
        }

        private void UpdateSettings() {
            var settings = clientEndpoint.GetUserSettings();

            var cropYear = desiredDatasource.AvailableYears.Max();

            if (settings.LastDataSourceId != desiredDatasource.DataSourceId || settings.LastCropYear != cropYear) { // don't save unnecessarily
                settings.LastDataSourceId = desiredDatasource.DataSourceId;
                settings.LastCropYear = cropYear;
                clientEndpoint.SaveUserSettings();
            }
            var mapsettings = clientEndpoint.GetMapSettings();
            if(!string.IsNullOrWhiteSpace(desiredDatasource.DefaultAreaUnit) && mapsettings.MapAreaUnit != desiredDatasource.DefaultAreaUnit) {
                mapsettings.MapAreaUnit = desiredDatasource.DefaultAreaUnit;
                clientEndpoint.SaveMapSettings(mapsettings);
            }

            Landdb.Client.Properties.Settings.Default.MasterlistGroup = desiredDatasource.MasterlistGroup;
            Landdb.Client.Properties.Settings.Default.DatasourceCulture = desiredDatasource.DatasourceCulture;
            Landdb.Client.Properties.Settings.Default.Save();
        }

        void CancelClose() {
            Messenger.Default.Send<HideOverlayMessage>(new HideOverlayMessage());
        }
    }
}
