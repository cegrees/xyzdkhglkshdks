﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GalaSoft.MvvmLight;
using ThinkGeo.MapSuite.WpfDesktopEdition;
using ThinkGeo.MapSuite.Core;
using Landdb.Infrastructure;
using System.IO;
using Microsoft.Win32;
using GalaSoft.MvvmLight.Command;
using System.Windows.Input;
using System.Collections.ObjectModel;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Maps.ImageryOverlay;
using Landdb.ViewModel.Fields.FieldDetails.MapModels;

namespace Landdb.ViewModel.Overlays {
    public class ConstructPivotOverlayViewModel : ViewModelBase {
        bool maploaded = false;
        bool startedOutOnline = false;
        InMemoryFeatureLayer currentLayer = new InMemoryFeatureLayer();
        LayerOverlay layerOverlay = new LayerOverlay();
        //InMemoryFeatureLayer hiddenshapeLayer = new InMemoryFeatureLayer();
        //LayerOverlay hiddenOverlay = new LayerOverlay();
        Proj4Projection projection;
        Feature selectedfield = new Feature();
        Feature loadedfeature = new Feature();
        RectangleShape currentmapextent = new RectangleShape();
        string fieldname = string.Empty;
        double radius = 0;
        double pointx = 0;
        double pointy = 0;
        BaseShape fieldshape = new MultipolygonShape();
        Cursor previousCursor;
        ZoomLevelFactory zoomlevelfactory = new ZoomLevelFactory();
        string coordinateformat = string.Empty;

        public ConstructPivotOverlayViewModel() {
            InitializeMap();

            DrawPivotCenterCommand = new RelayCommand(DrawPivotCenter);
            SavePivotCommand = new RelayCommand(SavePolygon);
            ExitCropzoneCommand = new RelayCommand(ExitCropzone);

        }

        public ICommand DrawPivotCenterCommand { get; private set; }
        public ICommand SavePivotCommand { get; private set; }
        public ICommand ExitCropzoneCommand { get; private set; }

        public WpfMap Map { get; set; }

        public Feature SelectedField {
            get { return selectedfield; }
            set {
                if (selectedfield == value) { return; }
                selectedfield = value;
                RaisePropertyChanged("SelectedField");
            }
        }

        public string FieldName {
            get { return fieldname; }
            set {
                if (fieldname == value) { return; }
                fieldname = value;
                RaisePropertyChanged("FieldName");
            }
        }

        public double Radius {
            get { return radius; }
            set {
                if (radius == value) { return; }
                radius = value;
                Drawpivot();
                RaisePropertyChanged("Radius");
            }
        }

        public double PointX {
            get { return pointx; }
            set {
                if (pointx == value) { return; }
                pointx = value;
                Drawpivot();
                RaisePropertyChanged("PointX");
            }
        }

        public double PointY {
            get { return pointy; }
            set {
                if (pointy == value) { return; }
                pointy = value;
                Drawpivot();
                RaisePropertyChanged("PointY");
            }
        }

        public RectangleShape CurrentMapExtent {
            get { return currentmapextent; }
            set {
                if (currentmapextent == value) { return; }
                currentmapextent = value;
                RaisePropertyChanged("CurrentMapExtent");
                ShapeValidationResult svr = currentmapextent.Validate(ShapeValidationMode.Simple);
                if (svr.IsValid) {
                    Map.CurrentExtent = currentmapextent;
                    Map.CenterAt(currentmapextent.GetCenterPoint());
                    Map.Refresh();
                }
            }
        }

        void InitializeMap() {
            Map = new WpfMap();
            startedOutOnline = Landdb.Client.Infrastructure.NetworkStatus.IsInternetAvailableFast();
            Map.MapUnit = GeographyUnit.Meter;
            Map.MapTools.PanZoomBar.Visibility = System.Windows.Visibility.Visible;
            Map.MapTools.PanZoomBar.GlobeButtonVisibility = System.Windows.Visibility.Hidden;
            Map.MapTools.PanZoomBar.VerticalAlignment = System.Windows.VerticalAlignment.Center;
            Map.MapTools.PanZoomBar.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            Map.Background = System.Windows.Media.Brushes.White;
            Map.Cursor = Cursors.Hand;
            zoomlevelfactory = new ZoomLevelFactory();
            Map.ZoomLevelSet = zoomlevelfactory.ZoomLevelSET;
            Map.MouseLeftButtonDown += (sender, e) => {
                ((MainWindow)System.Windows.Application.Current.MainWindow).Draggable = false;
            };

            projection = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetGoogleMapParametersString());

            Map.Loaded += (sender, e) => {
                if (maploaded) { return; }
                LoadBingMapsOverlay();
                maploaded = true;
            };
        }

        private void LoadBingMapsOverlay() {
            if (!startedOutOnline) { return; }
            IMapOverlay mapoverlay = new BingMapOverlay();
            Overlay bingOverlay = mapoverlay.Overlay;
            if (bingOverlay == null) { return; }
            if (Map.Overlays.Contains("Bing")) {
                Map.Overlays.Remove("Bing");
            }
            if (Map.Overlays.Contains("Bing Online")) {
                Map.Overlays.Remove("Bing Online");
            }
            if (Map.Overlays.Contains("Roads Only")) {
                Map.Overlays.Remove("Roads Only");
            }
            if (Map.Overlays.Contains("Road Online")) {
                Map.Overlays.Remove("Road Online");
            }
            if (Map.Overlays.Contains("None")) {
                Map.Overlays.Remove("None");
            }
            //bingOverlay.IsVisible = mapoverlay.Visibility;
            //((TileOverlay)bingOverlay).TileType = TileType.SingleTile;
            Map.Overlays.Add(mapoverlay.Name, bingOverlay);
            Map.Overlays.MoveToBottom(mapoverlay.Name);
        }

        private void OpenAndLoadaShapefile() {
            CreateInMemoryLayerAndOverlay();
            LoadShapeFileOverlayOntoMap();
            if (Map.Overlays.Contains("importOverlay")) {
                if (((LayerOverlay)Map.Overlays["importOverlay"]).Layers.Contains("ImportLayer")) {
                    Map.Refresh();
                }
            }
        }

        private void CreateInMemoryLayerAndOverlay() {
            layerOverlay = new LayerOverlay();
            layerOverlay.DrawingExceptionMode = DrawingExceptionMode.DrawException;
            currentLayer = new InMemoryFeatureLayer();
            AreaStyle currentLayerStyle = AreaStyles.CreateSimpleAreaStyle(new GeoColor(80, GeoColor.SimpleColors.PastelYellow), new GeoColor(100, GeoColor.SimpleColors.PastelYellow));
            currentLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = currentLayerStyle;
            currentLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            projection = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetGoogleMapParametersString());
            currentLayer.FeatureSource.Projection = projection;
            currentLayer.InternalFeatures.Add(SelectedField);
            layerOverlay.Layers.Add("ImportLayer", currentLayer);
        }

        private void LoadShapeFileOverlayOntoMap() {
            if (Map.Overlays.Contains("importOverlay")) {
                Map.Overlays.Remove("importOverlay");
            }
            if (layerOverlay.Layers.Contains("ImportLayer")) {
                Map.Overlays.Add("importOverlay", layerOverlay);
            }

            EnsureCurrentLayerIsOpen();
            var extent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
            try {
                var shapelayerfeatures = currentLayer.QueryTools.GetAllFeatures(ReturningColumnsType.AllColumns);
                if (shapelayerfeatures.Count > 0) {
                    var shapelayercolumns = currentLayer.FeatureSource.GetColumns();
                    extent = ExtentHelper.GetBoundingBoxOfItems(shapelayerfeatures);
                    Map.CurrentExtent = extent;
                    PointShape center = extent.GetCenterPoint();
                    Map.CenterAt(center);
                }
                else {
                    Map.CurrentExtent = extent;
                    PointShape center = extent.GetCenterPoint();
                    Map.CenterAt(center);
                }
            }
            catch {
                Map.CurrentExtent = extent;
                PointShape center = extent.GetCenterPoint();
                Map.CenterAt(center);
            }
        }

        private void EnsureCurrentLayerIsOpen() {
            if (currentLayer == null) {
                currentLayer = new InMemoryFeatureLayer();
            }

            if (!currentLayer.IsOpen) {
                currentLayer.Open();
            }

            if (!projection.IsOpen) {
                projection.Open();
            }
        }

        private void CutLine_TrackEnded(object sender, TrackEndedTrackInteractiveOverlayEventArgs e) {
            Map.TrackOverlay.TrackEnded -= new EventHandler<TrackEndedTrackInteractiveOverlayEventArgs>(CutLine_TrackEnded);
            Map.TrackOverlay.TrackMode = TrackMode.None;
            Map.Cursor = previousCursor;
            if (Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Count > 0) {
                int shapecount = Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Count - 1;
                Feature feat = Map.TrackOverlay.TrackShapeLayer.InternalFeatures[shapecount];
                BaseShape baseShape = feat.GetShape();
                if (baseShape is LineShape) {
                    LineShape lineshape = baseShape as LineShape;
                    Vertex vert = lineshape.Vertices[lineshape.Vertices.Count - 1];
                    PointShape ps1 = new PointShape(vert);
                    pointx = ps1.X;
                    pointy = ps1.Y;
                    Drawpivot();
                }
                if (baseShape is PointShape) {
                    PointShape ps1 = baseShape as PointShape;
                    pointx = ps1.X;
                    pointy = ps1.Y;
                    Drawpivot();
                }
            }
        }

        void Drawpivot() {
            if (radius == 0) { return; }
            if (pointx == 0) { return; }
            if (pointy == 0) { return; }
            PointShape centerpoint = new PointShape();
            centerpoint.X = pointx;
            centerpoint.Y = pointy;
            EllipseShape es = new EllipseShape(centerpoint, (radius * 0.3048));
            Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Clear();
            Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Add(new Feature(es));
            Map.Refresh();
        }

        private void SavePolygon() {
            MultipolygonShape multipoly = new MultipolygonShape();


            if (Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Count > 0) {
                int shapecount = Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Count - 1;
                Feature feat = Map.TrackOverlay.TrackShapeLayer.InternalFeatures[shapecount];
                BaseShape baseShape = feat.GetShape();
                if (baseShape is PolygonShape) {
                    PolygonShape ps = baseShape as PolygonShape;
                    //var importmessage = new ShapeImportMessage() { ShapeBase = ps };
                    //Messenger.Default.Send<ShapeImportMessage>(importmessage);
                }
                if (baseShape is MultipolygonShape) {
                    MultipolygonShape ps = baseShape as MultipolygonShape;
                    //var importmessage = new ShapeImportMessage() { ShapeBase = ps };
                    //Messenger.Default.Send<ShapeImportMessage>(importmessage);
                }
            }
        }

        private void ResetAndRefreshLayers() {
            Map.TrackOverlay.TrackShapeLayer.Open();
            Map.TrackOverlay.TrackShapeLayer.Clear();
            Map.TrackOverlay.TrackShapeLayer.Close();
            Map.EditOverlay.EditShapesLayer.Open();
            Map.EditOverlay.EditShapesLayer.Clear();
            Map.EditOverlay.CalculateAllControlPoints();
            Map.EditOverlay.EditShapesLayer.Close();
            Map.Overlays["fieldOverlay"].Refresh();
            Map.TrackOverlay.Refresh();
            Map.EditOverlay.Refresh();
        }

        void DrawPivotCenter() {
                Map.TrackOverlay.TrackShapeLayer.InternalFeatures.Clear();
                Map.Refresh();
                Map.TrackOverlay.TrackEnded += new EventHandler<TrackEndedTrackInteractiveOverlayEventArgs>(CutLine_TrackEnded);
                Map.TrackOverlay.TrackMode = TrackMode.Point;
                previousCursor = Map.Cursor;
                Map.Cursor = Cursors.Pen;
            }

            void ExitCropzone(){
                Map.Cursor = Cursors.Arrow;
                //var importmessage = new ShapeImportMessage() { ShapeBase = new MultipolygonShape() };
                //Messenger.Default.Send<ShapeImportMessage>(importmessage);
            }
    }
}
