﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Overlays {
    public class UpdateErrorOverlayViewModel : ViewModelBase {
        string technicalDetails;

        public UpdateErrorOverlayViewModel(Exception ex) {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(ex.GetType().FullName);
            sb.AppendLine(ex.Message);
            sb.Append(ex.StackTrace);

            var inex = ex.InnerException;
            while (inex != null) {
                sb.Append(Environment.NewLine);
                sb.AppendLine("------------------------------------------");
                sb.AppendLine(inex.Message);
                sb.Append(inex.StackTrace);
                inex = inex.InnerException;   
            }
            TechnicalDetails = sb.ToString();

            ConfirmCommand = new RelayCommand(CloseOverlay);
            CopyToClipboardCommand = new RelayCommand(CopyToClipboard);
        }

        void CloseOverlay() {
            Messenger.Default.Send<HideOverlayMessage>(new HideOverlayMessage());
        }

        void CopyToClipboard() {
            System.Windows.Clipboard.SetText(TechnicalDetails);
        }

        public RelayCommand ConfirmCommand { get; private set; }
        public RelayCommand CopyToClipboardCommand { get; private set; }

        public string TechnicalDetails {
            get { return technicalDetails; }
            set { technicalDetails = value; }
        }
    }
}
