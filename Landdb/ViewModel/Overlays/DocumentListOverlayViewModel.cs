﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using Landdb.ViewModel.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;

namespace Landdb.ViewModel.Overlays {
    public class DocumentListOverlayViewModel : ViewModelBase
    {
        DocumentListPageViewModel documentListModel;
        Action<bool> onFinish;
        private bool showsavebutton = false;

        public DocumentListOverlayViewModel(DocumentListPageViewModel documentListModel, Action<bool> onFinish)
        {
            documentListModel.OnDocumentInfoChanged = OnDocumentInfoChanged;
            this.documentListModel = documentListModel;

            this.onFinish = onFinish;

            CancelCommand = new RelayCommand(Cancel);
            SaveCommand = new RelayCommand(Save);
        }

        public ICommand SaveCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }

        public DocumentListPageViewModel DocumentListModel
        {
            get { return documentListModel; }
        }

        public bool ShowSaveButton {
            get { return showsavebutton; }
            set {
                showsavebutton = value;
                RaisePropertyChanged(() => ShowSaveButton);
            }
        }

        void Cancel() { onFinish(false); }
        void Save() { onFinish(true); }

        void OnDocumentInfoChanged() {
            ShowSaveButton = true;
        }
    }
}
