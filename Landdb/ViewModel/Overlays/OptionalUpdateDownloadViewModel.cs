﻿using GalaSoft.MvvmLight;
using Landdb.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Overlays {
    public class OptionalUpdateDownloadViewModel : ViewModelBase, IApplicationUpdateStatus {
        bool isUpdating;
        string statusText;
        double maxProgress;
        double currentProgress;

        private bool? _isUpdateAvailable = null;
        public bool? IsUpdateAvailable {
            get { return _isUpdateAvailable; }
            set {
                _isUpdateAvailable = value;
                RaisePropertyChanged(nameof(IsUpdateAvailable));
            }
        }

        public bool IsUpdating {
            get { return isUpdating; }
            set {
                isUpdating = value;
                RaisePropertyChanged(() => IsUpdating);
            }
        }

        public string StatusText {
            get { return statusText; }
            set {
                statusText = value;
                RaisePropertyChanged(() => StatusText);
            }
        }

        public double MaxProgress {
            get { return maxProgress; }
            set {
                maxProgress = value;
                RaisePropertyChanged(() => MaxProgress);
                RaisePropertyChanged(() => ProgressPercent);
            }
        }

        public double CurrentProgress {
            get { return currentProgress; }
            set {
                currentProgress = value;
                RaisePropertyChanged(() => CurrentProgress);
                RaisePropertyChanged(() => ProgressPercent);
            }
        }

        public double ProgressPercent {
            get {
                if (MaxProgress <= 0) { return 0; }
                return (CurrentProgress / MaxProgress) * 100;
            }
        }
    }
}
