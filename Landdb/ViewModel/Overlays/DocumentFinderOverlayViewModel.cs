﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using Landdb.ViewModel.Production;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.ViewModel.Production.Applications;
using Landdb.ViewModel.Production.Invoices;

namespace Landdb.ViewModel.Overlays {
    public enum DocumentFilterTypeEnum {
        [Description("Plans")]
        Plans,
        [Description("Work Orders")]
        WorkOrders,
        [Description("Recommendations")]
        Recommendations,
        [Description("Applications")]
        Applications,
        [Description("Invoices")]
        Invoices,
        //[Description("Site Data")]
        //SiteData,
        [Description("Scouting Application")]
        ScoutingApplication,
        //[Description("Scouting Work Order")]
        //ScoutingWorkOrder,
    }

    public class DocumentFilterConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            if (value == null) { return null; }

            string itemString = value.ToString();
            FieldInfo field = typeof(DocumentFilterTypeEnum).GetField(itemString);
            object[] attribs = field.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (null != attribs && attribs.Length > 0)
                itemString = ((DescriptionAttribute)attribs[0]).Description;

            return itemString;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            if (value.ToString() == "Work Orders") { return DocumentFilterTypeEnum.WorkOrders; }
            if (value.ToString() == "Recommendations") { return DocumentFilterTypeEnum.Recommendations; }
            if (value.ToString() == "Applications") { return DocumentFilterTypeEnum.Applications; }
            if (value.ToString() == "Invoices") { return DocumentFilterTypeEnum.Invoices; }
            if (value.ToString() == "Scouting Application") { return DocumentFilterTypeEnum.ScoutingApplication; }
            //if (value.ToString() == "Scouting Work Order") { return DocumentFilterTypeEnum.ScoutingWorkOrder; }
            return DocumentFilterTypeEnum.Plans;
        }
    }

    public class DocumentFinderOverlayViewModel : ViewModelBase {
        IClientEndpoint clientEndpoint;
        Dispatcher dispatcher;
        DocumentFilterTypeEnum selectedDocumentType = DocumentFilterTypeEnum.Plans;
        Action<DocumentDescriptor> onDocumentChosen;
        object basePage; // TODO: I promise to try and make this better. For reals. -bs

        public DocumentFinderOverlayViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, Action<DocumentDescriptor> onDocumentChosen) {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            this.onDocumentChosen = onDocumentChosen;
            ChooseDocumentCommand = new RelayCommand(ChooseDocument);
            CancelCommand = new RelayCommand(Cancel);
            UpdateBasePage(selectedDocumentType);
        }

        public ICommand ChooseDocumentCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }

        public DocumentFilterTypeEnum SelectedDocumentType {
            get { return selectedDocumentType; }
            set {
                if (selectedDocumentType == value) { return; }

                selectedDocumentType = value;
                RaisePropertyChanged("SelectedDocumentType");

                UpdateBasePage(value);
            }
        }

        public object BasePage {
            get { return basePage; }
        }

        void UpdateBasePage(DocumentFilterTypeEnum value) {
            switch (value) {
                case DocumentFilterTypeEnum.Plans:
                    basePage = new Production.PlansPageViewModel(clientEndpoint, dispatcher);
                    break;
                case DocumentFilterTypeEnum.WorkOrders:
                    basePage = new Production.WorkOrdersPageViewModel(clientEndpoint, dispatcher);
                    break;
                case DocumentFilterTypeEnum.Recommendations:
                    basePage = new Production.RecommendationsPageViewModel(clientEndpoint, dispatcher);
                    break;
                case DocumentFilterTypeEnum.Applications:
                    basePage = new ApplicationsPageViewModel(clientEndpoint, dispatcher);
                    break;
                case DocumentFilterTypeEnum.Invoices:
                    basePage = new InvoicesPageViewModel(clientEndpoint, dispatcher);
                    break;
                case DocumentFilterTypeEnum.ScoutingApplication:

                    //TO DO :: FILTER OUT THESE TO INCLUDE ONLY SCOUTING APPLICATIONS
                    basePage = new Production.SiteDataPageViewModel(clientEndpoint, dispatcher);
                    //List<SiteDataListItemViewModel> list = ((SiteDataPageViewModel)basePage).ListItems.ToList();
                    //list = list.Where(x => x.DocumentType == "ScoutingApplication").ToList();


                    //set basePage to a new view model that only includes scouting applications.


                    break;
                //case DocumentFilterTypeEnum.ScoutingWorkOrder:
                    //basePage = new Production.SiteDataPageViewModel(clientEndpoint, dispatcher);
                    //break;
                default:
                    break;
            }
            RaisePropertyChanged("BasePage");
        }

        string GetCurrentDescriptorTypeString() {
            switch (SelectedDocumentType) {
                case DocumentFilterTypeEnum.Plans:
                    return DocumentDescriptor.PlanTypeString;
                case DocumentFilterTypeEnum.WorkOrders:
                    return DocumentDescriptor.WorkOrderTypeString;
                case DocumentFilterTypeEnum.Recommendations:
                    return DocumentDescriptor.RecommendationTypeString;
                case DocumentFilterTypeEnum.Applications:
                    return DocumentDescriptor.ApplicationTypeString;
                case DocumentFilterTypeEnum.Invoices:
                    return DocumentDescriptor.InvoiceTypeString;
                case DocumentFilterTypeEnum.ScoutingApplication:
                    return DocumentDescriptor.ScoutingApplicationTypeString;
                //case DocumentFilterTypeEnum.ScoutingWorkOrder:
                //    return DocumentDescriptor.ScoutingWorkOrderTypeString;
                default:
                    return string.Empty;
            }
        }

        string GetCurrentSelectedItemName() {
            if (basePage is InvoicesPageViewModel) {
                return ((InvoicesPageViewModel)basePage).SelectedListItem.InvoiceNumber;
            }
            if (basePage is Production.WorkOrdersPageViewModel) {
                return ((Production.WorkOrdersPageViewModel)basePage).SelectedListItem.Name;
            }
            if (basePage is Production.RecommendationsPageViewModel) {
                return ((Production.RecommendationsPageViewModel)basePage).SelectedListItem.NameAndNumber;
            }
            if (basePage is ApplicationsPageViewModel) {
                return ((ApplicationsPageViewModel)basePage).SelectedListItem.Name;
            }
            if (basePage is Production.PlansPageViewModel) {
                return ((Production.PlansPageViewModel)basePage).SelectedListItem.Name;
            }
            if (basePage is Production.SiteDataPageViewModel)
            {
                return ((Production.SiteDataPageViewModel)basePage).SelectedListItem.Name;
            }
            return string.Empty;
        }

        void ChooseDocument() {
            var typeString = GetCurrentDescriptorTypeString();
            if(((dynamic)basePage).SelectedListItem == null) { return; }
            AbstractDataSourceIdentity<Guid, Guid> id = ((dynamic)basePage).SelectedListItem.Id as AbstractDataSourceIdentity<Guid, Guid>;
            var name = GetCurrentSelectedItemName();

            if (string.IsNullOrWhiteSpace(typeString) || id == null || string.IsNullOrWhiteSpace(name)) { return; }
            
            DocumentDescriptor sourceDoc = new DocumentDescriptor() { DocumentType = typeString, Identity = id, Name = name };
            onDocumentChosen(sourceDoc);
        }

        void Cancel()
        {
            onDocumentChosen(null);
        }
    }
}
