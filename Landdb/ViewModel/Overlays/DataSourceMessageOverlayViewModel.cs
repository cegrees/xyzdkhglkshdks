﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Landdb.ViewModel.Overlays {
    public class DataSourceMessageOverlayViewModel : ViewModelBase {
        string message;
        string url;

        public DataSourceMessageOverlayViewModel() {

            Url = "www.landdb.com";

            CloseCommand = new RelayCommand(Close);
            VisitUrlCommand = new RelayCommand(VisitUrl);
        }

        public string Message {
            get { return message; }
            set {
                message = value;
                RaisePropertyChanged("Message");
            }
        }

        public string Url {
            get { return url; }
            set {
                url = value;
                RaisePropertyChanged("Url");
            }
        }

        public ICommand CloseCommand { get; private set; }
        public ICommand VisitUrlCommand { get; private set; }

        private void Close() {
            Messenger.Default.Send<HideOverlayMessage>(new HideOverlayMessage());
        }

        private void VisitUrl() {
            System.Diagnostics.Process.Start(this.Url);

            Messenger.Default.Send<HideOverlayMessage>(new HideOverlayMessage());
        }
    }
}
