﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GalaSoft.MvvmLight;
using Landdb.Client.Services;

namespace Landdb.ViewModel.Overlays {
    public class ProgressOverlayViewModel : ViewModelBase, IProgressTracker {
        string statusMessage;
        int totalRecords;
        int currentRecord;

        public string StatusMessage {
            get { return statusMessage; }
            set {
                statusMessage = value;
                RaisePropertyChanged("StatusMessage");
            }
        }

        public int TotalRecords {
            get { return totalRecords; }
            set {
                totalRecords = value;
                RaisePropertyChanged("TotalRecords");
                RaisePropertyChanged("ProgressPercent");
                RaisePropertyChanged("HasProgress");
            }
        }

        public int CurrentRecord {
            get { return currentRecord; }
            set {
                currentRecord = value;
                RaisePropertyChanged("CurrentRecord");
                RaisePropertyChanged("ProgressPercent");
            }
        }

        public double ProgressPercent {
            get {
                if (totalRecords <= 0) { return 0; }

                return 100 * currentRecord / totalRecords; 
            }
        }

        public bool HasProgress {
            get {
                return totalRecords > 0;
            }
        }
    }
}
