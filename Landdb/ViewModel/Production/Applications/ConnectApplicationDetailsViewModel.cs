﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using AgC.UnitConversion;
using AgC.UnitConversion.Package;
using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using Landdb.Client.Models.Connect;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.ViewModel.Shared;
using Lokad.Cqrs;

namespace Landdb.ViewModel.Production.Applications
{
    public sealed class ConnectApplicationDetailsViewModel : AbstractApplicationDetailsViewModel
    {
        private readonly ConnectApplication connectApplication;
        private readonly IClientEndpoint clientEndpoint;
        private readonly Dispatcher dispatcher;

        public ConnectApplicationDetailsViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, ConnectApplication connectApplication)
        {
            this.dispatcher = dispatcher;
            this.clientEndpoint = clientEndpoint;
            this.connectApplication = connectApplication;
            MapConnectApplicationToProperties(connectApplication);
        }

        public override string Name { get; set; }
        public override string StartDateTime { get; set; }
        public override string Notes { get; set; }
        public override string ApplicationType { get; set; } = "By Rate Application";
        public override decimal TotalAvgCost { get; set; } = 0.0m;
        public override decimal TotalInvoiceCost { get; set; } = 0.0m;
        public override decimal TotalSpecificCost { get; set; } = 0.0m;
        public override string TimeDisplay { get; set; }
        public override ObservableCollection<CropZoneDetails> CropZones { get; set; }
        public override ObservableCollection<ApplicationProductDetails> Products { get; set; }
        public string TotalAreaDisplayText { get; private set; }

        public override DataTemplate ApplicationDataTemplate
        {
            get
            {
                ResourceDictionary applicationResourceDictionary = new ResourceDictionary
                {
                    Source = new Uri("/Landdb;component/Views/Templates/ConnectApplicationDetailsTemplate.xaml", UriKind.RelativeOrAbsolute)
                };

                return applicationResourceDictionary["ConnectApplicationDetailsTemplate"] as DataTemplate;
            }
        }

        public string ConnectWarningText => 
            "This application exists in Connect. It must be matched and imported before it is included in inventory, product summaries, and reports.";

        private void MapConnectApplicationToProperties(ConnectApplication connectApplication)
        {
            Name = connectApplication.Description;
            StartDateTime = (connectApplication.TimeScopes
                .TryGetValue("Start", out DateTime startDate) ? startDate : connectApplication.TimeScopes.ToList().First().Value)
                .ToShortDateString();
            Notes = connectApplication.Notes;
            TotalAreaDisplayText = $"{connectApplication.TotalArea?.Value:F2} {connectApplication.TotalArea?.Unit}";

            CropZones = new ObservableCollection<CropZoneDetails>(MapLandUseToCropZones(clientEndpoint, dispatcher, connectApplication.Land));

            Products = new ObservableCollection<ApplicationProductDetails>(MapProductUseToApplicaitonProductDetails(connectApplication.Products));
        }

        private static List<CropZoneDetails> MapLandUseToCropZones(IClientEndpoint clientEndpoint, Dispatcher dispatcher, List<ConnectApplication.LandUse> landUses)
        {
            List<CropZoneDetails> result = new List<CropZoneDetails>();

            foreach (ConnectApplication.LandUse landUse in landUses)
            {
                Maybe<CropZoneDetails> possibleMatch = CheckMatchForDetails(clientEndpoint, dispatcher, landUse);
                Maybe<CropZoneDetails> possibleCandidateDetails = possibleMatch.HasValue 
                                                                  ? Maybe<CropZoneDetails>.Empty 
                                                                  : CheckCandidatesForDetails(clientEndpoint, dispatcher, landUse);

                if (possibleMatch.HasValue)
                {
                    result.Add(possibleMatch.Value);
                }
                else if (possibleCandidateDetails.HasValue && !possibleMatch.HasValue)
                {
                    result.Add(possibleCandidateDetails.Value);
                }
                else
                {
                    result.Add(new CropZoneDetails
                    {
                        Name = $"{landUse.ResourceProperties?["FARM_NAME"]}\\{landUse.ResourceDescription}"
                    });
                }
            }

            return result;
        }

        private static Maybe<CropZoneDetails> CheckMatchForDetails(IClientEndpoint clientEndpoint, Dispatcher dispatcher, ConnectApplication.LandUse landUse)
        {
            var match = landUse.Match;

            if (match != null)
            {
                var possibleCropZone = clientEndpoint.GetView<CropZoneDetailsView>(new CropZoneId(match.Id.DataSourceId, match.Id.Id));
                var possibleFieldMatch = possibleCropZone.HasValue
                                    ? Maybe<FieldDetailsView>.Empty
                                    : clientEndpoint.GetView<FieldDetailsView>(new FieldId(match.Id.DataSourceId, match.Id.Id));

                if (possibleCropZone.HasValue)
                {
                    var matchCropZone = possibleCropZone.Value;

                    var possibleField =
                        clientEndpoint.GetView<FieldDetailsView>(new FieldId(matchCropZone.FieldId.DataSourceId,
                            matchCropZone.FieldId.Id));

                    FieldDetailsView field = possibleField.Value;

                    return CropZoneDetailsMapper.MapCropZoneDetailsViewToCropZoneDetails(clientEndpoint, dispatcher, possibleCropZone.Value,
                        field.Name);
                }
                else if (possibleFieldMatch.HasValue && !possibleCropZone.HasValue)
                {
                    return CropZoneDetailsMapper.MapFieldDetailsViewToCropZoneDetails(clientEndpoint, dispatcher, possibleFieldMatch.Value);
                }
                else
                {
                    return Maybe<CropZoneDetails>.Empty;
                }
            }
            else
            {
                return Maybe<CropZoneDetails>.Empty;
            }
        }

        private static Maybe<CropZoneDetails> CheckCandidatesForDetails(IClientEndpoint clientEndpoint, Dispatcher dispatcher,  ConnectApplication.LandUse landUse)
        {
            var candidates = landUse.Candidates;

            if (candidates.Count > 0)
            {
                var firstCandidate = candidates.First();
                Guid dataSourceId = firstCandidate.Id.DataSourceId;
                Guid possibleId = firstCandidate.Id.Id;
                
                Maybe<CropZoneDetails> possibleCropZone = GetPossibleCropZone(clientEndpoint, dispatcher, dataSourceId, possibleId);
                Maybe<CropZoneDetails> possibleField = possibleCropZone.HasValue 
                                                       ? Maybe<CropZoneDetails>.Empty 
                                                       : GetPossibleField(clientEndpoint, dispatcher, dataSourceId, possibleId);

                if (possibleCropZone.HasValue)
                {
                    return possibleCropZone.Value;
                }
                else if (!possibleCropZone.HasValue && possibleField.HasValue)
                {
                    return possibleField.Value;
                }
                else
                {
                    return Maybe<CropZoneDetails>.Empty;
                }
            }
            else
            {
                return Maybe<CropZoneDetails>.Empty;
            }
        }

        private static Maybe<CropZoneDetails> GetPossibleField(IClientEndpoint clientEndpoint, Dispatcher dispatcher, Guid dataSourceId, Guid fieldId)
        {
            Maybe<FieldDetailsView> maybeField = clientEndpoint.GetView<FieldDetailsView>(new FieldId(dataSourceId, fieldId));

            return maybeField.HasValue 
                   ? CropZoneDetailsMapper.MapFieldDetailsViewToCropZoneDetails(clientEndpoint, dispatcher, maybeField.Value)
                   : Maybe<CropZoneDetails>.Empty;
        }

        private static Maybe<CropZoneDetails> GetPossibleCropZone(IClientEndpoint clientEndpoint, Dispatcher dispatcher, Guid dataSourceId, Guid cropZoneId)
        {
            Maybe<CropZoneDetailsView> possibleCropZone = clientEndpoint.GetView<CropZoneDetailsView>(new CropZoneId(dataSourceId, cropZoneId));
            var possibleField = clientEndpoint.GetView<FieldDetailsView>(
                new FieldId(possibleCropZone.Value.FieldId.DataSourceId, possibleCropZone.Value.FieldId.Id));

            return possibleCropZone.HasValue 
                   ? CropZoneDetailsMapper.MapCropZoneDetailsViewToCropZoneDetails(clientEndpoint, dispatcher, possibleCropZone.Value, possibleField.Value.Name) 
                   : Maybe<CropZoneDetails>.Empty;
        }

        private static List<ApplicationProductDetails> MapProductUseToApplicaitonProductDetails(List<ConnectApplication.ProductUse> productUses)
        {
            List<ApplicationProductDetails> results = new List<ApplicationProductDetails>();

            foreach (ConnectApplication.ProductUse productUse in productUses)
            {
                if (productUse.IsTankMix)
                {
                    results.AddRange(TankMixParser.ParseATankMix(productUse));
                }
                else
                {
                    results.Add(TankMixParser.MapProductUseToApplicaitonProductDetails(productUse));
                }
            }

            return results;
        }

        public ICommand LaunchConnectApplicationInBrowserCommand => new RelayCommand(LaunchConnectApplicationInBrowser);

        private void LaunchConnectApplicationInBrowser()
        {
            System.Diagnostics.Process.Start(
                "https://connect.landdb.com?type=applications" +
                $"&dataSourceId={connectApplication.DatasourceId}" +
                $"&productionId={Guid.Parse(connectApplication.id.Split('_')[1]).ToString()}" +
                $"&cropYear={ApplicationEnvironment.CurrentCropYear}"
            );
        }
    }
}