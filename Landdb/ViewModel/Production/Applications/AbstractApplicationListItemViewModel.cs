﻿using System;
using System.Windows;
using System.Windows.Threading;
using GalaSoft.MvvmLight;
using Landdb.Client.Infrastructure;

namespace Landdb.ViewModel.Production.Applications
{
    public abstract class AbstractApplicationListItemViewModel : ViewModelBase
    {
        public abstract ApplicationId Id { get; }
        public abstract string Name { get; }
        public abstract string CharmName { get; set; }
        public abstract ApplicationListItemType ListItemType { get; }
        public abstract DateTime? StartDateTime { get; }
        public abstract int FieldCount { get; }
        public abstract int ProductCount { get; }
        public abstract decimal TotalCost { get; set; }
        public abstract DataTemplate ListItemTemplate { get; }

        public abstract AbstractApplicationDetailsViewModel CreateDetailsModel(AbstractApplicationListItemViewModel selectedItem, IClientEndpoint clientEndPoint, Dispatcher dispatcher, Action<string> setCharm);

        public abstract bool PerformItemRemoval(IClientEndpoint clientEndpoint, AbstractApplicationListItemViewModel selectedItem);

        public abstract IDomainCommand CreateSetCharmCommand(IClientEndpoint clientEndpoint, 
            AbstractApplicationListItemViewModel selectedItem, string charmName);
    }
}