﻿using System;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows;
using AgC.UnitConversion;
using GalaSoft.MvvmLight;
using Landdb.Resources;
using Landdb.ViewModel.Shared;

namespace Landdb.ViewModel.Production.Applications
{
    public abstract class AbstractApplicationDetailsViewModel : ViewModelBase
    {
        public abstract string Name { get; set; }
        public abstract string StartDateTime { get; set; }
        public abstract ObservableCollection<ApplicationProductDetails> Products { get; set; }
        public abstract string Notes { get; set; }
        public abstract DataTemplate ApplicationDataTemplate { get; }
        public abstract ObservableCollection<CropZoneDetails> CropZones { get; set; }
        public abstract string ApplicationType { get; set; }
        public abstract decimal TotalAvgCost { get; set; }
        public abstract decimal TotalInvoiceCost { get; set; }
        public abstract decimal TotalSpecificCost { get; set; }
        public abstract string TimeDisplay { get; set; }

        public static string CalculateTimeDisplay(DateTime dateTimeInUtc, TimeSpan? duration, DateTime? endDateTimeInUtc)
        {
            var sb = new StringBuilder();
            var dateTimeLocal = dateTimeInUtc.ToLocalTime();

            sb.Append(dateTimeLocal.ToString("t"));

            if (duration.HasValue)
            { // ignore end date
                DateTime end = dateTimeLocal + duration.Value;
                if (end.Date == dateTimeLocal.Date)
                {
                    sb.AppendFormat(" - {0:t} ({1:G} " + Strings.Hours_Text + ")", end, Math.Round(duration.Value.TotalHours, 2));
                }
                else
                {
                    sb.AppendFormat(" - {0:g} ({1:G} " + Strings.Hours_Text + ")", end, Math.Round(duration.Value.TotalHours, 2));
                }
            }
            else
            { // base on end date, if possible
                if (endDateTimeInUtc.HasValue)
                {
                    TimeSpan d = endDateTimeInUtc.Value - dateTimeInUtc;
                    if (endDateTimeInUtc.Value.Date == dateTimeInUtc.Date)
                    {
                        sb.AppendFormat(" - {0:t} ({1:G}" + Strings.Hours_Text + ")", endDateTimeInUtc.Value.ToLocalTime(), Math.Round(d.TotalHours, 2));
                    }
                    else
                    {
                        sb.AppendFormat(" - {0:g} ({1:G}" + Strings.Hours_Text + ")", endDateTimeInUtc.Value.ToLocalTime(), Math.Round(d.TotalHours, 2));
                    }
                }
            }

            return sb.ToString();
        }
    }
}