﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using AgC.UnitConversion;
using AgC.UnitConversion.Area;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Client.Services.Credentials;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Contract;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Domain.ReadModels.Invoice;
using Landdb.Domain.ReadModels.Shared;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Domain.ReadModels.WorkOrder;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.Resources;
using Landdb.ViewModel.Production.Popups;
using Landdb.ViewModel.Secondary.Application;
using Landdb.ViewModel.Secondary.Reports.Application;
using Landdb.ViewModel.Shared;
using Landdb.Views.Secondary.Reports.Work_Order;
using Lokad.Cqrs;
using Newtonsoft.Json;
using NLog;
using Application = DocumentFormat.OpenXml.ExtendedProperties.Application;

namespace Landdb.ViewModel.Production.Applications {
	public class ApplicationDetailsViewModel : AbstractApplicationDetailsViewModel {

		readonly IClientEndpoint clientEndpoint;
		readonly Dispatcher dispatcher;
		readonly IMasterlistService masterlist;
		readonly ApplicationView applicationView;
		readonly WorkOrderView workOrderView;
		ApplicationProductDetails selectedProduct;
		readonly CropYearId currentCropYearId;
		readonly Logger log = LogManager.GetCurrentClassLogger();
        string errorMessage = string.Empty;

        public ApplicationDetailsViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, CropYearId currentCropYearId, DateTime startDate, List<IncludedProduct> products, List<IncludedCropZone> cropzones, List<DocumentDescriptor> sources) {
            EnvironmentConditions = new EnvironmentConditions();
            this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;
            this.currentCropYearId = currentCropYearId;
            this.masterlist = clientEndpoint.GetMasterlistService();
			var inventory = clientEndpoint.GetView<InventoryListView>(currentCropYearId).GetValue(new InventoryListView());
			var productUnits = clientEndpoint.GetProductUnitResolver();
            errorMessage = string.Empty;
            StartDateTime = startDate.ToLocalTime().ToString(@"MMMM d, yyyy");
            Sources = new List<DocumentDescriptor>(sources ?? new List<DocumentDescriptor>()); // This insulates us from re-using the readmodel list and inadvertently modifying it
            var invoices = Sources.Where(x => x.DocumentType == "Invoice");

            List<InvoiceView> invoicesDetail = new List<InvoiceView>();
            foreach( var invoice in invoices)
            {
                var detailsMaybe = clientEndpoint.GetView<InvoiceView>(invoice.Identity as InvoiceId);

                if (detailsMaybe.HasValue)
                {
                    invoicesDetail.Add(detailsMaybe.Value);
                }
            }

            //var invoicesDetail = from inv in invoices
            //                     select clientEndpoint.GetView<InvoiceView>(inv.Identity as InvoiceId).Value;

			Products = new ObservableCollection<ApplicationProductDetails>();
            var pestList = masterlist.GetPestList();
			foreach (var p in products) {
				if (p == null || string.IsNullOrWhiteSpace(p.RateUnit) || string.IsNullOrWhiteSpace(p.AreaUnit) || string.IsNullOrWhiteSpace(p.RateUnit)) {
					continue; // Guard statement for weeding out badly formatted products
				}
                var masterlistProd = masterlist.GetProduct(p.Id);

				var invProd = inventory.Products.FirstOrDefault(x => x.Key == p.Id);
				var value = invProd.Value != null ? invProd.Value : null;
				var avgPriceUnit = value != null && value.AveragePriceUnit != null ? productUnits.GetPackageSafeUnit(value.ProductId, value.AveragePriceUnit) : null;
				var avgPriceValue = value != null ? value.AveragePriceValue : 0;

                var pest = p.TargetPest != null ? masterlist.GetPest(p.TargetPest.PestId, p.TargetPest.CommonName, p.TargetPest.LatinName) : null;
				var targetPest = pest != null ? pest : null;

				if (masterlistProd == null) {
					masterlistProd = new MiniProduct() {
						Id = p.Id.Id,
						Name = Strings.UnknownProduct_Text,
						Density = null,
						StdUnit = p.TotalProductUnit,
						StdPackageUnit = p.TotalProductUnit,
					};
				}

                var associatedProducts = new List<AssociatedProductItem>();
                foreach(var associate in p.AssociatedProducts)
                {
                    //var testView = clientEndpoint.GetView<ApplicationView>()
                    var associateProd = new AssociatedProductItem(clientEndpoint, masterlistProd, null);
                    var associateMLP = masterlist.GetProduct(associate.ProductId);
                    associateProd.CustomUnit = UnitFactory.GetPackageSafeUnit(associate.CustomProductUnit, associateMLP.StdUnit, associateMLP.StdFactor, associateMLP.StdPackageUnit);
                    associateProd.CustomRateValue = associate.CustomProductValue;
                    associateProd.HasCost = associate.HasCost;
                    associateProd.CostPerUnitValue = associate.HasCost ? associate.SpecificCostPerUnitValue : 0;
                    associateProd.CostPerUnitUnit = associate.HasCost && !string.IsNullOrEmpty(associate.SpecificCostPerUnitUnit) ? UnitFactory.GetPackageSafeUnit(associate.SpecificCostPerUnitUnit, associateMLP.StdUnit, associateMLP.StdFactor, associateMLP.StdPackageUnit) : null;
                    associateProd.ProductID = associate.ProductId;
                    associateProd.ProductName = associate.ProductName;
                    associateProd.RatePerAreaUnit = UnitFactory.GetPackageSafeUnit(associate.RatePerAreaUnit, associateMLP.StdUnit, associateMLP.StdFactor, associateMLP.StdPackageUnit);
                    associateProd.RatePerAreaValue = associate.RatePerAreaValue;
                    //associateProd.CostPerUnitUnit = UnitFactory.GetPackageSafeUnit(associate.SpecificCostPerUnitUnit, associateMLP.StdUnit, associateMLP.StdFactor, associateMLP.StdPackageUnit);
                    //associateProd.CostPerUnitValue = associate.SpecificCostPerUnitValue;
                    associateProd.TotalProductUnit = UnitFactory.GetPackageSafeUnit(associate.TotalProductUnit, associateMLP.StdUnit, associateMLP.StdFactor, associateMLP.StdPackageUnit);
                    associateProd.TotalProductValue = associate.TotalProductValue;
                    associateProd.TrackingID = associate.TrackingId;

                    switch (associate.CustomRateType)
                    {
                        case "ByBag":
                            associateProd.CustomRateType = AssociatedProductRateType.ByBag;
                            break;
                        case "ByCWT":
                            associateProd.CustomRateType = AssociatedProductRateType.ByCWT;
                            break;
                        case "ByRow":
                            associateProd.CustomRateType = AssociatedProductRateType.ByRow;
                            break;
                        case "BySeed":
                            associateProd.CustomRateType = AssociatedProductRateType.BySeed;
                            break;
                        default:
                            associateProd.CustomRateType = AssociatedProductRateType.BySeed;
                            break;
                    }

                    associatedProducts.Add(associateProd);
                }



				var density = p.Density == null ? masterlistProd.Density : p.Density;

				var totalProdUnit = p.TotalProductUnit != null ? productUnits.GetPackageSafeUnit(value.ProductId, p.TotalProductUnit) : null;
				var totalProdMeasure = totalProdUnit.GetMeasure((double)p.TotalProductValue, density);
				var convertedTotalProdMeasure = totalProdMeasure.CanConvertTo(avgPriceUnit) ? avgPriceUnit.GetMeasure(totalProdMeasure.GetValueAs(avgPriceUnit)) : totalProdMeasure;

                //TO DO :: ADD UP THE COST FOR THE ASSOCIATE PRODUCTS
                foreach(var associate in p.AssociatedProducts)
                {
                    if (associate.HasCost)
                    {
                        var associateMLP = masterlist.GetProduct(associate.ProductId);
                        var associateInvProd = inventory.Products.SingleOrDefault(x => x.Key == associate.ProductId).Value;

                            var avgCost = associateInvProd != null ? associateInvProd.AveragePriceValue : 0;
                            var avgCostUnit = associateInvProd != null && !string.IsNullOrEmpty(associateInvProd.AveragePriceUnit) ? UnitFactory.GetPackageSafeUnit(associateInvProd.AveragePriceUnit, associateMLP.StdUnit, associateMLP.StdFactor, associateMLP.StdPackageUnit) : null;
                            var totalProd = UnitFactory.GetPackageSafeUnit(associate.TotalProductUnit, associateMLP.StdUnit, associateMLP.StdFactor, associateMLP.StdPackageUnit).GetMeasure((double)associate.TotalProductValue, associateMLP.Density);
                            var convertedValue = avgCostUnit != null && totalProd.Unit != avgCostUnit && totalProd.CanConvertTo(avgCostUnit) ? totalProd.GetValueAs(avgCostUnit) : totalProd.Value;

                            TotalAvgCost += avgCost != 0 ? (decimal)(convertedValue * avgCost) : 0;
                        

                        if(associate.SpecificCostPerUnitValue != 0 && !string.IsNullOrEmpty( associate.SpecificCostPerUnitUnit ))
                        {
                            var specificUnit = UnitFactory.GetPackageSafeUnit(associate.SpecificCostPerUnitUnit, associateMLP.StdUnit, associateMLP.StdFactor, associateMLP.StdPackageUnit);
                            var convertedSpecificValue = totalProd.Unit != specificUnit && totalProd.CanConvertTo(specificUnit) ? totalProd.GetValueAs(specificUnit) : totalProd.Value;
                            var specificTotalCost = (decimal)convertedSpecificValue * associate.SpecificCostPerUnitValue;
                            TotalSpecificCost += specificTotalCost;
                        }
                    }

                }

				var specificCostUnit = p.SpecificCostUnit != null ? productUnits.GetPackageSafeUnit(value.ProductId, p.SpecificCostUnit) : null;
				var totalProductInSpecificPriceUnit = p.SpecificCostPerUnit != null && specificCostUnit != null ? (decimal)totalProdMeasure.GetValueAs(specificCostUnit) : 0m;
                TotalAvgCost += (decimal)(convertedTotalProdMeasure.Value * avgPriceValue);
                TotalSpecificCost += p.SpecificCostPerUnit != null ? (totalProductInSpecificPriceUnit * p.SpecificCostPerUnit.Value) : 0m;

				var displayRate = DisplayMeasureConverter.ConvertToRateDisplayMeasure(p.Id, p.RateValue, p.RateUnit, masterlistProd.Density, clientEndpoint, ApplicationEnvironment.CurrentDataSourceId);
				var displayTotal = DisplayMeasureConverter.ConvertToTotalDisplayMeasure(p.Id, p.TotalProductValue, p.TotalProductUnit, masterlistProd.Density, clientEndpoint, ApplicationEnvironment.CurrentDataSourceId);

				var displayTank = p.RatePerTankUnit != null ?
					DisplayMeasureConverter.ConvertToRatePerTankDisplayMeasure(p.Id, p.RatePerTankValue, p.RatePerTankUnit, density, clientEndpoint, ApplicationEnvironment.CurrentDataSourceId) ?? null :
					null;

                decimal costPerinvoiceTotalMeasureUnit = 0m;
                Measure convertedTotalProdToInvoiceUnit =  null;

                if (invoicesDetail.Any())
                {
                    var pds = from inv in invoicesDetail
                              from prod in inv.Products
                              where prod.ProductId == p.Id
                              select prod;

                    Measure invoiceTotalMeasure = null;
                    decimal totalInvoiceCost = 0m;
                    foreach (var invoiceProd in pds)
                    {
                        var totalInvoiceProdUnit = UnitFactory.GetPackageSafeUnit(invoiceProd.TotalProductUnit, masterlistProd.StdUnit, masterlistProd.StdFactor, masterlistProd.StdPackageUnit);
                        var totalInvoiceProd = totalInvoiceProdUnit.GetMeasure((double)invoiceProd.TotalProductValue, masterlistProd.Density);

                        if (invoiceTotalMeasure == null)
                        {
                            invoiceTotalMeasure = totalInvoiceProd;
                            totalInvoiceCost += invoiceProd.TotalCost;
                        }
                        else
                        {
                            var convertedTo = totalInvoiceProd.CanConvertTo(invoiceTotalMeasure.Unit) && invoiceTotalMeasure.Unit != totalInvoiceProdUnit ? totalInvoiceProd.GetValueAs(totalInvoiceProdUnit) : totalInvoiceProd.Value;
                            invoiceTotalMeasure = invoiceTotalMeasure.Unit.GetMeasure(invoiceTotalMeasure.Value + convertedTo, masterlistProd.Density);
                            totalInvoiceCost += invoiceProd.TotalCost;
                        }
                    }

                    costPerinvoiceTotalMeasureUnit = invoiceTotalMeasure != null ? (totalInvoiceCost / (decimal)invoiceTotalMeasure.Value) : 0m;
                    convertedTotalProdToInvoiceUnit = invoiceTotalMeasure != null && totalProdMeasure.CanConvertTo(invoiceTotalMeasure.Unit) && totalProdMeasure.Unit != invoiceTotalMeasure.Unit ? totalProdMeasure.CreateAs(invoiceTotalMeasure.Unit) : totalProdMeasure;
                    TotalInvoiceCost += (costPerinvoiceTotalMeasureUnit * (decimal)convertedTotalProdToInvoiceUnit.Value);
                }

                //Handling Invoice Cost Per Unit
                //InvoiceCostPerUnitDisplay = convertedTotalProdMeasure != null ? string.Format(@"{0} per {1}", costPerinvoiceTotalMeasureUnit.ToString("C2"), convertedTotalProdMeasure.Unit.AbbreviatedDisplay) : string.Empty,
                decimal TotalInvoiceCostxx = convertedTotalProdToInvoiceUnit != null ? (costPerinvoiceTotalMeasureUnit * (decimal)convertedTotalProdToInvoiceUnit.Value) : 0m;
                decimal invoicecostperunit = 0m;
                if (convertedTotalProdMeasure != null && convertedTotalProdMeasure.Value != 0) {
                    invoicecostperunit = TotalInvoiceCostxx / (decimal)convertedTotalProdMeasure.Value;
                }
                //Test the invoice display
                //string InvoiceCostPerUnitDisplayxx = convertedTotalProdMeasure != null ? string.Format(@"{0} per {1}", costPerinvoiceTotalMeasureUnit.ToString("C2"), convertedTotalProdMeasure.Unit.AbbreviatedDisplay) : string.Empty;
                //string ProductNamexx = masterlistProd.Name;

                Products.Add(new ApplicationProductDetails() {
                    ProductName = masterlistProd.Name,
                    RateMeasure = displayRate,
                    AreaUnit = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit),
                    AreaValue = (decimal)cropzones.Sum(x => x.Area.Value),
                    TotalProduct = displayTotal,
                    TotalCost = (decimal)(convertedTotalProdMeasure.Value * avgPriceValue),
                    CostPerUnitValue = (decimal)avgPriceValue,
                    CostUnit = avgPriceUnit == null ? null : avgPriceUnit,
                    ID = p.Id,
                    ProductAreaPercent = p.PercentApplied,
                    TrackingId = p.TrackingId,
                    ApplicationMethod = clientEndpoint.GetMasterlistService().GetApplicationMethodList().Where(x => x.Name == p.ApplicationMethod).FirstOrDefault(),
                    RatePerTank = displayTank,
                    Split = p.TotalGrowerProduct.ToString("N2") + " / " + (p.TotalProductValue - p.TotalGrowerProduct).ToString("N2"),
                    GrowerShare = p.GrowerShare,
                    FromGrowerInventory = p.FromGrowerInventory,
                    TargetPest = targetPest,
                    TotalSpecificCost = (p.SpecificCostPerUnit != null && specificCostUnit != null) ? (totalProductInSpecificPriceUnit * p.SpecificCostPerUnit) : 0m,
                    SpecificCostUnit = p.SpecificCostUnit == null ? null : productUnits.GetPackageSafeUnit(p.Id, p.SpecificCostUnit),
                    SpecificCostPerUnitValue = p.SpecificCostPerUnit,
                    TotalInvoiceCost = convertedTotalProdToInvoiceUnit != null ? (costPerinvoiceTotalMeasureUnit * (decimal)convertedTotalProdToInvoiceUnit.Value) : 0m,
                    InvoiceCostIsVisible = costPerinvoiceTotalMeasureUnit != 0 ? true : false,
                    InvoiceCostPerUnitDisplay = convertedTotalProdMeasure != null ? string.Format(@Strings.Per_Format_Text, invoicecostperunit.ToString("C2"), convertedTotalProdMeasure.Unit.AbbreviatedDisplay) : string.Empty,
                    HasInvoiceCost = convertedTotalProdToInvoiceUnit != null && (costPerinvoiceTotalMeasureUnit * (decimal)convertedTotalProdToInvoiceUnit.Value) > 0 ? true : false,
                    AssociatedProducts = new ObservableCollection<AssociatedProductItem>( associatedProducts ),   
                    HasAssociatedProducts = associatedProducts.Count > 0 ? true : false,                
				});
			}

			ShowSpecificCost = Products.Any(x => x.HasSpecificCost);
            ShowInvoiceCost = Products.Any(x => x.InvoiceCostIsVisible);

			CropZones = new ObservableCollection<CropZoneDetails>();

			var fTreeView = clientEndpoint.GetView<FlattenedTreeHierarchyView>(currentCropYearId);

			foreach (var c in cropzones) {
				var mapItem = clientEndpoint.GetView<Landdb.Domain.ReadModels.Map.ItemMap>(c.Id);
				string mapData = null;
				System.Windows.Shapes.Path shapePreview = null;
				var czDetails = clientEndpoint.GetView<CropZoneDetailsView>(c.Id).HasValue ? clientEndpoint.GetView<CropZoneDetailsView>(c.Id).Value : null;
				double area = 0.0;

				if (czDetails != null) {
					area = czDetails.ReportedArea != null ? (double)czDetails.ReportedArea : czDetails.BoundaryArea != null ? (double)czDetails.BoundaryArea : 0;
				} else {
					area = c.Area.Value;
				}

				var name = c.Name;

				if (fTreeView.HasValue) {
					var fti = fTreeView.Value.Items.Where(x => x.CropZoneId == c.Id).FirstOrDefault();
					if (fti != null) { name = fti.GetFullName(); }
				}

				double coveragePercent = area != 0 ? (c.Area.Value / area) * 100 : 0;

				if (mapItem.HasValue && mapItem.Value.MostRecentMapItem != null) {
					mapData = mapItem.Value.MostRecentMapItem.MapData;
					dispatcher.BeginInvoke(new Action(() => {
						shapePreview = Landdb.Client.Spatial.WpfTransforms.CreatePathFromMapData(new[] { mapData }, 40);
						CropZones.Add(new CropZoneDetails() { Id = c.Id, Name = name, Area = c.Area, ShapePreview = shapePreview, Percent = string.Format("{0}%", coveragePercent.ToString("N0")) });
					}));
				} else {
					CropZones.Add(new CropZoneDetails() { Name = name, Area = c.Area, Id = c.Id, Percent = string.Format("{0}%", coveragePercent.ToString("N0")) });
				}

				Contracts = new Dictionary<ContractId, RentContractDetailsView>();
				var contractYears = clientEndpoint.GetView<CropZoneRentContractView>(currentCropYearId).GetValue(new CropZoneRentContractView());
				if (contractYears.CropZoneRentContracts.ContainsKey(c.Id.Id)) {
					var rentContractId = contractYears.CropZoneRentContracts[c.Id.Id].RentContractId;

					if (rentContractId != null) {
						var contractDetails = clientEndpoint.GetView<RentContractDetailsView>(rentContractId).GetValue(() => null);
						if (contractDetails != null) {
							Contracts.Add(contractDetails.Id, contractDetails);
						}
					}
				}
			}

			TotalArea = cropzones.Count() > 0 ? cropzones[0].Area.Unit.GetMeasure(cropzones.Sum(x => x.Area.Value)) : AreaMeasure.DefaultUnit.GetMeasure(0.0);

			ChangeApplicationTimeCommand = new RelayCommand(this.OnChangeApplicationTime);
			ChangeFieldSelectionCommand = new RelayCommand(this.ChangeFieldSelection);
			ChangeTankCommand = new RelayCommand(this.ChangeTankSelection);
			EditWeatherInfo = new RelayCommand(this.WeatherInfoEdit);
			EditAuthorizerInfo = new RelayCommand(this.AuthorizerInfoEdit);
			EditApplicatorInfo = new RelayCommand(this.ApplicatorInfoEdit);
			EditNotesInfo = new RelayCommand(this.NotesInfoEdit);
			NameInfo = new RelayCommand(this.NameInfoEdit);
			AddProduct = new RelayCommand(this.AddAppliedProduct);
		}

	    /// <summary>
	    /// Loads the applications into the main Applications Details View.
	    /// </summary>
	    /// <param name="applicationId"></param>
	    /// <param name="clientEndpoint"></param>
	    /// <param name="dispatcher"></param>
	    /// <param name="applicationView"></param>
	    public ApplicationDetailsViewModel(ApplicationId applicationId, IClientEndpoint clientEndpoint, Dispatcher dispatcher, ApplicationView applicationView)
			: this(clientEndpoint, dispatcher, new CropYearId(applicationView.Id.DataSourceId, applicationView.CropYear), applicationView.StartDateTime, applicationView.Products, applicationView.CropZones, applicationView.Sources)
        {
            ApplicationId = applicationId;
		    EnvironmentConditions = new EnvironmentConditions();
            this.applicationView = applicationView;
			EditDocumentsCommand = new RelayCommand(EditApplicationDocuments);

			TankDetails = applicationView.TankInformation;
			Name = applicationView.Name;
			Notes = applicationView.Notes;
			TimeDisplay = CalculateTimeDisplay(applicationView.StartDateTime.ToLocalTime(), applicationView.Duration, applicationView.CustomEndDate);

			if (applicationView.Conditions != null) {
				EnvironmentConditions.Temperature = applicationView.Conditions.TemperatureValue;
			    EnvironmentConditions.WindSpeed = applicationView.Conditions.WindSpeedValue.GetValueOrDefault();
			}

			Authorizer = applicationView.Authorization != null ? applicationView.Authorization.PersonName : string.Empty;
			AuthorizeDate = applicationView.Authorization != null ? applicationView.Authorization.AuthorizationDate : new DateTime();
			Applicators = applicationView.Applicators;
			SetProductStrategyString(applicationView.ProductStrategy);

            errorMessage = string.Empty;
            ValidationResult vl = ValidateCropZones(applicationView.CropZones);
            if(vl == null) {
                ErrorMessage = string.Empty;
            } else {
                ErrorMessage = vl.ErrorMessage;
            }
        }

		public ApplicationDetailsViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, WorkOrderView workOrderView)
			: this(clientEndpoint, dispatcher, new CropYearId(workOrderView.Id.DataSourceId, workOrderView.CropYear), workOrderView.StartDateTime, workOrderView.Products, workOrderView.CropZones, workOrderView.Sources) {
		    EnvironmentConditions = new EnvironmentConditions();
            this.workOrderView = workOrderView;
			EditDocumentsCommand = new RelayCommand(EditWorkOrderDocuments);

			Name = workOrderView.Name;
			Notes = workOrderView.Notes;
			TankDetails = workOrderView.TankInformation;
			TimeDisplay = workOrderView.StartDateTime.ToShortDateString();
			//Temperature = workOrderView.Conditions.TemperatureValue.ToString() + "°";
			//WindSpeed = workOrderView..Conditions.WindSpeedValue + workOrderView.Conditions.WindSpeedUnit + " " + workOrderView.Conditions.WindDirection;
			Authorizer = workOrderView.Authorization != null ? workOrderView.Authorization.PersonName : string.Empty;
			AuthorizeDate = workOrderView.Authorization != null ? workOrderView.Authorization.AuthorizationDate : new DateTime();
			Applicators = workOrderView.Applicators.ToList();
			SetProductStrategyString(workOrderView.ApplicationStrategy);
            errorMessage = string.Empty;
            ValidationResult vl = ValidateCropZones(workOrderView.CropZones);
            if (vl == null) {
                ErrorMessage = string.Empty;
            }
            else {
                ErrorMessage = vl.ErrorMessage;
            }
        }

        public ApplicationId ApplicationId { get; }
        public ICommand ChangeApplicationTimeCommand { get; private set; }
		public ICommand ChangeFieldSelectionCommand { get; private set; }
		public ICommand ChangeTankCommand { get; private set; }
		public ICommand EditWeatherInfo { get; private set; }
		public ICommand EditAuthorizerInfo { get; private set; }
		public ICommand EditApplicatorInfo { get; private set; }
		public ICommand EditNotesInfo { get; private set; }
		public ICommand NameInfo { get; private set; }
		public ICommand AddProduct { get; private set; }
		public ICommand EditDocumentsCommand { get; private set; }

		public override string Notes { get; set; }

	    public EnvironmentConditions EnvironmentConditions { get; set; }
        private ObservableCollection<ApplicationProductDetails> _products { get; set; }
		public override ObservableCollection<ApplicationProductDetails> Products { get { return _products; } set { _products = value; RaisePropertyChanged(() => Products); } }
		public override ObservableCollection<CropZoneDetails> CropZones { get; set; }
		public Measure TotalArea { get; set; }
		public List<DocumentDescriptor> Sources { get; set; }
		public ApplicationTankInformation TankDetails { get; set; }
		public override string ApplicationType { get; set; }
		public Dictionary<ContractId, RentContractDetailsView> Contracts { get; private set; }
        public override decimal TotalAvgCost { get; set; }
        public override decimal TotalInvoiceCost { get; set; }
	    public override decimal TotalSpecificCost { get; set; }

	    public override DataTemplate ApplicationDataTemplate
	    {
	        get
	        {
	            ResourceDictionary applicationResourceDictionary = new ResourceDictionary
	            {
	                Source = new Uri("/Landdb;component/Views/Templates/ApplicationViewTemplate.xaml", UriKind.RelativeOrAbsolute)
	            };

	            return applicationResourceDictionary["ApplicationViewTemplate"] as DataTemplate;
            }
	    }

		private string name;
		public override string Name {
			get { return name; }
			set {
				name = value;
				RaisePropertyChanged(() => Name);
			}
		}

		private string startDate;
		public override string StartDateTime {
			get { return startDate; }
			set {
				startDate = value;
				RaisePropertyChanged(() => StartDateTime);
			}
		}

		private string authorizer;
		public string Authorizer {
			get { return authorizer; }
			set {
				authorizer = value;
				RaisePropertyChanged(() => Authorizer);
			}
		}

		private DateTime authDate;
		public DateTime AuthorizeDate {
			get { return authDate; }
			set {
				authDate = value;
				RaisePropertyChanged(() => AuthorizeDate);
				RaisePropertyChanged(() => AuthorizeDateDisplay);
			}
		}

		private List<ApplicationApplicator> applicators;
		public List<ApplicationApplicator> Applicators {
			get { return applicators; }
			set {
				applicators = value;
				RaisePropertyChanged(() => Applicators);
			}
		}

		public string AuthorizeDateDisplay {
			get { return string.Format(@"{0}", AuthorizeDate != new DateTime() ? AuthorizeDate.ToShortDateString() : string.Empty); }
		}

		public string TankDisplay {
			get {
				if (TankDetails == null) {
					return string.Empty;
				} else {
					return !string.IsNullOrEmpty(TankDetails.TankSizeUnit) ? string.Format(@"{0} {1}", TankDetails.TankSizeValue.ToString("N2"), UnitFactory.GetUnitByName(TankDetails.TankSizeUnit).AbbreviatedDisplay) : string.Empty;
				}
			}
		}
		public string CarrierPerAreaDisplay {
			get {
				if (TankDetails == null || string.IsNullOrWhiteSpace(TankDetails.CarrierPerAreaUnit)) {
					return string.Empty;
				} else {
					return string.Format(@"{0} {1}", TankDetails.CarrierPerAreaValue.ToString("N2"), UnitFactory.GetUnitByName(TankDetails.CarrierPerAreaUnit).AbbreviatedDisplay);
				}
			}
		}

		public ApplicationProductDetails SelectedProduct {
			get { return selectedProduct; }
			set {
				selectedProduct = value;
				if (selectedProduct != null) { AppliedProductEdit(selectedProduct); }
			}
		}

		private bool showSpecificCost;
		public bool ShowSpecificCost {
			get { return showSpecificCost; }
			set {
				showSpecificCost = value;
				RaisePropertyChanged(() => ShowSpecificCost);
			}
		}

        public bool ShowInvoiceCost { get; set; }

		private string timeDisplay;
		public override string TimeDisplay {
			get { return timeDisplay; }
			set {
				timeDisplay = value;
				RaisePropertyChanged(() => TimeDisplay);
			}
		}

        public string ErrorMessage {
            get { return errorMessage; }
            set {
                errorMessage = value;
                RaisePropertyChanged("ErrorMessage");
                RaisePropertyChanged("IsErrorMessageVisible");
            }
        }

        public bool IsErrorMessageVisible {
            get {
                //return !string.IsNullOrWhiteSpace(ErrorMessage);
                return !string.IsNullOrEmpty(ErrorMessage);
            }
        }

        public ValidationResult ValidateCropZones(List<IncludedCropZone> CropZoneList) { 
            ValidationResult result = null;
            List<CropZoneRentContract> associatedContracts = new List<CropZoneRentContract>();
            foreach (var cz in CropZoneList) {
                var contractYears = clientEndpoint.GetView<CropZoneRentContractView>(currentCropYearId).GetValue(new CropZoneRentContractView());
                if (contractYears.CropZoneRentContracts.ContainsKey(cz.Id.Id)) {
                    var czrc = contractYears.CropZoneRentContracts[cz.Id.Id];
                    if (czrc != null && czrc.RentContractId != null) {
                        associatedContracts.Add(czrc);
                    }
                    else {
                        associatedContracts.Add(new CropZoneRentContract() { CropZoneId = cz.Id });
                    }
                }
                else {
                    associatedContracts.Add(new CropZoneRentContract() { CropZoneId = cz.Id });
                }
            }

            var groupedContracts = from c in associatedContracts
                                   group c by new { c.GrowerCropProtShare, c.GrowerFertShare, c.GrowerServiceShare, c.GrowerSeedShare } into g
                                   select g.Key;

            bool areinputsharesequal = groupedContracts.Count() <= 1;

            if (!CropZoneList.Any()) {
                result = new ValidationResult(Strings.AtLeastOneCropZoneIsRequired_Text);
            }
            else if (CropZoneList.Any(x => x.Area.Value == 0)) {
                result = new ValidationResult(Strings.AtLeastOneSelectedCropZoneHasNoArea_Text);
            }
            else if (!areinputsharesequal) {
                result = new ValidationResult(Strings.WarningMultipleContractsSelected_Text);
            }

            return result;
        }

		void OnChangeApplicationTime() {
			Popups.ChangeTimeViewModel vm = null;

			if (applicationView != null) {
				vm = new Popups.ChangeTimeViewModel(applicationView.StartDateTime.ToLocalTime(), applicationView.Duration.GetValueOrDefault(), applicationView.CustomEndDate.GetValueOrDefault(), applicationView, clientEndpoint, dispatcher, this) {
					AvailableTimingEventTags = GetAvailableApplicationTimingEventTags(clientEndpoint)
				};
			} else if (workOrderView != null) {
				vm = new Popups.ChangeTimeViewModel(workOrderView.StartDateTime.ToLocalTime(), workOrderView, clientEndpoint, dispatcher, this) {
					AvailableTimingEventTags = GetAvailableWorkOrderTimingEventTags(clientEndpoint)
				};
			}

			Messenger.Default.Send(new ShowPopupMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Production.Popups.ChangeTimeView), "changeAppTime", vm)
			});
		}

		void ChangeFieldSelection() {
			Popups.ChangeCropZoneSelectionViewModel vm = null;

			if (applicationView != null) {
				vm = new Popups.ChangeCropZoneSelectionViewModel(clientEndpoint, dispatcher, currentCropYearId, applicationView.CropZones, null, true, onFieldsChanged);
			} else if (workOrderView != null) {
				vm = new Popups.ChangeCropZoneSelectionViewModel(clientEndpoint, dispatcher, currentCropYearId, workOrderView.CropZones, null, true, onFieldsChanged);
			}

			if (vm != null) {
				Messenger.Default.Send(new ShowOverlayMessage() {
					ScreenDescriptor = new ScreenDescriptor(typeof(Views.Shared.Popups.ChangeCropZoneSelectionView), "changeCropzones", vm)
				});
			}
		}

		private void onFieldsChanged(ChangeCropZoneSelectionViewModel vm) {
			if (vm != null) {
				IDomainCommand cmd = null;

				if (applicationView != null && (vm.AddedCropZones.Any() || vm.RemovedCropZones.Any() || vm.ChangedCropZones.Any())) {
					cmd = new ChangeCropZonesIncludedInApplication(applicationView.Id, clientEndpoint.GenerateNewMetadata(), vm.AddedCropZones.ToArray(), vm.RemovedCropZones.ToArray(), vm.ChangedCropZones.ToArray());
				} else if (workOrderView != null && (vm.AddedCropZones.Any() || vm.RemovedCropZones.Any() || vm.ChangedCropZones.Any())) {
					cmd = new ChangeCropZonesIncludedInWorkOrder(workOrderView.Id, clientEndpoint.GenerateNewMetadata(), vm.AddedCropZones.ToArray(), vm.RemovedCropZones.ToArray(), vm.ChangedCropZones.ToArray());
				}

				if (cmd != null) { clientEndpoint.SendOne(cmd); }

				ChangeInFieldSelection(vm.AddedCropZones, vm.ChangedCropZones, vm.RemovedCropZones);
			}

			Messenger.Default.Send<HideOverlayMessage>(new HideOverlayMessage());
		}

		void ChangeTankSelection() {
			Popups.ChangeTankInfo vm = null;

			if (applicationView != null) {
				vm = new Popups.ChangeTankInfo(clientEndpoint, applicationView, TotalArea);
			} else if (workOrderView != null) {
				vm = new Popups.ChangeTankInfo(clientEndpoint, workOrderView, TotalArea);
			}

			if (vm != null) {
				Messenger.Default.Send(new ShowPopupMessage() {
					ScreenDescriptor = new ScreenDescriptor(typeof(Views.Production.Popups.ChangeTankInfoView), "changeTankInfo", vm)
				});
			}
		}

		void AppliedProductEdit(ApplicationProductDetails prod) {
			object vm = null;

			if (applicationView != null) {
				vm = new EditAppliedProductViewModel(clientEndpoint, this.dispatcher, applicationView, this, prod);
			} else if (workOrderView != null) {
				vm = new EditWorkOrderProductViewModel(clientEndpoint, this.dispatcher, workOrderView, this, prod);
			}

			if (vm != null) {
				Messenger.Default.Send(new ShowOverlayMessage() {
					ScreenDescriptor = new ScreenDescriptor(typeof(Views.Production.Popups.EditAppliedProductView), "changeAppliedProduct", vm)
				});
			}
		}

		void AddAppliedProduct() {
			object vm = null;

			if (applicationView != null) {
				vm = new EditAppliedProductViewModel(clientEndpoint, this.dispatcher, applicationView, this, null);
			} else if (workOrderView != null) {
				vm = new EditWorkOrderProductViewModel(clientEndpoint, this.dispatcher, workOrderView, this, null);
			}

			if (vm != null) {
				Messenger.Default.Send(new ShowOverlayMessage() {
					ScreenDescriptor = new ScreenDescriptor(typeof(Views.Production.Popups.EditAppliedProductView), "changeAppliedProduct", vm)
				});
			}
		}

		void WeatherInfoEdit() {
			if (applicationView != null) {
				ChangeWeatherInfo vm = new ChangeWeatherInfo(clientEndpoint, dispatcher, applicationView);
				Messenger.Default.Send(new ShowPopupMessage() {
					ScreenDescriptor = new ScreenDescriptor(typeof(Views.Production.Popups.ChangeWeatherInfoView), "changeWeather", vm)
				});
			}
		}


		void AuthorizerInfoEdit() {
			ChangeAuthorizerInfo vm = null;

			if (applicationView != null) {
				vm = new ChangeAuthorizerInfo(clientEndpoint, dispatcher, applicationView, this);
			} else if (workOrderView != null) {
				vm = new ChangeAuthorizerInfo(clientEndpoint, dispatcher, workOrderView, this);
			}

			if (vm != null) {
				Messenger.Default.Send(new ShowPopupMessage() {
					ScreenDescriptor = new ScreenDescriptor(typeof(Views.Production.Popups.ChangeAuthorizerInfoView), "changeAuthorizer", vm)
				});
			}
		}

		void ApplicatorInfoEdit() {
			ChangeApplicatorInfo vm = null;

			if (applicationView != null) {
				ObservableCollection<ApplicationApplicator> applicators = new ObservableCollection<ApplicationApplicator>(applicationView.Applicators);
				vm = new ChangeApplicatorInfo(clientEndpoint, dispatcher, applicators, applicationView, this);
			} else if (workOrderView != null) {
				ObservableCollection<ApplicationApplicator> applicators = new ObservableCollection<ApplicationApplicator>(workOrderView.Applicators);
				vm = new ChangeApplicatorInfo(clientEndpoint, dispatcher, applicators, workOrderView, this);
			}

			if (vm != null) {
				Messenger.Default.Send(new ShowPopupMessage() {
					ScreenDescriptor = new ScreenDescriptor(typeof(Views.Secondary.PopUp.IncludeApplicatorView), "changeApplicator", vm)
				});
			}
		}

		void NotesInfoEdit() {
			if (applicationView != null || workOrderView != null) {
				DialogFactory.ShowNotesChangeDialog(Notes, newNotes => {
					IDomainCommand cmd = null;

					if (applicationView != null) {
						cmd = new ChangeApplicationNotes(applicationView.Id, clientEndpoint.GenerateNewMetadata(), newNotes);
					} else if (workOrderView != null) {
						cmd = new ChangeWorkOrderNotes(workOrderView.Id, clientEndpoint.GenerateNewMetadata(), newNotes);
					}

					if (cmd != null) { clientEndpoint.SendOne(cmd); }

					Notes = newNotes;
					RaisePropertyChanged(() => Notes);
				});
			}
		}

		void NameInfoEdit() {
			string entityDescription = null;

			if (applicationView != null) {
				entityDescription = "Application Name";
			} else if (workOrderView != null) {
				entityDescription = "Work Order Name";
			}

			DialogFactory.ShowNameChangeDialog(entityDescription, Name, newName => {
				IDomainCommand cmd = null;

				if (applicationView != null) {
					cmd = new RenameApplication(applicationView.Id, clientEndpoint.GenerateNewMetadata(), newName);
				} else if (workOrderView != null) {
					cmd = new RenameWorkOrder(workOrderView.Id, clientEndpoint.GenerateNewMetadata(), newName);
				}

				if (cmd != null) {
					clientEndpoint.SendOne(cmd);
					Name = newName;
				}
			});
		}

		public void AddToProducts(ApplicationProductDetails product) {
			Products.Add(product);
			RaisePropertyChanged(() => Products);
		}

		public void ChangeInFieldSelection(IEnumerable<CropZoneArea> added, IEnumerable<CropZoneArea> changed, IEnumerable<CropZoneId> removed) {
			foreach (var area in added) {
				var czDetailsView = clientEndpoint.GetView<CropZoneDetailsView>(area.CropZoneId).GetValue(() => null);
				var czName = czDetailsView != null ? czDetailsView.Name : Strings.UnknownCropZone_Text;
				var details = new CropZoneDetails() {
					Id = area.CropZoneId,
					Area = new AreaMeasure(area.AreaValue, (AreaUnit)UnitFactory.GetUnitByName(area.AreaUnit)),
					Name = czName
				};

				CropZones.Add(details);
			}

			foreach (var area in changed) {
				var cz = CropZones.FirstOrDefault(x => x.Id == area.CropZoneId);
				cz.Area = new AreaMeasure(UnitFactory.GetUnitByName(area.AreaUnit).GetMeasure(area.AreaValue).Value, (AreaUnit)UnitFactory.GetUnitByName(area.AreaUnit));
			}

			foreach (var id in removed) {
				CropZones.Remove(CropZones.Where(x => x.Id == id).FirstOrDefault());
			}

			TotalArea = CropZones[0].Area.Unit.GetMeasure(CropZones.Sum(x => x.Area.Value));

			RaisePropertyChanged(() => CropZones);
			RaisePropertyChanged(() => TotalArea);

			if (applicationView != null) {
				var tankVM = new ChangeTankInfo(clientEndpoint, applicationView, TotalArea);
				if (tankVM.CarrierApplied.Value > 0 || tankVM.CarrierPerArea.Value > 0 || tankVM.TankCount > 0 || tankVM.TankSize.Value > 0) {
					tankVM.UpdateTank();
					TankDetails = new ApplicationTankInformation((decimal)tankVM.TankSize.Value, tankVM.TankSize.Unit.Name, (decimal)tankVM.TankCount, (decimal)tankVM.CarrierPerArea.Value, tankVM.CarrierPerArea.Unit.Name, (decimal)tankVM.CarrierApplied.Value, tankVM.CarrierApplied.Unit.Name, applicationView.TankInformation != null ? applicationView.TankInformation.TankValueTypeEnteredByUser : string.Empty);
					RaisePropertyChanged(() => TankDetails);
				}
			} else {
				var tankVM = new ChangeTankInfo(clientEndpoint, workOrderView, TotalArea);
				if (tankVM.CarrierApplied.Value > 0 || tankVM.CarrierPerArea.Value > 0 || tankVM.TankCount > 0 || tankVM.TankSize.Value > 0) {
					tankVM.UpdateTank();
					TankDetails = new ApplicationTankInformation((decimal)tankVM.TankSize.Value, tankVM.TankSize.Unit.Name, (decimal)tankVM.TankCount, (decimal)tankVM.CarrierPerArea.Value, tankVM.CarrierPerArea.Unit.Name, (decimal)tankVM.CarrierApplied.Value, tankVM.CarrierApplied.Unit.Name, TankDetails.TankValueTypeEnteredByUser);
					RaisePropertyChanged(() => TankDetails);
				}
			}

			foreach (var prod in Products) {
				var mlp = masterlist.GetProduct(prod.ID);

                //Re-Calc AssociatedProducts
                if (prod.AssociatedProducts != null)
                {
                    foreach (var associate in prod.AssociatedProducts)
                    {
                        var associateProd = clientEndpoint.GetMasterlistService().GetProduct(associate.ProductID);
                        var ratePerArea = associate.RatePerAreaValue;
                        var ratePerAreaMeasure = UnitFactory.GetPackageSafeUnit(associate.RatePerAreaUnit.Name, associateProd.StdUnit, associateProd.StdFactor, associateProd.StdPackageUnit).GetMeasure((double)ratePerArea, associateProd.Density);
                        var totalValue = (double)ratePerArea * TotalArea.Value;
                        var totalMeasure = UnitFactory.GetPackageSafeUnit(associate.RatePerAreaUnit.Name, associateProd.StdUnit, associateProd.StdFactor, associateProd.StdPackageUnit).GetMeasure(totalValue, associateProd.Density);
                        var converted = totalMeasure.Unit != associate.TotalProductUnit && totalMeasure.CanConvertTo(associate.TotalProductUnit) ? totalMeasure.GetValueAs(associate.TotalProductUnit) : totalMeasure.Value;
                        associate.TotalProductValue = (decimal)converted;
                    }
                }

                /////////////////////////////

                //REFRESH PRODUCTS DUE TO CHANGE IN FIELDS
                if (applicationView != null) {
					switch (applicationView.ProductStrategy) {
						case ProductApplicationStrategy.ByRatePerArea:
							var totalProductValue = prod.RateMeasure.Unit.GetMeasure(prod.RateMeasure.Value * TotalArea.Value, mlp.Density).GetValueAs(prod.TotalProduct.Unit);
							prod.TotalProduct = prod.TotalProduct.Unit.GetMeasure(totalProductValue, mlp.Density);
							prod.TotalCost = prod.CostUnit != null ? (decimal)prod.TotalProduct.GetValueAs(prod.CostUnit) * prod.CostPerUnitValue : 0m;
							prod.AreaValue = (decimal)TotalArea.Value;
							prod.Split = (totalProductValue * (double)prod.GrowerShare).ToString("N2") + " / " + (totalProductValue - (totalProductValue * (double)prod.GrowerShare)).ToString("N2");
							break;
						case ProductApplicationStrategy.ByRatePerTank:
							var TotalTanks = applicationView.TankInformation.CarrierPerAreaValue * (decimal)TotalArea.Value / applicationView.TankInformation.TankSizeValue;
							totalProductValue = prod.RatePerTank.Unit.GetMeasure(prod.RatePerTank.Value * (double)TotalTanks).GetValueAs(prod.TotalProduct.Unit);
							prod.TotalProduct = prod.TotalProduct.Unit.GetMeasure(totalProductValue, mlp.Density);
							prod.RateMeasure = prod.RateMeasure.Unit.GetMeasure(prod.TotalProduct.GetValueAs(prod.RateMeasure.Unit) / TotalArea.Value);
							prod.TotalCost = prod.CostUnit != null ? (decimal)prod.TotalProduct.GetValueAs(prod.CostUnit) * prod.CostPerUnitValue : 0m;
							prod.AreaValue = (decimal)TotalArea.Value;
							prod.Split = (totalProductValue * (double)prod.GrowerShare).ToString("N2") + " / " + (totalProductValue - (totalProductValue * (double)prod.GrowerShare)).ToString("N2");
							break;
						case ProductApplicationStrategy.ByTotalProduct:
							prod.RateMeasure = prod.RateMeasure.Unit.GetMeasure(prod.TotalProduct.GetValueAs(prod.RateMeasure.Unit) / TotalArea.Value, mlp.Density);
							prod.AreaValue = (decimal)TotalArea.Value;
							break;
					}

					var tankVM = new ChangeTankInfo(clientEndpoint, applicationView, TotalArea);
					tankVM.UpdateTank();
					TankDetails = new ApplicationTankInformation((decimal)tankVM.TankSize.Value, tankVM.TankSize.Unit.Name, (decimal)tankVM.TankCount, (decimal)tankVM.CarrierPerArea.Value, tankVM.CarrierPerArea.Unit.Name, (decimal)tankVM.CarrierApplied.Value, tankVM.CarrierApplied.Unit.Name, TankDetails != null ? TankDetails.TankValueTypeEnteredByUser : string.Empty);
					RaisePropertyChanged(() => TankDetails);
				} else if (workOrderView != null) {
					var density = masterlist.GetProduct(prod.ID).Density;

					switch (workOrderView.ApplicationStrategy) {
						case ProductApplicationStrategy.ByRatePerArea:

							var newProdTotalMeasure = prod.TotalProduct.Unit.GetMeasure(prod.TotalProduct.Value, density);
							var totalProductValue = prod.RateMeasure.Unit.GetMeasure(prod.RateMeasure.Value * TotalArea.Value, density).GetValueAs(prod.TotalProduct.Unit);
							prod.TotalProduct = prod.TotalProduct.Unit.GetMeasure(totalProductValue);
							prod.TotalCost = prod.CostUnit != null ? (decimal)newProdTotalMeasure.GetValueAs(prod.CostUnit) * prod.CostPerUnitValue : 0m;
							prod.AreaValue = (decimal)TotalArea.Value;
							prod.Split = (totalProductValue * (double)prod.GrowerShare).ToString("N2") + " / " + (totalProductValue - (totalProductValue * (double)prod.GrowerShare)).ToString("N2");
							break;
						case ProductApplicationStrategy.ByRatePerTank:
							var ratePerTankMeasure = prod.RatePerTank.Unit.GetMeasure(prod.RatePerTank.Value, density);
							var TotalTanks = workOrderView.TankInformation.CarrierPerAreaValue * (decimal)TotalArea.Value / workOrderView.TankInformation.TankSizeValue;
							totalProductValue = ratePerTankMeasure.Unit.GetMeasure(ratePerTankMeasure.Value * (double)TotalTanks, density).GetValueAs(prod.TotalProduct.Unit); // prod.RatePerTank.Unit.GetMeasure(prod.RatePerTank.Value * (double)TotalTanks).GetValueAs(prod.TotalProduct.Unit);
							prod.TotalProduct = prod.TotalProduct.Unit.GetMeasure(totalProductValue, density);
							prod.RateMeasure = prod.RateMeasure.Unit.GetMeasure(prod.TotalProduct.GetValueAs(prod.RateMeasure.Unit) / TotalArea.Value);
							prod.TotalCost = prod.CostUnit != null ? (decimal)prod.TotalProduct.Unit.GetMeasure(prod.TotalProduct.Value, density).GetValueAs(prod.CostUnit) * prod.CostPerUnitValue : 0m;
							prod.AreaValue = (decimal)TotalArea.Value;
							prod.Split = (totalProductValue * (double)prod.GrowerShare).ToString("N2") + " / " + (totalProductValue - (totalProductValue * (double)prod.GrowerShare)).ToString("N2");
							break;
						case ProductApplicationStrategy.ByTotalProduct:
							var convertedRateValue = prod.TotalProduct.CanConvertTo(prod.RateMeasure.Unit) ? prod.TotalProduct.GetValueAs(prod.RateMeasure.Unit) / TotalArea.Value : prod.TotalProduct.Value / TotalArea.Value;
							var rateMeasure = prod.TotalProduct.CanConvertTo(prod.RateMeasure.Unit) ? prod.RateMeasure.Unit : prod.TotalProduct.Unit;
							prod.RateMeasure = rateMeasure.GetMeasure(convertedRateValue);
							prod.AreaValue = (decimal)TotalArea.Value;
							break;
					}

					var tankVM = new ChangeTankInfo(clientEndpoint, workOrderView, TotalArea);
					tankVM.UpdateTank();
					TankDetails = new ApplicationTankInformation((decimal)tankVM.TankSize.Value, tankVM.TankSize.Unit.Name, (decimal)tankVM.TankCount, (decimal)tankVM.CarrierPerArea.Value, tankVM.CarrierPerArea.Unit.Name, (decimal)tankVM.CarrierApplied.Value, tankVM.CarrierApplied.Unit.Name, TankDetails.TankValueTypeEnteredByUser);
					RaisePropertyChanged(() => TankDetails);
				}
			}

			RaisePropertyChanged(() => Products);
		}

		void SetProductStrategyString(ProductApplicationStrategy strat) {
			if (applicationView != null) {
				switch (strat) {
					case ProductApplicationStrategy.ByRatePerArea:
						ApplicationType = Strings.ByRateApplication_Text;
						break;
					case ProductApplicationStrategy.ByRatePerTank:
						ApplicationType = Strings.ByTankApplication_Text;
						break;
					case ProductApplicationStrategy.ByTotalProduct:
						ApplicationType = Strings.ByTotalApplication_Text;
						break;
				}
			} else if (workOrderView != null) {
				switch (strat) {
					case ProductApplicationStrategy.ByRatePerArea:
						ApplicationType = Strings.ByRateWorkOrder_Text;
						break;
					case ProductApplicationStrategy.ByRatePerTank:
						ApplicationType = Strings.ByTankWorkOrder_Text;
						break;
					case ProductApplicationStrategy.ByTotalProduct:
						ApplicationType = Strings.ByTotalWorkOrder_Text;
						break;
				}
			}
		}

		void EditApplicationDocuments() {
			EditDocuments_Generic(
				doc => { return new AddDocumentToApplication(applicationView.Id, clientEndpoint.GenerateNewMetadata(), doc); },
				doc => { return new RemoveDocumentFromApplication(applicationView.Id, clientEndpoint.GenerateNewMetadata(), doc); });
		}

		void EditWorkOrderDocuments() {
			EditDocuments_Generic(
				doc => { return new AddDocumentToWorkOrder(workOrderView.Id, clientEndpoint.GenerateNewMetadata(), doc); },
				doc => { return new RemoveDocumentFromWorkOrder(workOrderView.Id, clientEndpoint.GenerateNewMetadata(), doc); });
		}

		void EditDocuments_Generic(Func<DocumentDescriptor, IDomainCommand> addCommandFactory, Func<DocumentDescriptor, IDomainCommand> removeCommandFactory) {
			var vmBase = new Shared.DocumentListPageViewModel(clientEndpoint, dispatcher, currentCropYearId.Id, Sources);
			var vm = new Overlays.DocumentListOverlayViewModel(vmBase, shouldSave => {
				if (shouldSave) {
					foreach (var added in vmBase.AddedDocuments) {
						Sources.Add(added);
						var addCmd = addCommandFactory(added);
						clientEndpoint.SendOne(addCmd);
					}

					foreach (var removed in vmBase.RemovedDocuments) {
						Sources.RemoveAll(x => x.Identity == removed.Identity);
						var remCmd = removeCommandFactory(removed);
						clientEndpoint.SendOne(remCmd);
					}

					RaisePropertyChanged(() => Sources);
				}

				Messenger.Default.Send(new HideOverlayMessage());
			});

			Messenger.Default.Send(new ShowOverlayMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Overlays.DocumentListOverlayView), "editdocuments", vm)
			});
		}

		private Dictionary<string, List<string>> GetAvailableWorkOrderTimingEventTags(IClientEndpoint clientEndpoint) {
			Dictionary<string, List<string>> availableTimingEventTagz = new Dictionary<string, List<string>>();
			var woTagView = clientEndpoint.GetView<WorkOrderTagProjectionView>(currentCropYearId).GetValue(() => null);
			if (woTagView.TagInfo != null) {
				foreach (var v in woTagView.TagInfo) {
					if (string.IsNullOrEmpty(v.TimingEvent)) { continue; }

					if (!availableTimingEventTagz.ContainsKey(v.TimingEvent)) {
						availableTimingEventTagz.Add(v.TimingEvent, new List<string>());
					}

					foreach (var x in v.Tags) {
						if (availableTimingEventTagz.ContainsKey(v.TimingEvent)) {
							if (!string.IsNullOrEmpty(x.Key) && !availableTimingEventTagz[v.TimingEvent].Contains(x.Key)) {
								availableTimingEventTagz[v.TimingEvent].Add(x.Key);
							}
						}
					}
				}
			}

			return availableTimingEventTagz;
		}

		private Dictionary<string, List<string>> GetAvailableApplicationTimingEventTags(IClientEndpoint clientEndpoint) {
			Dictionary<string, List<string>> availableTimingEventTagz = new Dictionary<string, List<string>>();
			var appTagView = clientEndpoint.GetView<ApplicationTagProjectionView>(currentCropYearId).GetValue(() => null);

			if (appTagView.TagInfo != null) {
				foreach (var v in appTagView.TagInfo) {
					if (string.IsNullOrEmpty(v.TimingEvent)) { continue; }

					if (!availableTimingEventTagz.ContainsKey(v.TimingEvent)) {
						availableTimingEventTagz.Add(v.TimingEvent, new List<string>());
					}

					foreach (var x in v.Tags) {
						if (availableTimingEventTagz.ContainsKey(v.TimingEvent)) {
							if (!string.IsNullOrEmpty(x.Key) && !availableTimingEventTagz[v.TimingEvent].Contains(x.Key)) {
								availableTimingEventTagz[v.TimingEvent].Add(x.Key);
							}
						}
					}
				}
			}

			return availableTimingEventTagz;
		}

        public ICommand PrintCommand => new RelayCommand(CreateReport);

	    private void CreateReport()
	    {
	        Messenger.Default.Send(new ShowOverlayMessage
	        {
	            ScreenDescriptor = new ScreenDescriptor(typeof(SingleWorkOrderView), 
                    "viewReport", 
                    new SingleApplicationViewModel(clientEndpoint, ApplicationId)
                )
            });
	    }

	    public ICommand ShareFromSelectedCommand => new RelayCommand(ShareFromSelectedToTheWeb);

        private void ShareFromSelectedToTheWeb()
	    {
	        Maybe<ApplicationView> possibleApplication = clientEndpoint.GetView<ApplicationView>(ApplicationId);

	        if (possibleApplication.HasValue)
	        {
	            ChoosePersonToShareViewModel personToShareViewModel = new ChoosePersonToShareViewModel(clientEndpoint,
	                dispatcher, possibleApplication.Value, onPersonShared);
	            Messenger.Default.Send(new ShowOverlayMessage
	            {
	                ScreenDescriptor = new ScreenDescriptor(typeof(Views.Production.Popups.ChoosePersonToShareView), "changeCropZones", personToShareViewModel)
	            });
	        }
	    }

	    private void onPersonShared(ChoosePersonToShareViewModel viewModel)
	    {
	        if (viewModel != null)
	        {
	            var shareModel = new { ShareePersonId = viewModel.Person.Id.Id, Id = ApplicationId };

	            ShowIntegration(shareModel, "api/applications/share");
	        }
	        Messenger.Default.Send(new HideOverlayMessage());
        }

        private void ShowIntegration(object shareModel, string uriString)
	    {
	        Task.Factory.StartNew(() => {
	            DataSourceId dataSource = new DataSourceId(ApplicationEnvironment.CurrentDataSourceId);

	            if (NetworkStatus.IsInternetAvailable())
	            {
	                string baseDocumentUri =
	                    $"{RemoteConfigurationSettings.GetBaseUri()}{uriString}?datasourceid={dataSource.Id}";
	                HttpClient client = new HttpClient
	                {
	                    BaseAddress = new Uri(baseDocumentUri),
	                };

	                client.DefaultRequestHeaders.Add("Accept", "application / json");
	                Task<string> holder = PostData<string>(baseDocumentUri, client, shareModel);
	                holder.Wait();

	                return holder.Result;
	            }
	            else
	            {
	                return string.Empty;
	            }
	        });
	    }

	    private static async Task<T> PostData<T>(string baseDocumentUri, HttpClient client, object shareModel)
	    {
	        try
	        {
	            await new CloudClientFactory().ResolveStorageCredentials().SetBearerTokenAsync(client);

	            HttpResponseMessage response = await client.PostAsJsonAsync(baseDocumentUri, shareModel);

	            if (response.StatusCode == HttpStatusCode.OK)
	            {
	                return JsonConvert.DeserializeObject<T>(string.Empty);
	            }
	            else
	            {
	                return JsonConvert.DeserializeObject<T>(string.Empty);
	            }
	        }
	        catch (Exception)
	        {
	            throw new ApplicationException("Share Failed.");
	        }
	    }

	    public ICommand CopyApplication => new RelayCommand(CreateApplicationFromSelected);

	    void CreateApplicationFromSelected()
	    {
	        if (ApplicationId != null)
	        {
	            var applicationId = new ApplicationId(ApplicationEnvironment.CurrentDataSourceId, Guid.NewGuid());

	            DocumentDescriptor sourceDoc = new DocumentDescriptor()
	            {
	                DocumentType = DocumentDescriptor.ApplicationTypeString,
	                Identity = ApplicationId,
	                Name = Name
	            };

	            var vm = new ApplicatorViewModel(clientEndpoint, dispatcher, applicationId, ApplicationEnvironment.CurrentCropYear, null, sourceDoc);
	            Messenger.Default.Send(new ChangeScreenDescriptorMessage()
	            {
	                ScreenDescriptor = new ScreenDescriptor(typeof(Views.Secondary.Application.ApplicatorView), "create application", vm)
	            });
	        }
	    }

        public ICommand SetCharmCommand { get; set; }

        public ICommand SetCharmCommandAndClosePopUp => new RelayCommand<string>(charmName =>
        {
            SetCharmCommand.Execute(charmName);
            ClickSetCharmButton.Execute(null);
        });

	    public ICommand ClickSetCharmButton => new RelayCommand(() =>
	    {
	        IsCharmPopupOpen = !isCharmPopupOpen;
        });

        private bool isCharmPopupOpen;

	    public bool IsCharmPopupOpen
	    {
	        get => isCharmPopupOpen;
	        set
	        {
	            isCharmPopupOpen = value;
	            RaisePropertyChanged(() => IsCharmPopupOpen);
	        }
	    }
    } 
}