﻿using System.Windows;
using System.Windows.Controls;

namespace Landdb.ViewModel.Production.Applications.Convertors
{
    public class ApplicationDetailsTemplateSelector : DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            if (item is AbstractApplicationDetailsViewModel applicationDetailsViewModel)
            {
                return applicationDetailsViewModel.ApplicationDataTemplate;
            }
            else
            {
                return base.SelectTemplate(item, container);
            }
        }
    }
}