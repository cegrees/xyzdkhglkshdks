﻿using System.Windows;
using System.Windows.Controls;

namespace Landdb.ViewModel.Production.Applications.Convertors
{
    public class ApplicationListItemTemplateSelector : DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            if (item is AbstractApplicationListItemViewModel applicationListItemViewModel)
            {
                return applicationListItemViewModel.ListItemTemplate;
            }
            else
            {
                return base.SelectTemplate(item, container);
            }
        }
    }
}