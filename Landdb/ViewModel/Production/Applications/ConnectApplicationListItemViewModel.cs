﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Threading;
using Landdb.Client.Infrastructure;
using Landdb.Client.Models.Connect;

namespace Landdb.ViewModel.Production.Applications
{
    public class ConnectApplicationListItemViewModel : AbstractApplicationListItemViewModel
    {
        private readonly ConnectApplication connectApplication;

        public ConnectApplicationListItemViewModel(ConnectApplication connectApplication)
        {
            this.connectApplication = connectApplication;
        }

        public override ApplicationId Id => new ApplicationId(Guid.Parse(connectApplication.DatasourceId), connectApplication.ApplicationId.Id);
        public override string Name => connectApplication.Description;
        public override string CharmName { get; set; }
        public override ApplicationListItemType ListItemType => ApplicationListItemType.CONNECT;
        public override DateTime? StartDateTime => connectApplication.TimeScopes.TryGetValue("Start", out DateTime startDate) ? startDate : connectApplication.TimeScopes.ToList().First().Value;
        public override int FieldCount => connectApplication.Land.Count;
        public override decimal TotalCost { get; set; }

        public override int ProductCount
        {
            get
            {
                int result = 0;

                connectApplication.Products.ForEach(productUse =>
                {
                    if (productUse.IsTankMix)
                    {
                        int flattenedProductCount = TankMixParser.ParseATankMix(productUse).Count;
                        result += flattenedProductCount;
                    }
                    else
                    {
                        result++;
                    }
                });

                return result;
            }
        }

        public override DataTemplate ListItemTemplate
        {
            get
            {
                ResourceDictionary applicationResourceDictionary = new ResourceDictionary
                {
                    Source = new Uri("/Landdb;component/Views/Templates/ConnectApplicationListItemTemplate.xaml", UriKind.RelativeOrAbsolute)
                };

                return applicationResourceDictionary["ConnectApplicationListItemTemplate"] as DataTemplate;
            }
        }

        public override AbstractApplicationDetailsViewModel CreateDetailsModel(AbstractApplicationListItemViewModel selectedItem,
        IClientEndpoint clientEndPoint, Dispatcher dispatcher, Action<string> setCharm)
        {
            return new ConnectApplicationDetailsViewModel(clientEndPoint, dispatcher, connectApplication);
        }

        public override bool PerformItemRemoval(IClientEndpoint clientEndpoint, AbstractApplicationListItemViewModel selectedItem)
        {
            return clientEndpoint.GetConnectApplicationService().Remove(connectApplication).Result;
        }

        public override IDomainCommand CreateSetCharmCommand(IClientEndpoint clientEndpoint, AbstractApplicationListItemViewModel selectedItem, string charmName)
        {
            return null;
        }
    }
}