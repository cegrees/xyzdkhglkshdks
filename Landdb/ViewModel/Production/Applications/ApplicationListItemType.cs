﻿namespace Landdb.ViewModel.Production.Applications
{
    public enum ApplicationListItemType
    {
        CONNECT,
        LAND_DB,
        ALL_APPLICATIONS
    }
}