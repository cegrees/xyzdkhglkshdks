﻿using System;

namespace Landdb.ViewModel.Production.Applications
{
    public class ApplicationTypeSelector : IEquatable<ApplicationTypeSelector>
    {
        private const string ALL_APPLICATIONS = "All Applications";
        private const string PENDING_APPLICATION = "Pending Applicaitons";
        private const string NON_PENDING_APPLICATION = "Non-Pending Applications";

        public ApplicationTypeSelector(ApplicationListItemType applicationListItemType)
        {
            ApplicationListItemType = applicationListItemType;
            ApplicationTypeDisplayText = selectApplicationTypeDisplayText(applicationListItemType);
        }

        public ApplicationListItemType ApplicationListItemType { get; }
        public string ApplicationTypeDisplayText { get; }

        private static string selectApplicationTypeDisplayText(ApplicationListItemType applicationListItemType)
        {
            switch (applicationListItemType)
            {
                case ApplicationListItemType.CONNECT:
                    return PENDING_APPLICATION;
                case ApplicationListItemType.LAND_DB:
                    return NON_PENDING_APPLICATION;
                case ApplicationListItemType.ALL_APPLICATIONS:
                    return ALL_APPLICATIONS;
                default:
                    return string.Empty;
            }
        }

        public bool Equals(ApplicationTypeSelector other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return ApplicationListItemType == other.ApplicationListItemType && string.Equals(ApplicationTypeDisplayText, other.ApplicationTypeDisplayText);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((ApplicationTypeSelector) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((int) ApplicationListItemType * 397) ^ (ApplicationTypeDisplayText != null ? ApplicationTypeDisplayText.GetHashCode() : 0);
            }
        }
    }
}