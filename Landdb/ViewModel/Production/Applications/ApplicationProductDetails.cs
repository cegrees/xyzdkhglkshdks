﻿using System;
using System.Collections.ObjectModel;
using AgC.UnitConversion;
using GalaSoft.MvvmLight;
using Landdb.Resources;
using Landdb.ViewModel.Shared;

namespace Landdb.ViewModel.Production.Applications
{
    public class ApplicationProductDetails : ViewModelBase
    {
        Measure totalProduct;
        Measure rate;
        decimal area;
        decimal totalCost;
        decimal costPerUnit;
        decimal? totalSpecificCost;
        decimal? specificCostPerUnit;
        string split;

        public ApplicationProductDetails() { }

        public string ProductName { get; set; }

        public string Rate
        {
            get { return string.Format(@"{0} / {1}", RateMeasure.AbbreviatedDisplay, AreaUnit.FullDisplay); }
        }

        public decimal TotalCost
        {
            get { return totalCost; }
            set
            {
                totalCost = value;
                RaisePropertyChanged(() => TotalCost);
            }
        }

        public string CostPerUnitDisplay
        {
            get { return CostUnit != null ? string.Format(Strings.Per_Format_Text, CostPerUnitValue.ToString("C"), CostUnit.AbbreviatedDisplay) : string.Empty; }
        }

        public bool HasInvoiceCost { get; set; }
        public decimal? TotalInvoiceCost { get; set; }

        public bool HasSpecificCost
        {
            get { return TotalSpecificCost.HasValue; }
        }

        public decimal? TotalSpecificCost
        {
            get { return totalSpecificCost; }
            set
            {
                totalSpecificCost = value;
                RaisePropertyChanged(() => TotalSpecificCost);
                RaisePropertyChanged(() => HasSpecificCost);
            }
        }

        public string InvoiceCostPerUnitDisplay { get; set; }
        public bool InvoiceCostIsVisible { get; set; }

        public string SpecificCostPerUnitDisplay
        {
            get
            {
                if (SpecificCostUnit != null && SpecificCostPerUnitValue.HasValue)
                {
                    return string.Format(@Strings.Per_Format_Text, SpecificCostPerUnitValue.Value.ToString("C"), SpecificCostUnit.AbbreviatedDisplay);
                }
                else {
                    return string.Empty;
                }
            }
        }

        public Measure RateMeasure { get { return rate; } set { rate = value; RaisePropertyChanged(() => RateMeasure); } }
        public Measure TotalProduct { get { return totalProduct; } set { totalProduct = value; RaisePropertyChanged(() => TotalProduct); } }
        public IUnit AreaUnit { get; set; }
        public decimal AreaValue { get { return area; } set { area = value; RaisePropertyChanged(() => AreaValue); RaisePropertyChanged(() => RateDisplay); } }
        public decimal CostPerUnitValue { get { return costPerUnit; } set { costPerUnit = value; RaisePropertyChanged(() => CostPerUnitDisplay); } }
        public decimal? SpecificCostPerUnitValue { get { return specificCostPerUnit; } set { specificCostPerUnit = value; RaisePropertyChanged(() => SpecificCostPerUnitValue); } }
        public IUnit CostUnit { get; set; }
        public IUnit SpecificCostUnit { get; set; }
        public ProductId ID { get; set; }
        public decimal ProductAreaPercent { get; set; }
        public Guid TrackingId { get; set; }
        public MiniPest TargetPest { get; set; }
        public ApplicationMethod ApplicationMethod { get; set; }
        public Measure RatePerTank { get; set; }
        public string Split { get { return split; } set { split = value; RaisePropertyChanged(() => Split); } }
        public decimal GrowerShare { get; set; }
        public decimal FromGrowerInventory { get; set; }

        public string RateDisplay
        {
            get
            {
                var unit = RateMeasure.Unit;
                var areaUnit = AreaUnit;
                return string.Format("{0:n2} {1} / {2} ({3} {4})", RateMeasure.Value, unit.FullDisplay, areaUnit.AbbreviatedDisplay, (AreaValue * ProductAreaPercent).ToString("n2"), areaUnit.AbbreviatedDisplay);
            }
        }

        private ObservableCollection<AssociatedProductItem> _associatedProducts { get; set; }
        public ObservableCollection<AssociatedProductItem> AssociatedProducts
        {
            get
            {
                return _associatedProducts;
            }
            set
            {
                _associatedProducts = value;
                RaisePropertyChanged(() => AssociatedProducts);
            }
        }

        private bool _hasAssociatedProducts { get; set; }
        public bool HasAssociatedProducts { get { return _hasAssociatedProducts; } set{ _hasAssociatedProducts = value; RaisePropertyChanged(() => HasAssociatedProducts); } }
    }
}