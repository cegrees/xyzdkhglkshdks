﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Client.Models.Connect;
using Landdb.Client.Services;
using Landdb.Domain.ReadModels.Application;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.Resources;
using Landdb.ViewModel.Production.Popups;
using Landdb.ViewModel.Secondary.Application;

namespace Landdb.ViewModel.Production.Applications {
    public class ApplicationsPageViewModel : AbstractListPage<AbstractApplicationListItemViewModel, AbstractApplicationDetailsViewModel>, IHandleViewRouting
    {
        private readonly ConnectStore<ConnectApplication> connectApplicationStore;

        public ApplicationsPageViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher) : base(clientEndpoint, dispatcher) {
            this.FilterModel = new ApplicationsPageFilterViewModel(clientEndpoint);

            Messenger.Default.Register<ApplicationsAddedMessage>(this, msg => {
                RefreshItemsList(() => {
                    var q = from li in ListItems
                            where li.Id == msg.LastApplicationAdded
                            select li;
                    SelectedListItem = q.FirstOrDefault();
                });
            });

            ReSubmitFailedCommand = new RelayCommand(GetFailedCommand);

            Messenger.Default.Register<DataSourceChangedMessage>(this, UpdateDatasourceId);

            connectApplicationStore = clientEndpoint.GetApplicationStore();
            connectApplicationStore.UpdateDatasourceId(currentDataSourceId.Id);
            connectApplicationStore.Subscribe(() => HasNewPendingApplications = true, () => RefreshItemsList());

            clientEndpoint.GetConnectApplicationService().Start();
        }

        public RelayCommand ReSubmitFailedCommand { get; set; }

        public override string GetEntityName() => Strings.Application_Text.ToLower();
		public override string GetPluralEntityName() => Strings.Applications_Text.ToLower();

		protected override IEnumerable<AbstractApplicationListItemViewModel> GetListItemModels()
        {
            var cropYearId = new CropYearId(currentDataSourceId.Id, currentCropYear);
            var applicationsMaybe = clientEndpoint.GetView<ApplicationList>(cropYearId);
            var results = new List<AbstractApplicationListItemViewModel>();

		    if (applicationsMaybe.HasValue) {
                List<AbstractApplicationListItemViewModel> sortedProjectedApplications = 
                    applicationsMaybe.Value.Applications
                    .Select(x => new ApplicationListItemViewModel(x, clientEndpoint) as AbstractApplicationListItemViewModel).ToList();

		        results.AddRange(sortedProjectedApplications);
		    }

            List<ConnectApplication> connectResults = connectApplicationStore.GetData();

            connectResults.ForEach(connectApplication =>
            {
                results.Add(new ConnectApplicationListItemViewModel(connectApplication));
            });

            return new List<AbstractApplicationListItemViewModel>(results.OrderBy(x => x.ListItemType)
                                                                  .ThenByDescending(application => application.StartDateTime));
        }

        protected override async Task<ScreenDescriptor> CreateScreenDescriptorForNewItemCreation() {
            var applicationId = new ApplicationId(ApplicationEnvironment.CurrentDataSourceId, Guid.NewGuid());
            var vm = new ApplicatorViewModel(clientEndpoint, dispatcher, applicationId, ApplicationEnvironment.CurrentCropYear, null);
			return new ScreenDescriptor(typeof(Views.Secondary.Application.ApplicatorView), "create application", vm);
        }

        protected override bool PerformItemRemoval(AbstractApplicationListItemViewModel selectedItem)
        {
            return selectedItem.PerformItemRemoval(clientEndpoint, selectedItem);
        }

        protected override IDomainCommand CreateSetCharmCommand(AbstractApplicationListItemViewModel selectedItem, string charmName)
        {
            return selectedItem.CreateSetCharmCommand(clientEndpoint, selectedItem, charmName);
        }

        protected override AbstractApplicationDetailsViewModel CreateDetailsModel(AbstractApplicationListItemViewModel selectedItem)
        {
            return selectedItem.CreateDetailsModel(selectedItem, clientEndpoint, dispatcher, SetCharmOnSelectedItem);
        }

         public void HandleRouteRequest(RouteRequestMessage message) {
             if (message.Route is ApplicationId applicationId) {

                var q = from x in ListItems ?? new System.Collections.ObjectModel.ObservableCollection<AbstractApplicationListItemViewModel>()
                         where x.Id == applicationId
                         select x;
                 if (q.Any()) {
                     SelectedListItem = q.FirstOrDefault();
                 } else {
                     RefreshItemsList(() => {
                         var q2 = from li in ListItems
                                 where li.Id == applicationId
                                 select li;
                         SelectedListItem = q2.FirstOrDefault();
                     });
                 }
             }
         }

        void GetFailedCommand() {
            FailedCommandTemplateViewModel createItemViewModel = new FailedCommandTemplateViewModel(clientEndpoint, dispatcher);
            Messenger.Default.Send(new ShowOverlayMessage() {
	            ScreenDescriptor = new ScreenDescriptor(typeof(Views.Production.Popups.FailedCommandTemplateView), "failed events", createItemViewModel)
            });
        }

        private bool hasNewPendingApplications;

        public bool HasNewPendingApplications
        {
            get => hasNewPendingApplications;
            set
            {
                hasNewPendingApplications = value;
                RaisePropertyChanged(() => HasNewPendingApplications);
            }
        }

        public ICommand Refresh => new RelayCommand(async () =>
        {
            HasNewPendingApplications = false;
            await RefreshItemsList();
        });

        private void UpdateDatasourceId(DataSourceChangedMessage message)
        {
            HasNewPendingApplications = false;
            clientEndpoint.GetConnectApplicationService().UpdateDatasourceId(message.DataSourceId);
        }

        protected override void generateFilterHeader()
        {
            int originalCount = ListItems.Count;
            int filteredCount = ListItemsView.Count;
            int pendingCount = clientEndpoint.GetApplicationStore().GetData().Count;
            string entityPluralityAwareName = originalCount == 1 ? GetEntityName() : GetPluralEntityName();

            if (originalCount != filteredCount)
            {
                FilterHeader = string.Format(Strings.CountOfCountName_Format_Text, filteredCount, originalCount, entityPluralityAwareName);
            }
            else
            {
                FilterHeader = pendingCount == 0 
                                ? $"{filteredCount} {entityPluralityAwareName}" 
                                : $"{filteredCount} {entityPluralityAwareName}, {pendingCount} pending";
            }
        }
    }
}
