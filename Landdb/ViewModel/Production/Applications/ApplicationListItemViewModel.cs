﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Threading;
using AgC.UnitConversion;
using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Infrastructure;
using Lokad.Cqrs;

namespace Landdb.ViewModel.Production.Applications {
	public class ApplicationListItemViewModel : AbstractApplicationListItemViewModel
	{
		readonly ApplicationListItem li;

	    public ApplicationListItemViewModel(ApplicationListItem li, IClientEndpoint clientEndpoint) {
			this.li = li;

			var masterlistService = clientEndpoint.GetMasterlistService();

			var applicationView = clientEndpoint.GetView<ApplicationView>(li.Id).GetValue(() => null);
			var inventoryView = clientEndpoint.GetView<InventoryListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear)).GetValue(new InventoryListView());

			if (applicationView != null) {
				foreach (var prod in applicationView.Products) {
					var mlProduct = masterlistService.GetProduct(prod.Id);

					var totalMeasure = UnitFactory.GetPackageSafeUnit(prod.TotalProductUnit, mlProduct.StdUnit, mlProduct.StdFactor, mlProduct.StdPackageUnit).GetMeasure((double)prod.TotalProductValue, mlProduct.Density);

					double totalPrice = 0;
					var invProd = inventoryView.Products.FirstOrDefault(x => x.Key == prod.Id);
					if (invProd.Value != null && invProd.Value.AveragePriceUnit != null) {
						var targetUnit = UnitFactory.GetPackageSafeUnit(invProd.Value.AveragePriceUnit, mlProduct.StdUnit, mlProduct.StdFactor, mlProduct.StdPackageUnit);

						if (totalMeasure.CanConvertTo(targetUnit)) {
							totalPrice = totalMeasure.GetValueAs(targetUnit) * invProd.Value.AveragePriceValue;
						}
					}

					TotalCost += (decimal)totalPrice;
				}
			}
		}

		public override ApplicationId Id => li.Id;
		public override string Name => li.Name;
	    public override int FieldCount => li.FieldCount;
	    public override int ProductCount => li.ProductCount;
	    public override DateTime? StartDateTime => li.StartDateTime?.ToLocalTime();
	    public override ApplicationListItemType ListItemType => ApplicationListItemType.LAND_DB;
	    public override decimal TotalCost { get; set; }

	    public override DataTemplate ListItemTemplate
	    {
	        get
	        {
	            ResourceDictionary applicationResourceDictionary = new ResourceDictionary
	            {
	                Source = new Uri("/Landdb;component/Views/Templates/ApplicationListItemTemplate.xaml", UriKind.RelativeOrAbsolute)
	            };

	            return applicationResourceDictionary["ApplicationListItemTemplate"] as DataTemplate;
            }
	    }

	    public override string CharmName {
			get => li.CharmName;
	        set {
				li.CharmName = value;
				RaisePropertyChanged(() => CharmName);
			}
		}

	    public override AbstractApplicationDetailsViewModel CreateDetailsModel(AbstractApplicationListItemViewModel selectedItem, IClientEndpoint clientEndpoint, Dispatcher dispatcher, Action<string> setCharm)
	    {
	        Maybe<ApplicationView> appMaybe = clientEndpoint.GetView<ApplicationView>(selectedItem.Id);

	        if (appMaybe.HasValue)
	        {
	            ApplicationDetailsViewModel applicationDetailsViewModel =
	                new ApplicationDetailsViewModel(selectedItem.Id, clientEndpoint, dispatcher, appMaybe.Value)
	                {
	                    SetCharmCommand = new RelayCommand<string>(setCharm)
	                };

	            return applicationDetailsViewModel;
	        }
	        else
	        {
	            return null;
            }
	    }

	    public override bool PerformItemRemoval(IClientEndpoint clientEndpoint, AbstractApplicationListItemViewModel selectedItem)
	    {
	        RemoveApplication removeCommand = new RemoveApplication(selectedItem.Id, clientEndpoint.GenerateNewMetadata());
	        clientEndpoint.SendOne(removeCommand);
	        return true;
        }

	    public override IDomainCommand CreateSetCharmCommand(IClientEndpoint clientEndpoint, AbstractApplicationListItemViewModel selectedItem, string charmName)
	    {
	        if (selectedItem.CharmName == charmName) { return null; }
	        selectedItem.CharmName = charmName;
            return new FlagApplication(selectedItem.Id, clientEndpoint.GenerateNewMetadata(), charmName);
        }
	}
}
