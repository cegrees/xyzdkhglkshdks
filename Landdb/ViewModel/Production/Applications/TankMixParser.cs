﻿using System.Collections.Generic;
using AgC.UnitConversion;
using AgC.UnitConversion.Package;
using Landdb.Client.Models.Connect;

namespace Landdb.ViewModel.Production.Applications
{
    public static class TankMixParser
    {
        public static List<ApplicationProductDetails> ParseATankMix(ConnectApplication.ProductUse productUse)
        {
            List<ApplicationProductDetails> tankMixProducts = new List<ApplicationProductDetails>();

            foreach (ConnectApplication.ProductUse component in productUse.Components)
            {
                tankMixProducts.Add(MapProductUseToApplicaitonProductDetails(component));
            }

            return tankMixProducts;
        }

        public static ApplicationProductDetails MapProductUseToApplicaitonProductDetails(ConnectApplication.ProductUse productUse)
        {
            return new ApplicationProductDetails
            {
                ProductName = productUse.ResourceDescription,
                AreaValue = (decimal)productUse.Measurements["TotalArea"].Value,
                RateMeasure = Measure.CreateMeasure(productUse.Measurements["AverageActualRate"].Value, productUse.Measurements["AverageActualRate"].Unit),
                AreaUnit = new GeneralUnit(""),
                TotalProduct = Measure.CreateMeasure(productUse.Measurements["TotalActualProduct"].Value, productUse.Measurements["TotalActualProduct"].Unit)
            };
        }
    }
}