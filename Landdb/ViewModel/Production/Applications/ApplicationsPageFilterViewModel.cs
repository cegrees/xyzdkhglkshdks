﻿using System;
using System.Collections.Generic;
using System.Linq;
using GalaSoft.MvvmLight;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Shared;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Lokad.Cqrs;

namespace Landdb.ViewModel.Production.Applications {
    public class ApplicationsPageFilterViewModel : ViewModelBase, IPageFilterViewModel {
        string filterText = string.Empty;
        DateTime? startDate;
        DateTime? endDate;
        IClientEndpoint endpoint;
        public static List<ApplicationTypeSelector> ApplicationTypeSelectors => new List<ApplicationTypeSelector>
        {
            new ApplicationTypeSelector(ApplicationListItemType.ALL_APPLICATIONS),
            new ApplicationTypeSelector(ApplicationListItemType.CONNECT),
            new ApplicationTypeSelector(ApplicationListItemType.LAND_DB)
        };

        public ApplicationsPageFilterViewModel(IClientEndpoint endpoint)
        {
            this.endpoint = endpoint;
            SelectedApplicationTypeSelector = ApplicationTypeSelectors.First();
        }

        public void ClearFilter() {
            FilterText = string.Empty;
            StartDate = null;
            EndDate = null;
            SelectedApplicationTypeSelector = ApplicationTypeSelectors.First();
        }

        public void BeforeFilter() { }

		public void RefreshLists() { }

        public bool FilterItem(object item) {
            if (item is AbstractApplicationListItemViewModel listItem)
            {
                if (StartDate != null && listItem.StartDateTime < StartDate.Value.Date)
                {
                    return false;
                }

                if (EndDate != null && listItem.StartDateTime >
                    EndDate.Value.Date + TimeSpan.FromDays(1) - TimeSpan.FromSeconds(1))
                {
                    return false;
                }

                bool farmFieldCropZoneAndCropFilter = filterByFarmFieldCropZoneAndCrop(endpoint, listItem, filterText);

                bool nameFilter = filterByName(FilterText, listItem.Name);

                bool applicationTypeSelectorFilter =
                    filterByApplicationSelector(SelectedApplicationTypeSelector.ApplicationListItemType,
                        listItem.ListItemType);

                return farmFieldCropZoneAndCropFilter && applicationTypeSelectorFilter || nameFilter && applicationTypeSelectorFilter;
            }
            return true;
        }

        private static bool filterByName(string filterText, string name)
        {
            return filterText == null || name.ToLower().Contains(filterText.ToLower());
        }

        private static bool filterByApplicationSelector(ApplicationListItemType selectedApplicationListItemType, ApplicationListItemType filteringListItemType)
        {
            return selectedApplicationListItemType == ApplicationListItemType.ALL_APPLICATIONS || filteringListItemType == selectedApplicationListItemType;
        }

        private static bool filterByFarmFieldCropZoneAndCrop(IClientEndpoint endpoint, AbstractApplicationListItemViewModel listItem, string filterText)
        {
            Maybe<ApplicationView> appDetailMaybe = endpoint.GetView<ApplicationView>(listItem.Id);

            if (appDetailMaybe.HasValue)
            {
                List<FlattenedTreeHierarchyItem> flattenedItemsList = new List<FlattenedTreeHierarchyItem>();
                FlattenedTreeHierarchyView flattened = endpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear)).GetValue(new FlattenedTreeHierarchyView());

                foreach (IncludedCropZone czItem in appDetailMaybe.Value.CropZones)
                {
                    FlattenedTreeHierarchyItem flat = (from f in flattened.Items
                                                       where f.CropZoneId == czItem.Id
                                                       select f).FirstOrDefault();

                    if (flat == null)
                    {
                        flat = (from f in flattened.DeletedItems
                                where f.CropZoneId == czItem.Id
                                select f).FirstOrDefault();

                        if (flat == null) { continue; }
                    }

                    flattenedItemsList.Add(flat);
                }

                bool filterContainsCrop = (from flatItem in flattenedItemsList
                                           where flatItem != null && flatItem.CropName.ToLower().Contains(filterText.ToLower())
                                           select flatItem).Any();

                bool filterContainsCZ = (from flatItem in flattenedItemsList
                                         where flatItem != null && flatItem.CropZoneName.ToLower().Contains(filterText.ToLower())
                                         select flatItem).Any();
                bool filterContainsField = (from flatItem in flattenedItemsList
                                            where flatItem != null && flatItem.FieldName.ToLower().Contains(filterText.ToLower())
                                            select flatItem).Any();
                bool filterContainsFarm = (from flatItem in flattenedItemsList
                                           where flatItem != null && flatItem.FarmName.ToLower().Contains(filterText.ToLower())
                                           select flatItem).Any();

                return string.IsNullOrWhiteSpace(filterText) || filterContainsCZ || filterContainsField || filterContainsFarm || filterContainsCrop || listItem.Name.ToLower().Contains(filterText.ToLower());
            }
            else
            {
                return false;
            }
        }

        public string FilterText {
            get => filterText;
            set {
                filterText = value;
                RaisePropertyChanged("FilterText");
            }
        }

        public DateTime? StartDate {
            get => startDate;
            set {
                startDate = value;
                RaisePropertyChanged("StartDate");
            }
        }

        public DateTime? EndDate {
            get => endDate;
            set {
                endDate = value;
                RaisePropertyChanged("EndDate");
            }
        }

        private ApplicationTypeSelector selectedApplicationTypeSelector;

        public ApplicationTypeSelector SelectedApplicationTypeSelector
        {
            get => selectedApplicationTypeSelector;
            set
            {
                selectedApplicationTypeSelector = value;
                RaisePropertyChanged(() => SelectedApplicationTypeSelector);
            }
        }
    }
}
