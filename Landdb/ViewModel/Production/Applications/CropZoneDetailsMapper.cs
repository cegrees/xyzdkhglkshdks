﻿using System;
using System.Threading.Tasks;
using System.Windows.Shapes;
using System.Windows.Threading;
using AgC.UnitConversion;
using AgC.UnitConversion.Area;
using Landdb.Client.Infrastructure;
using Landdb.Client.Spatial;
using Landdb.Domain.ReadModels.Map;
using Landdb.Domain.ReadModels.Tree;
using Landdb.ViewModel.Shared;
using Lokad.Cqrs;

namespace Landdb.ViewModel.Production.Applications
{
    public static class CropZoneDetailsMapper
    {
        public static CropZoneDetails MapCropZoneDetailsViewToCropZoneDetails(IClientEndpoint clientEndpoint, Dispatcher dispatcher, CropZoneDetailsView cropZoneDetailsView, string fieldName)
        {
            Maybe<ItemMap> maps = clientEndpoint.GetView<ItemMap>(new CropZoneId(cropZoneDetailsView.Id.DataSourceId, cropZoneDetailsView.Id.Id));

            return new CropZoneDetails
            {
                Id = new CropZoneId(cropZoneDetailsView.Id.DataSourceId, cropZoneDetailsView.Id.Id),
                Name = $"{fieldName} : {cropZoneDetailsView.Name}",
                Area = (AreaMeasure)Measure.CreateMeasure(cropZoneDetailsView.BoundaryArea ?? 0.0, cropZoneDetailsView.BoundaryAreaUnit),
                ShapePreview = maps.HasValue ? GetPath(dispatcher, maps.Value) : null
            };
        }

        public static CropZoneDetails MapFieldDetailsViewToCropZoneDetails(IClientEndpoint clientEndpoint, Dispatcher dispatcher, FieldDetailsView fieldDetailsView)
        {
            Maybe<ItemMap> maps = clientEndpoint.GetView<ItemMap>(new FieldId(fieldDetailsView.Id.DataSourceId, fieldDetailsView.Id.Id));

            return new CropZoneDetails
            {
                Id = new CropZoneId(fieldDetailsView.Id.DataSourceId, fieldDetailsView.Id.Id),
                Name = $"{fieldDetailsView.Name}",
                Area = (AreaMeasure)Measure.CreateMeasure(fieldDetailsView.BoundaryArea ?? 0.0, fieldDetailsView.BoundaryAreaUnit),
                ShapePreview = maps.HasValue ? GetPath(dispatcher, maps.Value) : null
            };
        }

        private static Path GetPath(Dispatcher dispatcher, ItemMap map)
        {
            Path resultPath = null;

            dispatcher.BeginInvoke(new Action(
                () => resultPath = WpfTransforms.CreatePathFromMapData(new[] {map.MostRecentMapItem.MapData}, 40)
            )).Wait();

            return resultPath;
        }
    }
}