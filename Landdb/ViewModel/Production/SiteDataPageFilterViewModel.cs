﻿using GalaSoft.MvvmLight;
using Landdb.Client.Infrastructure;
using Landdb.Client.Infrastructure.DisplayItems;
using Landdb.Domain.ReadModels.Documents;
using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Windows.Data;

namespace Landdb.ViewModel.Production
{
    /// <summary>
    /// NOTE: Deprecated - see <see cref="SourceDataDisplayItem"/>.
    /// </summary>
    public class SiteDataUsedAsSourceFilterConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            throw new NotImplementedException();
            if (value == null) { return null; }

            string itemString = value.ToString();
            FieldInfo field = typeof(SourceDataDisplayType).GetField(itemString);
            object[] attribs = field.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (null != attribs && attribs.Length > 0)
                itemString = ((DescriptionAttribute)attribs[0]).Description;

            return itemString;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            throw new NotImplementedException();
            if (value == null) { return SourceDataDisplayType.Null; }
            if (value.ToString() == "Used in a recommendation") { return SourceDataDisplayType.UsedInRecommendation; }
            if (value.ToString() == "Used in a work order") { return SourceDataDisplayType.UsedInWorkOrder; }
            if (value.ToString() == "Used in an application") { return SourceDataDisplayType.UsedInApplication; }
            if (value.ToString() == "Not used") { return SourceDataDisplayType.NotUsed; }
            return SourceDataDisplayType.Null;
        }
    }


    public class SiteDataPageFilterViewModel: ViewModelBase, IPageFilterViewModel {
        IClientEndpoint clientEndpoint;
        string filterText = string.Empty;
        DateTime? startDate = null;
        DateTime? endDate = null;
        SourceDataDisplayItem siteUse = new SourceDataDisplayItem(SourceDataDisplayType.Null);

        public SiteDataPageFilterViewModel(IClientEndpoint clientEndpoint) {
            this.clientEndpoint = clientEndpoint;
        }

        public void ClearFilter() {
            FilterText = string.Empty;
            StartDate = null;
            EndDate = null;
            SiteDataUse = new SourceDataDisplayItem(SourceDataDisplayType.Null);
        }

        public void BeforeFilter() { }

		public void RefreshLists() { }

		public bool FilterItem(object item) {
            var x = item as SiteDataListItemViewModel;
            if (x == null) { return false; }

            if (StartDate != null && (x.StartDateTime < StartDate.Value.Date)) { return false; }
            if (EndDate != null && (x.EndDateTime > (EndDate.Value.Date + TimeSpan.FromDays(1) - TimeSpan.FromSeconds(1)))) { return false; }

            var siteDetailMaybe = x;

            if (SiteDataUse != null) {
                var docUse = clientEndpoint.GetView<DocumentUseIndex>(x.DocumentId);
                switch (SiteDataUse.SourceDataType) {
                    case SourceDataDisplayType.Null:
                        break;
                    case SourceDataDisplayType.UsedInWorkOrder:
                        if (!docUse.HasValue || docUse.Value.SourcedItems == null || !docUse.Value.SourcedItems.Any( i => i is WorkOrderId)) { return false; }
                        break;
                    case SourceDataDisplayType.UsedInApplication:
                        if (!docUse.HasValue || docUse.Value.SourcedItems == null || !docUse.Value.SourcedItems.Any(i => i is ApplicationId)) { return false; }
                        break;
                    case SourceDataDisplayType.NotUsed:
                        if (docUse.HasValue && docUse.Value.SourcedItems.Any()) { return false; }
                        break;
                }
            }

            return true;
        }

        public string FilterText {
            get { return filterText; }
            set {
                filterText = value;
                RaisePropertyChanged("FilterText");
            }
        }

        public DateTime? StartDate {
            get { return startDate; }
            set {
                startDate = value;
                RaisePropertyChanged("StartDate");
            }
        }

        public DateTime? EndDate {
            get { return endDate; }
            set {
                endDate = value;
                RaisePropertyChanged("EndDate");
            }
        }

        public SourceDataDisplayItem SiteDataUse {
            get { return siteUse; }
            set {
                siteUse = value;
                RaisePropertyChanged("SiteDataUse");
            }
        }
    }
}
