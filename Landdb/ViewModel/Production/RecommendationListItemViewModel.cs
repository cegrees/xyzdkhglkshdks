﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using Landdb.Domain.ReadModels.Recommendation;

namespace Landdb.ViewModel.Production
{
    public class RecommendationListItemViewModel : ViewModelBase
    {
        RecommendationListItem li;
    
        public RecommendationListItemViewModel(RecommendationListItem li)
        {
            this.li = li;

        }

        public RecommendationId Id { get { return li.Id; } }
        public string RecSource { get { return li.RecSource; } }
        public DateTime? ProposedDate { get { return li.ProposedDate; } }
        public DateTime? ExpirationDate { get { return li.ExpirationDate; } }
        public string ValidDateRange { get { return RecommendationView.CreateValidDateRangeShort(li.ProposedDate, li.ExpirationDate); } }
        public string RecNumber { get { return li.RecNumber; } }
        public string RecName { get { return li.RecName; } }
        public string NameAndNumber { get { return RecommendationView.CreateNameAndNumber(li.RecName, li.RecNumber); } }
        public int FieldCount { get { return li.FieldCount; } }
        public int ProductCount { get { return li.ProductCount; } }
        public bool Resolved
        {
            get
            {
                return((!li.HasUnknownAuthorizer) && (li.UnknownApplicatorCount == 0) && (li.UnknownCropZoneCount == 0) && (li.UnknownProductCount == 0));
            }
        }
        public string CharmName
        {
            get { return li.CharmName; }
            set
            {
                li.CharmName = value;
                RaisePropertyChanged("CharmName");
            }
        }
        public void Refresh()
        {
            RaisePropertyChanged("FieldCount");
            RaisePropertyChanged("ProductCount");
            RaisePropertyChanged("NameAndNumber");
            RaisePropertyChanged("ValidDateRange");
            RaisePropertyChanged("Resolved");
        }
    }
}
