﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Threading;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Recommendation;
using Landdb.Infrastructure;
using Landdb.ViewModel.Secondary.Recommendation;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Secondary.Reports.Recommendation;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net;
using Landdb.Resources;

namespace Landdb.ViewModel.Production {
    public class RecommendationsPageViewModel : AbstractListPage<RecommendationListItemViewModel, RecommendationDetailsViewModel> {
        public RecommendationsPageViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher)
            : base(clientEndpoint, dispatcher) {
                this.FilterModel = new RecommendationPageFilterViewModel(clientEndpoint);

            Messenger.Default.Register<RecommendationsAddedMessage>(this, msg => {
                RefreshItemsList(() => {
                    var q = from li in ListItems
                            where li.Id == msg.LastRecommendationAdded
                            select li;
                    SelectedListItem = q.FirstOrDefault();
                });
            });
            CreateWorkOrderFromSelectedCommand = new RelayCommand(CreateWorkOrderFromSelected, () => base.SelectedListItem != null);
            CreateApplicationFromSelectedCommand = new RelayCommand(CreateApplicationFromSelected, () => base.SelectedListItem != null);
            ShareFromSelectedCommand = new RelayCommand(ShareFromSelectedToTheWeb, () => base.SelectedListItem != null);
            ViewReport = new RelayCommand(this.UpdateReport);
        }
        public bool Resolved {
            get {
                if (base.SelectedListItem != null)
                    return (base.SelectedListItem.Resolved);
                else
                    return false;
            }
        }
        public ICommand CreateWorkOrderFromSelectedCommand { get; private set; }
        public ICommand CreateApplicationFromSelectedCommand { get; private set; }
        public ICommand ShareFromSelectedCommand { get; private set; }
        public RelayCommand ViewReport { get; private set; }

        public override string GetEntityName() { return Strings.Recommendation_Text.ToLower(); }

        public override string GetPluralEntityName() { return Strings.Recommendations_Text.ToLower(); }

        protected override IEnumerable<RecommendationListItemViewModel> GetListItemModels() {
            var cropYearId = new CropYearId(currentDataSourceId.Id, currentCropYear);
            var recommendationMaybe = clientEndpoint.GetView<RecommendationList>(cropYearId);

            if (recommendationMaybe.HasValue) {
                var sortedProjectedRecommendation = recommendationMaybe.Value.Recommendations.OrderByDescending(x => x.ProposedDate).Select(x => new RecommendationListItemViewModel(x));
                return sortedProjectedRecommendation;
            } else {
                return null;
            }
        }

        protected override async Task<ScreenDescriptor> CreateScreenDescriptorForNewItemCreation() {
            var recommendationId = new RecommendationId(ApplicationEnvironment.CurrentDataSourceId, Guid.NewGuid());

            var vm = new RecommendationViewModel(clientEndpoint, dispatcher, recommendationId, ApplicationEnvironment.CurrentCropYear, null);
            ScreenDescriptor descriptor = new ScreenDescriptor("Landdb.Views.Secondary.Recommendation.RecommendationView", "create recommendation", vm);
            return descriptor;
        }

        protected override bool PerformItemRemoval(RecommendationListItemViewModel selectedItem) {
            var removeCommand = new RemoveRecommendation(selectedItem.Id, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId));
            clientEndpoint.SendOne(removeCommand);
            return true;
        }

        protected override IDomainCommand CreateSetCharmCommand(RecommendationListItemViewModel selectedItem, string charmName) {
            if (selectedItem.CharmName == charmName) { return null; }
            selectedItem.CharmName = charmName;
            return new FlagRecommendation(selectedItem.Id, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), charmName);
        }

        protected override RecommendationDetailsViewModel CreateDetailsModel(RecommendationListItemViewModel selectedItem) {
            var recMaybe = clientEndpoint.GetView<RecommendationView>(selectedItem.Id);
            if (recMaybe.HasValue) {
                return new RecommendationDetailsViewModel(clientEndpoint, selectedItem, recMaybe.Value);
            } else {
                return null;
            }
        }

        void CreateWorkOrderFromSelected() {
            if (Resolved) {
                var workOrderId = new WorkOrderId(ApplicationEnvironment.CurrentDataSourceId, Guid.NewGuid());

                DocumentDescriptor sourceDoc = new DocumentDescriptor() { DocumentType = DocumentDescriptor.RecommendationTypeString, Identity = base.SelectedListItem.Id, Name = base.SelectedListItem.RecName };

                var vm = new Secondary.WorkOrder.WorkOrderEditorViewModel(clientEndpoint, dispatcher, workOrderId, ApplicationEnvironment.CurrentCropYear, null, sourceDoc);
                ScreenDescriptor descriptor = new ScreenDescriptor("Landdb.Views.Secondary.WorkOrder.WorkOrderEditorView", "create work order", vm);
                Messenger.Default.Send<ChangeScreenDescriptorMessage>(new ChangeScreenDescriptorMessage() { ScreenDescriptor = descriptor });
            }
        }

        void CreateApplicationFromSelected() {
            if (Resolved) {
                var applicationId = new ApplicationId(ApplicationEnvironment.CurrentDataSourceId, Guid.NewGuid());

                DocumentDescriptor sourceDoc = new DocumentDescriptor() { DocumentType = DocumentDescriptor.RecommendationTypeString, Identity = base.SelectedListItem.Id, Name = base.SelectedListItem.RecName };

                var vm = new Secondary.Application.ApplicatorViewModel(clientEndpoint, dispatcher, applicationId, ApplicationEnvironment.CurrentCropYear, null, sourceDoc);
                ScreenDescriptor descriptor = new ScreenDescriptor("Landdb.Views.Secondary.Application.ApplicatorView", "create application", vm);
                Messenger.Default.Send<ChangeScreenDescriptorMessage>(new ChangeScreenDescriptorMessage() { ScreenDescriptor = descriptor });
            }
        }

        void UpdateReport()
        {
            if (SelectedListItem != null && SelectedListItem.Id != null)
            {
                var vm = new SingleRecommendationViewModel(clientEndpoint, dispatcher, SelectedListItem.Id);
                var sd = new ScreenDescriptor("Landdb.Views.Secondary.Reports.Work_Order.SingleWorkOrderView", "viewReport", vm);
                Messenger.Default.Send(new ShowOverlayMessage() { ScreenDescriptor = sd });
            }
        }

        void ShareFromSelectedToTheWeb() {
            var pMaybe = clientEndpoint.GetView<RecommendationView>(base.SelectedListItem.Id);
            if (pMaybe.HasValue) {
                Popups.ChoosePersonToShareViewModel vm = null;
                vm = new Popups.ChoosePersonToShareViewModel(clientEndpoint, dispatcher, pMaybe.Value, onPersonShared);
                Messenger.Default.Send(new ShowOverlayMessage() {
                    ScreenDescriptor = new ScreenDescriptor(typeof(Views.Production.Popups.ChoosePersonToShareView), "changeCropzones", vm)
                });
            }
        }

        private void onPersonShared(Popups.ChoosePersonToShareViewModel vm) {
            if (vm != null) {
                var recId = new RecommendationId(currentDataSourceId.Id, base.SelectedListItem.Id.Id);
                var personid = vm.Person.Id;
                var sharemodel = new { ShareePersonId = personid.Id, Id = recId };

                ShowIntegration(sharemodel, "api/recommendations/share");
            }
            Messenger.Default.Send<HideOverlayMessage>(new HideOverlayMessage());
        }

        public void ShowIntegration(object sharemodel, string uristring) {
            //TO DO :: CALL API TO GET PAGE
            Task<string> getMLZone = Task.Factory.StartNew<string>(() => {

                var dataSource = new DataSourceId(ApplicationEnvironment.CurrentDataSourceId);
                var cropYear = ApplicationEnvironment.CurrentCropYear;

                if (NetworkStatus.IsInternetAvailable()) {
                    var baseDocumentUri = string.Format("{0}{1}?datasourceid={2}", RemoteConfigurationSettings.GetBaseUri(), uristring, dataSource.Id);
                    //var action = string.Format("?dataSourceId={0}&cropYear={2}&nodeId={1}&nodeType=field", dataSource.Id, fieldView.Id.Id, cropYear);
                    var client = new HttpClient {
                        BaseAddress = new Uri(baseDocumentUri),
                    };

                    client.DefaultRequestHeaders.Add("Accept", "application / json");
                    var holder = PostData<string>(baseDocumentUri, client, sharemodel);
                    holder.Wait();

                    //var formattedLink = holder.Result;
                    //System.Diagnostics.Process.Start(formattedLink);

                    return holder.Result;
                }
                else {
                    return string.Empty;
                }
            });
        }

        private async Task<T> PostData<T>(string baseDocumentUri, System.Net.Http.HttpClient client, object sharemodel) {
            try {
                var cloudClientFactory = new Landdb.Infrastructure.CloudClientFactory();
                var token = cloudClientFactory.ResolveStorageCredentials();
                await token.SetBearerTokenAsync(client);

                // TODO: Might want to log this for debugging

                //HttpResponseMessage response = await client.PostAsJsonAsync(baseDocumentUri, JsonConvert.SerializeObject(sharemodel));
                HttpResponseMessage response = await client.PostAsJsonAsync(baseDocumentUri, sharemodel);

                if (response.StatusCode == HttpStatusCode.OK) {
                    // var json = await response.Content.ReadAsStringAsync();
                    // var result = JsonConvert.DeserializeObject<T>(json);
                    // return result;

                    // TODO: Read the response later...
                    var fake = string.Empty;
                    var result = JsonConvert.DeserializeObject<T>(fake);
                    return result;
                }
                else {
                    var fake = string.Empty;
                    var result = JsonConvert.DeserializeObject<T>(fake);
                    return result;
                }
            }
            catch (Exception ex) {
                throw new ApplicationException("Share Failed.");
            }
        }

    }
}