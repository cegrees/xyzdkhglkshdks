﻿using GalaSoft.MvvmLight;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Documents;
using Landdb.Domain.ReadModels.Recommendation;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using Landdb.Client.Infrastructure.DisplayItems;

namespace Landdb.ViewModel.Production {
    public enum RecommendationTypeFilterResponseEnum {
        [Description("All")]
        All,
        [Description("Land.db")]
        Landdb,
        [Description("Agrian")]
        Agrian,
    }

    public class RecommendationTypeFilterConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            if (value == null) { return null; }

            string itemString = value.ToString();
            FieldInfo field = typeof(RecommendationTypeFilterResponseEnum).GetField(itemString);
            object[] attribs = field.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (null != attribs && attribs.Length > 0)
                itemString = ((DescriptionAttribute)attribs[0]).Description;

            return itemString;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            if (value == null) { return RecommendationTypeFilterResponseEnum.All; }
            if (value.ToString() == GlobalStrings.RecommendationType_Landdb) { return RecommendationTypeFilterResponseEnum.Landdb; }
            if (value.ToString() == GlobalStrings.RecommendationType_Agrian) { return RecommendationTypeFilterResponseEnum.Agrian; }
            return RecommendationTypeFilterResponseEnum.All;
        }
    }

    public class RecommendationUsedAsSourceFilterConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            if (value == null) { return null; }

            string itemString = value.ToString();
            FieldInfo field = typeof(SourceDataDisplayType).GetField(itemString);
            object[] attribs = field.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (null != attribs && attribs.Length > 0)
                itemString = ((DescriptionAttribute)attribs[0]).Description;

            return itemString;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            if (value == null) { return SourceDataDisplayType.Null; } else {

            }
            if (value.ToString() == "Used in a work order") { return SourceDataDisplayType.UsedInWorkOrder; }
            if (value.ToString() == "Used in an application") { return SourceDataDisplayType.UsedInApplication; }
            if (value.ToString() == "Not used") { return SourceDataDisplayType.NotUsed; }
            return SourceDataDisplayType.Null;
        }
    }


    public class RecommendationPageFilterViewModel : ViewModelBase, IPageFilterViewModel {
        IClientEndpoint clientEndpoint;
        string filterText = string.Empty;
        DateTime? startDate = null;
        DateTime? endDate = null;
        RecommendationTypeFilterResponseEnum recType = RecommendationTypeFilterResponseEnum.All;
        SourceDataDisplayItem recUse = new SourceDataDisplayItem(SourceDataDisplayType.Null);

        public RecommendationPageFilterViewModel(IClientEndpoint clientEndpoint) {
            this.clientEndpoint = clientEndpoint;
        }

        public void ClearFilter() {
            FilterText = string.Empty;
            StartDate = null;
            EndDate = null;
            RecommendationType = RecommendationTypeFilterResponseEnum.All;
            RecommendationUse = new SourceDataDisplayItem(SourceDataDisplayType.Null);
        }

        public void BeforeFilter() { }

		public void RefreshLists() { }

		public bool FilterItem(object item) {
            var x = item as RecommendationListItemViewModel;
            if (x == null) { return false; }

            if (StartDate != null && (!x.ProposedDate.HasValue || x.ProposedDate < StartDate.Value.Date)) { return false; }
            if (EndDate != null && (!x.ProposedDate.HasValue || x.ProposedDate > (EndDate.Value.Date + TimeSpan.FromDays(1) - TimeSpan.FromSeconds(1)))) { return false; }

            var recDetailMaybe = clientEndpoint.GetView<RecommendationView>(x.Id);

            if (recDetailMaybe.HasValue)
            {
                List<FlattenedTreeHierarchyItem> flattenedItemsList = new List<FlattenedTreeHierarchyItem>();
                var flattened = clientEndpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear)).GetValue(new FlattenedTreeHierarchyView());

                foreach (var czItem in recDetailMaybe.Value.CropZones)
                {
                    var flat = (from f in flattened.Items
                                where f.CropZoneId == czItem.Id
                                select f).FirstOrDefault();

					if (flat == null) {
						// check deleted cropzones
						flat = (from f in flattened.DeletedItems
								where f.CropZoneId == czItem.Id
								select f).FirstOrDefault();

						if (flat == null) { continue; }
					}

                    flattenedItemsList.Add(flat);
                }

                var filterContainsCZ = (from flatItem in flattenedItemsList
                                        where flatItem.CropZoneName.ToLower().Contains(FilterText.ToLower())
                                        select flatItem).Any();
                var filterContainsField = (from flatItem in flattenedItemsList
                                           where flatItem.FieldName.ToLower().Contains(FilterText.ToLower())
                                           select flatItem).Any();
                var filterContainsFarm = (from flatItem in flattenedItemsList
                                          where flatItem.FarmName.ToLower().Contains(FilterText.ToLower())
                                          select flatItem).Any();

                if (!string.IsNullOrWhiteSpace(FilterText) && filterContainsCZ == false && filterContainsField == false && filterContainsFarm == false && !x.NameAndNumber.ToLower().Contains(FilterText.ToLower()))
                {
                    return false;
                }
            }
            

            //if (!string.IsNullOrWhiteSpace(filterText) && !x.NameAndNumber.Contains(filterText)) {
            //    return false;
            //}

            if (RecommendationType != RecommendationTypeFilterResponseEnum.All) {
                switch (RecommendationType) {
                    case RecommendationTypeFilterResponseEnum.All:
                        break;
                    case RecommendationTypeFilterResponseEnum.Landdb:
                        if (x.RecSource != GlobalStrings.RecommendationType_Landdb) { return false; }
                        break;
                    case RecommendationTypeFilterResponseEnum.Agrian:
                        if (x.RecSource != GlobalStrings.RecommendationType_Agrian) { return false; }
                        break;
                    default:
                        break;
                }
            }

            if (RecommendationUse != null) {
                var docUse = clientEndpoint.GetView<DocumentUseIndex>(x.Id);
                switch (RecommendationUse.SourceDataType) {
                    case SourceDataDisplayType.Null:
                        break;
                    case SourceDataDisplayType.UsedInWorkOrder:
                        if (!docUse.HasValue || docUse.Value.SourcedItems == null || !docUse.Value.SourcedItems.Any( i => i is WorkOrderId)) { return false; }
                        break;
                    case SourceDataDisplayType.UsedInApplication:
                        if (!docUse.HasValue || docUse.Value.SourcedItems == null || !docUse.Value.SourcedItems.Any(i => i is ApplicationId)) { return false; }
                        break;
                    case SourceDataDisplayType.NotUsed:
                        if (docUse.HasValue && docUse.Value.SourcedItems.Any()) { return false; }
                        break;
                }
            }

            return true;
        }

        public string FilterText {
            get { return filterText; }
            set {
                filterText = value;
                RaisePropertyChanged("FilterText");
            }
        }

        public DateTime? StartDate {
            get { return startDate; }
            set {
                startDate = value;
                RaisePropertyChanged("StartDate");
            }
        }

        public DateTime? EndDate {
            get { return endDate; }
            set {
                endDate = value;
                RaisePropertyChanged("EndDate");
            }
        }

        public RecommendationTypeFilterResponseEnum RecommendationType {
            get { return recType; }
            set {
                recType = value;
                RaisePropertyChanged("RecommendationType");
            }
        }

        public IList<SourceDataDisplayItem> RecommendationUses {
            get {
                var displayList = SourceDataDisplayItem.AllAsList();
                displayList.Select(x => !x.SourceDataType.Equals(SourceDataDisplayType.UsedInRecommendation));
                return displayList;
            }
        }

        public SourceDataDisplayItem RecommendationUse {
            get { return RecommendationUses.First(x => x.Equals(recUse)); }
            set {
                recUse = RecommendationUses.First(x => x.Equals(value));
                RaisePropertyChanged("RecommendationUse");
            }
        }
    }
}
