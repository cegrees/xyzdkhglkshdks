﻿using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Client.Services;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Secondary.Reports.Scouting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Production
{
    public class SiteDataPageViewModel : AbstractListPage<SiteDataListItemViewModel, SiteDataDetailsViewModel>
    {
        public SiteDataPageViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher)
            : base(clientEndpoint, dispatcher)
        {
            this.FilterModel = new SiteDataPageFilterViewModel(clientEndpoint);
            List = new List<SiteDataListItemViewModel>();

            CreateWorkOrderFromSelectedCommand = new RelayCommand(CreateWorkOrderFromSelected, () => SelectedListItem != null);
            CreateRecommendationFromSelectedCommand = new RelayCommand(CreateRecommendationFromSelected, () => SelectedListItem != null);

            PrintCommand = new RelayCommand(GenerateReport);
        }

        public ICommand PrintCommand { get; }
        public ICommand CreateWorkOrderFromSelectedCommand { get; }
        public ICommand CreateRecommendationFromSelectedCommand { get; }
        public IEnumerable<SiteDataListItemViewModel> List { get; set; }

		public override string GetEntityName() => Strings.SiteData_Text.ToLower();
		public override string GetPluralEntityName() => Strings.SiteData_Text.ToLower();

		protected override IEnumerable<SiteDataListItemViewModel> GetListItemModels()
        {
            Task<IEnumerable<SiteDataListItemViewModel>> getListData = Task<IEnumerable<SiteDataListItemViewModel>>.Factory.StartNew(() =>
            {
                IEnumerable<SiteDataListItemViewModel> siteDataList = new List<SiteDataListItemViewModel>();
                siteDataList = GetData().Result;

                return siteDataList.OrderByDescending(x =>x.EndDateTime);
            });

            getListData.Wait();

            Task writeToFile = Task.Factory.StartNew(() =>
            {
                WriteScoutingData();
            });


            return getListData.Result;
        }

        protected override async Task<ScreenDescriptor> CreateScreenDescriptorForNewItemCreation()
        {
            var siteDataId = new ScoutingApplicationId(ApplicationEnvironment.CurrentDataSourceId, Guid.NewGuid());
            
            //var vm = new RecommendationViewModel(clientEndpoint, dispatcher, recommendationId, ApplicationEnvironment.CurrentCropYear, null);
            //ScreenDescriptor descriptor = new ScreenDescriptor("Landdb.Views.Secondary.Recommendation.RecommendationView", "create recommendation", vm);
            //return descriptor;
            return null;
        }

        protected override bool PerformItemRemoval(SiteDataListItemViewModel selectedItem)
        {
            var dataSource = new DataSourceId(ApplicationEnvironment.CurrentDataSourceId);
            var cropYear = ApplicationEnvironment.CurrentCropYear;

            if (NetworkStatus.IsInternetAvailable())
            {
                var selectedId = selectedItem.Id.Id;
                //var scoutingDocumentUri = "https://landdb.com/api/scoutingevents/";
                var scoutingDocumentUri = string.Format("{0}api/samples", RemoteConfigurationSettings.GetBaseUri());
                var action = string.Format("?dataSourceId={0}&id={1}&cropYear={2}", dataSource.Id, selectedId, cropYear);
                scoutingDocumentUri = string.Format("{0}{1}", scoutingDocumentUri, action);
                var client = new HttpClient
                {
                    BaseAddress = new Uri(scoutingDocumentUri),
                };

                client.DefaultRequestHeaders.Add("Accept", "application / json");

                var cloudClientFactory = new Landdb.Infrastructure.CloudClientFactory();
                var token = cloudClientFactory.ResolveStorageCredentials();
                token.SetBearerTokenAsync(client);

                var response = client.DeleteAsync(scoutingDocumentUri);

                ListItems.Remove(ListItems.FirstOrDefault(x => x.Id.Id == selectedId));
                var modifiedList = List.ToList();
                modifiedList.Remove(List.FirstOrDefault(x => x.Id.Id == selectedId));
                List = modifiedList;
                //TODO :: WRITE BACK DATA LOCALLY WITHOUT THE DELETED ITEM
                WriteScoutingData();

                RaisePropertyChanged("List");
                return true;
            } else { return false; }
        }

        protected override IDomainCommand CreateSetCharmCommand(SiteDataListItemViewModel selectedItem, string charmName)
        {
            return null;
        }

        protected override SiteDataDetailsViewModel CreateDetailsModel(SiteDataListItemViewModel selectedItem)
        {
            var SiteDetails = SelectedListItem;

            var details = List.FirstOrDefault(x => x.DocumentId == selectedItem.DocumentId);
            SiteDataDetailsViewModel detail = new SiteDataDetailsViewModel(clientEndpoint, dispatcher, details);

            return detail;
        }

        private async Task<IEnumerable<SiteDataListItemViewModel>> GetData()
        {
            //CALL TO PULL IN NEW SCOUTING DATA
            List = new List<SiteDataListItemViewModel>();
            var dataSource = new DataSourceId(ApplicationEnvironment.CurrentDataSourceId);
            var cropYear = ApplicationEnvironment.CurrentCropYear;

            if (NetworkStatus.IsInternetAvailable())
            {
                //var scoutingDocumentUri = "https://landdb.com/api/scoutingevents/";
                var scoutingDocumentUri = string.Format("{0}api/scoutingevents/", RemoteConfigurationSettings.GetBaseUri()); 
                var action = string.Format("?dataSourceId={0}&cropYear={1}", dataSource.Id, cropYear);
                var client = new HttpClient
                {
                    BaseAddress = new Uri(scoutingDocumentUri),
                };
                client.DefaultRequestHeaders.Add("Accept", "application / json");
                var holder = await GetScoutingData<ScoutingList>(action, client);

                List = holder != null ? holder.Results.ToArray() : List;
                return List.ToList();
            }
            else
            {
                var scoutingList = LoadScoutingData();
                List = scoutingList != null ? scoutingList.Results.ToArray() : List;
                return List.ToList();
            }
        }

        private async Task<T> GetScoutingData<T>(string scoutingDocumentUri, HttpClient client)
        {
            try
            {
                var cloudClientFactory = new Landdb.Infrastructure.CloudClientFactory();
                var token = cloudClientFactory.ResolveStorageCredentials();
                await token.SetBearerTokenAsync(client);

                var response = await client.GetAsync(scoutingDocumentUri);

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    
                    var list = new List<SiteDataListItemViewModel>();
                    var json = await response.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<T>(json);
                    return result;
                }
                else
                {
                    var fake = string.Empty;
                    var result = JsonConvert.DeserializeObject<T>(fake);
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Scouting Service Info Retrieval Failed.");
            }
        }

        private async void WriteScoutingData()
        {
            try
            {
                var baseDir = ApplicationEnvironment.UserAppDataPath; 
                var filePath = Path.Combine(baseDir, "Scouting");
                if (!Directory.Exists(filePath))
                {
                    System.IO.Directory.CreateDirectory(filePath);
                }

                string fileName = string.Format("{0}_{1}", ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear);
                filePath = Path.Combine(filePath, fileName);

                JsonSerializerSettings settings = new JsonSerializerSettings() { NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore, };
                ScoutingList newScoutingList = new ScoutingList() { Results = List, Revision = 0, TotalCount = List.Count() };
                var listData = JsonConvert.SerializeObject(newScoutingList, settings);
                File.WriteAllText(filePath, listData);
            }
            catch (Exception ex)
            {
                
            }
        }

        private ScoutingList LoadScoutingData()
        {
            var baseDir = ApplicationEnvironment.UserAppDataPath; 
            var fileDir = Path.Combine(baseDir, "Scouting");
            string fileName = string.Format("{0}_{1}", ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear);
            var filePath = Path.Combine(fileDir, fileName);

            if (Directory.Exists(fileDir) && File.Exists(filePath))
            {
                var data = File.ReadAllText(filePath);
                ScoutingList scoutingList = JsonConvert.DeserializeObject<ScoutingList>(data);

                return scoutingList;
            }
            else
            {
                return null;
            }
        }

        void CreateWorkOrderFromSelected()
        {
                var workOrderId = new WorkOrderId(ApplicationEnvironment.CurrentDataSourceId, Guid.NewGuid());

                DocumentDescriptor sourceDoc = new DocumentDescriptor() { DocumentType = base.SelectedListItem.DocumentType == "Sample" ? "ScoutingApplication" : base.SelectedListItem.DocumentType, Identity = base.SelectedListItem.Id, Name = base.SelectedListItem.Name };

                var vm = new Secondary.WorkOrder.WorkOrderEditorViewModel(clientEndpoint, dispatcher, workOrderId, ApplicationEnvironment.CurrentCropYear, null, sourceDoc);
                ScreenDescriptor descriptor = new ScreenDescriptor("Landdb.Views.Secondary.WorkOrder.WorkOrderEditorView", "create work order", vm);
                Messenger.Default.Send<ChangeScreenDescriptorMessage>(new ChangeScreenDescriptorMessage() { ScreenDescriptor = descriptor });
        }

        void CreateRecommendationFromSelected()
        {
            var recId = new RecommendationId(ApplicationEnvironment.CurrentDataSourceId, Guid.NewGuid());

            DocumentDescriptor sourceDoc = new DocumentDescriptor() { DocumentType = base.SelectedListItem.DocumentType == "Sample" ? "ScoutingApplication" : base.SelectedListItem.DocumentType, Identity = base.SelectedListItem.Id, Name = base.SelectedListItem.Name };

            var vm = new Secondary.Recommendation.RecommendationViewModel(clientEndpoint, dispatcher, recId, ApplicationEnvironment.CurrentCropYear, null, sourceDoc);
            ScreenDescriptor descriptor = new ScreenDescriptor("Landdb.Views.Secondary.Recommendation.RecommendationView", "create recommendation", vm);
            Messenger.Default.Send<ChangeScreenDescriptorMessage>(new ChangeScreenDescriptorMessage() { ScreenDescriptor = descriptor });
            
        }

        void GenerateReport()
        {
            if (SelectedListItem != null)
            {
                var vm = new SingleScoutingViewModel(clientEndpoint, dispatcher, SelectedListItem);
                Messenger.Default.Send(new ShowOverlayMessage()
                {
                    ScreenDescriptor = new ScreenDescriptor(typeof(Views.Secondary.Reports.Work_Order.SingleWorkOrderView), "viewReport", vm)
                });
            }
        }
    }

    public class ScoutingList
    {
        public IEnumerable<SiteDataListItemViewModel> Results { get; set; }
        public int Revision { get; set; }
        public int TotalCount { get; set; }
    }

}
