﻿using GalaSoft.MvvmLight;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Documents;
using Landdb.Domain.ReadModels.Recommendation;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Domain.ReadModels.WorkOrder;
using Landdb.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using Landdb.Client.Infrastructure.DisplayItems;

namespace Landdb.ViewModel.Production {

    /// <summary>
    /// Deprecated - see <see cref="SourceDataDisplayItem"/>
    /// </summary>
    public class WorkOrderUsedFilterConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            throw new NotImplementedException();
            //if (value == null) { return null; }

            //string itemString = value.ToString();
            //FieldInfo field = typeof(WorkOrderUsedAsSourceResponseEnum).GetField(itemString);
            //object[] attribs = field.GetCustomAttributes(typeof(DescriptionAttribute), false);

            //if (null != attribs && attribs.Length > 0)
            //    itemString = ((DescriptionAttribute)attribs[0]).Description;

            //return itemString;
        }

        /// <summary>
        /// Deprecated - see <see cref="SourceDataDisplayItem"/>
        /// </summary>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            throw new NotImplementedException();
            //if (value == null) { return WorkOrderUsedAsSourceResponseEnum.Null; }
            //if (value.ToString() == "Used in a work order") { return WorkOrderUsedAsSourceResponseEnum.UsedInWorkOrder; }
            //if (value.ToString() == "Used in an application") { return WorkOrderUsedAsSourceResponseEnum.UsedInApplication; }
            //if (value.ToString() == "Not used") { return WorkOrderUsedAsSourceResponseEnum.NotUsed; }
            //return WorkOrderUsedAsSourceResponseEnum.Null;
        }
    }


    public class WorkOrdersPageFilterViewModel : ViewModelBase, IPageFilterViewModel {
        IClientEndpoint clientEndpoint;
        string filterText = string.Empty;
        DateTime? startDate = null;
        DateTime? endDate = null;
        SourceDataDisplayItem usedAsSourceRecord = new SourceDataDisplayItem(SourceDataDisplayType.Null);

        public WorkOrdersPageFilterViewModel(IClientEndpoint clientEndpoint) {
            this.clientEndpoint = clientEndpoint;
        }

        public void ClearFilter() {
            FilterText = string.Empty;
            StartDate = null;
            EndDate = null;
            UsedAsSourceRecord = new SourceDataDisplayItem(SourceDataDisplayType.Null);
        }

        public void BeforeFilter() { }

		public void RefreshLists() { }

        public bool FilterItem(object item) {
            var x = item as WorkOrderListItemViewModel;
            if (x == null) { return false; }

            if (StartDate != null && x.StartDateTime < StartDate.Value.Date) { return false; }
            if (EndDate != null && x.StartDateTime > (EndDate.Value.Date + TimeSpan.FromDays(1) - TimeSpan.FromSeconds(1))) { return false; }

            var woDetailMaybe = clientEndpoint.GetView<WorkOrderView>(x.Id);

            if (woDetailMaybe.HasValue)
            {
                List<FlattenedTreeHierarchyItem> flattenedItemsList = new List<FlattenedTreeHierarchyItem>();
                var flattened = clientEndpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear)).GetValue(new FlattenedTreeHierarchyView());

                foreach (var czItem in woDetailMaybe.Value.CropZones)
                {
                    var flat = (from f in flattened.Items
                                where f.CropZoneId == czItem.Id
                                select f).FirstOrDefault();

					if (flat == null) {
						// check deleted cropzones
						flat = (from f in flattened.DeletedItems
								where f.CropZoneId == czItem.Id
								select f).FirstOrDefault();

						if (flat == null) { continue; }
					}

                    flattenedItemsList.Add(flat);
                }

                var filterContainsCZ = (from flatItem in flattenedItemsList
                                        where flatItem.CropZoneName.ToLower().Contains(FilterText.ToLower())
                                        select flatItem).Any();
                var filterContainsField = (from flatItem in flattenedItemsList
                                           where flatItem.FieldName.ToLower().Contains(FilterText.ToLower())
                                           select flatItem).Any();
                var filterContainsFarm = (from flatItem in flattenedItemsList
                                          where flatItem.FarmName.ToLower().Contains(FilterText.ToLower())
                                          select flatItem).Any();

                if (!string.IsNullOrWhiteSpace(FilterText) && filterContainsCZ == false && filterContainsField == false && filterContainsFarm == false && !x.Name.ToLower().Contains(FilterText.ToLower()))
                {
                    return false;
                }
            }

            //if (!string.IsNullOrWhiteSpace(filterText) && !x.Name.Contains(filterText)) {
            //    return false;
            //}

            if (UsedAsSourceRecord != null) { // i.e.: if there's even a reason to do any computation / model grabbing...
                var docUse = clientEndpoint.GetView<DocumentUseIndex>(x.Id);
                switch (UsedAsSourceRecord.SourceDataType) {
                    case SourceDataDisplayType.Null:
                        break;
                    case SourceDataDisplayType.UsedInWorkOrder:
                        if (!docUse.HasValue || docUse.Value.SourcedItems == null || !docUse.Value.SourcedItems.Any(i => i is WorkOrderId)) { return false; }
                        break;
                    case SourceDataDisplayType.UsedInApplication:
                        if (!docUse.HasValue || docUse.Value.SourcedItems == null || !docUse.Value.SourcedItems.Any(i => i is ApplicationId)) { return false; }
                        break;
                    case SourceDataDisplayType.NotUsed:
                        if (docUse.HasValue && docUse.Value.SourcedItems.Any()) { return false; }
                        break;
                    default:
                        break;
                }
            }

            return true;
        }

        public string FilterText {
            get { return filterText; }
            set {
                filterText = value;
                RaisePropertyChanged(() => FilterText);
            }
        }

        public DateTime? StartDate {
            get { return startDate; }
            set {
                startDate = value;
                RaisePropertyChanged(() => StartDate);
            }
        }

        public DateTime? EndDate {
            get { return endDate; }
            set {
                endDate = value;
                RaisePropertyChanged(() => EndDate);
            }
        }

        public List<SourceDataDisplayItem> Sources_WorkOrder => SourceDataDisplayItem.Sources_WorkOrder();
        public SourceDataDisplayItem UsedAsSourceRecord {
            get { return usedAsSourceRecord; }
            set {
                usedAsSourceRecord = value;
                RaisePropertyChanged(() => UsedAsSourceRecord);
            }
        }
    }
}
