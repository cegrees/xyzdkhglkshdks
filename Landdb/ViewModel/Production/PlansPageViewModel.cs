﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Secondary.Plan;
using Landdb.Domain.ReadModels.Plan;
using Landdb.ViewModel.Secondary.Reports.Plan;
using Landdb.Domain.ReadModels.Tags;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using Landdb.Resources;

namespace Landdb.ViewModel.Production {
    public class PlansPageViewModel : AbstractListPage<PlanListItemViewModel, PlanDetailsViewModel> {

        public PlansPageViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher)
            : base(clientEndpoint, dispatcher) {

                this.FilterModel = new PlansPageFilterViewModel(clientEndpoint);

            this.currentDataSourceId = new DataSourceId(ApplicationEnvironment.CurrentDataSourceId);
            this.currentCropYear = ApplicationEnvironment.CurrentCropYear;


            Messenger.Default.Register<PlansAddedMessage>(this, msg => {
                RefreshItemsList(() => {
                    var q = from li in ListItems
                            where li.Id == msg.LastPlanAdded
                            select li;
                    SelectedListItem = q.FirstOrDefault();
                });
            });

            CreateRecommendationFromSelectedCommand = new RelayCommand(CreateRecommendationFromSelected, () => base.SelectedListItem != null);
            CreateWorkOrderFromSelectedCommand = new RelayCommand(CreateWorkOrderFromSelected, () => base.SelectedListItem != null);
            CreateApplicationFromSelectedCommand = new RelayCommand(CreateApplicationFromSelected, () => base.SelectedListItem != null);
            CreatePlanFromSelectedCommand = new RelayCommand(CreatePlanFromSelected, () => base.SelectedListItem != null);
            MergePlansCommand = new RelayCommand(MergePlans, () => base.SelectedListItem != null);
            ShareFromSelectedCommand = new RelayCommand(ShareFromSelectedToTheWeb, () => base.SelectedListItem != null);
            PrintCommand = new RelayCommand(UpdateReport);
            CreateAppliedPlanCommand = new RelayCommand(CreateAppliedPlan);
        }

        public ICommand CreateAppliedPlanCommand { get; private set; }
        public ICommand CreatePlanSelectedCommand { get; private set; }
        public ICommand CreateRecommendationFromSelectedCommand { get; private set; }
        public ICommand CreateWorkOrderFromSelectedCommand { get; private set; }
        public ICommand CreateApplicationFromSelectedCommand { get; private set; }
        public ICommand CreatePlanFromSelectedCommand { get; private set; }
        public ICommand MergePlansCommand { get; private set; }
        public ICommand ShareFromSelectedCommand { get; private set; }
        public ICommand PrintCommand { get; private set; }

        public override string GetEntityName() {
            return Strings.Plan_Text.ToLower();
        }

        public override string GetPluralEntityName() {
            return Strings.PageHeader_Production_Plans.ToLower();
        }

        protected override async Task<ScreenDescriptor> CreateScreenDescriptorForNewItemCreation() {
            var planId = new CropPlanId(currentDataSourceId.Id, Guid.NewGuid());

            var vm = new PlannerViewModel(clientEndpoint, dispatcher, planId, currentCropYear, this);
            ScreenDescriptor descriptor = new ScreenDescriptor("Landdb.Views.Secondary.Plan.PlannerView", "create crop plan", vm);
            return descriptor;
        }

        protected override bool PerformItemRemoval(PlanListItemViewModel selectedItem) {
            var removeCommand = new RemovePlan(SelectedListItem.Id, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId));
            clientEndpoint.SendOne(removeCommand);
            return true;
        }

        protected override IDomainCommand CreateSetCharmCommand(PlanListItemViewModel selectedItem, string charmName) {
            if (selectedItem.CharmName == charmName) { return null; }
            selectedItem.CharmName = charmName;
            return new FlagPlan(selectedItem.Id, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), charmName);
        }

        protected override PlanDetailsViewModel CreateDetailsModel(PlanListItemViewModel selectedItem) {
            if (selectedItem == null) { return null; }

            Dictionary<string, List<string>> availableTimingEventTags = new Dictionary<string, List<string>>();
            var plantagviewMaybe = clientEndpoint.GetView<PlanProductTagProjectionView>(new CropYearId(currentDataSourceId.Id, currentCropYear));
			if (plantagviewMaybe.HasValue && plantagviewMaybe.Value.TagInfo != null) {
				foreach (var v in plantagviewMaybe.Value.TagInfo) {
					if (string.IsNullOrEmpty(v.TimingEvent)) { continue; }

					if (!availableTimingEventTags.ContainsKey(v.TimingEvent)) {
						availableTimingEventTags.Add(v.TimingEvent, new List<string>());
					}

					foreach (var x in v.Tags) {
						if (availableTimingEventTags.ContainsKey(v.TimingEvent)) {
							if (!string.IsNullOrEmpty(x.Key) && !availableTimingEventTags[v.TimingEvent].Contains(x.Key)) {
								availableTimingEventTags[v.TimingEvent].Add(x.Key);
							}
						}
					}
				}
			}

            var pMaybe = clientEndpoint.GetView<PlanView>(selectedItem.Id);

            if (pMaybe.HasValue) {
                foreach (var v in pMaybe.Value.Products) {
                    if (v.TimingEvent == null) { continue; }

                    if (!availableTimingEventTags.ContainsKey(v.TimingEvent)) {
                        availableTimingEventTags.Add(v.TimingEvent, new List<string>());
                    }

                    if (availableTimingEventTags.ContainsKey(v.TimingEvent)) {
                        if (!string.IsNullOrEmpty(v.TimingEventTag) && !availableTimingEventTags[v.TimingEvent].Contains(v.TimingEventTag)) {
                            availableTimingEventTags[v.TimingEvent].Add(v.TimingEventTag);
                        }
                    }
                }

                PlanDetailsViewModel plandetview = new PlanDetailsViewModel(clientEndpoint, dispatcher, pMaybe.Value, availableTimingEventTags);
                currentplandetview = plandetview;
                currentplandetview.AvailableTimingEventTags = new Dictionary<string, List<string>>();
                foreach (var key in availableTimingEventTags.Keys)
                {
                    currentplandetview.AvailableTimingEventTags.Add(key, availableTimingEventTags[key]);
                }
                //currentplandetview.AvailableTimingEventTags = availableTimingEventTags;
                return plandetview;
            }
            else {
                return null;
            }
        }

        protected override IEnumerable<PlanListItemViewModel> GetListItemModels() {
            var cropYearId = new CropYearId(currentDataSourceId.Id, currentCropYear);
            var plansMaybe = clientEndpoint.GetView<PlanList>(cropYearId);

            if (plansMaybe.HasValue) {
                var sortedProjectedPlans = plansMaybe.Value.Plans.OrderBy(x => x.Name).Select(x => new PlanListItemViewModel(x));
                return sortedProjectedPlans.ToArray();
            } else {
                return null;
            }
        }

        PlanDetailsViewModel currentplandetview;
        string createtype = string.Empty;

        //ObservableCollection<PlannedProductViewModel> copyProducts = new ObservableCollection<PlannedProductViewModel>();

        void GetPlanProductsForCopy() {
            ObservableCollection<FilterPlanProductDetailsViewModel> copyProducts = new ObservableCollection<FilterPlanProductDetailsViewModel>();

            foreach (var pp in currentplandetview.Products) {
                copyProducts.Add(new FilterPlanProductDetailsViewModel(pp));
            }
            List<TimingEvent> timingEvents2 = clientEndpoint.GetMasterlistService().GetTimingEventList().OrderBy(x => x.Order).ToList();
            List<TimingEvent> timingEvents = new List<TimingEvent>();
            timingEvents.Add(new TimingEvent());
            timingEvents.AddRange(timingEvents2);

            var vm = new FilterPlanProductsViewModel(copyProducts, timingEvents, currentplandetview.AvailableTimingEventTags, OnNewItemCreated, OnNewItemIgnored);
            Messenger.Default.Send<ShowOverlayMessage>(new ShowOverlayMessage() {
				ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Production.Popups.FilterPlanProductsView", "filter plans products", vm) 
			});
        }

        void OnNewItemCreated(ObservableCollection<ProductDetails> vm) {
            ObservableCollection<ProductDetails> filterdProducts = new ObservableCollection<ProductDetails>();
            if (vm != null) {
                filterdProducts = vm;
            }
            else {
                filterdProducts = null;
                return;
            }

            if (createtype == "Recommendation") {
                CreateRecommendationFromSelectedPage(filterdProducts);
                return;
            }

            if (createtype == "WorkOrder") {
                CreateWorkOrderFromSelectedPage(filterdProducts);
                return;
            }

            if (createtype == "Application") {
                CreateApplicationFromSelectedPage(filterdProducts);
                return;
            }
        }

        void OnNewItemIgnored() {
            //HasUnsavedChanges = false;
        }

        void CreateAppliedPlan() {
            var planId = new CropPlanId(currentDataSourceId.Id, Guid.NewGuid());
            var vm = new PlannerViewModel(clientEndpoint, dispatcher, planId, currentCropYear, this);
            vm.SetNewPlanWithAppliedProducts();
            //ScreenDescriptor descriptor = new ScreenDescriptor("Landdb.Views.Secondary.Plan.PlannerView", "create crop plan", vm);
            //Messenger.Default.Send<ChangeScreenDescriptorMessage>(new ChangeScreenDescriptorMessage() { ScreenDescriptor = descriptor });
        }

        void ShareFromSelectedToTheWeb() {
            var pMaybe = clientEndpoint.GetView<PlanView>(base.SelectedListItem.Id);
            if (pMaybe.HasValue) {
                Popups.ChoosePersonToShareViewModel vm = null;
                vm = new Popups.ChoosePersonToShareViewModel(clientEndpoint, dispatcher, pMaybe.Value, onPersonShared);
                Messenger.Default.Send(new ShowOverlayMessage() {
                    ScreenDescriptor = new ScreenDescriptor(typeof(Views.Production.Popups.ChoosePersonToShareView), "changeCropzones", vm)
                });
            }
        }

        private void onPersonShared(Popups.ChoosePersonToShareViewModel vm) {
            if (vm != null) {
                var planId = new CropPlanId(currentDataSourceId.Id, base.SelectedListItem.Id.Id);
                var personid = vm.Person.Id;
                var sharemodel = new { ShareePersonId = personid.Id, Id = planId };

                ShowIntegration(sharemodel, "api/plans/share");
            }
            Messenger.Default.Send<HideOverlayMessage>(new HideOverlayMessage());
        }

        public void ShowIntegration(object sharemodel, string uristring) {
            //TO DO :: CALL API TO GET PAGE
            Task<string> getMLZone = Task.Factory.StartNew<string>(() => {

                var dataSource = new DataSourceId(ApplicationEnvironment.CurrentDataSourceId);
                var cropYear = ApplicationEnvironment.CurrentCropYear;

                if (NetworkStatus.IsInternetAvailable()) {
                    var baseDocumentUri = string.Format("{0}{1}?datasourceid={2}", RemoteConfigurationSettings.GetBaseUri(), uristring, dataSource.Id);
                    //var action = string.Format("?dataSourceId={0}&cropYear={2}&nodeId={1}&nodeType=field", dataSource.Id, fieldView.Id.Id, cropYear);
                    var client = new HttpClient {
                        BaseAddress = new Uri(baseDocumentUri),
                    };

                    client.DefaultRequestHeaders.Add("Accept", "application / json");
                    var holder = PostData<string>(baseDocumentUri, client, sharemodel);
                    holder.Wait();

                    //var formattedLink = holder.Result;
                    //System.Diagnostics.Process.Start(formattedLink);

                    return holder.Result;
                }
                else {
                    return string.Empty;
                }
            });
        }

        private async Task<T> PostData<T>(string baseDocumentUri, System.Net.Http.HttpClient client, object sharemodel) {
            try {
                var cloudClientFactory = new Landdb.Infrastructure.CloudClientFactory();
                var token = cloudClientFactory.ResolveStorageCredentials();
                await token.SetBearerTokenAsync(client);

                // TODO: Might want to log this for debugging

                //HttpResponseMessage response = await client.PostAsJsonAsync(baseDocumentUri, JsonConvert.SerializeObject(sharemodel));
                HttpResponseMessage response = await client.PostAsJsonAsync(baseDocumentUri, sharemodel);

                if (response.StatusCode == HttpStatusCode.OK) {
                    // var json = await response.Content.ReadAsStringAsync();
                    // var result = JsonConvert.DeserializeObject<T>(json);
                    // return result;

                    // TODO: Read the response later...
                    var fake = string.Empty;
                    var result = JsonConvert.DeserializeObject<T>(fake);
                    return result;
                }
                else {
                    var fake = string.Empty;
                    var result = JsonConvert.DeserializeObject<T>(fake);
                    return result;
                }
            }
            catch (Exception ex) {
                throw new ApplicationException("Share Failed.");
            }
        }

        void CreatePlanFromSelected() {
            var planId = new CropPlanId(currentDataSourceId.Id, base.SelectedListItem.Id.Id);
            var vm = new PlannerViewModel(clientEndpoint, dispatcher, planId, currentCropYear, this);
            var view = clientEndpoint.GetView<PlanView>(SelectedListItem.Id).Value;
            vm.SetNewPlanWithThisData(view);

            Messenger.Default.Send<ChangeScreenDescriptorMessage>(new ChangeScreenDescriptorMessage() {
				ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Secondary.Plan.PlannerView", "create plan", vm)
			});
        }

        void CreateRecommendationFromSelected() {
            createtype = "Recommendation";
            GetPlanProductsForCopy();
        }

        void CreateWorkOrderFromSelected() {
            createtype = "WorkOrder";
            GetPlanProductsForCopy();
        }

        void CreateApplicationFromSelected() {
            createtype = "Application";
            GetPlanProductsForCopy();
        }

        void CreateRecommendationFromSelectedPage(ObservableCollection<ProductDetails> filterdProducts) {
            var recomendationId = new RecommendationId(currentDataSourceId.Id, Guid.NewGuid());

            DocumentDescriptor sourceDoc = new DocumentDescriptor() { 
				DocumentType = DocumentDescriptor.PlanTypeString, 
				Identity = base.SelectedListItem.Id,
				Name = base.SelectedListItem.Name 
			};

            var vm = new Secondary.Recommendation.RecommendationViewModel(clientEndpoint, dispatcher, recomendationId, currentCropYear, filterdProducts, sourceDoc);
            Messenger.Default.Send<ChangeScreenDescriptorMessage>(new ChangeScreenDescriptorMessage() {
				ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Secondary.Recommendation.RecommendationView", "create recommendation", vm)
			});
        }

        void CreateWorkOrderFromSelectedPage(ObservableCollection<ProductDetails> filterdProducts) {
            var workOrderId = new WorkOrderId(currentDataSourceId.Id, Guid.NewGuid());

            DocumentDescriptor sourceDoc = new DocumentDescriptor() { 
				DocumentType = DocumentDescriptor.PlanTypeString, 
				Identity = base.SelectedListItem.Id,
				Name = base.SelectedListItem.Name 
			};

            var vm = new Secondary.WorkOrder.WorkOrderEditorViewModel(clientEndpoint, dispatcher, workOrderId, currentCropYear, filterdProducts, sourceDoc);
            Messenger.Default.Send<ChangeScreenDescriptorMessage>(new ChangeScreenDescriptorMessage() {
				ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Secondary.WorkOrder.WorkOrderEditorView", "create work order", vm) 
			});
        }

        void CreateApplicationFromSelectedPage(ObservableCollection<ProductDetails> filterdProducts) {
            var applicationId = new ApplicationId(currentDataSourceId.Id, Guid.NewGuid());

            DocumentDescriptor sourceDoc = new DocumentDescriptor() { DocumentType = DocumentDescriptor.PlanTypeString, Identity = base.SelectedListItem.Id, Name = base.SelectedListItem.Name };

            var vm = new Secondary.Application.ApplicatorViewModel(clientEndpoint, dispatcher, applicationId, currentCropYear, filterdProducts, sourceDoc);
            Messenger.Default.Send<ChangeScreenDescriptorMessage>(new ChangeScreenDescriptorMessage() {
				ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Secondary.Application.ApplicatorView", "create application", vm)
			});
        }

        void UpdateReport()
        {
            //get map from ViewModel
            if (SelectedListItem != null && SelectedListItem.Id != null)
            {
                var vm = new SinglePlanViewModel(clientEndpoint, SelectedListItem.Id);
                Messenger.Default.Send<ShowOverlayMessage>(new ShowOverlayMessage()
                {
                    ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Secondary.Reports.Work_Order.SingleWorkOrderView", "viewReport", vm)
                });
            }
        }
        void MergePlans() {
            //get map from ViewModel
            var vm = new PlanMergeViewModel(clientEndpoint, dispatcher, ApplicationEnvironment.CurrentCropYear);
            Messenger.Default.Send<ShowOverlayMessage>(new ShowOverlayMessage() {
                ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Secondary.Plan.PlanMergeView", "mergePlans", vm)
            });
        }
    }
}