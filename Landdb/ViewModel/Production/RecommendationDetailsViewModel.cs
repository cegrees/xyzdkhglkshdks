﻿using AgC.UnitConversion;
using AgC.UnitConversion.Area;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Client.Spatial;
using Landdb.Domain.ReadModels.Map;
using Landdb.Domain.ReadModels.Product;
using Landdb.Domain.ReadModels.Recommendation;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Production.Popups;
using Landdb.ViewModel.Production.Popups.Matching;
using Landdb.ViewModel.Production.Popups.Recommendation;
using NLog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Shapes;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Production {
	public class RecommendationDetailsViewModel : ViewModelBase {

		readonly IClientEndpoint clientEndpoint;
		readonly Dispatcher dispatcher;

        readonly CropYearId currentCropYearId;

        readonly IMasterlistService masterlist;
		readonly RecommendationView recommendationView;
		readonly RecommendationListItemViewModel selectedItem;

		ProductDetails selectedProduct;
		Dictionary<Guid, UnknownRecommendedProduct> unknownProducts;

		Logger log = LogManager.GetCurrentClassLogger();

		public RecommendationDetailsViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, DateTime? proposedDate, DateTime? expirationDate, List<RecommendedProduct> products, List<RecommendedCropZone> cropzones, List<DocumentDescriptor> sources, List<UnknownRecommendedProduct> uProducts, List<UnknownCropZone> uCropZones) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			this.masterlist = clientEndpoint.GetMasterlistService();

			if (proposedDate.HasValue) {
				ProposedDate = proposedDate.Value.ToString(@"MMMM d, yyyy");
			}

			if (expirationDate.HasValue) {
				ExpirationDate = expirationDate.Value.ToString(@"MMMM d, yyyy");
			}

			ValidDateRange = RecommendationView.CreateValidDateRangeLong(proposedDate, expirationDate);


            currentCropYearId = new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear);

            AreaMeasure totalArea = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(0) as AreaMeasure;
			CropZones = new ObservableCollection<CropZoneDetails>();

			var fTreeView = clientEndpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear));

			foreach (var c in cropzones) {
				var name = c.Name;

				if (fTreeView.HasValue) {
					var fti = fTreeView.Value.Items.Where(x => x.CropZoneId == c.Id).FirstOrDefault();

					if (fti != null) {
						var farm = clientEndpoint.GetView<FarmDetailsView>(fti.FarmId);
						var field = clientEndpoint.GetView<FieldDetailsView>(fti.FieldId);

						if (farm.HasValue && field.HasValue) {
							name = $"{farm.Value.FarmNameByCropYear[currentCropYearId.Id]}\\{field.Value.FieldNameByCropYear[ApplicationEnvironment.CurrentCropYear]}\\{fti.CropZoneName}";
						}
					}
				}

                var czDetails = clientEndpoint.GetView<CropZoneDetailsView>(c.Id).HasValue ? clientEndpoint.GetView<CropZoneDetailsView>(c.Id).Value : null;
                double area = 0.0;
                if (czDetails != null) {
                    area = czDetails.ReportedArea != null ? (double)czDetails.ReportedArea : czDetails.BoundaryArea != null ? (double)czDetails.BoundaryArea : 0;
                }
                else {
                    area = c.Area.Value;
                }
                double coveragePercent = area != 0 ? (c.Area.Value / area) * 100 : 0;

                var mapItem = clientEndpoint.GetView<ItemMap>(c.Id);
				string mapData = null;
				Path shapePreview = null;

				if (mapItem.HasValue && mapItem.Value.MostRecentMapItem != null) {
					mapData = mapItem.Value.MostRecentMapItem.MapData;

					dispatcher.BeginInvoke(new Action(() => {
						shapePreview = WpfTransforms.CreatePathFromMapData(new[] { mapData }, 40);
						CropZones.Add(new CropZoneDetails() { Id = c.Id, Name = name, Area = c.Area, ShapePreview = shapePreview, Resolved = true, Percent = string.Format("{0}%", coveragePercent.ToString("N0")) });
					}));
				} else {
					CropZones.Add(new CropZoneDetails() { Id = c.Id, Name = name, Area = c.Area, Resolved = true, Percent = string.Format("{0}%", coveragePercent.ToString("N0")) });
				}

				if (totalArea == null) {
					totalArea = c.Area;
				} else if (c.Area != null) {
					totalArea = (AreaMeasure)(totalArea + c.Area);
				}
			}

			TotalArea = totalArea;
			foreach (var c in uCropZones) {
				CropZones.Add(new CropZoneDetails() { Id = null, Name = c.Name, Area = AreaMeasure.CreateMeasure(c.AreaValue, c.AreaUnit) as AreaMeasure, Resolved = false, UnknownInfo = c });
			}

			Products = new ObservableCollection<ProductDetails>();
			unknownProducts = new Dictionary<Guid, UnknownRecommendedProduct>();
			IProductUnitResolver productUnits = clientEndpoint.GetProductUnitResolver();

			foreach (var p in uProducts) {
				Guid trackingId = Guid.NewGuid();

				Products.Add(new ProductDetails() {
					ProductName = p.Name,
					RateMeasure = null,
					AreaUnit = null,
					AppliedArea = null,
					TotalProduct = null,
					ID = null,
					ProductAreaPercent = null,
					TrackingId = trackingId,
					ApplicationMethod = null,
					RatePerTank = null,
					TargetPest = null,
					Resolved = false
				});

				unknownProducts.Add(trackingId, p);
			}

			foreach (var p in products) {
                var pest = p.TargetPest != null ? masterlist.GetPestList().Where(x => x.Name == p.TargetPest.CommonName && x.LatinName == p.TargetPest.LatinName).FirstOrDefault() : null;
				var targetPest = pest != null ? new MiniPest() { id = pest.Id, Name = pest.Name, LatinList = pest.LatinName } : null;

				var masterlistProd = masterlist.GetProduct(p.ProductId);
				var userCreatedList = clientEndpoint.GetView<UserCreatedProductList>(new DataSourceId(ApplicationEnvironment.CurrentDataSourceId)).GetValue(new UserCreatedProductList());
				var userCreated = userCreatedList.UserCreatedProducts.Where(x => x.Id == p.ProductId.Id).FirstOrDefault();

				string prodName = null;

				if (masterlistProd == null && userCreated == null) {
					log.Warn("Couldn't find a masterlist or user-created product for ID {0}. (Product name: {1} ({2})", p.ProductId, p.ProductName, p.ManufacturerName);
					prodName = $"[{Strings.UnknownProduct_Text}]";
				} else {
					prodName = masterlistProd != null ? masterlistProd.Name : userCreated.Name;
				}

				Measure rateMeasure = null;
				if (p.RatePerAreaValue.HasValue) {
					rateMeasure = UnitFactory.GetUnitByName(p.RatePerAreaUnit).GetMeasure((double)p.RatePerAreaValue.Value, p.ProductDensity);
				}

				Measure totalProduct = null;
				if (p.TotalProductValue.HasValue) {
					totalProduct = UnitFactory.GetUnitByName(p.TotalProductUnit).GetMeasure((double)p.TotalProductValue.Value, p.ProductDensity);

					var psu = productUnits.GetProductPackageUnit(p.ProductId);
					if (psu.Name != p.TotalProductUnit) {
						totalProduct = psu.GetMeasure(totalProduct.GetValueAs(psu), p.ProductDensity);
					}
				} else {
					var psu = productUnits.GetProductPackageUnit(p.ProductId);
					totalProduct = psu.GetMeasure(0.0);
				}

				Measure ratePerTank = null;
				if (p.RatePerTankValue.HasValue) {
					ratePerTank = UnitFactory.GetUnitByName(p.RatePerTankUnit).GetMeasure((double)p.RatePerTankValue.Value, p.ProductDensity);
				}

				Measure appliedArea = null;
				if (p.AppliedAreaValue.HasValue) {
					appliedArea = UnitFactory.GetUnitByName(p.AppliedAreaUnit).GetMeasure((double)p.AppliedAreaValue.Value, null);
				}

				Products.Add(new ProductDetails() {
					ProductName = prodName,
					RateMeasure = rateMeasure,
					AreaUnit = string.IsNullOrEmpty(p.AreaUnit) ? UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit) : UnitFactory.GetUnitByName(p.AreaUnit),
					AppliedArea = appliedArea,
					TotalProduct = totalProduct,
					ID = p.ProductId,
					ProductAreaPercent = p.CoveragePercent,
					TrackingId = p.TrackingId,
					ApplicationMethod = clientEndpoint.GetMasterlistService().GetApplicationMethodList().Where(x => x.Name == p.ApplicationMethod).FirstOrDefault(),
					RatePerTank = ratePerTank,
					TargetPest = targetPest,
					Resolved = true
				});
			}

			EditRecommendationDates = new RelayCommand(this.RecommendationDatesEdit);
			EditFieldSelection = new RelayCommand(this.FieldSelectionEdit);
			ChangeTankCommand = new RelayCommand(this.ChangeTankSelection);
			EditSourceInfo = new RelayCommand(this.EditRecommendationDocuments);
			EditAuthorizationInfo = new RelayCommand(this.AuthorizerInfoEdit);
			EditApplicatorInfo = new RelayCommand(this.ApplicatorInfoEdit);
			EditNotesInfo = new RelayCommand(this.NotesInfoEdit);
			EditNameAndNumberInfo = new RelayCommand(this.NameAndNumberInfoEdit);
			AddProduct = new RelayCommand(this.AddRecommendedProduct);

			Sources = new List<DocumentDescriptor>(sources ?? new List<DocumentDescriptor>());
		}

		public RecommendationDetailsViewModel(IClientEndpoint clientEndpoint, RecommendationListItemViewModel selectedItem, RecommendationView recommendationView)
			: this(clientEndpoint, Application.Current.Dispatcher, recommendationView.ProposedDate, recommendationView.ExpirationDate, recommendationView.Products, recommendationView.CropZones, recommendationView.Sources, recommendationView.unknownProducts, recommendationView.unknownCropZones) {
			this.recommendationView = recommendationView;
			this.selectedItem = selectedItem;
			TankDetails = recommendationView.TankInformation;
			NameAndNumber = RecommendationView.CreateNameAndNumber(recommendationView.RecTitle, recommendationView.RecNumber);
			CreatedDate = recommendationView.CreatedDateTime;
			Notes = recommendationView.Notes;

			unknownAuthorizer = recommendationView.unknownAuthorization;
			Authorizer = recommendationView.Authorization != null ? recommendationView.Authorization.PersonName : string.Empty;
			AuthorizeDate = recommendationView.Authorization != null ? recommendationView.Authorization.AuthorizationDate : new DateTime();

			unknownApplicators = recommendationView.unknownApplicators;
			Applicators = recommendationView.Applicators;
			SetProductStrategyString(recommendationView.ApplicationStrategy);
		}

		public RelayCommand EditRecommendationDates { get; }
		public RelayCommand EditFieldSelection { get; }
		public RelayCommand ChangeTankCommand { get; }
		public RelayCommand EditSourceInfo { get; }
		public RelayCommand EditAuthorizationInfo { get; }
		public RelayCommand EditApplicatorInfo { get; }
		public RelayCommand EditNotesInfo { get; }
		public RelayCommand EditNameAndNumberInfo { get; }
		public RelayCommand AddProduct { get; }

		public string NameAndNumber { get; set; }
		public string ProposedDate { get; set; }
		public string ExpirationDate { get; set; }
		public DateTime CreatedDate { get; set; }
		public string ValidDateRange { get; set; }
		public string Authorizer { get; set; }
		public DateTime AuthorizeDate { get; set; }
		public string Notes { get; set; }
		public UnknownAuthorization unknownAuthorizer { get; set; }

		public bool AuthorizationResolved {
			get { return (unknownAuthorizer == null); }
		}

		public List<ApplicationApplicator> Applicators { get; set; }
		public List<UnknownApplicator> unknownApplicators { get; set; }

		public bool ApplicatorsResolved {
			get { return ((unknownApplicators == null) || (unknownApplicators.Count == 0)); }
		}

		public ObservableCollection<ProductDetails> Products { get; set; }
		public ObservableCollection<CropZoneDetails> CropZones { get; set; }

		public Measure TotalArea { get; set; }
		public List<DocumentDescriptor> Sources { get; set; }
		public ApplicationTankInformation TankDetails { get; set; }
		public string ApplicationType { get; set; }

		public string TankDisplay {
			get {
				if (TankDetails == null) {
					return string.Empty;
				} else {
					return string.Format(@"{0} {1}", TankDetails.TankSizeValue.ToString("N2"), UnitFactory.GetUnitByName(TankDetails.TankSizeUnit).AbbreviatedDisplay);
				}
			}
		}

		public string CarrierPerAreaDisplay {
			get {
				if (TankDetails == null) {
					return string.Empty;
				} else {
					return string.Format(@"{0} {1}", TankDetails.CarrierPerAreaValue.ToString("N2"), UnitFactory.GetUnitByName(TankDetails.CarrierPerAreaUnit).AbbreviatedDisplay);
				}
			}
		}

		public bool Resolved {
			get {
				bool resolved = (unknownAuthorizer == null) && ((unknownApplicators == null) || (unknownApplicators.Count == 0));
				foreach (var p in Products)
					resolved = resolved && p.Resolved;
				foreach (var c in CropZones)
					resolved = resolved && c.Resolved;
				return resolved;
			}
		}
		public string AuthorizeDateDisplay {
			get {
				return string.Format(@"{0}", AuthorizeDate != new DateTime() ? AuthorizeDate.ToShortDateString() : string.Empty);
			}
		}

		public ProductDetails SelectedProduct {
			get { return selectedProduct; }
			set {
				selectedProduct = value;
				if (selectedProduct != null) {
					RecommendedProductEdit(selectedProduct);
				}
			}
		}

		public class ProductDetails : ViewModelBase {
			Measure totalProduct;
			public ProductDetails() { }
			public string ProductName { get; set; }
			public Measure RateMeasure { get; set; }
			public IUnit AreaUnit { get; set; }
			public Measure AppliedArea { get; set; }
			public ProductId ID { get; set; }
			public decimal? ProductAreaPercent { get; set; }
			public Guid TrackingId { get; set; }
			public MiniPest TargetPest { get; set; }
			public ApplicationMethod ApplicationMethod { get; set; }
			public Measure RatePerTank { get; set; }
			public bool Resolved { get; set; }
			public UnknownRecommendedProduct UnknownInfo { get; set; }

			public Measure TotalProduct {
				get { return totalProduct; }
				set {
					totalProduct = value;
					RaisePropertyChanged(() => TotalProduct);
				}
			}

			public string RateDisplay {
				get {
					if (RateMeasure != null) {
						string rateInfo = $"{RateMeasure.Value:n2} {RateMeasure.Unit.FullDisplay} / {AreaUnit.AbbreviatedDisplay}";
						string areaInfo = (AppliedArea != null) ? (string.Format(" ({1} {2})", AppliedArea.Value, AppliedArea.Unit.AbbreviatedDisplay)) : string.Empty;
						return string.Format("{0}{1}", rateInfo, areaInfo);
					} else {
						return string.Empty;
					}
				}
			}

			public string Rate {
				get {
					if (RateMeasure != null) {
						return string.Format(@"{0} / {1}", RateMeasure.AbbreviatedDisplay, AreaUnit.FullDisplay);
					} else {
						return string.Empty;
					}
				}
			}
		}

		public class CropZoneDetails {
			public CropZoneId Id { get; set; }
			public string Name { get; set; }
			public AreaMeasure Area { get; set; }
			public Path ShapePreview { get; set; }
            public string Percent { get; set; }
            public bool Resolved { get; set; }
			public UnknownCropZone UnknownInfo { get; set; }
		}

		void RecommendationDatesEdit() {
			if (recommendationView != null) {
				// Lockout editing if record comes from Agrian
				if (recommendationView.RecSource == GlobalStrings.RecommendationType_Agrian) { return; }
				ChangeRecValidDateRangeViewModel vm = new ChangeRecValidDateRangeViewModel(clientEndpoint, recommendationView);
				Messenger.Default.Send(new ShowPopupMessage() {
					ScreenDescriptor = new ScreenDescriptor(typeof(Views.Production.Popups.Recommendation.ChangeValidDateRangeView), "editValidDateRange", vm)
				});
			}
		}

		void FieldSelectionEdit() {
			if (recommendationView != null) {
				if ((recommendationView.unknownCropZones != null) && (recommendationView.unknownCropZones.Count > 0)) {
					SelectUnknownCropZoneViewModel vm = new SelectUnknownCropZoneViewModel(clientEndpoint, dispatcher, recommendationView, this);
					Messenger.Default.Send(new ShowPopupMessage() {
						ScreenDescriptor = new ScreenDescriptor(typeof(Views.Production.Popups.Matching.SelectUnknownCropZoneView), "resolveCropzones", vm)
					});
				} else {
					ChangeRecFieldsViewModel vm = new ChangeRecFieldsViewModel(clientEndpoint, dispatcher, ApplicationEnvironment.CurrentCropYear, recommendationView.Id, recommendationView.CropZones, this);
					Messenger.Default.Send(new ShowOverlayMessage() {
						ScreenDescriptor = new ScreenDescriptor(typeof(Views.Shared.Popups.ChangeCropZoneSelectionView), "changeCropzones", vm)
					});
				}
			}
		}

		void ChangeTankSelection() {
			if (recommendationView != null) {
				ChangeTankInfo vm = new ChangeTankInfo(clientEndpoint, recommendationView, TotalArea);
				Messenger.Default.Send(new ShowPopupMessage() {
					ScreenDescriptor = new ScreenDescriptor(typeof(Views.Production.Popups.ChangeTankInfoView), "changeTankInfo", vm)
				});
			}
		}

		void RecommendedProductEdit(ProductDetails prod) {
			if (recommendationView != null) {
				if (prod.Resolved) {
					// Lockout editing if record comes from Agrian
					if (recommendationView.RecSource == GlobalStrings.RecommendationType_Agrian)
						return;
					var vm = new ChangeRecProductViewModel(clientEndpoint, this.dispatcher, recommendationView, this, prod);
					Messenger.Default.Send(new ShowOverlayMessage() {
						ScreenDescriptor = new ScreenDescriptor(typeof(Views.Production.Popups.Recommendation.ChangeRecommendedProductView), "changeRecommendedProduct", vm)
					});
				} else {
					var vm = new ResolveUnknownRecommendedProductViewModel(clientEndpoint, this.dispatcher, recommendationView, this, prod, unknownProducts[prod.TrackingId]);
					Messenger.Default.Send(new ShowOverlayMessage() {
						ScreenDescriptor = new ScreenDescriptor(typeof(Views.Production.Popups.Matching.ResolveUnknownRecommendedProductView), "resolveRecommendedProduct", vm)
					});
				}
			}
		}

		void AddRecommendedProduct() {
			if (recommendationView != null) {
				// Lockout editing if record comes from Agrian
				if (recommendationView.RecSource == GlobalStrings.RecommendationType_Agrian) { return; }
				var vm = new ChangeRecProductViewModel(clientEndpoint, this.dispatcher, recommendationView, this, null);
				Messenger.Default.Send(new ShowOverlayMessage() {
					ScreenDescriptor = new ScreenDescriptor(typeof(Views.Production.Popups.Recommendation.ChangeRecommendedProductView), "changeRecommendedProduct", vm)
				});
			}
		}

		void AuthorizerInfoEdit() {
			if (recommendationView != null) {
				if (recommendationView.unknownAuthorization != null) {
					ResolveUnknownAuthorizationViewModel vm = new ResolveUnknownAuthorizationViewModel(clientEndpoint, dispatcher, recommendationView, recommendationView.unknownAuthorization, this);
					Messenger.Default.Send(new ShowPopupMessage() {
						ScreenDescriptor = new ScreenDescriptor(typeof(Views.Production.Popups.Matching.ResolveUnknownAuthorizationView), "resolveAuthorizer", vm)
					});
				} else {
					ChangeAuthorizerInfo vm = new ChangeAuthorizerInfo(clientEndpoint, dispatcher, recommendationView);
					Messenger.Default.Send(new ShowPopupMessage() {
						ScreenDescriptor = new ScreenDescriptor(typeof(Views.Production.Popups.ChangeAuthorizerInfoView), "changeAuthorizer", vm)
					});
				}
			}
		}

		void ApplicatorInfoEdit() {
			if (recommendationView != null) {
				if ((recommendationView.unknownApplicators != null) && (recommendationView.unknownApplicators.Count > 0)) {
					ResolveUnknownApplicatorViewModel vm = new ResolveUnknownApplicatorViewModel(clientEndpoint, dispatcher, recommendationView, recommendationView.unknownApplicators.ToArray(), this);
					Messenger.Default.Send(new ShowPopupMessage() {
						ScreenDescriptor = new ScreenDescriptor(typeof(Views.Production.Popups.Matching.ResolveUnknownApplicatorView), "resolveApplicator", vm)
					});
				} else {
					var applicators = new ObservableCollection<ApplicationApplicator>(recommendationView.Applicators);
					ChangeRecApplicatorViewModel vm = new ChangeRecApplicatorViewModel(clientEndpoint, dispatcher, applicators, recommendationView);
					Messenger.Default.Send(new ShowPopupMessage() {
						ScreenDescriptor = new ScreenDescriptor(typeof(Views.Secondary.PopUp.IncludeApplicatorView), "changeApplicator", vm)
					});
				}
			}
		}

		void NotesInfoEdit() {
			if (recommendationView != null) {
				DialogFactory.ShowNotesChangeDialog(Notes, newNotes => {
					var cmd = new ChangeRecommendationNotes(recommendationView.Id, clientEndpoint.GenerateNewMetadata(), newNotes);
					clientEndpoint.SendOne(cmd);

					Notes = newNotes;
					RaisePropertyChanged(() => Notes);
				});
			}
		}

		void NameAndNumberInfoEdit() {
			if (recommendationView != null) {
				// Lockout editing if record comes from Agrian
				if (recommendationView.RecSource == GlobalStrings.RecommendationType_Agrian) { return; }
				ChangeRecNameAndNumberViewModel vm = new ChangeRecNameAndNumberViewModel(clientEndpoint, recommendationView);
				Messenger.Default.Send(new ShowPopupMessage() {
					ScreenDescriptor = new ScreenDescriptor(typeof(Views.Production.Popups.Recommendation.ChangeNameAndNumberView), "editNameAndNumber", vm)
				});
			}
		}

        void EditRecommendationDocuments() {
            EditDocuments_Generic(
                doc => { return new AddDocumentToRecommendation(recommendationView.Id, clientEndpoint.GenerateNewMetadata(), doc); },
                doc => { return new RemoveDocumentFromRecommendation(recommendationView.Id, clientEndpoint.GenerateNewMetadata(), doc); });
        }

        void EditDocuments_Generic(Func<DocumentDescriptor, IDomainCommand> addCommandFactory, Func<DocumentDescriptor, IDomainCommand> removeCommandFactory) {
            var vmBase = new Shared.DocumentListPageViewModel(clientEndpoint, dispatcher, currentCropYearId.Id, Sources);
            var vm = new Overlays.DocumentListOverlayViewModel(vmBase, shouldSave => {
                if (shouldSave) {
                    foreach (var added in vmBase.AddedDocuments) {
                        Sources.Add(added);
                        var addCmd = addCommandFactory(added);
                        clientEndpoint.SendOne(addCmd);
                    }

                    foreach (var removed in vmBase.RemovedDocuments) {
                        Sources.RemoveAll(x => x.Identity == removed.Identity);
                        var remCmd = removeCommandFactory(removed);
                        clientEndpoint.SendOne(remCmd);
                    }

                    RaisePropertyChanged(() => Sources);
                }

                Messenger.Default.Send(new HideOverlayMessage());
            });

            Messenger.Default.Send(new ShowOverlayMessage() {
                ScreenDescriptor = new ScreenDescriptor(typeof(Views.Overlays.DocumentListOverlayView), "editdocuments", vm)
            });
        }

        public void AddToProducts(ProductDetails product) {
			Products.Add(product);
			RaisePropertyChanged(() => Products);
		}

		public void ChangeInFieldSelection(List<CropZoneArea> added, List<CropZoneArea> changed, List<CropZoneId> removed) {
			foreach (var area in added) {
				var holder = UnitFactory.GetUnitByName(area.AreaUnit).GetMeasure(area.AreaValue);
				TotalArea = TotalArea.Unit.GetMeasure(TotalArea.Value + holder.CreateAs(TotalArea.Unit).Value);
				var czDetails = clientEndpoint.GetView<CropZoneDetailsView>(area.CropZoneId);

                double area2 = 0.0;
                if (czDetails.HasValue) {
                    area2 = czDetails.Value.ReportedArea != null ? (double)czDetails.Value.ReportedArea : czDetails.Value.BoundaryArea != null ? (double)czDetails.Value.BoundaryArea : 0;
                }
                else {
                    area2 = area.AreaValue;
                }
                double coveragePercent = area2 != 0 ? (area.AreaValue / area2) * 100 : 0;

                var details = new CropZoneDetails() { Id = area.CropZoneId, Area = new AreaMeasure(area.AreaValue, (AreaUnit)UnitFactory.GetUnitByName(area.AreaUnit)), Name = czDetails.Value.Name, Resolved = true };
				CropZones.Add(details);
			}

			foreach (var area in changed) {
				var cz = (from c in CropZones
						  where c.Id == area.CropZoneId
						  select c).FirstOrDefault();
				cz.Area = new AreaMeasure(UnitFactory.GetUnitByName(area.AreaUnit).GetMeasure(area.AreaValue).Value, (AreaUnit)UnitFactory.GetUnitByName(area.AreaUnit));
			}

			foreach (var id in removed) {
				var area = clientEndpoint.GetView<CropZoneDetailsView>(id);
				CropZones.Remove(CropZones.Where(x => x.Id == id).FirstOrDefault());
			}

			RefreshFieldsAndProducts();
		}

		public void RefreshFieldsAndProducts() {
			AreaMeasure totalArea = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(0) as AreaMeasure;
			CropZones.Clear();

			if (recommendationView.CropZones != null) {
				foreach (var c in recommendationView.CropZones) {

                    var czDetails = clientEndpoint.GetView<CropZoneDetailsView>(c.Id).HasValue ? clientEndpoint.GetView<CropZoneDetailsView>(c.Id).Value : null;
                    double area = 0.0;
                    if (czDetails != null) {
                        area = czDetails.ReportedArea != null ? (double)czDetails.ReportedArea : czDetails.BoundaryArea != null ? (double)czDetails.BoundaryArea : 0;
                    }
                    else {
                        area = c.Area.Value;
                    }
                    double coveragePercent = area != 0 ? (c.Area.Value / area) * 100 : 0;

                    var mapItem = clientEndpoint.GetView<ItemMap>(c.Id);
					string mapData = null;
					Path shapePreview = null;

					if (mapItem.HasValue && mapItem.Value.MostRecentMapItem != null) {
						mapData = mapItem.Value.MostRecentMapItem.MapData;

						dispatcher.BeginInvoke(new Action(() => {
							shapePreview = WpfTransforms.CreatePathFromMapData(new[] { mapData }, 40);
							CropZones.Add(new CropZoneDetails() { Id = c.Id, Name = c.Name, Area = c.Area, ShapePreview = shapePreview, Resolved = true, Percent = string.Format("{0}%", coveragePercent.ToString("N0")) });
						}));
					} else {
						CropZones.Add(new CropZoneDetails() { Id = c.Id, Name = c.Name, Area = c.Area, Resolved = true, Percent = string.Format("{0}%", coveragePercent.ToString("N0")) });
					}

					if (totalArea == null) {
						totalArea = c.Area;
					} else if (c.Area != null) {
						totalArea = (AreaMeasure)(totalArea + c.Area);
					}
				}
			}

			TotalArea = totalArea;

			if (recommendationView.unknownCropZones != null) {
				foreach (var c in recommendationView.unknownCropZones) {
					CropZones.Add(new CropZoneDetails() {
						Id = null,
						Name = c.Name,
						Area = Measure.CreateMeasure(c.AreaValue, c.AreaUnit) as AreaMeasure,
						Resolved = false,
						UnknownInfo = c
					});
				}
			}

			RaisePropertyChanged(() => CropZones);
			RaisePropertyChanged(() => TotalArea);

			foreach (var prod in Products) {
				if (!prod.Resolved) { continue; }

				var mlProd = masterlist.GetProduct(prod.ID);

				if (mlProd == null) { continue; }

				switch (recommendationView.ApplicationStrategy) {
					case ProductApplicationStrategy.ByRatePerArea:
						var totalProductValue = prod.RateMeasure.Unit.GetMeasure(prod.RateMeasure.Value * TotalArea.Value, mlProd.Density).GetValueAs(prod.TotalProduct.Unit);
						prod.TotalProduct = prod.TotalProduct.Unit.GetMeasure(totalProductValue, mlProd.Density);
						prod.AppliedArea = TotalArea;
						break;
					case ProductApplicationStrategy.ByRatePerTank:
						var TotalTanks = recommendationView.TankInformation.CarrierPerAreaValue * (decimal)TotalArea.Value / recommendationView.TankInformation.TankSizeValue;
						totalProductValue = prod.RatePerTank.Unit.GetMeasure(prod.RatePerTank.Value * (double)TotalTanks, mlProd.Density).GetValueAs(prod.TotalProduct.Unit);
						prod.TotalProduct = prod.TotalProduct.Unit.GetMeasure(totalProductValue, mlProd.Density);
						prod.RateMeasure = prod.RateMeasure.Unit.GetMeasure(prod.TotalProduct.GetValueAs(prod.RateMeasure.Unit) / TotalArea.Value, mlProd.Density);
						prod.AppliedArea = TotalArea;
						break;
					case ProductApplicationStrategy.ByTotalProduct:
						prod.RateMeasure = prod.RateMeasure.Unit.GetMeasure(prod.TotalProduct.GetValueAs(prod.RateMeasure.Unit) / TotalArea.Value, mlProd.Density);
						prod.AppliedArea = TotalArea;
						break;
				}
			}

			RaisePropertyChanged(() => Products);

			if (selectedItem != null) {
				selectedItem.Refresh();
			}
		}

		public void RefreshAuthorization() {
			Authorizer = recommendationView.Authorization != null ? recommendationView.Authorization.PersonName : string.Empty;
			AuthorizeDate = recommendationView.Authorization != null ? recommendationView.Authorization.AuthorizationDate : new DateTime();
			unknownAuthorizer = recommendationView.unknownAuthorization;

			RaisePropertyChanged(() => Authorizer);
			RaisePropertyChanged(() => AuthorizeDate);
			RaisePropertyChanged(() => AuthorizeDateDisplay);
			RaisePropertyChanged(() => AuthorizationResolved);

			if (selectedItem != null) { selectedItem.Refresh(); }
		}

		public void RefreshApplicators() {
			Applicators = recommendationView.Applicators;
			unknownApplicators = recommendationView.unknownApplicators;

			RaisePropertyChanged(() => Applicators);
			RaisePropertyChanged(() => ApplicatorsResolved);

			if (selectedItem != null) { selectedItem.Refresh(); }
		}

		void SetProductStrategyString(ProductApplicationStrategy strat) {
			if (recommendationView != null) {
				switch (strat) {
					case ProductApplicationStrategy.ByRatePerArea:
						ApplicationType = Strings.ByRateRecommendation_Text;
						break;
					case ProductApplicationStrategy.ByRatePerTank:
						ApplicationType = Strings.ByTankRecommendation_Text;
						break;
					case ProductApplicationStrategy.ByTotalProduct:
						ApplicationType = Strings.ByTotalRecommendation_Text;
						break;
				}
			}
		}
	}
}