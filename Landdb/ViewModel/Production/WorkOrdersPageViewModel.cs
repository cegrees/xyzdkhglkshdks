﻿using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.WorkOrder;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Secondary.Reports.WorkOrder;
using Landdb.ViewModel.Secondary.WorkOrder;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Resources;
using Landdb.ViewModel.Production.Applications;

namespace Landdb.ViewModel.Production {
	public class WorkOrdersPageViewModel : AbstractListPage<WorkOrderListItemViewModel, ApplicationDetailsViewModel> {

		public WorkOrdersPageViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher)
			: base(clientEndpoint, dispatcher) {
			this.FilterModel = new WorkOrdersPageFilterViewModel(clientEndpoint);

			CreateApplicationFromSelectedCommand = new RelayCommand(CreateApplicationFromSelected, () => base.SelectedListItem != null);
			CreateWorkOrderFromSelectedCommand = new RelayCommand(CreateWorkOrderFromSelected, () => base.SelectedListItem != null);
            ShareFromSelectedCommand = new RelayCommand(ShareFromSelectedToTheWeb, () => base.SelectedListItem != null);

            Messenger.Default.Register<WorkOrdersAddedMessage>(this, msg => {
				RefreshItemsList(() => {
					var q = from li in ListItems
							where li.Id == msg.LastWorkOrderAdded
							select li;
					SelectedListItem = q.FirstOrDefault();
				});
			});

			ViewReport = new RelayCommand(this.UpdateReport);
		}

		public ICommand CreateApplicationFromSelectedCommand { get; }
		public ICommand CreateWorkOrderFromSelectedCommand { get; }
        public ICommand ShareFromSelectedCommand { get; private set; }
        public RelayCommand ViewReport { get; }

		public override string GetEntityName() => Strings.WorkOrder_Text.ToLower();
		public override string GetPluralEntityName() => Strings.WorkOrders_Text.ToLower();

		protected override IEnumerable<WorkOrderListItemViewModel> GetListItemModels() {
			var cropYearId = new CropYearId(currentDataSourceId.Id, currentCropYear);
			var woMaybe = clientEndpoint.GetView<WorkOrderListView>(cropYearId);

			if (woMaybe.HasValue) {
				var sortedWos = woMaybe.Value.WorkOrders.OrderByDescending(x => x.StartDateTime).Select(x => new WorkOrderListItemViewModel(x, clientEndpoint));
				return sortedWos;
			} else {
				return null;
			}
		}

		protected override async Task<ScreenDescriptor> CreateScreenDescriptorForNewItemCreation() {
			var workOrderId = new WorkOrderId(ApplicationEnvironment.CurrentDataSourceId, Guid.NewGuid());

			var vm = new WorkOrderEditorViewModel(clientEndpoint, dispatcher, workOrderId, ApplicationEnvironment.CurrentCropYear, null);
			Dictionary<string, List<string>> availableTimingEventTags = GetAvailableWorkOrderTimingEventTags(clientEndpoint);
			vm.AvailableTimingEventTags = availableTimingEventTags;
			return new ScreenDescriptor(typeof(Views.Secondary.WorkOrder.WorkOrderEditorView), "create WorkOrder", vm);
		}

		private Dictionary<string, List<string>> GetAvailableWorkOrderTimingEventTags(IClientEndpoint clientEndpoint) {
			Dictionary<string, List<string>> availableTimingEventTagz = new Dictionary<string, List<string>>();
			var plantagviewMaybe = clientEndpoint.GetView<WorkOrderTagProjectionView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear));
			if (plantagviewMaybe.HasValue && plantagviewMaybe.Value.TagInfo != null) {
				foreach (var v in plantagviewMaybe.Value.TagInfo) {
					if (string.IsNullOrEmpty(v.TimingEvent)) { continue; }
					if (!availableTimingEventTagz.ContainsKey(v.TimingEvent)) {
						availableTimingEventTagz.Add(v.TimingEvent, new List<string>());
					}
					foreach (var x in v.Tags) {
						if (availableTimingEventTagz.ContainsKey(v.TimingEvent)) {
							if (!string.IsNullOrEmpty(x.Key) && !availableTimingEventTagz[v.TimingEvent].Contains(x.Key)) {
								availableTimingEventTagz[v.TimingEvent].Add(x.Key);
							}
						}
					}
				}
			}
			return availableTimingEventTagz;
		}

		protected override bool PerformItemRemoval(WorkOrderListItemViewModel selectedItem) {
			var removeCommand = new RemoveWorkOrder(selectedItem.Id, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId));
            clientEndpoint.SendOne(removeCommand);
            return true;
		}

		protected override IDomainCommand CreateSetCharmCommand(WorkOrderListItemViewModel selectedItem, string charmName) {
			if (selectedItem.CharmName == charmName) { return null; }
			selectedItem.CharmName = charmName;
			return new FlagWorkOrder(selectedItem.Id, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId), charmName);
		}

		protected override ApplicationDetailsViewModel CreateDetailsModel(WorkOrderListItemViewModel selectedItem) {
			var woMaybe = clientEndpoint.GetView<WorkOrderView>(selectedItem.Id);
			if (woMaybe.HasValue) {
				return new ApplicationDetailsViewModel(clientEndpoint, dispatcher, woMaybe.Value);
			} else {
				return null;
			}
		}

		void CreateApplicationFromSelected() {
			var applicationId = new ApplicationId(ApplicationEnvironment.CurrentDataSourceId, Guid.NewGuid());

			DocumentDescriptor sourceDoc = new DocumentDescriptor() { DocumentType = DocumentDescriptor.WorkOrderTypeString, Identity = base.SelectedListItem.Id, Name = base.SelectedListItem.Name };

			var vm = new Secondary.Application.ApplicatorViewModel(clientEndpoint, dispatcher, applicationId, ApplicationEnvironment.CurrentCropYear, null, sourceDoc);
			Messenger.Default.Send(new ChangeScreenDescriptorMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Secondary.Application.ApplicatorView), "create application", vm)
			});
		}

		void CreateWorkOrderFromSelected() {
			var workOrderId = new WorkOrderId(ApplicationEnvironment.CurrentDataSourceId, Guid.NewGuid());

			DocumentDescriptor sourceDoc = new DocumentDescriptor() { DocumentType = DocumentDescriptor.WorkOrderTypeString, Identity = base.SelectedListItem.Id, Name = base.SelectedListItem.Name };

			var vm = new Secondary.WorkOrder.WorkOrderEditorViewModel(clientEndpoint, dispatcher, workOrderId, ApplicationEnvironment.CurrentCropYear, null, sourceDoc);
			Messenger.Default.Send(new ChangeScreenDescriptorMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Secondary.WorkOrder.WorkOrderEditorView), "create work order", vm)
			});
		}

		void UpdateReport() {
			//get map from ViewModel
			//var mapGen = new MapImageGenerator();
			//var mapFile = await mapGen.GenerateMap(clientEndpoint, dispatcher, czIdQ);

			if (SelectedListItem != null) {
				var vm = new SingleWorkOrderViewModel(clientEndpoint, dispatcher, SelectedListItem.Id);
				Messenger.Default.Send(new ShowOverlayMessage() {
					ScreenDescriptor = new ScreenDescriptor(typeof(Views.Secondary.Reports.Work_Order.SingleWorkOrderView), "viewReport", vm)
				});
			}
		}

        void ShareFromSelectedToTheWeb() {
            var pMaybe = clientEndpoint.GetView<WorkOrderView>(base.SelectedListItem.Id);
            if (pMaybe.HasValue) {
                Popups.ChoosePersonToShareViewModel vm = null;
                vm = new Popups.ChoosePersonToShareViewModel(clientEndpoint, dispatcher, pMaybe.Value, onPersonShared);
                Messenger.Default.Send(new ShowOverlayMessage() {
                    ScreenDescriptor = new ScreenDescriptor(typeof(Views.Production.Popups.ChoosePersonToShareView), "changeCropzones", vm)
                });
            }
        }

        private void onPersonShared(Popups.ChoosePersonToShareViewModel vm) {
            if (vm != null) {
                var woId = new WorkOrderId(currentDataSourceId.Id, base.SelectedListItem.Id.Id);
                var personid = vm.Person.Id;
                var sharemodel = new { ShareePersonId = personid.Id, Id = woId };

                ShowIntegration(sharemodel, "api/workorders/share");
            }
            Messenger.Default.Send<HideOverlayMessage>(new HideOverlayMessage());
        }

        public void ShowIntegration(object sharemodel, string uristring) {
            //TO DO :: CALL API TO GET PAGE
            Task<string> getMLZone = Task.Factory.StartNew<string>(() => {

                var dataSource = new DataSourceId(ApplicationEnvironment.CurrentDataSourceId);
                var cropYear = ApplicationEnvironment.CurrentCropYear;

                if (NetworkStatus.IsInternetAvailable()) {
                    var baseDocumentUri = string.Format("{0}{1}?datasourceid={2}", RemoteConfigurationSettings.GetBaseUri(), uristring, dataSource.Id);
                    //var action = string.Format("?dataSourceId={0}&cropYear={2}&nodeId={1}&nodeType=field", dataSource.Id, fieldView.Id.Id, cropYear);
                    var client = new HttpClient {
                        BaseAddress = new Uri(baseDocumentUri),
                    };

                    client.DefaultRequestHeaders.Add("Accept", "application / json");
                    var holder = PostData<string>(baseDocumentUri, client, sharemodel);
                    holder.Wait();

                    //var formattedLink = holder.Result;
                    //System.Diagnostics.Process.Start(formattedLink);

                    return holder.Result;
                }
                else {
                    return string.Empty;
                }
            });
        }

        private async Task<T> PostData<T>(string baseDocumentUri, System.Net.Http.HttpClient client, object sharemodel) {
            try {
                var cloudClientFactory = new Landdb.Infrastructure.CloudClientFactory();
                var token = cloudClientFactory.ResolveStorageCredentials();
                await token.SetBearerTokenAsync(client);

                // TODO: Might want to log this for debugging

                //HttpResponseMessage response = await client.PostAsJsonAsync(baseDocumentUri, JsonConvert.SerializeObject(sharemodel));
                HttpResponseMessage response = await client.PostAsJsonAsync(baseDocumentUri, sharemodel);

                if (response.StatusCode == HttpStatusCode.OK) {
                    // var json = await response.Content.ReadAsStringAsync();
                    // var result = JsonConvert.DeserializeObject<T>(json);
                    // return result;

                    // TODO: Read the response later...
                    var fake = string.Empty;
                    var result = JsonConvert.DeserializeObject<T>(fake);
                    return result;
                }
                else {
                    var fake = string.Empty;
                    var result = JsonConvert.DeserializeObject<T>(fake);
                    return result;
                }
            }
            catch (Exception ex) {
                throw new ApplicationException("Share Failed.");
            }
        }

    }
}