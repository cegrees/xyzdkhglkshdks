﻿using System;
using System.Collections.ObjectModel;

namespace Landdb.ViewModel.Production.Invoices
{
    public class PendingInvoiceDetailsViewModel : InvoiceDetailsViewModel
    {
        public static PendingInvoiceDetailsViewModel Default { get; } = new PendingInvoiceDetailsViewModel()
        {
            InvoiceDate = DateTime.Today,
            InvoiceNumber = "I-4M-N07_R34L",
            Notes = "This is a sample from Connect",
            Vendor = "Brandt - Oakville",
            TotalCost = 2048.42m,
            Products = new ObservableCollection<ProductDetails>() {
                new ProductDetails() {
                    ProductName = "Trivapro",
                    ManufacturerName = "Syngenta",
                    TotalProduct = AgC.UnitConversion.Volume.Gallon.Self.GetMeasure(50),
                    TotalCost = 500m
                },
                new ProductDetails() {
                    ProductName = "Unobtanium",
                    ManufacturerName = "GrizzCo",
                    TotalProduct = AgC.UnitConversion.MassAndWeight.ShortTon.Self.GetMeasure(200),
                    TotalCost = 1400m

                },
                new ProductDetails() {
                    ProductName = "20-0-20",
                    ManufacturerName = "Custom Blend",
                    TotalProduct = AgC.UnitConversion.MassAndWeight.Pound.Self.GetMeasure(3000),
                    TotalCost = 700m

                }
            }
        };
    }
}