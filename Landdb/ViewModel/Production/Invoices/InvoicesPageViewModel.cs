﻿using Landdb.Resources;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Client.Models.Connect;
using Landdb.Domain.ReadModels.Invoice;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Secondary.Invoice;
using Landdb.ViewModel.Secondary.Reports.Invoice;
using Landdb.Views.Secondary.Reports.Work_Order;
using Lokad.Cqrs;
using InvoiceView = Landdb.Domain.ReadModels.Invoice.InvoiceView;

namespace Landdb.ViewModel.Production.Invoices {
    public class InvoicesPageViewModel : AbstractListPage<InvoiceListItemViewModel, InvoiceDetailsViewModel>, IHandleViewRouting {
        public ICommand LaunchConnectWebForSelectedItemCommand { get; }
        public ICommand PrintCommand { get; }
        public ICommand CreateApplicationFromSelectedCommand { get; }
        public ICommand Refresh { get; }
        private List<InvoiceBlockHeader> currentConnectInvoices;

        private bool hasMorePendingInvoices;

        public InvoicesPageViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher) : base(clientEndpoint, dispatcher) {
            FilterModel = new InvoicesPageFilterViewModel(clientEndpoint);
            HasMorePendingInvoices = false;
            LaunchConnectWebForSelectedItemCommand = new RelayCommand(LaunchConnectWebForSelectedItem);
            PrintCommand = new RelayCommand(UpdateReport);
            CreateApplicationFromSelectedCommand = new RelayCommand(CreateApplicationFromSelected, () => SelectedListItem != null);
            Refresh = new RelayCommand(async () => await RefreshItemsList(() => SelectedListItem = ListItems.FirstOrDefault()));

            Messenger.Default.Register<InvoicesAddedMessage>(this, async message => {
                await RefreshItemsList(() => {
                    IEnumerable<InvoiceListItemViewModel> invoiceListItemViewModels = from listItem in ListItems
                                                                                      where listItem.Id == message.LastInvoiceAdded
                                                                                      select listItem;
                    SelectedListItem =
                        invoiceListItemViewModels.Single(listItem => listItem.Id == message.LastInvoiceAdded);
                });
            });

            clientEndpoint.GetConnectEngine().Subscribe(PeriodicRefresh);
        }

        public override string GetEntityName() => this.SelectedListItem?.IsActual == true ? Strings.Invoices_Text.ToLower() : Strings.RemoteInvoices_Text.ToLower();
        public override string GetPluralEntityName() => this.SelectedListItem?.IsActual == true ? Strings.Invoices_Text.ToLower() : Strings.RemoteInvoices_Text.ToLower();

        public bool HasMorePendingInvoices {
            get => hasMorePendingInvoices;
            set {
                hasMorePendingInvoices = value;
                RaisePropertyChanged(() => HasMorePendingInvoices);
            }
        }

        protected virtual void LaunchConnectWebForSelectedItem() {
            if (SelectedListItem == null || SelectedListItem.IsActual) { return; }

            // TODO: update this to go through the engine instead of the client
            var uri = clientEndpoint.GetConnectClient().GetInvoiceEditExperienceUri(SelectedListItem.Id.DataSourceId, SelectedListItem.Id.Id, ApplicationEnvironment.CurrentCropYear);
            System.Diagnostics.Process.Start(uri);
        }

        public void UpdateReport() {
            SingleInvoiceViewModel singleInvoiceViewModel = new SingleInvoiceViewModel(clientEndpoint, SelectedListItem.Id);
            Messenger.Default.Send(new ShowOverlayMessage() {
                ScreenDescriptor = new ScreenDescriptor(typeof(SingleWorkOrderView), "viewReport", singleInvoiceViewModel)
            });
        }

        public void CreateApplicationFromSelected() {
            var applicationId = new ApplicationId(ApplicationEnvironment.CurrentDataSourceId, Guid.NewGuid());

            DocumentDescriptor sourceDoc = new DocumentDescriptor() {
                DocumentType = DocumentDescriptor.InvoiceTypeString,
                Identity = SelectedListItem.Id,
                Name = SelectedListItem.InvoiceNumber
            };

            var vm = new Secondary.Application.ApplicatorViewModel(clientEndpoint, dispatcher, applicationId, ApplicationEnvironment.CurrentCropYear, null, sourceDoc);
            Messenger.Default.Send(new ChangeScreenDescriptorMessage() {
                ScreenDescriptor = new ScreenDescriptor(typeof(Views.Secondary.Application.ApplicatorView), "create application", vm)
            });
        }

        protected override void generateFilterHeader() {
            var originalCount = ListItems.Count;
            var filteredCount = ListItemsView.Count;

            string entityPluralityAwareName = originalCount == 1 ? GetEntityName() : GetPluralEntityName();

            FilterHeader = originalCount != filteredCount
                ? $"{filteredCount} (of {originalCount}) {entityPluralityAwareName}"
                : $"{filteredCount} {entityPluralityAwareName}";

            var remoteOriginalCount = currentConnectInvoices.Count;
            var filteredRemoteCount = currentConnectInvoices.Count;
            if (remoteOriginalCount > 0) {
                FilterHeader += remoteOriginalCount != filteredRemoteCount
                    ? $", {filteredRemoteCount} (of {remoteOriginalCount}) pending"
                    : $", {filteredRemoteCount} pending";
            }

            RaisePropertyChanged(() => FilterHeader);
        }

        protected override IEnumerable<InvoiceListItemViewModel> GetListItemModels() {
            List<InvoiceBlockHeader> invoiceBlockHeaderList = clientEndpoint.GetConnectEngine().InvoiceRepository.GetInvoiceHeadersAsync().Result.ToList();

            currentConnectInvoices = new List<InvoiceBlockHeader>(invoiceBlockHeaderList);
            List<InvoiceListItemViewModel> connectListItems =
                invoiceBlockHeaderList.Select(invoice => new InvoiceListItemViewModel(invoice)).OrderByDescending(item => item.InvoiceDate).ToList();

            Maybe<InvoiceListView> possibleInvoices = clientEndpoint.GetView<InvoiceListView>(new CropYearId(currentDataSourceId.Id,
                currentCropYear));

            List<InvoiceListItemViewModel> activeListItems = possibleInvoices.HasValue
                ? new List<InvoiceListItemViewModel>(possibleInvoices.Value.Invoices.Select(item => new InvoiceListItemViewModel(item)).OrderByDescending(item => item.InvoiceDate))
                : new List<InvoiceListItemViewModel>();

            connectListItems.AddRange(activeListItems);

            HasMorePendingInvoices = false;

            return new List<InvoiceListItemViewModel>(connectListItems);
        }

        protected override async Task<ScreenDescriptor> CreateScreenDescriptorForNewItemCreation() {
            InvoiceId invoiceId = new InvoiceId(currentDataSourceId.Id, Guid.NewGuid());
            InvoiceEditorViewModel viewModel = new InvoiceEditorViewModel(clientEndpoint, dispatcher, invoiceId,
                ApplicationEnvironment.CurrentCropYear);
            return new ScreenDescriptor(typeof(Views.Secondary.Invoice.InvoiceEditorView), "create invoice", viewModel);
        }

        protected override bool PerformItemRemoval(InvoiceListItemViewModel selectedItem) {
            if (selectedItem.IsActual) {
                var removeCommand = new RemoveInvoice(selectedItem.Id, clientEndpoint.GenerateNewMetadata());
                clientEndpoint.SendOne(removeCommand);
                return true;
            } else {
                var task = Task.Run(async () => {
                    return await clientEndpoint.GetConnectEngine().InvoiceRepository.RemoveInvoiceAsync(selectedItem.Id);
                });
                return task.Result;
            }
        }

        protected override IDomainCommand CreateSetCharmCommand(InvoiceListItemViewModel selectedItem, string charmName) {
            throw new NotImplementedException();
        }

        protected override InvoiceDetailsViewModel CreateDetailsModel(InvoiceListItemViewModel selectedItem) {
            if (!selectedItem.IsActual) {
                var invoiceView = clientEndpoint.GetConnectEngine().InvoiceRepository.GetInvoiceAsync(selectedItem.Id).Result;
                if (invoiceView != null) {
                    var pendingInvoice = new PendingInvoiceDetailsViewModel() {
                        InvoiceDate = invoiceView.InvoiceDate,
                        InvoiceNumber = invoiceView.InvoiceNumber,
                        Notes = invoiceView.Notes,
                        Vendor = invoiceView.Vendor,
                        TotalCost = invoiceView.Products?.Sum(p => p.TotalCost) ?? -1m
                    };

                    var products = from p in invoiceView.Products
                                   select new InvoiceDetailsViewModel.ProductDetails() {
                                       ProductName = p.ProductName,
                                       TotalCost = p.TotalCost,
                                       TotalProduct = AgC.UnitConversion.UnitFactory.GetUnitByName(p.TotalProductUnit)
                                           .GetMeasure((double)p.TotalProductValue),
                                       TrackingId = Guid.NewGuid(),
                                       ManufacturerName = "<unknown>"
                                   };

                    pendingInvoice.Products = new ObservableCollection<InvoiceDetailsViewModel.ProductDetails>(products);

                    return pendingInvoice;
                }
            }

            Maybe<InvoiceView> invMaybe = clientEndpoint.GetView<InvoiceView>(selectedItem.Id);

            return invMaybe.HasValue
                ? new ActualInvoiceDetailsViewModel(clientEndpoint, invMaybe.Value)
                : null;
        }

        // TODO: Move this to the abstract base?
        public void HandleRouteRequest(RouteRequestMessage message) {
            if (message.Route is InvoiceId) {
                var id = (InvoiceId)message.Route;

                var q = ListItems.Where(x => x.Id == id);
                if (q.Any()) {
                    SelectedListItem = q.FirstOrDefault();
                }
            }
        }

        private void PeriodicRefresh() {
            if (HaveNewPendingInvoices()) {
                HasMorePendingInvoices = true;
            }
        }

        private bool HaveNewPendingInvoices() {
            return !AreSamePendingInvoiceLists(currentConnectInvoices, clientEndpoint.GetConnectEngine().InvoiceRepository.GetInvoiceHeadersAsync().Result.ToList());
        }

        private static bool AreSamePendingInvoiceLists(IReadOnlyList<InvoiceBlockHeader> original, IReadOnlyList<InvoiceBlockHeader> updated) {
            if (original.Count == updated.Count) {
                for (int index = 0; index < original.Count; index++) {
                    if (original[index].Id != updated[index].Id) {
                        return false;
                    }
                }

                return true;
            } else {
                return false;
            }
        }
    }
}