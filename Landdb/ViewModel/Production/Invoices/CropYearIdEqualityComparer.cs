﻿using System.Collections.Generic;

namespace Landdb.ViewModel.Production.Invoices
{
    public class CropYearIdEqualityComparer : IEqualityComparer<CropYearId>
    {
        public bool Equals(CropYearId x, CropYearId y)
        {
            if (x == null || y == null)
            {
                return false;
            }

            return x.DataSourceId == y.DataSourceId && x.Id == y.Id;
        }

        public int GetHashCode(CropYearId obj)
        {
            return obj.DataSourceId.GetHashCode() ^ obj.Id.GetHashCode();
        }
    }
}