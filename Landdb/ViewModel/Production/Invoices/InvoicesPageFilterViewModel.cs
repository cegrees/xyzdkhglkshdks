﻿using System;
using System.Collections.Generic;
using System.Linq;
using GalaSoft.MvvmLight;
using Landdb.Client.Infrastructure;
using Landdb.Client.Infrastructure.DisplayItems;
using Landdb.Domain.ReadModels.Documents;

namespace Landdb.ViewModel.Production.Invoices {

    public class InvoicesPageFilterViewModel : ViewModelBase, IPageFilterViewModel
    {
        private readonly IClientEndpoint clientEndpoint;
        string filterText;
        SourceDataDisplayItem usedAsSourceRecord = new SourceDataDisplayItem(SourceDataDisplayType.Null);

        public InvoicesPageFilterViewModel(IClientEndpoint clientEndpoint)
        {
            this.clientEndpoint = clientEndpoint;
            filterText = string.Empty;
            SelectedInvoiceType = new InvoiceTypeDisplayItem(InvoiceTypeDisplayType.All);
        }

        public void ClearFilter() {
            FilterText = string.Empty;
            StartDate = null;
            EndDate = null;
            UsedAsSourceRecord = new SourceDataDisplayItem(SourceDataDisplayType.Null);
            SelectedInvoiceType = new InvoiceTypeDisplayItem(InvoiceTypeDisplayType.All);
        }

        public void BeforeFilter() { }

		public void RefreshLists() { }

		public bool FilterItem(object item) {
		    if (!(item is InvoiceListItemViewModel invoiceToFilter))
		    {
		        return false;
		    }

            if (StartDate != null && invoiceToFilter.InvoiceDate < StartDate.Value.Date)
            {
                return false;
            }
            if (EndDate != null && invoiceToFilter.InvoiceDate > (EndDate.Value.Date + TimeSpan.FromDays(1) - TimeSpan.FromSeconds(1)))
            {
                return false;
            }

		    if (!string.IsNullOrWhiteSpace(filterText) && !invoiceToFilter.Vendor.ToLower().Contains(filterText.ToLower()) && !invoiceToFilter.InvoiceNumber.ToLower().Contains(filterText.ToLower()))
		    {
		        return false;
            }

            if (!IsFilteredOutByInvoiceType((InvoiceListItemViewModel)item))
            {
                return false;
            }

            if (UsedAsSourceRecord != null ) {
				var docUse = clientEndpoint.GetView<DocumentUseIndex>(invoiceToFilter.Id);
				switch (UsedAsSourceRecord.SourceDataType) {
					case SourceDataDisplayType.Null:
						break;
					case SourceDataDisplayType.NotUsed:
						if (!docUse.HasValue || docUse.Value.SourcedItems == null || !docUse.Value.SourcedItems.Any())
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
					case SourceDataDisplayType.UsedInApplication:
						if (docUse.HasValue && docUse.Value.SourcedItems.Any())
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
					default:
						break;
				}
			}

		    return true;
		}

        public string FilterText {
            get => filterText;
            set {
                filterText = value;
                RaisePropertyChanged(() => FilterText);
            }
        }

        DateTime? startDate;

        public DateTime? StartDate {
            get => startDate;
            set {
                startDate = value;
                RaisePropertyChanged(() => StartDate);
            }
        }

        DateTime? endDate;

        public DateTime? EndDate {
            get => endDate;
            set {
                endDate = value;
                RaisePropertyChanged(() => EndDate);
            }
        }

        public List<SourceDataDisplayItem> Sources_Invoice => SourceDataDisplayItem.Sources_Invoice();
        public SourceDataDisplayItem UsedAsSourceRecord
        {
            get => usedAsSourceRecord;
            set
            {
                usedAsSourceRecord = value;
                RaisePropertyChanged(() => UsedAsSourceRecord);
            }
        }

        private InvoiceTypeDisplayItem selectedInvoiceType;
        public List<InvoiceTypeDisplayItem> InvoiceTypes => InvoiceTypeDisplayItem.AllAsList();
        public InvoiceTypeDisplayItem SelectedInvoiceType
        {
            get => InvoiceTypes.First(x => x.Equals(selectedInvoiceType));
            set
            {
                selectedInvoiceType = InvoiceTypes.First(x => x.Equals(value));
                RaisePropertyChanged(() => SelectedInvoiceType);
            }
        }

        private bool IsFilteredOutByInvoiceType(InvoiceListItemViewModel item)
        {
            return IsFilteredOutByInvoiceType(item, SelectedInvoiceType);
        }

        private static bool IsFilteredOutByInvoiceType(InvoiceListItemViewModel item, InvoiceTypeDisplayItem selectedInvoiceType)
        {
            if (item.IsActual && selectedInvoiceType.Equals(InvoiceTypeDisplayType.Actual))
            {
                return true;
            }
            else if (!item.IsActual && selectedInvoiceType.Equals(InvoiceTypeDisplayType.Pending))
            {
                return true;
            }
            else if (selectedInvoiceType.Equals(InvoiceTypeDisplayType.All))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
