﻿using System;
using System.Collections.ObjectModel;
using AgC.UnitConversion;
using GalaSoft.MvvmLight;
using Landdb.Resources;

namespace Landdb.ViewModel.Production.Invoices {
    public abstract class InvoiceDetailsViewModel : ViewModelBase {
        public string ConnectWarningText { get; } = "This invoice exists via Land.db Connect. It must be matched and imported before it is included in pricing and inventory.";

        string notes;
        public string Notes {
            get { return notes; }
            set {
                notes = value;
                RaisePropertyChanged("Notes");
            }
        }

        DateTime invoiceDate;
        public DateTime? InvoiceDate {
            get { return invoiceDate; }
            set {
                invoiceDate = value ?? DateTime.MinValue;
                RaisePropertyChanged("InvoiceDate");
            }
        }

        public string InvoiceDateDisplayString => InvoiceDate != null ? invoiceDate.ToShortDateString() : string.Empty;

        string invoiceNumber;
        public string InvoiceNumber {
            get { return invoiceNumber; }
            set {
                invoiceNumber = value;
                RaisePropertyChanged("InvoiceNumber");
            }
        }

        string vendorName;
        public string Vendor {
            get { return vendorName; }
            set {
                vendorName = value;
                RaisePropertyChanged("Vendor");
            }
        }
        public decimal TotalCost { get; set; }
        public ObservableCollection<ProductDetails> Products { get; set; }

        public class ProductDetails : ViewModelBase {
            Measure total;
            decimal cost;

            public ProductId ID { get; set; }
            public string ProductName { get; set; }
            public string ManufacturerName { get; set; }
            public Guid TrackingId { get; set; }
            public Measure TotalProduct { 
                get { return total; } 
                set { 
                    total = value; 
                    RaisePropertyChanged(() => TotalProduct); 
                } 
            }
            public decimal TotalCost { 
                get { return cost; } 
                set { 
                    cost = value; 
                    RaisePropertyChanged(() => TotalCost); 
                } 
            }
            public string CostPerUnitDisplay {
                get {
                    if (TotalProduct == null || TotalProduct.Value <= 0) { return string.Empty; }
                    
                    string costPerUnit = ((double)TotalCost / TotalProduct.Value).ToString("N2");
                    return string.Format(Strings.Per_Format_Text, costPerUnit, TotalProduct.Unit.Name);
                }
            }            
        }
    }
}