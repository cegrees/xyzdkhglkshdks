﻿using Landdb.Client.Infrastructure.DisplayItems;
using System;
using System.Globalization;
using System.Windows.Data;

namespace Landdb.ViewModel.Production.Invoices
{
    /// <summary>
    /// NOTE: Deprecated - see InvoiceTypeDisplayItem
    /// </summary>
    public class InvoiceTypeFilterConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException();
            if (value == null) { return null; }

            var stringValue = value.ToString();
            var invoiceType = new InvoiceTypeDisplayItem(stringValue);
            var displayText = invoiceType.DisplayText;

            return displayText;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
            if (value != null) {
                var stringValue = (string) value;
                var invoiceType = new InvoiceTypeDisplayItem(stringValue);
                return invoiceType.InvoiceType;
            }
            else
            {
                return InvoiceTypeDisplayType.All;
            }
        }   
    }
}