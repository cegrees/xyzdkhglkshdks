﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using AgC.UnitConversion;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Invoice;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.Resources;
using Landdb.ViewModel.Production.Popups;

namespace Landdb.ViewModel.Production.Invoices
{
    public class ActualInvoiceDetailsViewModel : InvoiceDetailsViewModel
    {
        readonly IClientEndpoint clientEndpoint;
        readonly IMasterlistService masterlist;
        readonly InvoiceView invoiceView;
        ProductDetails selectedProduct;
        public ICommand EditInvoiceInfo { get; set; }
        public ICommand EditInvoiceNumber { get; }
        public ICommand EditNotes { get; set; }
        public ICommand AddProductCommand { get; set; }


        public ActualInvoiceDetailsViewModel(IClientEndpoint clientEndpoint, InvoiceView invoiceView)
        {
            this.clientEndpoint = clientEndpoint;
            this.invoiceView = invoiceView;
            masterlist = clientEndpoint.GetMasterlistService();
            Notes = !string.IsNullOrEmpty(invoiceView.Notes) ? invoiceView.Notes : string.Empty;
            InvoiceDate = invoiceView.InvoiceDate;
            InvoiceNumber = invoiceView.InvoiceNumber;
            Vendor = invoiceView.Vendor;
            Products = new ObservableCollection<ProductDetails>();
            EditInvoiceInfo = new RelayCommand(EditInfo);
            EditInvoiceNumber = new RelayCommand(EditNumber);
            EditNotes = new RelayCommand(EditInvoiceNotes);
            AddProductCommand = new RelayCommand(AddProduct);
            TotalCost = invoiceView.TotalCost;

            foreach (var p in invoiceView.Products)
            {
                var masterlistProd = masterlist.GetProduct(p.ProductId);

                if (masterlistProd == null)
                {
                    masterlistProd = new MiniProduct()
                    {
                        Id = p.ProductId.Id,
                        Name = Strings.UnknownProduct_Text,
                        Density = null,
                        StdUnit = p.TotalProductUnit,
                        StdPackageUnit = p.TotalProductUnit,
                    };
                }

                Products.Add(new ProductDetails()
                {
                    ProductName = masterlistProd.Name,
                    TrackingId = p.TrackingId,
                    ManufacturerName = masterlistProd.Manufacturer,
                    TotalCost = p.TotalCost,
                    TotalProduct = UnitFactory.GetUnitByName(p.TotalProductUnit).GetMeasure((double)p.TotalProductValue, masterlistProd.Density),
                    ID = p.ProductId
                });
            }
        }

        public ProductDetails SelectedProduct
        {
            get { return selectedProduct; }
            set
            {
                selectedProduct = value;
                if (selectedProduct != null) { EditProduct(); }
            }
        }

        void EditInvoiceNotes()
        {
            if (invoiceView != null)
            {
                DialogFactory.ShowNotesChangeDialog(Notes, newNotes => {
                    var cmd = new EditInvoiceNotes(invoiceView.Id, clientEndpoint.GenerateNewMetadata(), newNotes);
                    clientEndpoint.SendOne(cmd);
                    Notes = newNotes;
                });
            }
        }

        void EditProduct()
        {
            var invProd = invoiceView.Products.Where(x => x.TrackingId == SelectedProduct.TrackingId).FirstOrDefault();
            EditInvoiceProductViewModel vm = new EditInvoiceProductViewModel(clientEndpoint, invoiceView, invProd, this);
            Messenger.Default.Send(new ShowOverlayMessage()
            {
                ScreenDescriptor = new ScreenDescriptor(typeof(Views.Production.Popups.EditInvoiceProductView), "editProduct", vm)
            });
        }

        void AddProduct()
        {
            EditInvoiceProductViewModel vm = new EditInvoiceProductViewModel(clientEndpoint, invoiceView, null, this);
            Messenger.Default.Send(new ShowOverlayMessage()
            {
                ScreenDescriptor = new ScreenDescriptor(typeof(Views.Production.Popups.EditInvoiceProductView), "editProduct", vm)
            });
        }

        void EditNumber()
        {
            if (invoiceView != null)
            {
                DialogFactory.ShowNameChangeDialog(Strings.InvoiceNumber_Text, InvoiceNumber, newName => {
                    var cmd = new RenameInvoice(invoiceView.Id, clientEndpoint.GenerateNewMetadata(), newName);
                    clientEndpoint.SendOne(cmd);
                    InvoiceNumber = newName;
                });
            }
        }

        void EditInfo()
        {
            ChangeInvoiceInfo vm = new ChangeInvoiceInfo(clientEndpoint, invoiceView, this);
            Messenger.Default.Send(new ShowPopupMessage()
            {
                ScreenDescriptor = new ScreenDescriptor(typeof(Views.Production.Popups.ChangeInvoiceInfoView), "editInfo", vm)
            });
        }

        public void UpdateProductDetails(ProductDetails prodDetails)
        {
            var prod = Products.FirstOrDefault(x => x.TrackingId == prodDetails.TrackingId);
            prod.TotalProduct = prodDetails.TotalProduct;
            prod.TotalCost = prodDetails.TotalCost;
        }

        public void UpdateProductList(ProductDetails prodDetails)
        {
            Products.Add(prodDetails);
            RaisePropertyChanged(() => Products);
        }

        public void RemoveProductFromList(Guid trackingId)
        {
            var prod = Products.FirstOrDefault(x => x.TrackingId == trackingId);
            Products.Remove(prod);
            RaisePropertyChanged(() => Products);
        }
    }
}