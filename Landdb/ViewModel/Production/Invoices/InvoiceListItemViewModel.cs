﻿using System;
using Landdb.Client.Models.Connect;
using Landdb.Domain.ReadModels.Invoice;
using Landdb.Resources;

namespace Landdb.ViewModel.Production.Invoices {
    public class InvoiceListItemViewModel {
        public InvoiceListItemViewModel(InvoiceListItem listItemModel) {
            Id = listItemModel.Id;
            InvoiceDate = listItemModel.InvoiceDate;
            TotalCost = listItemModel.TotalCost;
            InvoiceNumber = listItemModel.InvoiceNumber;
            ProductCount = listItemModel.ProductCount;
            Vendor = listItemModel.Vendor;
            IsActual = true;
        }

        public InvoiceListItemViewModel(InvoiceBlockHeader invoiceBlockHeader) {
            Id = invoiceBlockHeader.InvoiceId;
            InvoiceDate = invoiceBlockHeader.InvoiceDate ?? DateTime.Parse("09/16/1980");
            TotalCost = invoiceBlockHeader.TotalCostValue;
            InvoiceNumber = invoiceBlockHeader.InvoiceNumber;
            ProductCount = invoiceBlockHeader.ProductCount;
            Vendor = invoiceBlockHeader.Source;
            IsActual = false;
        }

        public InvoiceId Id { get; }
        public DateTime InvoiceDate { get; }
        public string InvoiceDateDisplayText => InvoiceDate.ToShortDateString();
        public decimal TotalCost { get; }
        public string TotalCostAsCurrency => TotalCost.ToString("C2");
        public string InvoiceNumber { get; }
        public int ProductCount { get; }
        public string ProductCountDisplayText => $"{ProductCount} {Strings.Products_Text.ToLowerInvariant()}";
        public string Vendor { get; }
        public bool IsActual { get; }
    }
}
