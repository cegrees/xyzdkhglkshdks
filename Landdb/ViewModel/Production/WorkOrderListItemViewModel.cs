﻿using AgC.UnitConversion;
using GalaSoft.MvvmLight;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Domain.ReadModels.WorkOrder;
using Landdb.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Production
{
    public class WorkOrderListItemViewModel : ViewModelBase
    {
        WorkOrderListItem li;
        IClientEndpoint endPoint;
        decimal totalCost;

        public WorkOrderListItemViewModel(WorkOrderListItem li, IClientEndpoint endpoint)
        {
            this.li = li;
            this.endPoint = endpoint;

            var woMaybe = endPoint.GetView<WorkOrderView>(li.Id);
            var inventory = endPoint.GetView<InventoryListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear)).GetValue(new InventoryListView());
            if (woMaybe.HasValue)
            {
                var wo = woMaybe.Value;
                var products = wo.Products;

                foreach (var prod in products)
                {
                    var invProd = inventory.Products.FirstOrDefault(x => x.Key == prod.Id);
                    var totalMeasure = UnitFactory.GetUnitByName(prod.TotalProductUnit).GetMeasure((double)prod.TotalProductValue);
                    double totalPrice = 0.0;

                    var avgPriceUnit = invProd.Value != null && invProd.Value.AveragePriceUnit != null && 
                        invProd.Value.AveragePriceUnit != string.Empty
                        ? UnitFactory.GetUnitByName(invProd.Value.AveragePriceUnit) : null;
                    if (avgPriceUnit != null && totalMeasure.CanConvertTo(avgPriceUnit))
                    {
                        totalPrice = totalMeasure.GetValueAs(avgPriceUnit) * invProd.Value.AveragePriceValue;
                    }
                    else if (avgPriceUnit != null && totalMeasure.Unit == avgPriceUnit)
                    {
                        totalPrice = totalMeasure.Value * invProd.Value.AveragePriceValue;
                    }
                    else { totalPrice = 0; }

                    TotalCost += (decimal)totalPrice;
                }
            }
        }
        public WorkOrderId Id { get { return li.Id; } }
        public DateTime? StartDateTime
        {
            get
            {
                if (li.StartDateTime.HasValue)
                {
                    return li.StartDateTime.Value.ToLocalTime();
                }
                else
                {
                    return null;
                }
            }
        }
        public decimal TotalCost { get { return totalCost; } set { totalCost = value; } }
        public string Name { get { return li.Name; } }
        public int FieldCount { get { return li.FieldCount; } }
        public int ProductCount { get { return li.ProductCount; } }
        public string CharmName
        {
            get { return li.CharmName; }
            set
            {
                li.CharmName = value;
                RaisePropertyChanged("CharmName");
            }
        }
    }
}
