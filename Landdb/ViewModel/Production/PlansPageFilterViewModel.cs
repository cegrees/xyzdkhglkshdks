﻿using GalaSoft.MvvmLight;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Plan;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Production {
    public class PlansPageFilterViewModel : ViewModelBase, IPageFilterViewModel {
        string filterText = string.Empty;
        IClientEndpoint endPoint;

        public PlansPageFilterViewModel(IClientEndpoint endpoint)
        {
            this.endPoint = endpoint;
        }

        public void ClearFilter() {
            FilterText = string.Empty;
        }

        public void BeforeFilter() { }

		public void RefreshLists() { }

		public bool FilterItem(object item) {
            var x = item as PlanListItemViewModel;
            if (x == null) { return false; }

            //if (!string.IsNullOrWhiteSpace(filterText) && !x.Name.Contains(filterText)) {
            //    return false;
            //}

            /////////////////////////////////////////////////////////////////////////////////////

            var planDetailsMaybe = endPoint.GetView<PlanView>(x.Id);

            if (planDetailsMaybe.HasValue)
            {
                List<FlattenedTreeHierarchyItem> flattenedItemsList = new List<FlattenedTreeHierarchyItem>();
                var flattened = endPoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear)).GetValue(new FlattenedTreeHierarchyView());

                foreach (var czItem in planDetailsMaybe.Value.CropZones)
                {
                    var flat = (from f in flattened.Items
                                where f.CropZoneId == czItem.Id
                                select f).FirstOrDefault();

					if (flat == null) {
						// check deleted cropzones
						flat = (from f in flattened.DeletedItems
								where f.CropZoneId == czItem.Id
								select f).FirstOrDefault();

						if (flat == null) { continue; }
					}

                    flattenedItemsList.Add(flat);
                }

                var filterContainsCZ = (from flatItem in flattenedItemsList
                                        where flatItem != null && flatItem.CropZoneName.ToLower().Contains(FilterText.ToLower())
                                        select flatItem).Any();
                var filterContainsField = (from flatItem in flattenedItemsList
                                           where flatItem != null && flatItem.FieldName.ToLower().Contains(FilterText.ToLower())
                                           select flatItem).Any();
                var filterContainsFarm = (from flatItem in flattenedItemsList
                                          where flatItem != null && flatItem.FarmName.ToLower().Contains(FilterText.ToLower())
                                         select flatItem).Any();

                if (!string.IsNullOrWhiteSpace(FilterText) &&  filterContainsCZ == false && filterContainsField == false && filterContainsFarm == false && !x.Name.ToLower().Contains(FilterText.ToLower()))
                {
                    return false;
                }
            }
            
            ///////////////////////////////////////////////////////////////////////////////////////

            return true;
        }

        public string FilterText {
            get { return filterText; }
            set {
                filterText = value;
                RaisePropertyChanged("FilterText");
            }
        }
    }
}
