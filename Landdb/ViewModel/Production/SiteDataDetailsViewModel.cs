﻿using GalaSoft.MvvmLight;
using Landdb.Client.Infrastructure;
using Landdb.Client.Spatial;
using Landdb.Domain.ReadModels.Map;
using Microsoft.Maps.MapControl.WPF;
using System;
using System.Collections.Generic;
using System.Media;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Threading;
using Landdb.Resources;
using Newtonsoft.Json;

namespace Landdb.ViewModel.Production
{
    public class SiteDataDetailsViewModel : ViewModelBase
    {

        readonly IClientEndpoint clientEndpoint;
        readonly Dispatcher dispatcher;
        Path cropzonePreview;
        MapLayer layer;
        bool shouldShowBingMaps;
        bool shouldShowWpfMock;
        bool hasNoMapData;
        IEnumerable<string> mapData;
        readonly SiteDataListItemViewModel selectedItem;
        Observation selectedSet;

        public SiteDataDetailsViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, SiteDataListItemViewModel selectedItem)
        {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            this.selectedItem = selectedItem;
            Applicators = new List<ApplicationApplicator>();
            ReportedSets = new List<Observation>();
            mapData = new List<string>();
            SummaryItems = new List<SummaryItem>();
            Initialize();

            if (!mapData.Any())
            {
                HasNoMapData = true;
            }
            else
            {
                ShouldShowBingMaps = NetworkStatus.IsInternetAvailableFast();
                ShouldShowWpfMock = !ShouldShowBingMaps;

                dispatcher.BeginInvoke(new Action(() =>
                {
                    if (shouldShowBingMaps)
                    {
                        layer = BingMapsUtility.GetLayerForMapData(mapData);

                    }
                    else
                    {
                        cropzonePreview = WpfTransforms.CreatePathFromMapData(mapData);
                    }
                }));
            }
        }

        public ScoutingApplicationId DocumentId { get; set; }
        public string Name { get; set; }
        public DateTime? StartDateTime { get; set; }
        public DateTime? EndDateTime { get; set; }
        public int CropYear { get; set; }
        public string DocumentType { get; set; }
        public string Offering { get; set; }
        public string Notes { get; set; }
        public List<ApplicationApplicator> Applicators { get; set; }
        public string Authorization { get; set; }
        public string Centroid { get; set; }
        public ApplicationConditions Conditions { get; set; }
        public List<Observation> ReportedSets { get; set; }
        public List<SummaryItem> SummaryItems { get; set; }
        public SummaryItem OverallAvgItem { get; set; }
        public string TimingEvent { get; set; }
        public Observation SelectedSet 
        {
            get
            {
                return selectedSet;
            }
            set
            {
                selectedSet = value;

                //iterate over set of children in ObservationLayer and display selected Item.
                dispatcher.InvokeAsync(() =>
                {
                    List<Pushpin> pins = new List<Pushpin>();

                    foreach (var child in ObservationsLayer.Children)
                    {
                        var pin = child as Pushpin;
                        if (selectedSet.GeoPosition != null && pin.Location == new Location(selectedSet.GeoPosition.Latitude, selectedSet.GeoPosition.Longitude))
                        {
                            pin.Background = new SolidColorBrush(Colors.Blue);
                        }
                        else
                        {
                            pin.Background = new SolidColorBrush(Colors.Green);
                        }
                    }
                });
            }
        }

        public ObservedCropZone CropZone { get; set; } //DO SOMETHING ABOUT THIS ONE
        public string CropZoneName { get { return CropZone.Name; } }

        public MapLayer BingMapsLayer
        {
            get { return layer; }
        }

        public MapLayer ObservationsLayer
        {
            get;
            set;
        }

        public string MapCulture
        {
            get { return Infrastructure.ApplicationEnvironment.BingWpfMapCulture; }
        }

        public bool ShouldShowBingMaps
        {
            get { return shouldShowBingMaps; }
            private set
            {
                shouldShowBingMaps = value;
                RaisePropertyChanged("ShouldShowBingMaps");
            }
        }

        public bool ShouldShowWpfMock
        {
            get { return shouldShowWpfMock; }
            set
            {
                shouldShowWpfMock = value;
                RaisePropertyChanged("ShouldShowWpfMock");

            }
        }

        public bool HasNoMapData
        {
            get { return hasNoMapData; }
            set
            {
                hasNoMapData = value;
                RaisePropertyChanged("HasNoMapData");

                ShouldShowBingMaps = false;
                ShouldShowWpfMock = false;
            }
        }

        public bool IsCrmVisible { get; set; }


        void Initialize()
        {
            DocumentId = selectedItem.DocumentId;
            Name = selectedItem.Name;
            StartDateTime = selectedItem.StartDateTime;
            EndDateTime = selectedItem.EndDateTime;
            CropYear = selectedItem.CropYear;
            DocumentType = selectedItem.DocumentType;
            Offering = selectedItem.Offering == "CRM" || selectedItem.Offering == "SynCRM" ? "CRM" : string.Empty;
            IsCrmVisible = selectedItem.Offering == "CRM" || selectedItem.Offering == "SynCRM" ? true : false;
            Notes = selectedItem.Notes;
            Applicators = selectedItem.Applicators;
            Authorization = selectedItem.Authorization;
            Centroid = selectedItem.Centroid;
            Conditions = selectedItem.Conditions;
            CropZone = selectedItem.CropZones.Any() ? selectedItem.CropZones[0] : null;
            TimingEvent = selectedItem.TimingEvent;
            ReportedSets = ProcessSets(selectedItem.ReportedSets);
            CalculateSummaryItems();
            try
            {
                if (CropZone != null && CropZone.CropZoneId != null)
                {
                    var mi = clientEndpoint.GetView<ItemMap>(CropZone.CropZoneId);
                    List<string> filteredMapData = new List<string>();
                    if (mi.HasValue)
                    {
                        //var filteredData = from x in mi.Value.MapItems
                        //                   select x.MapData;
                        //filteredMapData.AddRange(filteredData);
                        if (mi.Value.MostRecentMapItem != null)
                        {
                            filteredMapData.Add(mi.Value.MostRecentMapItem.MapData);
                            mapData = filteredMapData;
                        }
                    }
                }

                dispatcher.InvokeAsync(() =>
                {
                    ObservationsLayer = new MapLayer();
                    foreach (var ob in ReportedSets)
                    {
                        var position = ob.GeoPosition;

                        Pushpin pin = new Pushpin();
                        pin.Background = new SolidColorBrush(Colors.Green);
                        pin.TouchDown += pin_TouchDown;
                        pin.MouseDown += pin_MouseDown;

                        if (ob.GeoPosition != null)
                        {
                            Location loc = new Location(ob.GeoPosition.Latitude, ob.GeoPosition.Longitude);
                            pin.Location = loc;
                            ObservationsLayer.Children.Add(pin);
                        }
                        // Adds the pushpin to the map.
                    }
                });
                
            }
            catch (Exception ex)
            {

            }
        }

        private void pin_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var touchedPin = sender as Pushpin;
            var location = touchedPin.Location;
            foreach (var set in ReportedSets)
            {
                if ( set.GeoPosition != null && set.GeoPosition.Longitude == location.Longitude && set.GeoPosition.Latitude == location.Latitude)
                {
                    SelectedSet = set;
                    RaisePropertyChanged(() => SelectedSet);
                }
            }
        }

        private void pin_TouchDown(object sender, System.Windows.Input.TouchEventArgs e)
        {
            var touchedPin = sender as Pushpin;
            var location = touchedPin.Location;
            foreach (var set in ReportedSets)
            {
                if (set.GeoPosition != null &&  set.GeoPosition.Longitude == location.Longitude && set.GeoPosition.Latitude == location.Latitude)
                {
                    SelectedSet = set;
                }
            }
        }

        public void CalculateSummaryItems()
        {
            var observations = ReportedSets;

            //var lmPercent = 0;
            var totLmValues = 0;
            var totPaValues = 0;
            var totPnValues = 0;
            double totRatedValues = 0;
            var totRatedCount = 0;
            //var PaPercent = 0;

            for (var j = 0; j < observations.Count(); j++) {

                for (var i = 0; i < observations[j].Properties.Count(); i++) {

                    //calc LM %
                    if (observations[j].Properties[i].Name == "LM")
                    {

                        if (observations[j].Properties[i].DisplayValue == "Y")
                        {
                            totLmValues += 1;
                        }
                    }
                    //calc Pa %
                    if (observations[j].Properties[i].Name == "Pa") {

                        if (observations[j].Properties[i].DisplayValue == "Y")
                        {
                            totPaValues += 1;
                        }
                    }
                    if (observations[j].Properties[i].Name == "Pn")
                    {

                        if (observations[j].Properties[i].DisplayValue == "Y")
                        {
                            totPnValues += 1;
                        }
                    }

                    ///////////////////////////////////////////////////////////////////////////////////////////
                    if (observations[j].Properties[i].Name == "A") {

                        try
                        {
                            var aValue = Convert.ToDouble(observations[j].Properties[i].Value);
                            if (aValue > 0)
                            {
                                totRatedCount++;
                            }

                            totRatedValues += aValue;
                        }
                        catch (Exception ex) { }
                    }


                    try
                    {
                        var bVAlue = Convert.ToDouble(observations[j].Properties[i].Value);

                        if (observations[j].Properties[i].Name == "B")
                        {

                            if (bVAlue > 0)
                            {
                                totRatedCount++;
                            }

                            totRatedValues += bVAlue;
                        }
                    }
                    catch (Exception x) { }
                    
                    ///////////////////////////////////////////////////////////////////////////////////////////
                }
            }

            SummaryItems = new List<SummaryItem>();
            //lmPercent = (totLmValues / observations.Count()) * 100;
            var smItem = new SummaryItem() { Label = Strings.OverallLmPercent_Text, Value = (decimal)(totLmValues / observations.Count()) };
            SummaryItems.Add(smItem);
            smItem = new SummaryItem() { Label = Strings.OverallPaPercent_Text, Value = (decimal)(totPaValues / observations.Count()) };
            SummaryItems.Add(smItem);
            smItem = new SummaryItem() { Label = Strings.OverallPnPercent_Text, Value = (decimal)(totPnValues / observations.Count()) };
            SummaryItems.Add(smItem);

            var percent = (totRatedCount / (observations.Count() * 2));
            var avg = totRatedValues / (observations.Count() * 2);

            smItem = new SummaryItem() { Label = Strings.OverallPercent_Text, Value = (decimal)(percent) };
            SummaryItems.Add(smItem);
            smItem = new SummaryItem() { Label = Strings.OverallAvg_Text, Value = (decimal)(avg) };
            OverallAvgItem = smItem;
            RaisePropertyChanged(() => OverallAvgItem);
        }

        public List<Observation> ProcessSets(List<ObservationSet> theSet)
        {
            var set = theSet;
            List<Observation> observations = new List<Observation>();
            foreach (var item in set)
            {
                var jsonToView = JsonConvert.SerializeObject(item);
                var observation = new Observation();
                observation.GeoPosition = item.GeoPosition;
                observation.Notes = item.Notes;
                observation.Properties = new List<ObservationItemDetails>();
                foreach (var property in item.ReportedProperties)
                {
                    ObservationItemDetails itemDetails = new ObservationItemDetails();
                    itemDetails.Name = property.ReportablePropertySpec.Label;
                    itemDetails.FullName = property.ReportablePropertySpec.Name;

                    if (property.ReportedListItem != null)
                    {
                        itemDetails.DisplayValue = property.ReportedListItem.Label;
                        itemDetails.Value = property.ReportedListItem.Value;
                    }
                    else
                    {
                        itemDetails.Unit = property.ReportedUnit != null ? property.ReportedUnit.FullDisplay : string.Empty;

                        if (!string.IsNullOrEmpty(itemDetails.Unit))
                        {
                            if (property.ReportedUnit.PackageUnitName == "%")
                            {
                                double theValue = 0.0;
                                if (Double.TryParse(property.ReportedValue, out theValue))
                                {
                                    itemDetails.Value = (theValue * 100).ToString("N0");
                                    itemDetails.DisplayValue = string.Format("{0} {1}", itemDetails.Value, itemDetails.Unit);
                                }
                            }
                            else
                            {
                                itemDetails.Value = !string.IsNullOrEmpty(property.ReportedValue) ? property.ReportedValue : string.Empty;
                                itemDetails.DisplayValue = string.Format("{0} {1}", itemDetails.Value, itemDetails.Unit);
                            }
                        }
                        else
                        {
                            var reportedValueString = !string.IsNullOrEmpty(property.ReportedValue) ? property.ReportedValue : string.Empty;
                            if (!string.IsNullOrEmpty(reportedValueString))
                            {
                                double theValue = 0.0;
                                if (Double.TryParse(reportedValueString, out theValue))
                                {
                                    itemDetails.Value = (theValue).ToString("P0");
                                    itemDetails.DisplayValue = (theValue).ToString("P0");
                                }
                                else
                                {
                                    if (reportedValueString.ToLower() == "true")
                                    {
                                        itemDetails.Value = Strings.Yes_Text.ToUpper();
                                        itemDetails.DisplayValue = Strings.Yes_Text.ToUpper();
                                    }
                                    else if (reportedValueString.ToLower() == "false")
                                    {
                                        itemDetails.Value = Strings.No_Text.ToUpper();
                                        itemDetails.DisplayValue = Strings.No_Text.ToUpper();
                                    }
                                    else
                                    {
                                        itemDetails.Value = reportedValueString;
                                        itemDetails.DisplayValue = reportedValueString;
                                    }
                                }                           
                            }
                            else
                            {
                                itemDetails.Value = Strings.No_Text.ToUpper();
                                itemDetails.DisplayValue = Strings.No_Text.ToUpper();
                            }
                        }
                    }
                    observation.Properties.Add(itemDetails);
                }

                observations.Add(observation);
            }
            return observations;
        }

    }

    public class Observation
    {
        public string Notes { get; set; }
        public GeoPositionDetails GeoPosition { get; set; }
        public List<ObservationItemDetails> Properties { get; set; }
    }

    public class ObservationItemDetails
    {
        public string Name { get; set; }
        public string FullName { get; set; }
        public string DisplayValue { get; set; }
        public string Value { get; set; }
        public string Unit { get; set; }
        public GeoPositionDetails GeoPosition { get; set; }
    }

    public class SummaryItem
    {
        public string Label { get; set; }
        public decimal Value { get; set; }
    }
}
