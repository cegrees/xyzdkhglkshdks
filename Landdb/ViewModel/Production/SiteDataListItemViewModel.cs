﻿using GalaSoft.MvvmLight;
using Landdb.Domain.ReadModels.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Production
{
    public class SiteDataListItemViewModel : ViewModelBase
    {
        DateTime? startDate;
        DateTime? endDate;
        public SiteDataListItemViewModel()
        {
        }

        public ScoutingApplicationId DocumentId { get; set; }
        public ScoutingApplicationId Id { get { return DocumentId; } }
        public string Name { get; set; }
        public DateTime? StartDateTime { get { return startDate.HasValue ? startDate.Value.ToLocalTime() : new DateTime?(); } set { startDate = value; } }
        public DateTime? EndDateTime { get { return endDate.HasValue ? endDate.Value.ToLocalTime() : new DateTime?(); } set { endDate = value; } }
        public int CropYear { get; set; }
        public string DocumentType { get; set; }
        public string Offering { get; set; }
        public string Notes { get; set; }
        public List<ApplicationApplicator> Applicators { get; set; }
        public string Authorization { get; set; }
        public string Centroid { get; set; }
        public ApplicationConditions Conditions { get; set; }
        public List<ObservationSet> ReportedSets { get; set; }
        public int ObservationCount { get { return ReportedSets.Count(); } }
        public List<ObservedCropZone> CropZones { get; set; } //DO SOMETHING ABOUT THIS ONE
        //public List<IncludedProduct> Products { get; set; } //DO SOMETHING ABOUT THIS ONE
        public string CropZoneName { get { return CropZones.Any() ? CropZones[0].Name : string.Empty; } }
        public string TimingEvent { get; set; }
        //public string Offering { get; set; }
    }

    public class ObservationSet
    {
        public string Notes { get; set; }
        public GeoPositionDetails GeoPosition { get; set; }
        public ObservationSetSpec ReportableSetSpec { get; set; }
        public List<ObservationProperty> ReportedProperties { get; set; }
    }

    public class ObservationSetSpec
    {
        public string ReturnToShape { get; set; }
        public string Name { get; set; }
        public Guid ReportableSetId { get; set; }
        public int ReqCntMax { get; set; }
        public int ReqCntMin { get; set; }
        public string ScoutingCategory { get; set; }
    }

    public class ObservationProperty
    {
        public ObservationPropertySpecs ReportablePropertySpec { get; set; }
        public ObservationListItem ReportedListItem { get; set; }
        public CompositeUnit ReportedUnit { get; set; }
        public string ReportedValue { get; set; }
    }

    public class ObservationListItem
    {
        public bool IsActive { get; set; }
        public string Label { get; set; }
        public Guid ReportablePropertyListItemId { get; set; }
        public string Value { get; set; }
    }

    public class ObservationPropertySpecs
    {
        public string DataType { get; set; }
        public string Label { get; set; }
        public string Name { get; set; }
        public List<ObservationListItem> ReportableListItems { get; set; }
        public ReportedPest ReportablePestCombo { get; set; }
        public Guid ReportablePropertyId { get; set; }
    }

    public class ReportedPest
    {
        public int? CommonId { get; set; }
        public string CommonName { get; set; }
        public int LatinId { get; set; }
        public string LatinName { get; set; }
        public int PestId { get; set; }
    }

    public class GeoPositionDetails
    {
        public string Accuracy { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public DateTime? GeoTimestamp { get; set; }
        public DateTime? SysTimestamp { get; set; }
    }

    public class ObservedCropZone
    {
        public CropZoneId CropZoneId { get; set; }
        public string CoverageUnit { get; set; }
        public double CoverageValue { get; set; }
        public string Name { get; set; }
        public string BaseUnit { get; set; }
        public double BaseValue { get; set; }
        public double CoveragePercentage { get; set; }
        public CropId CropId { get; set; }
        public string CropName { get; set; }
        public string CropZoneName { get; set; }
        public string FarmName { get; set; }
        public string FieldName { get; set; }
        public string AreaUnit { get; set; }
        public double AreaValue { get; set; }
    }
}
