﻿using AgC.UnitConversion;
using AgC.UnitConversion.Area;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Client.Spatial;
using Landdb.Domain.ReadModels.Map;
using Landdb.Domain.ReadModels.Plan;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Production.Popups;
using Landdb.ViewModel.Shared;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using System.Windows.Shapes;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Production {
	public class PlanDetailsViewModel : ViewModelBase {

		readonly IClientEndpoint clientEndpoint;
		readonly Dispatcher dispatcher;

		readonly IMasterlistService masterlist;

		readonly CropPlanId planId;

		// TODO: phase this out
		readonly PlanView planDetails;

		ProductDetails selectedProd;

		Popups.ChangePlanFieldsViewModel changeFields;
		Dictionary<string, List<string>> availableTimingEventTags = new Dictionary<string, List<string>>();

		public PlanDetailsViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatch, PlanView planDetails, Dictionary<string, List<string>> availableTimingEventTags) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatch;
			this.masterlist = clientEndpoint.GetMasterlistService();
			this.availableTimingEventTags = availableTimingEventTags;

			this.planId = planDetails.Id;

			this.planDetails = planDetails;

			buildDetailsFromProjection(planDetails);

			ChangeFieldSelectionCommand = new RelayCommand(ChangeFieldSelection);
			EditRevenue = new RelayCommand(EditRevenueInfo);
			EditBudget = new RelayCommand(Edit);
			AddProduct = new RelayCommand(AddPlanProduct);
			EditName = new RelayCommand(EditPlanName);
			EditEstimatedArea = new RelayCommand(EditEstimatedAreaInfo);
			EditNotesInfo = new RelayCommand(NotesInfoEdit);
		}

		public ICommand ChangeFieldSelectionCommand { get; private set; }
		public ICommand EditBudget { get; private set; }
		public ICommand EditRevenue { get; private set; }
		public ICommand AddProduct { get; private set; }
		public ICommand EditName { get; set; }
		public ICommand EditEstimatedArea { get; set; }
		public ICommand EditNotes { get; set; }
		public ICommand EditNotesInfo { get; private set; }

		public ObservableCollection<CropZoneDetails> CropZones { get; private set; }
		public ObservableCollection<ProductDetails> Products { get; private set; }

		public string Name { get; private set; }
		public double Rent { get; private set; }
		public string Notes { get; private set; }

		public double Insurance { get; set; }
		public double Equipment { get; set; }
		public double Labor { get; set; }
		public double Repairs { get; set; }
		public double Taxes { get; set; }
		public double Management { get; set; }

		public double NetRevenue { get; set; }
		public string Yield { get; set; }
		public double PricePerUnit { get; set; }
		public double FSA { get; set; }
		public double Share { get; set; }
		public Measure EstimatedArea { get; set; }
		public double Revenue { get; set; }
		public string RevenuePerAreaDisplay { get { return string.Format(@Strings.Per_Format_Text, (Revenue / EstimatedArea.Value).ToString("C"), EstimatedArea.Unit.AbbreviatedDisplay); } }
		public double Budget { get; set; }
		public double Inputs { get; set; }

		public double NetRevenuePer { get; set; }
		public string NetRevenuePerAreaDisplay { get { return string.Format(@Strings.Per_Format_Text, NetRevenuePer.ToString("C"), EstimatedArea.Unit.AbbreviatedDisplay); } }
		public double BudgetPer { get; set; }
		public string BudgetPerAreaDisplay { get { return string.Format(@Strings.Per_Format_Text, BudgetPer.ToString("C"), EstimatedArea.Unit.AbbreviatedDisplay); } }
		public double InputsPer { get; set; }

		public Dictionary<string, List<string>> AvailableTimingEventTags { get; set; }

		public string FieldCountDisplay { get { return CropZones.Count().ToString() + Strings.CropZones_Text; } }

		public string TotalAreaDisplay {
			get {
				if (CropZones.Any()) {
					return string.Format("{0} {1}", CropZones.Sum(x => x.Area.Value).ToString("N2"), CropZones[0].Area.Unit.FullDisplay);
				} else {
					return string.Format("{0} {1}", EstimatedArea.Value.ToString("N2"), EstimatedArea.Unit.FullDisplay);
				}
			}
		}

		public ProductDetails SelectedItem {
			get { return selectedProd; }
			set {
				selectedProd = value;
				if (value != null) { // TODO: Figure out a different way of doing this than on select
					var vm = new EditPlanProductViewModel(clientEndpoint, selectedProd, planDetails, false, this, availableTimingEventTags);
					var sd = new ScreenDescriptor("Landdb.Views.Production.Popups.EditProductView", "editPlanProduct", vm);
					Messenger.Default.Send(new ShowOverlayMessage() { ScreenDescriptor = sd });
				}
			}
		}

		void AddPlanProduct() {
			var vm = new EditPlanProductViewModel(clientEndpoint, null, planDetails, true, this, availableTimingEventTags);
			var sd = new ScreenDescriptor("Landdb.Views.Production.Popups.EditProductView", "editPlanProduct", vm);
			Messenger.Default.Send(new ShowOverlayMessage() { ScreenDescriptor = sd });
		}

		void ChangeFieldSelection() {
			if (planId != null) {
				changeFields = new Popups.ChangePlanFieldsViewModel(clientEndpoint, dispatcher, ApplicationEnvironment.CurrentCropYear, planDetails.Id, planDetails.CropZones, this);

				var sd = new ScreenDescriptor("Landdb.Views.Shared.Popups.ChangeCropZoneSelectionView", "changeCropzones", changeFields);
				Messenger.Default.Send(new ShowOverlayMessage() { ScreenDescriptor = sd });
			}
		}

		void InitializeProperties(PlanView view) {
			Taxes = view.Taxes;
			Repairs = view.Repairs;
			Management = view.ReturnToMgt;
			Labor = view.Labor;
			Equipment = view.Equipment;
			Insurance = view.Insurance;

			EstimatedArea = view.EstimatedArea;
			Yield = view.YieldPerArea.ToString();
			PricePerUnit = view.PricePerUnit;
			FSA = view.FSAPayment;
			Share = view.CropSharePercent;
		}

		void CalculateNetRevenue() {
			var revenue = ((planDetails.YieldPerArea * EstimatedArea.Value * (double)planDetails.PricePerUnit) * planDetails.CropSharePercent) + (planDetails.FSAPayment * EstimatedArea.Value);
			Revenue = revenue;

			var expend = (Rent + Taxes + Repairs + Management + Labor + Equipment + Insurance) * EstimatedArea.Value;
			Budget = expend;
			BudgetPer = expend / EstimatedArea.Value;

			var inputCost = (double)Products.Sum(x => x.TotalCost);

			Inputs = inputCost;
			InputsPer = inputCost / EstimatedArea.Value;
			NetRevenue = revenue - (expend + inputCost);
			NetRevenuePer = (revenue - (expend + inputCost)) / EstimatedArea.Value;

			RaisePropertyChanged(() => Revenue);
			RaisePropertyChanged(() => Budget);
			RaisePropertyChanged(() => BudgetPer);
			RaisePropertyChanged(() => BudgetPerAreaDisplay);
			RaisePropertyChanged(() => Inputs);
			RaisePropertyChanged(() => InputsPer);
			RaisePropertyChanged(() => NetRevenue);
			RaisePropertyChanged(() => NetRevenuePer);
			RaisePropertyChanged(() => NetRevenuePerAreaDisplay);
		}

		void EditPlanName() {
			if (planId != null) {
				DialogFactory.ShowNameChangeDialog(Strings.PlanName_Text, Name, newName => {
					var cmd = new RenamePlan(planId, clientEndpoint.GenerateNewMetadata(), newName);
					clientEndpoint.SendOne(cmd);

					Name = newName;
					RaisePropertyChanged(() => Name);
				});
			}
		}

		void Edit() {
			if (planId != null) {
				Popups.EditPlanBudget vm = new Popups.EditPlanBudget(clientEndpoint, planDetails, this);

				var sd = new ScreenDescriptor("Landdb.Views.Production.Popups.EditPlanBudgetView", "changeBudget", vm);
				Messenger.Default.Send(new ShowPopupMessage() { ScreenDescriptor = sd });
			}
		}

		void EditRevenueInfo() {
			if (planId != null) {
				Popups.EditPlanRevenue vm = new Popups.EditPlanRevenue(clientEndpoint, planDetails, this, UpdateBudget);

				var sd = new ScreenDescriptor("Landdb.Views.Production.Popups.EditPlanRevenueView", "changeRevenue", vm);
				Messenger.Default.Send(new ShowPopupMessage() { ScreenDescriptor = sd });
			}
		}

		void UpdateBudget(EditPlanRevenue planInfo) {
			PlanInfo info = new PlanInfo();
			if (IsRevenueInfoChanged(planInfo)) {
				info.PricePerUnit = planInfo.PricePerUnit;
				info.YieldPerArea = planInfo.Yield;
				info.FSAPayment = planInfo.FSA;
				info.EstimatedAreaValue = planInfo.EstimatedArea.Value;
				info.CropSharePercent = planInfo.Share;

				info.Equipment = planDetails.Equipment;
				info.Insurance = planDetails.Insurance;
				info.Labor = planDetails.Labor;
				info.LandRent = planDetails.LandRent;
				info.Repairs = planDetails.Repairs;
				info.ReturnToMgt = planDetails.ReturnToMgt;
				info.Taxes = planDetails.Taxes;

				var infoCommand = new ChangePlanInfo(planDetails.Id, clientEndpoint.GenerateNewMetadata(), info);
				clientEndpoint.SendOne(infoCommand);

				EstimatedArea = planInfo.EstimatedArea;
				Revenue = (planInfo.PricePerUnit * planInfo.Yield * planInfo.EstimatedArea.Value * planInfo.Share) + (planInfo.FSA * planInfo.EstimatedArea.Value);

				NetRevenue = (Revenue - (Budget + Inputs));
				NetRevenuePer = NetRevenue / planInfo.EstimatedArea.Value;

				RaisePropertyChanged(() => Revenue);
				RaisePropertyChanged(() => Budget);
				RaisePropertyChanged(() => BudgetPer);
				RaisePropertyChanged(() => BudgetPerAreaDisplay);
				RaisePropertyChanged(() => Inputs);
				RaisePropertyChanged(() => InputsPer);
				RaisePropertyChanged(() => NetRevenue);
				RaisePropertyChanged(() => NetRevenuePer);
				RaisePropertyChanged(() => NetRevenuePerAreaDisplay);
			}

			Messenger.Default.Send(new HidePopupMessage());
		}

		bool IsRevenueInfoChanged(EditPlanRevenue planInfo) {
			return planInfo.PricePerUnit != planDetails.PricePerUnit || planInfo.Yield != planDetails.YieldPerArea || planInfo.EstimatedArea != planDetails.EstimatedArea || planInfo.FSA != planDetails.FSAPayment || planInfo.Share != planDetails.CropSharePercent;
		}

		void EditEstimatedAreaInfo() {
			if (planId != null) {
				Popups.EditPlanRevenue vm = new Popups.EditPlanRevenue(clientEndpoint, planDetails, this, UpdateBudget);

				var sd = new ScreenDescriptor("Landdb.Views.Production.Popups.EditPlanAreaView", "changeEstimatedArea", vm);
				Messenger.Default.Send(new ShowPopupMessage() { ScreenDescriptor = sd });
			}
		}

		void CreateCropZoneDetails() {
			CropZones = new ObservableCollection<CropZoneDetails>();
			var fTreeView = clientEndpoint.GetView<FlattenedTreeHierarchyView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear));
			foreach (var c in planDetails.CropZones) {
				var name = c.Name;

				if (fTreeView.HasValue) {
					var fti = fTreeView.Value.Items.Where(x => x.CropZoneId == c.Id).FirstOrDefault();
					if (fti != null) {
						var farm = clientEndpoint.GetView<FarmDetailsView>(fti.FarmId);
						var field = clientEndpoint.GetView<FieldDetailsView>(fti.FieldId);
						if (farm.HasValue && field.HasValue) {
							name = string.Format("{0}\\{1}\\{2}", farm.Value.Name, field.Value.FieldNameByCropYear[ApplicationEnvironment.CurrentCropYear], fti.CropZoneName);
						}
					}
				}

				var mapItem = clientEndpoint.GetView<ItemMap>(c.Id);
				string mapData = null;
				Path shapePreview = null;

				if (mapItem.HasValue && mapItem.Value.MostRecentMapItem != null) {
					mapData = mapItem.Value.MostRecentMapItem.MapData;

					dispatcher.BeginInvoke(new Action(() => {
						shapePreview = WpfTransforms.CreatePathFromMapData(new[] { mapData }, 40);
						CropZones.Add(new CropZoneDetails() { Id = c.Id, Name = name, Area = c.Area, ShapePreview = shapePreview });
					}));
				} else {
					CropZones.Add(new CropZoneDetails() { Name = name, Area = c.Area, Id = c.Id });
				}
			}
		}

		public void AddProductToPlan(ProductDetails prod) {
			Products.Add(prod);
			Products = new ObservableCollection<ProductDetails>(Products.OrderBy(x => x.TimingEventOrder).ThenBy(y => y.TimingEventTag).ThenBy(x => x.TargetDate));
			RaisePropertyChanged(() => Products);

			CalculateNetRevenue();
		}

		public void EditProductInPlan(ProductDetails prod) {
			var product = Products.Where(x => x.TrackingId == prod.TrackingId).FirstOrDefault();
			Products.Remove(product);
			Products.Add(prod);
			Products = new ObservableCollection<ProductDetails>(Products.OrderBy(x => x.TimingEventOrder).ThenBy(y => y.TimingEventTag).ThenBy(x => x.TargetDate));
			RaisePropertyChanged(() => Products);

			CalculateNetRevenue();
		}

		public void RemoveProductFromPlan(ProductDetails prod) {
			var product = Products.Where(x => x.TrackingId == prod.TrackingId).FirstOrDefault();
			Products.Remove(product);
			Products = new ObservableCollection<ProductDetails>(Products.OrderBy(x => x.TimingEventOrder).ThenBy(y => y.TimingEventTag).ThenBy(x => x.TargetDate));
			RaisePropertyChanged(() => Products);

			CalculateNetRevenue();
		}

		void NotesInfoEdit() {
			if (planId != null) {
				DialogFactory.ShowNotesChangeDialog(Notes, newNotes => {
					var cmd = new EditPlanNotes(planId, clientEndpoint.GenerateNewMetadata(), newNotes);
					clientEndpoint.SendOne(cmd);

					Notes = newNotes;
					RaisePropertyChanged(() => Notes);
				});
			}
		}

		public void ChangeEstimatedArea(Measure estArea) {
			EstimatedArea = estArea;
			RaisePropertyChanged(() => EstimatedArea);

			//Update CropZones
			var czs = changeFields.GetCropZoneAreas();
			for (int i = 0; i < CropZones.Count(); i++) {
				var cz = CropZones[i];
				var hold = czs.Where(x => x.CropZoneId == cz.Id).FirstOrDefault();

				if (hold != null) {
					cz.Area = UnitFactory.GetUnitByName(hold.AreaUnit).GetMeasure(hold.AreaValue) as AreaMeasure;
				}

				if (czs.Where(x => x.CropZoneId == cz.Id).Count() < 1) {
					var removedCZ = CropZones.Where(x => x.Id == cz.Id).FirstOrDefault();
					CropZones.Remove(removedCZ);
				}
			}

			foreach (var cz in czs) {
				if (CropZones.Where(x => x.Id == cz.CropZoneId).Count() > 0) {
					//Initial CropZones contained cz
				} else {
					//Include Added CropZone
					var detailedMaybe = clientEndpoint.GetView<CropZoneDetails>(cz.CropZoneId);
					var czName = detailedMaybe.HasValue ? detailedMaybe.Value.Name : string.Empty;
					var czDetail = new CropZoneDetails() { Area = UnitFactory.GetUnitByName(cz.AreaUnit).GetMeasure(cz.AreaValue) as AreaMeasure, Id = cz.CropZoneId, Name = czName };
					CropZones.Add(czDetail);
				}
			}

			foreach (var prod in Products) {
				var product = clientEndpoint.GetMasterlistService().GetProduct(prod.ID);
				var totalUnit = UnitFactory.GetPackageSafeUnit(prod.TotalProduct.Unit.Name, product.StdUnit, product.StdFactor, product.StdPackageUnit);
				var newRateMeasure = prod.RateMeasure.Unit.GetMeasure(prod.RateMeasure.Value, product.Density);
				var convertedTotal = newRateMeasure.GetValueAs(totalUnit) * EstimatedArea.Value;
				prod.TotalProduct = prod.TotalProduct.Unit.GetMeasure(convertedTotal);
				prod.TotalCost = prod.CostPerUnitValue * (decimal)prod.TotalProduct.Value;
			}

			RaisePropertyChanged(() => CropZones);
			CalculateNetRevenue();
		}

		private void buildDetailsFromProjection(PlanView planDetails) {
			Name = planDetails.Name;
			Rent = planDetails.LandRent;
			Notes = planDetails.Notes;

            //PLANS ARE SUPPOSE TO RUN OFF ESTIMATED AREA DUE TO CROPZONES NOT BEING REQUIRED.
			//var totalArea = (decimal)planDetails.CropZones.Sum(x => x.Area.Value);
            var totalArea = (decimal)planDetails.EstimatedArea.Value;

			Products = new ObservableCollection<ProductDetails>();
            var timingEvents = clientEndpoint.GetMasterlistService().GetTimingEventList().ToList();

			foreach (var p in planDetails.Products) {
				if (p == null || string.IsNullOrWhiteSpace(p.RateUnit) || string.IsNullOrWhiteSpace(p.AreaUnit) || string.IsNullOrWhiteSpace(p.RateUnit) || string.IsNullOrWhiteSpace(p.CostUnit)) {
					continue; // Guard statement for weeding out badly formatted products
				}

				//HACK :: SOME USER CREATED PRODUCTS ARE GETTING THE INCORRECT PRODUCTID
				var masterlistProdQuery = clientEndpoint.GetMasterlistService().GetProduct(p.Id);
				var masterlistProd = masterlistProdQuery == null ? masterlist.GetProduct(new ProductId(ApplicationEnvironment.CurrentDataSourceId, p.Id.Id)) : masterlistProdQuery;
				//masterlistProd = masterlistProd == null ? masterlist.GetProductList().SingleOrDefault(x => x.Id == p.Id.Id) : masterlistProd;

				// we can't trust the value in p.TotalProductValue anymore
				//var displayTotal = DisplayMeasureConverter.ConvertToTotalDisplayMeasure(p.Id, p.TotalProductValue, p.TotalProductUnit, masterlistProd.Density, endPoint, ApplicationEnvironment.CurrentDataSourceId);

				// calculate the total from the rate
				var totalProduct = p.RateValue * totalArea * p.PercentApplied * p.ApplicationCount;
				var totalProductMeasure = UnitFactory.GetPackageSafeUnit(p.RateUnit, masterlistProd.StdUnit, masterlistProd.StdFactor, masterlistProd.StdPackageUnit).GetMeasure((double)totalProduct, masterlistProd.Density);
				var totalProductUnit = UnitFactory.GetPackageSafeUnit(p.TotalProductUnit, masterlistProd.StdUnit, masterlistProd.StdFactor, masterlistProd.StdPackageUnit);
				var convertedTotalProduct = totalProductMeasure.GetValueAs(totalProductUnit);
				var displayTotal = DisplayMeasureConverter.ConvertToTotalDisplayMeasure(p.Id, (decimal)convertedTotalProduct, p.TotalProductUnit, masterlistProd.Density, clientEndpoint, ApplicationEnvironment.CurrentDataSourceId);

				var displayRate = DisplayMeasureConverter.ConvertToRateDisplayMeasure(p.Id, p.RateValue, p.RateUnit, masterlistProd.Density, clientEndpoint, ApplicationEnvironment.CurrentDataSourceId);
				var unit = string.IsNullOrEmpty(p.SpecificCostUnit) ? null : UnitFactory.GetPackageSafeUnit(p.SpecificCostUnit, masterlistProd.StdUnit, masterlistProd.StdFactor, masterlistProd.StdPackageUnit);
				decimal totalCost = 0m;

				try {
					if (displayTotal.CanConvertTo(unit)) {
						var convertedTotal = unit != null && displayTotal.CanConvertTo(unit) ? displayTotal.GetValueAs(unit) : displayTotal.Value;
						totalCost = p.SpecificCostPerUnit.HasValue ? (decimal)convertedTotal * p.SpecificCostPerUnit.Value : p.ProductTotalCost;
					} else {
						totalCost = 0m;
					}
				} catch (Exception ex) {
					totalCost = 0m;
				}

                var timing = !string.IsNullOrEmpty(p.TimingEvent) ? timingEvents.SingleOrDefault(x => x.Name == p.TimingEvent) : null;

				Products.Add(new ProductDetails() {
					ProductName = masterlistProd.Name,
					RateMeasure = displayRate,
					AreaUnit = UnitFactory.GetUnitByName(p.AreaUnit),
					TotalProduct = displayTotal,
					TotalCost = totalCost,
					CostPerUnitValue = p.SpecificCostPerUnit.HasValue ? p.SpecificCostPerUnit.Value : 0m,
					CostUnit = !string.IsNullOrEmpty(p.SpecificCostUnit) ? UnitFactory.GetUnitByName(p.SpecificCostUnit) : UnitFactory.GetUnitByName(p.CostUnit),
					ID = p.Id,
					ProductAreaPercent = p.PercentApplied,
					TrackingId = p.TrackingId,
					TimingEvent = p.TimingEvent,
					TargetDate = p.TargetDate,
					TimingEventTag = p.TimingEventTag,
                    TimingEventOrder = timing != null ? (int?)timing.Order : null,
					ApplicationCount = p.ApplicationCount.HasValue ? p.ApplicationCount.Value : 1,
				});
			}

            //Products = Products.OrderBy(x => x.TimingEventOrder).ThenBy(x => x.TimingEventTag);
			Products = new ObservableCollection<ProductDetails>(Products.OrderBy(x => x.TimingEventOrder).ThenBy(x => x.TimingEventTag).ThenBy(b => b.TargetDate));

			CreateCropZoneDetails();
			InitializeProperties(planDetails);
			CalculateNetRevenue();
		}
	}

	public class ProductDetails : ViewModelBase {

		Measure totalProduct;
		decimal totalCost;

		public ProductDetails() { }

		public string ProductName { get; set; }
		public Measure RateMeasure { get; set; }
		public IUnit AreaUnit { get; set; }
		public decimal CostPerUnitValue { get; set; }
		public IUnit CostUnit { get; set; }
		public ProductId ID { get; set; }
		public decimal ProductAreaPercent { get; set; }
		public Guid TrackingId { get; set; }
		public string TimingEvent { get; set; }
		public int? TimingEventOrder { get; set; }
		public DateTime? TargetDate { get; set; }
		public string TimingEventTag { get; set; }
		public int ApplicationCount { get; set; }

		public string Rate {
			get { return string.Format(@"{0} / {1}", RateMeasure.AbbreviatedDisplay, AreaUnit.FullDisplay); }
		}

		public string CostPerUnitDisplay {
			get { return string.Format(Strings.Per_Format_Text, CostPerUnitValue.ToString("C"), CostUnit.AbbreviatedDisplay); }
		}

		public Measure TotalProduct {
			get { return totalProduct; }
			set { totalProduct = value; RaisePropertyChanged(() => TotalProduct); }
		}

		public decimal TotalCost {
			get { return totalCost; }
			set { totalCost = value; RaisePropertyChanged(() => TotalCost); }
		}
	}
}