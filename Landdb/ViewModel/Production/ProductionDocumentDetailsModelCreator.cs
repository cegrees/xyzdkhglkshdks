﻿using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Invoice;
using Landdb.Domain.ReadModels.WorkOrder;
using Landdb.Domain.ReadModels.Recommendation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Plan;
using System.Windows.Threading;
using Landdb.Domain.ReadModels.Tags;
using Landdb.Infrastructure;
using System.IO;
using Landdb.ViewModel.Production.Applications;
using Landdb.ViewModel.Production.Invoices;
using Newtonsoft.Json;

namespace Landdb.ViewModel.Production {
    public class ProductionDocumentDetailsModelCreator {
        IClientEndpoint clientEndpoint;
        Dispatcher dispatcher;

        public ProductionDocumentDetailsModelCreator(IClientEndpoint clientEndpoint, Dispatcher dispatcher) {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
        }

        public object CreateDetailsModel(AbstractDataSourceIdentity<Guid, Guid> id) {
            if (id is WorkOrderId) {
                return CreateDetailsModel((WorkOrderId)id);
            }
            if (id is InvoiceId) {
                return CreateDetailsModel((InvoiceId)id);
            }
            if (id is RecommendationId) {
                return CreateDetailsModel((RecommendationId)id);
            }
            if (id is ApplicationId) {
                return CreateDetailsModel((ApplicationId)id);
            }
            if (id is CropPlanId) {
                return CreateDetailsModel((CropPlanId)id);
            }
            if (id is SiteDataId || id is ScoutingApplicationId)
            {
                return CreateDetailsModel((ScoutingApplicationId)id);
            }
            return null;
        }

        public ApplicationDetailsViewModel CreateDetailsModel(WorkOrderId id) {
            var woMaybe = clientEndpoint.GetView<WorkOrderView>(id);
            if (woMaybe.HasValue) {
                return new ApplicationDetailsViewModel(clientEndpoint, dispatcher, woMaybe.Value);
            } else {
                return null;
            }
        }

        public InvoiceDetailsViewModel CreateDetailsModel(InvoiceId id) {
            var invMaybe = clientEndpoint.GetView<InvoiceView>(id);
            if (invMaybe.HasValue) {
                return new ActualInvoiceDetailsViewModel(clientEndpoint, invMaybe.Value);
            } else { return null; }
        }

        public ApplicationDetailsViewModel CreateDetailsModel(ApplicationId applicationId) {
            var invMaybe = clientEndpoint.GetView<ApplicationView>(applicationId);
            if (invMaybe.HasValue) {
                return new ApplicationDetailsViewModel(applicationId, clientEndpoint, dispatcher, invMaybe.Value);
            } else { return null; }
        }

        public RecommendationDetailsViewModel CreateDetailsModel(RecommendationId id) {
            var recMaybe = clientEndpoint.GetView<RecommendationView>(id);
            if (recMaybe.HasValue) {
                return new RecommendationDetailsViewModel(clientEndpoint, null, recMaybe.Value);
            } else { return null; }
        }

        public SiteDataDetailsViewModel CreateDetailsModel(ScoutingApplicationId id)
        {
            var baseDir = ApplicationEnvironment.UserAppDataPath; // ApplicationEnvironment.BaseDataDirectory;
            var fileDir = Path.Combine(baseDir, "Scouting");
            string fileName = string.Format("{0}_{1}", ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear);
            var filePath = Path.Combine(fileDir, fileName);

            if (Directory.Exists(fileDir) && File.Exists(filePath))
            {
                var data = File.ReadAllText(filePath);
                ScoutingList scoutingList = JsonConvert.DeserializeObject<ScoutingList>(data);

                //return scoutingList;
                var item = scoutingList.Results.FirstOrDefault(x => x.DocumentId == id);
                var detail = new SiteDataDetailsViewModel(clientEndpoint, dispatcher, item);
                return detail;
            }
            else
            {
                return null;
            }
        }

        public PlanDetailsViewModel CreateDetailsModel(CropPlanId id) {
            Dictionary<string, List<string>> availableTimingEventTags = new Dictionary<string, List<string>>();
            var plantagviewMaybe = clientEndpoint.GetView<PlanProductTagProjectionView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear));
            if (plantagviewMaybe.HasValue && plantagviewMaybe.Value.TagInfo != null) {
                foreach (var v in plantagviewMaybe.Value.TagInfo) {
                    if (string.IsNullOrEmpty(v.TimingEvent)) { continue; }
                    if (!availableTimingEventTags.ContainsKey(v.TimingEvent)) {
                        availableTimingEventTags.Add(v.TimingEvent, new List<string>());
                    }
                    foreach (var x in v.Tags) {
                        if (availableTimingEventTags.ContainsKey(v.TimingEvent)) {
                            if (!string.IsNullOrEmpty(x.Key) && !availableTimingEventTags[v.TimingEvent].Contains(x.Key)) {
                                availableTimingEventTags[v.TimingEvent].Add(x.Key);
                            }
                        }
                    }
                }
            }

            var invMaybe = clientEndpoint.GetView<PlanView>(id);
            if (invMaybe.HasValue) {
                foreach (var v in invMaybe.Value.Products) {
                    if (v.TimingEvent == null) { continue; }
                    if (!availableTimingEventTags.ContainsKey(v.TimingEvent)) {
                        availableTimingEventTags.Add(v.TimingEvent, new List<string>());
                    }
                    if (availableTimingEventTags.ContainsKey(v.TimingEvent)) {
                        if (!string.IsNullOrEmpty(v.TimingEventTag) && !availableTimingEventTags[v.TimingEvent].Contains(v.TimingEventTag)) {
                            availableTimingEventTags[v.TimingEvent].Add(v.TimingEventTag);
                        }
                    }
                }

                return new PlanDetailsViewModel(clientEndpoint, dispatcher, invMaybe.Value, availableTimingEventTags);
            } else { return null; }
        }
    }
}
