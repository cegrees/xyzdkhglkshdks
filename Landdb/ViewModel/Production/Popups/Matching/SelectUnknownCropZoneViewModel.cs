﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Recommendation;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace Landdb.ViewModel.Production.Popups.Matching
{
    public class SelectUnknownCropZoneViewModel : ViewModelBase
    {
        IClientEndpoint clientEndpoint;
        Dispatcher dispatcher;
        RecommendationView view;
        RecommendationDetailsViewModel parent;

        public SelectUnknownCropZoneViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, RecommendationView view, RecommendationDetailsViewModel parent)
        {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            this.view = view;
            this.parent = parent;

            ContinueCommand = new RelayCommand(Continue);
            CancelCommand = new RelayCommand(Cancel);
        }

        public ICommand ContinueCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }
        public ObservableCollection<UnknownCropZone> UnknownCropZones
        {
            get
            {
                return (new ObservableCollection<UnknownCropZone>(view.unknownCropZones));
            }
        }
        public UnknownCropZone SelectedUnknown { get; set; }

        public void Refresh()
        {
            SelectedUnknown = null;
        }
        public void RefreshParent()
        {
            parent.RefreshFieldsAndProducts();
        }
        void Continue()
        {
            if ((view != null) && (SelectedUnknown != null))
            {
                Messenger.Default.Send(new HidePopupMessage());

                ResolveUnknownCropZoneViewModel vm = new ResolveUnknownCropZoneViewModel(clientEndpoint, dispatcher, ApplicationEnvironment.CurrentCropYear, view, SelectedUnknown, this);
                var sd = new ScreenDescriptor("Landdb.Views.Production.Popups.Matching.ResolveUnknownCropZoneView", "resolveCropzone", vm);
                Messenger.Default.Send(new ShowOverlayMessage() { ScreenDescriptor = sd });
            }
           
        }

        void Cancel()
        {
            Messenger.Default.Send(new HidePopupMessage());
        }

    }
}
