﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Company;
using Landdb.Domain.ReadModels.Person;
using Landdb.Domain.ReadModels.Recommendation;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.Views.Production.Popups.Matching;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;


namespace Landdb.ViewModel.Production.Popups.Matching
{
    public class ResolveUnknownApplicatorViewModel : ViewModelBase
    {
        IClientEndpoint clientEndpoint;
        Dispatcher dispatcher;
        RecommendationView view;
        RecommendationDetailsViewModel parent;

        ObservableCollection<UnknownApplicator> unknownApplicators = new ObservableCollection<UnknownApplicator>();
        UnknownApplicator selectedUnknownApplicator = null;
        ObservableCollection<PotentialApplicatorViewModel> knownApplicators = new ObservableCollection<PotentialApplicatorViewModel>();
        PotentialApplicatorViewModel selectedKnownApplicator = null;
        List<CompanyListItem> companyList;
        ObservableCollection<KeyValuePair<UnknownApplicator, ApplicationApplicator>> matchedApplicators = new ObservableCollection<KeyValuePair<UnknownApplicator, ApplicationApplicator>>();
        KeyValuePair<UnknownApplicator, ApplicationApplicator> selectedMatchedApplicator;


        public ResolveUnknownApplicatorViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, RecommendationView view, UnknownApplicator[] unknowns, RecommendationDetailsViewModel parent)
        {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            this.view = view;
            this.parent = parent;

            unknownApplicators = new ObservableCollection<UnknownApplicator>(unknowns);

            PopulateKnownApplicators();

            SaveCommand = new RelayCommand(Save);
            CancelCommand = new RelayCommand(Cancel);
            MatchCommand = new RelayCommand(Match);

        }

        public ICommand SaveCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }
        public ICommand MatchCommand { get; private set; }

        public ObservableCollection<UnknownApplicator> UnknownApplicators
        {
            get { return unknownApplicators; }
            private set
            {
                unknownApplicators = value;
            }
        }
        public UnknownApplicator SelectedUnknownApplicator
        {
            get { return selectedUnknownApplicator; }
            set
            {
                selectedUnknownApplicator = value;
            }
        }
        public ObservableCollection<PotentialApplicatorViewModel> KnownApplicators
        {
            get { return knownApplicators; }
            private set
            {
                knownApplicators = value;
            }
        }
        public PotentialApplicatorViewModel SelectedKnownApplicator
        {
            get { return selectedKnownApplicator; }
            set
            {
                selectedKnownApplicator = value;
                RaisePropertyChanged("SelectedKnownApplicator");
            }
        }
        public ObservableCollection<KeyValuePair<UnknownApplicator, ApplicationApplicator>> MatchedApplicators
        {
            get { return matchedApplicators; }
            private set
            {
                matchedApplicators = value;
            }
        }
        public KeyValuePair<UnknownApplicator, ApplicationApplicator> SelectedMatchedApplicator
        {
            get { return selectedMatchedApplicator; }
            set
            {
                selectedMatchedApplicator = value;
            }
        }


        void PopulateKnownApplicators()
        {
            KnownApplicators.Clear();
            DataSourceId dsId = new DataSourceId(ApplicationEnvironment.CurrentDataSourceId);
			var currentCropYear = ApplicationEnvironment.CurrentCropYear;

            // Get companies
            var companyListView = clientEndpoint.GetView<CompanyListView>(dsId).GetValue(new CompanyListView());
			var sortedFilteredCompanies = from c in companyListView.Companies
										  where c.IsActiveInCropYear(currentCropYear)
										  orderby c.Name
										  select c;

			companyList = sortedFilteredCompanies.ToList();

            // Populate people
			var personListView = clientEndpoint.GetView<PersonListView>(dsId).GetValue(new PersonListView());
			var sortedFilteredPersons = from p in personListView.Persons
										where p.IsActiveInCropYear(currentCropYear)
										orderby p.Name
										select p;

			sortedFilteredPersons.ForEach(x => KnownApplicators.Add(new PotentialApplicatorViewModel(clientEndpoint, x, companyList, OnItemChanged)));
        }

        void OnItemChanged(PotentialApplicatorViewModel changedItem)
        {

        }

        void Match()
        {
            if ((SelectedKnownApplicator != null) && (SelectedUnknownApplicator != null))
            {
                string compName = null;
                CompanyId companyId = null;
                if (SelectedKnownApplicator.Company != null)
                {
                    compName = SelectedKnownApplicator.Company.Name;
                    companyId = SelectedKnownApplicator.Company.Id;
                }
                KeyValuePair<UnknownApplicator, ApplicationApplicator> newMatch = new KeyValuePair<UnknownApplicator, ApplicationApplicator>(SelectedUnknownApplicator, new ApplicationApplicator(SelectedKnownApplicator.PersonId, SelectedKnownApplicator.Name, companyId, compName, SelectedKnownApplicator.LicenseNumber, SelectedKnownApplicator.Expires));
                MatchedApplicators.Add(newMatch);
                UnknownApplicators.Remove(SelectedUnknownApplicator);
                SelectedUnknownApplicator = null;
                SelectedKnownApplicator = null;
                RaisePropertyChanged("UnknownApplicators");
                RaisePropertyChanged("SelectedUnknownApplicator");
                RaisePropertyChanged("SelectedKnownApplicator");
                RaisePropertyChanged("MatchedApplicators");
            }
        }
        void Remove(KeyValuePair<UnknownApplicator, ApplicationApplicator> itemToRemove)
        {
            UnknownApplicators.Add(itemToRemove.Key);
            MatchedApplicators.Remove(itemToRemove);
            RaisePropertyChanged("UnknownApplicators");
            RaisePropertyChanged("MatchedApplicators");
        }

        
        void Save()
        {
            Messenger.Default.Send(new HidePopupMessage());
            if (view != null)
            {
                SelectMatchingScopeViewModel vm = new SelectMatchingScopeViewModel(clientEndpoint, dispatcher, this);
                var sd = new ScreenDescriptor("Landdb.Views.Production.Popups.Matching.SelectMatchingScopeView", "selectScope", vm);
                Messenger.Default.Send(new ShowOverlayMessage() { ScreenDescriptor = sd });
            }
        }

        void Cancel()
        {
            Messenger.Default.Send(new HidePopupMessage());
        }

        public void SubmitMatching(MatchingScope scope, DateTime start, DateTime end)
        {
            foreach (var match in MatchedApplicators)
            {
                var cmd = new ResolveUnknownRecommendationApplicator(view.Id, clientEndpoint.GenerateNewMetadata(), match.Key, match.Value, scope, start, end);
                clientEndpoint.SendOne(cmd);
            }
            // I know this is lame....want to give the command time to propogate
            Thread.Sleep(2000);
            parent.RefreshApplicators();
        }
        public void CancelScope()
        {
            var sd = new ScreenDescriptor("Landdb.Views.Production.Popups.Matching.ResolveUnknownApplicatorView", "resolveApplicator", this);
            Messenger.Default.Send(new ShowPopupMessage() { ScreenDescriptor = sd });
        }

    }
}
