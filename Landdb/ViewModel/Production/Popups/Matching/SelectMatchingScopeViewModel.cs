﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Recommendation;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.Views.Production.Popups.Matching;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace Landdb.ViewModel.Production.Popups.Matching {
    public class SelectMatchingScopeViewModel : ViewModelBase {
        ResolveUnknownAuthorizationViewModel authorizerView = null;
        ResolveUnknownApplicatorViewModel applicatorView = null;
        ResolveUnknownCropZoneViewModel cropZoneView = null;
        ResolveUnknownRecommendedProductViewModel productView = null;

        public SelectMatchingScopeViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, ResolveUnknownAuthorizationViewModel parent) {
            authorizerView = parent;
            Initialize(clientEndpoint, dispatcher);
        }
        public SelectMatchingScopeViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, ResolveUnknownApplicatorViewModel parent) {
            applicatorView = parent;
            Initialize(clientEndpoint, dispatcher);
        }
        public SelectMatchingScopeViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, ResolveUnknownCropZoneViewModel parent) {
            cropZoneView = parent;
            Initialize(clientEndpoint, dispatcher);
        }
        public SelectMatchingScopeViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, ResolveUnknownRecommendedProductViewModel parent) {
            productView = parent;
            Initialize(clientEndpoint, dispatcher);
        }

        public void Initialize(IClientEndpoint clientEndpoint, Dispatcher dispatcher) {
            IsScopeSingleInstance = false;
            IsScopeAlways = false;
            IsScopeDateRange = false;
            StartDate = DateTime.MinValue;
            EndDate = DateTime.MaxValue;

            CancelCommand = new RelayCommand(Cancel);
            ContinueCommand = new RelayCommand(Continue);
            SetScopeSingleInstance = new RelayCommand(SingleInstance);
            SetScopeAlways = new RelayCommand(Always);
            SetScopeDateRange = new RelayCommand(DateRange);
        }

        public ICommand CancelCommand { get; private set; }
        public ICommand ContinueCommand { get; private set; }
        public bool IsScopeSingleInstance { get; set; }
        public ICommand SetScopeSingleInstance { get; private set; }
        public bool IsScopeAlways { get; set; }
        public ICommand SetScopeAlways { get; private set; }
        public bool IsScopeDateRange { get; set; }
        public ICommand SetScopeDateRange { get; private set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public MatchingScope SelectedScope {
            get {
                if (IsScopeAlways)
                    return (MatchingScope.Always);
                else if (IsScopeDateRange)
                    return (MatchingScope.DateRange);
                else
                    return (MatchingScope.SingleInstance);
            }
        }

        void Continue() {
            Messenger.Default.Send<HideOverlayMessage>(new HideOverlayMessage());
            if (authorizerView != null)
                authorizerView.SubmitMatching(SelectedScope, StartDate, EndDate);
            if (applicatorView != null)
                applicatorView.SubmitMatching(SelectedScope, StartDate, EndDate);
            if (cropZoneView != null)
                cropZoneView.SubmitMatching(SelectedScope, StartDate, EndDate);
            if (productView != null)
                productView.SubmitMatching(SelectedScope, StartDate, EndDate);

        }
        void Cancel() {
            Messenger.Default.Send<HideOverlayMessage>(new HideOverlayMessage());
            if (authorizerView != null)
                authorizerView.CancelScope();
            if (applicatorView != null)
                applicatorView.CancelScope();
            if (cropZoneView != null)
                cropZoneView.CancelScope();
            if (productView != null)
                productView.CancelScope();

        }
        void SingleInstance() {
            IsScopeSingleInstance = true;
            IsScopeAlways = false;
            IsScopeDateRange = false;
            Continue();
        }
        void Always() {
            IsScopeSingleInstance = false;
            IsScopeAlways = true;
            IsScopeDateRange = false;
            Continue();
        }
        void DateRange() {
            IsScopeSingleInstance = false;
            IsScopeAlways = false;
            IsScopeDateRange = true;
            StartDate = new DateTime(DateTime.Today.Year, 1, 1);
            EndDate = new DateTime(DateTime.Today.Year, 12, 31);
            RaisePropertyChanged("IsScopeDateRange");
            RaisePropertyChanged("StartDate");
            RaisePropertyChanged("EndDate");
        }
    }
}