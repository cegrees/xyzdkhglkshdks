﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Recommendation;
using Landdb.Domain.ReadModels.Shared;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Fields;
using Landdb.ViewModel.Shared;
using Landdb.Views.Production.Popups.Matching;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;

namespace Landdb.ViewModel.Production.Popups.Matching
{
    public class ResolveUnknownCropZoneViewModel : BaseFieldSelectionViewModel {
        UnknownCropZone unknownCropZone;
        SelectUnknownCropZoneViewModel parent;
        RecommendationView view;
        List<KeyValuePair<string, string>> summaryData = new List<KeyValuePair<string, string>>();


        public ResolveUnknownCropZoneViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, int cropYear, RecommendationView view, UnknownCropZone unknownCropZone, SelectUnknownCropZoneViewModel parent)
            : base(clientEndpoint, dispatcher, cropYear) {

            this.unknownCropZone = unknownCropZone;
            this.parent = parent;            
            this.view = view;

            SummaryData = JsonConvert.DeserializeObject<List<KeyValuePair<string, string>>>(unknownCropZone.Data);
            SaveCommand = new RelayCommand(Save);
            CancelCommand = new RelayCommand(Cancel);
        }


        public ICommand SaveCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }
        public List<KeyValuePair<string, string>> SummaryData
        {
            get
            {
                return (summaryData);
            }
            set
            {
                summaryData = value;
            }
        }

        void Save() 
        {
            Messenger.Default.Send<HideOverlayMessage>(new HideOverlayMessage());
            if (view != null)
            {
                SelectMatchingScopeViewModel vm = new SelectMatchingScopeViewModel(clientEndpoint, dispatcher, this);
                var sd = new ScreenDescriptor("Landdb.Views.Production.Popups.Matching.SelectMatchingScopeView", "selectScope", vm);
                Messenger.Default.Send(new ShowOverlayMessage() { ScreenDescriptor = sd });
            }
        }

        void Cancel() {
            Messenger.Default.Send<HideOverlayMessage>(new HideOverlayMessage());
            if ((view.unknownCropZones != null) && (view.unknownCropZones.Count > 0))
            {
                var sd = new ScreenDescriptor("Landdb.Views.Production.Popups.Matching.SelectUnknownCropZoneView", "resolveCropzones", parent);
                Messenger.Default.Send(new ShowPopupMessage() { ScreenDescriptor = sd });
            }
        }

        public void SubmitMatching(MatchingScope scope, DateTime start, DateTime end)
        {
            List<CropZoneArea> matches = new List<CropZoneArea>(GetCropZoneAreas());
            List<CropZoneArea> outgoing = new List<CropZoneArea>();
            double areaTotal = 0.0;
            foreach (var match in matches)
                areaTotal += match.AreaValue;
            if (areaTotal > 0.0)
            {
                foreach (var match in matches)
                {
                    double thisArea = (match.AreaValue / areaTotal) * unknownCropZone.AreaValue;
                    outgoing.Add(new CropZoneArea(match.CropZoneId, thisArea, unknownCropZone.AreaUnit));
                }
            }

            var cmd = new ResolveUnknownRecommendedCropZone(view.Id, clientEndpoint.GenerateNewMetadata(), unknownCropZone, outgoing.ToArray(), scope, start, end);
            clientEndpoint.SendOne(cmd);

            // I know this is lame....want to give the command time to propogate
            Thread.Sleep(2000);
            parent.RefreshParent();

            if ((view.unknownCropZones != null) && (view.unknownCropZones.Count > 0))
            {
                var sd = new ScreenDescriptor("Landdb.Views.Production.Popups.Matching.SelectUnknownCropZoneView", "resolveCropzones", parent);
                Messenger.Default.Send(new ShowPopupMessage() { ScreenDescriptor = sd });
            }
        }

        public void CancelScope()
        {
            var sd = new ScreenDescriptor("Landdb.Views.Production.Popups.Matching.ResolveUnknownCropZoneView", "resolveCropzone", this);
            Messenger.Default.Send(new ShowOverlayMessage() { ScreenDescriptor = sd });
        }

        protected override void OnFieldCheckedChanged(CropZoneTreeItemViewModel treeItem)
        {
            if (treeItem.IsChecked.HasValue && !treeItem.IsChecked.Value)
            {
                var found = SelectedCropZones.Where(x => x.Id == (CropZoneId)treeItem.Id).ToList();
                found.ForEach(x =>
                {
                    SelectedCropZones.Remove(x);
                    TotalArea -= x.SelectedArea;
                });
            }
        }

    }
}
