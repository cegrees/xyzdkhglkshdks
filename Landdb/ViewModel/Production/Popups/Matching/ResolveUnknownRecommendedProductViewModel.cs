﻿using AgC.UnitConversion;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Client.Models;
using Landdb.Domain.ReadModels.Product;
using Landdb.Domain.ReadModels.Recommendation;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Resources;
using Landdb.ViewModel.Secondary.Application;
using Landdb.ViewModel.Secondary.UserCreatedProduct;
using Landdb.ViewModel.Shared;
using Landdb.Views.Production.Popups.Matching;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Production.Popups.Matching
{
    public class ResolveUnknownRecommendedProductViewModel : ViewModelBase
    {
        Guid trackingId;
        ProductListItemDetails selectedProduct;

        // Rate per area values
        decimal ratePerAreaValue;
        UnitPerAreaHelper ratePerAreaUnitHelper;

        // Total product values
        decimal totalProductValue;
        IUnit totalProductUnit;

        // Rate per tank values
        decimal ratePerTankValue;
        IUnit ratePerTankUnit;

        decimal? productAreaPercent = 1m;
        Measure productArea;

        ApplicationMethod appMethod;
        PestListItemDetails pest;
        string pestSearch;
        IApplicationCalculationService applicationCalculationService;
        IRecommendationCommandService recommendationCommandService;

        bool isProductListVisible = false;
        bool isCreateProductPopupOpen = false;

        Measure totalArea;

        RecommendationDetailsViewModel parent;
        RecommendationDetailsViewModel.ProductDetails details;
        RecommendationView view;

        IClientEndpoint clientEndpoint;
        Dispatcher dispatcher;
        ApplicationTypes applyBy;
        
        UnknownRecommendedProduct unknown;
        DataSourceId currentDataSourceId;
        int currentCropYear;
        List<KeyValuePair<string, string>> summaryData = new List<KeyValuePair<string, string>>();

        public ResolveUnknownRecommendedProductViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, RecommendationView view, RecommendationDetailsViewModel parent, RecommendationDetailsViewModel.ProductDetails details, UnknownRecommendedProduct unknown)
        {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            this.view = view;
            this.parent = parent;
            this.unknown = unknown;

            this.currentDataSourceId = new DataSourceId(ApplicationEnvironment.CurrentDataSourceId);
            this.currentCropYear = ApplicationEnvironment.CurrentCropYear;

            SummaryData = JsonConvert.DeserializeObject<List<KeyValuePair<string, string>>>(unknown.Data);

            SaveCommand = new RelayCommand(Save);
            CancelCommand = new RelayCommand(Cancel);

            AllowedRateUnits = new ObservableCollection<UnitPerAreaHelper>();
            AllowedTotalUnits = new ObservableCollection<IUnit>();
            this.applicationCalculationService = new ApplicationCalculationService();
            this.recommendationCommandService = new RecommendationCommandService();
            Products = new ObservableCollection<ProductListItemDetails>();
            Task.Run(() => { BuildProductList(); PopulatePests(); });

            
            this.details = details;
            ApplyBy = (ApplicationTypes)view.ApplicationStrategy;
            totalArea = view.TotalArea;
            Guid mlDsId = GlobalIdentifiers.GetMasterlistDataSourceId();
            if (details.ID != null)
                SelectedProduct = new ProductListItemDetails(clientEndpoint.GetMasterlistService().GetProduct(details.ID), details.ID.DataSourceId, "Used this year", (short)0);
            else
                SelectedProduct = null;
            RatePerTankValue = details.RatePerTank != null ? (decimal)details.RatePerTank.Value : 0m;
            RatePerAreaValue = details.RateMeasure != null ? (decimal)details.RateMeasure.Value : 0m;
            ProductAreaPercent = details.ProductAreaPercent != null ? (decimal)details.ProductAreaPercent.Value : 1.0m;
            ProductArea = totalArea;
            ApplicationMethod = details.ApplicationMethod;
            RatePerTankValue = details.RatePerTank != null ? (decimal)details.RatePerTank.Value : 0m;
            RaisePropertyChanged("ApplicationMethod");
            if (SelectedProduct != null)
            {
                var rateSafeUnit = UnitFactory.GetPackageSafeUnit(details.RateMeasure.Unit.Name, SelectedProduct.Product.StdUnit, SelectedProduct.Product.StdFactor, SelectedProduct.Product.StdPackageUnit);
                SelectedRatePerAreaUnitHelper = new UnitPerAreaHelper(rateSafeUnit, view.TotalArea.Unit);
                var ratePerTankUnit = details.RatePerTank != null ? details.RatePerTank.Unit.Name : SelectedProduct.Product.StdPackageUnit;
                var ratePerTankSafeUnit = UnitFactory.GetPackageSafeUnit(ratePerTankUnit, SelectedProduct.Product.StdUnit, SelectedProduct.Product.StdFactor, SelectedProduct.Product.StdPackageUnit);
                SelectedRatePerTankUnit = ratePerTankSafeUnit;
                TotalProductUnit = UnitFactory.GetPackageSafeUnit(details.TotalProduct.Unit.Name, SelectedProduct.Product.StdUnit, SelectedProduct.Product.StdFactor, SelectedProduct.Product.StdPackageUnit);
                TotalProductValue = (decimal)details.TotalProduct.Value;
            }
            else
            {
                ProductSearchText = details.ProductName;
                RaisePropertyChanged("ProductSearchText");
            }
            trackingId = details.TrackingId;

            CreateUserProductCommand = new RelayCommand(CreateUserProduct);

        }

        public ICommand SaveCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }

        public List<KeyValuePair<string, string>> SummaryData
        {
            get
            {
                return (summaryData);
            }
            set
            {
                summaryData = value;
            }
        }

        void Save()
        {
            if (RatePerAreaValue == 0.0m)
                return;

            //Messenger.Default.Send(new HidePopupMessage());
            Messenger.Default.Send(new HideOverlayMessage());
            if (view != null)
            {
                SelectMatchingScopeViewModel vm = new SelectMatchingScopeViewModel(clientEndpoint, dispatcher, this);
                var sd = new ScreenDescriptor("Landdb.Views.Production.Popups.Matching.SelectMatchingScopeView", "selectScope", vm);
                Messenger.Default.Send(new ShowOverlayMessage() { ScreenDescriptor = sd });
            }
        }

        void Cancel()
        {
            //Messenger.Default.Send(new HidePopupMessage());
            Messenger.Default.Send(new HideOverlayMessage());
        }

        public void SubmitMatching(MatchingScope scope, DateTime start, DateTime end)
        {
            RecommendationCommandProductEntry known = ToRecommendationCommandProductEntry();
            var cmd = new ResolveUnknownRecommendedProduct(view.Id, clientEndpoint.GenerateNewMetadata(), unknown, known, scope, start, end);
            clientEndpoint.SendOne(cmd);
        // I know this is lame....want to give the command time to propogate
            Thread.Sleep(2000);
            parent.RefreshFieldsAndProducts();

        }
        public void CancelScope()
        {
            var sd = new ScreenDescriptor("Landdb.Views.Production.Popups.Matching.ResolveUnknownRecommendedProductView", "resolveRecommendedProduct", this);
            Messenger.Default.Send(new ShowPopupMessage() { ScreenDescriptor = sd });
        }

        public ObservableCollection<PestListItemDetails> Pests { get; private set; }
        public PestListItemDetails Pest { get { return pest; } set { pest = value; RaisePropertyChanged("Pest"); } }
        public ObservableCollection<UnitPerAreaHelper> AllowedRateUnits { get; private set; }
        public ObservableCollection<IUnit> AllowedTotalUnits { get; private set; }

        public string PestSearchText
        {
            get { return pestSearch; }
            set { pestSearch = value; }
        }
        public string ProductSearchText { get; set; }

        public List<ApplicationMethod> ApplicationMethods { get { return clientEndpoint.GetMasterlistService().GetApplicationMethodList().ToList(); } }

        public ApplicationMethod ApplicationMethod
        {
            get
            {
                return appMethod;
            }
            set
            {
                appMethod = value;
                RaisePropertyChanged("ApplicationMethod");
            }
        }

        public ApplicationTypes ApplyBy
        {
            get { return applyBy; }
            set
            {
                applyBy = value;
                RaisePropertyChanged("ApplyBy");
            }
        }

        public ObservableCollection<ProductListItemDetails> Products { get; private set; }

        public ProductListItemDetails SelectedProduct
        {
            get { return selectedProduct; }
            set
            {
                selectedProduct = value;
                RaisePropertyChanged("SelectedProduct");
                RaisePropertyChanged("DisplayName");
                RaisePropertyChanged("ApplicationMethod");
                UpdateAllowedUnits();

                if (value != null && value.Product != null && value.Product.ProductType == GlobalStrings.ProductType_Service && value.Product.StdPackageUnit == ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit)
                {
                    RatePerAreaValue = 1; // default to 1 for acre-based services because they're almost always "1 acre". Should probably extend this to other area units as well, for intl.
                }
                else
                {
                    RatePerAreaValue = 0;  // TODO: In the future, this should be the most recently used rate. We should also populate a list of the MR rates for this product.
                }

                if (AddProduct)
                {
                    RecalculateItem();

                }

                if (value == null || value.Product == null)
                {
                    IsCreateProductPopupOpen = true;
                }
            }
        }

        public string DisplayName
        {
            get
            {
                if (SelectedProduct == null) { return string.Empty; }
                return string.Format("{0} ({1})", SelectedProduct.Product.Name, SelectedProduct.Product.Manufacturer);
            }
        }

        public decimal RatePerAreaValue
        {
            get { return ratePerAreaValue; }
            set
            {
                ratePerAreaValue = value;
                RaisePropertyChanged("RatePerAreaValue");
                RaisePropertyChanged("RatePerAreaDisplay");
                if (applyBy == ApplicationTypes.RatePerArea)
                {
                    RecalculateItem();
                }
            }
        }

        public UnitPerAreaHelper SelectedRatePerAreaUnitHelper
        {
            get { return ratePerAreaUnitHelper; }
            set
            {
                ratePerAreaUnitHelper = value;
                RaisePropertyChanged("SelectedRatePerAreaUnitHelper");
                RaisePropertyChanged("RatePerAreaDisplay");
                if (applyBy == ApplicationTypes.RatePerArea)
                {
                    RecalculateItem();
                }
            }
        }

        public decimal RatePerTankValue
        {
            get { return ratePerTankValue; }
            set
            {
                ratePerTankValue = value;
                RaisePropertyChanged("RatePerTankValue");
                RaisePropertyChanged("RatePerTankDisplay");
                if (applyBy == ApplicationTypes.RatePerTank)
                {
                    RecalculateItem();
                }
            }
        }

        public IUnit SelectedRatePerTankUnit
        {
            get { return ratePerTankUnit; }
            set
            {
                ratePerTankUnit = value;
                RaisePropertyChanged("SelectedRatePerTankUnit");
                RaisePropertyChanged("RatePerTankDisplay");
                if (applyBy == ApplicationTypes.RatePerTank)
                {
                    RecalculateItem();
                }
            }
        }

        public string RatePerAreaDisplay
        {
            get
            {
                if (ratePerAreaUnitHelper == null) { return string.Empty; }
                var rateString = string.Format("{0} / {1}", ratePerAreaUnitHelper.Unit.GetMeasure((double)ratePerAreaValue, SelectedProduct.Product.Density).ToString(), ratePerAreaUnitHelper.AreaUnit.FullDisplay);
                return rateString;
            }
        }

        public string RatePerTankDisplay
        {
            get
            {
                if (ratePerTankUnit == null) { return string.Empty; }
                var rateString = ratePerTankUnit.GetMeasure((double)ratePerTankValue, SelectedProduct.Product.Density).ToString();
                return rateString;
            }
        }

        public decimal TotalProductValue
        {
            get { return totalProductValue; }
            set
            {
                totalProductValue = value;
                RaisePropertyChanged("TotalProductValue");
                RaisePropertyChanged("TotalProduct");
                if (applyBy == ApplicationTypes.TotalProduct)
                {
                    RecalculateItem();
                }
            }
        }

        public IUnit TotalProductUnit
        {
            get { return totalProductUnit; }
            set
            {
                totalProductUnit = value;
                RaisePropertyChanged("TotalProductUnit");
                RaisePropertyChanged("TotalProduct");
                if (applyBy == ApplicationTypes.TotalProduct)
                {
                    RecalculateItem();
                }
            }
        }

        public Measure TotalProduct
        {
            get
            {
                if (totalProductUnit != null && !(TotalProductUnit is NullUnit) && SelectedProduct != null)
                {
                    return totalProductUnit.GetMeasure((double)totalProductValue, SelectedProduct.Product.Density);
                }
                else
                {
                    return null;
                }
            }
        }

        public decimal? ProductAreaPercent
        {
            get { return productAreaPercent; }
            set
            {

                if (value != null && value > 1.0m)
                {
                    productAreaPercent = (value / 100);
                }
                else
                {
                    productAreaPercent = value;
                }

                RaisePropertyChanged("ProductAreaPercent");
                RecalculateItem();
            }
        }

        // readonly property for displaying product area. Setter is only for conveniently raising the prop-changed event for the view
        public Measure ProductArea
        {
            get { return productArea; }
            private set
            {
                productArea = value;
                RaisePropertyChanged("ProductArea");
            }
        }

        public bool AddProduct { get; set; }
        public bool IsProductListVisible
        {
            get { return isProductListVisible; }
            set
            {
                isProductListVisible = value;
                RaisePropertyChanged("IsProductListVisible");
            }
        }

        public bool IsCreateProductPopupOpen
        {
            get { return isCreateProductPopupOpen; }
            set
            {
                isCreateProductPopupOpen = value;
                RaisePropertyChanged("IsCreateProductPopupOpen");
            }
        }

        void UpdateAllowedUnits()
        {
            AllowedRateUnits.Clear();
            AllowedTotalUnits.Clear();

            SelectedRatePerAreaUnitHelper = null;

            if (SelectedProduct == null) { return; }

            var psu = UnitFactory.GetPackageSafeUnit(SelectedProduct.Product.StdPackageUnit, SelectedProduct.Product.StdUnit, SelectedProduct.Product.StdFactor, SelectedProduct.Product.StdPackageUnit);
            var compatibleUnits = AgC.UnitConversion.CompatibilityFactory.GetCompatibleUnits(psu, SelectedProduct.Product.Density);


            foreach (var u in compatibleUnits)
            {
                AllowedTotalUnits.Add(u);
                var uh = new UnitPerAreaHelper(u, UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit));
                AllowedRateUnits.Add(uh);
                if (u == psu)
                {
                    switch (ApplyBy)
                    {
                        case ApplicationTypes.RatePerArea:
                        case ApplicationTypes.RatePerTank:
                        default:
                            SelectedRatePerAreaUnitHelper = uh;
                            SelectedRatePerTankUnit = u;
                            break;
                        case ApplicationTypes.TotalProduct:
                            TotalProductUnit = u;
                            break;
                    }

                }
            }

            // If we couldn't find it in the list while we were building it, pick the first one, if any.
            if (SelectedRatePerAreaUnitHelper == null && AllowedRateUnits.Any()) { SelectedRatePerAreaUnitHelper = AllowedRateUnits[0]; }

        }

        void fieldSelectionModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "TotalArea")
            {
                totalArea = view.TotalArea;

                RecalculateItem();
            }
        }

        internal bool ProductInfoChanged()
        {
            switch (view.ApplicationStrategy)
            {
                case ProductApplicationStrategy.ByRatePerArea:
                    return details.RateMeasure.Value != (double)ratePerAreaValue || details.RateMeasure.Unit != ratePerAreaUnitHelper.Unit;
                case ProductApplicationStrategy.ByTotalProduct:
                    return details.TotalProduct.Value != (double)totalProductValue || details.TotalProduct.Unit != totalProductUnit;
                case ProductApplicationStrategy.ByRatePerTank:
                    return details.RatePerTank.Value != (double)ratePerTankValue || details.RatePerTank.Unit != ratePerTankUnit;
                default:
                    return false;
            }
        }

        internal bool AssociatedInfoChanged()
        {
            return details.ApplicationMethod != ApplicationMethod || details.TargetPest.id != Pest.PestId;
        }

        internal void RecalculateItem()
        {
            switch (ApplyBy)
            {
                case ApplicationTypes.RatePerTank:
                    RecalculateByTank();
                    break;
                case ApplicationTypes.TotalProduct:
                    RecalculateByTotal();
                    break;
                case ApplicationTypes.RatePerArea:
                default:
                    RecalculateByArea();
                    break;
            }
        }

        void RecalculateByArea()
        {
            if (SelectedProduct == null || SelectedRatePerAreaUnitHelper == null)
            {
                TotalProductValue = 0;
                TotalProductUnit = new NullUnit();

                SelectedRatePerTankUnit = null;
                RatePerTankValue = 0m;
                return;
            }

            var rate = SelectedRatePerAreaUnitHelper.Unit.GetMeasure((double)RatePerAreaValue, SelectedProduct.Product.Density);

            var totalArea = (view != null) ? view.TotalArea : null;

            var tankInfo = (view != null) ? view.TankInformation : null;

            ProductArea = totalArea.Unit.GetMeasure(totalArea.Value * (double)productAreaPercent);
            var calculation = applicationCalculationService.RecalculateByArea(
                SelectedProduct.Product,
                null,
                rate,
                ProductArea,
                tankInfo == null ? 0m : tankInfo.TankCount,
                AllowedTotalUnits
            );

            TotalProductValue = calculation.TotalProductValue;
            TotalProductUnit = calculation.TotalProductUnit;
            RatePerTankValue = calculation.RatePerTankValue;

            if (calculation.RatePerTankUnit != null)
            {
                SelectedRatePerTankUnit = calculation.RatePerTankUnit;
            }

        }

        void RecalculateByTotal()
        {
            if (SelectedProduct == null || TotalProductUnit == null)
            {
                SelectedRatePerAreaUnitHelper = null;
                RatePerAreaValue = 0m;

                SelectedRatePerTankUnit = null;
                RatePerTankValue = 0m;
                return;
            }

            var total = TotalProductUnit.GetMeasure((double)TotalProductValue, SelectedProduct.Product.Density);

            var totalArea = (view != null) ? view.TotalArea : null;
            var tankInfo = (view != null) ? view.TankInformation : null;

            var productArea = totalArea.Unit.GetMeasure(totalArea.Value * (double)productAreaPercent);

            var calculation = applicationCalculationService.RecalculateByTotal(
                SelectedProduct.Product,
                total,
                productArea,
                tankInfo == null ? 0m : tankInfo.TankCount,
                AllowedTotalUnits,
                null
            );

            ProductArea = calculation.ProductArea;
            RatePerAreaValue = calculation.RatePerAreaValue;
            SelectedRatePerAreaUnitHelper = (from r in AllowedRateUnits where r.Unit.Name == calculation.PackageSafeUnit.Name select r).FirstOrDefault();
            RatePerTankValue = calculation.RatePerTankValue;

            if (calculation.RatePerTankUnit != null)
            {
                SelectedRatePerTankUnit = calculation.RatePerTankUnit;
            }

        }

        void RecalculateByTank()
        {
            if (SelectedProduct == null || SelectedRatePerTankUnit == null)
            {
                SelectedRatePerAreaUnitHelper = null;
                RatePerAreaValue = 0m;

                TotalProductValue = 0;
                TotalProductUnit = new NullUnit();
                return;
            }

            var ratePerTank = SelectedRatePerTankUnit.GetMeasure((double)RatePerTankValue, SelectedProduct.Product.Density);
            var totalArea = (view != null) ? view.TotalArea : null;
            var tankInfo = (view != null) ? view.TankInformation : null;

            var productArea = totalArea.Unit.GetMeasure(totalArea.Value * (double)productAreaPercent);

            var calculation = applicationCalculationService.RecalculateByTank(
                SelectedProduct.Product,
                null,
                ratePerTank,
                productArea,
                tankInfo.TankCount
            );

            ProductArea = calculation.ProductArea;
            TotalProductValue = calculation.TotalProductValue;
            TotalProductUnit = calculation.TotalProductUnit;
            RatePerAreaValue = calculation.RatePerAreaValue;

            SelectedRatePerAreaUnitHelper = (from r in AllowedRateUnits where r.Unit.Name == calculation.PackageSafeUnit.Name select r).FirstOrDefault();

        }

        void OnProductCreated(MiniProduct product)
        {
            //var pli = parent.AddTemporaryMiniProduct(product, new ProductId(ApplicationEnvironment.CurrentDataSourceId, product.Id), true);
            //SelectedProduct = pli;
        }


        internal RecommendationDetailsViewModel.ProductDetails ToProductDetails()
        {

            var prod =
            new RecommendationDetailsViewModel.ProductDetails()
            {
                ProductName = clientEndpoint.GetMasterlistService().GetProductDisplay(SelectedProduct.ProductId),
                RateMeasure = UnitFactory.GetUnitByName(SelectedRatePerAreaUnitHelper.Unit.Name).GetMeasure((double)RatePerAreaValue, SelectedProduct.Product.Density),
                AreaUnit = UnitFactory.GetUnitByName(totalArea.Unit.Name),
                TotalProduct = UnitFactory.GetUnitByName(TotalProduct.Unit.Name).GetMeasure(TotalProduct.Value, SelectedProduct.Product.Density),
                ID = SelectedProduct.ProductId,
                ProductAreaPercent = ProductAreaPercent,
                TrackingId = trackingId,
                TargetPest = Pest != null ? new MiniPest() { Name = Pest.CommonName, LatinList = Pest.LatinName, id = Pest.PestId } : new MiniPest(),
                ApplicationMethod = ApplicationMethod,
            };

            return prod;
        }

        internal RecommendationCommandProductEntry ToRecommendationCommandProductEntry()
        {
            switch (applyBy)
            {
                case ApplicationTypes.RatePerArea:
                default:
                    return ToByAreaRecommendationCommandProductEntry();
                case ApplicationTypes.TotalProduct:
                    return ToByTotalRecommendationCommandProductEntry();
                case ApplicationTypes.RatePerTank:
                    return ToByTankRecommendationCommandProductEntry();
            }
        }

        RecommendationProductByArea ToByAreaRecommendationCommandProductEntry()
        {
            if (SelectedProduct == null || ratePerAreaUnitHelper == null) { return null; }
            MiniPest pest = new MiniPest();
            if (Pest != null) { pest = new MiniPest() { id = Pest.PestId, Name = Pest.CommonName, LatinList = Pest.LatinName }; }
            return recommendationCommandService.ToByAreaRecommendationCommandProductEntry(
                SelectedProduct.Product,
                SelectedProduct.ProductId,
                trackingId,
                ratePerAreaValue,
                ratePerAreaUnitHelper.Unit.Name,
                ratePerAreaUnitHelper.AreaUnit.Name,
                ProductAreaPercent,
                (decimal)ProductArea.Value,
                ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit,
                SelectedProduct.Product.Density,
                ApplicationMethod,
                pest
            );
        }

        RecommendationProductByTotal ToByTotalRecommendationCommandProductEntry()
        {
            if (SelectedProduct == null || totalProductUnit == null || ratePerAreaUnitHelper == null) { return null; }
            MiniPest pest = new MiniPest();
            if (Pest != null) { pest = new MiniPest() { id = Pest.PestId, Name = Pest.CommonName, LatinList = Pest.LatinName }; }
            return recommendationCommandService.ToByTotalRecommendationCommandProductEntry(
                SelectedProduct.Product,
                SelectedProduct.ProductId,
                trackingId,
                totalProductValue,
                totalProductUnit.Name,
                ratePerAreaUnitHelper.AreaUnit.Name,
                ProductAreaPercent,
                (decimal)ProductArea.Value,
                ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit,
                SelectedProduct.Product.Density,
                ApplicationMethod,
                pest
            );
        }

        RecommendationProductByTank ToByTankRecommendationCommandProductEntry()
        {
            if (SelectedProduct == null || ratePerTankUnit == null || ratePerAreaUnitHelper == null) { return null; }
            MiniPest pest = new MiniPest();
            if (Pest != null) { pest = new MiniPest() { id = Pest.PestId, Name = Pest.CommonName, LatinList = Pest.LatinName }; }
            return recommendationCommandService.ToByTankRecommendationCommandProductEntry(
                SelectedProduct.Product,
                SelectedProduct.ProductId,
                trackingId,
                ratePerTankValue,
                ratePerTankUnit.Name,
                ratePerAreaUnitHelper.AreaUnit.Name,
                ProductAreaPercent,
                (decimal)ProductArea.Value,
                ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit,
                SelectedProduct.Product.Density,
                ApplicationMethod,
                pest
            );
        }

        internal string ToName()
        {
            if (SelectedProduct != null && SelectedProduct.Product != null)
            {
                return SelectedProduct.Product.Name;
            }
            else
            {
                return string.Empty;
            }
        }

        async void BuildProductList()
        {
            var watch = Stopwatch.StartNew();

            await Task.Run(() =>
            {
                var mpl = clientEndpoint.GetMasterlistService().GetProductList();
                var mur = clientEndpoint.GetView<ProductUsageView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear)).GetValue(new ProductUsageView());
                var ucp = clientEndpoint.GetView<UserCreatedProductList>(new DataSourceId(ApplicationEnvironment.CurrentDataSourceId)).GetValue(new UserCreatedProductList());

                Guid mlDsId = GlobalIdentifiers.GetMasterlistDataSourceId();
				var productGuids = mur.GetProductGuids();

                var q = from p in mpl
                        let cont = productGuids.Contains(p.Id)
                        select new ProductListItemDetails(p, mlDsId, cont ? Strings.UsedThisYear_Text : Strings.Unused_Text, cont ? (short)0 : (short)1);

                var ucpQ = from p in ucp.UserCreatedProducts
                           let cont = productGuids.Contains(p.Id)
                           select new ProductListItemDetails(p, ApplicationEnvironment.CurrentDataSourceId, cont ? Strings.UsedThisYear_Text : Strings.Unused_Text, cont ? (short)0 : (short)1);

                Products = new ObservableCollection<ProductListItemDetails>(q.Union(ucpQ).OrderBy(x => x.GroupSortOrder).ToList());
                RaisePropertyChanged("Products");
            });
            watch.Stop();
        }

        void PopulatePests()
        {
            var ml = clientEndpoint.GetMasterlistService();
            var q = from p in ml.GetPestList()
                    //where p.Language == "en"
                    orderby p.Name
                    select new PestListItemDetails(p, 0);
            Pests = new ObservableCollection<PestListItemDetails>(q);
            RaisePropertyChanged("Pests");

            if (details != null && details.TargetPest != null) { Pest = Pests.FirstOrDefault(x => x.CommonName == details.TargetPest.Name && x.LatinName == details.TargetPest.LatinList); }
        }

        void CreateUserProduct()
        {
            IsProductListVisible = false;
            IsCreateProductPopupOpen = false;
            ShowOverlayMessage msg = new ShowOverlayMessage() {
                ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Secondary.UserCreatedProduct.UserProductCreatorView", "createProduct",
                    new UserProductCreatorViewModel(clientEndpoint, ProductSearchText, OnProductCreated))
            };
            Messenger.Default.Send<ShowOverlayMessage>(msg);
            //ShowPopupMessage msg = new ShowPopupMessage() {
            //    ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Secondary.UserCreatedProduct.UserProductCreatorView", "createProduct",
            //        new UserProductCreatorViewModel(clientEndpoint, ProductSearchText, OnProductCreated))
            //};
            //Messenger.Default.Send<ShowPopupMessage>(msg);
        }

        public RelayCommand CreateUserProductCommand { get; private set; }
    
    }
}
