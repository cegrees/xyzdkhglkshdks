﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Person;
using Landdb.Domain.ReadModels.Recommendation;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Resources;
using Landdb.Views.Production.Popups.Matching;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace Landdb.ViewModel.Production.Popups.Matching
{
    public class ResolveUnknownAuthorizationViewModel : ViewModelBase
    {
        IClientEndpoint clientEndpoint;
        Dispatcher dispatcher;
        RecommendationView view;
        RecommendationDetailsViewModel parent;
        UnknownAuthorization unknown;

        DataSourceId currentDataSourceId;
        int currentCropYear;
        List<KeyValuePair<string, string>> summaryData = new List<KeyValuePair<string, string>>();

        public ResolveUnknownAuthorizationViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, RecommendationView view, UnknownAuthorization unknown, RecommendationDetailsViewModel parent)
        {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            this.view = view;
            this.parent = parent;
            this.unknown = unknown;

            this.currentDataSourceId = new DataSourceId(ApplicationEnvironment.CurrentDataSourceId);
            this.currentCropYear = ApplicationEnvironment.CurrentCropYear;

            UpdatePeople();

            AuthorizationDate = unknown.AuthorizationDate;
            SummaryData = JsonConvert.DeserializeObject<List<KeyValuePair<string, string>>>(unknown.Data);

            SaveCommand = new RelayCommand(Save);
            CancelCommand = new RelayCommand(Cancel);
        }

        public ICommand SaveCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }

        public PersonListItemViewModel Person { get; set; }
        public List<PersonListItemViewModel> People { get; set; }
        public DateTime AuthorizationDate { get; set; }
        public List<KeyValuePair<string, string>> SummaryData
        {
            get
            {
                return (summaryData);
            }
            set
            {
                summaryData = value;
            }
        }

        void UpdatePeople() {
            var personListView = clientEndpoint.GetView<PersonListView>(currentDataSourceId).GetValue(() => null);

            if (personListView != null) {
				var sortedFilteredProjectedPersons = from p in personListView.Persons
													 where p.IsActiveInCropYear(currentCropYear)
													 orderby p.Name
													 select new PersonListItemViewModel(p);


                dispatcher.BeginInvoke(new Action(() => {
					People = new List<PersonListItemViewModel>(sortedFilteredProjectedPersons);
                    if (view != null) 
                        Person = view.Authorization != null ? People.Where(x => x.Id == view.Authorization.PersonId).FirstOrDefault() : null;

                    RaisePropertyChanged("People");
                    RaisePropertyChanged("Person");
                }));
            }
        }

        void Save()
        {
            Messenger.Default.Send(new HidePopupMessage());
            if (view != null)
            {
                SelectMatchingScopeViewModel vm = new SelectMatchingScopeViewModel(clientEndpoint, dispatcher, this);
                var sd = new ScreenDescriptor("Landdb.Views.Production.Popups.Matching.SelectMatchingScopeView", "selectScope", vm);
                Messenger.Default.Send(new ShowOverlayMessage() { ScreenDescriptor = sd });
            }
        }

        void Cancel()
        {
            Messenger.Default.Send(new HidePopupMessage());
        }

        public void SubmitMatching(MatchingScope scope, DateTime start, DateTime end)
        {
            ApplicationAuthorization known = new ApplicationAuthorization(Person.Id, Person.Name, AuthorizationDate);
            var cmd = new ResolveUnknownRecommendationAuthorization(view.Id, clientEndpoint.GenerateNewMetadata(), unknown, known, scope, start, end);
            clientEndpoint.SendOne(cmd);

            // I know this is lame....want to give the command time to propogate
            Thread.Sleep(2000);
            parent.RefreshAuthorization();
   
        }
        public void CancelScope()
        {
            var sd = new ScreenDescriptor("Landdb.Views.Production.Popups.Matching.ResolveUnknownAuthorizationView", "resolveAuthorizer", this);
            Messenger.Default.Send(new ShowPopupMessage() { ScreenDescriptor = sd });
        }

    }
}


