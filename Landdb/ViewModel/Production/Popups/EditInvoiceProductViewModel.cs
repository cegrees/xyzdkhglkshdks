﻿using AgC.UnitConversion;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Invoice;
using Landdb.Domain.ReadModels.Product;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Shared;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Landdb.Resources;
using Landdb.ViewModel.Production.Invoices;

namespace Landdb.ViewModel.Production.Popups {
	public class EditInvoiceProductViewModel : ValidationViewModelBase {

		readonly IClientEndpoint clientEndpoint;

		readonly string costCurrency = "USD"; // TODO: Make this a Currency class when we have a good one built.

		InvoiceView view;
		InvoiceProduct invProd;
		ActualInvoiceDetailsViewModel parent;

		public EditInvoiceProductViewModel(IClientEndpoint clientEndpoint, InvoiceView view, InvoiceProduct invProd, ActualInvoiceDetailsViewModel parent) {
			this.clientEndpoint = clientEndpoint;
			this.view = view;
			this.invProd = invProd;
			this.parent = parent;
            ProductSearchText = string.Empty;
            RaisePropertyChanged(() => ProductSearchText);
            UpdateProjection();

			AllowDelete = invProd != null && view.Products.Count > 1;

			CreateUserProductCommand = new RelayCommand(CreateUserProduct);

			UpdateCommand = new RelayCommand(this.Update);
			CancelCommand = new RelayCommand(this.Cancel);
			DeleteCommand = new RelayCommand(this.Delete);

			ValidateViewModel();
		}

		public ICommand UpdateCommand { get; private set; }
		public ICommand CancelCommand { get; private set; }
		public ICommand DeleteCommand { get; private set; }

		public ICommand CreateUserProductCommand { get; private set; }

		public Guid TrackingId { get; set; }

		public ObservableCollection<ProductListItemDetails> Products { get; set; }
		public ObservableCollection<IUnit> AllowedUnits { get; private set; }

		public bool AllowEdit { get; private set; }
		public bool AllowDelete { get; private set; }

		public string ProductSearchText { get; set; }

		private ProductListItemDetails selectedProduct;
		[CustomValidation(typeof(EditInvoiceProductViewModel), nameof(ValidateSelectedProduct))]
		public ProductListItemDetails SelectedProduct {
			get { return selectedProduct; }
			set {
				selectedProduct = value;
				ValidateAndRaisePropertyChanged(() => SelectedProduct);
				UpdateAllowedUnits();
			}
		}

		private IUnit costUnit;
		public IUnit CostUnit {
			get { return costUnit; }
			set {
				costUnit = value;
				RaisePropertyChanged(() => CostUnit);
			}
		}

		public string DisplayName {
			get {
				if (SelectedProduct == null) {
					return string.Empty;
				} else {
					return string.Format("{0} ({1})", SelectedProduct.Product.Name, SelectedProduct.Product.Manufacturer);
				}
			}
		}

		private decimal totalProductValue;
		public decimal TotalProductValue {
			get { return totalProductValue; }
			set {
				totalProductValue = value;
				ValidateAndRaisePropertyChanged(() => TotalProductValue);
			}
		}

		private IUnit totalProduceUnit;
		[CustomValidation(typeof(EditInvoiceProductViewModel), nameof(ValidateProductUnit))]
		public IUnit TotalProductUnit {
			get { return totalProduceUnit; }
			set {
				totalProduceUnit = value;
				ValidateAndRaisePropertyChanged(() => TotalProductUnit);
			}
		}

		private decimal totalCost;
		public decimal TotalCost {
			get { return totalCost; }
			set {
				totalCost = value;
				RaisePropertyChanged(() => TotalCost);
			}
		}

		private decimal productAreaPercent;
		public decimal ProductAreaPercent {
			get { return productAreaPercent; }
			set {
				productAreaPercent = value;
				RaisePropertyChanged(() => ProductAreaPercent);
			}
		}

		// readonly property for displaying product area. Setter is only for conveniently raising the prop-changed event for the view
		private Measure productArea;
		public Measure ProductArea {
			get { return productArea; }
			private set {
				productArea = value;
				RaisePropertyChanged(() => ProductArea);
			}
		}

		private bool isCreateProductPopupOpen;
		public bool IsCreateProductPopupOpen {
			get { return isCreateProductPopupOpen; }
			set {
				isCreateProductPopupOpen = value;
				RaisePropertyChanged(() => IsCreateProductPopupOpen);
			}
		}

		private bool isProductListVisible;
		public bool IsProductListVisible {
			get { return isProductListVisible; }
			set {
				isProductListVisible = value;
				RaisePropertyChanged(() => IsProductListVisible);
			}
		}

		public bool HasChanges {
			get {
				return TotalProductValue != invProd.TotalProductValue
					|| TotalCost != invProd.TotalCost
					|| TotalProductUnit.Name != invProd.TotalProductUnit
					|| CostUnit.Name != invProd.TotalCostCurrency;
			}
		}

		void UpdateAllowedUnits() {
			AllowedUnits = new ObservableCollection<IUnit>();
			AllowedUnits.Clear();
			TotalProductUnit = null;

			if (SelectedProduct == null) { return; }

			var psu = UnitFactory.GetPackageSafeUnit(SelectedProduct.Product.StdPackageUnit, SelectedProduct.Product.StdUnit, SelectedProduct.Product.StdFactor, SelectedProduct.Product.StdPackageUnit);
			var compatibleUnits = AgC.UnitConversion.CompatibilityFactory.GetCompatibleUnits(psu, SelectedProduct.Product.Density);

			foreach (var u in compatibleUnits) {
				AllowedUnits.Add(u);
				if (u == psu) {
					TotalProductUnit = u;
				}
			}

			RaisePropertyChanged(() => AllowedUnits);

			// If we couldn't find it in the list while we were building it, pick the first one, if any.
			if (TotalProductUnit == null && AllowedUnits.Any()) { TotalProductUnit = AllowedUnits[0]; }

			// TODO: Make CostUnit configurable
			CostUnit = UnitFactory.GetPackageSafeUnit(SelectedProduct.Product.StdPackageUnit, SelectedProduct.Product.StdUnit, SelectedProduct.Product.StdFactor, SelectedProduct.Product.StdPackageUnit);

			TotalProductUnit = TotalProductUnit == null ? psu : AllowedUnits.Where(x => x.Name == TotalProductUnit.Name).FirstOrDefault();

            //Added this so that on edit the Unit will be the same as when it was created.
            TotalProductUnit = invProd != null ? AllowedUnits.FirstOrDefault(x => x.Name == invProd.TotalProductUnit) : TotalProductUnit;
			RaisePropertyChanged(() => TotalProductUnit);
		}

		void UpdateProjection() {
			if (invProd != null) {
				TotalCost = invProd.TotalCost;
				TrackingId = invProd.TrackingId;
				TotalProductValue = invProd.TotalProductValue;

				var mpl = clientEndpoint.GetMasterlistService().GetProductList();
				var mur = clientEndpoint.GetView<ProductUsageView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear)).GetValue(new ProductUsageView());
				var ucp = clientEndpoint.GetView<UserCreatedProductList>(new DataSourceId(ApplicationEnvironment.CurrentDataSourceId)).GetValue(new UserCreatedProductList());
				var cont = mur.GetProductGuids().Contains(invProd.ProductId.Id); //productGuids.Contains(invProd.Id)

				SelectedProduct = new ProductListItemDetails(clientEndpoint.GetMasterlistService().GetProduct(invProd.ProductId), invProd.ProductId.DataSourceId, cont ? Strings.UsedThisYear_Text : Strings.Unused_Text, cont ? (short)0 : (short)1);
				RaisePropertyChanged(() => SelectedProduct);

				UpdateAllowedUnits();

				AllowEdit = false;
			} else {
				AllowEdit = true;
				TrackingId = Guid.NewGuid();
				Task.Run(() => { BuildProductList(); });
			}
		}

		internal string ToName() {
			return SelectedProduct.Product.Name;
		}

		void Update() {
			if (invProd != null && HasChanges) {
				UpdateInvoiceLineItem invoiceLineItem = new UpdateInvoiceLineItem(view.Id, clientEndpoint.GenerateNewMetadata(), TrackingId, TotalProductValue, TotalProductUnit.Name, TotalCost, costCurrency);
				clientEndpoint.SendOne(invoiceLineItem);

				//CHANGE SELECTED PRODUCT ON DETAILS VIEW MODEL
				var prodDetail = new InvoiceDetailsViewModel.ProductDetails() {
					ProductName = SelectedProduct.Product.Name,
					TotalCost = TotalCost,
                    TrackingId = TrackingId,
					TotalProduct = TotalProductUnit.GetMeasure((double)TotalProductValue),
					ID = SelectedProduct.ProductId
				};
				parent.UpdateProductDetails(prodDetail);

            }
            else if (invProd == null)
            {
                var invoiceProductEntry = ToInvoiceProductEntry();
                if (invoiceProductEntry == null) { return; } // If we don't resolve to an actual IPE, then don't allow them to continue.

                AddProductToInvoice invoiceCommand = new AddProductToInvoice(view.Id, clientEndpoint.GenerateNewMetadata(), ToInvoiceProductEntry());
                clientEndpoint.SendOne(invoiceCommand);

				//ADD PRODUCT TO DETAILS VIEW MODEL
				var prodDetail = new InvoiceDetailsViewModel.ProductDetails() {
					ProductName = SelectedProduct.Product.Name,
                    TrackingId = invoiceCommand.ProductEntry.TrackingId,
					TotalCost = TotalCost,
					TotalProduct = TotalProductUnit.GetMeasure((double)TotalProductValue),
					ID = SelectedProduct.ProductId
				};

				parent.UpdateProductList(prodDetail);
			}

			//Messenger.Default.Send(new HidePopupMessage());
			Messenger.Default.Send(new HideOverlayMessage());
		}

		void Cancel() {
			//Messenger.Default.Send(new HidePopupMessage());
			Messenger.Default.Send(new HideOverlayMessage());
		}

		void Delete() {
			if (invProd != null) {
				RemoveProductFromInvoice invoiceCommand = new RemoveProductFromInvoice(view.Id, clientEndpoint.GenerateNewMetadata(), TrackingId);
				clientEndpoint.SendOne(invoiceCommand);

				//REMOVE PRODUCT FROM DETAILS VIEW MODEL
				parent.RemoveProductFromList(TrackingId);
			}

			//Messenger.Default.Send(new HidePopupMessage());
			Messenger.Default.Send(new HideOverlayMessage());
		}

		async void BuildProductList() {
			var watch = System.Diagnostics.Stopwatch.StartNew();

			await Task.Run(() => {
				var mpl = clientEndpoint.GetMasterlistService().GetProductList();
				var mur = clientEndpoint.GetView<ProductUsageView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear)).GetValue(new ProductUsageView());
				var ucp = clientEndpoint.GetView<UserCreatedProductList>(new DataSourceId(ApplicationEnvironment.CurrentDataSourceId)).GetValue(new UserCreatedProductList());

				Guid mlDsId = GlobalIdentifiers.GetMasterlistDataSourceId();
				var productGuids = mur.GetProductGuids();

				var q = from p in mpl
						let cont = productGuids.Contains(p.Id)
						select new ProductListItemDetails(p, mlDsId, cont ? Strings.UsedThisYear_Text : Strings.Unused_Text, cont ? (short)0 : (short)1);

				var ucpQ = from p in ucp.UserCreatedProducts
						   let cont = productGuids.Contains(p.Id)
						   select new ProductListItemDetails(p, ApplicationEnvironment.CurrentDataSourceId, cont ? Strings.UsedThisYear_Text : Strings.Unused_Text, cont ? (short)0 : (short)1);

				Products = new ObservableCollection<ProductListItemDetails>(q.Union(ucpQ).OrderBy(x => x.GroupSortOrder).ToList());

				RaisePropertyChanged(() => Products);
			});

			watch.Stop();
		}

		void OnProductCreated(MiniProduct product) {
			var pli = AddTemporaryMiniProduct(product, new ProductId(ApplicationEnvironment.CurrentDataSourceId, product.Id), true);
			SelectedProduct = pli;
		}

		void CreateUserProduct() {
			IsProductListVisible = false;
			IsCreateProductPopupOpen = false;

			var vm = new Landdb.ViewModel.Secondary.UserCreatedProduct.UserProductCreatorViewModel(clientEndpoint, ProductSearchText, OnProductCreated);
			Messenger.Default.Send<ShowOverlayMessage>(new ShowOverlayMessage() {
				ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Secondary.UserCreatedProduct.UserProductCreatorView", "createProduct", vm)
			});

			//ShowPopupMessage msg = new ShowPopupMessage() {
			//    ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Secondary.UserCreatedProduct.UserProductCreatorView", "createProduct",
			//        new UserProductCreatorViewModel(endpoint, ProductSearchText, OnProductCreated))
			//};
			//Messenger.Default.Send<ShowPopupMessage>(msg);
		}

		internal ProductListItemDetails AddTemporaryMiniProduct(MiniProduct product, ProductId productId, bool isUsed) {
			var p = new ProductListItemDetails(product, productId.DataSourceId, isUsed ? Strings.UsedThisYear_Text : Strings.Unused_Text, isUsed ? (short)0 : (short)1);
			Products.Add(p);
			return p;
		}

		internal InvoiceProductEntry ToInvoiceProductEntry() {
            if (SelectedProduct == null || TotalProductUnit == null || SelectedProduct.Product == null) { return null; } // Prevents null-refs, and indicates invalidity

			// TODO: Handle currency deviation
			return new InvoiceProductEntry(SelectedProduct.ProductId, TrackingId, TotalProductValue, TotalProductUnit.Name, TotalCost, costCurrency, SelectedProduct.Product.Density);
		}

		#region Custom Validation
		public static ValidationResult ValidateSelectedProduct(ProductListItemDetails product, ValidationContext context) {
			ValidationResult result = null;

			if (product == null) {
				result = new ValidationResult(Strings.ProductIsRequired_Text);
			}

			return result;
		}

		public static ValidationResult ValidateProductUnit(IUnit selectedUnit, ValidationContext context) {
			ValidationResult result = null;

			if (selectedUnit == null) {
				result = new ValidationResult(Strings.UnitIsRequired_Text);
			}

			return result;
		}
		#endregion
	}
}