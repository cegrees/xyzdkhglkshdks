﻿using AgC.UnitConversion;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Contract;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Domain.ReadModels.Product;
using Landdb.Domain.ReadModels.ProductSetting;
using Landdb.Domain.ReadModels.WorkOrder;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ValueObjects;
using Landdb.ViewModel.Secondary.Application;
using Landdb.ViewModel.Secondary.PopUp;
using Landdb.ViewModel.Shared;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Resources;
using Landdb.ViewModel.Production.Applications;

namespace Landdb.ViewModel.Production.Popups {
	public class EditAppliedProductViewModel : ValidationViewModelBase {

		readonly IClientEndpoint endpoint;
		readonly Dispatcher dispatcher;

		Guid trackingId;
		ProductListItemDetails selectedProduct;

		// Rate per area values
		decimal ratePerAreaValue;
		UnitPerAreaHelper ratePerAreaUnitHelper;

		// Total product values
		decimal totalProductValue;
		IUnit totalProductUnit;

		// Rate per tank values
		decimal ratePerTankValue;
		IUnit ratePerTankUnit;

		decimal growerSplitPercent;
		decimal shareOwnerSplitPercent;
		decimal percentFromGrowerInventory = 1.0m;
		decimal growerSharePercent;
		Measure totalProductFromGrowerInventory;

		decimal costValuePerUnit; // TODO: Make this a Currency class when we have a good one built.
		IUnit costUnit;
		decimal totalCost = 0; // TODO: Make this a Currency class when we have a good one built.

		decimal productAreaPercent = 1m;
		Measure productArea;
		InventoryListView inventoryListView;
		PriceItem averagePriceItem;
		PriceItem selectedPrice;
		InventoryListItem currentInventoryItem;
		ApplicationMethod appMethod;
		RentContractDetailsView contractDetails;
		IApplicationCalculationService applicationCalculationService;
		IApplicationCommandService applicationCommandService;

		bool isProductListVisible = false;
		bool isCreateProductPopupOpen = false;

		Measure totalArea;

		ApplicationDetailsViewModel parent;
		ApplicationProductDetails details;
		ApplicationView appView;
		WorkOrderView woView;
		ApplicationTypes applyBy;
		string selectedPriceText;

		public EditAppliedProductViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, ApplicationView view, ApplicationDetailsViewModel parent, ApplicationProductDetails details) {
			this.endpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			this.parent = parent;
			this.appView = view;

            AssociatedProducts = new ObservableCollection<AssociatedProductItem>();
			AllowedRateUnits = new ObservableCollection<UnitPerAreaHelper>();
			AllowedTotalUnits = new ObservableCollection<IUnit>();
			PriceItems = new ObservableCollection<PriceItem>();
			Products = new ObservableCollection<ProductListItemDetails>();

			applicationCalculationService = new ApplicationCalculationService();
			applicationCommandService = new ApplicationCommandService();
			inventoryListView = clientEndpoint.GetView<InventoryListView>(new CropYearId(view.Id.DataSourceId, view.CropYear)).GetValue(new InventoryListView());

			AddProduct = details == null ? true : false;

            Task popPest = Task.Factory.StartNew(PopulatePests);
            Task.Run(() => { BuildProductList(); });

            Contract = parent.Contracts.Values.FirstOrDefault();

			if (AddProduct) {
				ApplyBy = (ApplicationTypes)view.ProductStrategy;
				ProductAreaPercent = 1.0m;
				trackingId = Guid.NewGuid();
				totalArea = view.TotalArea;
				ProductArea = totalArea;
				AllowEdit = true;
			} else {
				var masterlistProd = clientEndpoint.GetMasterlistService().GetProduct(details.ID);

				if (masterlistProd == null) {
					masterlistProd = new MiniProduct() {
						Id = details.ID.Id,
						Name = Strings.UnknownProduct_Text,
						Density = null,
						StdUnit = details.TotalProduct.Unit.Name,
						StdPackageUnit = details.TotalProduct.Unit.Name,
					};
				}

				Density = masterlistProd.Density;

				this.details = details;
				ApplyBy = (ApplicationTypes)view.ProductStrategy;
				var displayRate = DisplayMeasureConverter.ConvertToRateDisplayMeasure(details.ID, (decimal)details.RateMeasure.Value, details.RateMeasure.Unit.Name, Density, clientEndpoint, ApplicationEnvironment.CurrentDataSourceId);
				var displayTotal = DisplayMeasureConverter.ConvertToTotalDisplayMeasure(details.ID, (decimal)details.TotalProduct.Value, details.TotalProduct.Unit.Name, Density, clientEndpoint, ApplicationEnvironment.CurrentDataSourceId);
				var displayRatePerTank = DisplayMeasureConverter.ConvertToRatePerTankDisplayMeasure(details.ID, (decimal)details.RatePerTank.Value, details.RatePerTank.Unit.Name, Density, clientEndpoint, ApplicationEnvironment.CurrentDataSourceId);
				AllowEdit = false;
				totalArea = view.TotalArea;
                var itemNow = new ProductListItemDetails(masterlistProd, details.ID.DataSourceId, Strings.UsedThisYear_Text, (short)0);
                SelectedProduct = itemNow;
				RatePerTankValue = details.RatePerTank != null ? (decimal)displayRatePerTank.Value : 0m;
				RatePerAreaValue = (decimal)displayRate.Value;
				SelectedRatePerAreaUnitHelper = new UnitPerAreaHelper(displayRate.Unit, view.TotalArea.Unit);
				var ratePerTankUnit = details.RatePerTank != null ? displayRatePerTank.Unit.Name : SelectedProduct.Product.StdPackageUnit;
				var ratePerTankSafeUnit = UnitFactory.GetPackageSafeUnit(ratePerTankUnit, SelectedProduct.Product.StdUnit, SelectedProduct.Product.StdFactor, SelectedProduct.Product.StdPackageUnit);
				SelectedRatePerTankUnit = ratePerTankSafeUnit;
				ProductAreaPercent = details.ProductAreaPercent;
				ProductArea = totalArea;
				ApplicationMethod = details.ApplicationMethod;
				GrowerSharePercent = details.GrowerShare;
				FromGrowerInventory = details.FromGrowerInventory;
				TotalProductUnit = displayTotal.Unit;
				TotalProductValue = (decimal)displayTotal.Value;
				PercentFromGrowerInventory = details.FromGrowerInventory;

				if (details.ProductAreaPercent == 0) {
					productAreaPercent = 1.0m;
				} else {
					ProductAreaPercent = details.ProductAreaPercent;
				}

				trackingId = details.TrackingId;
				currentInventoryItem = inventoryListView.Products[SelectedProduct.ProductId];
				UpdatePriceItems();

                AssociatedProducts = new ObservableCollection<AssociatedProductItem>();
                foreach (var associate in details.AssociatedProducts)
                {
                    var newAssociate = new AssociatedProductItem(endpoint, masterlistProd, RemoveAssociatedProduct);

                    newAssociate.CustomUnit = associate.CustomUnit;
                    newAssociate.CustomRateValue = associate.CustomRateValue;
                    newAssociate.HasCost = associate.HasCost;
                    newAssociate.CostPerUnitValue = associate.HasCost ? associate.CostPerUnitValue : 0m;
                    newAssociate.CostPerUnitUnit = associate.CostPerUnitUnit;
                    newAssociate.ProductID = associate.ProductID;
                    newAssociate.ProductName = associate.ProductName;
                    newAssociate.RatePerAreaUnit = associate.RatePerAreaUnit;
                    newAssociate.RatePerAreaValue = associate.RatePerAreaValue;
                    newAssociate.TotalProductUnit = associate.TotalProductUnit;
                    newAssociate.TotalProductValue = associate.TotalProductValue;
                    newAssociate.TrackingID = associate.TrackingID;

                    switch (associate.CustomRateType.ToString())
                    {
                        case "ByBag":
                            newAssociate.CustomRateType = AssociatedProductRateType.ByBag;
                            break;
                        case "ByCWT":
                            newAssociate.CustomRateType = AssociatedProductRateType.ByCWT;
                            break;
                        case "ByRow":
                            newAssociate.CustomRateType = AssociatedProductRateType.ByRow;
                            break;
                        case "BySeed":
                            newAssociate.CustomRateType = AssociatedProductRateType.BySeed;
                            break;
                        default:
                            newAssociate.CustomRateType = AssociatedProductRateType.BySeed;
                            break;
                    }

                    AssociatedProducts.Add(newAssociate);
                }

                RaisePropertyChanged(() => AssociatedProducts);
                RaisePropertyChanged(() => HasAssociatedProducts);

                popPest.Wait();
                SelectedPest = details.TargetPest != null ? PestList.FirstOrDefault(x => x.CommonName == details.TargetPest.Name && x.LatinName == details.TargetPest.LatinList) : null;
                RaisePropertyChanged(() => SelectedPest);
            }

			AllowDelete = !AddProduct && view.Products.Count > 1;

			CreateUserProductCommand = new RelayCommand(CreateUserProduct);

			Cancel = new RelayCommand(CancelEdit);
			Update = new RelayCommand(UpdateProduct);
			Delete = new RelayCommand(DeleteProduct);
            AddAssociatedProduct = new RelayCommand(AssociateProductAddition);

            ValidateViewModel();
		}

		public ICommand Cancel { get; set; }
		public ICommand Update { get; set; }
		public ICommand Delete { get; set; }
        public ICommand AddAssociatedProduct { get; set; }

        public ObservableCollection<PestListItemDetails> PestList { get; private set; }
		public ObservableCollection<UnitPerAreaHelper> AllowedRateUnits { get; private set; }
		public ObservableCollection<IUnit> AllowedTotalUnits { get; private set; }
		public string ProductSearchText { get; set; }

		public bool AllowEdit { get; set; }
		public double? Density { get; set; }
		public bool AddProduct { get; set; }
		public bool AllowDelete { get; set; }
		public List<ApplicationMethod> ApplicationMethods { get { return endpoint.GetMasterlistService().GetApplicationMethodList().ToList(); } }

		private PestListItemDetails _selectedPest;
		[CustomValidation(typeof(EditAppliedProductViewModel), nameof(ValidateSelectedPest))]
		public PestListItemDetails SelectedPest {
			get { return _selectedPest; }
			set {
				_selectedPest = value;
				ValidateProperty(() => SelectedPest);
                RaisePropertyChanged(()=> SelectedPest);
			}
		}

		private string _pestSearchText = string.Empty;
		public string PestSearchText {
			get { return _pestSearchText; }
			set {
				_pestSearchText = value;
				ValidateProperty(() => SelectedPest);
			}
		}

		public ApplicationMethod ApplicationMethod {
			get { return appMethod; }
			set {
				appMethod = value;
				RaisePropertyChanged(() => ApplicationMethod);
			}
		}

		public ApplicationTypes ApplyBy {
			get { return applyBy; }
			set {
				applyBy = value;
				RaisePropertyChanged(() => ApplyBy);
			}
		}

        public bool HasAssociatedProducts
        {
            get
            {
                return AssociatedProducts != null && AssociatedProducts.Count > 0 ? true : false;
            }
        }


        public ObservableCollection<ProductListItemDetails> Products { get; private set; }
		public ObservableCollection<PriceItem> PriceItems { get; private set; }
		public PriceItem SelectedPriceItem { get { return selectedPrice; } set { selectedPrice = value; RaisePropertyChanged(() => SelectedPriceItem); } }
        public bool AllowSeedTreatment { get { return SelectedProduct != null && SelectedProduct.Product.ProductType == GlobalStrings.ProductType_Seed ? true : false; } }

        [CustomValidation(typeof(EditAppliedProductViewModel), nameof(ValidateSelectedProduct))]
		public ProductListItemDetails SelectedProduct {
			get { return selectedProduct; }
			set {
				selectedProduct = value;
				ValidateAndRaisePropertyChanged(() => SelectedProduct);
				RaisePropertyChanged(() => DisplayName);
				RaisePropertyChanged(() => ApplicationMethod);
				UpdateAllowedUnits();

				if (value != null && value.Product != null && value.Product.ProductType == GlobalStrings.ProductType_Service && value.Product.StdPackageUnit == ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit) {
					RatePerAreaValue = 1; // default to 1 for acre-based services because they're almost always "1 acre". Should probably extend this to other area units as well, for intl.
				} else {
					RatePerAreaValue = 0;  // TODO: In the future, this should be the most recently used rate. We should also populate a list of the MR rates for this product.
				}

                if (AddProduct && SelectedProduct != null)
                {
                    var invProd = inventoryListView.Products.FirstOrDefault(x => x.Key == SelectedProduct.ProductId);
                    currentInventoryItem = invProd.Value != null ? invProd.Value : null;
                    UpdateInventoryListItem();
                    ResetContractSpecificInfo();
                    UpdatePercentFromGrowerInventory();
                    UpdateGrowerShare();
                    UpdatePriceItems();
                    RecalculateItem();

                }
                else
                {
                    ResetContractSpecificInfo();
                }

				if (value == null || value.Product == null) {
					IsCreateProductPopupOpen = true;
				}

                RaisePropertyChanged(() => AllowSeedTreatment);
			}
		}

		public string DisplayName {
			get {
				if (SelectedProduct == null) {
					return string.Empty;
				} else {
					return string.Format("{0} ({1})", SelectedProduct.Product.Name, SelectedProduct.Product.Manufacturer);
				}
			}
		}

		public string SelectedPriceText {
			get { return selectedPriceText; }
			set {
                if (string.IsNullOrWhiteSpace(value)) { return; }

                if (selectedPriceText != value && (SelectedPriceItem == null || value != SelectedPriceItem.ToString()))
                {
                    selectedPriceText = value;
                    decimal price = 0m;
                    var seeThis = decimal.TryParse(SelectedPriceText, out price);

                    if (decimal.TryParse(selectedPriceText, out price))
                    {
                        var priceUnit = currentInventoryItem == null || currentInventoryItem.AveragePriceUnit == null ? selectedProduct.Product.StdPackageUnit : currentInventoryItem.AveragePriceUnit;
                        PriceItem priceItem = new PriceItem(price, priceUnit);
                        if (!PriceItems.Contains(priceItem)) { PriceItems.Add(priceItem); }
                        SelectedPriceItem = priceItem;
                        selectedPriceText = priceItem.ToString();
                    }
                    else
                    {
                        //INCASE OF PARSE FAILURE   
                        string parsedString = string.Empty;
                        for (int i = 0; i < selectedPriceText.Count(); i++)
                        {
                            if (Char.IsDigit(selectedPriceText[i]) || selectedPriceText[i] == '.')
                            {
                                parsedString += selectedPriceText[i];
                            }
                        }

                        if (decimal.TryParse(parsedString, out price))
                        {
                            var priceUnit = currentInventoryItem == null || currentInventoryItem.AveragePriceUnit == null ? selectedProduct.Product.StdPackageUnit : currentInventoryItem.AveragePriceUnit;
                            PriceItem priceItem = new PriceItem(price, priceUnit);
                            if (!PriceItems.Contains(priceItem)) { PriceItems.Add(priceItem); }
                            SelectedPriceItem = priceItem;
                            selectedPriceText = priceItem.ToString();
                        }
                    }
                }
                else
                {
                    selectedPriceText = SelectedPriceItem.ToString();
                    RaisePropertyChanged(() => SelectedPriceItem);
                    RaisePropertyChanged(() => SelectedPriceText);
                }
			}
		}

        //DEBUG: In case of an error, here's where I (Ethan) got the idea to do this: https://stackoverflow.com/questions/30848308/c-sharp-custom-attribute-with-support-for-string-resources-does-not-load-localiz
        //Addendum: //http://www.devcurry.com/2013/02/aspnet-mvc-using-resource-files-to.html
        [Range(typeof(decimal), "0", "1", ErrorMessageResourceName = "ValueMustBeBetweenZeroAndOneHundred_Text", ErrorMessageResourceType = typeof(Strings))] 
        public decimal GrowerSharePercent {
			get { return growerSharePercent; }
			set {
				growerSharePercent = value;
				ValidateAndRaisePropertyChanged(() => GrowerSharePercent);
			}
		}

		public decimal FromGrowerInventory { get; set; }

		[CustomValidation(typeof(EditAppliedProductViewModel), nameof(ValidateRate))]
		public decimal RatePerAreaValue {
			get { return ratePerAreaValue; }
			set {
				ratePerAreaValue = value;
				RaisePropertyChanged(() => RatePerAreaValue);
				RaisePropertyChanged(() => RatePerAreaDisplay);
				if (applyBy == ApplicationTypes.RatePerArea) {
					RecalculateItem();
				}
			}
		}

		[CustomValidation(typeof(EditAppliedProductViewModel), nameof(ValidateUnit))]
		public UnitPerAreaHelper SelectedRatePerAreaUnitHelper {
			get { return ratePerAreaUnitHelper; }
			set {
				ratePerAreaUnitHelper = value;
				RaisePropertyChanged(() => SelectedRatePerAreaUnitHelper);
				RaisePropertyChanged(() => RatePerAreaDisplay);
				if (applyBy == ApplicationTypes.RatePerArea) {
					RecalculateItem();
				}
			}
		}

		[CustomValidation(typeof(EditAppliedProductViewModel), nameof(ValidateRate))]
		public decimal RatePerTankValue {
			get { return ratePerTankValue; }
			set {
				ratePerTankValue = value;
				RaisePropertyChanged(() => RatePerTankValue);
				RaisePropertyChanged(() => RatePerTankDisplay);
				if (applyBy == ApplicationTypes.RatePerTank) {
					RecalculateItem();
				}
			}
		}

		[CustomValidation(typeof(EditAppliedProductViewModel), nameof(ValidateUnit))]
		public IUnit SelectedRatePerTankUnit {
			get { return ratePerTankUnit; }
			set {
				ratePerTankUnit = value;
				RaisePropertyChanged(() => SelectedRatePerTankUnit);
				RaisePropertyChanged(() => RatePerTankDisplay);
				if (applyBy == ApplicationTypes.RatePerTank) {
					RecalculateItem();
				}
			}
		}

		public PriceItem AveragePriceItem {
			get { return averagePriceItem; }
			set {
				averagePriceItem = value;
				RaisePropertyChanged(() => AveragePriceItem);
				RecalculateItem();
			}
		}

		public decimal CostValuePerUnit {
			get { return costValuePerUnit; }
			set {
				costValuePerUnit = value;
				RaisePropertyChanged(() => CostValuePerUnit);
				RecalculateItem();
			}
		}

		public IUnit CostUnit {
			get { return costUnit; }
			set {
				costUnit = value;
				RaisePropertyChanged(() => CostUnit);
			}
		}

		public string RatePerAreaDisplay {
			get {
				if (ratePerAreaUnitHelper == null) { return string.Empty; }
				var rateString = string.Format("{0} / {1}", ratePerAreaUnitHelper.Unit.GetMeasure((double)ratePerAreaValue, SelectedProduct.Product.Density).ToString(), ratePerAreaUnitHelper.AreaUnit.FullDisplay);
				return rateString;
			}
		}

		public string RatePerTankDisplay {
			get {
				if (ratePerTankUnit == null) { return string.Empty; }
				var rateString = ratePerTankUnit.GetMeasure((double)ratePerTankValue, SelectedProduct.Product.Density).ToString();
				return rateString;
			}
		}

		[CustomValidation(typeof(EditAppliedProductViewModel), nameof(ValidateRate))]
		public decimal TotalProductValue {
			get { return totalProductValue; }
			set {
				totalProductValue = value;
				RaisePropertyChanged(() => TotalProductValue);
				RaisePropertyChanged(() => TotalProduct);
				if (applyBy == ApplicationTypes.TotalProduct) {
					RecalculateItem();
				}
			}
		}

		[CustomValidation(typeof(EditAppliedProductViewModel), nameof(ValidateUnit))]
		public IUnit TotalProductUnit {
			get { return totalProductUnit; }
			set {
				totalProductUnit = value;
				RaisePropertyChanged(() => TotalProductUnit);
				RaisePropertyChanged(() => TotalProduct);
				if (applyBy == ApplicationTypes.TotalProduct) {
					RecalculateItem();
				}
			}
		}

		public Measure TotalProduct {
			get {
				if (totalProductUnit != null && !(TotalProductUnit is NullUnit) && SelectedProduct != null) {
					return totalProductUnit.GetMeasure((double)totalProductValue, SelectedProduct.Product.Density);
				} else {
					return null;
				}
			}
		}

		public decimal TotalCost {
			get { return totalCost; }
			set {
				totalCost = value;
				RaisePropertyChanged(() => TotalCost);
				RaisePropertyChanged(() => TotalGrowerCost);
			}
		}

		public decimal TotalGrowerCost {
			get {
				if (IsRentContractAssociated) {
					//return TotalCost * PercentFromGrowerInventory;
					return TotalCost * GrowerSplitPercent;
				} else {
					return TotalCost;
				}
			}
		}

		public decimal ProductAreaPercent {
			get { return productAreaPercent; }
			set {
				productAreaPercent = value;

				RaisePropertyChanged(() => ProductAreaPercent);
				RecalculateItem();
			}
		}

		public RentContractDetailsView Contract {
			get { return contractDetails; }
			set {
				contractDetails = value;

				ResetContractSpecificInfo();

				RaisePropertyChanged(() => Contract);
				RaisePropertyChanged(() => IsRentContractAssociated);
				RaisePropertyChanged(() => CostLabelText);
			}
		}

		public bool IsShareInfoVisible {
			get { return true; }
		}

		public bool IsRentContractAssociated {
			get { return contractDetails != null; }
		}

		public string CostLabelText {
			get { return IsRentContractAssociated ? Strings.GrowerCost_Text.ToUpper() : Strings.ProductCost_Text.ToUpper(); }
		}

		public decimal GrowerSplitPercent {
			get { return growerSplitPercent; }
			set {
				growerSplitPercent = value;
				ShareOwnerSplitPercent = 1.0m - growerSplitPercent;
				RaisePropertyChanged(() => GrowerSplitPercent);

				UpdatePercentFromGrowerInventory();
			}
		}

		public decimal ShareOwnerSplitPercent {
			get { return shareOwnerSplitPercent; }
			set {
				shareOwnerSplitPercent = value;
				RaisePropertyChanged(() => ShareOwnerSplitPercent);
			}
		}

        [Range(typeof(decimal), "0", "1", ErrorMessageResourceName = "ValueMustBeBetweenZeroAndOneHundred_Text", ErrorMessageResourceType=typeof(Strings))]
        public decimal PercentFromGrowerInventory {
			get { return percentFromGrowerInventory; }
			set {
				percentFromGrowerInventory = value;
				CalculateTotalProductFromGrowerInventory();

				ValidateAndRaisePropertyChanged(() => PercentFromGrowerInventory);
			}
		}

		private void CalculateTotalProductFromGrowerInventory() {
			if (TotalProductUnit != null && !(TotalProductUnit is NullUnit) && SelectedProduct != null) {
				var total = TotalProductValue * PercentFromGrowerInventory;
				TotalProductFromGrowerInventory = TotalProductUnit.GetMeasure((double)total, SelectedProduct.Product.Density);
			}
		}

		public Measure TotalProductFromGrowerInventory {
			get { return totalProductFromGrowerInventory; }
			set {
				totalProductFromGrowerInventory = value;
				RaisePropertyChanged(() => TotalProductFromGrowerInventory);
				RaisePropertyChanged(() => TotalGrowerCost);
			}
		}

		// readonly property for displaying product area. Setter is only for conveniently raising the prop-changed event for the view
		public Measure ProductArea {
			get { return productArea; }
			private set {
				productArea = value;
				RaisePropertyChanged(() => ProductArea);
				RaisePropertyChanged(() => ProductAreaDisplayText);
			}
		}

		public string ProductAreaDisplayText {
			get { return string.Format("{0:N2} {1}", ProductArea.Value, ProductArea.Unit.AbbreviatedDisplay); }
			set {
				double newArea = 0;

				if (double.TryParse(value, out newArea)) {
					// this will fire a recalc that will update the area
					ProductAreaPercent = totalArea.Value != 0 ? Convert.ToDecimal(newArea / totalArea.Value) : 1;
				}
			}
		}

		public bool IsProductListVisible {
			get { return isProductListVisible; }
			set {
				isProductListVisible = value;
				RaisePropertyChanged(() => IsProductListVisible);
			}
		}

		public bool IsCreateProductPopupOpen {
			get { return isCreateProductPopupOpen; }
			set {
				isCreateProductPopupOpen = value;
				RaisePropertyChanged(() => IsCreateProductPopupOpen);
			}
		}

        public ObservableCollection<AssociatedProductItem> AssociatedProducts { get; set; }

		public void ResetContractSpecificInfo() {
            if (contractDetails == null || SelectedProduct == null || SelectedProduct.Product == null)
            {
                PercentFromGrowerInventory = 1.0m;
                return;
            }

			if (SelectedProduct.Product.ProductType == GlobalStrings.ProductType_CropProtection) {
				GrowerSplitPercent = (decimal)contractDetails.IncludedShareInfo.GrowerCropProtectionShare;
			} else if (SelectedProduct.Product.ProductType == GlobalStrings.ProductType_Fertilizer) {
				GrowerSplitPercent = (decimal)contractDetails.IncludedShareInfo.GrowerFertilizerShare;
			} else if (SelectedProduct.Product.ProductType == GlobalStrings.ProductType_Seed) {
				GrowerSplitPercent = (decimal)contractDetails.IncludedShareInfo.GrowerSeedShare;
			} else if (SelectedProduct.Product.ProductType == GlobalStrings.ProductType_Service) {
				GrowerSplitPercent = (decimal)contractDetails.IncludedShareInfo.GrowerServiceShare;
			}

			UpdatePercentFromGrowerInventory();

			ShareOwnerSplitPercent = 1.0m - GrowerSplitPercent;
            RaisePropertyChanged(() => IsRentContractAssociated);
		}

		void UpdatePercentFromGrowerInventory() {
			if (contractDetails == null || SelectedProduct == null || SelectedProduct.Product == null) {
				PercentFromGrowerInventory = 1.0m;
				return;
			}

			if (SelectedProduct.Product.ProductType == GlobalStrings.ProductType_CropProtection) {
				PercentFromGrowerInventory = contractDetails.IncludedInventoryInfo.IsCropProtectionFromInventory ? 1.0m : GrowerSplitPercent;
			} else if (SelectedProduct.Product.ProductType == GlobalStrings.ProductType_Fertilizer) {
				PercentFromGrowerInventory = contractDetails.IncludedInventoryInfo.IsFertilizerFromInventory ? 1.0m : GrowerSplitPercent;
			} else if (SelectedProduct.Product.ProductType == GlobalStrings.ProductType_Seed) {
				PercentFromGrowerInventory = contractDetails.IncludedInventoryInfo.IsSeedFromInventory ? 1.0m : GrowerSplitPercent;
			} else if (SelectedProduct.Product.ProductType == GlobalStrings.ProductType_Service) {
				PercentFromGrowerInventory = contractDetails.IncludedInventoryInfo.IsServiceFromInventory ? 1.0m : GrowerSplitPercent;
			}
		}

		void UpdateGrowerShare() {
			if (contractDetails == null || SelectedProduct == null || SelectedProduct.Product == null) {
				GrowerSharePercent = 1.0m;
				return;
			}

			if (SelectedProduct.Product.ProductType == GlobalStrings.ProductType_CropProtection) {
				GrowerSharePercent = (decimal)contractDetails.IncludedShareInfo.GrowerCropProtectionShare;
			} else if (SelectedProduct.Product.ProductType == GlobalStrings.ProductType_Fertilizer) {
				GrowerSharePercent = (decimal)contractDetails.IncludedShareInfo.GrowerFertilizerShare;
			} else if (SelectedProduct.Product.ProductType == GlobalStrings.ProductType_Seed) {
				GrowerSharePercent = (decimal)contractDetails.IncludedShareInfo.GrowerSeedShare;
			} else if (SelectedProduct.Product.ProductType == GlobalStrings.ProductType_Service) {
				GrowerSharePercent = (decimal)contractDetails.IncludedShareInfo.GrowerServiceShare;
			}
		}

		void UpdateInventoryListItem() {
			if (selectedProduct == null || selectedProduct.Product == null) {
				currentInventoryItem = null;
				return;
			}

			currentInventoryItem = inventoryListView.Products.ContainsKey(selectedProduct.ProductId) ? inventoryListView.Products[selectedProduct.ProductId] : null;

			if (currentInventoryItem != null) {
				var priceItem = new PriceItem((decimal)currentInventoryItem.AveragePriceValue, currentInventoryItem.AveragePriceUnit, true);
				AveragePriceItem = priceItem;
			} else {
				var priceItem = new PriceItem(0m, selectedProduct.Product.StdPackageUnit, true);
				AveragePriceItem = priceItem;
			}
		}

		void UpdateAllowedUnits() {
			AllowedRateUnits.Clear();
			AllowedTotalUnits.Clear();

			SelectedRatePerAreaUnitHelper = null;

			if (SelectedProduct == null) { return; }

			var psu = UnitFactory.GetPackageSafeUnit(SelectedProduct.Product.StdPackageUnit, SelectedProduct.Product.StdUnit, SelectedProduct.Product.StdFactor, SelectedProduct.Product.StdPackageUnit);
			var compatibleUnits = AgC.UnitConversion.CompatibilityFactory.GetCompatibleUnits(psu, SelectedProduct.Product.Density);

            ///////////////////////////set default units//////////////////////////////////////////////////////////////////////
            var defaultUnit = endpoint.GetUserSettings().PreferredRateUnits.ContainsKey(SelectedProduct.ProductId.Id) ?
                (from u in compatibleUnits where u.Name == endpoint.GetUserSettings().PreferredRateUnits[SelectedProduct.ProductId.Id] select u).FirstOrDefault() ?? psu
                : psu;
            var defaultTotalUnit = endpoint.GetUserSettings().PreferredTotalUnits.ContainsKey(SelectedProduct.ProductId.Id) ?
                (from u in compatibleUnits where u.Name == endpoint.GetUserSettings().PreferredTotalUnits[SelectedProduct.ProductId.Id] select u).FirstOrDefault() ?? psu
                : psu;

            var defaultRatePerTankUnit = endpoint.GetUserSettings().PreferredRatePerTankUnits.ContainsKey(SelectedProduct.ProductId.Id) ?
                (from u in compatibleUnits where u.Name == endpoint.GetUserSettings().PreferredRatePerTankUnits[SelectedProduct.ProductId.Id] select u).FirstOrDefault() ?? psu
                : psu;

            var prodSettingsId = new ProductSettingId(ApplicationEnvironment.CurrentDataSourceId, SelectedProduct.ProductId.Id);
            var prodSettings = endpoint.GetView<ProductSettingsView>(prodSettingsId);
            if (prodSettings.HasValue) {
                var settings = prodSettings.Value;
                defaultUnit = string.IsNullOrEmpty(settings.RatePerAreaUnit) ? defaultUnit : UnitFactory.GetUnitByName(settings.RatePerAreaUnit);
                defaultTotalUnit = string.IsNullOrEmpty(settings.TotalUnit) ? defaultTotalUnit : UnitFactory.GetUnitByName(settings.TotalUnit);
                defaultRatePerTankUnit = string.IsNullOrEmpty(settings.RatePerTankUnit) ? defaultRatePerTankUnit : UnitFactory.GetUnitByName(settings.RatePerTankUnit);
            }
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            foreach (var u in compatibleUnits)
            {
                AllowedTotalUnits.Add(u);
                var uh = new UnitPerAreaHelper(u, UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit));
                AllowedRateUnits.Add(uh);
            }

            foreach (var u in compatibleUnits)
            {
                var uh = new UnitPerAreaHelper(u, UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit));
                switch (ApplyBy)
                {
                    case ApplicationTypes.RatePerArea:
                    default:
                        if (u == defaultUnit)
                        {
                            SelectedRatePerAreaUnitHelper = uh;
                        }
                        break;
                    case ApplicationTypes.RatePerTank:
                        if (u == defaultRatePerTankUnit)
                        {
                            SelectedRatePerTankUnit = u;
                        }
                        break;
                    case ApplicationTypes.TotalProduct:
                        if (u == defaultTotalUnit)
                        {
                            TotalProductUnit = u;
                        }
                        break;
                }
            }

            //foreach (var u in compatibleUnits) {
            //	AllowedTotalUnits.Add(u);
            //	var uh = new UnitPerAreaHelper(u, UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit));
            //	AllowedRateUnits.Add(uh);

            //	if (u == psu) {
            //		switch (ApplyBy) {
            //			case ApplicationTypes.RatePerArea:
            //			case ApplicationTypes.RatePerTank:
            //			default:
            //				SelectedRatePerAreaUnitHelper = uh;
            //				SelectedRatePerTankUnit = u;
            //				break;
            //			case ApplicationTypes.TotalProduct:
            //				TotalProductUnit = u;
            //				break;
            //		}
            //	}
            //}

            // If we couldn't find it in the list while we were building it, pick the first one, if any.
            if (SelectedRatePerAreaUnitHelper == null && AllowedRateUnits.Any()) { SelectedRatePerAreaUnitHelper = AllowedRateUnits[0]; }

			// TODO: Make CostUnit configurable
			CostUnit = UnitFactory.GetPackageSafeUnit(SelectedProduct.Product.StdPackageUnit, SelectedProduct.Product.StdUnit, SelectedProduct.Product.StdFactor, SelectedProduct.Product.StdPackageUnit);
		}

		void fieldSelectionModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e) {
			if (e.PropertyName == "TotalArea") {
				totalArea = appView.TotalArea;

				RecalculateItem();
			}
		}

		void DeleteProduct() {
			if (AllowDelete) {
				if (appView != null) {
					RemoveProductFromApplication productCommand = new RemoveProductFromApplication(appView.Id, endpoint.GenerateNewMetadata(), trackingId);
					endpoint.SendOne(productCommand);

					parent.Products.Remove(details);
				} else if (woView != null) {
					RemoveProductFromWorkOrder productCommand = new RemoveProductFromWorkOrder(woView.Id, endpoint.GenerateNewMetadata(), trackingId);
					endpoint.SendOne(productCommand);

					parent.Products.Remove(details);
				}

				Messenger.Default.Send<HideOverlayMessage>(new HideOverlayMessage());
			}
		}

        void AssociateProductAddition()
        {
            //TO DO :: CREATE A VIEW MODEL FOR ADDING SEED TREATMENTS TO AN EXISTING PRODUCT
            var ratePerArea = SelectedRatePerAreaUnitHelper.Unit.GetMeasure((double)RatePerAreaValue, SelectedProduct.Product.Density);
            var vm = new AssociatedSeedTreatmentViewModel(endpoint, ratePerArea, ProductArea, SelectedProduct.Product, AddSiblingProduct);
            Messenger.Default.Send<ShowPopupMessage>(new ShowPopupMessage()
            {
                ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Secondary.PopUp.SeedTreatmentProductAssociationCalculator", "calculator", vm)
            });
        }

        public void AddSiblingProduct(AssociatedSeedTreatmentViewModel siblingVM)
        {
            decimal customRateValue = 0m;
            IUnit customUnit;
            AssociatedProductRateType rateType;
            var tab = siblingVM.TabItem == null ? "PerSeedTab" : siblingVM.TabItem.Name;
            switch (tab)
            {
                case "PerSeedTab":
                    customRateValue = siblingVM.MGActivePerSeedValue;
                    var composite2 = new CompositeUnit(siblingVM.SelectedRatePerSeedUnit.AbbreviatedDisplay, 1, SelectedProduct.Product.StdUnit);
                    string text2 = composite2.AbbreviatedDisplay;
                    customUnit = siblingVM.SelectedRatePerSeedUnit;
                    rateType = AssociatedProductRateType.BySeed;
                    break;
                case "PerBagTab":
                    customRateValue = siblingVM.RatePerBagValue;
                    customUnit = siblingVM.SelectedRatePerBagUnit;
                    rateType = AssociatedProductRateType.ByBag;
                    break;
                case "PerCwtTab":
                    customRateValue = siblingVM.RatePerCWTValue;
                    customUnit = siblingVM.SelectedCWTUnit;
                    rateType = AssociatedProductRateType.ByCWT;
                    break;
                case "RowTab":
                    customRateValue = siblingVM.RatePer1000Value;
                    customUnit = siblingVM.SelectedRatePer1000FtRowHelper;
                    rateType = AssociatedProductRateType.ByRow;
                    break;
                default:
                    customRateValue = siblingVM.MGActivePerSeedValue;
                    customUnit = siblingVM.SelectedRatePerSeedUnit;
                    rateType = AssociatedProductRateType.BySeed;
                    break;
            }

            var associatedProduct = new AssociatedProductItem(endpoint, SelectedProduct.Product, RemoveAssociatedProduct)
            {
                RatePerAreaValue = siblingVM.RatePerAreaValue,
                RatePerAreaUnit = siblingVM.SelectedRatePerAreaUnitHelper.Unit,
                TotalProductValue = siblingVM.TotalProductValue,
                TotalProductUnit = siblingVM.SelectedTotalProductUnit,
                HasCost = false,
                ProductID = siblingVM.SelectedProduct.ProductId,
                TrackingID = Guid.NewGuid(),
                ProductName = siblingVM.SelectedProduct.Product.Name,
                CustomRateValue = customRateValue,
                CustomUnit = customUnit,
                CustomRateType = rateType
            };

            AssociatedProducts.Add(associatedProduct);

            RaisePropertyChanged(() => AssociatedProducts);
            RaisePropertyChanged(() => HasAssociatedProducts);

            if (!AddProduct)
            {
                var appProd = new ApplicationAssociatedProduct();
                appProd.ProductId = associatedProduct.ProductID;
                appProd.ProductName = associatedProduct.ProductName;
                appProd.ManufacturerName = endpoint.GetMasterlistService().GetProduct(associatedProduct.ProductID).Manufacturer;
                appProd.RatePerAreaValue = associatedProduct.RatePerAreaValue;
                appProd.RatePerAreaUnit = associatedProduct.RatePerAreaUnit.Name;
                appProd.TotalProductValue = associatedProduct.TotalProductValue;
                appProd.TotalProductUnit = associatedProduct.TotalProductUnit.Name;
                appProd.TrackingId = associatedProduct.TrackingID;
                appProd.CustomProductUnit = associatedProduct.CustomUnit.Name;
                appProd.CustomProductValue = associatedProduct.CustomRateValue;
                appProd.CustomRateType = associatedProduct.CustomRateType.ToString();
                appProd.HasCost = associatedProduct.HasCost;
                appProd.SpecificCostPerUnitValue = associatedProduct.HasCost && associatedProduct.SelectedPriceItem != null && associatedProduct.SelectedPriceItem.IsSystem == false ? associatedProduct.SelectedPriceItem.Price : 0m;
                appProd.SpecificCostPerUnitUnit = associatedProduct.HasCost && associatedProduct.SelectedPriceItem != null && associatedProduct.SelectedPriceItem.IsSystem == false ? associatedProduct.SelectedPriceItem.PriceUnit : string.Empty;

                AddAppliedAssociatedProduct command = new AddAppliedAssociatedProduct(appView.Id, endpoint.GenerateNewMetadata(), details.TrackingId, appProd);
                endpoint.SendOne(command);
            }
        }

        public void RemoveAssociatedProduct(Guid associatedProductTrackingID)
        {
            var item = AssociatedProducts.SingleOrDefault(x => x.TrackingID == associatedProductTrackingID);
            AssociatedProducts.Remove(item);
            RaisePropertyChanged(() => AssociatedProducts);
            
            if (appView != null)
            {
                RemoveAppliedAssociatedProduct remove = new RemoveAppliedAssociatedProduct(appView.Id, endpoint.GenerateNewMetadata(), details.TrackingId, item.TrackingID);
                endpoint.SendOne(remove);
            }

            if (!AssociatedProducts.Any())
            {
                RaisePropertyChanged(() => HasAssociatedProducts);
            }
        }

        internal bool ProductInfoChanged() {
			switch (appView.ProductStrategy) {
				case ProductApplicationStrategy.ByRatePerArea:
					return details.RateMeasure.Value != (double)ratePerAreaValue || details.RateMeasure.Unit != ratePerAreaUnitHelper.Unit;
				case ProductApplicationStrategy.ByTotalProduct:
					return details.TotalProduct.Value != (double)totalProductValue || details.TotalProduct.Unit != totalProductUnit;
				case ProductApplicationStrategy.ByRatePerTank:
					return details.RatePerTank.Value != (double)ratePerTankValue || details.RatePerTank.Unit != ratePerTankUnit;
				default:
					return false;
			}
		}

		internal bool AssociatedInfoChanged() {
			return details.ApplicationMethod != ApplicationMethod || details.TargetPest.id != SelectedPest.PestId;
		}

		void UpdateProduct() {
			if (SelectedProduct == null || !ValidateViewModel()) { return; }

			switch (ApplyBy) {
				case ApplicationTypes.RatePerTank:
					if (SelectedRatePerTankUnit == null) { return; }
					break;
				case ApplicationTypes.TotalProduct:
					if (TotalProductUnit == null) { return; }
					break;
				case ApplicationTypes.RatePerArea:
				default:
					if (SelectedRatePerAreaUnitHelper == null) { return; }
					break;
			}

			RecalculateItem();

			if (AddProduct) {
				AddProductToApplication addCommand = new AddProductToApplication(appView.Id, endpoint.GenerateNewMetadata(), ToApplicationProductEntry());
				endpoint.SendOne(addCommand);
				parent.AddToProducts(ToProductDetails());
			} else {
				switch (appView.ProductStrategy) {
					case ProductApplicationStrategy.ByRatePerArea:
						ChangeAppliedProductRatePerArea c = new ChangeAppliedProductRatePerArea(appView.Id, endpoint.GenerateNewMetadata(), trackingId, ratePerAreaValue, ratePerAreaUnitHelper.Unit.Name);
						endpoint.SendOne(c);
						break;
					case ProductApplicationStrategy.ByTotalProduct:
						ChangeAppliedProductTotalProduct d = new ChangeAppliedProductTotalProduct(appView.Id, endpoint.GenerateNewMetadata(), trackingId, TotalProductValue, TotalProductUnit.Name);
						endpoint.SendOne(d);
						break;
					case ProductApplicationStrategy.ByRatePerTank:
						ChangeAppliedProductRatePerTank e = new ChangeAppliedProductRatePerTank(appView.Id, endpoint.GenerateNewMetadata(), trackingId, RatePerTankValue, SelectedRatePerTankUnit.Name);
						endpoint.SendOne(e);
						break;
					default:
						break;

				}

				if (SelectedPriceItem != null && SelectedPriceItem.IsSystem == false && SelectedPriceItem.Price != details.SpecificCostPerUnitValue) {
					ChangeAppliedProductSpecificCost costCommand = new ChangeAppliedProductSpecificCost(appView.Id, endpoint.GenerateNewMetadata(), trackingId, SelectedPriceItem.Price, SelectedPriceItem.PriceUnit);
					endpoint.SendOne(costCommand);
				}

				if (ProductAreaPercent != details.ProductAreaPercent) {
					ChangeAppliedProductCoveragePercent coverageCommand = new ChangeAppliedProductCoveragePercent(appView.Id, endpoint.GenerateNewMetadata(), trackingId, ProductAreaPercent);
					endpoint.SendOne(coverageCommand);
				}

				if (ApplicationMethod != null && ApplicationMethod != details.ApplicationMethod) {
					UpdateAppliedProductApplicationMethod appMethodCommand = new UpdateAppliedProductApplicationMethod(appView.Id, endpoint.GenerateNewMetadata(), trackingId, ApplicationMethod.Name);
					endpoint.SendOne(appMethodCommand);
				}

				if (GrowerSharePercent != details.GrowerShare || PercentFromGrowerInventory != details.FromGrowerInventory) {
					var totalFromGrower = (decimal)TotalProduct.Value * PercentFromGrowerInventory;
					var newShareInfo = new ApplicationProductShareOwnerInformation(GrowerSharePercent, Convert.ToDecimal(TotalProductFromGrowerInventory.Value), TotalProductFromGrowerInventory.Unit.Name);
					UpdateAppliedProductShareInfo shareInfoCommand = new UpdateAppliedProductShareInfo(appView.Id, endpoint.GenerateNewMetadata(), trackingId, newShareInfo);
					endpoint.SendOne(shareInfoCommand);
				}

				var pest = SelectedPest != null ? new MiniPest() { id = SelectedPest.PestId, Name = SelectedPest.CommonName, LatinList = SelectedPest.LatinName } : new MiniPest();
				var detailsPest = details.TargetPest != null ? details.TargetPest : new MiniPest();

				if (SelectedPest != null && pest != detailsPest) {
					UpdateAppliedProductTargetPest pestCommand = new UpdateAppliedProductTargetPest(appView.Id, endpoint.GenerateNewMetadata(), trackingId, pest.id, pest.Name, pest.LatinList);
					endpoint.SendOne(pestCommand);
				}

				var prod = ToProductDetails();

				parent.Products.Remove(details);
				parent.AddToProducts(prod);
			}

			Messenger.Default.Send<HideOverlayMessage>(new HideOverlayMessage());
		}

		internal void RecalculateItem() {
			switch (ApplyBy) {
				case ApplicationTypes.RatePerTank:
					RecalculateByTank();
					break;
				case ApplicationTypes.TotalProduct:
					RecalculateByTotal();
					break;
				case ApplicationTypes.RatePerArea:
				default:
					RecalculateByArea();
					break;
			}

			CalculateTotalProductFromGrowerInventory();

			ValidateProperty(() => RatePerAreaValue);
			ValidateProperty(() => RatePerTankValue);
			ValidateProperty(() => TotalProductValue);

			ValidateProperty(() => SelectedRatePerAreaUnitHelper);
			ValidateProperty(() => SelectedRatePerTankUnit);
			ValidateProperty(() => TotalProductUnit);
		}

		void RecalculateByArea() {
			if (SelectedProduct == null || SelectedRatePerAreaUnitHelper == null) {
				TotalProductValue = 0;
				TotalProductUnit = new NullUnit();

				SelectedRatePerTankUnit = null;
				RatePerTankValue = 0m;

				return;
			}

			var rate = SelectedRatePerAreaUnitHelper.Unit.GetMeasure((double)RatePerAreaValue, SelectedProduct.Product.Density);
			var totalArea = appView != null ? appView.TotalArea : woView.TotalArea;
			var tankInfo = appView != null ? appView.TankInformation : woView.TankInformation;
			ProductArea = totalArea.Unit.GetMeasure(totalArea.Value * (double)productAreaPercent);

			var calculation = applicationCalculationService.RecalculateByArea(
				SelectedProduct.Product,
				AveragePriceItem,
				rate,
				ProductArea,
				tankInfo == null ? 0m : tankInfo.TankCount,
				AllowedTotalUnits
			);

			TotalProductValue = calculation.TotalProductValue;
			TotalProductUnit = calculation.TotalProductUnit;

			var displayTotal = DisplayMeasureConverter.ConvertToTotalDisplayMeasure(SelectedProduct.ProductId, TotalProductValue, TotalProductUnit.Name, SelectedProduct.Product.Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);
			TotalProductValue = (decimal)displayTotal.Value;
			TotalProductUnit = displayTotal.Unit;

			RatePerTankValue = calculation.RatePerTankValue;
			var displayRatePerTank = DisplayMeasureConverter.ConvertToRatePerTankDisplayMeasure(SelectedProduct.ProductId, RatePerTankValue, calculation.RatePerTankUnit.Name, SelectedProduct.Product.Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);
			RatePerTankValue = (decimal)displayRatePerTank.Value;

			if (calculation.RatePerTankUnit != null) {
				SelectedRatePerTankUnit = displayRatePerTank.Unit;
			}

			TotalCost = calculation.TotalCost;
		}

		void RecalculateByTotal() {
			if (SelectedProduct == null || TotalProductUnit == null) {
				SelectedRatePerAreaUnitHelper = null;
				RatePerAreaValue = 0m;

				SelectedRatePerTankUnit = null;
				RatePerTankValue = 0m;
				return;
			}

			var total = TotalProductUnit.GetMeasure((double)TotalProductValue, SelectedProduct.Product.Density);
			var totalArea = appView != null ? appView.TotalArea : woView.TotalArea;
			var tankInfo = appView != null ? appView.TankInformation : woView.TankInformation;
			var productArea = totalArea.Unit.GetMeasure(totalArea.Value * (double)productAreaPercent);

			var calculation = applicationCalculationService.RecalculateByTotal(
				SelectedProduct.Product,
				total,
				productArea,
				tankInfo == null ? 0m : tankInfo.TankCount,
				AllowedTotalUnits,
				averagePriceItem
			);

			ProductArea = calculation.ProductArea;

			RatePerAreaValue = calculation.RatePerAreaValue;
            SelectedRatePerAreaUnitHelper = AllowedRateUnits.Where(x => x.Unit == calculation.PackageSafeUnit).Any() ? (from r in AllowedRateUnits where r.Unit.Name == calculation.PackageSafeUnit.Name select r).FirstOrDefault() : new UnitPerAreaHelper(calculation.PackageSafeUnit, calculation.ProductArea.Unit);

			var displayRate = DisplayMeasureConverter.ConvertToRateDisplayMeasure(SelectedProduct.ProductId, RatePerAreaValue, SelectedRatePerAreaUnitHelper.Unit.Name, Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);
			RatePerAreaValue = (decimal)displayRate.Value;
			SelectedRatePerAreaUnitHelper = new UnitPerAreaHelper(displayRate.Unit, appView.TotalArea.Unit);

			RatePerTankValue = calculation.RatePerTankValue;
			var displayRatePerTank = DisplayMeasureConverter.ConvertToRatePerTankDisplayMeasure(SelectedProduct.ProductId, RatePerTankValue, calculation.RatePerTankUnit.Name, Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);
			RatePerTankValue = (decimal)displayRatePerTank.Value;

			if (calculation.RatePerTankUnit != null) {
				SelectedRatePerTankUnit = displayRatePerTank.Unit;
			}

			TotalCost = calculation.TotalCost;
		}

		void RecalculateByTank() {
			if (SelectedProduct == null || SelectedRatePerTankUnit == null) {
				SelectedRatePerAreaUnitHelper = null;
				RatePerAreaValue = 0m;

				TotalProductValue = 0;
				TotalProductUnit = new NullUnit();
				return;
			}

			var ratePerTank = SelectedRatePerTankUnit.GetMeasure((double)RatePerTankValue, SelectedProduct.Product.Density);
			var totalArea = appView != null ? appView.TotalArea : woView.TotalArea;
			var tankInfo = appView != null ? appView.TankInformation : woView.TankInformation;
			var productArea = totalArea.Unit.GetMeasure(totalArea.Value * (double)productAreaPercent);

			var calculation = applicationCalculationService.RecalculateByTank(
				SelectedProduct.Product,
				AveragePriceItem,
				ratePerTank,
				productArea,
				tankInfo.TankCount
			);

			ProductArea = calculation.ProductArea;
			TotalProductValue = calculation.TotalProductValue;
			TotalProductUnit = calculation.TotalProductUnit;

			var displayTotal = DisplayMeasureConverter.ConvertToTotalDisplayMeasure(SelectedProduct.ProductId, TotalProductValue, TotalProductUnit.Name, Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);
			TotalProductValue = (decimal)displayTotal.Value;
			TotalProductUnit = displayTotal.Unit;

			RatePerAreaValue = calculation.RatePerAreaValue;
			SelectedRatePerAreaUnitHelper = (from r in AllowedRateUnits where r.Unit.Name == calculation.PackageSafeUnit.Name select r).FirstOrDefault();

			var displayRate = DisplayMeasureConverter.ConvertToRateDisplayMeasure(SelectedProduct.ProductId, RatePerAreaValue, SelectedRatePerAreaUnitHelper.Unit.Name, Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);
			RatePerAreaValue = (decimal)displayRate.Value;
			SelectedRatePerAreaUnitHelper = new UnitPerAreaHelper(displayRate.Unit, appView.TotalArea.Unit);

			TotalCost = calculation.TotalCost;
		}

		void OnProductCreated(MiniProduct product) {
            var pli = AddTemporaryMiniProduct(product, new ProductId(ApplicationEnvironment.CurrentDataSourceId, product.Id), true);
            SelectedProduct = pli;
		}

        internal ProductListItemDetails AddTemporaryMiniProduct(MiniProduct product, ProductId productId, bool isUsed)
        {
            var p = new ProductListItemDetails(product, productId.DataSourceId, isUsed ? Strings.UsedThisYear_Text : Strings.Unused_Text, isUsed ? (short)0 : (short)1);
            Products.Add(p);
            return p;
        }

		internal ApplicationProductDetails ToProductDetails() {
			var avgPriceUnit = currentInventoryItem != null && currentInventoryItem.AveragePriceUnit != null ? UnitFactory.GetUnitByName(currentInventoryItem.AveragePriceUnit) : null;
			var avgPriceValue = currentInventoryItem != null ? currentInventoryItem.AveragePriceValue : 0;

            var prod = new ApplicationProductDetails() {
                ProductName = endpoint.GetMasterlistService().GetProductDisplay(SelectedProduct.ProductId),
                RateMeasure = UnitFactory.GetUnitByName(SelectedRatePerAreaUnitHelper.Unit.Name).GetMeasure((double)RatePerAreaValue, SelectedProduct.Product.Density),
                AreaValue = (decimal)totalArea.Value,
                AreaUnit = UnitFactory.GetUnitByName(totalArea.Unit.Name),
                TotalProduct = UnitFactory.GetUnitByName(TotalProduct.Unit.Name).GetMeasure(TotalProduct.Value, SelectedProduct.Product.Density),
                TotalCost = TotalCost,
                SpecificCostPerUnitValue = SelectedPriceItem != null && SelectedPriceItem.IsSystem == false ? SelectedPriceItem.Price : 0m,
                SpecificCostUnit = SelectedPriceItem != null && SelectedPriceItem.IsSystem == false ? UnitFactory.GetUnitByName(SelectedPriceItem.PriceUnit) : null,
                CostPerUnitValue = (decimal)avgPriceValue,
                CostUnit = avgPriceUnit,
                ID = SelectedProduct.ProductId,
                ProductAreaPercent = ProductAreaPercent,
                TrackingId = trackingId,
                GrowerShare = GrowerSharePercent,
                Split = (TotalProduct.Value * (double)GrowerSharePercent).ToString("N2") + " / " + (TotalProduct.Value - (TotalProduct.Value * (double)GrowerSharePercent)).ToString("N2"),
                TargetPest = SelectedPest != null ? new MiniPest() { Name = SelectedPest.CommonName, LatinList = SelectedPest.LatinName, id = SelectedPest.PestId } : new MiniPest(),
                ApplicationMethod = ApplicationMethod,
                RatePerTank = SelectedRatePerTankUnit.GetMeasure((double)RatePerTankValue, SelectedProduct.Product.Density),
			};

            prod.AssociatedProducts = new ObservableCollection<AssociatedProductItem>();
            foreach (var associate in AssociatedProducts)
            {
                prod.AssociatedProducts.Add(associate);
                prod.HasAssociatedProducts = true;
            }

			return prod;
		}

		internal ApplicationProductEntry ToApplicationProductEntry() {
			// TODO: This needs to represent the actual rate unit the user has chosen.
			var psu = UnitFactory.GetPackageSafeUnit(SelectedProduct.Product.StdPackageUnit, SelectedProduct.Product.StdUnit, SelectedProduct.Product.StdFactor, SelectedProduct.Product.StdPackageUnit);

			var ratePerAreaUnit = SelectedRatePerAreaUnitHelper != null ? SelectedRatePerAreaUnitHelper.Unit.Name : psu.Name;
			var appMethod = ApplicationMethod == null ? string.Empty : ApplicationMethod.Name;
			MiniPest targetPest = new MiniPest();
			if (SelectedPest != null) { targetPest = new MiniPest() { id = SelectedPest.PestId, Name = SelectedPest.CommonName, LatinList = SelectedPest.LatinName, text = "" }; } else { targetPest = null; }

			var specificCost = SelectedPriceItem == null || SelectedPriceItem.IsSystem ? null : (decimal?)SelectedPriceItem.Price;
			var specificCostUnit = SelectedPriceItem == null || SelectedPriceItem.IsSystem ? null : SelectedPriceItem.PriceUnit;

            //ASSOCIATED PRODUCTS
            List<ApplicationAssociatedProduct> appAssociatedProducts = new List<ApplicationAssociatedProduct>();
            foreach (var associate in AssociatedProducts)
            {
                var appProd = new ApplicationAssociatedProduct();
                appProd.ProductId = associate.ProductID;
                appProd.ProductName = associate.ProductName;
                appProd.ManufacturerName = endpoint.GetMasterlistService().GetProduct(associate.ProductID).Manufacturer;
                appProd.RatePerAreaValue = associate.RatePerAreaValue;
                appProd.RatePerAreaUnit = associate.RatePerAreaUnit.Name;
                appProd.TotalProductValue = associate.TotalProductValue;
                appProd.TotalProductUnit = associate.TotalProductUnit.Name;
                appProd.TrackingId = associate.TrackingID;
                appProd.CustomProductUnit = associate.CustomUnit.Name;
                appProd.CustomProductValue = associate.CustomRateValue;
                appProd.CustomRateType = associate.CustomRateType.ToString();
                appProd.HasCost = associate.HasCost;
                appProd.SpecificCostPerUnitValue = associate.CostPerUnitValue;
                appProd.SpecificCostPerUnitUnit = associate.CostPerUnitUnit != null ? associate.CostPerUnitUnit.Name : string.Empty;
                appAssociatedProducts.Add(appProd);
            }
            //

            return new ApplicationProductEntry(
				selectedProduct.ProductId,
				SelectedProduct.Product.Name,
				SelectedProduct.Product.Manufacturer,
				trackingId,
				ratePerAreaValue,
				ratePerAreaUnit,
				ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit,
				RatePerTankValue,
				SelectedRatePerTankUnit.Name,
				(decimal)TotalProduct.Value,
				TotalProduct.Unit.Name,
				AveragePriceItem.Price,
				AveragePriceItem.PriceUnit,
				TotalGrowerCost,
				ProductAreaPercent,
				(decimal)ProductArea.Value,
				ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit,
				GetShareOwnerInformation(),
				SelectedProduct.Product.Density,
				appMethod,
				targetPest,
                appAssociatedProducts,
                specificCost,
				specificCostUnit
			);
		}

		internal string ToName() {
			if (SelectedProduct != null && SelectedProduct.Product != null) {
				return SelectedProduct.Product.Name;
			} else {
				return string.Empty;
			}
		}

		ApplicationProductShareOwnerInformation GetShareOwnerInformation() {
			if (TotalProductFromGrowerInventory == null) {
				return null;
			} else {
				return new ApplicationProductShareOwnerInformation(GrowerSharePercent, (decimal)TotalProductFromGrowerInventory.Value, TotalProductFromGrowerInventory.Unit.Name);
			}
		}

		async void BuildProductList() {
			var watch = System.Diagnostics.Stopwatch.StartNew();

			await Task.Run(() => {
				var mpl = endpoint.GetMasterlistService().GetProductList();
				var mur = endpoint.GetView<ProductUsageView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear)).GetValue(new ProductUsageView());
				var ucp = endpoint.GetView<UserCreatedProductList>(new DataSourceId(ApplicationEnvironment.CurrentDataSourceId)).GetValue(new UserCreatedProductList());

				Guid mlDsId = GlobalIdentifiers.GetMasterlistDataSourceId();
				var productGuids = mur.GetProductGuids();

				var q = from p in mpl
						let cont = productGuids.Contains(p.Id)
						select new ProductListItemDetails(p, mlDsId, cont ? Strings.UsedThisYear_Text  : Strings.Unused_Text, cont ? (short)0 : (short)1);

				var ucpQ = from p in ucp.UserCreatedProducts
						   let cont = productGuids.Contains(p.Id)
						   select new ProductListItemDetails(p, ApplicationEnvironment.CurrentDataSourceId, cont ? Strings.UsedThisYear_Text : Strings.Unused_Text, cont ? (short)0 : (short)1);

				Products = new ObservableCollection<ProductListItemDetails>(q.Union(ucpQ).OrderBy(x => x.GroupSortOrder).ToList());
				RaisePropertyChanged(() => Products);
			});

			watch.Stop();
		}

		void PopulatePests() {
            var ml = endpoint.GetMasterlistService();
            var q = from p in ml.GetPestList()
                        //where p.Language == "en"
                    select new PestListItemDetails(p, (p.Name != string.Empty ? (short)0 : (short)1));
            q = q.OrderBy(b => b.GroupSortOrder).ThenBy(x => x.CommonName);
            PestList = new ObservableCollection<PestListItemDetails>(q);
            RaisePropertyChanged(() => PestList);
            //if (details != null && details.TargetPest != null && details.TargetPest.id != 0) { SelectedPest = new PestListItemDetails(endpoint.GetMasterlistService().GetPest(details.TargetPest.id), 0); }
        }

		void CancelEdit() {
			//just close the popup message
			Messenger.Default.Send<HideOverlayMessage>(new HideOverlayMessage());
		}

		void UpdatePriceItems() {
			PriceItems.Clear();
			if (currentInventoryItem != null) {
				var priceItem = new PriceItem((decimal)currentInventoryItem.AveragePriceValue, currentInventoryItem.AveragePriceUnit ?? SelectedProduct.Product.StdPackageUnit, true);

				PriceItems.Add(priceItem);
				AveragePriceItem = priceItem;
			} else {
				if (SelectedProduct != null) {
					var priceItem = new PriceItem(0m, SelectedProduct.Product.StdPackageUnit, false);
					PriceItems.Add(priceItem);
					AveragePriceItem = priceItem;
				}
			}

			if (details != null && details.SpecificCostPerUnitValue != null && details.SpecificCostUnit != null) {
				var specificPrice = new PriceItem((decimal)details.SpecificCostPerUnitValue, details.SpecificCostUnit.Name, false);
				PriceItems.Add(specificPrice);
				SelectedPriceItem = PriceItems.FirstOrDefault(x => x.Price == details.SpecificCostPerUnitValue);
			} else {
				SelectedPriceItem = PriceItems.FirstOrDefault();
                RaisePropertyChanged(() => SelectedPriceItem);

                selectedPriceText = SelectedPriceItem != null ? SelectedPriceItem.ToString() : string.Empty;
                RaisePropertyChanged(() => SelectedPriceText);
			}
		}

		void CreateUserProduct() {
			IsProductListVisible = false;
			IsCreateProductPopupOpen = false;

			var vm = new Landdb.ViewModel.Secondary.UserCreatedProduct.UserProductCreatorViewModel(endpoint, ProductSearchText, OnProductCreated);
			Messenger.Default.Send<ShowOverlayMessage>(new ShowOverlayMessage() {
				ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Secondary.UserCreatedProduct.UserProductCreatorView", "createProduct", vm)
			});

			//ShowPopupMessage msg = new ShowPopupMessage() {
			//    ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Secondary.UserCreatedProduct.UserProductCreatorView", "createProduct",
			//        new Landdb.ViewModel.Secondary.UserCreatedProduct.UserProductCreatorViewModel(endpoint, ProductSearchText, OnProductCreated))
			//};
			//Messenger.Default.Send<ShowPopupMessage>(msg);
		}

		public RelayCommand CreateUserProductCommand { get; private set; }

		#region Custom Validation
		public static ValidationResult ValidateSelectedProduct(ProductListItemDetails product, ValidationContext context) {
			ValidationResult result = null;

			if (product == null) {
				result = new ValidationResult(Strings.ProductIsRequired_Text);
			}

			return result;
		}

		public static ValidationResult ValidateRate(decimal rate, ValidationContext context) {
			ValidationResult result = null;

			var vm = (EditAppliedProductViewModel)context.ObjectInstance;

			var validateRatePerArea = vm.ApplyBy == ApplicationTypes.RatePerArea && context.MemberName == "RatePerAreaValue";
			var validateRatePerTank = vm.ApplyBy == ApplicationTypes.RatePerTank && context.MemberName == "RatePerTankValue";
			var validateTotalProduct = vm.ApplyBy == ApplicationTypes.TotalProduct && context.MemberName == "TotalProductValue";

			var shouldValidate = validateRatePerArea || validateRatePerTank || validateTotalProduct;

			if (shouldValidate && rate == 0) {
				result = new ValidationResult(Strings.RateCannotBeZero_Text);
			}

			return result;
		}

		public static ValidationResult ValidateUnit(object unit, ValidationContext context) {
			ValidationResult result = null;

			var vm = (EditAppliedProductViewModel)context.ObjectInstance;

			var validateRatePerArea = vm.ApplyBy == ApplicationTypes.RatePerArea && context.MemberName == "SelectedRatePerAreaUnitHelper";
			var validateRatePerTank = vm.ApplyBy == ApplicationTypes.RatePerTank && context.MemberName == "SelectedRatePerTankUnit";
			var validateTotalProduct = vm.ApplyBy == ApplicationTypes.TotalProduct && context.MemberName == "TotalProductUnit";

			var shouldValidate = validateRatePerArea || validateRatePerTank || validateTotalProduct;

			if (shouldValidate && unit == null) {
				result = new ValidationResult(Strings.ValidationResult_InvalidUnit_Text);
			}

			return result;
		}

		public static ValidationResult ValidateSelectedPest(PestListItemDetails selectedPest, ValidationContext context) {
			ValidationResult result = null;

			var vm = (EditAppliedProductViewModel)context.ObjectInstance;

			if (!string.IsNullOrWhiteSpace(vm.PestSearchText) && selectedPest == null) {
				result = new ValidationResult(Strings.InvalidPestSelected_Text);
			}

			return result;
		}
		#endregion
	}
}