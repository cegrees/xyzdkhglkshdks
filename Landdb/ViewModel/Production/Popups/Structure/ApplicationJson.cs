﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Landdb.Resources;

namespace Landdb.ViewModel.Production.Popups.Structure {
    public class ApplicationJson {
    }


    public partial class CreateApplication0 : IDomainCommand<ApplicationId> {
        [DataMember(Order = 1)]
        public ApplicationId Id { get; private set; }
        [DataMember(Order = 2)]
        public MessageMetadata Metadata { get; private set; }
        [DataMember(Order = 3)]
        public int CropYear { get; private set; }
        [DataMember(Order = 4)]
        public string Name { get; private set; }
        [DataMember(Order = 5)]
        public DateTime StartDateTime { get; private set; }
        [DataMember(Order = 6)]
        public Nullable<TimeSpan> Duration { get; private set; }
        [DataMember(Order = 7)]
        public string TimingEvent { get; private set; }
        [DataMember(Order = 8)]
        public Nullable<DateTime> EndDateTime { get; private set; }
        [DataMember(Order = 9)]
        public string Notes { get; private set; }
        [DataMember(Order = 10)]
        public ApplicationAuthorization Authorization { get; private set; }
        [DataMember(Order = 11)]
        public ApplicationApplicator[] Applicators { get; private set; }
        [DataMember(Order = 12)]
        public CropZoneArea[] CropZones { get; private set; }
        [DataMember(Order = 13)]
        public ApplicationTankInformation TankInformation { get; private set; }
        [DataMember(Order = 14)]
        public ProductApplicationStrategy ApplicationStrategy { get; private set; }
        [DataMember(Order = 15)]
        public ApplicationProductByArea[] Products { get; private set; }
        [DataMember(Order = 16)]
        public ApplicationConditions Conditions { get; private set; }
        [DataMember(Order = 17)]
        public DocumentDescriptor[] Documents { get; private set; }
        [DataMember(Order = 18)]
        public string TimingEventTag { get; private set; }

        CreateApplication0() {
            Applicators = new ApplicationApplicator[0];
            CropZones = new CropZoneArea[0];
            Products = new ApplicationProductByArea[0];
            Documents = new DocumentDescriptor[0];
        }
        public CreateApplication0(ApplicationId id, MessageMetadata metadata, int cropYear, string name, DateTime startDateTime, Nullable<TimeSpan> duration, string timingEvent, Nullable<DateTime> endDateTime, string notes, ApplicationAuthorization authorization, ApplicationApplicator[] applicators, CropZoneArea[] cropZones, ApplicationTankInformation tankInformation, ProductApplicationStrategy applicationStrategy, ApplicationProductByArea[] products, ApplicationConditions conditions, DocumentDescriptor[] documents, string timingEventTag) {
            Id = id;
            Metadata = metadata;
            CropYear = cropYear;
            Name = name;
            StartDateTime = startDateTime;
            Duration = duration;
            TimingEvent = timingEvent;
            EndDateTime = endDateTime;
            Notes = notes;
            Authorization = authorization;
            Applicators = applicators;
            CropZones = cropZones;
            TankInformation = tankInformation;
            ApplicationStrategy = applicationStrategy;
            Products = products;
            Conditions = conditions;
            Documents = documents;
            TimingEventTag = timingEventTag;
        }

        public override string ToString() {
            return string.Format(@Strings.CreateApplicationInIdWithCropyear_Format_Text, Id, CropYear);
        }
    }

    public partial class CreateApplication1 : IDomainCommand<ApplicationId> {
        [DataMember(Order = 1)]
        public ApplicationId Id { get; private set; }
        [DataMember(Order = 2)]
        public MessageMetadata Metadata { get; private set; }
        [DataMember(Order = 3)]
        public int CropYear { get; private set; }
        [DataMember(Order = 4)]
        public string Name { get; private set; }
        [DataMember(Order = 5)]
        public DateTime StartDateTime { get; private set; }
        [DataMember(Order = 6)]
        public Nullable<TimeSpan> Duration { get; private set; }
        [DataMember(Order = 7)]
        public string TimingEvent { get; private set; }
        [DataMember(Order = 8)]
        public Nullable<DateTime> EndDateTime { get; private set; }
        [DataMember(Order = 9)]
        public string Notes { get; private set; }
        [DataMember(Order = 10)]
        public ApplicationAuthorization Authorization { get; private set; }
        [DataMember(Order = 11)]
        public ApplicationApplicator[] Applicators { get; private set; }
        [DataMember(Order = 12)]
        public CropZoneArea[] CropZones { get; private set; }
        [DataMember(Order = 13)]
        public ApplicationTankInformation TankInformation { get; private set; }
        [DataMember(Order = 14)]
        public ProductApplicationStrategy ApplicationStrategy { get; private set; }
        [DataMember(Order = 15)]
        public ApplicationProductByTotal[] Products { get; private set; }
        [DataMember(Order = 16)]
        public ApplicationConditions Conditions { get; private set; }
        [DataMember(Order = 17)]
        public DocumentDescriptor[] Documents { get; private set; }
        [DataMember(Order = 18)]
        public string TimingEventTag { get; private set; }

        CreateApplication1() {
            Applicators = new ApplicationApplicator[0];
            CropZones = new CropZoneArea[0];
            Products = new ApplicationProductByTotal[0];
            Documents = new DocumentDescriptor[0];
        }
        public CreateApplication1(ApplicationId id, MessageMetadata metadata, int cropYear, string name, DateTime startDateTime, Nullable<TimeSpan> duration, string timingEvent, Nullable<DateTime> endDateTime, string notes, ApplicationAuthorization authorization, ApplicationApplicator[] applicators, CropZoneArea[] cropZones, ApplicationTankInformation tankInformation, ProductApplicationStrategy applicationStrategy, ApplicationProductByTotal[] products, ApplicationConditions conditions, DocumentDescriptor[] documents, string timingEventTag) {
            Id = id;
            Metadata = metadata;
            CropYear = cropYear;
            Name = name;
            StartDateTime = startDateTime;
            Duration = duration;
            TimingEvent = timingEvent;
            EndDateTime = endDateTime;
            Notes = notes;
            Authorization = authorization;
            Applicators = applicators;
            CropZones = cropZones;
            TankInformation = tankInformation;
            ApplicationStrategy = applicationStrategy;
            Products = products;
            Conditions = conditions;
            Documents = documents;
            TimingEventTag = timingEventTag;
        }

        public override string ToString() {
            return string.Format(Strings.CreateApplicationInIdWithCropyear_Format_Text, Id, CropYear);
        }
    }

    public partial class CreateApplication2 : IDomainCommand<ApplicationId> {
        [DataMember(Order = 1)]
        public ApplicationId Id { get; private set; }
        [DataMember(Order = 2)]
        public MessageMetadata Metadata { get; private set; }
        [DataMember(Order = 3)]
        public int CropYear { get; private set; }
        [DataMember(Order = 4)]
        public string Name { get; private set; }
        [DataMember(Order = 5)]
        public DateTime StartDateTime { get; private set; }
        [DataMember(Order = 6)]
        public Nullable<TimeSpan> Duration { get; private set; }
        [DataMember(Order = 7)]
        public string TimingEvent { get; private set; }
        [DataMember(Order = 8)]
        public Nullable<DateTime> EndDateTime { get; private set; }
        [DataMember(Order = 9)]
        public string Notes { get; private set; }
        [DataMember(Order = 10)]
        public ApplicationAuthorization Authorization { get; private set; }
        [DataMember(Order = 11)]
        public ApplicationApplicator[] Applicators { get; private set; }
        [DataMember(Order = 12)]
        public CropZoneArea[] CropZones { get; private set; }
        [DataMember(Order = 13)]
        public ApplicationTankInformation TankInformation { get; private set; }
        [DataMember(Order = 14)]
        public ProductApplicationStrategy ApplicationStrategy { get; private set; }
        [DataMember(Order = 15)]
        public ApplicationProductByTank[] Products { get; private set; }
        [DataMember(Order = 16)]
        public ApplicationConditions Conditions { get; private set; }
        [DataMember(Order = 17)]
        public DocumentDescriptor[] Documents { get; private set; }
        [DataMember(Order = 18)]
        public string TimingEventTag { get; private set; }

        CreateApplication2() {
            Applicators = new ApplicationApplicator[0];
            CropZones = new CropZoneArea[0];
            Products = new ApplicationProductByTank[0];
            Documents = new DocumentDescriptor[0];
        }
        public CreateApplication2(ApplicationId id, MessageMetadata metadata, int cropYear, string name, DateTime startDateTime, Nullable<TimeSpan> duration, string timingEvent, Nullable<DateTime> endDateTime, string notes, ApplicationAuthorization authorization, ApplicationApplicator[] applicators, CropZoneArea[] cropZones, ApplicationTankInformation tankInformation, ProductApplicationStrategy applicationStrategy, ApplicationProductByTank[] products, ApplicationConditions conditions, DocumentDescriptor[] documents, string timingEventTag) {
            Id = id;
            Metadata = metadata;
            CropYear = cropYear;
            Name = name;
            StartDateTime = startDateTime;
            Duration = duration;
            TimingEvent = timingEvent;
            EndDateTime = endDateTime;
            Notes = notes;
            Authorization = authorization;
            Applicators = applicators;
            CropZones = cropZones;
            TankInformation = tankInformation;
            ApplicationStrategy = applicationStrategy;
            Products = products;
            Conditions = conditions;
            Documents = documents;
            TimingEventTag = timingEventTag;
        }

        public override string ToString() {
            return string.Format(@Strings.CreateApplicationInIdWithCropyear_Format_Text, Id, CropYear);
        }
    }

}
