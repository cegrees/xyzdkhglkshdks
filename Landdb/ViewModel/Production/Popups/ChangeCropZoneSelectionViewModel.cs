﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Shared;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Fields;
using Landdb.ViewModel.Secondary.Application;
using Landdb.ViewModel.Shared;
using NLog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;

namespace Landdb.ViewModel.Production.Popups {
	public class ChangeCropZoneSelectionViewModel : FieldSelectionViewModel {

		readonly List<IncludedCropZone> initialCropZones;
		readonly Action<ChangeCropZoneSelectionViewModel> cropZonesChanged;

		public ChangeCropZoneSelectionViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, CropYearId currentCropYearId, List<IncludedCropZone> initialCropZones, ClearAgWeatherConditionsViewModel conditionsModel, bool areFieldSelectionsRequired, Action<ChangeCropZoneSelectionViewModel> onFieldsChanged)
			: base(clientEndpoint, dispatcher, currentCropYearId, conditionsModel, true) {

			this.initialCropZones = initialCropZones;

			checkInitialCropZones();

			TotalArea = TotalArea.Unit.GetMeasure(0);

			foreach (var area in SelectedCropZones) {
				var edited = initialCropZones.FirstOrDefault(x => x.Id == area.Id);
				area.SelectedArea = edited.Area;
				TotalArea = TotalArea.Unit.GetMeasure(TotalArea.Value + area.SelectedArea.Value);
			}

			this.cropZonesChanged += onFieldsChanged;

			SaveChangesCommand = new RelayCommand(onSaveChanges);
			CancelChangesCommand = new RelayCommand(onCancelChanges);
		}

		public ICommand SaveChangesCommand { get; private set; }
		public ICommand CancelChangesCommand { get; private set; }

		public IEnumerable<CropZoneArea> AddedCropZones {
			get {
				return from a in SelectedCropZones
					   where !initialCropZones.Exists(x => x.Id == a.Id)
					   select new CropZoneArea(a.Id, a.SelectedArea.Value, a.SelectedArea.Unit.Name);
			}
		}

		public IEnumerable<CropZoneArea> ChangedCropZones {
			get {
				return from c in SelectedCropZones
					   where initialCropZones.Exists(x => x.Id == c.Id && (x.Area.Unit != c.SelectedArea.Unit || Math.Abs(x.Area.Value - c.SelectedArea.Value) > 0.0001))
					   select new CropZoneArea(c.Id, c.SelectedArea.Value, c.SelectedArea.Unit.Name);
			}
		}

		public IEnumerable<CropZoneId> RemovedCropZones {
			get {
				return from r in initialCropZones
					   where !SelectedCropZones.Where(x => x.Id == r.Id).Any()
					   select r.Id;
			}
		}

		private void checkInitialCropZones() {
			var initialCzIds = initialCropZones.Select(x => x.Id);

			if (RootTreeItemModels.Any()) {
				if (RootTreeItemModels.First() is GrowerTreeItemViewModel) {
					var toCheck = from farm in RootTreeItemModels.First().Children
								  from field in farm.Children
								  from cz in field.Children
								  where initialCzIds.Contains(cz.Id)
								  select cz;
					toCheck.ForEach(x => x.IsChecked = true);
				} else {
					var toCheck = from crop in RootTreeItemModels.First().Children
								  from farm in crop.Children
								  from field in farm.Children
								  from cz in field.Children
								  where initialCzIds.Contains(cz.Id)
								  select cz;
					toCheck.ForEach(x => x.IsChecked = true);
				}
			}
		}

		private void onSaveChanges() {
			cropZonesChanged(this);
		}

		private void onCancelChanges() {
			cropZonesChanged(null);
		}
	}
}