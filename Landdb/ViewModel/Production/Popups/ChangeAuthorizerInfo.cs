﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Person;
using Landdb.Domain.ReadModels.Recommendation;
using Landdb.Domain.ReadModels.WorkOrder;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.ViewModel.Production.Applications;

namespace Landdb.ViewModel.Production.Popups {
    public class ChangeAuthorizerInfo : ViewModelBase {
        IClientEndpoint endpoint;
        Dispatcher dispatcher;
        ApplicationView view;
        WorkOrderView woView;
        RecommendationView recView;
        ApplicationDetailsViewModel parent;

        DataSourceId currentDataSourceId;
        int currentCropYear;

        public ChangeAuthorizerInfo(IClientEndpoint endpoint, Dispatcher dispatcher, ApplicationView view, ApplicationDetailsViewModel parent) {
            this.endpoint = endpoint;
            this.dispatcher = dispatcher;
            this.view = view;
            this.parent = parent;

            this.currentDataSourceId = new DataSourceId(ApplicationEnvironment.CurrentDataSourceId);
            this.currentCropYear = ApplicationEnvironment.CurrentCropYear;

            UpdatePeople();

            if (view.Authorization != null) {
                AuthorizationDate = view.Authorization.AuthorizationDate;
            } else {
                AuthorizationDate = view.StartDateTime;
            }

            RaisePropertyChanged("AuthorizationDate");

            Update = new RelayCommand(this.UpdateAuthorizer);
            Cancel = new RelayCommand(this.CancelAuthorizer);
        }

        public ChangeAuthorizerInfo(IClientEndpoint endpoint, Dispatcher dispatcher, WorkOrderView view, ApplicationDetailsViewModel parent) {
            this.endpoint = endpoint;
            this.dispatcher = dispatcher;
            this.woView = view;
            this.parent = parent;

            this.currentDataSourceId = new DataSourceId(ApplicationEnvironment.CurrentDataSourceId);
            this.currentCropYear = ApplicationEnvironment.CurrentCropYear;

            UpdatePeople();

            if (view.Authorization != null) {
                AuthorizationDate = view.Authorization.AuthorizationDate;
            } else {
                AuthorizationDate = view.StartDateTime.ToLocalTime();
            }

            RaisePropertyChanged("AuthorizationDate");

            Update = new RelayCommand(this.UpdateAuthorizer);
            Cancel = new RelayCommand(this.CancelAuthorizer);
        }

        public ChangeAuthorizerInfo(IClientEndpoint endpoint, Dispatcher dispatcher, RecommendationView view)
        {
            this.endpoint = endpoint;
            this.dispatcher = dispatcher;
            this.recView = view;

            this.currentDataSourceId = new DataSourceId(ApplicationEnvironment.CurrentDataSourceId);
            this.currentCropYear = ApplicationEnvironment.CurrentCropYear;

            UpdatePeople();

            if (view.Authorization != null)
            {
                AuthorizationDate = view.Authorization.AuthorizationDate;
            }
            else
            {
                AuthorizationDate = view.CreatedDateTime;
            }

            RaisePropertyChanged("AuthorizationDate");

            Update = new RelayCommand(this.UpdateAuthorizer);
            Cancel = new RelayCommand(this.CancelAuthorizer);
        }

        public ICommand Update { get; private set; }
        public ICommand Cancel { get; private set; }

        public PersonListItemViewModel Person { get; set; }
        public List<PersonListItemViewModel> People { get; set; }
        public DateTime AuthorizationDate { get; set; }

        void UpdatePeople() {
            var personListView = endpoint.GetView<PersonListView>(currentDataSourceId).GetValue(() => null);

            if (personListView != null) {
				var sortedFilteredProjectedPersons = from p in personListView.Persons
													 where p.IsActiveInCropYear(currentCropYear)
													 orderby p.Name
													 select new PersonListItemViewModel(p);

                dispatcher.BeginInvoke(new Action(() => {
                    People = new List<PersonListItemViewModel>(sortedFilteredProjectedPersons);

                    if (view != null) {
                        Person = view.Authorization != null ? People.Where(x => x.Id == view.Authorization.PersonId).FirstOrDefault() : null;
                    }

                    if (woView != null) {
                            Person = woView.Authorization != null ? People.Where(x => x.Id == woView.Authorization.PersonId).FirstOrDefault() : null;
                    }

                    if (recView != null)
                    {
                        Person = recView.Authorization != null ? People.Where(x => x.Id == recView.Authorization.PersonId).FirstOrDefault() : null;
                    }

                    RaisePropertyChanged("People");
                    RaisePropertyChanged("Person");
                }));
            }
        }

        bool IfInfoChange() {

            if (view != null)
            {
                if (view != null && view.Authorization != null && Person != null)
                {
                    return Person.Id != view.Authorization.PersonId || AuthorizationDate != view.Authorization.AuthorizationDate;
                }
                else if (view != null && (Person != null && AuthorizationDate != null))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else if (recView != null)
            {
                if (recView != null && recView.Authorization != null)
                {
                    return Person.Id != recView.Authorization.PersonId || AuthorizationDate != recView.Authorization.AuthorizationDate;
                }
                else if (recView != null && (Person != null && AuthorizationDate != null))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else if (woView != null)
            {
                if (woView != null && woView.Authorization != null && Person != null)
                {
                    return Person.Id != woView.Authorization.PersonId || AuthorizationDate != woView.Authorization.AuthorizationDate;
                }
                else if (woView != null && (Person != null && AuthorizationDate != null))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else { return false; }
        }

        void UpdateAuthorizer() {
            if (IfInfoChange()) {
                ApplicationAuthorization auth = new ApplicationAuthorization(Person.Id, Person.Name, AuthorizationDate);

                if (view != null) {
                    ChangeApplicationAuthorization authorizeCommand = new ChangeApplicationAuthorization(view.Id, endpoint.GenerateNewMetadata(), auth);
                    endpoint.SendOne(authorizeCommand);
                    parent.AuthorizeDate = AuthorizationDate;
                    parent.Authorizer = Person.Name;
                }
                if (woView != null) {
                    //send wo change
                    ChangeWorkOrderAuthorization woCommand = new ChangeWorkOrderAuthorization(woView.Id, endpoint.GenerateNewMetadata(), auth, ApplicationEnvironment.CurrentCropYear);
                    endpoint.SendOne(woCommand);
                    parent.AuthorizeDate = AuthorizationDate;
                    parent.Authorizer = Person.Name;
                }
                if (recView != null)
                {
                    //send wo change
                    ChangeRecommendationAuthorization recCommand = new ChangeRecommendationAuthorization(recView.Id, endpoint.GenerateNewMetadata(), auth);
                    endpoint.SendOne(recCommand);
                }

                Messenger.Default.Send(new HidePopupMessage());
            }
        }
        void CancelAuthorizer() {
            Messenger.Default.Send(new HidePopupMessage());
        }
    }
}
