﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Person;
using Landdb.Domain.ReadModels.Recommendation;
using Landdb.Domain.ReadModels.WorkOrder;
using Landdb.Domain.ReadModels.Plan;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;

namespace Landdb.ViewModel.Production.Popups {
    public class ChoosePersonToShareViewModel : ViewModelBase {
        IClientEndpoint endpoint;
        Dispatcher dispatcher;
        ApplicationView view;
        WorkOrderView woView;
        RecommendationView recView;
        PlanView planView;
        PersonListItemViewModel person;
        DataSourceId currentDataSourceId;
        int currentCropYear;
        readonly Action<ChoosePersonToShareViewModel> personShared;

        public ChoosePersonToShareViewModel(IClientEndpoint endpoint, Dispatcher dispatcher, ApplicationView view, Action<ChoosePersonToShareViewModel> onPersonShared) {
            this.endpoint = endpoint;
            this.dispatcher = dispatcher;
            this.view = view;

            this.currentDataSourceId = new DataSourceId(ApplicationEnvironment.CurrentDataSourceId);
            this.currentCropYear = ApplicationEnvironment.CurrentCropYear;

            UpdatePeople();

            this.personShared += onPersonShared;

            Update = new RelayCommand(this.UpdateAuthorizer);
            Cancel = new RelayCommand(this.CancelAuthorizer);
        }

        public ChoosePersonToShareViewModel(IClientEndpoint endpoint, Dispatcher dispatcher, WorkOrderView view, Action<ChoosePersonToShareViewModel> onPersonShared) {
            this.endpoint = endpoint;
            this.dispatcher = dispatcher;
            this.woView = view;

            this.currentDataSourceId = new DataSourceId(ApplicationEnvironment.CurrentDataSourceId);
            this.currentCropYear = ApplicationEnvironment.CurrentCropYear;

            UpdatePeople();

            this.personShared += onPersonShared;

            Update = new RelayCommand(this.UpdateAuthorizer);
            Cancel = new RelayCommand(this.CancelAuthorizer);
        }

        public ChoosePersonToShareViewModel(IClientEndpoint endpoint, Dispatcher dispatcher, RecommendationView view, Action<ChoosePersonToShareViewModel> onPersonShared) {
            this.endpoint = endpoint;
            this.dispatcher = dispatcher;
            this.recView = view;

            this.currentDataSourceId = new DataSourceId(ApplicationEnvironment.CurrentDataSourceId);
            this.currentCropYear = ApplicationEnvironment.CurrentCropYear;

            UpdatePeople();

            this.personShared += onPersonShared;

            Update = new RelayCommand(this.UpdateAuthorizer);
            Cancel = new RelayCommand(this.CancelAuthorizer);
        }

        public ChoosePersonToShareViewModel(IClientEndpoint endpoint, Dispatcher dispatcher, PlanView view, Action<ChoosePersonToShareViewModel> onPersonShared) {
            this.endpoint = endpoint;
            this.dispatcher = dispatcher;
            this.planView = view;

            this.currentDataSourceId = new DataSourceId(ApplicationEnvironment.CurrentDataSourceId);
            this.currentCropYear = ApplicationEnvironment.CurrentCropYear;

            UpdatePeople();

            this.personShared += onPersonShared;

            Update = new RelayCommand(this.UpdateAuthorizer);
            Cancel = new RelayCommand(this.CancelAuthorizer);
        }

        public ICommand Update { get; private set; }
        public ICommand Cancel { get; private set; }

        public PersonListItemViewModel Person {
            get { return person; }
            set {
                person = value;
                if (person != null) {
                    EmailAddress = person.PrimaryEmailAddress;
                    RaisePropertyChanged("EmailAddress");
                }
                RaisePropertyChanged("Person");
            }
        }
        public List<PersonListItemViewModel> People { get; set; }
        public string EmailAddress { get; set; }

        void UpdatePeople() {
            var personListView = endpoint.GetView<PersonListView>(currentDataSourceId).GetValue(() => null);

            if (personListView != null) {
                var sortedFilteredProjectedPersons = from p in personListView.Persons
                                                     where p.IsActiveInCropYear(currentCropYear)
                                                     orderby p.Name
                                                     select new PersonListItemViewModel(p);

                dispatcher.BeginInvoke(new Action(() => {
                    People = new List<PersonListItemViewModel>(sortedFilteredProjectedPersons);

                    Person = null;

                    RaisePropertyChanged("People");
                    RaisePropertyChanged("Person");
                }));
            }
        }


        void UpdateAuthorizer() {
            Messenger.Default.Send(new HidePopupMessage());
            personShared(this);
        }
        void CancelAuthorizer() {
            Person = null;
            EmailAddress = string.Empty;
            Messenger.Default.Send(new HidePopupMessage());
            personShared(null);
        }
    }
}
