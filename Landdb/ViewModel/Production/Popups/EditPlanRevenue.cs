﻿using AgC.UnitConversion;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Plan;
using Landdb.Domain.ReadModels.Shared;
using Landdb.Infrastructure.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Production.Popups
{
    public class EditPlanRevenue : ViewModelBase
    {
        PlanView plan;
        IClientEndpoint endpoint;
        PlanDetailsViewModel parent;

        double pricePerUnit;
        double yield;
        Measure estimatedArea;
        double fsa;
        bool isSet;
        double revenue;
        double budget;
        double netRevenue;
        string estimatedAreaDisplay;
        double share;
        Action<EditPlanRevenue> updateRevenue;

        public EditPlanRevenue(IClientEndpoint endPoint, PlanView detail, PlanDetailsViewModel parent)
        {
            this.plan = detail;
            this.endpoint = endPoint;
            this.parent = parent;
            isSet = false;

            InitializeProperties(plan);
            Update = new RelayCommand(UpdateBudget);
            Cancel = new RelayCommand(CancelBudget);
        }

        public EditPlanRevenue(IClientEndpoint endPoint, PlanView detail, PlanDetailsViewModel parent, Action<EditPlanRevenue> onUpdate)
        {
            this.plan = detail;
            this.endpoint = endPoint;
            this.parent = parent;
            isSet = false;

            InitializeProperties(plan);

            updateRevenue += onUpdate;
            Update = new RelayCommand(UpdateBudget);
            Cancel = new RelayCommand(CancelBudget);
        }

        public RelayCommand Update { get; set; }
        public RelayCommand Cancel { get; set; }

        public double NetRevenue { get { return netRevenue; } set { netRevenue = value; RaisePropertyChanged("NetRevenue"); } }
        public double Yield { get { return yield; } set { yield = value; if (isSet) CalculateNetRevenue(); } }
        public double PricePerUnit { get { return pricePerUnit; } set { pricePerUnit = value; if (isSet) CalculateNetRevenue(); } }
        public double FSA { get { return fsa; } set { fsa = value; if (isSet) CalculateNetRevenue(); } }
        public Measure EstimatedArea { get { return estimatedArea; } set { estimatedArea = value; if (isSet) CalculateNetRevenue(); } }

        public string EstimatedAreaDisplay
        {
            get { return string.Format(@"{0} {1}", EstimatedArea.Value.ToString("N2"), EstimatedArea.Unit.AbbreviatedDisplay); }
            set {
                if (string.IsNullOrWhiteSpace(value)) { return; }
                var oldArea = EstimatedArea.Value;
                if (estimatedAreaDisplay != value && EstimatedArea != null)
                {
                    estimatedAreaDisplay = value;
                    decimal area = 0m;
                    if (decimal.TryParse(estimatedAreaDisplay, out area))
                    {
                        var areaUnit = EstimatedArea.Unit;
                        var newTotalArea = areaUnit.GetMeasure((double)area);
                        
                        if (newTotalArea != null && newTotalArea.Value > 0)
                        {
                            EstimatedArea = newTotalArea;
                        }
                        RaisePropertyChanged("EstimateAreaDisplay");
                    }
                }
                else
                {
                    estimatedAreaDisplay = "0";
                }
            }
        }
        public double Revenue { get { return revenue; } set { revenue = value; RaisePropertyChanged("Revenue"); } }
        public double Budget { get { return budget; } set { budget = value; RaisePropertyChanged("Budget"); } }
        public double Inputs { get; set; }
        public double Share
        {
            get { return share; }
            set
            {
                if (value != null && value > 1.0)
                {
                    share = value / 100;
                }
                else
                {
                    share = value;
                }
                CalculateNetRevenue();
            }
        }

        void InitializeProperties(PlanView view)
        {
            EstimatedArea = view.EstimatedArea;
            Yield = view.YieldPerArea;
            PricePerUnit = view.PricePerUnit;
            FSA = view.FSAPayment;
            Share = view.CropSharePercent;
            isSet = true;
            CalculateNetRevenue();
        }

        void CalculateNetRevenue()
        {
            var revenue = ((Yield * EstimatedArea.Value * PricePerUnit) * Share) + (FSA * EstimatedArea.Value);
            Revenue = revenue;
            var expend = (parent.BudgetPer) * EstimatedArea.Value;
            Budget = expend;
            var inputCost = 0.0;

            foreach (IncludedProduct pd in plan.Products)
            {
                inputCost += (double)pd.CostPerUnit * (double)pd.TotalProductValue;
            }

            Inputs = inputCost;
            NetRevenue = revenue - (expend + inputCost);
        }

        void UpdateBudget()
        {
            updateRevenue(this);
            Messenger.Default.Send(new HidePopupMessage());
        }

        bool IsInfoChanged()
        {
            return PricePerUnit != plan.PricePerUnit || Yield != plan.YieldPerArea || EstimatedArea != plan.EstimatedArea || FSA != plan.FSAPayment || Share != plan.CropSharePercent;
        }

        void CancelBudget()
        {
            Messenger.Default.Send(new HidePopupMessage());
        }
    }
}
