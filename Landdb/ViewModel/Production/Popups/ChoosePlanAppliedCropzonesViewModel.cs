﻿using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.Services;
using Landdb.ViewModel.Fields;
using Landdb.ViewModel.Fields.FieldDetails;
using Landdb.ViewModel.Secondary.Plan;
using Landdb.ViewModel.Shared;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Threading;

namespace Landdb.ViewModel.Production.Popups {
	public class ChoosePlanAppliedCropzonesViewModel : BaseFieldSelectionViewModel {
		PlannerViewModel parent;
		Guid currentDataSourceId;
		int selectedcropyear;
		List<int> cropyearlist;
		ObservableCollection<FilterAppliedProductsViewModel> copyProducts;
		bool selectAllProducts = true;

		public ChoosePlanAppliedCropzonesViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, int cropYear, CropPlanId planId, PlannerViewModel parent)
			: base(clientEndpoint, dispatcher, cropYear) {
			this.currentDataSourceId = ApplicationEnvironment.CurrentDataSourceId;
			this.selectedcropyear = 0;
			this.parent = parent;

			SummaryItems = new ObservableCollection<ApplicationLineItemViewModel>();

			var years = clientEndpoint.GetView<FieldTreeView>(new DataSourceId(this.currentDataSourceId));
			var yearItem = (years.HasValue && years.Value.CropYearList.ContainsKey(cropYear)) ? years.Value.CropYearList[cropYear] : new TreeViewYearItem();
			cropyearlist = years.Value.CropYearList.Keys.Where(x => x < cropYear).ToList();
			selectedcropyear = 0;
			foreach (int yr in cropyearlist) {
				if (yr > selectedcropyear) {
					selectedcropyear = yr;
				}
			}

            //this.UsePercentCoverage = true;

            SaveChangesCommand = new RelayCommand(SaveChanges, () => selectedcropyear > 0);
			CancelChangesCommand = new RelayCommand(CancelChanges);

			if (cropyearlist.Count == 0 || selectedcropyear == 0) {
				CancelChangesCommand.Execute(null);
			}
			ChangeCropYearForTree(selectedcropyear);
			SaveChangesCommand.RaiseCanExecuteChanged();
			//RaisePropertyChanged(() => SelectedCropYear);
		}

		public RelayCommand SaveChangesCommand { get; }
		public RelayCommand CancelChangesCommand { get; }

		public int CropYear => currentCropYearId.Id;

		public List<int> CropYearList {
			get {
				if (cropyearlist == null) {
					return new List<int>();
				}

				return cropyearlist.OrderByDescending(q => q).ToList();
			}
			set {
				cropyearlist = value;
				RaisePropertyChanged(() => CropYearList);
			}
		}

		public int SelectedCropYear {
			get { return selectedcropyear; }
			set {
				if (selectedcropyear == value) { return; }
				selectedcropyear = value;
				RaisePropertyChanged(() => SelectedCropYear);

				if (selectedcropyear == 0) { return; }
				SaveChangesCommand.RaiseCanExecuteChanged();
				ChangeCropYearForTree(selectedcropyear);
			}
		}

		void SaveChanges() {
			ObservableCollection<ApplicationLineItemViewModel> SummaryItemsSaved = new ObservableCollection<ApplicationLineItemViewModel>();
			foreach (var prod in copyProducts ?? new ObservableCollection<FilterAppliedProductsViewModel>()) {
				if (prod.IsProductSelectedForCopy) {
					SummaryItemsSaved.Add(prod.ProductDetails);
				}
			}

			if (SummaryItemsSaved.Count > 0) {
				parent.SetSumarryItems(SummaryItemsSaved);
			}
			Messenger.Default.Send(new HideOverlayMessage());
		}

		void CancelChanges() {
			Messenger.Default.Send(new HideOverlayMessage());
		}

		//protected override void OnFieldCheckedChanged(CropZoneTreeItemViewModel treeItem)
		//{
		//    if (treeItem.IsChecked.HasValue) {
		//        RefreshSummaryItems(treeItem);
		//    }
		//}

		AbstractTreeItemViewModel selectedTreeItem;
		public AbstractTreeItemViewModel SelectedTreeItem {
			get { return selectedTreeItem; }
			set {
				selectedTreeItem = value;
				RaisePropertyChanged(() => SelectedTreeItem);
				if (value == null) { return; }
				RefreshSummaryItems(value);
			}
		}

		public async void RefreshSummaryItems(AbstractTreeItemViewModel treeModel) {
			SummaryItems.Clear();
			copyProducts = new ObservableCollection<FilterAppliedProductsViewModel>();

			var data = clientEndpoint.GetView<CropZoneApplicationDataView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, selectedcropyear)).GetValue(new CropZoneApplicationDataView());
			var inventory = clientEndpoint.GetView<InventoryListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, selectedcropyear)).GetValue(new InventoryListView());

			IEnumerable<CropZoneApplicationDataItem> filtered = null;
			double area = 0;

			if (treeModel is CropZoneTreeItemViewModel) {
				filtered = data.Items.Where(x => x.CropZoneId == (CropZoneId)((CropZoneTreeItemViewModel)treeModel).Id).OrderByDescending(x => x.StartDate);
				area = ((CropZoneTreeItemViewModel)treeModel).Area.Value;
			}

			if (treeModel is FieldTreeItemViewModel) {
				filtered = data.Items.Where(x => x.FieldId == (FieldId)((FieldTreeItemViewModel)treeModel).Id).OrderByDescending(x => x.StartDate);
			}

			//CostSummary = new CostSummaryViewModel(area);
			Landdb.Services.IInventoryCalculationService inventoryService = new InventoryCalculationService();

			if (filtered != null) {
				filtered.ForEach(prod => {
					var appSummaryItem = new ApplicationLineItemViewModel(clientEndpoint, inventoryService, inventory, prod);
					SummaryItems.Add(appSummaryItem);
					copyProducts.Add(new FilterAppliedProductsViewModel(appSummaryItem));
					//var p = clientEndpoint.GetMasterlistService().GetProduct(x.ProductId);
					//if (p != null) {
					// TODO: Calculating cost summaries like this will have perf implications for large lists. 
					// Look to offload this to another thread in the future. -bs

					//TODO  The following code related to NaN is a bandaid just so the grower/support can fix their own data  -mh
					//if (appSummaryItem.TotalCost.ToString() == "NaN") { int aaaa = 0; }
					//if (p.ProductType == GlobalStrings.ProductType_Seed) {
					//    Varieties.Add(new VarietyItemViewModel(string.Format("{0} ({1})", p.Name, p.Manufacturer), x.StartDate));
					//    CostSummary.SeedTotalCost += appSummaryItem.TotalCost.ToString() == "NaN" ? 0 : (decimal)appSummaryItem.TotalCost;
					//}
					//if (p.ProductType == GlobalStrings.ProductType_Fertilizer) {
					//    CostSummary.FertilizerTotalCost += appSummaryItem.TotalCost.ToString() == "NaN" ? 0 : (decimal)appSummaryItem.TotalCost;
					//}
					//if (p.ProductType == GlobalStrings.ProductType_CropProtection) {
					//    CostSummary.CropProtectionTotalCost += appSummaryItem.TotalCost.ToString() == "NaN" ? 0 : (decimal)appSummaryItem.TotalCost;
					//}
					//if (p.ProductType == GlobalStrings.ProductType_Service) {
					//    CostSummary.ServicesTotalCost += appSummaryItem.TotalCost.ToString() == "NaN" ? 0 : (decimal)appSummaryItem.TotalCost;
					//}
					//}
				});
				//totalProductionCostByAverage = CostSummary.TotalCost;
				//productionCostPerAreaByAverage = CostSummary.CostPerArea;

				//if (SelectedPricingViewStrategy == PricingViewStrategy.InvoiceMatching) {
				//    // yes, this is a bit silly. Potential refactor idea: calculate both cost summaries up front, and switch them out based on selected view strategy.
				//    RecalculateCostSummaryByInvoice();
				//}
				//else if (SelectedPricingViewStrategy == PricingViewStrategy.SpecificCost) {
				//    RecalculateCostSummaryBySpecific();
				//}

				//HasInvoices = SummaryItems.Any(x => x.HasInvoiceCost);
				//HasSpecificCost = SummaryItems.Any(x => x.HasSpecificCost);
				//RaisePropertyChanged(() => HasInvoices");
				//RaisePropertyChanged(() => HasSpecificCost");
				//RaisePropertyChanged(() => ShowDeviationLine");
			}
			RaisePropertyChanged(() => CopyProducts);

			if (treeModel is CropZoneTreeItemViewModel) {
				var czDetails = clientEndpoint.GetView<CropZoneDetailsView>(((CropZoneTreeItemViewModel)treeModel).Id).GetValue(() => null);
				CropId cropId = null;
				if (czDetails != null) {
					cropId = czDetails.CropId;
				}

				var czModel = (CropZoneTreeItemViewModel)treeModel;

				// TODO: need cancellability
				//var analysis = await CalculateFertilizerUsage(filtered, ((CropZoneTreeItemViewModel)treeModel).Area, cropId);
				//FertilizerInformation = analysis.FertilizerFormulation;
				//analysis.ActiveIngredients.Sort(delegate(ActiveIngredientLineItemViewModel g1, ActiveIngredientLineItemViewModel g2) {
				//    return g1.PercentRemaining.CompareTo(g2.PercentRemaining);
				//});

				//ActiveIngredients = analysis.ActiveIngredients;
			}

			//RefreshYieldSummary(treeModel);

			//CostSummary.ProductionRevenue = CostSummary.YieldSummaryItems.Sum(x => x.GrowerRevenue) - CostSummary.TotalCost;
		}

		public ObservableCollection<ApplicationLineItemViewModel> SummaryItems { get; private set; }

		protected override void OnFieldCheckedChanged(CropZoneTreeItemViewModel treeItem) {
			if (treeItem.IsChecked.HasValue) {
				RefreshSummaryItems(treeItem);
			}
		}

		public FilterAppliedProductsViewModel SelectedProduct { get; set; }
		public ObservableCollection<FilterAppliedProductsViewModel> CopyProducts {
			get { return copyProducts; }
			set {
				copyProducts = value;
				RaisePropertyChanged(() => CopyProducts);
			}
		}

		public bool SelectAllProducts {
			get { return selectAllProducts; }
			set {
				selectAllProducts = value;
				if (CopyProducts != null && CopyProducts.Count > 0) {
					for (int i = 0; i < CopyProducts.Count; i++) {
						CopyProducts[i].IsProductSelectedForCopy = selectAllProducts;
					}
				}
				RaisePropertyChanged(() => CopyProducts);
				RaisePropertyChanged(() => SelectAllProducts);
			}
		}


		//public override void OnFieldCheckedChanged(CropZoneTreeItemViewModel treeItem) {
		//    if (treeItem is CropZoneTreeItemViewModel) {
		//        if (treeItem.IsChecked.HasValue && treeItem.IsChecked.Value && !SelectedCropZones.Any(x => x.Id == (treeItem.Id as CropZoneId))) {

		//        }

		//        if (treeItem.IsChecked.HasValue && !treeItem.IsChecked.Value) {

		//        }
		//    }

		//}

		//private void OnFieldCheckedChanged_internal(AbstractTreeItemViewModel treeItem) {
		//    if (treeItem is CropZoneTreeItemViewModel) {
		//        if (treeItem.IsChecked.HasValue && treeItem.IsChecked.Value && !SelectedCropZones.Any(x => x.Id == (treeItem.Id as CropZoneId))) {
		//            var mapItem = clientEndpoint.GetView<ItemMap>(treeItem.Id);
		//            string mapData = null;
		//            if (mapItem.HasValue && mapItem.Value.MostRecentMapItem != null) {
		//                mapData = mapItem.Value.MostRecentMapItem.MapData;
		//            }

		//            var cztivm = new CropZoneSelectionViewModel(treeItem as CropZoneTreeItemViewModel, mapData, clientEndpoint, () => { IsEditPopupOpen = false; }, (delta) => {
		//                TotalArea = TotalArea.Unit.GetMeasure(TotalArea.Value + delta);
		//                RaisePropertyChanged(() => TotalArea");
		//            });

		//            SelectedCropZones.Add(cztivm);

		//            dispatcher.BeginInvoke(new Action(() => {
		//                List<string> selectedMapData = new List<string>();
		//                foreach (var sCZ in SelectedCropZones) {
		//                    if (sCZ.MapData != null) {
		//                        selectedMapData.Add(sCZ.MapData);
		//                    }
		//                }
		//                BingMapsLayer = BingMapsUtility.GetLayerForMapData(selectedMapData.ToArray());
		//                RaisePropertyChanged(() => BingMapsLayer");

		//                try {
		//                    MapUpdated(this, e);
		//                }
		//                catch (Exception ex) { var error = ex.InnerException; }
		//            }));
		//            if (cztivm.SelectedArea.CanConvertTo(TotalArea.Unit)) {
		//                TotalArea += cztivm.SelectedArea;
		//            }
		//            else {
		//                log.Debug("Selected unit {0} is not compatible with total unit {1} in cropzone {2} ", cztivm.SelectedArea.Unit.Name, totalArea.Unit.Name, cztivm.Name);

		//            }
		//        }

		//        if (treeItem.IsChecked.HasValue && !treeItem.IsChecked.Value) {
		//            var found = SelectedCropZones.Where(x => x.Id == (CropZoneId)treeItem.Id).ToList();
		//            found.ForEach(x => {
		//                SelectedCropZones.Remove(x);

		//                dispatcher.BeginInvoke(new Action(() => {
		//                    List<string> selectedMapData = new List<string>();
		//                    foreach (var sCZ in SelectedCropZones) {
		//                        if (sCZ.MapData != null) {
		//                            selectedMapData.Add(sCZ.MapData);
		//                        }
		//                    }
		//                    BingMapsLayer = BingMapsUtility.GetLayerForMapData(selectedMapData.ToArray());
		//                    RaisePropertyChanged(() => BingMapsLayer");
		//                    try {
		//                        MapUpdated(this, e);
		//                    }
		//                    catch (Exception ex) { var error = ex.InnerException; }
		//                }));

		//                TotalArea -= x.SelectedArea;
		//            });
		//        }

		//        RaisePropertyChanged(() => FieldCount");

		//        OnFieldCheckedChanged(treeItem as CropZoneTreeItemViewModel);
		//    }
		//}

	}
}
