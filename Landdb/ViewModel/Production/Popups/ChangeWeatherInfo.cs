﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Map;
using Landdb.Infrastructure.Messages;
using Landdb.Resources;
using Landdb.ViewModel.Secondary.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Client.Infrastructure.DisplayItems;

namespace Landdb.ViewModel.Production.Popups
{
    public class ChangeWeatherInfo : ViewModelBase {
        private readonly IClientEndpoint _endpoint;
        private readonly ApplicationView _view;
        private Dispatcher _dispatch;
        private readonly ClearAgWeatherConditionsViewModel _conditionsModel;

        public ChangeWeatherInfo(IClientEndpoint endpoint, Dispatcher dispatch, ApplicationView view) {
            EnvironmentConditions = new EnvironmentConditions();
            _view = view;
            _endpoint = endpoint;
            _dispatch = dispatch;
            ResetWeather = false;

            _conditionsModel = new ClearAgWeatherConditionsViewModel(endpoint, dispatch, view.CropYear);
            UpdateFromProjection(view);
            Update = new RelayCommand(UpdateWeather);
            Cancel = new RelayCommand(CancelWeather);
        }

        public ICommand Update { get; }
        public ICommand Cancel { get; }

        public EnvironmentConditions EnvironmentConditions { get; set; }

        public bool IsChecked { get; set; }

        public ClearAgWeatherConditionsViewModel ConditionsModel => _conditionsModel;

        public bool ResetWeather { get; set; }

        /// <summary>
        /// Loads data into the popup that appears when you click on the weather tile in Application Details View to edit the weather info.
        /// </summary>
        /// <param name="view"></param>
        private void UpdateFromProjection(ApplicationView view) {
            if (view.Conditions != null) {
                EnvironmentConditions.Temperature = view.Conditions.TemperatureValue;
                EnvironmentConditions.TemperatureUnit = new TemperatureDisplayItem(view.Conditions.TemperatureUnit);
                EnvironmentConditions.WindSpeed = view.Conditions.WindSpeedValue;
                EnvironmentConditions.WindSpeedUnit = 
                    string.IsNullOrEmpty(view.Conditions.WindSpeedUnit)? null : new SpeedDisplayItem(view.Conditions.WindSpeedUnit);
                var windType = ClearAgWindDirectionCategorizer.Categorize(view.Conditions.WindDirection);
                EnvironmentConditions.WindDirection = new ClearAgWindDirectionDisplayItem(windType);
                EnvironmentConditions.RelativeHumidity = view.Conditions.HumidityPercent;
                EnvironmentConditions.SkyCondition = new ClearAgSkyCoverDisplayItem(view.Conditions.SkyCondition);
                EnvironmentConditions.SoilMoistureDescriptor = new SoilMoistureDisplayItem(view.Conditions.SoilMoisture);
            } else {
                EnvironmentConditions.Clear();
                EnvironmentConditions.TemperatureUnit = null;
                EnvironmentConditions.WindSpeedUnit = null;
                EnvironmentConditions.SoilMoistureDescriptor = null;
            }

            if ((!EnvironmentConditions.Temperature.HasValue || EnvironmentConditions.Temperature == 0) && 
                (!EnvironmentConditions.WindSpeed.HasValue || EnvironmentConditions.WindSpeed == 0)) {
                ResetWeather = true;
                UpdateFromStation(view.StartDateTime);
            }

        }

        /// <summary>
        /// Updates the weather info if anything is changed in the change weather popup. 
        /// </summary>
        /// <returns></returns>
        private bool IfInfoChange() {
            if (_view.Conditions != null) {
                return EnvironmentConditions.Temperature != _view.Conditions.TemperatureValue || 
                       !EnvironmentConditions.TemperatureUnit.Equals(_view.Conditions.TemperatureUnit) || 
                       EnvironmentConditions.WindSpeed != _view.Conditions.WindSpeedValue ||
                       !EnvironmentConditions.WindSpeedUnit.Equals(_view.Conditions.WindSpeedUnit) || 
                       !EnvironmentConditions.WindDirection.DisplayText.Equals(_view.Conditions.WindDirection) || 
                       EnvironmentConditions.RelativeHumidity != _view.Conditions.HumidityPercent || 
                       EnvironmentConditions.SkyCondition != new ClearAgSkyCoverDisplayItem(_view.Conditions.SkyCondition) || 
                       !EnvironmentConditions.SoilMoistureDescriptor.Equals(_view.Conditions.SoilMoisture);
            } else {
                return EnvironmentConditions.Temperature.HasValue || 
                       EnvironmentConditions.WindSpeed.HasValue || 
                       EnvironmentConditions.WindDirection != null || 
                       EnvironmentConditions.RelativeHumidity.HasValue || 
                       EnvironmentConditions.SkyCondition != null ||
                       EnvironmentConditions.SoilMoistureDescriptor != null;
            }
        }


        private void UpdateWeather() {
            bool infohaschanged = IfInfoChange();

            if (IsChecked) {
                EnvironmentConditions.Digest(_conditionsModel);
                ApplicationConditions conditions = EnvironmentConditions.CreateApplicationConditionsFromCurrentWeatherInfo(_view);
                ChangeApplicationConditions conditionsCommand = new ChangeApplicationConditions(_view.Id, _endpoint.GenerateNewMetadata(), conditions);
                _endpoint.SendOne(conditionsCommand);

                Messenger.Default.Send(new HidePopupMessage());
                return;
            }

            if (infohaschanged) {
                ApplicationConditions conditions = EnvironmentConditions.CreateApplicationConditionsFromCurrentWeatherInfo(_view);
                ChangeApplicationConditions conditionsCommand = new ChangeApplicationConditions(_view.Id, _endpoint.GenerateNewMetadata(), conditions);
                _endpoint.SendOne(conditionsCommand);

                Messenger.Default.Send(new HidePopupMessage());
            }
        }

        private void UpdateFromStation(DateTime updatedDateTime) {
            if (_view.CropZones.Any() && _conditionsModel != null) {
                var czs = _view.CropZones;

                //get center lat lon of the included fields then set conditions lookuppoint
                //string mapData = null;
                List<string> mapShapeStrings = new List<string>();
                foreach (var cz in czs) {
                    var mapItem = _endpoint.GetView<ItemMap>(cz.Id);

                    if (mapItem.HasValue && mapItem.Value.MostRecentMapItem != null && mapItem.Value.MostRecentMapItem.MapData != "MULTIPOLYGON EMPTY") {
                        var mapString = mapItem.Value.MostRecentMapItem.MapData;
                        mapString = mapString.Substring(13, (mapString.Length - 14));
                        mapShapeStrings.Add(mapString);
                        //mapData += mapItem.Value.MostRecentMapItem.MapData;
                    }
                }


                if (mapShapeStrings.Any()) {
                    var multiString = string.Join(",", mapShapeStrings.ToArray());
                    var multiPolygonString = $"Multipolygon({multiString})";
                    var shape = ThinkGeo.MapSuite.Core.BaseShape.CreateShapeFromWellKnownData(multiPolygonString);
                    var center = shape.GetCenterPoint();
                    _conditionsModel.IncludeWeatherDataByDefault = true;
                    _conditionsModel.GeoLookupPoint = center;
                    //conditionsModel.GeoLookupPoint = center; // string.Format("{0},{1}", center.Y.ToString("n1"), center.X.ToString("n1"));
                }

                _conditionsModel.ApplicationStartDate = updatedDateTime;
            }

            //Temperature = (decimal)conditionsModel.Temperature;
            //TemperatureUnit = string.IsNullOrEmpty(conditionsModel.TempUnit) ? "F" : conditionsModel.TempUnit;
            //WindSpeed = (decimal)conditionsModel.WindSpeed;
            //WindUnit = string.IsNullOrEmpty(conditionsModel.SpeedUnit) ? "mph" : conditionsModel.SpeedUnit; ;
            //WindDirection = conditionsModel.WindDirection;
            //WindText = conditionsModel.WindSpeed.HasValue ? conditionsModel.WindSpeed.Value.ToString("N2") + " " + WindUnit : string.Empty;
            //Humidity = (decimal)conditionsModel.Humidity;
            //SkyCondition = conditionsModel.SkyCondition;
            //SoilMoisture = conditionsModel.SoilMoisture;

            //RaisePropertyChanged("Temperature");
            //RaisePropertyChanged("Humidity");
            //RaisePropertyChanged("WindSpeed");
            //RaisePropertyChanged("WindText");
            //RaisePropertyChanged("WindDirection");
            //RaisePropertyChanged("SkyCondition");
            //RaisePropertyChanged("BufferWidth");
            //RaisePropertyChanged("BufferWidthUnit");
            //RaisePropertyChanged("SelectedBufferTolerance");

            ////ResetWeather = true;
            //RaisePropertyChanged("ResetWeather");
            ////}
        }

        private static void CancelWeather() {
            Messenger.Default.Send(new HidePopupMessage());
        }
    }
}