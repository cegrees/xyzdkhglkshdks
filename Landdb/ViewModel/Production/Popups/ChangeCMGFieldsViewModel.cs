﻿using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Map;
using Landdb.Domain.ReadModels.Shared;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Fields;
using Landdb.ViewModel.Maps;
using Landdb.ViewModel.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;

namespace Landdb.ViewModel.Production.Popups
{
    public class ChangeCMGFieldsViewModel : BaseFieldSelectionViewModel
    {
        IClientEndpoint clientEndpoint;
        List<IncludedCropZone> initialCropZones;
        MapsPageViewModel parent;
        Guid currentDataSourceId;
        int currentCropYear;
        bool displayAllCropzones = false;
        Dictionary<string, string> unselectedFeatures = new Dictionary<string, string>();
        Dictionary<string, string> chosenFeatures = new Dictionary<string, string>();


        public ChangeCMGFieldsViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, int cropYear, IList<MapItem> mappedcropZones, MapsPageViewModel parent)
            : base(clientEndpoint, dispatcher, cropYear)
        {
            this.clientEndpoint = clientEndpoint;
            this.currentDataSourceId = ApplicationEnvironment.CurrentDataSourceId;
            this.currentCropYear = ApplicationEnvironment.CurrentCropYear;
            this.initialCropZones = new List<IncludedCropZone>();
            unselectedFeatures = new Dictionary<string, string>();
            chosenFeatures = new Dictionary<string, string>();
            Enable = false;
            displayAllCropzones = false;

            var q = from t in mappedcropZones
                    select new IncludedCropZone() { FarmId = new FarmId(this.currentDataSourceId, t.FarmId), FieldId = new FieldId(this.currentDataSourceId, t.FieldId), Id = new CropZoneId(this.currentDataSourceId, new Guid(t.CropZoneId.ToString())), Name = t.Name };

            this.initialCropZones = q.ToList();
            var initialCzIds = from icz in initialCropZones select icz.Id;
            this.parent = parent;
            //if (RootTreeItemModels.Any())
            //{
            //    if (RootTreeItemModels.First() is GrowerTreeItemViewModel)
            //    {
            //        var toCheck = from farm in RootTreeItemModels.First().Children
            //                      from field in farm.Children
            //                      from cz in field.Children
            //                      where initialCzIds.Contains(cz.Id)
            //                      select cz;
            //        toCheck.ForEach(x => x.IsChecked = true);
            //    }
            //    else
            //    {
            //        var toCheck = from crop in RootTreeItemModels.First().Children
            //                      from farm in crop.Children
            //                      from field in farm.Children
            //                      from cz in field.Children
            //                      where initialCzIds.Contains(cz.Id)
            //                      select cz;
            //        toCheck.ForEach(x => x.IsChecked = true);
            //    }
            //}

            //foreach (var f in SelectedCropZones) {
            //    f.ChangeSelectedArea(initialCropZones.Where(x => x.Id == f.Id).FirstOrDefault().Area);
            //}

            SaveChangesCommand = new RelayCommand(SaveChanges);
            CancelChangesCommand = new RelayCommand(CancelChanges);
        }

        public ICommand SaveChangesCommand { get; private set; }
        public ICommand CancelChangesCommand { get; private set; }

        public bool Enable { get; set; }

        public bool DisplayAllCropzones
        {
            get { return displayAllCropzones; }
            set
            {
                displayAllCropzones = value;
                RaisePropertyChanged("DisplayAllCropzones");
            }
        }
        public Dictionary<string, string> UnselectedFeatures
        {
            get { return unselectedFeatures; }
            set { unselectedFeatures = value; }
        }
        public Dictionary<string, string> ChosenFeatures
        {
            get { return chosenFeatures; }
            set { chosenFeatures = value; }
        }

        void SaveChanges()
        {
            var tree = clientEndpoint.GetView<CropTreeView>(new CropYearId(this.currentDataSourceId, this.currentCropYear)).GetValue(new CropTreeView());
            var q = from c in tree.Crops
                    from f in c.Farms
                    from fi in f.Fields
                    from cz in fi.CropZones
                    let fieldname = fi.Name
                    select new { f.FarmId, fi.FieldId, fieldname, cz.CropZoneId, cz.Name };

            chosenFeatures = new Dictionary<string, string>();
            IList<MapItem> czmaps = new List<MapItem>();
            IList<MapItem> czmapsUnselected = new List<MapItem>();
            IList<MapItem> czmapsSelected = new List<MapItem>();
            foreach (var item in q) {
                var maps = clientEndpoint.GetView<ItemMap>(item.CropZoneId);
                try {
                    if (maps.HasValue) {
                        MapItem mi = new MapItem();
                        mi.FarmId = item.FarmId.Id;
                        mi.FieldId = item.FieldId.Id;
                        mi.CropZoneId = item.CropZoneId.Id;
                        mi.Name = item.Name;
                        if (maps.Value.MostRecentMapItem != null) {
                            mi.MapData = maps.Value.MostRecentMapItem.MapData;
                            mi.MapDataType = maps.Value.MostRecentMapItem.DataType;
                        }
                        czmapsUnselected.Add(mi);
                        czmapsSelected.Add(mi);
                        var found = SelectedCropZones.Where(x => x.Id == item.CropZoneId).ToList();
                        found.ForEach(x => {
                            czmaps.Add(mi);
                            if (!chosenFeatures.ContainsKey(mi.FieldId.ToString()))
                            {
                                chosenFeatures.Add(mi.FieldId.ToString(), mi.FieldId.ToString());
                            }
                            if (!chosenFeatures.ContainsKey(mi.CropZoneId.ToString()))
                            {
                                chosenFeatures.Add(mi.CropZoneId.ToString(), mi.CropZoneId.ToString());
                            }
                        });

                    }
                }
                finally { }
            }
            foreach (var czshape in czmaps) {
                if (czmapsUnselected.Contains(czshape)) {
                    czmapsUnselected.Remove(czshape);
                }
            }
            unselectedFeatures = new Dictionary<string, string>();
            foreach (var unsel in czmapsUnselected) {
                if(!unselectedFeatures.ContainsKey(unsel.FieldId.ToString())) {
                    unselectedFeatures.Add(unsel.FieldId.ToString(), unsel.FieldId.ToString());
                }
                if (!unselectedFeatures.ContainsKey(unsel.CropZoneId.ToString())) {
                    unselectedFeatures.Add(unsel.CropZoneId.ToString(), unsel.CropZoneId.ToString());
                }
            }
            if (displayAllCropzones) {
                parent.DisplayCropzones = true;
                parent.ChosenFeatures = chosenFeatures;
                parent.UnselectedFeatures = unselectedFeatures;
                parent.CropZoneLayer = czmapsSelected;
            }
            else {
                parent.ChosenFeatures = new Dictionary<string, string>();
                parent.UnselectedFeatures = new Dictionary<string, string>();
                parent.CropZoneLayer = czmaps;
            }
            //parent.UnselectedFeatures = unselectedFeatures;
            //parent.CropZoneLayer = czmaps;
            Messenger.Default.Send<HideOverlayMessage>(new HideOverlayMessage());
        }

        void CancelChanges()
        {
            Messenger.Default.Send<HideOverlayMessage>(new HideOverlayMessage());
        }

        protected override void OnFieldCheckedChanged(CropZoneTreeItemViewModel treeItem)
        {

        }
    }
}
