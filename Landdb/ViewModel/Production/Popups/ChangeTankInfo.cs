﻿using AgC.UnitConversion;
using AgC.UnitConversion.Volume;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Recommendation;
using Landdb.Domain.ReadModels.WorkOrder;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using System.Windows.Input;
using Landdb.Client.Localization;

namespace Landdb.ViewModel.Production.Popups
{
    public class ChangeTankInfo : ViewModelBase {
        private readonly IClientEndpoint _endpoint;
        private readonly ApplicationView _view;
        private readonly WorkOrderView _woView;
        private readonly RecommendationView _recView;

        private bool _isTankApplication;

        private static Measure GetLocalizedVolumeMeasure(double tankVolume) { return DataSourceCultureInformation.UsesImperialUnits ? Gallon.Self.GetMeasure(tankVolume) : Liter.Self.GetMeasure(tankVolume); }
        private readonly Measure _totalArea;

        public ChangeTankInfo(IClientEndpoint endpoint, ApplicationView view, Measure totalArea) {
            _endpoint = endpoint;
            _view = view;
            _totalArea = totalArea;

            if (view.TankInformation != null) {
                TankSizeText = view.TankInformation.TankSizeValue.ToString();
                var lockValue = view.TankInformation.TankValueTypeEnteredByUser;
                switch (lockValue) {
                    case "CarrierPerArea":
                        CarrierPerAreaText = view.TankInformation.CarrierPerAreaValue.ToString();
                        break;
                    case "CarrierPerAreaValue":
                        CarrierPerAreaText = view.TankInformation.CarrierPerAreaValue.ToString();
                        break;
                    case "CarrierApplied":
                        CarrierAppliedText = view.TankInformation.TotalCarrierValue.ToString();
                        break;
                    case "TankCount":
                        TankCountText = view.TankInformation.TankCount.ToString();
                        break;
                    default:
                        CarrierAppliedText = view.TankInformation.CarrierPerAreaValue.ToString();
                        break;
                }
            }

            Update = new RelayCommand(UpdateTank);
            Cancel = new RelayCommand(CancelTank);
        }

        public ChangeTankInfo(IClientEndpoint endpoint, WorkOrderView woView, Measure totalArea) {
            _endpoint = endpoint;
            _woView = woView;
            _totalArea = totalArea;

            if (_woView.TankInformation != null) {
                TankSizeText = _woView.TankInformation.TankSizeValue.ToString();

                var lockValue = woView.TankInformation.TankValueTypeEnteredByUser;
                switch (lockValue) {
                    case "CarrierPerArea":
                        CarrierPerAreaText = woView.TankInformation.CarrierPerAreaValue.ToString();
                        break;
                    case "CarrierApplied":
                        CarrierAppliedText = woView.TankInformation.TotalCarrierValue.ToString();
                        break;
                    case "TankCount":
                        TankCountText = woView.TankInformation.TankCount.ToString();
                        break;
                    default:
                        CarrierPerAreaText = woView.TankInformation.CarrierPerAreaValue.ToString();
                        break;
                }
            }

            Update = new RelayCommand(UpdateTank);
            Cancel = new RelayCommand(CancelTank);
        }

        public ChangeTankInfo(IClientEndpoint endpoint, RecommendationView recView, Measure totalArea) {
            _endpoint = endpoint;
            _recView = recView;
            _totalArea = totalArea;

            if (_recView.TankInformation != null) {
                TankSizeText = _recView.TankInformation.TankSizeValue.ToString();
                TankCountText = _recView.TankInformation.TankCount.ToString();
            }

            Update = new RelayCommand(UpdateTank);
            Cancel = new RelayCommand(CancelTank);
        }

        public ICommand Update { get; set; }
        public ICommand Cancel { get; set; }

        public Measure TankSize { get; private set; } = GetLocalizedVolumeMeasure(0);

        public Measure CarrierApplied { get; private set; } = GetLocalizedVolumeMeasure(0);

        public Measure CarrierPerArea { get; private set; } = GetLocalizedVolumeMeasure(0);

        public decimal TankCount { get; private set; }


        public string TankSizeText {
            get => TankSize.FullDisplay;
            set {
                if (string.IsNullOrWhiteSpace(value)) { return; }

                if (double.TryParse(value, out var measureValue)) {
                    TankSize = GetLocalizedVolumeMeasure(measureValue);
                    if (TankSize.Value > 0) {
                        TankCount = (decimal)(CarrierApplied.Value / TankSize.Value);
                    } else {
                        TankCount = 0;
                    }
                    RaisePropertyChanged("TankCountText");
                    RaisePropertyChanged("TankSizeText");

                    SetTankUseStatus();
                }
            }
        }

        public string CarrierAppliedText {
            get => CarrierApplied.FullDisplay;
            set {
                if (string.IsNullOrWhiteSpace(value)) { return; }

                if (double.TryParse(value, out var measureValue)) {
                    CarrierApplied = GetLocalizedVolumeMeasure(measureValue);
                    if (_totalArea != null && _totalArea.Value > 0) {
                        CarrierPerArea = GetLocalizedVolumeMeasure(measureValue / _totalArea.Value);
                    } else {
                        CarrierPerArea = GetLocalizedVolumeMeasure(0);
                    }
                    if (TankSize.Value > 0) {
                        TankCount = (decimal)(CarrierApplied.Value / TankSize.Value);
                    } else {
                        TankCount = 0;
                    }
                    RaisePropertyChanged("TankCountText");
                    RaisePropertyChanged("CarrierAppliedText");
                    RaisePropertyChanged("CarrierPerAreaText");

                    SetTankUseStatus();
                }

            }
        }

        public string CarrierPerAreaText {
            get => CarrierPerArea.FullDisplay;
            set {
                if (string.IsNullOrWhiteSpace(value)) { return; }

                if (double.TryParse(value, out var measureValue)) {
                    CarrierPerArea = GetLocalizedVolumeMeasure(measureValue);
                    CarrierApplied = GetLocalizedVolumeMeasure(measureValue * _totalArea.Value);
                    if (TankSize.Value > 0) {
                        TankCount = (decimal)(CarrierApplied.Value / TankSize.Value);
                    } else {
                        TankCount = 0;
                    }
                    RaisePropertyChanged("TankCountText");
                    RaisePropertyChanged("CarrierAppliedText");
                    RaisePropertyChanged("CarrierPerAreaText");

                    SetTankUseStatus();
                }
            }
        }

        public string TankCountText {
            get => TankCount.ToString("N2");
            set {
                if (string.IsNullOrWhiteSpace(value)) { return; }

                if (decimal.TryParse(value, out var measureValue)) {
                    TankCount = measureValue;
                    CarrierApplied = GetLocalizedVolumeMeasure(TankSize.Value * (double)TankCount);
                    if (_totalArea == null || _totalArea.Value <= 0) {
                        CarrierPerArea = GetLocalizedVolumeMeasure(0);
                    } else {
                        CarrierPerArea = GetLocalizedVolumeMeasure(CarrierApplied.Value / _totalArea.Value);
                    }
                    RaisePropertyChanged("TankCountText");
                    RaisePropertyChanged("CarrierAppliedText");
                    RaisePropertyChanged("CarrierPerAreaText");

                    SetTankUseStatus();
                }
            }
        }

        public void SetTankInformation(ApplicationTankInformation tankInfo) {
            if (tankInfo != null) {
                TankSize = UnitFactory.GetUnitByName(tankInfo.TankSizeUnit).GetMeasure((double)tankInfo.TankSizeValue);
                var totalCarrier = UnitFactory.GetUnitByName(tankInfo.TotalCarrierUnit).GetMeasure((double)tankInfo.TotalCarrierValue);
                CarrierAppliedText = totalCarrier.Value.ToString(); // This is a bit of a hack, I admit...

                RaisePropertyChanged("TankSize");
                RaisePropertyChanged("CarrierApplied");

                RaisePropertyChanged("TankCountText");
                RaisePropertyChanged("CarrierAppliedText");
                RaisePropertyChanged("CarrierPerAreaText");

                SetTankUseStatus();
            }
        }

        public bool IsTankApplication {
            get => _isTankApplication;
            set {
                _isTankApplication = value;
                RaisePropertyChanged("IsTankApplication");
                RaisePropertyChanged("TankInformationDisplayText");
            }
        }

        public void SetTankUseStatus() {
            if (CarrierApplied == null || CarrierApplied.Value <= 0) {
                IsTankApplication = false;
            } else {
                IsTankApplication = true;
            }

            RaisePropertyChanged("TankInformationDisplayText");
        }

        public void UpdateTank() {
            if ((_view != null && _view.TankInformation == null) || (_view != null && (TankSize.Value != (double)_view.TankInformation.TankSizeValue || CarrierPerArea.Value != (double)_view.TankInformation.CarrierPerAreaValue || _view.TankInformation.TankCount != TankCount || _view.TankInformation.TotalCarrierValue != (decimal)CarrierApplied.Value))) {
                var tankCommand = new ChangeApplicationTankInformation(_view.Id, _endpoint.GenerateNewMetadata(), (decimal)TankSize.Value, TankSize.Unit.Name, (decimal)CarrierPerArea.Value, CarrierPerArea.Unit.Name);
                _endpoint.SendOne(tankCommand);
            } else if ((_woView != null && _woView.TankInformation == null) || (_woView != null && (TankSize.Value != (double)_woView.TankInformation.TankSizeValue || CarrierPerArea.Value != (double)_woView.TankInformation.CarrierPerAreaValue || _woView.TankInformation.TankCount != TankCount || _woView.TankInformation.TotalCarrierValue != (decimal)CarrierApplied.Value))) {
                var woTankCommand = new ChangeWorkOrderTankInformation(_woView.Id, _endpoint.GenerateNewMetadata(), (decimal)TankSize.Value, TankSize.Unit.Name, (decimal)CarrierPerArea.Value, CarrierPerArea.Unit.Name);
                _endpoint.SendOne(woTankCommand);
            } else if ((_recView != null && _recView.TankInformation == null) || (_recView != null && (TankSize.Value != (double)_recView.TankInformation.TankSizeValue || TankCount != _recView.TankInformation.TankCount || CarrierApplied.Value != (double)_recView.TankInformation.TotalCarrierValue))) {
                var recTankCommand = new ChangeRecommendationTankInformation(_recView.Id, _endpoint.GenerateNewMetadata(), (decimal)TankSize.Value, TankSize.Unit.Name, (decimal)CarrierPerArea.Value, CarrierPerArea.Unit.Name);
                _endpoint.SendOne(recTankCommand);
            }

            Messenger.Default.Send(new HidePopupMessage());
        }

        private static void CancelTank() {
            Messenger.Default.Send(new HidePopupMessage());
        }

        internal ApplicationTankInformation GetApplicationTankInformation() {
            return new ApplicationTankInformation((decimal)TankSize.Value, TankSize.Unit.Name, TankCount, (decimal)CarrierPerArea.Value, CarrierPerArea.Unit.Name, (decimal)CarrierApplied.Value, CarrierApplied.Unit.Name, "CarrierPerArea");
        }
    }
}
