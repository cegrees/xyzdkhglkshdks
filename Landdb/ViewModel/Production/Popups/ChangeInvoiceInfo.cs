﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Company;
using Landdb.Domain.ReadModels.Invoice;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Landdb.ViewModel.Production.Invoices;

namespace Landdb.ViewModel.Production.Popups
{
    public class ChangeInvoiceInfo : ViewModelBase
    {
        IClientEndpoint endpoint;
        InvoiceView view;

        CompanyListItem vendor;
        InvoiceDetailsViewModel parent;

        public ChangeInvoiceInfo(IClientEndpoint endpoint, InvoiceView view, InvoiceDetailsViewModel parent)
        {
            this.endpoint = endpoint;
            this.view = view;
            this.parent = parent;

            UpdateCommand = new RelayCommand(this.UpdateInfo);
            CancelCommand = new RelayCommand(this.CancelInfo);

            DataSourceId dsID = new DataSourceId(ApplicationEnvironment.CurrentDataSourceId);
			var currentCropYear = ApplicationEnvironment.CurrentCropYear;

            // Get companies
            var companyListView = endpoint.GetView<CompanyListView>(dsID).GetValue(new CompanyListView());
			var sortedFilteredCompanies = from c in companyListView.Companies
										  where c.IsActiveInCropYear(currentCropYear)
										  orderby c.Name
										  select c;

			if (sortedFilteredCompanies.Any()) {
				Vendors = sortedFilteredCompanies.ToList();
				Vendor = string.IsNullOrEmpty(view.Vendor) ? null : Vendors.Where(x => x.Name == view.Vendor).FirstOrDefault();
				RaisePropertyChanged("Vendors");
				RaisePropertyChanged("Vendor");
			}

            InvoiceDate = view.InvoiceDate;
            InvoiceDueDate = view.InvoiceDueDate;
            RaisePropertyChanged("InvoiceDate");
        }

        public ICommand UpdateCommand { get; set; }
        public ICommand CancelCommand { get; set; }

        public DateTime InvoiceDate { get; set; }
        public DateTime? InvoiceDueDate { get; set; }
        public string VendorName { get; set; }
        public List<CompanyListItem> Vendors { get; set; }
        public CompanyListItem Vendor
        {
            get
            {
                return vendor;
            }
            set
            {
                vendor = value;
                VendorName = vendor != null ? vendor.Name : string.Empty;
            }
        }

        void UpdateInfo()
        {
            if (InvoiceDate != view.InvoiceDate)
            {
                ChangeInvoiceDate invoiceCommand = new ChangeInvoiceDate(view.Id, endpoint.GenerateNewMetadata(), InvoiceDate);
                endpoint.SendOne(invoiceCommand);
                parent.InvoiceDate = InvoiceDate;
            }

            if (InvoiceDueDate != view.InvoiceDueDate)
            {
                ChangeInvoiceDueDate invoiceDueDateCommand = new ChangeInvoiceDueDate(view.Id, endpoint.GenerateNewMetadata(), InvoiceDueDate);
                endpoint.SendOne(invoiceDueDateCommand);
                
            }

            if (view.Vendor != null && VendorName != view.Vendor)
            {
                ChangeInvoiceVendor vendorCommand = new ChangeInvoiceVendor(view.Id, endpoint.GenerateNewMetadata(), VendorName);
                endpoint.SendOne(vendorCommand);
                parent.Vendor = VendorName;
            }

            Messenger.Default.Send(new HidePopupMessage());
        }

        void CancelInfo()
        {
            Messenger.Default.Send(new HidePopupMessage());
        }
    }
}
