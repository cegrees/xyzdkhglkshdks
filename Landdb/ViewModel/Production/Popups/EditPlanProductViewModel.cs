﻿using AgC.UnitConversion;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Domain.ReadModels.Plan;
using Landdb.Domain.ReadModels.Product;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ValueObjects;
using Landdb.ViewModel.Secondary.Application;
using Landdb.ViewModel.Shared;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Landdb.Resources;
using NLog;

namespace Landdb.ViewModel.Production.Popups {
	public class EditPlanProductViewModel : ValidationViewModelBase {

		readonly IClientEndpoint clientEndpoint;
        Logger log = LogManager.GetCurrentClassLogger();

        Guid trackingId;
		ProductListItemDetails selectedProduct;
		decimal rateValue;
		UnitPerAreaHelper rateUnitHelper;
		decimal costValuePerUnit; // TODO: Make this a Currency class when we have a good one built.
		IUnit costUnit;
		string costCurrency = "USD"; // TODO: When a Currency class is available, make this value compatible.
		decimal totalCost = 0; // TODO: Make this a Currency class when we have a good one built.
		Measure totalProduct;
		decimal productAreaPercent = 1;
		Measure productArea;
		PriceItem selectedPriceItem;
		string selectedPriceText;
		InventoryListItem currentInventoryItem;
		InventoryListView inventory;
		int appCnt;

		bool isProductListVisible = false;
		bool isCreateProductPopupOpen = false;

		Measure totalArea;
		DateTime? targetDate;
		TimingEvent timingEvent;
		string timingEventTag;
		string selectedTimingEventTag;
		List<string> timingEventTags;
		Dictionary<string, List<string>> availableTimingEventTags;
		Dictionary<string, List<string>> tempTimingEventTags;
		Dictionary<string, List<string>> mergedTimingEventTags;
		ProductDetails details;
		PlanView view;
		PlanDetailsViewModel parent;

		public EditPlanProductViewModel(IClientEndpoint endpoint, ProductDetails details, PlanView plan, bool AddProduct, PlanDetailsViewModel parent, Dictionary<string, List<string>> availableTimingEventTags) {
			this.view = plan;
			this.clientEndpoint = endpoint;
			this.parent = parent;
			this.availableTimingEventTags = availableTimingEventTags;
			AllowedUnits = new ObservableCollection<UnitPerAreaHelper>();
			PriceItems = new ObservableCollection<PriceItem>();
			this.AddProduct = AddProduct;
			var masterlist = clientEndpoint.GetMasterlistService();
			var masterlistProd = details != null ? masterlist.GetProduct(details.ID) : null;

			Products = new ObservableCollection<ProductListItemDetails>();

			Task.Run(() => { BuildProductList(); });

			if (AddProduct) {
				Task.Run(() => { BuildProductList(); });
				inventory = endpoint.GetView<InventoryListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear)).GetValue(new InventoryListView());
				trackingId = Guid.NewGuid();
				totalArea = plan.EstimatedArea;
				ProductArea = totalArea;
				AllowEdit = true;
				TargetDate = null; // set to null. reason: https://fogbugz.agconnections.com/fogbugz/default.asp?26358
				appCnt = 1;
				ProductAreaPercent = 1;
                ProductSearchText = string.Empty;
                RaisePropertyChanged(() => ProductSearchText);
            } else {
				this.details = details;

				var displayRate = DisplayMeasureConverter.ConvertToRateDisplayMeasure(details.ID, (decimal)details.RateMeasure.Value, details.RateMeasure.Unit.Name, masterlistProd.Density, clientEndpoint, ApplicationEnvironment.CurrentDataSourceId);
				var displayTotal = DisplayMeasureConverter.ConvertToTotalDisplayMeasure(details.ID, (decimal)details.TotalProduct.Value, details.TotalProduct.Unit.Name, masterlistProd.Density, clientEndpoint, ApplicationEnvironment.CurrentDataSourceId);

				AllowEdit = false;
				totalArea = plan.EstimatedArea;
				ProductArea = totalArea;

                //var prodDSId = details.ID.DataSourceId == GlobalIdentifiers.GetMasterlistDataSourceId() ? ApplicationEnvironment.CurrentDataSourceId : GlobalIdentifiers.GetMasterlistDataSourceId();
                //Guid dsId = masterlistProd != null ? details.ID.DataSourceId : GlobalIdentifiers.GetMasterlistDataSourceId();
                //WATCH :: HAVE HAD ISSUES IN THE PAST WHERE THE ID WE RECIEVED FROM DETAILS WAS INCORRECTLY FORMATTED ... BE SURE WE ARE RECIEVEING THE PROPER DATASOURCE ID
				SelectedProduct = new ProductListItemDetails(masterlistProd, details.ID.DataSourceId, Strings.UsedThisYear_Text, (short)0);
				RateValue = (decimal)displayRate.Value;
				SelectedRateUnitHelper = new UnitPerAreaHelper(displayRate.Unit, plan.EstimatedArea.Unit);
				//SelectedRateUnitHelper.Unit = details.RateMeasure.Unit;
				SelectedPriceText = details.CostPerUnitValue.ToString();
				TotalProduct = displayTotal;
				ProductArea = totalArea;
				TimingEvent = TimingEvents.FirstOrDefault(x => x.Name == details.TimingEvent);
                TimingEventTag = !string.IsNullOrEmpty(details.TimingEventTag) ? details.TimingEventTag : string.Empty;
				TargetDate = details.TargetDate;
				ApplicationCount = details.ApplicationCount;

				if (details.ProductAreaPercent == 0) {
					ProductAreaPercent = 1;
				} else {
					ProductAreaPercent = details.ProductAreaPercent;
				}

				trackingId = details.TrackingId;
			}

			AllowDelete = !AddProduct && view.Products.Count > 1;

			CreateUserProductCommand = new RelayCommand(CreateUserProduct);

			Cancel = new RelayCommand(CancelEdit);
			Update = new RelayCommand(UpdateProduct);
			Delete = new RelayCommand(DeleteProduct);

			ValidateViewModel();
		}

		public ICommand Cancel { get; private set; }
		public ICommand Update { get; private set; }
		public ICommand Delete { get; private set; }

		public ICommand CreateUserProductCommand { get; private set; }

		public ObservableCollection<UnitPerAreaHelper> AllowedUnits { get; private set; }
		public ObservableCollection<PriceItem> PriceItems { get; private set; }

		public bool AllowEdit { get; private set; }
		public bool AddProduct { get; private set; }
		public bool AllowDelete { get; private set; }

		public ObservableCollection<ProductListItemDetails> Products { get; private set; }
		public ObservableCollection<Landdb.ViewModel.Secondary.Plan.PlannedProductViewModel> PlannedProducts { get; private set; }

		public string ProductSearchText { get; set; }

		[CustomValidation(typeof(EditPlanProductViewModel), nameof(ValidateSelectedProduct))]
		public ProductListItemDetails SelectedProduct {
			get { return selectedProduct; }
			set {
				selectedProduct = value;
				ValidateAndRaisePropertyChanged(() => SelectedProduct);
				RaisePropertyChanged(() => DisplayName);

                UpdateAllowedUnits();

                if (AddProduct && SelectedProduct != null) {
					var invProd = inventory.Products.FirstOrDefault(x => x.Key == SelectedProduct.ProductId);
					currentInventoryItem = invProd.Value != null ? invProd.Value : null;
					UpdatePriceItems();
					RecalculateItem();
				}

                RateValue = 0;  // TODO: In the future, this should be the most recently used rate. We should also populate a list of the MR rates for this product.
            }
		}

		public string DisplayName {
			get {
				if (SelectedProduct == null) { return string.Empty; }
				return string.Format("{0} ({1})", SelectedProduct.Product.Name, SelectedProduct.Product.Manufacturer);
			}
		}

		[CustomValidation(typeof(EditPlanProductViewModel), nameof(ValidateRate))]
		public decimal RateValue {
			get { return rateValue; }
			set {
				rateValue = value;
				RaisePropertyChanged(() => RateValue);
				RecalculateItem();
			}
		}

		[CustomValidation(typeof(EditPlanProductViewModel), nameof(ValidateUnit))]
		public UnitPerAreaHelper SelectedRateUnitHelper {
			get { return rateUnitHelper; }
			set {
				rateUnitHelper = value;
				RaisePropertyChanged(() => SelectedRateUnitHelper);
				RecalculateItem();
			}
		}

		public PriceItem SelectedPriceItem {
			get { return selectedPriceItem; }
			set {
				selectedPriceItem = value;
				RaisePropertyChanged(() => SelectedPriceItem);
				RecalculateItem();
			}
		}

		public string SelectedPriceText {
			get { return selectedPriceText; }
			set {
				if (string.IsNullOrWhiteSpace(value)) { return; }

				if (selectedPriceText != value && (SelectedPriceItem == null || value != SelectedPriceItem.ToString())) {
					selectedPriceText = value;

					decimal price = 0m;
					string concatString = System.Text.RegularExpressions.Regex.Replace(selectedPriceText, "[/$A-Za-z]", "");
					concatString = concatString.Last().ToString() == "." ? concatString.Remove(concatString.Length - 1) : concatString;
					if (decimal.TryParse(concatString, out price)) {
						if (AddProduct && SelectedProduct != null) {
							var priceUnit = currentInventoryItem != null && currentInventoryItem.AveragePriceUnit != null ? currentInventoryItem.AveragePriceUnit : selectedProduct.Product.StdPackageUnit;
							PriceItem priceItem = new PriceItem(price, priceUnit);
							PriceItems.Add(priceItem);
							SelectedPriceItem = priceItem;
							selectedPriceText = priceItem.ToString();
						} else {
							var priceUnit = details.CostUnit;
							PriceItem priceItem = new PriceItem(price, priceUnit.Name, false);
							PriceItems.Add(priceItem);
							SelectedPriceItem = priceItem;
							selectedPriceText = priceItem.ToString();
						}
					}
				}
			}
		}

		public decimal CostValuePerUnit {
			get { return costValuePerUnit; }
			set {
				costValuePerUnit = value;
				RaisePropertyChanged(() => CostValuePerUnit);
				RecalculateItem();
			}
		}

		public IUnit CostUnit {
			get { return costUnit; }
			set {
				costUnit = value;
				RaisePropertyChanged(() => CostUnit);
			}
		}

		public Measure TotalProduct {
			get { return totalProduct; }
			set {
				totalProduct = value;
				RaisePropertyChanged(() => TotalProduct);
			}
		}

		public decimal TotalCost {
			get { return totalCost; }
			set {
				totalCost = value;
				RaisePropertyChanged(() => TotalCost);
			}
		}

		public decimal ProductAreaPercent {
			get { return productAreaPercent; }
			set {
				productAreaPercent = value;
				ProductArea = totalArea.Unit.GetMeasure(totalArea.Value * (double)productAreaPercent);

				RaisePropertyChanged(() => ProductAreaPercent);
				RaisePropertyChanged(() => ProductAreaDisplayText);

				RecalculateItem();
			}
		}

		// readonly property for displaying product area. Setter is only for conveniently raising the prop-changed event for the view
		public Measure ProductArea {
			get { return productArea; }
			private set {
				productArea = value;
				RaisePropertyChanged(() => ProductArea);
			}
		}

		public string ProductAreaDisplayText {
			get { return ProductArea.AbbreviatedDisplay; }
			set {
				if (string.IsNullOrWhiteSpace(value)) { return; }

				decimal parsedValue = 0;
				if (decimal.TryParse(value, out parsedValue)) {
					ProductArea = totalArea.Unit.GetMeasure(Convert.ToDouble(parsedValue));
				}

				productAreaPercent = totalArea.Value != 0 ? Convert.ToDecimal(productArea.Value / totalArea.Value) : 1;

				RaisePropertyChanged(() => ProductAreaPercent);
				RaisePropertyChanged(() => ProductAreaDisplayText);

				RecalculateItem();
			}
		}

		public bool IsCreateProductPopupOpen {
			get { return isCreateProductPopupOpen; }
			set {
				isCreateProductPopupOpen = value;
				RaisePropertyChanged(() => IsCreateProductPopupOpen);
			}
		}

		public bool IsProductListVisible {
			get { return isProductListVisible; }
			set {
				isProductListVisible = value;
				RaisePropertyChanged(() => IsProductListVisible);
			}
		}

		void UpdatePriceItems() {
			PriceItems.Clear();
			if (currentInventoryItem != null) {
				var priceItem = new PriceItem((decimal)currentInventoryItem.AveragePriceValue, currentInventoryItem.AveragePriceUnit, true);
				PriceItems.Add(priceItem);
				SelectedPriceItem = priceItem;
			} else {
				if (SelectedProduct != null) {
					var priceItem = new PriceItem(0m, SelectedProduct.Product.StdPackageUnit, false);
					PriceItems.Add(priceItem);
					SelectedPriceItem = priceItem;
				}
			}

            // Try to get the view with past price items
            var cachedPrices = clientEndpoint.GetView<SpecificPriceCacheView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear));
            if (cachedPrices.HasValue && cachedPrices.Value.Prices.ContainsKey(SelectedProduct.ProductId.Id)) {
                var priceList = cachedPrices.Value.Prices[SelectedProduct.ProductId.Id];
                foreach (var p in priceList) {
                    PriceItems.Add(new PriceItem(p.Price, p.PriceUnit, false));
                }
            }
        }

        void UpdateAllowedUnits() {
			AllowedUnits.Clear();
			SelectedRateUnitHelper = null;

			if (SelectedProduct == null) { return; }

			var psu = UnitFactory.GetPackageSafeUnit(SelectedProduct.Product.StdPackageUnit, SelectedProduct.Product.StdUnit, SelectedProduct.Product.StdFactor, SelectedProduct.Product.StdPackageUnit);
			var compatibleUnits = AgC.UnitConversion.CompatibilityFactory.GetCompatibleUnits(psu, SelectedProduct.Product.Density);


			foreach (var u in compatibleUnits) {
				var uh = new UnitPerAreaHelper(u, UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit));
				AllowedUnits.Add(uh);
				if (u == psu) {
					SelectedRateUnitHelper = uh;
				}
			}

			// If we couldn't find it in the list while we were building it, pick the first one, if any.
			if (SelectedRateUnitHelper == null && AllowedUnits.Any()) { SelectedRateUnitHelper = AllowedUnits[0]; }

			// TODO: Make CostUnit configurable
			CostUnit = UnitFactory.GetPackageSafeUnit(SelectedProduct.Product.StdPackageUnit, SelectedProduct.Product.StdUnit, SelectedProduct.Product.StdFactor, SelectedProduct.Product.StdPackageUnit);
		}

        [Range(typeof(int), "1", "999999", ErrorMessageResourceName = "ValueMustBeGreaterThanZero_Text", ErrorMessageResourceType=typeof(Strings))]
        public int ApplicationCount {
			get { return appCnt; }
			set {
				appCnt = value;
				RecalculateItem();

				ValidateAndRaisePropertyChanged(() => ApplicationCount);
			}
		}

		public List<TimingEvent> TimingEvents {
			get {
				List<TimingEvent> timingEvents = clientEndpoint.GetMasterlistService().GetTimingEventList().OrderBy(x => x.Order).ToList();
				return timingEvents;
			}
		}

		public TimingEvent TimingEvent {
			get { return timingEvent; }
			set {
				timingEvent = value;
				SelectAppropriateTags();
				RaisePropertyChanged(() => TimingEvent);
			}
		}

		public Dictionary<string, List<string>> AvailableTimingEventTags {
			get { return availableTimingEventTags; }
			set {
				availableTimingEventTags = value;
				SelectAppropriateTags();
			}
		}

		public Dictionary<string, List<string>> TempTimingEventTags {
			get { return tempTimingEventTags; }
			set {
				tempTimingEventTags = value;
				SelectAppropriateTags();
			}
		}

		public List<string> TimingEventTags {
			get {
				if (timingEventTags == null) {
					return new List<string>();
				}
				return timingEventTags.OrderBy(q => q).ToList();
			}
			set {
				timingEventTags = value;
				RaisePropertyChanged(() => TimingEventTags);
			}
		}

		public string TimingEventTag {
			get { return timingEventTag; }
			set {
				if (timingEventTag == value) { return; }
				timingEventTag = value;
				RaisePropertyChanged(() => TimingEventTag);
			}
		}

		public string SelectedTimingEventTag {
			get { return selectedTimingEventTag; }
			set {
				if (selectedTimingEventTag == value) { return; }
				selectedTimingEventTag = value;
				if (!string.IsNullOrEmpty(selectedTimingEventTag)) {
					timingEventTag = selectedTimingEventTag;
				}
				RaisePropertyChanged(() => SelectedTimingEventTag);
			}
		}

		private void SelectAppropriateTags() {
			if (timingEvent != null && !string.IsNullOrEmpty(timingEvent.Name)) {
				mergedTimingEventTags = availableTimingEventTags;
				if (tempTimingEventTags != null && tempTimingEventTags.Count > 0) {
					mergedTimingEventTags = availableTimingEventTags.Concat(tempTimingEventTags).GroupBy(d => d.Key).ToDictionary(d => d.Key, d => d.First().Value);
				}

				if (mergedTimingEventTags != null && mergedTimingEventTags.ContainsKey(timingEvent.Name)) {
					TimingEventTags = mergedTimingEventTags[timingEvent.Name];
				} else {
					TimingEventTags = new List<string>();
				}
			}
		}

		public DateTime? TargetDate {
			get { return targetDate; }
			set {
				targetDate = value;
				RaisePropertyChanged(() => TargetDate);
			}
		}

		void RecalculateItem() {
			RecalculateByArea();

			ValidateProperty(() => SelectedRateUnitHelper);
			ValidateProperty(() => RateValue);
		}

		void RecalculateByArea() {
			var areaUnit = ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit;
			//totalArea = Measure.CreateMeasure(ProductArea.Value, areaUnit);

			if (SelectedProduct == null || SelectedRateUnitHelper == null) {
				TotalProduct = new NullMeasure();
				return;
			}

			var psu = UnitFactory.GetPackageSafeUnit(SelectedProduct.Product.StdPackageUnit, SelectedProduct.Product.StdUnit, SelectedProduct.Product.StdFactor, SelectedProduct.Product.StdPackageUnit);
			var rate = SelectedRateUnitHelper.Unit.GetMeasure((double)RateValue, SelectedProduct.Product.Density); // TODO: Account for density, if necessary.

			ProductArea = totalArea.Unit.GetMeasure(totalArea.Value * (double)productAreaPercent);
			var totalValue = rate.Value * ProductArea.Value * ApplicationCount;
			var totalPreConvertedMeasure = rate.Unit.GetMeasure(totalValue, SelectedProduct.Product.Density);
			TotalProduct = totalPreConvertedMeasure.CreateAs(psu);

			var mlp = clientEndpoint.GetMasterlistService().GetProduct(SelectedProduct.ProductId);

			if (selectedPriceItem != null && selectedPriceItem.PriceUnit != null && mlp != null && TotalProduct.Value != 0) {
				var priceUnit = UnitFactory.GetPackageSafeUnit(SelectedPriceItem.PriceUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
				if (TotalProduct.CanConvertTo(priceUnit)) {
					var convertedTotalProduct = TotalProduct.Unit != priceUnit ? TotalProduct.CreateAs(priceUnit) : TotalProduct;
					TotalCost = selectedPriceItem.Price * (decimal)convertedTotalProduct.Value; // TODO: This only works because we're currently defaulting units of Total Product and Cost to be the same. This won't always be the case, and needs to be changed.
				} else {
					TotalCost = 0;
				}
			} else {
				TotalCost = 0;
			}
		}

		internal PlanProductEntry ToPlanProductEntry() {
			// TODO: This needs to represent the actual rate unit the user has chosen.
			var psu = UnitFactory.GetPackageSafeUnit(SelectedProduct.Product.StdPackageUnit, SelectedProduct.Product.StdUnit, SelectedProduct.Product.StdFactor, SelectedProduct.Product.StdPackageUnit);

			PriceStrategy strategy = PriceStrategy.Specific;
			decimal price = SelectedPriceItem.Price;
			string priceUnit = SelectedPriceItem.PriceUnit ?? SelectedProduct.Product.StdPackageUnit;
			var areaUnit = ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit;
			return new PlanProductEntry(new ProductId(SelectedProduct.DataSourceId, SelectedProduct.Product.Id), trackingId, RateValue, SelectedRateUnitHelper.Unit.Name, (decimal)TotalProduct.Value, TotalProduct.Unit.Name, areaUnit, strategy, price, priceUnit, (decimal)ProductAreaPercent, ApplicationCount, TimingEvent != null ? TimingEvent.Name : string.Empty, TargetDate, TimingEventTag);
		}

		internal ProductDetails ToProductDetails() {
			decimal price = SelectedPriceItem.Price;
			string priceUnit = SelectedPriceItem.PriceUnit ?? SelectedProduct.Product.StdPackageUnit;
			var priceIUnit = UnitFactory.GetPackageSafeUnit(priceUnit, SelectedProduct.Product.StdUnit, SelectedProduct.Product.StdFactor, SelectedProduct.Product.StdPackageUnit);
			var totalCost = 0m;

			try {
				totalCost = TotalProduct.CanConvertTo(priceIUnit) ? (decimal)TotalProduct.GetValueAs(priceIUnit) * price : (decimal)TotalProduct.Value * price;
			} catch (Exception ex) {
				totalCost = 0m;
			}

			var timingEventList = clientEndpoint.GetMasterlistService().GetTimingEventList();

			var prodDetail = new ProductDetails() {
				ProductName = SelectedProduct.Product.Name,
				RateMeasure = SelectedRateUnitHelper.Unit.GetMeasure((double)RateValue, SelectedProduct.Product.Density),
				AreaUnit = view.EstimatedArea.Unit,
				TotalProduct = TotalProduct,
				TotalCost = totalCost,
				CostPerUnitValue = price,
				CostUnit = priceIUnit,
				ID = SelectedProduct.ProductId,
				ProductAreaPercent = (decimal)ProductAreaPercent,
				TrackingId = trackingId,
				TimingEvent = TimingEvent != null ? TimingEvent.Name : string.Empty,
				TimingEventOrder = TimingEvent != null ? TimingEvent.Order : 0,
				TargetDate = TargetDate,
				TimingEventTag = TimingEventTag,
				ApplicationCount = ApplicationCount,
			};

			return prodDetail;
		}

		internal string ToName() {
			return SelectedProduct.Product.Name;
		}

		void UpdateProduct() {
			if (!ValidateViewModel()) { return; }

			if (AddProduct && SelectedProduct != null) {
				//add product 
				RecalculateItem();
				AddProductToPlan addCommand = new AddProductToPlan(view.Id, clientEndpoint.GenerateNewMetadata(), trackingId, ToPlanProductEntry());
				clientEndpoint.SendOne(addCommand);

				parent.AddProductToPlan(ToProductDetails());
			} else {
				//update a product if something has changed
				RecalculateItem();

				if (RateValue != (decimal)details.RateMeasure.Value || SelectedRateUnitHelper.Unit != details.RateMeasure.Unit) {
					ChangePlanProductRatePerArea planCommand = new ChangePlanProductRatePerArea(view.Id, clientEndpoint.GenerateNewMetadata(), trackingId, RateValue, SelectedRateUnitHelper.Unit.Name);
					clientEndpoint.SendOne(planCommand);
				}
				if ((decimal)ProductAreaPercent != details.ProductAreaPercent) {
					ChangePlanProductCoveragePercent coverage = new ChangePlanProductCoveragePercent(view.Id, clientEndpoint.GenerateNewMetadata(), trackingId, (decimal)ProductAreaPercent);
					clientEndpoint.SendOne(coverage);
				}

				var prodDetails = ToProductDetails();
				if (details.TimingEvent != prodDetails.TimingEvent || details.TimingEventTag != prodDetails.TimingEventTag) {
					ChangePlanProductTimingEvent timingCommand = new ChangePlanProductTimingEvent(view.Id, clientEndpoint.GenerateNewMetadata(), trackingId, prodDetails.TimingEvent, prodDetails.TimingEventTag);
					clientEndpoint.SendOne(timingCommand);
				}

				var planProd = view.Products.SingleOrDefault(p => p.TrackingId == details.TrackingId);
				if (planProd.SpecificCostPerUnit != prodDetails.CostPerUnitValue) {
					ChangePlanProductSpecificCost costCommand = new ChangePlanProductSpecificCost(view.Id, clientEndpoint.GenerateNewMetadata(), trackingId, prodDetails.CostPerUnitValue, prodDetails.CostUnit.Name);
					clientEndpoint.SendOne(costCommand);
				}

				if (planProd.TargetDate != prodDetails.TargetDate) {
					ChangePlanProductTargetDate targetDateCommand = new ChangePlanProductTargetDate(view.Id, clientEndpoint.GenerateNewMetadata(), trackingId, TargetDate);
					clientEndpoint.SendOne(targetDateCommand);
				}

				if (planProd.ApplicationCount != prodDetails.ApplicationCount) {
					var cmd = new ChangePlanProductApplicationCount(view.Id, clientEndpoint.GenerateNewMetadata(), trackingId, prodDetails.ApplicationCount);
					clientEndpoint.SendOne(cmd);
				}

				parent.EditProductInPlan(prodDetails);
			}

			//Messenger.Default.Send(new HidePopupMessage());
			Messenger.Default.Send(new HideOverlayMessage());
		}

		void CancelEdit() {
			//just close the popup message
			//Messenger.Default.Send(new HidePopupMessage());
			Messenger.Default.Send(new HideOverlayMessage());
		}

		void DeleteProduct() {
			if (!AddProduct) {
				parent.RemoveProductFromPlan(ToProductDetails());

				RemoveProductFromPlan removeCommand = new RemoveProductFromPlan(view.Id, clientEndpoint.GenerateNewMetadata(), trackingId);
				clientEndpoint.SendOne(removeCommand);
				//Messenger.Default.Send(new HidePopupMessage());
				Messenger.Default.Send(new HideOverlayMessage());
			}
		}

		async void BuildProductList() {
			var watch = System.Diagnostics.Stopwatch.StartNew();

			await Task.Run(() => {
				var mpl = clientEndpoint.GetMasterlistService().GetProductList();
				var mur = clientEndpoint.GetView<ProductUsageView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear)).GetValue(new ProductUsageView());
				var ucp = clientEndpoint.GetView<UserCreatedProductList>(new DataSourceId(ApplicationEnvironment.CurrentDataSourceId)).GetValue(new UserCreatedProductList());

				Guid mlDsId = GlobalIdentifiers.GetMasterlistDataSourceId();
				var productGuids = mur.GetProductGuids();

				var q = from p in mpl
						let cont = productGuids.Contains(p.Id)
						select new ProductListItemDetails(p, mlDsId, cont ? Strings.UsedThisYear_Text : Strings.Unused_Text, cont ? (short)0 : (short)1);

				var ucpQ = from p in ucp.UserCreatedProducts
						   let cont = productGuids.Contains(p.Id)
						   select new ProductListItemDetails(p, ApplicationEnvironment.CurrentDataSourceId, cont ? Strings.UsedThisYear_Text : Strings.Unused_Text, cont ? (short)0 : (short)1);

				Products = new ObservableCollection<ProductListItemDetails>(q.Union(ucpQ).OrderBy(x => x.GroupSortOrder).ToList());

				RaisePropertyChanged(() => Products);
			});

			watch.Stop();
		}

		void OnProductCreated(MiniProduct product) {
			var pli = AddTemporaryMiniProduct(product, new ProductId(ApplicationEnvironment.CurrentDataSourceId, product.Id), true);
			SelectedProduct = pli;
		}

		void CreateUserProduct() {
			IsProductListVisible = false;
			IsCreateProductPopupOpen = false;

			var vm = new Landdb.ViewModel.Secondary.UserCreatedProduct.UserProductCreatorViewModel(clientEndpoint, ProductSearchText, OnProductCreated);
			Messenger.Default.Send<ShowOverlayMessage>(new ShowOverlayMessage() {
				ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Secondary.UserCreatedProduct.UserProductCreatorView", "createProduct", vm)
			});

			//ShowPopupMessage msg = new ShowPopupMessage() {
			//    ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Secondary.UserCreatedProduct.UserProductCreatorView", "createProduct",
			//        new UserProductCreatorViewModel(endPoint, ProductSearchText, OnProductCreated))
			//};
			//Messenger.Default.Send<ShowPopupMessage>(msg);
		}

		internal ProductListItemDetails AddTemporaryMiniProduct(MiniProduct product, ProductId productId, bool isUsed) {
			var p = new ProductListItemDetails(product, productId.DataSourceId, isUsed ? Strings.UsedThisYear_Text : Strings.Unused_Text, isUsed ? (short)0 : (short)1);
			Products.Add(p);
			return p;
		}

		#region Custom Validation
		public static ValidationResult ValidateSelectedProduct(ProductListItemDetails product, ValidationContext context) {
			ValidationResult result = null;

			if (product == null) {
				result = new ValidationResult(Strings.ProductIsRequired_Text);
			}

			return result;
		}

		public static ValidationResult ValidateRate(decimal rate, ValidationContext context) {
			ValidationResult result = null;

			if (rate == 0) {
				result = new ValidationResult(Strings.RateCannotBeZero_Text);
			}

			return result;
		}

		public static ValidationResult ValidateUnit(object unit, ValidationContext context) {
			ValidationResult result = null;

			if (unit == null) {
				result = new ValidationResult(Strings.ValidationResult_InvalidUnit_Text);
			}

			return result;
		}
		#endregion
	}
}