﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels;
using Landdb.Domain.ReadModels.Inventory;
using NLog;
using Landdb.Client.Models;
using Landdb.Domain.ReadModels.Product;
using Landdb.Infrastructure;
using Landdb.Domain.ReadModels.Tags;
using Landdb.Domain.ReadModels.Plan;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure.Messages;
using System.Windows.Input;
using Landdb.Resources;
using Landdb.ViewModel.Production;
using Newtonsoft.Json;

namespace Landdb.ViewModel.Production.Popups {
    public class FailedCommandTemplateViewModel : ViewModelBase
    {
        readonly Dispatcher dispatcher;
        readonly IClientEndpoint clientEndpoint;
        readonly Logger log = LogManager.GetCurrentClassLogger();
        string jsonString = string.Empty;
        string selectedstrategy = string.Empty;
        IList<string> applicationstrategies = new List<string>();

        public FailedCommandTemplateViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher) {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;

            selectedstrategy = "0 ApplicationProductByArea";
            applicationstrategies = new List<string>();
            applicationstrategies.Add("0 ApplicationProductByArea");
            applicationstrategies.Add("1 ApplicationProductByTotal");
            applicationstrategies.Add("2 ApplicationProductByTank");
            RaisePropertyChanged("SelectedStrategy");
            RaisePropertyChanged("ApplicationStrategies");

            SendFailedCommand = new RelayCommand(Continue);
            CancelCommand = new RelayCommand(Cancel);

        }

        public ICommand CancelCommand { get; private set; }
        public ICommand SendFailedCommand { get; private set; }

        public string JsonString {
            get { return jsonString; }
            set {
                if (jsonString == value) { return; }
                jsonString = value;
                RaisePropertyChanged("JsonString");
            }
        }

        public string SelectedStrategy {
            get { return selectedstrategy; }
            set {
                if (selectedstrategy == value) { return; }
                selectedstrategy = value;
                RaisePropertyChanged("SelectedStrategy");
            }
        }

        public IList<string> ApplicationStrategies {
            get { return applicationstrategies; }
            set {
                if (applicationstrategies == value) { return; }
                applicationstrategies = value;
                RaisePropertyChanged("ApplicationStrategies");
            }
        }

        void Continue() {
            try {
                if (string.IsNullOrEmpty(JsonString)) { return; }
                //JsonSerializerSettings serializationSettings = new JsonSerializerSettings() { TypeNameHandling = Newtonsoft.Json.TypeNameHandling.All };
                //JsonSerializerSettings serializationSettings = new JsonSerializerSettings() { TypeNameHandling = TypeNameHandling.Auto };
                string jsonstring2 = JsonString;
                //try {
                //    //if (jsonstring2.EndsWith(@",")) {
                //    //    jsonstring2 = jsonstring2.Substring(0, jsonstring2.Length - 1);
                //    //    //jsonstring2.TrimEnd(.Trim(@",");
                //    //}
                //    string result1 = System.Text.RegularExpressions.Regex.Replace(JsonString, @"(\\)([\000\010\011\012\015\032\042\047\134\140])", "$2");
                //    string result2 = result1.Replace(@"\", @"\\");
                //    string teststring3 = result2;
                //    if (result2.EndsWith(@",")) {
                //        teststring3 = result2.Substring(1, result2.Length - 3);
                //    }
                //    else {
                //        teststring3 = result2.Substring(1, result2.Length - 2);
                //    }

                //    //string teststring3 = result2.Substring(1, result2.Length - 2);
                //    jsonstring2 = teststring3;
                //}
                //catch (Exception ex1) {
                //    int def = 0;
                //}


                if (selectedstrategy == "0 ApplicationProductByArea") {
                    var saveFailedCommand2 = JsonConvert.DeserializeObject<Landdb.ViewModel.Production.Popups.Structure.CreateApplication0>(jsonstring2);
                    //ApplicationId applicationId = new ApplicationId(ApplicationEnvironment.CurrentDataSourceId, Guid.NewGuid());
                    //MessageMetadata messageMetadatanew = new MessageMetadata(saveFailedCommand2.Metadata.SourceMessageId.Value, DateTime.UtcNow, clientEndpoint.DeviceId, clientEndpoint.UserId);
                    var saveFailedCommand = new CreateApplication(
                        saveFailedCommand2.Id,
                        saveFailedCommand2.Metadata,
                        saveFailedCommand2.CropYear,
                        saveFailedCommand2.Name,
                        saveFailedCommand2.StartDateTime,
                        saveFailedCommand2.Duration,
                        saveFailedCommand2.TimingEvent,
                        saveFailedCommand2.EndDateTime,
                        saveFailedCommand2.Name,
                        saveFailedCommand2.Authorization,
                        saveFailedCommand2.Applicators,
                        saveFailedCommand2.CropZones,
                        saveFailedCommand2.TankInformation,
                        saveFailedCommand2.ApplicationStrategy,
                        saveFailedCommand2.Products,
                        saveFailedCommand2.Conditions,
                        saveFailedCommand2.Documents,
                        saveFailedCommand2.TimingEventTag
                    );
                    clientEndpoint.SendOne(saveFailedCommand);
                }
                else if (selectedstrategy == "1 ApplicationProductByTotal") {
                    var saveFailedCommand2 = JsonConvert.DeserializeObject<Landdb.ViewModel.Production.Popups.Structure.CreateApplication1>(jsonstring2);
                    var saveFailedCommand = new CreateApplication(
                        saveFailedCommand2.Id,
                        saveFailedCommand2.Metadata,
                        saveFailedCommand2.CropYear,
                        saveFailedCommand2.Name,
                        saveFailedCommand2.StartDateTime,
                        saveFailedCommand2.Duration,
                        saveFailedCommand2.TimingEvent,
                        saveFailedCommand2.EndDateTime,
                        saveFailedCommand2.Name,
                        saveFailedCommand2.Authorization,
                        saveFailedCommand2.Applicators,
                        saveFailedCommand2.CropZones,
                        saveFailedCommand2.TankInformation,
                        saveFailedCommand2.ApplicationStrategy,
                        saveFailedCommand2.Products,
                        saveFailedCommand2.Conditions,
                        saveFailedCommand2.Documents,
                        saveFailedCommand2.TimingEventTag
                    );
                    clientEndpoint.SendOne(saveFailedCommand);
                }
                else if (selectedstrategy == "2 ApplicationProductByTank") {
                    var saveFailedCommand2 = JsonConvert.DeserializeObject<Landdb.ViewModel.Production.Popups.Structure.CreateApplication2>(jsonstring2);
                    var saveFailedCommand = new CreateApplication(
                        saveFailedCommand2.Id,
                        saveFailedCommand2.Metadata,
                        saveFailedCommand2.CropYear,
                        saveFailedCommand2.Name,
                        saveFailedCommand2.StartDateTime,
                        saveFailedCommand2.Duration,
                        saveFailedCommand2.TimingEvent,
                        saveFailedCommand2.EndDateTime,
                        saveFailedCommand2.Name,
                        saveFailedCommand2.Authorization,
                        saveFailedCommand2.Applicators,
                        saveFailedCommand2.CropZones,
                        saveFailedCommand2.TankInformation,
                        saveFailedCommand2.ApplicationStrategy,
                        saveFailedCommand2.Products,
                        saveFailedCommand2.Conditions,
                        saveFailedCommand2.Documents,
                        saveFailedCommand2.TimingEventTag
                    );
                    clientEndpoint.SendOne(saveFailedCommand);
                }

                Messenger.Default.Send<HideOverlayMessage>(new HideOverlayMessage());
                JsonString = string.Empty;

                
                //JsonConverter[] converters = { new FooConverter() };
                //var test = JsonConvert.DeserializeObject<List<BaseFoo>>(result, new JsonSerializerSettings() { Converters = converters });

                //var saveFailedCommand = JsonConvert.DeserializeObject<ApplicationCreated>(JsonString);

                //var commandFailedEventText = Encoding.UTF8.GetString(e.Event.Data);
                //var cmdFailedEvent = JsonConvert.DeserializeObject<DomainCommandFailedEvent>(commandFailedEventText);
            }
            catch (Exception ex) {
                System.Windows.MessageBox.Show(Strings.ThereWasProblemAttemptingToResubmitFailedCmd_Text, Strings.Warning_Text);
            }
        }
        void Cancel() {
            Messenger.Default.Send<HideOverlayMessage>(new HideOverlayMessage());
            JsonString = string.Empty;
        }
    }
}
