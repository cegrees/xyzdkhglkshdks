﻿using AgC.UnitConversion;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Company;
using Landdb.Domain.ReadModels.Person;
using Landdb.Domain.ReadModels.WorkOrder;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.ViewModel.Production.Applications;

namespace Landdb.ViewModel.Production.Popups
{
    public class ChangeApplicatorInfo : ViewModelBase
    {
        IClientEndpoint clientEndpoint;
        Dispatcher dispatcher;

        ObservableCollection<PotentialApplicatorViewModel> potentialApplicators = new ObservableCollection<PotentialApplicatorViewModel>();
        PotentialApplicatorViewModel selectedPotentialApplicator = null;

        ApplicationView view;
        WorkOrderView woView;

        ApplicationDetailsViewModel parent;

        public ChangeApplicatorInfo(IClientEndpoint clientEndpoint, Dispatcher dispatcher, ObservableCollection<ApplicationApplicator> currentApplicatorSelections, ApplicationView view, ApplicationDetailsViewModel parent)
        {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            this.view = view;
            this.parent = parent;

            UpdateCommand = new RelayCommand(Update);
            AddCommand = new RelayCommand(Add);
            RemoveCommand = new RelayCommand<ApplicationApplicator>(Remove);

            PopulatePotentialApplicators();

            Applicators = currentApplicatorSelections;

            if (view.EnvironmentalZoneDistanceValue.HasValue)
            {
                BufferWidth = view.EnvironmentalZoneDistanceValue;
                BufferWidthUnit = !string.IsNullOrEmpty(view.EnvironmentalZoneDistanceUnit) ? UnitFactory.GetUnitByName(view.EnvironmentalZoneDistanceUnit) : ( ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit.ToLower() == "acre" ? UnitFactory.GetUnitByName("foot") : UnitFactory.GetUnitByName("meter"));
                SelectedBufferTolerance = view.Severity;
            }
            else
            {
                BufferWidthUnit = (ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit.ToLower() == "acre" ? UnitFactory.GetUnitByName("foot") : UnitFactory.GetUnitByName("meter"));
            }

            IsEdit = true;
            RemovedApplicators = new List<ApplicationApplicator>();
        }

        public ChangeApplicatorInfo(IClientEndpoint clientEndpoint, Dispatcher dispatcher, ObservableCollection<ApplicationApplicator> currentApplicatorSelections, WorkOrderView view, ApplicationDetailsViewModel parent)
        {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            this.woView = view;
            this.parent = parent;

            UpdateCommand = new RelayCommand(Update);
            AddCommand = new RelayCommand(Add);
            RemoveCommand = new RelayCommand<ApplicationApplicator>(Remove);

            PopulatePotentialApplicators();

            Applicators = currentApplicatorSelections;

            if (woView.EnvironmentalZoneDistanceValue.HasValue)
            {
                BufferWidth = woView.EnvironmentalZoneDistanceValue;
                BufferWidthUnit = !string.IsNullOrEmpty(view.EnvironmentalZoneDistanceUnit) ? UnitFactory.GetUnitByName(view.EnvironmentalZoneDistanceUnit) : (ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit.ToLower() == "acre" ? UnitFactory.GetUnitByName("foot") : UnitFactory.GetUnitByName("meter"));
                SelectedBufferTolerance = woView.Severity;
                //RaisePropertyChanged(() => BufferWidthUnit);
            }
            else { BufferWidthUnit = (ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit.ToLower() == "acre" ? UnitFactory.GetUnitByName("foot") : UnitFactory.GetUnitByName("meter")); }

            IsEdit = true;
            RemovedApplicators = new List<ApplicationApplicator>();
        }

        public ICommand UpdateCommand { get; private set; }
        public ICommand AddCommand { get; private set; }
        public ICommand RemoveCommand { get; private set; }

        public bool IsEdit { get; set; }
        public ObservableCollection<PotentialApplicatorViewModel> PotentialApplicators {
            get { return potentialApplicators; }
            private set {
                potentialApplicators = value;
                RaisePropertyChanged("PotentialApplicators");
            }
        }

        public PotentialApplicatorViewModel SelectedPotentialApplicator {
            get { return selectedPotentialApplicator; }
            set {
                selectedPotentialApplicator = value;
                RaisePropertyChanged("SelectedPotentialApplicator");
            }
        }

        void PopulatePotentialApplicators() {
            PotentialApplicators.Clear();
            DataSourceId dsID = new DataSourceId(ApplicationEnvironment.CurrentDataSourceId);
			var currentCropYear = ApplicationEnvironment.CurrentCropYear;

            // Get companies
            var companyListView = clientEndpoint.GetView<CompanyListView>(dsID).GetValue(new CompanyListView());
			var companyList = companyListView.Companies;

            // Populate people
			var personListView = clientEndpoint.GetView<PersonListView>(dsID).GetValue(() => null);
			if (personListView.Persons != null) {
				var sortedFilteredProjectedPersons = from p in personListView.Persons
													 where p.IsActiveInCropYear(currentCropYear)
													 orderby p.Name
													 select p;

				sortedFilteredProjectedPersons.ForEach(x => PotentialApplicators.Add(new PotentialApplicatorViewModel(clientEndpoint, x, companyList, OnItemChanged)));
			}

            // Populate companies
            //foreach (var c in companyList) {
            //    PotentialApplicators.Add(new PotentialApplicatorViewModel(clientEndpoint, c));
            //}
        }

        void OnItemChanged(PotentialApplicatorViewModel changedItem) {

        }


        public ObservableCollection<ApplicationApplicator> Applicators { get; set; }
        public List<ApplicationApplicator> RemovedApplicators { get; set; }

        public decimal? BufferWidth { get; set; }
        public IUnit BufferWidthUnit { get; set; }
        public string SelectedBufferTolerance { get; set; }
        public List<string> BufferZoneList
        {
            get
            {
                List<string> zones = new List<string>();
                zones.Add("Spray Drift");
                zones.Add("Pollen Drift");
                zones.Add("Interim Aquatic");
                zones.Add("Bee Protection");
                return zones;
            }
        }

        bool IfBufferChange()
        {
            // && !string.IsNullOrEmpty(BufferWidthUnit)
            if (BufferWidth.HasValue && BufferWidth.Value > 0 && !string.IsNullOrEmpty(SelectedBufferTolerance))
            {
                if (view != null)
                {
                    return BufferWidth != view.EnvironmentalZoneDistanceValue || BufferWidthUnit.Name != view.EnvironmentalZoneDistanceUnit || SelectedBufferTolerance != view.Severity;
                }
                else if (woView != null)
                {
                    return BufferWidth != woView.EnvironmentalZoneDistanceValue || BufferWidthUnit.Name != woView.EnvironmentalZoneDistanceUnit || SelectedBufferTolerance != woView.Severity;
                }
            }
            return false;
        }
        void Add() {
            if (SelectedPotentialApplicator != null && !Applicators.Where(x => x.PersonId == SelectedPotentialApplicator.PersonId).Any()) {
                string compName = null;
                CompanyId companyId = null;
                if (SelectedPotentialApplicator.Company != null) {
                    compName = SelectedPotentialApplicator.Company.Name;
                    companyId = SelectedPotentialApplicator.Company.Id;
                }
                Applicators.Add(new ApplicationApplicator(SelectedPotentialApplicator.PersonId, SelectedPotentialApplicator.Name, companyId, compName, SelectedPotentialApplicator.LicenseNumber, SelectedPotentialApplicator.Expires));
            } else {
                //Applicators.Where(x => x.PersonId == SelectedPerson.Id).SingleOrDefault().LicenseNumber = LicenseNumber;
                //Applicators.Where(x => x.PersonId == SelectedPerson.Id).SingleOrDefault().Expires = Expires;
                //Applicators.Where(x => x.PersonId == SelectedPerson.Id).SingleOrDefault().CompanyName = Company.Name;
            }
        }

        void Remove(ApplicationApplicator itemToRemove) {
            if (itemToRemove == null) { return; }

            ApplicationApplicator appApplicator = Applicators.Where(x => x.PersonId == itemToRemove.PersonId).SingleOrDefault();
            Applicators.Remove(appApplicator);
            RemovedApplicators.Add(appApplicator);
        }

        void Update() {

            if (IfBufferChange() && view != null)
            {
                ChangeApplicationEnvironmentalZoneTolerance bufferCommand = new ChangeApplicationEnvironmentalZoneTolerance(view.Id, clientEndpoint.GenerateNewMetadata(), (Decimal)BufferWidth, BufferWidthUnit.Name, SelectedBufferTolerance);
                clientEndpoint.SendOne(bufferCommand);
            }
            else if (IfBufferChange() && woView != null)
            {
                ChangeWorkOrderEnvironmentalZoneTolerance bufferCommand = new ChangeWorkOrderEnvironmentalZoneTolerance(woView.Id, clientEndpoint.GenerateNewMetadata(), (Decimal)BufferWidth, BufferWidthUnit.Name, SelectedBufferTolerance);
                clientEndpoint.SendOne(bufferCommand);
            }

            //check to see if new applicators exist and update the domain if new applicators have been added....

            foreach (ApplicationApplicator appApp in RemovedApplicators)
            {
                if (view != null && view.Applicators.Contains(appApp))
                {
                    //call message to remove the ApplicationApplicator
                    ApplicationApplicator app = appApp;
                    RemoveApplicatorFromApplication applicatorCommand = new RemoveApplicatorFromApplication(view.Id, clientEndpoint.GenerateNewMetadata(), app);
                    var applicators = parent.Applicators;
                    parent.Applicators.Remove(app);
                    clientEndpoint.SendOne(applicatorCommand);
                }

                if (woView != null && woView.Applicators.Contains(appApp))
                {
                    //remove wo applicators
                    RemoveApplicatorFromWorkOrder woCommand = new RemoveApplicatorFromWorkOrder(woView.Id, clientEndpoint.GenerateNewMetadata(), appApp.PersonId, appApp, ApplicationEnvironment.CurrentCropYear);
                    parent.Applicators.Remove(appApp);
                    clientEndpoint.SendOne(woCommand);
                }
            }

            foreach (ApplicationApplicator appApp in Applicators)
            {
                if (view !=null && !view.Applicators.Contains(appApp))
                {
                    //call message to add the ApplicationApplicator
                    AddApplicatorToApplication applicatorCommand = new AddApplicatorToApplication(view.Id, clientEndpoint.GenerateNewMetadata(), appApp);
                    parent.Applicators.Add(appApp);
                    clientEndpoint.SendOne(applicatorCommand);
                }

                if (woView != null && !woView.Applicators.Contains(appApp))
                {
                    //add wo applicators
                    AddApplicatorToWorkOrder woCommand = new AddApplicatorToWorkOrder(woView.Id, clientEndpoint.GenerateNewMetadata(), appApp.PersonId, appApp, ApplicationEnvironment.CurrentCropYear);
                    parent.Applicators.Add(appApp);
                    clientEndpoint.SendOne(woCommand);
                }
            }

            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
        }
    }

    public class PotentialApplicatorViewModel : ViewModelBase {
        IClientEndpoint clientEndpoint;
        bool isPerson;
        bool isDetailLoaded = false;
        string licenseNumber;
        DateTime expires = DateTime.Today;
        CompanyListItem companyListItem;
        PersonListItem personListItem;
        List<CompanyListItem> companyList;
        Action<PotentialApplicatorViewModel> onChanged;

        public PotentialApplicatorViewModel(IClientEndpoint clientEndpoint, PersonListItem person, List<CompanyListItem> companyList, Action<PotentialApplicatorViewModel> onChanged) {
            this.clientEndpoint = clientEndpoint;
            this.personListItem = person;
            this.companyList = companyList;
            this.onChanged = onChanged;

            IsPerson = true;
        }

        public PotentialApplicatorViewModel(IClientEndpoint clientEndpoint, CompanyListItem company, Action<PotentialApplicatorViewModel> onChanged) {
            this.clientEndpoint = clientEndpoint;
            this.companyListItem = company;
            this.companyList = new List<CompanyListItem>();
            this.onChanged = onChanged;

            IsPerson = false;
        }

        public List<CompanyListItem> Companies {
            get { return companyList; }
        }

        public PersonId PersonId {
            get {
                if (IsPerson) {
                    return personListItem.Id;
                } else {
                    return null;
                }
            }
        }

        public string Name {
            get {
                EnsureDetailsAreLoaded();
                if (IsPerson) {
                    return personListItem.Name;
                } else {
                    return companyListItem.Name;
                }
            }
        }

        public CompanyListItem Company {
            get {
                EnsureDetailsAreLoaded();
                return companyListItem; 
            }
            set {
                companyListItem = value;
                RaiseOnChanged();
                RaisePropertyChanged("Company");
            }
        }

        public bool IsPerson {
            get { return isPerson; }
            set {
                isPerson = value;
                RaisePropertyChanged("IsPerson");
            }
        }

        public DateTime Expires {
            get {
                EnsureDetailsAreLoaded();
                return expires; 
            }
            set {
                if (value == DateTime.MinValue) {
                    expires = DateTime.Today + TimeSpan.FromDays(1); // expires tomorrow
                } else {
                    expires = value;
                }
                RaiseOnChanged();
                RaisePropertyChanged("Expires");
            }
        }

        public string LicenseNumber {
            get {
                EnsureDetailsAreLoaded();
                return licenseNumber; 
            }
            set {
                licenseNumber = value;
                RaiseOnChanged();
                RaisePropertyChanged("LicenseNumber");
            }
        }

        void RaiseOnChanged() {
            if (onChanged != null) {
                onChanged(this);
            }
        }

        void EnsureDetailsAreLoaded() {
            if (!isDetailLoaded) {
                if (IsPerson) {
                    var personDetails = clientEndpoint.GetView<PersonDetailsView>(personListItem.Id);
                    if (personDetails.HasValue) {
                        Expires = personDetails.Value.ApplicatorLicenseExpiration;
                        LicenseNumber = personDetails.Value.ApplicatorLicenseNumber;
                        companyListItem = companyList.FirstOrDefault(x => x.Id == personDetails.Value.CompanyId);
                        if (companyListItem == null) { // try to find on name if ID can't be matched
                            companyListItem = companyList.FirstOrDefault(x => x.Name == personDetails.Value.CompanyName);
                        }
                    }
                }

                isDetailLoaded = true;
            }
        }

    }
}
