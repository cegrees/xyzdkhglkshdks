﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Map;
using Landdb.Domain.ReadModels.WorkOrder;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Secondary;
using Landdb.ViewModel.Secondary.Application;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using Landdb.Resources;
using Landdb.ViewModel.Interfaces;
using Landdb.ViewModel.Production.Applications;

namespace Landdb.ViewModel.Production.Popups {
    public class ChangeTimeViewModel : ViewModelBase, IEnvironmentConditionsProperty {
        DateTime originalStartTime;
        TimeSpan? originalDuration;
        DateTime? originalEndTime;
        DateTime startTime;
        DateTime startDate;
        DurationViewModel selectedDuration;
        DateTime? endTime;
        DateTime? endDate;
        TimingEvent timingEvent;
        string timingEventTag;
        string selectedTimingEventTag;
        List<string> timingEventTags;
        Dictionary<string, List<string>> availableTimingEventTags;
        Dictionary<string, List<string>> tempTimingEventTags;
        Dictionary<string, List<string>> mergedTimingEventTags;
        //ApplicationConditionsViewModel conditionsModel;
        ClearAgWeatherConditionsViewModel conditionsModel;
        ApplicationView view;
        WorkOrderView woView;
        IClientEndpoint endpoint;
        Dispatcher dispatch;
        ApplicationDetailsViewModel parent;
        bool enableSaveButton = false;

        public ChangeTimeViewModel(DateTime originalStartTime, TimeSpan originalDuration, DateTime originalEndTime, ApplicationView view, IClientEndpoint endpoint, Dispatcher dispatch, ApplicationDetailsViewModel parent) {
            EnvironmentConditions = new EnvironmentConditions();
            this.view = view;
            this.endpoint = endpoint;
            this.dispatch = dispatch;
            this.parent = parent;

            InitializeDurations();
            TimingEvents = endpoint.GetMasterlistService().GetTimingEventList().OrderBy(x => x.Order).ToList();
            TimingEvent = TimingEvents.Where(x => x.Name == view.TimingEvent).FirstOrDefault();
            TimingEventTag = view.TimingEventTag;
            RaisePropertyChanged("TimingEvents");
            RaisePropertyChanged("TimingEvent");
            RaisePropertyChanged("TimingEventTag");

            this.originalStartTime = originalStartTime;
            this.StartTime = originalStartTime;
            this.StartDate = originalStartTime;
            ShowDuration = true;
            ResetWeather = false;
            UpdateWeather = false;

            var q = from d in Durations
                    where d.Duration == originalDuration
                    select d;

            if (q.Any()) {
                SelectedDuration = q.First();

                EndTime = null;
                EndDate = null;
                this.originalDuration = originalDuration;
                this.originalEndTime = null;
            } else {
                this.originalEndTime = originalEndTime;
                SelectedDuration = Durations.Last();
                EndTime = originalEndTime.ToLocalTime();
                EndDate = originalEndTime.ToLocalTime();
                this.originalDuration = null;
                
            }
            enableSaveButton = false;
            EnableSaveButton = CheckValidity();

            CompleteCommand = new RelayCommand(OnComplete);
            CancelCommand = new RelayCommand(OnCancel);

            conditionsModel = new ClearAgWeatherConditionsViewModel(endpoint, dispatch, view.CropYear);
        }

        public ChangeTimeViewModel(DateTime originalStartTime, WorkOrderView woView, IClientEndpoint endpoint, Dispatcher dispatch, ApplicationDetailsViewModel parent)
        {
            this.woView = woView;
            this.endpoint = endpoint;
            this.dispatch = dispatch;
            this.parent = parent;

            InitializeDurations();

            TimingEvents = endpoint.GetMasterlistService().GetTimingEventList().OrderBy(x => x.Order).ToList();
            TimingEvent = TimingEvents.Where(x => x.Name == woView.TimingEvent).FirstOrDefault();
            TimingEventTag = woView.TimingEventTag;
            RaisePropertyChanged("TimingEvents");
            RaisePropertyChanged("TimingEvent");
            RaisePropertyChanged("TimingEventTag");
            this.originalStartTime = originalStartTime;
            this.StartTime = originalStartTime;
            this.StartDate = originalStartTime;
            ResetWeather = false;
            UpdateWeather = false;
            SelectedDuration = new DurationViewModel(new TimeSpan());
            ShowDuration = false;

            enableSaveButton = false;
            EnableSaveButton = CheckValidity();

            CompleteCommand = new RelayCommand(OnComplete);
            CancelCommand = new RelayCommand(OnCancel);

            conditionsModel = new ClearAgWeatherConditionsViewModel(endpoint, dispatch, woView.CropYear);
        }

        public RelayCommand CompleteCommand { get; private set; }
        public RelayCommand CancelCommand { get; private set; }

        public DateTime StartDate
        {
            get { return startDate; }
            set
            {
                startDate = value;
                RaisePropertyChanged("StartDate");

                if (view != null) { UpdateFromStation(StartDate.Date + StartTime.TimeOfDay); }
                EnableSaveButton = CheckValidity();
            }
        }

        public bool ShowDuration { get; set; }

        public bool EnableSaveButton {
            get { return enableSaveButton; }
            set {
                enableSaveButton = value;
                RaisePropertyChanged("EnableSaveButton");
            }
        }

        public DateTime StartTime {
            get { return startTime; }
            set {
                startTime = value;
                RaisePropertyChanged("StartTime");
                EnableSaveButton = CheckValidity();
                if (view != null) { UpdateFromStation(StartDate.Date + StartTime.TimeOfDay); }
            }
        }

        public DateTime? EndTime {
            get { return endTime; }
            set {
                endTime = value;
                EnableSaveButton = CheckValidity();
                RaisePropertyChanged("EndTime");
            }
        }

        public DateTime? EndDate
        {
            get { return endDate; }
            set
            {
                endDate = value;
                EnableSaveButton = CheckValidity();
                RaisePropertyChanged("EndDate");
            }
        }

        public ObservableCollection<DurationViewModel> Durations { get; private set; }

        public DurationViewModel SelectedDuration {
            get {
                if (selectedDuration == null) { return new DurationViewModel(null); }
                return selectedDuration; 
            }
            set {
                //if (selectedDuration == value) { return; }
                selectedDuration = value;
                EnableSaveButton = CheckValidity();
                RaisePropertyChanged("SelectedDuration");
            }
        }

        public List<TimingEvent> TimingEvents { get; private set; }

        public TimingEvent TimingEvent {
            get { return timingEvent; }
            set {
                timingEvent = value;
                SelectAppropriateTags();
                RaisePropertyChanged("TimingEvent");
            }
        }

        public Dictionary<string, List<string>> AvailableTimingEventTags {
            get {
                return availableTimingEventTags;
            }
            set {
                availableTimingEventTags = value;
                SelectAppropriateTags();
            }
        }

        public Dictionary<string, List<string>> TempTimingEventTags {
            get {
                return tempTimingEventTags;
            }
            set {
                tempTimingEventTags = value;
                SelectAppropriateTags();
                RaisePropertyChanged("TempTimingEventTags");
            }
        }

        public List<string> TimingEventTags {
            get {
                if (timingEventTags == null) {
                    return new List<string>();
                }
                return timingEventTags.OrderBy(q => q).ToList();
            }
            set {
                timingEventTags = value;
                RaisePropertyChanged("TimingEventTags");
            }
        }

        public string TimingEventTag {
            get { return timingEventTag; }
            set {
                if (timingEventTag == value) { return; }
                timingEventTag = value;
                RaisePropertyChanged("TimingEventTag");
            }
        }

        public string SelectedTimingEventTag {
            get { return selectedTimingEventTag; }
            set {
                if (selectedTimingEventTag == value) { return; }
                selectedTimingEventTag = value;
                if (!string.IsNullOrEmpty(selectedTimingEventTag)) {
                    timingEventTag = selectedTimingEventTag;
                }
                RaisePropertyChanged("SelectedTimingEventTag");
            }
        }

        private void SelectAppropriateTags() {
            if (timingEvent != null && !string.IsNullOrEmpty(timingEvent.Name)) {
                mergedTimingEventTags = availableTimingEventTags;
                if (tempTimingEventTags != null && tempTimingEventTags.Count > 0) {
                    mergedTimingEventTags = availableTimingEventTags.Concat(tempTimingEventTags).GroupBy(d => d.Key).ToDictionary(d => d.Key, d => d.First().Value);
                }

                if (mergedTimingEventTags != null && mergedTimingEventTags.ContainsKey(timingEvent.Name)) {
                    TimingEventTags = mergedTimingEventTags[timingEvent.Name];
                }
                else {
                    TimingEventTags = new List<string>();
                }
            }
        }

        public bool HasChanges {
            get {
                if (view != null)
                {
                    return originalStartTime != StartDate.Date + StartTime.TimeOfDay || originalDuration != SelectedDuration.Duration || originalEndTime != EndTime || originalEndTime != EndDate
                        || (TimingEvent == null && !string.IsNullOrWhiteSpace(view.TimingEvent)) || (TimingEvent != null && TimingEvent.Name != view.TimingEvent) || (TimingEventTag != null && TimingEventTag != view.TimingEventTag);
                }
                else
                {
                    return originalStartTime != StartDate.Date + StartTime.TimeOfDay || originalDuration != SelectedDuration.Duration || originalEndTime != EndTime
                        || (TimingEvent == null && !string.IsNullOrWhiteSpace(view.TimingEvent)) || (TimingEvent != null && TimingEvent.Name != view.TimingEvent) || (TimingEventTag != null && TimingEventTag != view.TimingEventTag);
                }
            }
        }

        private DateTime SetDateTime(DateTime date, DateTime time)
        {
            var hours = date.Hour;
            var min = date.Minute;
            var sec = date.Second;
            var milsec = date.Millisecond;

            var newDate = date.AddHours(-hours).AddMinutes(-min).AddSeconds(-sec).AddMilliseconds(-milsec);
            newDate = newDate.AddHours(time.Hour).AddMinutes(time.Minute).AddSeconds(time.Second).AddMilliseconds(time.Millisecond);

            var local = newDate.ToLocalTime();

            return newDate;
        }

        private bool CheckValidity() {
            if(StartDate == null) {
                return false;
            }
            if (view != null) {
                if (SelectedDuration.IsCustom) {
                    if (!EndDate.HasValue) {
                        EndDate = StartDate;
                        EndTime = StartTime.AddHours(4.0);
                        return false;
                    }
                    if (StartTime == null) {
                        return false;
                    }
                    if (EndDate.HasValue || EndDate < StartDate) {
                        var enddate01 = EndDate.Value.Date;
                        var startdate01 = StartDate.Date;
                        if (enddate01 < startdate01) {
                            return false;
                        }
                        if (enddate01 == startdate01) {
                            DateTime sdate = StartDate.Date + StartTime.TimeOfDay;
                            if (EndDate.HasValue && EndTime.HasValue) {
                                DateTime end = new DateTime();
                                end = SetDateTime(EndDate.Value, EndTime.Value);
                                if (end < sdate) {
                                    return false;
                                }
                            }
                        }
                    }
                }
            }
            return true;
        }

        void OnComplete()
        {
            if (HasChanges)
            {
                if (view != null)
                {
                    DateTime end = new DateTime();
                    if (SelectedDuration.IsCustom)
                    {
                        EndDate = EndDate == null ? StartTime.Date : EndDate;

                        if (EndDate.HasValue && EndTime.HasValue)
                        {
                            end = SetDateTime(EndDate.Value, EndTime.Value);
                        }
                    }

                    DateTime start = SetDateTime(StartDate, StartTime);

                    ChangeApplicationTime c = new ChangeApplicationTime(view.Id, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, endpoint.DeviceId, endpoint.UserId), start.ToUniversalTime(), SelectedDuration.Duration, SelectedDuration.IsCustom ? (DateTime?)end.ToUniversalTime() : null);

                    TimeSpan? duration1 = SelectedDuration.Duration;
                    if (SelectedDuration.IsCustom)
                    {
                        if (EndDate.HasValue && EndTime.HasValue)
                        {
                            duration1 = end - start;
                        }
                    }
                    if (end.Year == 1 && duration1 == null)
                    {
                        end = start;
                        c = new ChangeApplicationTime(view.Id, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, endpoint.DeviceId, endpoint.UserId), start.ToUniversalTime(), SelectedDuration.Duration, SelectedDuration.IsCustom ? (DateTime?)start.ToUniversalTime() : null);
                    }

                    //ChangeApplicationTime c = new ChangeApplicationTime(view.Id, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, endpoint.DeviceId, endpoint.UserId), start.ToUniversalTime(), SelectedDuration.Duration, SelectedDuration.IsCustom ? (DateTime?)end.ToUniversalTime() : null);
                    endpoint.SendOne(c);
                    parent.StartDateTime = StartDate.ToString(@"MMMM d, yyyy");
                    TimeSpan? duration = SelectedDuration.Duration;
                    if (SelectedDuration.IsCustom) {
                        if (EndDate.HasValue && EndTime.HasValue) {
                            duration = end - start;
                        }
                    }

                    parent.TimeDisplay = AbstractApplicationDetailsViewModel.CalculateTimeDisplay(start, duration, (DateTime?)end.ToUniversalTime());

                    if (UpdateWeather)
                    {
                        ApplicationConditions conditions = EnvironmentConditions.CreateApplicationConditionsFromCurrentWeatherInfo(null, null, StartDate.Date + StartTime.TimeOfDay, null);
                        ChangeApplicationConditions infoCommand = new ChangeApplicationConditions(view.Id, endpoint.GenerateNewMetadata(), conditions);
                        endpoint.SendOne(infoCommand);
                    }

                    var timing = TimingEvent != null ? TimingEvent.Name : string.Empty;
                    if (!string.IsNullOrEmpty(timing) && view.TimingEvent != timing)
                    {
                        UpdateTimingEventOnApplication timingCommand = new UpdateTimingEventOnApplication(view.Id, endpoint.GenerateNewMetadata(), TimingEvent.Name, TimingEventTag);
                        endpoint.SendOne(timingCommand);
                    }
                }
                else
                {
                    DateTime start = SetDateTime(StartDate, StartTime);

                    ChangeWorkOrderStartTime c = new ChangeWorkOrderStartTime(woView.Id, new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, endpoint.DeviceId, endpoint.UserId), start.ToUniversalTime());
                    endpoint.SendOne(c);
                    parent.StartDateTime = StartDate.ToString(@"MMMM d, yyyy");
                    
                    var timing = TimingEvent != null ? TimingEvent.Name : string.Empty;
                    if (!string.IsNullOrEmpty(timing) && woView.TimingEvent != timing)
                    {
                        UpdateTimingEventOnWorkOrder timingCommand = new UpdateTimingEventOnWorkOrder(woView.Id, endpoint.GenerateNewMetadata(), TimingEvent.Name, TimingEventTag);
                        endpoint.SendOne(timingCommand);
                    }
                }
            }
            
            Messenger.Default.Send(new HidePopupMessage());
        }

        void OnCancel()
        {
            Messenger.Default.Send(new HidePopupMessage());
        }

        void InitializeDurations() {
            Durations = new ObservableCollection<DurationViewModel>() {
                new DurationViewModel(TimeSpan.FromHours(4)),
                new DurationViewModel(TimeSpan.FromHours(3.5)),
                new DurationViewModel(TimeSpan.FromHours(3)),
                new DurationViewModel(TimeSpan.FromHours(2.5)),
                new DurationViewModel(TimeSpan.FromHours(2)),
                new DurationViewModel(TimeSpan.FromHours(1.5)),
                new DurationViewModel(TimeSpan.FromHours(1)),
                new DurationViewModel(TimeSpan.FromHours(0.5)),
                new DurationViewModel(TimeSpan.FromHours(0)),
                new DurationViewModel(null)
            };

            SelectedDuration = Durations[4];  // TODO: Maybe remember last used and default to it?
        }

        public bool ResetWeather { get; set; }
        public bool UpdateWeather { get; set; }

        public EnvironmentConditions EnvironmentConditions { get; set; }
        public WeatherStationItem SelectedStation { get; set; }

        private void UpdateFromStation(DateTime updatedDateTime)
        {
            if (view.CropZones.Any() && conditionsModel != null)
            {
                var czs = view.CropZones;

                //get center lat lon of the included fields then set conditions lookuppoint
                //string mapData = null;
                List<string> mapShapeStrings = new List<string>();
                foreach (var cz in czs)
                {
                    var mapItem = endpoint.GetView<ItemMap>(cz.Id);

                    if (mapItem.HasValue && mapItem.Value.MostRecentMapItem != null && mapItem.Value.MostRecentMapItem.MapData != "MULTIPOLYGON EMPTY")
                    {
                        var mapString = mapItem.Value.MostRecentMapItem.MapData;
                        mapString = mapString.Substring(13, (mapString.Length - 14));
                        mapShapeStrings.Add(mapString);
                        //mapData += mapItem.Value.MostRecentMapItem.MapData;
                    }
                }


                if (mapShapeStrings.Any())
                {
                    var multiString = string.Join(",", mapShapeStrings.ToArray());
                    var multiPolygonString = string.Format("Multipolygon({0})", multiString);
                    var shape = ThinkGeo.MapSuite.Core.PolygonShape.CreateShapeFromWellKnownData(multiPolygonString);
                    var center = shape.GetCenterPoint();
                    conditionsModel.GeoLookupPoint = center;
                    //conditionsModel.GeoLookupPoint = center; // string.Format("{0},{1}", center.Y.ToString("n1"), center.X.ToString("n1"));
                }

                conditionsModel.ApplicationStartDate = updatedDateTime;

                EnvironmentConditions.Digest(conditionsModel);

                RaisePropertyChanged(() => EnvironmentConditions.WindText);
                ResetWeather = true;
                RaisePropertyChanged(() => ResetWeather);
            }
        }
    }
}
