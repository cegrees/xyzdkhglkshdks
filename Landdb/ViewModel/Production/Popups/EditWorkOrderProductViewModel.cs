﻿using AgC.UnitConversion;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Contract;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Domain.ReadModels.Product;
using Landdb.Domain.ReadModels.ProductSetting;
using Landdb.Domain.ReadModels.WorkOrder;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ValueObjects;
using Landdb.ViewModel.Secondary.Application;
using Landdb.ViewModel.Secondary.PopUp;
using Landdb.ViewModel.Secondary.UserCreatedProduct;
using Landdb.ViewModel.Shared;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Resources;
using Landdb.ViewModel.Production.Applications;

namespace Landdb.ViewModel.Production.Popups {
	public class EditWorkOrderProductViewModel : ValidationViewModelBase {
		Guid trackingId;
		ProductListItemDetails selectedProduct;

		// Rate per area values
		decimal ratePerAreaValue;
		UnitPerAreaHelper ratePerAreaUnitHelper;

		// Total product values
		decimal totalProductValue;
		IUnit totalProductUnit;

		// Rate per tank values
		decimal ratePerTankValue;
		IUnit ratePerTankUnit;

		decimal growerSplitPercent;
		decimal shareOwnerSplitPercent;
		decimal percentFromGrowerInventory = 1.0m;
		decimal growerSharePercent;
		Measure totalProductFromGrowerInventory;

		decimal costValuePerUnit; // TODO: Make this a Currency class when we have a good one built.
		IUnit costUnit;
		decimal totalCost = 0; // TODO: Make this a Currency class when we have a good one built.

		decimal productAreaPercent = 1m;
		Measure productArea;
		InventoryListView inventory;
		PriceItem averagePriceItem;
		InventoryListItem currentInventoryItem;
		ApplicationMethod appMethod;
		RentContractDetailsView contractDetails;
		IApplicationCalculationService applicationCalculationService;
		IApplicationCommandService applicationCommandService;

		bool isProductListVisible = false;
		bool isCreateProductPopupOpen = false;

		Measure totalArea;

		ApplicationDetailsViewModel parent;
		ApplicationProductDetails details;
		WorkOrderView woView;
		IClientEndpoint endpoint;
		Dispatcher dispatcher;
		ApplicationTypes applyBy;
		string selectedPriceText;

		public EditWorkOrderProductViewModel(IClientEndpoint endpoint, Dispatcher dispatcher, WorkOrderView view, ApplicationDetailsViewModel parent, ApplicationProductDetails details) {
			this.endpoint = endpoint;
			this.dispatcher = dispatcher;
			this.parent = parent;
			this.woView = view;
			AllowedRateUnits = new ObservableCollection<UnitPerAreaHelper>();
			AllowedTotalUnits = new ObservableCollection<IUnit>();
			PriceItems = new ObservableCollection<PriceItem>();
			Products = new ObservableCollection<ProductListItemDetails>();

			this.AddProduct = details == null ? true : false;
			inventory = endpoint.GetView<InventoryListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear)).GetValue(new InventoryListView());
			this.applicationCalculationService = new ApplicationCalculationService();
			this.applicationCommandService = new ApplicationCommandService();
			var masterlist = endpoint.GetMasterlistService();
			var masterlistProd = details != null ? masterlist.GetProduct(details.ID) : null;
			Density = masterlistProd != null ? masterlistProd.Density : null;

			Contract = parent.Contracts.Values.FirstOrDefault();

            Task popPest = Task.Factory.StartNew(PopulatePests);
			Task.Run(() => { BuildProductList(); });

			if (AddProduct) {
				ApplyBy = (ApplicationTypes)view.ApplicationStrategy;
				//Task.Run(() => { BuildProductList(); });
				ProductAreaPercent = 1.0m;
				trackingId = Guid.NewGuid();
				totalArea = view.TotalArea;
				ProductArea = totalArea;
				AllowEdit = true;
                AssociatedProducts = new ObservableCollection<AssociatedProductItem>();
			} else {
				this.details = details;
				ApplyBy = (ApplicationTypes)woView.ApplicationStrategy;
				var displayRate = DisplayMeasureConverter.ConvertToRateDisplayMeasure(details.ID, (decimal)details.RateMeasure.Value, details.RateMeasure.Unit.Name, masterlistProd.Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);
				var displayTotal = DisplayMeasureConverter.ConvertToTotalDisplayMeasure(details.ID, (decimal)details.TotalProduct.Value, details.TotalProduct.Unit.Name, masterlistProd.Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);
				var displayRatePerTank = DisplayMeasureConverter.ConvertToRatePerTankDisplayMeasure(details.ID, (decimal)details.RatePerTank.Value, details.RatePerTank.Unit.Name, masterlistProd.Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);
				AllowEdit = false;
				totalArea = woView.TotalArea;
				//THIS HAS TO BE THE DATASOURCE ID FOR THE PRODUCT SO THAT IT HANDLES USER CREATED PRODUCTS AND MASTERLIST PRODUCTS
				Guid mlDsId = details.ID.DataSourceId;
				SelectedProduct = new ProductListItemDetails(endpoint.GetMasterlistService().GetProduct(details.ID), mlDsId, Strings.UsedThisYear_Text, (short)0);

				RatePerTankValue = details.RatePerTank != null ? (decimal)displayRatePerTank.Value : 0m;
				RatePerAreaValue = (decimal)displayRate.Value;
				//var rateSafeUnit = UnitFactory.GetPackageSafeUnit(details.RateMeasure.Unit.Name, SelectedProduct.Product.StdUnit, SelectedProduct.Product.StdFactor, SelectedProduct.Product.StdPackageUnit);
				SelectedRatePerAreaUnitHelper = new UnitPerAreaHelper(displayRate.Unit, woView.TotalArea.Unit);
				//SelectedPriceText = details.CostPerUnitValue.ToString();
				ProductAreaPercent = details.ProductAreaPercent;
				ProductArea = totalArea;
				ApplicationMethod = details.ApplicationMethod;
				var ratePerTankUnit = details.RatePerTank != null ? displayRatePerTank.Unit.Name : SelectedProduct.Product.StdPackageUnit;
				var ratePerTankSafeUnit = UnitFactory.GetPackageSafeUnit(ratePerTankUnit, SelectedProduct.Product.StdUnit, SelectedProduct.Product.StdFactor, SelectedProduct.Product.StdPackageUnit);
				SelectedRatePerTankUnit = ratePerTankSafeUnit;
				GrowerSharePercent = details.GrowerShare;
				PercentFromGrowerInventory = details.FromGrowerInventory;
				TotalProductUnit = displayTotal.Unit;
				TotalProductValue = (decimal)displayTotal.Value;
                
				if (details.ProductAreaPercent == 0) {
					productAreaPercent = 1.0m;
				} else {
					ProductAreaPercent = details.ProductAreaPercent;
				}
				var invProd = inventory.Products.FirstOrDefault(x => x.Key == SelectedProduct.ProductId);
				currentInventoryItem = invProd.Value != null ? invProd.Value : null;

                popPest.Wait();
				SelectedPest = details.TargetPest != null ? PestList.FirstOrDefault(x => x.CommonName == details.TargetPest.Name && x.LatinName == details.TargetPest.LatinList) : null;
                
				UpdatePriceItems();
				trackingId = details.TrackingId;

                AssociatedProducts = new ObservableCollection<AssociatedProductItem>();
                foreach (var associate in details.AssociatedProducts)
                {
                    var newAssociate = new AssociatedProductItem(endpoint, masterlistProd, RemoveAssociatedProduct);

                    newAssociate.CustomUnit = associate.CustomUnit;
                    newAssociate.CustomRateValue = associate.CustomRateValue;
                    newAssociate.HasCost = associate.HasCost;
                    newAssociate.CostPerUnitValue = associate.HasCost ? associate.CostPerUnitValue : 0m;
                    newAssociate.CostPerUnitUnit = associate.CostPerUnitUnit;
                    newAssociate.ProductID = associate.ProductID;
                    newAssociate.ProductName = associate.ProductName;
                    newAssociate.RatePerAreaUnit = associate.RatePerAreaUnit;
                    newAssociate.RatePerAreaValue = associate.RatePerAreaValue;
                    newAssociate.TotalProductUnit = associate.TotalProductUnit;
                    newAssociate.TotalProductValue = associate.TotalProductValue;
                    newAssociate.TrackingID = associate.TrackingID;

                    switch (associate.CustomRateType.ToString())
                    {
                        case "ByBag":
                            newAssociate.CustomRateType = AssociatedProductRateType.ByBag;
                            break;
                        case "ByCWT":
                            newAssociate.CustomRateType = AssociatedProductRateType.ByCWT;
                            break;
                        case "ByRow":
                            newAssociate.CustomRateType = AssociatedProductRateType.ByRow;
                            break;
                        case "BySeed":
                            newAssociate.CustomRateType = AssociatedProductRateType.BySeed;
                            break;
                        default:
                            newAssociate.CustomRateType = AssociatedProductRateType.BySeed;
                            break;
                    }

                    AssociatedProducts.Add(newAssociate);
                }

                RaisePropertyChanged(() => SelectedPest);
                RaisePropertyChanged(() => AssociatedProducts);
                RaisePropertyChanged(() => HasAssociatedProducts);
            }

			AllowDelete = !AddProduct && view.Products.Count > 1;

			CreateUserProductCommand = new RelayCommand(CreateUserProduct);

			Cancel = new RelayCommand(CancelEdit);
			Update = new RelayCommand(UpdateProduct);
			Delete = new RelayCommand(DeleteProduct);
            AddAssociatedProduct = new RelayCommand(AssociateProductAddition);

            ValidateViewModel();
		}

		public ICommand Cancel { get; private set; }
		public ICommand Update { get; private set; }
		public ICommand Delete { get; private set; }
        public ICommand AddAssociatedProduct { get; set; }

        public ICommand CreateUserProductCommand { get; private set; }

		public ObservableCollection<PestListItemDetails> PestList { get; private set; }
		public ObservableCollection<UnitPerAreaHelper> AllowedRateUnits { get; private set; }
		public ObservableCollection<IUnit> AllowedTotalUnits { get; private set; }

		public string ProductSearchText { get; set; }

		public bool AllowEdit { get; set; }
		public double? Density { get; set; }
		public bool AddProduct { get; set; }
		public bool AllowDelete { get; set; }

		public List<ApplicationMethod> ApplicationMethods { get { return endpoint.GetMasterlistService().GetApplicationMethodList().ToList(); } }

		public ObservableCollection<ProductListItemDetails> Products { get; private set; }
		public ObservableCollection<PriceItem> PriceItems { get; private set; }
		public PriceItem SelectedPriceItem { get; set; }
		public decimal FromGrowerInventory { get; set; }

		private PestListItemDetails _selectedPest;
		[CustomValidation(typeof(EditWorkOrderProductViewModel), nameof(ValidateSelectedPest))]
		public PestListItemDetails SelectedPest {
			get { return _selectedPest; }
			set {
				_selectedPest = value;
				ValidateAndRaisePropertyChanged(() => SelectedPest);
			}
		}

		private string _pestSearchText = string.Empty;
		public string PestSearchText {
			get { return _pestSearchText; }
			set {
				_pestSearchText = value;
				ValidateAndRaisePropertyChanged(() => SelectedPest);
			}
		}

		public ApplicationMethod ApplicationMethod {
			get { return appMethod; }
			set {
				appMethod = value;
				RaisePropertyChanged(() => ApplicationMethod);
			}
		}

		public ApplicationTypes ApplyBy {
			get { return applyBy; }
			set {
				applyBy = value;
				RaisePropertyChanged(() => ApplyBy);
			}
		}

		[CustomValidation(typeof(EditAppliedProductViewModel), nameof(ValidateSelectedProduct))]
		public ProductListItemDetails SelectedProduct {
			get { return selectedProduct; }
			set {
				selectedProduct = value;
				ValidateAndRaisePropertyChanged(() => SelectedProduct);
				RaisePropertyChanged(() => DisplayName);
				RaisePropertyChanged(() => ApplicationMethod);
				UpdateAllowedUnits();

				if (value != null && value.Product != null && value.Product.ProductType == GlobalStrings.ProductType_Service && value.Product.StdPackageUnit == ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit) {
					RatePerAreaValue = 1; // default to 1 for acre-based services because they're almost always "1 acre". Should probably extend this to other area units as well, for intl.
				} else {
					RatePerAreaValue = 0;  // TODO: In the future, this should be the most recently used rate. We should also populate a list of the MR rates for this product.
				}

				if (AddProduct && SelectedProduct != null) {
					var invProd = inventory.Products.FirstOrDefault(x => x.Key == SelectedProduct.ProductId);
					currentInventoryItem = invProd.Value != null ? invProd.Value : null;
					UpdateInventoryListItem();
					ResetContractSpecificInfo();
					UpdatePercentFromGrowerInventory();
					UpdateGrowerShare();
					UpdatePriceItems();
					RecalculateItem();

				}

				if (value == null || value.Product == null) {
					IsCreateProductPopupOpen = true;
				}
			}
		}

		public string DisplayName {
			get {
				if (SelectedProduct == null) { return string.Empty; }
				return string.Format("{0} ({1})", SelectedProduct.Product.Name, SelectedProduct.Product.Manufacturer);
			}
		}

		public string SelectedPriceText {
			get { return selectedPriceText; }
			set {
				if (string.IsNullOrWhiteSpace(value)) { return; }

				if (selectedPriceText != value && (AveragePriceItem == null || value != AveragePriceItem.ToString())) {
					selectedPriceText = value;
					decimal price = 0m;
					string concatString = Regex.Replace(selectedPriceText, "[/$A-Za-z]", "");
					concatString = concatString.Last().ToString() == "." ? concatString.Remove(concatString.Length - 1) : concatString;
					if (decimal.TryParse(concatString, out price)) {
						if (AddProduct && SelectedProduct != null) {
							var priceUnit = currentInventoryItem != null && !string.IsNullOrWhiteSpace(currentInventoryItem.AveragePriceUnit) ? currentInventoryItem.AveragePriceUnit : selectedProduct.Product.StdPackageUnit;
							PriceItem priceItem = new PriceItem(price, priceUnit);
							PriceItems.Add(priceItem);
							AveragePriceItem = priceItem;
							selectedPriceText = priceItem.ToString();
						} else {
							var psu = UnitFactory.GetPackageSafeUnit(SelectedProduct.Product.StdPackageUnit, SelectedProduct.Product.StdUnit, SelectedProduct.Product.StdFactor, SelectedProduct.Product.StdPackageUnit);
							var priceUnit = details.CostUnit != null ? details.CostUnit : psu;
							PriceItem priceItem = new PriceItem(price, priceUnit.Name, false);
							PriceItems.Add(priceItem);
							AveragePriceItem = priceItem;
							selectedPriceText = priceItem.ToString();
						}
					}
				}
			}
		}

        [Range(typeof(decimal), "0", "1", ErrorMessageResourceName = "ValueMustBeBetweenZeroAndOneHundred_Text", ErrorMessageResourceType = typeof(Strings))]
        public decimal GrowerSharePercent {
			get { return growerSharePercent; }
			set {
				growerSharePercent = value;
				ValidateAndRaisePropertyChanged(() => GrowerSharePercent);
			}
		}

		[CustomValidation(typeof(EditWorkOrderProductViewModel), nameof(ValidateRate))]
		public decimal RatePerAreaValue {
			get { return ratePerAreaValue; }
			set {
				ratePerAreaValue = value;
				RaisePropertyChanged(() => RatePerAreaValue);
				RaisePropertyChanged(() => RatePerAreaDisplay);

				if (applyBy == ApplicationTypes.RatePerArea) {
					RecalculateItem();
				}
			}
		}

		[CustomValidation(typeof(EditWorkOrderProductViewModel), nameof(ValidateUnit))]
		public UnitPerAreaHelper SelectedRatePerAreaUnitHelper {
			get { return ratePerAreaUnitHelper; }
			set {
				ratePerAreaUnitHelper = value;
				RaisePropertyChanged(() => SelectedRatePerAreaUnitHelper);
				RaisePropertyChanged(() => RatePerAreaDisplay);

				if (applyBy == ApplicationTypes.RatePerArea) {
					RecalculateItem();
				}
			}
		}

		[CustomValidation(typeof(EditWorkOrderProductViewModel), nameof(ValidateRate))]
		public decimal RatePerTankValue {
			get { return ratePerTankValue; }
			set {
				ratePerTankValue = value;
				RaisePropertyChanged(() => RatePerTankValue);
				RaisePropertyChanged(() => RatePerTankDisplay);

				if (applyBy == ApplicationTypes.RatePerTank) {
					RecalculateItem();
				}
			}
		}

		[CustomValidation(typeof(EditWorkOrderProductViewModel), nameof(ValidateUnit))]
		public IUnit SelectedRatePerTankUnit {
			get { return ratePerTankUnit; }
			set {
				ratePerTankUnit = value;
				RaisePropertyChanged(() => SelectedRatePerTankUnit);
				RaisePropertyChanged(() => RatePerTankDisplay);

				if (applyBy == ApplicationTypes.RatePerTank) {
					RecalculateItem();
				}
			}
		}

		public PriceItem AveragePriceItem {
			get { return averagePriceItem; }
			set {
				averagePriceItem = value;
				RaisePropertyChanged(() => AveragePriceItem);
				RecalculateItem();
			}
		}

		public decimal CostValuePerUnit {
			get { return costValuePerUnit; }
			set {
				costValuePerUnit = value;
				RaisePropertyChanged(() => CostValuePerUnit);
				RecalculateItem();
			}
		}

		public IUnit CostUnit {
			get { return costUnit; }
			set {
				costUnit = value;
				RaisePropertyChanged(() => CostUnit);
			}
		}

		public string RatePerAreaDisplay {
			get {
				if (ratePerAreaUnitHelper == null) { return string.Empty; }
				var rateString = string.Format("{0} / {1}", ratePerAreaUnitHelper.Unit.GetMeasure((double)ratePerAreaValue, SelectedProduct.Product.Density).ToString(), ratePerAreaUnitHelper.AreaUnit.FullDisplay);
				return rateString;
			}
		}

		public string RatePerTankDisplay {
			get {
				if (ratePerTankUnit == null) { return string.Empty; }
				var rateString = ratePerTankUnit.GetMeasure((double)ratePerTankValue, SelectedProduct.Product.Density).ToString();
				return rateString;
			}
		}

		[CustomValidation(typeof(EditWorkOrderProductViewModel), nameof(ValidateRate))]
		public decimal TotalProductValue {
			get { return totalProductValue; }
			set {
				totalProductValue = value;
				RaisePropertyChanged(() => TotalProductValue);
				RaisePropertyChanged(() => TotalProduct);

				if (applyBy == ApplicationTypes.TotalProduct) {
					RecalculateItem();
				}
			}
		}

		[CustomValidation(typeof(EditWorkOrderProductViewModel), nameof(ValidateUnit))]
		public IUnit TotalProductUnit {
			get { return totalProductUnit; }
			set {
				totalProductUnit = value;
				RaisePropertyChanged(() => TotalProductUnit);
				RaisePropertyChanged(() => TotalProduct);

				if (applyBy == ApplicationTypes.TotalProduct) {
					RecalculateItem();
				}
			}
		}

		public Measure TotalProduct {
			get {
				if (totalProductUnit != null && !(TotalProductUnit is NullUnit) && SelectedProduct != null) {
					return totalProductUnit.GetMeasure((double)totalProductValue, SelectedProduct.Product.Density);
				} else {
					return null;
				}
			}
		}

		public decimal TotalCost {
			get { return totalCost; }
			set {
				totalCost = value;
				RaisePropertyChanged(() => TotalCost);
				RaisePropertyChanged(() => TotalGrowerCost);
			}
		}

		public decimal TotalGrowerCost {
			get {
				if (IsRentContractAssociated) {
					//return TotalCost * PercentFromGrowerInventory;
					return TotalCost * GrowerSplitPercent;
				} else {
					return TotalCost;
				}
			}
		}

		public decimal ProductAreaPercent {
			get { return productAreaPercent; }
			set {
				productAreaPercent = value;
				RaisePropertyChanged(() => ProductAreaPercent);
				RecalculateItem();
			}
		}

		public RentContractDetailsView Contract {
			get { return contractDetails; }
			set {
				contractDetails = value;

				ResetContractSpecificInfo();

				RaisePropertyChanged(() => Contract);
				RaisePropertyChanged(() => IsRentContractAssociated);
				RaisePropertyChanged(() => CostLabelText);
			}
		}

		public bool IsShareInfoVisible {
			get { return false; }
		}

		public bool IsRentContractAssociated {
			get { return contractDetails != null; }
		}

		public string CostLabelText {
			get { return IsRentContractAssociated ? Strings.GrowerCost_Text.ToUpper() : Strings.ProductCost_Text.ToUpper(); }
		}

		public decimal GrowerSplitPercent {
			get { return growerSplitPercent; }
			set {
				growerSplitPercent = value;
				ShareOwnerSplitPercent = 1.0m - growerSplitPercent;
				RaisePropertyChanged(() => GrowerSplitPercent);

				UpdatePercentFromGrowerInventory();
			}
		}

		public decimal ShareOwnerSplitPercent {
			get { return shareOwnerSplitPercent; }
			set {
				shareOwnerSplitPercent = value;
				RaisePropertyChanged(() => ShareOwnerSplitPercent);
			}
		}

        [Range(typeof(decimal), "0", "1", ErrorMessageResourceName = "ValueMustBeBetweenZeroAndOneHundred_Text", ErrorMessageResourceType = typeof(Strings))]
        public decimal PercentFromGrowerInventory {
			get { return percentFromGrowerInventory; }
			set {
				percentFromGrowerInventory = value;
				ValidateAndRaisePropertyChanged(() => PercentFromGrowerInventory);
				CalculateTotalProductFromGrowerInventory();
			}
		}

		private void CalculateTotalProductFromGrowerInventory() {
			if (TotalProductUnit != null && !(TotalProductUnit is NullUnit) && SelectedProduct != null) {
				var total = TotalProductValue * PercentFromGrowerInventory;
				TotalProductFromGrowerInventory = TotalProductUnit.GetMeasure((double)total, SelectedProduct.Product.Density);
			}
		}

		public Measure TotalProductFromGrowerInventory {
			get { return totalProductFromGrowerInventory; }
			set {
				totalProductFromGrowerInventory = value;
				RaisePropertyChanged(() => TotalProductFromGrowerInventory);
				RaisePropertyChanged(() => TotalGrowerCost);
			}
		}

		// readonly property for displaying product area. Setter is only for conveniently raising the prop-changed event for the view
		public Measure ProductArea {
			get { return productArea; }
			private set {
				productArea = value;
				RaisePropertyChanged(() => ProductArea);
				RaisePropertyChanged(() => ProductAreaDisplayText);
			}
		}

		public string ProductAreaDisplayText {
			get { return ProductArea.AbbreviatedDisplay; }
			set {
				if (string.IsNullOrWhiteSpace(value)) { return; }

				decimal parsedValue = 0;
				if (decimal.TryParse(value, out parsedValue)) {
					ProductArea = totalArea.Unit.GetMeasure(Convert.ToDouble(parsedValue));
				}

				productAreaPercent = totalArea.Value != 0 ? Convert.ToDecimal(productArea.Value / totalArea.Value) : 1;

				RaisePropertyChanged(() => ProductAreaPercent);
				RaisePropertyChanged(() => ProductAreaDisplayText);
			}
		}

		public bool IsProductListVisible {
			get { return isProductListVisible; }
			set {
				isProductListVisible = value;
				RaisePropertyChanged(() => IsProductListVisible);
			}
		}

		public bool IsCreateProductPopupOpen {
			get { return isCreateProductPopupOpen; }
			set {
				isCreateProductPopupOpen = value;
				RaisePropertyChanged(() => IsCreateProductPopupOpen);
			}
		}

        private AssociatedProductItem _associatedProduct { get; set; }
        public AssociatedProductItem AssociatedProduct
        {
            get { return _associatedProduct; }
            set
            {
                _associatedProduct = value;
                RaisePropertyChanged(() => AssociatedProduct);
            }
        }

        public ObservableCollection<AssociatedProductItem> AssociatedProducts { get; set; }
        public bool HasAssociatedProducts
        {
            get
            {
                return AssociatedProducts != null && AssociatedProducts.Count > 0 ? true : false;
            }
        }

        public void ResetContractSpecificInfo() {
			if (contractDetails == null || SelectedProduct == null || SelectedProduct.Product == null) {
				PercentFromGrowerInventory = 1.0m;
				return;
			}

			if (SelectedProduct.Product.ProductType == GlobalStrings.ProductType_CropProtection) {
				GrowerSplitPercent = (decimal)contractDetails.IncludedShareInfo.GrowerCropProtectionShare;
			} else if (SelectedProduct.Product.ProductType == GlobalStrings.ProductType_Fertilizer) {
				GrowerSplitPercent = (decimal)contractDetails.IncludedShareInfo.GrowerFertilizerShare;
			} else if (SelectedProduct.Product.ProductType == GlobalStrings.ProductType_Seed) {
				GrowerSplitPercent = (decimal)contractDetails.IncludedShareInfo.GrowerSeedShare;
			} else if (SelectedProduct.Product.ProductType == GlobalStrings.ProductType_Service) {
				GrowerSplitPercent = (decimal)contractDetails.IncludedShareInfo.GrowerServiceShare;
			}

			UpdatePercentFromGrowerInventory();

			ShareOwnerSplitPercent = 1.0m - GrowerSplitPercent;
		}

		void UpdatePercentFromGrowerInventory() {
			if (contractDetails == null || SelectedProduct == null || SelectedProduct.Product == null) {
				PercentFromGrowerInventory = 1.0m;
				return;
			}

			if (SelectedProduct.Product.ProductType == GlobalStrings.ProductType_CropProtection) {
				PercentFromGrowerInventory = contractDetails.IncludedInventoryInfo.IsCropProtectionFromInventory ? 1.0m : GrowerSplitPercent;
			} else if (SelectedProduct.Product.ProductType == GlobalStrings.ProductType_Fertilizer) {
				PercentFromGrowerInventory = contractDetails.IncludedInventoryInfo.IsFertilizerFromInventory ? 1.0m : GrowerSplitPercent;
			} else if (SelectedProduct.Product.ProductType == GlobalStrings.ProductType_Seed) {
				PercentFromGrowerInventory = contractDetails.IncludedInventoryInfo.IsSeedFromInventory ? 1.0m : GrowerSplitPercent;
			} else if (SelectedProduct.Product.ProductType == GlobalStrings.ProductType_Service) {
				PercentFromGrowerInventory = contractDetails.IncludedInventoryInfo.IsServiceFromInventory ? 1.0m : GrowerSplitPercent;
			}
		}

		void UpdateGrowerShare() {
			if (contractDetails == null || SelectedProduct == null || SelectedProduct.Product == null) {
				GrowerSharePercent = 1.0m;
				return;
			}

			if (SelectedProduct.Product.ProductType == GlobalStrings.ProductType_CropProtection) {
				GrowerSharePercent = (decimal)contractDetails.IncludedShareInfo.GrowerCropProtectionShare;
			} else if (SelectedProduct.Product.ProductType == GlobalStrings.ProductType_Fertilizer) {
				GrowerSharePercent = (decimal)contractDetails.IncludedShareInfo.GrowerFertilizerShare;
			} else if (SelectedProduct.Product.ProductType == GlobalStrings.ProductType_Seed) {
				GrowerSharePercent = (decimal)contractDetails.IncludedShareInfo.GrowerSeedShare;
			} else if (SelectedProduct.Product.ProductType == GlobalStrings.ProductType_Service) {
				GrowerSharePercent = (decimal)contractDetails.IncludedShareInfo.GrowerServiceShare;
			}
		}

		void UpdateInventoryListItem() {
			if (selectedProduct == null || selectedProduct.Product == null) {
				currentInventoryItem = null;
				return;
			}

			currentInventoryItem = inventory.Products.ContainsKey(selectedProduct.ProductId) ? inventory.Products[selectedProduct.ProductId] : null;

			if (currentInventoryItem != null) {
				var priceItem = new PriceItem((decimal)currentInventoryItem.AveragePriceValue, currentInventoryItem.AveragePriceUnit, true);
				AveragePriceItem = priceItem;
			} else {
				var priceItem = new PriceItem(0m, selectedProduct.Product.StdPackageUnit, true);
				AveragePriceItem = priceItem;
			}
		}

		void UpdateAllowedUnits() {
			AllowedRateUnits.Clear();
			AllowedTotalUnits.Clear();

			SelectedRatePerAreaUnitHelper = null;

			if (SelectedProduct == null) { return; }

			var psu = UnitFactory.GetPackageSafeUnit(SelectedProduct.Product.StdPackageUnit, SelectedProduct.Product.StdUnit, SelectedProduct.Product.StdFactor, SelectedProduct.Product.StdPackageUnit);
			var compatibleUnits = AgC.UnitConversion.CompatibilityFactory.GetCompatibleUnits(psu, SelectedProduct.Product.Density);

            ///////////////////////////set default units//////////////////////////////////////////////////////////////////////
            var defaultUnit = endpoint.GetUserSettings().PreferredRateUnits.ContainsKey(SelectedProduct.ProductId.Id) ?
                (from u in compatibleUnits where u.Name == endpoint.GetUserSettings().PreferredRateUnits[SelectedProduct.ProductId.Id] select u).FirstOrDefault() ?? psu
                : psu;
            var defaultTotalUnit = endpoint.GetUserSettings().PreferredTotalUnits.ContainsKey(SelectedProduct.ProductId.Id) ?
                (from u in compatibleUnits where u.Name == endpoint.GetUserSettings().PreferredTotalUnits[SelectedProduct.ProductId.Id] select u).FirstOrDefault() ?? psu
                : psu;

            var defaultRatePerTankUnit = endpoint.GetUserSettings().PreferredRatePerTankUnits.ContainsKey(SelectedProduct.ProductId.Id) ?
                (from u in compatibleUnits where u.Name == endpoint.GetUserSettings().PreferredRatePerTankUnits[SelectedProduct.ProductId.Id] select u).FirstOrDefault() ?? psu
                : psu;

            var prodSettingsId = new ProductSettingId(ApplicationEnvironment.CurrentDataSourceId, SelectedProduct.ProductId.Id);
            var prodSettings = endpoint.GetView<ProductSettingsView>(prodSettingsId);
            if (prodSettings.HasValue) {
                var settings = prodSettings.Value;
                defaultUnit = string.IsNullOrEmpty(settings.RatePerAreaUnit) ? defaultUnit : UnitFactory.GetUnitByName(settings.RatePerAreaUnit);
                defaultTotalUnit = string.IsNullOrEmpty(settings.TotalUnit) ? defaultTotalUnit : UnitFactory.GetUnitByName(settings.TotalUnit);
                defaultRatePerTankUnit = string.IsNullOrEmpty(settings.RatePerTankUnit) ? defaultRatePerTankUnit : UnitFactory.GetUnitByName(settings.RatePerTankUnit);
            }
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            foreach (var u in compatibleUnits) {
                AllowedTotalUnits.Add(u);
                var uh = new UnitPerAreaHelper(u, UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit));
                AllowedRateUnits.Add(uh);

                switch (ApplyBy) {
                    case ApplicationTypes.RatePerArea:
                    default:
                        if (u == defaultUnit) {
                            SelectedRatePerAreaUnitHelper = uh;
                        }
                        break;
                    case ApplicationTypes.RatePerTank:
                        if (u == defaultRatePerTankUnit) {
                            SelectedRatePerTankUnit = u;
                        }
                        break;
                    case ApplicationTypes.TotalProduct:
                        if (u == defaultTotalUnit) {
                            TotalProductUnit = u;
                        }
                        break;
                }
            }

            //foreach (var u in compatibleUnits) {
            //	AllowedTotalUnits.Add(u);
            //	var uh = new UnitPerAreaHelper(u, UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit));
            //	AllowedRateUnits.Add(uh);
            //	if (u == psu) {
            //		switch (ApplyBy) {
            //			case ApplicationTypes.RatePerArea:
            //			case ApplicationTypes.RatePerTank:
            //			default:
            //				SelectedRatePerAreaUnitHelper = uh;
            //				SelectedRatePerTankUnit = u;
            //				break;
            //			case ApplicationTypes.TotalProduct:
            //				TotalProductUnit = u;
            //				break;
            //		}
            //	}
            //}

            // If we couldn't find it in the list while we were building it, pick the first one, if any.
            if (SelectedRatePerAreaUnitHelper == null && AllowedRateUnits.Any()) { SelectedRatePerAreaUnitHelper = AllowedRateUnits[0]; }

			// TODO: Make CostUnit configurable
			CostUnit = UnitFactory.GetPackageSafeUnit(SelectedProduct.Product.StdPackageUnit, SelectedProduct.Product.StdUnit, SelectedProduct.Product.StdFactor, SelectedProduct.Product.StdPackageUnit);
		}

		void fieldSelectionModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e) {
			if (e.PropertyName == "TotalArea") {
				totalArea = woView.TotalArea;
				RecalculateItem();
			}
		}

		void DeleteProduct() {
			if (!AddProduct) {
				if (woView != null) {
					RemoveProductFromWorkOrder productCommand = new RemoveProductFromWorkOrder(woView.Id, endpoint.GenerateNewMetadata(), trackingId);
					endpoint.SendOne(productCommand);

					parent.Products.Remove(details);
				}

				//Messenger.Default.Send(new HidePopupMessage());
				Messenger.Default.Send(new HideOverlayMessage());
			}
		}

		internal bool AssociatedInfoChanged() {
			return details.ApplicationMethod != ApplicationMethod || details.TargetPest.id != SelectedPest.PestId;
		}

		void UpdateProduct() {
			if (SelectedProduct == null || !ValidateViewModel()) { return; }

			if (AddProduct) {
				//add product 
				RecalculateItem();

				AddProductToWorkOrder addWoCommand = new AddProductToWorkOrder(woView.Id, endpoint.GenerateNewMetadata(), ToApplicationProductEntry());
				endpoint.SendOne(addWoCommand);

				parent.AddToProducts(ToProductDetails());
			} else {
				RecalculateItem();

				switch (woView.ApplicationStrategy) {
					case ProductApplicationStrategy.ByRatePerArea:
						ChangeWorkOrderProductRatePerArea x = new ChangeWorkOrderProductRatePerArea(woView.Id, endpoint.GenerateNewMetadata(), trackingId, ratePerAreaValue, ratePerAreaUnitHelper.Unit.Name);
						endpoint.SendOne(x);
						break;
					case ProductApplicationStrategy.ByTotalProduct:
						ChangeWorkOrderProductTotalProduct y = new ChangeWorkOrderProductTotalProduct(woView.Id, endpoint.GenerateNewMetadata(), trackingId, TotalProductValue, TotalProductUnit.Name);
						endpoint.SendOne(y);
						break;
					case ProductApplicationStrategy.ByRatePerTank:
						ChangeWorkOrderProductRatePerTank z = new ChangeWorkOrderProductRatePerTank(woView.Id, endpoint.GenerateNewMetadata(), trackingId, RatePerTankValue, SelectedRatePerTankUnit.Name);
						endpoint.SendOne(z);
						break;
					default:
						break;

				}

				if (SelectedPriceItem != null && SelectedPriceItem.IsSystem == false && SelectedPriceItem.Price != details.SpecificCostPerUnitValue) {
					//TO DO :: UPDATE THE SPECIFIC COST FOR WORK ORDER PRODUCT

					ChangeWorkOrderProductSpecificCost costCommand = new ChangeWorkOrderProductSpecificCost(woView.Id, endpoint.GenerateNewMetadata(), trackingId, SelectedPriceItem.Price, SelectedPriceItem.PriceUnit);
					endpoint.SendOne(costCommand);
				}

				if (ProductAreaPercent != details.ProductAreaPercent) {
					ChangeWorkOrderProductCoveragePercent woCoverage = new ChangeWorkOrderProductCoveragePercent(woView.Id, endpoint.GenerateNewMetadata(), trackingId, ProductAreaPercent);
					endpoint.SendOne(woCoverage);
				}

				if (ApplicationMethod != null && ApplicationMethod != details.ApplicationMethod) {
					UpdateWorkOrderProductApplicationMethod appMethodCommand = new UpdateWorkOrderProductApplicationMethod(woView.Id, endpoint.GenerateNewMetadata(), trackingId, ApplicationMethod.Name);
					endpoint.SendOne(appMethodCommand);
				}

				if (GrowerSharePercent != details.GrowerShare || PercentFromGrowerInventory != details.FromGrowerInventory) {
					var totalFromGrower = (decimal)TotalProduct.Value * PercentFromGrowerInventory;
					var newShareInfo = new ApplicationProductShareOwnerInformation(GrowerSharePercent, Convert.ToDecimal(TotalProductFromGrowerInventory.Value), TotalProductFromGrowerInventory.Unit.Name);
					UpdateWorkOrderProductShareInfo shareInfoCommand = new UpdateWorkOrderProductShareInfo(woView.Id, endpoint.GenerateNewMetadata(), trackingId, newShareInfo);
					endpoint.SendOne(shareInfoCommand);
				}

				var pest = SelectedPest != null ? new MiniPest() { id = SelectedPest.PestId, Name = SelectedPest.CommonName, LatinList = SelectedPest.LatinName } : new MiniPest();
				var detailsPest = details.TargetPest != null ? details.TargetPest : new MiniPest();
				if (SelectedPest != null && pest != detailsPest) {
					UpdateWorkOrderProductTargetPest pestCommand = new UpdateWorkOrderProductTargetPest(woView.Id, endpoint.GenerateNewMetadata(), trackingId, pest.id, pest.Name, pest.LatinList);
					endpoint.SendOne(pestCommand);
				}

				var prod = ToProductDetails();
				parent.Products.Remove(details);
				parent.AddToProducts(prod);

			}

			//Messenger.Default.Send(new HidePopupMessage());
			Messenger.Default.Send(new HideOverlayMessage());
		}

		internal void RecalculateItem() {
			switch (ApplyBy) {
				case ApplicationTypes.RatePerTank:
					RecalculateByTank();
					break;
				case ApplicationTypes.TotalProduct:
					RecalculateByTotal();
					break;
				case ApplicationTypes.RatePerArea:
				default:
					RecalculateByArea();
					break;
			}

			CalculateTotalProductFromGrowerInventory();

			ValidateProperty(() => RatePerAreaValue);
			ValidateProperty(() => RatePerTankValue);
			ValidateProperty(() => TotalProductValue);

			ValidateProperty(() => SelectedRatePerAreaUnitHelper);
			ValidateProperty(() => SelectedRatePerTankUnit);
			ValidateProperty(() => TotalProductUnit);
		}

		void RecalculateByArea() {
			if (SelectedProduct == null || SelectedRatePerAreaUnitHelper == null) {
				TotalProductValue = 0;
				TotalProductUnit = new NullUnit();

				SelectedRatePerTankUnit = null;
				RatePerTankValue = 0m;
				return;
			}

			var rate = SelectedRatePerAreaUnitHelper.Unit.GetMeasure((double)RatePerAreaValue, SelectedProduct.Product.Density);
			var totalArea = woView.TotalArea;
			var tankInfo = woView.TankInformation;
			ProductArea = totalArea.Unit.GetMeasure(totalArea.Value * (double)productAreaPercent);

			var calculation = applicationCalculationService.RecalculateByArea(
				SelectedProduct.Product,
				AveragePriceItem,
				rate,
				ProductArea,
				tankInfo == null ? 0m : tankInfo.TankCount,
				AllowedTotalUnits
			);

			TotalProductValue = calculation.TotalProductValue;
			TotalProductUnit = calculation.TotalProductUnit;

			var displayTotal = DisplayMeasureConverter.ConvertToTotalDisplayMeasure(SelectedProduct.ProductId, TotalProductValue, TotalProductUnit.Name, Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);
			TotalProductValue = (decimal)displayTotal.Value;
			TotalProductUnit = displayTotal.Unit;

			RatePerTankValue = calculation.RatePerTankValue;

			var displayRatePerTank = DisplayMeasureConverter.ConvertToRatePerTankDisplayMeasure(
				SelectedProduct.ProductId,
				RatePerTankValue,
				calculation.RatePerTankUnit.Name,
				Density,
				endpoint,
				ApplicationEnvironment.CurrentDataSourceId
			);

			RatePerTankValue = (decimal)displayRatePerTank.Value;

			if (calculation.RatePerTankUnit != null) {
				SelectedRatePerTankUnit = displayRatePerTank.Unit;
			}

			TotalCost = calculation.TotalCost;
		}

		void RecalculateByTotal() {
            //if(TotalProductValue == null )
			if (SelectedProduct == null || TotalProductUnit == null) {
				SelectedRatePerAreaUnitHelper = null;
				RatePerAreaValue = 0m;

				SelectedRatePerTankUnit = null;
				RatePerTankValue = 0m;
				return;
			}

			var total = TotalProductUnit.GetMeasure((double)TotalProductValue, SelectedProduct.Product.Density);
			var totalArea = woView.TotalArea;
			var tankInfo = woView.TankInformation;
			var productArea = totalArea.Unit.GetMeasure(totalArea.Value * (double)productAreaPercent);

			var calculation = applicationCalculationService.RecalculateByTotal(
				SelectedProduct.Product,
				total,
				productArea,
				tankInfo == null ? 0m : tankInfo.TankCount,
				AllowedTotalUnits,
				averagePriceItem
			);

			ProductArea = calculation.ProductArea;
			RatePerAreaValue = calculation.RatePerAreaValue;
			SelectedRatePerAreaUnitHelper = (from r in AllowedRateUnits where r.Unit.Name == calculation.PackageSafeUnit.Name select r).FirstOrDefault();
            if(SelectedRatePerAreaUnitHelper == null) { return; }
			var density = AddProduct ? SelectedProduct.Product.Density : Density; // On an add, we need to use the selected product, since it's changeable.
			var displayRate = DisplayMeasureConverter.ConvertToRateDisplayMeasure(SelectedProduct.ProductId, RatePerAreaValue, SelectedRatePerAreaUnitHelper.Unit.Name, density, endpoint, ApplicationEnvironment.CurrentDataSourceId);
			RatePerAreaValue = (decimal)displayRate.Value;
			SelectedRatePerAreaUnitHelper = new UnitPerAreaHelper(displayRate.Unit, woView.TotalArea.Unit);

			RatePerTankValue = calculation.RatePerTankValue;
			var displayRatePerTank = DisplayMeasureConverter.ConvertToRatePerTankDisplayMeasure(SelectedProduct.ProductId, RatePerTankValue, calculation.RatePerTankUnit.Name, Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);
			RatePerTankValue = (decimal)displayRatePerTank.Value;

			if (calculation.RatePerTankUnit != null) {
				SelectedRatePerTankUnit = displayRatePerTank.Unit;
			}

			TotalCost = calculation.TotalCost;
		}

		void RecalculateByTank() {
			if (SelectedProduct == null || SelectedRatePerTankUnit == null) {
				SelectedRatePerAreaUnitHelper = null;
				RatePerAreaValue = 0m;

				TotalProductValue = 0;
				TotalProductUnit = new NullUnit();
				return;
			}

			var ratePerTank = SelectedRatePerTankUnit.GetMeasure((double)RatePerTankValue, SelectedProduct.Product.Density);
			var totalArea = woView.TotalArea;
			var tankInfo = woView.TankInformation;
			var productArea = totalArea.Unit.GetMeasure(totalArea.Value * (double)productAreaPercent);

			var calculation = applicationCalculationService.RecalculateByTank(
				SelectedProduct.Product,
				AveragePriceItem,
				ratePerTank,
				productArea,
				tankInfo.TankCount
			);

			ProductArea = calculation.ProductArea;
			TotalProductValue = calculation.TotalProductValue;
			TotalProductUnit = calculation.TotalProductUnit;

			var displayTotal = DisplayMeasureConverter.ConvertToTotalDisplayMeasure(SelectedProduct.ProductId, TotalProductValue, TotalProductUnit.Name, Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);
			TotalProductValue = (decimal)displayTotal.Value;
			TotalProductUnit = displayTotal.Unit;

			RatePerAreaValue = calculation.RatePerAreaValue;
			SelectedRatePerAreaUnitHelper = (from r in AllowedRateUnits where r.Unit.Name == calculation.PackageSafeUnit.Name select r).FirstOrDefault();
            if(SelectedRatePerAreaUnitHelper == null) { return; }
			var displayRate = DisplayMeasureConverter.ConvertToRateDisplayMeasure(SelectedProduct.ProductId, RatePerAreaValue, SelectedRatePerAreaUnitHelper.Unit.Name, Density, endpoint, ApplicationEnvironment.CurrentDataSourceId);
			RatePerAreaValue = (decimal)displayRate.Value;
			SelectedRatePerAreaUnitHelper = new UnitPerAreaHelper(displayRate.Unit, woView.TotalArea.Unit);

			TotalCost = calculation.TotalCost;
		}

		void OnProductCreated(MiniProduct product) {
			//var pli = parent.AddTemporaryMiniProduct(product, new ProductId(ApplicationEnvironment.CurrentDataSourceId, product.Id), true);
			//SelectedProduct = pli;
		}

		internal ApplicationProductDetails ToProductDetails() {

			var avgPriceUnit = currentInventoryItem != null && currentInventoryItem.AveragePriceUnit != null ? UnitFactory.GetUnitByName(currentInventoryItem.AveragePriceUnit) : null;
			var avgPriceValue = currentInventoryItem != null ? currentInventoryItem.AveragePriceValue : 0;

			var prod =
			new ApplicationProductDetails() {
				ProductName = endpoint.GetMasterlistService().GetProductDisplay(SelectedProduct.ProductId),
				RateMeasure = UnitFactory.GetUnitByName(SelectedRatePerAreaUnitHelper.Unit.Name).GetMeasure((double)RatePerAreaValue, SelectedProduct.Product.Density),
				AreaUnit = UnitFactory.GetUnitByName(totalArea.Unit.Name),
				TotalProduct = UnitFactory.GetUnitByName(TotalProduct.Unit.Name).GetMeasure(TotalProduct.Value, SelectedProduct.Product.Density),
				TotalCost = TotalCost,
				CostPerUnitValue = (decimal)avgPriceValue,
				CostUnit = avgPriceUnit,
				ID = SelectedProduct.ProductId,
				ProductAreaPercent = ProductAreaPercent,
				TrackingId = trackingId,
				GrowerShare = GrowerSharePercent,
				Split = (TotalProduct.Value * (double)GrowerSharePercent).ToString("N2") + " / " + (TotalProduct.Value - (TotalProduct.Value * (double)GrowerSharePercent)).ToString("N2"),
				TargetPest = SelectedPest != null ? new MiniPest() { Name = SelectedPest.CommonName, LatinList = SelectedPest.LatinName, id = SelectedPest.PestId } : new MiniPest(),
				ApplicationMethod = ApplicationMethod,

			};

			return prod;
		}

		internal ApplicationProductEntry ToApplicationProductEntry() {
			// TODO: This needs to represent the actual rate unit the user has chosen.
			var psu = UnitFactory.GetPackageSafeUnit(SelectedProduct.Product.StdPackageUnit, SelectedProduct.Product.StdUnit, SelectedProduct.Product.StdFactor, SelectedProduct.Product.StdPackageUnit);

			PriceStrategy strategy = AveragePriceItem.IsSystem ? PriceStrategy.SystemDefined : PriceStrategy.Specific;
			decimal price = AveragePriceItem.IsSystem ? 0m : AveragePriceItem.Price;
			//changed this to keep error from happening...NEED TO ASK BRANDON
			string priceUnit = AveragePriceItem.IsSystem ? AveragePriceItem.PriceUnit : psu.Name;
			var ratePerAreaUnit = SelectedRatePerAreaUnitHelper != null ? SelectedRatePerAreaUnitHelper.Unit.Name : psu.Name;
			var appMethod = ApplicationMethod == null ? string.Empty : ApplicationMethod.Name;
			MiniPest targetPest = new MiniPest();

            //ASSOCIATED PRODUCTS
            List<ApplicationAssociatedProduct> appAssociatedProducts = new List<ApplicationAssociatedProduct>();
            foreach (var associate in AssociatedProducts)
            {
                var appProd = new ApplicationAssociatedProduct();
                appProd.ProductId = associate.ProductID;
                appProd.ProductName = associate.ProductName;
                appProd.ManufacturerName = endpoint.GetMasterlistService().GetProduct(associate.ProductID).Manufacturer;
                appProd.RatePerAreaValue = associate.RatePerAreaValue;
                appProd.RatePerAreaUnit = associate.RatePerAreaUnit.Name;
                appProd.TotalProductValue = associate.TotalProductValue;
                appProd.TotalProductUnit = associate.TotalProductUnit.Name;
                appProd.TrackingId = associate.TrackingID;
                appProd.CustomProductUnit = associate.CustomUnit.Name;
                appProd.CustomProductValue = associate.CustomRateValue;
                appProd.CustomRateType = associate.CustomRateType.ToString();
                appProd.HasCost = associate.HasCost;
                appProd.SpecificCostPerUnitValue = associate.CostPerUnitValue;
                appProd.SpecificCostPerUnitUnit = associate.CostPerUnitUnit != null ? associate.CostPerUnitUnit.Name : string.Empty;
                appAssociatedProducts.Add(appProd);
            }
            //

            if (SelectedPest != null) { targetPest = new MiniPest() { id = SelectedPest.PestId, Name = SelectedPest.CommonName, LatinList = SelectedPest.LatinName, text = string.Empty }; } else { targetPest = null; }
			return new ApplicationProductEntry(selectedProduct.ProductId, SelectedProduct.Product.Name, SelectedProduct.Product.Manufacturer, trackingId,
				ratePerAreaValue, ratePerAreaUnit, ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit, RatePerTankValue, SelectedRatePerTankUnit.Name, (decimal)TotalProduct.Value, TotalProduct.Unit.Name, price, priceUnit, TotalGrowerCost, ProductAreaPercent, (decimal)ProductArea.Value, ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit, GetShareOwnerInformation(), SelectedProduct.Product.Density, appMethod, targetPest, appAssociatedProducts);
		}

		internal string ToName() {
			if (SelectedProduct != null && SelectedProduct.Product != null) {
				return SelectedProduct.Product.Name;
			} else {
				return string.Empty;
			}
		}

		ApplicationProductShareOwnerInformation GetShareOwnerInformation() {
			if (TotalProductFromGrowerInventory == null) {
				return null;
			} else {
				return new ApplicationProductShareOwnerInformation(GrowerSharePercent, (decimal)TotalProductFromGrowerInventory.Value, TotalProductFromGrowerInventory.Unit.Name);
			}
		}

		async void BuildProductList() {
			var watch = Stopwatch.StartNew();

			await Task.Run(() => {
				var mpl = endpoint.GetMasterlistService().GetProductList();
				var mur = endpoint.GetView<ProductUsageView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear)).GetValue(new ProductUsageView());
				var ucp = endpoint.GetView<UserCreatedProductList>(new DataSourceId(ApplicationEnvironment.CurrentDataSourceId)).GetValue(new UserCreatedProductList());

				Guid mlDsId = GlobalIdentifiers.GetMasterlistDataSourceId();
				var productGuids = mur.GetProductGuids();

				var q = from p in mpl
						let cont = productGuids.Contains(p.Id)
						select new ProductListItemDetails(p, mlDsId, cont ? Strings.UsedThisYear_Text : Strings.Unused_Text, cont ? (short)0 : (short)1);

				var ucpQ = from p in ucp.UserCreatedProducts
						   let cont = productGuids.Contains(p.Id)
						   select new ProductListItemDetails(p, ApplicationEnvironment.CurrentDataSourceId, cont ? Strings.UsedThisYear_Text : Strings.Unused_Text, cont ? (short)0 : (short)1);

				Products = new ObservableCollection<ProductListItemDetails>(q.Union(ucpQ).OrderBy(x => x.GroupSortOrder).ToList());
				RaisePropertyChanged(() => Products);
			});
			watch.Stop();
		}

		void PopulatePests() {
            var ml = endpoint.GetMasterlistService();
            var q = from p in ml.GetPestList()
                        //where p.Language == "en"
                    select new PestListItemDetails(p, (p.Name != string.Empty ? (short)0 : (short)1));
            q = q.OrderBy(b => b.GroupSortOrder).ThenBy(x => x.CommonName);
            PestList = new ObservableCollection<PestListItemDetails>(q);
            RaisePropertyChanged(() => PestList);

            if (details != null && details.TargetPest != null && details.TargetPest.id > 0) { SelectedPest = PestList.FirstOrDefault(x => x.CommonName == details.TargetPest.Name && x.LatinName == details.TargetPest.LatinList); }
		}

		void CancelEdit() {
			//just close the popup message
			//Messenger.Default.Send(new HidePopupMessage());
			Messenger.Default.Send(new HideOverlayMessage());
		}

		void UpdatePriceItems() {
			PriceItems.Clear();
			if (currentInventoryItem != null) {
				var priceItem = new PriceItem((decimal)currentInventoryItem.AveragePriceValue, currentInventoryItem.AveragePriceUnit, true);

				PriceItems.Add(priceItem);
				AveragePriceItem = priceItem;
			} else {
				if (SelectedProduct != null) {
					var priceItem = new PriceItem(0m, SelectedProduct.Product.StdPackageUnit, false);
					PriceItems.Add(priceItem);
					AveragePriceItem = priceItem;
				}
			}

			if (details != null && details.SpecificCostPerUnitValue != null) {
				var specificPrice = new PriceItem((decimal)details.SpecificCostPerUnitValue, details.SpecificCostUnit.Name, false);
				PriceItems.Add(specificPrice);
				SelectedPriceItem = PriceItems.FirstOrDefault(x => x.Price == details.SpecificCostPerUnitValue);
			} else {
				SelectedPriceItem = PriceItems.FirstOrDefault();
			}
		}

		void CreateUserProduct() {
			IsProductListVisible = false;
			IsCreateProductPopupOpen = false;
			ShowOverlayMessage msg = new ShowOverlayMessage() {
				ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Secondary.UserCreatedProduct.UserProductCreatorView", "createProduct",
					new UserProductCreatorViewModel(endpoint, ProductSearchText, OnProductCreated))
			};
			Messenger.Default.Send<ShowOverlayMessage>(msg);
		}

        public bool AllowSeedTreatment { get { return SelectedProduct != null && SelectedProduct.Product.ProductType == GlobalStrings.ProductType_Seed ? true : false; } }

        void AssociateProductAddition()
        {
            //TO DO :: CREATE A VIEW MODEL FOR ADDING SEED TREATMENTS TO AN EXISTING PRODUCT
            var ratePerArea = SelectedRatePerAreaUnitHelper.Unit.GetMeasure((double)RatePerAreaValue, SelectedProduct.Product.Density);
            var vm = new AssociatedSeedTreatmentViewModel(endpoint, ratePerArea, ProductArea, SelectedProduct.Product, AddSiblingProduct);
            Messenger.Default.Send<ShowPopupMessage>(new ShowPopupMessage()
            {
                ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Secondary.PopUp.SeedTreatmentProductAssociationCalculator", "calculator", vm)
            });
        }

        public void AddSiblingProduct(AssociatedSeedTreatmentViewModel siblingVM)
        {
            decimal customRateValue = 0m;
            IUnit customUnit;
            AssociatedProductRateType rateType;
            var tab = siblingVM.TabItem == null ? "PerSeedTab" : siblingVM.TabItem.Name;
            switch (tab)
            {
                case "PerSeedTab":
                    customRateValue = siblingVM.MGActivePerSeedValue;
                    var composite2 = new CompositeUnit(siblingVM.SelectedRatePerSeedUnit.AbbreviatedDisplay, 1, SelectedProduct.Product.StdUnit);
                    string text2 = composite2.AbbreviatedDisplay;
                    customUnit = siblingVM.SelectedRatePerSeedUnit;
                    rateType = AssociatedProductRateType.BySeed;
                    break;
                case "PerBagTab":
                    customRateValue = siblingVM.RatePerBagValue;
                    customUnit = siblingVM.SelectedRatePerBagUnit;
                    rateType = AssociatedProductRateType.ByBag;
                    break;
                case "PerCwtTab":
                    customRateValue = siblingVM.RatePerCWTValue;
                    customUnit = siblingVM.SelectedCWTUnit;
                    rateType = AssociatedProductRateType.ByCWT;
                    break;
                case "RowTab":
                    customRateValue = siblingVM.RatePer1000Value;
                    customUnit = siblingVM.SelectedRatePer1000FtRowHelper;
                    rateType = AssociatedProductRateType.ByRow;
                    break;
                default:
                    customRateValue = siblingVM.MGActivePerSeedValue;
                    customUnit = siblingVM.SelectedRatePerSeedUnit;
                    rateType = AssociatedProductRateType.BySeed;
                    break;
            }

            var associatedProduct = new AssociatedProductItem(endpoint, SelectedProduct.Product, RemoveAssociatedProduct)
            {
                RatePerAreaValue = siblingVM.RatePerAreaValue,
                RatePerAreaUnit = siblingVM.SelectedRatePerAreaUnitHelper.Unit,
                TotalProductValue = siblingVM.TotalProductValue,
                TotalProductUnit = siblingVM.SelectedTotalProductUnit,
                HasCost = false,
                ProductID = siblingVM.SelectedProduct.ProductId,
                TrackingID = Guid.NewGuid(),
                ProductName = siblingVM.SelectedProduct.Product.Name,
                CustomRateValue = customRateValue,
                CustomUnit = customUnit,
                CustomRateType = rateType
            };

            AssociatedProducts.Add(associatedProduct);

            RaisePropertyChanged(() => AssociatedProducts);
            RaisePropertyChanged(() => HasAssociatedProducts);

            if (!AddProduct)
            {
                var appProd = new ApplicationAssociatedProduct();
                appProd.ProductId = associatedProduct.ProductID;
                appProd.ProductName = associatedProduct.ProductName;
                appProd.ManufacturerName = endpoint.GetMasterlistService().GetProduct(associatedProduct.ProductID).Manufacturer;
                appProd.RatePerAreaValue = associatedProduct.RatePerAreaValue;
                appProd.RatePerAreaUnit = associatedProduct.RatePerAreaUnit.Name;
                appProd.TotalProductValue = associatedProduct.TotalProductValue;
                appProd.TotalProductUnit = associatedProduct.TotalProductUnit.Name;
                appProd.TrackingId = associatedProduct.TrackingID;
                appProd.CustomProductUnit = associatedProduct.CustomUnit.Name;
                appProd.CustomProductValue = associatedProduct.CustomRateValue;
                appProd.CustomRateType = associatedProduct.CustomRateType.ToString();
                appProd.HasCost = associatedProduct.HasCost;
                appProd.SpecificCostPerUnitValue = associatedProduct.HasCost && associatedProduct.SelectedPriceItem != null && associatedProduct.SelectedPriceItem.IsSystem == false ? associatedProduct.SelectedPriceItem.Price : 0m;
                appProd.SpecificCostPerUnitUnit = associatedProduct.HasCost && associatedProduct.SelectedPriceItem != null && associatedProduct.SelectedPriceItem.IsSystem == false ? associatedProduct.SelectedPriceItem.PriceUnit : string.Empty;

                AddWorkOrderAssociatedProduct command = new AddWorkOrderAssociatedProduct(woView.Id, endpoint.GenerateNewMetadata(), details.TrackingId, appProd);
                endpoint.SendOne(command);
            }
        }

        public void RemoveAssociatedProduct(Guid associatedProductTrackingID)
        {
            var item = AssociatedProducts.SingleOrDefault(x => x.TrackingID == associatedProductTrackingID);
            AssociatedProducts.Remove(item);
            RaisePropertyChanged(() => AssociatedProducts);

            if (woView != null)
            {
                RemoveWorkOrderAssociatedProduct remove = new RemoveWorkOrderAssociatedProduct(woView.Id, endpoint.GenerateNewMetadata(), details.TrackingId, item.TrackingID);
                endpoint.SendOne(remove);
            }

            if (!AssociatedProducts.Any())
            {
                RaisePropertyChanged(() => HasAssociatedProducts);
            }
        }
        #region Custom Validation
        public static ValidationResult ValidateSelectedProduct(ProductListItemDetails product, ValidationContext context) {
			ValidationResult result = null;

			if (product == null) {
				result = new ValidationResult(Strings.ProductIsRequired_Text);
			}

			return result;
		}

		public static ValidationResult ValidateRate(decimal rate, ValidationContext context) {
			ValidationResult result = null;

			var vm = (EditWorkOrderProductViewModel)context.ObjectInstance;

			var validateRatePerArea = vm.ApplyBy == ApplicationTypes.RatePerArea && context.MemberName == "RatePerAreaValue";
			var validateRatePerTank = vm.ApplyBy == ApplicationTypes.RatePerTank && context.MemberName == "RatePerTankValue";
			var validateTotalProduct = vm.ApplyBy == ApplicationTypes.TotalProduct && context.MemberName == "TotalProductValue";

			var shouldValidate = validateRatePerArea || validateRatePerTank || validateTotalProduct;

			if (shouldValidate && rate == 0) {
				result = new ValidationResult(Strings.RateCannotBeZero_Text);
			}

			return result;
		}

		public static ValidationResult ValidateUnit(object unit, ValidationContext context) {
			ValidationResult result = null;

			var vm = (EditWorkOrderProductViewModel)context.ObjectInstance;

			var validateRatePerArea = vm.ApplyBy == ApplicationTypes.RatePerArea && context.MemberName == "SelectedRatePerAreaUnitHelper";
			var validateRatePerTank = vm.ApplyBy == ApplicationTypes.RatePerTank && context.MemberName == "SelectedRatePerTankUnit";
			var validateTotalProduct = vm.ApplyBy == ApplicationTypes.TotalProduct && context.MemberName == "TotalProductUnit";

			var shouldValidate = validateRatePerArea || validateRatePerTank || validateTotalProduct;

			if (shouldValidate && unit == null) {
				result = new ValidationResult(Strings.ValidationResult_InvalidUnit_Text);
			}

			return result;
		}

		public static ValidationResult ValidateSelectedPest(PestListItemDetails selectedPest, ValidationContext context) {
			ValidationResult result = null;

			var vm = (EditWorkOrderProductViewModel)context.ObjectInstance;

			if (!string.IsNullOrWhiteSpace(vm.PestSearchText) && selectedPest == null) {
				result = new ValidationResult(Strings.InvalidPestSelected_Text);
			}

			return result;
		}
		#endregion
	}
}