﻿using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Shared;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Fields;
using Landdb.ViewModel.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;

namespace Landdb.ViewModel.Production.Popups
{
    public class ChangePlanFieldsViewModel : BaseFieldSelectionViewModel
    {
        List<IncludedCropZone> initialCropZones;
        CropPlanId planId;
        PlanDetailsViewModel parent;

        public ChangePlanFieldsViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, int cropYear, CropPlanId planId, List<IncludedCropZone> initialCropZones, PlanDetailsViewModel parent)
            : base(clientEndpoint, dispatcher, cropYear)
        {
            this.initialCropZones = new List<IncludedCropZone>(initialCropZones);
            var initialCzIds = from icz in initialCropZones select icz.Id;
            this.parent = parent;
            if (RootTreeItemModels.Any())
            {
                if (RootTreeItemModels.First() is GrowerTreeItemViewModel)
                {
                    var toCheck = from farm in RootTreeItemModels.First().Children
                                  from field in farm.Children
                                  from cz in field.Children
                                  where initialCzIds.Contains(cz.Id)
                                  select cz;
                    toCheck.ForEach(x => x.IsChecked = true);
                }
                else
                {
                    var toCheck = from crop in RootTreeItemModels.First().Children
                                  from farm in crop.Children
                                  from field in farm.Children
                                  from cz in field.Children
                                  where initialCzIds.Contains(cz.Id)
                                  select cz;
                    toCheck.ForEach(x => x.IsChecked = true);
                }
            }

            foreach (var f in SelectedCropZones)
            {
                f.ChangeSelectedArea(initialCropZones.Where(x => x.Id == f.Id).FirstOrDefault().Area);
            }

            this.planId = planId;

            //this.UsePercentCoverage = true;

            SaveChangesCommand = new RelayCommand(SaveChanges);
            CancelChangesCommand = new RelayCommand(CancelChanges);
        }

        public ICommand SaveChangesCommand { get; private set; }
        public ICommand CancelChangesCommand { get; private set; }

        void SaveChanges()
        {
            List<CropZoneArea> added = new List<CropZoneArea>();
            List<CropZoneArea> removed = new List<CropZoneArea>();
            List<CropZoneArea> changed = new List<CropZoneArea>();

            var addedQ = from a in SelectedCropZones
                         where !initialCropZones.Exists(x => x.Id == a.Id)
                         select new CropZoneArea(a.Id, a.SelectedArea.Value, a.SelectedArea.Unit.Name);
            var changedQ = from c in SelectedCropZones
                           where initialCropZones.Exists(x => x.Id == c.Id && (x.Area.Unit != c.SelectedArea.Unit || Math.Abs(x.Area.Value - c.SelectedArea.Value) > 0.0001))
                           select new CropZoneArea(c.Id, c.SelectedArea.Value, c.SelectedArea.Unit.Name);
            var removedQ = from r in initialCropZones
                           where !SelectedCropZones.Where(x => x.Id == r.Id).Any()
                           select r.Id;

            var cmd = new ChangeCropZonesIncludedInPlan(planId, clientEndpoint.GenerateNewMetadata(), addedQ.ToArray(), removedQ.ToArray(), changedQ.ToArray());
            clientEndpoint.SendOne(cmd);

            parent.ChangeEstimatedArea(TotalArea);

            Messenger.Default.Send<HideOverlayMessage>(new HideOverlayMessage());
        }

        void CancelChanges()
        {
            Messenger.Default.Send<HideOverlayMessage>(new HideOverlayMessage());
        }

        protected override void OnFieldCheckedChanged(CropZoneTreeItemViewModel treeItem)
        {

        }
    }
}
