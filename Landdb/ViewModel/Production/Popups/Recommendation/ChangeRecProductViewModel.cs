﻿using AgC.UnitConversion;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Product;
using Landdb.Domain.ReadModels.ProductSetting;
using Landdb.Domain.ReadModels.Recommendation;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Secondary.Application;
using Landdb.ViewModel.Shared;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Production.Popups.Recommendation {
	public class ChangeRecProductViewModel : ValidationViewModelBase {

		readonly IClientEndpoint clientEndpoint;
		readonly Dispatcher dispatcher;

		Guid trackingId;
		ProductListItemDetails selectedProduct;

		// Rate per area values
		decimal ratePerAreaValue;
		UnitPerAreaHelper ratePerAreaUnitHelper;

		// Total product values
		decimal totalProductValue;
		IUnit totalProductUnit;

		// Rate per tank values
		decimal ratePerTankValue;
		IUnit ratePerTankUnit;

		decimal? productAreaPercent = 1m;
		Measure productArea;

		ApplicationMethod appMethod;
		IApplicationCalculationService applicationCalculationService;
		IRecommendationCommandService recommendationCommandService;

		bool isProductListVisible = false;
		bool isCreateProductPopupOpen = false;

		Measure totalArea;

		RecommendationDetailsViewModel parent;
		RecommendationDetailsViewModel.ProductDetails details;
		RecommendationView view;

		ApplicationTypes applyBy;

		public ChangeRecProductViewModel(IClientEndpoint endpoint, Dispatcher dispatcher, RecommendationView view, RecommendationDetailsViewModel parent, RecommendationDetailsViewModel.ProductDetails details) {
			this.clientEndpoint = endpoint;
			this.dispatcher = dispatcher;
			this.parent = parent;
			this.view = view;
			AllowedRateUnits = new ObservableCollection<UnitPerAreaHelper>();
			AllowedTotalUnits = new ObservableCollection<IUnit>();
			this.AddProduct = details == null ? true : false;
			this.applicationCalculationService = new ApplicationCalculationService();
			this.recommendationCommandService = new RecommendationCommandService();
			Products = new ObservableCollection<ProductListItemDetails>();

            Task popPest = Task.Factory.StartNew(PopulatePests);
            Task.Run(() => { BuildProductList(); });

			if (AddProduct) {
				//Task.Run(() => { BuildProductList(); });
				ApplyBy = (ApplicationTypes)view.ApplicationStrategy;
				ProductAreaPercent = 1.0m;
				trackingId = Guid.NewGuid();
				totalArea = view.TotalArea;
				ProductArea = totalArea;
				AllowEdit = true;
			} else {
				this.details = details;
                ApplyBy = (ApplicationTypes)view.ApplicationStrategy;
				AllowEdit = false;
				totalArea = view.TotalArea;
				//ApplicationCount = (int)view.Products.Where(x => x.TrackingId == details.TrackingId).SingleOrDefault().ApplicationCount;
				Guid mlDsId = GlobalIdentifiers.GetMasterlistDataSourceId();
				SelectedProduct = new ProductListItemDetails(endpoint.GetMasterlistService().GetProduct(details.ID), details.ID.DataSourceId, Strings.UsedThisYear_Text, (short)0);
				RatePerTankValue = details.RatePerTank != null ? (decimal)details.RatePerTank.Value : 0m;
				RatePerAreaValue = details.RateMeasure != null ? (decimal)details.RateMeasure.Value : 0m;

				if (details.RateMeasure != null) {
					var rateSafeUnit = UnitFactory.GetPackageSafeUnit(details.RateMeasure.Unit.Name, SelectedProduct.Product.StdUnit, SelectedProduct.Product.StdFactor, SelectedProduct.Product.StdPackageUnit);
					SelectedRatePerAreaUnitHelper = new UnitPerAreaHelper(rateSafeUnit, view.TotalArea.Unit);
				}

				var ratePerTankUnit = details.RatePerTank != null ? details.RatePerTank.Unit.Name : SelectedProduct.Product.StdPackageUnit;
				var ratePerTankSafeUnit = UnitFactory.GetPackageSafeUnit(ratePerTankUnit, SelectedProduct.Product.StdUnit, SelectedProduct.Product.StdFactor, SelectedProduct.Product.StdPackageUnit);
				SelectedRatePerTankUnit = ratePerTankSafeUnit;
				ProductAreaPercent = details.ProductAreaPercent;
				ProductArea = totalArea;
				ApplicationMethod = details.ApplicationMethod;
				RatePerTankValue = details.RatePerTank != null ? (decimal)details.RatePerTank.Value : 0m;
				TotalProductUnit = UnitFactory.GetPackageSafeUnit(details.TotalProduct.Unit.Name, SelectedProduct.Product.StdUnit, SelectedProduct.Product.StdFactor, SelectedProduct.Product.StdPackageUnit);
				TotalProductValue = (decimal)details.TotalProduct.Value;
				Resolved = details.Resolved;

				if (details.ProductAreaPercent == 0) {
					productAreaPercent = 1.0m;
				} else {
					ProductAreaPercent = details.ProductAreaPercent;
				}

				trackingId = details.TrackingId;

                popPest.Wait();
                SelectedPest = details.TargetPest != null ? PestList.FirstOrDefault(x => x.CommonName == details.TargetPest.Name && x.LatinName == details.TargetPest.LatinList) : null;
                RaisePropertyChanged(() => SelectedPest);
            }

            AllowDelete = !AddProduct && view.Products.Count > 1;

			CreateUserProductCommand = new RelayCommand(CreateUserProduct);

			Cancel = new RelayCommand(CancelEdit);
			Update = new RelayCommand(UpdateProduct);
			Delete = new RelayCommand(DeleteProduct);

			ValidateViewModel();
		}

		public ICommand Cancel { get; }
		public ICommand Update { get; }
		public ICommand Delete { get; }
		public bool AddProduct { get; }
		public bool AllowEdit { get; }
		public bool AllowDelete { get; }

		public ICommand CreateUserProductCommand { get; }

		public ObservableCollection<UnitPerAreaHelper> AllowedRateUnits { get; }
		public ObservableCollection<IUnit> AllowedTotalUnits { get; }

		public ObservableCollection<PestListItemDetails> PestList { get; private set; }

		public ObservableCollection<ProductListItemDetails> Products { get; private set; }

		public string ProductSearchText { get; set; }

		public List<ApplicationMethod> ApplicationMethods {
			get { return clientEndpoint.GetMasterlistService().GetApplicationMethodList().ToList(); }
		}

		public ApplicationMethod ApplicationMethod {
			get { return appMethod; }
			set {
				appMethod = value;
				RaisePropertyChanged(() => ApplicationMethod);
			}
		}

		private PestListItemDetails _selectedPest;
		[CustomValidation(typeof(ChangeRecProductViewModel), nameof(ValidateSelectedPest))]
		public PestListItemDetails SelectedPest {
			get { return _selectedPest; }
			set {
				_selectedPest = value;
				ValidateProperty(() => SelectedPest);
			}
		}

		private string _pestSearchText;
		public string PestSearchText {
			get { return _pestSearchText; }
			set {
				_pestSearchText = value;
				ValidateProperty(() => SelectedPest);
			}
		}

		public ApplicationTypes ApplyBy {
			get { return applyBy; }
			set {
				applyBy = value;
				RaisePropertyChanged(() => ApplyBy);
			}
		}

		[CustomValidation(typeof(ChangeRecProductViewModel), nameof(ValidateSelectedProduct))]
		public ProductListItemDetails SelectedProduct {
			get { return selectedProduct; }
			set {
				selectedProduct = value;
				ValidateAndRaisePropertyChanged(() => SelectedProduct);
				RaisePropertyChanged(() => DisplayName);
				RaisePropertyChanged(() => ApplicationMethod);

				UpdateAllowedUnits();

				if (value != null && value.Product != null && value.Product.ProductType == GlobalStrings.ProductType_Service && value.Product.StdPackageUnit == ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit) {
					RatePerAreaValue = 1; // default to 1 for acre-based services because they're almost always "1 acre". Should probably extend this to other area units as well, for intl.
				} else {
					RatePerAreaValue = 0;  // TODO: In the future, this should be the most recently used rate. We should also populate a list of the MR rates for this product.
				}

				if (AddProduct) {
					RecalculateItem();
				}

				if (value == null || value.Product == null) {
					IsCreateProductPopupOpen = true;
				}
			}
		}

		public string DisplayName {
			get {
				if (SelectedProduct == null) { return string.Empty; }
				return $"{SelectedProduct.Product.Name} ({SelectedProduct.Product.Manufacturer})";
			}
		}

		public decimal RatePerAreaValue {
			get { return ratePerAreaValue; }
			set {
				ratePerAreaValue = value;

				RaisePropertyChanged(() => RatePerAreaValue);
				RaisePropertyChanged(() => RatePerAreaDisplay);

				if (applyBy == ApplicationTypes.RatePerArea) {
					RecalculateItem();
				}
			}
		}

		[CustomValidation(typeof(ChangeRecProductViewModel), nameof(ValidateUnit))]
		public UnitPerAreaHelper SelectedRatePerAreaUnitHelper {
			get { return ratePerAreaUnitHelper; }
			set {
				ratePerAreaUnitHelper = value;

				RaisePropertyChanged(() => SelectedRatePerAreaUnitHelper);
				RaisePropertyChanged(() => RatePerAreaDisplay);

				if (applyBy == ApplicationTypes.RatePerArea) {
					RecalculateItem();
				}
			}
		}

		public decimal RatePerTankValue {
			get { return ratePerTankValue; }
			set {
				ratePerTankValue = value;

				RaisePropertyChanged(() => RatePerTankValue);
				RaisePropertyChanged(() => RatePerTankDisplay);

				if (applyBy == ApplicationTypes.RatePerTank) {
					RecalculateItem();
				}
			}
		}

		[CustomValidation(typeof(ChangeRecProductViewModel), nameof(ValidateUnit))]
		public IUnit SelectedRatePerTankUnit {
			get { return ratePerTankUnit; }
			set {
				ratePerTankUnit = value;

				RaisePropertyChanged(() => SelectedRatePerTankUnit);
				RaisePropertyChanged(() => RatePerTankDisplay);

				if (applyBy == ApplicationTypes.RatePerTank) {
					RecalculateItem();
				}
			}
		}

		public string RatePerAreaDisplay {
			get {
				if (ratePerAreaUnitHelper == null) { return string.Empty; }
				var rateString = string.Format("{0} / {1}", ratePerAreaUnitHelper.Unit.GetMeasure((double)ratePerAreaValue, SelectedProduct.Product.Density).ToString(), ratePerAreaUnitHelper.AreaUnit.FullDisplay);
				return rateString;
			}
		}

		public string RatePerTankDisplay {
			get {
				if (ratePerTankUnit == null) { return string.Empty; }
				var rateString = ratePerTankUnit.GetMeasure((double)ratePerTankValue, SelectedProduct.Product.Density).ToString();
				return rateString;
			}
		}

		public decimal TotalProductValue {
			get { return totalProductValue; }
			set {
				totalProductValue = value;

				RaisePropertyChanged(() => TotalProductValue);
				RaisePropertyChanged(() => TotalProduct);

				if (applyBy == ApplicationTypes.TotalProduct) {
					RecalculateItem();
				}
			}
		}

		[CustomValidation(typeof(ChangeRecProductViewModel), nameof(ValidateUnit))]
		public IUnit TotalProductUnit {
			get { return totalProductUnit; }
			set {
				totalProductUnit = value;

				RaisePropertyChanged(() => TotalProductUnit);
				RaisePropertyChanged(() => TotalProduct);

				if (applyBy == ApplicationTypes.TotalProduct) {
					RecalculateItem();
				}
			}
		}

		public Measure TotalProduct {
			get {
				if (totalProductUnit != null && !(TotalProductUnit is NullUnit) && SelectedProduct != null) {
					return totalProductUnit.GetMeasure((double)totalProductValue, SelectedProduct.Product.Density);
				} else {
					return null;
				}
			}
		}

		public decimal? ProductAreaPercent {
			get { return productAreaPercent; }
			set {
				productAreaPercent = value;
				RecalculateItem();

				RaisePropertyChanged(() => ProductAreaPercent);
				RaisePropertyChanged(() => ProductAreaDisplayText);
			}
		}

		// readonly property for displaying product area. Setter is only for conveniently raising the prop-changed event for the view
		public Measure ProductArea {
			get { return productArea; }
			private set {
				productArea = value;
				RaisePropertyChanged(() => ProductArea);
			}
		}

		public string ProductAreaDisplayText {
			get { return $"{ProductArea.Value:N2} {ProductArea.Unit.AbbreviatedDisplay}"; }
			set {
				double newArea = 0;

				if (double.TryParse(value, out newArea)) {
					// this will fire a recalc that will update the area
					ProductAreaPercent = totalArea.Value != 0 ? Convert.ToDecimal(newArea / totalArea.Value) : 1;
				}
			}
		}

		private bool isResolved;
		public bool Resolved {
			get { return isResolved; }
			set {
				isResolved = value;
				RaisePropertyChanged(() => Resolved);
			}
		}

		public bool IsProductListVisible {
			get { return isProductListVisible; }
			set {
				isProductListVisible = value;
				RaisePropertyChanged(() => IsProductListVisible);
			}
		}

		public bool IsCreateProductPopupOpen {
			get { return isCreateProductPopupOpen; }
			set {
				isCreateProductPopupOpen = value;
				RaisePropertyChanged(() => IsCreateProductPopupOpen);
			}
		}

		void UpdateAllowedUnits() {
			AllowedRateUnits.Clear();
			AllowedTotalUnits.Clear();

			SelectedRatePerAreaUnitHelper = null;

			if (SelectedProduct == null) { return; }

			var psu = UnitFactory.GetPackageSafeUnit(SelectedProduct.Product.StdPackageUnit, SelectedProduct.Product.StdUnit, SelectedProduct.Product.StdFactor, SelectedProduct.Product.StdPackageUnit);
			var compatibleUnits = CompatibilityFactory.GetCompatibleUnits(psu, SelectedProduct.Product.Density);

            /////////////////////////////set default units//////////////////////////////////////////////////////////////////////
            var defaultUnit = clientEndpoint.GetUserSettings().PreferredRateUnits.ContainsKey(SelectedProduct.ProductId.Id) ?
                (from u in compatibleUnits where u.Name == clientEndpoint.GetUserSettings().PreferredRateUnits[SelectedProduct.ProductId.Id] select u).FirstOrDefault() ?? psu
                : psu;
            var defaultTotalUnit = clientEndpoint.GetUserSettings().PreferredTotalUnits.ContainsKey(SelectedProduct.ProductId.Id) ?
                (from u in compatibleUnits where u.Name == clientEndpoint.GetUserSettings().PreferredTotalUnits[SelectedProduct.ProductId.Id] select u).FirstOrDefault() ?? psu
                : psu;

            var defaultRatePerTankUnit = clientEndpoint.GetUserSettings().PreferredRatePerTankUnits.ContainsKey(SelectedProduct.ProductId.Id) ?
                (from u in compatibleUnits where u.Name == clientEndpoint.GetUserSettings().PreferredRatePerTankUnits[SelectedProduct.ProductId.Id] select u).FirstOrDefault() ?? psu
                : psu;

            var prodSettingsId = new ProductSettingId(ApplicationEnvironment.CurrentDataSourceId, SelectedProduct.ProductId.Id);
            var prodSettings = clientEndpoint.GetView<ProductSettingsView>(prodSettingsId);
            if (prodSettings.HasValue) {
                var settings = prodSettings.Value;
                defaultUnit = string.IsNullOrEmpty(settings.RatePerAreaUnit) ? defaultUnit : UnitFactory.GetUnitByName(settings.RatePerAreaUnit);
                defaultTotalUnit = string.IsNullOrEmpty(settings.TotalUnit) ? defaultTotalUnit : UnitFactory.GetUnitByName(settings.TotalUnit);
                defaultRatePerTankUnit = string.IsNullOrEmpty(settings.RatePerTankUnit) ? defaultRatePerTankUnit : UnitFactory.GetUnitByName(settings.RatePerTankUnit);
            }
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            foreach (var u in compatibleUnits) {
                AllowedTotalUnits.Add(u);
                var uh = new UnitPerAreaHelper(u, UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit));
                AllowedRateUnits.Add(uh);

                switch (ApplyBy) {
                    case ApplicationTypes.RatePerArea:
                    default:
                        if (u == defaultUnit) {
                            SelectedRatePerAreaUnitHelper = uh;
                        }
                        break;
                    case ApplicationTypes.RatePerTank:
                        if (u == defaultRatePerTankUnit) {
                            SelectedRatePerTankUnit = u;
                        }
                        break;
                    case ApplicationTypes.TotalProduct:
                        if (u == defaultTotalUnit) {
                            TotalProductUnit = u;
                        }
                        break;
                }
            }

            //foreach (var u in compatibleUnits) {
            //	AllowedTotalUnits.Add(u);
            //	var uh = new UnitPerAreaHelper(u, UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit));
            //	AllowedRateUnits.Add(uh);

            //	if (u == psu) {
            //		switch (ApplyBy) {
            //			case ApplicationTypes.RatePerArea:
            //			case ApplicationTypes.RatePerTank:
            //			default:
            //				SelectedRatePerAreaUnitHelper = uh;
            //				SelectedRatePerTankUnit = u;
            //				break;
            //			case ApplicationTypes.TotalProduct:
            //				TotalProductUnit = u;
            //				break;
            //		}
            //	}
            //}

            // If we couldn't find it in the list while we were building it, pick the first one, if any.
            if (SelectedRatePerAreaUnitHelper == null && AllowedRateUnits.Any()) { SelectedRatePerAreaUnitHelper = AllowedRateUnits[0]; }

		}

		void fieldSelectionModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e) {
			if (e.PropertyName == "TotalArea") {
				totalArea = view.TotalArea;

				RecalculateItem();
			}
		}

		void DeleteProduct() {
			if (!AddProduct) {
				if (view != null) {
					RemoveProductFromRecommendation productCommand = new RemoveProductFromRecommendation(view.Id, clientEndpoint.GenerateNewMetadata(), trackingId);
					clientEndpoint.SendOne(productCommand);
					parent.Products.Remove(details);
				}

				//Messenger.Default.Send(new HidePopupMessage());
				Messenger.Default.Send(new HideOverlayMessage());
			}
		}

		internal bool ProductInfoChanged() {
			switch (view.ApplicationStrategy) {
				case ProductApplicationStrategy.ByRatePerArea:
					return details.RateMeasure.Value != (double)ratePerAreaValue || details.RateMeasure.Unit != ratePerAreaUnitHelper.Unit;
				case ProductApplicationStrategy.ByTotalProduct:
					return details.TotalProduct.Value != (double)totalProductValue || details.TotalProduct.Unit != totalProductUnit;
				case ProductApplicationStrategy.ByRatePerTank:
					return details.RatePerTank.Value != (double)ratePerTankValue || details.RatePerTank.Unit != ratePerTankUnit;
				default:
					return false;
			}
		}

		internal bool AssociatedInfoChanged() {
			return details.ApplicationMethod != ApplicationMethod || details.TargetPest.id != SelectedPest.PestId;
		}

		void UpdateProduct() {
			if (view == null) { return; }

			RecalculateItem();

			if (AddProduct) {
				//add product 
				AddProductToRecommendation addRecCommand = new AddProductToRecommendation(view.Id, clientEndpoint.GenerateNewMetadata(), ToRecommendationProductEntry());
				clientEndpoint.SendOne(addRecCommand);
				parent.AddToProducts(ToProductDetails());
			} else {
				switch (view.ApplicationStrategy) {
					case ProductApplicationStrategy.ByRatePerArea:
						ChangeRecommendedProductRatePerArea c = new ChangeRecommendedProductRatePerArea(view.Id, clientEndpoint.GenerateNewMetadata(), trackingId, ratePerAreaValue, ratePerAreaUnitHelper.Unit.Name);
						clientEndpoint.SendOne(c);
						break;
					case ProductApplicationStrategy.ByTotalProduct:
						ChangeRecommendedProductTotalProduct d = new ChangeRecommendedProductTotalProduct(view.Id, clientEndpoint.GenerateNewMetadata(), trackingId, TotalProductValue, TotalProductUnit.Name);
						clientEndpoint.SendOne(d);
						break;
					case ProductApplicationStrategy.ByRatePerTank:
						ChangeRecommendedProductRatePerTank e = new ChangeRecommendedProductRatePerTank(view.Id, clientEndpoint.GenerateNewMetadata(), trackingId, RatePerTankValue, SelectedRatePerTankUnit.Name);
						clientEndpoint.SendOne(e);
						break;
					default:
						break;
				}

				if (ProductAreaPercent != details.ProductAreaPercent) {
					ChangeRecommendedProductCoveragePercent coveragePercentCommand = new ChangeRecommendedProductCoveragePercent(view.Id, clientEndpoint.GenerateNewMetadata(), trackingId, ProductAreaPercent.Value);
					clientEndpoint.SendOne(coveragePercentCommand);
				}

				if (ApplicationMethod != null && ApplicationMethod != details.ApplicationMethod) {
					UpdateRecommendedProductApplicationMethod appMethodCommand = new UpdateRecommendedProductApplicationMethod(view.Id, clientEndpoint.GenerateNewMetadata(), trackingId, ApplicationMethod.Name);
					clientEndpoint.SendOne(appMethodCommand);
				}

				var pest = SelectedPest != null ? new MiniPest() { id = SelectedPest.PestId, Name = SelectedPest.CommonName, LatinList = SelectedPest.LatinName } : new MiniPest();
				var detailsPest = details.TargetPest != null ? details.TargetPest : new MiniPest();

				if (SelectedPest != null && pest != detailsPest) {
					UpdateRecommendedProductTargetPest pestCommand = new UpdateRecommendedProductTargetPest(view.Id, clientEndpoint.GenerateNewMetadata(), trackingId, pest.id, pest.Name, pest.LatinList);
					clientEndpoint.SendOne(pestCommand);
				}

				var prod = ToProductDetails();

				parent.Products.Remove(details);
				parent.AddToProducts(prod);
			}

			Messenger.Default.Send(new HideOverlayMessage());
		}

		internal void RecalculateItem() {
			switch (ApplyBy) {
				case ApplicationTypes.RatePerTank:
					RecalculateByTank();
					break;
				case ApplicationTypes.TotalProduct:
					RecalculateByTotal();
					break;
				case ApplicationTypes.RatePerArea:
				default:
					RecalculateByArea();
					break;
			}

			ValidateProperty(() => RatePerAreaValue);
			ValidateProperty(() => RatePerTankValue);
			ValidateProperty(() => TotalProductValue);

			ValidateProperty(() => SelectedRatePerAreaUnitHelper);
			ValidateProperty(() => SelectedRatePerTankUnit);
			ValidateProperty(() => TotalProductUnit);
		}

		void RecalculateByArea() {
			if (SelectedProduct == null || SelectedRatePerAreaUnitHelper == null) {
				TotalProductValue = 0;
				TotalProductUnit = new NullUnit();

				SelectedRatePerTankUnit = null;
				RatePerTankValue = 0m;
				return;
			}

			var rate = SelectedRatePerAreaUnitHelper.Unit.GetMeasure((double)RatePerAreaValue, SelectedProduct.Product.Density);

			var totalArea = (view != null) ? view.TotalArea : null;

			var tankInfo = (view != null) ? view.TankInformation : null;

			ProductArea = totalArea.Unit.GetMeasure(totalArea.Value * (double)productAreaPercent);
			var calculation = applicationCalculationService.RecalculateByArea(
				SelectedProduct.Product,
				null,
				rate,
				ProductArea,
				tankInfo == null ? 0m : tankInfo.TankCount,
				AllowedTotalUnits
			);

			TotalProductValue = calculation.TotalProductValue;
			TotalProductUnit = calculation.TotalProductUnit;
			RatePerTankValue = calculation.RatePerTankValue;

			if (calculation.RatePerTankUnit != null) {
				SelectedRatePerTankUnit = calculation.RatePerTankUnit;
			}
		}

		void RecalculateByTotal() {
			if (SelectedProduct == null || TotalProductUnit == null) {
				SelectedRatePerAreaUnitHelper = null;
				RatePerAreaValue = 0m;

				SelectedRatePerTankUnit = null;
				RatePerTankValue = 0m;
				return;
			}

			var total = TotalProductUnit.GetMeasure((double)TotalProductValue, SelectedProduct.Product.Density);

			var totalArea = (view != null) ? view.TotalArea : null;
			var tankInfo = (view != null) ? view.TankInformation : null;

			var productArea = totalArea.Unit.GetMeasure(totalArea.Value * (double)productAreaPercent);

			var calculation = applicationCalculationService.RecalculateByTotal(
				SelectedProduct.Product,
				total,
				productArea,
				tankInfo == null ? 0m : tankInfo.TankCount,
				AllowedTotalUnits,
				null
			);

			ProductArea = calculation.ProductArea;
			RatePerAreaValue = calculation.RatePerAreaValue;
			SelectedRatePerAreaUnitHelper = (from r in AllowedRateUnits where r.Unit.Name == calculation.PackageSafeUnit.Name select r).FirstOrDefault();
			RatePerTankValue = calculation.RatePerTankValue;

			if (calculation.RatePerTankUnit != null) {
				SelectedRatePerTankUnit = calculation.RatePerTankUnit;
			}
		}

		void RecalculateByTank() {
			if (SelectedProduct == null || SelectedRatePerTankUnit == null) {
				SelectedRatePerAreaUnitHelper = null;
				RatePerAreaValue = 0m;

				TotalProductValue = 0;
				TotalProductUnit = new NullUnit();
				return;
			}

			var ratePerTank = SelectedRatePerTankUnit.GetMeasure((double)RatePerTankValue, SelectedProduct.Product.Density);
			var totalArea = (view != null) ? view.TotalArea : null;
			var tankInfo = (view != null) ? view.TankInformation : null;

			var productArea = totalArea.Unit.GetMeasure(totalArea.Value * (double)productAreaPercent);

			var calculation = applicationCalculationService.RecalculateByTank(
				SelectedProduct.Product,
				null,
				ratePerTank,
				productArea,
				tankInfo.TankCount
			);

			ProductArea = calculation.ProductArea;
			TotalProductValue = calculation.TotalProductValue;
			TotalProductUnit = calculation.TotalProductUnit;
			RatePerAreaValue = calculation.RatePerAreaValue;

			SelectedRatePerAreaUnitHelper = (from r in AllowedRateUnits where r.Unit.Name == calculation.PackageSafeUnit.Name select r).FirstOrDefault();

		}

		void OnProductCreated(MiniProduct product) {
			//var pli = parent.AddTemporaryMiniProduct(product, new ProductId(ApplicationEnvironment.CurrentDataSourceId, product.Id), true);
			//SelectedProduct = pli;
		}

		internal RecommendationDetailsViewModel.ProductDetails ToProductDetails() {
			var prod = new RecommendationDetailsViewModel.ProductDetails() {
				ProductName = clientEndpoint.GetMasterlistService().GetProductDisplay(SelectedProduct.ProductId),
				RateMeasure = UnitFactory.GetUnitByName(SelectedRatePerAreaUnitHelper.Unit.Name).GetMeasure((double)RatePerAreaValue, SelectedProduct.Product.Density),
				AreaUnit = UnitFactory.GetUnitByName(totalArea.Unit.Name),
				TotalProduct = UnitFactory.GetUnitByName(TotalProduct.Unit.Name).GetMeasure(TotalProduct.Value, SelectedProduct.Product.Density),
				ID = SelectedProduct.ProductId,
				ProductAreaPercent = ProductAreaPercent,
				TrackingId = trackingId,
				TargetPest = SelectedPest != null ? new MiniPest() { Name = SelectedPest.CommonName, LatinList = SelectedPest.LatinName, id = SelectedPest.PestId } : new MiniPest(),
				ApplicationMethod = ApplicationMethod,
				Resolved = Resolved,
			};

			return prod;
		}

		internal RecommendationProductEntry ToRecommendationProductEntry() {
			// TODO: This needs to represent the actual rate unit the user has chosen.
			var psu = UnitFactory.GetPackageSafeUnit(SelectedProduct.Product.StdPackageUnit, SelectedProduct.Product.StdUnit, SelectedProduct.Product.StdFactor, SelectedProduct.Product.StdPackageUnit);

			var ratePerAreaUnit = SelectedRatePerAreaUnitHelper != null ? SelectedRatePerAreaUnitHelper.Unit.Name : psu.Name;
			var appMethod = ApplicationMethod == null ? string.Empty : ApplicationMethod.Name;

			MiniPest targetPest = null;

			if (SelectedPest != null) {
				targetPest = new MiniPest() {
					id = SelectedPest.PestId,
					Name = SelectedPest.CommonName,
					LatinList = SelectedPest.LatinName,
					text = ""
				};
			}

			var rpe = new RecommendationProductEntry(
				selectedProduct.ProductId,
				SelectedProduct.Product.Name,
				SelectedProduct.Product.Manufacturer,
				trackingId,
				ratePerAreaValue,
				ratePerAreaUnit,
				ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit,
				RatePerTankValue,
				SelectedRatePerTankUnit.Name,
				(decimal)TotalProduct.Value,
				TotalProduct.Unit.Name,
				ProductAreaPercent,
				(decimal)ProductArea.Value,
				ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit,
				SelectedProduct.Product.Density,
				appMethod,
				targetPest
			);

			return rpe;
		}

		internal string ToName() {
			if (SelectedProduct != null && SelectedProduct.Product != null) {
				return SelectedProduct.Product.Name;
			} else {
				return string.Empty;
			}
		}

		async void BuildProductList() {
			var watch = Stopwatch.StartNew();

			await Task.Run(() => {
				var mpl = clientEndpoint.GetMasterlistService().GetProductList();
				var mur = clientEndpoint.GetView<ProductUsageView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear)).GetValue(new ProductUsageView());
				var ucp = clientEndpoint.GetView<UserCreatedProductList>(new DataSourceId(ApplicationEnvironment.CurrentDataSourceId)).GetValue(new UserCreatedProductList());

				Guid mlDsId = GlobalIdentifiers.GetMasterlistDataSourceId();
				var productGuids = mur.GetProductGuids();

				var q = from p in mpl
						let cont = productGuids.Contains(p.Id)
						select new ProductListItemDetails(p, mlDsId, cont ? Strings.UsedThisYear_Text : Strings.Unused_Text, cont ? (short)0 : (short)1);

				var ucpQ = from p in ucp.UserCreatedProducts
						   let cont = productGuids.Contains(p.Id)
						   select new ProductListItemDetails(p, ApplicationEnvironment.CurrentDataSourceId, cont ? Strings.UsedThisYear_Text : Strings.Unused_Text, cont ? (short)0 : (short)1);

				Products = new ObservableCollection<ProductListItemDetails>(q.Union(ucpQ).OrderBy(x => x.GroupSortOrder).ToList());

				RaisePropertyChanged(() => Products);
			});

			watch.Stop();
		}

		void PopulatePests() {
            var ml = clientEndpoint.GetMasterlistService();
            var q = from p in ml.GetPestList()
                        //where p.Language == "en"
                    select new PestListItemDetails(p, (p.Name != string.Empty ? (short)0 : (short)1));
            q = q.OrderBy(b => b.GroupSortOrder).ThenBy(x => x.CommonName);
            PestList = new ObservableCollection<PestListItemDetails>(q);
            RaisePropertyChanged(() => PestList);
        }

        void CancelEdit() {
			//just close the popup message
			//Messenger.Default.Send(new HidePopupMessage());
			Messenger.Default.Send(new HideOverlayMessage());
		}

		void CreateUserProduct() {
			IsProductListVisible = false;
			IsCreateProductPopupOpen = false;

			var vm = new Secondary.UserCreatedProduct.UserProductCreatorViewModel(clientEndpoint, ProductSearchText, OnProductCreated);
			Messenger.Default.Send(new ShowOverlayMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Secondary.UserCreatedProduct.UserProductCreatorView), "createProduct", vm)
			});

			//ShowPopupMessage msg = new ShowPopupMessage() {
			//    ScreenDescriptor = new ScreenDescriptor("Landdb.Views.Secondary.UserCreatedProduct.UserProductCreatorView", "createProduct",
			//        new UserProductCreatorViewModel(endpoint, ProductSearchText, OnProductCreated))
			//};
			//Messenger.Default.Send<ShowPopupMessage>(msg);
		}

		#region Custom Validation
		public static ValidationResult ValidateSelectedProduct(ProductListItemDetails product, ValidationContext context) {
			ValidationResult result = null;

			if (product == null) {
				result = new ValidationResult(Strings.ProductIsRequired_Text);
			}

			return result;
		}

		public static ValidationResult ValidateUnit(object unit, ValidationContext context) {
			ValidationResult result = null;

			var vm = (ChangeRecProductViewModel)context.ObjectInstance;

			var validateRatePerArea = vm.ApplyBy == ApplicationTypes.RatePerArea && context.MemberName == nameof(SelectedRatePerAreaUnitHelper);
			var validateRatePerTank = vm.ApplyBy == ApplicationTypes.RatePerTank && context.MemberName == nameof(SelectedRatePerTankUnit);
			var validateTotalProduct = vm.ApplyBy == ApplicationTypes.TotalProduct && context.MemberName == nameof(TotalProductUnit);

			var shouldValidate = validateRatePerArea || validateRatePerTank || validateTotalProduct;

			if (shouldValidate && unit == null) {
				result = new ValidationResult(Strings.ValidationResult_InvalidUnit_Text);
			}

			return result;
		}

		public static ValidationResult ValidateSelectedPest(PestListItemDetails selectedPest, ValidationContext context) {
			ValidationResult result = null;

			var vm = (ChangeRecProductViewModel)context.ObjectInstance;

			if (!string.IsNullOrWhiteSpace(vm.PestSearchText) && selectedPest == null) {
				result = new ValidationResult(Strings.InvalidPestSelected_Text);
			}

			return result;
		}
		#endregion
	}
}