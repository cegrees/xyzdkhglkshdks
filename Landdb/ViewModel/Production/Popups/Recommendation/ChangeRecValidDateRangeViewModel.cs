﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Recommendation;
using Landdb.Infrastructure.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Landdb.ViewModel.Production.Popups.Recommendation
{
    public class ChangeRecValidDateRangeViewModel : ViewModelBase
    {
        IClientEndpoint endpoint;
        RecommendationView recView;
        string timingEventt;
        List<string> timingEvents;

        public ChangeRecValidDateRangeViewModel(IClientEndpoint endpoint, RecommendationView view)
        {
            this.endpoint = endpoint;
            this.recView = view;

            ProposedDate = recView.ProposedDate;
            ExpirationDate = recView.ExpirationDate;
            TimingEventt = recView.TimingEvent;
            timingEvents = new List<string>();
            List<TimingEvent> TimingEventsss = endpoint.GetMasterlistService().GetTimingEventList().OrderBy(x => x.Order).ToList();
            foreach(var tname in TimingEventsss) {
                timingEvents.Add(tname.Name);
            }

            UpdateCommand = new RelayCommand(this.UpdateValues);
            CancelCommand = new RelayCommand(this.CancelEdit);
        }

        public ICommand UpdateCommand { get; set; }
        public ICommand CancelCommand { get; set; }

        public DateTime? ProposedDate { get; set; }
        public DateTime? ExpirationDate { get; set; }

        public List<string> TimingEvents {
            get { return timingEvents; }
            set {
                timingEvents = value;
                RaisePropertyChanged(() => TimingEvents);
            }
        }

        public string TimingEventt {
            get { return timingEventt; }
            set {
                timingEventt = value;
                RaisePropertyChanged(() => TimingEventt);
            }
        }

        void UpdateValues()
        {
            if (recView != null)
            {
                if ((ProposedDate != recView.ProposedDate) || (ExpirationDate != recView.ExpirationDate))
                {
                    ChangeRecommendationValidDateRange recommendationCommand = new ChangeRecommendationValidDateRange(recView.Id, endpoint.GenerateNewMetadata(), ProposedDate, ExpirationDate);
                    endpoint.SendOne(recommendationCommand);
                }

                if (TimingEventt != recView.TimingEvent) {
                    ChangeRecommendationTimingEvent recommendationCommand2 = new ChangeRecommendationTimingEvent(recView.Id, endpoint.GenerateNewMetadata(), timingEventt, recView.TimingEventTag);
                    endpoint.SendOne(recommendationCommand2);
                }
            }
            Messenger.Default.Send(new HidePopupMessage());
        }

        void CancelEdit()
        {
            Messenger.Default.Send(new HidePopupMessage());
        }
    }
}
