﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Recommendation;
using Landdb.Infrastructure.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Landdb.ViewModel.Production.Popups.Recommendation
{
    public class ChangeRecNotesViewModel : ViewModelBase
    {
        IClientEndpoint endpoint;
        RecommendationView view;

        public ChangeRecNotesViewModel(IClientEndpoint endpoint, RecommendationView view)
        {
            this.endpoint = endpoint;
            this.view = view;

            Notes = view.Notes;

            Update = new RelayCommand(this.UpdateNotes);
            Cancel = new RelayCommand(this.CancelNotes);
        }

        public ICommand Update { get; set; }
        public ICommand Cancel { get; set; }

        public string Notes { get; set; }
        public bool IsEditable
        {
            get
            {
                if (view.RecSource == GlobalStrings.RecommendationType_Agrian)
                    return false;
                else
                    return true;
            }
        }
        public bool IsReadOnly
        {
            get
            {
                if (view.RecSource == GlobalStrings.RecommendationType_Agrian)
                    return true;
                else
                    return false;
            }
        }
        void UpdateNotes()
        {
            if (view != null && view.Notes != Notes)
            {
                ChangeRecommendationNotes recCommand = new ChangeRecommendationNotes(view.Id, endpoint.GenerateNewMetadata(), Notes);
                endpoint.SendOne(recCommand);
                Messenger.Default.Send(new HidePopupMessage());
            }
        }

        void CancelNotes()
        {
            Messenger.Default.Send(new HidePopupMessage());
        }
    }
}
