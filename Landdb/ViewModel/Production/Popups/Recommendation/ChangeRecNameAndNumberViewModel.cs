﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Recommendation;
using Landdb.Infrastructure.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Landdb.ViewModel.Production.Popups.Recommendation
{
    public class ChangeRecNameAndNumberViewModel : ViewModelBase
    {
        IClientEndpoint endpoint;
        RecommendationView recView;

        public ChangeRecNameAndNumberViewModel(IClientEndpoint endpoint, RecommendationView view)
        {
            this.endpoint = endpoint;
            this.recView = view;

            RecName = recView.RecTitle;
            RecNumber = recView.RecNumber;

            UpdateCommand = new RelayCommand(this.UpdateValues);
            CancelCommand = new RelayCommand(this.CancelEdit);
        }

        public ICommand UpdateCommand { get; set; }
        public ICommand CancelCommand { get; set; }

        public string RecName { get; set; }
        public string RecNumber { get; set; }

        void UpdateValues()
        {
            if (recView != null)
            {
                if (RecName != recView.RecTitle)
                {
                    //change rec name
                    ChangeRecommendationTitle recommendationCommand = new ChangeRecommendationTitle(recView.Id, endpoint.GenerateNewMetadata(), RecName);
                    endpoint.SendOne(recommendationCommand);
                }
                if (RecNumber != recView.RecNumber)
                {
                    //change rec name
                    ChangeRecommendationNumber recommendationCommand = new ChangeRecommendationNumber(recView.Id, endpoint.GenerateNewMetadata(), RecNumber);
                    endpoint.SendOne(recommendationCommand);
                }
            }
            Messenger.Default.Send(new HidePopupMessage());
        }

        void CancelEdit()
        {
            Messenger.Default.Send(new HidePopupMessage());
        }
    }
}
