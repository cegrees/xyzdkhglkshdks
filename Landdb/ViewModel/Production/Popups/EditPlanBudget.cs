﻿using AgC.UnitConversion;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Plan;
using Landdb.Domain.ReadModels.Shared;
using Landdb.Infrastructure.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Production.Popups
{
    public class EditPlanBudget : ViewModelBase
    {
        PlanView plan;
        IClientEndpoint endpoint;
        PlanDetailsViewModel parent;
        double rent;
        double equipment;
        double labor;
        double insurance;
        double returnToMgt;
        double taxes;
        double repairs;
        bool isSet;
        double revenue;
        double budget;
        double netRevenue;

        public EditPlanBudget(IClientEndpoint endPoint, PlanView detail, PlanDetailsViewModel parent)
        {
            this.plan = detail;
            this.endpoint = endPoint;
            this.parent = parent;
            isSet = false;

            InitializeProperties(plan);
            Update = new RelayCommand(UpdateBudget);
            Cancel = new RelayCommand(CancelBudget);
        }

        public RelayCommand Update { get; set; }
        public RelayCommand Cancel { get; set; }

        public double Rent { get { return rent; } set { rent = value; if(isSet) CalculateNetRevenue(); } }
        public double Insurance { get { return insurance; } set { insurance = value; if (isSet) CalculateNetRevenue(); } }
        public double Equipment { get { return equipment; } set { equipment = value; if (isSet) CalculateNetRevenue(); } }
        public double Labor { get { return labor; } set { labor = value; if (isSet) CalculateNetRevenue(); } }
        public double Repairs { get { return repairs; } set { repairs = value; if (isSet) CalculateNetRevenue(); } }
        public double Taxes { get { return taxes; } set { taxes = value; if (isSet) CalculateNetRevenue(); } }
        public double Management { get { return returnToMgt; } set { returnToMgt = value; if (isSet) CalculateNetRevenue(); } }

        public double NetRevenue { get { return netRevenue; } set { netRevenue = value; RaisePropertyChanged("NetRevenue"); } }
        public double Revenue { get { return revenue; } set { revenue = value; RaisePropertyChanged("Revenue"); } }
        public double Budget { get { return budget; } set { budget = value; RaisePropertyChanged("Budget"); } }
        public double Inputs { get; set; }

        void InitializeProperties(PlanView view)
        {
            Rent = view.LandRent;
            Taxes = view.Taxes;
            Repairs = view.Repairs;
            Management = view.ReturnToMgt;
            Labor = view.Labor;
            Equipment = view.Equipment;
            Insurance = view.Insurance;

            isSet = true;
            CalculateNetRevenue();
        }

        void CalculateNetRevenue()
        {
            Revenue = parent.Revenue;
            var expend = (Rent + Taxes + Repairs + Management + Labor + Equipment + Insurance) * parent.EstimatedArea.Value;
            Budget = expend;
            var inputCost = 0.0;

            foreach (IncludedProduct pd in plan.Products)
            {
                inputCost += (double)pd.CostPerUnit * (double)pd.TotalProductValue;
            }

            Inputs = inputCost;
            NetRevenue = revenue - (expend + inputCost);
        }

        void UpdateBudget()
        {
            PlanInfo info = new PlanInfo();
            if( IsInfoChanged() )
            {
                info.LandRent = Rent;
                info.Equipment = Equipment;
                info.Insurance = Insurance;
                info.Labor = Labor;
                info.ReturnToMgt = Management;
                info.Repairs = Repairs;
                info.Taxes = Taxes;

                info.PricePerUnit = plan.PricePerUnit;
                info.YieldPerArea = plan.YieldPerArea;
                info.FSAPayment = plan.FSAPayment;
                info.EstimatedAreaValue = plan.EstimatedArea.Value;
                info.CropSharePercent = plan.CropSharePercent;

                var infoCommand = new ChangePlanInfo(plan.Id, endpoint.GenerateNewMetadata(), info);
                endpoint.SendOne(infoCommand);
            }
            
            Messenger.Default.Send(new HidePopupMessage());
        }

        bool IsInfoChanged()
        {
            return Rent != plan.LandRent || Equipment != plan.Equipment || Insurance != plan.Insurance || Labor != plan.Labor || Management != plan.ReturnToMgt || Repairs != plan.Repairs || Taxes != plan.Taxes;
        }

        void CancelBudget()
        {
            Messenger.Default.Send(new HidePopupMessage());
        }
    }
}
