﻿using GalaSoft.MvvmLight;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Documents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Landdb.Resources;

namespace Landdb.ViewModel.Resources {
	public class ContractPageFilterViewModel : ViewModelBase, IPageFilterViewModel {

		readonly IClientEndpoint clientEndpoint;

		public ContractPageFilterViewModel(IClientEndpoint clientEndpoint) {
			this.clientEndpoint = clientEndpoint;
		}

		public void ClearFilter() {
			FilterText = string.Empty;
			StartDate = null;
			EndDate = null;
			ContractType = string.Empty;
		}

		public void BeforeFilter() {
			// do nothing
		}

		public void RefreshLists() { }

        public bool FilterItem(object item) {
            var li = item as ContractListItemViewModel;

            // note: false return means the item is not displayed (filtered out)
            if (li == null) {
                return false;
            } else if (StartDate.HasValue && li.Date < StartDate.Value.Date) {
                return false;
            } else if (EndDate.HasValue && li.Date > (EndDate.Value.Date + TimeSpan.FromDays(1) - TimeSpan.FromSeconds(1))) {
                return false;
            } else if (!string.IsNullOrWhiteSpace(filterText) && !li.Name.ToLower().Contains(filterText.ToLower())) {
                return false;
            }
            else if (!string.IsNullOrWhiteSpace(ContractType) && ContractType != "Copy" && ContractType != "NotCopied" && ContractType != li.Type) {
                return false;
            }
            else if (ContractType == "Copy") {
                var docUse = clientEndpoint.GetView<DocumentUseIndex>(li.Id);
                if (!docUse.HasValue || docUse.Value.SourcedItems == null || !docUse.Value.SourcedItems.Any(i => i is ContractId)) { return false; }
            }
            else if (ContractType == "NotCopied") {
                var docUse = clientEndpoint.GetView<DocumentUseIndex>(li.Id);
                if (!docUse.HasValue || docUse.Value.SourcedItems == null || !docUse.Value.SourcedItems.Any(i => i is ContractId)) { return true; } else { return false; }
            }
            return true;
        }

        public Dictionary<string, string> ContractTypeDic
        {
            get
            {
                return new Dictionary<string, string>() {
                    { string.Empty, Strings.AllContracts_Text},
                    { Strings.Rent_Text, Strings.RentContracts_Text },
                    { Strings.Production_Text, Strings.ProductionContracts_Text },
                    { "Copy", Strings.UsedInContractCopy_Text },
                    { "NotCopied", Strings.NotUsedInContractCopy_Text },
                };
            }
        }
        private string filterText;
		public string FilterText {
			get { return filterText; }
			set {
				filterText = value;
				RaisePropertyChanged(() => FilterText);
			}
		}

		private DateTime? startDate;
		public DateTime? StartDate {
			get { return startDate; }
			set {
				startDate = value;
				RaisePropertyChanged(() => StartDate);
			}
		}

		private DateTime? endDate;
		public DateTime? EndDate {
			get { return endDate; }
			set {
				endDate = value;
				RaisePropertyChanged(() => EndDate);
			}
		}

		private string contractType = string.Empty;
		public string ContractType {
			get { return contractType; }
			set {
				contractType = value;
				RaisePropertyChanged(() => ContractType);
			}
		}
	}
}
