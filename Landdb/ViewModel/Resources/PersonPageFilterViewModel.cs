﻿using GalaSoft.MvvmLight;
using Landdb.Client.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Resources {
	public class PersonPageFilterViewModel : ViewModelBase, IPageFilterViewModel {

		readonly IClientEndpoint clientEndpoint;
		readonly CropYearId currentCropYearId;

		public PersonPageFilterViewModel(IClientEndpoint clientEndpoint, CropYearId currentCropYearId) {
			this.clientEndpoint = clientEndpoint;
			this.currentCropYearId = currentCropYearId;
		}

		private string filterText;
		public string FilterText {
			get { return filterText; }
			set {
				filterText = value;
				RaisePropertyChanged(() => FilterText);
			}
		}

		public bool FilterItem(object item) {
			var li = item as PersonListItemViewModel;

			// note: false return means the item is not displayed (filtered out)
			if (li == null) {
				return false;
			} else if (!string.IsNullOrWhiteSpace(filterText)) {
				return li.Name.ToLower().Contains(filterText.ToLower());
			}

			return true;
		}

		public void ClearFilter() {
			FilterText = string.Empty;
		}

		public void BeforeFilter() {
			// do nothing
		}

		public void RefreshLists() { }
	}
}
