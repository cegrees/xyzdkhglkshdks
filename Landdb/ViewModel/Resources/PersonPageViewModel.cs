﻿using ExportToExcel;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Person;
using Landdb.Infrastructure.Messages;
using Landdb.ReportModels.Person;
using Landdb.ViewModel.Resources.Popups;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Resources {
	public class PersonPageViewModel : AbstractListPage<PersonListItemViewModel, PersonDetailsViewModel> {

		public PersonPageViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher)
			: base(clientEndpoint, dispatcher) {
			Messenger.Default.Register<PersonAddedMessage>(this, msg => {
				RefreshItemsList(() => {
					var q = from li in ListItems
							where li.Id == msg.LastPersonAdded
							select li;
					SelectedListItem = q.FirstOrDefault();
				});
			});

			FilterModel = new PersonPageFilterViewModel(clientEndpoint, new CropYearId(currentDataSourceId.Id, currentCropYear));

			PrintCommand = new RelayCommand(printReport);
		}

		public ICommand PrintCommand { get; }

		public override string GetEntityName() => Strings.Person_Text.ToLower();
		public override string GetPluralEntityName() => Strings.People_Text.ToLower();

		protected override IEnumerable<PersonListItemViewModel> GetListItemModels() {
			var currentCropYearId = new CropYearId(currentDataSourceId.Id, currentCropYear);
			return PersonListItemViewModel.GetListItemsForCropYear(clientEndpoint, currentCropYearId, false);
		}

		protected override async Task<ScreenDescriptor> CreateScreenDescriptorForNewItemCreation() {
			var personId = new PersonId(currentDataSourceId.Id, Guid.NewGuid());

			var vm = new AddPersonViewModel(clientEndpoint, dispatcher, personId, currentCropYear, li => {

			    List<PersonListItemViewModel> temporaryListItems = ListItems.ToList();
			    temporaryListItems.Add(li);
			    ListItems.Clear();
			    temporaryListItems = temporaryListItems.OrderBy(listItem => listItem.Name).ToList();
			    temporaryListItems.ForEach(listItem =>
			    {
			        ListItems.Add(listItem);
			    });

                SelectedListItem = ListItems.SingleOrDefault(x => x.Id == li.Id);
			    ApplyFilter();
			});

			return new ScreenDescriptor(typeof(Views.Resources.Popups.AddPersonView), "add new company", vm);
		}

        protected override bool PerformItemRemoval(PersonListItemViewModel selectedItem) {
            var removeCommand = new RemovePersonFromYear(selectedItem.Id, clientEndpoint.GenerateNewMetadata(), currentCropYear);
            clientEndpoint.SendOne(removeCommand);
            return true;
        }

		protected override IDomainCommand CreateSetCharmCommand(PersonListItemViewModel selectedItem, string charmName) {
			// no charms on people currently
			throw new NotImplementedException();
		}

		protected override PersonDetailsViewModel CreateDetailsModel(PersonListItemViewModel selectedItem) {
			var personDetailsMaybe = clientEndpoint.GetView<PersonDetailsView>(selectedItem.Id);

			if (personDetailsMaybe.HasValue) {
				return new PersonDetailsViewModel(clientEndpoint, dispatcher, personDetailsMaybe.Value);
			} else {
				return null;
			}
		}

		protected override IEnumerable<RestorableItem> GetRestorableItems() {
			var personList = clientEndpoint.GetView<PersonListView>(currentDataSourceId).GetValue(() => new PersonListView());

			return from p in personList.Persons
				   where p.RemovedCropYear.HasValue && p.RemovedCropYear <= currentCropYear
				   orderby p.Name
				   select new RestorableItem(p.Id.Id, p.Name, Strings.Person_Text, p.InitialCropYear, p.RemovedCropYear.Value);
		}

		protected override void RestoreDeletedItem(Guid id) {
			// TODO: send command
		}

		private void printReport() {
			string filename = string.Empty;
			//get map from ViewModel
			List<PersonData> peopleData = new List<PersonData>();
			foreach (var item in ListItems) {
				PersonData data = new PersonData();
				var detailsMaybe = clientEndpoint.GetView<PersonDetailsView>(item.Id);

				if (detailsMaybe.HasValue) {
					var details = detailsMaybe.Value;
					data.Name = details.Name;
					if (details.StreetAddress != null) {
						data.Address = details.StreetAddress.AddressLine1;
						data.City = details.StreetAddress.City;
						data.State = details.StreetAddress.State;
                        data.Address2 = details.StreetAddress.AddressLine2;
                        data.ZipCode = details.StreetAddress.ZipCode;
					}
					data.PhoneNumber = details.PrimaryPhoneNumber;
					data.Email = details.PrimaryEmail;
					data.ApplicatorLicense = details.ApplicatorLicenseNumber;
					data.LicenseExpiration = details.ApplicatorLicenseExpiration;
					data.Company = details.CompanyName;
                    data.Notes = details.Notes;

					peopleData.Add(data);
				}
			}

			DataTable dt1 = new DataTable();

			dt1 = CreateExcelFile.ListToDataTable(peopleData, true);

			dt1.TableName = Strings.PeopleDetails_Text;

			DataSet ds = new DataSet("excelDS");
			ds.Tables.Add(dt1);

			try {
				//open up file dialog to save file....
				//then call createexcelfile to create the excel...
				SaveFileDialog saveFileDialog1 = new SaveFileDialog();

				saveFileDialog1.Filter = "Excel|*.xlsx";
				saveFileDialog1.FilterIndex = 2;
				saveFileDialog1.RestoreDirectory = true;

				bool? resultSaved = saveFileDialog1.ShowDialog();

				// Process save file dialog box results 
				if (resultSaved == true) {
					// Save document 
					filename = saveFileDialog1.FileName;
					CreateExcelFile.CreateExcelDocument(ds, filename);
				}

				//now open file....
				System.Diagnostics.Process.Start(filename);

				Messenger.Default.Send(new HidePopupMessage());
			} catch (Exception ex) {
				Messenger.Default.Send(new HidePopupMessage());
				return;
			}
		}
	}
}