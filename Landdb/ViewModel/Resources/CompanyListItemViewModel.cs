﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using Landdb.Domain.ReadModels.Company;
using Landdb.Client.Infrastructure;

namespace Landdb.ViewModel.Resources {
	public class CompanyListItemViewModel : ViewModelBase, IEquatable<CompanyListItemViewModel> {

		public CompanyListItemViewModel(CompanyListItem li) {
			Id = li.Id;
			Name = li.Name;
			Email = li.EmailAddress;
			PrimaryPhoneNumber = li.PrimaryPhoneNumber;
		}

		public CompanyListItemViewModel(CompanyId id, string name, string primaryPhoneNumber, string primaryEmailAddress) {
			Id = id;
			Name = name;
			PrimaryPhoneNumber = primaryPhoneNumber;
			Email = primaryEmailAddress;
		}

		// private constructor for adding empty entries to lists
		private CompanyListItemViewModel(string name) {
			Id = null;
			Name = name;
		}

		public CompanyId Id { get; }
		public string Name { get; }
		public string Email { get; }
		public string PrimaryPhoneNumber { get; }

		public static List<CompanyListItemViewModel> GetListItemsForCropYear(IClientEndpoint clientEndpoint, CropYearId cropYearId, bool shouldInsertEmptyEntryAtTop) {

			var companyListView = clientEndpoint.GetView<CompanyListView>(new DataSourceId(cropYearId.DataSourceId)).GetValue(new CompanyListView());
			var sortedFilteredProjectedCompanyList = from c in companyListView.Companies
													 where c.IsActiveInCropYear(cropYearId.Id)
													 orderby c.Name
													 select new CompanyListItemViewModel(c);

			var companyList = sortedFilteredProjectedCompanyList.ToList();

			if (shouldInsertEmptyEntryAtTop && companyList.Any()) {
				companyList.Insert(0, new CompanyListItemViewModel(string.Empty));
			}

			return companyList;
		}

		public bool Equals(CompanyListItemViewModel other) {
			if (ReferenceEquals(null, other)) { return false; }
			if (ReferenceEquals(this, other)) { return true; }

			return Id == other.Id;
		}

		public override string ToString() {
			return Name;
		}
	}
}