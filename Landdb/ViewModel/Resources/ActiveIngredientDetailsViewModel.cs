﻿using AgC.UnitConversion;
using AgC.UnitConversion.MassAndWeight;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.ActiveIngredientSetting;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Resources;


namespace Landdb.ViewModel.Resources {
	public class ActiveIngredientDetailsViewModel : ViewModelBase {

		private readonly IClientEndpoint clientEndpoint;
		private readonly Dispatcher dispatcher;
		private readonly ActiveIngredientId ingredientId;

		private readonly ActiveIngredientSettingId settingsId;
		private readonly ActiveIngredientSettingsView settings;

		public ActiveIngredientDetailsViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, ActiveIngredientId ingredientId) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			this.ingredientId = ingredientId;
			var name = clientEndpoint.GetMasterlistService().GetActiveIngredientDisplay(ingredientId);
			settingsId = new ActiveIngredientSettingId(ApplicationEnvironment.CurrentDataSourceId, ingredientId);
			settings = clientEndpoint.GetView<ActiveIngredientSettingsView>(settingsId).GetValue(() => new ActiveIngredientSettingsView());

			CropItems = new ObservableCollection<ActiveIngredientDetailsCropItemViewModel>();
			PopulateCropItems();
		}

		void PopulateCropItems() {
			var ml = clientEndpoint.GetMasterlistService();
			if (ml == null) { return; }

			var cropYearId = new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear);

			var yearSettings = new List<ActiveIngredientSettingItem>();

			if (settings.ItemsByYear.ContainsKey(cropYearId.Id)) {
				yearSettings = settings.ItemsByYear[cropYearId.Id];
			} else {
				yearSettings = (from m in settings.ItemsByYear where m.Key < ApplicationEnvironment.CurrentCropYear select m).Any()
					? (from m in settings.ItemsByYear where m.Key < ApplicationEnvironment.CurrentCropYear select m).Last().Value : new List<ActiveIngredientSettingItem>();
			}

			var cropTree = clientEndpoint.GetView<CropTreeView>(cropYearId);
			List<ActiveIngredientDetailsCropItemViewModel> list = new List<ActiveIngredientDetailsCropItemViewModel>();

			if (cropTree.HasValue) {
				foreach (var crop in cropTree.Value.Crops) {
					var cropName = ml.GetCropDisplay(crop.CropId);
					var item = new ActiveIngredientDetailsCropItemViewModel(clientEndpoint, dispatcher, ingredientId, crop.CropId, cropName);
					list.Add(item);

					var yearSetting = yearSettings.Where(x => x.CropId == item.CropId).FirstOrDefault();
					if (yearSetting != null) {
						item.CurrentMaximumValue = yearSetting.MaximumValue;
						item.LastModified = yearSetting.LastModified;
						item.Notes = yearSetting.Notes;
					}
				}

				list.Sort(new Comparison<ActiveIngredientDetailsCropItemViewModel>((x, y) => x.CropName.CompareTo(y.CropName)));
				foreach (var i in list) {
					CropItems.Add(i);
				}
			}
		}

		public ObservableCollection<ActiveIngredientDetailsCropItemViewModel> CropItems { get; }
	}

	public class ActiveIngredientDetailsCropItemViewModel : ViewModelBase {
		readonly IClientEndpoint clientEndpoint;
		readonly Dispatcher dispatcher;

		ActiveIngredientId ingredientId;
		CropId cropId;
		string cropName;
		decimal? currentMaximumValue;
		DateTime? lastModified;
		string notes;

		public ActiveIngredientDetailsCropItemViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, ActiveIngredientId ingredientId, CropId cropId, string cropName) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			this.ingredientId = ingredientId;
			this.cropId = cropId;
			this.cropName = cropName;

			UpdateMaximumValueCommand = new RelayCommand(UpdateMaximumUse);
		}

		public ICommand UpdateMaximumValueCommand { get; }

		public CropId CropId {
			get { return cropId; }
			set {
				cropId = value;
				RaisePropertyChanged(() => CropId);
			}
		}

		public string CropName {
			get { return cropName; }
			set {
				cropName = value;
				RaisePropertyChanged(() => CropName);
			}
		}

		public decimal? CurrentMaximumValue {
			get { return currentMaximumValue; }
			set {
				currentMaximumValue = value;
				RaisePropertyChanged(() => CurrentMaximumValue);
				RaisePropertyChanged(() => MaximumDisplay);
			}
		}

		public string MaximumDisplay {
			get {
				string display = string.Empty;
				if (currentMaximumValue.HasValue) {
					display = string.Format(Strings.MaxValueWeighPerUnit_Text, currentMaximumValue.Value, UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).Name);
				} else {
					display = Strings.NotSet_Text.ToLower();
				}

				return display;
			}
		}

		public string Notes {
			get { return notes; }
			set {
				notes = value;
				RaisePropertyChanged(() => Notes);
			}
		}

		public DateTime? LastModified {
			get { return lastModified; }
			set {
				lastModified = value;
				RaisePropertyChanged(() => LastModified);
				RaisePropertyChanged(() => ModifiedDateDisplay);
			}
		}

		public string ModifiedDateDisplay {
			get {
				if (LastModified.HasValue) {
					return LastModified.Value.ToShortDateString();
				} else {
					return Strings.Never_Text.ToLower();
				}
			}
		}

		void UpdateMaximumUse() {
			var vm = new Shared.Popups.EditActiveIngredientLoadViewModel(CurrentMaximumValue.GetValueOrDefault(), x => {
				var domainCmd = new UpdateMaximumActiveIngredientUsePerCropYear(
					new ActiveIngredientSettingId(ApplicationEnvironment.CurrentDataSourceId, ingredientId),
					clientEndpoint.GenerateNewMetadata(),
					x.MaximumValue,
					Pound.Self.Name,
					CropId,
					ApplicationEnvironment.CurrentCropYear,
					string.Empty
				);

				clientEndpoint.SendOne(domainCmd);

				if (x.MaximumValue <= 0) {
					CurrentMaximumValue = null;
				} else {
					CurrentMaximumValue = x.MaximumValue;
				}

				LastModified = DateTime.Today;
			});

			Messenger.Default.Send(new ShowPopupMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Shared.Popups.EditActiveIngredientLoadView), "EditAiMax", vm)
			});
		}

	}
}
