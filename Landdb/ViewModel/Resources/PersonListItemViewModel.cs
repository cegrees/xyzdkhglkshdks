﻿using GalaSoft.MvvmLight;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Person;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Landdb.ViewModel.Resources {
	public class PersonListItemViewModel : ViewModelBase, IEquatable<PersonListItemViewModel> {

		public PersonListItemViewModel(PersonListItem li) {
			Id = li.Id;
			Name = li.Name;
			PrimaryEmailAddress = li.PrimaryEmailAddress;

			if (!string.IsNullOrWhiteSpace(li.PrimaryPhoneNumber)) {
				var phoneNumber = new PhoneNumber(string.Empty, li.PrimaryPhoneNumber);
				PrimaryPhoneNumber = phoneNumber.GetFormattedString();
			}
		}

		public PersonListItemViewModel(PersonId id, string name, string primaryPhoneNumber, string emailAddress) {
			Id = id;
			Name = name;
			PrimaryPhoneNumber = primaryPhoneNumber;
			PrimaryEmailAddress = emailAddress;
		}
		
		// private constructor for adding empty entries to lists
		private PersonListItemViewModel(string name) {
			Id = null;
			Name = name;
		}

		public PersonId Id { get; }
		public string Name { get; }
		public string PrimaryPhoneNumber { get; }
		public string PrimaryEmailAddress { get; }

		public static List<PersonListItemViewModel> GetListItemsForCropYear(IClientEndpoint clientEndpoint, CropYearId cropYearId, bool shouldInsertEmptyEntryAtTop) {
			var personListView = clientEndpoint.GetView<PersonListView>(new DataSourceId(cropYearId.DataSourceId)).GetValue(new PersonListView());
			var sortedFilteredPersonList = from p in personListView.Persons
										   where p.IsActiveInCropYear(cropYearId.Id)
										   orderby p.Name
										   select new PersonListItemViewModel(p);

			var personList = sortedFilteredPersonList.ToList();

			if (shouldInsertEmptyEntryAtTop && personList.Any()) {
				personList.Insert(0, new PersonListItemViewModel(string.Empty));
			}

			return personList;
		}

		public bool Equals(PersonListItemViewModel other) {
			if (ReferenceEquals(null, other)) { return false; }
			if (ReferenceEquals(this, other)) { return true; }

			return Id == other.Id;
		}

		public override string ToString() => Name;
	}
}