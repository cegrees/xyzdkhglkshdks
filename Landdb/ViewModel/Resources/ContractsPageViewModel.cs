﻿using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Contract;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Domain.ReadModels.Yield;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Resources.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Resources {
	public class ContractsPageViewModel : AbstractListPage<ContractListItemViewModel, ContractDetailsBaseViewModel> {

		public ContractsPageViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher)
			: base(clientEndpoint, dispatcher) {

			this.FilterModel = new ContractPageFilterViewModel(clientEndpoint);

			Messenger.Default.Register<ContractAddedMessage>(this, msg => {
				RefreshItemsList(() => {
					var q = from li in ListItems
							where li.Id == msg.LastContractAdded
							select li;

					SelectedListItem = q.FirstOrDefault();
				});
			});

			CreateNewRentContractCommand = new RelayCommand(onCreateNewRentContract);
			CreateNewProductionContractCommand = new RelayCommand(onCreateNewProductionContract);
            CancelContractCreationCommand = new RelayCommand(onCancelContractCreation);
            CreateRentContractFromSelectedCommand = new RelayCommand(onCopyRentContract);
            ViewReport = new RelayCommand(onUpdateReport);
		}

		public ICommand CreateNewRentContractCommand { get; }
		public ICommand CreateNewProductionContractCommand { get; }
        public ICommand CancelContractCreationCommand { get; }
        public ICommand CreateRentContractFromSelectedCommand { get; }

        
        public ICommand ViewReport { get; }

		public override string GetEntityName() => Strings.Contract_Text.ToLower();
		public override string GetPluralEntityName() => Strings.Contracts_Text.ToLower();

		public override bool CanDelete(ContractListItemViewModel toDelete) {
			var currentCropYearId = new CropYearId(currentDataSourceId.Id, currentCropYear);

			var hasAssociatedRecords = false;

			if (toDelete.Type == Strings.Rent_Text) {
				var cropZoneLoadData = clientEndpoint.GetView<CropZoneLoadDataView>(currentCropYearId).GetValue(() => new CropZoneLoadDataView()).Items;
				var cropZoneAppData = clientEndpoint.GetView<CropZoneApplicationDataView>(currentCropYearId).GetValue(() => new CropZoneApplicationDataView()).Items;

				var rentDetails = clientEndpoint.GetView<RentContractDetailsView>(toDelete.Id).GetValue(new RentContractDetailsView());

				var associatedLoads = new HashSet<LoadId>();
				var associatedApplications = new HashSet<ApplicationId>();

				foreach(var cz in rentDetails.CropZones) {
					var cropZoneLoads = cropZoneLoadData.Where(x => x.CropZoneId == cz.Id);

					foreach(var load in cropZoneLoads) {
						if (!associatedLoads.Contains(load.LoadId)) {
							associatedLoads.Add(load.LoadId);
						}
					}

					var cropZoneApplications = cropZoneAppData.Where(x => x.CropZoneId == cz.Id);
					
					foreach(var app in cropZoneApplications) {
						if (!associatedApplications.Contains(app.ApplicationId)) {
							associatedApplications.Add(app.ApplicationId);
						}
					}
				}

				var loadCount = associatedLoads.Count;
				var appCount = associatedApplications.Count;

				if (loadCount + appCount > 0) {
					RemovalPopupText = String.Format(Strings.ItemMayNotBeRemovedBecauseItIsAssociatedWith_Format_Text, toDelete.Name);

					var recordDescriptions = new List<string>();

					if (appCount > 0) { recordDescriptions.Add("     - " + String.Format(Strings.CountOfApplications_Text, appCount)); }
					if (loadCount > 0) { recordDescriptions.Add("     - " + String.Format(Strings.CountOfLoads_Format_Text, loadCount)); }

					RemovalPopupText += string.Join("\n", recordDescriptions);

					hasAssociatedRecords = true;
				}
			} else if (toDelete.Type == Strings.Production_Text) {
				var loadCount = clientEndpoint.GetView<ProductionContractLoadsView>(currentCropYearId)
					.GetValue(new ProductionContractLoadsView())
					.ProductionContractLoads
					.Values
					.Count(x => x.GrowerProductionContractId == toDelete.Id || x.LandOwnerProductionContractId == toDelete.Id);

				if (loadCount > 0) {
					RemovalPopupText = String.Format(Strings.ItemMayNotBeRemovedBecauseItHasBeenUsedIn_Format_Text, toDelete.Name);
					RemovalPopupText += " - " + String.Format(Strings.CountOfLoads_Format_Text, loadCount);
					hasAssociatedRecords = true;
				}
			} else {
				return false; // go ahead and bail if we encounter some weird contract type
			}

			return !hasAssociatedRecords;
		}

		protected override async Task CreateNewItem() {
			Messenger.Default.Send(new ShowOverlayMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Secondary.Contract.ContractCreationOverlay), "create contract", this)
			});
		}

		protected override IEnumerable<ContractListItemViewModel> GetListItemModels() {
			var cropYearId = new CropYearId(currentDataSourceId.Id, currentCropYear);

			var rentContractListView = clientEndpoint.GetView<RentContractListView>(cropYearId).GetValue(new RentContractListView());
			var rentContractListItems = rentContractListView.RentContracts.Select(x => new ContractListItemViewModel(x));

			var productionContractListView = clientEndpoint.GetView<ProductionContractListView>(cropYearId).GetValue(new ProductionContractListView());
			var productionContractListItems = productionContractListView.ProductionContracts.Select(x => new ContractListItemViewModel(x));

			return rentContractListItems.Union(productionContractListItems).OrderBy(x => x.Name);
		}

		protected override async Task<ScreenDescriptor> CreateScreenDescriptorForNewItemCreation() {
			// we've implemented a custom creation routine for new contracts. if this gets hit, we're doing something wrong.
			throw new NotImplementedException();
		}

		protected override bool PerformItemRemoval(ContractListItemViewModel selectedItem) {
			if (selectedItem == null) { return false; }

            IDomainCommand removeCommand = null;

            if (selectedItem.Type == Strings.Rent_Text) {
				removeCommand = new RemoveContractFromYear(selectedItem.Id, clientEndpoint.GenerateNewMetadata(), currentCropYear);
			} else if (selectedItem.Type == Strings.Production_Text) {
                removeCommand = new RemoveProductionContractFromYear(selectedItem.Id, clientEndpoint.GenerateNewMetadata(), currentCropYear);
			}

            if (removeCommand != null) {
                clientEndpoint.SendOne(removeCommand);
                return true;
            } else { return false; }
		}

		protected override IDomainCommand CreateSetCharmCommand(ContractListItemViewModel selectedItem, string charmName) {
			if (selectedItem.CharmName == charmName) { return null; }

			selectedItem.CharmName = charmName;

			IDomainCommand cmd = null;
			if (selectedItem.Type == Strings.Rent_Text) {
				cmd = new FlagRentContract(selectedItem.Id, clientEndpoint.GenerateNewMetadata(), charmName);
			} else if (selectedItem.Type == Strings.Production_Text) {
				cmd = new FlagProductionContract(selectedItem.Id, clientEndpoint.GenerateNewMetadata(), charmName);
			}

			return cmd;
		}

		protected override ContractDetailsBaseViewModel CreateDetailsModel(ContractListItemViewModel selectedItem) {
			ContractDetailsBaseViewModel details = null;

			if (selectedItem.Type == Strings.Rent_Text) {
				var rentContractsMaybe = clientEndpoint.GetView<RentContractDetailsView>(selectedItem.Id);
				rentContractsMaybe.IfValue(detailsView => {
					details = new Contract.Rent.RentContractDetailsViewModel(clientEndpoint, dispatcher, currentDataSourceId, currentCropYear, detailsView);
				});
			} else if (selectedItem.Type == Strings.Production_Text) {
				var productionContractsMaybe = clientEndpoint.GetView<ProductionContractDetailsView>(selectedItem.Id);
				productionContractsMaybe.IfValue(detailsView => {
					details = new Contract.Production.ProductionContractDetailsViewModel(clientEndpoint, dispatcher, currentDataSourceId, currentCropYear, detailsView);
				});
			}

			return details;
		}

		private void onCreateNewRentContract() {
			Messenger.Default.Send(new HideOverlayMessage());

			var contractId = new ContractId(currentDataSourceId.Id, Guid.NewGuid());
			var vm = new Secondary.RentContract.AddRentContractViewModel(clientEndpoint, dispatcher, contractId, currentCropYear);
			Messenger.Default.Send(new ChangeScreenDescriptorMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Secondary.RentContract.AddRentContractView), "create rent contract", vm)
			});
		}

		private void onCreateNewProductionContract() {
			Messenger.Default.Send(new HideOverlayMessage());

			var contractId = new ContractId(currentDataSourceId.Id, Guid.NewGuid());
			var vm = new Secondary.ProductionContract.AddProductionContractViewModel(clientEndpoint, dispatcher, contractId, currentCropYear);
			Messenger.Default.Send(new ChangeScreenDescriptorMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Secondary.ProductionContract.AddProductionContractView), "create production contract", vm)
			});
		}

        CopyContractsPageViewModel selectcontracts;
        void onCopyRentContract() {
            IsOptionsMapPopupOpen = false;
            selectcontracts = new CopyContractsPageViewModel(clientEndpoint, dispatcher);
            //Messenger.Default.Send(new ShowOverlayMessage() {
            //    ScreenDescriptor = new ScreenDescriptor(typeof(Landdb.Views.Resources.CopyContractsPageView), "selectContracts", selectcontracts)
            //});
            Messenger.Default.Send(new ChangeScreenDescriptorMessage() {
                ScreenDescriptor = new ScreenDescriptor(typeof(Landdb.Views.Resources.CopyContractsPageView), "copy rent contract", selectcontracts)
            });
        }

        private void onCancelContractCreation() {
			Messenger.Default.Send(new HideOverlayMessage());
		}

		private void onUpdateReport() {
            //get map from ViewModel
            //var mapGen = new MapImageGenerator();
            //var mapFile = await mapGen.GenerateMap(clientEndpoint, dispatcher, czIdQ);
            if (SelectedListItem != null && SelectedListItem.Id != null)
            {
                var vm = new Secondary.Reports.Contract.SingleContractViewModel(clientEndpoint, dispatcher, SelectedListItem.Id);
                Messenger.Default.Send(new ShowOverlayMessage()
                {
                    ScreenDescriptor = new ScreenDescriptor(typeof(Views.Secondary.Reports.Work_Order.SingleWorkOrderView), "viewReport", vm)
                });
            }
		}

        bool isOptionsMapPopupOpen = false;
        public bool IsOptionsMapPopupOpen
        {
            get { return isOptionsMapPopupOpen; }
            set
            {
                isOptionsMapPopupOpen = value;
                RaisePropertyChanged(() => IsOptionsMapPopupOpen);
            }
        }
    }
}