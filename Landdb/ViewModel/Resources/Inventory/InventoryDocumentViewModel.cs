﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using Landdb.Client.Models;
using Landdb.Client.Services.Documents;
using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Landdb.ViewModel.Resources.Inventory {
    public class InventoryDocumentViewModel : ViewModelBase {
        Logger log = LogManager.GetCurrentClassLogger();
        IDocumentService documentService;
        ProductDocument localDocumentMetadata;
        ProductDocument remoteDocumentMetadata;
        string productName;

        public InventoryDocumentViewModel(IDocumentService documentService, ProductDocument localDocumentMetadata, ProductDocument remoteDocumentMetadata, string productName) {
            this.documentService = documentService;
            this.localDocumentMetadata = localDocumentMetadata;
            this.remoteDocumentMetadata = remoteDocumentMetadata;
            this.productName = productName;

            ViewDocumentCommand = new AwaitableRelayCommand(ViewDocument);
        }

        public ICommand GetDocumentCommand { get; private set; }
        public ICommand ViewDocumentCommand { get; private set; }

        public bool NeedsUpdate {
            get {
                if (localDocumentMetadata == null || remoteDocumentMetadata == null) {
                    return false;
                }

                return remoteDocumentMetadata.LastUpdated > localDocumentMetadata.LastUpdated;
            }
        }

        public bool ExistsRemotelyOnly {
            get {
                return localDocumentMetadata == null && remoteDocumentMetadata != null;
            }
        }

        public string DocumentType {
            get {
                if (remoteDocumentMetadata != null) { return remoteDocumentMetadata.Type; }
                if (localDocumentMetadata != null) { return localDocumentMetadata.Type; }
                return string.Empty;
            }
        }

        public string DocumentDescription {
            get {
                if (remoteDocumentMetadata != null) { return remoteDocumentMetadata.Description; }
                if (localDocumentMetadata != null) { return localDocumentMetadata.Description; }
                return string.Empty;
            }
        }

        public string DocumentFilename {
            get {
                if (remoteDocumentMetadata != null) { return remoteDocumentMetadata.Filename; }
                if (localDocumentMetadata != null) { return localDocumentMetadata.Filename; }
                return string.Empty;
            }
        }

        public string ShortDocumentFilename {
            get {
                var fullFilename = DocumentFilename;

                if (fullFilename.Length > 50) {
                    return string.Format("{0}...{1}",
                        fullFilename.Substring(0, 27),
                        fullFilename.Substring((fullFilename.Length - 1) - 20, 21));
                } else {
                    return fullFilename;
                }
            }
        }

        public DateTime? LocalUpdateDate {
            get {
                if (localDocumentMetadata != null) { return localDocumentMetadata.LastUpdated; }
                return null;
            }
        }

        public DateTime? RemoteUpdateDate {
            get {
                if (remoteDocumentMetadata != null) { return remoteDocumentMetadata.LastUpdated; }
                return null;
            }
        }

        async Task ViewDocument() {
            try {
                if (NeedsUpdate || ExistsRemotelyOnly ) {

                        await GetDocument();
                }

                if (localDocumentMetadata != null)
                {
                    using (Process p = new Process())
                    {
                        p.StartInfo.RedirectStandardInput = false;
                        p.StartInfo.FileName = localDocumentMetadata.Path;

                        if (File.Exists(p.StartInfo.FileName))
                        {
                            p.StartInfo.UseShellExecute = true;
                            p.Start();
                        }
                        else
                        {
                            await GetDocument();
                            p.StartInfo.UseShellExecute = true;
                            p.Start();
                        }
                    }
                }
            } catch (Exception ex) {
                log.Warn("No application for viewing the file named {0} exists on the system. {1}", localDocumentMetadata.Filename, ex.InnerException);
                // TODO: Visually notify the user of the reason nothing happened.
            }
        }

        async Task GetDocument() {
            // TODO: Async and display a download dialog
            if (NetworkStatus.IsInternetAvailable())
            {
                var remoteDocumentData = await documentService.GetRemoteDocumentData(remoteDocumentMetadata);
                if (remoteDocumentData != null)
                {
                    var path = documentService.StoreDocumentLocally(remoteDocumentMetadata, productName, remoteDocumentData);
                    localDocumentMetadata = remoteDocumentMetadata;
                    localDocumentMetadata.Path = path;
                }
                RaisePropertyChanged("NeedsUpdate");
                RaisePropertyChanged("ExistsRemotelyOnly");
            }
        }
    }
}
