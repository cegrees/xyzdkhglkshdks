﻿using AgC.UnitConversion.Area;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.ProductSetting;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Landdb.ViewModel.Resources.Inventory
{
    public class AddInventoryReiPhiItemViewModel : ViewModelBase
    {
        readonly IClientEndpoint clientEndpoint;
        readonly Dispatcher dispatcher;

        ObservableCollection<InventoryReiPhiItemViewModel> reiPhis = new ObservableCollection<InventoryReiPhiItemViewModel>();
        string reiString;
        string phiString;
        ProductSettingId prodsettingsId;
        ProductId prodId;
        IMasterlistService ml;
        MiniCrop newItemCropSelection;
        string hour = AgC.UnitConversion.Time.Hour.Self.FullDisplay;
        string day = AgC.UnitConversion.Time.Day.Self.FullDisplay;
        InventoryDetailsViewModel parent;

        public AddInventoryReiPhiItemViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, ProductId selectedProductId, ObservableCollection<InventoryReiPhiItemViewModel> reiPhis, InventoryDetailsViewModel parent)
        {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            this.prodsettingsId = new ProductSettingId(ApplicationEnvironment.CurrentDataSourceId, selectedProductId);
            this.prodId = selectedProductId;
            this.reiPhis = reiPhis;
            this.ml = clientEndpoint.GetMasterlistService();
            this.parent = parent;
            SaveCommand = new RelayCommand(AddReiPhi);
            CancelCommand = new RelayCommand(CloseReiPhi);
        }

        public RelayCommand SaveCommand { get; set; }
        public RelayCommand CancelCommand { get; set; }

        public string ReiDisplay
        {
            get
            {
                return string.Format("{0:0.#} {1}", ReiValue, hour);
            }
            set
            {
                if (string.IsNullOrWhiteSpace(value)) { return; }

                if (reiString != value)
                {
                    reiString = value;
                    string concatString = System.Text.RegularExpressions.Regex.Replace(reiString, "[/$A-Za-z]", "");
                    concatString = concatString.Last().ToString() == "." ? concatString.Remove(concatString.Length - 1) : concatString;
                    decimal reiOut = 0m;
                    if (decimal.TryParse(concatString, out reiOut))
                    {
                        ReiValue = reiOut;
                    }
                }
            }
        }

        public decimal? ReiValue
        {
            get;
            set;
        }

        public string PhiDisplay
        {
            get
            {
                return string.Format("{0:0.#} {1}", PhiValue, day);
            }
            set
            {
                if (string.IsNullOrWhiteSpace(value)) { return; }

                if (phiString != value)
                {
                    phiString = value;
                    string concatString = System.Text.RegularExpressions.Regex.Replace(phiString, "[/$A-Za-z]", "");
                    concatString = concatString.Last().ToString() == "." ? concatString.Remove(concatString.Length - 1) : concatString;
                    decimal phiOut = 0m;
                    if (decimal.TryParse(concatString, out phiOut))
                    {
                        PhiValue = phiOut;
                    }
                }
            }
        }

        public decimal? PhiValue
        {
            get;
            set;
        }

        public string Notes { get; set; }
        public DateTime EditedOn { get; set; }
        public bool Edit { get; set; }
        public InventoryReiPhiItemViewModel ReiPhi { get; set; }
        public IEnumerable<MiniCrop> CropList
        {
            get { return ml.GetCropList().OrderBy(x => x.Name); }
        }

        public MiniCrop NewItemCropSelection
        {
            get { return newItemCropSelection; }
            set
            {
                // Remember the old name so that we can adjust default naming below, if it was defaulted before.
                var oldName = newItemCropSelection != null ? newItemCropSelection.Name : string.Empty;

                newItemCropSelection = value;

                if (value != null && (string.IsNullOrWhiteSpace(NewItemName) || NewItemName == oldName))
                {
                    NewItemName = value.Name;
                    RaisePropertyChanged("NewItemName");
                }

                var cnt = CheckForCropItem();
                if (cnt > 0)
                {
                    Edit = true;
                    ReiValue = ReiPhi.ReiValue;
                    PhiValue = ReiPhi.PhiValue;
                }
                else
                {
                    Edit = false;
                }
            }
        }
        public string NewItemName
        {
            get;
            set;
        }

        public void AddReiPhi()
        {
            if (Edit)
            {
                var prodSettings = clientEndpoint.GetView<ProductSettingsView>(prodsettingsId);
                var yearItems = prodSettings.HasValue && prodSettings.Value.ItemsByYear.ContainsKey(ApplicationEnvironment.CurrentCropYear) ? prodSettings.Value.ItemsByYear[ApplicationEnvironment.CurrentCropYear] : null;
                var cropItem = yearItems != null ? yearItems.SingleOrDefault(c => c.CropId.Id == NewItemCropSelection.Id) : null;

                if (ReiValue != (decimal)ReiPhi.ReiValue || PhiValue != (decimal)ReiPhi.PhiValue)
                {
                    UpdateProductReiPhiPerCrop command = new UpdateProductReiPhiPerCrop(prodsettingsId, clientEndpoint.GenerateNewMetadata(), new CropId(NewItemCropSelection.Id), ApplicationEnvironment.CurrentCropYear, ReiValue, hour, PhiValue, day, string.Empty);
                    clientEndpoint.SendOne(command);

                    //REPORT TO PARENT THAT A CHANGE HAS HAPPENED
                    var selectedReiPhi = parent.ReiPhis.Where(x => x.CropId == NewItemCropSelection.Id).FirstOrDefault();
                    parent.ReiPhis.Remove(selectedReiPhi);
                    RaisePropertyChanged(() => parent.ReiPhis);
                    selectedReiPhi.ReiValue = ReiValue;
                    selectedReiPhi.PhiValue = PhiValue;
                    parent.ReiPhis.Add(selectedReiPhi);
                    RaisePropertyChanged(() => parent.ReiPhis);
                }
            }
            else
            {
                if (NewItemCropSelection != null)
                {
                    UpdateProductReiPhiPerCrop command = new UpdateProductReiPhiPerCrop(prodsettingsId, clientEndpoint.GenerateNewMetadata(), new CropId(NewItemCropSelection.Id), ApplicationEnvironment.CurrentCropYear, ReiValue, hour, PhiValue, day, string.Empty);
                    clientEndpoint.SendOne(command);

                    //need to alert parent that item has been added....
                    ReiPhi rP = new Landdb.ReiPhi() { Crop = NewItemCropSelection.Id, Phi = (double?)PhiValue, PhiU = day, Rei = (double?)ReiValue, ReiU = hour };
                    ReiPhi = new InventoryReiPhiItemViewModel(clientEndpoint, dispatcher, rP, prodId);
                    ReiPhi.EditedOn = DateTime.Now;
                    ReiPhi.IsEdited = true;

                    parent.ReiPhis.Add(ReiPhi);
                    RaisePropertyChanged(() => parent.ReiPhis);
                }
            }

            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
        }

        public void CloseReiPhi()
        {
            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
        }

        public int CheckForCropItem()
        {
            var count = reiPhis.Where(c => c.CropId == NewItemCropSelection.Id).Count();
            ReiPhi = reiPhis.SingleOrDefault(c => c.CropId == NewItemCropSelection.Id);
            return count;   
        }
    }
}
