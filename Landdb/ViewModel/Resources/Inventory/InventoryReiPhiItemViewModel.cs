﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.ProductSetting;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using System;
using System.Linq;
using System.Windows.Input;
using System.Windows.Threading;

namespace Landdb.ViewModel.Resources.Inventory {
	public class InventoryReiPhiItemViewModel : ViewModelBase {

		private readonly IClientEndpoint clientEndpoint;
		private readonly Dispatcher dispatcher;

		ReiPhi reiPhi;
		string reiString;
		string phiString;
		ProductSettingId prodsettingsId;
        string hour = AgC.UnitConversion.Time.Hour.Self.FullDisplay;
        string day = AgC.UnitConversion.Time.Day.Self.FullDisplay;

        public InventoryReiPhiItemViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, ReiPhi reiPhi, ProductId selectedProductId) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;
			this.reiPhi = reiPhi;
			this.prodsettingsId = new ProductSettingId(ApplicationEnvironment.CurrentDataSourceId, selectedProductId);
			IsEdited = false;
			var ml = clientEndpoint.GetMasterlistService();
			CropName = ml.GetCropDisplay(new CropId(reiPhi.Crop));
			CropId = reiPhi.Crop;
			Initialize();

			UpdateMaximumValueCommand = new RelayCommand(OpenPopUp);
			SaveCommand = new RelayCommand(UpdateReiPhi);
			CancelCommand = new RelayCommand(CloseReiPhi);
		}

		public ICommand UpdateMaximumValueCommand { get; }
		public ICommand SaveCommand { get; }
		public ICommand CancelCommand { get; }

		public string CropName { get; set; }
		public Guid CropId { get; set; }
		public string ReiDisplay {
			get { return $"{ReiValue:0.#} {hour}"; }
			set {
				if (string.IsNullOrWhiteSpace(value)) { return; }

				if (reiString != value) {
					reiString = value;
					string concatString = System.Text.RegularExpressions.Regex.Replace(reiString, "[/$A-Za-z]", "");
					concatString = concatString.Last().ToString() == "." ? concatString.Remove(concatString.Length - 1) : concatString;
					decimal reiOut = 0m;
					if (decimal.TryParse(concatString, out reiOut)) {
						ReiValue = reiOut;
					}
				}
			}
		}

		public decimal? ReiValue { get; set; }

		public string PhiDisplay {
			get { return $"{PhiValue:0.#} {day}"; }
			set {
				if (string.IsNullOrWhiteSpace(value)) { return; }

				if (phiString != value) {
					phiString = value;
					string concatString = System.Text.RegularExpressions.Regex.Replace(phiString, "[/$A-Za-z]", "");
					concatString = concatString.Last().ToString() == "." ? concatString.Remove(concatString.Length - 1) : concatString;
					decimal phiOut = 0m;
					if (decimal.TryParse(concatString, out phiOut)) {
						PhiValue = phiOut;
					}
				}
			}
		}

		public decimal? PhiValue { get; set; }

		public string Notes { get; set; }
		public DateTime EditedOn { get; set; }
		public bool IsEdited { get; set; }

		public void Initialize() {
			ReiValue = (decimal?)reiPhi.Rei;
			PhiValue = (decimal?)reiPhi.Phi;
		}

		public void OpenPopUp() {
			//open ui to edit the reiphi values
			Messenger.Default.Send(new ShowPopupMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Resources.Popups.EditReiPhiView), "editFertView", this)
			});
		}
		public void UpdateReiPhi() {
			var prodSettings = clientEndpoint.GetView<ProductSettingsView>(prodsettingsId);
			var yearItems = prodSettings.HasValue && prodSettings.Value.ItemsByYear.ContainsKey(ApplicationEnvironment.CurrentCropYear) ? prodSettings.Value.ItemsByYear[ApplicationEnvironment.CurrentCropYear] : null;
			var cropItem = yearItems != null ? yearItems.SingleOrDefault(c => c.CropId.Id == CropId) : null;

			if (ReiValue != (reiPhi.Rei.HasValue ? (decimal)reiPhi.Rei.Value : 0) || PhiValue != (reiPhi.Phi.HasValue ? (decimal)reiPhi.Phi.Value : 0)) {
				UpdateProductReiPhiPerCrop command = new UpdateProductReiPhiPerCrop(prodsettingsId, clientEndpoint.GenerateNewMetadata(), new CropId(reiPhi.Crop), ApplicationEnvironment.CurrentCropYear, ReiValue, reiPhi.ReiU, PhiValue, reiPhi.PhiU, string.Empty);
				clientEndpoint.SendOne(command);
			} else if (IsEdited && cropItem != null && (ReiValue != cropItem.ReiValue || PhiValue != cropItem.PhiValue)) {
				UpdateProductReiPhiPerCrop command = new UpdateProductReiPhiPerCrop(prodsettingsId, clientEndpoint.GenerateNewMetadata(), new CropId(reiPhi.Crop), ApplicationEnvironment.CurrentCropYear, ReiValue, reiPhi.ReiU, PhiValue, reiPhi.PhiU, string.Empty);
				clientEndpoint.SendOne(command);
			}

			RaisePropertyChanged(() => ReiDisplay);
			RaisePropertyChanged(() => PhiDisplay);
			Messenger.Default.Send(new HidePopupMessage());
		}

		public void CloseReiPhi() {
			Messenger.Default.Send(new HidePopupMessage());
		}
	}
}