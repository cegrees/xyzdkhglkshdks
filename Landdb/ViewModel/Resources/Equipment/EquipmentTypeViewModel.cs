﻿using System;
using System.Collections.Generic;
using Landdb.Resources;

namespace Landdb.ViewModel.Resources.Equipment
{
	public class EquipmentTypeViewModel : IEquatable<EquipmentTypeViewModel>
	{
		public static List<EquipmentTypeViewModel> EquipmentTypeList { get; } = new List<EquipmentTypeViewModel>
		{
			new EquipmentTypeViewModel(EquipmentType.EARTH_MOVING, Strings.EquipmentType_EarthMoving),
			new EquipmentTypeViewModel(EquipmentType.FERTILIZER, Strings.EquipmentType_Fertilizer),
			new EquipmentTypeViewModel(EquipmentType.FORAGE_AND_HAY, Strings.EquipmentType_ForageAndHay),
			new EquipmentTypeViewModel(EquipmentType.HARVESTING, Strings.EquipmentType_Harvesting),
			new EquipmentTypeViewModel(EquipmentType.IRRIGATION, Strings.EquipmentType_Irrigation),
			new EquipmentTypeViewModel(EquipmentType.MANURE, Strings.EquipmentType_Manure),
			new EquipmentTypeViewModel(EquipmentType.MISCELLANEOUS,  Strings.EquipmentType_Miscellaneous),
			new EquipmentTypeViewModel(EquipmentType.PLANTER, Strings.EquipmentType_Planter),
			new EquipmentTypeViewModel(EquipmentType.SHOP, Strings.EquipmentType_Shop),
			new EquipmentTypeViewModel(EquipmentType.SPRAYER, Strings.EquipmentType_Sprayer),
			new EquipmentTypeViewModel(EquipmentType.TILLAGE, Strings.EquipmentType_Tillage),
			new EquipmentTypeViewModel(EquipmentType.TRACTOR, Strings.EquipmentType_Tractor),
			new EquipmentTypeViewModel(EquipmentType.TRAILER, Strings.EquipmentType_Trailer),
			new EquipmentTypeViewModel(EquipmentType.TRUCK, Strings.EquipmentType_Truck),
			new EquipmentTypeViewModel(EquipmentType.VEHICLE, Strings.EquipmentType_Vehicle)
		};

		private readonly Tuple<EquipmentType, string> tuple;

		public EquipmentTypeViewModel(EquipmentType equipmentType, string equipmentTypeString)
		{
			tuple = new Tuple<EquipmentType, string>(equipmentType, equipmentTypeString);
		}

		public EquipmentType GetEquipmentType => tuple.Item1;

		public string EquipmentTypeString => tuple.Item2;

		public bool Equals(EquipmentTypeViewModel other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return Equals(tuple, other.tuple);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return Equals((EquipmentTypeViewModel) obj);
		}

		public override int GetHashCode()
		{
			return (tuple != null ? tuple.GetHashCode() : 0);
		}
	}
}