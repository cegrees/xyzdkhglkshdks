﻿using System;
using Landdb.Resources;

namespace Landdb.ViewModel.Resources.Equipment
{
	public class EquipmentTypeViewModelFactory
    { 
		public static EquipmentTypeViewModel BuildEquipmentTypeViewModel(EquipmentType equipmentType)
		{
			return new EquipmentTypeViewModel(equipmentType, GetEquipmentTypeString(equipmentType));
		}

		private static string GetEquipmentTypeString(EquipmentType equipmentType)
		{
			switch (equipmentType)
			{
				case EquipmentType.EARTH_MOVING:
				    return Strings.EquipmentType_EarthMoving;
				case EquipmentType.FERTILIZER:
				    return Strings.EquipmentType_Fertilizer;
                case EquipmentType.FORAGE_AND_HAY:
                    return Strings.EquipmentType_ForageAndHay;
                case EquipmentType.HARVESTING:
                    return Strings.EquipmentType_Harvesting;
                case EquipmentType.IRRIGATION:
                    return Strings.EquipmentType_Irrigation;
                case EquipmentType.MANURE:
					return Strings.EquipmentType_Manure;
				case EquipmentType.MISCELLANEOUS:
					return Strings.EquipmentType_Miscellaneous;
				case EquipmentType.PLANTER:
					return Strings.EquipmentType_Planter;
				case EquipmentType.SHOP:
					return Strings.EquipmentType_Shop;
				case EquipmentType.SPRAYER:
					return Strings.EquipmentType_Sprayer;
				case EquipmentType.TILLAGE:
					return Strings.EquipmentType_Tillage;
				case EquipmentType.TRACTOR:
					return Strings.EquipmentType_Tractor;
				case EquipmentType.TRAILER:
					return Strings.EquipmentType_Trailer;
				case EquipmentType.TRUCK:
					return Strings.EquipmentType_Truck;
				case EquipmentType.VEHICLE:
					return Strings.EquipmentType_Vehicle;
				default:
					throw new ArgumentOutOfRangeException(nameof(equipmentType), equipmentType, null);
			}
		}
	}
}