﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Product;
using Landdb.Infrastructure;
using Landdb.ViewModel.Shared;

namespace Landdb.ViewModel.Resources.Equipment
{
	public class ProductListFactory
	{
		private readonly IClientEndpoint clientEndpoint;
		private readonly CropYearId cropYearId;

		private ProductListFactory(IClientEndpoint clientEndpoint, CropYearId cropYearId)
		{
			this.clientEndpoint = clientEndpoint;
			this.cropYearId = cropYearId;
		}

		public static ProductListFactory CreateProductListFactory(IClientEndpoint clientEndpoint, CropYearId cropYearId)
		{
			return new ProductListFactory(clientEndpoint, cropYearId);
		}

		public ObservableCollection<ProductListItemDetails> BuildProductList()
		{
			return BuildProductList(clientEndpoint, cropYearId);
		}

		private static ObservableCollection<ProductListItemDetails> BuildProductList(IClientEndpoint clientEndpoint, CropYearId cropYearId)
		{
			UserCreatedProductList userCreatedProductList = GetUserCreatedProductList(clientEndpoint);
			List<Guid> usedProductGuids = GetUsedProductGuids(clientEndpoint, cropYearId);
			IEnumerable<ProductListItemDetails> masterlistMiniProducts =
				GetProductsFromMasterList(GlobalIdentifiers.GetMasterlistDataSourceId(), clientEndpoint.GetMasterlistService().GetProductList(), usedProductGuids);
			IEnumerable<ProductListItemDetails> userCreatedMiniProducts =
				GetUserCreateProducts(userCreatedProductList, usedProductGuids);

			return BuildProductList(masterlistMiniProducts, userCreatedMiniProducts);
		}

		private static UserCreatedProductList GetUserCreatedProductList(IClientEndpoint clientEndpoint)
		{
			return clientEndpoint.GetView<UserCreatedProductList>(new DataSourceId(ApplicationEnvironment.CurrentDataSourceId))
				.GetValue(new UserCreatedProductList());
		}

		private static List<Guid> GetUsedProductGuids(IClientEndpoint clientEndpoint, CropYearId cropYearId)
		{
			return clientEndpoint.GetView<ProductUsageView>(cropYearId).GetValue(new ProductUsageView()).GetProductGuids();
		}

		private static IEnumerable<ProductListItemDetails> GetProductsFromMasterList(Guid masterListDataSourceId, IEnumerable<MiniProduct> miniProducts, ICollection<Guid> productGuids)
		{
			return from miniProduct in miniProducts
				let cont = productGuids.Contains(miniProduct.Id)
				select new ProductListItemDetails(miniProduct, masterListDataSourceId, cont ? "Used this year" : "Unused", cont ? (short)0 : (short)1);
		}

		private static IEnumerable<ProductListItemDetails> GetUserCreateProducts(UserCreatedProductList userCreatedProductList, ICollection<Guid> productGuids)
		{
			return from webMiniProduct in userCreatedProductList.UserCreatedProducts
				let cont = productGuids.Contains(webMiniProduct.Id)
				select new ProductListItemDetails(webMiniProduct, ApplicationEnvironment.CurrentDataSourceId, cont ? "Used this year" : "Unused", cont ? (short)0 : (short)1);
		}

		private static ObservableCollection<ProductListItemDetails> BuildProductList(IEnumerable<ProductListItemDetails> masterListProducts,
			IEnumerable<ProductListItemDetails> userCreatedProducts)
		{
			return new ObservableCollection<ProductListItemDetails>(masterListProducts.Union(userCreatedProducts).OrderBy(x => x.GroupSortOrder).ToList());
		}

		public ObservableCollection<ProductListItemDetails> BuildServicesList()
		{
			return BuildServicesList(clientEndpoint, cropYearId);
		}
		private static ObservableCollection<ProductListItemDetails> BuildServicesList(IClientEndpoint clientEndpoint, CropYearId cropYearId)
		{
			UserCreatedProductList userCreatedProductList = GetUserCreatedProductList(clientEndpoint);
			List<Guid> usedProductGuids = GetUsedProductGuids(clientEndpoint, cropYearId);
			IEnumerable<ProductListItemDetails> masterlistMiniProducts =
				GetServicesFromMasterList(GlobalIdentifiers.GetMasterlistDataSourceId(), clientEndpoint.GetMasterlistService().GetProductList(), usedProductGuids);
			IEnumerable<ProductListItemDetails> userCreatedMiniProducts =
				GetUserCreatedServices(userCreatedProductList, usedProductGuids);

			return BuildProductList(masterlistMiniProducts, userCreatedMiniProducts);
		}
		
		private static IEnumerable<ProductListItemDetails> GetServicesFromMasterList(Guid masterListDataSourceId, IEnumerable<MiniProduct> miniProducts, ICollection<Guid> productGuids)
		{
			return from miniProduct in miniProducts
				where miniProduct.ProductType.Equals("Service")
				let cont = productGuids.Contains(miniProduct.Id)
				select new ProductListItemDetails(miniProduct, masterListDataSourceId, cont ? "Used this year" : "Unused", cont ? (short)0 : (short)1);
		}

		private static IEnumerable<ProductListItemDetails> GetUserCreatedServices(UserCreatedProductList userCreatedProductList, ICollection<Guid> productGuids)
		{
			return from webMiniProduct in userCreatedProductList.UserCreatedProducts
				where webMiniProduct.ProductType.Equals("Service")
				let cont = productGuids.Contains(webMiniProduct.Id)
				select new ProductListItemDetails(webMiniProduct, ApplicationEnvironment.CurrentDataSourceId, cont ? "Used this year" : "Unused", cont ? (short)0 : (short)1);
		}
	}
}