﻿using System;
using System.Collections.Generic;
using System.Linq;
using GalaSoft.MvvmLight;
using Landdb.Resources;
using Landdb.ViewModel.Resources.Popups.Equipment.Tabs;
using Optional;
using Optional.Unsafe;

namespace Landdb.ViewModel.Resources.Equipment {
	public class EquipmentPageFilterViewModel : ViewModelBase, IPageFilterViewModel
	{
		/*
		* If you don't call ToList it will make a shallow copy. Then it will edit the list in the EquipmentTypeViewModel class. 
		* We don't want to do that.
		*/
		public static List<StatusInfoViewModel> AllStatusInfoViewModelList { get; } = StatusInfoViewModel.AllStatusInfoViewModels.ToList();
		public static List<EquipmentTypeViewModel> AllEquipmentTypeList { get; } = EquipmentTypeViewModel.EquipmentTypeList.ToList();
		private static StatusInfoViewModel onlyActiveStatuses;
		private static StatusInfoViewModel onlyInactiveStatuses;
		private static StatusInfoViewModel allStatuses;
		private static EquipmentTypeViewModel allTypes;

		public EquipmentPageFilterViewModel()
		{
			onlyActiveStatuses = new StatusInfoViewModel(new EquipmentStatus(Guid.NewGuid(), EquipmentStatusType.OWNED, DateTime.MinValue, true), Strings.StatusInfoViewModel_Equipment_OnlyActiveStatuses_Text);
			onlyInactiveStatuses = new StatusInfoViewModel(new EquipmentStatus(Guid.NewGuid(), EquipmentStatusType.OWNED, DateTime.MaxValue, false), Strings.StatusInfoViewModel_Equipment_OnlyInactiveStatuses_Text);
			allStatuses = new StatusInfoViewModel(new EquipmentStatus(Guid.NewGuid(), EquipmentStatusType.OWNED, DateTime.MinValue, true), Strings.StatusInfoViewModel_Equipment_AllStatuses_Text);
			AllStatusInfoViewModelList.Insert(0, onlyActiveStatuses);
			AllStatusInfoViewModelList.Insert(1, onlyInactiveStatuses);
			AllStatusInfoViewModelList.Insert(2, allStatuses);
			allTypes = new EquipmentTypeViewModel(EquipmentType.EARTH_MOVING, Strings.EquipmentType_AllTypes_Text);
			AllEquipmentTypeList.Insert(0, allTypes);
		}

		private string filterNameText;

		public string FilterNameText
		{
			get => filterNameText;
			set
			{
				filterNameText = value;
				RaisePropertyChanged(() => FilterNameText);
			}
		}

		private Option<StatusInfoViewModel> filterStatusInfoViewModel;

		public StatusInfoViewModel FilterStatusInfoViewModel
		{
			get => filterStatusInfoViewModel.HasValue ? filterStatusInfoViewModel.ValueOrFailure() : AllStatusInfoViewModelList.FirstOrDefault();
			set
			{
				filterStatusInfoViewModel = value.SomeNotNull();
				RaisePropertyChanged(() => FilterStatusInfoViewModel);
			} 
		}

		private Option<EquipmentTypeViewModel> filterEquipmentType;

		public EquipmentTypeViewModel FilterEquipmentType
		{
			get => filterEquipmentType.ValueOr(allTypes);
			set
			{
				filterEquipmentType = value.SomeNotNull();
				RaisePropertyChanged(() => FilterEquipmentType);
			}
		}

		public bool FilterItem(object item)
		{
			if (item is EquipmentListItemViewModel equipmentListItemViewModel)
			{
				string nameToFilterThrough = $"{equipmentListItemViewModel.BasicInfoViewModel.Name} {equipmentListItemViewModel.BasicInfoViewModel.YearMakeModelString}";
				bool filterByNameResult = FilterByName(nameToFilterThrough, filterNameText);
				bool filterByStatusResult = FilterByStatus(equipmentListItemViewModel.SelectedStatusInfoViewModel, FilterStatusInfoViewModel);
				bool filterByTypeResult = FilterByType(equipmentListItemViewModel.BasicInfoViewModel.SelectedEquipmentType, FilterEquipmentType );

				return filterByNameResult && filterByStatusResult && filterByTypeResult;
			}

			//False means the item is not displayed (filtered out).
			return false;
		}

		private static bool FilterByName(string nameToFilterThrough,  string nameToFilterWith)
		{
			return string.IsNullOrWhiteSpace(nameToFilterWith) || nameToFilterThrough.ToLower().Contains(nameToFilterWith.ToLower());
		}

		private static bool FilterByStatus(StatusInfoViewModel listItemStatusInfoViewModel,
			StatusInfoViewModel filterStatusInfoViewModel)
		{
			if( listItemStatusInfoViewModel != null && filterStatusInfoViewModel != null) {
				if (filterStatusInfoViewModel.Equals(allStatuses))
				{
					return true;
				}
				else if (filterStatusInfoViewModel.Equals(onlyActiveStatuses))
				{
					return listItemStatusInfoViewModel.IsActive;
				}
				else if (filterStatusInfoViewModel.Equals(onlyInactiveStatuses))
				{
					return !listItemStatusInfoViewModel.IsActive;
				}
				else
				{
					return filterStatusInfoViewModel.EquipmentStatusType == listItemStatusInfoViewModel.EquipmentStatusType;
				}
			}
			return false;			
		}

		private static bool FilterByType(EquipmentTypeViewModel equipmentTypeViewModel, EquipmentTypeViewModel equipmentTypeFromFilter)
		{
			return equipmentTypeFromFilter.Equals(allTypes) || equipmentTypeFromFilter.Equals(equipmentTypeViewModel);
		}

		public void ClearFilter()
		{
			FilterNameText = string.Empty;
			FilterStatusInfoViewModel = AllStatusInfoViewModelList.FirstOrDefault();
			FilterEquipmentType = AllEquipmentTypeList.FirstOrDefault();
		}

		public void BeforeFilter()
		{
			// do nothing
		}

		public void RefreshLists() { }
	}
}
