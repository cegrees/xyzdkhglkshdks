﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Landdb.ViewModel.Resources.Equipment
{
	public class StatusInfoSelectorItem : IEquatable<StatusInfoSelectorItem>
	{
		private static readonly List<StatusInfoSelectorItem> ActiveStatusInfoSelectorItems = new List<StatusInfoSelectorItem>
		{
			CreateStatusInfoSelectorItem(EquipmentStatusType.OWNED, true, StatusInfoFactory.OWNED),
			CreateStatusInfoSelectorItem(EquipmentStatusType.CONTRACTED, true, StatusInfoFactory.CONTRACTED),
			CreateStatusInfoSelectorItem(EquipmentStatusType.LEASED, true, StatusInfoFactory.LEASED),
		};
		private static readonly List<StatusInfoSelectorItem> InactiveStatusInfoSelectorItems = new List<StatusInfoSelectorItem>
		{
			CreateStatusInfoSelectorItem(EquipmentStatusType.SOLD, false, StatusInfoFactory.SOLD),
			CreateStatusInfoSelectorItem(EquipmentStatusType.JUNKED, false, StatusInfoFactory.JUNKED),
			CreateStatusInfoSelectorItem(EquipmentStatusType.CONTRACT_TERMINATED, false, StatusInfoFactory.CONTRACT_TERMINATED),
			CreateStatusInfoSelectorItem(EquipmentStatusType.LEASE_TERMINATED, false, StatusInfoFactory.LEASE_TERMINATED)
		};
		private static readonly List<StatusInfoSelectorItem> AllStatusInfoSelectorItems =
			ActiveStatusInfoSelectorItems.Concat(InactiveStatusInfoSelectorItems).ToList();
		private readonly EquipmentStatusType equipmentStatusType;
		private readonly bool isActive;
		private readonly string equipmentStatusDisplayText;
		
		private StatusInfoSelectorItem(EquipmentStatusType equipmentStatusType, bool isActive ,string equipmentStatusDisplayText)
		{
			this.equipmentStatusType = equipmentStatusType;
			this.isActive = isActive;
			this.equipmentStatusDisplayText = equipmentStatusDisplayText;
		}

		public static StatusInfoSelectorItem CreateStatusInfoSelectorItem(EquipmentStatusType equipmentStatusType, bool isActive, 
			string equipmentStatusDisplayText)
		{
			return new StatusInfoSelectorItem(equipmentStatusType, isActive, equipmentStatusDisplayText);
		}

		public EquipmentStatusType EquipmentStatusType => equipmentStatusType;
		public bool IsActive => isActive;
		public string EquipmentStatusDisplayText => equipmentStatusDisplayText;

		public static List<StatusInfoSelectorItem> GetActiveStatusInfoSelectorItems => ActiveStatusInfoSelectorItems.ToList();
		public static List<StatusInfoSelectorItem> GetInactiveStatusInfoSelectorItems => InactiveStatusInfoSelectorItems.ToList();
		public static List<StatusInfoSelectorItem> GetAllStatusInfoSelectorItems => AllStatusInfoSelectorItems.ToList();

		public bool Equals(StatusInfoSelectorItem other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return equipmentStatusType == other.equipmentStatusType && 
				isActive == other.isActive && 
				string.Equals(equipmentStatusDisplayText, other.equipmentStatusDisplayText);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			return obj.GetType() == GetType() && Equals((StatusInfoSelectorItem) obj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				int hashCode = (int) equipmentStatusType;
				hashCode = (hashCode * 397) ^ isActive.GetHashCode();
				hashCode = (hashCode * 397) ^ (equipmentStatusDisplayText != null ? equipmentStatusDisplayText.GetHashCode() : 0);
				return hashCode;
			}
		}
	}
}