﻿using System;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Secondary.UserCreatedProduct;
using Landdb.Views.Secondary.UserCreatedProduct;

namespace Landdb.ViewModel.Resources.Equipment
{
	public class UserProductFactory
	{
		private readonly IClientEndpoint clientEndpoint;
		private readonly string productSearchText;
		private readonly Action<MiniProduct> onProductCreated;
		private readonly string productType;

		private UserProductFactory(IClientEndpoint clientEndpoint, string productSearchText,
			Action<MiniProduct> onProductCreated, string productType)
		{
			this.clientEndpoint = clientEndpoint;
			this.productSearchText = productSearchText;
			this.onProductCreated = onProductCreated;
			this.productType = productType;
		}

		public static UserProductFactory CreateUserProductFactory(IClientEndpoint clientEndpoint,
			string productSearchText,
			Action<MiniProduct> onProductCreated)
		{
			return new UserProductFactory(clientEndpoint, productSearchText, onProductCreated,
				UserProductCreatorViewModel.FertilizerType);
		}

		public static UserProductFactory CreateUserProductFactory(IClientEndpoint clientEndpoint, string productSearchText,
			Action<MiniProduct> onProductCreated, string productType)
		{
			return new UserProductFactory(clientEndpoint, productSearchText, onProductCreated, productType);
		}

		public void CreateUserProduct()
		{
			CreateUserProduct(clientEndpoint, productSearchText, onProductCreated, productType);
		}

		private static void CreateUserProduct(IClientEndpoint clientEndpoint, string productSearchText,
			Action<MiniProduct> onProductCreated, string productType)
		{
			UserProductCreatorViewModel userProductCreatorViewModel =
				new UserProductCreatorViewModel(clientEndpoint, productSearchText, onProductCreated)
				{
					SelectedProductType = productType
				};
			DisplayUserProductCreatorView(userProductCreatorViewModel);
		}

		private static void DisplayUserProductCreatorView(UserProductCreatorViewModel userProductCreatorViewModel)
		{
			Messenger.Default.Send(new ShowOverlayMessage
			{
				ScreenDescriptor = new ScreenDescriptor(typeof(UserProductCreatorView), "createProduct", userProductCreatorViewModel)
			});
		}
	}
}