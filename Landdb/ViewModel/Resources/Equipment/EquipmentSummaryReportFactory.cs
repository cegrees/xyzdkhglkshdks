﻿using System;
using Landdb.ReportModels.Equipment;
using Landdb.Views.Resources.Equipment;
using Microsoft.Win32;
using Telerik.Reporting;
using Telerik.Reporting.Processing;

namespace Landdb.ViewModel.Resources.Equipment
{
	public class EquipmentSummaryReportFactory : ReportFactory<EquipmentSummaryData>
	{
        private EquipmentSummaryReportFactory(EquipmentSummaryData equipmentSummaryData, string displayName) : 
            base(equipmentSummaryData, displayName)
		{

		}

		public static EquipmentSummaryReportFactory CreateReportSourceFactory(EquipmentSummaryData equipmentSummaryData, 
            string displayName)
		{
		    return new EquipmentSummaryReportFactory(equipmentSummaryData, displayName);
		}

	    protected override ReportSource BuildReportSource(EquipmentSummaryData equipmentSummaryData)
		{
			EquipmentSummaryReport reportView = new EquipmentSummaryReport
			{
				DataSource = equipmentSummaryData,
				Name = "Equipment"
			};

			ReportBook reportBook = new ReportBook();
			reportBook.Reports.Add(reportView);

			return new InstanceReportSource {ReportDocument = reportBook};
		}

	    public override void Export()
	    {
	        RenderingResult result = new ReportProcessor().RenderReport("PDF", BuildReportSource(), new System.Collections.Hashtable());

	        SaveFileDialog saveFileDialog1 = new SaveFileDialog
	        {
	            Filter = "Adobe Reader|*.pdf",
	            FilterIndex = 2,
	            RestoreDirectory = true
	        };

	        // Process save file dialog box results 
	        if (saveFileDialog1.ShowDialog() == true)
	        {
	            // Save document 
	            try
	            {
	                using (System.IO.FileStream fs = new System.IO.FileStream(saveFileDialog1.FileName, System.IO.FileMode.Create))
	                {
	                    fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
	                }
	            }
	            catch (Exception)
	            {
	                // ignored
	            }
	        }
        }
	}
}