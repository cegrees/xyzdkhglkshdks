﻿using Telerik.Reporting;

namespace Landdb.ViewModel.Resources.Equipment
{
    public abstract class ReportFactory<ReportData>
    {
        protected readonly ReportData reportData;
        public string DisplayName { get; }

        protected ReportFactory(ReportData reportData, string displayName)
        {
            this.reportData = reportData;
            DisplayName = displayName;
        }

        public ReportSource BuildReportSource()
        {
            return BuildReportSource(reportData);
        }

        protected abstract ReportSource BuildReportSource(ReportData reportData);

        public abstract void Export();
    }
}