﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure.Messages;
using Landdb.ReportModels.Equipment;
using Microsoft.Win32;
using Telerik.Reporting;
using Telerik.Reporting.Processing;

namespace Landdb.ViewModel.Resources.Equipment
{
	public class EquipmentSummaryReportViewModel : ViewModelBase
	{
		public RelayCommand PDFExport { get; }
		public RelayCommand HideReport { get; }
	    public ObservableCollection<ReportFactory<EquipmentSummaryData>> ReportGenerators { get; }
	    private ReportFactory<EquipmentSummaryData> selectedReportGenerator;

		public EquipmentSummaryReportViewModel(EquipmentSummaryData equipmentSummaryData)
		{
			PDFExport = new RelayCommand(ExportPDF);
			HideReport = new RelayCommand(CloseReport);
            ReportGenerators = new ObservableCollection<ReportFactory<EquipmentSummaryData>>
            {
                EquipmentSummaryReportFactory.CreateReportSourceFactory(equipmentSummaryData, "Equipment Summary Report")
            };
		    selectedReportGenerator = ReportGenerators.FirstOrDefault();
		    if (selectedReportGenerator != null) ReportSource = selectedReportGenerator.BuildReportSource();
		}

		public ReportSource ReportSource { get; set; }

	    public ReportFactory<EquipmentSummaryData> SelectedReportGenerator
	    {
	        get => selectedReportGenerator;
	        set
	        {
	            selectedReportGenerator = value;
	            ReportSource = value.BuildReportSource();
	            RaisePropertyChanged(() => ReportSource);
	        }
	    }

		public void ExportPDF()
		{
	        selectedReportGenerator.Export();
		}

		public void CloseReport()
		{
			Messenger.Default.Send(new HideOverlayMessage());
		}
	}
}
