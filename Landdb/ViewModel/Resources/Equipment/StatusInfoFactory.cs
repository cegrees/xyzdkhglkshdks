﻿using System;
using Landdb.Resources;
using Landdb.ViewModel.Resources.Popups.Equipment.Tabs;

namespace Landdb.ViewModel.Resources.Equipment
{
    public class StatusInfoFactory
    {
        public static string OWNED = Strings.EquipmentStatusType_Owned;
        public static string CONTRACTED = Strings.EquipmentStatusType_Contracted;
        public static string LEASED = Strings.EquipmentStatusType_Leased;
        public static string SOLD = Strings.EquipmentStatusType_Sold;
        public static string CONTRACT_TERMINATED = Strings.EquipmentStatusType_ContractTerminated;
        public static string LEASE_TERMINATED = Strings.EquipmentStatusType_LeaseTerminated;
        public static string JUNKED = Strings.EquipmentStatusType_Junked;

        public static StatusInfoViewModel BuildDefaultStatusInfoViewModel()
        {
            return BuildStatusInfoViewModel(EquipmentStatus.GetDefaultEquipmentStatus());
        }

        public static StatusInfoViewModel BuildStatusInfoViewModel(EquipmentStatus equipmentStatus)
        {
            return new StatusInfoViewModel(equipmentStatus, PickEquipmentStatusDisplayText(equipmentStatus.EquipmentStatusType));
        }

        private static string PickEquipmentStatusDisplayText(EquipmentStatusType equipmentStatusType)
        {
            switch (equipmentStatusType)
            {
                case EquipmentStatusType.OWNED:
                    return OWNED;
                case EquipmentStatusType.CONTRACTED:
                    return CONTRACTED;
                case EquipmentStatusType.LEASED:
                    return LEASED;
                case EquipmentStatusType.LEASE_TERMINATED:
                    return LEASE_TERMINATED;
                case EquipmentStatusType.SOLD:
                    return SOLD;
                case EquipmentStatusType.CONTRACT_TERMINATED:
                    return CONTRACT_TERMINATED;
                case EquipmentStatusType.JUNKED:
                    return JUNKED;
                default:
                    throw new ArgumentOutOfRangeException(nameof(equipmentStatusType), equipmentStatusType, null);
            }
        }

        public static StatusInfoWithRemoveCommandViewModel BuildStatusInfoWithRemoveCommand(EquipmentStatus equipmentStatus, 
            Action<StatusInfoWithRemoveCommandViewModel> onRemove, Func<bool> isEnabled,
            Func<StatusInfoWithRemoveCommandViewModel, bool> isCurrent)
        {
            return new StatusInfoWithRemoveCommandViewModel(equipmentStatus, PickEquipmentStatusDisplayText(equipmentStatus.EquipmentStatusType), onRemove, isEnabled, isCurrent);
        }
    }
}