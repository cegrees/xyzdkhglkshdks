﻿using System;
using System.Collections.Generic;
using System.Linq;
using GalaSoft.MvvmLight;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Equipment;
using Landdb.ViewModel.Resources.Popups.Equipment.Tabs;
using Lokad.Cqrs;

namespace Landdb.ViewModel.Resources.Equipment {
	public class EquipmentListItemViewModel : ViewModelBase, IEquatable<EquipmentListItemViewModel>
	{
		private readonly IClientEndpoint clientEndpoint;
		private readonly int cropYear;
		public EquipmentId EquipmentId { get; }
		public BasicInfoViewModel BasicInfoViewModel { get; private set; }
		public int AssociatedLoadCount { get; set; }

		public EquipmentListItemViewModel(IClientEndpoint clientEndpoint, int cropYear, EquipmentListItem listItem)
		{
			this.clientEndpoint = clientEndpoint;
			this.cropYear = cropYear;
			EquipmentId = listItem.Id;
			BasicInfoViewModel = new BasicInfoViewModel(listItem.BasicInfo , listItem.StatusList.ToList() ,string.Empty);
		}

		public StatusInfoViewModel SelectedStatusInfoViewModel
		{
			get => BasicInfoViewModel.GetCurrentStatusInfoViewModel;
			set
			{
				List<EquipmentStatus> equipmentStatuses = clientEndpoint.GetView<EquipmentDetailsView>(EquipmentId).Value.EquipmentStatusList;
				equipmentStatuses.Add(value.GetEquipmentStatus());
				clientEndpoint.SendOne(new ChangeEquipmentStatusList(EquipmentId, clientEndpoint.GenerateNewMetadata(), cropYear, equipmentStatuses.ToArray()));
				BasicInfoViewModel = new BasicInfoViewModel(BasicInfoViewModel.GetEquipmentBasicInfo(), equipmentStatuses,
					BasicInfoViewModel.Notes);
				RaisePropertyChanged(() => SelectedStatusInfoViewModel);
				RaisePropertyChanged(() => BasicInfoViewModel);
			}
		}

        public object Id { get; internal set; }

        public static List<EquipmentListItemViewModel> GetListItemsForCropYear(IClientEndpoint clientEndpoint, CropYearId cropYearId)
		{
			DataSourceId dataSourceId = new DataSourceId(cropYearId.DataSourceId);

			Maybe<EquipmentListView> possibleEquipmentListView = clientEndpoint.GetView<EquipmentListView>(dataSourceId);

			if (possibleEquipmentListView.HasValue)
			{
				EquipmentListView equipmentListView = possibleEquipmentListView.Value;

				IEnumerable<EquipmentListItemViewModel> sortedFilteredEquipmentList =
					from equipmentListItem in equipmentListView.EquipmentList
					where equipmentListItem.IsActiveInCropYear(cropYearId.Id)
					orderby equipmentListItem.BasicInfo.Name
					select new EquipmentListItemViewModel(clientEndpoint, cropYearId.Id, equipmentListItem);

				return sortedFilteredEquipmentList.ToList();
			}
			else
			{
				return new List<EquipmentListItemViewModel>();
			}
		}

		public static List<EquipmentListItemViewModel> GetActiveListItemsForCropYearSortByType(IClientEndpoint clientEndpoint, CropYearId cropYearId)
		{
			return SortEquipmentListItemsByType(GetListItemsForCropYear(clientEndpoint, cropYearId).FindAll(model => model.BasicInfoViewModel.CurrentStatusInfo.IsActive));
		}

		private static List<EquipmentListItemViewModel> SortEquipmentListItemsByType(List<EquipmentListItemViewModel> allActiveEquipmentListItemViewModels)
		{
			List<EquipmentListItemViewModel> typeFilteredEquipmentListItemViewModels = new List<EquipmentListItemViewModel>();

			FilterEquipmentByType(allActiveEquipmentListItemViewModels, EquipmentType.TRUCK)
				.ForEach(model => typeFilteredEquipmentListItemViewModels.Add(model));
			FilterEquipmentByType(allActiveEquipmentListItemViewModels, EquipmentType.TRAILER)
				.ForEach(model => typeFilteredEquipmentListItemViewModels.Add(model));
			FilterEquipmentByType(allActiveEquipmentListItemViewModels, EquipmentType.MISCELLANEOUS)
				.ForEach(model => typeFilteredEquipmentListItemViewModels.Add(model));
			FilterEquipmentByType(allActiveEquipmentListItemViewModels, EquipmentType.EARTH_MOVING)
				.ForEach(model => typeFilteredEquipmentListItemViewModels.Add(model));
			FilterEquipmentByType(allActiveEquipmentListItemViewModels, EquipmentType.FERTILIZER)
				.ForEach(model => typeFilteredEquipmentListItemViewModels.Add(model));
			FilterEquipmentByType(allActiveEquipmentListItemViewModels, EquipmentType.FORAGE_AND_HAY)
				.ForEach(model => typeFilteredEquipmentListItemViewModels.Add(model));
			FilterEquipmentByType(allActiveEquipmentListItemViewModels, EquipmentType.HARVESTING)
				.ForEach(model => typeFilteredEquipmentListItemViewModels.Add(model));
			FilterEquipmentByType(allActiveEquipmentListItemViewModels, EquipmentType.IRRIGATION)
				.ForEach(model => typeFilteredEquipmentListItemViewModels.Add(model));
			FilterEquipmentByType(allActiveEquipmentListItemViewModels, EquipmentType.MANURE)
				.ForEach(model => typeFilteredEquipmentListItemViewModels.Add(model));
			FilterEquipmentByType(allActiveEquipmentListItemViewModels, EquipmentType.PLANTER)
				.ForEach(model => typeFilteredEquipmentListItemViewModels.Add(model));
			FilterEquipmentByType(allActiveEquipmentListItemViewModels, EquipmentType.SHOP)
				.ForEach(model => typeFilteredEquipmentListItemViewModels.Add(model));
			FilterEquipmentByType(allActiveEquipmentListItemViewModels, EquipmentType.SPRAYER)
				.ForEach(model => typeFilteredEquipmentListItemViewModels.Add(model));
			FilterEquipmentByType(allActiveEquipmentListItemViewModels, EquipmentType.TILLAGE)
				.ForEach(model => typeFilteredEquipmentListItemViewModels.Add(model));
			FilterEquipmentByType(allActiveEquipmentListItemViewModels, EquipmentType.TRACTOR)
				.ForEach(model => typeFilteredEquipmentListItemViewModels.Add(model));
			FilterEquipmentByType(allActiveEquipmentListItemViewModels, EquipmentType.VEHICLE)
				.ForEach(model => typeFilteredEquipmentListItemViewModels.Add(model));

			return typeFilteredEquipmentListItemViewModels;
		}

		private static List<EquipmentListItemViewModel> FilterEquipmentByType(List<EquipmentListItemViewModel> equipmentListToBeFiltered, EquipmentType equipmentTypeToFilterBy)
		{
			return equipmentListToBeFiltered.FindAll(model =>
				model.BasicInfoViewModel.SelectedEquipmentType.GetEquipmentType == equipmentTypeToFilterBy);
		}

		public bool Equals(EquipmentListItemViewModel other) {
			if (ReferenceEquals(null, other)) { return false; }
			if (ReferenceEquals(this, other)) { return true; }

			return EquipmentId == other.EquipmentId;
		}

		public override string ToString() => BasicInfoViewModel.Name;
	}
}