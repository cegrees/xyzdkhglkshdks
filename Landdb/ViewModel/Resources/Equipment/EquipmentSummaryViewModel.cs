﻿using System;
using System.Collections.Generic;
using System.Data;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Equipment;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Resources.Popups.Equipment;
using Landdb.ViewModel.Resources.Popups.Equipment.Tabs;
using Landdb.Views.Resources.Popups.Equipment;
using System.Linq;
using System.Windows.Input;
using ExportToExcel;
using Landdb.ReportModels.Equipment;
using Landdb.Views.Reports;
using Lokad.Cqrs;
using Microsoft.Win32;

namespace Landdb.ViewModel.Resources.Equipment {
	public class EquipmentSummaryViewModel : ValidationViewModelBase {

		private readonly IClientEndpoint clientEndpoint;
		public readonly CropYearId currentCropYearId;
		private readonly Action<EquipmentSummaryViewModel> editEquipmentListItemViewModel;
		public EquipmentId EquipmentId { get; }
		public BasicInfoViewModel BasicInfoViewModel { get; private set; }
		public HistoryInfoViewModel HistoryInfoViewModel { get; }
		public ServicesInfoCollectionViewModel ServicesInfoCollectionViewModel { get; }
		public StatusInfoCollectionViewModel StatusInfoCollectionViewModel { get; private set; }
		public ICommand DisplayEditBasicInfoView { get; }
		public ICommand DisplayEditHistoryInfoView { get; }
		public ICommand DisplayEditServicesInfoView { get; }
		public ICommand DisplayEditStatusInfoCollectionView { get; }
		public ICommand UpdateNotes { get; }
		public ICommand PrintCommand { get; }
	    public ICommand ExportAllEquipmentCommand { get; }

		public EquipmentSummaryViewModel(IClientEndpoint clientEndpoint, CropYearId currentCropYearId, 
			EquipmentId equipmentId, EquipmentDetailsView detailsView, 
			Action<EquipmentSummaryViewModel> editEquipmentListItemViewModel)
		{
			this.clientEndpoint = clientEndpoint;
			this.currentCropYearId = currentCropYearId;
			EquipmentId = equipmentId;
			DisplayEditBasicInfoView = new RelayCommand(OnEditBasicInfo);
			DisplayEditHistoryInfoView = new RelayCommand(OnEditHistoryInfo);
			DisplayEditServicesInfoView = new RelayCommand(OnEditServicesInfo);
			DisplayEditStatusInfoCollectionView = new RelayCommand(OnEditStatusInfo);
			PrintCommand = new RelayCommand(DisplayEquipmentPrintView);
			UpdateNotes = new RelayCommand(OnNotesUpdate);
		    BasicInfoViewModel = new BasicInfoViewModel(detailsView.BasicInfo, detailsView.EquipmentStatusList, detailsView.Notes);
		    HistoryInfoViewModel = new HistoryInfoViewModel(detailsView.HistoryInfo);
		    ServicesInfoCollectionViewModel = new ServicesInfoCollectionViewModel(clientEndpoint, currentCropYearId, detailsView.EquipmentServices);
		    StatusInfoCollectionViewModel = new StatusInfoCollectionViewModel(detailsView.EquipmentStatusList);
            this.editEquipmentListItemViewModel = editEquipmentListItemViewModel;
            ExportAllEquipmentCommand = new RelayCommand(ExportAllEquipment);
		}

		private void OnEditBasicInfo() {
			OnEditBasicInfo(clientEndpoint, BasicInfoViewModel);
		}

		private void OnEditBasicInfo(IClientEndpoint clientEndpoint, BasicInfoViewModel basicInfoViewModel) {
			displayEditBasicInfoView(CreateEditBasicInfoViewModel(clientEndpoint, basicInfoViewModel));
		}

		private void displayEditBasicInfoView(EditBasicInfoViewModel editBasicInfoViewModel) {
			Messenger.Default.Send(new ShowOverlayMessage {
				ScreenDescriptor = new ScreenDescriptor(typeof(EditBasicInfoView), "edit details", editBasicInfoViewModel)
			});
		}

		private EditBasicInfoViewModel CreateEditBasicInfoViewModel(IClientEndpoint clientEndpoint, BasicInfoViewModel basicInfoViewModel)
		{
			return new EditBasicInfoViewModel(basicInfoViewModel.GetEquipmentBasicInfo(), basicInfoViewModel.GetEquipmentStatuses(),
				basicInfoViewModel.Notes, editedDetailsTabView => {
					if (editedDetailsTabView != null && editedDetailsTabView.BasicInfoViewModel.ValidateViewModel())
					{
						BasicInfoViewModel = new BasicInfoViewModel(editedDetailsTabView.BasicInfoViewModel.GetEquipmentBasicInfo(),
							editedDetailsTabView.BasicInfoViewModel.GetEquipmentStatuses(), editedDetailsTabView.BasicInfoViewModel.Notes);
						StatusInfoCollectionViewModel = new StatusInfoCollectionViewModel(editedDetailsTabView.BasicInfoViewModel.GetEquipmentStatuses());

						editEquipmentListItemViewModel(this);

						clientEndpoint.SendOne(new ChangeEquipmentBasicInfo(
							EquipmentId,
							clientEndpoint.GenerateNewMetadata(),
							currentCropYearId.Id,
							BasicInfoViewModel.GetEquipmentBasicInfo()
						));

						clientEndpoint.SendOne(new ChangeEquipmentStatusList(
							EquipmentId,
							clientEndpoint.GenerateNewMetadata(),
							currentCropYearId.Id,
							BasicInfoViewModel.GetEquipmentStatuses().ToArray()
						));

						RaisePropertyChanged(() => BasicInfoViewModel);
						RaisePropertyChanged(() => StatusInfoCollectionViewModel);
					}
				}
			);
		}

		private void OnEditHistoryInfo() {
			OnEditHistoryInfo(HistoryInfoViewModel);
		}

		private void OnEditHistoryInfo(HistoryInfoViewModel historyInfoViewModel) {
			displayEditHistoryInfoView(createEditHistoryInfoViewModel(historyInfoViewModel));
		}

		private void displayEditHistoryInfoView(EditHistoryInfoViewModel editHistoryInfoViewModel) {
			Messenger.Default.Send(new ShowOverlayMessage {
				ScreenDescriptor = new ScreenDescriptor(typeof(EditHistoryInfoView), "edit details", editHistoryInfoViewModel)
			});
		}

		private EditHistoryInfoViewModel createEditHistoryInfoViewModel(HistoryInfoViewModel historyInfoViewModel) {
			return new EditHistoryInfoViewModel(historyInfoViewModel.GetEquipmentHistoryInfo(), editHistoryInfoViewModel => {
				if (editHistoryInfoViewModel != null) {
					HistoryInfoViewModel.ChangeProperties(editHistoryInfoViewModel.HistoryInfoViewModel);
					clientEndpoint.SendOne(new ChangeEquipmentHistoryInfo(EquipmentId, new MessageMetadata(),
						ApplicationEnvironment.CurrentCropYear, editHistoryInfoViewModel.HistoryInfoViewModel.GetEquipmentHistoryInfo()));
				}
			});
		}

		private void OnEditServicesInfo() {
			OnEditServicesInfo(ServicesInfoCollectionViewModel);
		}

		private void OnEditServicesInfo(ServicesInfoCollectionViewModel servicesInfoCollectionViewModel) {
			displayEditServicesInfoView(createEditServicesInfoViewModel(servicesInfoCollectionViewModel));
		}

		private void displayEditServicesInfoView(EditServicesInfoViewModel editServicesInfoViewModel) {
			Messenger.Default.Send(new ShowOverlayMessage {
				ScreenDescriptor = new ScreenDescriptor(typeof(EditServicesInfoView), "edit details", editServicesInfoViewModel)
			});
		}

		private EditServicesInfoViewModel createEditServicesInfoViewModel(ServicesInfoCollectionViewModel servicesInfoCollectionViewModel) {
			return new EditServicesInfoViewModel(clientEndpoint, currentCropYearId, servicesInfoCollectionViewModel.GetEquipmentServiceInfoArray(), editServicesInfoViewModel => {
				if (editServicesInfoViewModel != null) {
					ServicesInfoCollectionViewModel.ChangeProperties(editServicesInfoViewModel.ServicesInfoCollectionViewModel);
					clientEndpoint.SendOne(new ChangeEquipmentServiceInfo(EquipmentId, new MessageMetadata(), ApplicationEnvironment.CurrentCropYear,
						editServicesInfoViewModel.ServicesInfoCollectionViewModel.GetEquipmentServiceInfoArray()));
				}
			});
		}

		private void OnNotesUpdate() {
			displayNotesEditView();
		}

		private void displayNotesEditView() {
			DialogFactory.ShowNotesChangeDialog(BasicInfoViewModel.Notes, newNotes => {
				BasicInfoViewModel.ChangeNotes(newNotes);
				clientEndpoint.SendOne(new UpdateEquipmentNotes(EquipmentId, clientEndpoint.GenerateNewMetadata(), newNotes));
			});
		}

		private void OnEditStatusInfo() {
			OnEditStatusInfo(StatusInfoCollectionViewModel);
		}

		private void OnEditStatusInfo(StatusInfoCollectionViewModel statusInfoCollectionViewModel) {
			displayEditStatusInfoCollectionView(createEditStatusInfoCollectionViewModel(statusInfoCollectionViewModel));
		}

		private void displayEditStatusInfoCollectionView(EditStatusInfoCollectionViewModel editStatusInfoCollectionViewModel) {
			Messenger.Default.Send(new ShowOverlayMessage {
				ScreenDescriptor = new ScreenDescriptor(typeof(EditStatusInfoCollectionView), "edit status", editStatusInfoCollectionViewModel)
			});
		}

		private EditStatusInfoCollectionViewModel createEditStatusInfoCollectionViewModel(StatusInfoCollectionViewModel statusInfoCollectionViewModel) {
			return new EditStatusInfoCollectionViewModel(statusInfoCollectionViewModel.GetEquipmentStatusValueObjectList(), editedStatusInfoCollectionView => {

				StatusInfoCollectionViewModel = new StatusInfoCollectionViewModel(
					editedStatusInfoCollectionView.StatusInfoCollectionViewModel.GetEquipmentStatusValueObjectList());

				BasicInfoViewModel = new BasicInfoViewModel(BasicInfoViewModel.GetEquipmentBasicInfo(), StatusInfoCollectionViewModel.GetEquipmentStatusValueObjectList(), 
					BasicInfoViewModel.Notes);

				editEquipmentListItemViewModel(this);

				clientEndpoint.SendOne(new ChangeEquipmentStatusList(
					EquipmentId,
					clientEndpoint.GenerateNewMetadata(),
					currentCropYearId.Id,
					StatusInfoCollectionViewModel.GetEquipmentStatusValueObjectList().ToArray()
				));

				RaisePropertyChanged(() => BasicInfoViewModel);
				RaisePropertyChanged(() => StatusInfoCollectionViewModel);
			});
		}

		private void DisplayEquipmentPrintView()
		{
			EquipmentSummaryReportViewModel equipmentSummaryReportViewModel = new EquipmentSummaryReportViewModel(new EquipmentSummaryData(this));
			Messenger.Default.Send(new ShowOverlayMessage
			{
				ScreenDescriptor = new ScreenDescriptor(typeof(ReportSelectorWithPreviewView), "Equipment Summary Report", equipmentSummaryReportViewModel)
			});
		}

	    private void ExportAllEquipment()
	    {
            string filename = string.Empty;

	        List<EquipmentListItemViewModel> ListItems = EquipmentListItemViewModel.GetListItemsForCropYear(clientEndpoint, currentCropYearId);
	        List<EquipmentExcelData> resultingEquipmentSummaryData = new List<EquipmentExcelData>();
            ListItems.ForEach(listItem => {
	            Maybe<EquipmentDetailsView> possibleData = clientEndpoint.GetView<EquipmentDetailsView>(listItem.EquipmentId);

	            if (possibleData.HasValue)
	            {
	                EquipmentDetailsView details = possibleData.Value;
	                resultingEquipmentSummaryData.Add(
                        new EquipmentExcelData(
	                        new EquipmentSummaryViewModel(clientEndpoint, currentCropYearId, details.Id, details, editEquipmentListItemViewModel => { })
	                    )
                    );
	            }
            });

	        DataTable listToDataTable = CreateExcelFile.ListToDataTable(resultingEquipmentSummaryData, true);
            listToDataTable.TableName = "Equipment Details";

            DataSet dataSet = new DataSet("excelDS");
            dataSet.Tables.Add(listToDataTable);

            try
            {
                SaveFileDialog saveFileDialog1 = new SaveFileDialog
                {
                    Filter = "Excel|*.xlsx",
                    FilterIndex = 2,
                    RestoreDirectory = true
                };

                bool? resultSaved = saveFileDialog1.ShowDialog();

                if (resultSaved == true)
                {
                    filename = saveFileDialog1.FileName;
                    CreateExcelFile.CreateExcelDocument(dataSet, filename);
                }

                //Open file
                System.Diagnostics.Process.Start(filename);

                Messenger.Default.Send(new HidePopupMessage());
            }
            catch (Exception)
            {
                Messenger.Default.Send(new HidePopupMessage());
            }
        }
	}
}