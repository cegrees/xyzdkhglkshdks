﻿using System;
using Landdb.ReportModels.Equipment;
using Landdb.Views.Resources.Equipment;
using Microsoft.Win32;
using Telerik.Reporting;
using Telerik.Reporting.Processing;

namespace Landdb.ViewModel.Resources.Equipment
{
    public class EquipmentSummaryExcelReportFactory : ReportFactory<EquipmentSummaryData>
    {
        private EquipmentSummaryExcelReportFactory(EquipmentSummaryData reportData, string displayName) 
            : base(reportData, displayName)
        {

        }

        public static EquipmentSummaryExcelReportFactory CreateReportFactory(EquipmentSummaryData equipmentSummaryData, 
            string displayName)
        {
            return new EquipmentSummaryExcelReportFactory(equipmentSummaryData, displayName);
        }

        protected override ReportSource BuildReportSource(EquipmentSummaryData equipmentSummaryData)
        {
            EquipmentSummaryReport reportView = new EquipmentSummaryReport
            {
                DataSource = equipmentSummaryData,
                Name = "Equipment"
            };

            ReportBook reportBook = new ReportBook();
            reportBook.Reports.Add(reportView);

            return new InstanceReportSource { ReportDocument = reportBook };
        }

        public override void Export()
        {
            ReportSource reportExport = BuildReportSource();

            ReportProcessor reportProcessor = new ReportProcessor();
            System.Collections.Hashtable deviceInfo =
                new System.Collections.Hashtable();
            RenderingResult result = reportProcessor.RenderReport("PDF", reportExport, deviceInfo);

            SaveFileDialog saveFileDialog1 = new SaveFileDialog
            {
                Filter = "Adobe Reader|*.pdf",
                FilterIndex = 2,
                RestoreDirectory = true
            };


            bool? resultSaved = saveFileDialog1.ShowDialog();

            // Process save file dialog box results 
            if (resultSaved == true)
            {
                // Save document 
                string filename = saveFileDialog1.FileName;

                try
                {
                    using (System.IO.FileStream fs = new System.IO.FileStream(filename, System.IO.FileMode.Create))
                    {
                        fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
                    }
                }
                catch (Exception)
                {
                    // ignored
                }
            }
        }
    }
}