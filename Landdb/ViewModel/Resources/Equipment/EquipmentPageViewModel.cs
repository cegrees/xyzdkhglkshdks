﻿using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Equipment;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Resources.Popups.Equipment;
using Landdb.Views.Resources.Popups.Equipment;
using Lokad.Cqrs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Resources.Equipment
{
    public class EquipmentPageViewModel : AbstractListPage<EquipmentListItemViewModel, EquipmentSummaryViewModel>
    {
        public ICommand DisplayRemovalOverlay { get; set; }
        private readonly CropYearId cropYearId;
        public static StatusInfoSelectorItem LastUsedStatusInfoSelectorItem { get; set; } = StatusInfoSelectorItem.CreateStatusInfoSelectorItem(
            StatusInfoFactory.BuildDefaultStatusInfoViewModel().EquipmentStatusType,
            StatusInfoFactory.BuildDefaultStatusInfoViewModel().IsActive,
            StatusInfoFactory.BuildDefaultStatusInfoViewModel().EquipmentStatusDisplayText);
        public static EquipmentType LastUsedEquipmentType { get; set; } = EquipmentType.EARTH_MOVING;
        private readonly List<Tuple<IIdentity, Action<EquipmentId, string>>> subscribers;

        public EquipmentPageViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher)
            : base(clientEndpoint, dispatcher)
        {

            Messenger.Default.Register<EquipmentAddedMessage>(this, msg => {
                RefreshItemsList(() => {
                    var q = from li in ListItems
                            where li.EquipmentId == msg.LastEquipmentAdded
                            select li;
                    SelectedListItem = q.FirstOrDefault();
                });
            });

            FilterModel = new EquipmentPageFilterViewModel();
            DisplayRemovalOverlay = new RelayCommand(OnRemoveEquipment);
            cropYearId = new CropYearId(currentDataSourceId.Id, currentCropYear);
            subscribers = new List<Tuple<IIdentity, Action<EquipmentId, string>>>();
        }

		public override string GetEntityName() => Strings.Equipment_Text.ToLower();
		public override string GetPluralEntityName() => Strings.Machines_Text.ToLower();

        public override bool CanDelete(EquipmentListItemViewModel toDelete) => toDelete.AssociatedLoadCount == 0;

        protected override IEnumerable<EquipmentListItemViewModel> GetListItemModels()
        {
            var currentCropYearId = new CropYearId(currentDataSourceId.Id, currentCropYear);
            return EquipmentListItemViewModel.GetListItemsForCropYear(clientEndpoint, currentCropYearId);
        }

        private EquipmentDetailsView equipmentDetailsView;

        protected override async Task<ScreenDescriptor> CreateScreenDescriptorForNewItemCreation()
        {
            EquipmentId equipmentId = new EquipmentId(currentDataSourceId.Id, Guid.NewGuid());
            CropYearId currentCropYearId = new CropYearId(currentDataSourceId.Id, currentCropYear);

            AddEquipmentViewModel addEquipmentViewModel = new AddEquipmentViewModel(clientEndpoint, equipmentId, currentCropYearId,
                (equipmentListItemViewModel, equipmentDetailsView) => {
                    List<EquipmentListItemViewModel> temporaryListItems = ListItems.ToList();
                    temporaryListItems.Add(equipmentListItemViewModel);
                    ListItems.Clear();
                    temporaryListItems = temporaryListItems.OrderBy(listItem => listItem.BasicInfoViewModel.Name).ToList();
                    temporaryListItems.ForEach(listItem =>
                    {
                        ListItems.Add(listItem);
                    });

                    LastUsedStatusInfoSelectorItem = StatusInfoSelectorItem.CreateStatusInfoSelectorItem(
                        equipmentListItemViewModel.BasicInfoViewModel.CurrentStatusInfo.EquipmentStatusType,
                        equipmentListItemViewModel.BasicInfoViewModel.CurrentStatusInfo.IsActive,
                        equipmentListItemViewModel.BasicInfoViewModel.CurrentStatusInfo.EquipmentStatusDisplayText);
                    LastUsedEquipmentType = equipmentListItemViewModel.BasicInfoViewModel.SelectedEquipmentType.GetEquipmentType;
                    this.equipmentDetailsView = new EquipmentDetailsView
                    {
                        Id = equipmentDetailsView.Id,
                        BasicInfo = equipmentDetailsView.BasicInfo,
                        EquipmentServices = equipmentDetailsView.EquipmentServices,
                        EquipmentStatusList = equipmentDetailsView.EquipmentStatusList,
                        HistoryInfo = equipmentDetailsView.HistoryInfo,
                        InitialCropYear = equipmentDetailsView.InitialCropYear,
                        Notes = equipmentDetailsView.Notes
                    };
                    DetailsModel = CreateDetailsModel(equipmentListItemViewModel);
                    SelectedListItem = ListItems.SingleOrDefault(possibleEquipmentListItemViewModel =>
                            possibleEquipmentListItemViewModel.EquipmentId == equipmentListItemViewModel.EquipmentId);
                    ApplyFilter();
                }, LastUsedStatusInfoSelectorItem, LastUsedEquipmentType
            );
            return new ScreenDescriptor(typeof(AddEquipmentView), "add equipment", addEquipmentViewModel);
        }

        protected override EquipmentSummaryViewModel CreateDetailsModel(EquipmentListItemViewModel selectedItem)
        {
            Maybe<EquipmentDetailsView> equipmentMaybe = clientEndpoint.GetView<EquipmentDetailsView>(selectedItem.EquipmentId);

            return equipmentMaybe.HasValue
                ? new EquipmentSummaryViewModel(clientEndpoint, cropYearId, selectedItem.EquipmentId, equipmentMaybe.Value, editEquipmentListItemViewModel)
                : new EquipmentSummaryViewModel(clientEndpoint, cropYearId, equipmentDetailsView.Id, equipmentDetailsView, editEquipmentListItemViewModel);
        }

        private void editEquipmentListItemViewModel(EquipmentSummaryViewModel equipmentSummaryViewModel)
        {
            SelectedListItem.BasicInfoViewModel.ChangeProperties(equipmentSummaryViewModel.BasicInfoViewModel);
            UpdateSubscribers(equipmentSummaryViewModel.EquipmentId, equipmentSummaryViewModel.BasicInfoViewModel.Name);
        }

        protected override bool PerformItemRemoval(EquipmentListItemViewModel selectedItem) {
            var removeCommand = new RemoveEquipmentFromYear(selectedItem.EquipmentId, clientEndpoint.GenerateNewMetadata(), currentCropYear);
            clientEndpoint.SendOne(removeCommand);
            return true;
        }

        private void OnRemoveEquipment()
        {
            displayRemoveEquipmentView(createRemoveEquipmentViewModel());
        }

        private void displayRemoveEquipmentView(RemoveEquipmentViewModel removeEquipmentViewModel)
        {
            Messenger.Default.Send(new ShowOverlayMessage
            {
                ScreenDescriptor = new ScreenDescriptor(typeof(RemoveEquipmentView), "edit details", removeEquipmentViewModel)
            });
        }

        private RemoveEquipmentViewModel createRemoveEquipmentViewModel()
        {
            return new RemoveEquipmentViewModel(new RelayCommand(deleteSelectedListItem), removeOverlayEquipmentStatus => {
                if (removeOverlayEquipmentStatus != null && SelectedListItem != null)
                {
                    SelectedListItem.SelectedStatusInfoViewModel = removeOverlayEquipmentStatus;
                    ApplyFilter();
                }
            });
        }

        private void deleteSelectedListItem()
        {
            RemoveSelectedItem();
            Messenger.Default.Send(new HideOverlayMessage());
        }

		protected override void RemoveSelectedItem() {
			RemoveSelectedItem(string.Format(Strings.SpecialtyDeleteDialog_Text, GetEntityName(), "\n"));
		}

        protected override IDomainCommand CreateSetCharmCommand(EquipmentListItemViewModel selectedItem, string charmName)
        {
            // charms not implemented in equipment currently
            throw new NotImplementedException();
        }

        public void Subscribe(IIdentity subscriberId, Action<EquipmentId, string> subscriberAction)
        {
            subscribers.Add(Tuple.Create(subscriberId, subscriberAction));
        }

        public void Unsubscribe(IIdentity subscriberId)
        {
            subscribers.RemoveAll(subscriber => Equals(subscriber.Item1, subscriberId));
        }

        private void UpdateSubscribers(EquipmentId equipmentId, string updatedEquipmentName)
        {
            subscribers.ForEach(subscriber => subscriber.Item2.Invoke(equipmentId, updatedEquipmentName));
        }
    }
}