﻿using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Contract;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Domain.ReadModels.Yield;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Resources.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb;
using Landdb.Resources;
using Landdb.ViewModel;
using Landdb.ViewModel.Resources;

namespace Landdb.ViewModel.Resources {
	public class CopyContractsPageViewModel : AbstractListPage<ContractListItemViewModel, ContractDetailsBaseViewModel> {
        int selectedcropyear;
        List<int> cropyearlist;
        protected CropYearId currentCropYearId;
        ContractDetailsBaseViewModel selecteddetails;
        ContractId selectedcontractid;

        public CopyContractsPageViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher)
			: base(clientEndpoint, dispatcher) {

            currentCropYearId = new CropYearId(ApplicationEnvironment.CurrentDataSourceId, currentCropYear);

            var years = clientEndpoint.GetView<FieldTreeView>(new DataSourceId(ApplicationEnvironment.CurrentDataSourceId));
            var yearItem = (years.HasValue && years.Value.CropYearList.ContainsKey(currentCropYear)) ? years.Value.CropYearList[currentCropYear] : new TreeViewYearItem();
            cropyearlist = years.Value.CropYearList.Keys.Where(x => x <= currentCropYear).ToList();
            selectedcropyear = 0;
            foreach (int yr in cropyearlist) {
                if (yr > selectedcropyear) {
                    selectedcropyear = yr;
                }
            }

            selecteddetails = null;
            selectedcontractid = null;

            this.FilterModel = new ContractPageFilterViewModel(clientEndpoint);

            CreateNewRentContractCommand = new RelayCommand(onCreateNewRentContract);
			CancelContractCreationCommand = new RelayCommand(onCancelContractCreation);

            if (cropyearlist.Count == 0 || selectedcropyear == 0) {
                CancelContractCreationCommand.Execute(null);
            }
            RaisePropertyChanged(() => CropYearList);
            ChangeCropYearForTree(selectedcropyear);
        }

        public ICommand CreateNewRentContractCommand { get; }
		//public ICommand CreateNewProductionContractCommand { get; }
		public ICommand CancelContractCreationCommand { get; }
        //public ICommand ViewReport { get; }

        public List<int> CropYearList
        {
            get
            {
                if (cropyearlist == null) {
                    return new List<int>();
                }

                return cropyearlist.OrderByDescending(q => q).ToList();
            }
            set
            {
                cropyearlist = value;
                RaisePropertyChanged(() => CropYearList);
            }
        }

        public int SelectedCropYear
        {
            get { return selectedcropyear; }
            set
            {
                if (selectedcropyear == value) { return; }
                selectedcropyear = value;
                RaisePropertyChanged(() => SelectedCropYear);

                if (selectedcropyear == 0) { return; }
                //SaveChangesCommand.RaiseCanExecuteChanged();
                ChangeCropYearForTree(selectedcropyear);
            }
        }

        public void ChangeCropYearForTree(int cropyear) {
            currentCropYearId = new CropYearId(currentCropYearId.DataSourceId, cropyear);
            //ResetData();
            RefreshItemsList(() => {
                var q = from li in ListItems
                        select li;

                SelectedListItem = q.FirstOrDefault();
            });
        }

        public override string GetEntityName() => Strings.Contract_Text.ToLower();
		public override string GetPluralEntityName() => Strings.Contracts_Text.ToLower();

		public override bool CanDelete(ContractListItemViewModel toDelete) {
			var currentCropYearId = new CropYearId(currentDataSourceId.Id, currentCropYear);

			var hasAssociatedRecords = false;

			if (toDelete.Type == Strings.Rent_Text) {
				var cropZoneLoadData = clientEndpoint.GetView<CropZoneLoadDataView>(currentCropYearId).GetValue(() => new CropZoneLoadDataView()).Items;
				var cropZoneAppData = clientEndpoint.GetView<CropZoneApplicationDataView>(currentCropYearId).GetValue(() => new CropZoneApplicationDataView()).Items;

				var rentDetails = clientEndpoint.GetView<RentContractDetailsView>(toDelete.Id).GetValue(new RentContractDetailsView());

				var associatedLoads = new HashSet<LoadId>();
				var associatedApplications = new HashSet<ApplicationId>();

				foreach(var cz in rentDetails.CropZones) {
					var cropZoneLoads = cropZoneLoadData.Where(x => x.CropZoneId == cz.Id);

					foreach(var load in cropZoneLoads) {
						if (!associatedLoads.Contains(load.LoadId)) {
							associatedLoads.Add(load.LoadId);
						}
					}

					var cropZoneApplications = cropZoneAppData.Where(x => x.CropZoneId == cz.Id);
					
					foreach(var app in cropZoneApplications) {
						if (!associatedApplications.Contains(app.ApplicationId)) {
							associatedApplications.Add(app.ApplicationId);
						}
					}
				}

				var loadCount = associatedLoads.Count;
				var appCount = associatedApplications.Count;

				if (loadCount + appCount > 0) {
					RemovalPopupText = String.Format(Strings.ItemMayNotBeRemovedBecauseItIsAssociatedWith_Format_Text, toDelete.Name);

					var recordDescriptions = new List<string>();

					if (appCount > 0) { recordDescriptions.Add("     - " + String.Format(Strings.CountOfApplications_Text, appCount)); }
					if (loadCount > 0) { recordDescriptions.Add("     - " + String.Format(Strings.CountOfLoads_Format_Text, loadCount)); }

					RemovalPopupText += string.Join("\n", recordDescriptions);

					hasAssociatedRecords = true;
				}
			} else if (toDelete.Type == Strings.Production_Text) {
				var loadCount = clientEndpoint.GetView<ProductionContractLoadsView>(currentCropYearId)
					.GetValue(new ProductionContractLoadsView())
					.ProductionContractLoads
					.Values
					.Count(x => x.GrowerProductionContractId == toDelete.Id || x.LandOwnerProductionContractId == toDelete.Id);

				if (loadCount > 0) {
					RemovalPopupText = String.Format(Strings.ItemMayNotBeRemovedBecauseItHasBeenUsedIn_Format_Text, toDelete.Name);
					RemovalPopupText += " - " + String.Format(Strings.CountOfLoads_Format_Text, loadCount);
					hasAssociatedRecords = true;
				}
			} else {
				return false; // go ahead and bail if we encounter some weird contract type
			}

			return !hasAssociatedRecords;
		}

		protected override async Task CreateNewItem() {
			Messenger.Default.Send(new ShowOverlayMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Secondary.Contract.ContractCreationOverlay), "create contract", this)
			});
		}

		protected override IEnumerable<ContractListItemViewModel> GetListItemModels() {
			var cropYearId = new CropYearId(currentDataSourceId.Id, selectedcropyear);

			var rentContractListView = clientEndpoint.GetView<RentContractListView>(cropYearId).GetValue(new RentContractListView());
			var rentContractListItems = rentContractListView.RentContracts.Select(x => new ContractListItemViewModel(x));

			return rentContractListItems.OrderBy(x => x.Name);
		}

		protected override async Task<ScreenDescriptor> CreateScreenDescriptorForNewItemCreation() {
			// we've implemented a custom creation routine for new contracts. if this gets hit, we're doing something wrong.
			throw new NotImplementedException();
		}

		protected override bool PerformItemRemoval(ContractListItemViewModel selectedItem) {
			if (selectedItem == null) { return false; }

            IDomainCommand removeCommand = null;
            if (selectedItem.Type == Strings.Rent_Text) {
				removeCommand =  new RemoveContractFromYear(selectedItem.Id, clientEndpoint.GenerateNewMetadata(), currentCropYear);
			} else if (selectedItem.Type == Strings.Production_Text) {
				removeCommand =  new RemoveProductionContractFromYear(selectedItem.Id, clientEndpoint.GenerateNewMetadata(), currentCropYear);
			}

            if (removeCommand != null) {
                clientEndpoint.SendOne(removeCommand);
                return true;
            } else { return false; }
        }

		protected override IDomainCommand CreateSetCharmCommand(ContractListItemViewModel selectedItem, string charmName) {
			if (selectedItem.CharmName == charmName) { return null; }

			selectedItem.CharmName = charmName;

			IDomainCommand cmd = null;
			if (selectedItem.Type == Strings.Rent_Text) {
				cmd = new FlagRentContract(selectedItem.Id, clientEndpoint.GenerateNewMetadata(), charmName);
			} else if (selectedItem.Type == Strings.Production_Text) {
				cmd = new FlagProductionContract(selectedItem.Id, clientEndpoint.GenerateNewMetadata(), charmName);
			}

			return cmd;
		}

		protected override ContractDetailsBaseViewModel CreateDetailsModel(ContractListItemViewModel selectedItem) {
			ContractDetailsBaseViewModel details = null;
            selecteddetails = null;

            if (selectedItem.Type == Strings.Rent_Text) {
				var rentContractsMaybe = clientEndpoint.GetView<RentContractDetailsView>(selectedItem.Id);
				rentContractsMaybe.IfValue(detailsView => {
					details = new Contract.Rent.RentContractDetailsViewModel(clientEndpoint, dispatcher, currentDataSourceId, currentCropYear, detailsView);
                    details.IsReadOnly = true;
				});
			} else if (selectedItem.Type == Strings.Production_Text) {
				var productionContractsMaybe = clientEndpoint.GetView<ProductionContractDetailsView>(selectedItem.Id);
				productionContractsMaybe.IfValue(detailsView => {
					details = new Contract.Production.ProductionContractDetailsViewModel(clientEndpoint, dispatcher, currentDataSourceId, currentCropYear, detailsView);
                    details.IsReadOnly = true;
                });
			}
            selecteddetails = details;
            selectedcontractid = selectedItem.Id;
			return details;
		}

		private void onCreateNewRentContract() {
			var contractId = new ContractId(currentDataSourceId.Id, Guid.NewGuid());
			var vm = new Secondary.RentContract.AddRentContractViewModel(clientEndpoint, dispatcher, contractId, currentCropYear, selecteddetails, selectedcontractid);

			Messenger.Default.Send(new ChangeScreenDescriptorMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Secondary.RentContract.AddRentContractView), "create rent contract", vm)
			});
		}

		private void onCancelContractCreation() {
            Messenger.Default.Send(new ShowMainViewMessage());
        }
    }
}