﻿using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Product;
using Landdb.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Landdb.ViewModel.Resources {
    public class ActiveIngredientPageViewModel : AbstractListPage<ActiveIngredientListItemModel, ActiveIngredientDetailsViewModel> {
        public ActiveIngredientPageViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher) : base(clientEndpoint, dispatcher) {}

        protected override IEnumerable<ActiveIngredientListItemModel> GetListItemModels() {
            var cropYearId = new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear);
            var productList = clientEndpoint.GetView<ProductUsageView>(cropYearId);
            var ml = clientEndpoint.GetMasterlistService();
            if (ml == null) { return new List<ActiveIngredientListItemModel>(); }

            var aiIdList = new List<Guid>();

            productList.IfValue(pList => {
                foreach (var p in pList.ProductsInUse) {
                    ProductId pId = new ProductId(p.DataSourceId, p.Id);
                    var mlProduct = ml.GetProduct(pId);
                    if (mlProduct != null) {
                        foreach (var ai in mlProduct.ActiveIngredients) {
                            if (Landdb.Domain.ReadModels.Services.ActiveIngredientXRef.Self.IsGoodActiveIngredient(ai.Id)) {
                                var _id = Landdb.Domain.ReadModels.Services.ActiveIngredientXRef.Self.CrossReferenceActiveIngredient(ai.Id);
                                if (!aiIdList.Contains(_id)) {
                                    aiIdList.Add(_id);
                                }
                            }
                            //if (!aiIdList.Contains(ai.Id)) {
                            //    aiIdList.Add(ai.Id);
                            //}
                        }
                    }
                }
            });

            var aiList = new List<ActiveIngredientListItemModel>();
            foreach (var aiId in aiIdList) {
                if (aiId == Guid.Empty) { continue; }
                var ingredientId = new ActiveIngredientId(aiId);
                var aiName = ml.GetActiveIngredientDisplay(ingredientId);
                aiList.Add(new ActiveIngredientListItemModel(ingredientId, aiName ));
            }
            aiList.Sort(new Comparison<ActiveIngredientListItemModel>((x, y) => x.IngredientName.CompareTo(y.IngredientName)));

            return aiList;
        }

        protected override async Task<ScreenDescriptor> CreateScreenDescriptorForNewItemCreation() {
            return null;
        }

        public override bool CanDelete(ActiveIngredientListItemModel toDelete) {
            return false;
        }

        protected override bool PerformItemRemoval(ActiveIngredientListItemModel selectedItem) {
            return false;
        }

        protected override IDomainCommand CreateSetCharmCommand(ActiveIngredientListItemModel selectedItem, string charmName) {
            return null;
        }

        protected override ActiveIngredientDetailsViewModel CreateDetailsModel(ActiveIngredientListItemModel selectedItem) {
            return new ActiveIngredientDetailsViewModel(clientEndpoint, dispatcher, selectedItem.IngredientId);
        }
    }
}
