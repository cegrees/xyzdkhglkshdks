﻿using GalaSoft.MvvmLight;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Resources
{
    public class ContractCZListItemViewModel : ViewModelBase
    {
        IncludedCropZone czArea;

        public ContractCZListItemViewModel(IncludedCropZone cz)
        {
            this.czArea = cz;
        }

        public CropZoneId ID { get { return czArea.Id; } }
        public string Name { get { return czArea.Name; } }
        public double Area { get { return czArea.Area.Value; } }
        public string Unit { get { return czArea.Area.Unit.AbbreviatedDisplay; } }
    }
}
