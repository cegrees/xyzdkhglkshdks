﻿using GalaSoft.MvvmLight;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Resources;

namespace Landdb.ViewModel.Resources {
	public class InventoryListItemModel : ViewModelBase {

		private readonly IClientEndpoint clientEndpoint;
		private readonly IMasterlistService masterlist;

		private readonly InventoryListItem item;
		
		private readonly MiniProduct masterlistProd;

		public InventoryListItemModel(IClientEndpoint clientEndpoint, IMasterlistService masterlist, InventoryListItem item) {
			this.clientEndpoint = clientEndpoint;
			this.masterlist = masterlist;

			this.item = item;

			ProductId = item.ProductId;

			masterlistProd = masterlist.GetProduct(item.ProductId);

			// We can't do a fallback here because of bad data in the product's datasource ID as we were doing previously. Better to let them know that something's 
			// wrong than try to hide it, because we'll only cause more problems for ourselves in the future.
			// In cases where the product was referenced correctly in some records, but incorrectly in others, it leads to the situation where a user-created 
			// product can be duplicated in the list, with one record appearing to be from the masterlist.
			//
			// TODO: We should add list functionality for showing all other record types this product is associated with to the InventoryDetailsView, so that they can find and edit
			// any problem records.
			ProductName = masterlistProd != null ? masterlistProd.Name : $"[{Strings.UnknownProduct_Text.ToLower()}]";
			ManufacturerName = masterlistProd != null ? masterlistProd.Manufacturer : "---";
			InvoiceCount = item.InvoiceCount;
			ApplicationCount = item.ApplicationCount;
			AveragePrice = item.AveragePriceValue;
			AveragePriceUnit = item.AveragePriceUnit;
		}

		public ProductId ProductId { get; }
		public int InvoiceCount { get; }
		public int ApplicationCount { get; }
		public double AveragePrice { get; }
		public string AveragePriceUnit { get; }

		public string ProductType => masterlistProd != null ? masterlistProd.ProductType : string.Empty;
		public double? Density => masterlistProd != null ? masterlistProd.Density : null;
		public bool IsUserCreated => ProductId.DataSourceId != GlobalIdentifiers.GetMasterlistDataSourceId();
		public bool IsFertilizer => masterlistProd != null && masterlistProd.ProductType == GlobalStrings.ProductType_Fertilizer;

		private string _productName;
		public string ProductName {
			get { return _productName; }
			set {
				_productName = value;
				RaisePropertyChanged(() => ProductName);
			}
		}

		private string _manufacturerName;
		public string ManufacturerName {
			get { return _manufacturerName; }
			set {
				_manufacturerName = value;
				RaisePropertyChanged(() => ManufacturerName);
			}
		}

		public override string ToString() => $"[{ProductType}] - {ProductName} ({ManufacturerName})";
	}
}