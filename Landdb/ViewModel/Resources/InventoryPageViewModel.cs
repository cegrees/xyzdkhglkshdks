﻿using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Client.Services.Documents;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Secondary.Reports.Inventory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Resources {
	public class InventoryPageViewModel : AbstractListPage<InventoryListItemModel, InventoryDetailsViewModel> {
		IDocumentService documentService;

		public InventoryPageViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher) : base(clientEndpoint, dispatcher) {
			this.FilterModel = new InventoryPageFilterViewModel();
			documentService = new DocumentServiceEndpoint();

			Messenger.Default.Register<ApplicationsAddedMessage>(this, msg => { RefreshItemsList(); });
			PrintCommand = new RelayCommand(UpdateReport);

			Messenger.Default.Register<UpdateResourcesProductListMessage>(this, OnProductAdded);
		}

		protected override IEnumerable<InventoryListItemModel> GetListItemModels() {
			var productsMaybe = clientEndpoint.GetView<InventoryListView>(new CropYearId(currentDataSourceId.Id, currentCropYear));

			if (productsMaybe.HasValue) {
				var sortedProducts = productsMaybe.Value.Products.Values
					.Where(x => x.ApplicationCount > 0 || x.InvoiceCount > 0 || x.PlanCount > 0 || x.WorkOrderCount > 0 || x.RecommendationCount > 0)
					.Select(x => new InventoryListItemModel(clientEndpoint, clientEndpoint.GetMasterlistService(), x))
					.OrderBy(x => x.ProductName);

				return sortedProducts;
			} else {
				return null;
			}
		}

		public RelayCommand PrintCommand { get; }

		public override string GetEntityName() => Strings.Product_Text.ToLower();
		public override string GetPluralEntityName() => Strings.Products_Text.ToLower();

		protected override async Task<ScreenDescriptor> CreateScreenDescriptorForNewItemCreation() {
			throw new NotSupportedException("add inventory not supported");
		}

		protected override bool PerformItemRemoval(InventoryListItemModel selectedItem) {
			throw new NotSupportedException("remove inventory not supported");
		}

		protected override IDomainCommand CreateSetCharmCommand(InventoryListItemModel selectedItem, string charmName) {
			throw new NotImplementedException("inventory charms not implemented");
		}

		protected override InventoryDetailsViewModel CreateDetailsModel(InventoryListItemModel selectedItem) {
			var invId = new InventoryId(currentDataSourceId.Id, new Tuple<ProductId, int>(selectedItem.ProductId, currentCropYear));

			var readmodel = clientEndpoint.GetView<InventoryDetailView>(invId).GetValue(new InventoryDetailView());
			return new InventoryDetailsViewModel(selectedItem, readmodel, clientEndpoint, documentService, dispatcher);
		}

		void UpdateReport() {
			//get map from ViewModel

			var vm = new SingleInventoryReportViewModel(clientEndpoint, ApplicationEnvironment.CurrentCropYear);
			Messenger.Default.Send(new ShowOverlayMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Secondary.Reports.Work_Order.SingleWorkOrderView), "viewReport", vm)
			});
		}

		public void OnProductAdded(UpdateResourcesProductListMessage message) {
			GetListItemModels();
		}
	}
}
