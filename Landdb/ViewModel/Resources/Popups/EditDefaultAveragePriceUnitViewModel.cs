﻿using AgC.UnitConversion;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Resources.Popups
{
    public class EditDefaultAveragePriceUnitViewModel : ViewModelBase
    {
        IClientEndpoint endpoint;
        ProductId productId;
        InventoryDetailsViewModel parent;

        public EditDefaultAveragePriceUnitViewModel(IClientEndpoint endPoint, ProductId productId, int cropYear, DataSourceId dsId, IUnit initialUnit, InventoryDetailView detailsView)
        {
            this.endpoint = endPoint;
            this.productId = productId;
            this.parent = parent;

            var mlp = endpoint.GetMasterlistService().GetProduct(productId);
            var psu = UnitFactory.GetPackageSafeUnit(mlp.StdPackageUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
            var compatibleUnits = AgC.UnitConversion.CompatibilityFactory.GetCompatibleUnits(psu, mlp.Density);
            AllowedUnits = compatibleUnits.ToList();
            RaisePropertyChanged(() => AllowedUnits);

            SelectedUnit = AllowedUnits.Where(x => x == initialUnit).FirstOrDefault();
            RaisePropertyChanged(() => SelectedUnit);

            _averagePrice = detailsView.AveragePriceValue;
            _averagePriceUnit = string.IsNullOrEmpty(detailsView.AveragePriceUnit) ? endpoint.GetProductUnitResolver().GetProductPackageUnit(productId) : UnitFactory.GetPackageSafeUnit(detailsView.AveragePriceUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
            ProductName = mlp.Name;
            RaisePropertyChanged(() => ProductName);
            RaisePropertyChanged(() => AveragePriceText);

            UpdateCommand = new RelayCommand(Update);
            CancelCommand = new RelayCommand(Cancel);
        }

        public RelayCommand UpdateCommand { get; set; }
        public RelayCommand CancelCommand { get; set; }

        public IList<IUnit> AllowedUnits { get; set; }
        public IUnit SelectedUnit { get; set; }
        
        public string ProductName { get; set; }
        private decimal _averagePrice { get; set; }
        public IUnit _averagePriceUnit { get; set; }
        public string AveragePriceText { get { return string.Format("{0} / {1}", _averagePrice.ToString("C2"), _averagePriceUnit.AbbreviatedDisplay); } }

		void Update() {
			var productSettingId = new ProductSettingId(ApplicationEnvironment.CurrentDataSourceId, productId);

			if (SelectedUnit != null)
			{
				var command = new UpdateProductDefaultAveragePriceUnit(
					productSettingId,
					endpoint.GenerateNewMetadata(),
					productId,
					ApplicationEnvironment.CurrentCropYear,
					SelectedUnit.Name
				);

				endpoint.SendOne(command);

				Cancel();
			}
		}

        void Cancel()
        {
            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
        }
    }
}
