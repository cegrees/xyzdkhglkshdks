﻿using AgC.UnitConversion;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.ProductSetting;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Secondary.Application;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Landdb.ViewModel.Resources.Popups
{
    public class EditDefaultProductUnitsViewModel : ViewModelBase
    {
        IClientEndpoint clientEndpoint;
        Dispatcher dispatcher;

        UnitPerAreaHelper ratePerAreaUnitHelper;
        IUnit ratePer100UnitHelper;
        IUnit totalProductUnit;
        IUnit ratePerTankUnit;
        ProductId productId;

        public EditDefaultProductUnitsViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, ProductId productId)
        {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            this.productId = productId;

            AllowedRateUnits = new ObservableCollection<UnitPerAreaHelper>();
            AllowedTotalUnits = new ObservableCollection<IUnit>();

            ProductName = clientEndpoint.GetMasterlistService().GetProductDisplay(productId);
            UpdateAllowedUnits();
            SetDefaultUnits();
            UpdateCommand = new RelayCommand(Update);
            CancelCommand = new RelayCommand(Cancel);
        }

        public RelayCommand UpdateCommand { get; set; }
        public RelayCommand CancelCommand { get; set; }

        public string ProductName { get; set; }
        public ObservableCollection<UnitPerAreaHelper> AllowedRateUnits { get; private set; }
        public ObservableCollection<IUnit> AllowedTotalUnits { get; private set; }

        public UnitPerAreaHelper SelectedRatePerAreaUnitHelper
        {
            get { return ratePerAreaUnitHelper; }
            set
            {
                ratePerAreaUnitHelper = value;

                RaisePropertyChanged("SelectedRatePerAreaUnitHelper");
            }
        }

        public IUnit SelectedRatePer100UnitHelper
        {
            get { return ratePer100UnitHelper; }
            set
            {
                ratePer100UnitHelper = value;

                RaisePropertyChanged("SelectedRatePer100UnitHelper");
            }
        }

        public IUnit SelectedRatePerTankUnit
        {
            get { return ratePerTankUnit; }
            set
            {
                ratePerTankUnit = value;

                RaisePropertyChanged("SelectedRatePerTankUnit");
            }
        }

        public IUnit TotalProductUnit
        {
            get { return totalProductUnit; }
            set
            {
                totalProductUnit = value;

                RaisePropertyChanged("TotalProductUnit");
            }
        }

        void UpdateAllowedUnits()
        {
            AllowedRateUnits.Clear();
            AllowedTotalUnits.Clear();

            SelectedRatePerAreaUnitHelper = null;
            var mlp = clientEndpoint.GetMasterlistService().GetProduct(productId);
            if (mlp == null) { return; }

            var psu = UnitFactory.GetPackageSafeUnit(mlp.StdPackageUnit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
            var compatibleUnits = AgC.UnitConversion.CompatibilityFactory.GetCompatibleUnits(psu, mlp.Density);

            var defaultUnit = clientEndpoint.GetUserSettings().PreferredRateUnits.ContainsKey(productId.Id) ?
                (from u in compatibleUnits where u.Name == clientEndpoint.GetUserSettings().PreferredRateUnits[productId.Id] select u).FirstOrDefault() ?? psu
                : psu;
            var defaultTotalUnit = clientEndpoint.GetUserSettings().PreferredTotalUnits.ContainsKey(productId.Id) ?
                (from u in compatibleUnits where u.Name == clientEndpoint.GetUserSettings().PreferredTotalUnits[productId.Id] select u).FirstOrDefault() ?? psu
                : psu;


            var defaultRatePerTankUnit = clientEndpoint.GetUserSettings().PreferredRatePerTankUnits.ContainsKey(productId.Id) ?
                (from u in compatibleUnits where u.Name == clientEndpoint.GetUserSettings().PreferredRatePerTankUnits[productId.Id] select u).FirstOrDefault() ?? psu
                : psu;

            foreach (var u in compatibleUnits)
            {
                AllowedTotalUnits.Add(u);
                var uh = new UnitPerAreaHelper(u, UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit));
                AllowedRateUnits.Add(uh);
            }
        }

        private ProductSettingsView ProductSetting { get; set; }

        void SetDefaultUnits()
        {
            //get the values....
            var mlp = clientEndpoint.GetMasterlistService().GetProduct(productId);

            try
            {
                var prodsettingMaybe = clientEndpoint.GetView<ProductSettingsView>(new ProductSettingId(ApplicationEnvironment.CurrentDataSourceId, productId.Id));

                if (prodsettingMaybe.HasValue)
                {
                    var prodSetting = prodsettingMaybe.Value;
                    ProductSetting = prodSetting;

                    SelectedRatePerAreaUnitHelper = string.IsNullOrEmpty(prodSetting.RatePerAreaUnit) ? null : AllowedRateUnits.SingleOrDefault(x => x.Unit.Name == prodSetting.RatePerAreaUnit);
                    SelectedRatePer100UnitHelper = string.IsNullOrEmpty(prodSetting.RatePer100Unit) ? null : AllowedTotalUnits.SingleOrDefault(x => x.Name == prodSetting.RatePer100Unit);
                    SelectedRatePerTankUnit = string.IsNullOrEmpty(prodSetting.RatePerTankUnit) ? null : AllowedTotalUnits.SingleOrDefault(x => x.Name == prodSetting.RatePerTankUnit);
                    TotalProductUnit = string.IsNullOrEmpty(prodSetting.TotalUnit) ? null : AllowedTotalUnits.SingleOrDefault(x => x.Name == prodSetting.TotalUnit);

                }
            }
            catch { }
        }

        void Update()
        {
            string ratePerArea = SelectedRatePerAreaUnitHelper != null ? SelectedRatePerAreaUnitHelper.Unit.Name : string.Empty;
            string ratePer100 = SelectedRatePer100UnitHelper != null ? SelectedRatePer100UnitHelper.Name : string.Empty;
            string ratePerTank = SelectedRatePerTankUnit != null ? SelectedRatePerTankUnit.Name : string.Empty;
            string total = TotalProductUnit != null ? TotalProductUnit.Name : string.Empty;

            if (ProductSetting == null || ratePerArea != ProductSetting.RatePerAreaUnit || ratePer100 != ProductSetting.RatePer100Unit || ratePerTank != ProductSetting.RatePerTankUnit || total != ProductSetting.TotalUnit)
            {
                var productCommand = new UpdateProductDefaultUnits(new ProductSettingId(ApplicationEnvironment.CurrentDataSourceId, productId.Id), clientEndpoint.GenerateNewMetadata(), ratePerArea, ratePer100, ratePerTank, total);
                clientEndpoint.SendOne(productCommand);
            }

            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
        }

        void Cancel()
        {
            Messenger.Default.Send<HidePopupMessage>(new HidePopupMessage());
        }
    }
}
