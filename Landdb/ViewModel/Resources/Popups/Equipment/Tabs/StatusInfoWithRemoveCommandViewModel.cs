﻿using System;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;

namespace Landdb.ViewModel.Resources.Popups.Equipment.Tabs
{
	public class StatusInfoWithRemoveCommandViewModel : StatusInfoViewModel, IEquatable<StatusInfoWithRemoveCommandViewModel>
	{
		private readonly Action<StatusInfoWithRemoveCommandViewModel> onRemove;
		private readonly Func<bool> isEnabled;
		private readonly Func<StatusInfoWithRemoveCommandViewModel, bool> isCurrent;
		public ICommand RemoveStatus { get; }

		public StatusInfoWithRemoveCommandViewModel(EquipmentStatus equipmentStatusReadModel, string equipmentStatusDisplayText,
			Action<StatusInfoWithRemoveCommandViewModel> onRemove, Func<bool> isEnabled, 
			Func<StatusInfoWithRemoveCommandViewModel, bool> isCurrent) : 
			base(equipmentStatusReadModel, equipmentStatusDisplayText) 
		{
			this.onRemove = onRemove;
			this.isEnabled = isEnabled;
			this.isCurrent = isCurrent;
			RemoveStatus = new RelayCommand(Remove);
		}

		public bool IsEnabled => isEnabled();

		public bool IsCurrent => isCurrent(this);

		private void Remove()
		{
			onRemove?.Invoke(this);
			RaisePropertyChanged(() => IsEnabled);
			RaisePropertyChanged(() => IsCurrent);
		}

		public bool Equals(StatusInfoWithRemoveCommandViewModel other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return EquipmentStatusType == other.EquipmentStatusType && StatusDate.Equals(other.StatusDate)
			       && string.Equals(EquipmentStatusDisplayText, other.EquipmentStatusDisplayText) && IsActive == other.IsActive;
		}

		public new bool Equals(object obj)
		{
			StatusInfoWithRemoveCommandViewModel statusInfoWithRemoveCommandViewModel = obj as StatusInfoWithRemoveCommandViewModel;
			return Equals(statusInfoWithRemoveCommandViewModel);
		}
	}
}