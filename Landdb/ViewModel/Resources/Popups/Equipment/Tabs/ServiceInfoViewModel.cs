﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure;
using Landdb.ViewModel.Resources.Equipment;
using Landdb.ViewModel.Secondary.UserCreatedProduct;
using Landdb.ViewModel.Shared;
using Optional;
using Optional.Unsafe;
using Telerik.ReportViewer.Common;

namespace Landdb.ViewModel.Resources.Popups.Equipment.Tabs
{
	public sealed class ServiceInfoViewModel : ViewModelBase
	{
		private readonly IClientEndpoint clientEndpoint;
		private readonly Action<ServiceInfoViewModel> onRemove;
		public ObservableCollection<ProductListItemDetails> Products { get; }
		public ICommand RemoveService { get; }
		public ICommand CreateUserProductCommand { get; }
		public string ProductSearchText { get; set; }
		private Option<double> operationCost;

		public ServiceInfoViewModel(IClientEndpoint clientEndpoint, Action<ServiceInfoViewModel> onRemove,
			ServicesInfoCollectionViewModel servicesInfoCollectionViewModel)
		{
			this.clientEndpoint = clientEndpoint;
			this.onRemove = onRemove;
			Products = servicesInfoCollectionViewModel.Products;
			RemoveService = new RelayCommand(RemoveCommand);
			CreateUserProductCommand = new RelayCommand(CreateUserProduct);
		}

		public ServiceInfoViewModel(IClientEndpoint clientEndpoint, EquipmentServiceInfo equipmentServiceInfo, 
			Action<ServiceInfoViewModel> onRemove,
			ServicesInfoCollectionViewModel servicesInfoCollectionViewModel) :
			this(clientEndpoint, onRemove, servicesInfoCollectionViewModel)
		{
			OperationCost = equipmentServiceInfo.OperationCost;
			SelectedProduct = ProductListFactory
				.CreateProductListFactory(clientEndpoint, new CropYearId(new Guid(), ApplicationEnvironment.CurrentCropYear))
				.BuildProductList()
				.Find(productListItemDetails => productListItemDetails.ProductId == equipmentServiceInfo.ServiceProductId);
		}

		private void RemoveCommand()
		{
			onRemove?.Invoke(this);
		}

		private void CreateUserProduct()
		{
			UserProductFactory.CreateUserProductFactory(clientEndpoint, ProductSearchText, OnProductCreated, UserProductCreatorViewModel.ServiceType).CreateUserProduct();
		}

		private void OnProductCreated(MiniProduct product)
		{
			ProductListItemDetails temporaryProductListItemDetails = new ProductListItemDetails(product, new ProductId(ApplicationEnvironment.CurrentDataSourceId, product.Id).Id, "Used this year", 0);
			Products.Add(temporaryProductListItemDetails);
			SelectedProduct = temporaryProductListItemDetails;
		}

		private ProductListItemDetails selectedProductListItemDetails;

		public ProductListItemDetails SelectedProduct
		{
			get => selectedProductListItemDetails;
			set
			{
				selectedProductListItemDetails = value;
				RaisePropertyChanged(() => ProductUnit);
				RaisePropertyChanged(() => ServiceName);
			}
		}

		public string ServiceName => SelectedProduct?.Product.Name;

		public string ProductUnit => SelectedProduct?.Product?.StdUnit;

		public double? OperationCost
		{
			get => operationCost.HasValue ? operationCost.ValueOrFailure() : (double?)null;
			set
			{
				if (value != null)
				{
					double castValue = (double)value;
					operationCost = castValue.SomeNotNull();
				}
				else
				{
					operationCost = Option.None<double>();
				}
				RaisePropertyChanged(() => OperationCost);
			}
		}

		public string OperationCostAsString
		{
			get => $"{OperationCost:C2}";
			set
			{
				if (string.IsNullOrEmpty(value))
				{
					OperationCost = null;
				}
				else
				{
					OperationCost = double.TryParse(value, out double result) ? result : (double?) null;
				}
			}
		}
	}
}