﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using Landdb.Resources;
using Landdb.ViewModel.Resources.Equipment;
using Landdb.ViewModel.Shared;

namespace Landdb.ViewModel.Resources.Popups.Equipment.Tabs
{
	public class ServicesInfoCollectionViewModel : ValidationViewModelBase
	{
		private readonly IClientEndpoint clientEndpoint;
		private readonly CropYearId cropYearId;
		public ObservableCollection<ServiceInfoViewModel> Services { get; set; }
		public ICommand AddNewService { get; }


		public ServicesInfoCollectionViewModel(IClientEndpoint clientEndpoint, CropYearId cropYearId)
		{
			this.clientEndpoint = clientEndpoint;
			this.cropYearId = cropYearId;
			Services = new ObservableCollection<ServiceInfoViewModel>();
			AddNewService = new RelayCommand(AddService);

		}

		public ServicesInfoCollectionViewModel(IClientEndpoint clientEndpoint, CropYearId cropYearId, IEnumerable<EquipmentServiceInfo> equipmentServiceInfoReadModels) 
			: this(clientEndpoint, cropYearId)
		{
			CreateServicesList(equipmentServiceInfoReadModels);
		}

		public void AddService()
		{
			Services.Add(new ServiceInfoViewModel(clientEndpoint, RemoveService, this));
		}

		private void RemoveService(ServiceInfoViewModel serviceInfoViewModel)
		{
			Services.Remove(serviceInfoViewModel);
			RaisePropertyChanged(() => HasServices);
			RaisePropertyChanged(() => ServicesHeader);
		}

		public bool HasServices => Services.Count > 0;

		public string ServicesHeader => HasServices ? Strings.Cost_Text : Strings.BracketedAddCost_Text;

		private void CreateServicesList(IEnumerable<EquipmentServiceInfo> equipmentServicesView)
		{
			equipmentServicesView.ForEach(service =>
			{
				Services.Add(new ServiceInfoViewModel(clientEndpoint, service, RemoveService, this));
			});
		}

		public void ChangeProperties(ServicesInfoCollectionViewModel otherServicesInfoCollectionViewModel)
		{
			Services = otherServicesInfoCollectionViewModel.Services;
			RaisePropertyChanged(() => Services);
			RaisePropertyChanged(() => HasServices);
			RaisePropertyChanged(() => ServicesHeader);
		}

		public ObservableCollection<ProductListItemDetails> Products { get; set; }

		public void BuildProductList()
		{
			Products = ProductListFactory.CreateProductListFactory(clientEndpoint, cropYearId).BuildServicesList();
		}

		public EquipmentServiceInfo[] GetEquipmentServiceInfoArray()
		{
			List<EquipmentServiceInfo> equipmentServiceInfoList = new List<EquipmentServiceInfo>();

			foreach (ServiceInfoViewModel service in Services)
			{
				if (service.SelectedProduct != null)
				{
					equipmentServiceInfoList.Add(new EquipmentServiceInfo(service.SelectedProduct.ProductId, service.OperationCost));
				}
			}

			return equipmentServiceInfoList.ToArray();
		}
	}
}