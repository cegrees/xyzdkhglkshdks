﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Landdb.ViewModel.Resources.Equipment;
using Optional;
using Optional.Unsafe;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace Landdb.ViewModel.Resources.Popups.Equipment.Tabs {
	public class StatusInfoCollectionViewModel : ViewModelBase {

		private const int numberOfRecentStatuses = 5;
		private const int indexMinimumPosition = 0;

		public StatusInfoCollectionViewModel(IEnumerable<EquipmentStatus> equipmentStatusReadModels) {
			Statuses = OrderStatuses(BuildStatusInfoListFromReadModel(equipmentStatusReadModels));
			RecentStatuses = GetRecentStatuses(Statuses);
			AddNewStatus = new RelayCommand(AddStatus);
		}

		public ObservableCollection<StatusInfoWithRemoveCommandViewModel> Statuses { get; set; }
		public ObservableCollection<StatusInfoWithRemoveCommandViewModel> RecentStatuses { get; private set; }

		public ICommand AddNewStatus { get; }

		private IEnumerable<StatusInfoWithRemoveCommandViewModel> BuildStatusInfoListFromReadModel(IEnumerable<EquipmentStatus> equipmentStatusReadModels)
		{
			List<StatusInfoWithRemoveCommandViewModel> result = new List<StatusInfoWithRemoveCommandViewModel>();

			equipmentStatusReadModels.ForEach(equipmentStatusReadModel => {
				result.Add(StatusInfoFactory.BuildStatusInfoWithRemoveCommand(equipmentStatusReadModel, RemoveStatus, IsEnabled, IsCurrent));
			});

			return OrderStatuses(result);
		}

		private static ObservableCollection<StatusInfoWithRemoveCommandViewModel> GetRecentStatuses(IReadOnlyCollection<StatusInfoWithRemoveCommandViewModel> statuses)
		{
			ObservableCollection<StatusInfoWithRemoveCommandViewModel> recentStatuses =
				new ObservableCollection<StatusInfoWithRemoveCommandViewModel>();

			int index = GetMaximumIndex(statuses);
			int count = 0;

			while (count < numberOfRecentStatuses && index >= indexMinimumPosition || index >= GetCurrentStatusIndex(statuses)) {
				recentStatuses.Add(statuses.ElementAt(index));
				count++;
				index--;
			}

			return new ObservableCollection<StatusInfoWithRemoveCommandViewModel>(recentStatuses.OrderBy(status => status.GetStatusDate()));
		}

		private static int GetMaximumIndex(IEnumerable<StatusInfoViewModel> collection) {
			return collection.Count() - 1;
		}

		public bool HasMoreStatusesThanRecentStatuses => Statuses.Count > RecentStatuses.Count;

		private void AddStatus() {
			Statuses.Add(StatusInfoFactory.BuildStatusInfoWithRemoveCommand(
				StatusInfoFactory.BuildDefaultStatusInfoViewModel().GetEquipmentStatus(),
				RemoveStatus, IsEnabled, IsCurrent));
			Statuses = OrderStatuses(Statuses);
			RaisePropertyChanged(() => Statuses);
			RaisePropertyChanged(() => HasMoreThanOneStatus);
			RaisePropertyChanged(() => HasMoreStatusesThanRecentStatuses);
		}

		private void RemoveStatus(StatusInfoWithRemoveCommandViewModel statusInfoViewModelToBeRemoved) {
			Statuses.Remove(statusInfoViewModelToBeRemoved);
			RefreshStatuses();
		}

		public bool IsEnabled() {
			return HasMoreThanOneStatus;
		}

		public bool HasMoreThanOneStatus => Statuses?.Count > 1;

		public bool IsCurrent(StatusInfoWithRemoveCommandViewModel status) {
			return status.Equals(GetCurrentStatus().ValueOrFailure());
		}

		private static ObservableCollection<StatusInfoWithRemoveCommandViewModel> OrderStatuses(IEnumerable<StatusInfoWithRemoveCommandViewModel> statuses) {
			return new ObservableCollection<StatusInfoWithRemoveCommandViewModel>(
				statuses.OrderBy(status => status.StatusDate.Equals(DateTime.MinValue) || status.StatusDate == null ? DateTime.MaxValue : status.StatusDate));
		}

		public Option<StatusInfoViewModel> GetCurrentStatus() {
			Statuses = OrderStatuses(Statuses);
			StatusInfoViewModel defaultStatus = Statuses.ElementAt(GetCurrentStatusIndex());
			return defaultStatus.Some();
		}

		public void SetCurrentStatus(StatusInfoViewModel otherStatusInfoViewModel)
		{
			GetCurrentStatus().ValueOrFailure().EquipmentStatusType = otherStatusInfoViewModel.EquipmentStatusType;
			GetCurrentStatus().ValueOrFailure().EquipmentStatusDisplayText = otherStatusInfoViewModel.EquipmentStatusDisplayText;
			GetCurrentStatus().ValueOrFailure().IsActive = otherStatusInfoViewModel.IsActive;
			GetCurrentStatus().ValueOrFailure().StatusDate = otherStatusInfoViewModel.StatusDate;
			RefreshStatuses();
		}

		private int GetCurrentStatusIndex() {
			return GetCurrentStatusIndex(Statuses);
		}

		private static int GetCurrentStatusIndex(IReadOnlyCollection<StatusInfoWithRemoveCommandViewModel> statuses) {
			int result = indexMinimumPosition;

			for (int index = indexMinimumPosition; index < statuses.Count; index++) {
				if (statuses.ElementAt(index).StatusDate <= DateTime.Today) {
					result = index;
				}
			}
			return result;
		}

		public IEnumerable<EquipmentStatus> GetEquipmentStatusValueObjectList() {
			return Statuses.Select(x => new EquipmentStatus(x.StatusId, x.EquipmentStatusType, x.GetStatusDate(), x.IsActive));
		}

		public void RefreshStatuses() {
			Statuses = OrderStatuses(Statuses);
			RecentStatuses = GetRecentStatuses(Statuses);
			RaisePropertyChanged(() => Statuses);
			RaisePropertyChanged(() => RecentStatuses);
			RaisePropertyChanged(() => HasMoreThanOneStatus);
			RaisePropertyChanged(() => HasMoreStatusesThanRecentStatuses);
		}
	}
}