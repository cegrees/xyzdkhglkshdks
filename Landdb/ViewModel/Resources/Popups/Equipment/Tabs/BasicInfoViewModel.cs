﻿using System;
using Optional;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Landdb.Resources;
using Landdb.ViewModel.Resources.Equipment;
using Optional.Unsafe;

namespace Landdb.ViewModel.Resources.Popups.Equipment.Tabs
{
	public class BasicInfoViewModel : ValidationViewModelBase
	{
		public static List<StatusInfoSelectorItem> StatusesList { get; } = StatusInfoSelectorItem.GetActiveStatusInfoSelectorItems;
		public static List<EquipmentTypeViewModel> EquipmentTypeList { get; } = EquipmentTypeViewModel.EquipmentTypeList;
		private Option<string> name;
		private Option<string> make;
		private Option<string> model;
		private Option<uint> year;
		private Option<string> serialNumber;
		private Option<string> unitId;
		private Option<string> licenseNumber;
		private Option<string> notes;
		private Option<EquipmentTypeViewModel> selectedEquipmentType;
		private readonly StatusInfoCollectionViewModel StatusInfoCollectionViewModel;
		private StatusInfoViewModel CurrentStatusInfoViewModel;

		public BasicInfoViewModel(EquipmentBasicInfo basicInfoValueObject, IEnumerable<EquipmentStatus> statusList, string notes)
		{
			Name = basicInfoValueObject.Name;
			Model = basicInfoValueObject.Model;
			Make = basicInfoValueObject.Make;
			Year = basicInfoValueObject.ModelYear;
			SelectedEquipmentType = EquipmentTypeViewModelFactory.BuildEquipmentTypeViewModel(basicInfoValueObject.EquipmentType);
			Notes = notes;
			SerialNumber = basicInfoValueObject.SerialNumber;
			UnitId = basicInfoValueObject.UnitID;
			LicenseNumber = basicInfoValueObject.LicenseNumber;
			StatusInfoCollectionViewModel = new StatusInfoCollectionViewModel(statusList);
			CurrentStatusInfoViewModel = StatusInfoCollectionViewModel.GetCurrentStatus().ValueOrFailure();
		}

	    [Required(AllowEmptyStrings = false, ErrorMessageResourceName = "ErrorMessage_NameIsRequired_Text", ErrorMessageResourceType = typeof(Strings))]
        public string Name
		{
			get => name.HasValue ? name.ValueOrFailure() : YearMakeModelString;
			set
			{
				name = string.IsNullOrWhiteSpace(value) ? Option.None<string>() : value.SomeNotNull();
				ValidateAndRaisePropertyChanged(() => Name);
			}
		}

		public string YearMakeModelString => $"{Year} {Make} {Model}".Trim();

		public string YearMakeModelDisplayText =>
			string.IsNullOrWhiteSpace(YearMakeModelString) ? Strings.BracketedAddMakeModelYear_Text : YearMakeModelString;

		public string Make
		{
			get => make.ValueOr("");
			set
			{
				make = value.SomeNotNull();
				RaisePropertyChanged(() => Make);
				ValidateAndRaisePropertyChanged(() => Name);
				RaisePropertyChanged(() => YearMakeModelString);
				RaisePropertyChanged(() => YearMakeModelDisplayText);
			}
		}

		public string Model
		{
			get => model.ValueOr("");
			set
			{
				model = value.SomeNotNull();
				RaisePropertyChanged(() => Model);
				ValidateAndRaisePropertyChanged(() => Name);
				RaisePropertyChanged(() => YearMakeModelString);
				RaisePropertyChanged(() => YearMakeModelDisplayText);
			}
		}

		public string SerialNumber
		{
			get => serialNumber.ValueOr("");
			set
			{
				serialNumber = value.SomeNotNull();
				RaisePropertyChanged(() => SerialNumber);
				RaisePropertyChanged(() => SerialNumberDisplayText);
			} 
		}

		public string SerialNumberDisplayText => !string.IsNullOrWhiteSpace(SerialNumber)? SerialNumber : Strings.BracketedAddSerialNumber_Text;

		public string UnitId
		{
			get => unitId.ValueOr("");
			set
			{
				unitId = value.SomeNotNull();
				RaisePropertyChanged(() => UnitId);
				RaisePropertyChanged(() => UnitIdDisplayText);
			}
		}

		public string UnitIdDisplayText => !string.IsNullOrWhiteSpace(UnitId) ? UnitId : Strings.BracketedAddUnitId_Text;

		public string LicenseNumber
		{
			get => licenseNumber.ValueOr("");
			set
			{
				licenseNumber = value.SomeNotNull();
				RaisePropertyChanged(() => LicenseNumber);
				RaisePropertyChanged(() => LicenseNumberDisplayText);
			} 
		}

		public string LicenseNumberDisplayText => !string.IsNullOrWhiteSpace(LicenseNumber) ? LicenseNumber : Strings.BracketedAddLicenseNumber_Text;

		public string Notes
		{
			get => notes.ValueOr("");
			set
			{
				notes = value.SomeNotNull();
				RaisePropertyChanged(() => Notes);
				RaisePropertyChanged(() => HasNotes);
				RaisePropertyChanged(() => NotesHeader);
			} 
		}

		public bool HasNotes => !string.IsNullOrWhiteSpace(Notes);

		public string NotesHeader => HasNotes ? Strings.Notes_Text : Strings.BracketedAddNotes_Text;

		public EquipmentTypeViewModel SelectedEquipmentType
		{
			get => selectedEquipmentType.ValueOr(EquipmentTypeViewModel.EquipmentTypeList.FirstOrDefault());
			set
			{
				selectedEquipmentType = value.SomeNotNull();
				RaisePropertyChanged(() => SelectedEquipmentType);
			}
		}

		public StatusInfoSelectorItem CurrentStatusInfo
		{
			get => StatusInfoSelectorItem.CreateStatusInfoSelectorItem(CurrentStatusInfoViewModel.EquipmentStatusType,
				CurrentStatusInfoViewModel.IsActive, CurrentStatusInfoViewModel.EquipmentStatusDisplayText);
			set
			{
				DateTime currentDate = StatusInfoCollectionViewModel.GetCurrentStatus().ValueOrFailure().GetStatusDate();
				EquipmentStatus updatedEquipmentStatus = new EquipmentStatus(CurrentStatusInfoViewModel.StatusId,
					value.EquipmentStatusType, currentDate, value.IsActive);
				StatusInfoCollectionViewModel.SetCurrentStatus(StatusInfoFactory.BuildStatusInfoViewModel(updatedEquipmentStatus));
				CurrentStatusInfoViewModel = StatusInfoFactory.BuildStatusInfoViewModel(updatedEquipmentStatus);
				RaisePropertyChanged(() => CurrentStatusInfo);
			}
		}

		public DateTime? CurrentStatusInfoDate
		{
			get
			{
				DateTime currentDate = StatusInfoCollectionViewModel.GetCurrentStatus().ValueOrFailure().GetStatusDate();
				return currentDate.Equals(DateTime.MinValue) ? (DateTime?) null : currentDate;
			}

			set
			{
				DateTime updatedDate = value ?? DateTime.MinValue;
				EquipmentStatus updatedEquipmentStatus = new EquipmentStatus(CurrentStatusInfoViewModel.StatusId,
					CurrentStatusInfo.EquipmentStatusType, updatedDate, CurrentStatusInfo.IsActive);
				StatusInfoCollectionViewModel.SetCurrentStatus(StatusInfoFactory.BuildStatusInfoViewModel(updatedEquipmentStatus));
				CurrentStatusInfoViewModel = StatusInfoFactory.BuildStatusInfoViewModel(updatedEquipmentStatus);
				RaisePropertyChanged(() => CurrentStatusInfoDate);
			}
		}

		public string CurrentStatusInfoDateDisplayText => CurrentStatusInfoDate?.ToShortDateString();

		public StatusInfoViewModel GetCurrentStatusInfoViewModel =>
			StatusInfoCollectionViewModel.GetCurrentStatus().ValueOrFailure();

		public IEnumerable<EquipmentStatus> GetEquipmentStatuses()
		{
			return StatusInfoCollectionViewModel.GetEquipmentStatusValueObjectList();
		}

		public uint? Year
		{
			get
			{
				if (year.HasValue)
				{
					return year.ValueOrFailure();
				}
				return null;
			}

			set
			{
				
				if (value != null)
				{
					uint castValue = (uint) value;

					year = castValue == 0 ? Option.None<uint>() : castValue.SomeNotNull();
				}
				else
				{
					year = Option.None<uint>();
				}
				RaisePropertyChanged(()=>Year);
				ValidateAndRaisePropertyChanged(() => Name);
				RaisePropertyChanged(() => YearMakeModelString);
				RaisePropertyChanged(() => YearMakeModelDisplayText);
			}
		}

		public string YearAsString
		{
			get => $"{Year}";
			set => Year = uint.TryParse(value, out uint result) ? result : (uint?) null;
		}

		public void ChangeProperties(BasicInfoViewModel otherBasicInfoViewModel)
		{
			Name = otherBasicInfoViewModel.Name;
			Model = otherBasicInfoViewModel.Model;
			Make = otherBasicInfoViewModel.Make;
			Year = otherBasicInfoViewModel.Year;
			LicenseNumber = otherBasicInfoViewModel.LicenseNumber;
			SelectedEquipmentType = otherBasicInfoViewModel.SelectedEquipmentType;
			SerialNumber = otherBasicInfoViewModel.SerialNumber;
			Notes = otherBasicInfoViewModel.Notes;
			UnitId = otherBasicInfoViewModel.UnitId;
			CurrentStatusInfo = otherBasicInfoViewModel.CurrentStatusInfo;
		}

		public void ChangeNotes(string changedNotes)
		{
			Notes = changedNotes;
		}

		public EquipmentBasicInfo GetEquipmentBasicInfo()
		{
			return new EquipmentBasicInfo(Name, Make, Model, year.ValueOr(0), SerialNumber, UnitId, LicenseNumber, SelectedEquipmentType.GetEquipmentType);
		}
	}
}