﻿using System;
using System.Collections.Generic;
using System.Linq;
using GalaSoft.MvvmLight;
using Landdb.ViewModel.Resources.Equipment;
using Optional;
using Optional.Unsafe;

namespace Landdb.ViewModel.Resources.Popups.Equipment.Tabs
{
	public class StatusInfoViewModel : ViewModelBase, IEquatable<StatusInfoViewModel>
	{
		//TODO: Remove these three lists.
		public static List<StatusInfoViewModel> ActiveStatusInfoViewModelList { get; } = new List<StatusInfoViewModel>
		{
			StatusInfoFactory.BuildStatusInfoViewModel(new EquipmentStatus(Guid.NewGuid(), EquipmentStatusType.OWNED, DateTime.Today, true)),
			StatusInfoFactory.BuildStatusInfoViewModel(new EquipmentStatus(Guid.NewGuid(), EquipmentStatusType.CONTRACTED, DateTime.Today, true)),
			StatusInfoFactory.BuildStatusInfoViewModel(new EquipmentStatus(Guid.NewGuid(), EquipmentStatusType.LEASED, DateTime.Today, true))
		};
		public static List<StatusInfoViewModel> InactiveStatusInfoViewModelList { get; } = new List<StatusInfoViewModel>
		{
			StatusInfoFactory.BuildStatusInfoViewModel(new EquipmentStatus(Guid.NewGuid(), EquipmentStatusType.SOLD, DateTime.Today, false)),
			StatusInfoFactory.BuildStatusInfoViewModel(new EquipmentStatus(Guid.NewGuid(), EquipmentStatusType.CONTRACT_TERMINATED, DateTime.Today, false)),
			StatusInfoFactory.BuildStatusInfoViewModel(new EquipmentStatus(Guid.NewGuid(), EquipmentStatusType.LEASE_TERMINATED, DateTime.Today, false)),
			StatusInfoFactory.BuildStatusInfoViewModel(new EquipmentStatus(Guid.NewGuid(), EquipmentStatusType.JUNKED, DateTime.Today, false)),
		};
		public static List<StatusInfoViewModel> AllStatusInfoViewModels { get; } =
			ActiveStatusInfoViewModelList.Concat(InactiveStatusInfoViewModelList).ToList();



		public List<StatusInfoSelectorItem> AllStatusInfoSelectorItems { get; }=
			StatusInfoSelectorItem.GetAllStatusInfoSelectorItems;
		public Guid StatusId { get; }
		public EquipmentStatusType EquipmentStatusType { get; set; }
		private Option<DateTime> statusDate;
		public string EquipmentStatusDisplayText { get; set; }
		public bool IsActive { get; set; }

		public StatusInfoViewModel(EquipmentStatus equipmentStatusReadModel, string equipmentStatusDisplayText)
		{
			StatusId = equipmentStatusReadModel.StatusId;
			EquipmentStatusType = equipmentStatusReadModel.EquipmentStatusType;
			StatusDate = equipmentStatusReadModel.StatusDate;
			EquipmentStatusDisplayText = equipmentStatusDisplayText;
			IsActive = equipmentStatusReadModel.IsActive;
			selectedStatusInfoSelectorItem = StatusInfoSelectorItem.CreateStatusInfoSelectorItem(
				equipmentStatusReadModel.EquipmentStatusType, equipmentStatusReadModel.IsActive, equipmentStatusDisplayText);
		}

		public DateTime? StatusDate
		{
			get
			{
				if (statusDate.HasValue)
				{
					if (statusDate.ValueOrFailure().Equals(DateTime.MinValue))
					{
						return null;
					}
					else
					{
						return statusDate.ValueOrFailure();
					}
				}
				else
				{
					return null;
				}
			}
			set
			{
				if (value != null)
				{
					DateTime valueCastToDateTime = (DateTime) value;
					statusDate = valueCastToDateTime.SomeNotNull();
				}
				RaisePropertyChanged(() => StatusDate);
			}
		}

		public DateTime GetStatusDate()
		{
			return statusDate.ValueOr(DateTime.MinValue);
		}

		public string DisplayStatusDate => StatusDate?.ToShortDateString();

		private StatusInfoSelectorItem selectedStatusInfoSelectorItem;

		public StatusInfoSelectorItem SelectedStatusInfoSelectorItem {
			get => selectedStatusInfoSelectorItem ?? AllStatusInfoSelectorItems.FirstOrDefault();
			set
			{
				selectedStatusInfoSelectorItem = StatusInfoSelectorItem.CreateStatusInfoSelectorItem(value.EquipmentStatusType,
					value.IsActive, value.EquipmentStatusDisplayText);
				EquipmentStatusType = value.EquipmentStatusType;
				EquipmentStatusDisplayText = value.EquipmentStatusDisplayText;
				IsActive = value.IsActive;
				RaisePropertyChanged(() => SelectedStatusInfoSelectorItem);
			}
		}

		public bool Equals(StatusInfoViewModel other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return StatusId == other.StatusId;
		}
		
		public override bool Equals(object obj)
		{
			StatusInfoViewModel statusInfoViewModel = obj as StatusInfoViewModel;
			return Equals(statusInfoViewModel);
		}

		public override int GetHashCode()
		{
			return StatusId.GetHashCode();
		}

		public EquipmentStatus GetEquipmentStatus()
		{
			return new EquipmentStatus(StatusId, EquipmentStatusType, statusDate.ValueOrFailure(), IsActive);
		}
	}
}