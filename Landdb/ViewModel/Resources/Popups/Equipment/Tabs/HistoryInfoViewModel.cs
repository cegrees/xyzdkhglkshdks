﻿using System;
using Landdb.Resources;
using Optional;
using Optional.Unsafe;

namespace Landdb.ViewModel.Resources.Popups.Equipment.Tabs
{
	public class HistoryInfoViewModel : ValidationViewModelBase
	{
		private Option<double> purchasePrice;
		private DateTime purchaseDate;
		private Option<double> currentValue;
		private DateTime currentDate;

		public HistoryInfoViewModel(EquipmentHistoryInfo equipmentHistoryInfo)
		{
			if (equipmentHistoryInfo != null)
			{
				PurchasePrice = equipmentHistoryInfo.PurchasePrice;
				PurchaseDate = equipmentHistoryInfo.PurchaseDate;
				CurrentValue = equipmentHistoryInfo.CurrentValue;
				CurrentDate = equipmentHistoryInfo.CurrentValueDate;
			}
		}

		public double? PurchasePrice
		{
			get => purchasePrice.HasValue ? purchasePrice.ValueOrFailure() : (double?) null;
			set
			{
				if (value != null)
				{
					double castValue = (double) value;
					purchasePrice = castValue.SomeNotNull();
				}
				else
				{
					purchasePrice = Option.None<double>();
				}

				RaisePropertyChanged(() => PurchasePrice);
				RaisePropertyChanged(() => PurchasePriceDisplayText);
			}
		}

		public string PurchasePriceAsString
		{

			get => $"{PurchasePrice :C2}";

			set
			{
				if (string.IsNullOrEmpty(value))
				{
					PurchasePrice = null;
				}
				else
				{
					PurchasePrice = double.TryParse(value, out double result) ? result : (double?) null;
				}
			}
		}

		public string PurchasePriceDisplayText => string.IsNullOrWhiteSpace(PurchasePriceAsString)
			? Strings.Bracketed_AddPurchasePrice_Text
            : PurchasePriceAsString;

		public DateTime? PurchaseDate
		{
			get => purchaseDate.Equals(DateTime.MinValue) ? (DateTime?)null : purchaseDate;
			set
			{
				purchaseDate = value ?? DateTime.MinValue;
				RaisePropertyChanged(() => PurchaseDate);
				RaisePropertyChanged(() => DisplayPurchaseDate);
			}
		}

		public string DisplayPurchaseDate => purchaseDate.Equals(DateTime.MinValue) ? null : purchaseDate.ToShortDateString();

		public double? CurrentValue
		{
			get => currentValue.HasValue ? currentValue.ValueOrFailure() : (double?) null;
			set
			{
				if (value != null)
				{
					double castValue = (double) value;
					currentValue = castValue.SomeNotNull();
				}
				else
				{
					currentValue = Option.None<double>();
				}

				RaisePropertyChanged(() => CurrentValue);
				RaisePropertyChanged(() => CurrentValueDisplayText);
			}
		}

		public string CurrentValueAsString
		{

			get => $"{CurrentValue:C2}";

			set
			{
				if (string.IsNullOrEmpty(value))
				{
					CurrentValue = null;
				}
				else
				{
					CurrentValue = double.TryParse(value, out double result) ? result : (double?) null;
				}
			}
		}

		public string CurrentValueDisplayText => string.IsNullOrWhiteSpace(CurrentValueAsString)
			? Strings.Bracketed_AddCurrentValue_Text
            : CurrentValueAsString;

		public DateTime? CurrentDate
		{
			get => currentDate.Equals(DateTime.MinValue) ? (DateTime?) null : currentDate;
			set
			{
				currentDate = value ?? DateTime.MinValue;
				RaisePropertyChanged(() => CurrentDate);
				RaisePropertyChanged(() => DisplayCurrentDate);
			}
		}

		public string DisplayCurrentDate => currentDate.Equals(DateTime.MinValue) ? null : currentDate.ToShortDateString();

		public void ChangeProperties(HistoryInfoViewModel otherHistoryInfoViewModel)
		{
			PurchasePrice = otherHistoryInfoViewModel.PurchasePrice;
			CurrentValue = otherHistoryInfoViewModel.CurrentValue;
			CurrentDate = otherHistoryInfoViewModel.CurrentDate;
			PurchaseDate = otherHistoryInfoViewModel.PurchaseDate;
		}

		public EquipmentHistoryInfo GetEquipmentHistoryInfo()
		{
			return new EquipmentHistoryInfo(PurchasePrice, purchaseDate, CurrentValue, currentDate);
		}
	}
}