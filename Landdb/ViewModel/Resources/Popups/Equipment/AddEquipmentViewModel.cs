﻿using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Equipment;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Resources.Equipment;
using Landdb.ViewModel.Resources.Popups.Equipment.Tabs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;

namespace Landdb.ViewModel.Resources.Popups.Equipment {
	public class AddEquipmentViewModel : ValidationViewModelBase {

		private readonly IClientEndpoint clientEndpoint;
		private readonly CropYearId cropYearId;
		private readonly Action<EquipmentListItemViewModel, EquipmentDetailsView> equipmentAdded;

		public AddEquipmentViewModel(IClientEndpoint clientEndpoint, EquipmentId equipmentId, CropYearId cropYearId,
			Action<EquipmentListItemViewModel, EquipmentDetailsView> onEquipmentAdded, StatusInfoSelectorItem lastUsedStatusInfoSelectorItem, 
			EquipmentType lastUsedEquipmenttype)
		{
			this.clientEndpoint = clientEndpoint;
			this.cropYearId = cropYearId;
			equipmentAdded = onEquipmentAdded;
			EquipmentId = equipmentId;
			BasicInfoViewModel = new BasicInfoViewModel(new EquipmentBasicInfo {EquipmentType = lastUsedEquipmenttype}, 
				GetDefaultEquipmentStatuses(lastUsedStatusInfoSelectorItem), string.Empty);
			HistoryInfoViewModel = new HistoryInfoViewModel(EquipmentHistoryInfo.GetDefaultEquipmentHistoryInfo());
			ServicesInfoCollectionViewModel = new ServicesInfoCollectionViewModel(clientEndpoint, cropYearId);
			ServicesInfoCollectionViewModel.BuildProductList();
			ServicesInfoCollectionViewModel.AddService();
			AddNewCommand = new RelayCommand(onAddNew);
			CancelCommand = new RelayCommand(CloseOverlay);
		}

		public EquipmentId EquipmentId { get; }
		public BasicInfoViewModel BasicInfoViewModel { get; set; }
		public HistoryInfoViewModel HistoryInfoViewModel { get; set; }
		public ServicesInfoCollectionViewModel ServicesInfoCollectionViewModel { get; set; }
		public int StartingTabIndex { get; set; } = 0;
		public ICommand AddNewCommand { get; }
		public ICommand CancelCommand { get; }

		private void onAddNew()
		{
			if (!BasicInfoViewModel.ValidateViewModel()) return;

			clientEndpoint.SendOne(new CreateEquipment(
				EquipmentId,
				clientEndpoint.GenerateNewMetadata(),
				cropYearId.Id,
				BasicInfoViewModel.GetEquipmentBasicInfo(),
				HistoryInfoViewModel.GetEquipmentHistoryInfo(),
				ServicesInfoCollectionViewModel.GetEquipmentServiceInfoArray(),
				BasicInfoViewModel.GetCurrentStatusInfoViewModel.GetEquipmentStatus(),
				BasicInfoViewModel.Notes
			));
			EquipmentListItem equipmentListItem = new EquipmentListItem(
				EquipmentId,
				BasicInfoViewModel.GetEquipmentBasicInfo(),
				BasicInfoViewModel.GetEquipmentStatuses(),
				cropYearId.Id
			);
			EquipmentDetailsView equipmentDetailsView = new EquipmentDetailsView
			{
				Id = EquipmentId,
				InitialCropYear = cropYearId.Id,
				BasicInfo = BasicInfoViewModel.GetEquipmentBasicInfo(),
				EquipmentServices = ServicesInfoCollectionViewModel.GetEquipmentServiceInfoArray().ToList(),
				EquipmentStatusList = new List<EquipmentStatus> { BasicInfoViewModel.GetCurrentStatusInfoViewModel.GetEquipmentStatus() },
				HistoryInfo = HistoryInfoViewModel.GetEquipmentHistoryInfo(),
				Notes = BasicInfoViewModel.Notes
			};
			equipmentAdded(new EquipmentListItemViewModel(clientEndpoint, cropYearId.Id, equipmentListItem), equipmentDetailsView);
			CloseOverlay();
		}

		private static IEnumerable<EquipmentStatus> GetDefaultEquipmentStatuses(StatusInfoSelectorItem lastUsedStatusInfoSelectorItem)
		{
			return lastUsedStatusInfoSelectorItem != null
				? new List<EquipmentStatus>
				{
					new EquipmentStatus(EquipmentStatus.GetDefaultEquipmentStatus().StatusId, lastUsedStatusInfoSelectorItem.EquipmentStatusType,
						EquipmentStatus.GetDefaultEquipmentStatus().StatusDate, lastUsedStatusInfoSelectorItem.IsActive)
				}
				: new List<EquipmentStatus>
				{
					EquipmentStatus.GetDefaultEquipmentStatus()
				};
		}

		private static void CloseOverlay() {
			Messenger.Default.Send(new HideOverlayMessage());
		}
	}
}