﻿using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Resources.Popups.Equipment.Tabs;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Input;

namespace Landdb.ViewModel.Resources.Popups.Equipment {
	public class EditStatusInfoCollectionViewModel {

		private readonly Action<EditStatusInfoCollectionViewModel> statusInfoChanged;

		public EditStatusInfoCollectionViewModel(IEnumerable<EquipmentStatus> currentStatusList, Action<EditStatusInfoCollectionViewModel> onStatusInfoChanged) {
			StatusInfoCollectionViewModel = new StatusInfoCollectionViewModel(currentStatusList);

			this.statusInfoChanged = onStatusInfoChanged;

			Complete = new RelayCommand(onComplete);
			Cancel = new RelayCommand(CloseOverlay);
		}

		public StatusInfoCollectionViewModel StatusInfoCollectionViewModel { get; }

		public ICommand Complete { get; }
		public ICommand Cancel { get; }

		private void onComplete() {
			statusInfoChanged(this);
			StatusInfoCollectionViewModel.RefreshStatuses();
			CloseOverlay();
		}

		private static void CloseOverlay() {
			Messenger.Default.Send(new HideOverlayMessage());
		}
	}
}