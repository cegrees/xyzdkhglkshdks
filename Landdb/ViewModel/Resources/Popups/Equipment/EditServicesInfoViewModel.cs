﻿using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Resources.Popups.Equipment.Tabs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;

namespace Landdb.ViewModel.Resources.Popups.Equipment {
	public class EditServicesInfoViewModel {

		private readonly Action<EditServicesInfoViewModel> detailsChanged;

		public EditServicesInfoViewModel(IClientEndpoint clientEndpoint, CropYearId cropYearId, IEnumerable<EquipmentServiceInfo> currentServiceInfoList, Action<EditServicesInfoViewModel> detailsChanged) {
			this.detailsChanged = detailsChanged;

			ServicesInfoCollectionViewModel = new ServicesInfoCollectionViewModel(
				clientEndpoint,
				cropYearId,
				currentServiceInfoList
			);

			Cancel = new RelayCommand(CloseOverlay);
			Complete = new RelayCommand(OnComplete);

			ServicesInfoCollectionViewModel.BuildProductList();
			AddBlankServiceToServices();
		}

		public ServicesInfoCollectionViewModel ServicesInfoCollectionViewModel { get; }

		public ICommand Cancel { get; }
		public ICommand Complete { get; }

		private void AddBlankServiceToServices() {
			if (!ServicesInfoCollectionViewModel.Services.Any()) {
				ServicesInfoCollectionViewModel.AddService();
			}
		}

		private void OnComplete() {
			detailsChanged(this);
			CloseOverlay();
		}

		private static void CloseOverlay() {
			Messenger.Default.Send(new HideOverlayMessage());
		}
	}
}