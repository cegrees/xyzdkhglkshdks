﻿using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Resources.Popups.Equipment.Tabs;
using System;
using System.Windows.Input;

namespace Landdb.ViewModel.Resources.Popups.Equipment {
	public class EditHistoryInfoViewModel {
		private readonly Action<EditHistoryInfoViewModel> detailsChanged;

		public EditHistoryInfoViewModel(EquipmentHistoryInfo currentHistoryInfo, Action<EditHistoryInfoViewModel> detailsChanged) {
			this.detailsChanged = detailsChanged;

			HistoryInfoViewModel = new HistoryInfoViewModel(currentHistoryInfo);

			Cancel = new RelayCommand(CloseOverlay);
			Complete = new RelayCommand(onComplete);
		}

		public HistoryInfoViewModel HistoryInfoViewModel { get; }

		public ICommand Cancel { get; }
		public ICommand Complete { get; }

		private void onComplete() {
			detailsChanged(this);
			CloseOverlay();
		}

		private static void CloseOverlay() {
			Messenger.Default.Send(new HideOverlayMessage());
		}
	}
}