﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Resources.Popups.Equipment.Tabs;

namespace Landdb.ViewModel.Resources.Popups.Equipment
{
	public class RemoveEquipmentViewModel
	{
		public List<StatusInfoViewModel> RemovalStatuses { get; } = StatusInfoViewModel.InactiveStatusInfoViewModelList;
		private readonly Action<StatusInfoViewModel> ChangeEquipmentStatusTo;
		public StatusInfoViewModel RemovalSelectedStatusInfoViewModel { get; set; }
		public DateTime RemovalSelectedStatusDate { get; set; }
		public ICommand Complete { get; }
		public ICommand Delete { get; }

		public RemoveEquipmentViewModel(ICommand deleteSelectedListItem, Action<StatusInfoViewModel> changeEquipmentStatusTo)
		{
			ChangeEquipmentStatusTo = changeEquipmentStatusTo;
			RemovalSelectedStatusDate = DateTime.Today;
			Complete = new RelayCommand(CompleteStatusChange);
			RemovalSelectedStatusInfoViewModel = RemovalStatuses.FirstOrDefault();
			Delete = deleteSelectedListItem;
		}

		private void CompleteStatusChange()
		{
			RemovalSelectedStatusInfoViewModel.StatusDate = RemovalSelectedStatusDate;
			ChangeEquipmentStatusTo(RemovalSelectedStatusInfoViewModel);
			CloseOverlay();
		}

		public ICommand Cancel => new RelayCommand(CloseOverlay);

		private static void CloseOverlay()
		{
			Messenger.Default.Send(new HideOverlayMessage());
		}
	}
}