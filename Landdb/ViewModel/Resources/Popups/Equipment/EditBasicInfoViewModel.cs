﻿using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Resources.Popups.Equipment.Tabs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;

namespace Landdb.ViewModel.Resources.Popups.Equipment {
	public class EditBasicInfoViewModel : ValidationViewModelBase
	{
		private readonly Action<EditBasicInfoViewModel> detailsChanged;

		public EditBasicInfoViewModel(EquipmentBasicInfo currentBasicInfo, IEnumerable<EquipmentStatus> statusEnumerable, string notes, 
			Action<EditBasicInfoViewModel> detailsChanged)
		{
			this.detailsChanged = detailsChanged;
			BasicInfoViewModel = new BasicInfoViewModel(currentBasicInfo, statusEnumerable.ToList(), notes);

			Complete = new RelayCommand(onComplete);
			Cancel = new RelayCommand(CloseOverlay);
		}

		public BasicInfoViewModel BasicInfoViewModel { get; }
		public ICommand Cancel { get; }
		public ICommand Complete { get; }

		private void onComplete()
		{
			if (!BasicInfoViewModel.ValidateViewModel()) return;
			CloseOverlay();
			detailsChanged(this);
		}

		private static void CloseOverlay()
		{
			Messenger.Default.Send(new HideOverlayMessage());
		}
	}
}