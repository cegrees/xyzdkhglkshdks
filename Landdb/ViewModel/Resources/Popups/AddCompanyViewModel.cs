﻿using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Shared;
using NLog;
using System;
using System.ComponentModel.DataAnnotations;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Resources.Popups {
	public class AddCompanyViewModel : ValidationViewModelBase {

		private readonly IClientEndpoint clientEndpoint;
		private readonly Dispatcher dispatcher;

		private readonly Logger log = LogManager.GetCurrentClassLogger();

		private readonly int currentCropYear;

		private readonly Action<CompanyListItemViewModel> companyCreated;

		public AddCompanyViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, CompanyId companyId, int currentCropYear, Action<CompanyListItemViewModel> onCompanyCreated) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			this.currentCropYear = currentCropYear;

			CompanyId = companyId;

			StreetAddress = new StreetAddressViewModel(clientEndpoint, dispatcher);

			companyCreated += onCompanyCreated;

			CreateNewCommand = new RelayCommand(onCreateNew);
			CancelCommand = new RelayCommand(onCancel);
		}

		public ICommand CreateNewCommand { get; }
		public ICommand CancelCommand { get; }

		public CompanyId CompanyId { get; }

		public StreetAddressViewModel StreetAddress { get; }

		private string _name;
        [Required(AllowEmptyStrings = false, ErrorMessageResourceName = "NameIsRequired_Text", ErrorMessageResourceType = typeof(Strings))]
        public string Name {
			get { return _name; }
			set {
				_name = value;
				ValidateAndRaisePropertyChanged(() => Name);
			}
		}

		private string _primaryPhoneNumber;
		public string PrimaryPhoneNumber {
			get { return _primaryPhoneNumber; }
			set {
				_primaryPhoneNumber = value;
				RaisePropertyChanged(() => PrimaryPhoneNumber);
			}
		}

		private string _primaryEmailAddress;
		public string PrimaryEmailAddress {
			get { return _primaryEmailAddress; }
			set {
				_primaryEmailAddress = value;
				RaisePropertyChanged(() => PrimaryEmailAddress);
			}
		}

		private void onCreateNew() {
			if (!ValidateViewModel()) { return; }

			PhoneNumber primaryPhone = string.IsNullOrWhiteSpace(PrimaryPhoneNumber) ? null : new PhoneNumber("Primary", PrimaryPhoneNumber);
			Email primaryEmail = string.IsNullOrWhiteSpace(PrimaryEmailAddress) ? null : new Email(PrimaryEmailAddress, Name);

			var cmd = new CreateCompany(CompanyId, clientEndpoint.GenerateNewMetadata(), Name, StreetAddress.GetValueObject(), primaryPhone, primaryEmail, currentCropYear);
			clientEndpoint.SendOne(cmd);

			companyCreated(new CompanyListItemViewModel(
				CompanyId,
				Name,
				primaryPhone != null ? primaryPhone.GetFormattedString() : null,
				PrimaryEmailAddress
			));

			//Now close and return to parent...
			Messenger.Default.Send(new HideOverlayMessage());
		}

		private void onCancel() {
			Messenger.Default.Send(new HideOverlayMessage());
		}
	}
}