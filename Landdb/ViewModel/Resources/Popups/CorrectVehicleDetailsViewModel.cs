﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using System;
using System.Windows.Input;
using System.Windows.Threading;

namespace Landdb.ViewModel.Resources.Popups {
	public class CorrectVehicleDetailsViewModel : ViewModelBase {

		private readonly IClientEndpoint clientEndpoint;
		private readonly Dispatcher dispatcher;

		private readonly string originalMake;
		private readonly string originalModel;
		private readonly string originalModelYear;

		private readonly Action<CorrectVehicleDetailsViewModel> vehicleDetailsCorrected;

		public CorrectVehicleDetailsViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, string originalMake, string originalModel, string originalModelYear, Action<CorrectVehicleDetailsViewModel> onVehicleDetailsCorrected) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			this.originalMake = originalMake;
			this.originalModel = originalModel;
			this.originalModelYear = originalModelYear;

			Make = originalMake;
			Model = originalModel;
			ModelYear = originalModelYear;

			vehicleDetailsCorrected += onVehicleDetailsCorrected;

			CompleteCommand = new RelayCommand(onComplete);
			CancelCommand = new RelayCommand(onCancel);
		}

		public ICommand CompleteCommand { get; }
		public ICommand CancelCommand { get; }

		private string _make;
		public string Make {
			get { return _make != null ? _make.Trim() : null; }
			set {
				_make = value;
				RaisePropertyChanged(() => Make);
			}
		}

		private string _model;
		public string Model {
			get { return _model != null ? _model.Trim() : null; }
			set {
				_model = value;
				RaisePropertyChanged(() => Model);
			}
		}

		private string _modelYear;
		public string ModelYear {
			get { return _modelYear != null ? _modelYear.Trim() : null; }
			set {
				_modelYear = value;
				RaisePropertyChanged(() => ModelYear);
			}
		}

		public bool HasChanges => 
			Make != originalMake
				|| Model != originalModel
				|| ModelYear != originalModelYear;

		private void onComplete() {
			vehicleDetailsCorrected(this);
		}

		private void onCancel() {
			vehicleDetailsCorrected(null);
		}
	}
}