﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Resources.Popups {
    public class EditUserCreatedProductNameViewModel : ViewModelBase {
        IClientEndpoint clientEndpoint;
        ProductId productId;
        string existingName;
        string existingManufacturer;
        Action<string, string> onClose;

        string name;
        string manufacturer;
        
        public EditUserCreatedProductNameViewModel(IClientEndpoint clientEndpoint, ProductId productId, string existingName, string existingManufacturer, Action<string, string> onClose) {
            this.clientEndpoint = clientEndpoint;
            this.productId = productId;
            this.existingName = existingName;
            this.existingManufacturer = existingManufacturer;
            this.name = existingName;
            this.manufacturer = existingManufacturer;
            this.onClose = onClose;

            CancelCommand = new RelayCommand(Close);
            SaveCommand = new RelayCommand(SaveChanges);
        }

        public RelayCommand SaveCommand { get; private set; }
        public RelayCommand CancelCommand { get; private set; }

        public string Name {
            get { return name; }
            set {
                name = value;
                RaisePropertyChanged("Name");
            }
        }

        public string Manufacturer {
            get { return manufacturer; }
            set {
                manufacturer = value;
                RaisePropertyChanged("Manufacturer");
            }
        }

        void Close() {
            onClose(existingName, existingManufacturer);
        }
        
        void SaveChanges() {
            if (!string.IsNullOrWhiteSpace(Name) && Name != existingName) {
                // change name
                var cmd = new RenameUserCreatedProduct(productId, clientEndpoint.GenerateNewMetadata(), Name);
                clientEndpoint.SendOne(cmd);
            }

            if (!string.IsNullOrWhiteSpace(Manufacturer) && Manufacturer != existingManufacturer) {
                // change manu
                var cmd = new ChangeUserCreatedProductManufacturer(productId, clientEndpoint.GenerateNewMetadata(), Manufacturer);
                clientEndpoint.SendOne(cmd);
            }

            onClose(Name, Manufacturer);
        }
    }
}
