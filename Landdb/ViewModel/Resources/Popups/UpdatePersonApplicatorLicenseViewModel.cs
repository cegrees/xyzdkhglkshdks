﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using System;
using System.Windows.Input;
using System.Windows.Threading;

namespace Landdb.ViewModel.Resources.Popups {
	public class UpdatePersonApplicatorLicenseViewModel : ViewModelBase {

		private readonly IClientEndpoint clientEndpoint;
		private readonly Dispatcher dispatcher;

		private readonly string originalLicenseNumber;
		private readonly DateTime originalLicenseExpiration;

		private readonly Action<UpdatePersonApplicatorLicenseViewModel> licenseUpdated;
        bool enableSaveButton = false;
        private DateTime ValidateLicenseDate;
        private DateTime licenseExpiration;

        public UpdatePersonApplicatorLicenseViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, string originalLicenseNumber, DateTime originalLicenseExpiration, Action<UpdatePersonApplicatorLicenseViewModel> onLicenseUpdated) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			this.originalLicenseNumber = originalLicenseNumber;
			this.originalLicenseExpiration = originalLicenseExpiration;

			LicenseNumber = originalLicenseNumber;
			LicenseExpiration = originalLicenseExpiration;
            ValidateLicenseDate = new DateTime(2000, 1, 1);
            EnableSaveButton = CheckValidity();

            CompleteCommand = new RelayCommand(onComplete);
			CancelCommand = new RelayCommand(onCancel);

			licenseUpdated += onLicenseUpdated;
		}

		public ICommand CompleteCommand { get; }
		public ICommand CancelCommand { get; }

		public string LicenseNumber { get; set; }

        //public DateTime LicenseExpiration { get; set; }


        public DateTime LicenseExpiration {
            get { return licenseExpiration; }
            set {
                licenseExpiration = value;
                EnableSaveButton = CheckValidity();
                RaisePropertyChanged("LicenseExpiration");
            }
        }
        public bool EnableSaveButton {
            get { return enableSaveButton; }
            set {
                enableSaveButton = value;
                RaisePropertyChanged("EnableSaveButton");
            }
        }
        public bool HasChanges => LicenseNumber != originalLicenseNumber || LicenseExpiration != originalLicenseExpiration;

		private void onComplete() {
			licenseUpdated(this);
		}

		private void onCancel() {
			licenseUpdated(null);
		}

        private bool CheckValidity() {
            if (LicenseExpiration == null) {
                return false;
            }
            if (LicenseExpiration < ValidateLicenseDate) {
                return false;
            }
            return true;
        }
    }
}
