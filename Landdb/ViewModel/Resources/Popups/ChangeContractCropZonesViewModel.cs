﻿using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using Landdb.ViewModel.Fields;
using Landdb.ViewModel.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using System.Windows.Threading;

namespace Landdb.ViewModel.Resources.Contract.Popups {
	public class ChangeContractCropZonesViewModel : BaseFieldSelectionViewModel {

		readonly ContractId contractId;
		readonly List<CropZoneDetails> originalCropZones;

		readonly Action<ChangeContractCropZonesViewModel> cropZonesChanged;

		private Dictionary<CropId, int> cropDictionary = new Dictionary<CropId, int>();
		private CropId selectedCrop;

		public ChangeContractCropZonesViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, int currentCropYear, ContractId contractId, IEnumerable<CropZoneDetails> originalCropZones, Action<ChangeContractCropZonesViewModel> onCropZonesChanged)
			: base(clientEndpoint, dispatcher, currentCropYear) {

            this.Enable = false;
			this.contractId = contractId;
			this.originalCropZones = new List<CropZoneDetails>(originalCropZones);

			var initialCzIds = originalCropZones.Select(x => x.Id);

			if (RootTreeItemModels.Any()) {
				if (RootTreeItemModels.First() is GrowerTreeItemViewModel) {
					var toCheck = from farm in RootTreeItemModels.First().Children
								  from field in farm.Children
								  from cz in field.Children
								  where initialCzIds.Contains(cz.Id)
								  select cz;
					toCheck.ForEach(x => x.IsChecked = true);
				} else {
					var toCheck = from crop in RootTreeItemModels.First().Children
								  from farm in crop.Children
								  from field in farm.Children
								  from cz in field.Children
								  where initialCzIds.Contains(cz.Id)
								  select cz;
					toCheck.ForEach(x => x.IsChecked = true);
				}
			}

			TotalArea = TotalArea.Unit.GetMeasure(0);
			foreach (var selectedCz in SelectedCropZones) {
				var treeCz = originalCropZones.FirstOrDefault(x => x.Id == selectedCz.Id);
				selectedCz.SelectedArea = treeCz.Area;
				TotalArea = TotalArea.Unit.GetMeasure(TotalArea.Value + selectedCz.SelectedArea.Value);
			}

			this.cropZonesChanged += onCropZonesChanged;

			SaveChangesCommand = new RelayCommand(onSaveChanges);
			CancelChangesCommand = new RelayCommand(onCancelChanges);
		}

		public ICommand SaveChangesCommand { get; private set; }
		public ICommand CancelChangesCommand { get; private set; }

        public bool Enable { get; set; }

        public List<CropZoneArea> AddedCropZones {
			get {
				var added = from a in SelectedCropZones
							where !originalCropZones.Exists(x => x.Id == a.Id)
							select new CropZoneArea(a.Id, a.SelectedArea.Value, a.SelectedArea.Unit.Name);

				return added.ToList();
			}
		}

		public List<CropZoneArea> ChangedCropZones {
			get {
				var changed = from c in SelectedCropZones
							  where originalCropZones.Exists(x => x.Id == c.Id && (x.Area.Unit != c.SelectedArea.Unit || Math.Abs(x.Area.Value - c.SelectedArea.Value) > 0.0001))
							  select new CropZoneArea(c.Id, c.SelectedArea.Value, c.SelectedArea.Unit.Name);

				return changed.ToList();
			}
		}

		public List<CropZoneId> RemovedCropZones {
			get {
				var removed = from r in originalCropZones
							  where !SelectedCropZones.Where(x => x.Id == r.Id).Any()
							  select r.Id;

				return removed.ToList();
			}
		}

		public bool HasChanges {
			get { return AddedCropZones.Any() || ChangedCropZones.Any() || RemovedCropZones.Any(); }
		}

		protected override void OnFieldCheckedChanged(CropZoneTreeItemViewModel treeItem) {
			if (selectedCrop == null) { selectedCrop = treeItem.CropId; }
			if (!cropDictionary.ContainsKey(treeItem.CropId)) { cropDictionary.Add(treeItem.CropId, 0); }

			if (treeItem.IsChecked.HasValue && treeItem.IsChecked.Value) { // cz is checked
				cropDictionary[treeItem.CropId]++;
			} else { // cz is unchecked
				if (--cropDictionary[treeItem.CropId] == 0) { cropDictionary.Remove(treeItem.CropId); }
			}

			var otherCropsAreSelected = cropDictionary.Keys.Where(x => x.Id != selectedCrop.Id).Any();

			//if (otherCropsAreSelected) {
			//	ErrorMessage = "Invalid field selections. Please ensure that all selected fields share the same crop as the original load ticket.";
			//} else {
			//	ErrorMessage = null;
			//}
		}

		private void onSaveChanges() {
			if (!HasErrors) {
                UsePercentCoverage = true;
                cropZonesChanged(this);
			}
		}

		private void onCancelChanges() {
            UsePercentCoverage = true;
            cropZonesChanged(null);
		}
	}
}