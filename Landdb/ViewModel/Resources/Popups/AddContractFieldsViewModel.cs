﻿using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Contract;
using Landdb.ViewModel.Fields;
using Landdb.ViewModel.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Landdb.ViewModel.Resources.Popups
{
    public class AddContractFieldsViewModel : BaseFieldSelectionViewModel
    {
        public AddContractFieldsViewModel(IClientEndpoint end, Dispatcher dis, int cropyear)
            : base(end, dis, cropyear)
        {
            Contracts = new Dictionary<ContractId, ContractDetails>();
        }

        public Dictionary<ContractId, ContractDetails> Contracts { get; private set; }

        protected override void OnFieldCheckedChanged(Fields.CropZoneTreeItemViewModel treeItem)
        {
            if (treeItem is CropZoneTreeItemViewModel)
            {
                if (treeItem.IsChecked.HasValue && treeItem.IsChecked.Value)
                {
                    if (SelectedCropZones.Count == 1)
                    {
                        var mapData = SelectedCropZones.First().MapData;
                        if (!string.IsNullOrWhiteSpace(mapData))
                        {
                            var shape = ThinkGeo.MapSuite.Core.PolygonShape.CreateShapeFromWellKnownData(mapData);
                            var center = shape.GetCenterPoint();
                            //conditionsModel.GeoLookupPoint = center; // string.Format("{0},{1}", center.Y.ToString("n1"), center.X.ToString("n1"));
                        }
                    }

                    // Get contract ID and add it if it doesn't exist
                    var contractYears = clientEndpoint.GetView<CropZoneContracts>(treeItem.Id).GetValue(new CropZoneContracts());
                    if (contractYears.Contracts.ContainsKey(cropYear))
                    {
                        var contracts = contractYears.Contracts[cropYear];
                        if (contracts.Any())
                        {
                            if (!Contracts.ContainsKey(contracts.First()))
                            {
                                var c = clientEndpoint.GetView<ContractDetails>(contracts.First());
                                if (c.HasValue)
                                {
                                    Contracts.Add(contracts.First(), c.Value);
                                    RaisePropertyChanged("Contracts");
                                }
                            }
                        }
                    }
                }

                if (treeItem.IsChecked.HasValue && !treeItem.IsChecked.Value)
                {
                    var found = SelectedCropZones.Where(x => x.Id == (CropZoneId)treeItem.Id).ToList();
                    found.ForEach(x =>
                    {

                        SelectedCropZones.Remove(x);
                        TotalArea -= x.SelectedArea;

                        // Get contract ID and remove it if it exists
                        var contractYears = clientEndpoint.GetView<CropZoneContracts>(x.Id).GetValue(new CropZoneContracts());
                        if (contractYears.Contracts.ContainsKey(cropYear))
                        {
                            var contracts = contractYears.Contracts[cropYear];
                            if (contracts.Any())
                            {
                                if (Contracts.ContainsKey(contracts.First()))
                                {
                                    Contracts.Remove(contracts.First());
                                    RaisePropertyChanged("Contracts");
                                }
                            }
                        }

                    });
                }

            }
        }
    }
}
