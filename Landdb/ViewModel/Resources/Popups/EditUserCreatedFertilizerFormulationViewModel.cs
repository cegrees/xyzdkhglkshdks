﻿using AgC.UnitConversion.Density;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure;
using Landdb.Resources;
using System;
using Landdb.Client.Localization;

namespace Landdb.ViewModel.Resources.Popups {
    public class EditUserCreatedFertilizerFormulationViewModel : ViewModelBase {
        private readonly IClientEndpoint _clientEndpoint;
        private readonly ProductId _productId;
        private readonly FertilizerFormulation _existingFormulation;
        private readonly Action<FertilizerFormulation> _onClose;
        private readonly decimal? _previousDensity;

        private FertilizerFormulation _formulation;

        public EditUserCreatedFertilizerFormulationViewModel(IClientEndpoint clientEndpoint, ProductId productId, FertilizerFormulation existingFormulation, decimal? densityAsSpecificGravity, Action<FertilizerFormulation> onClose) {
            _clientEndpoint = clientEndpoint;
            _productId = productId;
            _existingFormulation = existingFormulation == null ? new FertilizerFormulation() : existingFormulation;
            _formulation = existingFormulation == null ? new FertilizerFormulation() : existingFormulation.Clone();
            _onClose = onClose;
            _previousDensity = densityAsSpecificGravity;
            _specificGravity = densityAsSpecificGravity;
            CancelCommand = new RelayCommand(Close);
            SaveCommand = new RelayCommand(SaveChanges);
        }

        public RelayCommand SaveCommand { get; private set; }
        public RelayCommand CancelCommand { get; private set; }

        public FertilizerFormulation Formulation {
            get => _formulation;
            set {
                _formulation = value;
                RaisePropertyChanged("Formulation");
            }
        }

        public decimal? Density {
            get {
                double density = SpecificGravity.HasValue ?
                        (DataSourceCultureInformation.UsesImperialUnits ?
                            AgC.UnitConversion.Density.SpecificGravity.Self.GetMeasure((double) SpecificGravity.Value).CreateAs(PoundsPerGallon.Self).Value :
                            (double) SpecificGravity.Value) : 
                        0;

                //to correct for the rounding error that occurs during conversion
                if ( Math.Abs(Math.Round(density, 2) - density) < .01) {
                    density = Math.Round(density, 0);
                }

                return (decimal?) density;
            }
            set => SpecificGravity = value;
        }

        private decimal? _specificGravity;
        private decimal? SpecificGravity {
            get => _specificGravity;
            set => _specificGravity = value.HasValue? (decimal?)(DataSourceCultureInformation.UsesImperialUnits ? 
                PoundsPerGallon.Self.GetMeasure((double)value.Value).GetValueAs(AgC.UnitConversion.Density.SpecificGravity.Self) :
                (double)value.Value) : null;
        }

        public static string DensityText =>
            DataSourceCultureInformation.UsesImperialUnits ?
                $"{AgC.UnitConversion.MassAndWeight.Pound.Self.AbbreviatedDisplay} / {AgC.UnitConversion.Volume.Gallon.Self.AbbreviatedDisplay}" :
                $"{AgC.UnitConversion.MassAndWeight.Kilogram.Self.AbbreviatedDisplay} / {AgC.UnitConversion.Volume.Liter.Self.AbbreviatedDisplay}";

        private void Close() {
            _onClose(_existingFormulation);
        }

        private void SaveChanges() {
            if (Formulation != null && Formulation != _existingFormulation) {
                // change name
                var cmd = new ChangeUserCreatedFertilizerFormulation(_productId, _clientEndpoint.GenerateNewMetadata(), Formulation);
                _clientEndpoint.SendOne(cmd);
            }

            if (_previousDensity != Density) {
                //TO DO :: UPDATE USER CREATED PRODUCT DENSITY
                var sg = SpecificGravity;
                decimal? sgVal = sg > 0 ? (decimal?) sg : null;
                var densityCmd = new ChangeUserCreatedFertilizerDensity(_productId, _clientEndpoint.GenerateNewMetadata(), sgVal);
                _clientEndpoint.SendOne(densityCmd);
            }

            _onClose(Formulation);
        }
    }
}