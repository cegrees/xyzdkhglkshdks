﻿using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Shared;
using NLog;
using System;
using System.ComponentModel.DataAnnotations;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Resources.Popups {
	public class AddPersonViewModel : ValidationViewModelBase {

		private readonly IClientEndpoint clientEndpoint;
		private readonly Dispatcher dispatcher;

		private readonly int currentCropYear;

		private readonly Logger log = LogManager.GetCurrentClassLogger();

		private readonly Action<PersonListItemViewModel> personCreated;

		public AddPersonViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, PersonId personId, int cropYear, Action<PersonListItemViewModel> onPersonCreated) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			currentCropYear = cropYear;

			PersonId = personId;

			StreetAddress = new StreetAddressViewModel(clientEndpoint, dispatcher);

			personCreated += onPersonCreated;

			CreateNewCommand = new RelayCommand(onCreateNew);
			CancelCommand = new RelayCommand(onCancel);
		}

		public ICommand CreateNewCommand { get; }
		public ICommand CancelCommand { get; }

		public PersonId PersonId { get; }

		public StreetAddressViewModel StreetAddress { get; }

		private string _name = string.Empty;
        [Required(AllowEmptyStrings = false, ErrorMessageResourceName = "NameIsRequired_Text", ErrorMessageResourceType = typeof(Strings))]
        public string Name {
			get { return _name; }
			set {
				_name = value;
				ValidateAndRaisePropertyChanged(() => Name);
			}
		}

		private string _primaryPhoneNumber = string.Empty;
		public string PrimaryPhoneNumber {
			get { return _primaryPhoneNumber; }
			set {
				_primaryPhoneNumber = value;
				ValidateAndRaisePropertyChanged(() => PrimaryPhoneNumber);
			}
		}

		private string _emailAddress = string.Empty;
		public string EmailAddress {
			get { return _emailAddress; }
			set {
				_emailAddress = value;
				ValidateAndRaisePropertyChanged(() => EmailAddress);
			}
		}

		private void onCreateNew() {
			if (!ValidateViewModel()) { return; }

			var primaryPhone = string.IsNullOrWhiteSpace(PrimaryPhoneNumber) ? null : new PhoneNumber("Primary", PrimaryPhoneNumber);
			var primaryEmail = string.IsNullOrWhiteSpace(EmailAddress) ? null : new Email(EmailAddress, Name);

			// TODO:  Accept CompanyId from the UI at some point in the near future...
			var cmd = new CreatePerson(PersonId, clientEndpoint.GenerateNewMetadata(), Name, StreetAddress.GetValueObject(), primaryPhone, primaryEmail, currentCropYear, null);
			clientEndpoint.SendOne(cmd);

			// send the newly-created list item back to the caller
			personCreated(new PersonListItemViewModel(
				PersonId,
				Name,
				primaryPhone != null ? primaryPhone.GetFormattedString() : null,
				primaryEmail != null ? EmailAddress : null
			));

			// Now close and return to parent...
			Messenger.Default.Send(new HideOverlayMessage());
		}

		private void onCancel() {
			Messenger.Default.Send(new HideOverlayMessage());
		}
	}
}