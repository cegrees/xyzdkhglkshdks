﻿using AgC.UnitConversion;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Contract;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Domain.ReadModels.Shared;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Domain.ReadModels.YieldLocation;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ValueObjects;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Resources.Contract.Rent {
	public class RentContractDetailsViewModel : ContractDetailsBaseViewModel {

		public RentContractDetailsViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, DataSourceId currentDataSourceId, int currentCropYear, RentContractDetailsView contractDetails)
			: base(clientEndpoint, dispatcher, currentCropYear, contractDetails.Id, contractDetails.Name, contractDetails.ContractType, contractDetails.CropYear, contractDetails.IncludedShareInfo, contractDetails.CropZones, contractDetails.Notes) {

			buildDetailsFromReadModel(contractDetails);

			UpdateLandOwnerInfoCommand = new RelayCommand(onUpdateLandOwnerInfo);
			UpdateInventoryInfoCommand = new RelayCommand(onUpdateInventoryInfo);
			ChangeContractStartDateCommand = new RelayCommand(onChangeContractStartDate);
			AddFlexInfoCommand = new RelayCommand(onAddFlexInfo);
			RemoveFlexInfoCommand = new RelayCommand(onRemoveFlexInfo);
			UpdateActualYieldCommand = new RelayCommand(onUpdateActualYield);
			UpdateFlatCropPriceCommand = new RelayCommand(onUpdateFlatCropPrice);
			UpdateFlexPercentageCommand = new RelayCommand(onUpdateFlexPercentage);
		}

		#region Properties
		public ICommand UpdateLandOwnerInfoCommand { get; }
		public ICommand UpdateInventoryInfoCommand { get; }
		public ICommand ChangeContractStartDateCommand { get; }
		public ICommand AddFlexInfoCommand { get; }
		public ICommand RemoveFlexInfoCommand { get; }
		public ICommand UpdateActualYieldCommand { get; }
		public ICommand UpdateFlatCropPriceCommand { get; }
		public ICommand UpdateFlexPercentageCommand { get; }

		public ObservableCollection<FlexLevelListItemViewModel> FlexLevelList { get; private set; }
		public List<ProductException> ProductExceptions { get; private set; }

		public DateTime StartDate { get; private set; }
		public double PerAcre { get; private set; }
		public TimeSpan? Duration { get; private set; }
		public DateTime RentDue { get; private set; }
		public LandOwner LandOwner { get; private set; }
		public double ActualYield { get; private set; }
		public double FlatCropPrice { get; private set; }
		public double FlexPercentage { get; private set; }
		public RentContractFlexPercentageSources FlexPercentageSource { get; private set; }

		public IEnumerable<IncludedSaleInfoViewModel> SaleInfo { get; private set; }
		public IncludedInventoryInfoViewModel InventoryInfo { get; private set; }

		public FlexLevelListItemViewModel SelectedFlexLevel { get; set; }

		public string FlexPercentageSourceDisplay {
			get {
				var desc = string.Empty;

				switch (FlexPercentageSource) {
					case RentContractFlexPercentageSources.TotalRevenue:
						desc = Strings.OfTotalRevenue_Text;
						break;
					case RentContractFlexPercentageSources.ProductionRevenue:
						desc = Strings.OfProductionRevenue_Text;
						break;
					case RentContractFlexPercentageSources.TotalQuantityAtFlatPrice:
						desc = Strings.OfTotalQuantityAtFlatCropPrice_Text;
						break;
					case RentContractFlexPercentageSources.None:
					default:
						return $"[{Strings.SetFlexPercentage_Text}]";
				}

				return desc.ToLower();
			}
		}

		public bool HasFlexPercentage => FlexPercentage != 0 && FlexPercentageSource != RentContractFlexPercentageSources.None;
		#endregion

		#region Change Handlers
		private void onChangeContractStartDate() {
			if (contractId != null && !IsReadOnly) {
				var vmDisplayMode = Shared.Popups.ChangeEntityDateTimeViewModel.EntityDateTimeDisplayMode.DateOnly;

				DialogFactory.ShowDateTimeChangeDialog(StartDate, vmDisplayMode, newStartDate => {
					clientEndpoint.SendOne(new UpdateContractStartDateInfo(
						contractId, 
						clientEndpoint.GenerateNewMetadata(), 
						newStartDate.ToUniversalTime()
					));

					StartDate = newStartDate;
					RaisePropertyChanged(() => StartDate);
				});
			}
		}

		private void onAddFlexInfo() {
			if (contractId != null && !IsReadOnly) {
				CropId cropId = null;
				if (CropZones != null && CropZones.Any()) {
					var firstCzId = CropZones.Select(x => x.Id).First();
					var czDetails = clientEndpoint.GetView<CropZoneDetailsView>(firstCzId).GetValue(new CropZoneDetailsView());
					cropId = czDetails.CropId;
				}

				var vm = new AddFlexInfoViewModel(clientEndpoint, dispatcher, contractId, cropId, newFlexLevel => {
					if (newFlexLevel != null) {
						clientEndpoint.SendOne(new AddContractFlexLevel(
							contractId,
							clientEndpoint.GenerateNewMetadata(), 
							newFlexLevel
						));

						FlexLevelList.Add(new FlexLevelListItemViewModel(newFlexLevel));
					}

					Messenger.Default.Send(new HidePopupMessage());
				});

				Messenger.Default.Send(new ShowPopupMessage() {
					ScreenDescriptor = new ScreenDescriptor(typeof(Views.Resources.Contract.Rent.AddFlexInfoView), "flexinfo", vm)
				});
			}
		}

		private void onRemoveFlexInfo() {
			if (contractId != null && SelectedFlexLevel != null && !IsReadOnly) {
				var flex = new FlexLevel(contractId, SelectedFlexLevel.Level, SelectedFlexLevel.Unit, SelectedFlexLevel.Bonus);
				clientEndpoint.SendOne(new RemoveContractFlexLevel(
					contractId,
					clientEndpoint.GenerateNewMetadata(),
					flex
				));

				FlexLevelList.Remove(SelectedFlexLevel);
			}
		}

		private void onUpdateActualYield() {
			if (contractId != null && !IsReadOnly) {
				DialogFactory.ShowNumericChangeDialog(Strings.ActualYield_Text, Convert.ToDecimal(ActualYield), "N2", null, newValue => {
					var converted = Convert.ToDouble(newValue);

					clientEndpoint.SendOne(new UpdateActualYield(
						contractId, 
						clientEndpoint.GenerateNewMetadata(), 
						converted
					));

					ActualYield = converted;
					base.RaisePropertyChanged(() => ActualYield);
				});
			}
		}

		private void onUpdateFlatCropPrice() {
			if (contractId != null && !IsReadOnly) {
				DialogFactory.ShowNumericChangeDialog(Strings.FlatCropPrice_Text, Convert.ToDecimal(FlatCropPrice), "C2", null, newValue => {
					var newPrice = Convert.ToDouble(newValue);
					clientEndpoint.SendOne(new UpdateSetPriceInfo(
						contractId, 
						clientEndpoint.GenerateNewMetadata(), 
						newPrice
					));

					FlatCropPrice = newPrice;
					RaisePropertyChanged(() => FlatCropPrice);
				});
			}
		}

		private void onUpdateFlexPercentage() {
            if (!IsReadOnly) {
                var vm = new UpdateFlexPercentageViewModel(clientEndpoint, dispatcher, FlexPercentage, FlexPercentageSource, changeVm => {
                    if (changeVm != null && changeVm.HasChanges) {
                        clientEndpoint.SendOne(new UpdateFlexPercentage(
                            contractId,
                            clientEndpoint.GenerateNewMetadata(),
                            changeVm.FlexPercentage,
                            changeVm.SelectedFlexPercentageSource
                        ));

                        FlexPercentage = changeVm.FlexPercentage;
                        FlexPercentageSource = changeVm.SelectedFlexPercentageSource;

                        base.RaisePropertyChanged(() => FlexPercentage);
                        base.RaisePropertyChanged(() => FlexPercentageSource);
                        base.RaisePropertyChanged(() => HasFlexPercentage);
                        base.RaisePropertyChanged(() => FlexPercentageSourceDisplay);
                    }

                    Messenger.Default.Send(new HidePopupMessage());
                });

                Messenger.Default.Send(new ShowPopupMessage() {
                    ScreenDescriptor = new ScreenDescriptor(typeof(Views.Resources.Contract.RentContract.Popup.EditFlexPercentageView), "update flex percentage", vm)
                });
            }
		}

		private void onUpdateInventoryInfo() {
			if (contractId != null && !IsReadOnly) {
				var vm = new UpdateInventoryInfoViewModel(clientEndpoint, dispatcher, InventoryInfo.GetIncludedInventoryInfoObject(), changeVm => {
					if (changeVm != null && changeVm.HasChanges) {
						var newInventoryInfo = changeVm.InventoryInfo.GetIncludedInventoryInfoObject();
						
						clientEndpoint.SendOne(new UpdateContractInventoryInfo(
							contractId,
							clientEndpoint.GenerateNewMetadata(),
							newInventoryInfo.CropProtectionMargin,
							newInventoryInfo.IsCropProtectionFromInventory,
							newInventoryInfo.FertilizerMargin,
							newInventoryInfo.IsFertilizerFromInventory,
							newInventoryInfo.SeedMargin,
							newInventoryInfo.IsSeedFromInventory,
							newInventoryInfo.ServiceMargin,
							newInventoryInfo.IsServiceFromInventory
						));

						InventoryInfo = changeVm.InventoryInfo;
						base.RaisePropertyChanged(() => InventoryInfo);
					}

					Messenger.Default.Send(new HidePopupMessage());
				});

				Messenger.Default.Send(new ShowPopupMessage() {
					ScreenDescriptor = new ScreenDescriptor(typeof(Views.Resources.Contract.Rent.Popup.EditInventoryInfoView), "update inventory info", vm)
				});
			}
		}

		private void onUpdateLandOwnerInfo() {
			if (contractId != null && !IsReadOnly) {
				var vm = new UpdateLandOwnerInfoViewModel(clientEndpoint, dispatcher, currentCropYearId, RentDue, PerAcre, LandOwner, changeVm => {
					if (changeVm != null && changeVm.HasChanges) {
						var newLandOwner = changeVm.GetLandOwnerValueObject();

						clientEndpoint.SendOne(new UpdateContractLandOwnerInfo(
							contractId, 
							clientEndpoint.GenerateNewMetadata(),
							newLandOwner, 
							changeVm.PerAcre, 
							changeVm.RentDue.ToUniversalTime()
						));

						LandOwner = newLandOwner;
						PerAcre = changeVm.PerAcre;
						RentDue = changeVm.RentDue;

						base.RaisePropertyChanged(() => LandOwner);
						base.RaisePropertyChanged(() => PerAcre);
						base.RaisePropertyChanged(() => RentDue);
					}

					Messenger.Default.Send(new HidePopupMessage());
				});

				Messenger.Default.Send(new ShowPopupMessage() {
					ScreenDescriptor = new ScreenDescriptor(typeof(Views.Resources.Contract.Rent.Popup.EditLandOwnerInfoView), "update landowner info", vm)
				});
			}
		}
		#endregion

		private void buildDetailsFromReadModel(RentContractDetailsView contractDetails) {
			StartDate = contractDetails.StartDate;

			SaleInfo = getSaleInfo(contractDetails);

			InventoryInfo = new IncludedInventoryInfoViewModel(contractDetails.IncludedInventoryInfo);

			ProductExceptions = contractDetails.ProductExceptions;
			PerAcre = contractDetails.PerAcre;
			Duration = contractDetails.Duration;
			RentDue = contractDetails.RentDue;

			ActualYield = contractDetails.ActualYield;
			FlatCropPrice = contractDetails.FlatCropPrice;
			FlexPercentage = contractDetails.FlexPercentage;
			FlexPercentageSource = contractDetails.FlexPercentageSource;

			FlexLevelList = new ObservableCollection<FlexLevelListItemViewModel>();

			foreach (var f in contractDetails.FlexDetails) {
				FlexLevelList.Add(new FlexLevelListItemViewModel(f));
			}

			RentDue = contractDetails.RentDue;
			PerAcre = contractDetails.PerAcre;
			LandOwner = contractDetails.LandOwner;

            SourceName = string.Empty;
            if (contractDetails != null && contractDetails.Sources != null && contractDetails.Sources.Count > 0) {
                DocumentDescriptor dd = contractDetails.Sources[0];
                if (dd != null && !string.IsNullOrEmpty(dd.Name)) {
                    SourceName = string.Format(Strings.CopiedFrom_Format_Text, dd.Name);
                }
            }
        }

		private IEnumerable<IncludedSaleInfoViewModel> getSaleInfo(RentContractDetailsView contractDetails) {
			var czrcView = clientEndpoint.GetView<CropZoneRentContractView>(currentCropYearId).GetValue(new CropZoneRentContractView());
			var czIds = contractDetails.CropZones.Select(x => x.Id.Id);
			var associatedCropZones = czrcView.CropZoneRentContracts.Where(x => czIds.Contains(x.Key)).Select(x => x.Value);

			// build up a dictionary of all used units by load id
			// use of the dictionary prevents a field-to-sale load from getting its unit double-counted
			var loadUnitDic = new Dictionary<Guid, CompositeUnit>();

			foreach (var load in contractDetails.AssociatedSaleLoads) {
				if (load.FinalQuantityValue != 0) {
					loadUnitDic.Add(load.LoadId.Id, load.FinalQuantityUnit);
				}
			}

			foreach (var czrc in associatedCropZones) {
				foreach (var load in czrc.LoadQuantities) {
					if (load.Value.QuantityValue != 0 && !loadUnitDic.ContainsKey(load.Key)) {
						loadUnitDic.Add(load.Key, load.Value.QuantityUnit);
					}
				}
			}

			var yieldLocationList = clientEndpoint.GetView<YieldLocationListView>(new DataSourceId(currentCropYearId.DataSourceId)).GetValue(() => new YieldLocationListView());
			var excludedYieldLocations = from loc in yieldLocationList.YieldLocations
										 where loc.IsActiveInCropYear(currentCropYearId.Id) && loc.IsQuantityExcludedFromInventory
										 select loc.Id;

			var orderedUnitList = from u in loadUnitDic.Values
								  group u by u into g
								  orderby g.Count() descending
								  select new IncludedSaleInfoViewModel(
												g.Key,
												Convert.ToDecimal(ShareInfo.CropGrowerShare),
												contractDetails.AssociatedSaleLoads,
												associatedCropZones,
												excludedYieldLocations
											);

			return orderedUnitList;
		}
	}

	public class IncludedInventoryInfoViewModel : ViewModelBase {

		string INV_GROWER = Strings.FromGrowerInventory_Text;
		string INV_SHARE_OWNER = Strings.DirectBilledToShareOwner_Text;

		public IncludedInventoryInfoViewModel(IncludedRentContractInventoryInfo includedInventoryInfo) {
			InventoryTypeList = new List<string>() {
				INV_GROWER,
				INV_SHARE_OWNER,
			};

			SelectedCropProtectionInventory = includedInventoryInfo.IsCropProtectionFromInventory ? INV_GROWER : INV_SHARE_OWNER;
			SelectedFertilizerInventory = includedInventoryInfo.IsFertilizerFromInventory ? INV_GROWER : INV_SHARE_OWNER;
			SelectedServiceInventory = includedInventoryInfo.IsServiceFromInventory ? INV_GROWER : INV_SHARE_OWNER;
			SelectedSeedInventory = includedInventoryInfo.IsSeedFromInventory ? INV_GROWER : INV_SHARE_OWNER;

			CropProtectionMargin = includedInventoryInfo.CropProtectionMargin;
			FertilizerMargin = includedInventoryInfo.FertilizerMargin;
			ServiceMargin = includedInventoryInfo.ServiceMargin;
			SeedMargin = includedInventoryInfo.SeedMargin;
		}

		public List<string> InventoryTypeList { get; }

		private string _cpInventoryType;
		public string SelectedCropProtectionInventory {
			get { return _cpInventoryType; }
			set {
				_cpInventoryType = value;
				RaisePropertyChanged(() => SelectedCropProtectionInventory);
			}
		}

		private string _fertInventoryType;
		public string SelectedFertilizerInventory {
			get { return _fertInventoryType; }
			set {
				_fertInventoryType = value;
				RaisePropertyChanged(() => SelectedFertilizerInventory);
			}
		}

		private string _seedInventoryType;
		public string SelectedSeedInventory {
			get { return _seedInventoryType; }
			set {
				_seedInventoryType = value;
				RaisePropertyChanged(() => SelectedSeedInventory);
			}
		}

		private string _serviceInventoryType;
		public string SelectedServiceInventory {
			get { return _serviceInventoryType; }
			set {
				_serviceInventoryType = value;
				RaisePropertyChanged(() => SelectedServiceInventory);
			}
		}

		private double _cpMargin;
		public double CropProtectionMargin {
			get { return _cpMargin; }
			set {
				if (value > 1.0) {
					_cpMargin = value / 100;
				} else {
					_cpMargin = value;
				}

				RaisePropertyChanged(() => CropProtectionMargin);
			}
		}

		private double _fertMargin;
		public double FertilizerMargin {
			get { return _fertMargin; }
			set {
				if (value > 1.0) {
					_fertMargin = value / 100;
				} else {
					_fertMargin = value;
				}

				RaisePropertyChanged(() => FertilizerMargin);
			}
		}

		private double _seedMargin;
		public double SeedMargin {
			get { return _seedMargin; }
			set {
				if (value > 1.0) {
					_seedMargin = value / 100;
				} else {
					_seedMargin = value;
				}

				RaisePropertyChanged(() => SeedMargin);
			}
		}

		private double _serviceMargin;
		public double ServiceMargin {
			get { return _serviceMargin; }
			set {
				if (value > 1.0) {
					_serviceMargin = value / 100;
				} else {
					_serviceMargin = value;
				}

				RaisePropertyChanged(() => ServiceMargin);
			}
		}

		public IncludedRentContractInventoryInfo GetIncludedInventoryInfoObject() {
			return new IncludedRentContractInventoryInfo(
				SelectedCropProtectionInventory != null && SelectedCropProtectionInventory == INV_GROWER,
				SelectedFertilizerInventory != null && SelectedFertilizerInventory == INV_GROWER,
				SelectedServiceInventory != null && SelectedServiceInventory == INV_GROWER,
				SelectedSeedInventory != null && SelectedSeedInventory == INV_GROWER,
				CropProtectionMargin,
				FertilizerMargin,
				ServiceMargin,
				SeedMargin
			);
		}
	}

	public class IncludedShareInfoViewModel : ViewModelBase {

		public IncludedShareInfoViewModel(IncludedContractShareInfo shareInfo) {
			CropProtectionGrowerShare = shareInfo.GrowerCropProtectionShare;
			FertilizerGrowerShare = shareInfo.GrowerFertilizerShare;
			ServiceGrowerShare = shareInfo.GrowerServiceShare;
			SeedGrowerShare = shareInfo.GrowerSeedShare;
			CropGrowerShare = shareInfo.GrowerCropShare;
		}

		private double cpGrowerShare;
		public double CropProtectionGrowerShare {
			get { return cpGrowerShare; }
			set {
				if (value > 1.0) {
					cpGrowerShare = value / 100;
				} else {
					cpGrowerShare = value;
				}

				cpOwnerShare = 1.0 - cpGrowerShare;
				RaisePropertyChanged(() => CropProtectionGrowerShare);
				RaisePropertyChanged(() => CropProtectionOwnerShare);
			}
		}

		private double cpOwnerShare;
		public double CropProtectionOwnerShare {
			get { return cpOwnerShare; }
			set {

				if (value > 1.0) {
					cpOwnerShare = value / 100;
				} else {
					cpOwnerShare = value;
				}

				cpGrowerShare = 1.0 - cpOwnerShare;
				RaisePropertyChanged(() => CropProtectionOwnerShare);
				RaisePropertyChanged(() => CropProtectionGrowerShare);
			}
		}

		private double fertGrowerShare;
		public double FertilizerGrowerShare {
			get { return fertGrowerShare; }
			set {
				if (value > 1.0) {
					fertGrowerShare = value / 100;
				} else {
					fertGrowerShare = value;
				}

				fertOwnerShare = 1.0 - fertGrowerShare;
				RaisePropertyChanged(() => FertilizerGrowerShare);
				RaisePropertyChanged(() => FertilizerOwnerShare);
			}
		}

		private double fertOwnerShare;
		public double FertilizerOwnerShare {
			get { return fertOwnerShare; }
			set {
				if (value > 1.0) {
					fertOwnerShare = value / 100;
				} else {
					fertOwnerShare = value;
				}

				fertGrowerShare = 1.0 - fertOwnerShare;
				RaisePropertyChanged(() => FertilizerOwnerShare);
				RaisePropertyChanged(() => FertilizerGrowerShare);
			}
		}

		private double seedGrowerShare;
		public double SeedGrowerShare {
			get { return seedGrowerShare; }
			set {
				if (value > 1.0) {
					seedGrowerShare = value / 100;
				} else {
					seedGrowerShare = value;
				}

				seedOwnerShare = 1.0 - seedGrowerShare;
				RaisePropertyChanged(() => SeedGrowerShare);
				RaisePropertyChanged(() => SeedOwnerShare);
			}
		}

		private double seedOwnerShare;
		public double SeedOwnerShare {
			get { return seedOwnerShare; }
			set {
				if (value > 1.0) {
					seedOwnerShare = value / 100;
				} else {
					seedOwnerShare = value;
				}

				seedGrowerShare = 1.0 - seedOwnerShare;
				RaisePropertyChanged(() => SeedOwnerShare);
				RaisePropertyChanged(() => SeedGrowerShare);
			}
		}

		private double serviceGrowerShare;
		public double ServiceGrowerShare {
			get { return serviceGrowerShare; }
			set {
				if (value > 1.0) {
					serviceGrowerShare = value / 100;
				} else {
					serviceGrowerShare = value;
				}

				serviceOwnerShare = 1.0 - serviceGrowerShare;
				RaisePropertyChanged(() => ServiceGrowerShare);
				RaisePropertyChanged(() => ServiceOwnerShare);
			}
		}

		private double serviceOwnerShare;
		public double ServiceOwnerShare {
			get { return serviceOwnerShare; }
			set {
				if (value > 1.0) {
					serviceOwnerShare = value / 100;
				} else {
					serviceOwnerShare = value;
				}

				serviceGrowerShare = 1.0 - serviceOwnerShare;
				RaisePropertyChanged(() => ServiceOwnerShare);
				RaisePropertyChanged(() => ServiceGrowerShare);
			}
		}

		private double cropGrowerShare;
		public double CropGrowerShare {
			get { return cropGrowerShare; }
			set {
				if (value > 1.0) {
					cropGrowerShare = value / 100;
				} else {
					cropGrowerShare = value;
				}

				cropOwnerShare = 1.0 - cropGrowerShare;
				RaisePropertyChanged(() => CropGrowerShare);
				RaisePropertyChanged(() => CropOwnerShare);
			}
		}

		private double cropOwnerShare;
		public double CropOwnerShare {
			get { return cropOwnerShare; }
			set {
				if (value > 1.0) {
					cropOwnerShare = value / 100;
				} else {
					cropOwnerShare = value;
				}

				cropGrowerShare = 1.0 - cropOwnerShare;
				RaisePropertyChanged(() => CropOwnerShare);
				RaisePropertyChanged(() => CropGrowerShare);
			}
		}

		private double cpGrowerShareDollars;
		public double CropProtectionGrowerShareDollars {
			get { return cpGrowerShareDollars; }
			set {
				cpGrowerShareDollars = value;
				RaisePropertyChanged(() => CropProtectionGrowerShareDollars);
				RaisePropertyChanged(() => CropProtectionTotalDollars);
			}
		}

		private double fertGrowerShareDollars;
		public double FertilizerGrowerShareDollars {
			get { return fertGrowerShareDollars; }
			set {
				fertGrowerShareDollars = value;
				RaisePropertyChanged(() => FertilizerGrowerShareDollars);
				RaisePropertyChanged(() => FertilizerTotalDollars);
			}
		}

		private double serviceGrowerShareDollars;
		public double ServiceGrowerShareDollars {
			get { return serviceGrowerShareDollars; }
			set {
				serviceGrowerShareDollars = value;
				RaisePropertyChanged(() => ServiceGrowerShareDollars);
				RaisePropertyChanged(() => ServiceTotalDollars);
			}
		}

		private double seedGrowerShareDollars;
		public double SeedGrowerShareDollars {
			get { return seedGrowerShareDollars; }
			set {
				seedGrowerShareDollars = value;
				RaisePropertyChanged(() => SeedGrowerShareDollars);
				RaisePropertyChanged(() => SeedTotalDollars);
			}
		}

		private double cpLandOwnerShareDollars;
		public double CropProtectionLandOwnerShareDollars {
			get { return cpLandOwnerShareDollars; }
			set {
				cpLandOwnerShareDollars = value;
				RaisePropertyChanged(() => CropProtectionLandOwnerShareDollars);
				RaisePropertyChanged(() => CropProtectionTotalDollars);
			}
		}

		private double fertLandOwnerShareDollars;
		public double FertilizerLandOwnerShareDollars {
			get { return fertLandOwnerShareDollars; }
			set {
				fertLandOwnerShareDollars = value;
				RaisePropertyChanged(() => FertilizerLandOwnerShareDollars);
				RaisePropertyChanged(() => FertilizerTotalDollars);
			}
		}

		private double serviceLandOwnerShareDollars;
		public double ServiceLandOwnerShareDollars {
			get { return serviceLandOwnerShareDollars; }
			set {
				serviceLandOwnerShareDollars = value;
				RaisePropertyChanged(() => ServiceLandOwnerShareDollars);
				RaisePropertyChanged(() => CropProtectionTotalDollars);
			}
		}

		private double seedLandOwnerShareDollars;
		public double SeedLandOwnerShareDollars {
			get { return seedLandOwnerShareDollars; }
			set {
				seedLandOwnerShareDollars = value;
				RaisePropertyChanged(() => SeedLandOwnerShareDollars);
				RaisePropertyChanged(() => CropProtectionTotalDollars);
			}
		}

		public double CropProtectionTotalDollars => CropProtectionGrowerShareDollars + CropProtectionLandOwnerShareDollars;
		public double FertilizerTotalDollars => FertilizerGrowerShareDollars + FertilizerLandOwnerShareDollars;
		public double ServiceTotalDollars => ServiceGrowerShareDollars + ServiceLandOwnerShareDollars;
		public double SeedTotalDollars => SeedGrowerShareDollars + SeedLandOwnerShareDollars;

		public IncludedContractShareInfo GetShareInfoObject() {
			return new IncludedContractShareInfo(
				CropProtectionGrowerShare,
				FertilizerGrowerShare,
				SeedGrowerShare,
				ServiceGrowerShare,
				CropGrowerShare
			);
		}

		public void CalculateShareDollars(IClientEndpoint clientEndpoint, CropYearId currentCropYearId, int contractInitialCropYear, IEnumerable<Guid> contractIncludedCropZoneIds, ContractId contractId) {
			var masterlistService = clientEndpoint.GetMasterlistService();
			var inventoryProductList = clientEndpoint.GetView<InventoryListView>(currentCropYearId).GetValue(new InventoryListView());
			var contractDetials = clientEndpoint.GetView<RentContractDetailsView>(contractId).Value;
			//var czrcList = clientEndpoint.GetView<CropZoneRentContractView>(currentCropYearId).GetValue(() => null);
            var cropZonesAppliedProducts = clientEndpoint.GetView<CropZoneApplicationDataView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear)).GetValue(new CropZoneApplicationDataView());
            //if (czrcList != null) {
            foreach (var czId in contractIncludedCropZoneIds) {
                var czAppliedProducts = cropZonesAppliedProducts.Items.Where(x => x.CropZoneId == new CropZoneId(ApplicationEnvironment.CurrentDataSourceId, czId));
				if (czAppliedProducts.Any()) {
					//var czrc = czrcList.CropZoneRentContracts[czId];
					var appliedCZProducts = czAppliedProducts;
					if (appliedCZProducts.Any()) {
						foreach (var prod in appliedCZProducts) {
							if (inventoryProductList.Products.ContainsKey(prod.ProductId)) {
								var inventoryProduct = inventoryProductList.Products[prod.ProductId];

								if (inventoryProduct.AveragePriceValue == 0 || string.IsNullOrWhiteSpace(inventoryProduct.AveragePriceUnit)) { continue; }

								var mlProduct = masterlistService.GetProduct(inventoryProduct.ProductId);
								var destinationUnit = UnitFactory.GetPackageSafeUnit(inventoryProduct.AveragePriceUnit, mlProduct.StdUnit, mlProduct.StdFactor, mlProduct.StdPackageUnit);

								var sourceUnit = UnitFactory.GetPackageSafeUnit(prod.TotalProductUnit, mlProduct.StdUnit, mlProduct.StdFactor, mlProduct.StdPackageUnit);
								var sourceMeasure = sourceUnit.GetMeasure(Convert.ToDouble(prod.TotalProductValue), mlProduct.Density);
								var convertedQuantity = sourceMeasure.GetValueAs(destinationUnit);
								var appliedDollars = convertedQuantity * inventoryProduct.AveragePriceValue;
								var growerDollars = appliedDollars * Convert.ToDouble(prod.GrowerShare);
								var landOwnerDollars = appliedDollars - growerDollars;

								if (mlProduct.ProductType == GlobalStrings.ProductType_CropProtection || mlProduct.ProductType.ToLower() == "surfactant") {
									CropProtectionGrowerShareDollars += growerDollars;
									CropProtectionLandOwnerShareDollars += contractDetials.IncludedInventoryInfo.IsCropProtectionFromInventory ? landOwnerDollars : 0;
								} else if (mlProduct.ProductType == GlobalStrings.ProductType_Fertilizer) {
									FertilizerGrowerShareDollars += growerDollars;
									FertilizerLandOwnerShareDollars += contractDetials.IncludedInventoryInfo.IsFertilizerFromInventory ? landOwnerDollars : 0;
								} else if (mlProduct.ProductType == GlobalStrings.ProductType_Service) {
									ServiceGrowerShareDollars += growerDollars;
									ServiceLandOwnerShareDollars += contractDetials.IncludedInventoryInfo.IsServiceFromInventory ? landOwnerDollars : 0;
								} else if (mlProduct.ProductType == GlobalStrings.ProductType_Seed) {
									SeedGrowerShareDollars += growerDollars;
									SeedLandOwnerShareDollars += contractDetials.IncludedInventoryInfo.IsSeedFromInventory ? landOwnerDollars : 0;
								}
							}
						}
					}
				}
			}
		}

		//}
	}

	public class IncludedSaleInfoViewModel : ViewModelBase {

		public IncludedSaleInfoViewModel(CompositeUnit unit, decimal growerCropShare, IEnumerable<RentContractIncludedLoad> associatedLoads, IEnumerable<CropZoneRentContract> associatedCropZones, IEnumerable<YieldLocationId> excludedYieldLocationIds) {
			DisplayUnit = unit;

			var cropZoneLoadsWithThisUnit = from cz in associatedCropZones
											from load in cz.LoadQuantities.Values
											from locId in load.DestinationLocationIds
											where load.QuantityUnit == unit
												&& !excludedYieldLocationIds.Contains(locId)
											select load;

			foreach (var load in cropZoneLoadsWithThisUnit) {
				var locationWeightedQuantity = load.QuantityValue * (1m / load.DestinationLocationIds.Count);

				TotalQuantity += locationWeightedQuantity;

				if (load.OperationType == YieldOperationTypes.FieldToStorage || (load.ShareSplitStrategy.HasValue && load.ShareSplitStrategy.Value == ShareSplitStrategyTypes.Split)) {
					LandOwnerShares += locationWeightedQuantity * (1 - growerCropShare);
				} else if (load.OperationType == YieldOperationTypes.FieldToSale && load.ShareSplitStrategy.HasValue && load.ShareSplitStrategy.Value == ShareSplitStrategyTypes.LandOwner) {
					LandOwnerShares += locationWeightedQuantity;
				}
			}

			var associatedLoadsWithThisUnit = associatedLoads.Where(x => x.FinalQuantityUnit == unit);

			foreach (var load in associatedLoadsWithThisUnit) {
				LandOwnerSales += load.LandOwnerSalePrice;

				if (load.IsSplitLoad) {
					LandOwnerSoldShares += load.FinalQuantityValue * (1 - growerCropShare);
				} else {
					LandOwnerSoldShares += load.FinalQuantityValue;
				}
			}
		}

		public CompositeUnit DisplayUnit { get; }
		public decimal TotalQuantity { get; }
		public decimal LandOwnerShares { get; }
		public decimal LandOwnerSoldShares { get; }
		public decimal LandOwnerSales { get; }
	}
}