﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Person;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Resources.Person.PersonParts;
using Landdb.ViewModel.Shared;
using Landdb.Views.Resources.Popups;
using Landdb.Views.Shared.Popups;
using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Resources {
	public class PersonDetailsViewModel : ViewModelBase {

		private readonly IClientEndpoint clientEndpoint;
		private readonly Dispatcher dispatcher;

		private readonly PersonId personId;

		public PersonDetailsViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, PersonDetailsView detailsView) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			personId = detailsView.Id;

			buildDetailsFromReadModel(detailsView);

			UpdatePersonContactInfoCommand = new RelayCommand(onUpdatePersonContactInfo);
			UpdatePersonAddressCommand = new RelayCommand(onUpdatePersonAddress);
			UpdatePersonApplicatorLicenseCommand = new RelayCommand(onUpdatePersonApplicatorLicense);
			UpdatePersonNotesCommand = new RelayCommand(onUpdatePersonNotes);
		}

		public ICommand UpdatePersonContactInfoCommand { get; }
		public ICommand UpdatePersonAddressCommand { get; }
		public ICommand UpdatePersonApplicatorLicenseCommand { get; }
		public ICommand UpdatePersonNotesCommand { get; }

		public string Name { get; private set; }
		public string PrimaryPhoneNumber { get; private set; }
		public string EmailAddress { get; private set; }
		public StreetAddressViewModel StreetAddress { get; private set; }
		public string ApplicatorLicenseNumber { get; private set; }
		public DateTime ApplicatorLicenseExpiration { get; private set; }
		public string Notes { get; private set; }

		public ObservableCollection<PersonApplicationInfo> ApplicationInfo { get; private set; }
		public ObservableCollection<PersonContractInfo> ContractInfo { get; private set; }

		public string StreetAddressDisplayText {
			get {
				var streetAddressIsPopulated = StreetAddress != null && !string.IsNullOrWhiteSpace(StreetAddress.MultiLineDisplay);
				return streetAddressIsPopulated ? StreetAddress.MultiLineDisplay : $"[{Strings.SpecifyStreetAddress_Text}]";
			}
		}

		public string ApplicatorLicenseHeader => HasApplicatorLicenseInfo ? Strings.ApplicatorLicense_Text : Strings.AddApplicatorLicense_Text;
		public bool HasApplicatorLicenseInfo => HasApplicatorLicenseNumber || HasApplicatorLicenseExpiration;
		public bool HasApplicatorLicenseNumber => !string.IsNullOrWhiteSpace(ApplicatorLicenseNumber);
		public bool HasApplicatorLicenseExpiration => ApplicatorLicenseExpiration != DateTime.MinValue;
		public string NotesHeader => HasNotes ? Strings.Notes_Text : $"[{Strings.AddNotes_Text}]";
		public bool HasNotes => !string.IsNullOrWhiteSpace(Notes);

		private void onUpdatePersonContactInfo() {
			if (personId != null) {
				DialogFactory.ShowContactInfoChangeDialog(Name, PrimaryPhoneNumber, EmailAddress, newInfo => {
					var newName = newInfo.Item1;
					var newPhone = newInfo.Item2;
					var newEmail = newInfo.Item3;

					clientEndpoint.SendOne(new UpdatePersonContactInfo(
						personId,
						clientEndpoint.GenerateNewMetadata(),
						newName,
						newPhone,
						newEmail
					));

					Name = newName;
					PrimaryPhoneNumber = newPhone != null ? newPhone.GetFormattedString() : null;
                    EmailAddress = newEmail != null ? newEmail.Address : null;

					base.RaisePropertyChanged(() => Name);
					base.RaisePropertyChanged(() => PrimaryPhoneNumber);
					base.RaisePropertyChanged(() => EmailAddress);
				});
			}
		}

		private void onUpdatePersonAddress() {
			if (personId != null) {
				DialogFactory.ShowStreetAddressChangeDialog(StreetAddress.GetValueObject(), newAddress => {
					clientEndpoint.SendOne(new UpdatePersonStreetAddressInfo(
						personId,
						clientEndpoint.GenerateNewMetadata(),
						newAddress
					));

					StreetAddress = new StreetAddressViewModel(newAddress);
					base.RaisePropertyChanged(() => StreetAddress);
				});
			}
		}

		private void onUpdatePersonApplicatorLicense() {
			if (personId != null) {
				var vm = new Popups.UpdatePersonApplicatorLicenseViewModel(clientEndpoint, dispatcher, ApplicatorLicenseNumber, ApplicatorLicenseExpiration, changeVm => {
					if (changeVm != null && changeVm.HasChanges) {
						clientEndpoint.SendOne(new UpdatePersonApplicatorLicense(
							personId, 
							clientEndpoint.GenerateNewMetadata(), 
							changeVm.LicenseNumber, 
							changeVm.LicenseExpiration
						));

						ApplicatorLicenseNumber = changeVm.LicenseNumber;
						ApplicatorLicenseExpiration = changeVm.LicenseExpiration;

						base.RaisePropertyChanged(() => ApplicatorLicenseNumber);
						base.RaisePropertyChanged(() => ApplicatorLicenseExpiration);
						base.RaisePropertyChanged(() => HasApplicatorLicenseInfo);
						base.RaisePropertyChanged(() => HasApplicatorLicenseNumber);
						base.RaisePropertyChanged(() => HasApplicatorLicenseExpiration);
						base.RaisePropertyChanged(() => ApplicatorLicenseHeader);
					}

					Messenger.Default.Send(new HidePopupMessage());
				});

				Messenger.Default.Send(new ShowPopupMessage() {
					ScreenDescriptor = new ScreenDescriptor(typeof(UpdatePersonApplicatorLicenseView), "update person applicator license", vm)
				});
			}
		}

		private void onUpdatePersonNotes() {
			if (personId != null) {
				DialogFactory.ShowNotesChangeDialog(Notes, newNotes => {
					clientEndpoint.SendOne(new UpdatePersonNotes(
						personId, 
						clientEndpoint.GenerateNewMetadata(),
						newNotes
					));

					Notes = newNotes;
					base.RaisePropertyChanged(() => Notes);
					base.RaisePropertyChanged(() => HasNotes);
					base.RaisePropertyChanged(() => NotesHeader);
				});
			}
		}

		private void buildDetailsFromReadModel(PersonDetailsView detailsView) {
			Name = detailsView.Name;
			PrimaryPhoneNumber = new PhoneNumber("", detailsView.PrimaryPhoneNumber).GetFormattedString();
			EmailAddress = detailsView.PrimaryEmail;
			StreetAddress = detailsView.StreetAddress != null ? new StreetAddressViewModel(detailsView.StreetAddress) : new StreetAddressViewModel(new StreetAddress());
			ApplicatorLicenseNumber = detailsView.ApplicatorLicenseNumber;
			ApplicatorLicenseExpiration = detailsView.ApplicatorLicenseExpiration;
			Notes = detailsView.Notes;

			ApplicationInfo = new ObservableCollection<PersonApplicationInfo>();
			if (detailsView.AssociatedApplications != null) {
				foreach (var app in detailsView.AssociatedApplications) {
					ApplicationInfo.Add(new PersonApplicationInfo(app));
				}
			}

			ContractInfo = new ObservableCollection<PersonContractInfo>();
			if (detailsView.AssociatedContracts != null) {
				foreach (var contract in detailsView.AssociatedContracts) {
					ContractInfo.Add(new PersonContractInfo(contract));
				}
			}
		}
	}
}