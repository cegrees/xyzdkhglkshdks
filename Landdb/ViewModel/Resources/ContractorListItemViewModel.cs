﻿using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Company;
using Landdb.Domain.ReadModels.Person;
using System.Collections.Generic;
using System.Linq;
using Landdb.Resources;

namespace Landdb.ViewModel.Resources {
	public class ContractorListItemViewModel {

		public ContractorListItemViewModel(PersonListItem person) {
			PersonId = person.Id;
			Name = person.Name;
			PhoneNumber = person.PrimaryPhoneNumber;
			Group = Strings.People_Text;
		}

		public ContractorListItemViewModel(CompanyListItem company) {
			CompanyId = company.Id;
			Name = company.Name;
			PhoneNumber = company.PrimaryPhoneNumber;
			Group = Strings.Companies_Text;
		}

		// private constructor for adding empty entries to lists
		private ContractorListItemViewModel(string name) {
			PersonId = null;
			CompanyId = null;
			Name = name;
		}

		public PersonId PersonId { get; }
		public CompanyId CompanyId { get; }
		public string Group { get; }
		public string Name { get; }
		public string PhoneNumber { get; }

		public static List<ContractorListItemViewModel> GetListItemsForCropYear(IClientEndpoint clientEndpoint, CropYearId cropYearId, bool shouldInsertEmptyEntryAtTop) {
			var dsID = new DataSourceId(cropYearId.DataSourceId);

			var contractorList = new List<ContractorListItemViewModel>();

			var personListView = clientEndpoint.GetView<PersonListView>(dsID).GetValue(new PersonListView());
			var sortedFilteredPersons = from p in personListView.Persons
										where p.IsActiveInCropYear(cropYearId.Id)
										orderby p.Name
										select p;

			sortedFilteredPersons.For_Each(person => {
				contractorList.Add(new ContractorListItemViewModel(person));
			});

			var companyListView = clientEndpoint.GetView<CompanyListView>(dsID).GetValue(new CompanyListView());
			var sortedFilteredCompanies = from c in companyListView.Companies
										  where c.IsActiveInCropYear(cropYearId.Id)
										  orderby c.Name
										  select c;

			sortedFilteredCompanies.ForEach(company => {
				contractorList.Add(new ContractorListItemViewModel(company));
			});

			if (shouldInsertEmptyEntryAtTop && contractorList.Any()) {
				contractorList.Add(new ContractorListItemViewModel(string.Empty));
			}

			return contractorList;
		}

		public override string ToString() {
			return Name;
		}
	}
}