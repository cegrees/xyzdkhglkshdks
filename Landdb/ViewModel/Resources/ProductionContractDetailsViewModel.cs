﻿using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Contract;
using Landdb.Infrastructure.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using System.Windows.Threading;

namespace Landdb.ViewModel.Resources.Contract.Production {
	public class ProductionContractDetailsViewModel : ContractDetailsBaseViewModel {

		public ProductionContractDetailsViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, DataSourceId currentDataSourceId, int currentCropYear, ProductionContractDetailsView contractDetails)
			: base(clientEndpoint, dispatcher, currentCropYear, contractDetails.Id, contractDetails.Name, contractDetails.ContractType, contractDetails.CropYear, contractDetails.IncludedShareInfo, contractDetails.CropZones, contractDetails.Notes) {

			buildDetailsFromReadModel(contractDetails);

			UpdateContractInfoCommand = new RelayCommand(onUpdateContractInfo);
			UpdateCommodityInfoCommand = new RelayCommand(onUpdateCommodityInfo);
		}

		public ICommand UpdateContractInfoCommand { get; }
		public ICommand UpdateCommodityInfoCommand { get; }

		public string ContractNumber { get; private set; }
		public string ContractGrowerId { get; private set; }
		public DateTime ContractDate { get; private set; }
		public DateTime DeliveryDate { get; private set; }
		public Buyer Buyer { get; private set; }
		public IncludedCommodityInfoViewModel CommodityInfo { get; private set; }

		public string BuyerDisplayText => Buyer != null ? Buyer.Name : string.Empty;

		private void onUpdateContractInfo() {
			if (contractId != null && !IsReadOnly) {
				var vm = new UpdateContractInfoViewModel(clientEndpoint, dispatcher, currentCropYearId, ContractNumber, ContractGrowerId, ContractDate, DeliveryDate, Buyer, changeVm => {
					if (changeVm != null && changeVm.HasChanges) {
						clientEndpoint.SendOne(new UpdateProductionContractInfo(
							contractId,
							clientEndpoint.GenerateNewMetadata(),
							changeVm.ContractDate,
							changeVm.DeliveryDate,
							changeVm.ContractNumber,
							changeVm.ContractGrowerId,
							changeVm.GetBuyerValueObject()
						));

						Buyer newBuyer = null;

						if (changeVm.SelectedBuyer != null) {
							var b = changeVm.SelectedBuyer;

							if (b.CompanyId != null) {
								newBuyer = new Buyer(b.CompanyId, b.Name, b.PhoneNumber);
							} else if (b.PersonId != null) {
								newBuyer = new Buyer(b.PersonId, b.Name, b.PhoneNumber);
							}
						}

						Buyer = newBuyer;

						ContractNumber = changeVm.ContractNumber;
						ContractGrowerId = changeVm.ContractGrowerId;

						ContractDate = changeVm.ContractDate;
						DeliveryDate = changeVm.DeliveryDate;

						RaisePropertyChanged(() => BuyerDisplayText);

						RaisePropertyChanged(() => ContractNumber);
						RaisePropertyChanged(() => ContractGrowerId);

						RaisePropertyChanged(() => ContractDate);
						RaisePropertyChanged(() => DeliveryDate);
					}

					Messenger.Default.Send(new HidePopupMessage());
				});

				Messenger.Default.Send(new ShowPopupMessage() {
					ScreenDescriptor = new ScreenDescriptor(typeof(Views.Resources.Contract.Production.Popup.EditContractInfoView), "update contract info", vm)
				});
			}
		}

		private void onUpdateCommodityInfo() {
			if (contractId != null && !IsReadOnly) {
				var vm = new UpdateCommodityInfoViewModel(clientEndpoint, dispatcher, currentCropYearId, contractId, CommodityInfo.GetCommodityInfoValueObject(), changeVm => {
					if (changeVm != null && changeVm.HasChanges) {
						var newCommodityInfo = changeVm.GetCommodityInfoValueObject();

						clientEndpoint.SendOne(new UpdateProductionContractCommodityInfo(
							contractId,
							clientEndpoint.GenerateNewMetadata(),
							newCommodityInfo.CropId,
							newCommodityInfo.ContractPrice,
							newCommodityInfo.FuturesPrice,
							newCommodityInfo.ContractedAmount,
							newCommodityInfo.ContractedAmountUnit
						));

						CommodityInfo = new IncludedCommodityInfoViewModel(
							clientEndpoint,
							dispatcher, 
							currentCropYearId,
							contractId,
							newCommodityInfo
						);

						RaisePropertyChanged(() => CommodityInfo);
					}

					Messenger.Default.Send(new HidePopupMessage());
				});

				Messenger.Default.Send(new ShowPopupMessage() {
					ScreenDescriptor = new ScreenDescriptor(typeof(Views.Resources.Contract.Production.Popup.EditCommodityInfoView), "update commodity info", vm)
				});
			}
		}

		private void buildDetailsFromReadModel(ProductionContractDetailsView contractDetails) {
			ContractNumber = contractDetails.ContractNumber;
			ContractGrowerId = contractDetails.GrowerId;

			ContractDate = contractDetails.StartDate;
			DeliveryDate = contractDetails.DeliveryDate;

			CommodityInfo = new IncludedCommodityInfoViewModel(clientEndpoint, dispatcher, currentCropYearId, contractId, contractDetails.IncludedCommodityInfo);

			Buyer = contractDetails.Buyer;
		}
	}

	public class IncludedCommodityInfoViewModel {

		readonly IClientEndpoint clientEndpoint;
		readonly Dispatcher dispatcher;

		public IncludedCommodityInfoViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, CropYearId currentCropYearId, ContractId contractId, IncludedProductionContractCommodityInfo commodityInfo) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;


			var cropId = new CropId(commodityInfo.CropId);
			CropId = cropId;
			CropName = clientEndpoint.GetMasterlistService().GetCropDisplay(cropId);

			ContractedAmount = commodityInfo.ContractedAmount;
			ContractedAmountUnit = commodityInfo.ContractedAmountUnit;
			ContractPrice = commodityInfo.ContractPrice;
			FuturesPrice = commodityInfo.FuturesPrice;

			calculateFulfilledAmount(currentCropYearId, contractId, cropId);
		}

		public CropId CropId { get; }
		public string CropName { get; }
		public double ContractedAmount { get; }
		public CompositeUnit ContractedAmountUnit { get; }
		public double ContractPrice { get; }
		public double FuturesPrice { get; }

		public double FulfilledAmount { get; private set; }

		public double FulfilledPercentage => ContractedAmount > 0 ? FulfilledAmount / ContractedAmount * 100 : 0;
		public string FulfilledAmountDisplayText => $"{	FulfilledAmount:N2} {(ContractedAmountUnit != null ? ContractedAmountUnit.FullDisplay : "pounds")}";
		public string ContractedAmountDisplayText => $"{ContractedAmount:N2} {ContractedAmountUnit.FullDisplay}";

		public IncludedProductionContractCommodityInfo GetCommodityInfoValueObject() =>
			new IncludedProductionContractCommodityInfo(
				CropId.Id,
				ContractedAmount,
				ContractedAmountUnit,
				ContractPrice,
				FuturesPrice
			);

		private void calculateFulfilledAmount(CropYearId currentCropYearId, ContractId contractId, CropId cropId) {
			if (ContractedAmountUnit != null) {
				var loads = clientEndpoint.GetView<ProductionContractLoadsView>(currentCropYearId)
					.GetValue(new ProductionContractLoadsView())
					.ProductionContractLoads
					.Values
					.Where(x => x.GrowerProductionContractId == contractId || x.LandOwnerProductionContractId == contractId);

				var shareDic = new Dictionary<ContractId, decimal>();
				decimal shareQuantity = 0;

				foreach (var load in loads) {
					if (load.IsSplitLoad && load.GrowerProductionContractId != load.LandOwnerProductionContractId) {
						decimal growerSharePercentage;

						if (shareDic.ContainsKey(load.RentContractId)) {
							growerSharePercentage = shareDic[load.RentContractId];
						} else {
							var rentContractDetails = clientEndpoint.GetView<RentContractDetailsView>(load.RentContractId).GetValue(new RentContractDetailsView());
							growerSharePercentage = Convert.ToDecimal(rentContractDetails.IncludedShareInfo.GrowerCropShare);
							shareDic.Add(load.RentContractId, growerSharePercentage);
						}

						if (load.GrowerProductionContractId == contractId) {
							shareQuantity = load.FinalQuantityValue * growerSharePercentage;
						} else if (load.LandOwnerProductionContractId == contractId) {
							shareQuantity = load.FinalQuantityValue * (1 - growerSharePercentage);
						}
					} else {
						shareQuantity = load.FinalQuantityValue;
					}

					var convertedQuantity = CompositeUnitConverter.ConvertValue(shareQuantity, load.FinalQuantityUnit, ContractedAmountUnit);
					FulfilledAmount += Convert.ToDouble(convertedQuantity);
				}
			}
		}
	}
}