﻿using GalaSoft.MvvmLight;
using Landdb.Domain.ReadModels.Person;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Resources.Person.PersonParts
{
    public class PersonContractInfo : ViewModelBase
    {
        public PersonContractInfo(IncludedRentContract contract)
        {
            Name = contract.Name;
            RentDue = contract.RentDue;
            Share = contract.Share;
            PerAcre = contract.PerAcre;
            Id = contract.Id;
        }

        public string Name { get; set; }
        public DateTime RentDue { get; set; }
        public string Share { get; set; }
        public double PerAcre { get; set; }
        public ContractId Id { get; set; }
    }
}
