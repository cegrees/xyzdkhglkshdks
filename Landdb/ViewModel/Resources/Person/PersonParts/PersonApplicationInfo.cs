﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Person;
using Landdb.Infrastructure.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Landdb.ViewModel.Resources.Person.PersonParts
{
    public class PersonApplicationInfo : ViewModelBase
    {
        public PersonApplicationInfo(IncludedApplication app)
        {
            Name = app.Name;
            StartDate = app.Date;
            TotalArea = app.TotalArea;
            Id = app.Id;
        }

        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public decimal TotalArea { get; set; }
        public ApplicationId Id { get; set; }

    }
}
