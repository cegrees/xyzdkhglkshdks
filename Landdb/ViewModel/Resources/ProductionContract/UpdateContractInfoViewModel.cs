﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Company;
using Landdb.Domain.ReadModels.Person;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Resources.Contract.Production {
	public class UpdateContractInfoViewModel : ViewModelBase {

		readonly IClientEndpoint clientEndpoint;
		readonly Dispatcher dispatcher;

		readonly Buyer originalBuyer;
		readonly string originalContractNumber;
		readonly string originalContractGrowerId;
		readonly DateTime originalContractDate;
		readonly DateTime originalDeliveryDate;

		readonly Action<UpdateContractInfoViewModel> contractInfoUpdated;

		public UpdateContractInfoViewModel(IClientEndpoint clientEndPoint, Dispatcher dispatcher, CropYearId currentCropYearId, string originalContractNumber, string originalContractGrowerId, DateTime originalContractDate, DateTime originalDeliveryDate, Buyer originalBuyer, Action<UpdateContractInfoViewModel> onContractInfoUpdated) {
			this.clientEndpoint = clientEndPoint;
			this.dispatcher = dispatcher;

			this.originalBuyer = originalBuyer;
			this.originalContractNumber = originalContractNumber;
			this.originalContractGrowerId = originalContractGrowerId;
			this.originalContractDate = originalContractDate;
			this.originalDeliveryDate = originalDeliveryDate;

			buildBuyerList(currentCropYearId);

			ContractNumber = originalContractNumber;
			ContractGrowerId = originalContractGrowerId;
			ContractDate = originalContractDate;
			DeliveryDate = originalDeliveryDate;

			if (originalBuyer != null) {
				var matchingListItems = from b in BuyerList
										where (b.Group == Strings.People_Text && b.PersonId == originalBuyer.PersonId)
											|| (b.Group == Strings.Companies_Text && b.CompanyId == originalBuyer.CompanyId)
										select b;

				SelectedBuyer = matchingListItems.FirstOrDefault();
			}

			contractInfoUpdated += onContractInfoUpdated;

			CompleteCommand = new RelayCommand(onComplete);
			CancelCommand = new RelayCommand(onCancel);
		}

		public ICommand CompleteCommand { get; private set; }
		public ICommand CancelCommand { get; private set; }

		public List<ContractorListItemViewModel> BuyerList { get; private set; }

		public ICollectionView BuyerDisplayCollection {
			get {
				var collectionView = CollectionViewSource.GetDefaultView(BuyerList.OrderBy(x => x.Group).ThenBy(x => x.Name));
				collectionView.GroupDescriptions.Add(new PropertyGroupDescription("Group"));
				return collectionView;
			}
		}

		public string ContractNumber { get; set; }
		public string ContractGrowerId { get; set; }
		public DateTime ContractDate { get; set; }
		public DateTime DeliveryDate { get; set; }
		public ContractorListItemViewModel SelectedBuyer { get; set; }

		public bool HasChanges {
			get {
				return ContractNumber != originalContractNumber
					|| ContractGrowerId != originalContractGrowerId
					|| GetBuyerValueObject() != originalBuyer
					|| ContractDate != originalContractDate
					|| DeliveryDate != originalDeliveryDate;
			}
		}

		public Buyer GetBuyerValueObject() {
			Buyer retBuyer = null;

			if (SelectedBuyer == null) {
				// leave it null
			} else if (SelectedBuyer.Group == Strings.People_Text) {
				retBuyer = new Buyer(SelectedBuyer.PersonId, SelectedBuyer.Name, SelectedBuyer.PhoneNumber);
			} else if (SelectedBuyer.Group == Strings.Companies_Text) {
				retBuyer = new Buyer(SelectedBuyer.CompanyId, SelectedBuyer.Name, SelectedBuyer.PhoneNumber);
			} else {
				// bad group? leave it null
			}

			return retBuyer;
		}

		private void onComplete() {
			contractInfoUpdated(this);
		}

		private void onCancel() {
			contractInfoUpdated(null);
		}

		private void buildBuyerList(CropYearId currentCropYearId) {
			var dataSourceId = new DataSourceId(currentCropYearId.DataSourceId);

			var newBuyerList = new List<ContractorListItemViewModel>();

			var personListView = clientEndpoint.GetView<PersonListView>(dataSourceId).GetValue(new PersonListView());
			var sortedFilteredPersons = from p in personListView.Persons
										where p.IsActiveInCropYear(currentCropYearId.Id)
										orderby p.Name
										select p;

			sortedFilteredPersons.ForEach(x => newBuyerList.Add(new ContractorListItemViewModel(x)));

			var companyListView = clientEndpoint.GetView<CompanyListView>(dataSourceId).GetValue(new CompanyListView());
			var sortedFilteredCompanies = from c in companyListView.Companies
										  where c.IsActiveInCropYear(currentCropYearId.Id)
										  orderby c.Name
										  select c;

			sortedFilteredCompanies.ForEach(x => newBuyerList.Add(new ContractorListItemViewModel(x)));

			BuyerList = newBuyerList.OrderBy(x => x.Group).ThenBy(x => x.Name).ToList();
		}
	}
}