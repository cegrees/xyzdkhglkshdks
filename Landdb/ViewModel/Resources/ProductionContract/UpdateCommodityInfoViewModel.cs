﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using System.Windows.Threading;

namespace Landdb.ViewModel.Resources.Contract.Production {
	public class UpdateCommodityInfoViewModel : ViewModelBase {

		readonly IClientEndpoint clientEndpoint;
		readonly Dispatcher dispatcher;

		readonly IncludedProductionContractCommodityInfo originalCommodityInfo;

		readonly Action<UpdateCommodityInfoViewModel> commodityInfoUpdated;

		public UpdateCommodityInfoViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, CropYearId currentCropYearId, ContractId contractId, IncludedProductionContractCommodityInfo originalCommodityInfo, Action<UpdateCommodityInfoViewModel> onCommodityInfoUpdated) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			this.originalCommodityInfo = originalCommodityInfo;

			CommodityList = clientEndpoint.GetMasterlistService().GetCropList();
			AllowedUnitList = CompositeUnitHelper.GetWeightUnitsForCrop(clientEndpoint, dispatcher, currentCropYearId, new CropId(originalCommodityInfo.CropId)).OrderBy(x => x.FullDisplay);

			SelectedCommodity = CommodityList.FirstOrDefault(x => x.Id == originalCommodityInfo.CropId);
			ContractedAmountUnit = AllowedUnitList.FirstOrDefault(x => x == originalCommodityInfo.ContractedAmountUnit);
			ContractedAmount = originalCommodityInfo.ContractedAmount;
			ContractPrice = originalCommodityInfo.ContractPrice;
			FuturesPrice = originalCommodityInfo.FuturesPrice;

			commodityInfoUpdated += onCommodityInfoUpdated;

			CompleteCommand = new RelayCommand(onComplete);
			CancelCommand = new RelayCommand(onCancel);
		}

		public ICommand CompleteCommand { get; private set; }
		public ICommand CancelCommand { get; private set; }

		public IEnumerable<MiniCrop> CommodityList { get; private set; }
		public IEnumerable<CompositeUnit> AllowedUnitList { get; private set; }

		public MiniCrop SelectedCommodity { get; set; }
		public double ContractedAmount { get; set; }
		public CompositeUnit ContractedAmountUnit { get; set; }
		public double ContractPrice { get; set; }
		public double FuturesPrice { get; set; }

		public bool HasChanges {
			get { return GetCommodityInfoValueObject() != originalCommodityInfo; }
		}

		public IncludedProductionContractCommodityInfo GetCommodityInfoValueObject() {
			return new IncludedProductionContractCommodityInfo(
				SelectedCommodity != null ? SelectedCommodity.Id : Guid.Empty,
				ContractedAmount,
				ContractedAmountUnit,
				ContractPrice,
				FuturesPrice
			);
		}

		private void onComplete() {
			commodityInfoUpdated(this);
		}

		private void onCancel() {
			commodityInfoUpdated(null);
		}
	}
}