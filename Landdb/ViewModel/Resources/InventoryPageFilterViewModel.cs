﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Landdb.ViewModel.Resources {
    public class InventoryPageFilterViewModel : ViewModelBase, IPageFilterViewModel {
        string filterText = string.Empty;
        string productType = string.Empty;
        bool? isUserCreated = null;

        public void ClearFilter() {
            FilterText = string.Empty;
            IsUserCreated = null;
            ProductType = string.Empty;
        }

        public void BeforeFilter() { }

		public void RefreshLists() { }

		public bool FilterItem(object item) {
            var x = item as InventoryListItemModel;
            if (x == null) { return false; }

            if (isUserCreated.HasValue) {
                if (isUserCreated.Value != x.IsUserCreated) { return false; }
            }

            if (!string.IsNullOrWhiteSpace(filterText) && !x.ProductName.ToLower().Contains(filterText.ToLower()) && !x.ManufacturerName.ToLower().Contains(filterText.ToLower())) {
                return false;
            }

            if (!string.IsNullOrWhiteSpace(productType) && x.ProductType != productType) {
                return false;
            }

            return true;
        }

        public string FilterText {
            get { return filterText; }
            set {
                filterText = value;
                RaisePropertyChanged("FilterText");
            }
        }

        public bool? IsUserCreated {
            get { return isUserCreated; }
            set {
                isUserCreated = value;
                RaisePropertyChanged("IsUserCreated");
            }
        }

        public string ProductType {
            get { return productType; }
            set {
                productType = value;
                RaisePropertyChanged("ProductType");
            }
        }

        public List<string> ProductTypes {
            get {
                
                return new List<string>() { string.Empty, Landdb.Resources.Strings.ProductType_CropProtection, Landdb.Resources.Strings.ProductType_Fertilizer, Landdb.Resources.Strings.ProductType_Seed, Landdb.Resources.Strings.ProductType_Service };
            }
        }
    }
}
