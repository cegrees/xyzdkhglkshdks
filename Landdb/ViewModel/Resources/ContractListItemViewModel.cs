﻿using GalaSoft.MvvmLight;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using Landdb.Resources;

namespace Landdb.ViewModel.Resources {
	public class ContractListItemViewModel : ViewModelBase {

		// constructor for inserting empty entries
		public ContractListItemViewModel(ContractId id, string name) {
			Id = id;
			Name = name;
		}

		public ContractListItemViewModel(RentContractListItem li) {
			Id = li.Id;
			Name = li.Name;
			CropId = li.Crop;
			Date = li.RentDue;
			DateDisplay = string.Format("{0}: {1:M/d/yyyy}", Strings.DueDate_Text, li.RentDue);
			CharmName = li.CharmName;
			Type = Strings.Rent_Text;
			FieldCount = li.FieldCount;
		}

		public ContractListItemViewModel(ProductionContractListItem li) {
			Id = li.Id;
			Name = li.Name;
			CropId = li.Crop;
			Date = li.DeliveryDate;
			DateDisplay = string.Format("{0}: {1:M/d/yyyy}", Strings.DeliveryDate_Text, li.DeliveryDate);
			CharmName = li.CharmName;
			Type = Strings.Production_Text;
			FieldCount = li.FieldCount;
		}

		public ContractId Id { get; }
		public string Name { get; }
		public CropId CropId { get; }
		public DateTime Date { get; }
		public string DateDisplay { get; }
		public int FieldCount { get; }
		public string Type { get; }

		private string charmName;
		public string CharmName {
			get { return charmName; }
			set {
				charmName = value;
				RaisePropertyChanged(() => CharmName);
			}
		}

		public static List<ContractListItemViewModel> GetListItemsForCropYear(IClientEndpoint clientEndpoint, CropYearId cropYearId, string type, bool shouldInsertEmptyEntryAtTop) {
			var contractList = new List<ContractListItemViewModel>();

			if (type == Strings.Rent_Text) {
				var rentContractList = clientEndpoint.GetView<RentContractListView>(cropYearId).GetValue(new RentContractListView());
				var sortedFilteredContracts = from c in rentContractList.RentContracts
											  orderby c.Name
											  select new ContractListItemViewModel(c);
			} else if (type == Strings.Production_Text) {
				var productionContractList = clientEndpoint.GetView<ProductionContractListView>(cropYearId).GetValue(new ProductionContractListView());
				var sortedFilteredContracts = from c in productionContractList.ProductionContracts
											  orderby c.Name
											  select new ContractListItemViewModel(c);

				contractList = sortedFilteredContracts.ToList();
			}


			if (shouldInsertEmptyEntryAtTop && contractList.Any()) {
				contractList.Insert(0, new ContractListItemViewModel(null, string.Empty));
			}

			return contractList;
		}

		public override string ToString() {
			return $"{Name} ({Type} {Strings.Contract_Text})";
		}
	}
}