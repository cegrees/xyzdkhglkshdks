﻿using ExportToExcel;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Company;
using Landdb.Infrastructure.Messages;
using Landdb.ReportModels.Company;
using Landdb.ViewModel.Resources.Popups;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Resources {
	public class CompanyPageViewModel : AbstractListPage<CompanyListItemViewModel, CompanyDetailsViewModel> {

		public CompanyPageViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher)
			: base(clientEndpoint, dispatcher) {

			Messenger.Default.Register<CompanyAddedMessage>(this, msg => {
				RefreshItemsList(() => {
					var q = from li in ListItems
							where li.Id == msg.LastCompanyAdded
							select li;

					SelectedListItem = q.FirstOrDefault();
				});
			});

			FilterModel = new CompanyPageFilterViewModel(clientEndpoint, dispatcher);
			PrintCommand = new RelayCommand(onUpdateReport);
		}

		public ICommand PrintCommand { get; }

		public override string GetEntityName() => Strings.Company_Text.ToLower();
		public override string GetPluralEntityName() => Strings.Companies_Text.ToLower();

		protected override IEnumerable<CompanyListItemViewModel> GetListItemModels() {
			var currentCropYearId = new CropYearId(currentDataSourceId.Id, currentCropYear);
			return CompanyListItemViewModel.GetListItemsForCropYear(clientEndpoint, currentCropYearId, false);
		}

		protected override async Task<ScreenDescriptor> CreateScreenDescriptorForNewItemCreation()
        {
			var companyId = new CompanyId(currentDataSourceId.Id, Guid.NewGuid());

			var vm = new AddCompanyViewModel(clientEndpoint, dispatcher, companyId, currentCropYear, li => {
                List<CompanyListItemViewModel> temporaryListItems = ListItems.ToList();
                temporaryListItems.Add(li);
                ListItems.Clear();
                temporaryListItems = temporaryListItems.OrderBy(listItem => listItem.Name).ToList();
                temporaryListItems.ForEach(listItem =>
                {
                    ListItems.Add(listItem);
                });
			    SelectedListItem = ListItems.SingleOrDefault(x => x.Id == li.Id);
                ApplyFilter();
			});

			return new ScreenDescriptor(typeof(Views.Fields.Popups.AddCompanyView), "add new company", vm);
		}

		protected override bool PerformItemRemoval(CompanyListItemViewModel selectedItem) {
			var removeCommand = new RemoveCompanyFromYear(selectedItem.Id, clientEndpoint.GenerateNewMetadata(), currentCropYear);
            clientEndpoint.SendOne(removeCommand);
            return true;
		}

		protected override IDomainCommand CreateSetCharmCommand(CompanyListItemViewModel selectedItem, string charmName) {
			// no charms on companies currently
			throw new NotImplementedException();
		}

		protected override CompanyDetailsViewModel CreateDetailsModel(CompanyListItemViewModel selectedItem) {
			var companyDetailsMaybe = clientEndpoint.GetView<CompanyDetailsView>(selectedItem.Id);

			if (companyDetailsMaybe.HasValue) {
				return new CompanyDetailsViewModel(clientEndpoint, dispatcher, companyDetailsMaybe.Value);
			} else {
				return null;
			}
		}

		protected override IEnumerable<RestorableItem> GetRestorableItems() {
			var companyList = clientEndpoint.GetView<CompanyListView>(currentDataSourceId).GetValue(() => new CompanyListView());

			return from c in companyList.Companies
				   where c.RemovedCropYear.HasValue && c.RemovedCropYear <= currentCropYear
				   select new RestorableItem(c.Id.Id, c.Name, "Company", c.InitialCropYear, c.RemovedCropYear.Value);
		}

		protected override void RestoreDeletedItem(Guid id) {
			// TODO: send command
		}

		void onUpdateReport() {
			string filename = string.Empty;
			//get map from ViewModel
			List<CompanyData> companyData = new List<CompanyData>();
			foreach (var item in ListItems) {
				CompanyData data = new CompanyData();

				var details = clientEndpoint.GetView<CompanyDetailsView>(item.Id).GetValue(() => null);

				if (details != null) {
					data.Name = details.Name;

					if (details.StreetAddress != null) {
						data.Address = details.StreetAddress.AddressLine1;
						data.City = details.StreetAddress.City;
						data.State = details.StreetAddress.State;
					}

					data.PhoneNumber = details.PrimaryPhoneNumber;
					data.Email = details.PrimaryEmail;

					companyData.Add(data);
				}
			}

			DataTable dt1 = new DataTable();

			dt1 = CreateExcelFile.ListToDataTable(companyData, true);

			dt1.TableName = Strings.PeopleDetails_Text;

			DataSet ds = new DataSet("excelDS");
			ds.Tables.Add(dt1);

			try {
				//open up file dialog to save file....
				//then call createexcelfile to create the excel...
				SaveFileDialog saveFileDialog1 = new SaveFileDialog();

				saveFileDialog1.Filter = "Excel|*.xlsx";
				saveFileDialog1.FilterIndex = 2;
				saveFileDialog1.RestoreDirectory = true;

				bool? resultSaved = saveFileDialog1.ShowDialog();

				// Process save file dialog box results 
				if (resultSaved == true) {
					// Save document 
					filename = saveFileDialog1.FileName;
					CreateExcelFile.CreateExcelDocument(ds, filename);
				}

				//now open file....
				System.Diagnostics.Process.Start(filename);

				Messenger.Default.Send(new HidePopupMessage());
			} catch (Exception ex) {
				Messenger.Default.Send(new HidePopupMessage());
				return;
			}
		}
	}
}