﻿using AgC.UnitConversion;
using AgC.UnitConversion.Density;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Client.Models;
using Landdb.Client.Services.Documents;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.Domain.ReadModels.ProductSetting;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.Resources;
using Landdb.ViewModel.Resources.Inventory;
using Landdb.ViewModel.Resources.Popups;
using NLog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Threading;
using Landdb.Client.Localization;
using Landdb.ViewModel.Legal;

namespace Landdb.ViewModel.Resources
{
    public class InventoryDetailsViewModel : ViewModelBase {

		private readonly IClientEndpoint clientEndpoint;
        private readonly Dispatcher dispatcher;

        private readonly InventoryListItemModel listItemModel;
        private readonly InventoryDetailView detailsView;
        
        private readonly IDocumentService documentService;

        public EulaDescriptionViewModel EulaViewModel { get; }

        private bool isRefreshingDocuments;
        private bool isMasterlistProduct = false;

        private Logger log = LogManager.GetCurrentClassLogger();

        public InventoryDetailsViewModel(InventoryListItemModel listItemModel, InventoryDetailView detailsView, IClientEndpoint clientEndpoint, IDocumentService documentService, Dispatcher dispatcher) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

            this.listItemModel = listItemModel;
            this.detailsView = detailsView;            

            this.documentService = documentService;     
       
            EditUserCreatedProductNameCommand = new RelayCommand(EditUserCreatedProductName);
            EditUserCreatedProductFormulationCommand = new RelayCommand(EditFertilizerFormulation);
            EditAveragePriceUnit = new RelayCommand(EditDefaultAveragePriceUnit);
            EditProductUnitsCommand = new RelayCommand(SetDefaultProductUnits);
            AddCrop = new RelayCommand(AddCropREIPHIItem);
            ShowEULACommand = new RelayCommand(EULA);
            AcceptCommand = new RelayCommand(HideEula);
            EulaViewModel = new EulaDescriptionViewModel();

            IsMasterlistProduct = listItemModel.ProductId.DataSourceId == GlobalIdentifiers.GetMasterlistDataSourceId();

            ProductName = listItemModel.ProductName;
            Manufacturer = listItemModel.ManufacturerName;
            AverageCostValue = detailsView.AveragePriceValue;
            AverageCostUnit = detailsView.AveragePriceUnit != null ? clientEndpoint.GetProductUnitResolver().GetPackageSafeUnit(listItemModel.ProductId, detailsView.AveragePriceUnit) : clientEndpoint.GetProductUnitResolver().GetProductPackageUnit(listItemModel.ProductId);
            //TO DO :: THIS IS COMMENTED OUT UNTIL THE READ MODEL ISSUES IS CLEANED UP.
            //var rem = detailsView.TotalPurchasedValue - detailsView.TotalGrowerUsed;
            //RemainingQuantity = AverageCostUnit.GetMeasure(rem);  // TODO: Account for density?
            PurchasedQuantity = AverageCostUnit.GetMeasure(detailsView.TotalPurchasedValue); // TODO: Account for density?
            AppliedQuantity = AverageCostUnit.GetMeasure(detailsView.TotalAppliedValue); // TODO: Account for density?
			GrowerAppliedQuantity = AverageCostUnit.GetMeasure(detailsView.TotalGrowerUsed); // TODO: Account for density?

            //TO DO :: THIS VALUE IS NOT CORRECT... NEED TO EVALUATE THE READ MODEL AND FIX THIS VALUE... FOR NOW THE VALUE WILL BE CALCULATED FROM THE APPLICATIONS ON LINE 80
			//FromShareOwnerAppliedQuantity = AverageCostUnit.GetMeasure(AppliedQuantity.Value - GrowerAppliedQuantity.Value);

            var productResolver = clientEndpoint.GetProductUnitResolver();

            var iQ = from i in detailsView.Invoices
                     orderby i.InvoiceDate descending
                     select new InventoryInvoiceViewModel(i, listItemModel.ProductId, productResolver);
            Invoices = new List<InventoryInvoiceViewModel>(iQ);

            var aQ = from a in detailsView.Applications
                     orderby a.StartDateTime descending
                     select new InventoryApplicationViewModel(a, listItemModel.ProductId, productResolver, clientEndpoint);

            Applications = new List<InventoryApplicationViewModel>(aQ);
            
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //TO DO :: THIS IS A BAND-AID UNTIL THE INVENTORYDETAILSVIEW READMODEL IS FIXED  ~ INCORRECT GROWERAPPLIEDQUATITY & APPLIEDQUANTITY
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            decimal totalGrowerQty = 0;
            double totalProdQty = 0;

            var groupedByUnits = Applications.GroupBy(x => x.TotalProductQuantity.Unit);
            var theUnit = groupedByUnits.Any() ? groupedByUnits.First() : null;

            var mlp = clientEndpoint.GetMasterlistService().GetProduct(listItemModel.ProductId);
            var unitToConvertTo = theUnit != null ? UnitFactory.GetPackageSafeUnit(theUnit.Key.Name, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit) : AverageCostUnit;

            foreach (var appProd in Applications)
            {
                if (appProd.TotalProductQuantity.Unit == unitToConvertTo)
                {
                    //added this in to fix old data. In the past we were not properly handling FromGrowerInventory so we have some bad data showing up.  This Should fix the issue.
                    totalGrowerQty += appProd.FromGrowerInventoryPercentage != 1m ? appProd.GrowerProductQuantity : Convert.ToDecimal(appProd.TotalProductQuantity.Value);
                    totalProdQty += appProd.TotalProductQuantity.Value;
                }
                else
                {
                    var currentUnit = UnitFactory.GetPackageSafeUnit(appProd.TotalProductQuantity.Unit.Name, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);
                    var growerQty = currentUnit.GetMeasure(Convert.ToDouble(appProd.GrowerProductQuantity), mlp.Density);
                    var totalQty = currentUnit.GetMeasure(appProd.TotalProductQuantity.Value, mlp.Density);

                    var convertedGrowerQty = growerQty.Unit.Name != unitToConvertTo.Name && growerQty.CanConvertTo(unitToConvertTo) ? growerQty.GetValueAs(unitToConvertTo) : growerQty.Value;
                    var convertedTotalQty = totalQty.Unit.Name != unitToConvertTo.Name && totalQty.CanConvertTo(unitToConvertTo) ? totalQty.GetValueAs(unitToConvertTo) : totalQty.Value;

                    //added this in to fix old data. In the past we were not properly handling FromGrowerInventory so we have some bad data showing up.  This Should fix the issue.
                    totalGrowerQty += appProd.FromGrowerInventoryPercentage != 1m ? Convert.ToDecimal(convertedGrowerQty) : Convert.ToDecimal(convertedTotalQty);
                    totalProdQty += convertedTotalQty;
                }
            }

            FromShareOwnerAppliedQuantity = AverageCostUnit.GetMeasure(totalProdQty - Convert.ToDouble(totalGrowerQty));

            var preConverted = unitToConvertTo.GetMeasure((double)totalGrowerQty, mlp.Density);
            var convertedGrowerAppliedQty = unitToConvertTo != AverageCostUnit && AverageCostUnit != null && preConverted.CanConvertTo(AverageCostUnit) ? Convert.ToDecimal( preConverted.GetValueAs(AverageCostUnit) ) : totalGrowerQty;

            GrowerAppliedQuantity = AverageCostUnit.GetMeasure(Convert.ToDouble(convertedGrowerAppliedQty));

            var rem = detailsView.TotalPurchasedValue - GrowerAppliedQuantity.Value;
            RemainingQuantity = AverageCostUnit.GetMeasure(rem);  // TODO: Account for density?
            AppliedQuantity = unitToConvertTo.GetMeasure(totalProdQty, mlp.Density);
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            Documents = new ObservableCollection<InventoryDocumentViewModel>();
            RefreshDocuments();

            ReiPhis = new ObservableCollection<InventoryReiPhiItemViewModel>();
            RefreshReiPhis();
        }

        public RelayCommand EditUserCreatedProductNameCommand { get; }
		public RelayCommand EditUserCreatedProductFormulationCommand { get; }
		public RelayCommand EditProductUnitsCommand { get; }
		public RelayCommand AddCrop { get; }
        public RelayCommand ShowEULACommand { get; }
		public RelayCommand AcceptCommand { get; }
		public RelayCommand EditAveragePriceUnit { get; }

		public List<InventoryInvoiceViewModel> Invoices { get; }

		public ObservableCollection<InventoryDocumentViewModel> Documents { get; }
		public ObservableCollection<InventoryReiPhiItemViewModel> ReiPhis { get; private set; }

		public string ProductName { get; set; }
        public string Manufacturer { get; set; }
        public decimal AverageCostValue { get; set; }
        public IUnit AverageCostUnit { get; set; }
        public Measure PurchasedQuantity { get; set; }
        public Measure AppliedQuantity { get; set; }
		public Measure GrowerAppliedQuantity { get; set; }
		public Measure FromShareOwnerAppliedQuantity { get; set; }
        public Measure RemainingQuantity { get; set; }

		public bool IsUserCreatedProduct => listItemModel.IsUserCreated;
		public bool IsUserCreatedFertilizer => listItemModel.IsUserCreated && listItemModel.IsFertilizer;
		public string AverageCostText => $"{AverageCostValue:c} / {AverageCostUnit.FullDisplay}";

		public bool IsRefreshingDocuments {
            get { return isRefreshingDocuments; }
            private set {
                isRefreshingDocuments = value;
                RaisePropertyChanged(() => IsRefreshingDocuments);
            }
        }

		public string DensityText {
            get {
                if (listItemModel == null || listItemModel.Density == null || listItemModel.Density.Value <= 0) { return string.Empty; }
                var d = DataSourceCultureInformation.UsesImperialUnits ?
                    SpecificGravity.Self.GetMeasure(listItemModel.Density.Value).CreateAs(PoundsPerGallon.Self).ToString() :
                    $"{listItemModel.Density} {AgC.UnitConversion.MassAndWeight.Kilogram.Self.AbbreviatedDisplay} / {AgC.UnitConversion.Volume.Liter.Self.AbbreviatedDisplay}";
                return d;
            }
        }

		public bool IsMissingDocumentation {
			get {
				return listItemModel.ProductType == GlobalStrings.ProductType_CropProtection
					&& !Documents.Any()
					&& !IsRefreshingDocuments; // isrefreshing keeps icon from showing prematurely
			}
		}

        public bool IsMasterlistProduct {
            get { return isMasterlistProduct; }
            set {
                isMasterlistProduct = value;
                RaisePropertyChanged(() => IsMasterlistProduct);
            }
        }

        private List<InventoryApplicationViewModel> apps;
		public List<InventoryApplicationViewModel> Applications {
			get { return apps; }
			private set {
				apps = value;
				var sumOfApps = apps.Sum(x => x.TotalProductQuantity.Value);
			}
		}

		public class InventoryInvoiceViewModel {

            InventoryInvoiceLineItem model;
            ProductId productId;
            IProductUnitResolver unitResolver;

            public InventoryInvoiceViewModel(InventoryInvoiceLineItem model, ProductId productId, IProductUnitResolver unitResolver) {
                this.model = model;
                this.productId = productId;
                this.unitResolver = unitResolver;

                JumpToInvoiceCommand = new RelayCommand(JumpToInvoice);
            }

            public RelayCommand JumpToInvoiceCommand { get; }

			public DateTime InvoiceDate => model.InvoiceDate;
			public string InvoiceNumber => model.InvoiceNumber;
			public string Vendor => model.Vendor;
			public decimal ProductCost => model.ProductCost;

			public Measure ProductQuantity {
                get {
                    var unit = unitResolver.GetPackageSafeUnit(productId, model.ProductQuantityUnit);
                    return unit.GetMeasure((double)model.ProductQuantityValue); // TODO: Account for density?
                }
            }

            public string UnitPriceText {
				get {
					if (ProductQuantity != null) {
						return $"{(double)ProductCost / ProductQuantity.Value:C} {Strings.Per_Text.ToLower()} {ProductQuantity.Unit.AbbreviatedDisplay}";
					} else {
						return null;
					}
				}
            }

            void JumpToInvoice() {
                ViewRouter.JumpToInvoice(model.InvoiceId);
            }
        }

        public class InventoryApplicationViewModel {

			private readonly IClientEndpoint clientEndpoint;
			private readonly IProductUnitResolver unitResolver;

			private readonly InventoryApplicationLineItem model;
            private readonly ProductId productId;
		
            public InventoryApplicationViewModel(InventoryApplicationLineItem model, ProductId productId, IProductUnitResolver unitResolver, IClientEndpoint clientEndpoint) {
                this.model = model;
                this.productId = productId;
                this.unitResolver = unitResolver;
                this.clientEndpoint = clientEndpoint;

                var appDetails = clientEndpoint.GetView<Domain.ReadModels.Application.ApplicationView>(model.ApplicationId).Value;
                var appliedProduct = appDetails.Products.FirstOrDefault(x => x.Id == productId);

                FromGrowerInventoryPercentage = appliedProduct.FromGrowerInventory;

                JumpToApplicationCommand = new RelayCommand(JumpToApplication);
            }

            public RelayCommand JumpToApplicationCommand { get; }

			public DateTime StartDateTime => model.StartDateTime;
			public string ApplicationName => model.ApplicationName;
            public ApplicationId ApplicationId => model.ApplicationId;
            public decimal FromGrowerInventoryPercentage { get; set; }
            public decimal GrowerProductQuantity => model.TotalFromGrowerInventory;
			public decimal ShareOwnerProductQuantity => Convert.ToDecimal(TotalProductQuantity.Value) - GrowerProductQuantity;
			public string ShareDisplayText => ShareOwnerProductQuantity > 0 && FromGrowerInventoryPercentage != 1m ? $"{GrowerProductQuantity:N2} / {ShareOwnerProductQuantity:N2}" : string.Empty;

			public Measure ProductArea { 
                get {
                    var unit = UnitFactory.GetUnitByName(model.ProductAreaUnit);
                    return unit.GetMeasure((double)model.ProductAreaValue); 
                } 
            }

            public Measure TotalProductQuantity { 
                get {
					var unit = unitResolver.GetPackageSafeUnit(productId, model.ProductQuantityUnit);
					var density = unitResolver.GetDensity(productId);
                    var totalMeasure = unit.GetMeasure((double)(model.ProductQuantityValue), density); // Handled density
                    var displayMeasure = DisplayMeasureConverter.ConvertToTotalDisplayMeasure(productId, totalMeasure, clientEndpoint, ApplicationEnvironment.CurrentDataSourceId);
                    return displayMeasure;
                } 
            }

            public string RatePerAreaText {
                get {
                    if (model.ProductQuantityUnit == null || ProductArea == null || ProductArea.Unit == null || string.IsNullOrWhiteSpace(model.RatePerAreaUnit)) { return null; }
                    
                    var unit = unitResolver.GetPackageSafeUnit(productId, model.RatePerAreaUnit);
                    var measure = unit.GetMeasure((double)model.RatePerAreaValue); // TODO: Handle density?
                    var displayMeasure = DisplayMeasureConverter.ConvertToRateDisplayMeasure(productId, measure, clientEndpoint, ApplicationEnvironment.CurrentDataSourceId);

                    return $"{displayMeasure.AbbreviatedDisplay} / {ProductArea.Unit.AbbreviatedDisplay}";
                }
            }

            public string CoveragePercentText {
                get {
                    if (model.PercentOfTotalArea > 0 && model.PercentOfTotalArea != 1.00m) {
                        return String.Format(Strings.PercentCoverage_Format_Text, model.PercentOfTotalArea);
                    } else {
                        return null;
                    }
                }
            }

			public bool HasCoverage => (model.PercentOfTotalArea > 0 && model.PercentOfTotalArea != 1.00m);

			void JumpToApplication() {
                ViewRouter.JumpToApplication(model.ApplicationId);
            }
        }

        internal class DocumentEqualityComparer : IEqualityComparer<ProductDocument> {
			public bool Equals(ProductDocument x, ProductDocument y) => x.Id == y.Id;
			public int GetHashCode(ProductDocument obj) => obj.Id.GetHashCode();
		}

        async void RefreshDocuments() {
            try {
                await dispatcher.BeginInvoke(new Action(() => IsRefreshingDocuments = true));
                // TODO: Async document retrieval.
                // TODO: Don't show remote-only docs if we're offline
                var localDocuments = documentService.GetLocalDocuments(listItemModel.ProductId.Id);
                IList<ProductDocument> remoteDocuments = new List<ProductDocument>();
                if (NetworkStatus.IsInternetAvailableFast()) {
                    remoteDocuments = await documentService.GetRemoteDocuments(listItemModel.ProductId.Id);
                } else {
                    log.Info("Network was offline. Remote documents were skipped.");
                }

                log.Info("There were {0} local documents located for {1}.", localDocuments.Count, listItemModel.ProductName);
                log.Info("There were {0} remote documents located for {1}.", remoteDocuments.Count, listItemModel.ProductName);

                await dispatcher.BeginInvoke(new Action(() => {
                    var syncedDocuments = localDocuments.Intersect(remoteDocuments, new DocumentEqualityComparer());
                    var localOnlyDocuments = localDocuments.Except(remoteDocuments, new DocumentEqualityComparer());
                    var remoteOnlyDocuments = remoteDocuments.Except(localDocuments, new DocumentEqualityComparer());

                    Documents.Clear();

                    foreach (var d in localOnlyDocuments) {
                        Documents.Add(new InventoryDocumentViewModel(documentService, d, null, listItemModel.ProductName));
                    }

                    foreach (var d in syncedDocuments) {
                        var remoteDocument = (from rd in remoteDocuments where rd.Id == d.Id select rd).SingleOrDefault();
                        Documents.Add(new InventoryDocumentViewModel(documentService, d, remoteDocument, listItemModel.ProductName));
                    }

                    foreach (var d in remoteOnlyDocuments) {
                        Documents.Add(new InventoryDocumentViewModel(documentService, null, d, listItemModel.ProductName));
                    }

                    if (Documents.Any()) {
                        var q = from d in Documents
                                where d.DocumentDescription == "Label"
                                select Documents.IndexOf(d);
                        var labelIndex = q.FirstOrDefault();
                        Documents.Move(labelIndex, 0); // Move to top of the list.
                    }

                    IsRefreshingDocuments = false;
                    RaisePropertyChanged(() => Documents);
					RaisePropertyChanged(() => IsMissingDocumentation);
                }));
            } catch (Exception ex) {
                log.ErrorException($"Error while refreshing documents for {listItemModel.ProductName} ({listItemModel.ProductId})", ex);
            }
        }

        void RefreshReiPhis() {
            // TODO: async
            var ml = clientEndpoint.GetMasterlistService();
            ObservableCollection<InventoryReiPhiItemViewModel> reiPhiVM = new ObservableCollection<InventoryReiPhiItemViewModel>();
            var reiphis = ml.GetProductReiPhis(listItemModel.ProductId);
            var cropYearId = new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear);
            var cropTreeView = clientEndpoint.GetView<CropTreeView>(cropYearId).GetValue(() => null);
			var productSettingId = new ProductSettingId(ApplicationEnvironment.CurrentDataSourceId, listItemModel.ProductId);
            var productSettingsView = clientEndpoint.GetView<ProductSettingsView>(productSettingId).GetValue(() => null);

            if(cropTreeView != null) {
                var cropIds = cropTreeView.Crops.Select(x => x.CropId).ToList();
				var parentCropIds = from cid in cropIds
									let mlCrop = ml.GetCrop(cid)
									where mlCrop != null && mlCrop.ParentId.HasValue
									select new CropId(mlCrop.ParentId.Value);
                //cropIds.Union(parentCropIds);
                cropIds.AddRange(parentCropIds.ToList());
                
                foreach (var reiphi in reiphis) {
                    if (cropIds.Any(c => c.Id == reiphi.Crop))
                    {
                        // TODO: Might need an additional view to support this if we don't want to keep using the crop tree view.
                        reiPhiVM.Add(new InventoryReiPhiItemViewModel(clientEndpoint, dispatcher, reiphi, listItemModel.ProductId));
                    }
                }

                if (productSettingsView != null && productSettingsView.ItemsByYear.Any(x => x.Key <= ApplicationEnvironment.CurrentCropYear))
                {
                    var holder = (from i in productSettingsView.ItemsByYear where i.Key <= ApplicationEnvironment.CurrentCropYear select i).OrderByDescending(x => x.Key); //productSettingsView.ItemsByYear.Where(x => x.Key <= ApplicationEnvironment.CurrentCropYear).Max().Value;

                    foreach (var setting in holder.First().Value)
                    {
                        if (!reiphis.Any(x => x.Crop == setting.CropId.Id))
                        {
                            var newReiPhi = new ReiPhi();
                            newReiPhi.Crop = setting.CropId.Id;
                            newReiPhi.Phi = (double?)setting.PhiValue;
                            newReiPhi.PhiU = setting.PhiUnit;
                            newReiPhi.Rei = (double?)setting.ReiValue;
                            newReiPhi.ReiU = setting.ReiUnit;

                            InventoryReiPhiItemViewModel itemVM = new InventoryReiPhiItemViewModel(clientEndpoint, dispatcher, newReiPhi, listItemModel.ProductId);
                            itemVM.IsEdited = true;
                            itemVM.EditedOn = setting.LastModified.ToLocalTime();
                            reiPhiVM.Add(itemVM);
                        }
                        else
                        {
                            var editedReiPhi = reiPhiVM.FirstOrDefault(p => p.CropId == setting.CropId.Id);
                            if (editedReiPhi != null)
                            {
                                editedReiPhi.ReiValue = setting.ReiValue;
                                editedReiPhi.PhiValue = setting.PhiValue;
                                editedReiPhi.IsEdited = true;
                                editedReiPhi.EditedOn = setting.LastModified.ToLocalTime();
                            }
                            else
                            {
                                //product is labeled for rei phi but there is none in the tree.
                                var newReiPhi = new ReiPhi();
                                newReiPhi.Crop = setting.CropId.Id;
                                newReiPhi.Phi = (double?)setting.PhiValue;
                                newReiPhi.PhiU = setting.PhiUnit;
                                newReiPhi.Rei = (double?)setting.ReiValue;
                                newReiPhi.ReiU = setting.ReiUnit;

                                InventoryReiPhiItemViewModel itemVM = new InventoryReiPhiItemViewModel(clientEndpoint, dispatcher, newReiPhi, listItemModel.ProductId);
                                itemVM.IsEdited = true;
                                itemVM.EditedOn = setting.LastModified.ToLocalTime();
                                reiPhiVM.Add(itemVM);
                            }
                        }
                    }
                }

                //ReiPhis = new ObservableCollection<InventoryReiPhiItemViewModel>(reiPhiVM.OrderBy(x => x.CropName));
                if (listItemModel.ProductId.DataSourceId == GlobalIdentifiers.GetMasterlistDataSourceId()) {
                    ReiPhis = new ObservableCollection<InventoryReiPhiItemViewModel>(reiPhiVM.OrderBy(x => x.CropName));
                }
                else {
                    ReiPhis = new ObservableCollection<InventoryReiPhiItemViewModel>();
                }
                RaisePropertyChanged(() => ReiPhis);
            }
        }

        void SetDefaultProductUnits()
        {
            var vm = new EditDefaultProductUnitsViewModel(clientEndpoint, dispatcher, listItemModel.ProductId);
			Messenger.Default.Send(new ShowPopupMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Resources.Popups.EditDefaultProductUnitsView), "EditUnits", vm)
			});
        }

        void AddCropREIPHIItem()
        {
            //if (productId.DataSourceId == GlobalIdentifiers.GetMasterlistDataSourceId()) {
            //    throw DomainError.Named(DomainErrorNames.invalid_datasource_id, "Cannot create a user created product with a masterlist datasource ID");
            //}

            //var item = new InventoryReiPhiItemViewModel();
            var vm = new AddInventoryReiPhiItemViewModel(clientEndpoint, dispatcher, listItemModel.ProductId, ReiPhis, this);
			Messenger.Default.Send(new ShowPopupMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Resources.Popups.AddReiPhiView), "AddReiPhiView", vm)
			});
        }

        private void EULA()
        {
			Messenger.Default.Send(new ShowPopupMessage {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Prerelease.EulaDescriptionView), "eula", this)
			});
        }

        void HideEula()
        {
            Messenger.Default.Send(new HidePopupMessage());
        }

        void EditUserCreatedProductName() {
            if (IsUserCreatedProduct) {
                var vm = new EditUserCreatedProductNameViewModel(clientEndpoint, listItemModel.ProductId, listItemModel.ProductName, listItemModel.ManufacturerName, (name, manufacturer) => {
                    Messenger.Default.Send(new HidePopupMessage());
                    dispatcher.BeginInvoke(new Action(() => {
                        listItemModel.ProductName = name;
                        listItemModel.ManufacturerName = manufacturer;

                        ProductName = name;
                        Manufacturer = manufacturer;
                        RaisePropertyChanged(() => ProductName);
                        RaisePropertyChanged(() => Manufacturer);
                    }));
                });

				Messenger.Default.Send(new ShowPopupMessage() {
					ScreenDescriptor = new ScreenDescriptor(typeof(Views.Resources.Popups.EditUserCreatedProductNameView), "editNameView", vm)
				});
            }
        }

        void EditFertilizerFormulation() {
            if (IsUserCreatedFertilizer) {
                var p = clientEndpoint.GetMasterlistService().GetProduct(listItemModel.ProductId);
                if (p != null) {
                    var vm = new EditUserCreatedFertilizerFormulationViewModel(clientEndpoint, listItemModel.ProductId, p.Formulation, (decimal?)p.Density, (f) => {
                        Messenger.Default.Send(new HidePopupMessage());
                        //dispatcher.BeginInvoke(new Action(() => {
                        //}));
                        
                    });

					Messenger.Default.Send(new ShowPopupMessage() {
						ScreenDescriptor = new ScreenDescriptor(typeof(Views.Resources.Popups.EditUserCreatedFertilizerFormulationView), "editFertView", vm)
					});
                }
            }
        }
        
        void EditDefaultAveragePriceUnit()
        {
            //Open UI to Edit Average Price Unit
            if (!string.IsNullOrEmpty(detailsView.AveragePriceUnit))
            {
                var mlp = clientEndpoint.GetMasterlistService().GetProduct(listItemModel.ProductId);
                var unit = string.IsNullOrEmpty(detailsView.AveragePriceUnit) ? clientEndpoint.GetProductUnitResolver().GetProductPackageUnit(listItemModel.ProductId).Name : detailsView.AveragePriceUnit;
                var avgPriceUnit = UnitFactory.GetPackageSafeUnit(unit, mlp.StdUnit, mlp.StdFactor, mlp.StdPackageUnit);

                var vm = new EditDefaultAveragePriceUnitViewModel(clientEndpoint, listItemModel.ProductId, ApplicationEnvironment.CurrentCropYear, new DataSourceId(ApplicationEnvironment.CurrentDataSourceId), avgPriceUnit, detailsView);

				Messenger.Default.Send(new ShowPopupMessage() {
					ScreenDescriptor = new ScreenDescriptor(typeof(Views.Resources.Popups.EditAveragePriceUnitView), "editPriceUnitView", vm)
				});
            }
        }
    }
}