﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Resources {
    public class ActiveIngredientListItemModel {

        public ActiveIngredientListItemModel(ActiveIngredientId ingredientId, string name) {
            IngredientId = ingredientId;
            IngredientName = name;
        }

        public ActiveIngredientId IngredientId { get; set; }
        public string IngredientName { get; set; }
    }
}
