﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Resources.Contract.Rent {
	public class UpdateFlexPercentageViewModel : ViewModelBase {

		readonly IClientEndpoint clientEndpoint;
		readonly Dispatcher dispatcher;

		readonly double originalFlexPercentage;
		readonly RentContractFlexPercentageSources originalSource;

		readonly Action<UpdateFlexPercentageViewModel> changeFlexPercentage;

		public UpdateFlexPercentageViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, double originalFlexPercentage, RentContractFlexPercentageSources originalSource, Action<UpdateFlexPercentageViewModel> onFlexPercentageChanged) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			this.originalFlexPercentage = originalFlexPercentage;
			this.originalSource = originalSource;

			_flexPercentage = originalFlexPercentage;

			FlexPercentageSources = new Dictionary<RentContractFlexPercentageSources, string>() {
				{ RentContractFlexPercentageSources.None, string.Empty },
				{ RentContractFlexPercentageSources.TotalRevenue, Strings.OfTotalRevenue_Text },
				{ RentContractFlexPercentageSources.ProductionRevenue, Strings.OfProductionRevenue_Text },
				{ RentContractFlexPercentageSources.TotalQuantityAtFlatPrice, Strings.OfTotalQuantityAtFlatCropPrice_Text },
			};

			SelectedFlexPercentageSource = FlexPercentageSources.FirstOrDefault(x => x.Key == originalSource).Key;

			this.changeFlexPercentage += onFlexPercentageChanged;

			CompleteCommand = new RelayCommand(onComplete);
			CancelCommand = new RelayCommand(onCancel);
		}

		public ICommand CompleteCommand { get; private set; }
		public ICommand CancelCommand { get; private set; }

		public Dictionary<RentContractFlexPercentageSources, string> FlexPercentageSources { get; private set; }

		private double _flexPercentage;
		public double FlexPercentage {
			get { return _flexPercentage; }
			set {
				if (value != 0) {
					_flexPercentage = value / 100;
				} else {
					_flexPercentage = 0;
				}

				RaisePropertyChanged(() => FlexPercentage);
			}
		}

		public RentContractFlexPercentageSources SelectedFlexPercentageSource { get; set; }

		public bool HasChanges {
			get {
				return FlexPercentage != originalFlexPercentage
					|| SelectedFlexPercentageSource != originalSource;
			}
		}

		private void onComplete() {
			changeFlexPercentage(this);
		}

		private void onCancel() {
			changeFlexPercentage(null);
		}
	}
}