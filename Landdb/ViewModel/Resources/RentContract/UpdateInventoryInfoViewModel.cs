﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Contract;
using System;
using System.Windows.Input;
using System.Windows.Threading;

namespace Landdb.ViewModel.Resources.Contract.Rent {
	public class UpdateInventoryInfoViewModel : ViewModelBase {

		readonly IClientEndpoint clientEndpoint;
		readonly Dispatcher dispatcher;

		readonly IncludedRentContractInventoryInfo originalInventoryInfo;

		readonly Action<UpdateInventoryInfoViewModel> inventoryInfoUpdated;

		public UpdateInventoryInfoViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, IncludedRentContractInventoryInfo originalInventoryInfo, Action<UpdateInventoryInfoViewModel> onInventoryUpdated) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			this.originalInventoryInfo = originalInventoryInfo;

			InventoryInfo = new IncludedInventoryInfoViewModel(originalInventoryInfo);

			this.inventoryInfoUpdated += onInventoryUpdated;

			CompleteCommand = new RelayCommand(onComplete);
			CancelCommand = new RelayCommand(onCancel);
		}

		public ICommand CompleteCommand { get; private set; }
		public ICommand CancelCommand { get; private set; }

		public IncludedInventoryInfoViewModel InventoryInfo { get; private set; }

		public bool HasChanges {
			get { return InventoryInfo.GetIncludedInventoryInfoObject() != originalInventoryInfo; }
		}

		private void onComplete() {
			inventoryInfoUpdated(this);
		}

		private void onCancel() {
			inventoryInfoUpdated(null);
		}
	}
}