﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using System;
using System.Windows.Input;
using System.Windows.Threading;

namespace Landdb.ViewModel.Resources.Contract.Rent {
	public class UpdateActualYieldViewModel : ViewModelBase {

		readonly IClientEndpoint clientEndpoint;
		readonly Dispatcher dispatcher;

		readonly double originalActualYield;

		readonly Action<UpdateActualYieldViewModel> actualYieldUpdated;

		public UpdateActualYieldViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, double originalActualYield, Action<UpdateActualYieldViewModel> onActualYieldUpdated) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			this.originalActualYield = originalActualYield;

			this.actualYieldUpdated += onActualYieldUpdated;

			CancelCommand = new RelayCommand(onCancel);
			CompleteCommand = new RelayCommand(onComplete);
		}

		public ICommand CompleteCommand { get; private set; }
		public ICommand CancelCommand { get; private set; }

		public double ActualYield { get; set; }

		public bool HasChanges {
			get { return ActualYield != originalActualYield; }
		}

		private void onComplete() {
			actualYieldUpdated(this);
		}

		private void onCancel() {
			actualYieldUpdated(null);
		}
	}
}