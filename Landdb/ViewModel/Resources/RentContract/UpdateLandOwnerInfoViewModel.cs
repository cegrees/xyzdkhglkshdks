﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Company;
using Landdb.Domain.ReadModels.Person;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Resources.Contract.Rent {
	public class UpdateLandOwnerInfoViewModel : ViewModelBase {

		readonly IClientEndpoint clientEndpoint;
		readonly Dispatcher dispatcher;

		readonly LandOwner originalLandOwner;
		readonly DateTime originalRentDue;
		readonly double originalPerAcre;

		readonly Action<UpdateLandOwnerInfoViewModel> landOwnerInfoUpdated;

		public UpdateLandOwnerInfoViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, CropYearId currentCropYearId, DateTime originalRentDue, double originalPerAcre, LandOwner originalLandOwner, Action<UpdateLandOwnerInfoViewModel> onLandOwnerInfoUpdated) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			this.originalRentDue = originalRentDue;
			this.originalPerAcre = originalPerAcre;
			this.originalLandOwner = originalLandOwner;

			buildLandOwnerList(currentCropYearId);

			RentDue = originalRentDue;
			PerAcre = originalPerAcre;

			if (originalLandOwner != null) {
				var matchingListItems = from l in LandOwnerList
										where (l.Group == Strings.People_Text && l.PersonId == originalLandOwner.PersonId)
										  || (l.Group == Strings.Companies_Text && l.CompanyId == originalLandOwner.CompanyId)
										select l;

				SelectedLandOwner = matchingListItems.FirstOrDefault();
			}

			landOwnerInfoUpdated += onLandOwnerInfoUpdated;

			CompleteCommand = new RelayCommand(onComplete);
			CancelCommand = new RelayCommand(onCancel);
		}

		public ICommand CompleteCommand { get; private set; }
		public ICommand CancelCommand { get; private set; }

		public List<ContractorListItemViewModel> LandOwnerList { get; private set; }

		public ContractorListItemViewModel SelectedLandOwner { get; set; }
		public double PerAcre { get; set; }
		public DateTime RentDue { get; set; }

		public ICollectionView LandOwnerDisplayCollection {
			get {
				var collectionView = CollectionViewSource.GetDefaultView(LandOwnerList.OrderBy(x => x.Group).ThenBy(x => x.Name));
				collectionView.GroupDescriptions.Add(new PropertyGroupDescription("Group"));
				return collectionView;
			}
		}

		public bool HasChanges {
			get {
				return RentDue != originalRentDue
					|| PerAcre != originalPerAcre
					|| GetLandOwnerValueObject() != originalLandOwner;
			}
		}

		public LandOwner GetLandOwnerValueObject() {
			LandOwner retLandOwner = null;

			if (SelectedLandOwner == null) {
				// leave it null
			} else if (SelectedLandOwner.Group == Strings.People_Text) {
				retLandOwner = new LandOwner(SelectedLandOwner.PersonId, SelectedLandOwner.Name, SelectedLandOwner.PhoneNumber);
			} else if (SelectedLandOwner.Group == Strings.Companies_Text) {
				retLandOwner = new LandOwner(SelectedLandOwner.CompanyId, SelectedLandOwner.Name, SelectedLandOwner.PhoneNumber);
			} else {
				// bad group?
			}

			return retLandOwner;
		}

		private void onComplete() {
			landOwnerInfoUpdated(this);
		}

		private void onCancel() {
			landOwnerInfoUpdated(null);
		}

		private void buildLandOwnerList(CropYearId currentCropYearId) {
			var dataSourceId = new DataSourceId(currentCropYearId.DataSourceId);

			var newLandOwnerList = new List<ContractorListItemViewModel>();

			var personListView = clientEndpoint.GetView<PersonListView>(dataSourceId).GetValue(new PersonListView());
			var sortedFilteredPersons = from p in personListView.Persons
										where p.IsActiveInCropYear(currentCropYearId.Id)
										orderby p.Name
										select p;

			sortedFilteredPersons.For_Each(person => {
				newLandOwnerList.Add(new ContractorListItemViewModel(person));
			});

			var companyListView = clientEndpoint.GetView<CompanyListView>(dataSourceId).GetValue(new CompanyListView());
			var sortedFilteredCompanies = from c in companyListView.Companies
										  where c.IsActiveInCropYear(currentCropYearId.Id)
										  orderby c.Name
										  select c;

			sortedFilteredCompanies.ForEach(company => {
				newLandOwnerList.Add(new ContractorListItemViewModel(company));
			});

			LandOwnerList = newLandOwnerList.OrderBy(x => x.Group).ThenBy(x => x.Name).ToList();
		}
	}
}