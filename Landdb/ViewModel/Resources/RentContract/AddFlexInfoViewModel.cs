﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure.Messages;
using AgC.UnitConversion;
using Landdb.ViewModel.Yield;
using Landdb.Infrastructure;
using Landdb.Domain.ReadModels.Tree;
using System.ComponentModel.DataAnnotations;
using Landdb.Domain.ReadModels.Contract;
using Landdb.Resources;

namespace Landdb.ViewModel.Resources.Contract.Rent {
	public class AddFlexInfoViewModel : ValidationViewModelBase {

		readonly IClientEndpoint clientEndpoint;
		readonly Dispatcher dispatcher;

		readonly DataSourceId currentDataSourceId;
		readonly int currentCropYear;
		readonly CropId cropId;

		readonly ContractId contractId;

		readonly Action<FlexLevel> flexLevelCreated;

		public AddFlexInfoViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, ContractId contractId, CropId cropId, Action<FlexLevel> onFlexLevelCreated) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			this.contractId = contractId;
			this.cropId = cropId;

			this.flexLevelCreated += onFlexLevelCreated;
            this.currentDataSourceId = new DataSourceId(ApplicationEnvironment.CurrentDataSourceId);
            this.currentCropYear = ApplicationEnvironment.CurrentCropYear;

			CreateNewCommand = new RelayCommand(onCreate);
			CancelCommand = new RelayCommand(onCancel);
		}

		public ICommand CreateNewCommand { get; private set; }
		public ICommand CancelCommand { get; private set; }

		public IEnumerable<CompositeUnit> YieldUnitList {
			get {
				var cyId = new CropYearId(currentDataSourceId.Id, currentCropYear);
				return CompositeUnitHelper.GetWeightUnitsForCrop(clientEndpoint, dispatcher, cyId, cropId).OrderBy(x => x.FullDisplay);
			}
		}

		private double level;
		[Range(1, 1000000)]
		public double Level {
			get { return level; }
			set {
				level = value;
				RaisePropertyChanged(() => Level);
			}
		}

		private CompositeUnit unit;
        [Required(AllowEmptyStrings = false, ErrorMessageResourceName = "UnitIsRequired_Text", ErrorMessageResourceType = typeof(Strings))]
        public CompositeUnit SelectedUnit {
			get { return unit; }
			set {
				unit = value;
				RaisePropertyChanged(() => SelectedUnit);
			}
		}

		private double bonus;
		public double Bonus {
			get { return bonus; }
			set {
				if (value > 1.0) {
					bonus = value / 100;
				} else {
					bonus = value;
				}
				RaisePropertyChanged(() => Bonus);
			}
		}

		public bool IsValid {
			get { return ValidateViewModel() && Level > 0 && SelectedUnit != null; }
		}

		private void onCreate() {
			if (IsValid) {
				FlexLevel flexLevel = new FlexLevel(contractId, Level, SelectedUnit, Bonus);
				flexLevelCreated(flexLevel);
			}
		}

		private void onCancel() {
			flexLevelCreated(null);
		}
	}
}