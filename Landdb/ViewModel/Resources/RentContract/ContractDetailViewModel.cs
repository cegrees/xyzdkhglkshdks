﻿//using GalaSoft.MvvmLight;
//using Landdb.Client.Infrastructure;
//using Landdb.Client.Spatial;
//using Microsoft.Maps.MapControl.WPF;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using System.Windows.Shapes;
//using System.Windows.Threading;
//using ThinkGeo.MapSuite.Core;
//using Landdb.Infrastructure.Mapping;
//using System.Windows.Media;
//using System.Windows.Media.Imaging;

//namespace Landdb.ViewModel.Resources.Agreement
//{

//	public class ContractDetailViewModel : ViewModelBase
//	{
//		Path fieldPreview;
//		MapLayer layer;
//		bool shouldShowBingMaps;
//		bool shouldShowWpfMock;
//		bool hasNoMapData;
//		Dispatcher dispatcher;

//		public ContractDetailViewModel(IEnumerable<string> mapData, Dispatcher dispatcher)
//		{
//			if (!mapData.Any())
//			{
//				HasNoMapData = true;
//			}
//			else
//			{
//				ShouldShowBingMaps = NetworkStatus.IsInternetAvailableFast();
//				ShouldShowWpfMock = !ShouldShowBingMaps;

//				dispatcher.BeginInvoke(new Action(() =>
//				{
//					if (ShouldShowBingMaps)
//					{
//						layer = BingMapsUtility.GetLayerForMapData(mapData);
//					}
//					else
//					{
//						fieldPreview = WpfTransforms.CreatePathFromMapData(mapData);
//					}
//				}));
//			}
//		}

//		public Path FieldPreview {
//			get { return fieldPreview; }
//		}

//		public MapLayer BingMapsLayer {
//			get { return layer; }
//		}

//		public bool ShouldShowBingMaps {
//			get { return shouldShowBingMaps; }
//			private set {
//				shouldShowBingMaps = value;
//				RaisePropertyChanged("ShouldShowBingMaps");
//			}
//		}

//		public bool ShouldShowWpfMock {
//			get { return shouldShowWpfMock; }
//			set {
//				shouldShowWpfMock = value;
//				RaisePropertyChanged("ShouldShowWpfMock");

//			}
//		}

//		public bool HasNoMapData {
//			get { return hasNoMapData; }
//			set {
//				hasNoMapData = value;
//				RaisePropertyChanged("HasNoMapData");

//				ShouldShowBingMaps = false;
//				ShouldShowWpfMock = false;
//			}
//		}
//	}
//}
