﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using Landdb.Client.Infrastructure;
using AgC.UnitConversion;

namespace Landdb.ViewModel.Resources.Contract.Rent {
	public class FlexLevelListItemViewModel : ViewModelBase {
		FlexLevel flex;

		public FlexLevelListItemViewModel(FlexLevel flex) {
			this.flex = flex;
		}

		public ContractId Id { get { return flex.ContractID; } }
		public double Level { get { return flex.LevelValue; } }
		public CompositeUnit Unit { get { return flex.LevelUnit; } }
		public double Bonus { get { return flex.BonusPercent; } }
	}
}
