﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Shared;
using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace Landdb.ViewModel.Resources.Contract.Rent {
	public class UpdateShareInfoViewModel : ViewModelBase {

		readonly IClientEndpoint clientEndpoint;
		readonly Dispatcher dispatcher;

		readonly IncludedContractShareInfo originalShareInfo;

		readonly Action<UpdateShareInfoViewModel> shareInfoUpdated;

		public UpdateShareInfoViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, IncludedContractShareInfo originalShareInfo, bool hasCropShare, Action<UpdateShareInfoViewModel> onShareInfoUpdated) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			this.originalShareInfo = originalShareInfo;

			CropShareVisibility = hasCropShare ? Visibility.Visible : Visibility.Collapsed;

			ShareInfo = new IncludedShareInfoViewModel(originalShareInfo);

			this.shareInfoUpdated += onShareInfoUpdated;

			CompleteCommand = new RelayCommand(onComplete);
			CancelCommand = new RelayCommand(onCancel);
		}

		public ICommand CompleteCommand { get; private set; }
		public ICommand CancelCommand { get; private set; }

		public Visibility CropShareVisibility { get; private set; }

		public IncludedShareInfoViewModel ShareInfo { get; set; }		

		public bool HasChanges {
			get { return ShareInfo.GetShareInfoObject() != originalShareInfo; }
		}

		private void onComplete() {
			shareInfoUpdated(this);
		}

		private void onCancel() {
			shareInfoUpdated(null);
		}
	}
}
