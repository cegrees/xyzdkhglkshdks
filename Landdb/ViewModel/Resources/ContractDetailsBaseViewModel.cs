﻿using AgC.UnitConversion.Area;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Client.Spatial;
using Landdb.Domain.ReadModels.Map;
using Landdb.Domain.ReadModels.Shared;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Resources.Contract.Rent;
using Landdb.ViewModel.Shared;
using NLog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Resources.Contract {
	public abstract class ContractDetailsBaseViewModel : ViewModelBase {

		protected readonly IClientEndpoint clientEndpoint;
		protected readonly Dispatcher dispatcher;

		protected readonly CropYearId currentCropYearId;

		protected readonly ContractId contractId;

		protected readonly Logger log = LogManager.GetCurrentClassLogger();

		public ContractDetailsBaseViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, int currentCropYear, ContractId contractId, string contractName, string contractType, int initialCropYear, IncludedContractShareInfo shareInfo, IEnumerable<IncludedCropZone> cropZones, string notes) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;
            SourceName = string.Empty;

            currentCropYearId = new CropYearId(contractId.DataSourceId, currentCropYear);

			this.contractId = contractId;

			Name = contractName;
			InitialCropYear = initialCropYear;
		    ContractType = new Func<string>(() => {

                switch (contractType.ToLower()) {
		            case "rent":
    		            return Strings.RentContract_Text;
                    case "production":
                        return Strings.ProductionContract_Text;
                    default:
                        return "error";

		        }} )();
            Notes = notes;

			IsRentContract = contractType.ToLower() == "rent";
			IsProductionContract = contractType.ToLower() == "production";
            IsReadOnly = false;

			ShareInfo = new IncludedShareInfoViewModel(shareInfo);

			if (IsRentContract) {
				ShareInfo.CalculateShareDollars(clientEndpoint, currentCropYearId, initialCropYear, cropZones.Select(x => x.Id.Id), contractId);
			}

			refreshCropZones(cropZones);

			ChangeContractNameCommand = new RelayCommand(onChangeContractName);
			ChangeCropZoneSelectionCommand = new RelayCommand(onChangeCropZones);
			UpdateShareInfoCommand = new RelayCommand(onUpdateShareInfo);
			UpdateContractNotesCommand = new RelayCommand(onUpdateContractNotes);
		}

		public ICommand ChangeContractNameCommand { get; }
		public ICommand ChangeCropZoneSelectionCommand { get; }
		public ICommand UpdateShareInfoCommand { get; }
		public ICommand UpdateContractNotesCommand { get; }

		public string ContractType { get; }
		public int InitialCropYear { get; }

		public bool IsRentContract { get; }
		public bool IsProductionContract { get; }
        public bool IsReadOnly { get; set; }

        public string Name { get; private set; }
        public string SourceName { get; set; }
        public string Notes { get; private set; }

		public IncludedShareInfoViewModel ShareInfo { get; private set; }

		public ObservableCollection<CropZoneDetails> CropZones { get; private set; }

		public string NotesDisplay => HasNotes ? Notes : $"[{Strings.AddNotes_Text}]";
		public bool HasNotes => !string.IsNullOrWhiteSpace(Notes);

		private void onChangeContractName() {
			if (contractId != null && !IsReadOnly) {
				DialogFactory.ShowNameChangeDialog(Strings.ContractName_Text, Name, newName => {
					IDomainCommand cmd = null;

					if (IsProductionContract) {
						cmd = new UpdateProductionContractName(contractId, clientEndpoint.GenerateNewMetadata(), newName);
					} else if (IsRentContract) {
						cmd = new UpdateContractName(contractId, clientEndpoint.GenerateNewMetadata(), newName);
					} else {
						// wtf? new contract type?
						log.Error("Encountered unhandled contract type while attempting to change name");
					}

					if (cmd != null) {
						clientEndpoint.SendOne(cmd);

						Name = newName;
						RaisePropertyChanged(() => Name);
					}
				});
			}
		}

		private void onUpdateContractNotes() {
			if (contractId != null && !IsReadOnly) {
				DialogFactory.ShowNotesChangeDialog(Notes, newNotes => {
					IDomainCommand cmd = null;

					if (IsProductionContract) {
						cmd = new UpdateProductionContractNotes(contractId, clientEndpoint.GenerateNewMetadata(), newNotes);
					} else if (IsRentContract) {
						// TODO: implement command/event
					} else {
						// wtf? new contract type?
						log.Error("Encountered unhandled contract type while attempting to change notes");
					}

					if (cmd != null) {
						clientEndpoint.SendOne(cmd);

						Notes = newNotes;
						RaisePropertyChanged(() => Notes);
						RaisePropertyChanged(() => HasNotes);
						RaisePropertyChanged(() => NotesDisplay);
					}
				});
			}
		}

		private void onChangeCropZones() {
			if (contractId != null && !IsReadOnly) {
				var vm = new Popups.ChangeContractCropZonesViewModel(clientEndpoint, dispatcher, currentCropYearId.Id, contractId, CropZones.ToList(), changeVm => {
					if (changeVm != null && changeVm.HasChanges) {
                        changeVm.UsePercentCoverage = true;
						IDomainCommand cmd = null;

						if (IsProductionContract) {
							cmd = new ChangeCropZonesIncludedInProductionContract(contractId, clientEndpoint.GenerateNewMetadata(), changeVm.AddedCropZones.ToArray(), changeVm.RemovedCropZones.ToArray(), changeVm.ChangedCropZones.ToArray());
						} else if (IsRentContract) {
							cmd = new ChangeCropZonesIncludedInRentContract(contractId, clientEndpoint.GenerateNewMetadata(), changeVm.AddedCropZones.ToArray(), changeVm.RemovedCropZones.ToArray(), changeVm.ChangedCropZones.ToArray());
						} else {
							// wtf? new contract type?
							log.Error("Encountered unhandled contract type while attempting to change crop zones");
						}

						if (cmd != null) {
							clientEndpoint.SendOne(cmd);

							var treeView = clientEndpoint.GetView<FlattenedTreeHierarchyView>(currentCropYearId).GetValue(new FlattenedTreeHierarchyView());

							var includedCropZones = new List<IncludedCropZone>();
							foreach (var cz in changeVm.SelectedCropZones) {
								string czName = cz.Name;

								var treeItem = treeView.Items.FirstOrDefault(x => x.CropZoneId == cz.Id);
								if (treeItem != null) {
									var treeItemName = treeItem.GetFullName();

									if (!string.IsNullOrWhiteSpace(treeItemName)) { czName = treeItemName; }
								}

								includedCropZones.Add(new IncludedCropZone() {
									Id = cz.Id,
									Name = czName,
									Area = cz.SelectedArea as AreaMeasure,
								});
							}

							refreshCropZones(includedCropZones);
						}
					}

					Messenger.Default.Send(new HideOverlayMessage());
				});

			    vm.IsRentContract = IsRentContract;
                vm.UsePercentCoverage = false;
				Messenger.Default.Send(new ShowOverlayMessage() {
					ScreenDescriptor = new ScreenDescriptor(typeof(Views.Resources.Contract.ChangeContractCropZonesView), "change cropzones", vm)
				});
                //vm.UsePercentCoverage = true;
			}
		}

		public void onUpdateShareInfo() {
			if (contractId != null && !IsReadOnly) {
				var hasCropShare = IsRentContract;

				var vm = new UpdateShareInfoViewModel(clientEndpoint, dispatcher, ShareInfo.GetShareInfoObject(), hasCropShare, changeVm => {
					if (changeVm != null && changeVm.HasChanges) {
						var newShareInfo = changeVm.ShareInfo.GetShareInfoObject();

						IDomainCommand cmd = null;

						if (IsRentContract) {
							cmd = new UpdateContractShareInfo(
								contractId,
								clientEndpoint.GenerateNewMetadata(),
								newShareInfo.GrowerCropProtectionShare,
								newShareInfo.GrowerFertilizerShare,
								newShareInfo.GrowerSeedShare,
								newShareInfo.GrowerServiceShare,
								newShareInfo.GrowerCropShare
							);
						} else if (IsProductionContract) {
							cmd = new UpdateProductionContractShareInfo(
								contractId,
								clientEndpoint.GenerateNewMetadata(),
								newShareInfo.GrowerCropProtectionShare,
								newShareInfo.GrowerFertilizerShare,
								newShareInfo.GrowerSeedShare,
								newShareInfo.GrowerServiceShare
							);
						} else {
							// wtf? new contract type?
							log.Error("Encountered unhandled contract type while attempting to change share info");
						}

						if (cmd != null) {
							clientEndpoint.SendOne(cmd);

							ShareInfo = changeVm.ShareInfo;
							RaisePropertyChanged(() => ShareInfo);

							if (IsRentContract) {
								ShareInfo.CalculateShareDollars(clientEndpoint, currentCropYearId, InitialCropYear, CropZones.Select(x => x.Id.Id), contractId);
							}
						}
					}

					Messenger.Default.Send(new HidePopupMessage());
				});

				Messenger.Default.Send(new ShowPopupMessage() {
					ScreenDescriptor = new ScreenDescriptor(typeof(Views.Resources.Contract.Rent.Popup.EditShareInfoView), "editShareInfo", vm)
				});
			}
		}

		private void refreshCropZones(IEnumerable<IncludedCropZone> cropZones) {
			CropZones = new ObservableCollection<CropZoneDetails>();

			var flattenedTreeView = clientEndpoint.GetView<FlattenedTreeHierarchyView>(currentCropYearId);

			foreach (var cz in cropZones) {
				var mapItem = clientEndpoint.GetView<ItemMap>(cz.Id);
				string mapData = null;
				System.Windows.Shapes.Path shapePreview = null;
				var czDetails = clientEndpoint.GetView<CropZoneDetailsView>(cz.Id).HasValue ? clientEndpoint.GetView<CropZoneDetailsView>(cz.Id).Value : null;
				double area = 0.0;

				if (czDetails != null) {
					area = czDetails.ReportedArea != null ? (double)czDetails.ReportedArea : czDetails.BoundaryArea != null ? (double)czDetails.BoundaryArea : 0;
				} else {
					area = cz.Area.Value;
				}

				var name = cz.Name;

				if (flattenedTreeView.HasValue) {
					var fti = flattenedTreeView.Value.Items.Where(x => x.CropZoneId == cz.Id).FirstOrDefault();
					if (fti != null) { name = fti.GetFullName(); }
				}

				double coveragePercent = area != 0 ? (cz.Area.Value / area) * 100 : 0;

				if (mapItem.HasValue && mapItem.Value.MostRecentMapItem != null) {
					mapData = mapItem.Value.MostRecentMapItem.MapData;
					dispatcher.BeginInvoke(new Action(() => {
						shapePreview = WpfTransforms.CreatePathFromMapData(new[] { mapData }, 40);

						CropZones.Add(new CropZoneDetails() {
							Id = cz.Id,
							Name = name,
							Area = cz.Area,
							ShapePreview = shapePreview,
							Percent = $"{coveragePercent:N0}%"
						});
					}));
				} else {
					CropZones.Add(new CropZoneDetails() {
						Name = name,
						Area = cz.Area,
						Id = cz.Id,
						Percent = $"{coveragePercent:N0}%"
					});
				}
			}
		}
	}
}