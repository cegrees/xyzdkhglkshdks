﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Company;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Shared;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Resources {
	public class CompanyDetailsViewModel : ViewModelBase {

		private readonly IClientEndpoint clientEndpoint;
		private readonly Dispatcher dispatcher;

		private readonly CompanyId companyId;

		public CompanyDetailsViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, CompanyDetailsView detailsView) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			companyId = detailsView.Id;

			buildDetailsFromReadModel(detailsView);

			UpdateCompanyContactInfoCommand = new RelayCommand(onUpdateCompanyContactInfo);
			UpdateCompanyStreetAddressCommand = new RelayCommand(onUpdateCompanyStreetAddress);
			UpdateCompanyNotesCommand = new RelayCommand(onUpdateCompanyNotes);
		}

		public ICommand UpdateCompanyContactInfoCommand { get; }
		public ICommand UpdateCompanyStreetAddressCommand { get; }
		public ICommand UpdateCompanyNotesCommand { get; }

		public string Name { get; private set; }
		public string PrimaryPhoneNumber { get; private set; }
		public string EmailAddress { get; private set; }
		public StreetAddressViewModel StreetAddress { get; private set; }
		public string Notes { get; private set; }

		public string NotesHeader => HasNotes ? Strings.Notes_Text : $"[{Strings.AddNotes_Text}]";
		public bool HasNotes => !string.IsNullOrWhiteSpace(Notes);

		public string StreetAddressDisplayText {
			get {
				var streetAddressIsPopulated = StreetAddress != null && !string.IsNullOrWhiteSpace(StreetAddress.MultiLineDisplay);
				return streetAddressIsPopulated ? StreetAddress.MultiLineDisplay : $"[{Strings.SpecifyStreetAddress_Text}]";
			}
		}

		private void onUpdateCompanyContactInfo() {
			if (companyId != null) {
				DialogFactory.ShowContactInfoChangeDialog(Name, PrimaryPhoneNumber, EmailAddress, newInfo => {
					var newName = newInfo.Item1;
					var newPhone = newInfo.Item2;
					var newEmail = newInfo.Item3;

					clientEndpoint.SendOne(new UpdateCompanyContactInfo(
						companyId,
						clientEndpoint.GenerateNewMetadata(),
						newName,
						newPhone,
						newEmail
					));

					Name = newName;
					PrimaryPhoneNumber = newPhone != null ? newPhone.GetFormattedString() : null;
                    EmailAddress = newEmail != null ? newEmail.Address : null;

					RaisePropertyChanged(() => Name);
					RaisePropertyChanged(() => PrimaryPhoneNumber);
					RaisePropertyChanged(() => EmailAddress);
				});
			}
		}

		private void onUpdateCompanyStreetAddress() {
			if (companyId != null) {
				DialogFactory.ShowStreetAddressChangeDialog(StreetAddress.GetValueObject(), newAddress => {
					clientEndpoint.SendOne(new UpdateCompanyStreetAddressInfo(
						companyId,
						clientEndpoint.GenerateNewMetadata(),
						newAddress
					));

					StreetAddress = new StreetAddressViewModel(newAddress);
					base.RaisePropertyChanged(() => StreetAddress);
				});
			}
		}

		private void onUpdateCompanyNotes() {
			if (companyId != null) {
				DialogFactory.ShowNotesChangeDialog(Notes, newNotes => {
					clientEndpoint.SendOne(new UpdateCompanyNotes(
						companyId, 
						clientEndpoint.GenerateNewMetadata(),
						newNotes
					));

					Notes = newNotes;
					base.RaisePropertyChanged(() => Notes);
					base.RaisePropertyChanged(() => HasNotes);
					base.RaisePropertyChanged(() => NotesHeader);
				});
			}
		}

		private void buildDetailsFromReadModel(CompanyDetailsView detailsView) {
			Name = detailsView.Name;
			PrimaryPhoneNumber = new PhoneNumber("", detailsView.PrimaryPhoneNumber).GetFormattedString();
			EmailAddress = detailsView.PrimaryEmail;
			StreetAddress = new StreetAddressViewModel(detailsView.StreetAddress);
			Notes = detailsView.Notes;
		}
	}
}