﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GalaSoft.MvvmLight;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Account;
using NLog;
using System.Windows.Threading;
using Landdb.Client.Infrastructure;
using Landdb.Web.ServiceContracts.Data;
using System.Reflection;
using WPFLocalizeExtension.Extensions;
using Thinktecture.IdentityModel.Client;
using Landdb.Models;
using System.Threading.Tasks;
using Landdb.Resources;

namespace Landdb.ViewModel {
    public class StartupViewModel : ViewModelBase {
        Logger log = LogManager.GetCurrentClassLogger();
        private readonly ICloudClientFactory cloudClientFactory;
        private readonly IClientEndpoint clientEndpoint;
        private readonly Dispatcher dispatcher;

        string message;
        bool isBusy;

        public StartupViewModel() :
            this(App.CloudClientFactory, ServiceLocator.Get<IClientEndpoint>(), App.Current.Dispatcher) { }

        public StartupViewModel(ICloudClientFactory cloudClientFactory, IClientEndpoint clientEndpoint, Dispatcher dispatcher) {
            this.cloudClientFactory = cloudClientFactory;
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;

            AuthMigrationCheck migrationCheck = new AuthMigrationCheck(clientEndpoint);
            if (migrationCheck.ShouldPromptForAuthMigration()) {
                Landdb.Views.Secondary.PopUp.AuthMigrationInformationView view = new Views.Secondary.PopUp.AuthMigrationInformationView();
                view.Owner = App.Current.MainWindow;
                view.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterOwner;
                view.Show();
            }
        }

        public string Message {
            get { return this.message; }
            set {
                if (this.message != value) {
                    this.message = value;
                    this.RaisePropertyChanged(() => Message);
                }
            }
        }

        public bool IsBusy {
            get { return this.isBusy; }
            set {
                if (this.isBusy != value) {
                    this.isBusy = value;
                    this.RaisePropertyChanged(() => IsBusy);
                }
            }
        }

        public void OnLoginComplete(Controls.OAuth2.OAuthCredentials response) {
            if (response == null) { // offline mode
                var user = GetMostRecentUser();
                /* Shaky code... */
                var accountStatus = ServiceLocator.Get<IAccountStatus>();
                accountStatus.AuthorizationToken = null;
                accountStatus.ParseUserAccountData(user);
                accountStatus.IsWorkingOffline = true;

                Messenger.Default.Send<LoggedInMessage>(new LoggedInMessage() { UserAccountInformation = user ?? new UserAccount(), WorkingOffline = true });
                /* End shaking */

            } else { // online
                this.dispatcher.Invoke<Task>(
                    async () => {
                        try {
                            this.IsBusy = true;
                            this.Message = LocExtension.GetLocalizedValue<string>("Landdb:Strings:Startup_CheckingRegistration"); // "Checking user registration...";
                            log.Debug("Checking registration of signed in user.");
                            var registrationResults = await this.cloudClientFactory.ResolveRegistrationClient().CheckUserRegistration();

                            if (!registrationResults.HasError) {
                                if (registrationResults.IsRegistered) {
                                    log.Info("User was registered. Id={0} ", registrationResults.User.Id);
                                    //log.Info("User was registered. Id={0} {1} {2} ", registrationResults.User.Id, registrationResults.User.GivenName, registrationResults.User.Surname);
                                    //string asdf = registrationResults.User.ToString();
                                    SaveMostRecentUser(registrationResults.User);

                                    this.dispatcher.InvokeAsync(
                                        () => {
                                            this.IsBusy = false;
                                            this.Message = Strings.UserRegistered_Text;

                                            /* Shaky code... */
                                            var accountStatus = ServiceLocator.Get<IAccountStatus>();
                                            accountStatus.AuthorizationToken = response.Token;
                                            accountStatus.ParseUserAccountData(registrationResults.User);

                                            Messenger.Default.Send<LoggedInMessage>(new LoggedInMessage() { UserAccountInformation = registrationResults.User, WorkingOffline = false });
                                            /* End shaking */
                                        }
                                    );

                                } else {
                                    log.Info("User was not registered.");
                                    this.dispatcher.InvokeAsync(
                                        () => {
                                            this.IsBusy = false;
                                            this.Message = Strings.UserNotRegistered_Text;
                                        }
                                    );
                                }
                            } else { // error
                                log.ErrorException("An exception was encountered while checking user registration.", registrationResults.Exception);
                            }

                        } catch (Exception ex) {
                            log.ErrorException("Encountered an exception while instantiating the cloud client factory.", ex);
                            this.IsBusy = false;
                            this.Message = Strings.ServerConfigurationError_Text;
                        }
                    });
            }
        }

        void SaveMostRecentUser(UserAccount account) {
            var settingsId = new NullId();
            //clientEndpoint.SavePersistentEntity<UserAccount>(settingsId, account);
            var info = new LastUserSignInInformation() { UserAccount = account, LastLogin = DateTime.UtcNow};
            clientEndpoint.SavePersistentEntity<LastUserSignInInformation>(settingsId, info);
        }

        UserAccount GetMostRecentUser() {
            var settingsId = new NullId();
            //var account = clientEndpoint.GetPersistentView<UserAccount>(settingsId);
            var info = clientEndpoint.GetPersistentView<LastUserSignInInformation>(settingsId);

            return info.HasValue ? info.Value.UserAccount : null;
        }

		public string ApplicationVersionText => $"v. {Assembly.GetEntryAssembly().GetName().Version.ToString()}";
	}
}
