﻿using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.YieldLocation;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Secondary.Reports.Yield;
using Landdb.ViewModel.Secondary.Reports.Yield.Generators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Yield {
	public class YieldLocationsPageViewModel : AbstractListPage<YieldLocationListItemViewModel, YieldLocationDetailsViewModel> {

		public YieldLocationsPageViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher)
			: base(clientEndpoint, dispatcher) {

			FilterModel = new YieldLocationsPageFilterViewModel(clientEndpoint, new CropYearId(currentDataSourceId.Id, currentCropYear));

			Messenger.Default.Register<YieldLocationAddedMessage>(this, msg => {
				RefreshItemsList(() => {
					SelectedListItem = ListItems.FirstOrDefault(x => x.Id == msg.LastYieldLocationAdded);
				});
			});

			Messenger.Default.Register<YieldLocationDetailsUpdatedMessage>(this, msg => {
				SelectedListItem.UpdateFromDetails(msg.Details);
			});

			PrintReport = new RelayCommand(onPrintLocationsReport);
		}

		public ICommand PrintReport { get; }

		protected override IEnumerable<YieldLocationListItemViewModel> GetListItemModels() {
			var currentCropYearId = new CropYearId(currentDataSourceId.Id, currentCropYear);
			return YieldLocationListItemViewModel.GetListItemsForCropYear(clientEndpoint, currentCropYearId);
		}

		protected override async Task<ScreenDescriptor> CreateScreenDescriptorForNewItemCreation() {
			var locationId = new YieldLocationId(currentDataSourceId.Id, Guid.NewGuid());

			var vm = new Popups.AddYieldLocationViewModel(clientEndpoint, dispatcher, locationId, currentCropYear, li => {
				RefreshItemsList(() => {
					ListItems.Add(li);
					SelectedListItem = ListItems.SingleOrDefault(x => x.Id == li.Id);
				});
			});

			return new ScreenDescriptor(typeof(Views.Yield.Popups.AddYieldLocationView), Strings.ScreenDescriptor_AddYieldLocation_Text, vm);
		}

        protected override bool PerformItemRemoval(YieldLocationListItemViewModel selectedItem) {
            var removeCommand = new RemoveYieldLocationFromYear(selectedItem.Id, clientEndpoint.GenerateNewMetadata(), currentCropYear);
            clientEndpoint.SendOne(removeCommand);
            return true;
        }

		protected override IDomainCommand CreateSetCharmCommand(YieldLocationListItemViewModel selectedItem, string charmName) {
			// charms not implemented in yield locations currently
			throw new NotImplementedException();
		}

		protected override YieldLocationDetailsViewModel CreateDetailsModel(YieldLocationListItemViewModel selectedItem) {
			var locationsMaybe = clientEndpoint.GetView<YieldLocationDetailsView>(selectedItem.Id);

			if (locationsMaybe.HasValue) {
				return new YieldLocationDetailsViewModel(clientEndpoint, dispatcher, currentDataSourceId, currentCropYear, locationsMaybe.Value);
			} else {
				return null;
			}
		}

		public override bool CanDelete(YieldLocationListItemViewModel toDelete) => toDelete.LoadCount == 0 && !toDelete.SubLocationList.Any();

		private void onPrintLocationsReport() {
			var reportGenerator = new LocationSummaryReportGenerator(clientEndpoint, dispatcher, currentCropYear);

			var vm = new SingleYieldReportViewModel(clientEndpoint, dispatcher, reportGenerator);
			Messenger.Default.Send(new ShowOverlayMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Secondary.Reports.Work_Order.SingleWorkOrderView), Strings.ScreenDescriptor_ViewReport_Text, vm)
			});
		}

		protected override IEnumerable<RestorableItem> GetRestorableItems() {
			var yieldLocations = clientEndpoint.GetView<YieldLocationListView>(currentDataSourceId).GetValue(() => new YieldLocationListView());

			return from loc in yieldLocations.YieldLocations
				   where loc.RemovedCropYear.HasValue && loc.RemovedCropYear.Value <= currentCropYear
				   orderby loc.Name
				   select new RestorableItem(loc.Id.Id, loc.Name, $"{loc.YieldLocationType} {Strings.Location_Text}", loc.InitialCropYear, loc.RemovedCropYear.Value);
		}

		protected override void RestoreDeletedItem(Guid id) {
			// TODO: send cmd
		}
	}
}