﻿using GalaSoft.MvvmLight;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.YieldLocation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace Landdb.ViewModel.Yield {
	public class YieldLocationListItemViewModel : ViewModelBase, IEquatable<YieldLocationListItemViewModel> {
		public YieldLocationListItemViewModel(YieldLocationListItem li, int currentCropYear) {
			Id = li.Id;
			Name = li.Name;
			YieldLocationType = li.YieldLocationType;
			IsExcludedFromInventory = li.IsQuantityExcludedFromInventory;

			SubLocationList = new ObservableCollection<YieldLocationListItemViewModel>();

			refreshDescriptorAndNumber();

			if (li.AssociatedLoadIdsByCropYear.ContainsKey(currentCropYear)) {
				DirectlyAssociatedLoadIdList = from load in li.AssociatedLoadIdsByCropYear[currentCropYear]
											   select new LoadId(li.Id.DataSourceId, load);
			} else {
				DirectlyAssociatedLoadIdList = new List<LoadId>();
			}
		}

		public YieldLocationListItemViewModel(string name, YieldLocationTypes type, int cropYear) :
			this(new YieldLocationListItem() { Id = null, Name = name, YieldLocationType = type }, cropYear) { }

		public ICommand ManageSubLocationsCommand { get; }

		public YieldLocationTypes YieldLocationType { get; }
		public bool IsExcludedFromInventory { get; }
		public IEnumerable<LoadId> DirectlyAssociatedLoadIdList { get; }
		public ObservableCollection<YieldLocationListItemViewModel> SubLocationList { get; }

		public YieldLocationId Id { get; private set; }
		public YieldLocationListItemViewModel ParentLocation { get; private set; }
		public string Descriptor { get; private set; }
		public int? LocationNumber { get; private set; }

		public int LoadCount => DirectlyAssociatedLoadIdList.Count() + SubLocationList.Sum(x => x.LoadCount);
		public int Depth => 1 + ParentLocation?.Depth ?? 0;
		public Thickness Indentation => new Thickness(Depth * 10, 0, 0, 0);
		public string FullDisplay => ParentLocation != null ? $@"{ParentLocation.FullDisplay} \ {Name}" : Name;

		private string _name;
		public string Name {
			get { return _name; }
			set {
				_name = value;
				RaisePropertyChanged(() => Name);

				refreshDescriptorAndNumber();
			}
		}

        private MapAnnotationId mapAnnotationID;
        public MapAnnotationId MapAnnotationID
        {
            get { return mapAnnotationID; }
            set
            {
                mapAnnotationID = value;
                RaisePropertyChanged(() => MapAnnotationID);
            }
        }

        private bool _isSelected;
		public bool IsSelected {
			get { return _isSelected; }
			set {
				_isSelected = value;
				RaisePropertyChanged(() => IsSelected);
			}
		}

		private bool _isExpanded;
		public bool IsExpanded {
			get { return _isExpanded; }
			set {
				_isExpanded = value;
				RaisePropertyChanged(() => IsExpanded);
			}
		}

		public void UpdateFromDetails(YieldLocationDetailsViewModel detailsModel) {
			Name = detailsModel.Name;
            MapAnnotationID = detailsModel.MapAnnotationID;

            SubLocationList.Clear();
			foreach (var subLoc in detailsModel.SubLocationList) {
				SubLocationList.Add(subLoc);
			}
		}

		public void AddSubLocation(YieldLocationListItemViewModel subLocation) {
			subLocation.ParentLocation = this;
			SubLocationList.Add(subLocation);
		}

		public void RemoveParent() {
			ParentLocation.SubLocationList.Remove(this);
			ParentLocation = null;
		}

		public YieldLocationId GetIdForSaving(Guid dataSourceId) {
			if (Id == null) {
				Id = new YieldLocationId(dataSourceId, Guid.NewGuid());
			}

			return Id;
		}

		public static List<YieldLocationListItemViewModel> GetListItemsForCropYear(IClientEndpoint clientEndpoint, CropYearId cropYearId) {
			var orderedLocations = clientEndpoint.GetView<YieldLocationListView>(new DataSourceId(cropYearId.DataSourceId))
				.GetValue(new YieldLocationListView())
				.YieldLocations
				.Where(x => x.IsActiveInCropYear(cropYearId.Id))
				.OrderBy(x => x.Name);

			var parentDic = new Dictionary<YieldLocationId, YieldLocationId>();
			var locDic = new Dictionary<YieldLocationId, YieldLocationListItemViewModel>();

			foreach (var loc in orderedLocations) {
				var li = new YieldLocationListItemViewModel(loc, cropYearId.Id);

				locDic.Add(loc.Id, li);
				parentDic.Add(loc.Id, loc.ParentLocationId);
			}

			foreach (var loc in locDic.Values.ToList()) {
				var parent = parentDic[loc.Id];

				if (parent != null) {
					var parentId = parentDic[loc.Id];

					if (locDic.ContainsKey(parentId)) {
						locDic[parentDic[loc.Id]].AddSubLocation(loc);
					}
				}
			}

			return locDic.Values.Where(x => x.ParentLocation == null).ToList();
		}

		public static List<YieldLocationListItemViewModel> GetFlattenedListItemsForCropYear(IClientEndpoint clientEndpoint, CropYearId cropYearId, bool shouldInsertEmptyEntryAtTop) {
			var locationList = GetListItemsForCropYear(clientEndpoint, cropYearId);

			if (shouldInsertEmptyEntryAtTop && locationList.Any()) {
				locationList.Insert(0, new YieldLocationListItemViewModel(string.Empty, YieldLocationTypes.Unknown, 0));
			}

			return GetFlattenedList(locationList);
		}

		public static List<YieldLocationListItemViewModel> GetFlattenedList(IEnumerable<YieldLocationListItemViewModel> locationList) {
			var retList = new List<YieldLocationListItemViewModel>();

			foreach (var loc in locationList) {
				retList.Add(loc);
				retList.AddRange(GetFlattenedList(loc.SubLocationList));
			}

			return retList;
		}

		public bool Equals(YieldLocationListItemViewModel other) {
			if (ReferenceEquals(null, other)) { return false; }
			if (ReferenceEquals(this, other)) { return true; }

			return Id == other.Id;
		}

		public override string ToString() => FullDisplay;

		private static int getDepth(Dictionary<YieldLocationId, YieldLocationId> parentDic, YieldLocationId locationId) {
			var depth = 1;

			var idToCheck = locationId;

			while (parentDic[idToCheck] != null) {
				depth++;
				idToCheck = parentDic[idToCheck];
			}

			return depth;
		}

		private void refreshDescriptorAndNumber() {
			LocationNumber = null;

			var nonNumericTokens = new List<string>();

			var stringTokens = Name.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

			foreach (var token in stringTokens) {
				int parsedVal;

				if (int.TryParse(token, out parsedVal)) {
					if (LocationNumber == null) {
						LocationNumber = parsedVal;
					} else {
						// we found a second number in the name. ignore it
					}
				} else {
					nonNumericTokens.Add(token);
				}
			}

			Descriptor = string.Join(" ", nonNumericTokens);
		}
	}
}