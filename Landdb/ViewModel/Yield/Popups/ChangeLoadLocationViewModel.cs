﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;

namespace Landdb.ViewModel.Yield.Popups {
	public class ChangeLoadLocationViewModel : ViewModelBase {

		private readonly Action<ChangeLoadLocationViewModel> locationChanged;

		private readonly YieldLocationListItemViewModel originalLocation;
		private readonly YieldLocationListItemViewModel originalFromSubLocation;
		private readonly YieldLocationListItemViewModel originalToSubLocation;

		public ChangeLoadLocationViewModel(
			IClientEndpoint clientEndpoint,
			IEnumerable<YieldLocationListItemViewModel> availableTopLevelLocationsList, 
			YieldLocationListItemViewModel currentTopLevelLocation, 
			YieldLocationListItemViewModel currentFromSubLocation, 
			YieldLocationListItemViewModel currentToSubLocation,
			string locationDescription,
			decimal finalQuantityValue,
			CompositeUnit finalQuantityUnit,
			Action<ChangeLoadLocationViewModel> onLocationChanged) {

			originalLocation = currentTopLevelLocation;
			originalFromSubLocation = currentFromSubLocation;
			originalToSubLocation = currentToSubLocation;

			EntityDescription = locationDescription;

			SubLocationInfo = new LoadSubLocationInfoViewModel(clientEndpoint) {
				FinalQuantityValue = finalQuantityValue,
				FinalQuantityUnit = finalQuantityUnit,
			};

			AvailableTopLevelLocations = availableTopLevelLocationsList.ToList();

			// assign to private member so that we don't trigger update to sublocationinfo
			SelectedTopLevelLocation = currentTopLevelLocation;

			SubLocationInfo.FromSelectedSubLocation = SubLocationInfo.AvailableFromSubLocations.SingleOrDefault(x => x.Id == currentFromSubLocation?.Id);
			SubLocationInfo.ToSelectedSubLocation = SubLocationInfo.AvailableToSubLocations.SingleOrDefault(x => x.Id == currentToSubLocation?.Id);

			locationChanged += onLocationChanged;

			CompleteCommand = new RelayCommand(onComplete);
			CancelCommand = new RelayCommand(onCancel);
		}

		public ICommand CompleteCommand { get; }
		public ICommand CancelCommand { get; }

		public string EntityDescription { get; }

		public List<YieldLocationListItemViewModel> AvailableTopLevelLocations { get; }

		public LoadSubLocationInfoViewModel SubLocationInfo { get; }

		private YieldLocationListItemViewModel _selectedTopLevelLocation;
		public YieldLocationListItemViewModel SelectedTopLevelLocation {
			get { return _selectedTopLevelLocation; }
			set {
				_selectedTopLevelLocation = value;
				RaisePropertyChanged(() => SelectedTopLevelLocation);

				SubLocationInfo.SelectedTopLevelLocation = _selectedTopLevelLocation;
			}
		}

		public bool HasChanges() => SelectedTopLevelLocation != originalLocation
									|| SubLocationInfo.FromSelectedSubLocation != originalFromSubLocation
									|| SubLocationInfo.ToSelectedSubLocation != originalToSubLocation;

		private void onComplete() {
			locationChanged(this);
		}

		private void onCancel() {
			locationChanged(null);
		}
	}
}