﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using System.Windows.Threading;

namespace Landdb.ViewModel.Yield.Popups {
	public class ChangeLoadPropertiesViewModel : ViewModelBase {

		private readonly IClientEndpoint clientEndpoint;
		private readonly Dispatcher dispatcher;

		private readonly LoadPropertyDefinitions propDefinitions;

		private readonly CropId cropId;
		private readonly List<LoadPropertySetViewModel> originalProperties;

		private readonly Action<ChangeLoadPropertiesViewModel> propertiesChanged;

		public ChangeLoadPropertiesViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, CropId cropId, IEnumerable<LoadPropertySetViewModel> originalProperties, Action<ChangeLoadPropertiesViewModel> onLoadPropertiesChanged) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			this.cropId = cropId;

			propDefinitions = clientEndpoint.GetMasterlistService().GetLoadPropertyDefinitions();

			this.originalProperties = originalProperties.ToList();

			buildLoadPropertiesList();

			propertiesChanged += onLoadPropertiesChanged;

			CompleteCommand = new RelayCommand(onComplete);
			CancelCommand = new RelayCommand(onCancel);
		}

		public ICommand CompleteCommand { get; }
		public ICommand CancelCommand { get; }

		public List<LoadPropertySetSelectionViewModel> AvailableLoadPropertySets { get; private set; }
		public List<LoadPropertySetSelectionViewModel> UnrelatedLoadPropertySets { get; private set; }
		public ObservableCollection<LoadPropertySetViewModel> ReportedLoadPropertySets { get; private set; }
		public IEnumerable<CompositeUnit> WeightUnitList { get; private set; }

		public IEnumerable<ReportedLoadPropertySet> AddedProperties {
			get {
				return from a in ReportedLoadPropertySets
					   where !originalProperties.Exists(x => x.Id == a.Id)
					   select new ReportedLoadPropertySet() {
						   LoadPropertySetId = a.Id,
						   ReportedPropertyValues = (from v in a.LoadProperties
													 select new ReportedLoadPropertyValue() {
														 LoadPropertyId = v.Id,
														 ReportedValue = v.Value,
														 ReportedUnit = v.SelectedUnit,
													 }).ToList()
					   };
			}
		}

		public IEnumerable<ReportedLoadPropertySet> ChangedProperties {
			get {
				return from c in ReportedLoadPropertySets
					   where originalProperties.Exists(set => set.Id == c.Id && c.LoadProperties.Any(prop => prop.IsChanged))
					   select new ReportedLoadPropertySet() {
						   LoadPropertySetId = c.Id,
						   ReportedPropertyValues = (from v in c.LoadProperties
													 select new ReportedLoadPropertyValue() {
														 LoadPropertyId = v.Id,
														 ReportedValue = v.Value,
														 ReportedUnit = v.SelectedUnit,
													 }).ToList()
					   };
			}
		}

		public IEnumerable<Guid> RemovedProperties => from r in originalProperties
													  where !ReportedLoadPropertySets.Where(x => x.Id == r.Id).Any()
													  select r.Id;

		public bool HasChanges => AddedProperties.Any() || ChangedProperties.Any() || RemovedProperties.Any();

		private void onComplete() {
			propertiesChanged(this);
		}

		private void onCancel() {
			propertiesChanged(null);
		}

		private void onCheckChanged(Guid setId, bool isChecked) {
			if (isChecked) {
				if (!ReportedLoadPropertySets.Any(x => x.Id == setId)) {
					var setToAdd = propDefinitions.LoadPropertySets.FirstOrDefault(x => x.Id == setId);
					if (setToAdd != null) {
						ReportedLoadPropertySets.Add(new LoadPropertySetViewModel(setToAdd) {
							LoadProperties = propDefinitions.LoadProperties.Where(prop => setToAdd.LoadPropertyIds.Contains(prop.Id)).Select(prop => new LoadPropertyViewModel(prop) {
								ListItems = propDefinitions.LoadPropertyListItems
												.Where(li => prop.ListItemIds != null && prop.ListItemIds.Contains(li.Id))
												.Select(li => new LoadPropertyListItemViewModel(li))
												.ToList()
							}).ToList()
						});
					}
				}
			} else {
				var setToRemove = ReportedLoadPropertySets.FirstOrDefault(x => x.Id == setId);
				if (setToRemove != null) {
					ReportedLoadPropertySets.Remove(setToRemove);
				}
			}
		}

		private void buildLoadPropertiesList() {
			WeightUnitList = CompositeUnitHelper.GetWeightPartUnits().OrderBy(x => x.AbbreviatedDisplay).ToList();

			ReportedLoadPropertySets = new ObservableCollection<LoadPropertySetViewModel>();
			originalProperties.Select(x => x.Clone()).ForEach(x => ReportedLoadPropertySets.Add(x));

			foreach (var set in originalProperties) {
				foreach (var prop in set.LoadProperties) {
					if (prop.UnitSelectionType == LoadPropertyUnitSelectionTypes.Weight) {
						prop.SelectedUnit = WeightUnitList.FirstOrDefault(x => x == prop.SelectedUnit);
					}
				}
			}

			AvailableLoadPropertySets = new List<LoadPropertySetSelectionViewModel>();
			UnrelatedLoadPropertySets = new List<LoadPropertySetSelectionViewModel>();

			var activeSets = propDefinitions.LoadPropertySets.Where(x => x.IsActive).OrderBy(x => x.Name);
			foreach (var set in activeSets) {
				var setToAdd = new LoadPropertySetSelectionViewModel(set, onCheckChanged) {
					IsChecked = originalProperties.Any(x => x.Id == set.Id)
				};

				if (set.RecommendedCropIds == null || (set.RecommendedCropIds != null && set.RecommendedCropIds.Contains(cropId.Id))) {
					AvailableLoadPropertySets.Add(setToAdd);
				} else {
					UnrelatedLoadPropertySets.Add(setToAdd);
				}
			}
		}
	}
}