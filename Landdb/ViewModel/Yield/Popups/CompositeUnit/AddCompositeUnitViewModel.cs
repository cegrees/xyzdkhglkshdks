﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Yield;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Yield.Popups {
	public class AddCompositeUnitViewModel : ViewModelBase {

		private readonly IClientEndpoint clientEndpoint;
		private readonly Dispatcher dispatcher;

		private readonly Action<AddCompositeUnitViewModel> compositeUnitCreated;

		string CUSTOM_UNIT_TEXT = $"-{Strings.NewCustomUnit_Text.ToLower()}-";

		public AddCompositeUnitViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, CropYearId currentCropYear, CropId cropId, IEnumerable<CompositeUnit> unsavedCustomUnits, Action<AddCompositeUnitViewModel> onCompositeUnitCreated) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			var customPartUnits = unsavedCustomUnits.Select(x => new CompositeUnit(x.PartUnitName));

			var appliedLoadValuesView = clientEndpoint.GetView<AppliedLoadValuesView>(currentCropYear).GetValue(() => null);
			if (appliedLoadValuesView != null && appliedLoadValuesView.AppliedCropValues.ContainsKey(cropId.Id)) {
				customPartUnits = appliedLoadValuesView.AppliedCropValues[cropId.Id].AppliedUnits.Select(x => new CompositeUnit(x.PartUnitName));
			}

			AllowedPartUnits = new ObservableCollection<CompositeUnit>();
			var partUnits = CompositeUnitHelper.GetWeightPartUnits()
				.Union(CompositeUnitHelper.GetVolumePartUnits())
				.Union(customPartUnits);
			partUnits.OrderBy(x => x.FullDisplay).ForEach(x => AllowedPartUnits.Add(x));

			AllowedPartUnits.Insert(0, new CompositeUnit(CUSTOM_UNIT_TEXT));

			compositeUnitCreated += onCompositeUnitCreated;

			CreateNewCommand = new RelayCommand(onCreate);
			CancelCommand = new RelayCommand(onCancel);
		}

		public RelayCommand CancelCommand { get; }
		public RelayCommand CreateNewCommand { get; }

		public ObservableCollection<CompositeUnit> AllowedPartUnits { get; }

		public string PreviewDisplay => IsValid ? GetNewUnit().FullDisplay : string.Empty;
		public bool IsCustomUnitEntryVisible => SelectedPartUnit != null ? SelectedPartUnit.PackageUnitName == CUSTOM_UNIT_TEXT : false;

		private string _name = string.Empty;
		public string Name {
			get { return _name; }
			set {
				_name = value;
				RaisePropertyChanged(() => Name);
				RaisePropertyChanged(() => PreviewDisplay);
			}
		}

		private bool _isPackageUnit;
		public bool IsPackageUnit {
			get { return _isPackageUnit; }
			set {
				_isPackageUnit = value;
				RaisePropertyChanged(() => IsPackageUnit);
				RaisePropertyChanged(() => PreviewDisplay);
			}
		}

		private decimal _factor = 1;
		public decimal Factor {
			get { return _factor; }
			set {
				_factor = value;
				RaisePropertyChanged(() => Factor);
				RaisePropertyChanged(() => PreviewDisplay);
			}
		}

		private CompositeUnit _partUnit = null;
		public CompositeUnit SelectedPartUnit {
			get { return _partUnit; }
			set {
				_partUnit = value;

				RaisePropertyChanged(() => SelectedPartUnit);
				RaisePropertyChanged(() => IsCustomUnitEntryVisible);
				RaisePropertyChanged(() => PreviewDisplay);
			}
		}

		private string _newPartUnit = null;
		public string NewPartUnit {
			get { return _newPartUnit; }
			set {
				_newPartUnit = value;

				RaisePropertyChanged(() => NewPartUnit);
				RaisePropertyChanged(() => PreviewDisplay);
			}
		}

		public bool IsValid {
			get {
				if (IsPackageUnit) {
					if (IsCustomUnitEntryVisible) {
						var isFactorValid = GetSanitizedName() == GetSanitizedPart() ? Factor == 1 : Factor > 0;

						return !string.IsNullOrWhiteSpace(Name)
							&& !string.IsNullOrWhiteSpace(NewPartUnit)
							&& isFactorValid;
					} else {
						return !string.IsNullOrWhiteSpace(Name)
							&& SelectedPartUnit != null
							&& Factor > 0;
					}
				} else {
					return !string.IsNullOrWhiteSpace(Name);
				}
			}
		}

		public string GetSanitizedName() => !string.IsNullOrWhiteSpace(Name) ? Name.ToLower().Trim() : null;
		public string GetSanitizedPart() => !string.IsNullOrWhiteSpace(NewPartUnit) ? NewPartUnit.ToLower().Trim() : null;

		public CompositeUnit GetNewUnit() {
			if (!IsValid) {
				return null;
			} else if (!IsPackageUnit) {
				return new CompositeUnit(GetSanitizedName(), 1m, GetSanitizedName());
			} else if (IsCustomUnitEntryVisible) {
				return new CompositeUnit(GetSanitizedName(), Factor, GetSanitizedPart());
			} else {
				return new CompositeUnit(GetSanitizedName(), Factor, SelectedPartUnit.PackageUnitName);
			}
		}

		private void onCreate() {
			if (IsValid) {
				compositeUnitCreated(this);
			}
		}

		private void onCancel() {
			compositeUnitCreated(null);
		}
	}
}