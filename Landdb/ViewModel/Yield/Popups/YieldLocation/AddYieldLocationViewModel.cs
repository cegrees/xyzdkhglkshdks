﻿using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Company;
using Landdb.Domain.ReadModels.Person;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Resources;
using Landdb.ViewModel.Shared;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Yield.Popups {
	public class AddYieldLocationViewModel : ValidationViewModelBase {

		private readonly IClientEndpoint clientEndpoint;
		private readonly Dispatcher dispatcher;

		private readonly CropYearId currentCropYearId;

		private readonly Logger log;

		private readonly Action<YieldLocationListItemViewModel> yieldLocationAdded;

		public AddYieldLocationViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, YieldLocationId locationId, int currentCropYear, Action<YieldLocationListItemViewModel> onYieldLocationAdded) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			currentCropYearId = new CropYearId(locationId.DataSourceId, currentCropYear);

			log = LogManager.GetCurrentClassLogger();

			LocationId = locationId;

			YieldLocationTypeList = Enum.GetValues(typeof(YieldLocationTypes))
				.Cast<YieldLocationTypes>()
				.Where(x => x != YieldLocationTypes.Unknown)
				.OrderBy(x => x.ToString());

			CompanyList = getCompanyList();

			var segmentTypes = clientEndpoint.GetMasterlistService()
				.GetYieldLocationSegmentTypes()
				.SegmentTypeList
				.Select(x => x.Name);

			var locList = YieldLocationListItemViewModel.GetFlattenedListItemsForCropYear(clientEndpoint, currentCropYearId, true);

			AvailableParentLocationsList = (from loc in locList
											let hasSublocs = loc.SubLocationList.Any()
											where !segmentTypes.Contains(loc.Descriptor) || hasSublocs
											select loc).ToList();

			StreetAddress = new StreetAddressViewModel(clientEndpoint, dispatcher);

			SelectedCompany = CompanyList.Where(x => x.Id != null && x.Id.Id == Guid.Empty).SingleOrDefault();

            buildLandOwnerList(currentCropYearId);

            SystemList = new List<string>();
            SystemList.Add(Strings.SystemList_Auger_Text);
            SystemList.Add(Strings.SystemList_BucketElevator_Text);
            SystemList.Add(Strings.SystemList_DragChain_Text);
            SystemList.Add(Strings.SystemList_VacuumSystem_Text);         

            yieldLocationAdded += onYieldLocationAdded;
            LocationShape = LocationGeometry.Round;
            LocationStatus = LocationStatusEnum.Owned;
            TermStartDate = DateTime.Now;
            TermEndDate = DateTime.Now.AddDays(1);

			CreateNewCommand = new RelayCommand(onCreateNew);
			CancelCommand = new RelayCommand(onCancel);
		}

		#region Properties
		public ICommand CreateNewCommand { get; }
		public ICommand CancelCommand { get; }

		public IEnumerable<YieldLocationTypes> YieldLocationTypeList { get; }
		public IEnumerable<YieldLocationListItemViewModel> AvailableParentLocationsList { get; }

		public List<CompanyListItem> CompanyList { get; }

		public YieldLocationId LocationId { get; }

		public StreetAddressViewModel StreetAddress { get; private set; }

		public YieldLocationListItemViewModel SelectedParentLocation { get; set; }

		public bool IsCompanySelectionVisible => SelectedType == YieldLocationTypes.Sale;
		public bool IsParentSelectionVisible => SelectedType == YieldLocationTypes.Storage;

		private string _name = string.Empty;
		[Required(AllowEmptyStrings = false, ErrorMessageResourceName = "ErrorMessage_NameIsRequired_Text", ErrorMessageResourceType = typeof(Strings))]
		public string Name {
			get { return _name != null ? _name.Trim() : null; }
			set {
				_name = value;
				ValidateAndRaisePropertyChanged(() => Name);
			}
		}

		private YieldLocationTypes _yieldLocationType = YieldLocationTypes.Unknown;
		[YieldLocationTypeNotUnknownValidator(ErrorMessageResourceName = "ErrorMessage_TypeMustBeSpecified_Text", ErrorMessageResourceType = typeof(Strings))]
		public YieldLocationTypes SelectedType {
			get { return _yieldLocationType; }
			set {
				_yieldLocationType = value;
				ValidateAndRaisePropertyChanged(() => SelectedType);
				RaisePropertyChanged(() => IsCompanySelectionVisible);
				RaisePropertyChanged(() => IsParentSelectionVisible);
			}
		}

		private CompanyListItem _company = null;
		public CompanyListItem SelectedCompany {
			get { return _company; }
			set {
				_company = value;
				RaisePropertyChanged(() => SelectedCompany);

				var isValidCompanySelected = _company != null && _company.Id != null && _company.Id.Id != Guid.Empty;
				if (isValidCompanySelected) {
					var companyDetailsView = clientEndpoint.GetView<CompanyDetailsView>(_company.Id).GetValue(() => null);
					if (companyDetailsView != null) {
						PrimaryPhoneNumber = companyDetailsView.PrimaryPhoneNumber;
						EmailAddress = companyDetailsView.PrimaryEmail;

						StreetAddress = new StreetAddressViewModel(clientEndpoint, dispatcher, companyDetailsView.StreetAddress);
						RaisePropertyChanged(() => StreetAddress);
					}
				}
			}
		}

		private string _phone = string.Empty;
		public string PrimaryPhoneNumber {
			get { return _phone != null ? _phone.Trim() : null; }
			set {
				_phone = value;
				RaisePropertyChanged(() => PrimaryPhoneNumber);
			}
		}

		private string _emailAddress = string.Empty;
		public string EmailAddress {
			get { return _emailAddress != null ? _emailAddress.Trim() : null; }
			set {
				_emailAddress = value;
				RaisePropertyChanged(() => EmailAddress);
			}
		}

		private string _notes = string.Empty;
		public string Notes {
			get { return _notes != null ? _notes.Trim() : null; }
			set {
				_notes = value;
				RaisePropertyChanged(() => Notes);
			}
		}

        public ICollectionView LandOwnerDisplayCollection
        {
            get
            {
                var collectionView = CollectionViewSource.GetDefaultView(LandOwnerList.OrderBy(x => x.Group).ThenBy(x => x.Name));
                collectionView.GroupDescriptions.Add(new PropertyGroupDescription("Group"));
                return collectionView;
            }
        }
        public List<ContractorListItemViewModel> LandOwnerList { get; private set; }
        public ContractorListItemViewModel SelectedLandOwner { get; set; }

        public decimal TotalCost { get; set; }
        public DateTime TermStartDate { get; set; }
        public DateTime TermEndDate { get; set; }
        public string CoolingSystem { get; set; }

        public List<string> SystemList { get; set; }
        public string FillingSystem { get; set; }
        public string UnloadingSystem { get; set; }
        public int YearBuilt { get; set; }
        public string QualityGrade { get; set; }
        public string PropertyTaxId { get; set; }
        private LocationGeometry _locationShape { get; set; }
        public LocationGeometry LocationShape
        {
            get { return _locationShape; }
            set
            {
                _locationShape = value;
                RaisePropertyChanged(() => LocationShape);
            }
        }

        private decimal? _width;
        public decimal? Width { get { return _width; } set { _width = value; CalculateVolume(); } }
        private decimal? _height;
        public decimal? Height { get { return _height; } set { _height = value; CalculateVolume(); } }
        private decimal? _length;
        public decimal? Length { get { return _length; } set { _length = value; CalculateVolume(); } }
        private decimal? _cone;
        public decimal? ConeHeight { get { return _cone; } set { _cone = value; CalculateVolume(); } }
        public double BinVolume { get; set; }
        private double _binCapacity { get; set; }
        public double BinCapacity { get { return _binCapacity; } set { _binCapacity = value; } }
        private LocationStatusEnum _locationStatus { get; set; }
        public LocationStatusEnum LocationStatus { get { return _locationStatus; } set { _locationStatus = value; RaisePropertyChanged(() => LocationStatus); } }
        #endregion

        private void onCreateNew() {
			if (!ValidateViewModel()) { return; }

			var phone = string.IsNullOrWhiteSpace(PrimaryPhoneNumber) ? null : new PhoneNumber("Primary", PrimaryPhoneNumber);
			var email = string.IsNullOrWhiteSpace(EmailAddress) ? null : new Email(EmailAddress, Name);
			var streetAddress = StreetAddress.GetValueObject();

			IDomainCommand createLocationCommand = null;

			if (SelectedType == YieldLocationTypes.Sale) {
				createLocationCommand = getSaleLocationCreateCommand(phone, email, streetAddress);
			} else if (SelectedType.Equals(YieldLocationTypes.Storage)) {
				createLocationCommand = getStorageLocationCreateCommand(phone, email, streetAddress);
			} else {
				// we somehow ended up with an invalid location type selected. best to just log and abort.
				log.Warn($"Attempted to create an unhandled location type: {SelectedType}");
				return;
			}

			if (createLocationCommand != null) {
				clientEndpoint.SendOne(createLocationCommand);

				var li = new Domain.ReadModels.YieldLocation.YieldLocationListItem() {
					Id = LocationId,
					Name = Name,
					YieldLocationType = SelectedType,
				};

				// send the newly-created list item back to the caller
				yieldLocationAdded(new YieldLocationListItemViewModel(li, currentCropYearId.Id));
			} else {
				log.Warn($"Failed to generate a yield location creation command.");
			}

			Messenger.Default.Send(new HideOverlayMessage());
		}

		private IDomainCommand getSaleLocationCreateCommand(PhoneNumber phone, Email email, StreetAddress streetAddress) {
			CompanyId companyId = null;

			if (SelectedCompany != null && SelectedCompany.Id != null) {
				if (SelectedCompany.Id.Id == Guid.Empty) { // user has opted to create a new company using these details
					companyId = new CompanyId(currentCropYearId.DataSourceId, Guid.NewGuid());

					clientEndpoint.SendOne(new CreateCompany(
						companyId, 
						clientEndpoint.GenerateNewMetadata(), 
						Name,
						streetAddress, 
						phone,
						email, 
						currentCropYearId.Id
					));
				} else { // user has selected a company that already exists in the system
					companyId = SelectedCompany.Id;
				}
			}

			var createLocationCommand = new CreateYieldSaleLocation(
				LocationId,
				clientEndpoint.GenerateNewMetadata(),
				Name, 
				streetAddress, 
				phone,
				email,
				currentCropYearId.Id,
				companyId,
				Notes
			);

			return createLocationCommand;
		}

		private IDomainCommand getStorageLocationCreateCommand(PhoneNumber phone, Email email, StreetAddress streetAddress) {
			YieldLocationId parentLocationId = null;

			if (SelectedParentLocation != null && SelectedParentLocation.Id != null && SelectedParentLocation.Id.Id != Guid.Empty) {
				parentLocationId = SelectedParentLocation.Id;
			}

            var binOwner = this.SelectedLandOwner != null ? ( this.SelectedLandOwner.CompanyId != null ? new LandOwner(this.SelectedLandOwner.CompanyId, this.SelectedLandOwner.Name, this.SelectedLandOwner.PhoneNumber) : new LandOwner(this.SelectedLandOwner.PersonId, this.SelectedLandOwner.Name, this.SelectedLandOwner.PhoneNumber)) : null;

            var advancedInfo = new YieldLocationAdvancedInformation()
            {
                ConeHeight = this.ConeHeight,
                Height = this.Height,
                Length = this.Length,
                LocationShape = this.LocationShape.ToString(),
                Owner = binOwner,
                PossessionStatus = this.LocationStatus.ToString(),
                PropertyTaxId = this.PropertyTaxId,
                QualityGrade = this.QualityGrade,
                TermEndDate = this.TermEndDate,
                TermStartDate = this.TermStartDate,
                TotalRentDue = this.TotalCost,
                Width = this.Width,
                YearBuilt = this.YearBuilt,
                FillingSystem = this.FillingSystem,
                UnloadingSystem = this.UnloadingSystem,
                CoolingSystem = this.CoolingSystem,
                BushelCapacity = this.BinCapacity,
            };


            var createLocationCommand = new CreateYieldStorageLocation(
				LocationId, 
				clientEndpoint.GenerateNewMetadata(), 
				parentLocationId, 
				Name, 
				streetAddress, 
				phone,
				email,
				currentCropYearId.Id,
				Notes, 
                advancedInfo
			);

			return createLocationCommand;
		}

		private void onCancel() {
			Messenger.Default.Send(new HideOverlayMessage());
		}

		private List<CompanyListItem> getCompanyList() {
			var companyListView = clientEndpoint.GetView<CompanyListView>(new DataSourceId(currentCropYearId.DataSourceId)).GetValue(new CompanyListView());
			var sortedFilteredCompanies = from c in companyListView.Companies
										  where c.IsActiveInCropYear(currentCropYearId.Id)
										  orderby c.Name
										  select c;

			var retList = sortedFilteredCompanies.ToList();

			retList.Insert(0, new CompanyListItem() {
				Id = null,
				Name = string.Empty,
			});

			retList.Insert(1, new CompanyListItem() {
				Id = new CompanyId(currentCropYearId.DataSourceId, Guid.Empty),
				Name = Strings.CompanyListItem_NewCompanyUsingDetails_Text,
			});

			return retList;
		}

        private void buildLandOwnerList(CropYearId currentCropYearId)
        {
            var dataSourceId = new DataSourceId(currentCropYearId.DataSourceId);

            var newLandOwnerList = new List<ContractorListItemViewModel>();

            var personListView = clientEndpoint.GetView<PersonListView>(dataSourceId).GetValue(new PersonListView());
            var sortedFilteredPersons = from p in personListView.Persons
                                        where p.IsActiveInCropYear(currentCropYearId.Id)
                                        orderby p.Name
                                        select p;

            sortedFilteredPersons.For_Each(person => {
                newLandOwnerList.Add(new ContractorListItemViewModel(person));
            });

            var companyListView = clientEndpoint.GetView<CompanyListView>(dataSourceId).GetValue(new CompanyListView());
            var sortedFilteredCompanies = from c in companyListView.Companies
                                          where c.IsActiveInCropYear(currentCropYearId.Id)
                                          orderby c.Name
                                          select c;

            sortedFilteredCompanies.ForEach(company => {
                newLandOwnerList.Add(new ContractorListItemViewModel(company));
            });

            LandOwnerList = newLandOwnerList.OrderBy(x => x.Group).ThenBy(x => x.Name).ToList();
            RaisePropertyChanged(() => LandOwnerList);
        }

        void CalculateVolume()
        {
            switch (LocationShape.ToString())
            {
                case "Round":
                    if (!Width.HasValue || !Height.HasValue) { return; }
                    var volume = Math.PI * Math.Pow(((double)Width.Value / 2.0), 2.0) * (double)Height.Value;
                    var coneVolumn = ConeHeight.HasValue ? ((1.0 / 3.0) * Math.PI * Math.Pow((((double)Width.Value) / 2.0), 2.0) * (double)ConeHeight.Value) : 0.0;
                    //(pi / 3)H^2(3R - H)
                    var domeVolume = ConeHeight.HasValue ? (Math.PI / 3.0) * Math.Pow((double)ConeHeight.Value, 2.0) * ((3.0 * ((double)Width.Value / 2.0)) - (double)ConeHeight.Value) : 0.0;
                    BinVolume = volume + coneVolumn;
                    BinCapacity = BinVolume / 1.24445602149;
                    break;
                case "Flat":
                    if (!Width.HasValue || !Length.HasValue || !Height.HasValue) { return; }
                    var flatVolume = Length.Value * Width.Value * Height.Value;
                    var flatConeVolume = ConeHeight.HasValue ? (double)((1 / 2) * (4 / 3) * (Length.Value / 2m) * (Width.Value / 2m) * ConeHeight.Value) : 0.0;
                    BinVolume = (double)flatVolume + flatConeVolume;
                    BinCapacity = BinVolume / 1.24445602149;
                    break;
            }

            RaisePropertyChanged(() => BinVolume);
            RaisePropertyChanged(() => BinCapacity);
        }
    }

    public enum LocationGeometry
    {
        Round,
        Flat
    }
    public enum LocationStatusEnum
    {
        Owned,
        Leased
    }
}