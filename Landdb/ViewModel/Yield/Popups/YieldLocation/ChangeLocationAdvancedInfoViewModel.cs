﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Company;
using Landdb.Domain.ReadModels.Person;
using Landdb.Domain.ReadModels.Yield;
using Landdb.Domain.ReadModels.YieldLocation;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Input;
using Landdb.Resources;

namespace Landdb.ViewModel.Yield.Popups.YieldLocation
{
    public class ChangeLocationAdvancedInfoViewModel : ViewModelBase
    {
        IClientEndpoint endpoint;
        YieldLocationDetailsView _location;
        public ChangeLocationAdvancedInfoViewModel(IClientEndpoint endPoint, YieldLocationDetailsView location)
        {
            this.endpoint = endPoint;
            //Shape = "Round";

            SystemList = new List<string>();
            SystemList.Add(Strings.SystemList_Auger_Text);
            SystemList.Add(Strings.SystemList_BucketElevator_Text);
            SystemList.Add(Strings.SystemList_DragChain_Text);
            SystemList.Add(Strings.SystemList_VacuumSystem_Text);
            _location = location;
            UpdateFromProjections(_location);

            UpdateAdvancedInfoCommand = new RelayCommand(Update);
            CancelCommand = new RelayCommand(Cancel);
        }

        public ICommand UpdateAdvancedInfoCommand { get; }
        public ICommand CancelCommand { get; }

        public YieldLocationId ID { get; set; }
        private decimal? _width;
        public decimal? Width { get { return _width; } set { _width = value; CalculateVolume(); } }
        private decimal? _height;
        public decimal? Height { get { return _height; } set { _height = value; CalculateVolume(); } }
        private decimal? _length;
        public decimal? Length { get { return _length; } set { _length = value; CalculateVolume(); } }
        private decimal? _cone;
        public decimal? ConeHeight { get { return _cone; } set { _cone = value; CalculateVolume(); } }
        private LocationGeometry _locationShape { get; set; }
        public LocationGeometry LocationShape
        {
            get { return _locationShape; }
            set
            {
                _locationShape = value;
                RaisePropertyChanged(() => LocationShape);
            }
        }
        private LocationStatusEnum _locationStatus { get; set; }
        public LocationStatusEnum LocationStatus { get { return _locationStatus; } set { _locationStatus = value; RaisePropertyChanged(() => LocationStatus); } }
        public double BinVolume { get; set; }
        private double _binCapacity { get; set; }
        public double BinCapacity { get { return _binCapacity; } set { _binCapacity = value; } }
        public decimal TotalCost { get; set; }
        public DateTime TermStartDate { get; set; }
        public DateTime TermEndDate { get; set; }
        public string CoolingSystem { get; set; }
        public List<string> SystemList { get; set; }
        public string FillingSystem { get; set; }
        public string UnloadingSystem { get; set; }
        public int YearBuilt { get; set; }
        public string QualityGrade { get; set; }
        public string PropertyTaxId { get; set; }
        public ICollectionView LandOwnerDisplayCollection
        {
            get
            {
                var collectionView = CollectionViewSource.GetDefaultView(LandOwnerList.OrderBy(x => x.Group).ThenBy(x => x.Name));
                collectionView.GroupDescriptions.Add(new PropertyGroupDescription("Group"));
                return collectionView;
            }
        }
        public List<ContractorListItemViewModel> LandOwnerList { get; private set; }
        public ContractorListItemViewModel SelectedLandOwner { get; set; }
        void Update()
        {
            //if changed then fire off event to domain
            var binOwner = this.SelectedLandOwner != null ? (this.SelectedLandOwner.CompanyId != null ? new LandOwner(this.SelectedLandOwner.CompanyId, this.SelectedLandOwner.Name, this.SelectedLandOwner.PhoneNumber) : new LandOwner(this.SelectedLandOwner.PersonId, this.SelectedLandOwner.Name, this.SelectedLandOwner.PhoneNumber)) : null;

            var advancedInfo = new YieldLocationAdvancedInformation()
            {
                ConeHeight = this.ConeHeight,
                Height = this.Height,
                Length = this.Length,
                LocationShape = this.LocationShape.ToString(),
                Owner = binOwner,
                PossessionStatus = this.LocationStatus.ToString(),
                PropertyTaxId = this.PropertyTaxId,
                QualityGrade = this.QualityGrade,
                TermEndDate = this.TermEndDate,
                TermStartDate = this.TermStartDate,
                TotalRentDue = this.TotalCost,
                Width = this.Width,
                YearBuilt = this.YearBuilt,
                FillingSystem = this.FillingSystem,
                UnloadingSystem = this.UnloadingSystem,
                BushelCapacity = this.BinCapacity,
            };
            var previousInfo = _location.AdvancedInformation != null ? _location.AdvancedInformation : new YieldLocationAdvancedInformation();

            if (previousInfo.ConeHeight != ConeHeight || previousInfo.CoolingSystem != CoolingSystem || previousInfo.FillingSystem != FillingSystem || previousInfo.Height != Height
                || previousInfo.Length != Length || previousInfo.LocationShape != LocationShape.ToString() || previousInfo.Owner != binOwner || previousInfo.PossessionStatus != LocationStatus.ToString() || previousInfo.PropertyTaxId != PropertyTaxId
                || previousInfo.QualityGrade != QualityGrade || previousInfo.TermEndDate != TermEndDate || previousInfo.TermStartDate != TermStartDate || previousInfo.TotalRentDue != TotalCost || previousInfo.UnloadingSystem != UnloadingSystem ||
                previousInfo.Width != Width || previousInfo.YearBuilt != YearBuilt || previousInfo.BushelCapacity != BinCapacity)
            {
                var command = new UpdateYieldLocationAdvancedInfo(_location.Id, endpoint.GenerateNewMetadata(), advancedInfo);
                endpoint.SendOne(command);
            }
                
            Messenger.Default.Send(new HidePopupMessage());
        }
        void Cancel()
        {
            Messenger.Default.Send(new HidePopupMessage());
        }

        private void UpdateFromProjections(YieldLocationDetailsView location )
        {
            buildLandOwnerList(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear));

            if(location.AdvancedInformation == null) { return; }

            var advanced = location.AdvancedInformation;
            ID = location.Id;
            Width = location.AdvancedInformation.Width;
            Length = location.AdvancedInformation.Length;
            Height = location.AdvancedInformation.Height;
            ConeHeight = location.AdvancedInformation.ConeHeight;

            LocationShape = location.AdvancedInformation.LocationShape == "Round" ? LocationGeometry.Round : LocationGeometry.Flat;
            var owner = location.AdvancedInformation.Owner;
            SelectedLandOwner = owner != null ? (from o in LandOwnerList where (o.CompanyId == owner.CompanyId && o.PersonId == null) || (o.CompanyId == null && o.PersonId == owner.PersonId) select o).FirstOrDefault() : null;

            LocationStatus = !string.IsNullOrEmpty(advanced.PossessionStatus) && "Owned" == advanced.PossessionStatus ? LocationStatusEnum.Owned : LocationStatusEnum.Leased;
            LocationStatus = location.AdvancedInformation.PossessionStatus == "Leased" ? LocationStatusEnum.Leased : LocationStatusEnum.Owned;
            TermEndDate = advanced.TermEndDate != null ? advanced.TermEndDate.Value : DateTime.Now;
            TermStartDate = advanced.TermStartDate != null ? advanced.TermStartDate.Value : DateTime.Now.AddDays(30);
            TotalCost = advanced.TotalRentDue;
            YearBuilt = advanced.YearBuilt;
            QualityGrade = advanced.QualityGrade;
            PropertyTaxId = advanced.PropertyTaxId;
            CoolingSystem = advanced.CoolingSystem;
            FillingSystem = advanced.FillingSystem;
            UnloadingSystem = advanced.UnloadingSystem;
            BinCapacity = advanced.BushelCapacity;
            RaisePropertyChanged(() => LocationShape);
            RaisePropertyChanged(() => FillingSystem);
            RaisePropertyChanged(() => UnloadingSystem);
            //BinVolume = location.AdvancedInformation.
        }

        void CalculateVolume()
        {
            switch (LocationShape.ToString())
            {
                case "Round":
                    if (!Width.HasValue || !Height.HasValue) { return; }
                    var volume = Math.PI * Math.Pow(((double)Width.Value / 2.0), 2.0) * (double)Height.Value;
                    var coneVolumn = ConeHeight.HasValue ? ((1.0 / 3.0) * Math.PI * Math.Pow((((double)Width.Value) / 2.0), 2.0) * (double)ConeHeight.Value) : 0.0;
                    //(pi / 3)H^2(3R - H)
                    var domeVolume = ConeHeight.HasValue ? (Math.PI / 3.0) * Math.Pow((double)ConeHeight.Value, 2.0) * ((3.0 * ((double)Width.Value / 2.0)) - (double)ConeHeight.Value) : 0.0;
                    BinVolume = volume + coneVolumn;
                    BinCapacity = BinVolume / 1.24445602149;
                    break;
                case "Flat":
                    if (!Width.HasValue || !Length.HasValue || !Height.HasValue) { return; }
                    var flatVolume = Length.Value * Width.Value * Height.Value;
                    var flatConeVolume = ConeHeight.HasValue ? (double)((1 / 2) * (4 / 3) * (Length.Value / 2m) * (Width.Value / 2m) * ConeHeight.Value) : 0.0;
                    BinVolume = (double)flatVolume + flatConeVolume;
                    BinCapacity = BinVolume / 1.24445602149;
                    break;
            }

            RaisePropertyChanged(() => BinVolume);
            RaisePropertyChanged(() => BinCapacity);

        }

        private void buildLandOwnerList(CropYearId currentCropYearId)
        {
            var dataSourceId = new DataSourceId(currentCropYearId.DataSourceId);

            var newLandOwnerList = new List<ContractorListItemViewModel>();

            var personListView = endpoint.GetView<PersonListView>(dataSourceId).GetValue(new PersonListView());
            var sortedFilteredPersons = from p in personListView.Persons
                                        where p.IsActiveInCropYear(currentCropYearId.Id)
                                        orderby p.Name
                                        select p;

            sortedFilteredPersons.For_Each(person => {
                newLandOwnerList.Add(new ContractorListItemViewModel(person));
            });

            var companyListView = endpoint.GetView<CompanyListView>(dataSourceId).GetValue(new CompanyListView());
            var sortedFilteredCompanies = from c in companyListView.Companies
                                          where c.IsActiveInCropYear(currentCropYearId.Id)
                                          orderby c.Name
                                          select c;

            sortedFilteredCompanies.ForEach(company => {
                newLandOwnerList.Add(new ContractorListItemViewModel(company));
            });

            LandOwnerList = newLandOwnerList.OrderBy(x => x.Group).ThenBy(x => x.Name).ToList();
            RaisePropertyChanged(() => LandOwnerList);
        }
    }
}
