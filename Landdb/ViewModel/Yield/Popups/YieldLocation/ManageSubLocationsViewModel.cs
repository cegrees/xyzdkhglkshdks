﻿using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Yield.Popups {
	public class ManageSubLocationsViewModel : ValidationViewModelBase {

		private readonly IClientEndpoint clientEndpoint;
		private readonly Dispatcher dispatcher;

		private readonly CropYearId currentCropYearId;

		private readonly Action<ManageSubLocationsViewModel> applyChanges;

		private readonly List<YieldLocationListItemViewModel> originalSubLocationList;

		public ManageSubLocationsViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, CropYearId currentCropYearId, YieldLocationId parentId, IEnumerable<YieldLocationListItemViewModel> currentSubLocationList, Action<ManageSubLocationsViewModel> onApplyChanges) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			this.currentCropYearId = currentCropYearId;

			AvailableSegmentNames = clientEndpoint.GetMasterlistService()
				.GetYieldLocationSegmentTypes()
				.SegmentTypeList
				.OrderBy(x => x.Name)
				.Select(x => x.Name)
				.ToList();

			var locList = YieldLocationListItemViewModel.GetFlattenedListItemsForCropYear(clientEndpoint, currentCropYearId, true);

			var filteredLoclist = from loc in locList
								  let hasSublocs = loc.SubLocationList.Any()
								  where !AvailableSegmentNames.Contains(loc.Descriptor) || hasSublocs
								  select loc;

			AvailableYieldLocations = new ObservableCollection<YieldLocationListItemViewModel>(filteredLoclist);

			// easier to recursively remove parent after getting the full list
			// rather than trying to filter it out when pulling the list
			var parentLoc = AvailableYieldLocations.SingleOrDefault(x => x.Id == parentId);
			removeAvailableLocation(parentLoc);

			ParentLocationName = parentLoc.Name;

			// tolist() on both of these forces two separate copies of the list.
			// this allows us to operate on one without affecting the other.
			originalSubLocationList = currentSubLocationList.ToList();
			SubLocationList = new ObservableCollection<YieldLocationListItemViewModel>(currentSubLocationList);

			applyChanges += onApplyChanges;

			CompleteCommand = new RelayCommand(onComplete);
			CancelCommand = new RelayCommand(onCancel);

			GenerateSubLocationsCommand = new RelayCommand(onGenerateSubLocations);
			AssignSubLocationCommand = new RelayCommand(onAssignSubLocation);
			RemoveSubLocationCommand = new RelayCommand<YieldLocationListItemViewModel>(onRemoveSubLocation);
			RemoveAllCommand = new RelayCommand(onRemoveAll);
		}

		public ICommand CompleteCommand { get; }
		public ICommand CancelCommand { get; }

		public ICommand GenerateSubLocationsCommand { get; }
		public ICommand AssignSubLocationCommand { get; }
		public ICommand RemoveSubLocationCommand { get; }
		public ICommand RemoveAllCommand { get; }

		public string ParentLocationName { get; }

		public ObservableCollection<YieldLocationListItemViewModel> AvailableYieldLocations { get; }
		public ObservableCollection<YieldLocationListItemViewModel> SubLocationList { get; }

		public List<string> AvailableSegmentNames { get; }

		public IEnumerable<YieldLocationListItemViewModel> RemovedLocations => originalSubLocationList.Except(SubLocationList);
		public IEnumerable<YieldLocationListItemViewModel> AddedLocations => SubLocationList.Except(originalSubLocationList);

		public bool HasSubLocations => SubLocationList.Any();
		public bool IsSegmentGenerationEnabled => !string.IsNullOrWhiteSpace(SelectedSegmentName) && GeneratedSegmentCount > 0 && ValidateProperty(() => GeneratedSegmentCount);
		public bool IsMakeSubLocationEnabled => SelectedYieldLocation != null && SelectedYieldLocation.Id != null;
		
		public bool IsCompletionAllowed => HasChanges();

		private YieldLocationListItemViewModel _selectedYieldLocation;
		public YieldLocationListItemViewModel SelectedYieldLocation {
			get { return _selectedYieldLocation; }
			set {
				_selectedYieldLocation = value;
				RaisePropertyChanged(() => SelectedYieldLocation);
				RaisePropertyChanged(() => IsMakeSubLocationEnabled);
			}
		}

		private string _selectedSegmentName;
		public string SelectedSegmentName {
			get { return _selectedSegmentName; }
			set {
				_selectedSegmentName = value;
				RaisePropertyChanged(() => SelectedSegmentName);
				RaisePropertyChanged(() => IsSegmentGenerationEnabled);
				RaisePropertyChanged(() => HasSubLocations);
			}
		}

		private int _generatedSegmentCount;
		[Range(1, 50, ErrorMessageResourceName = "ErrorMessage_ValueMustBeBetweenLimits_Text", ErrorMessageResourceType = typeof(Strings))]
		public int GeneratedSegmentCount {
			get { return _generatedSegmentCount; }
			set {
				_generatedSegmentCount = value;
				ValidateAndRaisePropertyChanged(() => GeneratedSegmentCount);
				RaisePropertyChanged(() => IsSegmentGenerationEnabled);
				RaisePropertyChanged(() => HasSubLocations);
			}
		}

		public bool HasChanges() => AddedLocations.Any() || RemovedLocations.Any();

		private void onGenerateSubLocations() {
			if (IsSegmentGenerationEnabled) {
				SubLocationList.Clear();

				var padding = GeneratedSegmentCount.ToString().Length;

				for (int i = 1; i <= GeneratedSegmentCount; i++) {
					SubLocationList.Add(new YieldLocationListItemViewModel(
						$"{SelectedSegmentName} {i.ToString().PadLeft(padding, '0')}",
						YieldLocationTypes.Storage,
						currentCropYearId.Id
					));
				}

				SelectedSegmentName = null;
				GeneratedSegmentCount = 0;

				RaisePropertyChanged(() => IsCompletionAllowed);
			}
		}

		private void onAssignSubLocation() {
			if (SelectedYieldLocation != null && SelectedYieldLocation.Id != null) {
				SubLocationList.Add(SelectedYieldLocation);
				removeAvailableLocation(SelectedYieldLocation);

				SelectedYieldLocation = null;

				RaisePropertyChanged(() => HasSubLocations);
				RaisePropertyChanged(() => IsCompletionAllowed);
			}
		}

		private void onRemoveSubLocation(YieldLocationListItemViewModel li) {
			addAvailableLocation(li);
			SubLocationList.Remove(li);

			RaisePropertyChanged(() => HasSubLocations);
		}

		private void addAvailableLocation(YieldLocationListItemViewModel locationToAdd) {
			AvailableYieldLocations.Add(locationToAdd);

			foreach (var loc in locationToAdd.SubLocationList) {
				addAvailableLocation(loc);
			}
		}

		private void removeAvailableLocation(YieldLocationListItemViewModel locationToRemove) {
			foreach (var loc in locationToRemove.SubLocationList) {
				removeAvailableLocation(loc);
			}

			AvailableYieldLocations.Remove(locationToRemove);
		}

		private void onRemoveAll() {
			foreach (var loc in SubLocationList.ToList()) {
				if (loc.Id != null) {
					addAvailableLocation(loc);
				}

				SubLocationList.Remove(loc);
			}

			RaisePropertyChanged(() => HasSubLocations);
		}

		private void onComplete() {
			applyChanges(this);
		}

		private void onCancel() {
			applyChanges(null);
		}
	}
}