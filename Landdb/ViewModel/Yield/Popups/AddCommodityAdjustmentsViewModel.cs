﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Contract;
using Landdb.Infrastructure.Messages;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Yield.Popups {
	public class AddCommodityAdjustmentsViewModel : ViewModelBase {

		private readonly IClientEndpoint clientEndpoint;
		private readonly Dispatcher dispatcher;

		private readonly Action<AddCommodityAdjustmentsViewModel> completeCommodityAdjustments;

		public AddCommodityAdjustmentsViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, CropYearId currentCropYearId, CropId cropId, decimal preAdjustedQuantity, CompositeUnit quantityUnit, Action<AddCommodityAdjustmentsViewModel> onCompleteCommodityAdjustments) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			PreAdjustedQuantity = preAdjustedQuantity;
			QuantityUnit = quantityUnit;

			CropName = clientEndpoint.GetMasterlistService().GetCropDisplay(cropId);

			RentContractList = getRentContractList(currentCropYearId, cropId);

			CommodityAdjustmentList = new ObservableCollection<CommodityAdjustmentLineItemViewModel>();

			ResponsibilityList = new Dictionary<CommodityAdjustmentResponsibilityTypes, string>() {
				{ CommodityAdjustmentResponsibilityTypes.Grower, Strings.Grower_Text },
				{ CommodityAdjustmentResponsibilityTypes.LandOwner, Strings.LandOwner_Text },
				{ CommodityAdjustmentResponsibilityTypes.Split, $"{Strings.Grower_Text}/{Strings.LandOwnerSplit_Text}" },
			};

			completeCommodityAdjustments += onCompleteCommodityAdjustments;

			AddNewLineItem = new RelayCommand(onAddNewLineItem);
			RemoveSelectedLineItem = new RelayCommand(onRemoveSelectedLineItem);

			CompleteCommand = new RelayCommand(onComplete);
			CancelCommand = new RelayCommand(onCancel);

			// add the first line item
			AddNewLineItem.Execute(null);
		}

		public ICommand AddNewLineItem { get; }
		public ICommand RemoveSelectedLineItem { get; }

		public ICommand CompleteCommand { get; }
		public ICommand CancelCommand { get; }

		public string CropName { get; }
		public decimal PreAdjustedQuantity { get; }
		public CompositeUnit QuantityUnit { get; }

		public List<RentContractListItem> RentContractList { get; }
		public ObservableCollection<CommodityAdjustmentLineItemViewModel> CommodityAdjustmentList { get; }
		public Dictionary<CommodityAdjustmentResponsibilityTypes, string> ResponsibilityList { get; }

		private CommodityAdjustmentLineItemViewModel _selectedAdjustment;
		public CommodityAdjustmentLineItemViewModel SelectedCommodityAdjustment {
			get { return _selectedAdjustment; }
			set {
				_selectedAdjustment = value;
				RaisePropertyChanged(() => SelectedCommodityAdjustment);

				RefreshTotals();
			}
		}

		public decimal NetAdjustmentQuantity => CommodityAdjustmentList.Sum(x => x.AdjustmentQuantity);
		public decimal PostAdjustmentQuantity => NetAdjustmentQuantity + PreAdjustedQuantity;

		internal void RefreshTotals() {
			RaisePropertyChanged(() => NetAdjustmentQuantity);
			RaisePropertyChanged(() => PostAdjustmentQuantity);
		}

		private void onAddNewLineItem() {
			var newItem = new CommodityAdjustmentLineItemViewModel(clientEndpoint, dispatcher, this);
			CommodityAdjustmentList.Add(newItem);
			SelectedCommodityAdjustment = newItem;
		}

		private void onRemoveSelectedLineItem() {
			CommodityAdjustmentList.Remove(SelectedCommodityAdjustment);
			SelectedCommodityAdjustment = CommodityAdjustmentList.FirstOrDefault();
		}

		private void onComplete() {
			completeCommodityAdjustments(this);
		}

		private void onCancel() {
			completeCommodityAdjustments(null);
		}

		private List<RentContractListItem> getRentContractList(CropYearId currentCropYearId, CropId cropId) {
			var retList = clientEndpoint.GetView<RentContractListView>(currentCropYearId)
				.GetValue(new RentContractListView())
				.RentContracts
				.Where(x => x.Crop == cropId)
				.OrderBy(x => x.Name)
				.ToList();

			if (retList.Any()) {
				retList.Insert(0, new RentContractListItem() { Name = string.Empty });
			}

			return retList;
		}
	}
}