﻿using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using Landdb.ViewModel.Fields;
using Landdb.ViewModel.Secondary.YieldOperation;
using Landdb.ViewModel.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using System.Windows.Threading;

namespace Landdb.ViewModel.Yield.Popups {
	public class ChangeLoadCropZonesViewModel : FieldSelectionViewModel {

		private readonly Action<ChangeLoadCropZonesViewModel> cropZonesChanged;

		private readonly List<CropZoneDetails> originalCropZones;

		public ChangeLoadCropZonesViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, int currentCropYear, IEnumerable<CropZoneDetails> originalCropZones, Action<ChangeLoadCropZonesViewModel> onCropZonesChanged)
			: base(clientEndpoint, dispatcher, currentCropYear, null) {

			this.originalCropZones = new List<CropZoneDetails>(originalCropZones);

			SelectedTreeDisplayMode = new TreeDisplayModeDisplayItem("Crops");

			var initialCzIds = originalCropZones.Select(x => x.Id);

			if (RootTreeItemModels.Any()) {
				if (RootTreeItemModels.First() is GrowerTreeItemViewModel) {
					var toCheck = from farm in RootTreeItemModels.First().Children
								  from field in farm.Children
								  from cz in field.Children
								  where initialCzIds.Contains(cz.Id)
								  select cz;
					toCheck.ForEach(x => x.IsChecked = true);
				} else {
					var toCheck = from crop in RootTreeItemModels.First().Children
								  from farm in crop.Children
								  from field in farm.Children
								  from cz in field.Children
								  where initialCzIds.Contains(cz.Id)
								  select cz;
					toCheck.ForEach(x => x.IsChecked = true);
				}
			}

			TotalArea = TotalArea.Unit.GetMeasure(0);
			foreach (var selectedCz in SelectedCropZones) {
				var treeCz = originalCropZones.FirstOrDefault(x => x.Id == selectedCz.Id);
				selectedCz.SelectedArea = treeCz.Area;
				TotalArea = TotalArea.Unit.GetMeasure(TotalArea.Value + selectedCz.SelectedArea.Value);
			}

			cropZonesChanged += onCropZonesChanged;

			SaveChangesCommand = new RelayCommand(onSaveChanges);
			CancelChangesCommand = new RelayCommand(onCancelChanges);
		}

		public ICommand SaveChangesCommand { get; }
		public ICommand CancelChangesCommand { get; }

		public IEnumerable<CropZoneArea> AddedCropZones => from a in SelectedCropZones
														   where !originalCropZones.Exists(x => x.Id == a.Id)
														   select new CropZoneArea(a.Id, a.SelectedArea.Value, a.SelectedArea.Unit.Name);

		public IEnumerable<CropZoneArea> ChangedCropZones => from c in SelectedCropZones
															 where originalCropZones.Exists(x => x.Id == c.Id && (x.Area.Unit != c.SelectedArea.Unit || Math.Abs(x.Area.Value - c.SelectedArea.Value) > 0.0001))
															 select new CropZoneArea(c.Id, c.SelectedArea.Value, c.SelectedArea.Unit.Name);

		public IEnumerable<CropZoneId> RemovedCropZones => from r in originalCropZones
														   where !SelectedCropZones.Where(x => x.Id == r.Id).Any()
														   select r.Id;

		public bool HasChanges => AddedCropZones.Any() || ChangedCropZones.Any() || RemovedCropZones.Any();

		private void onSaveChanges() {
			if (!HasErrors) {
				cropZonesChanged(this);
			}
		}

		private void onCancelChanges() {
			cropZonesChanged(null);
		}
	}
}