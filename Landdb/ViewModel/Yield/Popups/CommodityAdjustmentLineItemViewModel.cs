﻿using GalaSoft.MvvmLight;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Threading;

namespace Landdb.ViewModel.Yield.Popups {
	public class CommodityAdjustmentLineItemViewModel : ViewModelBase {

		private readonly IClientEndpoint clientEndpoint;
		private readonly Dispatcher dispatcher;

		private readonly AddCommodityAdjustmentsViewModel parentVm;

		public CommodityAdjustmentLineItemViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, AddCommodityAdjustmentsViewModel parentVm) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			this.parentVm = parentVm;

			var now = DateTime.Now;
			AdjustmentDate = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0);

			SelectedRentContract = parentVm.RentContractList.FirstOrDefault();

			if (parentVm.ResponsibilityList.Any()) {
				SelectedResponsibility = parentVm.ResponsibilityList.First().Key;
			}
		}

		public decimal AdjustmentQuantity { get; set; }
		public DateTime AdjustmentDate { get; set; }
		public string Notes { get; set; }

		public bool IsRentContractSelected => _selectedRentContract != null && _selectedRentContract.Id != null && _selectedRentContract.Id.Id != Guid.Empty;
		public bool IsValid => AdjustmentQuantity != 0 && !string.IsNullOrWhiteSpace(AdjustmentNumber);

		private string _adjustmentNumber;
		public string AdjustmentNumber {
			get { return _adjustmentNumber; }
			set {
				_adjustmentNumber = value;
				RaisePropertyChanged(() => AdjustmentNumber);
			}
		}

		private RentContractListItem _selectedRentContract;
		public RentContractListItem SelectedRentContract {
			get { return _selectedRentContract; }
			set {
				_selectedRentContract = value;
				RaisePropertyChanged(() =>SelectedRentContract);
				RaisePropertyChanged(() =>IsRentContractSelected);

				DetermineGrowerResponsibility();
			}
		}

		private CommodityAdjustmentResponsibilityTypes _selectedResponsibility;
		public CommodityAdjustmentResponsibilityTypes SelectedResponsibility {
			get { return _selectedResponsibility; }
			set {
				_selectedResponsibility = value;
				RaisePropertyChanged(() =>SelectedResponsibility);

				DetermineGrowerResponsibility();
			}
		}

		private decimal _growerPercentage;
		public decimal GrowerPercentage {
			get { return _growerPercentage; }
			set {
				_growerPercentage = value;
				_landOwnerPercentage = 1 - _growerPercentage;
				RaisePropertyChanged(() =>GrowerPercentage);
				RaisePropertyChanged(() =>LandOwnerPercentage);
			}
		}

		private decimal _landOwnerPercentage;
		public decimal LandOwnerPercentage {
			get { return _landOwnerPercentage; }
			set {
				_landOwnerPercentage = value;
				_growerPercentage = 1 - _landOwnerPercentage;
				RaisePropertyChanged(() =>GrowerPercentage);
				RaisePropertyChanged(() =>LandOwnerPercentage);
			}
		}

		public string AdjustmentQuantityDisplayText {
			get { return $"{AdjustmentQuantity:N2} {parentVm.QuantityUnit.AbbreviatedDisplay}"; }
			set {
				if (string.IsNullOrWhiteSpace(value)) { return; }

				decimal parsedValue = 0;
				if (decimal.TryParse(value, out parsedValue)) {
					AdjustmentQuantity = parsedValue;
				}

				parentVm.RefreshTotals();
			}
		}

		private void DetermineGrowerResponsibility() {
			if (IsRentContractSelected) {
				switch (SelectedResponsibility) {
					case CommodityAdjustmentResponsibilityTypes.Grower:
						GrowerPercentage = 1;
						break;
					case CommodityAdjustmentResponsibilityTypes.LandOwner:
						GrowerPercentage = 0;
						break;
					case CommodityAdjustmentResponsibilityTypes.Split:
						var growerShare = clientEndpoint.GetView<RentContractDetailsView>(SelectedRentContract.Id)
							.GetValue(new RentContractDetailsView())
							.IncludedShareInfo
							.GrowerCropShare;

						GrowerPercentage = Convert.ToDecimal(growerShare);
						break;
					case CommodityAdjustmentResponsibilityTypes.Unknown:
					default:
						break;
				}
			}
		}
	}
}