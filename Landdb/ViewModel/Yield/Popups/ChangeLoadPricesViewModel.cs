﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Yield.Popups {
	public class ChangeLoadPricesViewModel : ViewModelBase {

		private readonly IClientEndpoint clientEndpoint;
		private readonly Dispatcher dispatcher;

		private readonly CropYearId currentCropYearId;

		private readonly Action<ChangeLoadPricesViewModel> loadPriceInfoChanged;

		private readonly ContractId originalRentContractId;

		private readonly LoadPriceInformation originalGrowerPriceInfo;
		private readonly LoadPriceInformation originalLandOwnerPriceInfo;

		public ChangeLoadPricesViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, CropYearId currentCropYearId, CropId cropId, decimal finalQuantityValue, CompositeUnit finalQuantityUnit, ContractId rentContractId, LoadPriceInformation growerPriceInfo, LoadPriceInformation landOwnerPriceInfo, Action<ChangeLoadPricesViewModel> onLoadPriceInfoChanged) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			this.currentCropYearId = currentCropYearId;

			originalRentContractId = rentContractId;

			originalGrowerPriceInfo = growerPriceInfo;
			originalLandOwnerPriceInfo = landOwnerPriceInfo;

			RentContractList = getRentContractList(cropId);
			ProductionContractList = getProductionContractList(cropId);

			LoadPriceSplitInfo = new LoadPriceSplitInfoViewModel(
				clientEndpoint,
				dispatcher,
				finalQuantityValue,
				finalQuantityUnit,
				rentContractId,
				growerPriceInfo,
				landOwnerPriceInfo,
				RentContractList,
				ProductionContractList
			);

			loadPriceInfoChanged += onLoadPriceInfoChanged;

			CompleteCommand = new RelayCommand(onComplete);
			CancelCommand = new RelayCommand(onCancel);
		}

		public ICommand CompleteCommand { get; }
		public ICommand CancelCommand { get; }

		public LoadPriceSplitInfoViewModel LoadPriceSplitInfo { get; }

		public List<RentContractListItem> RentContractList { get; }
		public List<ProductionContractListItem> ProductionContractList { get; }

		public bool HasChanges => 
			originalRentContractId != LoadPriceSplitInfo.GetRentContractId()
				|| originalGrowerPriceInfo != LoadPriceSplitInfo.GetGrowerPriceInfoValueObject()
				|| originalLandOwnerPriceInfo != LoadPriceSplitInfo.GetLandOwnerPriceInfoValueObject();

		private void onComplete() {
			loadPriceInfoChanged(this);
		}

		private void onCancel() {
			loadPriceInfoChanged(null);
		}

		private List<ProductionContractListItem> getProductionContractList(CropId cropId) {
			var productionContractList = clientEndpoint.GetView<ProductionContractListView>(currentCropYearId)
				.GetValue(new ProductionContractListView())
				.ProductionContracts
				.Where(x => x.Crop == cropId)
				.OrderBy(x => x.Name)
				.ToList();

			if (productionContractList.Any()) {
				productionContractList.Insert(0, new ProductionContractListItem() { Name = string.Empty });
			}

			return productionContractList;
		}

		private List<RentContractListItem> getRentContractList(CropId cropId) {
			var rentContractList = clientEndpoint.GetView<RentContractListView>(currentCropYearId)
				.GetValue(new RentContractListView())
				.RentContracts
				.Where(x => x.Crop == cropId)
				.OrderBy(x => x.Name)
				.ToList();

			if (rentContractList.Any()) {
				rentContractList.Insert(0, new RentContractListItem() { Name = Strings.GrowerShares_Text });
			}

			return rentContractList;
		}
	}
}