﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Domain.ReadModels.Yield;
using Landdb.ViewModel.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using System.Windows.Threading;

namespace Landdb.ViewModel.Yield.Popups {
	public class ChangeLoadCropViewModel : ViewModelBase {

		private readonly IClientEndpoint clientEndpoint;
		private readonly Dispatcher dispatcher;

		private readonly CropId originalCropId;
		private readonly string originalCommodityDescription;

		private readonly Action<ChangeLoadCropViewModel> cropChanged;

		public ChangeLoadCropViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, CropYearId currentCropYearId, CropId originalCropId, string originalCommodityDescription, Action<ChangeLoadCropViewModel> cropChanged) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			this.originalCropId = originalCropId;
			this.originalCommodityDescription = originalCommodityDescription;

			CropList = CropListItemViewModel.GetListItemsForCropYear(clientEndpoint, currentCropYearId);
			CommodityDescriptionList = getCommodityDescriptionList(currentCropYearId, originalCropId);

			SelectedCrop = CropList.FirstOrDefault(x => x.CropId == originalCropId);
			CommodityDescription = originalCommodityDescription;

			this.cropChanged += cropChanged;

			CompleteCommand = new RelayCommand(onComplete);
			CancelCommand = new RelayCommand(onCancel);
		}

		public ICommand CompleteCommand { get; }
		public ICommand CancelCommand { get; }

		// please do not be tempted to turn this into an ienumerable.
		// doing so keeps the ui from updating when the selected item is set programmatically.
		public List<CropListItemViewModel> CropList { get; }

		public IEnumerable<string> CommodityDescriptionList { get; }

		public CropListItemViewModel SelectedCrop { get; set; }

		public string CommodityDescription { get; set; }

		public bool HasCropChanged => GetSelectedCropId() != originalCropId;
		public bool HasCommodityDescriptionChanged => CommodityDescription != originalCommodityDescription;

		public CropId GetSelectedCropId() => SelectedCrop != null ? SelectedCrop.CropId : null;

		private void onComplete() {
			if (GetSelectedCropId() != null) {
				cropChanged(this);
			}
		}

		private void onCancel() {
			cropChanged(null);
		}

		private IEnumerable<string> getCommodityDescriptionList(CropYearId currentCropYearId, CropId cropId) {
			IEnumerable<string> retList = new List<string>();

			var appliedValuesView = clientEndpoint.GetView<AppliedLoadValuesView>(currentCropYearId).GetValue(() => null);
			if (appliedValuesView != null && appliedValuesView.AppliedCropValues.ContainsKey(cropId.Id)) {
				var cropProperties = appliedValuesView.AppliedCropValues[cropId.Id];
				retList = cropProperties.AppliedCommodityDescriptions.OrderBy(x => x);
			}

			return retList;
		}
	}
}