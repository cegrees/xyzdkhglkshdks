﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.CommoditySetting;
using System;
using System.Windows.Input;
using System.Windows.Threading;

namespace Landdb.ViewModel.Yield.Popups {
	public class ChangeLoadQuantitiesViewModel : ViewModelBase {

		private readonly IClientEndpoint clientEndpoint;
		private readonly Dispatcher dispatcher;

		private readonly LoadQuantityInformation_v2 originalQuantityInfo;

		private readonly Action<ChangeLoadQuantitiesViewModel> loadQuantityInfoChanged;

		public ChangeLoadQuantitiesViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, CropYearId cropYearId, CropId cropId, LoadQuantityInformation_v2 originalQuantityInfo, Action<ChangeLoadQuantitiesViewModel> onLoadQuantityInfoChanged) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			this.originalQuantityInfo = originalQuantityInfo;

			loadQuantityInfoChanged += onLoadQuantityInfoChanged;

			var allowedUnits = CompositeUnitHelper.GetWeightUnitsForCrop(clientEndpoint, dispatcher, cropYearId, cropId);

			QuantityInfo = new LoadQuantityInfoViewModel(clientEndpoint, dispatcher, originalQuantityInfo, allowedUnits);

			decimal standardDryMoisturePercentage = 0;
			decimal userDefinedShrinkPercentage = 0;
			decimal userDefinedTargetMoisturePercentage = 0;

			var cropDefaults = clientEndpoint.GetMasterlistService().GetCropDefaultsForCrop(cropId);
			if (cropDefaults != null) {
				standardDryMoisturePercentage = cropDefaults.DryMoisturePercentage.HasValue ? cropDefaults.DryMoisturePercentage.Value : 0;
				IsStandardMoistureShrinkUIVisible = standardDryMoisturePercentage > 0;
			}

			var commoditySettingsKey = new Tuple<CropYearId, CropId>(cropYearId, cropId);
			var commoditySettingsView = clientEndpoint.GetView<CommoditySettingsDetailsView>(commoditySettingsKey).GetValue(() => null);
			if (commoditySettingsView != null) {
				userDefinedShrinkPercentage = commoditySettingsView.EstimatedShrinkRatePercentage;
				userDefinedTargetMoisturePercentage = commoditySettingsView.HandlingShrinkTargetMoisturePercentage;
				IsHandlingShrinkUIVisible = userDefinedTargetMoisturePercentage > 0 && userDefinedShrinkPercentage > 0;
			}

			QuantityInfo.SetShrinkDefaults(
				standardDryMoisturePercentage,
				userDefinedShrinkPercentage,
				userDefinedTargetMoisturePercentage
			);

			IsInitialMoistureUIVisible = IsStandardMoistureShrinkUIVisible || IsHandlingShrinkUIVisible;

			CompleteCommand = new RelayCommand(onComplete);
			CancelCommand = new RelayCommand(onCancel);
		}

		public ICommand CompleteCommand { get; }
		public ICommand CancelCommand { get; }

		public LoadQuantityInfoViewModel QuantityInfo { get; }

		public bool IsInitialMoistureUIVisible { get; }
		public bool IsStandardMoistureShrinkUIVisible { get; }
		public bool IsHandlingShrinkUIVisible { get; }

		public bool HasChanges => originalQuantityInfo != QuantityInfo.GetQuantityInfoV2ValueObject();

		private void onComplete() {
			if (!QuantityInfo.HasErrors) {
				loadQuantityInfoChanged(this);
			}
		}

		private void onCancel() {
			loadQuantityInfoChanged(null);
		}
	}
}