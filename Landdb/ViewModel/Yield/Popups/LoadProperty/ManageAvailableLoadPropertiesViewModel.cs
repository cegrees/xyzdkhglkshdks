﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using System.Windows.Threading;

namespace Landdb.ViewModel.Yield.Popups {
	public class ManageAvailableLoadPropertiesViewModel : ViewModelBase {

		private readonly IClientEndpoint clientEndpoint;
		private readonly Dispatcher dispatcher;

		private readonly Action<ManageAvailableLoadPropertiesViewModel> propertySetSelectionsChanged;

		public ManageAvailableLoadPropertiesViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, CropId crop, IEnumerable<LoadPropertySetSelectionViewModel> propertySetSelections, Action<ManageAvailableLoadPropertiesViewModel> onSelectedLoadPropertiesChanged) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			// the tolists here force us to make a copy of the original list so that we're not working with the original list.
			AvailablePropertySets = propertySetSelections.Where(x => !x.RecommendedCropIds.Any() || x.RecommendedCropIds.Contains(crop.Id) || x.IsChecked).ToList();
			AdditionalPropertySets = propertySetSelections.Except(AvailablePropertySets).ToList();

			propertySetSelectionsChanged += onSelectedLoadPropertiesChanged;

			CompleteCommand = new RelayCommand(onComplete);
			CancelCommand = new RelayCommand(onCancel);
		}

		public ICommand CompleteCommand { get; }
		public ICommand CancelCommand { get; }

		public IEnumerable<LoadPropertySetSelectionViewModel> AvailablePropertySets { get; }
		public IEnumerable<LoadPropertySetSelectionViewModel> AdditionalPropertySets { get; }

		private void onComplete() {
			propertySetSelectionsChanged(this);
		}

		private void onCancel() {
			propertySetSelectionsChanged(null);
		}
	}
}