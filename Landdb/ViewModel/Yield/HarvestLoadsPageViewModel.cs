﻿using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Yield;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ValueObjects;
using Landdb.ViewModel.Secondary.Reports.Yield;
using Landdb.ViewModel.Secondary.Reports.Yield.Generators;
using Landdb.ViewModel.Secondary.YieldOperation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Threading;
using Landdb.Resources;
using Landdb.ViewModel.Resources.Equipment;

namespace Landdb.ViewModel.Yield {
	public class HarvestLoadsPageViewModel : AbstractListPage<LoadListItemViewModel, LoadDetailsViewModel>, IHandleViewRouting
	{
	    private readonly EquipmentPageViewModel equipmentPageViewModel;

		public HarvestLoadsPageViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, EquipmentPageViewModel equipmentPageViewModel)
			: base(clientEndpoint, dispatcher)
		{
		    this.equipmentPageViewModel = equipmentPageViewModel;
			this.FilterModel = new LoadPageFilterViewModel(clientEndpoint, new CropYearId(currentDataSourceId.Id, currentCropYear));
		    this.equipmentPageViewModel = equipmentPageViewModel;
			Messenger.Default.Register<YieldOperationAddedMessage>(this, msg => {
				RefreshItemsList(() => { SelectedListItem = ListItems.FirstOrDefault(x => x.OperationId == msg.LastYieldOperationAdded); });
			});

            PrintCommand = new RelayCommand(CreateReport);
		}

        public RelayCommand PrintCommand { get; set; }

        public void HandleRouteRequest(RouteRequestMessage message) {
			if (message.Route is LoadId) {
				var id = (LoadId)message.Route;

				var q = from x in ListItems ?? new System.Collections.ObjectModel.ObservableCollection<LoadListItemViewModel>()
						where x.Id == id
						select x;

				if (q.Any()) {
					SelectedListItem = q.FirstOrDefault();
				} else {
					RefreshItemsList(() => { SelectedListItem = ListItems.FirstOrDefault(x => x.Id == id); });
				}
			}
		}

		public override string GetEntityName() => Strings.EntityName_Load_Text.ToLower();
		public override string GetPluralEntityName() => Strings.Loads_Text.ToLower();

		protected override IEnumerable<LoadListItemViewModel> GetListItemModels() {
			var loadsMaybe = clientEndpoint.GetView<LoadListView>(new CropYearId(currentDataSourceId.Id, currentCropYear));

			if (loadsMaybe.HasValue) {
				var sortedProjectedLoads = from load in loadsMaybe.Value.Loads
										   where load.OperationType == YieldOperationTypes.FieldToSale || load.OperationType == YieldOperationTypes.FieldToStorage
										   orderby load.StartDateTime descending, load.LoadNumber descending
										   select new LoadListItemViewModel(load);

				return sortedProjectedLoads;
			} else {
				return null;
			}
		}

		protected override async Task<ScreenDescriptor> CreateScreenDescriptorForNewItemCreation() {
			var currentCropYearId = new CropYearId(currentDataSourceId.Id, currentCropYear);
			var yieldOperationId = new YieldOperationId(currentDataSourceId.Id, Guid.NewGuid());

			var vm = new HarvestLoadCreatorViewModel(clientEndpoint, dispatcher, currentCropYearId, yieldOperationId);

			return new ScreenDescriptor(typeof(Views.Secondary.YieldOperation.HarvestLoadCreatorView), Strings.CreateHarvestLoads_Text, vm);
		}

		protected override bool PerformItemRemoval(LoadListItemViewModel selectedItem) {
			var removeCommand = new RemoveLoad(selectedItem.Id, clientEndpoint.GenerateNewMetadata());
            clientEndpoint.SendOne(removeCommand);
            return true;
		}

		protected override IDomainCommand CreateSetCharmCommand(LoadListItemViewModel selectedItem, string charmName) {
			if (selectedItem.CharmName == charmName) { return null; }
			selectedItem.CharmName = charmName;
			return new FlagLoad(selectedItem.Id, clientEndpoint.GenerateNewMetadata(), charmName);
		}

		protected override LoadDetailsViewModel CreateDetailsModel(LoadListItemViewModel selectedItem) {
			var loadDetailsMaybe = clientEndpoint.GetView<LoadDetailsView>(selectedItem.Id);

			if (loadDetailsMaybe.HasValue) {
				return new LoadDetailsViewModel(clientEndpoint, dispatcher, loadDetailsMaybe.Value, equipmentPageViewModel);
			} else {
				return null;
			}
		}

        private void CreateReport()
        {
            var selectedItem = SelectedListItem;
            if (selectedItem == null) { return; }

            var reportGenerator = new CompletedLoadTicketReportGenerator(selectedItem, clientEndpoint);
            var vm = new SingleYieldReportViewModel(clientEndpoint, dispatcher, reportGenerator);
            Messenger.Default.Send(new ShowOverlayMessage()
            {
                ScreenDescriptor = new ScreenDescriptor(typeof(Views.Secondary.Reports.Work_Order.SingleWorkOrderView), Strings.ScreenDescriptor_ViewReport_Text, vm)
            });
        }
    }
}