using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Domain.ReadModels.Yield;
using Landdb.Domain.ReadModels.YieldLocation;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Secondary.Map;
using Landdb.ViewModel.Shared;
using Landdb.ViewModel.Yield.Popups.YieldLocation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Yield {
	public class YieldLocationDetailsViewModel : ViewModelBase {

		private readonly IClientEndpoint clientEndpoint;
		private readonly Dispatcher dispatcher;

		private readonly CropYearId currentCropYearId;

		private readonly Dictionary<LoadId, List<IncludedLoadViewModel>> loadLookup;
		private readonly List<Guid> managedLocations;
        private YieldLocationDetailsView detailsView;
		// TODO: kill this once the masterlist synchronization is working correctly
		private static readonly List<Guid> managedLocationIds = new List<Guid>() {
			new Guid("f248f013-6377-4d1a-9da9-e22b78ee4ce4"), // GreenStar
			new Guid("e8c7d6e2-dca5-4872-9dc1-6f1e3b12b0ee"), // CNH
			new Guid("aa5f9cf3-86fa-4b5f-9bf6-f9d27a071602"), // Avery
			new Guid("fe8765b3-e2ee-458b-8142-f664760c4cc9"), // Digi-Star
			new Guid("d9768223-c2b4-4dfa-ba82-7730336700a5"), // Rice Lake
			new Guid("9d9b5a65-8d93-4e94-9dc1-ebd09a7c05ff"), // R&R
		};

		public YieldLocationDetailsViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, DataSourceId currentDataSourceId, int currentCropYear, YieldLocationDetailsView detailsView) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

            this.detailsView = detailsView;
			LocationId = detailsView.Id;
			ParentLocationId = detailsView.ParentLocationId;
            MapAnnotationID = detailsView.MapAnnotationID;

			currentCropYearId = new CropYearId(currentDataSourceId.Id, currentCropYear);

			loadLookup = new Dictionary<LoadId, List<IncludedLoadViewModel>>();

			IncomingLoads = new List<IncludedLoadViewModel>();
			OutgoingLoads = new List<IncludedLoadViewModel>();
			CommodityDetailList = new List<IncludedCommodityDetails>();

			SubLocationList = new ObservableCollection<YieldLocationListItemViewModel>();

			managedLocations = clientEndpoint.GetMasterlistService()
				.GetManagedYieldLocations()
				.Locations
				.Select(x => x.Id)
				.ToList();

			if (!managedLocations.Any()) {
				managedLocations = managedLocationIds;
			}

			buildDetailsFromReadModel(detailsView);
            LocationMapVM = new LocationMapViewModel(clientEndpoint, dispatcher, currentDataSourceId, currentCropYear, detailsView);
            var holder = LocationMapVM.HasNoMapData;
            calculateCommodityBalances();

			ChangeLocationNameCommand = new RelayCommand(onChangeLocationName);
			UpdateLocationStreetAddressCommand = new RelayCommand(onUpdateLocationStreetAddress);
			UpdateLocationAssociatedCompanyCommand = new RelayCommand(onUpdateLocationCompany);
			ManageSubLocationsCommand = new RelayCommand(onManageSubLocations);
			UpdateLocationNotesCommand = new RelayCommand(onUpdateLocationNotes);
            UpdateLocationAdvancedInfoCommand = new RelayCommand(onViewAdvancedInfo);
            MapStorageLocationCommand = new RelayCommand(MapLocation);
        }

		#region Properties
		public ICommand ChangeLocationNameCommand { get; }
		public ICommand UpdateLocationStreetAddressCommand { get; }
		public ICommand UpdateLocationAssociatedCompanyCommand { get; }
		public ICommand ManageSubLocationsCommand { get; }
		public ICommand UpdateLocationNotesCommand { get; }
        public ICommand UpdateLocationAdvancedInfoCommand { get; }
        public ICommand MapStorageLocationCommand { get; }
        public YieldLocationId LocationId { get; }
		public YieldLocationId ParentLocationId { get; }

		public string PrimaryPhoneNumber { get; }
		public string PrimaryEmailAddress { get; }

		public List<IncludedLoadViewModel> IncomingLoads { get; }
		public List<IncludedLoadViewModel> OutgoingLoads { get; }
		public List<IncludedCommodityDetails> CommodityDetailList { get; }

		public ObservableCollection<YieldLocationListItemViewModel> SubLocationList { get; }

		public string Name { get; private set; }
		public string Descriptor { get; private set; }
		public int? LocationNumber { get; private set; }
		public YieldLocationTypes YieldLocationType { get; private set; }
		public StreetAddressViewModel StreetAddress { get; private set; }


		public CompanyId AssociatedCompanyId { get; private set; }
		public string AssociatedCompanyName { get; private set; }
		public string Notes { get; private set; }
        public MapAnnotationId MapAnnotationID { get; private set; }
        public LocationMapViewModel LocationMapVM { get; private set; }
        public bool HasPhoneNumber => !string.IsNullOrWhiteSpace(PrimaryPhoneNumber);
		public bool HasEmailAddress => !string.IsNullOrWhiteSpace(PrimaryEmailAddress);
		public bool HasContents => CommodityDetailList.Any();
		public string AssociatedCompanyDisplayText => AssociatedCompanyId != null ? AssociatedCompanyName : $"[{Strings.AssociateACompany_Text}]";
		public string NotesHeader => HasNotes ? Strings.Notes_Text : $"[{Strings.AddNotes_Text}]";
		public bool HasNotes => !string.IsNullOrWhiteSpace(Notes);
		public bool IsSaleLocation => YieldLocationType == YieldLocationTypes.Sale;
		public bool IsInventoryToggleVisible => YieldLocationType == YieldLocationTypes.Storage && managedLocations.Contains(LocationId.Id);
		public string SubLocationDisplayText => SubLocationList.Any() ? $"{SubLocationList.Count} {Strings.SubLocations_Text}" : $"[{Strings.AddSubLocations_Text}]";

		public bool IsSubLocationsEnabled {
			get {
				var segmentTypes = clientEndpoint.GetMasterlistService().GetYieldLocationSegmentTypes().SegmentTypeList.Select(x => x.Name);

				return !IsSaleLocation
					&& !IsInventoryToggleVisible
					&& (!segmentTypes.Contains(Descriptor) || SubLocationList.Any());
			}
		}

		public string StreetAddressDisplayText {
			get {
				var streetAddressIsPopulated = StreetAddress != null && !string.IsNullOrWhiteSpace(StreetAddress.MultiLineDisplay);
				return streetAddressIsPopulated ? StreetAddress.MultiLineDisplay : $"[{Strings.SpecifyStreetAddress_Text}]";
			}
		}

		private bool _isQuantityExcludedFromInventory;
		public bool IsQuantityExcludedFromInventory {
			get { return _isQuantityExcludedFromInventory; }
			set {
				_isQuantityExcludedFromInventory = value;

				clientEndpoint.SendOne(new ToggleInventoryExclusion(
					LocationId,
					clientEndpoint.GenerateNewMetadata(),
					value
				));

				RaisePropertyChanged(() => IsQuantityExcludedFromInventory);
			}
		}

        private bool _isPublic;
        public bool IsPublic {
            get
            {
                return _isPublic;
            }
            set
            {
                _isPublic = value;

                clientEndpoint.SendOne(new ToggleIsPublic(LocationId, clientEndpoint.GenerateNewMetadata(), value));

                RaisePropertyChanged(() => IsPublic);
            }
        }


        public bool IsAdvancedInfoVisible { get; set; }
		#endregion

		#region Change Handlers
		private void onChangeLocationName() {
			if (LocationId != null) {
				DialogFactory.ShowNameChangeDialog(Strings.DialogFactory_LocationName_Text, Name, newName => {
					clientEndpoint.SendOne(new ChangeYieldLocationName(
						LocationId,
						clientEndpoint.GenerateNewMetadata(),
						newName
					));

					Name = newName;
					base.RaisePropertyChanged(() => Name);

					Messenger.Default.Send(new YieldLocationDetailsUpdatedMessage(this));
				});
			}
		}

		private void onUpdateLocationStreetAddress() {
			if (LocationId != null) {
				DialogFactory.ShowStreetAddressChangeDialog(StreetAddress.GetValueObject(), newAddress => {
					clientEndpoint.SendOne(new UpdateYieldLocationStreetAddressInfo(
						LocationId,
						clientEndpoint.GenerateNewMetadata(),
						newAddress
					));

					StreetAddress = new StreetAddressViewModel(newAddress);
					base.RaisePropertyChanged(() => StreetAddress);
				});
			}
		}

		private void onUpdateLocationCompany() {
			if (LocationId != null) {
				var companyList = Resources.CompanyListItemViewModel.GetListItemsForCropYear(clientEndpoint, currentCropYearId, true);

				var currentlySelectedItem = companyList.FirstOrDefault(x => x.Id == AssociatedCompanyId);

				DialogFactory.ShowComboBoxSelectionChangeDialog(currentlySelectedItem, companyList, Strings.DialogFactory_AssociatedCompany_Text, newCompany => {
					var newCompanyId = newCompany != null ? newCompany.Id : null;

					clientEndpoint.SendOne(new UpdateYieldLocationAssociatedCompany(
						LocationId,
						clientEndpoint.GenerateNewMetadata(),
						newCompanyId
					));

					AssociatedCompanyId = newCompanyId;
					AssociatedCompanyName = newCompanyId != null ? newCompany.Name : string.Empty;

					base.RaisePropertyChanged(() => AssociatedCompanyName);
					base.RaisePropertyChanged(() => AssociatedCompanyDisplayText);
				});
			}
		}

		private void onManageSubLocations() {
			if (LocationId != null) {
				var vm = new Popups.ManageSubLocationsViewModel(clientEndpoint, dispatcher, currentCropYearId, LocationId, SubLocationList, changeVm => {
					if (changeVm != null && changeVm.HasChanges()) {
						foreach (var loc in changeVm.AddedLocations) {
							IDomainCommand cmd = null;

							if (loc.Id != null) {
								cmd = new ChangeYieldLocationParent(loc.Id, clientEndpoint.GenerateNewMetadata(), LocationId);
							} else {
								var phone = new PhoneNumber("Primary", PrimaryPhoneNumber);
								var email = new Email(PrimaryEmailAddress);

								cmd = new CreateYieldStorageLocation(
									loc.GetIdForSaving(currentCropYearId.DataSourceId),
									clientEndpoint.GenerateNewMetadata(),
									LocationId,
									loc.Name,
									StreetAddress.GetValueObject(),
									phone,
									email,
									currentCropYearId.Id,
									null, null
								);
							}

							if (cmd != null) {
								clientEndpoint.SendOne(cmd);
							}
						}

						foreach (var loc in changeVm.RemovedLocations) {
							clientEndpoint.SendOne(new ChangeYieldLocationParent(
								loc.Id,
								clientEndpoint.GenerateNewMetadata(),
								null
							));

							loc.RemoveParent();
						}

						SubLocationList.Clear();

						foreach (var loc in changeVm.SubLocationList) {
							SubLocationList.Add(loc);
						}

						// gotta figure out a better way to refresh the yield location tree,
						// because this is not reliable. at all.
						Messenger.Default.Send(new YieldLocationAddedMessage() { LastYieldLocationAdded = ParentLocationId });
						Thread.Sleep(2 * 1000);
					}

					Messenger.Default.Send(new HideOverlayMessage());
				});

				Messenger.Default.Send(new ShowOverlayMessage() {
					ScreenDescriptor = new ScreenDescriptor(typeof(Views.Yield.Popups.ManageSubLocationsView), Strings.ScreenDescriptor_ManageSubLocations_Text.ToLower(), vm)
				});
			}
		}
        private void onViewAdvancedInfo()
        {
            var vm = new ChangeLocationAdvancedInfoViewModel(clientEndpoint, detailsView);
            Messenger.Default.Send(new ShowPopupMessage()
            {
                ScreenDescriptor = new ScreenDescriptor(typeof(Views.Yield.Popups.ChangeLocationAdvancedInfoView), Strings.ScreenDescriptor_ViewAdvancedLocationInfo_Text.ToLower(), vm)
            });
        }

        private void onUpdateLocationNotes() {
			if (LocationId != null) {
				DialogFactory.ShowNotesChangeDialog(Notes, newNotes => {
					clientEndpoint.SendOne(new UpdateYieldLocationNotes(
						LocationId,
						clientEndpoint.GenerateNewMetadata(),
						newNotes
					));

					Notes = newNotes;

					base.RaisePropertyChanged(() => Notes);
					base.RaisePropertyChanged(() => HasNotes);
					base.RaisePropertyChanged(() => NotesHeader);
				});
			}
		}
		#endregion

		private void buildDetailsFromReadModel(YieldLocationDetailsView detailsView) {
			Name = detailsView.Name;
			YieldLocationType = detailsView.YieldLocationType;
			StreetAddress = new StreetAddressViewModel(detailsView.StreetAddress);
			AssociatedCompanyId = detailsView.AssociatedCompanyId;
			AssociatedCompanyName = detailsView.AssociatedCompanyName;
			Notes = detailsView.Notes;

			setDescriptorAndLocationNumber();

			// assign to private member so that we don't generate and send a change command
			_isQuantityExcludedFromInventory = detailsView.IsQuantityExcludedFromInventory;
            IsAdvancedInfoVisible = YieldLocationType == YieldLocationTypes.Storage ? true : false;
			var flattenedTreeView = clientEndpoint.GetView<FlattenedTreeHierarchyView>(currentCropYearId).GetValue(() => null);
			var locationDictionary = YieldLocationListItemViewModel.GetFlattenedListItemsForCropYear(clientEndpoint, currentCropYearId, false)
				.ToDictionary(x => x.Id, x => x.FullDisplay);

			if (detailsView.IncomingLoadsByCropYear.ContainsKey(currentCropYearId.Id)) {
				foreach (var load in detailsView.IncomingLoadsByCropYear[currentCropYearId.Id]) {
					if (!loadLookup.ContainsKey(load.Id)) {
						loadLookup.Add(load.Id, IncludedLoadViewModel.GetListFromIncludedLoad(flattenedTreeView, locationDictionary, load));
					}

					IncomingLoads.AddRange(loadLookup[load.Id].Where(x => x.DestinationLocationId == detailsView.Id));
				}
			}

			if (detailsView.OutgoingLoadsByCropYear.ContainsKey(currentCropYearId.Id)) {
				foreach (var load in detailsView.OutgoingLoadsByCropYear[currentCropYearId.Id]) {
					if (!loadLookup.ContainsKey(load.Id)) {
						loadLookup.Add(load.Id, IncludedLoadViewModel.GetListFromIncludedLoad(flattenedTreeView, locationDictionary, load));
					}

					var loadToAdd = loadLookup[load.Id].SingleOrDefault(x => x.SourceLocationId == detailsView.Id);
					if (loadToAdd != null) {
						OutgoingLoads.Add(loadToAdd); 
					}
				}
			}

			var subLocationList = YieldLocationListItemViewModel.GetFlattenedListItemsForCropYear(clientEndpoint, currentCropYearId, false)
				.Where(x => x.ParentLocation != null && x.ParentLocation.Id == detailsView.Id);

			var loadList = clientEndpoint.GetView<LoadListView>(currentCropYearId).GetValue(() => new LoadListView()).Loads;
			var yieldLocationDictionary = YieldLocationListItemViewModel.GetFlattenedListItemsForCropYear(clientEndpoint, currentCropYearId, false)
				.ToDictionary(x => x.Id, x => x.FullDisplay);

			var loadIds = new List<LoadId>();

			foreach (var subLoc in subLocationList) {
				SubLocationList.Add(subLoc);
				addSubLocationLoads(flattenedTreeView, subLoc, loadList, yieldLocationDictionary);
			}
		}

		private void setDescriptorAndLocationNumber() {
			var nonNumericTokens = new List<string>();

			var stringTokens = Name.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

			foreach (var token in stringTokens) {
				int parsedVal;

				if (int.TryParse(token, out parsedVal)) {
					if (LocationNumber == null) {
						LocationNumber = parsedVal;
					} else {
						// we found a second number in the name. ignore it
					}
				} else {
					nonNumericTokens.Add(token);
				}
			}

			Descriptor = string.Join(" ", nonNumericTokens);
		}

		private void addSubLocationLoads(FlattenedTreeHierarchyView treeView, YieldLocationListItemViewModel location, List<LoadListItem> loadList, Dictionary<YieldLocationId, string> yieldLocationDictionary) {
			foreach (var loadId in location.DirectlyAssociatedLoadIdList) {
				if (!loadLookup.ContainsKey(loadId)) {
					var loadListItem = loadList.SingleOrDefault(x => x.Id == loadId);

					if (loadListItem != null) {
						loadLookup.Add(loadId, IncludedLoadViewModel.GetIncludedLoadsFromListItem(treeView, yieldLocationDictionary, loadListItem));
					} else {
						loadLookup.Add(loadId, new List<IncludedLoadViewModel>());
					}
				}

				IncomingLoads.AddRange(loadLookup[loadId].Where(x => x.DestinationLocationId == location.Id));
				OutgoingLoads.AddRange(loadLookup[loadId].Where(x => x.SourceLocationId == location.Id));
			}

			foreach (var loc in location.SubLocationList) {
				addSubLocationLoads(treeView, loc, loadList, yieldLocationDictionary);
			}
		}

        private void MapLocation()
        {
            var vm = new MapStorageLocationViewModel(clientEndpoint, dispatcher, this.LocationId);
            Messenger.Default.Send(new ShowPopupMessage()
            {
                ScreenDescriptor = new ScreenDescriptor(typeof(Views.Secondary.Map.StorageLocationMapperView), Strings.ScreenDescriptor_MapLocation_Text.ToLower(), vm)
            });

        }

        private void calculateCommodityBalances() {
			var flattenedTreeView = clientEndpoint.GetView<FlattenedTreeHierarchyView>(currentCropYearId).GetValue(() => null);

			CommodityDetailList.Clear();

			var allLoads = IncomingLoads.Union(OutgoingLoads);

			// build a list of all commodities across all loads ordered by commodity name
			var commodityDisplayList = allLoads
				.Distinct(x => x.FullCommodityDisplayText)
				.OrderBy(x => x.FullCommodityDisplayText)
				.Select(x => x.FullCommodityDisplayText);

			foreach (var commodity in commodityDisplayList) {
				var loadsForThisCommodity = allLoads.Where(x => x.FullCommodityDisplayText == commodity);

				// build a list of all units across all loads in this commodity ordered by load count
				var orderedUnits = from load in loadsForThisCommodity
								   group load by load.QuantityUnit into g
								   orderby g.Count() descending
								   select g.Key;

				foreach (var targetUnit in orderedUnits) {
					decimal balance = 0;

					// add incoming loads of this crop to the balance
					// tolist for performance
					var incomingLoadsForThisCrop = IncomingLoads.Where(x => loadsForThisCommodity.Contains(x) && x.QuantityUnit == targetUnit).ToList();
					balance += incomingLoadsForThisCrop.Sum(x => x.LocationWeightedFinalQuantity);

					// subtract outgoing loads of this crop from the balance
					// tolist for performance
					var outgoingLoadsForThisCrop = OutgoingLoads.Where(x => loadsForThisCommodity.Contains(x) && x.QuantityUnit == targetUnit).ToList();
					balance -= outgoingLoadsForThisCrop.Sum(x => x.LocationWeightedDeliveredQuantity);

					var saleAmount = IsSaleLocation ? incomingLoadsForThisCrop.Where(x => x.IsLoadSold).Sum(x => x.GrowerSalePrice + x.LandOwnerSalePrice) : (decimal?)null;

					CommodityDetailList.Add(new IncludedCommodityDetails(commodity, balance, targetUnit, saleAmount) {
						IncomingLoads = (from l in incomingLoadsForThisCrop
										 orderby l.StartDateTime descending, l.LoadNumber descending
										 select l).ToList(),
						OutgoingLoads = (from l in outgoingLoadsForThisCrop
										 orderby l.StartDateTime descending, l.LoadNumber descending
										 select l).ToList(),
					});
				}
			}
		}
	}

	public class IncludedCommodityDetails : ViewModelBase {
		public IncludedCommodityDetails(string commodityDisplayText, decimal quantityValue, CompositeUnit unit, decimal? saleAmount) {
			CommodityDisplay = commodityDisplayText;
			QuantityValue = quantityValue;
			QuantityUnit = unit;
			SaleAmount = saleAmount;
		}

		public string CommodityDisplay { get; }
		public decimal QuantityValue { get; }
		public CompositeUnit QuantityUnit { get; }
		public decimal? SaleAmount { get; }

		public List<IncludedLoadViewModel> IncomingLoads { get; set; }
		public List<IncludedLoadViewModel> OutgoingLoads { get; set; }

		public bool HasIncomingLoads => IncomingLoads.Any();
		public bool HasOutgoingLoads => OutgoingLoads.Any();
		public int IncomingLoadCount => IncomingLoads.Count();
		public int OutgoingLoadCount => OutgoingLoads.Count();
		public bool IsSaleAmountVisible => SaleAmount != null;
		public string CommodityBalanceQuantityDisplayText => $"{QuantityValue:N2} {QuantityUnit.FullDisplay}";

		public string IncomingLoadHeader {
			get {
				var sum = IncomingLoads.Sum(x => x.LocationWeightedFinalQuantity);
				return $"{IncomingLoadCount} {Strings.Incoming_Text.ToLower()} {(IncomingLoadCount == 1 ? Strings.Load_Text.ToLower() : Strings.Loads_Text.ToLower())} ({sum:N2} {QuantityUnit.AbbreviatedDisplay})";
			}
		}

		public string OutgoingLoadHeader {
			get {
				var sum = OutgoingLoads.Sum(x => x.LocationWeightedDeliveredQuantity);
				return $"{OutgoingLoadCount} {Strings.Outgoing_Text.ToLower()} {(OutgoingLoadCount == 1 ? Strings.Load_Text : Strings.Loads_Text)} ({sum:N2} {QuantityUnit.AbbreviatedDisplay})";
			}
		}
	}
}