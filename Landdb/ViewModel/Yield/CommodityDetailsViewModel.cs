﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Domain.ReadModels.Yield;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Yield {
	public class CommodityDetailsViewModel : ViewModelBase {

		private readonly IClientEndpoint clientEndpoint;
		private readonly Dispatcher dispatcher;

		private readonly CropYearId currentCropYearId;

		private List<IncludedLoadViewModel> allHarvestedLoads;
		private List<IncludedLoadViewModel> allStoredLoads;
		private List<IncludedLoadViewModel> allSoldLoads;

		private IPageFilterViewModel harvestedLoadFilterModel;
		private IPageFilterViewModel storedLoadFilterModel;
		private IPageFilterViewModel soldLoadFilterModel;

		public CommodityDetailsViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, CropYearId currentCropYearId, CommodityDetailsView commodityDetailsView) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			this.currentCropYearId = currentCropYearId;

			buildDetailsFromProjection(commodityDetailsView);

			SelectedTabIndex = 0;

			CompatibleUnits = CompositeUnitHelper.GetWeightUnitsForCrop(clientEndpoint, dispatcher, currentCropYearId, commodityDetailsView.CropId).OrderBy(x => x.FullDisplay).ToList();
			CompatibleUnits.Insert(0, new CompositeUnit($"-{Strings.DoNotConsolidate_Text.ToLower()}-"));

			_selectedConsolidationUnit = CompatibleUnits.First();

			ApplyFilterCommand = new RelayCommand(onApplyFilter);
			ClearFilterCommand = new RelayCommand(onClearFiter);
		}

		public ICommand ApplyFilterCommand { get; }
		public ICommand ClearFilterCommand { get; }

		public List<CompositeUnit> CompatibleUnits { get; }

		public CropId CropId { get; private set; }
		public string CropName { get; private set; }
		public string CommodityDescription { get; private set; }
		public CommoditySummaryViewModel CommoditySummary { get; private set; }

		public List<IncludedLoadViewModel> HarvestedLoads { get; private set; }
		public List<IncludedLoadViewModel> StoredLoads { get; private set; }
		public List<IncludedLoadViewModel> SoldLoads { get; private set; }

		public IPageFilterViewModel FilterModel {
			get {
				if (SelectedTabIndex == 1) {
					return harvestedLoadFilterModel;
				} else if (SelectedTabIndex == 2) {
					return storedLoadFilterModel;
				} else if (SelectedTabIndex == 3) {
					return soldLoadFilterModel;
				} else {
					return new CommodityDetailsLoadFilterModel(clientEndpoint, currentCropYearId);
				}
			}
		}

		public string HarvestedLoadsFilterHeader => generateFilterHeader(Strings.FilterHeader_HarvestedLoad_Text.ToLower(), allHarvestedLoads, HarvestedLoads);
		public string StoredLoadsFilterHeader => generateFilterHeader(Strings.FilterHeader_StoredLoad_Text.ToLower(), allStoredLoads, StoredLoads);
		public string SoldLoadsFilterHeader => generateFilterHeader(Strings.FilterHeader_SoldLoad_Text.ToLower(), allSoldLoads, SoldLoads);

		private bool _isFilterVisible;
		public bool IsFilterVisible {
			get { return _isFilterVisible; }
			set {
				_isFilterVisible = value;
				RaisePropertyChanged(() => IsFilterVisible);
			}
		}

		private int _selectedTabIndex;
		public int SelectedTabIndex {
			get { return _selectedTabIndex; }
			set {
				_selectedTabIndex = value;
				RaisePropertyChanged(() => SelectedTabIndex);
				RaisePropertyChanged(() => FilterModel);
			}
		}

		public string FullCommodityDisplayText {
			get {
				if (string.IsNullOrWhiteSpace(CommodityDescription)) {
					return CropName;
				} else {
					return $"{CropName} : {CommodityDescription}";
				}
			}
		}

		private CompositeUnit _selectedConsolidationUnit;
		public CompositeUnit SelectedConsolidationUnit {
			get { return _selectedConsolidationUnit; }
			set {
				_selectedConsolidationUnit = value;
				RaisePropertyChanged(() => SelectedConsolidationUnit);

				var unitToPass = CompatibleUnits.IndexOf(value) > 0 ? value : null;

				CommoditySummary = new CommoditySummaryViewModel(
					clientEndpoint, 
					dispatcher, 
					currentCropYearId, 
					CropId, 
					CommodityDescription,
					unitToPass
				);

				RaisePropertyChanged(() => CommoditySummary);
			}
		}

		private void buildDetailsFromProjection(CommodityDetailsView commodityDetailsView) {
			CropId = commodityDetailsView.CropId;
			CropName = commodityDetailsView.CropName;
			CommodityDescription = commodityDetailsView.CommodityDescription;

			var flattenedTreeView = clientEndpoint.GetView<FlattenedTreeHierarchyView>(currentCropYearId).GetValue(() => null);

			var locationList = YieldLocationListItemViewModel.GetFlattenedListItemsForCropYear(clientEndpoint, currentCropYearId, false);

			var locationDictionary = locationList.ToDictionary(x => x.Id, x => x.FullDisplay);
			var excludedYieldLocationIds = locationList.Where(x => x.IsExcludedFromInventory).Select(x => x.Id.Id).ToList();

			CommoditySummary = new CommoditySummaryViewModel(clientEndpoint, dispatcher, currentCropYearId, commodityDetailsView.CropId, commodityDetailsView.CommodityDescription);

			if (CommoditySummary.UnitSummaries.Any()) {
				// TODO: this needs to be more efficient / less hackish
				// the linq queries pull data way too greedily and then cull duplicates with .distinct()
				// turn these into foreach and use load lookup dictionary similar to location details viewmodel?

				allHarvestedLoads = (from load in commodityDetailsView.HarvestedLoads
									 from loc in load.DestinationLocations
									 where !(excludedYieldLocationIds.Contains(loc.Id.Id))
									 orderby load.StartTime descending, load.LoadNumber descending
									 select IncludedLoadViewModel.GetListFromIncludedLoad(flattenedTreeView, locationDictionary, load))
										.SelectMany(x => x)
										.Distinct(x => $"{x.LoadId} {x.SourceLocationId} {x.DestinationLocationId}")
										.ToList();

				allStoredLoads = (from load in commodityDetailsView.StoredLoads
								  from loc in load.DestinationLocations
								  where !excludedYieldLocationIds.Contains(loc.Id.Id)
								  orderby load.StartTime descending, load.LoadNumber descending
								  select IncludedLoadViewModel.GetListFromIncludedLoad(flattenedTreeView, locationDictionary, load))
									  .SelectMany(x => x)
									  .Distinct(x => $"{x.LoadId} {x.SourceLocationId} {x.DestinationLocationId}")
									  .ToList();

				allSoldLoads = (from load in commodityDetailsView.SoldLoads
								orderby load.StartTime descending, load.LoadNumber descending
								select IncludedLoadViewModel.GetListFromIncludedLoad(flattenedTreeView, locationDictionary, load))
									.SelectMany(x => x)
									.Distinct(x => $"{x.LoadId} {x.SourceLocationId} {x.DestinationLocationId}")
									.ToList();

				harvestedLoadFilterModel = new CommodityDetailsLoadFilterModel(clientEndpoint, currentCropYearId);
				storedLoadFilterModel = new CommodityDetailsLoadFilterModel(clientEndpoint, currentCropYearId);
				soldLoadFilterModel = new CommodityDetailsLoadFilterModel(clientEndpoint, currentCropYearId);

				HarvestedLoads = allHarvestedLoads.Where(x => harvestedLoadFilterModel.FilterItem(x)).ToList();
				StoredLoads = allStoredLoads.Where(x => storedLoadFilterModel.FilterItem(x)).ToList();
				SoldLoads = allSoldLoads.Where(x => soldLoadFilterModel.FilterItem(x)).ToList();

				RaisePropertyChanged(() => HarvestedLoads);
				RaisePropertyChanged(() => StoredLoads);
				RaisePropertyChanged(() => SoldLoads);
			} else {
				HarvestedLoads = new List<IncludedLoadViewModel>();
				StoredLoads = new List<IncludedLoadViewModel>();
				SoldLoads = new List<IncludedLoadViewModel>();
			}
		}

		private void onApplyFilter() {
			IsFilterVisible = false;

			if (SelectedTabIndex == 1) {
				HarvestedLoads = allHarvestedLoads.Where(x => harvestedLoadFilterModel.FilterItem(x)).ToList();
				RaisePropertyChanged(() => HarvestedLoads);
				RaisePropertyChanged(() => HarvestedLoadsFilterHeader);
			} else if (SelectedTabIndex == 2) {
				StoredLoads = allStoredLoads.Where(x => storedLoadFilterModel.FilterItem(x)).ToList();
				RaisePropertyChanged(() => StoredLoads);
				RaisePropertyChanged(() => StoredLoadsFilterHeader);
			} else if (SelectedTabIndex == 3) {
				SoldLoads = allSoldLoads.Where(x => soldLoadFilterModel.FilterItem(x)).ToList();
				RaisePropertyChanged(() => SoldLoads);
				RaisePropertyChanged(() => SoldLoadsFilterHeader);
			}
		}

		private void onClearFiter() {
			IsFilterVisible = false;

			if (SelectedTabIndex == 1) {
				harvestedLoadFilterModel.ClearFilter();
				HarvestedLoads = allHarvestedLoads.ToList();
				RaisePropertyChanged(() => HarvestedLoads);
				RaisePropertyChanged(() => HarvestedLoadsFilterHeader);
			} else if (SelectedTabIndex == 2) {
				storedLoadFilterModel.ClearFilter();
				StoredLoads = allStoredLoads.ToList();
				RaisePropertyChanged(() => StoredLoads);
				RaisePropertyChanged(() => StoredLoadsFilterHeader);
			} else if (SelectedTabIndex == 3) {
				soldLoadFilterModel.ClearFilter();
				SoldLoads = allSoldLoads.ToList();
				RaisePropertyChanged(() => SoldLoads);
				RaisePropertyChanged(() => SoldLoadsFilterHeader);
			}
		}

		private string generateFilterHeader(string description, List<IncludedLoadViewModel> unfilteredList, List<IncludedLoadViewModel> filteredList) {
			var ofText = string.Empty;
			if (unfilteredList.Count != filteredList.Count) {
				ofText = $" ({Strings.Of_Text.ToLower()} {unfilteredList.Count})";
			}

			var pluralityModifier = filteredList.Count != 1 ? Strings.S_Text.ToLower() : string.Empty;

			return $"{filteredList.Count}{ofText} {description}{pluralityModifier}";
		}
	}
}