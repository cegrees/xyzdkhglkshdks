﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.CommoditySetting;
using Landdb.Domain.ReadModels.Contract;
using Landdb.Domain.ReadModels.Yield;
using Landdb.Domain.ReadModels.YieldLocation;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Yield {
	public class CommoditySummaryViewModel : ViewModelBase {

		private readonly IClientEndpoint clientEndpoint;
		private readonly Dispatcher dispatcher;

		private readonly Dictionary<ContractId, decimal> rentContractYieldShareDictionary;

		public CommoditySummaryViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, CropYearId currentCropYearId, CropId cropId, string commodityDescription, CompositeUnit consolidationUnit = null) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			CurrentCropYearId = currentCropYearId;
			CropId = cropId;
			CommodityDescription = commodityDescription;

			rentContractYieldShareDictionary = new Dictionary<ContractId, decimal>();

			var commodityDetailsKey = new CommodityDetailsViewKey(currentCropYearId, cropId.Id, commodityDescription);
			var commodityDetails = clientEndpoint.GetView<CommodityDetailsView>(commodityDetailsKey).GetValue(new CommodityDetailsView());

			UnitSummaries = getUnitSummariesList(commodityDetails);

			addHarvestLoadsToUnitSummaries(commodityDetails.HarvestedLoads);
			addSoldLoadsToUnitSummaries(commodityDetails.SoldLoads);
			addShrinkFromTransferLoads(commodityDetails.StoredLoads);

			if (consolidationUnit != null) {
				consolidateCompatibleUnitSummaries(consolidationUnit);
			}

			#region Commodity Adjustment Code (Currently Disabled)
			//foreach (var adj in commodityDetailsView.Adjustments) {
			//	UnitSummaries[adj.QuantityUnit].CommodityAdjustmentList.Add(new CommodityAdjustmentLineItemViewModel(clientEndpoint, adj));
			//}
			#endregion

			calculateUnitSummaryAveragesAndTotals(new Tuple<CropYearId, CropId>(currentCropYearId, cropId), commodityDescription);
		}

		public CropYearId CurrentCropYearId { get; }
		public CropId CropId { get; }
		public string CommodityDescription { get; }

		public Dictionary<CompositeUnit, CommodityCalculation> UnitSummaries { get; }

		private Dictionary<CompositeUnit, CommodityCalculation> getUnitSummariesList(CommodityDetailsView commodityDetails) {
			var allLoads = commodityDetails.HarvestedLoads
				.Union(commodityDetails.StoredLoads)
				.Union(commodityDetails.SoldLoads)
				.Distinct();

			var unitList = from h in allLoads
						   group h by h.QuantityUnit into g
						   orderby g.Count() descending
						   select g.Key;

			var unitSummaries = new Dictionary<CompositeUnit, CommodityCalculation>();

			foreach (var u in unitList) {
				unitSummaries.Add(u, new CommodityCalculation(clientEndpoint, dispatcher, this, u));
			}

			return unitSummaries;
		}

		private void addHarvestLoadsToUnitSummaries(IEnumerable<IncludedLoad> harvestedLoads) {
			var cropZoneYieldSharesView = clientEndpoint.GetView<CropZoneRentContractView>(CurrentCropYearId).GetValue(new CropZoneRentContractView());
			var czYieldShareDictionary = cropZoneYieldSharesView.CropZoneRentContracts;

			var yieldLocationList = clientEndpoint.GetView<YieldLocationListView>(new DataSourceId(CurrentCropYearId.DataSourceId)).GetValue(() => new YieldLocationListView());
			var excludedYieldLocations = from loc in yieldLocationList.YieldLocations
										 where loc.IsActiveInCropYear(CurrentCropYearId.Id) && loc.IsQuantityExcludedFromInventory
										 select loc.Id;

			// iterate through all harvested loads
			foreach (var load in harvestedLoads) {
				foreach (var loc in load.DestinationLocations) {
					if (excludedYieldLocations.Contains(loc.Id)) { continue; }

					var commodityCalc = UnitSummaries[load.QuantityUnit];

					commodityCalc.HarvestedGrossQuantity += load.LocationWeightedDeliveredQuantity;
					commodityCalc.HarvestedNetQuantity += load.LocationWeightedFinalQuantity;

					// determine the grower share percentage
					decimal growerSharePercentage = 1;

					if (load.IsLoadStored) {
						foreach (var cz in load.CropZones) {
							if (czYieldShareDictionary.ContainsKey(cz.Id)) {
								if (growerSharePercentage == 1) {
									growerSharePercentage = czYieldShareDictionary[cz.Id].GrowerCropShare;
									break;
								} else if (growerSharePercentage != czYieldShareDictionary[cz.Id].GrowerCropShare) {
									// if we get here, it probably means that the load has cropzones on it that fall under multiple rent contracts.
									// this is typically bad practice, but it's too late at this point to feasibly do anything about it.
									// TODO: warn the user that calculations may be off in this scenario?
									break;
								}
							}
						}
					} else if (load.IsShareSplit && load.RentContractId != null) {
						if (!rentContractYieldShareDictionary.ContainsKey(load.RentContractId)) {
							var rentContractDetailsView = clientEndpoint.GetView<RentContractDetailsView>(load.RentContractId).GetValue(new RentContractDetailsView());
							rentContractYieldShareDictionary.Add(load.RentContractId, Convert.ToDecimal(rentContractDetailsView.IncludedShareInfo.GrowerCropShare));
						}

						growerSharePercentage = rentContractYieldShareDictionary[load.RentContractId];
					} else if (load.RentContractId != null) {
						growerSharePercentage = 0;
					}

					var growerGrossHarvest = load.LocationWeightedDeliveredQuantity * growerSharePercentage;
					commodityCalc.GrowerHarvestedGrossQuantity += growerGrossHarvest;
					commodityCalc.LandOwnerHarvestedGrossQuantity += load.LocationWeightedDeliveredQuantity - growerGrossHarvest;

					var growerNetHarvest = load.LocationWeightedFinalQuantity * growerSharePercentage;
					commodityCalc.GrowerHarvestedNetQuantity += growerNetHarvest;
					commodityCalc.LandOwnerHarvestedNetQuantity += load.LocationWeightedFinalQuantity - growerNetHarvest;

					commodityCalc.AddLoadToShrinkDic(load, growerSharePercentage); 
				}
			}
		}

		private void addSoldLoadsToUnitSummaries(IEnumerable<IncludedLoad> soldLoads) {
			foreach (var load in soldLoads) {
				var commodityCalc = UnitSummaries[load.QuantityUnit];

				decimal growerSharePercentage = 0;
				decimal landOwnerSharePercentage = 0;

				if (load.IsShareSplit) {
					// load sale is split between grower and landowner using contract percentage
					if (!rentContractYieldShareDictionary.ContainsKey(load.RentContractId)) {
						var rentContractDetailsView = clientEndpoint.GetView<RentContractDetailsView>(load.RentContractId).GetValue(new RentContractDetailsView());
						rentContractYieldShareDictionary.Add(load.RentContractId, Convert.ToDecimal(rentContractDetailsView.IncludedShareInfo.GrowerCropShare));
					}

					growerSharePercentage = rentContractYieldShareDictionary[load.RentContractId];
					landOwnerSharePercentage = 1 - growerSharePercentage;

					commodityCalc.TotalRevenueSold += load.GrowerSalePrice;
				} else if (load.RentContractId == null) {
					// load is all grower share
					growerSharePercentage = 1;
					landOwnerSharePercentage = 0;

					commodityCalc.TotalRevenueSold += load.GrowerSalePrice;
				} else {
					// load is all landowner share
					growerSharePercentage = 0;
					landOwnerSharePercentage = 1;
				}

				commodityCalc.GrowerSoldGrossQuantity += load.DeliveredQuantityValue * growerSharePercentage;
				commodityCalc.GrowerSoldNetQuantity += load.FinalQuantityValue * growerSharePercentage;

				commodityCalc.LandOwnerSoldGrossQuantity += load.DeliveredQuantityValue * landOwnerSharePercentage;
				commodityCalc.LandOwnerSoldNetQuantity += load.FinalQuantityValue * landOwnerSharePercentage;

				commodityCalc.AddLoadToShrinkDic(load, growerSharePercentage);
			}
		}

		public void addShrinkFromTransferLoads(IEnumerable<IncludedLoad> storedLoads) {
			foreach(var load in storedLoads) {
				if (!load.IsLoadFromStorage) { continue; }

				var commodityCalc = UnitSummaries[load.QuantityUnit];

				// TODO: what to do with storage to storage shrink?
				// grower eats for now
				commodityCalc.AddLoadToShrinkDic(load, 1);
			}
		}

		private void consolidateCompatibleUnitSummaries(CompositeUnit consolidationUnit) {
			if (!UnitSummaries.ContainsKey(consolidationUnit)) {
				UnitSummaries.Add(consolidationUnit, new CommodityCalculation(clientEndpoint, dispatcher, this, consolidationUnit));
			}

			var targetSummary = UnitSummaries[consolidationUnit];

			var otherSummaries = UnitSummaries.Where(x => x.Value != null && x.Value.QuantityUnit != targetSummary.QuantityUnit).ToList();

			foreach (var summ in otherSummaries) {
				var val = summ.Value;

				if (CompositeUnitConverter.CanConvert(val.QuantityUnit, targetSummary.QuantityUnit)) {
					targetSummary.HarvestedGrossQuantity += CompositeUnitConverter.ConvertValue(val.HarvestedGrossQuantity, val.QuantityUnit, targetSummary.QuantityUnit);
					targetSummary.HarvestedNetQuantity += CompositeUnitConverter.ConvertValue(val.HarvestedNetQuantity, val.QuantityUnit, targetSummary.QuantityUnit);

					targetSummary.GrowerHarvestedGrossQuantity += CompositeUnitConverter.ConvertValue(val.GrowerHarvestedGrossQuantity, val.QuantityUnit, targetSummary.QuantityUnit);
					targetSummary.GrowerSoldGrossQuantity += CompositeUnitConverter.ConvertValue(val.GrowerSoldGrossQuantity, val.QuantityUnit, targetSummary.QuantityUnit);
					targetSummary.GrowerSoldNetQuantity += CompositeUnitConverter.ConvertValue(val.GrowerSoldNetQuantity, val.QuantityUnit, targetSummary.QuantityUnit);

					targetSummary.LandOwnerHarvestedGrossQuantity += CompositeUnitConverter.ConvertValue(val.LandOwnerHarvestedGrossQuantity, val.QuantityUnit, targetSummary.QuantityUnit);
					targetSummary.LandOwnerSoldGrossQuantity += CompositeUnitConverter.ConvertValue(val.LandOwnerSoldGrossQuantity, val.QuantityUnit, targetSummary.QuantityUnit);
					targetSummary.LandOwnerSoldNetQuantity += CompositeUnitConverter.ConvertValue(val.LandOwnerSoldNetQuantity, val.QuantityUnit, targetSummary.QuantityUnit);

					targetSummary.ReplaceShrinkDic(val.GetConvertedShrinkDic(targetSummary.QuantityUnit));

					UnitSummaries.Remove(summ.Key);
				}
			}
		}

		private void calculateUnitSummaryAveragesAndTotals(Tuple<CropYearId, CropId> commodityKey, string commodityDescription) {
			foreach (var commodityCalc in UnitSummaries.Values) {
				if (commodityCalc.QuantityUnit != null) {
					var commoditySettingsView = clientEndpoint.GetView<CommoditySettingsDetailsView>(commodityKey).GetValue(new CommoditySettingsDetailsView());

					var priceKey = new EstimatedPriceKey(commodityCalc.QuantityUnit, commodityDescription);

					if (commoditySettingsView.EstimatedUnitPrices.ContainsKey(priceKey.ToString())) {
						commodityCalc.CalculateAveragesAndTotals(commoditySettingsView.EstimatedUnitPrices[priceKey.ToString()]);
					} else {
						commodityCalc.CalculateAveragesAndTotals();
					}
				}
			}
		}

		public class LoadShrinkDetails {
			private LoadShrinkDetails(LoadId loadId, decimal growerShrink, decimal landOwnerShrink) {
				LoadId = loadId;

				GrowerShrink = growerShrink;
				LandOwnerShrink = landOwnerShrink;
			}

			public LoadId LoadId { get; }
			public decimal GrowerShrink { get; }
			public decimal LandOwnerShrink { get; }

			public static LoadShrinkDetails CreateFromPercentage(LoadId loadId, decimal growerSharePercentage, decimal totalShrink) {
				var growerShrink = growerSharePercentage * totalShrink;
				return new LoadShrinkDetails(loadId, growerShrink, totalShrink - growerShrink);
			}

			public static LoadShrinkDetails CreateFromValues(LoadId loadId, decimal growerShrink, decimal landOwnerShrink) {
				return new LoadShrinkDetails(loadId, growerShrink, landOwnerShrink);
			}
		}

		public class CommodityCalculation : ViewModelBase {

			private readonly IClientEndpoint clientEndpoint;
			private readonly Dispatcher dispatcher;

			private readonly CommoditySummaryViewModel parentVm;

			private readonly Dictionary<LoadId, LoadShrinkDetails> loadShrinkDetailsDic;

			public CommodityCalculation(IClientEndpoint clientEndpoint, Dispatcher dispatcher, CommoditySummaryViewModel parentVm, CompositeUnit unit) {
				this.clientEndpoint = clientEndpoint;
				this.dispatcher = dispatcher;

				this.parentVm = parentVm;

				QuantityUnit = unit;

				CommodityAdjustmentList = new ObservableCollection<CommodityAdjustmentLineItemViewModel>();

				loadShrinkDetailsDic = new Dictionary<LoadId, LoadShrinkDetails>();

				ChangeEstimatedPriceCommand = new RelayCommand(onChangeEstimatedPrice);
				AddAdjustmentsCommand = new RelayCommand(onAddAdjustmentsCommand);
				RemoveAdjustmentCommand = new RelayCommand(onRemoveAdjustmentCommand);
			}

			public ICommand ChangeEstimatedPriceCommand { get; }
			public ICommand AddAdjustmentsCommand { get; }
			public ICommand RemoveAdjustmentCommand { get; }

			public ObservableCollection<CommodityAdjustmentLineItemViewModel> CommodityAdjustmentList { get; }

			public CommodityAdjustmentLineItemViewModel SelectedAdjustment { get; set; }

			public decimal HarvestedGrossQuantity { get; set; }
			public decimal HarvestedNetQuantity { get; set; }
			public CompositeUnit QuantityUnit { get; set; }

			#region Grower Properties
			public decimal GrowerHarvestedGrossQuantity { get; set; }
			public decimal GrowerHarvestedNetQuantity { get; set; }
			public decimal GrowerSoldGrossQuantity { get; set; }
			public decimal GrowerSoldNetQuantity { get; set; }

			public decimal GrowerShrinkQuantity => loadShrinkDetailsDic.Values.Sum(x => x.GrowerShrink);
			public decimal GrowerUnsoldQuantity => GrowerHarvestedGrossQuantity - GrowerSoldNetQuantity - GrowerShrinkQuantity;

			public decimal GrowerAdjustmentQuantity {
				get {
					decimal total = 0;

					foreach (var adj in CommodityAdjustmentList) {
						switch (adj.Responsibility) {
							case CommodityAdjustmentResponsibilityTypes.Grower:
								total += adj.AdjustmentQuantity;
								break;
							case CommodityAdjustmentResponsibilityTypes.Split:
								total += adj.AdjustmentQuantity * adj.GrowerCropShare;
								break;
							case CommodityAdjustmentResponsibilityTypes.LandOwner:
							case CommodityAdjustmentResponsibilityTypes.Unknown:
							default:
								break;
						}
					}

					return total;
				}
			}

			public decimal AdjustedGrowerShareQuantity => GrowerSoldNetQuantity + GrowerUnsoldQuantity + GrowerAdjustmentQuantity;

			public decimal GrowerEstimatedInventoryPrice { get; set; }
			public decimal GrowerSoldAverage { get; set; }
			public decimal GrowerAverageRevenue { get; set; }

			public decimal TotalRevenueStored { get; set; }
			public decimal TotalRevenueSold { get; set; }
			public decimal TotalRevenue { get; set; }
			#endregion

			#region LandOwner Properties
			public decimal LandOwnerHarvestedGrossQuantity { get; set; }
			public decimal LandOwnerHarvestedNetQuantity { get; set; }
			public decimal LandOwnerSoldGrossQuantity { get; set; }
			public decimal LandOwnerSoldNetQuantity { get; set; }

			public decimal LandOwnerShrinkQuantity => loadShrinkDetailsDic.Values.Sum(x => x.LandOwnerShrink);
			public decimal LandOwnerUnsoldQuantity => LandOwnerHarvestedGrossQuantity - LandOwnerSoldNetQuantity - LandOwnerShrinkQuantity;

			public decimal LandOwnerAdjustmentQuantity {
				get {
					decimal total = 0;

					foreach (var adj in CommodityAdjustmentList) {
						switch (adj.Responsibility) {
							case CommodityAdjustmentResponsibilityTypes.LandOwner:
								total += adj.AdjustmentQuantity;
								break;
							case CommodityAdjustmentResponsibilityTypes.Split:
								total += adj.AdjustmentQuantity * (1 - adj.GrowerCropShare);
								break;
							case CommodityAdjustmentResponsibilityTypes.Unknown:
							case CommodityAdjustmentResponsibilityTypes.Grower:
							default:
								break;
						}
					}

					return total;
				}
			}

			public decimal AdjustedLandOwnerShareQuantity => LandOwnerSoldNetQuantity + LandOwnerUnsoldQuantity + LandOwnerAdjustmentQuantity;
			#endregion

			public decimal TotalSoldQuantity => GrowerSoldNetQuantity + LandOwnerSoldNetQuantity;
			public decimal TotalUnsoldQuantity => GrowerUnsoldQuantity + LandOwnerUnsoldQuantity + TotalAdjustmentQuantity;
			public decimal TotalShrinkQuantity => GrowerShrinkQuantity + LandOwnerShrinkQuantity;
			public decimal TotalAdjustmentQuantity => CommodityAdjustmentList.Sum(x => x.AdjustmentQuantity);
			public string QuantityUnitAbbreviation => QuantityUnit.AbbreviatedDisplay;
			public bool IsCommodityYieldShared => LandOwnerHarvestedGrossQuantity > 0;

			public void CalculateAveragesAndTotals(decimal newGrowerEstimatedInventoryPrice = 0) {
				GrowerEstimatedInventoryPrice = newGrowerEstimatedInventoryPrice;
				RaisePropertyChanged(() => GrowerEstimatedInventoryPrice);

				if (GrowerSoldNetQuantity > 0) {
					GrowerSoldAverage = TotalRevenueSold / GrowerSoldNetQuantity;
					RaisePropertyChanged(() => GrowerSoldAverage);
				}

				if (GrowerUnsoldQuantity > 0) {
					TotalRevenueStored = GrowerUnsoldQuantity * GrowerEstimatedInventoryPrice;
				} else {
					TotalRevenueStored = 0;
				}

				RaisePropertyChanged(() => TotalRevenueStored);

				TotalRevenue = TotalRevenueStored + TotalRevenueSold + GrowerAdjustmentQuantity;
				RaisePropertyChanged(() => TotalRevenue);

				if (AdjustedGrowerShareQuantity > 0 && GrowerUnsoldQuantity > 0) {
					GrowerAverageRevenue = TotalRevenue / AdjustedGrowerShareQuantity;
				} else {
					GrowerAverageRevenue = GrowerSoldAverage;
				}

				RaisePropertyChanged(() => GrowerAverageRevenue);
			}

			public void AddLoadToShrinkDic(IncludedLoad load, decimal growerSharePercentage) {
				if (!loadShrinkDetailsDic.ContainsKey(load.Id)) {
					loadShrinkDetailsDic.Add(load.Id, LoadShrinkDetails.CreateFromPercentage(load.Id, growerSharePercentage, load.LocationWeightedShrinkQuantity));
				}
			}

			public IEnumerable<LoadShrinkDetails> GetConvertedShrinkDic(CompositeUnit newUnit) {
				return from lsd in loadShrinkDetailsDic.Values
					   select LoadShrinkDetails.CreateFromValues(
						   lsd.LoadId,
						   CompositeUnitConverter.ConvertValue(lsd.GrowerShrink, QuantityUnit, newUnit),
						   CompositeUnitConverter.ConvertValue(lsd.LandOwnerShrink, QuantityUnit, newUnit)
						);
			}

			public void ReplaceShrinkDic(IEnumerable<LoadShrinkDetails> newShrinkDic) {
				loadShrinkDetailsDic.Clear();

				foreach(var lsd in newShrinkDic) {
					loadShrinkDetailsDic.Add(lsd.LoadId, lsd);
				}
			}

			private void onChangeEstimatedPrice() {
				DialogFactory.ShowNumericChangeDialog(Strings.EstimatedPrice_Text, GrowerEstimatedInventoryPrice, "C2", null, newValue => {
					var newSettingId = new CommoditySettingId(parentVm.CurrentCropYearId.DataSourceId, Guid.NewGuid());

					var cmd = new UpdateEstimatedCommodityPrice(
						newSettingId,
						clientEndpoint.GenerateNewMetadata(),
						parentVm.CropId,
						parentVm.CommodityDescription,
						parentVm.CurrentCropYearId.Id,
						newValue,
						QuantityUnit
					);

					clientEndpoint.SendOne(cmd);

					CalculateAveragesAndTotals(newValue);
				});
			}

			#region Commodity Adjustment Logic (Unused for now)
			private void onAddAdjustmentsCommand() {
				var vm = new Popups.AddCommodityAdjustmentsViewModel(clientEndpoint, dispatcher, parentVm.CurrentCropYearId, parentVm.CropId, TotalUnsoldQuantity, QuantityUnit, changeVm => {
					if (changeVm != null) {
						foreach (var adj in changeVm.CommodityAdjustmentList) {
							if (adj.IsValid) {
								var adjustmentId = new CommodityAdjustmentId(parentVm.CurrentCropYearId.DataSourceId, Guid.NewGuid());

								ContractId rentContractId = null;
								if (adj.IsRentContractSelected) {
									rentContractId = adj.SelectedRentContract.Id;
								}

								var cmd = new CreateCommodityAdjustment(
									adjustmentId,
									clientEndpoint.GenerateNewMetadata(),
									adj.AdjustmentNumber,
									adj.AdjustmentQuantity,
									QuantityUnit,
									rentContractId,
									rentContractId != null ? adj.SelectedResponsibility : CommodityAdjustmentResponsibilityTypes.Grower,
									adj.AdjustmentDate.ToUniversalTime(),
									parentVm.CurrentCropYearId.Id,
									parentVm.CropId,
									adj.Notes
								);

								clientEndpoint.SendOne(cmd);

								var li = new CommodityAdjustmentLineItem() {
									Id = adjustmentId,
									AdjustmentDate = adj.AdjustmentDate,
									AdjustmentNumber = adj.AdjustmentNumber,
									QuantityValue = adj.AdjustmentQuantity,
									QuantityUnit = QuantityUnit,
									RentContractId = rentContractId,
									Responsibility = adj.SelectedResponsibility,
									Notes = adj.Notes,
								};

								CommodityAdjustmentList.Add(new CommodityAdjustmentLineItemViewModel(clientEndpoint, li));

								RaisePropertyChanged(() => GrowerAdjustmentQuantity);
								RaisePropertyChanged(() => AdjustedGrowerShareQuantity);

								RaisePropertyChanged(() => LandOwnerAdjustmentQuantity);
								RaisePropertyChanged(() => AdjustedLandOwnerShareQuantity);

								RaisePropertyChanged(() => TotalAdjustmentQuantity);
								RaisePropertyChanged(() => TotalUnsoldQuantity);

								CalculateAveragesAndTotals(GrowerEstimatedInventoryPrice);
							}
						}

						Messenger.Default.Send(new HidePopupMessage());
					}
				});

				Messenger.Default.Send(new ShowPopupMessage() {
					ScreenDescriptor = new ScreenDescriptor(typeof(Views.Yield.Popups.AddCommodityAdjustmentsView), Strings.ScreenDescriptor_AddAdjustments_Text, vm)
				});
			}

			private void onRemoveAdjustmentCommand() {
				if (SelectedAdjustment != null) {
					var cmd = new RemoveCommodityAdjustment(SelectedAdjustment.Id, clientEndpoint.GenerateNewMetadata());
					clientEndpoint.SendOne(cmd);

					CommodityAdjustmentList.Remove(SelectedAdjustment);

					RaisePropertyChanged(() => GrowerAdjustmentQuantity);
					RaisePropertyChanged(() => AdjustedGrowerShareQuantity);

					RaisePropertyChanged(() => LandOwnerAdjustmentQuantity);
					RaisePropertyChanged(() => AdjustedLandOwnerShareQuantity);

					RaisePropertyChanged(() => TotalAdjustmentQuantity);
					RaisePropertyChanged(() => TotalUnsoldQuantity);

					CalculateAveragesAndTotals(GrowerEstimatedInventoryPrice);
				}
			}
		}

		public class CommodityAdjustmentLineItemViewModel {

			readonly CommodityAdjustmentLineItem li;

			public CommodityAdjustmentLineItemViewModel(IClientEndpoint clientEndpoint, CommodityAdjustmentLineItem li) {
				this.li = li;

				if (li.RentContractId != null) {
					var rentContractMaybe = clientEndpoint.GetView<RentContractDetailsView>(li.RentContractId);
					rentContractMaybe.IfValue(rentContractDetails => {
						RentContractName = rentContractDetails.Name;
						GrowerCropShare = Convert.ToDecimal(rentContractDetails.IncludedShareInfo.GrowerCropShare);
					});
				}
			}

			public CommodityAdjustmentId Id => li.Id;
			public DateTime AdjustmentDate => li.AdjustmentDate;
			public string AdjustmentNumber => li.AdjustmentNumber;
			public decimal AdjustmentQuantity => li.QuantityValue;
			public ContractId RentContractId => li.RentContractId;
			public CommodityAdjustmentResponsibilityTypes Responsibility => li.Responsibility;

			public string RentContractName { get; private set; }
			public decimal GrowerCropShare { get; private set; }

			public string AdjustmentQuantityDisplayText => $"{li.QuantityValue:N2} {li.QuantityUnit.AbbreviatedDisplay}";
		}
		#endregion
	}
}