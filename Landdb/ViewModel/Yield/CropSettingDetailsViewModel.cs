﻿using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.CommoditySetting;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Yield {
	public class CropSettingDetailsViewModel : ValidationViewModelBase {

		private readonly IClientEndpoint clientEndpoint;
		private readonly Dispatcher dispatcher;

		private readonly CropYearId currentCropYearId;
		private readonly CropId cropId;

		private CompositeUnit currentUnit;

		public CropSettingDetailsViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, CropYearId currentCropYearId, CommoditySettingsDetailsView detailsView) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			this.currentCropYearId = currentCropYearId;

			cropId = detailsView.CropId;

			AllowedUnits = new List<CompositeUnitListItem>();

			buildDetailsFromProjection(detailsView);
		}

		public List<CompositeUnitListItem> AllowedUnits { get; }

		public string CropName { get; private set; }
		public decimal? StandardDryMoisturePercentage { get; private set; }

		public bool HasStandardDryMoisturePercentage => StandardDryMoisturePercentage.HasValue;

		private CompositeUnitListItem _selectedUnit;
		public CompositeUnitListItem SelectedUnit {
			get { return _selectedUnit; }
			set {
				_selectedUnit = value;
				RaisePropertyChanged(() => SelectedUnit);

				var newUnit = value != null ? value.Unit : null;

				var changed = newUnit != currentUnit;

				if (changed) {
					currentUnit = newUnit;

					clientEndpoint.SendOne(new UpdateDefaultUnit(
						new CommoditySettingId(currentCropYearId.DataSourceId, Guid.NewGuid()),
						clientEndpoint.GenerateNewMetadata(),
						cropId,
						currentCropYearId.Id,
						currentUnit
					));
				}
			}
		}

		private decimal _shrinkPercentage;
		public decimal ShrinkPercentage {
			get { return _shrinkPercentage; }
			set {
				var dividedValue = value / 100;
				var changed = _shrinkPercentage != dividedValue;

				if (changed) {
					_shrinkPercentage = dividedValue;
					RaisePropertyChanged(() => ShrinkPercentage);

					var cmd = new UpdateEstimatedShrinkRatePercentage(
						new CommoditySettingId(currentCropYearId.DataSourceId, Guid.NewGuid()),
						clientEndpoint.GenerateNewMetadata(),
						cropId,
						currentCropYearId.Id,
						_shrinkPercentage
					);

					clientEndpoint.SendOne(cmd);
				}
			}
		}

		private decimal _targetMoisturePercentage;
        [Range(typeof(decimal), "0", "1", ErrorMessageResourceName = "ErrorMessage_TargetMoisturePercentage_Text", ErrorMessageResourceType = typeof(Strings))]
        public decimal TargetMoisturePercentage {
			get { return _targetMoisturePercentage; }
			set {
				var dividedValue = value / 100;
				var changed = _targetMoisturePercentage != dividedValue;

				if (changed) {
					_targetMoisturePercentage = dividedValue;
					RaisePropertyChanged(() => TargetMoisturePercentage);

					clientEndpoint.SendOne(new UpdateTargetMoisturePercentage(
						new CommoditySettingId(currentCropYearId.DataSourceId, Guid.NewGuid()),
						clientEndpoint.GenerateNewMetadata(),
						cropId,
						currentCropYearId.Id,
						_targetMoisturePercentage
					));
				}
			}
		}

		private void buildDetailsFromProjection(CommoditySettingsDetailsView detailsView) {
			CropName = detailsView.CropName;
			var cropDefaults = clientEndpoint.GetMasterlistService().GetCropDefaultsForCrop(detailsView.CropId);
			StandardDryMoisturePercentage = cropDefaults != null ? cropDefaults.DryMoisturePercentage : null;

			var unitList = CompositeUnitHelper.GetWeightUnitsForCrop(clientEndpoint, dispatcher, currentCropYearId, detailsView.CropId);

			AllowedUnits.AddRange(unitList.OrderBy(x => x.FullDisplay).Select(x => new CompositeUnitListItem(x.FullDisplay, x)));
			AllowedUnits.Insert(0, new CompositeUnitListItem($"-{Strings.LastUsed_Text}-", null));

			var key = new Tuple<CropYearId, CropId>(currentCropYearId, cropId);
			var cropSettingsView = clientEndpoint.GetView<CommoditySettingsDetailsView>(key).GetValue(() => null);
			if (cropSettingsView != null) {
				_selectedUnit = AllowedUnits.FirstOrDefault(x => x.Unit == cropSettingsView.LastUsedUnit);
				_shrinkPercentage = cropSettingsView.EstimatedShrinkRatePercentage;
				_targetMoisturePercentage = cropSettingsView.HandlingShrinkTargetMoisturePercentage;

				currentUnit = _selectedUnit != null ? _selectedUnit.Unit : null;
			}
		}

		public class CompositeUnitListItem {
			public CompositeUnitListItem(string name, CompositeUnit unit) {
				Name = name;
				Unit = unit;
			}

			public string Name { get; }
			public CompositeUnit Unit { get; }

			public override string ToString() => Name;
		}
	}
}