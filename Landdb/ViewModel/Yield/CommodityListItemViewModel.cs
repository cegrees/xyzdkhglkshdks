﻿using GalaSoft.MvvmLight;
using Landdb.Domain.ReadModels.Yield;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Landdb.ViewModel.Yield {
	public class CommodityListItemViewModel : ViewModelBase {
		public CommodityListItemViewModel(CommodityListItem li, IEnumerable<Guid> excludedLocationIds) {
			CropId = li.CropId;
			CropName = li.CropName;
			CommodityDescription = li.CommodityDescription;

			var allLoads = li.HarvestedLoads
				.Union(li.SoldLoads)
				.Union(li.StoredLoads)
				.Distinct(x => x.Id);

			allLoads = from load in allLoads
					   from loc in load.DestinationLocations
					   where !excludedLocationIds.Contains(loc.Id.Id)
					   select load;

			LoadCount = allLoads.Count();

			if (LoadCount != 0) {
				// determine the most frequently used unit
				QuantityUnit = (from h in allLoads
								group h by h.QuantityUnit into g
								orderby g.Count() descending
								select g.Key).First();


				// now accumulate the harvested load total quantity
				var filteredHarvestedLoads = from load in li.HarvestedLoads
											 from loc in load.DestinationLocations
											 where load.QuantityUnit == QuantityUnit
												 && !excludedLocationIds.Contains(loc.Id.Id)
											 select load;

				HarvestedQuantity = filteredHarvestedLoads.Sum(x => x.LocationWeightedFinalQuantity);

				// and accumulate the sold load total quantity
				SoldQuantity = li.SoldLoads.Where(x => x.QuantityUnit == QuantityUnit).Sum(x => x.LocationWeightedFinalQuantity);

				var shrink = allLoads.Where(x => x.QuantityUnit == QuantityUnit).Sum(x => x.LocationWeightedShrinkQuantity);

				var wetHarvestedQuantity = filteredHarvestedLoads.Sum(x => x.LocationWeightedDeliveredQuantity);
				UnsoldQuantity = wetHarvestedQuantity - SoldQuantity - shrink;
			}
		}

		public CropId CropId { get; }
		public string CropName { get; }
		public string CommodityDescription { get; }
		public CompositeUnit QuantityUnit { get; }
		public decimal HarvestedQuantity { get; }
		public decimal UnsoldQuantity { get; }
		public decimal SoldQuantity { get; }
		public int LoadCount { get; }

		public string HarvestedQuantityDisplayText => $"{HarvestedQuantity:N2} {QuantityUnit.AbbreviatedDisplay}";

		public string StoredQuantityDisplayText => $"{UnsoldQuantity:N2} {QuantityUnit.AbbreviatedDisplay}";

		public string SoldQuantityDisplayText => $"{SoldQuantity:N2} {QuantityUnit.AbbreviatedDisplay}";

		public string FullCommodityDisplayText {
			get {
				if (string.IsNullOrWhiteSpace(CommodityDescription)) {
					return CropName;
				} else {
					return $"{CropName} : {CommodityDescription}";
				}
			}
		}
	}
}