﻿using GalaSoft.MvvmLight;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.YieldLocation;
using System;
using System.Collections.Generic;
using System.Linq;
using Landdb.Resources;

namespace Landdb.ViewModel.Yield {
	public class CommodityDetailsLoadFilterModel : ViewModelBase, IPageFilterViewModel {

		private readonly IClientEndpoint clientEndpoint;
		private readonly CropYearId currentCropYearId;

		public CommodityDetailsLoadFilterModel(IClientEndpoint clientEndpoint, CropYearId currentCropYearId) {
			this.clientEndpoint = clientEndpoint;
			this.currentCropYearId = currentCropYearId;

			RefreshLists();
		}

		public List<YieldLocationListItem> YieldLocationList { get; private set; }

		private string _filterText;
		public string FilterText {
			get { return _filterText; }
			set {
				_filterText = value;
				RaisePropertyChanged(() => FilterText);
			}
		}

		private DateTime? _filterStartDate;
		public DateTime? FilterStartDate {
			get { return _filterStartDate; }
			set {
				_filterStartDate = value;
				RaisePropertyChanged(() => FilterStartDate);
			}
		}

		private DateTime? _filterEndDate;
		public DateTime? FilterEndDate {
			get { return _filterEndDate; }
			set {
				_filterEndDate = value;
				RaisePropertyChanged(() => FilterEndDate);
			}
		}

		private YieldLocationListItem _selectedDestinationLocation;
		public YieldLocationListItem SelectedFilterLocation {
			get { return _selectedDestinationLocation; }
			set {
				_selectedDestinationLocation = value;
				RaisePropertyChanged(() => SelectedFilterLocation);
			}
		}

		public void BeforeFilter() {
			// do nothing
		}

		public void RefreshLists() {
			YieldLocationList = clientEndpoint.GetView<YieldLocationListView>(new DataSourceId(currentCropYearId.DataSourceId))
				.GetValue(() => new YieldLocationListView())
				.YieldLocations
				.Where(x => !x.IsQuantityExcludedFromInventory && x.IsActiveInCropYear(currentCropYearId.Id))
				.OrderBy(x => x.Name)
				.ToList();

			YieldLocationList.Insert(0, new YieldLocationListItem() {
				Id = null,
				Name = Strings.AnyLocation_Text
            });

			SelectedFilterLocation = YieldLocationList.First();
		}

		public bool FilterItem(object item) {
			var load = item as IncludedLoadViewModel;

			bool? filter = null;

			if (!string.IsNullOrWhiteSpace(FilterText)) {
				var match = load.LoadNumber.ToLower().Contains(FilterText.ToLower());
				filter = filter.HasValue ? filter.Value && match : match;
			}

			if (FilterStartDate.HasValue) {
				var match = load.StartDateTime > FilterStartDate.Value;
				filter = filter.HasValue ? filter.Value && match : match;
			}

			if (FilterEndDate.HasValue) {
				var match = load.StartDateTime < FilterEndDate.Value.AddDays(1);
				filter = filter.HasValue ? filter.Value && match : match;
			}

			if (SelectedFilterLocation != null && SelectedFilterLocation.Id != null) {
				var match = load.DestinationLocationId == SelectedFilterLocation.Id || load.SourceLocationId == SelectedFilterLocation.Id;
				filter = filter.HasValue ? filter.Value && match : match;
			}

			return filter == null || filter.Value;
		}

		public void ClearFilter() {
			FilterText = null;
			FilterStartDate = null;
			FilterEndDate = null;
			SelectedFilterLocation = YieldLocationList.First();
		}

		public override void Cleanup() {
			base.Cleanup();
		}
	}
}
