﻿using GalaSoft.MvvmLight;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Yield;
using System;
using System.Collections.Generic;
using System.Linq;
using Landdb.Resources;

namespace Landdb.ViewModel.Yield {
	public class LoadPageFilterViewModel : ViewModelBase, IPageFilterViewModel {

		private readonly IClientEndpoint clientEndpoint;
		private readonly CropYearId currentCropYearId;

		public LoadPageFilterViewModel(IClientEndpoint clientEndpoint, CropYearId currentCropYearId) {
			this.clientEndpoint = clientEndpoint;
			this.currentCropYearId = currentCropYearId;

			RefreshLists();
		}

		public void RefreshLists() {
			var commodityList = clientEndpoint.GetView<CommodityListView>(currentCropYearId).GetValue(new CommodityListView()).Commodities;
			CropList = (from c in commodityList
						where c.HasLoads
						orderby c.CropName, c.CommodityDescription
						select new CropListItem(c.CropId, c.CropName, c.CommodityDescription)).ToList();

			CropList.Insert(0, new CropListItem(new CropId(Guid.Empty), Strings.AllCrops_Text, null));

			RaisePropertyChanged(() => CropList);

			SelectedCrop = CropList.FirstOrDefault();
		}

		public void ClearFilter() {
			FilterText = string.Empty;
			StartDate = null;
			EndDate = null;
			DestinationLocationType = YieldLocationTypes.Unknown;
			SelectedCrop = CropList.FirstOrDefault(x => x.CropId.Id == Guid.Empty);
		}

		public void BeforeFilter() {
			// do nothing
		}

		public bool FilterItem(object item) {
			var li = item as LoadListItemViewModel;

			// note: false return means the item is not displayed (filtered out)
			if (li == null) {
				return false;
			} else if (StartDate.HasValue && li.StartDateTime < StartDate.Value.Date) {
				return false;
			} else if (EndDate.HasValue && li.StartDateTime > (EndDate.Value.Date + TimeSpan.FromDays(1) - TimeSpan.FromSeconds(1))) {
				return false;
			} else if (DestinationLocationType != YieldLocationTypes.Unknown && DestinationLocationType != li.DestinationLocationType) {
				return false;
			} else if (SelectedCrop != null && SelectedCrop.CropId != null && SelectedCrop.CropId.Id != Guid.Empty) {
				if (string.IsNullOrWhiteSpace(SelectedCrop.CommodityDescription)) {
					var ret = SelectedCrop.CropId == li.Crop && string.IsNullOrWhiteSpace(li.CommodityDescription);
					return SelectedCrop.CropId == li.Crop && string.IsNullOrWhiteSpace(li.CommodityDescription);
				} else {
					var ret = SelectedCrop.CropId == li.Crop && SelectedCrop.CommodityDescription == li.CommodityDescription;
					return SelectedCrop.CropId == li.Crop && SelectedCrop.CommodityDescription == li.CommodityDescription;
				}
			} else if (!string.IsNullOrWhiteSpace(_filterText)) {
				var filterCompare = _filterText.ToLower();

				if (li.LoadNumber.ToLower().Contains(filterCompare)) {
					return true;
				} else {
					var loadDetailsView = clientEndpoint.GetView<LoadDetailsView>(li.Id).GetValue(() => null);
					if (loadDetailsView != null) {
						if (loadDetailsView.CropZones.Any(x => x.Name.ToLower().Contains(filterCompare))) {
							return true;
						} else if (string.Join("|", loadDetailsView.DestinationLocations.Select(x => x.Name)).ToLower().Contains(filterCompare)) {
							return true;
						} else if (string.Join("|", loadDetailsView.SourceLocations.Select(x => x.Name)).ToLower().Contains(filterCompare)) {
							return true;
						} else if ((loadDetailsView.TruckName ?? string.Empty).ToLower().Contains(filterCompare)) {
							return true;
						} else if ((loadDetailsView.DriverName ?? string.Empty).ToLower().Contains(filterCompare)) {
							return true;
						} else if (loadDetailsView.ReportedPropertySets != null && loadDetailsView.ReportedPropertySets.Any()) {
							var reportedPropertyStrings = from set in loadDetailsView.ReportedPropertySets
														  from prop in set.ReportedPropertyValues
														  select prop.ReportedValue;

							var combinedString = string.Join(" ", reportedPropertyStrings).ToLower();

							if (combinedString.Contains(_filterText)) {
								return true;
							}
						}
					}
				}

				return false;
			}

			return true;
		}

		public List<CropListItem> CropList { get; private set; }

		public static Dictionary<YieldLocationTypes, string> LocationDic {
			get {
				return new Dictionary<YieldLocationTypes, string>() {
					{ YieldLocationTypes.Unknown, Strings.DeliveredToAnyDestination_Text },
					{ YieldLocationTypes.Sale, Strings.SoldToASaleLocation_Text },
					{ YieldLocationTypes.Storage, Strings.DeliveredToAStorageLocation_Text }
				};
			}
		}

		private string _filterText;
		public string FilterText {
			get { return _filterText; }
			set {
				_filterText = value;
				RaisePropertyChanged(() => FilterText);
			}
		}

		private DateTime? _startDate;
		public DateTime? StartDate {
			get { return _startDate; }
			set {
				_startDate = value;
				RaisePropertyChanged(() => StartDate);
			}
		}

		private DateTime? _endDate;
		public DateTime? EndDate {
			get { return _endDate; }
			set {
				_endDate = value;
				RaisePropertyChanged(() => EndDate);
			}
		}

		private YieldLocationTypes _destinationLocationType;
		public YieldLocationTypes DestinationLocationType {
			get { return _destinationLocationType; }
			set {
				_destinationLocationType = value;
				RaisePropertyChanged(() => DestinationLocationType);
			}
		}

		private CropListItem _selectedCrop;
		public CropListItem SelectedCrop {
			get { return _selectedCrop; }
			set {
				_selectedCrop = value;
				RaisePropertyChanged(() => SelectedCrop);
			}
		}

		public class CropListItem {
			public CropListItem(CropId cropId, string cropName, string commodityDescription) {
				CropId = cropId;
				CropName = cropName;
				CommodityDescription = commodityDescription;
			}

			public CropId CropId { get; }
			public string CropName { get; }
			public string CommodityDescription { get; }

			public override string ToString() {
				if (string.IsNullOrWhiteSpace(CommodityDescription)) {
					return CropName;
				} else {
					return $"{CropName} : {CommodityDescription}";
				}
			}
		}
	}
}