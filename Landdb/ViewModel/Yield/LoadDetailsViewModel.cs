﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Contract;
using Landdb.Domain.ReadModels.Shared;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Domain.ReadModels.Yield;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ValueObjects;
using Landdb.ViewModel.Shared;
using Landdb.Views.Yield.Popups;
using NLog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Domain.ReadModels.Person;
using Landdb.Resources;
using Landdb.ViewModel.Resources;
using Landdb.ViewModel.Resources.Equipment;
using Lokad.Cqrs;
using static Landdb.ViewModel.Resources.Equipment.EquipmentListItemViewModel;
using static Landdb.ViewModel.Resources.PersonListItemViewModel;

namespace Landdb.ViewModel.Yield
{
    public class LoadDetailsViewModel : ViewModelBase
    {

        protected readonly IClientEndpoint clientEndpoint;
        protected readonly Dispatcher dispatcher;
        protected readonly LoadId loadId;
        protected readonly Logger log;
        private readonly EquipmentPageViewModel equipmentPageViewModel;

        public LoadDetailsViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, LoadDetailsView detailsView,
            EquipmentPageViewModel equipmentPageViewModel)
        {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            loadId = detailsView.Id;
            log = LogManager.GetCurrentClassLogger();
            this.equipmentPageViewModel = equipmentPageViewModel;
            this.equipmentPageViewModel.Subscribe(loadId, UpdateEquipmentName);
            CropZones = new ObservableCollection<CropZoneDetails>();

            buildDetailsFromReadModel(detailsView);

            ChangeLoadCropZoneSelectionCommand = new RelayCommand(onChangeCropZoneSelection);
            ChangeLoadSourceLocationCommand = new RelayCommand(onChangeLoadSourceLocation);
            ChangeLoadNumberCommand = new RelayCommand(onChangeLoadNumber);
            ChangeLoadCropCommand = new RelayCommand(onChangeLoadCrop);
            ChangeLoadQuantitiesCommand = new RelayCommand(onChangeLoadQuantities);
            ChangeLoadPricesCommand = new RelayCommand(onChangeLoadPrices);
            ChangeLoadDestinationLocationCommand = new RelayCommand(onChangeDestinationLocation);
            ChangeLoadTimeCommand = new RelayCommand(onChangeLoadTime);
            ChangeLoadTruckCommand = new RelayCommand(onChangeLoadTruck);
            ChangeLoadDriverCommand = new RelayCommand(onChangeLoadDriver);
            UpdateLoadNotesCommand = new RelayCommand(onUpdateLoadNotes);
            ChangeLoadPropertiesCommand = new RelayCommand(onChangeLoadProperties);
            UpdateLoadSourcesCommand = new RelayCommand(onUpdateLoadSources);
        }

        public ICommand ChangeLoadNumberCommand { get; }
        public ICommand ChangeLoadCropCommand { get; }
        public ICommand ChangeLoadQuantitiesCommand { get; }
        public ICommand ChangeLoadPricesCommand { get; }
        public ICommand ChangeLoadDestinationLocationCommand { get; }
        public ICommand ChangeLoadTimeCommand { get; }
        public ICommand ChangeLoadTruckCommand { get; }
        public ICommand ChangeLoadDriverCommand { get; }
        public ICommand UpdateLoadNotesCommand { get; }
        public ICommand ChangeLoadPropertiesCommand { get; }
        public ICommand UpdateLoadSourcesCommand { get; }
        public ICommand ChangeLoadCropZoneSelectionCommand { get; }
        public ICommand ChangeLoadSourceLocationCommand { get; }
        public ObservableCollection<CropZoneDetails> CropZones { get; }
        public List<DocumentDescriptor> SourceDocumentList { get; private set; }
        public List<LoadPropertySetViewModel> LoadProperties { get; private set; }
        public YieldOperationTypes OperationType { get; private set; }
        public string LoadNumber { get; private set; }
        public DateTime StartDateTime { get; private set; }
        public CropYearId CropYearId { get; private set; }
        public CropId CropId { get; protected set; }
        public string CropName { get; protected set; }
        public string CommodityDescription { get; private set; }
        public LoadSubLocationInfoViewModel SourceSubLocationInfo { get; set; }
        public LoadSubLocationInfoViewModel DestinationSubLocationInfo { get; set; }
        public ContractId RentContractId { get; private set; }
        public string RentContractName { get; private set; }
        public IncludedQuantityInfoViewModel IncludedLoadQuantityInfo { get; private set; }
        public IncludedSplitLoadInfoViewModel IncludedLoadPriceSplitInfo { get; private set; }
        public string Notes { get; private set; }
        public bool IsSaleLoad => OperationType == YieldOperationTypes.FieldToSale || OperationType == YieldOperationTypes.StorageToSale;
        public string StartDateDisplay => StartDateTime.ToString(@"MMMM d, yyyy");
        public string StartTimeDisplay => StartDateTime.ToString(@"h:mm tt");
        public bool HasRentContract => !string.IsNullOrWhiteSpace(RentContractName);
        public string NotesHeader => HasNotes ? Strings.Notes_Text : $"[{Strings.AddNotes_Text}]";
        public bool HasNotes => !string.IsNullOrWhiteSpace(Notes);
        public bool HasLoadProperties => LoadProperties.Any();
        public int SourceDocumentCount => SourceDocumentList != null ? SourceDocumentList.Count() : 0;

        public string TruckName { get; private set; }
        public string TruckDisplayText => string.IsNullOrWhiteSpace(TruckName) ? $"[{Strings.AssignATruck_Text}]" : TruckName;
        private EquipmentId equipmentId;

        public EquipmentId TruckId
        {
            get => equipmentId;
            private set
            {
                equipmentPageViewModel?.Unsubscribe(loadId);
                equipmentId = value;
                equipmentPageViewModel?.Subscribe(loadId, UpdateEquipmentName);
            }
        }

        private void UpdateEquipmentName(EquipmentId equipmentId, string updatedEquipmentName)
        {
            if (TruckId != null)
            {
                if (TruckId.Equals(equipmentId))
                {
                    TruckName = updatedEquipmentName;
                    RaisePropertyChanged(() => TruckName);
                    RaisePropertyChanged(() => TruckDisplayText);
                }
            }
        }

        public PersonId DriverId { get; private set; }
        public string DriverName { get; private set; }
        public string DriverDisplayText => DriverId == null ? $"[{Strings.AssignADriver_Text}]" : DriverName;

        private void UpdateDriver(PersonId driverId)
        {
            DriverId = driverId;
            DriverName = GetUpdatedDriverName(clientEndpoint, driverId);
            RaisePropertyChanged(() => DriverId);
            RaisePropertyChanged(() => DriverName);
            RaisePropertyChanged(() => DriverDisplayText);
        }

        private static string GetUpdatedDriverName(IClientEndpoint clientEndpoint, PersonId personId)
        {
            if (personId != null)
            {
                Maybe<PersonDetailsView> possiblePerson = clientEndpoint.GetView<PersonDetailsView>(personId);
                return possiblePerson.HasValue ? possiblePerson.Value.Name : string.Empty;
            }
            return string.Empty;
        }

        // until we have the ability to attach source documents to yield records,
        // hide the button unless documents are attached already.
        public bool HasSourceDocuments => SourceDocumentCount > 0;

        public string LoadPropertiesHeader
        {
            get
            {
                string retString = null;

                if (HasLoadProperties)
                {
                    var count = LoadProperties.Count;
                    retString = $"{count} {Strings.LoadProperties_Text}";
                }
                else
                {
                    retString = $"[{Strings.AddLoadProperties_Text}]";
                }

                return retString;
            }
        }

        public string FullCommodityDisplayText
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(CommodityDescription))
                {
                    return $"{CropName} ({CommodityDescription})";
                }
                else
                {
                    return CropName;
                }
            }
        }

        //public string DestinationLocationDisplayText {
        //	get {
        //		if (DestinationLocations.Count == 1) {
        //			return DestinationLocations.Single().Name;
        //		} else {
        //			var firstLoc = DestinationLocations.First();
        //			if (firstLoc.pa)
        //		}
        //	}
        //}

        //public string DestinationLocationStreetAddressDisplay {
        //	get {
        //		if (DestinationLocations.Count == 1) {
        //			return DestinationLocations.First().StreetAddress.MultiLineDisplay;
        //		} else {
        //			return $"{DestinationLocations.First().Name}\nto {DestinationLocations.Last().Name}";
        //		}
        //	}
        //}

        public string SourceDescription
        {
            get
            {
                var czCount = CropZones.Count();
                var firstCz = CropZones.FirstOrDefault();

                var plurality = czCount != 1 ? Strings.Fields_Text : Strings.Field_Text;
                var totalArea = CropZones.Sum(x => x.Area.Value);
                var areaUnit = firstCz != null ? firstCz.Area.Unit.FullDisplay : ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit;

                return $"{Strings.From_Text} {czCount} {plurality} ({totalArea:N2} {areaUnit})";
            }
        }

        public string SourcesDisplayText
        {
            get
            {
                var displayText = string.Empty;

                if (SourceDocumentCount > 0)
                {
                    displayText = $"{SourceDocumentCount} {Strings.SourceDocuments_Text}";
                }
                else
                {
                    displayText = $"[{Strings.AttachSourceDocuments_Text}]";
                }

                return displayText;
            }
        }

        #region Change/Update Handlers
        private void onChangeCropZoneSelection()
        {
            if (loadId != null)
            {
                var vm = new Popups.ChangeLoadCropZonesViewModel(clientEndpoint, dispatcher, CropYearId.Id, CropZones, changeVm => {
                    if (changeVm != null && changeVm.HasChanges)
                    {
                        clientEndpoint.SendOne(new ChangeLoadIncludedCropZones(
                            loadId,
                            clientEndpoint.GenerateNewMetadata(),
                            changeVm.AddedCropZones.ToArray(),
                            changeVm.RemovedCropZones.ToArray(),
                            changeVm.ChangedCropZones.ToArray()
                        ));

                        var treeView = clientEndpoint.GetView<FlattenedTreeHierarchyView>(CropYearId).GetValue(new FlattenedTreeHierarchyView());

                        var includedCropZones = new List<IncludedCropZone>();
                        foreach (var cz in changeVm.SelectedCropZones)
                        {
                            string czName = cz.Name;

                            var treeItem = treeView.Items.FirstOrDefault(x => x.CropZoneId == cz.Id);
                            if (treeItem != null)
                            {
                                var treeItemName = treeItem.GetFullName();

                                if (!string.IsNullOrWhiteSpace(treeItemName)) { czName = treeItemName; }
                            }

                            includedCropZones.Add(new IncludedCropZone()
                            {
                                Id = cz.Id,
                                Name = czName,
                                Area = cz.SelectedArea as AgC.UnitConversion.Area.AreaMeasure,
                            });
                        }

                        refreshCropZones(includedCropZones);
                    }

                    Messenger.Default.Send(new HideOverlayMessage());
                });

                Messenger.Default.Send(new ShowOverlayMessage()
                {
                    ScreenDescriptor = new ScreenDescriptor(typeof(Views.Shared.Popups.ChangeCropZoneSelectionView), Strings.ScreenDescriptor_ChangeLoadCropzones_Text.ToLower(), vm)
                });
            }
        }

        private void onChangeLoadSourceLocation()
        {
            if (loadId != null)
            {
                var segmentTypes = clientEndpoint.GetMasterlistService()
                    .GetYieldLocationSegmentTypes()
                    .SegmentTypeList
                    .Select(x => x.Name);

                var locList = YieldLocationListItemViewModel.GetFlattenedListItemsForCropYear(clientEndpoint, CropYearId, false);

                var availableSources = from loc in locList
                                       let hasSublocs = loc.SubLocationList.Any()
                                       where loc.YieldLocationType == SourceSubLocationInfo.SelectedTopLevelLocation.YieldLocationType
                                           && (!segmentTypes.Contains(loc.Descriptor) || hasSublocs)
                                       select loc;

                var currentSource = availableSources.SingleOrDefault(x => x.Id == SourceSubLocationInfo.SelectedTopLevelLocation.Id);

                var vm = new Popups.ChangeLoadLocationViewModel(
                    clientEndpoint,
                    availableSources,
                    currentSource,
                    SourceSubLocationInfo.FromSelectedSubLocation,
                    SourceSubLocationInfo.ToSelectedSubLocation,
                    Strings.SourceLocation_Text,
                    IncludedLoadQuantityInfo.BaseQuantityInfo.FinalQuantityValue,
                    IncludedLoadQuantityInfo.BaseQuantityInfo.FinalQuantityUnit,
                    changeVm => {
                        if (changeVm != null && changeVm.HasChanges())
                        {
                            var formerLocationIds = SourceSubLocationInfo.GetSelectedLocationIds();
                            var newLocationIds = changeVm.SubLocationInfo.GetSelectedLocationIds();
                            var addedLocationIds = newLocationIds.Except(formerLocationIds);
                            var removedLocationIds = formerLocationIds.Except(newLocationIds);

                            if (addedLocationIds.Any(x => x != null) == false)
                            {
                                return;
                            }

                            clientEndpoint.SendOne(new ChangeLoadSourceLocations(
                                loadId,
                                clientEndpoint.GenerateNewMetadata(),
                                addedLocationIds.ToArray(),
                                removedLocationIds.ToArray()
                            ));

                            SourceSubLocationInfo = changeVm.SubLocationInfo;
                            RaisePropertyChanged(() => SourceSubLocationInfo);
                        }

                        Messenger.Default.Send(new HidePopupMessage());
                    });

                Messenger.Default.Send(new ShowPopupMessage()
                {
                    ScreenDescriptor = new ScreenDescriptor(typeof(ChangeLoadLocationView), Strings.ScreenDescriptor_ChangeLoadSource_Text.ToLower(), vm)
                });
            }
        }

        private void onChangeDestinationLocation()
        {
            if (loadId != null)
            {
                var segmentTypes = clientEndpoint.GetMasterlistService()
                    .GetYieldLocationSegmentTypes()
                    .SegmentTypeList
                    .Select(x => x.Name);

                var locList = YieldLocationListItemViewModel.GetFlattenedListItemsForCropYear(clientEndpoint, CropYearId, false);

                var availableDestinations = from loc in locList
                                            let hasSublocs = loc.SubLocationList.Any()
                                            where loc.YieldLocationType == DestinationSubLocationInfo.SelectedTopLevelLocation.YieldLocationType
                                                && (!segmentTypes.Contains(loc.Descriptor) || hasSublocs)
                                            select loc;

                var currentDestination = availableDestinations.SingleOrDefault(x => x.Id == DestinationSubLocationInfo.SelectedTopLevelLocation.Id);

                var vm = new Popups.ChangeLoadLocationViewModel(
                    clientEndpoint,
                    availableDestinations,
                    currentDestination,
                    DestinationSubLocationInfo.FromSelectedSubLocation,
                    DestinationSubLocationInfo.ToSelectedSubLocation,
                    Strings.DestinationLocation_Text,
                    IncludedLoadQuantityInfo.BaseQuantityInfo.FinalQuantityValue,
                    IncludedLoadQuantityInfo.BaseQuantityInfo.FinalQuantityUnit,
                    changeVm => {
                        if (changeVm != null && changeVm.HasChanges())
                        {
                            var formerLocationIds = DestinationSubLocationInfo.GetSelectedLocationIds();
                            var newLocationIds = changeVm.SubLocationInfo.GetSelectedLocationIds();
                            var addedLocationIds = newLocationIds.Except(formerLocationIds);
                            var removedLocationIds = formerLocationIds.Except(newLocationIds);

                            clientEndpoint.SendOne(new ChangeLoadDestinationLocations(
                                loadId,
                                clientEndpoint.GenerateNewMetadata(),
                                addedLocationIds.ToArray(),
                                removedLocationIds.ToArray()
                            ));

                            DestinationSubLocationInfo = changeVm.SubLocationInfo;
                            RaisePropertyChanged(() => DestinationSubLocationInfo);
                        }

                        Messenger.Default.Send(new HidePopupMessage());
                    });

                Messenger.Default.Send(new ShowPopupMessage()
                {
                    ScreenDescriptor = new ScreenDescriptor(typeof(ChangeLoadLocationView), Strings.ScreenDescriptor_ChangeLoadDestination_Text.ToLower(), vm)
                });
            }
        }

        private void onChangeLoadNumber()
        {
            if (loadId != null)
            {
                DialogFactory.ShowNameChangeDialog(Strings.LoadNumber_Text, LoadNumber, newName => {
                    clientEndpoint.SendOne(new ChangeLoadNumber(
                        loadId,
                        clientEndpoint.GenerateNewMetadata(),
                        newName
                    ));

                    LoadNumber = newName;
                    base.RaisePropertyChanged(() => LoadNumber);
                });
            }
        }

        private void onChangeLoadCrop()
        {
            if (loadId != null)
            {
                var vm = new Popups.ChangeLoadCropViewModel(clientEndpoint, dispatcher, CropYearId, CropId, CommodityDescription, changeVm => {
                    if (changeVm != null)
                    {
                        if (changeVm.HasCropChanged)
                        {
                            var newCropId = changeVm.GetSelectedCropId();

                            clientEndpoint.SendOne(new ChangeLoadCrop(
                                loadId,
                                clientEndpoint.GenerateNewMetadata(),
                                newCropId
                            ));

                            CropId = newCropId;
                            CropName = changeVm.SelectedCrop.CropName;

                            base.RaisePropertyChanged(() => CropId);
                            base.RaisePropertyChanged(() => CropName);
                            base.RaisePropertyChanged(() => FullCommodityDisplayText);
                        }

                        if (changeVm.HasCommodityDescriptionChanged)
                        {
                            clientEndpoint.SendOne(new ChangeLoadCommodityDescription(
                                loadId,
                                clientEndpoint.GenerateNewMetadata(),
                                changeVm.CommodityDescription
                            ));

                            CommodityDescription = changeVm.CommodityDescription;
                            base.RaisePropertyChanged(() => CommodityDescription);
                            base.RaisePropertyChanged(() => FullCommodityDisplayText);
                        }
                    }

                    Messenger.Default.Send(new HidePopupMessage());
                });

                Messenger.Default.Send(new ShowPopupMessage()
                {
                    ScreenDescriptor = new ScreenDescriptor(typeof(ChangeLoadCropView), Strings.ScreenDescriptor_ChangeCrop_Text.ToLower(), vm)
                });
            }
        }

        private void onChangeLoadQuantities()
        {
            if (loadId != null)
            {
                var vm = new Popups.ChangeLoadQuantitiesViewModel(clientEndpoint, dispatcher, CropYearId, CropId, IncludedLoadQuantityInfo.BaseQuantityInfo, changeVm => {
                    if (changeVm != null && changeVm.HasChanges)
                    {
                        var newQuantityInfo = changeVm.QuantityInfo.GetQuantityInfoV2ValueObject();

                        clientEndpoint.SendOne(new ChangeLoadQuantityInfo(
                            loadId,
                            clientEndpoint.GenerateNewMetadata(),
                            newQuantityInfo
                        ));

                        IncludedLoadQuantityInfo = new IncludedQuantityInfoViewModel(newQuantityInfo);
                        RaisePropertyChanged(() => IncludedLoadQuantityInfo);

                        // if this is a sale load, adjust the price based off the new quantity and issue a price change command
                        if (IsSaleLoad)
                        {

                            // shares are recalculated automatically in the constructor using the new quantity
                            var splitInfo = new LoadPriceSplitInfoViewModel(
                                clientEndpoint,
                                dispatcher,
                                newQuantityInfo.FinalQuantityValue,
                                newQuantityInfo.FinalQuantityUnit,
                                RentContractId,
                                IncludedLoadPriceSplitInfo.GetPrimaryPriceInfoValueObject(),
                                IncludedLoadPriceSplitInfo.GetSecondaryPriceInfoValueObject(),
                                getRentContractList(),
                                getProductionContractList()
                            );

                            clientEndpoint.SendOne(new ChangeLoadSharePricingInfo(
                                loadId,
                                clientEndpoint.GenerateNewMetadata(),
                                RentContractId,
                                splitInfo.GetGrowerPriceInfoValueObject(),
                                splitInfo.GetLandOwnerPriceInfoValueObject()
                            ));

                            IncludedLoadPriceSplitInfo = new IncludedSplitLoadInfoViewModel(
                                clientEndpoint,
                                dispatcher,
                                newQuantityInfo.FinalQuantityValue,
                                newQuantityInfo.FinalQuantityUnit,
                                RentContractId,
                                splitInfo.GetGrowerPriceInfoValueObject(),
                                splitInfo.GetLandOwnerPriceInfoValueObject()
                            );

                            RaisePropertyChanged(() => IncludedLoadPriceSplitInfo);
                        }
                    }

                    Messenger.Default.Send(new HidePopupMessage());
                });

                Messenger.Default.Send(new ShowPopupMessage()
                {
                    ScreenDescriptor = new ScreenDescriptor(typeof(ChangeLoadQuantitiesView), Strings.ScreenDescriptor_ChangeLoadQuantities_Text.ToLower(), vm)
                });
            }
        }

        private void onChangeLoadPrices()
        {
            if (loadId != null)
            {
                var vm = new Popups.ChangeLoadPricesViewModel(
                    clientEndpoint,
                    dispatcher,
                    CropYearId,
                    CropId,
                    IncludedLoadQuantityInfo.BaseQuantityInfo.FinalQuantityValue,
                    IncludedLoadQuantityInfo.BaseQuantityInfo.FinalQuantityUnit,
                    RentContractId,
                    IncludedLoadPriceSplitInfo.GetPrimaryPriceInfoValueObject(),
                    IncludedLoadPriceSplitInfo.GetSecondaryPriceInfoValueObject(),
                    changeVm => {
                        if (changeVm != null && changeVm.HasChanges)
                        {
                            var newSplitInfo = changeVm.LoadPriceSplitInfo;

                            var newRentContractId = newSplitInfo.GetRentContractId();
                            var newGrowerPriceInfo = newSplitInfo.GetGrowerPriceInfoValueObject();
                            var newLandOwnerPriceInfo = newSplitInfo.GetLandOwnerPriceInfoValueObject();

                            clientEndpoint.SendOne(new ChangeLoadSharePricingInfo(
                                loadId,
                                clientEndpoint.GenerateNewMetadata(),
                                newRentContractId,
                                newGrowerPriceInfo,
                                newLandOwnerPriceInfo
                            ));

                            IncludedLoadPriceSplitInfo = new IncludedSplitLoadInfoViewModel(
                                clientEndpoint,
                                dispatcher,
                                newSplitInfo.FinalQuantityValue,
                                newSplitInfo.FinalQuantityUnit,
                                newRentContractId,
                                newGrowerPriceInfo,
                                newLandOwnerPriceInfo
                            );

                            RaisePropertyChanged(() => IncludedLoadPriceSplitInfo);
                        }

                        Messenger.Default.Send(new HidePopupMessage());
                    }
                );

                Messenger.Default.Send(new ShowPopupMessage()
                {
                    ScreenDescriptor = new ScreenDescriptor(typeof(ChangeLoadPricesView), Strings.ScreenDescriptor_ChangeLoadPrices_Text.ToLower(), vm)
                });
            }
        }

        private void onChangeLoadTime()
        {
            if (loadId != null)
            {
                var vmDisplayMode = Shared.Popups.ChangeEntityDateTimeViewModel.EntityDateTimeDisplayMode.DateTime;

                DialogFactory.ShowDateTimeChangeDialog(StartDateTime, vmDisplayMode, newStartDateTime => {
                    clientEndpoint.SendOne(new ChangeLoadTime(
                        loadId,
                        clientEndpoint.GenerateNewMetadata(),
                        newStartDateTime.ToUniversalTime()
                    ));

                    StartDateTime = newStartDateTime;

                    RaisePropertyChanged(() => StartDateTime);
                    RaisePropertyChanged(() => StartDateDisplay);
                    RaisePropertyChanged(() => StartTimeDisplay);
                });
            }
        }

        private void onChangeLoadTruck()
        {
            if (loadId != null)
            {
                List<EquipmentListItemViewModel> activeEquipmentListItems =
                    GetActiveListItemsForCropYearSortByType(clientEndpoint, CropYearId);
                EquipmentListItemViewModel equipmentListItemToBeSelected =
                    activeEquipmentListItems.FirstOrDefault(equipmentListItemViewModel =>
                        equipmentListItemViewModel.EquipmentId.Equals(TruckId));

                DialogFactory.ShowComboBoxSelectionChangeDialog(equipmentListItemToBeSelected, activeEquipmentListItems, Strings.Truck_Text,
                    newEquipment => {
                        if (newEquipment != null)
                        {
                            clientEndpoint.SendOne(new ChangeLoadTruck(
                                loadId,
                                clientEndpoint.GenerateNewMetadata(),
                                newEquipment.EquipmentId
                            ));

                            TruckId = newEquipment.EquipmentId;
                            UpdateEquipmentName(TruckId, newEquipment.BasicInfoViewModel.Name);
                        }
                    }
                );
            }
        }

        private void onChangeLoadDriver()
        {
            if (loadId != null)
            {
                List<PersonListItemViewModel> driverList = GetListItemsForCropYear(clientEndpoint, CropYearId, true);
                PersonListItemViewModel selectedDriver = driverList.FirstOrDefault(x => x.Id == DriverId);

                DialogFactory.ShowComboBoxSelectionChangeDialog(selectedDriver, driverList, Strings.Driver_Text, newDriver => {
                    clientEndpoint.SendOne(new ChangeLoadDriver(
                        loadId,
                        clientEndpoint.GenerateNewMetadata(),
                        newDriver?.Id
                    ));
                    UpdateDriver(newDriver?.Id);
                });
            }
        }

        private void onUpdateLoadNotes()
        {
            if (loadId != null)
            {
                DialogFactory.ShowNotesChangeDialog(Notes, newNotes => {
                    clientEndpoint.SendOne(new UpdateLoadNotes(
                        loadId,
                        clientEndpoint.GenerateNewMetadata(),
                        newNotes
                    ));

                    Notes = newNotes;
                    RaisePropertyChanged(() => Notes);
                    RaisePropertyChanged(() => HasNotes);
                    RaisePropertyChanged(() => NotesHeader);
                });
            }
        }

        private void onChangeLoadProperties()
        {
            if (loadId != null)
            {
                var vm = new Popups.ChangeLoadPropertiesViewModel(clientEndpoint, dispatcher, CropId, LoadProperties, changeVm => {
                    if (changeVm != null && changeVm.HasChanges)
                    {
                        clientEndpoint.SendOne(new UpdateLoadProperties(
                            loadId,
                            clientEndpoint.GenerateNewMetadata(),
                            changeVm.AddedProperties.ToArray(),
                            changeVm.RemovedProperties.ToArray(),
                            changeVm.ChangedProperties.ToArray()
                        ));

                        LoadProperties = changeVm.ReportedLoadPropertySets.OrderBy(x => x.Name).Select(x => x.Clone()).ToList();

                        RaisePropertyChanged(() => LoadProperties);
                        RaisePropertyChanged(() => HasLoadProperties);
                        RaisePropertyChanged(() => LoadPropertiesHeader);
                    }

                    Messenger.Default.Send(new HidePopupMessage());
                });

                Messenger.Default.Send(new ShowPopupMessage()
                {
                    ScreenDescriptor = new ScreenDescriptor(typeof(ChangeLoadPropertiesView), Strings.ScreenDescriptor_ChangeLoadProperties_Text.ToLower(), vm)
                });
            }
        }

        private void onUpdateLoadSources()
        {
            if (loadId != null)
            {
                var vmBase = new DocumentListPageViewModel(clientEndpoint, dispatcher, CropYearId.Id, SourceDocumentList, false);
                var vm = new Overlays.DocumentListOverlayViewModel(vmBase, shouldSave => {
                    if (shouldSave)
                    {
                        foreach (var added in vmBase.AddedDocuments)
                        {
                            SourceDocumentList.Add(added);
                            clientEndpoint.SendOne(new AddDocumentToLoad(
                                loadId,
                                clientEndpoint.GenerateNewMetadata(),
                                added
                            ));
                        }

                        foreach (var removed in vmBase.RemovedDocuments)
                        {
                            SourceDocumentList.RemoveAll(x => x.Identity == removed.Identity);
                            clientEndpoint.SendOne(new RemoveDocumentFromLoad(
                                loadId,
                                clientEndpoint.GenerateNewMetadata(),
                                removed
                            ));
                        }

                        RaisePropertyChanged(() => SourceDocumentList);
                        RaisePropertyChanged(() => SourceDocumentCount);
                        RaisePropertyChanged(() => SourcesDisplayText);
                    }

                    Messenger.Default.Send(new HideOverlayMessage());
                });

                Messenger.Default.Send(new ShowOverlayMessage()
                {
                    ScreenDescriptor = new ScreenDescriptor(typeof(Views.Overlays.DocumentListOverlayView), Strings.ScreenDescriptor_EditDocuments_Text, vm)
                });
            }
        }
        #endregion

        private void buildDetailsFromReadModel(LoadDetailsView detailsView)
        {
            CropYearId = new CropYearId(loadId.DataSourceId, detailsView.CropYear);

            OperationType = detailsView.OperationType;

            LoadNumber = detailsView.LoadNumber;
            StartDateTime = detailsView.StartDateTime;

            CropId = detailsView.Commodity.CropId;
            CropName = detailsView.Commodity.CropName;
            CommodityDescription = detailsView.Commodity.CommodityDescription;

            TruckId = detailsView.TruckId;
            InitializeTruckName();

            UpdateDriver(detailsView.DriverId);

            Notes = detailsView.Notes;

            var locationList = YieldLocationListItemViewModel.GetFlattenedListItemsForCropYear(clientEndpoint, CropYearId, false);

            var destinationLocationListItems = from liLoc in locationList
                                               from destLoc in detailsView.DestinationLocations
                                               where liLoc.Id == destLoc.Id
                                               select liLoc;

            DestinationSubLocationInfo = new LoadSubLocationInfoViewModel(clientEndpoint, destinationLocationListItems.ToList());

            if (detailsView.IsLoadFromField)
            {
                refreshCropZones(detailsView.CropZones);
            }
            else if (detailsView.IsLoadFromStorage)
            {
                var sourceLocationListItems = from liLoc in locationList
                                              from sourceLoc in detailsView.SourceLocations
                                              where liLoc.Id == sourceLoc.Id
                                              select liLoc;

                SourceSubLocationInfo = new LoadSubLocationInfoViewModel(clientEndpoint, sourceLocationListItems.ToList());
            }
            else
            {
                if (Debugger.IsAttached)
                {
                    Debugger.Break();
                }
                else
                {
                    log.Warn($"Invalid yield operation type: {detailsView.OperationId}");
                }
            }

            IncludedLoadQuantityInfo = new IncludedQuantityInfoViewModel(detailsView.QuantityInformation);

            if (detailsView.IsLoadSold)
            {
                IncludedLoadPriceSplitInfo = new IncludedSplitLoadInfoViewModel(
                    clientEndpoint,
                    dispatcher,
                    detailsView.QuantityInformation.FinalQuantityValue,
                    detailsView.QuantityInformation.FinalQuantityUnit,
                    detailsView.RentContractId,
                    detailsView.GrowerSharePriceInformation,
                    detailsView.LandOwnerSharePriceInformation
                );

                RentContractId = detailsView.RentContractId;
                RentContractName = detailsView.RentContractName;
            }

            LoadProperties = getLoadPropertiesList(detailsView.ReportedPropertySets);

            SourceDocumentList = detailsView.Sources;
        }

	    private void InitializeTruckName()
	    {
	        List<EquipmentListItemViewModel> equipmentList = GetListItemsForCropYear(clientEndpoint, CropYearId);
	        if (TruckId != null)
	        {
	            EquipmentListItemViewModel equipment = equipmentList
	                .Find(equipmentListItem => equipmentListItem.EquipmentId.Equals(TruckId));
	            if (equipment != null)
	            {
	                UpdateEquipmentName(equipment.EquipmentId, equipment.BasicInfoViewModel.Name);
                }
	            
	        }
        }

		private List<LoadPropertySetViewModel> getLoadPropertiesList(List<ReportedLoadPropertySet> reportedProperties) {
			var retList = new List<LoadPropertySetViewModel>();

            if (reportedProperties != null && reportedProperties.Any())
            {
                var propDefs = clientEndpoint.GetMasterlistService().GetLoadPropertyDefinitions();

                foreach (var rlp in reportedProperties)
                {
                    var set = (from lps in propDefs.LoadPropertySets
                               where lps.Id == rlp.LoadPropertySetId
                               select new LoadPropertySetViewModel(lps)).FirstOrDefault();

                    if (set != null)
                    {
                        var propList = new List<LoadPropertyViewModel>();
                        foreach (var rp in rlp.ReportedPropertyValues)
                        {
                            var prop = (from lp in propDefs.LoadProperties
                                        where lp.Id == rp.LoadPropertyId
                                        select new LoadPropertyViewModel(lp, rp.ReportedValue, rp.ReportedUnit)).FirstOrDefault();

                            if (prop != null)
                            {
                                propList.Add(prop);
                            }
                        }

                        if (propList.Any())
                        {
                            set.LoadProperties = propList;
                            retList.Add(set);
                        }
                    }
                }
            }

            return retList.OrderBy(x => x.Name).ToList();
        }

        private void refreshCropZones(IEnumerable<IncludedCropZone> cropZones)
        {
            CropZones.Clear();

            var flattenedTreeView = clientEndpoint.GetView<FlattenedTreeHierarchyView>(CropYearId).GetValue(new FlattenedTreeHierarchyView());

            foreach (var cz in cropZones)
            {
                var mapItem = clientEndpoint.GetView<Domain.ReadModels.Map.ItemMap>(cz.Id);
                string mapData = null;
                System.Windows.Shapes.Path shapePreview = null;
                var czDetails = clientEndpoint.GetView<CropZoneDetailsView>(cz.Id).Value;
                var name = cz.Name;

                var fti = flattenedTreeView.Items.FirstOrDefault(x => x.CropZoneId == cz.Id);
                if (fti != null) { name = fti.GetFullName(); }

                double area = czDetails.ReportedArea != null ? (double)czDetails.ReportedArea : czDetails.BoundaryArea != null ? (double)czDetails.BoundaryArea : 0;
                double coveragePercent = area != 0 ? (cz.Area.Value / area) * 100 : 0;

                if (mapItem.HasValue && mapItem.Value.MostRecentMapItem != null)
                {
                    mapData = mapItem.Value.MostRecentMapItem.MapData;

                    dispatcher.BeginInvoke(new Action(() => {
                        shapePreview = Client.Spatial.WpfTransforms.CreatePathFromMapData(new[] { mapData }, 40);
                        CropZones.Add(new CropZoneDetails(
                            cz.Id,
                            name,
                            cz.Area,
                            $"{coveragePercent:N0}%",
                            shapePreview
                        ));
                    }));
                }
                else
                {
                    CropZones.Add(new CropZoneDetails(
                        cz.Id,
                        name,
                        cz.Area,
                        $"{coveragePercent:N0}%"
                    ));
                }
            }
        }

        private List<ProductionContractListItem> getProductionContractList()
        {
            var productionContractList = clientEndpoint.GetView<ProductionContractListView>(CropYearId)
                .GetValue(new ProductionContractListView())
                .ProductionContracts
                .Where(x => x.Crop == CropId)
                .OrderBy(x => x.Name)
                .ToList();

            if (productionContractList.Any())
            {
                productionContractList.Insert(0, new ProductionContractListItem() { Name = string.Empty });
            }

            return productionContractList;
        }

        private List<RentContractListItem> getRentContractList()
        {
            var rentContractList = clientEndpoint.GetView<RentContractListView>(CropYearId)
                .GetValue(new RentContractListView())
                .RentContracts
                .Where(x => x.Crop == CropId)
                .OrderBy(x => x.Name)
                .ToList();

            if (rentContractList.Any())
            {
                rentContractList.Insert(0, new RentContractListItem() { Name = Strings.GrowerShares_Text });
            }

            return rentContractList;
        }
    }
}