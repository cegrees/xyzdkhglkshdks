﻿using GalaSoft.MvvmLight;
using Landdb.Domain.ReadModels.Yield;
using Landdb.ValueObjects;
using System;
using System.Linq;
using Landdb.Resources;

namespace Landdb.ViewModel.Yield {
	public class LoadListItemViewModel : ViewModelBase {

		private readonly LoadListItem li;

		public LoadListItemViewModel(LoadListItem li) {
			this.li = li;
		}

		public LoadId Id => li.Id;
		public YieldOperationId OperationId => li.OperationId;
		public YieldOperationTypes OperationType => li.OperationType;
		public CropId Crop => li.Commodity.CropId;
		public string CommodityDescription => li.Commodity.CommodityDescription;
		public string LoadNumber => li.LoadNumber;
		public decimal FinalQuantityValue => li.FinalQuantityValue;
		public CompositeUnit FinalQuantityUnit => li.FinalQuantityUnit;
		public DateTime StartDateTime => li.StartDateTime;
		public YieldLocationTypes DestinationLocationType => li.DestinationLocations.First().YieldLocationType;

		public string ShortDescription {
			get {
				string conveyanceDescription = null;

				switch (li.OperationType) {
					case YieldOperationTypes.FieldToStorage: {
							var czCount = li.CropZoneIds.Count;
							var fieldOrFields = czCount == 1 ? Strings.Field_Text : Strings.Fields_Text;
							conveyanceDescription = String.Format(Strings.Format_HarvestedFromAndStored_Text, czCount, fieldOrFields);
							break;
						}
					case YieldOperationTypes.FieldToSale: {
							var czCount = li.CropZoneIds.Count;
							var fieldOrFields = czCount == 1 ? Strings.Field_Text : Strings.Fields_Text;
							conveyanceDescription = String.Format(Strings.Format_HarvestedFromAndSold_Text, czCount, fieldOrFields);
							break;
						}
					case YieldOperationTypes.StorageToStorage: {
							conveyanceDescription = Strings.MovedToADifferentStorageLocation_Text;
							break;
						}
					case YieldOperationTypes.StorageToSale: {
							conveyanceDescription = Strings.SoldFromStorage_Text;
							break;
						}
					default:
						break;
				}

				var shortCropName = li.Commodity.CropName.Split(':').First().Trim();
				return $"{shortCropName} {conveyanceDescription}";
			}
		}

		public string CharmName {
			get { return li.CharmName; }
			set {
				li.CharmName = value;
				RaisePropertyChanged(() => CharmName);
			}
		}

		public override string ToString() => $"{OperationType}: {LoadNumber}";
	}
}