﻿using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.CommoditySetting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Landdb.ViewModel.Yield {
	public class CropSettingsPageViewModel : AbstractListPage<CropSettingListItemViewModel, CropSettingDetailsViewModel> {

		public CropSettingsPageViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher)
			: base(clientEndpoint, dispatcher) {
		}

		protected override IEnumerable<CropSettingListItemViewModel> GetListItemModels() {
			var cropYearId = new CropYearId(currentDataSourceId.Id, currentCropYear);
			var cropSettingsMaybe = clientEndpoint.GetView<CommoditySettingsListView>(cropYearId);

			if (cropSettingsMaybe.HasValue) {
				return from li in cropSettingsMaybe.Value.Crops.Values
					   orderby li.CropName
					   select new CropSettingListItemViewModel(li);
			} else {
				return null;
			}
		}

		protected override Task<ScreenDescriptor> CreateScreenDescriptorForNewItemCreation() {
			throw new NotImplementedException("Manually created crop settings are not implemented");
		}

		protected override bool PerformItemRemoval(CropSettingListItemViewModel selectedItem) {
			throw new NotSupportedException("Crop setting removal not supported");
		}

		protected override IDomainCommand CreateSetCharmCommand(CropSettingListItemViewModel selectedItem, string charmName) {
			throw new NotSupportedException("Charms not supported on crop settings");
		}

		protected override CropSettingDetailsViewModel CreateDetailsModel(CropSettingListItemViewModel selectedItem) {
			var currentCropYearId = new CropYearId(currentDataSourceId.Id, currentCropYear);
			var key = new Tuple<CropYearId, CropId>(currentCropYearId, selectedItem.CropId);
			var detailsMaybe = clientEndpoint.GetView<CommoditySettingsDetailsView>(key);

			if (detailsMaybe.HasValue) {
				return new CropSettingDetailsViewModel(clientEndpoint, dispatcher, currentCropYearId, detailsMaybe.Value);
			} else {
				return null;
			}
		}
	}
}
