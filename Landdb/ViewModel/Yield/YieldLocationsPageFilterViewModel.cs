﻿using GalaSoft.MvvmLight;
using Landdb.Client.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Landdb.Resources;

namespace Landdb.ViewModel.Yield {
	public class YieldLocationsPageFilterViewModel : ViewModelBase, IPageFilterViewModel {

		private readonly IClientEndpoint clientEndpoint;
		private readonly CropYearId currentCropYearId;

		public YieldLocationsPageFilterViewModel(IClientEndpoint clientEndpoint, CropYearId currentCropYearId) {
			this.clientEndpoint = clientEndpoint;
			this.currentCropYearId = currentCropYearId;

			RefreshLists();
		}

		private string _filterText;
		public string FilterText {
			get { return _filterText; }
			set {
				_filterText = value;
				RaisePropertyChanged(() => FilterText);
			}
		}

		private YieldLocationTypes _selectedLocationType;
		public YieldLocationTypes SelectedLocationType {
			get { return _selectedLocationType; }
			set {
				_selectedLocationType = value;
				RaisePropertyChanged(() => SelectedLocationType);
			}
		}

		public static Dictionary<YieldLocationTypes, string> YieldLocationTypeList {
			get {
				return new Dictionary<YieldLocationTypes, string>() {
					{ YieldLocationTypes.Unknown, Strings.YieldLocationTypes_AnyType_Text },
					{ YieldLocationTypes.Sale, Strings.YieldLocationTypes_SaleLocation_Text },
					{ YieldLocationTypes.Storage, Strings.YieldLocationTypes_StorageLocation_Text }
				};
			}
		}

		public void BeforeFilter() {
			// do nothing
		}

		public void ClearFilter() {
			FilterText = string.Empty;
			SelectedLocationType = YieldLocationTypes.Unknown;
		}

		public bool FilterItem(object item) {
			var li = item as YieldLocationListItemViewModel;

			// note: false return means the item is not displayed (filtered out)
			if (li == null) {
				return false;
			} else if (SelectedLocationType != YieldLocationTypes.Unknown && li.YieldLocationType != SelectedLocationType) {
				return false;
			} else if (!string.IsNullOrWhiteSpace(FilterText) && !li.Name.Equals(FilterText, StringComparison.OrdinalIgnoreCase)) {
				return false;
			}

			return true;
		}

		public void RefreshLists() {
			// do nothing
		}
	}
}
