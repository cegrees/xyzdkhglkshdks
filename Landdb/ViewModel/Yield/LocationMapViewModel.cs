﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Landdb.Domain.ReadModels.Tree;
using GalaSoft.MvvmLight;
using System.Windows.Shapes;
using Landdb.Client.Spatial;
using System.Windows.Threading;
using System.Windows;
using Microsoft.Maps.MapControl.WPF;
using Landdb.Client.Infrastructure;
//using Landdb.ViewModel.Farms.PropertyParts;
//using Landdb.ViewModel.Fields.FieldDetails.PropertyParts;
using Landdb.Infrastructure;
using System.Windows.Input;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure.Messages;
using GalaSoft.MvvmLight.Command;
using Landdb.Models;
using AgC.UnitConversion;
using Landdb.Views.Yield;
using Landdb.Domain.ReadModels.YieldLocation;
using Landdb.Domain.ReadModels.MapAnnotation;

namespace Landdb.ViewModel.Yield {
    public class LocationMapViewModel : ViewModelBase {
        string name;
        Path locationPreview;
        MapLayer layer; 
        bool shouldShowBingMaps;
        bool shouldShowWpfMock;
        bool hasNoMapData;
        Dispatcher dispatcher;
        IClientEndpoint clientEndpoint;
        DataSourceId currentDataSourceId;
        int currentCropYear;
        YieldLocationDetailsView detailsView;

        public LocationMapViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, DataSourceId currentDataSourceId, int currentCropYear, YieldLocationDetailsView detailsView) {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
            this.currentDataSourceId = currentDataSourceId;
            this.currentCropYear = currentCropYear;
            this.detailsView = detailsView;
            
            UpdateMapData(detailsView);
        }

        private void UpdateMapData(YieldLocationDetailsView detailsView) {
            if(detailsView == null) { return; }
            
            if(detailsView.MapAnnotationID == null || string.IsNullOrEmpty(detailsView.MapAnnotationID.Id.ToString())) { HasNoMapData = true; return; }
            IEnumerable<string> mapData;

            //TO DO :: ENABLE REMOVAL OF ANNOTATION LAYERS FROM DETAILS READMODEL WHEN DOMAIN GETS UPDATED
            var annotationListMaybe = clientEndpoint.GetView<MapAnnotationListView>(new DataSourceId(ApplicationEnvironment.CurrentDataSourceId));
            var annotationList = annotationListMaybe.HasValue ? annotationListMaybe.Value.Annotations : new List<MapAnnotationListItem>();
            if (detailsView.MapAnnotationID == null) { HasNoMapData = true; return; }

            if (annotationList.Any(x => x.Id == detailsView.MapAnnotationID))
            {
                
                var shapeItemMaybe = clientEndpoint.GetView<MapAnnotationDetailView>(detailsView.MapAnnotationID);
                if (shapeItemMaybe.HasValue && shapeItemMaybe.Value.CropYear == null)
                {
                    HasNoMapData = true;
                    return;
                }
                shapeItemMaybe.IfValue(annotation =>
                {
                    List<string> wktlist = new List<string>();
                    wktlist.Add(annotation.WktData);
                    mapData = from mi in wktlist select mi;
                    if (!mapData.Any())
                    {
                        HasNoMapData = true;
                    }
                    else
                    {
                        ShouldShowBingMaps = NetworkStatus.IsInternetAvailableFast();
                        ShouldShowWpfMock = !ShouldShowBingMaps;

                        dispatcher.BeginInvoke(new Action(() =>
                        {
                            if (shouldShowBingMaps)
                            {
                                layer = BingMapsUtility.GetLayerForMapData(mapData);
                                RaisePropertyChanged("BingMapsLayer");
                            }
                            else
                            {
                                locationPreview = WpfTransforms.CreatePathFromMapData(mapData);
                                RaisePropertyChanged("LocationPreview");
                            }
                        }));
                    }
                });
            }
            else
            {
                HasNoMapData = true;
            }
        }

        public ICommand UpdateLocation { get; set; }

        public string Name {
            get { return name; }
            set {
                name = value;
                RaisePropertyChanged("Name");
            }
        }

        public Path LocationPreview {
            get { return locationPreview; }
        }

        public MapLayer BingMapsLayer
        {
            get { return layer; }
        }

        public string MapCulture
        {
            get { return Infrastructure.ApplicationEnvironment.BingWpfMapCulture; }
        }

        internal YieldLocationDetailsView YieldLocationDetails {
            get { return detailsView; }
            set {
                detailsView = value;
                UpdateMapData(detailsView);
            }
        }

        public bool ShouldShowBingMaps {
            get { return shouldShowBingMaps; }
            private set {
                shouldShowBingMaps = value;
                RaisePropertyChanged("ShouldShowBingMaps");
            }
        }

        public bool ShouldShowWpfMock {
            get { return shouldShowWpfMock; }
            set {
                shouldShowWpfMock = value;
                RaisePropertyChanged("ShouldShowWpfMock");
            }
        }

        public bool HasNoMapData {
            get { return hasNoMapData; }
            set {
                hasNoMapData = value;
                RaisePropertyChanged("HasNoMapData");

                ShouldShowBingMaps = false;
                ShouldShowWpfMock = false;
            }
        }
    }
}
