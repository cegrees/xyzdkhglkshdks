﻿using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Yield;
using Landdb.Domain.ReadModels.YieldLocation;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Secondary.Reports.Yield;
using Landdb.ViewModel.Secondary.Reports.Yield.Generators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Yield {
	public class CommoditiesPageViewModel : AbstractListPage<CommodityListItemViewModel, CommodityDetailsViewModel> {

		public CommoditiesPageViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher)
			: base(clientEndpoint, dispatcher) {

			Messenger.Default.Register<YieldOperationAddedMessage>(this, msg => { RefreshItemsList(); });

			PrintReport = new RelayCommand(onPrintCommoditySummaryReport);
		}

		public RelayCommand PrintReport { get; }

		protected override IEnumerable<CommodityListItemViewModel> GetListItemModels() {
			var cropYearId = new CropYearId(currentDataSourceId.Id, currentCropYear);
			var commoditiesMaybe = clientEndpoint.GetView<CommodityListView>(cropYearId);

			if (commoditiesMaybe.HasValue) {
				var yieldLocationList = clientEndpoint.GetView<YieldLocationListView>(new DataSourceId(cropYearId.DataSourceId)).GetValue(() => new YieldLocationListView()).YieldLocations;

				var excludedYieldLocationIds = from loc in yieldLocationList
											   where loc.IsActiveInCropYear(cropYearId.Id) && loc.IsQuantityExcludedFromInventory
											   select loc.Id.Id;

				var listItems = from li in commoditiesMaybe.Value.Commodities
								where li.HasLoads
								orderby li.CropName, li.CommodityDescription
								select new CommodityListItemViewModel(li, excludedYieldLocationIds);

				return listItems.Where(x => x.LoadCount != 0);
			} else {
				return null;
			}
		}

		protected override async Task<ScreenDescriptor> CreateScreenDescriptorForNewItemCreation() {
			throw new NotSupportedException();
		}

		protected override bool PerformItemRemoval(CommodityListItemViewModel selectedItem) {
			throw new NotSupportedException();
		}

		protected override IDomainCommand CreateSetCharmCommand(CommodityListItemViewModel selectedItem, string charmName) {
			throw new NotSupportedException();
		}

		protected override CommodityDetailsViewModel CreateDetailsModel(CommodityListItemViewModel selectedItem) {
			var currentCropYearId = new CropYearId(currentDataSourceId.Id, currentCropYear);
			var detailsViewKey = new CommodityDetailsViewKey(currentCropYearId, selectedItem.CropId.Id, selectedItem.CommodityDescription);
			var detailsMaybe = clientEndpoint.GetView<CommodityDetailsView>(detailsViewKey);

			if (detailsMaybe.HasValue) {
				return new CommodityDetailsViewModel(clientEndpoint, dispatcher, currentCropYearId, detailsMaybe.Value);
			} else {
				return null;
			}
		}

		private void onPrintCommoditySummaryReport() {
			var reportGenerator = new CommoditySummaryReportGenerator(clientEndpoint, dispatcher, currentCropYear);
			var vm = new SingleYieldReportViewModel(clientEndpoint, dispatcher, reportGenerator);
			Messenger.Default.Send(new ShowOverlayMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Secondary.Reports.Work_Order.SingleWorkOrderView), Strings.ScreenDescriptor_ViewReport_Text.ToLower(), vm)
			});
		}
	}
}