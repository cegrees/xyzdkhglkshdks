﻿using Landdb.Domain.ReadModels.CommoditySetting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Yield {
	public class CropSettingListItemViewModel {

		public CropSettingListItemViewModel(CommoditySettingsListItem li) {
			CropId = li.CropId;
			CropName = li.CropName;
		}

		public CropId CropId { get; }
		public string CropName { get; }
	}
}