﻿using AgC.UnitConversion;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Landdb.ViewModel.Yield {
	public class LoadPropertySetViewModel {

		public LoadPropertySetViewModel(LoadPropertySet set) {
			Id = set.Id;
			Name = set.Name;

			if (set.RecommendedCropIds != null) {
				RecommendedCrops = set.RecommendedCropIds.Select(x => new CropId(x));
			} else {
				RecommendedCrops = new List<CropId>();
			}

			LoadProperties = new List<LoadPropertyViewModel>();
		}

		// private constructor for cloning
		private LoadPropertySetViewModel(LoadPropertySetViewModel source) {
			Id = source.Id;
			Name = source.Name;
			RecommendedCrops = source.RecommendedCrops;
			IsChecked = source.IsChecked;
			LoadProperties = source.LoadProperties.Select(x => x.Clone()).ToList();
		}

		public Guid Id { get; set; }
		public string Name { get; set; }
		public IEnumerable<CropId> RecommendedCrops { get; set; }
		public bool IsChecked { get; set; }

		public List<LoadPropertyViewModel> LoadProperties { get; set; }

		public LoadPropertySetViewModel Clone() => new LoadPropertySetViewModel(this);
		public override string ToString() => Name;
	}

	public class LoadPropertyViewModel {

		private readonly string originalValue;
		private readonly CompositeUnit originalUnit;

		public LoadPropertyViewModel(LoadProperty prop) {
			Id = prop.Id;
			Name = prop.Name;
			UnitSelectionType = prop.UnitSelectionType;
			InputType = prop.InputType;
			DataType = prop.DataType;

			if (UnitSelectionType == LoadPropertyUnitSelectionTypes.Weight) {
				SelectedUnit = new CompositeUnit(AgC.UnitConversion.MassAndWeight.Pound.Self);
			}

			ListItems = new List<LoadPropertyListItemViewModel>();
		}

		public LoadPropertyViewModel(LoadProperty prop, string reportedValue, CompositeUnit reportedUnit)
			: this(prop) {
			originalValue = reportedValue;
			originalUnit = reportedUnit;

			Value = reportedValue;
			SelectedUnit = reportedUnit;
		}

		// private constructor for cloning
		private LoadPropertyViewModel(LoadPropertyViewModel source) {
			originalValue = source.Value;
			originalUnit = source.originalUnit;

			Id = source.Id;
			Name = source.Name;
			UnitSelectionType = source.UnitSelectionType;
			InputType = source.InputType;
			DataType = source.DataType;
			DisplayOrder = source.DisplayOrder;
			ListItems = source.ListItems.Select(x => x.Clone()).ToList();
			Value = source.Value;
			SelectedUnit = source.SelectedUnit;
		}

		public Guid Id { get; }
		public string Name { get; }
		public LoadPropertyUnitSelectionTypes UnitSelectionType { get; }
		public LoadPropertyInputTypes InputType { get; }
		public LoadPropertyDataTypes DataType { get; }
		public int DisplayOrder { get; }

		public List<LoadPropertyListItemViewModel> ListItems { get; set; }

		public string Value { get; set; }
		public CompositeUnit SelectedUnit { get; set; }

		public string FormattedValue {
			get {
				switch (DataType) {
					case LoadPropertyDataTypes.Text:
						return Value;
					case LoadPropertyDataTypes.Number: {
							decimal retDec = 0;
							decimal.TryParse(Value, out retDec);
							return retDec.ToString("N2");
						}
					case LoadPropertyDataTypes.Currency: {
							decimal retCur = 0;
							decimal.TryParse(Value, out retCur);
							return retCur.ToString("C2");
						}
					case LoadPropertyDataTypes.Percentage: {
							decimal retPer = 0;
							decimal.TryParse(Value, out retPer);
							return retPer.ToString("P2");
						}
					case LoadPropertyDataTypes.Boolean: {
							bool retBool = false;
							bool.TryParse(Value, out retBool);
							return retBool.ToString();
						}
					case LoadPropertyDataTypes.Date: {
							DateTime retDT = DateTime.MinValue;
							DateTime.TryParse(Value, out retDT);
							return retDT.ToString();
						}
					case LoadPropertyDataTypes.Time: {
							DateTime retDT = DateTime.MinValue;
							DateTime.TryParse(Value, out retDT);
							return retDT.ToString();
						}
					default:
						return Value;
				}
			}
			set {
				if (DataType == LoadPropertyDataTypes.Percentage) {
					decimal newValue = 0;
					decimal.TryParse(value, out newValue);
					Value = Convert.ToString(newValue / 100);
				} else {
					Value = value;
				}
			}
		}

		public string DisplayText {
			get {
				if (UnitSelectionType != LoadPropertyUnitSelectionTypes.None && SelectedUnit != null) {
					return $"{FormattedValue} {SelectedUnit.AbbreviatedDisplay}";
				} else {
					return FormattedValue;
				}
			}
		}

		public bool IsUnitSelectionVisible => UnitSelectionType != LoadPropertyUnitSelectionTypes.None;
		public bool IsChanged => Value != originalValue || SelectedUnit != originalUnit;
		public LoadPropertyViewModel Clone() => new LoadPropertyViewModel(this);
		public override string ToString() => $"{Name}: {DisplayText}";
	}

	public class LoadPropertyListItemViewModel {

		public LoadPropertyListItemViewModel(LoadPropertyListItem li) {
			Id = li.Id;
			Value = li.Value;
		}

		// private constructor for cloning
		private LoadPropertyListItemViewModel(LoadPropertyListItemViewModel source) {
			Id = source.Id;
			Value = source.Value;
		}

		public Guid Id { get; set; }
		public string Value { get; set; }

		public LoadPropertyListItemViewModel Clone() => new LoadPropertyListItemViewModel(this);
		public override string ToString() => Value;
	}

	public class LoadPropertySetSelectionViewModel {

		readonly Action<Guid, bool> checkChanged;

		public LoadPropertySetSelectionViewModel(LoadPropertySet set, Action<Guid, bool> onCheckChanged = null) {
			Id = set.Id;
			Name = set.Name;
			Description = set.Description;
			RecommendedCropIds = set.RecommendedCropIds ?? new List<Guid>();
			IsChecked = false;

			checkChanged += onCheckChanged;
		}

		public Guid Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public IEnumerable<Guid> RecommendedCropIds { get; set; }

		private bool _isChecked;
		public bool IsChecked {
			get { return _isChecked; }
			set {
				_isChecked = value;
				checkChanged?.Invoke(Id, value);
			}
		}

		public bool HasDescription => !string.IsNullOrWhiteSpace(Description);
	}
}