﻿using Landdb.Resources;

namespace Landdb.ViewModel.Yield {
	public class IncludedQuantityInfoViewModel {
		public IncludedQuantityInfoViewModel(LoadQuantityInformation_v2 quantityInfo) {
			BaseQuantityInfo = quantityInfo;

			FinalQuantity = $"{quantityInfo.FinalQuantityValue:N2} {quantityInfo.FinalQuantityUnit.FullDisplay}";
			TruckGrossWeight = $"{quantityInfo.TruckGrossWeight:N2} {quantityInfo.TruckWeightUnit.AbbreviatedDisplay}";
			TruckTareWeight = $"{quantityInfo.TruckTareWeight:N2} {quantityInfo.TruckWeightUnit.AbbreviatedDisplay}";
			TruckNetWeight = $"{quantityInfo.TruckNetWeight:N2} {quantityInfo.TruckWeightUnit.AbbreviatedDisplay}";
			ShowTruckWeights = quantityInfo.TruckGrossWeight > 0 && quantityInfo.TruckTareWeight > 0;

			decimal deliveredQuantity = 0;
			if (CompositeUnitConverter.CanConvert(quantityInfo.TruckWeightUnit, quantityInfo.FinalQuantityUnit)) {
				deliveredQuantity = CompositeUnitConverter.ConvertValue(quantityInfo.TruckNetWeight, quantityInfo.TruckWeightUnit, quantityInfo.FinalQuantityUnit);
			} else {
				deliveredQuantity = quantityInfo.FinalQuantityValue - quantityInfo.StandardMoistureShrinkQuantity - quantityInfo.HandlingShrinkQuantity - quantityInfo.AdjustmentShrinkQuantity;
			}

			DeliveredQuantity = $"{deliveredQuantity:N2} {quantityInfo.FinalQuantityUnit.AbbreviatedDisplay}";

			IsMoistureDetailVisible = quantityInfo.InitialMoisturePercentage > 0;
			InitialMoisturePercentage = $"{quantityInfo.InitialMoisturePercentage:P2}";
			TargetMoisturePercentage = $"{quantityInfo.StandardTargetMoisturePercentage:P2}";
			IsTargetMoistureVisible = IsMoistureDetailVisible && quantityInfo.StandardTargetMoisturePercentage >= quantityInfo.HandlingShrinkTargetMoisturePercentage;
			HandlingTargetPercentage = $"{quantityInfo.HandlingShrinkTargetMoisturePercentage:P2}";
			IsHandlingTargetVisible = IsMoistureDetailVisible && quantityInfo.HandlingShrinkTargetMoisturePercentage > 0 && quantityInfo.EstimatedShrinkRatePercentage > 0;

			MoistureShrinkQuantity = $"{quantityInfo.StandardMoistureShrinkQuantity:N2} {quantityInfo.FinalQuantityUnit.AbbreviatedDisplay}";
			MoistureShrinkPercentage = $"{quantityInfo.StandardMoistureShrinkPercentage:P2} {Strings.Moisture_Text.ToLower()}";
			HasMoistureShrink = quantityInfo.StandardMoistureShrinkPercentage != 0;

			HandlingShrinkQuantity = $"{quantityInfo.HandlingShrinkQuantity:N2} {quantityInfo.FinalQuantityUnit.AbbreviatedDisplay}";
			HandlingShrinkPercentage = $"{quantityInfo.HandlingShrinkPercentage:P2} {Strings.Handling_Text.ToLower()}";
			HasHandlingShrink = quantityInfo.HandlingShrinkPercentage != 0;

			ShrinkAdjustmentQuantity = $"{quantityInfo.AdjustmentShrinkQuantity:N2} {quantityInfo.FinalQuantityUnit.AbbreviatedDisplay}";
			ShrinkAdjustmentPercentage = $"{quantityInfo.AdjustmentShrinkPercentage:P2} {Strings.Adjustment_Text.ToLower()}";
			HasAdjustmentShrink = quantityInfo.AdjustmentShrinkPercentage != 0;
		}

		public LoadQuantityInformation_v2 BaseQuantityInfo { get; }

		public string FinalQuantity { get; }

		public string TruckGrossWeight { get; }
		public string TruckTareWeight { get; }
		public string TruckNetWeight { get; }
		public bool ShowTruckWeights { get; }

		public string DeliveredQuantity { get; }

		public bool IsMoistureDetailVisible { get; }
		public string InitialMoisturePercentage { get; }
		public string TargetMoisturePercentage { get; }
		public bool IsTargetMoistureVisible { get; }
		public string HandlingTargetPercentage { get; }
		public bool IsHandlingTargetVisible { get; }

		public string MoistureShrinkQuantity { get; }
		public string MoistureShrinkPercentage { get; }

		public string HandlingShrinkQuantity { get; }
		public string HandlingShrinkPercentage { get; }

		public string ShrinkAdjustmentQuantity { get; }
		public string ShrinkAdjustmentPercentage { get; }

		public bool HasMoistureShrink { get; }
		public bool HasHandlingShrink { get; }
		public bool HasAdjustmentShrink { get; }
	}
}