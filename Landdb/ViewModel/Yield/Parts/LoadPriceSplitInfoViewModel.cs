﻿using GalaSoft.MvvmLight;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Threading;

namespace Landdb.ViewModel.Yield {
	public class LoadPriceSplitInfoViewModel : ViewModelBase {

		private readonly IClientEndpoint clientEndpoint;
		private readonly Dispatcher dispatcher;

		private LoadPriceInformation growerPriceInfo;
		private LoadPriceInformation landOwnerPriceInfo;

		private readonly IEnumerable<ProductionContractListItem> productionContractList;

		private decimal growerSharePercentage = 1;

		// constructor for new loads
		public LoadPriceSplitInfoViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, IEnumerable<ProductionContractListItem> productionContractList) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			this.productionContractList = productionContractList;

			growerPriceInfo = new LoadPriceInformation();
			landOwnerPriceInfo = new LoadPriceInformation();
		}

		// constructor for load edits
		public LoadPriceSplitInfoViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, decimal finalQuantityValue, CompositeUnit finalQuantityUnit, ContractId rentContractId, LoadPriceInformation growerPriceInfo, LoadPriceInformation landOwnerPriceInfo, IEnumerable<RentContractListItem> rentContractList, IEnumerable<ProductionContractListItem> productionContractList) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			this.growerPriceInfo = growerPriceInfo ?? new LoadPriceInformation();
			this.landOwnerPriceInfo = landOwnerPriceInfo ?? new LoadPriceInformation();

			if (growerPriceInfo != null && landOwnerPriceInfo != null) {
				_isSplitLoad = true;
			}

			_finalQuantityValue = finalQuantityValue;
			_finalQuantityUnit = finalQuantityUnit;

			this.productionContractList = productionContractList;

			// keep last in constructor. forces a recalc with appropriate grower share percentage.
			if (rentContractId != null) {
				SelectedRentContract = rentContractList.FirstOrDefault(x => x.Id == rentContractId);
			} else {
				SelectedRentContract = rentContractList.FirstOrDefault();
			}
		}

		#region Properties
		public LoadPriceInfoViewModel GrowerPriceInfo { get; private set; }
		public LoadPriceInfoViewModel LandOwnerPriceInfo { get; private set; }

		private decimal _finalQuantityValue;
		public decimal FinalQuantityValue {
			get { return _finalQuantityValue; }
			set {
				if (value != _finalQuantityValue) {
					_finalQuantityValue = value;
					RaisePropertyChanged(() => FinalQuantityValue);
					recalculateShares();
				}
			}
		}

		private CompositeUnit _finalQuantityUnit;
		public CompositeUnit FinalQuantityUnit {
			get { return _finalQuantityUnit; }
			set {
				if (value != _finalQuantityUnit) {
					_finalQuantityUnit = value;
					RaisePropertyChanged(() => FinalQuantityUnit);
					recalculateShares();
				}
			}
		}

		private RentContractListItem _rentContract;
		public RentContractListItem SelectedRentContract {
			get { return _rentContract; }
			set {
				_rentContract = value;
				RaisePropertyChanged(() => SelectedRentContract);

				growerSharePercentage = 1;

				if (GetRentContractId() != null) {
					var rentContractDetails = clientEndpoint.GetView<RentContractDetailsView>(_rentContract.Id).GetValue(new RentContractDetailsView());
					growerSharePercentage = Convert.ToDecimal(rentContractDetails.IncludedShareInfo.GrowerCropShare);
				}

				recalculateShares();
			}
		}

		private bool _isSplitLoad;
		public bool IsSplitLoad {
			get { return _isSplitLoad; }
			set {
				_isSplitLoad = value;
				RaisePropertyChanged(() => IsSplitLoad);
				recalculateShares();
			}
		}

		private decimal _growerShareQuantity;
		public decimal GrowerShareQuantity {
			get { return _growerShareQuantity; }
			set {
				_growerShareQuantity = value;
				RaisePropertyChanged(() => GrowerShareQuantity);
				RaisePropertyChanged(() => GrowerShareQuantityDisplayText);
			}
		}

		private decimal _landOwnerShareQuantity;
		public decimal LandOwnerShareQuantity {
			get { return _landOwnerShareQuantity; }
			set {
				_landOwnerShareQuantity = value;
				RaisePropertyChanged(() => LandOwnerShareQuantity);
				RaisePropertyChanged(() => LandOwnerShareQuantityDisplayText);
			}
		}

		public string GrowerShareQuantityDisplayText => $"{GrowerShareQuantity:N2} {FinalQuantityUnit.AbbreviatedDisplay}";
		public string LandOwnerShareQuantityDisplayText => $"{LandOwnerShareQuantity:N2} {FinalQuantityUnit.AbbreviatedDisplay}";
		#endregion

		public ContractId GetRentContractId() {
			if (SelectedRentContract != null) {
				return SelectedRentContract.Id;
			} else {
				return null;
			}
		}

		public LoadPriceInformation GetGrowerPriceInfoValueObject() {
			if (GrowerPriceInfo != null && (GetRentContractId() == null || IsSplitLoad)) {
				return GrowerPriceInfo.GetPriceInfoValueObject();
			} else {
				return null;
			}
		}

		public LoadPriceInformation GetLandOwnerPriceInfoValueObject() {
			if (LandOwnerPriceInfo != null && GetRentContractId() != null) {
				return LandOwnerPriceInfo.GetPriceInfoValueObject();
			} else {
				return null;
			}
		}

		private void recalculateShares() {
			if (IsSplitLoad) {
				GrowerShareQuantity = FinalQuantityValue * growerSharePercentage;
				LandOwnerShareQuantity = FinalQuantityValue * (1 - growerSharePercentage);
			} else {
				if (GetRentContractId() == null || (GetRentContractId() != null && growerSharePercentage == 1)) {
					GrowerShareQuantity = FinalQuantityValue;
					LandOwnerShareQuantity = 0;
				} else {
					GrowerShareQuantity = 0;
					LandOwnerShareQuantity = FinalQuantityValue;
				}
			}

			//// special case for contracts that are 100% grower share that had load quantities saved as landowner shares
			//if (growerSharePercentage == 1 && growerPriceInfo == null && landOwnerPriceInfo != null) {
			//	growerPriceInfo = landOwnerPriceInfo;
			//	landOwnerPriceInfo = null;
			//}

			GrowerPriceInfo = new LoadPriceInfoViewModel(
				clientEndpoint,
				dispatcher,
				GrowerShareQuantity,
				FinalQuantityUnit,
				productionContractList,
				GetGrowerPriceInfoValueObject() ?? growerPriceInfo
			);

			GrowerPriceInfo.RecalculateValues();

			LandOwnerPriceInfo = new LoadPriceInfoViewModel(
				clientEndpoint,
				dispatcher,
				LandOwnerShareQuantity,
				FinalQuantityUnit,
				productionContractList,
				GetLandOwnerPriceInfoValueObject() ?? landOwnerPriceInfo
			);

			LandOwnerPriceInfo.RecalculateValues();

			RaisePropertyChanged(() => GrowerPriceInfo);
			RaisePropertyChanged(() => LandOwnerPriceInfo);
		}
	}

	public class LoadPriceInfoViewModel : ViewModelBase {

		private readonly IClientEndpoint clientEndpoint;
		private readonly Dispatcher dispatcher;

		private bool isCalculating = false;

		public LoadPriceInfoViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, decimal shareQuantity, CompositeUnit shareQuantityUnit, IEnumerable<ProductionContractListItem> productionContractList, LoadPriceInformation priceInfo) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			ShareQuantity = shareQuantity;
			ShareQuantityUnit = shareQuantityUnit;

			_grossUnitSalePrice = priceInfo.GrossUnitSalePrice;
			_grossSalePrice = priceInfo.GrossSalePrice;
			_salePriceAdjustment = priceInfo.SalePriceAdjustment;
			_finalUnitSalePrice = priceInfo.FinalUnitSalePrice;
			_finalSalePrice = priceInfo.FinalSalePrice;
			_lockedSalePrice = priceInfo.LockedSalePrice;

			_productionContract = productionContractList.SingleOrDefault(x => x.Id == priceInfo.ContractId);
		}

		#region Properties
		public decimal ShareQuantity { get; }
		public CompositeUnit ShareQuantityUnit { get; }

		public bool HasShareQuantity => ShareQuantity != 0;
		public bool IsProductionContractNull => SelectedProductionContract == null || SelectedProductionContract.Id == null;
		public bool IsUnitPriceLockedIn => _lockedSalePrice == LoadPriceLockTypes.GrossUnitPrice;
		public bool IsGrossPriceLockedIn => _lockedSalePrice == LoadPriceLockTypes.GrossPrice;

		LoadPriceLockTypes _lockedSalePrice = LoadPriceLockTypes.None;
		public LoadPriceLockTypes LockedSalePrice {
			get { return _lockedSalePrice; }
			set {
				_lockedSalePrice = value;
				RaisePropertyChanged(() => LockedSalePrice);
				RaisePropertyChanged(() => IsUnitPriceLockedIn);
				RaisePropertyChanged(() => IsGrossPriceLockedIn);
			}
		}

		private ProductionContractListItem _productionContract = null;
		public ProductionContractListItem SelectedProductionContract {
			get { return _productionContract; }
			set {
				_productionContract = value;

				if (_productionContract != null && _productionContract.Id != null) {
					var contractDetails = clientEndpoint.GetView<ProductionContractDetailsView>(_productionContract.Id).GetValue(() => null);

					if (contractDetails != null) {
						var commInfo = contractDetails.IncludedCommodityInfo;

						if (commInfo.ContractedAmountUnit == ShareQuantityUnit) {
							GrossUnitSalePrice = Convert.ToDecimal(commInfo.ContractPrice);
						} else if (commInfo.ContractedAmountUnit != null) {
							if (CompositeUnitConverter.CanConvert(ShareQuantityUnit, commInfo.ContractedAmountUnit)) {
								var quantityInContractUnit = CompositeUnitConverter.ConvertValue(
									ShareQuantity,
									ShareQuantityUnit,
									commInfo.ContractedAmountUnit
								);

								GrossSalePrice = quantityInContractUnit * Convert.ToDecimal(commInfo.ContractPrice);
								LockedSalePrice = LoadPriceLockTypes.GrossUnitPrice;
							}
						}
					}
				} else {
					GrossUnitSalePrice = 0;
					LockedSalePrice = LoadPriceLockTypes.None;
				}

				RaisePropertyChanged(() => SelectedProductionContract);
				RaisePropertyChanged(() => IsProductionContractNull);
			}
		}

		private decimal _grossUnitSalePrice = 0;
		public decimal GrossUnitSalePrice {
			get { return _grossUnitSalePrice; }
			set {
				_grossUnitSalePrice = value;
				recalculateValues(nameof(GrossUnitSalePrice));
				RaisePropertyChanged(() => GrossUnitSalePrice);
			}
		}

		private decimal _grossSalePrice = 0;
		public decimal GrossSalePrice {
			get { return _grossSalePrice; }
			set {
				_grossSalePrice = value;
				recalculateValues(nameof(GrossSalePrice));
				RaisePropertyChanged(() => GrossSalePrice);
			}
		}

		private decimal _salePriceAdjustment = 0;
		public decimal SalePriceAdjustment {
			get { return _salePriceAdjustment; }
			set {
				_salePriceAdjustment = value;
				recalculateValues(nameof(SalePriceAdjustment));
				RaisePropertyChanged(() => SalePriceAdjustment);
			}
		}

		private decimal _finalSalePrice = 0;
		public decimal FinalSalePrice {
			get { return _finalSalePrice; }
			set {
				_finalSalePrice = value;
				recalculateValues(nameof(FinalSalePrice));
				RaisePropertyChanged(() => FinalSalePrice);
			}
		}

		private decimal _finalUnitSalePrice = 0;
		public decimal FinalUnitSalePrice {
			get { return _finalUnitSalePrice; }
			set {
				_finalUnitSalePrice = value;
				recalculateValues(nameof(FinalUnitSalePrice));
				RaisePropertyChanged(() => FinalUnitSalePrice);
			}
		}
		#endregion Properties

		public void RecalculateValues() {
			recalculateValues();
		}

		public LoadPriceInformation GetPriceInfoValueObject() {
			ContractId productionContractId = null;
			string productionContractName = null;

			if (SelectedProductionContract != null) {
				productionContractId = SelectedProductionContract.Id;
				productionContractName = SelectedProductionContract.Name;
			}

			return new LoadPriceInformation(
				productionContractId,
				productionContractName,
				GrossUnitSalePrice,
				GrossSalePrice,
				SalePriceAdjustment,
				FinalUnitSalePrice,
				FinalSalePrice,
				LockedSalePrice
			);
		}

		private void recalculateValues(string callingPropertyName = null) {
			if (isCalculating == true) { return; }

			isCalculating = true;

			if (callingPropertyName == nameof(GrossUnitSalePrice)) {
				LockedSalePrice = LoadPriceLockTypes.GrossUnitPrice;
			} else if (callingPropertyName == nameof(GrossSalePrice)) {
				LockedSalePrice = LoadPriceLockTypes.GrossPrice;
			}

			switch (LockedSalePrice) {
				case LoadPriceLockTypes.None:
					// no price has been locked yet. do nothing.
					break;
				case LoadPriceLockTypes.GrossUnitPrice:
					GrossSalePrice = ShareQuantity * GrossUnitSalePrice;
					break;
				case LoadPriceLockTypes.GrossPrice:
					if (ShareQuantity != 0) {
						GrossUnitSalePrice = GrossSalePrice / ShareQuantity;
					} else {
						GrossUnitSalePrice = 0;
					}
					break;
				default:
					break;
			}

			if (callingPropertyName == nameof(FinalSalePrice)) {
				if (GrossSalePrice == 0) {
					LockedSalePrice = LoadPriceLockTypes.GrossPrice;
					GrossSalePrice = FinalSalePrice;
					GrossUnitSalePrice = GrossSalePrice / ShareQuantity;
				} else {
					SalePriceAdjustment = FinalSalePrice - GrossSalePrice;
				}
			}

			FinalSalePrice = GrossSalePrice + SalePriceAdjustment;

			if (ShareQuantity != 0) {
				FinalUnitSalePrice = FinalSalePrice / ShareQuantity;
			} else {
				FinalSalePrice = 0;
				FinalUnitSalePrice = 0;
			}

			isCalculating = false;
		}
	}
}