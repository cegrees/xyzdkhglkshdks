﻿using GalaSoft.MvvmLight;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.YieldLocation;
using Landdb.Infrastructure;
using Landdb.ViewModel.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using Landdb.Resources;

namespace Landdb.ViewModel.Yield {
	public class LoadSubLocationInfoViewModel : ViewModelBase {

		IClientEndpoint clientEndpoint;

		public LoadSubLocationInfoViewModel(IClientEndpoint clientEndpoint) {
			this.clientEndpoint = clientEndpoint;

			SubLocationsInRange = new List<YieldLocationListItemViewModel>();

			AvailableFromSubLocations = new List<YieldLocationListItemViewModel>();
			AvailableToSubLocations = new List<YieldLocationListItemViewModel>();
		}

		public LoadSubLocationInfoViewModel(IClientEndpoint clientEndpoint, List<YieldLocationListItemViewModel> locations) {
			this.clientEndpoint = clientEndpoint;

			SubLocationsInRange = new List<YieldLocationListItemViewModel>();

			AvailableFromSubLocations = new List<YieldLocationListItemViewModel>();
			AvailableToSubLocations = new List<YieldLocationListItemViewModel>();

			if (locations.Count == 1) {
				SelectedTopLevelLocation = locations.Single();
			} else if (locations.Count > 1)
            {
				var orderedSubLocations = locations.OrderBy(x => x.LocationNumber);

				SelectedTopLevelLocation = orderedSubLocations.First().ParentLocation;

				FromSelectedSubLocation = orderedSubLocations.First();
				ToSelectedSubLocation = orderedSubLocations.Last();
			} else
            {
                //SelectedTopLevelLocation = new YieldLocationListItemViewModel(string.Empty, YieldLocationTypes.Unknown, 0);
                SelectedTopLevelLocation = new YieldLocationListItemViewModel("Unknown", YieldLocationTypes.Unknown, 0);
            }
        }

		public List<YieldLocationListItemViewModel> AvailableFromSubLocations { get; private set; }
		public List<YieldLocationListItemViewModel> AvailableToSubLocations { get; private set; }

		public int SelectedSubLocationRangeCount { get; private set; }

		public List<YieldLocationListItemViewModel> SubLocationsInRange { get; }

		public decimal FinalQuantityPerSubLocation => FinalQuantityValue / SelectedSubLocationRangeCount;
		public string LocationRangeDisplayText => SubLocationsInRange.Any() ? string.Join(", ", SubLocationsInRange.Select(x => x.Name)) : string.Empty;

		public bool IsSubLocationSelectionUIEnabled {
			get {
				var retBool = false;

				if (SelectedTopLevelLocation?.SubLocationList.Any() ?? false) {
					retBool = true;

					var segmentTypes = clientEndpoint.GetMasterlistService()
						.GetYieldLocationSegmentTypes()
						.SegmentTypeList
						.Select(x => x.Name);

					foreach (var loc in SelectedTopLevelLocation.SubLocationList) {
						if (!segmentTypes.Contains(loc.Descriptor) || loc.SubLocationList.Any()) {
							retBool = false;
							break;
						}
					}
				}

				return retBool;
			}
		}

		public bool RangeIsValid =>
			FromSelectedSubLocation?.LocationNumber != null
			&& ToSelectedSubLocation?.LocationNumber != null
			&& !string.IsNullOrWhiteSpace(FromSelectedSubLocation?.Descriptor)
			&& !string.IsNullOrWhiteSpace(ToSelectedSubLocation?.Descriptor)
			&& FromSelectedSubLocation.Descriptor == ToSelectedSubLocation.Descriptor;

		public string SaleLocationAddress {
			get {
				var retString = string.Empty;

				if (SelectedTopLevelLocation != null && SelectedTopLevelLocation.YieldLocationType == YieldLocationTypes.Sale) {
					var locationDetails = clientEndpoint.GetView<YieldLocationDetailsView>(SelectedTopLevelLocation.Id).GetValue(() => null);

					if (locationDetails != null && locationDetails.StreetAddress != null) {
						retString = new StreetAddressViewModel(locationDetails.StreetAddress).MultiLineDisplay;
					}
				}

				return retString;
			}
		}

		public string SubLocationSplitDisplayText {
			get {
				var locationPlurality = SelectedSubLocationRangeCount != 1 ? Strings.Locations_Text : Strings.Location_Text;

				if (FromSelectedSubLocation != null) {
					if (FinalQuantityUnit != null) {
						return $"{SelectedSubLocationRangeCount} {Strings.Selected_Text.ToLower()} {locationPlurality} {Strings.InRange_Text.ToLower()} ({FinalQuantityPerSubLocation:N2} {FinalQuantityUnit.AbbreviatedDisplay} {Strings.PerLocation_Text})";
					} else {
						return $"{SelectedSubLocationRangeCount} {Strings.Selected_Text.ToLower()} {locationPlurality} {Strings.InRange_Text.ToLower()}";
					}
				} else {
					return string.Empty;
				}
			}
		}

		private decimal _finalQuantityValue;
		public decimal FinalQuantityValue {
			get { return _finalQuantityValue; }
			set {
				_finalQuantityValue = value;
				RaisePropertyChanged(() => FinalQuantityValue);
				RaisePropertyChanged(() => SubLocationSplitDisplayText);
			}
		}

		private CompositeUnit _finalQuantityUnit;
		public CompositeUnit FinalQuantityUnit {
			get { return _finalQuantityUnit; }
			set {
				_finalQuantityUnit = value;
				RaisePropertyChanged(() => FinalQuantityUnit);
				RaisePropertyChanged(() => SubLocationSplitDisplayText);
			}
		}

		private YieldLocationListItemViewModel _selectedTopLevelLocation;
		public YieldLocationListItemViewModel SelectedTopLevelLocation {
			get { return _selectedTopLevelLocation; }
			set {
				_selectedTopLevelLocation = value;
				RaisePropertyChanged(() => SelectedTopLevelLocation);

				FromSelectedSubLocation = null;

				if (_selectedTopLevelLocation != null) {
					AvailableFromSubLocations = YieldLocationListItemViewModel.GetFlattenedList(_selectedTopLevelLocation.SubLocationList)
						.Where(x => x.Depth == _selectedTopLevelLocation.Depth + 1)
						.ToList();
				} else {
					AvailableFromSubLocations.Clear();
				}

				RaisePropertyChanged(() => AvailableFromSubLocations);
				RaisePropertyChanged(() => IsSubLocationSelectionUIEnabled);
			}
		}

		private YieldLocationListItemViewModel _fromSelectedSubLocation;
		public YieldLocationListItemViewModel FromSelectedSubLocation {
			get { return _fromSelectedSubLocation; }
			set {
				var formerSelectedFromLocation = _fromSelectedSubLocation;

				_fromSelectedSubLocation = value;
				RaisePropertyChanged(() => FromSelectedSubLocation);

				if (_fromSelectedSubLocation != null) {
					AvailableToSubLocations = (from loc in AvailableFromSubLocations
											   where loc.ParentLocation.Id == FromSelectedSubLocation.ParentLocation.Id
											   select loc).ToList();
				} else {
					ToSelectedSubLocation = null;
					AvailableToSubLocations.Clear();
				}

				RaisePropertyChanged(() => AvailableToSubLocations);

				if (ToSelectedSubLocation == null || ToSelectedSubLocation == formerSelectedFromLocation) {
					ToSelectedSubLocation = AvailableToSubLocations.SingleOrDefault(x => x == FromSelectedSubLocation);
				}

				refreshLocationsInSelectedRange();
			}
		}

		private YieldLocationListItemViewModel _toSelectedSubLocation;
		public YieldLocationListItemViewModel ToSelectedSubLocation {
			get { return _toSelectedSubLocation; }
			set {
				_toSelectedSubLocation = value;
				RaisePropertyChanged(() => ToSelectedSubLocation);
				refreshLocationsInSelectedRange();
			}
		}

		public YieldLocationId[] GetSelectedLocationIds() {
			if (SubLocationsInRange.Any()) {
				return SubLocationsInRange.Select(x => x.Id).ToArray();
			} else {
                return new YieldLocationId[] { SelectedTopLevelLocation != null ? SelectedTopLevelLocation.Id : null };
            }
		}

		private void refreshLocationsInSelectedRange() {
			SubLocationsInRange.Clear();

			if (RangeIsValid) {
				var initialValue = Math.Min(FromSelectedSubLocation.LocationNumber.Value, ToSelectedSubLocation.LocationNumber.Value);
				var terminatingValue = Math.Max(FromSelectedSubLocation.LocationNumber.Value, ToSelectedSubLocation.LocationNumber.Value);

				for (int i = initialValue; i <= terminatingValue; i++) {
					var matchingLocations = from loc in AvailableToSubLocations
											where loc.LocationNumber == i
												 && loc.Descriptor == FromSelectedSubLocation.Descriptor
											select loc;

					if (matchingLocations.Count() == 1) {
						SubLocationsInRange.Add(matchingLocations.Single());
					}
				}
			}

			SelectedSubLocationRangeCount = SubLocationsInRange.Count;

			RaisePropertyChanged(() => SubLocationSplitDisplayText);
		}
	}
}