﻿using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Contract;
using System;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Yield {
	public class IncludedSplitLoadInfoViewModel {

		private readonly IClientEndpoint clientEndpoint;
		private readonly Dispatcher dispatcher;

		public IncludedSplitLoadInfoViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, decimal finalQuantityValue, CompositeUnit finalQuantityUnit, ContractId rentContractId, LoadPriceInformation growerPriceInfo, LoadPriceInformation landOwnerPriceInfo) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			decimal growerSharePercentage = 1;

			if (rentContractId != null) {
				HasRentContract = true;

				var rentContractDetails = clientEndpoint.GetView<RentContractDetailsView>(rentContractId).GetValue(new RentContractDetailsView());
				RentContractName = rentContractDetails.Name;
				growerSharePercentage = Convert.ToDecimal(rentContractDetails.IncludedShareInfo.GrowerCropShare);

				if (growerPriceInfo == null && landOwnerPriceInfo != null) {
					if (growerSharePercentage == 1) {
						// special case for contracts that are 100% grower share that had load quantities saved as landowner shares
						growerPriceInfo = landOwnerPriceInfo;
						landOwnerPriceInfo = null;
					} else {
						// apply everything to landowner
						growerSharePercentage = 0;
					}
				}
			}

			var growerShareQuantity = finalQuantityValue * growerSharePercentage;
			var landOwnerShareQuantity = finalQuantityValue * (1 - growerSharePercentage);

			if (growerShareQuantity > 0) {
				var shareDescription = $"{growerSharePercentage:P2} {Strings.GrowerShare_Text.ToLower()}";
				GrowerPriceInfo = new IncludedPriceInfoViewModel(growerShareQuantity, finalQuantityUnit, growerPriceInfo, shareDescription);
			}

			if (landOwnerShareQuantity > 0) {
				var shareDescription = $"{(1 - growerSharePercentage):P2} {Strings.LandownerShare_Text.ToLower()}";
				LandOwnerPriceInfo = new IncludedPriceInfoViewModel(landOwnerShareQuantity, finalQuantityUnit, landOwnerPriceInfo, shareDescription);
			}
		}

		public string RentContractName { get; }
		public bool HasRentContract { get; }

		public IncludedPriceInfoViewModel GrowerPriceInfo { get; }
		public IncludedPriceInfoViewModel LandOwnerPriceInfo { get; }

		public bool HasGrowerPriceInfo => GrowerPriceInfo != null;
		public bool HasLandOwnerPriceInfo => LandOwnerPriceInfo != null;

		public LoadPriceInformation GetPrimaryPriceInfoValueObject() {
			if (GrowerPriceInfo != null) {
				return GrowerPriceInfo.BasePriceInfo;
			} else {
				return null;
			}
		}

		public LoadPriceInformation GetSecondaryPriceInfoValueObject() {
			if (LandOwnerPriceInfo != null) {
				return LandOwnerPriceInfo.BasePriceInfo;
			} else {
				return null;
			}
		}
	}

	public class IncludedPriceInfoViewModel {
		public IncludedPriceInfoViewModel(decimal finalQuantityValue, CompositeUnit finalQuantityUnit, LoadPriceInformation priceInfo, string shareDescription) {
			BasePriceInfo = priceInfo;

			ProductionContractName = !string.IsNullOrWhiteSpace(priceInfo.ContractName) ? priceInfo.ContractName : Strings.UnspecifiedProductionContractSpotSale_Text;
			GrossUnitSalePrice = $"{priceInfo.GrossUnitSalePrice:C} / {finalQuantityUnit.AbbreviatedDisplay}";
			GrossSalePrice = priceInfo.GrossSalePrice.ToString("C");
			SalePriceAdjustment = priceInfo.SalePriceAdjustment.ToString("C");
			FinalSalePrice = priceInfo.FinalSalePrice.ToString("C");
			FinalUnitSalePrice = $"{priceInfo.FinalUnitSalePrice:C} / {finalQuantityUnit.AbbreviatedDisplay}";
			ShareQuantity = $"{finalQuantityValue:N2} {finalQuantityUnit.AbbreviatedDisplay}";
			ShareDescription = shareDescription;
		}

		public LoadPriceInformation BasePriceInfo { get; }

		public string ProductionContractName { get; }
		public string GrossUnitSalePrice { get; }
		public string GrossSalePrice { get; }
		public string SalePriceAdjustment { get; }
		public string FinalSalePrice { get; }
		public string FinalUnitSalePrice { get; }
		public string ShareQuantity { get; }
		public string ShareDescription { get; }

		public bool HasShareDescription => !string.IsNullOrWhiteSpace(ShareDescription);
	}
}