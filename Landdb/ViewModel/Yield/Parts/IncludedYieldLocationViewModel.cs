﻿//using Landdb.Domain.ReadModels.Yield;
//using Landdb.Domain.ReadModels.YieldLocation;
//using Landdb.ViewModel.Shared;

//namespace Landdb.ViewModel.Yield {
//	public class IncludedYieldLocationViewModel {

//		public IncludedYieldLocationViewModel(YieldLocationId id, string name, StreetAddress streetAddress, YieldLocationTypes type, YieldLocationId parentLocationId) {
//			Id = id;
//			Name = name;
//			StreetAddress = new StreetAddressViewModel(streetAddress);
//			Type = type;
//			ParentLocationId = parentLocationId;
//		}

//		public IncludedYieldLocationViewModel(YieldLocationDetailsView locationDetailsView) :
//			this(locationDetailsView.Id, locationDetailsView.Name, locationDetailsView.StreetAddress, locationDetailsView.YieldLocationType, locationDetailsView.ParentLocationId) { }

//		public IncludedYieldLocationViewModel(IncludedYieldLocation includedYieldLocation):
//			this(includedYieldLocation.Id, includedYieldLocation.Name, includedYieldLocation.StreetAddress, includedYieldLocation.YieldLocationType, includedYieldLocation.ParentLocationId) { }

//		public YieldLocationId Id { get; }
//		public string Name { get; }
//		public StreetAddressViewModel StreetAddress { get; }
//		public YieldLocationTypes Type { get; }
//		public YieldLocationId ParentLocationId { get; set; }

//		public override string ToString() {
//			return $"{Name} ({Type})";
//		}
//	}
//}