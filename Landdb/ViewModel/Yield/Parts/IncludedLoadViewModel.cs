﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Domain.ReadModels.Yield;
using Landdb.Infrastructure;
using Landdb.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using Landdb.Resources;

namespace Landdb.ViewModel.Yield {
	public class IncludedLoadViewModel : ViewModelBase {

		private IncludedLoadViewModel(
			YieldOperationTypes operationType,
			LoadId loadId,
			string loadNumber,
			DateTime startDateTime,
			List<CropZoneId> cropZoneIds,
			string fullCommodityDisplayText,
			CompositeUnit finalQuantityUnit,
			decimal totalShrinkQuantityValue,
			decimal finalQuantityValue,
			decimal locationWeightedFinalQuantity,
			decimal deliveredQuantity,
			decimal locationWeightedDeliveredQuantity,
			YieldLocationId sourceLocationId,
			string sourceDescription,
			YieldLocationId destinationLocationId,
			string destinationLocationName,
			decimal growerSalePrice,
			decimal locationWeightedGrowerSalePrice,
			decimal landOwnerSalePrice,
			decimal locationWeightedLandOwnerSalePrice,
			ContractId rentContractId,
			bool hasPrice) {

			OperationType = operationType;
			LoadId = loadId;
			LoadNumber = loadNumber;
			StartDateTime = startDateTime;
			CropZones = cropZoneIds;
			FullCommodityDisplayText = fullCommodityDisplayText;
			QuantityUnit = finalQuantityUnit;
			TotalShrinkQuantityValue = totalShrinkQuantityValue;
			FinalQuantityValue = finalQuantityValue;
			LocationWeightedFinalQuantity = locationWeightedFinalQuantity;
			DeliveredQuantity = deliveredQuantity;
			LocationWeightedDeliveredQuantity = locationWeightedDeliveredQuantity;
			SourceLocationId = sourceLocationId;
			SourceDescription = sourceDescription;
			DestinationLocationId = destinationLocationId;
			DestinationLocationDescription = destinationLocationName;
			GrowerSalePrice = growerSalePrice;
			LocationWeightedGrowerSalePrice = locationWeightedGrowerSalePrice;
			LandOwnerSalePrice = landOwnerSalePrice;
			LocationWeightedLandOwnerSalePrice = locationWeightedLandOwnerSalePrice;
			FinalSalePrice = growerSalePrice + landOwnerSalePrice;
			LocationWeightedFinalSalePrice = locationWeightedGrowerSalePrice + locationWeightedLandOwnerSalePrice;
			FinalUnitPrice = FinalQuantityValue > 0 ? FinalSalePrice / FinalQuantityValue : 0;
			LocationWeightedFinalUnitPrice = FinalQuantityValue > 0 ? LocationWeightedFinalSalePrice / FinalQuantityValue : 0;
			RentContractId = rentContractId;
			HasPrice = hasPrice;

			JumpToLoadCommand = new RelayCommand(onJumpToLoad);
		}

		public ICommand JumpToLoadCommand { get; }

		public YieldOperationTypes OperationType { get; }
		public LoadId LoadId { get; }
		public string LoadNumber { get; }
		public DateTime StartDateTime { get; }
		public string FullCommodityDisplayText { get; }
		public List<CropZoneId> CropZones { get; }
		public CompositeUnit QuantityUnit { get; }
		public decimal TotalShrinkQuantityValue { get; }
		public decimal FinalQuantityValue { get; }
		public decimal LocationWeightedFinalQuantity { get; }
		public decimal DeliveredQuantity { get; }
		public decimal LocationWeightedDeliveredQuantity { get; }
		public YieldLocationId SourceLocationId { get; }
		public string SourceDescription { get; }
		public YieldLocationId DestinationLocationId { get; }
		public string DestinationLocationDescription { get; }
		public decimal GrowerSalePrice { get; }
		public decimal LocationWeightedGrowerSalePrice { get; }
		public decimal LandOwnerSalePrice { get; }
		public decimal LocationWeightedLandOwnerSalePrice { get; }
		public decimal FinalSalePrice { get; }
		public decimal LocationWeightedFinalSalePrice { get; }
		public decimal FinalUnitPrice { get; }
		public decimal LocationWeightedFinalUnitPrice { get; }
		public ContractId RentContractId { get; }
		public bool HasPrice { get; }

		public string LoadQuantityDisplayText => $"{LocationWeightedFinalQuantity:N2} {QuantityUnit.AbbreviatedDisplay}";
		public string FinalPricePerUnitDisplayText => $"{FinalUnitPrice:C} / {QuantityUnit.AbbreviatedDisplay}";
		public string LocationWeightedFinalPricePerUnitDisplayText => $"{LocationWeightedFinalUnitPrice:C} / {QuantityUnit.AbbreviatedDisplay}";

		public bool IsLoadFromStorage =>
			OperationType == YieldOperationTypes.StorageToSale
				|| OperationType == YieldOperationTypes.StorageToStorage;

		public bool IsLoadFromField =>
			OperationType == YieldOperationTypes.FieldToSale
				|| OperationType == YieldOperationTypes.FieldToStorage;

		public bool IsLoadSold =>
			OperationType == YieldOperationTypes.FieldToSale
				|| OperationType == YieldOperationTypes.StorageToSale;

		public bool IsLoadStored =>
			OperationType == YieldOperationTypes.FieldToStorage
				|| OperationType == YieldOperationTypes.StorageToStorage;

		public static List<IncludedLoadViewModel> GetIncludedLoadsFromListItem(FlattenedTreeHierarchyView treeView, Dictionary<YieldLocationId, string> locationDictionary, LoadListItem loadListItem) {
			var retList = new List<IncludedLoadViewModel>();

			var destinationWeight = (loadListItem.DestinationLocations?.Count ?? 0) != 0 ? (1m / loadListItem.DestinationLocations.Count) : 0;

			if (loadListItem.OperationType == YieldOperationTypes.FieldToStorage || loadListItem.OperationType == YieldOperationTypes.FieldToSale) {
				string sourceDescription = null;

				if (loadListItem.CropZoneIds.Count != 1) {
					sourceDescription = $"{loadListItem.CropZoneIds.Count} {Strings.Fields_Text}";
				} else {
					var cz = treeView.Items.FirstOrDefault(x => x.CropZoneId == loadListItem.CropZoneIds.Single());
					sourceDescription = cz != null ? cz.GetFullName() : $"-{Strings.UnknownField_Text}-";
				}

				foreach (var destinationLoc in loadListItem.DestinationLocations) {
					retList.Add(new IncludedLoadViewModel(
						loadListItem.OperationType,
						loadListItem.Id,
						loadListItem.LoadNumber,
						loadListItem.StartDateTime,
						loadListItem.CropZoneIds,
						loadListItem.Commodity.FullCommodityDisplayText,
						loadListItem.FinalQuantityUnit,
						loadListItem.TotalShrinkQuantityValue,
						loadListItem.FinalQuantityValue,
						loadListItem.FinalQuantityValue * destinationWeight,
						loadListItem.FinalQuantityValue + loadListItem.TotalShrinkQuantityValue,
						(loadListItem.FinalQuantityValue + loadListItem.TotalShrinkQuantityValue) * destinationWeight,
						null,
						sourceDescription,
						destinationLoc.Id,
						locationDictionary.ContainsKey(destinationLoc.Id) ? locationDictionary[destinationLoc.Id] : $"-{Strings.UnknownLocation_Text}-",
						0,
						0,
						0,
						0,
						null,
						loadListItem.OperationType == YieldOperationTypes.FieldToSale || loadListItem.OperationType == YieldOperationTypes.StorageToSale
					));
				}
			} else {
				var sourceWeight = (loadListItem.SourceLocations?.Count ?? 0) != 0 ? (1m / loadListItem.SourceLocations.Count) : 0;

				foreach (var sourceLoc in loadListItem.SourceLocations) {
					foreach (var destinationLoc in loadListItem.DestinationLocations) {
						retList.Add(new IncludedLoadViewModel(
							loadListItem.OperationType,
							loadListItem.Id,
							loadListItem.LoadNumber,
							loadListItem.StartDateTime,
							loadListItem.CropZoneIds,
							loadListItem.Commodity.FullCommodityDisplayText,
							loadListItem.FinalQuantityUnit,
							loadListItem.TotalShrinkQuantityValue,
							loadListItem.FinalQuantityValue,
							loadListItem.FinalQuantityValue * sourceWeight * destinationWeight,
							loadListItem.FinalQuantityValue + loadListItem.TotalShrinkQuantityValue,
							(loadListItem.FinalQuantityValue + loadListItem.TotalShrinkQuantityValue) * sourceWeight * destinationWeight,
							sourceLoc.Id,
							locationDictionary.ContainsKey(sourceLoc.Id) ? locationDictionary[sourceLoc.Id] : $"-{Strings.UnknownLocation_Text}-",
							destinationLoc.Id,
							locationDictionary.ContainsKey(destinationLoc.Id) ? locationDictionary[destinationLoc.Id] : $"-{Strings.UnknownLocation_Text}-",
							0,
							0,
							0,
							0,
							null,
							loadListItem.OperationType == YieldOperationTypes.FieldToSale || loadListItem.OperationType == YieldOperationTypes.StorageToSale
						));
					}
				}
			}

			return retList;
		}

		public static List<IncludedLoadViewModel> GetListFromIncludedLoad(FlattenedTreeHierarchyView treeView, Dictionary<YieldLocationId, string> locationNameDictionary, IncludedLoad includedLoad) {
			var retList = new List<IncludedLoadViewModel>();

			if (includedLoad.IsLoadFromField) {
				string sourceDescription = null;

				if (includedLoad.CropZones.Count != 1) {
					sourceDescription = $"{includedLoad.CropZones.Count} {Strings.Fields_Text.ToLower()}";
				} else {
					var cz = treeView.Items.FirstOrDefault(x => x.CropZoneId == includedLoad.CropZones.Single());
					sourceDescription = cz != null ? cz.GetFullName() : $"-{Strings.UnknownField_Text}-";
				}

				foreach (var destLoc in includedLoad.DestinationLocations) {
					retList.Add(new IncludedLoadViewModel(
						includedLoad.OperationType,
						includedLoad.Id,
						includedLoad.LoadNumber,
						includedLoad.StartTime,
						includedLoad.CropZones,
						includedLoad.Commodity.FullCommodityDisplayText,
						includedLoad.QuantityUnit,
						includedLoad.TotalShrinkQuantityValue,
						includedLoad.FinalQuantityValue,
						includedLoad.LocationWeightedFinalQuantity,
						includedLoad.DeliveredQuantityValue,
						includedLoad.LocationWeightedDeliveredQuantity,
						null,
						sourceDescription,
						destLoc.Id,
						locationNameDictionary.ContainsKey(destLoc.Id) ? locationNameDictionary[destLoc.Id] : $"-{Strings.UnknownLocation_Text}-",
						includedLoad.GrowerSalePrice,
						includedLoad.LocationWeightedGrowerSalePrice,
						includedLoad.LandOwnerSalePrice,
						includedLoad.LocationWeightedLandOwnerSalePrice,
						includedLoad.RentContractId,
						includedLoad.IsLoadSold
					));
				}
			} else {
				foreach (var sourceLoc in includedLoad.SourceLocations) {
					foreach (var destLoc in includedLoad.DestinationLocations) {
						retList.Add(new IncludedLoadViewModel(
							includedLoad.OperationType,
							includedLoad.Id,
							includedLoad.LoadNumber,
							includedLoad.StartTime,
							includedLoad.CropZones,
							includedLoad.Commodity.FullCommodityDisplayText,
							includedLoad.QuantityUnit,
							includedLoad.TotalShrinkQuantityValue,
							includedLoad.FinalQuantityValue,
							includedLoad.LocationWeightedFinalQuantity,
							includedLoad.DeliveredQuantityValue,
							includedLoad.LocationWeightedDeliveredQuantity,
							sourceLoc.Id,
							locationNameDictionary.ContainsKey(sourceLoc.Id) ? locationNameDictionary[sourceLoc.Id] : $"-{Strings.UnknownLocation_Text}-",
							destLoc.Id,
							locationNameDictionary.ContainsKey(destLoc.Id) ? locationNameDictionary[destLoc.Id] : $"-{Strings.UnknownLocation_Text}-",
							includedLoad.GrowerSalePrice,
							includedLoad.LocationWeightedGrowerSalePrice,
							includedLoad.LandOwnerSalePrice,
							includedLoad.LocationWeightedLandOwnerSalePrice,
							includedLoad.RentContractId,
							includedLoad.IsLoadSold
						));
					}
				}
			}

			return retList;
		}

		public override string ToString() => $"{OperationType} {Strings.LoadNumber_Text.ToLower()} {LoadNumber}";

		private void onJumpToLoad() {
			var isFromField = OperationType == YieldOperationTypes.FieldToSale || OperationType == YieldOperationTypes.FieldToStorage;

			ViewRouter.JumpToYieldLoad(LoadId, isFromField);
		}
	}
}