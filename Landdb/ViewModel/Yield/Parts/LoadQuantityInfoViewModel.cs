﻿using Landdb.Client.Infrastructure;
using NLog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Yield {
	public class LoadQuantityInfoViewModel : ValidationViewModelBase {

		private readonly IClientEndpoint clientEndpoint;
		private readonly Dispatcher dispatcher;

		private readonly Logger log = LogManager.GetCurrentClassLogger();

		private readonly Action finalQuantityChanged;
		private readonly Action truckUnitChanged;

		private bool isCalculating = false;

		// constructor for new loads
		public LoadQuantityInfoViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, Action finalQuantityChanged, Action truckUnitChanged) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			this.finalQuantityChanged += finalQuantityChanged;
			this.truckUnitChanged += truckUnitChanged;
		}

		// constructor for editing loads
		public LoadQuantityInfoViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher, LoadQuantityInformation_v2 quantityInfo, IEnumerable<CompositeUnit> allowedUnits) {
			this.clientEndpoint = clientEndpoint;
			this.dispatcher = dispatcher;

			AllowedUnitList = new ObservableCollection<CompositeUnit>(allowedUnits.OrderBy(x => x.FullDisplay));

			_truckGrossWeight = quantityInfo.TruckGrossWeight;
			_truckTareWeight = quantityInfo.TruckTareWeight;
			_truckNetWeight = quantityInfo.TruckNetWeight;
			_truckWeightUnit = quantityInfo.TruckWeightUnit;
			_initialMoisturePercentage = quantityInfo.InitialMoisturePercentage;
			_standardDryMoisturePercentage = quantityInfo.StandardTargetMoisturePercentage;
			_standardMoistureShrinkQuantity = quantityInfo.StandardMoistureShrinkQuantity;
			_standardMoistureShrinkPercentage = quantityInfo.StandardMoistureShrinkPercentage;
			_handlingShrinkPercentage = quantityInfo.HandlingShrinkPercentage;
			_handlingShrinkQuantity = quantityInfo.HandlingShrinkQuantity;
			_adjustmentShrinkQuantity = quantityInfo.AdjustmentShrinkQuantity;
			_adjustmentShrinkPercentage = quantityInfo.AdjustmentShrinkPercentage;
			_lockedAdjustmentShrink = quantityInfo.LockedAdjustmentShrink;
			_finalQuantity = quantityInfo.FinalQuantityValue;

			if (CompositeUnitConverter.CanConvert(quantityInfo.TruckWeightUnit, quantityInfo.FinalQuantityUnit)) {
				_deliveredQuantity = CompositeUnitConverter.ConvertValue(quantityInfo.TruckNetWeight, quantityInfo.TruckWeightUnit, quantityInfo.FinalQuantityUnit);
			} else {
				_deliveredQuantity = quantityInfo.FinalQuantityValue - quantityInfo.StandardMoistureShrinkQuantity - quantityInfo.HandlingShrinkQuantity - quantityInfo.AdjustmentShrinkQuantity;
			}

			var unit = AllowedUnitList.FirstOrDefault(x => x.FullDisplay == quantityInfo.FinalQuantityUnit.FullDisplay);
			if (unit != null) { _finalQuantityUnit = unit; }

			recalculateValues();
		}

		#region Public Properties
		public ObservableCollection<CompositeUnit> AllowedUnitList { get; }

		// used for the custom combo box for units
		private bool _isFinalQuantityUnitListVisible = false;
		public bool IsFinalQuantityUnitListVisible {
			get { return _isFinalQuantityUnitListVisible; }
			set {
				_isFinalQuantityUnitListVisible = value;
				RaisePropertyChanged(() => IsFinalQuantityUnitListVisible);
			}
		}

		private CompositeUnit _truckWeightUnit = new CompositeUnit(AgC.UnitConversion.MassAndWeight.Pound.Self);
		public CompositeUnit SelectedTruckWeightUnit {
			get { return _truckWeightUnit; }
			set {
				try {
					if (value == null) { throw new ArgumentNullException(); }

					_truckWeightUnit = value;
					recalculateValues(nameof(SelectedTruckWeightUnit));

					truckUnitChanged?.Invoke();

					RaisePropertyChanged(() => SelectedTruckWeightUnit);
					RaisePropertyChanged(() => TruckGrossWeightDisplayText);
					RaisePropertyChanged(() => TruckTareWeightDisplayText);
					RaisePropertyChanged(() => TruckNetWeightDisplayText);
				} catch (ArgumentNullException ex) {
					log.WarnException("Tried to assign null to SelectedTruckWeightUnit", ex);
				}
			}
		}

		private decimal _truckGrossWeight = 0;
		public decimal TruckGrossWeight {
			get { return _truckGrossWeight; }
			set {
				_truckGrossWeight = value;
				recalculateValues(nameof(TruckGrossWeight));
				RaisePropertyChanged(() => TruckGrossWeight);
				RaisePropertyChanged(() => TruckGrossWeightDisplayText);
			}
		}

		public string TruckGrossWeightDisplayText {
			get { return $"{TruckGrossWeight:N2} {SelectedTruckWeightUnit.AbbreviatedDisplay}"; }
			set {
				if (string.IsNullOrWhiteSpace(value)) { return; }

				decimal parsedValue = 0;
				if (decimal.TryParse(value, out parsedValue)) {
					TruckGrossWeight = parsedValue;
				}
			}
		}

		private decimal _truckTareWeight = 0;
		public decimal TruckTareWeight {
			get { return _truckTareWeight; }
			set {
				_truckTareWeight = value;
				recalculateValues(nameof(TruckTareWeight));
				RaisePropertyChanged(() => TruckTareWeight);
				RaisePropertyChanged(() => TruckTareWeightDisplayText);
			}
		}

		public string TruckTareWeightDisplayText {
			get { return $"{TruckTareWeight:N2} {SelectedTruckWeightUnit.AbbreviatedDisplay}"; }
			set {
				if (string.IsNullOrWhiteSpace(value)) { return; }

				decimal parsedValue = 0;
				if (decimal.TryParse(value, out parsedValue)) {
					TruckTareWeight = parsedValue;
				}
			}
		}

		private decimal _truckNetWeight = 0;
		public decimal TruckNetWeight {
			get { return _truckNetWeight; }
			set {
				_truckNetWeight = value;
				recalculateValues(nameof(TruckNetWeight));
				RaisePropertyChanged(() => TruckNetWeight);
				RaisePropertyChanged(() => TruckNetWeightDisplayText);
			}
		}

		public string TruckNetWeightDisplayText {
			get { return $"{TruckNetWeight:N2} {SelectedTruckWeightUnit.AbbreviatedDisplay}"; }
			set {
				if (string.IsNullOrWhiteSpace(value)) { return; }

				decimal parsedValue = 0;
				if (decimal.TryParse(value, out parsedValue)) {
					TruckNetWeight = parsedValue;
				}
			}
		}

		private decimal _truckUnitPerSaleUnit;

		public decimal TruckUnitPerSaleUnit {
			get { return _truckUnitPerSaleUnit; }
			set {
				_truckUnitPerSaleUnit = value;
				RaisePropertyChanged(() => TruckUnitPerSaleUnit);
				RaisePropertyChanged(() => TruckUnitPerSaleUnitDisplayText);
				RaisePropertyChanged(() => IsTruckUnitPerSaleUnitVisible);
			}
		}

		private decimal _deliveredQuantity = 0;
		public decimal DeliveredQuantity {
			get { return _deliveredQuantity; }
			set {
				_deliveredQuantity = value;
				recalculateValues(nameof(DeliveredQuantity));
				RaisePropertyChanged(() => DeliveredQuantity);
				RaisePropertyChanged(() => DeliveredQuantityDisplayText);
			}
		}

		public string DeliveredQuantityDisplayText {
			get { return $"{DeliveredQuantity:N2} {SelectedFinalQuantityUnit.AbbreviatedDisplay}"; }
			set {
				if (string.IsNullOrWhiteSpace(value)) { return; }

				decimal parsedValue = 0;
				if (decimal.TryParse(value, out parsedValue)) {
					DeliveredQuantity = parsedValue;
				}
			}
		}

		private decimal _initialMoisturePercentage = 0;
		[Range(typeof(decimal), "0", "1", ErrorMessageResourceName = "ErrorMessage_InitialMoisturePercentage_Text", ErrorMessageResourceType = typeof(Strings))]
		public decimal InitialMoisturePercentage {
			get { return _initialMoisturePercentage; }
			set {
				_initialMoisturePercentage = value;
				recalculateValues(nameof(InitialMoisturePercentage));
				RaisePropertyChanged(() => InitialMoisturePercentage);
			}
		}

		private decimal _adjustmentShrinkPercentage = 0;
		[Range(typeof(decimal), "-1", "1", ErrorMessageResourceName = "ErrorMessage_ShrinkAdjustmentPercentage_Text", ErrorMessageResourceType = typeof(Strings))]
		public decimal AdjustmentShrinkPercentage {
			get { return _adjustmentShrinkPercentage; }
			set {
				_adjustmentShrinkPercentage = value;
				recalculateValues(nameof(AdjustmentShrinkPercentage));
				RaisePropertyChanged(() => AdjustmentShrinkPercentage);
				RaisePropertyChanged(() => AdjustmentShrinkPercentageDisplayText);
			}
		}

		public string AdjustmentShrinkPercentageDisplayText {
			get { return AdjustmentShrinkPercentage.ToString("P2"); }
			set {
				if (string.IsNullOrWhiteSpace(value)) { return; }

				decimal parsedValue = 0;
				if (decimal.TryParse(value, out parsedValue)) {
					AdjustmentShrinkPercentage = parsedValue / 100;
				}
			}
		}

		private decimal _adjustmentShrinkQuantity = 0;
		public decimal AdjustmentShrinkQuantity {
			get { return _adjustmentShrinkQuantity; }
			set {
				_adjustmentShrinkQuantity = value;
				recalculateValues(nameof(AdjustmentShrinkQuantity));
				RaisePropertyChanged(() => AdjustmentShrinkQuantity);
				RaisePropertyChanged(() => AdjustmentShrinkQuantityDisplayText);
			}
		}

		public string AdjustmentShrinkQuantityDisplayText {
			get { return $"{AdjustmentShrinkQuantity:N2} {SelectedFinalQuantityUnit.AbbreviatedDisplay}"; }
			set {
				if (string.IsNullOrWhiteSpace(value)) { return; }

				decimal parsedValue = 0;
				if (decimal.TryParse(value, out parsedValue)) {
					AdjustmentShrinkQuantity = parsedValue;
				}
			}
		}

		private decimal _finalQuantity = 0;
		public decimal FinalQuantity {
			get { return _finalQuantity; }
			set {
				_finalQuantity = value;
				recalculateValues(nameof(FinalQuantity));
				RaisePropertyChanged(() => FinalQuantity);
				RaisePropertyChanged(() => FinalQuantityDisplayText);
			}
		}

		private CompositeUnit _finalQuantityUnit = new CompositeUnit(AgC.UnitConversion.MassAndWeight.Pound.Self);
		public CompositeUnit SelectedFinalQuantityUnit {
			get { return _finalQuantityUnit; }
			set {
				try {
					if (value != null) {
						_finalQuantityUnit = value;
					} else {
						_finalQuantityUnit = new CompositeUnit(AgC.UnitConversion.MassAndWeight.Pound.Self);
					}

					recalculateValues(nameof(SelectedFinalQuantityUnit));

					RaisePropertyChanged(() => SelectedFinalQuantityUnit);
					RaisePropertyChanged(() => SelectedTruckWeightUnit);
					RaisePropertyChanged(() => TruckGrossWeightDisplayText);
					RaisePropertyChanged(() => TruckTareWeightDisplayText);
					RaisePropertyChanged(() => TruckNetWeightDisplayText);
					RaisePropertyChanged(() => AdjustmentShrinkQuantityDisplayText);
				} catch (ArgumentNullException ex) {
					log.WarnException("Tried to assign null to SelectedFinalQuantityUnit", ex);
				}
			}
		}
		#endregion

		#region Get-Only Properties
		public bool IsAdjustmentShrinkValueLockedIn => _lockedAdjustmentShrink == LoadShrinkLockTypes.Value;
		public bool IsAdjustmentShrinkPercentageLockedIn => _lockedAdjustmentShrink == LoadShrinkLockTypes.Percentage;
		public string StandardMoistureShrinkQuantityDisplayText => $"{StandardMoistureShrinkQuantity:N2} {SelectedFinalQuantityUnit.AbbreviatedDisplay}";
		public string HandlingShrinkQuantityDisplayText => $"{HandlingShrinkQuantity:N2} {SelectedFinalQuantityUnit.AbbreviatedDisplay}";
		public string TotalShrinkQuantityDisplayText => $"{TotalShrinkQuantity:N2} {SelectedFinalQuantityUnit.AbbreviatedDisplay}";
		public string FinalQuantityDisplayText => $"{FinalQuantity:N2} {SelectedFinalQuantityUnit.AbbreviatedDisplay}";
		public string TruckUnitPerSaleUnitDisplayText => $"{TruckUnitPerSaleUnit:N2} {SelectedTruckWeightUnit.AbbreviatedDisplay} / {SelectedFinalQuantityUnit.AbbreviatedDisplay}";
		public bool IsTruckUnitPerSaleUnitVisible => !CompositeUnitConverter.CanConvert(SelectedTruckWeightUnit, SelectedFinalQuantityUnit);
		#endregion

		#region Private Properties
		private bool _isTruckGrossWeightLockedIn = false;
		public bool IsTruckGrossWeightLockedIn {
			get { return _isTruckGrossWeightLockedIn; }
			private set {
				_isTruckGrossWeightLockedIn = value;
				RaisePropertyChanged(() => IsTruckGrossWeightLockedIn);
			}
		}

		LoadShrinkLockTypes _lockedAdjustmentShrink = LoadShrinkLockTypes.None;
		public LoadShrinkLockTypes LockedAdjustmentShrink {
			get { return _lockedAdjustmentShrink; }
			private set {
				_lockedAdjustmentShrink = value;
				RaisePropertyChanged(() => LockedAdjustmentShrink);
				RaisePropertyChanged(() => IsAdjustmentShrinkValueLockedIn);
				RaisePropertyChanged(() => IsAdjustmentShrinkPercentageLockedIn);
			}
		}

		private decimal _standardDryMoisturePercentage = 0;
		public decimal StandardDryMoisturePercentage {
			get { return _standardDryMoisturePercentage; }
			private set {
				_standardDryMoisturePercentage = value;
				RaisePropertyChanged(() => StandardDryMoisturePercentage);
			}
		}

		private decimal _estimatedShrinkRatePercentage;
		public decimal EstimatedShrinkRatePercentage {
			get { return _estimatedShrinkRatePercentage; }
			private set {
				_estimatedShrinkRatePercentage = value;
				RaisePropertyChanged(() => EstimatedShrinkRatePercentage);
			}
		}

		private decimal _handlingShrinkTargetMoisturePercentage;
		public decimal HandlingShrinkTargetMoisturePercentage {
			get { return _handlingShrinkTargetMoisturePercentage; }
			private set {
				_handlingShrinkTargetMoisturePercentage = value;
				RaisePropertyChanged(() => HandlingShrinkTargetMoisturePercentage);
			}
		}

		private decimal _standardMoistureShrinkPercentage = 0;
		[Range(typeof(decimal), "-1", "1", ErrorMessageResourceName = "ErrorMessage_MoistureShrinkPercentage_Text", ErrorMessageResourceType = typeof(Strings))]
		public decimal StandardMoistureShrinkPercentage {
			get { return _standardMoistureShrinkPercentage; }
			private set {
				_standardMoistureShrinkPercentage = value;
				RaisePropertyChanged(() => StandardMoistureShrinkPercentage);
			}
		}

		private decimal _standardMoistureShrinkQuantity = 0;
		public decimal StandardMoistureShrinkQuantity {
			get { return _standardMoistureShrinkQuantity; }
			private set {
				_standardMoistureShrinkQuantity = value;
				RaisePropertyChanged(() => StandardMoistureShrinkQuantity);
				RaisePropertyChanged(() => StandardMoistureShrinkQuantityDisplayText);
			}
		}

		private decimal _handlingShrinkPercentage = 0;
		[Range(typeof(decimal), "-1", "1", ErrorMessageResourceName = "ErrorMessage_HandlingShrinkPercentage_Text", ErrorMessageResourceType = typeof(Strings))]
		public decimal HandlingShrinkPercentage {
			get { return _handlingShrinkPercentage; }
			private set {
				_handlingShrinkPercentage = value;
				RaisePropertyChanged(() => HandlingShrinkPercentage);
			}
		}


		private decimal _handlingShrinkQuantity = 0;
		public decimal HandlingShrinkQuantity {
			get { return _handlingShrinkQuantity; }
			private set {
				_handlingShrinkQuantity = value;
				RaisePropertyChanged(() => HandlingShrinkQuantity);
				RaisePropertyChanged(() => HandlingShrinkQuantityDisplayText);
			}
		}

		private decimal _totalShrinkPercentage = 0;
		[Range(typeof(decimal), "-1", "1", ErrorMessageResourceName = "ErrorMessage_TotalShrinkPercentage_Text", ErrorMessageResourceType = typeof(Strings))]
		public decimal TotalShrinkPercentage {
			get { return _totalShrinkPercentage; }
			private set {
				_totalShrinkPercentage = value;
				RaisePropertyChanged(() => TotalShrinkPercentage);
			}
		}

		private decimal _totalShrinkQuantity = 0;
		public decimal TotalShrinkQuantity {
			get { return _totalShrinkQuantity; }
			private set {
				_totalShrinkQuantity = value;
				RaisePropertyChanged(() => TotalShrinkQuantity);
				RaisePropertyChanged(() => TotalShrinkQuantityDisplayText);
			}
		}
		#endregion

		public void SetShrinkDefaults(decimal standardDryMoisturePercentage, decimal estimatedShrinkRatePercentage, decimal handlingShrinkTargetMoisturePercentage) {
			StandardDryMoisturePercentage = standardDryMoisturePercentage;
			EstimatedShrinkRatePercentage = estimatedShrinkRatePercentage;
			HandlingShrinkTargetMoisturePercentage = handlingShrinkTargetMoisturePercentage;

			recalculateValues();
		}

		public LoadQuantityInformation_v2 GetQuantityInfoV2ValueObject() =>
			new LoadQuantityInformation_v2(
				TruckGrossWeight,
				TruckTareWeight,
				TruckNetWeight,
				SelectedTruckWeightUnit,
				InitialMoisturePercentage,
				StandardDryMoisturePercentage,
				StandardMoistureShrinkPercentage,
				StandardMoistureShrinkQuantity,
				EstimatedShrinkRatePercentage,
				HandlingShrinkTargetMoisturePercentage,
				HandlingShrinkPercentage,
				HandlingShrinkQuantity,
				AdjustmentShrinkPercentage,
				AdjustmentShrinkQuantity,
				LockedAdjustmentShrink,
				FinalQuantity,
				SelectedFinalQuantityUnit
			);

		private void recalculateValues(string callingPropertyName = null) {
			if (isCalculating) { return; }

			isCalculating = true;

			if (callingPropertyName == nameof(DeliveredQuantity) || callingPropertyName == nameof(SelectedFinalQuantityUnit)) {
				IsTruckGrossWeightLockedIn = false;

				if (CompositeUnitConverter.CanConvert(SelectedFinalQuantityUnit, SelectedTruckWeightUnit)) {
					TruckNetWeight = CompositeUnitConverter.ConvertValue(DeliveredQuantity, SelectedFinalQuantityUnit, SelectedTruckWeightUnit);
				} else {
					TruckNetWeight = 0;
				}


				TruckGrossWeight = TruckTareWeight + TruckNetWeight;
			} else if (callingPropertyName == nameof(TruckGrossWeight)) {
				IsTruckGrossWeightLockedIn = true;
				TruckNetWeight = TruckGrossWeight - TruckTareWeight;
			} else if (callingPropertyName == nameof(TruckTareWeight)) {
				if (IsTruckGrossWeightLockedIn) { IsTruckGrossWeightLockedIn = TruckGrossWeight != 0; }

				if (IsTruckGrossWeightLockedIn) {
					TruckNetWeight = TruckGrossWeight - TruckTareWeight;
				} else {
					TruckGrossWeight = TruckTareWeight + TruckNetWeight;
				}
			} else if (callingPropertyName == nameof(TruckNetWeight)) {
				IsTruckGrossWeightLockedIn = false;
				TruckGrossWeight = TruckTareWeight + TruckNetWeight;
			}

			if (CompositeUnitConverter.CanConvert(SelectedTruckWeightUnit, SelectedFinalQuantityUnit)) {
				DeliveredQuantity = CompositeUnitConverter.ConvertValue(TruckNetWeight, SelectedTruckWeightUnit, SelectedFinalQuantityUnit);
			}

			TruckUnitPerSaleUnit = TruckNetWeight != 0 ? DeliveredQuantity / TruckNetWeight : 0;

			StandardMoistureShrinkQuantity = 0;
			StandardMoistureShrinkPercentage = 0;
			HandlingShrinkQuantity = 0;
			HandlingShrinkPercentage = 0;

			if (InitialMoisturePercentage != 0 && DeliveredQuantity != 0) {
				var shouldShrinkToStandardTarget = StandardDryMoisturePercentage > 0
					&& StandardDryMoisturePercentage < InitialMoisturePercentage
					&& StandardDryMoisturePercentage >= HandlingShrinkTargetMoisturePercentage;

				if (shouldShrinkToStandardTarget) {
					// http://www.caes.uga.edu/departments/bae/extension/pubs/documents/shrinakage%20uga.pdf
					var dryWeight = DeliveredQuantity * ((1 - InitialMoisturePercentage) / (1 - StandardDryMoisturePercentage));

					StandardMoistureShrinkQuantity = DeliveredQuantity - dryWeight;
					StandardMoistureShrinkPercentage = (StandardMoistureShrinkQuantity / DeliveredQuantity);
				}

				var shouldShrinkToHandlingTarget = EstimatedShrinkRatePercentage > 0
					&& HandlingShrinkTargetMoisturePercentage > 0
					&& HandlingShrinkTargetMoisturePercentage < InitialMoisturePercentage;

				if (shouldShrinkToHandlingTarget) {
					var moisturePercentagePointsLost = (InitialMoisturePercentage - HandlingShrinkTargetMoisturePercentage) * 100m;
					var handlingShrinkPercentage = (moisturePercentagePointsLost * (EstimatedShrinkRatePercentage * 100m)) / 100m;

					if (handlingShrinkPercentage > StandardMoistureShrinkPercentage) {
						HandlingShrinkQuantity = (DeliveredQuantity * handlingShrinkPercentage) - StandardMoistureShrinkQuantity;
						HandlingShrinkPercentage = HandlingShrinkQuantity / DeliveredQuantity;
					}
				}
			}

			if (callingPropertyName == nameof(AdjustmentShrinkPercentage)) {
				LockedAdjustmentShrink = LoadShrinkLockTypes.Percentage;
			} else if (callingPropertyName == nameof(AdjustmentShrinkQuantity)) {
				LockedAdjustmentShrink = LoadShrinkLockTypes.Value;
			}

			switch (LockedAdjustmentShrink) {
				case LoadShrinkLockTypes.None:
				case LoadShrinkLockTypes.Formula:
					break;
				case LoadShrinkLockTypes.Percentage:
					AdjustmentShrinkQuantity = DeliveredQuantity * AdjustmentShrinkPercentage;
					break;
				case LoadShrinkLockTypes.Value:
					AdjustmentShrinkPercentage = AdjustmentShrinkQuantity / DeliveredQuantity;
					break;
				default:
					break;
			}

			if (AdjustmentShrinkPercentage == 0) { LockedAdjustmentShrink = LoadShrinkLockTypes.None; }

			TotalShrinkPercentage = StandardMoistureShrinkPercentage + HandlingShrinkPercentage + AdjustmentShrinkPercentage;
			TotalShrinkQuantity = StandardMoistureShrinkQuantity + HandlingShrinkQuantity + AdjustmentShrinkQuantity;

			FinalQuantity = DeliveredQuantity - TotalShrinkQuantity;

			isCalculating = false;

			finalQuantityChanged?.Invoke();

			ValidateViewModel();
		}
	}
}