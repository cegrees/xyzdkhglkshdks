﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Landdb.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel {
    public class ApplicationUpdateStatusViewModel : ViewModelBase, IApplicationUpdateStatus {

        public ApplicationUpdateStatusViewModel() {
            RestartAppCommand = new RelayCommand(() => App.RestartApplication());
        }

        public RelayCommand RestartAppCommand { get; private set; }

        bool isUpdating = false;
		public bool IsUpdating {
            get { return isUpdating; }
            set {
                isUpdating = value;
                RaisePropertyChanged(nameof(IsUpdating));
            }
        }

        bool isUpdateComplete;
        public bool IsUpdateComplete {
            get { return isUpdateComplete; }
            set {
                isUpdateComplete = value;
                RaisePropertyChanged(nameof(IsUpdateComplete));
            }
        }

        bool hasErrors;
        public bool HasErrors {
            get { return hasErrors; }
            set {
                hasErrors = value;
                RaisePropertyChanged(nameof(HasErrors));
            }
        }

		string statusText;
		public string StatusText {
            get { return statusText; }
            set {
                statusText = value;
                RaisePropertyChanged(nameof(StatusText));
            }
        }

		double maxProgress = 100;
		public double MaxProgress {
            get { return maxProgress; }
            set {
                maxProgress = value;
                RaisePropertyChanged(nameof(MaxProgress));
            }
        }

		double _currentProgress = 0;
		public double CurrentProgress {
            get { return _currentProgress; }
            set {
                _currentProgress = value;
                RaisePropertyChanged(nameof(CurrentProgress));
            }
        }

        bool? isUpdateAvailable = null;
        public bool? IsUpdateAvailable {
            get { return isUpdateAvailable; }
            set {
                isUpdateAvailable = value;
                RaisePropertyChanged(nameof(IsUpdateAvailable));
            }
        }

        Version availableVersion;
        public Version AvailableVersion {
            get { return availableVersion; }
            set {
                availableVersion = value;
                RaisePropertyChanged(nameof(AvailableVersion));
            }
        }
    }
}
