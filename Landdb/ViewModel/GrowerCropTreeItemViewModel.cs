﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Landdb.Domain.ReadModels.Tree;
using Landdb.ViewModel.Fields;

namespace Landdb.ViewModel {
    public class GrowerCropTreeItemViewModel : AbstractTreeItemViewModel {
        private readonly string name;

        public GrowerCropTreeItemViewModel(string name, IList<TreeViewCropItem> crops, bool sortChildrenAlphabetically, Action<AbstractTreeItemViewModel> onCheckedChanged = null, Action<List<CropZoneId>> onParentCompleted = null, Predicate<object> filter = null)
            : base(null, onCheckedChanged, onParentCompleted, filter) {
            this.name = name;

            var q = from c in crops
                    select new CropTreeItemViewModel(c, this, sortChildrenAlphabetically, onCheckedChanged, onParentCompleted, filter); // and the horse it rode in on...

			// always sort the list of parent crops regardless of the value of sortChildrenAlphabetically
            q = q.OrderBy(x => x.Name);

            Children = new ObservableCollection<AbstractTreeItemViewModel>(q);
        }
            
        public string Name { get { return name; } }

        public override IIdentity Id {
            get { return null; } // TODO: Maybe return DataSourceId?
        }
    }
}
