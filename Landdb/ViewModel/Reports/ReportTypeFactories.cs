﻿using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Secondary.Reports.AppliedProduct;
using Landdb.ViewModel.Secondary.Reports.AppliedProduct.Generators;
using Landdb.ViewModel.Secondary.Reports.Crop_History;
using Landdb.ViewModel.Secondary.Reports.CropZone;
using Landdb.ViewModel.Secondary.Reports.Document;
using Landdb.ViewModel.Secondary.Reports.ExcelDump;
using Landdb.ViewModel.Secondary.Reports.FSA;
using Landdb.ViewModel.Secondary.Reports.Inventory;
using Landdb.ViewModel.Secondary.Reports.Plan;
using Landdb.ViewModel.Secondary.Reports.Processor;
using Landdb.ViewModel.Secondary.Reports.Specialty;
using Landdb.ViewModel.Secondary.Reports.Yield;
using Landdb.ViewModel.Secondary.Reports.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.ViewModel.Reports {
    public interface IReportTypeFactory {
        string ReportTypeDisplay { get; }
        void LaunchReportCreator();
        ReportTemplateViewModel SelectedTemplate { get; set; }
    }

    public class AppliedProductReportFactory : IReportTypeFactory {
        IClientEndpoint clientEndpoint;
        Dispatcher dispatcher;
        ReportTemplateViewModel selectedTemplate;
        public AppliedProductReportFactory(IClientEndpoint clientEndpoint, Dispatcher dispatcher) {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
        }

        public string ReportTypeDisplay {
            get { return Strings.ReportTitle_AppliedProducts; }
        }

        public void LaunchReportCreator() {
            var vm = new AppliedProductReportCreatorViewModel(clientEndpoint, dispatcher, ApplicationEnvironment.CurrentCropYear, SelectedTemplate, ReportTypeDisplay);
            ScreenDescriptor descriptor = new ScreenDescriptor("Landdb.Views.Secondary.Reports.AppliedProduct.AppliedProductReportCreatorView", "create applied product report", vm);
            Messenger.Default.Send<ChangeScreenDescriptorMessage>(new ChangeScreenDescriptorMessage() { ScreenDescriptor = descriptor });
        }

        public ReportTemplateViewModel SelectedTemplate
        {
            get
            {
                return selectedTemplate;
            }
            set
            {
                selectedTemplate = value;
            }
        }
    }

    public class ProcessorReportFactory : IReportTypeFactory
    {
        IClientEndpoint clientEndpoint;
        Dispatcher dispatcher;
        ReportTemplateViewModel selectedTemplate;
        public ProcessorReportFactory(IClientEndpoint clientEndpoint, Dispatcher dispatcher)
        {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
        }

        public string ReportTypeDisplay
        {
            get { return Strings.ReportTitle_Processor; }
        }

        public void LaunchReportCreator()
        {
            var vm = new ProcessorReportCreatorViewModel(clientEndpoint, dispatcher, ApplicationEnvironment.CurrentCropYear, SelectedTemplate, ReportTypeDisplay);
            ScreenDescriptor descriptor = new ScreenDescriptor("Landdb.Views.Secondary.Reports.Processor.ProcessorReportCreatorView", "create processor report", vm);
            Messenger.Default.Send<ChangeScreenDescriptorMessage>(new ChangeScreenDescriptorMessage() { ScreenDescriptor = descriptor });
        }

        public ReportTemplateViewModel SelectedTemplate
        {
            get
            {
                return selectedTemplate;
            }
            set
            {
                selectedTemplate = value;
            }
        }
    }

    public class InventoryReportFactory : IReportTypeFactory {
        IClientEndpoint clientEndpoint;
        Dispatcher dispatcher;
        ReportTemplateViewModel selectedTemplate;
        public InventoryReportFactory(IClientEndpoint clientEndpoint, Dispatcher dispatcher) {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
        }

        public string ReportTypeDisplay {
            get { return Strings.ReportTitle_Inventory; }
        }

        public void LaunchReportCreator() {
            var vm = new InventoryReportCreatorViewModel(clientEndpoint, ApplicationEnvironment.CurrentCropYear, SelectedTemplate, ReportTypeDisplay);
            ScreenDescriptor descriptor = new ScreenDescriptor("Landdb.Views.Secondary.Reports.Inventory.InventoryReportCreatorView", "create inventory report", vm);
            Messenger.Default.Send<ChangeScreenDescriptorMessage>(new ChangeScreenDescriptorMessage() { ScreenDescriptor = descriptor });
        }

        public ReportTemplateViewModel SelectedTemplate
        {
            get
            {
                return selectedTemplate;
            }
            set
            {
                selectedTemplate = value;
            }
        }
    }

    public class PlanReportFactory : IReportTypeFactory
    {

        IClientEndpoint clientEndpoint;
        Dispatcher dispatcher;
        ReportTemplateViewModel selectedTemplate;
        public PlanReportFactory(IClientEndpoint clientEndpoint, Dispatcher dispatcher)
        {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
        }

        public string ReportTypeDisplay
        {
            get { return Strings.ReportTitle_Plans; }
        }

        public void LaunchReportCreator()
        {
            //var vm = new CropZoneReportCreatorViewModel(clientEndpoint, dispatcher, ApplicationEnvironment.CurrentCropYear, SelectedTemplate, ReportTypeDisplay);
            //ScreenDescriptor descriptor = new ScreenDescriptor("Landdb.Views.Secondary.Reports.Field.CropZoneReportCreatorView", "create crop zone report", vm);
            //Messenger.Default.Send<ChangeScreenDescriptorMessage>(new ChangeScreenDescriptorMessage() { ScreenDescriptor = descriptor });

            var vm = new PlanReportCreatorViewModel(clientEndpoint, dispatcher, ApplicationEnvironment.CurrentCropYear, SelectedTemplate, ReportTypeDisplay);
            ScreenDescriptor descriptor = new ScreenDescriptor("Landdb.Views.Secondary.Reports.Plan.PlanReportCreatorView", "create plan report", vm);
            Messenger.Default.Send<ChangeScreenDescriptorMessage>(new ChangeScreenDescriptorMessage() { ScreenDescriptor = descriptor });
        }

        public ReportTemplateViewModel SelectedTemplate
        {
            get
            {
                return selectedTemplate;
            }
            set
            {
                selectedTemplate = value;
            }
        }
    }

    public class DocumentReportFactory : IReportTypeFactory
    {

        IClientEndpoint clientEndpoint;
        Dispatcher dispatcher;
        ReportTemplateViewModel selectedTemplate;
        public DocumentReportFactory(IClientEndpoint clientEndpoint, Dispatcher dispatcher)
        {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
        }

        public string ReportTypeDisplay
        {
            get { return Strings.ReportTitle_MultipleDocuments; }
        }

        public void LaunchReportCreator()
        {
            //var vm = new CropZoneReportCreatorViewModel(clientEndpoint, dispatcher, ApplicationEnvironment.CurrentCropYear, SelectedTemplate, ReportTypeDisplay);
            //ScreenDescriptor descriptor = new ScreenDescriptor("Landdb.Views.Secondary.Reports.Field.CropZoneReportCreatorView", "create crop zone report", vm);
            //Messenger.Default.Send<ChangeScreenDescriptorMessage>(new ChangeScreenDescriptorMessage() { ScreenDescriptor = descriptor });

            var vm = new DocumentReportCreatorViewModel(clientEndpoint, dispatcher, ApplicationEnvironment.CurrentCropYear, SelectedTemplate, ReportTypeDisplay);
            ScreenDescriptor descriptor = new ScreenDescriptor("Landdb.Views.Secondary.Reports.Work_Order.DocumentReportCreatorView", "create document report", vm);
            Messenger.Default.Send<ChangeScreenDescriptorMessage>(new ChangeScreenDescriptorMessage() { ScreenDescriptor = descriptor });
        }

        public ReportTemplateViewModel SelectedTemplate
        {
            get
            {
                return selectedTemplate;
            }
            set
            {
                selectedTemplate = value;
            }
        }
    }

    public class FieldReportFactory : IReportTypeFactory {
        IClientEndpoint clientEndpoint;
        Dispatcher dispatcher;
        ReportTemplateViewModel selectedTemplate;
        public FieldReportFactory(IClientEndpoint clientEndpoint, Dispatcher dispatcher) {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
        }

        public string ReportTypeDisplay {
            get { return Strings.ReportTitle_Fields; }
        }

        public void LaunchReportCreator() {
            var vm = new CropZoneReportCreatorViewModel(clientEndpoint, dispatcher, ApplicationEnvironment.CurrentCropYear, SelectedTemplate, ReportTypeDisplay);
            ScreenDescriptor descriptor = new ScreenDescriptor("Landdb.Views.Secondary.Reports.Field.CropZoneReportCreatorView", "create crop zone report", vm);
            Messenger.Default.Send<ChangeScreenDescriptorMessage>(new ChangeScreenDescriptorMessage() { ScreenDescriptor = descriptor });
        }

        public ReportTemplateViewModel SelectedTemplate
        {
            get
            {
                return selectedTemplate;
            }
            set
            {
                selectedTemplate = value;
            }
        }
    }

    public class ExcelReportFactory : IReportTypeFactory
    {
        IClientEndpoint clientEndpoint;
        Dispatcher dispatcher;
        ReportTemplateViewModel selectedTemplate;

        public ExcelReportFactory(IClientEndpoint clientEndpoint, Dispatcher dispatcher)
        {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
        }

        public string ReportTypeDisplay { 
            get { return Strings.ReportTitle_Excel; }
        }

        public void LaunchReportCreator()
        {
            //call up the excel dump creator....
            var vm = new AppliedProductExcelDumpCreatorViewModel(clientEndpoint, dispatcher, ApplicationEnvironment.CurrentCropYear, SelectedTemplate, ReportTypeDisplay);
            ScreenDescriptor descriptor = new ScreenDescriptor("Landdb.Views.Secondary.Reports.Excel.ExcelReportCreatorView", "create applied product report", vm);
            Messenger.Default.Send<ChangeScreenDescriptorMessage>(new ChangeScreenDescriptorMessage() { ScreenDescriptor = descriptor });
        }

        public ReportTemplateViewModel SelectedTemplate
        {
            get
            {
                return selectedTemplate;
            }
            set
            {
                selectedTemplate = value;
            }
        }
    }

    public class SpecialtyReportFactory : IReportTypeFactory
    {
        IClientEndpoint clientEndpoint;
        Dispatcher dispatcher;
        ReportTemplateViewModel selectedTemplate;

        public SpecialtyReportFactory(IClientEndpoint clientEndpoint, Dispatcher dispatcher)
        {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
        }

        public string ReportTypeDisplay
        {
            get { return Strings.ReportTitle_Speciality; }
        }

        public void LaunchReportCreator()
        {
            var vm = new SpecialtyReportCreatorViewModel(clientEndpoint, dispatcher, ApplicationEnvironment.CurrentCropYear, SelectedTemplate, ReportTypeDisplay);
            ScreenDescriptor descriptor = new ScreenDescriptor("Landdb.Views.Secondary.Reports.Specialty.SpecialtyReportCreatorView", "create specialty report", vm);
            //ScreenDescriptor descriptor = new ScreenDescriptor("Landdb.Views.Secondary.Reports.Specialty.SpecialtyReportSelectionView", "createView", vm);
            Messenger.Default.Send<ChangeScreenDescriptorMessage>(new ChangeScreenDescriptorMessage() { ScreenDescriptor = descriptor });
        }

        public ReportTemplateViewModel SelectedTemplate
        {
            get
            {
                return selectedTemplate;
            }
            set
            {
                selectedTemplate = value;
            }
        }
    }

    public class FSAReportFactory : IReportTypeFactory
    {
        IClientEndpoint clientEndpoint;
        Dispatcher dispatcher;
        ReportTemplateViewModel selectedTemplate;

        public FSAReportFactory(IClientEndpoint clientEndpoint, Dispatcher dispatcher)
        {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
        }

        public string ReportTypeDisplay
        {
            get { return Strings.ReportTitle_FSA; }
        }

        public void LaunchReportCreator()
        {
            var vm = new FSAReportCreatorViewModel(clientEndpoint, dispatcher, ApplicationEnvironment.CurrentCropYear, SelectedTemplate, ReportTypeDisplay);
            ScreenDescriptor descriptor = new ScreenDescriptor("Landdb.Views.Secondary.Reports.FSA.FSAReportCreatorView", "create fsa report", vm);
            Messenger.Default.Send<ChangeScreenDescriptorMessage>(new ChangeScreenDescriptorMessage() { ScreenDescriptor = descriptor });
        }

        public ReportTemplateViewModel SelectedTemplate
        {
            get
            {
                return selectedTemplate;
            }
            set
            {
                selectedTemplate = value;
            }
        }
    }

    public class YieldReportFactory : IReportTypeFactory
    {
        IClientEndpoint clientEndpoint;
        Dispatcher dispatcher;
        ReportTemplateViewModel selectedTemplate;

        public YieldReportFactory(IClientEndpoint clientEndpoint, Dispatcher dispatcher)
        {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
        }

        public string ReportTypeDisplay
        {
            get { return Strings.ReportTitle_YieldAndProfitability; }
        }

        public void LaunchReportCreator()
        {
            var vm = new YieldReportCreatorViewModel(clientEndpoint, dispatcher, ApplicationEnvironment.CurrentCropYear, SelectedTemplate, ReportTypeDisplay);
            ScreenDescriptor descriptor = new ScreenDescriptor("Landdb.Views.Secondary.Reports.Yield.YieldReportCreatorView", "create yield report", vm);
            Messenger.Default.Send<ChangeScreenDescriptorMessage>(new ChangeScreenDescriptorMessage() { ScreenDescriptor = descriptor });
        }

        public ReportTemplateViewModel SelectedTemplate
        {
            get
            {
                return selectedTemplate;
            }
            set
            {
                selectedTemplate = value;
            }
        }
    }

    public class CropHistoryReportFactory : IReportTypeFactory
    {
        IClientEndpoint clientEndpoint;
        Dispatcher dispatcher;
        ReportTemplateViewModel selectedTemplate;
        public CropHistoryReportFactory(IClientEndpoint clientEndpoint, Dispatcher dispatcher)
        {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
        }

        public string ReportTypeDisplay
        {
            get { return Strings.ReportTitle_CropHistory; }
        }

        public void LaunchReportCreator()
        {
            var vm = new CropHistoryReportCreatorViewModel(clientEndpoint, dispatcher, SelectedTemplate, ReportTypeDisplay);
            ScreenDescriptor descriptor = new ScreenDescriptor("Landdb.Views.Secondary.Reports.Crop_History.CropHistoryReportCreatorView", "create applied product report", vm);
            Messenger.Default.Send<ChangeScreenDescriptorMessage>(new ChangeScreenDescriptorMessage() { ScreenDescriptor = descriptor });
        }

        public ReportTemplateViewModel SelectedTemplate
        {
            get
            {
                return selectedTemplate;
            }
            set
            {
                selectedTemplate = value;
            }
        }
    }
    public class FormsReportFactory : IReportTypeFactory
    {
        IClientEndpoint clientEndpoint;
        Dispatcher dispatcher;
        ReportTemplateViewModel selectedTemplate;
        public FormsReportFactory(IClientEndpoint clientEndpoint, Dispatcher dispatcher)
        {
            this.clientEndpoint = clientEndpoint;
            this.dispatcher = dispatcher;
        }

        public string ReportTypeDisplay
        {
            get { return Strings.ReportTitle_Forms; }
        }

        public void LaunchReportCreator()
        {
            var vm = new FormsReportCreatorViewModel(clientEndpoint, dispatcher, ApplicationEnvironment.CurrentCropYear, SelectedTemplate, ReportTypeDisplay);
            ScreenDescriptor descriptor = new ScreenDescriptor("Landdb.Views.Secondary.Reports.Forms.FormsReportCreatorView", "create forms report", vm);
            Messenger.Default.Send<ChangeScreenDescriptorMessage>(new ChangeScreenDescriptorMessage() { ScreenDescriptor = descriptor });
        }

        public ReportTemplateViewModel SelectedTemplate
        {
            get
            {
                return selectedTemplate;
            }
            set
            {
                selectedTemplate = value;
            }
        }
    }
}
