﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Converters.Reports;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Input;
using System.Windows.Threading;
using NLog;

namespace Landdb.ViewModel.Reports {
    public class ReportSelectionPageViewModel : ViewModelBase {
        IReportTypeFactory selectedReportFactory;
        ReportTemplateViewModel selectedTemplate;
        IClientEndpoint endPoint;
        Dispatcher dispatcher;
        Timer timer;

        public ReportSelectionPageViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher) {
            ReportTemplates = new List<ReportTemplateViewModel>();
            AssociatedReportTemplates = new List<ReportTemplateViewModel>();
            DeserializeReportTemplates();
            this.endPoint = clientEndpoint;
            this.dispatcher = dispatcher;

            LaunchReportCreator = new RelayCommand(LaunchReport);
            LaunchSelectedTemplate = new RelayCommand<ReportTemplateViewModel>(LaunchTemplate);

            Messenger.Default.Register<PropertyChangedMessage<ReportTemplateViewModel>>(this, ReportTemplatesRefresh);
            Messenger.Default.Register<PropertyChangedMessage<ReportTemplateViewModel>>(this, ReportTemplateRemoved);
            Messenger.Default.Register<DataSourceChangedMessage>(this, OnDataSourceChanged);
            //ReportFactories = AddCulturallyRelevantReports();
            //RaisePropertyChanged(() => ReportFactories);
            timer = new Timer(1000);
            timer.Elapsed += timer_Elapsed;
        }

        public List<ReportTemplateViewModel> ReportTemplates { get; set; }
        public List<ReportTemplateViewModel> AssociatedReportTemplates { get; set; }
        public ReportTemplateViewModel SelectedTemplate 
        {
            get { return selectedTemplate; }

            set
            {
                selectedTemplate = value;
                SelectedReportFactory.SelectedTemplate = selectedTemplate;
                if (selectedTemplate != null) 
                { 
                    SelectedReportFactory.LaunchReportCreator();
                    RaisePropertyChanged("SelectedTemplate");
                }
            }
        }

        public ICommand LaunchReportCreator { get; private set; }
        public ICommand LaunchSelectedTemplate { get; set; }
        public ObservableCollection<IReportTypeFactory> ReportFactories { get; private set; }
        public IReportTypeFactory SelectedReportFactory {
            get { return selectedReportFactory; }
            set {
                selectedReportFactory = value;

                if (ReportTemplates != null && ReportTemplates.Count() > 0 && selectedReportFactory != null)
                {
                    AssociatedReportTemplates = ReportTemplates.Where(t => t.ReportType == selectedReportFactory.ReportTypeDisplay && t.DataSourceId == ApplicationEnvironment.CurrentDataSourceId).OrderByDescending(d => d.LastUsed).ToList();
                    RaisePropertyChanged("AssociatedReportTemplates");
                }

                RaisePropertyChanged("SelectedReportFactory");
            }
        }
        
        void LaunchReport()
        {
            if (SelectedReportFactory != null)
            {
                SelectedTemplate = null;
                string selectedreportstring = SelectedReportFactory.ReportTypeDisplay;
                var properties = new Dictionary<string, string> { { "Item Type", "Report" }, { "Item Name", selectedreportstring }, { "Action", "Touched" } };
                var metrics = new Dictionary<string, double> { };
                App.CurrentApp.Telemetry.TrackEvent(selectedreportstring, properties, metrics);
                SelectedReportFactory.LaunchReportCreator();
            }
        }

        void LaunchTemplate(ReportTemplateViewModel rtVM )
        {
            SelectedTemplate = rtVM;
        }

        void ReportTemplateRemoved(PropertyChangedMessage<ReportTemplateViewModel> template)
        {
            if (template.PropertyName == "ReportTemplateRemoved")
            {  
                DeserializeReportTemplates();

                RaisePropertyChanged(() => ReportTemplates);

                if (ReportTemplates != null && ReportTemplates.Count() > 0)
                {
                    AssociatedReportTemplates = ReportTemplates.Where(t => t.ReportType == selectedReportFactory.ReportTypeDisplay && t.DataSourceId == ApplicationEnvironment.CurrentDataSourceId).OrderByDescending(d => d.LastUsed).ToList();
                    RaisePropertyChanged("AssociatedReportTemplates");
                }
                else
                {
                    AssociatedReportTemplates = new List<ReportTemplateViewModel>();
                    RaisePropertyChanged("AssociatedReportTemplates");
                }
            }
        }

        void ReportTemplatesRefresh(PropertyChangedMessage<ReportTemplateViewModel> template)
        {
            if (template.PropertyName == "NewTemplateCreated")
            {
                DeserializeReportTemplates();

                var sd = new ScreenDescriptor("Landdb.Views.Reports.TemplateSavedView", "templateSaved", null);
                Messenger.Default.Send(new ShowPopupMessage() { ScreenDescriptor = sd });


                if (ReportTemplates != null && ReportTemplates.Count() > 0)
                {
                    AssociatedReportTemplates = ReportTemplates.Where(t => t.ReportType == selectedReportFactory.ReportTypeDisplay && t.DataSourceId == ApplicationEnvironment.CurrentDataSourceId).OrderByDescending(d => d.LastUsed).ToList();
                    RaisePropertyChanged("AssociatedReportTemplates");
                }

                timer.Start();
            }
        }

        private void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Messenger.Default.Send(new HidePopupMessage());
        }

        private ObservableCollection<IReportTypeFactory> AddCulturallyRelevantReports() {
            string thisCulture = ApplicationEnvironment.CurrentDataSource.DatasourceCulture.ToLower();

            ObservableCollection <IReportTypeFactory> reportList = new ObservableCollection<IReportTypeFactory>();
            if (ApplicationEnvironment.CurrentDataSource == null)
            {
                reportList = new ObservableCollection<IReportTypeFactory>();
            }
            else if (thisCulture == "en-us")
            {
                reportList = new ObservableCollection<IReportTypeFactory>() { 
                new AppliedProductReportFactory(endPoint, dispatcher), 
                new CropHistoryReportFactory(endPoint, dispatcher),
                new ExcelReportFactory(endPoint, dispatcher),
                new FieldReportFactory(endPoint, dispatcher),
                new FSAReportFactory(endPoint, dispatcher),
                new InventoryReportFactory(endPoint, dispatcher), 
                new PlanReportFactory(endPoint, dispatcher),
                new DocumentReportFactory(endPoint, dispatcher),
                new SpecialtyReportFactory(endPoint, dispatcher),
                new YieldReportFactory(endPoint, dispatcher),
                new ProcessorReportFactory(endPoint, dispatcher),
                new FormsReportFactory(endPoint,dispatcher),
            };
            }
            else if (thisCulture == "en-ca") {
                reportList = new ObservableCollection<IReportTypeFactory>() {
                    new AppliedProductReportFactory(endPoint, dispatcher),
                    new CropHistoryReportFactory(endPoint, dispatcher),
                    new ExcelReportFactory(endPoint, dispatcher),
                    new FieldReportFactory(endPoint, dispatcher),
                    new InventoryReportFactory(endPoint, dispatcher),
                    new PlanReportFactory(endPoint, dispatcher),
                    new DocumentReportFactory(endPoint, dispatcher),
                    new YieldReportFactory(endPoint, dispatcher),
                };
            } else {
                reportList = new ObservableCollection<IReportTypeFactory>() {
                    new ExcelReportFactory(endPoint, dispatcher),
                    new YieldReportFactory(endPoint, dispatcher)
                };
            }

            return reportList;
        }

        void OnDataSourceChanged(DataSourceChangedMessage message)
        {
            ReportFactories = AddCulturallyRelevantReports();
            RaisePropertyChanged(() => ReportFactories);
        }

        void DeserializeReportTemplates()
        {
            try
            {
                ReportTemplates.Clear();
                //Deserialize all the report templates and add them to the list....


                var filePath = Path.Combine(ApplicationEnvironment.TemplateDirectory, "report_templates.txt");

                if (!Directory.Exists(ApplicationEnvironment.TemplateDirectory)) { Directory.CreateDirectory(ApplicationEnvironment.TemplateDirectory); }
                if (!File.Exists(filePath)) { File.Create(filePath); }

                var data = File.ReadLines(filePath);

                foreach (var line in data)
                {
                    ReportTemplateViewModel template = JsonConvert.DeserializeObject<ReportTemplateViewModel>(line);
                    ReportTemplates.Add(template);
                }
            }
            catch (Exception ex) 
            {
            }
        }
    }
}
