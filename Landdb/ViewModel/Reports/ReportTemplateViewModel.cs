﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Landdb.Converters.Reports;
using System;
using System.Windows.Input;

namespace Landdb.ViewModel.Reports
{
    public class ReportTemplateViewModel : ViewModelBase
    {
        public ReportTemplateViewModel()
        {
            DeleteTemplateCommand = new RelayCommand(DeleteTemplate);
        }

        public ICommand DeleteTemplateCommand { get; set; }
        public Guid DataSourceId { get; set; }
        public string ReportType { get; set; }
        public string TemplateName { get; set; }
        public string CriteriaJSON { get; set; }
        public DateTime LastUsed { get; set; }
        public DateTime Created { get; set; }

	    private void DeleteTemplate()
        {
            ReportTemplateViewModelToFile templateService = new ReportTemplateViewModelToFile();
            templateService.RemoveTemplate(this);

            RaisePropertyChanged("ReportTemplateRemoved", null, this, true);
        }
    }
}
