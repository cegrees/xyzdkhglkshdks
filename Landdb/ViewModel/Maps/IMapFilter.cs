﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Landdb.Client.Infrastructure;
using Landdb.Client.Infrastructure.DisplayItems;
using ThinkGeo.MapSuite.Core;

namespace Landdb.ViewModel.Maps {
    public interface IMapFilter {
        string Name { get; }
        FilterType Filter { get; }
        ObservableCollection<ColorItem> ColorItems { get; set; }
        Dictionary<string, Color> CropzoneColors { get; set; }
        IList<Color> UsedColors { get; }
        Dictionary<string, Color> Execute(IClientEndpoint clientEndpoint, Random rand);
        //void FilterFarm(InMemoryFeatureLayer fieldsLayer, string selectedFarm);
        LegendAdornmentLayer BuildLegend(InMemoryFeatureLayer fieldsLayer, CropMapsFarmDisplayItem selectedFarm, bool isLegendVisible, Dictionary<string, string> unselectedFeatures);
        void OnColorUpdated();
    }

    public enum FilterType {
        ActiveIngredients,
        Blank,
        CropFieldFSA,
        Crop,
        CropProtection,
        Fertilizer,
        FieldFsa,
        ProductionContract,
        ReiPhi,
        RentContract,
        SeedVariety,
        Services,
        SSurgoItem,
        SSurgoLayers,
        Tag,
        TagOrdered,
        TagVariety,
        TraceGroup,
        Variety,
    }
}
