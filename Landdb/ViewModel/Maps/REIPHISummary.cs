﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using GalaSoft.MvvmLight;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Infrastructure;
using Landdb.Domain.ReadModels.Inventory;
using AgC.UnitConversion;
using AgC.UnitConversion.MassAndWeight;
using AgC.UnitConversion.Area;
using AgC.UnitConversion.Volume;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using Landdb.Domain.ReadModels.ActiveIngredientSetting;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Fields;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Resources;

namespace Landdb.ViewModel.Maps {
	public class REIPHISummary : ViewModelBase {
		readonly IClientEndpoint clientEndpoint;
		//readonly Dispatcher dispatcher;
		List<SummaryModel> cropzonesAnalysis;

		public REIPHISummary(IClientEndpoint clientEndpoint) {
			this.clientEndpoint = clientEndpoint;
			//this.dispatcher = dispatcher;
		}

		public List<SummaryModel> CropzonesAnalysis {
			get { return cropzonesAnalysis; }
			set {
				cropzonesAnalysis = value;
				RaisePropertyChanged("CropzonesAnalysis");
			}
		}

		public List<SummaryModel> RefreshSummaryItems(List<TreeViewCropZoneItem> zones) {
			cropzonesAnalysis = new List<SummaryModel>();
			foreach (TreeViewCropZoneItem treeModel in zones) {
				SummaryModel model = new SummaryModel() { };
				var data = clientEndpoint.GetView<CropZoneApplicationDataView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear)).GetValue(new CropZoneApplicationDataView());
				IEnumerable<CropZoneApplicationDataItem> filtered = data.Items.Where(x => x.CropZoneId == treeModel.CropZoneId).OrderByDescending(x => x.StartDate);
				if (filtered != null) {
					var analysis = CalculateREIPHIStatus(filtered, treeModel.CropId);
					model = analysis;
					model.Status = analysis.Status;
					model.Id = treeModel.CropZoneId.Id;
					model.CropZoneIdString = treeModel.CropZoneId.Id.ToString();
					cropzonesAnalysis.Add(model);
				}
			}
			return cropzonesAnalysis;
		}

		private SummaryModel CalculateREIPHIStatus(IEnumerable<CropZoneApplicationDataItem> items, CropId cropId) {
			SummaryModel model = new SummaryModel() { };
			int highestvalue = 0;
			var ml = clientEndpoint.GetMasterlistService();
			string cropidstring = cropId.Id.ToString();
			string cropname = clientEndpoint.GetMasterlistService().GetCropDisplay(cropId);
			foreach (var item in items) {
				string productidstring = item.ProductId.ToString();
				var product = ml.GetProduct(item.ProductId);
				// TODO FIX HACK - handle unknown products - MH
				if (product == null) { continue; }
				string productname = product.Name;
				var reiphis = ml.GetProductReiPhis(item.ProductId);

				ReiPhi selectedreiphi = new ReiPhi();
				foreach (ReiPhi rp in reiphis) {
					CropId reiphicrop = new CropId(rp.Crop);
					if (reiphicrop == cropId) {
						selectedreiphi = rp;
						break;
					}
				}
				DateTime reentry = item.StartDate;
				if (selectedreiphi.Rei.HasValue && selectedreiphi.ReiU == Strings.Hour_Text.ToLower()) {
					reentry.AddHours(System.Convert.ToDouble(selectedreiphi.Rei));
				} else if (selectedreiphi.Rei.HasValue && string.IsNullOrWhiteSpace(selectedreiphi.ReiU)) {
					reentry.AddHours(System.Convert.ToDouble(selectedreiphi.Rei));
				} else if (selectedreiphi.Rei.HasValue && selectedreiphi.ReiU == Strings.Day_Text.ToLower()) {
					reentry.AddDays(System.Convert.ToDouble(selectedreiphi.Rei));
				}

				DateTime harvestinterval = item.StartDate;
				if (selectedreiphi.Phi.HasValue && selectedreiphi.PhiU == Strings.Day_Text.ToLower()) {
					harvestinterval.AddDays(System.Convert.ToDouble(selectedreiphi.Phi));
				} else if (selectedreiphi.Phi.HasValue && string.IsNullOrWhiteSpace(selectedreiphi.PhiU)) {
					harvestinterval.AddDays(System.Convert.ToDouble(selectedreiphi.Phi));
				}


				int status = 0;
				if (DateTime.Now >= harvestinterval) {
					status = 0;
				}
				if (DateTime.Now < harvestinterval && DateTime.Now >= reentry) {
					status = 1;
				}
				if (DateTime.Now < reentry) {
					status = 2;
				}
				if (status > highestvalue) {
					highestvalue = status;
				}

				TimeSpan timetoreentry = DateTime.Now - reentry;
				TimeSpan timetoharvest = DateTime.Now - harvestinterval;
				//TimeSpan testtime = new TimeSpan(0);
				//if(timetoharvest >= testtime) {
				//    status = 0;
				//}
				//if(timetoharvest < testtime && timetoreentry >= testtime) {
				//    status = 1;
				//}
				//if(timetoreentry < testtime) {
				//    status = 2;
				//}

				string cropzoneidstring = item.CropZoneId.Id.ToString();
				REIPHILineItem lineitem = new REIPHILineItem(cropzoneidstring, cropidstring, cropname, productidstring, productname, reentry, harvestinterval, timetoreentry, timetoharvest, status);
				model.ReiPhiItems.Add(lineitem);
				model.Status = highestvalue;
			}
			return model;
		}

	}

	public class SummaryModel {
		public SummaryModel() {
			ReiPhiItems = new List<REIPHILineItem>();
			Status = 0;
		}

		public Guid Id { get; set; }
		public string CropZoneIdString { get; set; }
		public List<REIPHILineItem> ReiPhiItems { get; set; }
		public int Status { get; set; }
	}

	public class REIPHILineItem {

		public REIPHILineItem(string cropzoneidstring, string cropidstring, string cropname, string productidstring, string productname, DateTime reentry, DateTime harvestinterval, TimeSpan timetoreentry, TimeSpan timetoharvest, int status) {
			this.CropzoneIDstring = cropzoneidstring;
			this.CropIDstring = cropidstring;
			this.CropName = cropname;
			this.ProductIDstring = productidstring;
			this.ProductName = productname;
			this.ReEntry = reentry;
			this.HarvestInverval = harvestinterval;
			this.TimeToReEntry = timetoreentry;
			this.TimeToHarvest = timetoharvest;
			this.Status = status;
		}

		public string CropzoneIDstring { get; set; }
		public string CropIDstring { get; set; }
		public string CropName { get; set; }
		public string ProductIDstring { get; set; }
		public string ProductName { get; set; }
		public DateTime ReEntry { get; set; }
		public DateTime HarvestInverval { get; set; }
		public TimeSpan TimeToReEntry { get; set; }
		public TimeSpan TimeToHarvest { get; set; }
		public int Status { get; set; }
	}
}
