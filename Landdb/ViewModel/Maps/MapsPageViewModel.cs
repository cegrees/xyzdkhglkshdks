﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Windows.Threading;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Map;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.MapStyles;
using Landdb.ViewModel.Fields.FieldDetails;
using NLog;
using ThinkGeo.MapSuite.Core;
using ThinkGeo.MapSuite.WpfDesktopEdition;
using Landdb.ViewModel.Maps.ImageryOverlay;
using AgC.UnitConversion;
using Landdb.ViewModel.Fields.FieldDetails.MapModels;
using Landdb.Domain.ReadModels.MapAnnotation;
using System.Windows;
using System.Windows.Media.Imaging;
using AgC.UnitConversion.Area;
using Landdb.ViewModel.Secondary.Options;
using Landdb.Client.Account;
using Landdb.ViewModel.Production.Popups;
using Landdb.ViewModel.Secondary.Map;
using System.Drawing;
using System.Windows.Media;
using System.IO;
using Landdb.Client.Infrastructure.DisplayItems;
using Landdb.Resources;

namespace Landdb.ViewModel.Maps {
    public class MapsPageViewModel : ViewModelBase {
        readonly IClientEndpoint clientEndpoint;
        readonly Dispatcher dispatcher;
        readonly Logger log = LogManager.GetCurrentClassLogger();
        readonly IDetailsViewModelFactory detailsFactory;

        IStyleFactory styleFactory;
        Dictionary<string, MapAnnotationCategory> userstylenames = new Dictionary<string, MapAnnotationCategory>();
        MapStyles.AnnotationMultiStyle customMultiStyles = new MapStyles.AnnotationMultiStyle();
        AnnotationScalingTextStyle scalingTextStyle2 = new AnnotationScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, 24, 6);
        List<AnnotationStyleItem> changedstyles = new List<AnnotationStyleItem>();
        MapSettings mapsettings;
        InMemoryFeatureLayer annotationLayer = new InMemoryFeatureLayer();
        int annotationopacity = 125;
        bool annotationvisibility = false;
        bool mapannotationlabelsvisible = false;
        bool displaymapzoombar = false;

        Guid currentDataSourceId;
        bool displaycropzones = true;
        //bool displaycropzones = false;
        bool processingfilter = false;
        bool datasoucrechanging = false;
        bool refreshimagery = false;
        int currentCropYear;

        //LayerOverlay ssurgolayersoverlay = new LayerOverlay();
        LayerOverlay layerOverlay = new LayerOverlay();
        InMemoryFeatureLayer fieldsLayer = new InMemoryFeatureLayer();
        InMemoryFeatureLayer zoomLayer = new InMemoryFeatureLayer();
        GeoCollection<Feature> fieldcollection = new GeoCollection<Feature>();
        Proj4Projection projection;
        long newticks = 0;
        long oldticks = 0;
        RectangleShape mapextent = new RectangleShape();
        SimpleMarkerOverlay simpleMarkerOverlay = new SimpleMarkerOverlay();
        ColoredMapGeneratorMultiAreaStyle cmgAreaStyle = null;
        ColoredMapGeneratorScalingTextStyle scalingTextStyle = new ColoredMapGeneratorScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, 24, 6);
        ScalingTextStyle scalingTextStyle4 = new ScalingTextStyle(GeographyUnit.DecimalDegree, @"Musym", 19000, 1200, 12, 4);
        SSurgoMultiAreaStyle cookiecutLayerAreaStyle = null;
        private Dictionary<string, double> rotationAngles = new Dictionary<string, double>();
        private Dictionary<string, PointShape> labelPositions = new Dictionary<string, PointShape>();
        IList<MapItem> clientLayerItems;
        IList<MapItem> cropzoneLayerItems;
        Dictionary<string, string> unselectedFeatures;
        Dictionary<string, string> chosenFeatures;
        Cursor defaultcursor = Cursors.Arrow;
        bool maploaded = false;
        bool startedOutOnline = false;
        bool repaintthescreen = false;
        bool isLoadingTree;
        RectangleShape extent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
        private Dictionary<string, AreaStyle> colorstyles = new Dictionary<string, AreaStyle>();
        private Dictionary<string, System.Drawing.Color> cropzonecolors = new Dictionary<string, System.Drawing.Color>();
        private Dictionary<string, ColorItem> cropzonecolordict = new Dictionary<string, ColorItem>();
        private Dictionary<string, int> cropzonepercentages = new Dictionary<string, int>();
        private Dictionary<string, string> cropzonelabels = new Dictionary<string, string>();
        private Dictionary<string, string> cropzonenamess = new Dictionary<string, string>();
        private Dictionary<string, string> cropzonefields = new Dictionary<string, string>();
        private Dictionary<string, string> cropzonefeatures = new Dictionary<string, string>();
        private Dictionary<string, string> farmNames = new Dictionary<string, string>();
        private Dictionary<string, string> fieldnamess = new Dictionary<string, string>();
        private Dictionary<string, string> fieldnamess2 = new Dictionary<string, string>();
        private Dictionary<string, string> fieldnameff = new Dictionary<string, string>();
        private Dictionary<string, string> fieldnameff2 = new Dictionary<string, string>();
        ObservableCollection<ColorItem> coloritems = new ObservableCollection<ColorItem>();
        IList<CropZoneColorItem> cropzonecoloritems = new List<CropZoneColorItem>();
        IMapFilter selectedfilter;
        CropMapsFarmDisplayItem selectedfarm;
        AdornmentLocationDisplayItem selectedposition;
        ObservableCollection<IMapFilter> filterelements;
        ObservableCollection<CropMapsFarmDisplayItem> filterfarms;
        ObservableCollection<AdornmentLocationDisplayItem> filterposition;
        ObservableCollection<ColorItem> coloritemlist = new ObservableCollection<ColorItem>();
        ColorItem selectedcoloritem;
        Random rand = new Random();
        List<AnalysisModel> cropzonesAnalysis = new List<AnalysisModel>();
        ClassBreakStyle classBreakStyle = new ClassBreakStyle();
        bool isOptionsMapPopupOpen = false;
        bool isColorsExceeded = false;
        IMapOverlay selectedoverlay;
        string areaUnitString = string.Empty;
        string coordinateformat = string.Empty;
        AgC.UnitConversion.Area.AreaUnit agcAreaUnit;
        ThinkGeo.MapSuite.Core.AreaUnit mapAreaUnit;
        ThinkGeo.MapSuite.Core.DistanceUnit mapDistanceUnit;
        bool bingmapvisible = true;
        int fieldopacity = 125;
        int fieldopacityfill = 125;
        int cmgopacity = 255;
        int areadecimals = 2;
        int bordersize = 2;
        bool displayareainfieldlabel = false;
        bool displayareaonlylabel = false;
        bool cmgfieldsorcropzones = true;
        bool displayLabels = false;
        bool displayFillColor = false;
        bool displaycmglegend = false;
        bool labelallpolygonparts = false;
        bool allowlabeloverlapping = true;
        bool fittingpolygon = true;
        double fittingpolygonfactor = 2;
        string fieldsselectedfillcolor;
        string fieldsunselectedfillcolor;
        string fieldsselectedbordercolor;
        string fieldsunselectedbordercolor;
        GeoColor fieldsselectedGeoColor = GeoColor.StandardColors.Yellow;
        GeoColor fieldsunselectedGeoColor = GeoColor.StandardColors.Gray;
        GeoColor fieldsselectedborderGeoColor = new GeoColor();
        GeoColor fieldsunselectedborderGeoColor = new GeoColor();
        double lastmapscale = -9999999;
        MapOptionsViewModel mapOptionsViewModel;
        IAccountStatus accountStatus;
        UserSettings userSettings;
        Visibility mapOptionsVisibility = Visibility.Collapsed;
        Visibility mapTreeVisibility = Visibility.Collapsed;
        ChangeCMGFieldsViewModel changeFields;
        PrintMapToolViewModel printmapvm;
        bool isLegendVisible = false;
        LegendAdornmentLayer legendLayer = new LegendAdornmentLayer();
        AdornmentOverlay adornmentOverlay = new AdornmentOverlay();
        Dictionary<string, double> dictarea = new Dictionary<string, double>();
        Dictionary<string, double> dictpercent = new Dictionary<string, double>();
        Dictionary<string, double> surgoareas = new Dictionary<string, double>();
        ZoomLevelFactory zoomlevelfactory = new ZoomLevelFactory();
        private Projection projectionFromSphericalMercator;
        private Projection projectionFromWGS84;
        private Projection projectionFromSM;
        bool showGoogleRadarCommands = false;
        //bool notcenteredyet = false;

        public MapsPageViewModel(IClientEndpoint clientEndpoint, IDetailsViewModelFactory detailsFactory, Dispatcher dispatcher) {
            this.dispatcher = dispatcher;
            this.clientEndpoint = clientEndpoint;
            this.detailsFactory = detailsFactory;
            this.currentDataSourceId = ApplicationEnvironment.CurrentDataSourceId;
            this.currentCropYear = ApplicationEnvironment.CurrentCropYear;
            this.mapsettings = clientEndpoint.GetMapSettings();

            showGoogleRadarCommands = false;
            this.accountStatus = ServiceLocator.Get<IAccountStatus>(); // TODO: Bad factoring here. Redo.
            var settingsId = new UserSettingsId(accountStatus.UserId);
            this.userSettings = clientEndpoint.GetUserSettings();
            mapTreeVisibility = Visibility.Collapsed;
            mapOptionsVisibility = Visibility.Collapsed;
            mapOptionsViewModel = new MapOptionsViewModel(clientEndpoint, mapsettings, settingsId, ApplicationEnvironment.ShowPremiumMap);
            bingmapvisible = mapsettings.BingMapVisible;
            fieldopacity = mapsettings.FieldsOpacity;
            fieldopacityfill = mapsettings.FieldsOpacity;
            cmgopacity = mapsettings.UnselectedOpacity;
            annotationopacity = mapsettings.MapAnnotationOpacity;
            displayLabels = mapsettings.ScalingTextVisible;
            if (mapsettings.DisplayFillColor.HasValue) {
                displayFillColor = mapsettings.DisplayFillColor.Value;
                if (displayFillColor) {
                    fieldopacityfill = mapsettings.FieldsOpacity;
                }
                else {
                    fieldopacityfill = 0;
                }
            }
            if (mapsettings.CMGMapAnnotationsVisible.HasValue) {
                annotationvisibility = mapsettings.CMGMapAnnotationsVisible.Value;
            }
            if (mapsettings.DisplayAreaInFieldLabel.HasValue) {
                displayareainfieldlabel = mapsettings.DisplayAreaInFieldLabel.Value;
            }
            if (mapsettings.DisplayAreaOnlyLabel.HasValue) {
                displayareaonlylabel = mapsettings.DisplayAreaOnlyLabel.Value;
            }
            if (mapsettings.CMGFieldOrCropzones.HasValue) {
                cmgfieldsorcropzones = mapsettings.CMGFieldOrCropzones.Value;
            }
            if (mapsettings.MapAnnotationLabelsVisible.HasValue) {
                mapannotationlabelsvisible = mapsettings.MapAnnotationLabelsVisible.Value;
            }
            if (mapsettings.DisplayCMGLegend.HasValue) {
                displaycmglegend = mapsettings.DisplayCMGLegend.Value;
            }
            displaymapzoombar = false;
            if (mapsettings.DisplayMapZoomBar.HasValue) {
                displaymapzoombar = mapsettings.DisplayMapZoomBar.Value;
            }
            //StyleData.Self.UserStyleNames = this.mapsettings.MapAnnotationCategories;
            styleFactory = new StyleFactory();
            changedstyles = new List<AnnotationStyleItem>();
            userstylenames = new Dictionary<string, MapAnnotationCategory>();
            cropzonecoloritems = new List<CropZoneColorItem>();
            bordersize = mapsettings.BorderSize;
            areadecimals = mapsettings.AreaDecimals;
            areaUnitString = mapsettings.MapAreaUnit;
            coordinateformat = mapsettings.DisplayCoordinateFormat;
            agcAreaUnit = UnitFactory.GetUnitByName(areaUnitString) as AgC.UnitConversion.Area.AreaUnit;
            mapAreaUnit = MapUnitFactory.AreaMeasureConversion(agcAreaUnit);
            mapDistanceUnit = MapUnitFactory.AreaDistanceMeasureConversion(agcAreaUnit);
            scalingTextStyle = new ColoredMapGeneratorScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, mapsettings.ScalingTextMaximumSize, this.mapsettings.ScalingTextMinimumSize);
            //scalingTextStyle.SetTexTColumnName(@"AreaLabel");
            cookiecutLayerAreaStyle = new SSurgoMultiAreaStyle();
            if (displayareainfieldlabel) {
                if (cmgfieldsorcropzones) {
                    //scalingTextStyle.SetTexTColumnName(@"AreaLabel");
                    if (displayareaonlylabel) {
                        scalingTextStyle.SetTexTColumnName(@"AreaOnlyLabel");
                    }
                    else {
                        scalingTextStyle.SetTexTColumnName(@"AreaLabel");
                    }
                }
                else {
                    //scalingTextStyle.SetTexTColumnName(@"AreaDisplay");
                    if (displayareaonlylabel) {
                        scalingTextStyle.SetTexTColumnName(@"AreaOnlyCZLabel");
                    }
                    else {
                        scalingTextStyle.SetTexTColumnName(@"AreaDisplay");
                    }
                }
            }
            else {
                if (cmgfieldsorcropzones) {
                    scalingTextStyle.SetTexTColumnName(@"LdbLabel");
                }
                else {
                    scalingTextStyle.SetTexTColumnName(@"Cropzone");
                }
                //scalingTextStyle.SetTexTColumnName(@"AreaOnlyLabel");
            }
            scalingTextStyle2 = new AnnotationScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, mapsettings.ScalingTextMaximumSize, this.mapsettings.ScalingTextMinimumSize);
            scalingTextStyle2.ColumnsList = new List<string>() { @"Id", @"LdbLabel", @"TypeName", @"ShapeType" };
            //scalingTextStyle2.YOffsetInPixel = 20;

            fittingpolygonfactor = mapsettings.FittingPolygonFactor;
            if (mapsettings.AllowLabelOverlapping.HasValue) {
                allowlabeloverlapping = mapsettings.AllowLabelOverlapping.Value;
            }
            if (mapsettings.FittingPolygon.HasValue) {
                fittingpolygon = mapsettings.FittingPolygon.Value;
            }
            scalingTextStyle.LabelAllPolygonParts = labelallpolygonparts;
            scalingTextStyle.FittingPolygon = fittingpolygon;
            scalingTextStyle.FittingPolygonFactor = fittingpolygonfactor;
            if (allowlabeloverlapping) {
                scalingTextStyle.OverlappingRule = LabelOverlappingRule.AllowOverlapping;
            }
            else {
                scalingTextStyle.OverlappingRule = LabelOverlappingRule.NoOverlapping;
            }

            fieldsselectedfillcolor = mapsettings.FieldsSelectedFillColor;
            fieldsunselectedfillcolor = mapsettings.FieldsUnSelectedFillColor;
            if (!string.IsNullOrEmpty(fieldsselectedfillcolor)) {
                fieldsselectedGeoColor = GeoColor.FromHtml(fieldsselectedfillcolor);
            }
            if (!string.IsNullOrEmpty(fieldsunselectedfillcolor)) {
                fieldsunselectedGeoColor = GeoColor.FromHtml(fieldsunselectedfillcolor);
            }
            fieldsselectedbordercolor = mapsettings.FieldsSelectedBorderColor;
            fieldsunselectedbordercolor = mapsettings.FieldsUnSelectedBorderColor;
            if (!string.IsNullOrEmpty(fieldsselectedbordercolor)) {
                fieldsselectedborderGeoColor = GeoColor.FromHtml(fieldsselectedbordercolor);
            }
            else {
                fieldsselectedborderGeoColor = fieldsselectedGeoColor;
            }
            if (!string.IsNullOrEmpty(fieldsunselectedbordercolor)) {
                fieldsunselectedborderGeoColor = GeoColor.FromHtml(fieldsunselectedbordercolor);
            }
            else {
                fieldsunselectedborderGeoColor = fieldsunselectedGeoColor;
            }
            //selectedoverlay = MAPOptionsViewModel.MapFactory(mapsettings.CMGMapInfoSelectorItem);
            selectedoverlay = MAPOptionsViewModel.MapFactory(mapsettings.CMGMapInfoSelectorItem, mapsettings.AllowOfflineMap);
            clientLayerItems = new List<MapItem>();
            cropzoneLayerItems = new List<MapItem>();
            unselectedFeatures = new Dictionary<string, string>();
            chosenFeatures = new Dictionary<string, string>();
            Messenger.Default.Register<DataSourceChangedMessage>(this, OnDataSourceChanged);
            UpdatePositionFilters();

            InitializeMap();
            RefreshCMGMapCommand = new RelayCommand(RefreshImageryLayer);
            SetCMGMapTypeCommand = new RelayCommand(SetCMGMapType);
            ToggleCMGLegendCommand = new RelayCommand(SetCMGLegend);
            ToggleCMGAnnotationsCommand = new RelayCommand(SetMapAnnotation);
            ToggleDisplayAllCropzonesCommand = new RelayCommand(SetDisplayAllCropzones);
            ZoomToFieldsExtentCommand = new RelayCommand(ZoomToFieldsExtent);
            ExportSelectedShapeFilesCommand = new RelayCommand(ExportSelectedShapeFiles);
            ExportAllCropzonesCommand = new RelayCommand(ExportAllCropzones);
            MAPOptionsVisibilityCommand = new RelayCommand(ToggleOptionsVisibility);
            MAPTreeVisibilityCommand = new RelayCommand(ChangeFieldSelection);
            PrintCMGCommand = new RelayCommand(PrintMap);
            PrintMapCommand = new RelayCommand(Print);
            PrintCMGMapViewCommand = new RelayCommand(PrintCMGMapView);
            GoogleRadarCommand = new RelayCommand(SnapToGoogleImagery);
        }

        public MapsPageViewModel(IClientEndpoint clientEndpoint, Dispatcher dispatcher)
        {
            this.dispatcher = dispatcher;
            this.clientEndpoint = clientEndpoint;
            this.currentDataSourceId = ApplicationEnvironment.CurrentDataSourceId;
            this.currentCropYear = ApplicationEnvironment.CurrentCropYear;
            var mapsettings = clientEndpoint.GetMapSettings();
            //selectedoverlay = MAPOptionsViewModel.MapFactory(mapsettings.CMGMapInfoSelectorItem);
            selectedoverlay = MAPOptionsViewModel.MapFactory(mapsettings.CMGMapInfoSelectorItem, mapsettings.AllowOfflineMap);
            clientLayerItems = new List<MapItem>();
            cropzoneLayerItems = new List<MapItem>();
            unselectedFeatures = new Dictionary<string, string>();
            chosenFeatures = new Dictionary<string, string>();
            Messenger.Default.Register<DataSourceChangedMessage>(this, OnDataSourceChanged);
            bingmapvisible = mapsettings.BingMapVisible;
            fieldopacity = mapsettings.FieldsOpacity;
            fieldopacityfill = mapsettings.FieldsOpacity;
            cmgopacity = mapsettings.UnselectedOpacity;
            UpdatePositionFilters();

            InitializeMap();

            RefreshCMGMapCommand = new RelayCommand(RefreshImageryLayer);
            SetCMGMapTypeCommand = new RelayCommand(SetCMGMapType);
            ToggleCMGLegendCommand = new RelayCommand(SetCMGLegend);
            ToggleCMGAnnotationsCommand = new RelayCommand(SetMapAnnotation);
            ToggleDisplayAllCropzonesCommand = new RelayCommand(SetDisplayAllCropzones);
            ZoomToFieldsExtentCommand = new RelayCommand(ZoomToFieldsExtent);
            ExportSelectedShapeFilesCommand = new RelayCommand(ExportSelectedShapeFiles);
            ExportAllCropzonesCommand = new RelayCommand(ExportAllCropzones);
            MAPOptionsVisibilityCommand = new RelayCommand(ToggleOptionsVisibility);
            MAPTreeVisibilityCommand = new RelayCommand(ChangeFieldSelection);
            PrintCMGCommand = new RelayCommand(PrintMap);
            PrintMapCommand = new RelayCommand(Print);
            PrintCMGMapViewCommand = new RelayCommand(PrintCMGMapView);
            GoogleRadarCommand = new RelayCommand(SnapToGoogleImagery);
        }

        public ICommand RefreshCMGMapCommand { get; private set; }
        public ICommand SetCMGMapTypeCommand { get; private set; }
        public ICommand ToggleCMGLegendCommand { get; private set; }
        public ICommand ToggleCMGAnnotationsCommand { get; private set; }
        public ICommand ToggleDisplayAllCropzonesCommand { get; private set; }
        public ICommand ZoomToFieldsExtentCommand { get; private set; }
        public ICommand ExportSelectedShapeFilesCommand { get; private set; }
        public ICommand ExportAllCropzonesCommand { get; private set; }
        public ICommand MAPOptionsVisibilityCommand { get; private set; }
        public ICommand MAPTreeVisibilityCommand { get; private set; }
        public ICommand PrintCMGCommand { get; private set; }
        public ICommand PrintMapCommand { get; private set; }
        public ICommand PrintCMGMapViewCommand { get; private set; }
        public ICommand GoogleRadarCommand { get; private set; }
        public WpfMap Map { get; set; }

        public ObservableCollection<IMapFilter> FilterElements {
            get { return filterelements; }
            set {
                if (filterelements == value) { return; }
                filterelements = value;
                RaisePropertyChanged("FilterElements");
            }
        }

        public ObservableCollection<CropMapsFarmDisplayItem> FilterFarms {
            get { return filterfarms; }
            set {
                if (filterfarms == value) { return; }
                filterfarms = value;
                RaisePropertyChanged("filterfarms");
            }
        }

        public CropMapsFarmDisplayItem SelectedFarm {
            get { return selectedfarm; }
            set {
                if (selectedfarm == value) { return; }
                selectedfarm = value;
                RaisePropertyChanged("SelectedFarm");
                if (value == null) { return; }
                log.Info("CMGTools - Change Selected farm to " + selectedfarm);
                if (!refreshimagery && !datasoucrechanging && maploaded) {
                    FilterBySelectedFarm(true);
                }
            }
        }

        public IMapFilter SelectedFilter {
            get {
                if (selectedfilter == null && FilterElements != null && FilterElements.Count > 0)
                {
                    return FilterElements[0];
                }
                return selectedfilter;
            }
            set {
                if (selectedfilter == value) { return; }
                selectedfilter = value;
                simpleMarkerOverlay.Markers.Clear();
                cmgAreaStyle.ColorStyles = new Dictionary<string, AreaStyle>();
                scalingTextStyle.ColorStyles = new Dictionary<string, AreaStyle>();

                if (value == null) {
                    selectedfilter = FilterElements[0];
                    //return;
                }
                RaisePropertyChanged("SelectedFilter");
                log.Info("CMGTools - Change Selected filter to " + Enum.GetName(typeof(FilterType), selectedfilter.Filter));
                ProcessSelectedFilter();
            }
        }

        public ObservableCollection<AdornmentLocationDisplayItem> FilterPosition
        {
            get { return filterposition; }
            set
            {
                if (filterposition == value) { return; }
                filterposition = value;
                RaisePropertyChanged("FilterPosition");
            }
        }

        public AdornmentLocationDisplayItem SelectedPosition
        {
            get { return selectedposition; }
            set
            {
                if (selectedposition == value) { return; }
                selectedposition = value;
                RaisePropertyChanged("SelectedPosition");
            }
        }

        private void ProcessSelectedFilter() {
            processingfilter = true;
            isColorsExceeded = false;
            selectedfilter.Execute(clientEndpoint, rand);

            if(selectedfilter.UsedColors.Count > 500) {
                cmgAreaStyle.ColorStyles = new Dictionary<string, AreaStyle>();
                scalingTextStyle.ColorStyles = new Dictionary<string, AreaStyle>();
                //isColorsExceeded = true;
            }
            if (!refreshimagery && !datasoucrechanging && maploaded) {
                FilterBySelectedFarm(true);
            }
            processingfilter = false;
        }

        public ColorItem SelectedColorItem {
            get { return selectedcoloritem; }
            set {
                if (selectedcoloritem == value) { return; }
                selectedcoloritem = value;
                RaisePropertyChanged("SelectedColorItem");
            }
        }

        public ObservableCollection<ColorItem> ColorItemList {
            get { return coloritemlist; }
            set {
                if (coloritemlist == value) { return; }
                coloritemlist = value;
                RaisePropertyChanged("ColorItemList");
            }
        }

        public string DisplayName {
            get {
                if (SelectedFilter == null) { return string.Empty; }
                return SelectedFilter.Name;
            }
        }

        static object clientLayerLock = new object();
        public IList<MapItem> ClientLayer {
            get { return clientLayerItems; }
            set {
                lock (clientLayerLock) {
                    clientLayerItems = value;
                    ReloadMapItems();
                    RaisePropertyChanged("ClientLayer");
                }
            }
        }

        static object cropzoneLayerLock = new object();
        public IList<MapItem> CropZoneLayer {
            get { return cropzoneLayerItems; }
            set {
                lock (cropzoneLayerLock) {
                    cropzoneLayerItems = value;
                    clientLayerItems = cropzoneLayerItems;
                    RefreshImageryLayer2();
                    RaisePropertyChanged("CropZoneLayer");
                }
            }
        }

        public Dictionary<string, string> UnselectedFeatures
        {
            get { return unselectedFeatures; }
            set {
                unselectedFeatures = value;
                cmgAreaStyle.UnselectedFeatures = unselectedFeatures;
            }
        }

        public Dictionary<string, string> ChosenFeatures
        {
            get { return chosenFeatures; }
            set
            {
                chosenFeatures = value;
                //cmgAreaStyle.UnselectedFeatures = unselectedFeatures;
            }
        }
        public ObservableCollection<ColorItem> ColorItems {
            get { return coloritems; }
            set {
                //if (coloritems == value) { return; }
                coloritems = value;
                //SetColorStyles();
                //OnColorUpdated();
                RaisePropertyChanged("ColorItem");
                RaisePropertyChanged("ColorItems");
                RaisePropertyChanged("CropzoneColors");
            }
        }

        public Dictionary<string, AreaStyle> ColorStyles {
            get { return colorstyles; }
            set {
                if (colorstyles == value) { return; }
                colorstyles = value;
                scalingTextStyle.ColorStyles = colorstyles;
                cmgAreaStyle.ColorStyles = colorstyles;
                RefreshFieldsLayer(false, true);
                RaisePropertyChanged("ColorStyles");
            }
        }

        public Dictionary<string, System.Drawing.Color> CropzoneColors {
            get { return cropzonecolors; }
            set {
                //if (cropzonecolors == value) { return; }
                cropzonecolors = value;
                SetColorStyles();
                RaisePropertyChanged("CropzoneColors");
            }
        }

        public void SetCropzoneColors(Dictionary<string, ColorItem> cropzoneColorDict) {
            cropzonecolordict = cropzoneColorDict;
        }

        public void SetCropzoneColors(Dictionary<string, System.Drawing.Color> cropzoneColors) {
            cropzonecolors = cropzoneColors;
        }

        private void SetColorStyles() {
            Dictionary<string, AreaStyle> cstyles = new Dictionary<string, AreaStyle>();
            foreach (var colr in cropzonecolors) {
                GeoColor gc = new GeoColor(cmgopacity, colr.Value.R, colr.Value.G, colr.Value.B);
                GeoColor blackColor = new GeoColor(cmgopacity, GeoColor.StandardColors.Black);
                AreaStyle astyle = new AreaStyle(new GeoPen(blackColor, bordersize), new GeoSolidBrush(gc));
                if (cropzonefeatures.ContainsKey(colr.Key)) {
                    cstyles.Add(colr.Key, astyle);
                }
            }
            //simpleMarkerOverlay.Markers.Clear();
            //scalingTextStyle.ColorStyles = cstyles;
            //ColorStyles = cstyles;
            if (colorstyles == cstyles) { return; }
            colorstyles = cstyles;
            scalingTextStyle.ColorStyles = colorstyles;
            cmgAreaStyle.ColorStyles = colorstyles;
            if (!refreshimagery && !processingfilter) {
                RefreshFieldsLayer(false, true);
            }
            RaisePropertyChanged("ColorStyles");
        }

        public Dictionary<string, ColorItem> CropzoneColorDict {
            get { return cropzonecolordict; }
            set {
                cropzonecolordict = value;
                SetColorHatchStyles();
                RaisePropertyChanged("CropzoneColorDict");
            }
        }

        private void SetColorHatchStyles() {
            Dictionary<string, AreaStyle> cstyles = new Dictionary<string, AreaStyle>();
            foreach (var colr in cropzonecolordict) {
                GeoColor gc = new GeoColor(cmgopacity, colr.Value.MapColor.R, colr.Value.MapColor.G, colr.Value.MapColor.B);
                GeoColor blackColor = new GeoColor(cmgopacity, GeoColor.StandardColors.Black);
                AreaStyle astyle = new AreaStyle(new GeoPen(blackColor, bordersize), new GeoSolidBrush(gc));
                if (string.IsNullOrWhiteSpace(colr.Value.HatchType) || colr.Value.HatchType == "None" || colr.Value.HatchType == "Solid") {
                    astyle = new AreaStyle(new GeoPen(blackColor, bordersize), new GeoSolidBrush(gc));
                }
                else {
                    astyle = AreaStyles.CreateHatchStyle(HatchStyleFactory.GetHatchStyle(colr.Value.HatchType), GeoColor.FromArgb(cmgopacity, colr.Value.MapColorFG.R, colr.Value.MapColorFG.G, colr.Value.MapColorFG.B), GeoColor.FromArgb(cmgopacity, colr.Value.MapColor.R, colr.Value.MapColor.G, colr.Value.MapColor.B), blackColor, bordersize, LineDashStyle.Solid, 0, 0);
                }

                if (cropzonefeatures.ContainsKey(colr.Key)) {
                    cstyles.Add(colr.Key, astyle);
                }
            }
            //simpleMarkerOverlay.Markers.Clear();
            //scalingTextStyle.ColorStyles = cstyles;
            if (colorstyles == cstyles) { return; }
            colorstyles = cstyles;
            scalingTextStyle.ColorStyles = colorstyles;
            cmgAreaStyle.ColorStyles = colorstyles;
            if (!processingfilter) {
                RefreshFieldsLayer(false, true);
            }
            RaisePropertyChanged("ColorStyles");
        }

        public Dictionary<string, int> CropzonePercentages {
            get { return cropzonepercentages; }
            set {
                //if (cropzonepercentages == value) { return; }
                cropzonepercentages = value;
                LoadMapMarkers();
                RaisePropertyChanged("CropzonePercentages");
            }
        }

        public bool ShowGoogleRadarCommands {
            get { return showGoogleRadarCommands; }
            set {
                showGoogleRadarCommands = value;
                RaisePropertyChanged("ShowGoogleRadarCommands");
            }
        }

        private void LoadMapMarkers() {
            simpleMarkerOverlay.Markers.Clear();
            if (selectedfilter == null) {
                try {
                    Map.Refresh(simpleMarkerOverlay);
                }
                catch { }
                return;
            }
            if (selectedfilter.Filter != FilterType.ActiveIngredients) {
                try {
                    Map.Refresh(simpleMarkerOverlay);
                }
                catch { }
                return; 
            }
            foreach (var m in clientLayerItems) {
                if (m.MapData != null && !string.IsNullOrEmpty(m.MapData) && m.MapData != "MULTIPOLYGON EMPTY") {
                    var feature = new Feature(m.MapData);
                    string featurekey = m.FieldId.ToString();
                    if (displaycropzones) {
                        featurekey = m.CropZoneId.ToString();
                    }
                    if (!CropzonePercentages.ContainsKey(featurekey)) { continue; }
                    if (CropzonePercentages[featurekey] > 20) { continue; }
                    AreaBaseShape abs = feature.GetShape() as AreaBaseShape;
                    PointShape midPointShape1 = abs.GetCenterPoint();
                    PointShape midPointShape2 = projection.ConvertToInternalProjection(midPointShape1) as PointShape;
                    PointShape midPointShape3 = projection.ConvertToExternalProjection(midPointShape1) as PointShape;
                    Marker marker = new Marker(midPointShape3);
                    PointMarkerStyle markerstyle = new PointMarkerStyle();
                    marker.ImageSource = new BitmapImage(new Uri("/Resources/Sign-Warning-icon.png", UriKind.RelativeOrAbsolute));
                    marker.Width = 20;
                    marker.Height = 20;
                    marker.YOffset = -17;
                    marker.ToolTip = CropzonePercentages[featurekey];
                    marker.FontSize = 14;
                    marker.FontStyle = FontStyles.Oblique;
                    simpleMarkerOverlay.Markers.Add(marker);
                }
            }
            if (Map.Overlays.Contains("SimpleMarkerOverlay")) {
                Map.Overlays.Remove("SimpleMarkerOverlay");
            }
            if (!Map.Overlays.Contains("SimpleMarkerOverlay")) {
                Map.Overlays.Add("SimpleMarkerOverlay", simpleMarkerOverlay);
            }
            Map.Refresh(simpleMarkerOverlay);
        }

        public Dictionary<string, string> CropzoneLabels {
            get { return cropzonelabels; }
            set {
                if (cropzonelabels == value) { return; }
                cropzonelabels = value;
                scalingTextStyle.CustomLabels = cropzonelabels;
                RaisePropertyChanged("CropzoneLabels");
            }
        }

        public bool IsLoadingTree {
            get { return isLoadingTree; }
            set {
                isLoadingTree = value;
                RaisePropertyChanged("IsLoadingTree");
            }
        }

        public bool DisplayCropzones {
            get { return displaycropzones; }
            set {
                displaycropzones = value;
                if (displaycropzones) {
                    cmgAreaStyle.DisplayAllCropzones = true;
                }
                RaisePropertyChanged("DisplayCropzones");
            }
        }

        public bool IsOptionsMapPopupOpen {
            get { return isOptionsMapPopupOpen; }
            set {
                isOptionsMapPopupOpen = value;
                RaisePropertyChanged("IsOptionsMapPopupOpen");
            }
        }
        public bool IsColorsExceeded {
            get { return isColorsExceeded; }
            set {
                isColorsExceeded = value;
                RaisePropertyChanged("IsProcessingCommands");
            }
        }


        public IMapOverlay SelectedOverlay {
            get { return selectedoverlay; }
            set {
                if (selectedoverlay == value) { return; }
                if (selectedoverlay == null) { return; }
                if (string.IsNullOrWhiteSpace(value.Name)) { return; }
                selectedoverlay = value;
                if (selectedoverlay != null && SelectedOverlayIsGoogle()) {
                    ShowGoogleRadarCommands = true;
                }
                else {
                    ShowGoogleRadarCommands = false;
                }
                RaisePropertyChanged("SelectedOverlay");
            }
        }

        private bool SelectedOverlayIsGoogle() {
            return selectedoverlay.MapInfoSelectorItem.IsGoogle();
        }

        public Visibility MAPOptionsVisibility {
            get { return mapOptionsVisibility; }
            set {
                mapOptionsVisibility = value;
                RaisePropertyChanged("MAPOptionsVisibility");
            }
        }

        public Visibility MAPTreeVisibility {
            get { return mapTreeVisibility; }
            set {
                mapTreeVisibility = value;
                RaisePropertyChanged("MAPTreeVisibility");
            }
        }

        public MapOptionsViewModel MAPOptionsViewModel {
            get { return mapOptionsViewModel; }
            set {
                mapOptionsViewModel = value;
                RaisePropertyChanged("MAPOptionsViewModel");
            }
        }

        public bool IsLegendVisible {
            get { return isLegendVisible; }
            set {
                isLegendVisible = value;
                RaisePropertyChanged("IsLegendVisible");
            }
        }

        public LegendAdornmentLayer LegendLayer {
            get { return legendLayer; }
            set {
                legendLayer = value;
                RaisePropertyChanged("LegendLayer");
            }
        }

        public Bitmap MapImage { get; set; }

        void Print()
        {
            log.Info("CMGTools - Print bitmap");
            IsOptionsMapPopupOpen = false;
            var width = Map.ActualWidth;
            var height = Map.ActualHeight;
            RenderTargetBitmap bmp = new RenderTargetBitmap((int)width, (int)height, 96, 96, PixelFormats.Pbgra32);
            bmp.Render(Map);
            PngBitmapEncoder png = new PngBitmapEncoder();
            png.Frames.Add(BitmapFrame.Create(bmp));

            try
            {

                MemoryStream fs = new MemoryStream();
                png.Save(fs);
                MapImage = new Bitmap(fs);
            }
            catch (Exception ex)
            {
                return;
            }

            var czs = from c in ClientLayer
                      where c.CropZoneId != null
                      select new Guid(c.CropZoneId.ToString());

            //Now generate Report
            var subHeader = string.IsNullOrEmpty(SelectedFarm.DisplayText) || SelectedFarm.IsAllFarms ? string.Empty : SelectedFarm.DisplayText;
            var vm = new Secondary.Reports.Map.PrintMapViewModel(clientEndpoint, dispatcher, MapImage, czs.ToList(), subHeader, new FieldId());
            var sd = new ScreenDescriptor("Landdb.Views.Secondary.Reports.Work_Order.SingleWorkOrderView", "viewReport", vm);
            Messenger.Default.Send(new ShowOverlayMessage() { ScreenDescriptor = sd });
        }

        void PrintCMGMapView() {
        }

        void OnDataSourceChanged(DataSourceChangedMessage message) {
            this.currentDataSourceId = message.DataSourceId;
            this.currentCropYear = message.CropYear;
            if (!maploaded) { return; }
            datasoucrechanging = true;
            RebuildMap();
            //filterelements = new ObservableCollection<IMapFilter>();
            UpdateFilters();
            FilterBySelectedFarm(true);
            datasoucrechanging = false;
        }

        void InitializeMap() {
            if (maploaded) { return; }
            Map = new WpfMap();
            if (SelectedOverlay != null && SelectedOverlayIsGoogle()) {
                ShowGoogleRadarCommands = true;
            }
            else {
                ShowGoogleRadarCommands = false;
            }
            startedOutOnline = Landdb.Client.Infrastructure.NetworkStatus.IsInternetAvailableFast();
            if (SelectedOverlayIsGoogle()) {
                Map.MaxHeight = 2048;
                Map.MaxWidth = 2048;
                //Map.Height = 640;
                //Map.Width = 640;
            }
            Map.MapUnit = GeographyUnit.Meter;
            Map.MapTools.PanZoomBar.Visibility = System.Windows.Visibility.Visible;
            Map.MapTools.PanZoomBar.GlobeButtonVisibility = System.Windows.Visibility.Hidden;
            Map.MapTools.PanZoomBar.VerticalAlignment = System.Windows.VerticalAlignment.Center;
            Map.MapTools.PanZoomBar.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            displaymapzoombar = false;
            if (mapsettings.DisplayMapZoomBar.HasValue) {
                displaymapzoombar = mapsettings.DisplayMapZoomBar.Value;
            }
            if (!displaymapzoombar) {
                Map.MapTools.PanZoomBar.Visibility = System.Windows.Visibility.Collapsed;
            }
            Map.Background = System.Windows.Media.Brushes.White;
            //zoomlevelfactory = new ZoomLevelFactory(mapsettings.ZoomLevel);
            //Map.ZoomLevelSet = zoomlevelfactory.ZoomLevelSET;
            Map.MouseLeftButtonDown += (sender, e) => {
                ((MainWindow)System.Windows.Application.Current.MainWindow).Draggable = false;
            };

            projection = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetGoogleMapParametersString());

            scalingTextStyle.RotationAngles = new Dictionary<string, double>();
            scalingTextStyle.LabelPositions = new Dictionary<string, PointShape>();
            scalingTextStyle2.RotationAngles = new Dictionary<string, double>();
            scalingTextStyle2.LabelPositions = new Dictionary<string, PointShape>();
            scalingTextStyle4.RotationAngles = new Dictionary<string, double>();
            scalingTextStyle4.LabelPositions = new Dictionary<string, PointShape>();

            //fieldsLayer
            fieldsLayer.FeatureSource.Open();
            FeatureSourceColumn column = new FeatureSourceColumn(@"LdbLabel", DbfColumnType.Character.ToString(), 64);
            FeatureSourceColumn column2 = new FeatureSourceColumn(@"Percent", DbfColumnType.IntegerInBinary.ToString(), 4);
            FeatureSourceColumn column3 = new FeatureSourceColumn(@"Farm", DbfColumnType.Character.ToString(), 64);
            FeatureSourceColumn column4 = new FeatureSourceColumn(@"Field", DbfColumnType.Character.ToString(), 64);
            FeatureSourceColumn column5 = new FeatureSourceColumn(@"Cropzone", DbfColumnType.Character.ToString(), 64);
            FeatureSourceColumn column6 = new FeatureSourceColumn(@"AreaDisplay", DbfColumnType.Character.ToString(), 64);
            FeatureSourceColumn column7 = new FeatureSourceColumn(@"AreaLabel", DbfColumnType.Character.ToString(), 64);
            FeatureSourceColumn column8 = new FeatureSourceColumn(@"AreaOnlyLabel", DbfColumnType.Character.ToString(), 64);
            FeatureSourceColumn column9 = new FeatureSourceColumn(@"AreaOnlyCZLabel", DbfColumnType.Character.ToString(), 64);
            FeatureSourceColumn column10 = new FeatureSourceColumn(@"FieldID", DbfColumnType.Character.ToString(), 64);
            FeatureSourceColumn column11 = new FeatureSourceColumn(@"CropZoneID", DbfColumnType.Character.ToString(), 64);
            fieldsLayer.Columns.Add(column);
            fieldsLayer.Columns.Add(column2);
            fieldsLayer.Columns.Add(column3);
            fieldsLayer.Columns.Add(column4);
            fieldsLayer.Columns.Add(column5);
            fieldsLayer.Columns.Add(column6);
            fieldsLayer.Columns.Add(column7);
            fieldsLayer.Columns.Add(column8);
            fieldsLayer.Columns.Add(column9);
            fieldsLayer.Columns.Add(column10);
            fieldsLayer.Columns.Add(column11);
            fieldsLayer.FeatureSource.Close();
            cmgAreaStyle = new ColoredMapGeneratorMultiAreaStyle();
            cmgAreaStyle.ColumnsList = new List<string>() { @"LdbLabel", @"Percent", @"Farm", @"Field", @"Cropzone", @"AreaDisplay", @"AreaLabel", @"AreaOnlyLabel", @"AreaOnlyCZLabel", @"FieldID", @"CropZoneID" };
            cmgAreaStyle.SelectedAreaStyle = new AreaStyle(new GeoPen(GeoColor.StandardColors.Transparent, 2), new GeoSolidBrush(GeoColor.StandardColors.Transparent));
            cmgAreaStyle.UnselectedAreaStyle = new AreaStyle(new GeoPen(GeoColor.StandardColors.Black, bordersize), new GeoSolidBrush(new GeoColor(10, GeoColor.StandardColors.LightGray)));
            cmgAreaStyle.UnselectedFeatures = new Dictionary<string, string>();
            cmgAreaStyle.DisplayAllCropzones = false;
            fieldsLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(cmgAreaStyle);
            if (displayLabels) {
            fieldsLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(scalingTextStyle);
            }
            fieldsLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            fieldsLayer.FeatureSource.Projection = projection;
            cmgAreaStyle.ColorStyles = new Dictionary<string, AreaStyle>();
            scalingTextStyle.ColorStyles = new Dictionary<string, AreaStyle>();

            //shapes overlay
            layerOverlay = new LayerOverlay();
            layerOverlay.DrawingExceptionMode = DrawingExceptionMode.DrawException;
            if (!layerOverlay.Layers.Contains("fieldLayer")) {
                layerOverlay.Layers.Add("fieldLayer", fieldsLayer);
            }

            classBreakStyle = new ClassBreakStyle(@"Percent");
            classBreakStyle.ClassBreaks.Add(new ClassBreak(double.MinValue, new AreaStyle(new GeoSolidBrush(GeoColor.FromArgb(255, 255, 0, 0)))));
            classBreakStyle.ClassBreaks.Add(new ClassBreak(11, new AreaStyle(new GeoSolidBrush(GeoColor.FromArgb(255, 255, 128, 0)))));
            classBreakStyle.ClassBreaks.Add(new ClassBreak(21, new AreaStyle(new GeoSolidBrush(GeoColor.FromArgb(255, 245, 210, 10)))));
            classBreakStyle.ClassBreaks.Add(new ClassBreak(31, new AreaStyle(new GeoSolidBrush(GeoColor.FromArgb(255, 225, 255, 0)))));
            classBreakStyle.ClassBreaks.Add(new ClassBreak(41, new AreaStyle(new GeoSolidBrush(GeoColor.FromArgb(255, 224, 251, 132)))));
            classBreakStyle.ClassBreaks.Add(new ClassBreak(51, new AreaStyle(new GeoSolidBrush(GeoColor.FromArgb(255, 128, 255, 128)))));
            classBreakStyle.ClassBreaks.Add(new ClassBreak(61, new AreaStyle(new GeoSolidBrush(GeoColor.FromArgb(255, 0, 255, 0)))));

            CreateAnnotationMemoryLayer();
            if (!layerOverlay.Layers.Contains("annotationLayer")) {
                layerOverlay.Layers.Add("annotationLayer", annotationLayer);
            }

            Map.Loaded += (sender, e) => {
                if (maploaded) { return; }
                var projectionstring = Proj4Projection.GetGoogleMapParametersString();
                colorstyles = new Dictionary<string, AreaStyle>();

                LoadBingMapsOverlay();
                RebuildMapAnnotations();
                if (!Map.Overlays.Contains("fieldOverlay")) {
                    Map.Overlays.Add("fieldOverlay", layerOverlay);
                }

                simpleMarkerOverlay = new SimpleMarkerOverlay();
                simpleMarkerOverlay.DrawingExceptionMode = DrawingExceptionMode.DrawException;
                if (!Map.Overlays.Contains("SimpleMarkerOverlay")) {
                    Map.Overlays.Add("SimpleMarkerOverlay", simpleMarkerOverlay);
                }

                EnsureFieldsLayerIsOpen();
                EnsureAnnotationLayerIsOpen();
                RebuildMap();
                filterelements = new ObservableCollection<IMapFilter>();
                selectedfilter = null;
                maploaded = true;
                UpdateFilters();
                log.Info("CMGDisplay - Map loaded");
                RefreshTheMap();
            };

            Map.CurrentScaleChanged += (sender, e) => {
                scalingTextStyle.MapScale = e.CurrentScale;
                scalingTextStyle2.MapScale = e.CurrentScale;
                scalingTextStyle4.MapScale = Map.CurrentScale;
                lastmapscale = e.CurrentScale;
                if (repaintthescreen) {
                    repaintthescreen = false;
                }
            };

            Map.CurrentExtentChanged += (sender, e) => {
                mapextent = e.CurrentExtent;
                if (!maploaded) { return; }
                if (!ApplicationEnvironment.ShowPremiumMap) { return; }
                //if (notcenteredyet) {
                //    notcenteredyet = false;
                //    return;
                //}

                if (SelectedOverlayIsGoogle()) {

                    oldticks = newticks;
                    newticks = DateTime.Now.Ticks;
                    long diffticks = newticks - oldticks;
                    //if (diffticks < 5000000) { return; }

                    PointShape centerpoint = mapextent.GetCenterPoint();
                    PointShape wgscenterpoint = projection.ConvertToInternalProjection(centerpoint) as PointShape;
                    UTMConstruction utmclass = zoomlevelfactory.GetUTMZone(wgscenterpoint);
                    int epsgcode = utmclass.epsgcode;
                    projection.ExternalProjectionParametersString = Proj4Projection.GetEpsgParametersString(epsgcode);
                    Proj4Projection projectionutm = new Proj4Projection(Proj4Projection.GetEpsgParametersString(epsgcode), Proj4Projection.GetGoogleMapParametersString());
                    projectionutm.Open();
                    PointShape gglcenterpoint = projection.ConvertToExternalProjection(centerpoint) as PointShape;
                    RectangleShape gglextent = projection.ConvertToExternalProjection(mapextent) as RectangleShape;
                    RectangleShape wgsextent = projection.ConvertToInternalProjection(mapextent) as RectangleShape;
                    RectangleShape utmextent = projectionutm.ConvertToInternalProjection(mapextent) as RectangleShape;
                    int currentzoomlevel = zoomlevelfactory.GetZoomLevelFromScale(Map.CurrentScale);
                    bool isthisagooglezoomlevel = zoomlevelfactory.VerifyGoogleZoomLevelFromScale(Map.CurrentScale);
                    if (!isthisagooglezoomlevel) { return; }

                    if (this.projectionFromSphericalMercator == null) {
                        string googleMapParametersString = ManagedProj4Projection.GetGoogleMapParametersString();
                        string googleMapParametersString2 = ManagedProj4Projection.GetGoogleMapParametersString();
                        this.projectionFromSphericalMercator = new ManagedProj4Projection(googleMapParametersString, googleMapParametersString2);
                        this.projectionFromSphericalMercator.Open();
                    }
                    RectangleShape gglextent2 = (RectangleShape)this.projectionFromSphericalMercator.ConvertToInternalProjection(mapextent);
                    if (this.projectionFromWGS84 == null) {
                        string wgsParametersString = ManagedProj4Projection.GetWgs84ParametersString();
                        string wgspParametersString2 = ManagedProj4Projection.GetWgs84ParametersString();
                        this.projectionFromWGS84 = new ManagedProj4Projection(wgsParametersString, wgspParametersString2);
                        this.projectionFromWGS84.Open();
                    }
                    RectangleShape wgsextent2 = (RectangleShape)this.projectionFromWGS84.ConvertToInternalProjection(mapextent);
                    PointShape wgscenterpoint2 = wgsextent2.GetCenterPoint();
                    UTMConstruction utmclass2 = zoomlevelfactory.GetUTMZone(wgscenterpoint2);
                    int epsgcode2 = utmclass2.epsgcode;
                    PointShape wgscenterpoint3 = (PointShape)this.projectionFromWGS84.ConvertToInternalProjection(centerpoint);
                    UTMConstruction utmclass3 = zoomlevelfactory.GetUTMZone(wgscenterpoint3);
                    int epsgcode3 = utmclass3.epsgcode;

                    if (this.projectionFromSM == null) {
                        string smParametersString = ManagedProj4Projection.GetWgs84ParametersString();
                        string smParametersString2 = ManagedProj4Projection.GetWgs84ParametersString();
                        this.projectionFromSM = new ManagedProj4Projection(smParametersString, smParametersString2);
                        this.projectionFromSM.Open();
                    }
                    RectangleShape wgsextent4 = (RectangleShape)this.projectionFromSM.ConvertToInternalProjection(mapextent);

                    log.Info("CMGTools - Get Google imagery.");
                    //Map.Height = 640;
                    //Map.Width = 640;
                    int tileHeight = System.Convert.ToInt32(Map.ActualHeight);
                    int tileWidth = System.Convert.ToInt32(Map.ActualWidth);
                    Overlay googloverlay = SelectedOverlay.ExtentHasChanged(tileHeight, tileWidth, utmextent, gglextent2, wgscenterpoint, currentzoomlevel, epsgcode);
                    if (Map.Overlays.Contains("Bing")) {
                        Map.Overlays.Remove("Bing");
                    }
                    if (Map.Overlays.Contains("Bing Online")) {
                        Map.Overlays.Remove("Bing Online");
                    }
                    if (Map.Overlays.Contains("Roads Only")) {
                        Map.Overlays.Remove("Roads Only");
                    }
                    if (Map.Overlays.Contains("Road Online")) {
                        Map.Overlays.Remove("Road Online");
                    }
                    if (Map.Overlays.Contains("Google")) {
                        Map.Overlays.Remove("Google");
                    }
                    if (Map.Overlays.Contains("Google1")) {
                        Map.Overlays.Remove("Google1");
                    }
                    if (Map.Overlays.Contains("None")) {
                        Map.Overlays.Remove("None");
                    }
                    if (googloverlay != null) {
                        Map.Overlays.Add(SelectedOverlay.Name, googloverlay);
                        Map.Overlays.MoveToBottom(SelectedOverlay.Name);
                        Map.Overlays[SelectedOverlay.Name].Refresh();
                    }
                }
            };
        }

        private void ZoomToLayerExtent() {
            if (fieldsLayer.InternalFeatures.Count > 0) {
                Map.CurrentExtent = fieldsLayer.GetBoundingBox();
                PointShape center = fieldsLayer.GetBoundingBox().GetCenterPoint();
                //Map.CenterAt(center);
                //mapextent = Map.CurrentExtent;
            }
            else {
                Map.CurrentExtent = extent;
                PointShape center = extent.GetCenterPoint();
                //Map.CenterAt(center);
                //mapextent = Map.CurrentExtent;
            }
            SnapToGoogleImagery();
        }

        private void LoadBingMapsOverlay() {
            if (!startedOutOnline) { return; }
            log.Info("CMGDisplay - Load map imagery");
            //IMapOverlay mapoverlay = new BingMapOverlay();
            //SelectedOverlay.NewOverlay();
            IMapOverlay mapoverlay = SelectedOverlay;
            Overlay bingOverlay = mapoverlay.Overlay;
            if (bingOverlay == null) { return; }
			if (Map.Overlays.Contains("Bing")) {
				Map.Overlays.Remove("Bing");
			}
            if (Map.Overlays.Contains("Bing Online")) {
                Map.Overlays.Remove("Bing Online");
            }
            if (Map.Overlays.Contains("Roads Only")) {
                Map.Overlays.Remove("Roads Only");
            }
			if (Map.Overlays.Contains("Road Online")) {
				Map.Overlays.Remove("Road Online");
			}
            if (Map.Overlays.Contains("Google")) {
                Map.Overlays.Remove("Google");
            }
            if (Map.Overlays.Contains("Google1")) {
                Map.Overlays.Remove("Google1");
            }
            if (Map.Overlays.Contains("None")) {
                Map.Overlays.Remove("None");
            }
            //bingOverlay.IsVisible = mapoverlay.Visibility;
            //((TileOverlay)bingOverlay).TileType = TileType.SingleTile;
            Map.Overlays.Add(mapoverlay.Name, bingOverlay);
            Map.Overlays.MoveToBottom(mapoverlay.Name);
            ZoomLevelSet zls = SelectedOverlay.GetZoomLevelSet();
            if (zls == null) {
                zoomlevelfactory = new ZoomLevelFactory("Google40");
                //zoomlevelfactory = new ZoomLevelFactory(mapsettings.ZoomLevel);
                Map.ZoomLevelSet = zoomlevelfactory.ZoomLevelSET;
            }
            else {
                Map.ZoomLevelSet = SelectedOverlay.GetZoomLevelSet();
            }

        }

        internal void EnsureFieldsLayerIsOpen() {
            if (fieldsLayer == null) {
                fieldsLayer = new InMemoryFeatureLayer();
            }

            if (!fieldsLayer.IsOpen) {
                fieldsLayer.Open();
            }

            if (!projection.IsOpen) {
                projection.Open();
            }
        }

        internal void EnsureAnnotationLayerIsOpen() {
            if (annotationLayer == null) {
                annotationLayer = new InMemoryFeatureLayer();
            }

            if (!annotationLayer.IsOpen) {
                annotationLayer.Open();
            }

            if (!projection.IsOpen) {
                projection.Open();
            }
        }

        private void RebuildMap() {
            //dispatcher.BeginInvoke(new Action(() => {
                log.Info("CMGDisplay - Load shapes");
                IsLoadingTree = true;
                var begin = DateTime.UtcNow;
                var years = clientEndpoint.GetView<FieldTreeView>(new DataSourceId(this.currentDataSourceId));
                fieldnamess = new Dictionary<string, string>();
                fieldnamess2 = new Dictionary<string, string>();
                fieldnameff = new Dictionary<string, string>();
                fieldnameff2 = new Dictionary<string, string>();
                if (displaycropzones) {
                    if (cropzoneLayerItems.Count == 0) {
                        var tree = clientEndpoint.GetView<CropTreeView>(new CropYearId(this.currentDataSourceId, this.currentCropYear)).GetValue(new CropTreeView());
                        var q = from c in tree.Crops
                                from f in c.Farms
                                from fi in f.Fields
                                from cz in fi.CropZones
                                let fieldname = fi.Name
                                select new { f.FarmId, fi.FieldId, fieldname, cz.CropZoneId, cz.Name };

                        IList<MapItem> czmaps = new List<MapItem>();
                        foreach (var item in q) {
                            var maps = clientEndpoint.GetView<ItemMap>(item.CropZoneId);
                            try {
                                if (maps.HasValue) {
                                    MapItem mi = new MapItem();
                                    mi.FarmId = item.FarmId.Id;
                                    mi.FieldId = item.FieldId.Id;
                                    mi.CropZoneId = item.CropZoneId.Id;
                                    mi.Name = item.Name;
                                    if (maps.Value.MostRecentMapItem != null) {
                                        mi.MapData = maps.Value.MostRecentMapItem.MapData;
                                        mi.MapDataType = maps.Value.MostRecentMapItem.DataType;
                                    }
                                    czmaps.Add(mi);
                                    if (!fieldnamess2.ContainsKey(mi.FieldId.ToString())) {
                                        //string customarealabel = GetCustomFieldName(new Guid(mi.FieldId.ToString()), item.fieldname);
                                        string customareaonlylabel = GetCustomAreaLabel(new Guid(mi.FieldId.ToString()), true);
                                        string customarealabel = DisplayLabelWithArea(item.fieldname, customareaonlylabel);
                                        fieldnamess2.Add(mi.FieldId.ToString(), customarealabel);
                                    }
                                    if (!fieldnameff2.ContainsKey(mi.FieldId.ToString())) {
                                        fieldnameff2.Add(mi.FieldId.ToString(), item.fieldname);
                                    }
                                }
                            }
                            finally { }
                        }
                        ClientLayer = czmaps;
                    }
                    else {
                        var tree = clientEndpoint.GetView<CropTreeView>(new CropYearId(this.currentDataSourceId, this.currentCropYear)).GetValue(new CropTreeView());
                        var q = from c in tree.Crops
                                from f in c.Farms
                                from fi in f.Fields
                                from cz in fi.CropZones
                                let fieldname = fi.Name
                                select new { f.FarmId, fi.FieldId, fieldname, cz.CropZoneId, cz.Name };

                        foreach (var item in q) {
                            var found = cropzoneLayerItems.Where(x => x.CropZoneId == item.CropZoneId.Id).ToList();
                            found.ForEach(x => {
                                if (!fieldnamess2.ContainsKey(item.FieldId.Id.ToString())) {
                                    //string customarealabel = GetCustomFieldName(new Guid(item.FieldId.Id.ToString()), item.fieldname);
                                    string customareaonlylabel = GetCustomAreaLabel(new Guid(item.FieldId.Id.ToString()), true);
                                    string customarealabel = DisplayLabelWithArea(item.fieldname, customareaonlylabel);
                                    fieldnamess2.Add(item.FieldId.Id.ToString(), customarealabel);
                                }
                                if (!fieldnameff2.ContainsKey(item.FieldId.Id.ToString())) {
                                    fieldnameff2.Add(item.FieldId.Id.ToString(), item.fieldname);
                                }
                            });
                        }

                        ClientLayer = cropzoneLayerItems;
                    }
                }
                else {
                    var yearItem = (years.HasValue && years.Value.CropYearList.ContainsKey(currentCropYear)) ? years.Value.CropYearList[currentCropYear] : new TreeViewYearItem();
                    ClientLayer = yearItem.Maps.Where(x => x.IsVisible).ToList();
                }
                //var userSettings = clientEndpoint.GetUserSettings();
                //var mapSettings = clientEndpoint.GetMapAnnotationSettings();
                var end = DateTime.UtcNow;
                IsLoadingTree = false;
                log.Debug("Tree load took " + (end - begin).TotalMilliseconds + "ms");
            //}));
        }

        private void RebuildMap2() {
            //dispatcher.BeginInvoke(new Action(() => {
            IsLoadingTree = true;
            var begin = DateTime.UtcNow;
            var years = clientEndpoint.GetView<FieldTreeView>(new DataSourceId(this.currentDataSourceId));
            fieldnamess = new Dictionary<string, string>();
            fieldnamess2 = new Dictionary<string, string>();
            fieldnameff = new Dictionary<string, string>();
            fieldnameff2 = new Dictionary<string, string>();
            if (displaycropzones) {
                if (cropzoneLayerItems.Count == 0) {
                    var tree = clientEndpoint.GetView<CropTreeView>(new CropYearId(this.currentDataSourceId, this.currentCropYear)).GetValue(new CropTreeView());
                    var q = from c in tree.Crops
                            from f in c.Farms
                            from fi in f.Fields
                            from cz in fi.CropZones
                            let fieldname = fi.Name
                            select new { f.FarmId, fi.FieldId, fieldname, cz.CropZoneId, cz.Name };

                    IList<MapItem> czmaps = new List<MapItem>();
                    foreach (var item in q) {
                        var maps = clientEndpoint.GetView<ItemMap>(item.CropZoneId);
                        try {
                            if (maps.HasValue) {
                                MapItem mi = new MapItem();
                                mi.FarmId = item.FarmId.Id;
                                mi.FieldId = item.FieldId.Id;
                                mi.CropZoneId = item.CropZoneId.Id;
                                mi.Name = item.Name;
                                if (maps.Value.MostRecentMapItem != null) {
                                    mi.MapData = maps.Value.MostRecentMapItem.MapData;
                                    mi.MapDataType = maps.Value.MostRecentMapItem.DataType;
                                }
                                czmaps.Add(mi);
                                if (!fieldnamess2.ContainsKey(mi.FieldId.ToString())) {
                                    //string customarealabel = GetCustomFieldName(new Guid(mi.FieldId.ToString()), item.fieldname);
                                    string customareaonlylabel = GetCustomAreaLabel(new Guid(mi.FieldId.ToString()), true);
                                    string customarealabel = DisplayLabelWithArea(item.fieldname, customareaonlylabel);
                                    fieldnamess2.Add(mi.FieldId.ToString(), customarealabel);
                                }
                                if (!fieldnameff2.ContainsKey(mi.FieldId.ToString())) {
                                    fieldnameff2.Add(mi.FieldId.ToString(), item.fieldname);
                                }
                            }
                        }
                        finally { }
                    }
                    ClientLayer = czmaps;
                }
                else {
                    var tree = clientEndpoint.GetView<CropTreeView>(new CropYearId(this.currentDataSourceId, this.currentCropYear)).GetValue(new CropTreeView());
                    var q = from c in tree.Crops
                            from f in c.Farms
                            from fi in f.Fields
                            from cz in fi.CropZones
                            let fieldname = fi.Name
                            select new { f.FarmId, fi.FieldId, fieldname, cz.CropZoneId, cz.Name };

                    foreach (var item in q) {
                        var found = cropzoneLayerItems.Where(x => x.CropZoneId == item.CropZoneId.Id).ToList();
                        found.ForEach(x => {
                            if (!fieldnamess2.ContainsKey(item.FieldId.Id.ToString())) {
                                //string customarealabel = GetCustomFieldName(new Guid(item.FieldId.Id.ToString()), item.fieldname);
                                string customareaonlylabel = GetCustomAreaLabel(new Guid(item.FieldId.Id.ToString()), true);
                                string customarealabel = DisplayLabelWithArea(item.fieldname, customareaonlylabel);
                                fieldnamess2.Add(item.FieldId.Id.ToString(), customarealabel);
                            }
                            if (!fieldnameff2.ContainsKey(item.FieldId.Id.ToString())) {
                                fieldnameff2.Add(item.FieldId.Id.ToString(), item.fieldname);
                            }
                        });
                    }

                    clientLayerItems = cropzoneLayerItems;
                    ReloadMapItems2();

                }
            }
            else {
                var yearItem = (years.HasValue && years.Value.CropYearList.ContainsKey(currentCropYear)) ? years.Value.CropYearList[currentCropYear] : new TreeViewYearItem();
                ClientLayer = yearItem.Maps.Where(x => x.IsVisible).ToList();
            }
            //var userSettings = clientEndpoint.GetUserSettings();
            //var mapSettings = clientEndpoint.GetMapAnnotationSettings();
            var end = DateTime.UtcNow;
            IsLoadingTree = false;
            log.Debug("Tree load took " + (end - begin).TotalMilliseconds + "ms");
            //}));
        }

        private void ReloadMapItems() {
            //simpleMarkerOverlay.Markers.Clear();
            log.Info("CMGDisplay - Reload map items");

            EnsureFieldsLayerIsOpen();
            UpdateFarmFilters();
            cropzonefeatures = new Dictionary<string, string>();
            cropzonenamess = new Dictionary<string, string>();
            cropzonefields = new Dictionary<string, string>();
            scalingTextStyle.RotationAngles = new Dictionary<string, double>();
            scalingTextStyle.LabelPositions = new Dictionary<string, PointShape>();
            fieldcollection = new GeoCollection<Feature>();
            fieldsLayer.InternalFeatures.Clear();
            foreach (var m in clientLayerItems) {
                if (m.MapData != null && !string.IsNullOrEmpty(m.MapData) && m.MapData != "MULTIPOLYGON EMPTY") {
                    Dictionary<string, string> columnvalues = new Dictionary<string, string>();
                    string cropzoneid = m.FieldId.ToString();
                    string cropzonelabel = m.Name;
                    if (!fieldnameff.ContainsKey(m.FieldId.ToString())) {
                        if (fieldnameff2.ContainsKey(m.FieldId.ToString())) {
                            cropzonelabel = fieldnameff2[m.FieldId.ToString()];
                            fieldnameff.Add(m.FieldId.ToString(), cropzonelabel);
                        }
                    }
                    else {
                        cropzonelabel = " ";
                    }
                    string fieldlabel = m.Name;
                    if (fieldnameff2.ContainsKey(m.FieldId.ToString())) {
                        fieldlabel = fieldnameff2[m.FieldId.ToString()];
                    }
                    if (displaycropzones) {
                        cropzoneid = m.CropZoneId.ToString();
                    }
                    if (CropzoneLabels.Count > 0) {
                        cropzoneid = m.CropZoneId.ToString();
                        if (CropzoneLabels.ContainsKey(cropzoneid)) {
                            cropzonelabel = CropzoneLabels[cropzoneid];
                        }
                    }
                    string czfarmname = " ";
                    if (farmNames.ContainsKey(m.FarmId.ToString())) {
                        czfarmname = farmNames[m.FarmId.ToString()];
                    }
                    string czfieldname = " ";
                    if (!fieldnamess.ContainsKey(m.FieldId.ToString())) {
                        if (fieldnamess2.ContainsKey(m.FieldId.ToString())) {
                            czfieldname = fieldnamess2[m.FieldId.ToString()];
                            fieldnamess.Add(m.FieldId.ToString(), czfieldname);
                        }
                    }
                    string cropzonearealabel = string.Empty;
                    string customareaonlylabel = string.Empty;
                    string customareaonlyczlabel = string.Empty;
                    customareaonlyczlabel = GetCustomAreaLabel(new Guid(m.CropZoneId.ToString()), false);
                    customareaonlylabel = GetCustomAreaLabel(new Guid(m.FieldId.ToString()), true);
                    if (displaycropzones) {
                        cropzonearealabel = DisplayLabelWithArea(m.Name, customareaonlylabel);

                        var labels = clientEndpoint.GetView<ItemMap>(new CropZoneId(ApplicationEnvironment.CurrentDataSourceId, new Guid(cropzoneid)));
                        if (labels.HasValue && labels.Value.MostRecentLabelItem != null) {
                            ItemLabelItem mrli = labels.Value.MostRecentLabelItem;
                            if (mrli != null && !mrli.OptionalVisibility) {
                                continue;
                            }
                            if (!string.IsNullOrWhiteSpace(mrli.SpatialLabelPosition)) {
                                PointShape ps = new PointShape(mrli.SpatialLabelPosition);
                                if (labelPositions.ContainsKey(cropzoneid)) {
                                    labelPositions[cropzoneid] = ps;
                                }
                                else {
                                    labelPositions.Add(cropzoneid, ps);
                                }
                            }
                            if (rotationAngles.ContainsKey(cropzoneid)) {
                                rotationAngles[cropzoneid] = (double)mrli.HorizontalLabelRotation;
                            }
                            else {
                                if (mrli.HorizontalLabelRotation != 0)
                                {
                                    rotationAngles.Add(cropzoneid, (double)mrli.HorizontalLabelRotation);
                                }
                            }
                        }

                    }
                    else {
                        cropzonearealabel = DisplayLabelWithArea(fieldlabel, customareaonlylabel);

                        var labels = clientEndpoint.GetView<ItemMap>(new FieldId(ApplicationEnvironment.CurrentDataSourceId, new Guid(cropzoneid)));
                        if (labels.HasValue && labels.Value.MostRecentLabelItem != null) {
                            ItemLabelItem mrli = labels.Value.MostRecentLabelItem;
                            if (mrli != null && !mrli.OptionalVisibility) {
                                continue;
                            }
                            if (!string.IsNullOrWhiteSpace(mrli.SpatialLabelPosition)) {
                                PointShape ps = new PointShape(mrli.SpatialLabelPosition);
                                if (labelPositions.ContainsKey(cropzoneid)) {
                                    labelPositions[cropzoneid] = ps;
                                }
                                else {
                                    labelPositions.Add(cropzoneid, ps);
                                }
                            }
                            if (rotationAngles.ContainsKey(cropzoneid)) {
                                rotationAngles[cropzoneid] = (double)mrli.HorizontalLabelRotation;
                            }
                            else {
                                if (mrli.HorizontalLabelRotation != 0)
                                {
                                    rotationAngles.Add(cropzoneid, (double)mrli.HorizontalLabelRotation);
                                }
                            }
                        }
                    }
                    cropzonearealabel = DisplayLabelWithArea(m.Name, customareaonlyczlabel);

                    columnvalues.Add(@"LdbLabel", cropzonelabel);
                    columnvalues.Add(@"Percent", "100");
                    columnvalues.Add(@"Farm", czfarmname);
                    columnvalues.Add(@"Field", fieldlabel);
                    columnvalues.Add(@"Cropzone", m.Name);
                    columnvalues.Add(@"AreaDisplay", cropzonearealabel);
                    columnvalues.Add(@"AreaLabel", czfieldname);
                    columnvalues.Add(@"AreaOnlyLabel", customareaonlylabel);
                    columnvalues.Add(@"AreaOnlyCZLabel", customareaonlyczlabel);
                    columnvalues.Add(@"FieldID", m.FieldId.ToString());
                    columnvalues.Add(@"CropZoneID", m.CropZoneId.ToString());
                    var f = new Feature(m.MapData, cropzoneid, columnvalues);
                    //if (m.Name == "Canela")
                    //{
                    //    int ljlkjlkj = 0;
                    //}

                    if (fieldsLayer.InternalFeatures.Contains(f)) { continue; }
                    fieldsLayer.InternalFeatures.Add(cropzoneid, f);
                    fieldcollection.Add(cropzoneid, f);
                    if (!cropzonefeatures.ContainsKey(cropzoneid)) {
                        cropzonefeatures.Add(cropzoneid, cropzonelabel);
                    }
                    if (!cropzonenamess.ContainsKey(cropzoneid)) {
                        cropzonenamess.Add(cropzoneid, m.Name);
                    }
                    if (!cropzonefields.ContainsKey(cropzoneid)) {
                        cropzonefields.Add(cropzoneid, czfieldname);
                    }

                    //Marker marker = new Marker(-95.2806, 38.9554);
                    //marker.ImageSource = new BitmapImage(new Uri("/Resources/AQUA.png", UriKind.RelativeOrAbsolute));
                    //marker.Width = 20;
                    //marker.Height = 34;
                    //marker.YOffset = -17;
                    //markerOverlay.Markers.Add(marker);
                }
            }

            scalingTextStyle.RotationAngles = rotationAngles;
            scalingTextStyle.LabelPositions = labelPositions;
            if (!datasoucrechanging && maploaded) {
                FilterBySelectedFarm(true);
            }
        }

        private void ReloadMapItems2() {
            //simpleMarkerOverlay.Markers.Clear();
            EnsureFieldsLayerIsOpen();
            UpdateFarmFilters();
            cropzonefeatures = new Dictionary<string, string>();
            cropzonenamess = new Dictionary<string, string>();
            cropzonefields = new Dictionary<string, string>();
            scalingTextStyle.RotationAngles = new Dictionary<string, double>();
            scalingTextStyle.LabelPositions = new Dictionary<string, PointShape>();
            fieldcollection = new GeoCollection<Feature>();
            fieldsLayer.InternalFeatures.Clear();
            foreach (var m in clientLayerItems) {
                if (m.MapData != null && !string.IsNullOrEmpty(m.MapData) && m.MapData != "MULTIPOLYGON EMPTY") {
                    Dictionary<string, string> columnvalues = new Dictionary<string, string>();
                    string cropzoneid = m.FieldId.ToString();
                    string cropzonelabel = m.Name;
                    if (!fieldnameff.ContainsKey(m.FieldId.ToString())) {
                        if (fieldnameff2.ContainsKey(m.FieldId.ToString())) {
                            cropzonelabel = fieldnameff2[m.FieldId.ToString()];
                            fieldnameff.Add(m.FieldId.ToString(), cropzonelabel);
                        }
                    }
                    else {
                        cropzonelabel = " ";
                    }
                    string fieldlabel = m.Name;
                    if (fieldnameff2.ContainsKey(m.FieldId.ToString())) {
                        fieldlabel = fieldnameff2[m.FieldId.ToString()];
                    }
                    if (displaycropzones) {
                        cropzoneid = m.CropZoneId.ToString();
                    }
                    if (CropzoneLabels.Count > 0) {
                        cropzoneid = m.CropZoneId.ToString();
                        if (CropzoneLabels.ContainsKey(cropzoneid)) {
                            cropzonelabel = CropzoneLabels[cropzoneid];
                        }
                    }
                    string czfarmname = " ";
                    if (farmNames.ContainsKey(m.FarmId.ToString())) {
                        czfarmname = farmNames[m.FarmId.ToString()];
                    }
                    string czfieldname = " ";
                    if (!fieldnamess.ContainsKey(m.FieldId.ToString())) {
                        if (fieldnamess2.ContainsKey(m.FieldId.ToString())) {
                            czfieldname = fieldnamess2[m.FieldId.ToString()];
                            fieldnamess.Add(m.FieldId.ToString(), czfieldname);
                        }
                    }
                    //string cropzonearealabel = GetCustomCropZoneName(new Guid(m.CropZoneId.ToString()), m.Name);
                    string customareaonlylabel = GetCustomAreaLabel(new Guid(m.FieldId.ToString()), true);
                    string customareaonlyczlabel = GetCustomAreaLabel(new Guid(m.CropZoneId.ToString()), false);
                    string cropzonearealabel = DisplayLabelWithArea(m.Name, customareaonlyczlabel);

                    columnvalues.Add(@"LdbLabel", cropzonelabel);
                    columnvalues.Add(@"Percent", "100");
                    columnvalues.Add(@"Farm", czfarmname);
                    columnvalues.Add(@"Field", fieldlabel);
                    columnvalues.Add(@"Cropzone", m.Name);
                    columnvalues.Add(@"AreaDisplay", cropzonearealabel);
                    columnvalues.Add(@"AreaLabel", czfieldname);
                    columnvalues.Add(@"AreaOnlyLabel", customareaonlylabel);
                    columnvalues.Add(@"AreaOnlyCZLabel", customareaonlyczlabel);
                    columnvalues.Add(@"FieldID", m.FieldId.ToString());
                    columnvalues.Add(@"CropZoneID", m.CropZoneId.ToString());
                    var f = new Feature(m.MapData, cropzoneid, columnvalues);
                    //if(m.Name == "Canela") {
                    //    int ljlkjlkj = 0;
                    //}
                    if (fieldsLayer.InternalFeatures.Contains(f)) { continue; }
                    if (fieldsLayer.InternalFeatures.Contains(cropzoneid)) { continue; }
                    fieldsLayer.InternalFeatures.Add(cropzoneid, f);
                    fieldcollection.Add(cropzoneid, f);
                    if (!cropzonefeatures.ContainsKey(cropzoneid)) {
                        cropzonefeatures.Add(cropzoneid, cropzonelabel);
                    }
                    if (!cropzonenamess.ContainsKey(cropzoneid)) {
                        cropzonenamess.Add(cropzoneid, m.Name);
                    }
                    if (!cropzonenamess.ContainsKey(cropzoneid)) {
                        cropzonenamess.Add(cropzoneid, czfieldname);
                    }
                    if (!cropzonefields.ContainsKey(cropzoneid)) {
                        cropzonefields.Add(cropzoneid, czfieldname);
                    }

                    if (displaycropzones) {
                        var labels = clientEndpoint.GetView<ItemMap>(new CropZoneId(ApplicationEnvironment.CurrentDataSourceId, new Guid(cropzoneid)));
                        if (labels.HasValue && labels.Value.MostRecentLabelItem != null) {
                            ItemLabelItem mrli = labels.Value.MostRecentLabelItem;
                            if (mrli != null && !mrli.OptionalVisibility) {
                                continue;
                            }
                            if (!string.IsNullOrWhiteSpace(mrli.SpatialLabelPosition)) {
                                PointShape ps = new PointShape(mrli.SpatialLabelPosition);
                                if (labelPositions.ContainsKey(cropzoneid)) {
                                    labelPositions[cropzoneid] = ps;
                                }
                                else {
                                    labelPositions.Add(cropzoneid, ps);
                                }
                            }
                            if (rotationAngles.ContainsKey(cropzoneid)) {
                                rotationAngles[cropzoneid] = (double)mrli.HorizontalLabelRotation;
                            }
                            else {
                                if (mrli.HorizontalLabelRotation != 0)
                                {
                                    rotationAngles.Add(cropzoneid, (double)mrli.HorizontalLabelRotation);
                                }
                            }
                        }
                    }
                    else {
                        var labels = clientEndpoint.GetView<ItemMap>(new FieldId(ApplicationEnvironment.CurrentDataSourceId, new Guid(cropzoneid)));
                        if (labels.HasValue && labels.Value.MostRecentLabelItem != null) {
                            ItemLabelItem mrli = labels.Value.MostRecentLabelItem;
                            if (mrli != null && !mrli.OptionalVisibility) {
                                continue;
                            }
                            if (!string.IsNullOrWhiteSpace(mrli.SpatialLabelPosition)) {
                                PointShape ps = new PointShape(mrli.SpatialLabelPosition);
                                if (labelPositions.ContainsKey(cropzoneid)) {
                                    labelPositions[cropzoneid] = ps;
                                }
                                else {
                                    labelPositions.Add(cropzoneid, ps);
                                }
                            }
                            if (rotationAngles.ContainsKey(cropzoneid)) {
                                rotationAngles[cropzoneid] = (double)mrli.HorizontalLabelRotation;
                            }
                            else {
                                if (mrli.HorizontalLabelRotation != 0)
                                {
                                    rotationAngles.Add(cropzoneid, (double)mrli.HorizontalLabelRotation);
                                }
                            }
                        }
                    }

                    //Marker marker = new Marker(-95.2806, 38.9554);
                    //marker.ImageSource = new BitmapImage(new Uri("/Resources/AQUA.png", UriKind.RelativeOrAbsolute));
                    //marker.Width = 20;
                    //marker.Height = 34;
                    //marker.YOffset = -17;
                    //markerOverlay.Markers.Add(marker);
                }
            }
            scalingTextStyle.RotationAngles = rotationAngles;
            scalingTextStyle.LabelPositions = labelPositions;
        }

        void UpdateFilters() {
            log.Info("CMGDisplay - Update filters");

            ObservableCollection<IMapFilter> filterelements2 = new ObservableCollection<IMapFilter>();
            filterelements2.Add(new BlankFilterItem(this, clientEndpoint));
            filterelements2.Add(new CropFilterItem(this, clientEndpoint));
            filterelements2.Add(new SeedVarietyFilterItem(this, clientEndpoint));
            filterelements2.Add(new CropProtectionFilterItem(this, clientEndpoint));
            filterelements2.Add(new FertilizerFilterItem(this, clientEndpoint));
            filterelements2.Add(new ServiceFilterItem(this, clientEndpoint));
            filterelements2.Add(new AILoadFilterItem(this, clientEndpoint));
            //filterelements2.Add(new SSurgoLayers(this, clientEndpoint));
            //filterelements2.Add(new REIPHIFilterItem(this));
            filterelements2.Add(new TagFilterItem(this, clientEndpoint));
            filterelements2.Add(new TagOrderedFilterItem(this, clientEndpoint));
            filterelements2.Add(new TagVarietyFilterItem(this, clientEndpoint));
            filterelements2.Add(new RentContractFilterItem(this, clientEndpoint));
            filterelements2.Add(new ProductionContractFilterItem(this, clientEndpoint));

            if (ApplicationEnvironment.CurrentDataSource.DatasourceCulture == "en-US") {
                filterelements2.Add(new FieldFsaFilterItem(this, clientEndpoint));
                filterelements2.Add(new CropFieldFsaFilterItem(this, clientEndpoint));
            }

            FilterElements = filterelements2;
            SelectedFilter = FilterElements[0];
        }

        void UpdateFarmFilters() {
            var years = clientEndpoint.GetView<FieldTreeView>(new DataSourceId(this.currentDataSourceId));
            var yearItem = (years.HasValue && years.Value.CropYearList.ContainsKey(currentCropYear)) ? years.Value.CropYearList[currentCropYear] : new TreeViewYearItem();
            IList<TreeViewFarmItem> farms2 = yearItem.Farms != null ? yearItem.Farms : new List<TreeViewFarmItem>();

            var farms = from f in farms2
                        orderby f.Name
                        select new { f.FarmId, f.Name, f.Fields };

            farmNames = new Dictionary<string, string>();
            var filterfarms2 = new ObservableCollection<CropMapsFarmDisplayItem>();
            filterfarms2.Add(new CropMapsFarmDisplayItem("All Farms"));
            foreach (var farm in farms) {
                if (!filterfarms2.Any(x => x.DisplayText == farm.Name)) {
                    filterfarms2.Add(new CropMapsFarmDisplayItem(farm.Name));
                    if (!farmNames.ContainsKey(farm.FarmId.Id.ToString())) {
                        farmNames.Add(farm.FarmId.Id.ToString(), farm.Name);
                    }
                }
            }
            FilterFarms = filterfarms2;
            SelectedFarm = FilterFarms.First(x => x.IsAllFarms);
        }

        void UpdatePositionFilters() {
            var tempFilterPosition =
                new ObservableCollection<AdornmentLocationDisplayItem> {
                    new AdornmentLocationDisplayItem(AdornmentLocation.UpperLeft),
                    new AdornmentLocationDisplayItem(AdornmentLocation.CenterLeft),
                    new AdornmentLocationDisplayItem(AdornmentLocation.LowerLeft),
                    new AdornmentLocationDisplayItem(AdornmentLocation.UpperCenter),
                    new AdornmentLocationDisplayItem(AdornmentLocation.Center),
                    new AdornmentLocationDisplayItem(AdornmentLocation.LowerCenter),
                    new AdornmentLocationDisplayItem(AdornmentLocation.UpperRight),
                    new AdornmentLocationDisplayItem(AdornmentLocation.CenterRight),
                    new AdornmentLocationDisplayItem(AdornmentLocation.LowerRight),

                };
            FilterPosition = tempFilterPosition;
            SelectedPosition = FilterPosition.First(x => x.AdornmentLocationEquals(AdornmentLocation.UpperLeft));
        }

        void SetCMGMapType(){
            log.Info("CMGTools - Toggle map imagery");
            IsOptionsMapPopupOpen = false;
            selectedoverlay = MAPOptionsViewModel.MapToggle(selectedoverlay.MapInfoSelectorItem, !selectedoverlay.OnlineOnly);
            LoadBingMapsOverlay();
            RebuildMap();
            if (fieldsLayer.InternalFeatures.Count > 0) {
                RefreshFieldsLayer(true, true);
            }
            var mapsettings = clientEndpoint.GetMapSettings();
            mapsettings.CMGMapInfoSelectorItem = selectedoverlay.MapInfoSelectorItem;
            clientEndpoint.SaveMapSettings(mapsettings);
        }

        void SetCMGLegend() {
            log.Info("CMGTools - Toggle legend");
            IsOptionsMapPopupOpen = false;
            MapSettings mapsettings2 = this.clientEndpoint.GetMapSettings();
            mapsettings2.DisplayCMGLegend = !mapsettings2.DisplayCMGLegend;
            this.clientEndpoint.SaveMapSettings(mapsettings2);
            RefreshImageryLayer();
        }

        void SetMapAnnotation() {
            log.Info("CMGTools - Toggle annotations");
            IsOptionsMapPopupOpen = false;
            MapSettings mapsettings2 = this.clientEndpoint.GetMapSettings();
            mapsettings2.CMGMapAnnotationsVisible = !mapsettings2.CMGMapAnnotationsVisible;
            this.clientEndpoint.SaveMapSettings(mapsettings2);
            RefreshImageryLayer();
        }

        void SetDisplayAllCropzones() {
            log.Info("CMGTools - Toggle display all cropzones");
            IsOptionsMapPopupOpen = false;
            cmgAreaStyle.DisplayAllCropzones = !cmgAreaStyle.DisplayAllCropzones;
            //MapSettings mapsettings2 = this.clientEndpoint.GetMapSettings();
            //mapsettings2.CMGMapAnnotationsVisible = !mapsettings2.CMGMapAnnotationsVisible;
            //this.clientEndpoint.SaveMapSettings(mapsettings2);
            RefreshImageryLayer();
        }

        public void RefreshImageryLayer() {
            refreshimagery = true;
            log.Info("CMGTools - Refresh imagery.");
            IsOptionsMapPopupOpen = false;
            mapsettings = clientEndpoint.GetMapSettings();
            //var a = mapsettings.BingMapVisible;
            //var d = mapsettings.DisplayCoordinateFormat;
            //var f = mapsettings.AreaDecimals;
            //var h = mapsettings.FieldsVisible;
            //var i = mapsettings.UnselectedOpacity;
            //var j = mapsettings.UnselectedVisible;
            //var k = mapsettings.MapAnnotationsVisible;
            //var kk = mapsettings.CMGMapAnnotationsVisible;
            //var l = mapsettings.ScalingTextVisible;
            //var o = mapsettings.FieldsSelectedFillColor;
            //var p = mapsettings.FieldsUnSelectedFillColor;
            bingmapvisible = mapsettings.BingMapVisible;
            fieldopacity = mapsettings.FieldsOpacity;
            fieldopacityfill = mapsettings.FieldsOpacity;
            cmgopacity = mapsettings.UnselectedOpacity;
            annotationopacity = mapsettings.MapAnnotationOpacity;
            displayLabels = mapsettings.ScalingTextVisible;
            if (mapsettings.DisplayFillColor.HasValue) {
                displayFillColor = mapsettings.DisplayFillColor.Value;
                if (displayFillColor) {
                    fieldopacityfill = mapsettings.FieldsOpacity;
                }
                else {
                    fieldopacityfill = 0;
                }
            }
            if (mapsettings.CMGMapAnnotationsVisible.HasValue) {
                annotationvisibility = mapsettings.CMGMapAnnotationsVisible.Value;
            }
            if (mapsettings.DisplayAreaInFieldLabel.HasValue) {
                displayareainfieldlabel = mapsettings.DisplayAreaInFieldLabel.Value;
            }
            if (mapsettings.DisplayAreaOnlyLabel.HasValue) {
                displayareaonlylabel = mapsettings.DisplayAreaOnlyLabel.Value;
            }
            if (mapsettings.CMGFieldOrCropzones.HasValue) {
                cmgfieldsorcropzones = mapsettings.CMGFieldOrCropzones.Value;
            }
            if (mapsettings.MapAnnotationLabelsVisible.HasValue) {
                mapannotationlabelsvisible = mapsettings.MapAnnotationLabelsVisible.Value;
            }
            if (mapsettings.DisplayCMGLegend.HasValue) {
                displaycmglegend = mapsettings.DisplayCMGLegend.Value;
            }
            displaymapzoombar = false;
            if (mapsettings.DisplayMapZoomBar.HasValue) {
                displaymapzoombar = mapsettings.DisplayMapZoomBar.Value;
            }
            if (!displaymapzoombar) {
                Map.MapTools.PanZoomBar.Visibility = System.Windows.Visibility.Collapsed;
            }
            //StyleData.Self.UserStyleNames = this.mapsettings.MapAnnotationCategories;
            bordersize = mapsettings.BorderSize;
            areadecimals = mapsettings.AreaDecimals;
            areaUnitString = mapsettings.MapAreaUnit;
            coordinateformat = mapsettings.DisplayCoordinateFormat;
            agcAreaUnit = UnitFactory.GetUnitByName(areaUnitString) as AgC.UnitConversion.Area.AreaUnit;
            mapAreaUnit = MapUnitFactory.AreaMeasureConversion(agcAreaUnit);
            mapDistanceUnit = MapUnitFactory.AreaDistanceMeasureConversion(agcAreaUnit);
            //selectedoverlay = MAPOptionsViewModel.MapFactory(mapsettings.CMGMapInfoSelectorItem);
            selectedoverlay = MAPOptionsViewModel.MapFactory(mapsettings.CMGMapInfoSelectorItem, mapsettings.AllowOfflineMap);
            //zoomlevelfactory = new ZoomLevelFactory(mapsettings.ZoomLevel);
            //Map.ZoomLevelSet = zoomlevelfactory.ZoomLevelSET;

            fieldsselectedfillcolor = mapsettings.FieldsSelectedFillColor;
            fieldsunselectedfillcolor = mapsettings.FieldsUnSelectedFillColor;
            if (!string.IsNullOrEmpty(fieldsselectedfillcolor)) {
                fieldsselectedGeoColor = GeoColor.FromHtml(fieldsselectedfillcolor);
            }
            if (!string.IsNullOrEmpty(fieldsunselectedfillcolor)) {
                fieldsunselectedGeoColor = GeoColor.FromHtml(fieldsunselectedfillcolor);
            }
            fieldsselectedbordercolor = mapsettings.FieldsSelectedBorderColor;
            fieldsunselectedbordercolor = mapsettings.FieldsUnSelectedBorderColor;
            if (!string.IsNullOrEmpty(fieldsselectedbordercolor)) {
                fieldsselectedborderGeoColor = GeoColor.FromHtml(fieldsselectedbordercolor);
            }
            else {
                fieldsselectedborderGeoColor = fieldsselectedGeoColor;
            }
            if (!string.IsNullOrEmpty(fieldsunselectedbordercolor)) {
                fieldsunselectedborderGeoColor = GeoColor.FromHtml(fieldsunselectedbordercolor);
            }
            else {
                fieldsunselectedborderGeoColor = fieldsunselectedGeoColor;
            }

            scalingTextStyle = new ColoredMapGeneratorScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, mapsettings.ScalingTextMaximumSize, this.mapsettings.ScalingTextMinimumSize);
            //scalingTextStyle.SetTexTColumnName(@"AreaLabel");
            if (displayareainfieldlabel) {
                if (cmgfieldsorcropzones) {
                    //scalingTextStyle.SetTexTColumnName(@"AreaLabel");
                    if (displayareaonlylabel) {
                        scalingTextStyle.SetTexTColumnName(@"AreaOnlyLabel");
                    }
                    else {
                        scalingTextStyle.SetTexTColumnName(@"AreaLabel");
                    }
                }
                else {
                    //scalingTextStyle.SetTexTColumnName(@"AreaDisplay");
                    if (displayareaonlylabel) {
                        scalingTextStyle.SetTexTColumnName(@"AreaOnlyCZLabel");
                    }
                    else {
                        scalingTextStyle.SetTexTColumnName(@"AreaDisplay");
                    }
                }
            }
            else {
                if (cmgfieldsorcropzones) {
                    scalingTextStyle.SetTexTColumnName(@"LdbLabel");
                }
                else {
                    scalingTextStyle.SetTexTColumnName(@"Cropzone");
                }
            }
            fittingpolygonfactor = mapsettings.FittingPolygonFactor;
            if (mapsettings.AllowLabelOverlapping.HasValue) {
                allowlabeloverlapping = mapsettings.AllowLabelOverlapping.Value;
            }
            if (mapsettings.FittingPolygon.HasValue) {
                fittingpolygon = mapsettings.FittingPolygon.Value;
            }
            scalingTextStyle.LabelAllPolygonParts = labelallpolygonparts;
            scalingTextStyle.FittingPolygon = fittingpolygon;
            scalingTextStyle.FittingPolygonFactor = fittingpolygonfactor;
            if (allowlabeloverlapping) {
                scalingTextStyle.OverlappingRule = LabelOverlappingRule.AllowOverlapping;
            }
            else {
                scalingTextStyle.OverlappingRule = LabelOverlappingRule.NoOverlapping;
            }
            scalingTextStyle2 = new AnnotationScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, mapsettings.ScalingTextMaximumSize, this.mapsettings.ScalingTextMinimumSize);
            scalingTextStyle2.ColumnsList = new List<string>() { @"Id", @"LdbLabel", @"TypeName", @"ShapeType" };
            //scalingTextStyle2.YOffsetInPixel = 20;

            scalingTextStyle.ColorStyles = colorstyles;
            cmgAreaStyle.ColorStyles = colorstyles;
            //cmgAreaStyle.UnselectedFeatures = new Dictionary<string, string>();
            //Map.CurrentScale;

            LoadBingMapsOverlay();
            RebuildMapAnnotations();

            fieldsLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Clear();
            fieldsLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(cmgAreaStyle);
            if (displayLabels) {
                fieldsLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(scalingTextStyle);
            }
            fieldsLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            RebuildMap();
            SetColorStyles();
            if (fieldsLayer.InternalFeatures.Count > 0) {
                scalingTextStyle = new ColoredMapGeneratorScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, mapsettings.ScalingTextMaximumSize, this.mapsettings.ScalingTextMinimumSize);
                RefreshFieldsLayer(true, true);
            }
            if (SelectedFarm.IsAllFarms && !string.IsNullOrEmpty(selectedfarm.DisplayText)) {
                SelectedFarm = selectedfarm;
                RefreshTheMap();
            }
            SnapToGoogleImagery();
            refreshimagery = false;
        }

        public void RefreshImageryLayer2() {
            refreshimagery = true;
            IsOptionsMapPopupOpen = false;
            mapsettings = clientEndpoint.GetMapSettings();
            bingmapvisible = mapsettings.BingMapVisible;
            fieldopacity = mapsettings.FieldsOpacity;
            fieldopacityfill = mapsettings.FieldsOpacity;
            cmgopacity = mapsettings.UnselectedOpacity;
            annotationopacity = mapsettings.MapAnnotationOpacity;
            displayLabels = mapsettings.ScalingTextVisible;
            if (mapsettings.DisplayFillColor.HasValue) {
                displayFillColor = mapsettings.DisplayFillColor.Value;
                if (displayFillColor) {
                    fieldopacityfill = mapsettings.FieldsOpacity;
                }
                else {
                    fieldopacityfill = 0;
                }
            }
            if (mapsettings.CMGMapAnnotationsVisible.HasValue) {
                annotationvisibility = mapsettings.CMGMapAnnotationsVisible.Value;
            }
            if (mapsettings.DisplayAreaInFieldLabel.HasValue) {
                displayareainfieldlabel = mapsettings.DisplayAreaInFieldLabel.Value;
            }
            if (mapsettings.DisplayAreaOnlyLabel.HasValue) {
                displayareaonlylabel = mapsettings.DisplayAreaOnlyLabel.Value;
            }
            if (mapsettings.CMGFieldOrCropzones.HasValue) {
                cmgfieldsorcropzones = mapsettings.CMGFieldOrCropzones.Value;
            }
            if (mapsettings.MapAnnotationLabelsVisible.HasValue) {
                mapannotationlabelsvisible = mapsettings.MapAnnotationLabelsVisible.Value;
            }
            if (mapsettings.DisplayCMGLegend.HasValue) {
                displaycmglegend = mapsettings.DisplayCMGLegend.Value;
            }
            displaymapzoombar = false;
            if (mapsettings.DisplayMapZoomBar.HasValue) {
                displaymapzoombar = mapsettings.DisplayMapZoomBar.Value;
            }
            if (!displaymapzoombar) {
                Map.MapTools.PanZoomBar.Visibility = System.Windows.Visibility.Collapsed;
            }
            //StyleData.Self.UserStyleNames = this.mapsettings.MapAnnotationCategories;
            bordersize = mapsettings.BorderSize;
            areadecimals = mapsettings.AreaDecimals;
            areaUnitString = mapsettings.MapAreaUnit;
            coordinateformat = mapsettings.DisplayCoordinateFormat;
            //if (coordinateformat == "Decimal Degree") {
            //    Map.MapTools.MouseCoordinate.MouseCoordinateType = MouseCoordinateType.DegreesMinutesSeconds;
            //}
            //else {
            //    Map.MapTools.MouseCoordinate.MouseCoordinateType = MouseCoordinateType.LatitudeLongitude;
            //}
            agcAreaUnit = UnitFactory.GetUnitByName(areaUnitString) as AgC.UnitConversion.Area.AreaUnit;
            mapAreaUnit = MapUnitFactory.AreaMeasureConversion(agcAreaUnit);
            mapDistanceUnit = MapUnitFactory.AreaDistanceMeasureConversion(agcAreaUnit);
            //selectedoverlay = MAPOptionsViewModel.MapFactory(mapsettings.CMGMapInfoSelectorItem);
            selectedoverlay = MAPOptionsViewModel.MapFactory(mapsettings.CMGMapInfoSelectorItem, mapsettings.AllowOfflineMap);
            fieldsselectedfillcolor = mapsettings.FieldsSelectedFillColor;
            fieldsunselectedfillcolor = mapsettings.FieldsUnSelectedFillColor;
            if (!string.IsNullOrEmpty(fieldsselectedfillcolor)) {
                fieldsselectedGeoColor = GeoColor.FromHtml(fieldsselectedfillcolor);
            }
            if (!string.IsNullOrEmpty(fieldsunselectedfillcolor)) {
                fieldsunselectedGeoColor = GeoColor.FromHtml(fieldsunselectedfillcolor);
            }
            fieldsselectedbordercolor = mapsettings.FieldsSelectedBorderColor;
            fieldsunselectedbordercolor = mapsettings.FieldsUnSelectedBorderColor;
            if (!string.IsNullOrEmpty(fieldsselectedbordercolor)) {
                fieldsselectedborderGeoColor = GeoColor.FromHtml(fieldsselectedbordercolor);
            }
            else {
                fieldsselectedborderGeoColor = fieldsselectedGeoColor;
            }
            if (!string.IsNullOrEmpty(fieldsunselectedbordercolor)) {
                fieldsunselectedborderGeoColor = GeoColor.FromHtml(fieldsunselectedbordercolor);
            }
            else {
                fieldsunselectedborderGeoColor = fieldsunselectedGeoColor;
            }

            scalingTextStyle = new ColoredMapGeneratorScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, mapsettings.ScalingTextMaximumSize, this.mapsettings.ScalingTextMinimumSize);
            //scalingTextStyle.SetTexTColumnName(@"AreaLabel");
            if (displayareainfieldlabel) {
                if (cmgfieldsorcropzones) {
                    //scalingTextStyle.SetTexTColumnName(@"AreaLabel");
                    if (displayareaonlylabel) {
                        scalingTextStyle.SetTexTColumnName(@"AreaOnlyLabel");
                    }
                    else {
                        scalingTextStyle.SetTexTColumnName(@"AreaLabel");
                    }
                }
                else {
                    //scalingTextStyle.SetTexTColumnName(@"AreaDisplay");
                    if (displayareaonlylabel) {
                        scalingTextStyle.SetTexTColumnName(@"AreaOnlyCZLabel");
                    }
                    else {
                        scalingTextStyle.SetTexTColumnName(@"AreaDisplay");
                    }
                }
            }
            else {
                if (cmgfieldsorcropzones) {
                    scalingTextStyle.SetTexTColumnName(@"LdbLabel");
                }
                else {
                    scalingTextStyle.SetTexTColumnName(@"Cropzone");
                }
            }
            fittingpolygonfactor = mapsettings.FittingPolygonFactor;
            if (mapsettings.AllowLabelOverlapping.HasValue) {
                allowlabeloverlapping = mapsettings.AllowLabelOverlapping.Value;
            }
            if (mapsettings.FittingPolygon.HasValue) {
                fittingpolygon = mapsettings.FittingPolygon.Value;
            }
            scalingTextStyle.LabelAllPolygonParts = labelallpolygonparts;
            scalingTextStyle.FittingPolygon = fittingpolygon;
            scalingTextStyle.FittingPolygonFactor = fittingpolygonfactor;
            if (allowlabeloverlapping) {
                scalingTextStyle.OverlappingRule = LabelOverlappingRule.AllowOverlapping;
            }
            else {
                scalingTextStyle.OverlappingRule = LabelOverlappingRule.NoOverlapping;
            }
            scalingTextStyle2 = new AnnotationScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, mapsettings.ScalingTextMaximumSize, this.mapsettings.ScalingTextMinimumSize);
            scalingTextStyle2.ColumnsList = new List<string>() { @"Id", @"LdbLabel", @"TypeName", @"ShapeType" };
            //scalingTextStyle2.YOffsetInPixel = 20;
            //Map.CurrentScale;
            scalingTextStyle.ColorStyles = colorstyles;
            cmgAreaStyle.ColorStyles = colorstyles;

            LoadBingMapsOverlay();
            RebuildMapAnnotations();

            fieldsLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Clear();
            fieldsLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(cmgAreaStyle);
            if (displayLabels) {
                fieldsLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(scalingTextStyle);
            }
            fieldsLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;

            RebuildMap2();
            SetColorStyles();
            if (fieldsLayer.InternalFeatures.Count > 0) {
                RefreshFieldsLayer(true, true);
            }
            refreshimagery = false;
        }

        private void CreateAnnotationMemoryLayer() {
            annotationLayer = new InMemoryFeatureLayer();
            annotationLayer.FeatureSource.Open();
            FeatureSourceColumn column1 = new FeatureSourceColumn(@"Id", "string", 64);
            FeatureSourceColumn column2 = new FeatureSourceColumn(@"LdbLabel", "string", 64);
            FeatureSourceColumn column3 = new FeatureSourceColumn(@"TypeName", "string", 64);
            FeatureSourceColumn column4 = new FeatureSourceColumn(@"ShapeType", "string", 64);
            annotationLayer.Columns.Add(column1);
            annotationLayer.Columns.Add(column2);
            annotationLayer.Columns.Add(column3);
            annotationLayer.Columns.Add(column4);
            annotationLayer.FeatureSource.Close();
            annotationLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = null;
            annotationLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = null;
            annotationLayer.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = null;
            annotationLayer.ZoomLevelSet.ZoomLevel01.DefaultTextStyle = null;
            annotationLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Clear();
            AreaStyle areastyle = new AreaStyle(new GeoPen(new GeoColor(100, GeoColor.StandardColors.Transparent), 2), new GeoSolidBrush(new GeoColor(60, GeoColor.StandardColors.Transparent)));
            LineStyle linestyle = new LineStyle(new GeoPen(new GeoColor(100, GeoColor.StandardColors.Transparent), 2));
            PointStyle pointstyle = new PointStyle(PointSymbolType.Circle, new GeoSolidBrush(new GeoColor(100, GeoColor.StandardColors.Transparent)), 10);
            customMultiStyles = new MapStyles.AnnotationMultiStyle(areastyle, linestyle, pointstyle, annotationopacity);
            customMultiStyles.ColumnsList = new List<string>() { @"Id", @"LdbLabel", @"TypeName", @"ShapeType" };
            customMultiStyles.Opacity = annotationopacity;
            customMultiStyles.MaximumLabelSize = this.mapsettings.ScalingTextMaximumSize;
            customMultiStyles.MinimumLabelSize = this.mapsettings.ScalingTextMinimumSize;
            customMultiStyles.MaximumScale = 19000;
            customMultiStyles.MinimumScale = 1200;

            annotationLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customMultiStyles);
            if (mapannotationlabelsvisible) {
                annotationLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(scalingTextStyle2);
            }
            annotationLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            annotationLayer.FeatureSource.Projection = projection;
        }


        private void RebuildMapAnnotations() {
            log.Info("CMGDisplay - Load annotations");
            var begin = DateTime.UtcNow;
            userstylenames = this.mapsettings.MapAnnotationCategories;
            //annotationopacity = this.mapsettings.MapAnnotationOpacity;
            customMultiStyles.Opacity = annotationopacity;
            if (userstylenames.Count > 0) {
                StyleData.Self.UserStyleNames = userstylenames;
                changedstyles = new List<AnnotationStyleItem>();
                foreach (KeyValuePair<string, MapAnnotationCategory> asi in userstylenames) {
                    AnnotationStyleItem annotatonstyle = styleFactory.GetStyle(asi.Value.Name, asi.Value.ShapeType);
                    annotatonstyle.Visible = asi.Value.Visible;
                    changedstyles.Add(annotatonstyle);
                }
                StyleData.Self.CustomStyles = changedstyles;
            }
            EnsureAnnotationLayerIsOpen();
            annotationLayer.InternalFeatures.Clear();
            if (!annotationvisibility) { return; }

            annotationLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = null;
            annotationLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = null;
            annotationLayer.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = null;
            annotationLayer.ZoomLevelSet.ZoomLevel01.DefaultTextStyle = null;
            annotationLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Clear();
            AreaStyle areastyle = new AreaStyle(new GeoPen(new GeoColor(100, GeoColor.StandardColors.Transparent), 2), new GeoSolidBrush(new GeoColor(60, GeoColor.StandardColors.Transparent)));
            LineStyle linestyle = new LineStyle(new GeoPen(new GeoColor(100, GeoColor.StandardColors.Transparent), 2));
            PointStyle pointstyle = new PointStyle(PointSymbolType.Circle, new GeoSolidBrush(new GeoColor(100, GeoColor.StandardColors.Transparent)), 10);
            customMultiStyles = new MapStyles.AnnotationMultiStyle(areastyle, linestyle, pointstyle, annotationopacity);
            customMultiStyles.ColumnsList = new List<string>() { @"Id", @"LdbLabel", @"TypeName", @"ShapeType" };
            customMultiStyles.Opacity = annotationopacity;
            customMultiStyles.MaximumLabelSize = this.mapsettings.ScalingTextMaximumSize;
            customMultiStyles.MinimumLabelSize = this.mapsettings.ScalingTextMinimumSize;
            customMultiStyles.MaximumScale = 19000;
            customMultiStyles.MinimumScale = 1200;
            annotationLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(customMultiStyles);
            scalingTextStyle2 = new AnnotationScalingTextStyle(GeographyUnit.DecimalDegree, @"LdbLabel", 19000, 1200, mapsettings.ScalingTextMaximumSize, this.mapsettings.ScalingTextMinimumSize);
            scalingTextStyle2.ColumnsList = new List<string>() { @"Id", @"LdbLabel", @"TypeName", @"ShapeType" };
            //scalingTextStyle2.YOffsetInPixel = 20;
            if (mapannotationlabelsvisible) {
                annotationLayer.ZoomLevelSet.ZoomLevel01.CustomStyles.Add(scalingTextStyle2);
            }
            annotationLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;

            var annotations = clientEndpoint.GetView<MapAnnotationListView>(new DataSourceId(ApplicationEnvironment.CurrentDataSourceId));
            List<MapAnnotationListItem> anlist = annotations.HasValue && annotations.Value.Annotations != null ? annotations.Value.Annotations : new List<MapAnnotationListItem>();
            IList<MapAnnotationDetailView> detaillist = new List<MapAnnotationDetailView>();
            IList<AnnotationStyleItem> stylelist = new List<AnnotationStyleItem>();
            foreach (var m in anlist) {
                if (m.Name != null && !string.IsNullOrEmpty(m.Name)) {
                    try {
                        var shapeItemMaybe = clientEndpoint.GetView<MapAnnotationDetailView>(m.Id);
                        MapAnnotationId mapAnnotationId = new MapAnnotationId();
                        string annotationName = string.Empty;
                        string annotationWktData = string.Empty;
                        string annotationShapeType = string.Empty;
                        string annotationType = string.Empty;
                        int? annotationCropYear = null;
                        shapeItemMaybe.IfValue(annotation => {
                            detaillist.Add(annotation);
                            string anid = annotation.Id.Id.ToString();
                            mapAnnotationId = annotation.Id;
                            annotationName = annotation.Name;
                            annotationWktData = annotation.WktData;
                            annotationShapeType = annotation.ShapeType;
                            annotationType = annotation.AnnotationType;
                            annotationCropYear = annotation.CropYear;
                            IDictionary<string, string> columnvalues = new Dictionary<string, string>();
                            columnvalues.Add(@"Id", anid);
                            columnvalues.Add(@"LdbLabel", annotationName);
                            columnvalues.Add(@"TypeName", annotationType);
                            columnvalues.Add(@"ShapeType", annotationShapeType);
                            Feature newfeature = new Feature(annotationWktData, anid, columnvalues);
                            //annotationLayer.InternalFeatures.Add(anid, newfeature);
                            var f = new Feature(annotationWktData, mapAnnotationId.Id.ToString(), columnvalues);
                            if (annotationLayer.InternalFeatures.Contains(anid)) {
                                annotationLayer.InternalFeatures.Remove(anid);
                            }
                            if (!annotationLayer.InternalFeatures.Contains(anid)) {
                                annotationLayer.InternalFeatures.Add(anid, newfeature);
                            }
                        });

                    }
                    catch { }
                }
            }
            var end = DateTime.UtcNow;
            log.Debug("Annotation Shapes load took " + (end - begin).TotalMilliseconds + "ms");
        }

            void ZoomToFieldsExtent(){
            log.Info("CMGTools - Zoom to shape extents");
            IsOptionsMapPopupOpen = false;
                ZoomToLayerExtent();
                FilterBySelectedFarm(true);
            }

        public void SnapToGoogleImagery() {
            if (SelectedOverlay != null && SelectedOverlayIsGoogle()) {
                ShowGoogleRadarCommands = true;
            }
            else {
                ShowGoogleRadarCommands = false;
            }

            if (SelectedOverlay != null && SelectedOverlayIsGoogle()) {
                double googlescale = zoomlevelfactory.SnapToGoogleZoomLevelScale(Map.CurrentScale);
                Map.ZoomToScale(googlescale);
            }
        }

        void ToggleOptionsVisibility() {
                log.Info("CMGTools - Toggle map options");
                IsOptionsMapPopupOpen = false;

            if (!SelectedOverlayIsGoogle() && this.mapsettings.CMGMapInfoSelectorItem.IsGoogle()) {
                this.mapsettings.MapInfoSelectorItem = this.mapsettings.CMGMapInfoSelectorItem;
                this.clientEndpoint.SaveMapSettings(this.mapsettings);
                var vm = new Landdb.ViewModel.Overlays.DataSourceMapZoomLevelChangeConfirmationViewModel(clientEndpoint);
                ScreenDescriptor sd = new ScreenDescriptor("Landdb.Views.Overlays.DataSourceMapZoomLevelChangeConfirmationView", "zoomlevelChangeConfirm", vm);
                Messenger.Default.Send<ShowOverlayMessage>(new ShowOverlayMessage() { ScreenDescriptor = sd });
                return;
            }

            if (SelectedOverlayIsGoogle() && !this.mapsettings.CMGMapInfoSelectorItem.IsGoogle()) {
                this.mapsettings.MapInfoSelectorItem = this.mapsettings.CMGMapInfoSelectorItem;
                this.clientEndpoint.SaveMapSettings(this.mapsettings);
                var vm = new Landdb.ViewModel.Overlays.DataSourceMapZoomLevelChangeConfirmationViewModel(clientEndpoint);
                ScreenDescriptor sd = new ScreenDescriptor("Landdb.Views.Overlays.DataSourceMapZoomLevelChangeConfirmationView", "zoomlevelChangeConfirm", vm);
                Messenger.Default.Send<ShowOverlayMessage>(new ShowOverlayMessage() { ScreenDescriptor = sd });
                return;
            }
            //mapOptionsViewModel.RebootRequired = false;

            if (MAPOptionsVisibility == Visibility.Collapsed) {
                    MAPOptionsViewModel.RefreshMapSettings(ApplicationEnvironment.ShowPremiumMap);
                    MAPOptionsVisibility = Visibility.Visible;
                }
                else {
                    MAPOptionsVisibility = Visibility.Collapsed;
                    RefreshImageryLayer();
                }
            }

            void ToggleTreeVisibility() {
                IsOptionsMapPopupOpen = false;
                if (MAPTreeVisibility == Visibility.Collapsed) {
                    MAPTreeVisibility = Visibility.Visible;
                }
                else {
                    MAPTreeVisibility = Visibility.Collapsed;

                    //var years = clientEndpoint.GetView<FieldTreeView>(new DataSourceId(this.currentDataSourceId));
                    //var yearItem = (years.HasValue && years.Value.CropYearList.ContainsKey(currentCropYear)) ? years.Value.CropYearList[currentCropYear] : new TreeViewYearItem();
                    //IList<TreeViewFarmItem> farms = yearItem.Farms != null ? yearItem.Farms : new List<TreeViewFarmItem>();
                    //mapViewModel.ClientLayer = yearItem.Maps.Where(m => m.IsVisible).ToList();


                    //IList<MapItem> ClientLayer =
                    //RefreshImageryLayer();
                }
            }

            void ExportSelectedShapeFiles() {
                log.Info("CMGTools - Export shape");
                IsOptionsMapPopupOpen = false;
                IList<MapItem> exportItems = new List<MapItem>();
                foreach (MapItem item in clientLayerItems) {
                    if (colorstyles.ContainsKey(item.CropZoneId.ToString())) {
                        exportItems.Add(item);
                    }
                }
                //SelectedShapeExport sse = new SelectedShapeExport(clientEndpoint, projection, "WorkOrderNumber", true);
                SelectedShapeExport sse = new SelectedShapeExport(clientEndpoint, projection, "CMG", "", true);
                sse.ExportShapeFile(exportItems);
            }

        void ExportAllCropzones() {
            log.Info("CMGTools - Export shape");
            IsOptionsMapPopupOpen = false;
            SelectedShapeExport sse = new SelectedShapeExport(clientEndpoint, projection, "CMG", "", true);
            sse.ExportShapeSSurgoFile(fieldsLayer);
        }

        void ChangeFieldSelection() {
            log.Info("CMGTools - Change field selections");
            IsOptionsMapPopupOpen = false;
            cmgAreaStyle.UnselectedFeatures = new Dictionary<string, string>();
            unselectedFeatures = new Dictionary<string, string>();
            chosenFeatures = new Dictionary<string, string>();
            changeFields = new ChangeCMGFieldsViewModel(clientEndpoint, dispatcher, ApplicationEnvironment.CurrentCropYear, clientLayerItems, this);
            var sd = new ScreenDescriptor("Landdb.Views.Production.Popups.ChangeCMGCropZoneSelectionView", "changeCropzones", changeFields);
            Messenger.Default.Send(new ShowOverlayMessage() { ScreenDescriptor = sd });
        }

            void PrintMap() {
                log.Info("CMGTools - Print map");
                IsOptionsMapPopupOpen = false;
                //if (detail != null) {
                printmapvm = new PrintMapToolViewModel();
                printmapvm.FieldsLayer = fieldsLayer;
                printmapvm.AnnotationLayer = annotationLayer;
                if (Map.Overlays.Contains("Bing")) {
                    printmapvm.BingMapsMaptype = BingMapsMapType.AerialWithLabels;
                }
                if (Map.Overlays.Contains("Bing Online")) {
                    printmapvm.BingMapsMaptype = BingMapsMapType.AerialWithLabels;
                }
                if (Map.Overlays.Contains("Roads Only")) {
                    printmapvm.BingMapsMaptype = BingMapsMapType.Road;
                }
                if (Map.Overlays.Contains("Road Online")) {
                    printmapvm.BingMapsMaptype = BingMapsMapType.Road;
                }
                if (Map.Overlays.Contains("None")) {
                    printmapvm.BingMapsMaptype = BingMapsMapType.Road;
                }
                printmapvm.PreviousMapExtent = Map.CurrentExtent;
                printmapvm.ColorItems = selectedfilter.ColorItems;
                var sd = new ScreenDescriptor("Landdb.Views.Secondary.Map.PrintMapToolView", "printColoredMap", printmapvm);
                Messenger.Default.Send(new ShowOverlayMessage() { ScreenDescriptor = sd });
                //}
            }

        //private string GetCustomFieldName(Guid Id, string name) {
        //    string stringid = Id.ToString();
        //    string labelname = name;
        //    var fieldMaybe = clientEndpoint.GetView<FieldDetailsView>(new FieldId(ApplicationEnvironment.CurrentDataSourceId, Id));
        //    fieldMaybe.IfValue(field => {
        //        if (field.ReportedArea != null && !string.IsNullOrWhiteSpace(field.ReportedAreaUnit)) {
        //            labelname = DisplaySelectedAreaDecimals(name, field.ReportedArea.Value, field.ReportedAreaUnit);
        //        }
        //        else if (field.BoundaryArea != null && !string.IsNullOrWhiteSpace(field.BoundaryAreaUnit)) {
        //            labelname = DisplaySelectedAreaDecimals(name, field.BoundaryArea.Value, field.BoundaryAreaUnit);
        //        }
        //        else {
        //            labelname = name + " " + UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(0);
        //        }

        //    });
        //    return labelname;
        //}

        //private string GetCustomCropZoneName(Guid Id, string name) {
        //    string stringid = Id.ToString();
        //    string labelname = name;
        //    var fieldMaybe = clientEndpoint.GetView<CropZoneDetailsView>(new CropZoneId(ApplicationEnvironment.CurrentDataSourceId, Id));
        //    fieldMaybe.IfValue(field => {
        //        if (field.ReportedArea != null && !string.IsNullOrWhiteSpace(field.ReportedAreaUnit)) {
        //            labelname = DisplaySelectedAreaDecimals(name, field.ReportedArea.Value, field.ReportedAreaUnit);
        //        }
        //        else if (field.BoundaryArea != null && !string.IsNullOrWhiteSpace(field.BoundaryAreaUnit)) {
        //            labelname = DisplaySelectedAreaDecimals(name, field.BoundaryArea.Value, field.BoundaryAreaUnit);
        //        }
        //        else {
        //            labelname = name + " " + UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(0);
        //        }

        //    });
        //    return labelname;
        //}

        //private string DisplaySelectedAreaDecimals(string name, double unitvalue, string unitname) {
        //    IUnit _unit = UnitFactory.GetUnitByName(unitname);
        //    string decimalstring = "N" + areadecimals.ToString("N0");
        ////  + "\r\n" + 
        //return name + "\r\n" + unitvalue.ToString(decimalstring);
        ////return name + ": " + unitvalue.ToString(decimalstring) + " " + _unit.AbbreviatedDisplay;
        //}

        private string DisplayLabelWithArea(string name, string customarealabel) {
            return name + "\r\n" + customarealabel;
        }

        private string GetCustomAreaLabel(Guid Id, bool isfieldlabel) {
            string stringid = Id.ToString();
            string labelname = string.Empty;
            if (isfieldlabel) {
                var fieldMaybe = this.clientEndpoint.GetView<FieldDetailsView>(new FieldId(ApplicationEnvironment.CurrentDataSourceId, Id));
                fieldMaybe.IfValue(field => {
                    labelname = string.Empty;
                    if (field.ReportedArea != null && !string.IsNullOrWhiteSpace(field.ReportedAreaUnit)) {
                        labelname = DisplaySelectedAreaOnlyDecimals(field.ReportedArea.Value, field.ReportedAreaUnit);
                    }
                    else if (field.BoundaryArea != null && !string.IsNullOrWhiteSpace(field.BoundaryAreaUnit)) {
                        labelname = DisplaySelectedAreaOnlyDecimals(field.BoundaryArea.Value, field.BoundaryAreaUnit);
                    }
                });
            }
            else {
                var fieldMaybe = this.clientEndpoint.GetView<CropZoneDetailsView>(new CropZoneId(ApplicationEnvironment.CurrentDataSourceId, Id));
                fieldMaybe.IfValue(field => {
                    labelname = string.Empty;
                    if (field.ReportedArea != null && !string.IsNullOrWhiteSpace(field.ReportedAreaUnit)) {
                        labelname = DisplaySelectedAreaOnlyDecimals(field.ReportedArea.Value, field.ReportedAreaUnit);
                    }
                    else if (field.BoundaryArea != null && !string.IsNullOrWhiteSpace(field.BoundaryAreaUnit)) {
                        labelname = DisplaySelectedAreaOnlyDecimals(field.BoundaryArea.Value, field.BoundaryAreaUnit);
                    }
                });
            }
            return labelname;
        }

        private string DisplaySelectedAreaOnlyDecimals(double unitvalue, string unitname) {
            IUnit _unit = UnitFactory.GetUnitByName(unitname);
            string decimalstring = "N" + areadecimals.ToString("N0");
            return unitvalue.ToString(decimalstring);
        }
        private Measure GetCropZoneArea(Guid Id) {
                string stringid = Id.ToString();
                Measure resultarea = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(0);
                var fieldMaybe = clientEndpoint.GetView<CropZoneDetailsView>(new CropZoneId(ApplicationEnvironment.CurrentDataSourceId, Id));
                fieldMaybe.IfValue(field => {
                    if (field.ReportedArea != null && !string.IsNullOrWhiteSpace(field.ReportedAreaUnit)) {
                        resultarea = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(field.ReportedArea.Value);
                    }
                    else if (field.BoundaryArea != null && !string.IsNullOrWhiteSpace(field.BoundaryAreaUnit)) {
                        resultarea = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(field.BoundaryArea.Value);
                    }
                    else {
                        resultarea = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(0);
                    }
                });
                return resultarea;
            }

        private void BuildLegend() {
            try
            {
                log.Info("CMGDisplay - Build legend");
                for (int o = Map.Overlays.Count - 1; o > 0; o--)
                {
                    if (Map.Overlays[o] is AdornmentOverlay)
                    {
                        Map.Overlays.RemoveAt(o);
                    }
                }
                //SelectedFilter.filterf(fieldsLayer, SelectedFarm, IsLegendVisible);
                legendLayer = SelectedFilter.BuildLegend(fieldsLayer, SelectedFarm, IsLegendVisible, chosenFeatures);
                if (legendLayer == null) { return; }
                legendLayer.Location = SelectedPosition.AdornmentLocation;
                if (SelectedPosition.Equals(AdornmentLocation.UpperLeft) || SelectedPosition.Equals(AdornmentLocation.CenterLeft))
                {
                    legendLayer.XOffsetInPixel = 0;
                    legendLayer.YOffsetInPixel = 0;
                }
                if (SelectedPosition.Equals(AdornmentLocation.LowerLeft))
                {
                    legendLayer.XOffsetInPixel = 100;
                    legendLayer.YOffsetInPixel = 0;
                }
                if (SelectedPosition.Equals(AdornmentLocation.UpperRight) || 
                    SelectedPosition.Equals(AdornmentLocation.CenterRight) || 
                    SelectedPosition.Equals(AdornmentLocation.LowerRight) || 
                    SelectedPosition.Equals(AdornmentLocation.UpperCenter) || 
                    SelectedPosition.Equals(AdornmentLocation.Center) || 
                    SelectedPosition.Equals(AdornmentLocation.LowerCenter))
                {
                    legendLayer.XOffsetInPixel = -100;
                    legendLayer.YOffsetInPixel = 0;
                }
                if (!displaymapzoombar && SelectedPosition.Equals(AdornmentLocation.CenterRight))
                {
                    legendLayer.XOffsetInPixel = 0;
                    legendLayer.YOffsetInPixel = 0;
                }

                if (!IsLegendVisible) { return; }
                if (legendLayer == null) { return; }
                if (legendLayer.LegendItems.Count == 0) { return; }

                legendLayer.IsVisible = displaycmglegend;

            adornmentOverlay = new AdornmentOverlay();
            adornmentOverlay.DrawingExceptionMode = DrawingExceptionMode.DrawException;
            if (!adornmentOverlay.Layers.Contains("LegendLayer")) {
                adornmentOverlay.Layers.Add("LegendLayer", legendLayer);
            }

                if (!Map.Overlays.Contains(adornmentOverlay))
                {
                    Map.Overlays.Add(adornmentOverlay);
                }
            } catch (Exception ex)
            {
                int i = 0;
            }
        }

        private void DetermineLegendWitdh(LegendAdornmentLayer legendLayer, CustomLegendItem legendItem) {
            SetLegendItemWidth(legendItem);

            if (legendItem.Width > legendLayer.Width) {
                legendLayer.Width = legendItem.Width;
            }
        }

        private static void SetLegendItemWidth(CustomLegendItem legendItem) {
            var fudgeFactor = 10;
            var lineWidth = (int)new GdiPlusGeoCanvas().MeasureText(legendItem.TextStyle.TextColumnName, legendItem.TextStyle.Font).Width;
            var totalImageLength = legendItem.ImageWidth + legendItem.ImageLeftPadding + legendItem.ImageRightPadding;
            var totalTextLength = legendItem.TextLeftPadding + legendItem.TextRightPadding + lineWidth;
            legendItem.Width = totalImageLength + totalTextLength + fudgeFactor;
        }



        private void FilterBySelectedFarm(bool buildlegendandrefresh) {
            log.Info("CMGDisplay - Filter by selected farm {0}", selectedfarm);
            cmgAreaStyle.SelectedFarm = selectedfarm;
            scalingTextStyle.SelectedFarm = selectedfarm;
            if (fieldsLayer.InternalFeatures.Count > 0) {
                if (selectedfarm.IsAllFarms || selectedfarm.DisplayText == string.Empty) {
                    lock (fieldsLayer) {
                        EnsureFieldsLayerIsOpen();
                        try {
                            Map.CurrentExtent = fieldsLayer.GetBoundingBox();
                            SnapToGoogleImagery();
                        }
                        catch { }
                    }
                }
                else {
                    lock (fieldsLayer) {
                        EnsureFieldsLayerIsOpen();
                        try {
                            Collection<Feature> selectedFeatures = fieldsLayer.QueryTools.GetFeaturesByColumnValue(@"Farm", selectedfarm.DisplayText, ReturningColumnsType.AllColumns);
                            Map.CurrentExtent = ExtentHelper.GetBoundingBoxOfItems(selectedFeatures);
                            SnapToGoogleImagery();
                        }
                        catch (Exception ex) {
                            log.ErrorException("Caught an exception while filtering selected farm", ex);
                            //System.Windows.MessageBox.Show("ImportTools - Caught an exception while working with a simple draw shape.");
                        }

                    }
                }
                if (fieldsLayer.InternalFeatures.Count > 0) {
                    RefreshFieldsLayer(false, buildlegendandrefresh);
                }
            }
        }

        public void RefreshFieldsLayer(bool setboundingboxextent, bool buildlegendandrefresh) {
            try {
                scalingTextStyle.MapScale = Map.CurrentScale;
                scalingTextStyle.MinimumLabelSize = mapsettings.ScalingTextMinimumSize;
                scalingTextStyle.MaximumLabelSize = mapsettings.ScalingTextMaximumSize;
                scalingTextStyle2.MapScale = Map.CurrentScale;
                scalingTextStyle2.MinimumLabelSize = mapsettings.ScalingTextMinimumSize;
                scalingTextStyle2.MaximumLabelSize = mapsettings.ScalingTextMaximumSize;
                scalingTextStyle4.MapScale = Map.CurrentScale;
                scalingTextStyle4.MinimumLabelSize = mapsettings.ScalingTextMinimumSize;
                scalingTextStyle4.MaximumLabelSize = mapsettings.ScalingTextMaximumSize;
                if (setboundingboxextent) {
                    Map.CurrentExtent = fieldsLayer.GetBoundingBox();
                    SnapToGoogleImagery();
                }
                if (maploaded) {
                    FormulateProperLabels();
                    if (buildlegendandrefresh) {
                        BuildLegend();
                        RefreshTheMap();
                    }
                }
        }
            catch { }
        }

        private void FormulateProperLabels() {
            try {
                if (selectedfilter.Filter == FilterType.Blank) { return; }
                if (displaycropzones) {
                    //if (selectedfilter.Filter == FilterType.Crop) {
                    fieldcollection = new GeoCollection<Feature>();
                    EnsureFieldsLayerIsOpen();
                    Dictionary<string, string> fieldslabels = new Dictionary<string, string>();
                    for (int i = 0; i < fieldsLayer.InternalFeatures.Count; i++) {
                        string featureid = fieldsLayer.InternalFeatures[i].Id.ToString();
                        string farmName = fieldsLayer.InternalFeatures[i].ColumnValues[@"Farm"];
                        string fieldid = fieldsLayer.InternalFeatures[i].ColumnValues[@"FieldID"];
                        string cropzoneid = fieldsLayer.InternalFeatures[i].ColumnValues[@"CropZoneID"];
                        fieldsLayer.InternalFeatures[i].ColumnValues[@"LdbLabel"] = " ";
                        fieldsLayer.InternalFeatures[i].ColumnValues[@"AreaLabel"] = " ";
                        string fieldarealabel = DisplayLabelWithArea(fieldsLayer.InternalFeatures[i].ColumnValues[@"Field"], fieldsLayer.InternalFeatures[i].ColumnValues[@"AreaOnlyLabel"]);
                        if (cropzonecolordict.ContainsKey(featureid)) {
                            if (selectedfarm.IsAllFarms || selectedfarm.DisplayText == string.Empty || selectedfarm.DisplayText == farmName) {
                                for (int j = 0; j < coloritems.Count; j++) {
                                    ColorItem ci = coloritems[j];
                                    if(!ci.Visible) { continue; }
                                    if (!fieldslabels.ContainsKey(fieldid)) {
                                        fieldslabels.Add(fieldid, fieldid);
                                        fieldsLayer.InternalFeatures[i].ColumnValues[@"LdbLabel"] = fieldsLayer.InternalFeatures[i].ColumnValues[@"Field"];
                                        fieldsLayer.InternalFeatures[i].ColumnValues[@"AreaLabel"] = fieldarealabel;
                                    }
                                    //if (ci.MapColor.R == cropzonecolordict[featureid].MapColor.R && ci.MapColor.G == cropzonecolordict[featureid].MapColor.G && ci.MapColor.B == cropzonecolordict[featureid].MapColor.B && ci.HatchType == cropzonecolordict[featureid].HatchType) {
                                    //    //Dictionary<string, string> fieldslabels = new Dictionary<string, string>();
                                    //    if (!fieldslabels.ContainsKey(fieldid)) {
                                    //        fieldslabels.Add(fieldid, fieldid);
                                    //        fieldsLayer.InternalFeatures[i].ColumnValues[@"LdbLabel"] = fieldsLayer.InternalFeatures[i].ColumnValues[@"Field"];
                                    //        fieldsLayer.InternalFeatures[i].ColumnValues[@"AreaLabel"] = fieldsLayer.InternalFeatures[i].ColumnValues[@"AreaDisplay"];
                                    //    }
                                    //}
                                }
                            }
                        }
                        fieldcollection.Add(fieldsLayer.InternalFeatures[i].Id, fieldsLayer.InternalFeatures[i]);
                    }
                    int q = fieldcollection.Count;
                }
            } catch (Exception ex) {
                int x = 0;
            }
        }

        public void RefreshTheMap() {
            try
            {
                Map.Refresh();
            }
            catch (Exception ex)
            {
                int i = 0;
            }
        }
    }
}
