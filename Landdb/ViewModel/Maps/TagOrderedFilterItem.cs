﻿using Landdb.Resources;
using AgC.UnitConversion;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using NLog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Windows.Threading;
using ThinkGeo.MapSuite.Core;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Map;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.ViewModel.Fields;
using Landdb.MapStyles;
using System.Windows.Data;
using Landdb.Client.Infrastructure.DisplayItems;

namespace Landdb.ViewModel.Maps {
    public class TagOrderedFilterItem : ViewModelBase, IMapFilter {
        MapsPageViewModel mapspage;
        readonly Logger log = LogManager.GetCurrentClassLogger();
        Guid currentDataSourceId = Guid.Empty;
        int currentCropYear = 0;
        string name = string.Empty;
        bool showthetoggle = true;
        ColorItem selectedcoloritem = null;
        int colormultiplier = 50;
        int randomchoice = 5;
        IList<Color> usedcolors = new List<Color>();
        ObservableCollection<ColorItem> coloritems = new ObservableCollection<ColorItem>();
        Dictionary<string, string> cropzonefarm = new Dictionary<string, string>();
        Dictionary<string, string> cropzonecrops = new Dictionary<string, string>();
        Dictionary<string, Color> cropzonecolors = new Dictionary<string, Color>();
        Dictionary<string, ColorItem> coloritemsindex = new Dictionary<string, ColorItem>();
        Dictionary<string, ColorItem> cropzonecolordict = new Dictionary<string, ColorItem>();
        Dictionary<string, List<string>> cropzonetags = new Dictionary<string, List<string>>();
        Dictionary<string, List<ColorItem>> cropzoneorderedcolordict = new Dictionary<string, List<ColorItem>>();
        ObservableCollection<string> croplist = new ObservableCollection<string>();
        IList<CropZoneColorItem> cropzonecoloritems = new List<CropZoneColorItem>();
        string selectedcrop = "All Crops";
        IClientEndpoint clientEndpoint;
        MapSettings mapsettings;
        LegendAdornmentLayer legendLayer = new LegendAdornmentLayer();
        int limitlegendlabellength = 0;

        public TagOrderedFilterItem(MapsPageViewModel mapspage, IClientEndpoint clientEndpoint) {
            this.mapspage = mapspage;
            this.clientEndpoint = clientEndpoint;
            InitializeFilter();
            ToggleVarietyColorCommand = new RelayCommand<ColorItem>(ToggleThisItem);
            ConfigureVarietyColorCommand = new RelayCommand(ChangeTheColor);
            RaisePropertyChanged("HatchTypeList");
        }

        public ICommand ToggleVarietyColorCommand { get; private set; }
        public ICommand ConfigureVarietyColorCommand { get; private set; }

        public string Name => Strings.FilterType_TagOrdered;

        public FilterType Filter => FilterType.TagOrdered;

        public ObservableCollection<ColorItem> ColorItems {
            get { return coloritems; }
            set {
                if (coloritems == value) { return; }
                coloritems = value;
                OnColorUpdated();
                RaisePropertyChanged("ColorItems");
                RaisePropertyChanged("CropzoneColors");
                RaisePropertyChanged("Croplist");
                RaisePropertyChanged("SelectedCrop");
            }
        }

        public ColorItem SelectedColorItem {
            get { return selectedcoloritem; }
            set {
                if (selectedcoloritem == value) { return; }
                selectedcoloritem = value;
                RaisePropertyChanged("SelectedColorItem");
                RaisePropertyChanged("ColorItems");
                RaisePropertyChanged("CropzoneColors");
            }
        }

        public ObservableCollection<string> Croplist {
            get { return croplist; }
            set {
                if (croplist == value) { return; }
                croplist = value;
                RaisePropertyChanged("Croplist");
            }
        }

        public string SelectedCrop {
            get { return selectedcrop; }
            set {
                if (selectedcrop == value) { return; }
                selectedcrop = value;
                for (int i = 0; i < ColorItems.Count; i++) {
                    if (selectedcrop == "All Crops" || selectedcrop == ColorItems[i].CropName) {
                        ColorItems[i].Visible = true;
                    }
                    else {
                        ColorItems[i].Visible = false;
                    }
                }
                OnColorUpdated();
                RaisePropertyChanged("SelectedCrop");
            }
        }

        public Dictionary<string, Color> CropzoneColors {
            get { return cropzonecolors; }
            set {
                if (cropzonecolors == value) { return; }
                cropzonecolors = value;
                RaisePropertyChanged("CropzoneColors");
            }
        }
        public IList<Color> UsedColors {
            get { return usedcolors; }
        }

        public Dictionary<string, Color> Execute(IClientEndpoint clientEndpoint, Random rand) {
            //if (CropzoneColors.Count > 0) {
            //    //OnColorUpdated();
            //    return CropzoneColors; 
            //}
            if (cropzonecolordict.Count > 0) {
                mapspage.CropzoneColorDict = cropzonecolordict;
                return CropzoneColors;
            }
            InitializeFilter();
            selectedcrop = "All Crops";
            croplist.Add("All Crops");
            var tree = clientEndpoint.GetView<CropTreeView>(new CropYearId(this.currentDataSourceId, this.currentCropYear)).GetValue(new CropTreeView());

            var q = from c in tree.Crops
                    from f in c.Farms
                    from fi in f.Fields
                    from cz in fi.CropZones
                    let farmName = f.Name
                    let itemMap = clientEndpoint.GetView<ItemMap>(cz.CropZoneId)
                    where itemMap.HasValue && itemMap.Value.MostRecentMapItem != null
                    select new { c.CropId, f.FarmId, farmName, fi.FieldId, cz.CropZoneId, cz.Name, itemMap.Value.MostRecentMapItem.MapData, itemMap.Value.MostRecentMapItem.DataType };

            Dictionary<string, string> CCp = new Dictionary<string, string>();
            cropzonefarm = new Dictionary<string, string>();
            foreach (var item in q) {
                Guid czidguid = item.CropZoneId.Id;
                string czid = czidguid.ToString();

                string cropname = clientEndpoint.GetMasterlistService().GetCropDisplay(item.CropId);
                CCp.Add(czid, cropname);
                if (!croplist.Contains(cropname)) {
                    croplist.Add(cropname);
                }
                if (!cropzonefarm.ContainsKey(czid)) {
                    cropzonefarm.Add(czid, item.farmName);
                }
            }
            croplist = new ObservableCollection<string>(croplist.OrderBy(x => x));
            for (int j = 1; j < croplist.Count; j++) {
                if (croplist[j] == "All Crops") {
                    croplist.Move(j, 0);
                    break;
                }
            }

            ObservableCollection<ColorItem> CI = new ObservableCollection<ColorItem>();
            Dictionary<string, string> CC = new Dictionary<string, string>();
            mapsettings = clientEndpoint.GetMapSettings();
            if (mapsettings.TagFilterColors == null) {
                mapsettings.TagFilterColors = new Dictionary<string, MapColorItem>();
            }

            IList<CropZoneId> cropzonelist = new List<CropZoneId>();
            foreach (var item in q) {
                cropzonelist.Add(item.CropZoneId);
            }
            int zzzzz = 975;
            int xyz = 0;
            for (int z = 0; z <= cropzonelist.Count - 1; z++) {
                try {
                    xyz++;
                    if (xyz > zzzzz) {
                        xyz = 0;
                    }

                    Guid czidguid = cropzonelist[z].Id;
                    string czid = czidguid.ToString();
                    var fieldMaybe = clientEndpoint.GetView<CropZoneDetailsView>(cropzonelist[z]);
                    fieldMaybe.IfValue(field => {
                        if (!cropzonetags.ContainsKey(czid)) {
                            cropzonetags.Add(czid, field.Tags);
                        }
                        foreach (var tag in field.Tags) {
                            try {
                                string idcompare = tag;
                                if (string.IsNullOrWhiteSpace(tag)) { continue; }
                                string cropname = string.Empty;
                                if (CCp.ContainsKey(czidguid.ToString())) {
                                    cropname = CCp[czidguid.ToString()];
                                }
                                ColorItem coloritem = new ColorItem(this, tag, tag, cropname, Color.Black);
                                if (mapsettings.TagFilterColors.ContainsKey(tag)) {
                                    coloritem.IsLoading = true;
                                    coloritem.MapColorInt = mapsettings.TagFilterColors[idcompare].MapColor;
                                    coloritem.MapColorIntFG = mapsettings.TagFilterColors[idcompare].MapColorFG;
                                    coloritem.HatchType = mapsettings.TagFilterColors[idcompare].HatchType;
                                    coloritem.Visible = mapsettings.TagFilterColors[idcompare].Visible;
                                    coloritem.IsLoading = false;
                                    bool colornotfound = true;
                                    foreach (ColorItem cit in CI) {
                                        if (cit.Key == tag) {
                                            colornotfound = false;
                                        }
                                    }
                                    if (colornotfound) {
                                        CI.Add(coloritem);
                                    }
                                    if (!coloritemsindex.ContainsKey(tag)) {
                                        coloritemsindex.Add(tag, coloritem);
                                    }
                                    if (!usedcolors.Contains(coloritem.MapColor)) {
                                        usedcolors.Add(coloritem.MapColor);
                                    }
                                }
                                else {
                                    Color color = Color.Red;
                                    bool colorused = true;
                                    while (colorused) {
                                        if (usedcolors.Count > 60) {
                                            colormultiplier = 25;
                                            randomchoice = 10;
                                        }
                                        color = Color.FromArgb(255, (rand.Next(0, randomchoice) * colormultiplier), (rand.Next(0, randomchoice) * colormultiplier), (rand.Next(0, randomchoice) * colormultiplier));
                                        if (!usedcolors.Contains(color)) {
                                            usedcolors.Add(color);
                                            colorused = false;
                                            break;
                                        }
                                        if (usedcolors.Count > 500) {
                                            colorused = false;
                                            break;
                                        }
                                    }

                                    if (!coloritemsindex.ContainsKey(tag)) {
                                        coloritem.IsLoading = true;
                                        coloritem.MapColor = color;
                                        coloritem.IsLoading = false;
                                        bool colornotfound = true;
                                        foreach (ColorItem cit in CI) {
                                            if (cit.Key == tag) {
                                                colornotfound = false;
                                            }
                                        }
                                        if (colornotfound) {
                                            CI.Add(coloritem);
                                        }
                                        coloritemsindex.Add(tag, coloritem);
                                    }
                                }
                                if (!CC.ContainsKey(czidguid.ToString())) {
                                    CC.Add(czidguid.ToString(), tag);
                                    cropzonecolors.Add(czidguid.ToString(), coloritemsindex[tag].MapColor);
                                }
                                double itemareavalue = 0;
                                if (itemareavalue == 0) {
                                    Measure czarea = GetCropZoneArea(new Guid(czidguid.ToString()));
                                    itemareavalue = czarea.Value;
                                }
                                cropzonecoloritems.Add(new CropZoneColorItem(this, czidguid.ToString(), "", tag, tag, cropname, coloritem.MapColor, itemareavalue));
                            }
                            catch (Exception ex) {
                                int fds = 0;
                            }
                        }
                    });
                }
                catch (Exception ex) {
                    int asdf = 0;
                }
            }
            cropzonecrops = CC;
            ColorItems = new ObservableCollection<ColorItem>(CI.OrderBy(x => x.Label));
            return CropzoneColors;
        }

        public void OnColorUpdated() {
            //coloritemsindex.Clear();
            cropzoneorderedcolordict = new Dictionary<string, List<ColorItem>>();
            mapsettings = clientEndpoint.GetMapSettings();
            if (mapsettings.TagFilterColors == null) {
                mapsettings.TagFilterColors = new Dictionary<string, MapColorItem>();
            }
            foreach (var ci in coloritems) {
                if (coloritemsindex.ContainsKey(ci.Key)) {
                    coloritemsindex[ci.Key].MapColor = ci.MapColor;
                    coloritemsindex[ci.Key].MapColorFG = ci.MapColorFG;
                    coloritemsindex[ci.Key].HatchType = ci.HatchType;
                    coloritemsindex[ci.Key].Visible = ci.Visible;
                }
                else {
                    coloritemsindex.Add(ci.Key, ci);
                }
                MapColorItem mci = new MapColorItem() { Key = ci.Key, Label = ci.Label, CropName = ci.CropName, MapColor = ci.MapColorInt, MapColorFG = ci.MapColorIntFG, HatchType = ci.HatchType, Visible = ci.Visible };
                //MapColorItem mci = new MapColorItem() { Key = ci.Key, Label = ci.Label, CropName = ci.CropName, MapColor = ci.MapColorInt, Visible = ci.Visible };
                if (mapsettings.TagFilterColors.ContainsKey(ci.Key)) {
                    mapsettings.TagFilterColors[ci.Key].MapColor = ci.MapColorInt;
                    mapsettings.TagFilterColors[ci.Key].MapColorFG = ci.MapColorIntFG;
                    mapsettings.TagFilterColors[ci.Key].HatchType = ci.HatchType;
                    mapsettings.TagFilterColors[ci.Key].Visible = ci.Visible;
                }
                else {
                    mapsettings.TagFilterColors.Add(ci.Key, mci);
                }
            }

            var czcoloritems = from c in coloritems
                               from cz in cropzonecoloritems
                               where (c.Key == cz.ProductID && c.Visible)
                               orderby (c.Label)
                               select new CropZoneColorItem() { CropZoneID = cz.CropZoneID, CropZonelLabel = cz.CropZonelLabel, ProductID = cz.ProductID, ProductName = cz.ProductName, CropName = cz.CropName, Area = cz.Area, MapColor = c.MapColor, Visible = c.Visible };


            cropzonecolors.Clear();
            foreach (var cc in czcoloritems) {
                if (selectedcrop == "All Crops") {
                    if (!cropzonecolors.ContainsKey(cc.CropZoneID)) {
                        cropzonecolors.Add(cc.CropZoneID, cc.MapColor);
                    }
                }
                else {
                    if (selectedcrop == cc.CropName) {
                        if (!cropzonecolors.ContainsKey(cc.CropZoneID)) {
                            cropzonecolors.Add(cc.CropZoneID, cc.MapColor);
                        }
                    }
                }
            }
            //cropzonetags.Add(czid, field.Tags)
            cropzoneorderedcolordict = new Dictionary<string, List<ColorItem>>();
            cropzonecolordict.Clear();
            foreach (KeyValuePair<string, List<string>> cztags in cropzonetags) {
                List<ColorItem> taglist = new List<ColorItem>();

                var czcoloritems2 = from c in coloritems
                                    from cz in cropzonecoloritems
                                    let coloritem = c
                                    let cropzonecoloritem = cz
                                    where (c.Key == cz.ProductID && c.Visible && cz.CropZoneID == cztags.Key)
                                    select new { cz.CropZoneID, cz.CropZonelLabel, cz.ProductID, cz.ProductName, cz.CropName, coloritem, cropzonecoloritem };


                foreach (var cc in czcoloritems2) {
                    if (selectedcrop == "All Crops") {
                        taglist.Add(cc.coloritem);
                    }
                    else {
                        if (selectedcrop == cc.CropName) {
                            taglist.Add(cc.coloritem);
                        }
                    }
                }
                if (taglist.Count > 0) {
                    cropzoneorderedcolordict.Add(cztags.Key, taglist);
                }
            }

            foreach (KeyValuePair<string, List<ColorItem>> cc in cropzoneorderedcolordict) {
                foreach (ColorItem ci in cc.Value) {
                    if (!cropzonecolordict.ContainsKey(cc.Key)) {
                        cropzonecolordict.Add(cc.Key, ci);
                    }
                    //if (selectedcrop == "All Crops") {
                    //    if (!cropzonecolordict.ContainsKey(cc.Key)) {
                    //        cropzonecolordict.Add(cc.Key, ci);
                    //    }
                    //}
                    //else {
                    //    if (selectedcrop == ci.CropName) {
                    //        if (!cropzonecolordict.ContainsKey(cc.Key)) {
                    //            cropzonecolordict.Add(cc.Key, ci);
                    //        }
                    //    }
                    //}
                }
            }

            mapspage.IsLegendVisible = true;
            mapspage.CropzoneLabels = new Dictionary<string, string>();
            mapspage.CropzonePercentages = new Dictionary<string, int>();
            //mapspage.SSurgoLayersOverlay = new ThinkGeo.MapSuite.WpfDesktopEdition.LayerOverlay();
            //mapspage.CropzoneColors = cropzonecolors;
            mapspage.ColorItems = ColorItems;
            mapspage.SetCropzoneColors(cropzonecolors);
            mapspage.CropzoneColorDict = cropzonecolordict;
            clientEndpoint.SaveMapSettings(mapsettings);
        }

        private void InitializeFilter() {
            this.currentDataSourceId = ApplicationEnvironment.CurrentDataSourceId;
            this.currentCropYear = ApplicationEnvironment.CurrentCropYear;
            coloritems = new ObservableCollection<ColorItem>();
            cropzonecrops = new Dictionary<string, string>();
            cropzonecolors = new Dictionary<string, Color>();
            coloritemsindex = new Dictionary<string, ColorItem>();
            croplist = new ObservableCollection<string>();
            usedcolors = new List<Color>();
            cropzonecoloritems = new List<CropZoneColorItem>();
            cropzonecolordict = new Dictionary<string, ColorItem>();
            cropzonetags = new Dictionary<string, List<string>>();
            cropzoneorderedcolordict = new Dictionary<string, List<ColorItem>>();
            colormultiplier = 50;
            randomchoice = 5;
            mapspage.IsLegendVisible = false;
        }

        void ToggleThisItem(ColorItem itemToSelect) {
            if (itemToSelect == null) { return; }
            SelectedColorItem = ColorItems.Where(x => x.Key == itemToSelect.Key).SingleOrDefault();
        }

        void ChangeTheColor() {
            showthetoggle = !showthetoggle;
            for (int i = 0; i < ColorItems.Count; i++) {
                ColorItems[i].ColorPickerColor = System.Windows.Media.Color.FromArgb(255, ColorItems[i].MapColor.R, ColorItems[i].MapColor.G, ColorItems[i].MapColor.B);
                ColorItems[i].ShowToggleButton = showthetoggle;
            }
            if (showthetoggle) {
                OnColorUpdated();
            }
            RaisePropertyChanged("ColorItems");
            RaisePropertyChanged("CropzoneColors");
        }

        private Measure GetCropZoneArea(Guid Id) {
            string stringid = Id.ToString();
            Measure resultarea = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(0);
            var fieldMaybe = clientEndpoint.GetView<CropZoneDetailsView>(new CropZoneId(ApplicationEnvironment.CurrentDataSourceId, Id));
            fieldMaybe.IfValue(field => {
                if (field.ReportedArea != null && !string.IsNullOrWhiteSpace(field.ReportedAreaUnit)) {
                    resultarea = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(field.ReportedArea.Value);
                }
                else if (field.BoundaryArea != null && !string.IsNullOrWhiteSpace(field.BoundaryAreaUnit)) {
                    resultarea = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(field.BoundaryArea.Value);
                }
                else {
                    resultarea = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(0);
                }
            });
            return resultarea;
        }

        private string DisplayLegendSelectedAreaDecimals(string label, double unitvalue, string unitname, int labelpad, int areapad) {
            string featdesc3 = string.Empty;
            IUnit _unit = UnitFactory.GetUnitByName(unitname);
            string decimalstring = "N" + mapsettings.AreaDecimals.ToString("N0");
            string string1 = string.Format("{0}:", FormatLegendLabelLength(label));
            string string2 = string.Format("{0} {1} ", unitvalue.ToString(decimalstring), _unit.AbbreviatedDisplay);
            if (unitvalue == 0) {
                string2 = string.Format("0 {0} ", _unit.AbbreviatedDisplay);
            }
            featdesc3 = string.Format("{0}{1}", string1.PadRight(labelpad), string2.PadLeft(areapad));
            return featdesc3;
        }

        private string FormatLegendLabelLength(string labeltext) {
            string newlabel = labeltext;
            if (limitlegendlabellength > 10 && labeltext.Length > limitlegendlabellength) {
                newlabel = labeltext.Substring(0, limitlegendlabellength);
            }
            return newlabel;
        }

        public LegendAdornmentLayer BuildLegend(InMemoryFeatureLayer fieldsLayer, CropMapsFarmDisplayItem selectedFarm, bool isLegendVisible, Dictionary<string, string> unselectedFeatures) {
            legendLayer = new LegendAdornmentLayer();
            float legendheight = 0;
            float baselength = 50;
            float baselegendwidth = 100;
            int padrightname = 30;
            int padrightarea = 10;
            int padrightnamemin = 5;
            int padrightnamecalc = 0;
            float baseheight = 2;
            float baselegendheight = 20;
            legendLayer.BackgroundMask = AreaStyles.CreateLinearGradientStyle(new GeoColor(255, 255, 255, 255), new GeoColor(255, 230, 230, 230), 90, GeoColor.SimpleColors.Black);
            CustomLegendItem title = new CustomLegendItem();
            title.TextStyle = new TextStyle(Strings.MapLegend_Text, new GeoFont("Arial", 10, DrawingFontStyles.Bold), new GeoSolidBrush(GeoColor.SimpleColors.Black));
            SizeF TextStylelength = GetStringPixelLength(Strings.MapLegend_Text, "Arial", 10);
            //title.Height = 30;
            title.Height = TextStylelength.Height + 5;
            legendheight += title.Height;
            //title.Width = 200;
            title.Width = TextStylelength.Width;
            DetermineLegendWitdh(legendLayer, title);
            legendLayer.Title = title;
            for (int i = 0; i < coloritems.Count; i++) {
                ColorItem ci = coloritems[i];
                coloritems[i].Area = 0;
                coloritems[i].ShowFilterItem = false;
            }
            double varietyarea = 0;
            var czcoloritems = from c in coloritems
                               from cz in cropzonecoloritems
                               where (c.Key == cz.ProductID && c.Visible)
                               select new CropZoneColorItem() { CropZoneID = cz.CropZoneID, CropZonelLabel = cz.CropZonelLabel, ProductID = cz.ProductID, ProductName = cz.ProductName, CropName = cz.CropName, Area = cz.Area, MapColor = c.MapColor, HatchType = c.HatchType, Visible = c.Visible };

            bool didnotfindacoloritem = true;
            foreach (var cc in czcoloritems) {
                try {
                    if (fieldsLayer.InternalFeatures.Contains(cc.CropZoneID)) {
                        Feature feature = fieldsLayer.InternalFeatures[cc.CropZoneID];
                        //if (unselectedFeatures != null && unselectedFeatures.Count > 0 && unselectedFeatures.ContainsKey(featureid)) { continue; }
                        if (unselectedFeatures == null || unselectedFeatures.Count == 0 || unselectedFeatures.ContainsKey(cc.CropZoneID.ToString()))
                        {
                            if (selectedFarm.IsAllFarms || selectedFarm.DisplayText == string.Empty || feature.ColumnValues[@"Farm"] == selectedFarm.DisplayText)
                            {
                                if (selectedcrop == "All Crops")
                                {
                                    varietyarea = cc.Area;
                                    for (int i = 0; i < coloritems.Count; i++)
                                    {
                                        ColorItem ci = coloritems[i];
                                        if (coloritems[i].Label.Length > padrightnamecalc)
                                        {
                                            padrightnamecalc = coloritems[i].Label.Length;
                                        }
                                        if (ci.Key == cc.ProductID)
                                        {
                                            coloritems[i].Area += varietyarea;
                                            coloritems[i].ShowFilterItem = true;
                                            didnotfindacoloritem = false;
                                        }
                                    }
                                }
                                else
                                {
                                    if (selectedcrop == cc.CropName)
                                    {
                                        varietyarea = cc.Area;
                                        for (int i = 0; i < coloritems.Count; i++)
                                        {
                                            ColorItem ci = coloritems[i];
                                            if (coloritems[i].Label.Length > padrightnamecalc)
                                            {
                                                padrightnamecalc = coloritems[i].Label.Length;
                                            }
                                            if (ci.Key == cc.ProductID)
                                            {
                                                coloritems[i].Area += varietyarea;
                                                coloritems[i].ShowFilterItem = true;
                                                didnotfindacoloritem = false;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex) {
                    continue;
                }
            }

            if (!selectedFarm.IsAllFarms) {
                var czcoloritems2 = from c in coloritems
                                    from cz in cropzonecoloritems
                                    from cf in cropzonefarm
                                    where (c.Key == cz.ProductID && cz.CropZoneID == cf.Key && cf.Value == selectedFarm.DisplayText)
                                    select new CropZoneColorItem() { CropZoneID = cz.CropZoneID, CropZonelLabel = cz.CropZonelLabel, ProductID = cz.ProductID, ProductName = cz.ProductName, CropName = cz.CropName, Area = cz.Area, MapColor = c.MapColor, HatchType = c.HatchType, Visible = c.Visible };

                foreach (var item in czcoloritems2) {
                    for (int i = 0; i < coloritems.Count; i++) {
                        ColorItem ci = coloritems[i];
                        try {
                            if (ci.Key == item.ProductID) {
                                coloritems[i].ShowFilterItem = true;
                                didnotfindacoloritem = false;
                            }
                        }
                        catch { }
                    }
                }
            }
            else {
                for (int i = 0; i < coloritems.Count; i++) {
                    coloritems[i].ShowFilterItem = true;
                }
            }
            if (didnotfindacoloritem) {
                for (int i = 0; i < coloritems.Count; i++) {
                    coloritems[i].ShowFilterItem = true;
                }
            }

            padrightnamecalc += padrightnamemin;

            int baselabelwidth = 0;
            int baseareawidth = 0;
            foreach (var ci in coloritems) {
                if (ci.Visible && ci.Area > 0) {
                    string _unitname = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).AbbreviatedDisplay;
                    IUnit _unit = UnitFactory.GetUnitByName(_unitname);
                    string decimalstring = "N" + mapsettings.AreaDecimals.ToString("N0");

                    string string1 = string.Format("{0}: ", FormatLegendLabelLength(ci.Label));
                    int featlabellength = string1.Length;
                    if (featlabellength > baselabelwidth) {
                        baselabelwidth = featlabellength;
                    }

                    string string2 = string.Format(" {0} {1} ", ci.Area.ToString(decimalstring), _unit.AbbreviatedDisplay);
                    int featarealength = string2.Length;
                    if (featarealength > baseareawidth) {
                        baseareawidth = featarealength;
                    }
                }
            }
            baselabelwidth += 1;
            baseareawidth += 1;

            foreach (var ci in coloritems) {
                if (ci.Visible && ci.Area > 0) {
                    string _unitname = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).AbbreviatedDisplay;
                    string featdesc3 = DisplayLegendSelectedAreaDecimals(ci.Label, ci.Area, _unitname, baselabelwidth, baseareawidth);
                    SizeF featdesclength = GetStringPixelLength(featdesc3, "Consolas", 8);
                    if ((featdesclength.Width + baselength) > baselegendwidth) {
                        baselegendwidth = featdesclength.Width + baselength;
                    }
                    if ((featdesclength.Height + baseheight) > baselegendheight) {
                        baselegendheight = featdesclength.Height + baseheight;
                    }
                }
            }
            foreach (var ci in coloritems) {
                if (ci.Visible && ci.Area > 0) {
                    string _unitname = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).AbbreviatedDisplay;
                    CustomLegendItem legendItem4 = new CustomLegendItem();
                    //legendItem4.ImageStyle = AreaStyles.CreateSimpleAreaStyle(GeoColor.FromArgb(ci.MapColor.A, ci.MapColor.R, ci.MapColor.G, ci.MapColor.B));
                    if (string.IsNullOrWhiteSpace(ci.HatchType) || ci.HatchType == "None" || ci.HatchType == "Solid") {
                        legendItem4.ImageStyle = AreaStyles.CreateSimpleAreaStyle(GeoColor.FromArgb(ci.MapColor.A, ci.MapColor.R, ci.MapColor.G, ci.MapColor.B));
                    }
                    else {
                        legendItem4.ImageStyle = AreaStyles.CreateHatchStyle(HatchStyleFactory.GetHatchStyle(ci.HatchType), GeoColor.FromArgb(ci.MapColorFG.A, ci.MapColorFG.R, ci.MapColorFG.G, ci.MapColorFG.B), GeoColor.FromArgb(ci.MapColor.A, ci.MapColor.R, ci.MapColor.G, ci.MapColor.B));
                    }
                    string featdesc3 = string.Empty;
                    featdesc3 = DisplayLegendSelectedAreaDecimals(ci.Label, ci.Area, _unitname, baselabelwidth, baseareawidth);
                    legendItem4.TextStyle = new TextStyle(featdesc3, new GeoFont("Consolas", 8), new GeoSolidBrush(GeoColor.SimpleColors.Black));
                    //legendItem4.Height = 25;
                    legendItem4.Height = baselegendheight;
                    legendheight += legendItem4.Height;
                    //legendItem4.Width = 250;
                    legendLayer.Width = baselegendwidth;
                    DetermineLegendWitdh(legendLayer, legendItem4);
                    legendLayer.LegendItems.Add(ci.Key, legendItem4);
                }
            }
            legendheight += 20;
            if (legendheight < 100) {
                legendheight = 100;
            }
            legendLayer.Height = legendheight;
            //legendLayer.Height = 325;
            //legendLayer.Width = 250;
            legendLayer.Width = baselegendwidth;
            legendLayer.ContentResizeMode = LegendContentResizeMode.Fixed;
            legendLayer.Location = AdornmentLocation.UpperLeft;
            legendLayer.YOffsetInPixel = 50;
            _icv = CollectionViewSource.GetDefaultView(ColorItems);
            _icv.Filter = FarmFilter;
            _icv.Refresh();
            RaisePropertyChanged("ICV");
            if (!isLegendVisible) { return null; }
            return legendLayer;
        }

        private void DetermineLegendWitdh(LegendAdornmentLayer legendLayer, CustomLegendItem legendItem) {
            SetLegendItemWidth(legendItem);

            if (legendItem.Width > legendLayer.Width) {
                legendLayer.Width = legendItem.Width;
            }
        }

        private static void SetLegendItemWidth(CustomLegendItem legendItem) {
            var fudgeFactor = 10;
            var lineWidth = (int)new GdiPlusGeoCanvas().MeasureText(legendItem.TextStyle.TextColumnName, legendItem.TextStyle.Font).Width;
            var totalImageLength = legendItem.ImageWidth + legendItem.ImageLeftPadding + legendItem.ImageRightPadding;
            var totalTextLength = legendItem.TextLeftPadding + legendItem.TextRightPadding + lineWidth;
            legendItem.Width = totalImageLength + totalTextLength + fudgeFactor;
        }

        private static SizeF GetStringPixelLength(string text, string fontname, float fontsize) {
            SizeF size = new SizeF();
            using (System.Drawing.Graphics graphics = System.Drawing.Graphics.FromImage(new Bitmap(1, 1))) {
                size = graphics.MeasureString(text, new Font(fontname, fontsize, FontStyle.Regular, GraphicsUnit.Point));
            }
            return size;
        }

        private ICollectionView _icv;
        public ICollectionView ICV {
            get { return _icv; }
        }

        private bool FarmFilter(object item) {
            ColorItem _item = item as ColorItem;
            return _item.ShowFilterItem;
        }

    }
}
