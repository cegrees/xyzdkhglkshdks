﻿using Landdb.Resources;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Map;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using NLog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Client.Infrastructure.DisplayItems;
using ThinkGeo.MapSuite.Core;

namespace Landdb.ViewModel.Maps {
    public class BlankFilterItem : ViewModelBase, IMapFilter  {
        MapsPageViewModel mapspage;
        readonly Logger log = LogManager.GetCurrentClassLogger();
        Guid currentDataSourceId = Guid.Empty;
        int currentCropYear = 0;
        string name = string.Empty;
        bool showthetoggle = true;
        ColorItem selectedcoloritem = null;
        ObservableCollection<ColorItem> coloritems = new ObservableCollection<ColorItem>();
        Dictionary<string, string> cropzonecrops = new Dictionary<string, string>();
        Dictionary<string, Color> cropzonecolors = new Dictionary<string,Color>();
        Dictionary<string, ColorItem> coloritemsindex = new Dictionary<string,ColorItem>();
        IClientEndpoint clientEndpoint;

        public BlankFilterItem(MapsPageViewModel mapspage, IClientEndpoint clientEndpoint) {
            this.mapspage = mapspage;
            this.clientEndpoint = clientEndpoint;
            InitializeFilter();
            ToggleBlankColorCommand = new RelayCommand<ColorItem>(ToggleThisItem);
            ConfigureCropColorCommand = new RelayCommand(ChangeTheColor);
        }

        public ICommand ToggleBlankColorCommand { get; private set; }
        public ICommand ConfigureCropColorCommand { get; private set; }

        public string Name => Strings.FilterType_Blank; 
        public FilterType Filter => FilterType.Blank;

        public ObservableCollection<ColorItem> ColorItems {
            get { return coloritems; }
            set {
                if (coloritems == value) { return; }
                coloritems = value;
                OnColorUpdated();
                RaisePropertyChanged("ColorItem");
                RaisePropertyChanged("ColorItems");
                RaisePropertyChanged("CropzoneColors");
            }
        }

        public ColorItem SelectedColorItem {
            get { return selectedcoloritem; }
            set {
                if (selectedcoloritem == value) { return; }
                selectedcoloritem = value;
                RaisePropertyChanged("SelectedColorItem");
                RaisePropertyChanged("ColorItems");
                RaisePropertyChanged("CropzoneColors");
            }
        }

        public Dictionary<string, Color> CropzoneColors {
            get { return cropzonecolors; }
            set {
                if (cropzonecolors == value) { return; }
                cropzonecolors = value;
                RaisePropertyChanged("CropzoneColors");
            }
        }
        public IList<Color> UsedColors {
            get { return new List<Color>(); ; }
        }

        public Dictionary<string, Color> Execute(IClientEndpoint clientEndpoint, Random rand) {
            if (CropzoneColors.Count > 0) { return CropzoneColors; }
            InitializeFilter();
            mapspage.CropzoneLabels = new Dictionary<string, string>();
            mapspage.CropzonePercentages = new Dictionary<string, int>();
            //mapspage.SSurgoLayersOverlay = new ThinkGeo.MapSuite.WpfDesktopEdition.LayerOverlay();
            coloritems = new ObservableCollection<ColorItem>();
            cropzonecrops = new Dictionary<string, string>();
            cropzonecolors = new Dictionary<string, Color>();
            coloritemsindex = new Dictionary<string, ColorItem>();
            mapspage.IsLegendVisible = false;
            mapspage.CropzoneLabels = new Dictionary<string, string>();
            mapspage.CropzonePercentages = new Dictionary<string, int>();
            //mapspage.SSurgoLayersOverlay = new ThinkGeo.MapSuite.WpfDesktopEdition.LayerOverlay();
            mapspage.SetCropzoneColors(cropzonecolors);
            mapspage.ColorItems = ColorItems;
            mapspage.CropzoneColorDict = new Dictionary<string, ColorItem>();
            return CropzoneColors;
        }

        public void OnColorUpdated() {
            coloritemsindex.Clear();
            cropzonecolors.Clear();
            mapspage.CropzoneColors = cropzonecolors;
        }

        private void InitializeFilter() {
            this.currentDataSourceId = ApplicationEnvironment.CurrentDataSourceId;
            this.currentCropYear = ApplicationEnvironment.CurrentCropYear;
            coloritems = new ObservableCollection<ColorItem>();
            cropzonecrops = new Dictionary<string, string>();
            cropzonecolors = new Dictionary<string, Color>();
            coloritemsindex = new Dictionary<string, ColorItem>();
        }

        void ToggleThisItem(ColorItem itemToSelect) {
        }

        void ChangeTheColor() {
        }

        public LegendAdornmentLayer BuildLegend(InMemoryFeatureLayer fieldsLayer, CropMapsFarmDisplayItem selectedFarm, bool isLegendVisible, Dictionary<string, string> unselectedFeatures) {
            throw new NotImplementedException();
        }

    }
}
