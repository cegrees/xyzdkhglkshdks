﻿using Landdb.Resources;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Map;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.ViewModel.Secondary.Map;
using NLog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Windows.Threading;
using Landdb.Client.Infrastructure.DisplayItems;
using ThinkGeo.MapSuite.Core;

namespace Landdb.ViewModel.Maps {
    public class SSurgoItem : ViewModelBase, IMapFilter {
        SSurgoNrcsWfsViewModel mapspage;
        readonly Logger log = LogManager.GetCurrentClassLogger();
        Guid currentDataSourceId = Guid.Empty;
        int currentCropYear = 0;
        string name = string.Empty;
        bool showthetoggle = true;
        ColorItem selectedcoloritem = null;
        int colormultiplier = 50;
        int randomchoice = 5;
        IList<Color> usedcolors = new List<Color>();
        ObservableCollection<ColorItem> coloritems = new ObservableCollection<ColorItem>();
        Dictionary<string, string> cropzonecrops = new Dictionary<string, string>();
        Dictionary<string, Color> cropzonecolors = new Dictionary<string, Color>();
        Dictionary<string, ColorItem> coloritemsindex = new Dictionary<string, ColorItem>();
        IClientEndpoint clientEndpoint;
        int cmgopacity = 255;

        public SSurgoItem(SSurgoNrcsWfsViewModel mapspage, IClientEndpoint clientEndpoint) {
            this.mapspage = mapspage;
            this.clientEndpoint = clientEndpoint;
            InitializeFilter();
            ToggleSSurgoColorCommand = new RelayCommand<ColorItem>(ToggleThisItem);
            ConfigureSSurgoColorCommand = new RelayCommand(ChangeTheColor);
        }

        public ICommand ToggleSSurgoColorCommand { get; private set; }
        public ICommand ConfigureSSurgoColorCommand { get; private set; }

        public string Name => Strings.FilterType_Crop;

        public FilterType Filter => FilterType.SSurgoItem;

        public ObservableCollection<ColorItem> ColorItems {
            get { return coloritems; }
            set {
                if (coloritems == value) { return; }
                coloritems = value;
                OnColorUpdated();
                RaisePropertyChanged("ColorItem");
                RaisePropertyChanged("ColorItems");
                RaisePropertyChanged("CropzoneColors");
            }
        }

        public ColorItem SelectedColorItem {
            get { return selectedcoloritem; }
            set {
                if (selectedcoloritem == value) { return; }
                selectedcoloritem = value;
                RaisePropertyChanged("SelectedColorItem");
                RaisePropertyChanged("ColorItems");
                RaisePropertyChanged("CropzoneColors");
            }
        }

        public Dictionary<string, Color> CropzoneColors {
            get { return cropzonecolors; }
            set {
                if (cropzonecolors == value) { return; }
                cropzonecolors = value;
                RaisePropertyChanged("CropzoneColors");
            }
        }

        public IList<string> HatchTypeList {
            get { return HatchStyleFactory.GetHatchStyleList(); }
        }

        public IList<Color> UsedColors {
            get { return usedcolors; }
        }

        public Dictionary<string, Color> Execute(IClientEndpoint clientEndpoint, Random rand) {
            if (CropzoneColors.Count > 0) { return CropzoneColors; }
            InitializeFilter();
            ObservableCollection<ColorItem> CI = new ObservableCollection<ColorItem>();
            MapSettings mapsettings = clientEndpoint.GetMapSettings();
            cmgopacity = mapsettings.UnselectedOpacity;
            if (mapsettings.SSurgoColors == null) {
                mapsettings.SSurgoColors = new Dictionary<string, MapColorItem>();
            }
            if (mapsettings.SSurgoColors != null) {
                foreach (var colr in mapsettings.SSurgoColors) {
                    ColorItem colitem = new ColorItem(this, colr.Value.Key, colr.Value.Label, colr.Value.Label, Color.Black);
                    colitem.IsLoading = true;
                    colitem.MapColorInt = colr.Value.MapColor;
                    colitem.Visible = colr.Value.Visible;
                    colitem.MapColorInt = colr.Value.MapColor;
                    colitem.Visible = colr.Value.Visible;
                    colitem.IsLoading = false;
                    CI.Add(colitem);
                    if (!usedcolors.Contains(colitem.MapColor)) {
                        usedcolors.Add(colitem.MapColor);
                    }
                }
            }
            ColorItems = CI;
            return CropzoneColors;
        }

        public void OnColorUpdated() {
            coloritemsindex.Clear();
            cropzonecolors.Clear();
            var mapsettings = clientEndpoint.GetMapSettings();
            if (mapsettings.SSurgoColors == null) {
                mapsettings.SSurgoColors = new Dictionary<string, MapColorItem>();
            }
            foreach (var ci in coloritems) {
                if (!coloritemsindex.ContainsKey(ci.Key)) {
                    coloritemsindex.Add(ci.Key, ci);
                }
                MapColorItem mci = new MapColorItem() { Key = ci.Key, Label = ci.Label, CropName = ci.CropName, MapColor = ci.MapColorInt, Visible = ci.Visible };
                if (mapsettings.SSurgoColors.ContainsKey(ci.Key)) {
                    mapsettings.SSurgoColors[ci.Key].MapColor = ci.MapColorInt;
                    mapsettings.SSurgoColors[ci.Key].Visible = ci.Visible;
                }
                else {
                    mapsettings.SSurgoColors.Add(ci.Key, mci);
                }
                if (coloritemsindex[ci.Key].Visible) {
                    if (!cropzonecolors.ContainsKey(ci.Key)) {
                        cropzonecolors.Add(ci.Key, coloritemsindex[ci.Key].MapColor);
                    }
                }
            }
            //cropzonecolors.Clear();
            //foreach (var cc in cropzonecrops) {
            //    if (coloritemsindex[cc.Value].Visible) {
            //        cropzonecolors.Add(cc.Key, coloritemsindex[cc.Value].MapColor);
            //    }
            //}
            mapspage.ColorItems = ColorItems;
            mapspage.CropzoneColors = CropzoneColors;
            clientEndpoint.SaveMapSettings(mapsettings);
        }

        private void InitializeFilter() {
            this.currentDataSourceId = ApplicationEnvironment.CurrentDataSourceId;
            this.currentCropYear = ApplicationEnvironment.CurrentCropYear;
            coloritems = new ObservableCollection<ColorItem>();
            cropzonecrops = new Dictionary<string, string>();
            cropzonecolors = new Dictionary<string, Color>();
            coloritemsindex = new Dictionary<string, ColorItem>();
            usedcolors = new List<Color>();
            colormultiplier = 50;
            randomchoice = 5;
        }

        void ToggleThisItem(ColorItem itemToSelect) {
            if (itemToSelect == null) { return; }
            SelectedColorItem = ColorItems.Where(x => x.Key == itemToSelect.Key).SingleOrDefault();
        }

        void ChangeTheColor() {
            showthetoggle = !showthetoggle;
            for (int i = 0; i < ColorItems.Count; i++) {
                ColorItems[i].ColorPickerColor = System.Windows.Media.Color.FromArgb(255, ColorItems[i].MapColor.R, ColorItems[i].MapColor.G, ColorItems[i].MapColor.B);
                ColorItems[i].ShowToggleButton = showthetoggle;
            }
            if (showthetoggle) {
                OnColorUpdated();
            }
            RaisePropertyChanged("ColorItems");
            RaisePropertyChanged("CropzoneColors");
        }

        public LegendAdornmentLayer BuildLegend(InMemoryFeatureLayer fieldsLayer, CropMapsFarmDisplayItem selectedFarm, bool isLegendVisible, Dictionary<string, string> unselectedFeatures) {
            throw new NotImplementedException();
        }

    }
}
