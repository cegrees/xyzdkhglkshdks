﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.ComponentModel;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;

namespace Landdb.ViewModel.Maps {
    public class CropZoneColorItem : ViewModelBase {
        IMapFilter parent;
        string cropzoneid = string.Empty;
        string cropzonelabel = string.Empty;
        string productid = string.Empty;
        string productname = string.Empty;
        string cropname = string.Empty;
        string name = string.Empty;
        string tag = string.Empty;
        Color mapcolor = new Color();
        System.Windows.Media.Color colorPickerColor = new System.Windows.Media.Color();
        Color mapcolorFG = Color.White;
        System.Windows.Media.Color colorPickerColorFG = new System.Windows.Media.Color();
        string hatchtype = string.Empty;
        bool visible = true;
        bool showToggleButton = true;
        bool isSelected = false;
        double area = 0;

        public CropZoneColorItem() { }

        public CropZoneColorItem(IMapFilter parent, string cropzoneid, string cropzonelabel, string productid, string productname, string cropname, Color mapcolor) {
            this.parent = parent;
            this.cropzoneid = cropzoneid;
            this.cropzonelabel = cropzonelabel;
            this.productid = productid;
            this.productname = productname;
            this.cropname = cropname;
            this.mapcolor = mapcolor;
            this.mapcolorFG = mapcolor;
            this.hatchtype = string.Empty;
            this.area = 0;
        }

        public CropZoneColorItem(IMapFilter parent, string cropzoneid, string cropzonelabel, string productid, string productname, string cropname, Color mapcolor, double area) {
            this.parent = parent;
            this.cropzoneid = cropzoneid;
            this.cropzonelabel = cropzonelabel;
            this.productid = productid;
            this.productname = productname;
            this.cropname = cropname;
            this.mapcolor = mapcolor;
            this.mapcolorFG = mapcolor;
            this.hatchtype = string.Empty;
            this.area = area;
        }

        public CropZoneColorItem(IMapFilter parent, string cropzoneid, string cropzonelabel, string productid, string productname, string cropname, Color mapcolor, Color mapcolorFG, string hatchtype, double area) {
            this.parent = parent;
            this.cropzoneid = cropzoneid;
            this.cropzonelabel = cropzonelabel;
            this.productid = productid;
            this.productname = productname;
            this.cropname = cropname;
            this.mapcolor = mapcolor;
            this.mapcolorFG = mapcolorFG;
            this.hatchtype = hatchtype;
            this.area = area;
        }

        public CropZoneColorItem(IMapFilter parent, string cropzoneid, string cropzonelabel, string productid, string productname, string cropname, Color mapcolor, double area, string name, string tag) {
            this.parent = parent;
            this.cropzoneid = cropzoneid;
            this.cropzonelabel = cropzonelabel;
            this.productid = productid;
            this.productname = productname;
            this.cropname = cropname;
            this.mapcolor = mapcolor;
            this.mapcolorFG = mapcolor;
            this.hatchtype = string.Empty;
            this.area = area;
            this.name = name;
            this.tag = tag;
        }

        public string CropZoneID {
            get { return cropzoneid; }
            set {
                if (cropzoneid == value) { return; }
                cropzoneid = value;
                RaisePropertyChanged("CropZoneID");
            }
        }

        public string CropZonelLabel {
            get { return cropzonelabel; }
            set {
                if (cropzonelabel == value) { return; }
                cropzonelabel = value;
                RaisePropertyChanged("CropZonelLabel");
            }
        }

        public string ProductID {
            get { return productid; }
            set {
                if (productid == value) { return; }
                productid = value;
                RaisePropertyChanged("ProductID");
            }
        }

        public string ProductName {
            get { return productname; }
            set {
                if (productname == value) { return; }
                productname = value;
                RaisePropertyChanged("ProductName");
            }
        }

        public string CropName {
            get { return cropname; }
            set {
                if (cropname == value) { return; }
                cropname = value;
                RaisePropertyChanged("CropName");
            }
        }

        public double Area {
            get { return area; }
            set {
                if (area == value) { return; }
                area = value;
                RaisePropertyChanged("Area");
            }
        }

        public string Name {
            get { return name; }
            set {
                if (name == value) { return; }
                name = value;
                RaisePropertyChanged("Name");
            }
        }

        public string Tag {
            get { return tag; }
            set {
                if (tag == value) { return; }
                tag = value;
                RaisePropertyChanged("Tag");
            }
        }

        public Color MapColor {
            get { return mapcolor; }
            set {
                if (mapcolor == value) { return; }
                mapcolor = value;
                colorPickerColor = System.Windows.Media.Color.FromArgb(255, mapcolor.R, mapcolor.G, mapcolor.B);
                RaisePropertyChanged("MapColor");
                RaisePropertyChanged("WpfFillColor");
                RaisePropertyChanged("ColorPickerColor");
            }
        }

        public System.Windows.Media.Color ColorPickerColor {
            get { return colorPickerColor; }
            set {
                if (colorPickerColor == value) { return; }
                colorPickerColor = value;
                MapColor = System.Drawing.Color.FromArgb(255, colorPickerColor.R, colorPickerColor.G, colorPickerColor.B);
                RaisePropertyChanged("MapColor");
                RaisePropertyChanged("WpfFillColor");
                RaisePropertyChanged("ColorPickerColor");
            }
        }

        public System.Windows.Media.SolidColorBrush WpfFillColor {
            get { return new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(mapcolor.A, mapcolor.R, mapcolor.G, mapcolor.B)); }
        }

        public int MapColorInt {
            get { return MapColor.ToArgb(); }
            set { MapColor = System.Drawing.Color.FromArgb(value); }
        }

        public Color MapColorFG {
            get { return mapcolorFG; }
            set {
                if (mapcolorFG == value) { return; }
                mapcolorFG = value;
                colorPickerColorFG = System.Windows.Media.Color.FromArgb(255, mapcolorFG.R, mapcolorFG.G, mapcolorFG.B);
                RaisePropertyChanged("MapColorFG");
                RaisePropertyChanged("WpfFillColorFG");
                RaisePropertyChanged("ColorPickerColorFG");
            }
        }

        public System.Windows.Media.Color ColorPickerColorFG {
            get { return colorPickerColorFG; }
            set {
                if (colorPickerColorFG == value) { return; }
                colorPickerColorFG = value;
                MapColorFG = System.Drawing.Color.FromArgb(255, colorPickerColorFG.R, colorPickerColorFG.G, colorPickerColorFG.B);
                RaisePropertyChanged("MapColorFG");
                RaisePropertyChanged("WpfFillColorFG");
                RaisePropertyChanged("ColorPickerColorFG");
            }
        }

        public System.Windows.Media.SolidColorBrush WpfFillColorFG {
            get { return new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(mapcolorFG.A, mapcolorFG.R, mapcolorFG.G, mapcolorFG.B)); }
        }

        public int MapColorIntFG {
            get { return MapColorFG.ToArgb(); }
            set { MapColorFG = System.Drawing.Color.FromArgb(value); }
        }

        public string HatchType {
            get { return hatchtype; }
            set {
                if (hatchtype == value) { return; }
                hatchtype = value;
                RaisePropertyChanged("HatchType");
            }
        }

        public bool Visible {
            get { return visible; }
            set {
                if (visible == value) { return; }
                visible = value;
                RaisePropertyChanged("Visible");
            }
        }

        public bool IsSelected {
            get { return isSelected; }
            set {
                if (isSelected == value) { return; }
                isSelected = value;
                RaisePropertyChanged("IsSelected");
            }
        }

        public bool ShowToggleButton {
            get { return showToggleButton; }
            set {
                if (showToggleButton == value) { return; }
                showToggleButton = value;
                RaisePropertyChanged("ShowColorPicker");
                RaisePropertyChanged("ShowToggleButton");
                RaisePropertyChanged("ToggleButtonVisibility");
                RaisePropertyChanged("ColorPickerVisibility");
            }
        }

        public bool ShowColorPicker {
            get { return !showToggleButton; }
        }

        public System.Windows.Visibility ToggleButtonVisibility {
            get {
                if (showToggleButton) { return System.Windows.Visibility.Visible; }
                return System.Windows.Visibility.Collapsed;
            }
        }

        public System.Windows.Visibility ColorPickerVisibility {
            get {
                if (!showToggleButton) { return System.Windows.Visibility.Visible; }
                return System.Windows.Visibility.Collapsed;
            }
        }
    }
}
