﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Maps {
    public class HatchStyleFactory {

        public static ThinkGeo.MapSuite.Core.GeoHatchStyle GetHatchStyle(string hatchstyle) {
            if (string.IsNullOrWhiteSpace(hatchstyle)) return new ThinkGeo.MapSuite.Core.GeoHatchStyle();
            if (hatchstyle == "None") return new ThinkGeo.MapSuite.Core.GeoHatchStyle();
            if (hatchstyle == "Solid") return new ThinkGeo.MapSuite.Core.GeoHatchStyle();
            if (hatchstyle == "Horizontal") return ThinkGeo.MapSuite.Core.GeoHatchStyle.Horizontal;
            if (hatchstyle == "Vertical") return ThinkGeo.MapSuite.Core.GeoHatchStyle.Vertical;
            if (hatchstyle == "ForwardDiagonal") return ThinkGeo.MapSuite.Core.GeoHatchStyle.ForwardDiagonal;
            if (hatchstyle == "BackwardDiagonal") return ThinkGeo.MapSuite.Core.GeoHatchStyle.BackwardDiagonal;
            if (hatchstyle == "LargeGrid") return ThinkGeo.MapSuite.Core.GeoHatchStyle.LargeGrid;
            if (hatchstyle == "DiagonalCross") return ThinkGeo.MapSuite.Core.GeoHatchStyle.DiagonalCross;
            if (hatchstyle == "LightDownwardDiagonal") return ThinkGeo.MapSuite.Core.GeoHatchStyle.LightDownwardDiagonal;
            if (hatchstyle == "LightUpwardDiagonal") return ThinkGeo.MapSuite.Core.GeoHatchStyle.LightUpwardDiagonal;
            if (hatchstyle == "DarkDownwardDiagonal") return ThinkGeo.MapSuite.Core.GeoHatchStyle.DarkDownwardDiagonal;
            if (hatchstyle == "DarkUpwardDiagonal") return ThinkGeo.MapSuite.Core.GeoHatchStyle.DarkUpwardDiagonal;
            if (hatchstyle == "WideDownwardDiagonal") return ThinkGeo.MapSuite.Core.GeoHatchStyle.WideDownwardDiagonal;
            if (hatchstyle == "WideUpwardDiagonal") return ThinkGeo.MapSuite.Core.GeoHatchStyle.WideUpwardDiagonal;
            if (hatchstyle == "LightVertical") return ThinkGeo.MapSuite.Core.GeoHatchStyle.LightVertical;
            if (hatchstyle == "LightHorizontal") return ThinkGeo.MapSuite.Core.GeoHatchStyle.LightHorizontal;
            if (hatchstyle == "NarrowVertical") return ThinkGeo.MapSuite.Core.GeoHatchStyle.NarrowVertical;
            if (hatchstyle == "NarrowHorizontal") return ThinkGeo.MapSuite.Core.GeoHatchStyle.NarrowHorizontal;
            if (hatchstyle == "DarkVertical") return ThinkGeo.MapSuite.Core.GeoHatchStyle.DarkVertical;
            if (hatchstyle == "DarkHorizontal") return ThinkGeo.MapSuite.Core.GeoHatchStyle.DarkHorizontal;
            if (hatchstyle == "DashedDownwardDiagonal") return ThinkGeo.MapSuite.Core.GeoHatchStyle.DashedDownwardDiagonal;
            if (hatchstyle == "DashedUpwardDiagonal") return ThinkGeo.MapSuite.Core.GeoHatchStyle.DashedUpwardDiagonal;
            if (hatchstyle == "DashedHorizontal") return ThinkGeo.MapSuite.Core.GeoHatchStyle.DashedHorizontal;
            if (hatchstyle == "DashedVertical") return ThinkGeo.MapSuite.Core.GeoHatchStyle.DashedVertical;
            if (hatchstyle == "SmallConfetti") return ThinkGeo.MapSuite.Core.GeoHatchStyle.SmallConfetti;
            if (hatchstyle == "LargeConfetti") return ThinkGeo.MapSuite.Core.GeoHatchStyle.LargeConfetti;
            if (hatchstyle == "ZigZag") return ThinkGeo.MapSuite.Core.GeoHatchStyle.ZigZag;
            if (hatchstyle == "Wave") return ThinkGeo.MapSuite.Core.GeoHatchStyle.Wave;
            if (hatchstyle == "DiagonalBrick") return ThinkGeo.MapSuite.Core.GeoHatchStyle.DiagonalBrick;
            if (hatchstyle == "HorizontalBrick") return ThinkGeo.MapSuite.Core.GeoHatchStyle.HorizontalBrick;
            if (hatchstyle == "Weave") return ThinkGeo.MapSuite.Core.GeoHatchStyle.Weave;
            if (hatchstyle == "Plaid") return ThinkGeo.MapSuite.Core.GeoHatchStyle.Plaid;
            if (hatchstyle == "Divot") return ThinkGeo.MapSuite.Core.GeoHatchStyle.Divot;
            if (hatchstyle == "DottedGrid") return ThinkGeo.MapSuite.Core.GeoHatchStyle.DottedGrid;
            if (hatchstyle == "DottedDiamond") return ThinkGeo.MapSuite.Core.GeoHatchStyle.DottedDiamond;
            if (hatchstyle == "Shingle") return ThinkGeo.MapSuite.Core.GeoHatchStyle.Shingle;
            if (hatchstyle == "Trellis") return ThinkGeo.MapSuite.Core.GeoHatchStyle.Trellis;
            if (hatchstyle == "Sphere") return ThinkGeo.MapSuite.Core.GeoHatchStyle.Sphere;
            if (hatchstyle == "SmallGrid") return ThinkGeo.MapSuite.Core.GeoHatchStyle.SmallGrid;
            if (hatchstyle == "SmallCheckerBoard") return ThinkGeo.MapSuite.Core.GeoHatchStyle.SmallCheckerBoard;
            if (hatchstyle == "LargeCheckerBoard") return ThinkGeo.MapSuite.Core.GeoHatchStyle.LargeCheckerBoard;
            if (hatchstyle == "OutlinedDiamond") return ThinkGeo.MapSuite.Core.GeoHatchStyle.OutlinedDiamond;
            if (hatchstyle == "SolidDiamond") return ThinkGeo.MapSuite.Core.GeoHatchStyle.SolidDiamond;
            if (hatchstyle == "Cross") return ThinkGeo.MapSuite.Core.GeoHatchStyle.Cross;
            return new ThinkGeo.MapSuite.Core.GeoHatchStyle();
        }


        public static IList<string> GetHatchStyleList() {
            IList<string> hatchlist = new List<string>();
            hatchlist.Add("");
            hatchlist.Add("Horizontal");
            hatchlist.Add("Vertical");
            hatchlist.Add("ForwardDiagonal");
            hatchlist.Add("BackwardDiagonal");
            hatchlist.Add("LargeGrid");
            hatchlist.Add("DiagonalCross");
            hatchlist.Add("LightDownwardDiagonal");
            hatchlist.Add("LightUpwardDiagonal");
            hatchlist.Add("DarkDownwardDiagonal");
            hatchlist.Add("DarkUpwardDiagonal");
            hatchlist.Add("WideDownwardDiagonal");
            hatchlist.Add("WideUpwardDiagonal");
            hatchlist.Add("LightVertical");
            hatchlist.Add("LightHorizontal");
            hatchlist.Add("NarrowVertical");
            hatchlist.Add("NarrowHorizontal");
            hatchlist.Add("DarkVertical");
            hatchlist.Add("DarkHorizontal");
            hatchlist.Add("DashedDownwardDiagonal");
            hatchlist.Add("DashedUpwardDiagonal");
            hatchlist.Add("DashedHorizontal");
            hatchlist.Add("DashedVertical");
            hatchlist.Add("SmallConfetti");
            hatchlist.Add("LargeConfetti");
            hatchlist.Add("ZigZag");
            hatchlist.Add("Wave");
            hatchlist.Add("DiagonalBrick");
            hatchlist.Add("HorizontalBrick");
            hatchlist.Add("Weave");
            hatchlist.Add("Plaid");
            hatchlist.Add("Divot");
            hatchlist.Add("DottedGrid");
            hatchlist.Add("DottedDiamond");
            hatchlist.Add("Shingle");
            hatchlist.Add("Trellis");
            hatchlist.Add("Sphere");
            hatchlist.Add("SmallGrid");
            hatchlist.Add("SmallCheckerBoard");
            hatchlist.Add("LargeCheckerBoard");
            hatchlist.Add("OutlinedDiamond");
            hatchlist.Add("SolidDiamond");
            hatchlist.Add("Cross");
            return hatchlist;
        }

        //GeoHatchStyle.Horizontal               	A Horizontal pattern fill.
        //GeoHatchStyle.Vertical                 	A Vertical pattern fill.
        //GeoHatchStyle.ForwardDiagonal          	A Forward Diagonal pattern fill.
        //GeoHatchStyle.BackwardDiagonal         	A Backward Diagonal pattern fill.
        //GeoHatchStyle.LargeGrid                	A Large Grid pattern fill.
        //GeoHatchStyle.DiagonalCross            	A Diagonal Cross pattern fill.
        //GeoHatchStyle.LightDownwardDiagonal    	A Light Downward Diagonal pattern fill.
        //GeoHatchStyle.LightUpwardDiagonal      	A Light Upward Diagonal pattern fill.
        //GeoHatchStyle.DarkDownwardDiagonal     	A Dark Downward Diagonal pattern fill.
        //GeoHatchStyle.DarkUpwardDiagonal       	A Dark Upward Diagonal pattern fill.
        //GeoHatchStyle.WideDownwardDiagonal     	A Wide Downward Diagonal pattern fill.
        //GeoHatchStyle.WideUpwardDiagonal       	A Wide Upward Diagonal pattern fill.
        //GeoHatchStyle.LightVertical            	A Light Vertical pattern fill.
        //GeoHatchStyle.LightHorizontal          	A Light Horizontal pattern fill.
        //GeoHatchStyle.NarrowVertical           	A Narrow Vertical pattern fill.
        //GeoHatchStyle.NarrowHorizontal         	A Narrow Horizontal pattern fill.
        //GeoHatchStyle.DarkVertical             	A Dark Vertical pattern fill.
        //GeoHatchStyle.DarkHorizontal           	A Dark Horizontal pattern fill.
        //GeoHatchStyle.DashedDownwardDiagonal   	A Dashed Downward Diagonal pattern fill.
        //GeoHatchStyle.DashedUpwardDiagonal     	A Dashed Upward Diagonal pattern fill.
        //GeoHatchStyle.DashedHorizontal         	A Dashed Horizontal pattern fill.
        //GeoHatchStyle.DashedVertical           	A Dashed Vertical pattern fill.
        //GeoHatchStyle.SmallConfetti            	A Small Confetti pattern fill.
        //GeoHatchStyle.LargeConfetti            	A Large Confetti pattern fill.
        //GeoHatchStyle.ZigZag                   	A Zig Zag pattern fill.
        //GeoHatchStyle.Wave                     	A Wave pattern fill.
        //GeoHatchStyle.DiagonalBrick            	A Diagonal Brick pattern fill.
        //GeoHatchStyle.HorizontalBrick          	A Horizontal Brick pattern fill.
        //GeoHatchStyle.Weave                    	A Weave pattern fill.
        //GeoHatchStyle.Plaid                    	A Plaid pattern fill.
        //GeoHatchStyle.Divot                    	A Divot pattern fill.
        //GeoHatchStyle.DottedGrid               	A Dotted Grid pattern fill.
        //GeoHatchStyle.DottedDiamond            	A Dotted Diamond pattern fill.
        //GeoHatchStyle.Shingle                  	A Shingle pattern fill.
        //GeoHatchStyle.Trellis                  	A Trellis pattern fill.
        //GeoHatchStyle.Sphere                   	A Sphere pattern fill.
        //GeoHatchStyle.SmallGrid                	A Small Grid pattern fill.
        //GeoHatchStyle.SmallCheckerBoard          	A Small Checker Board pattern fill.
        //GeoHatchStyle.LargeCheckerBoard        	A Large Checker Board pattern fill.
        //GeoHatchStyle.OutlinedDiamond          	A Outlined Diamond pattern fill.
        //GeoHatchStyle.SolidDiamond             	A Solid Diamond pattern fill.
        //GeoHatchStyle.Cross                    	A Cross pattern fill. 
    }
}
