﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.ComponentModel;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;

namespace Landdb.ViewModel.Maps {
    public class ColorItem : ViewModelBase {
        IMapFilter parent;
        string key = string.Empty;
        string label = string.Empty;
        string cropname = string.Empty;
        string name = string.Empty;
        string tag = string.Empty;
        Color mapcolor = new Color();
        System.Windows.Media.Color colorPickerColor = new System.Windows.Media.Color();
        Color mapcolorFG = Color.White;
        System.Windows.Media.Color colorPickerColorFG = new System.Windows.Media.Color();
        IList<string> hatchTypeList = new List<string>();
        string hatchtype = string.Empty;
        bool visible = true;
        bool showToggleButton = true;
        bool showFilterItem = true;
        bool isSelected = false;
        bool isLoading = false;
        bool colorPickerIsOpen = false;
        bool colorPickerFGIsOpen = false;
        double area = 0;

        public ColorItem(IMapFilter parent, string key, string label, string cropname, Color mapcolor) {
            this.parent = parent;
            this.key = key;
            this.label = label;
            this.cropname = cropname;
            this.mapcolor = mapcolor;
            this.mapcolorFG = mapcolor;
            HatchTypeList = HatchStyleFactory.GetHatchStyleList();
            this.hatchtype = string.Empty;
            this.visible = true;
            this.showFilterItem = true;
            RaisePropertyChanged("HatchType");
        }

        public ColorItem(IMapFilter parent, string key, string label, string cropname, Color mapcolor, Color mapcolorFG, string hatchtype) {
            this.parent = parent;
            this.key = key;
            this.label = label;
            this.cropname = cropname;
            this.mapcolor = mapcolor;
            this.mapcolorFG = mapcolorFG;
            HatchTypeList = HatchStyleFactory.GetHatchStyleList();
            this.hatchtype = hatchtype;
            this.visible = true;
            this.showFilterItem = true;
            RaisePropertyChanged("HatchType");
        }

        public ColorItem(IMapFilter parent, string key, string label, string cropname, Color mapcolor, Color mapcolorFG, string hatchtype, bool visible) {
            this.parent = parent;
            this.key = key;
            this.label = label;
            this.cropname = cropname;
            this.mapcolor = mapcolor;
            this.mapcolorFG = mapcolorFG;
            HatchTypeList = HatchStyleFactory.GetHatchStyleList();
            this.hatchtype = hatchtype;
            this.visible = visible;
            this.showFilterItem = true;
            RaisePropertyChanged("HatchType");
        }

        public string Key {
            get { return key; }
            set {
                if (key == value) { return; }
                key = value;
                RaisePropertyChanged("Key");
            }
        }

        public string Label {
            get { return label; }
            set {
                if (label == value) { return; }
                label = value;
                RaisePropertyChanged("Label");
            }
        }

        public string CropName {
            get { return cropname; }
            set {
                if (cropname == value) { return; }
                cropname = value;
                RaisePropertyChanged("CropName");
            }
        }

        public string Name {
            get { return name; }
            set {
                if (name == value) { return; }
                name = value;
                RaisePropertyChanged("Name");
            }
        }

        public string Tag {
            get { return tag; }
            set {
                if (tag == value) { return; }
                tag = value;
                RaisePropertyChanged("Tag");
            }
        }

        public double Area {
            get { return area; }
            set {
                if (area == value) { return; }
                area = value;
                RaisePropertyChanged("Area");
            }
        }

        public Color MapColor {
            get { return mapcolor; }
            set {
                if (mapcolor == value) { return; }
                mapcolor = value;
                colorPickerColor = System.Windows.Media.Color.FromArgb(255, mapcolor.R, mapcolor.G, mapcolor.B);
                RaisePropertyChanged("MapColor");
                RaisePropertyChanged("WpfFillColor");
                RaisePropertyChanged("ColorPickerColor");
                if (parent != null && !isLoading) {
                    //parent.OnColorUpdated();
                }
            }
        }

        public System.Windows.Media.Color ColorPickerColor {
            get { return colorPickerColor; }
            set {
                if (colorPickerColor == value) { return; }
                colorPickerColor = value;
                MapColor = System.Drawing.Color.FromArgb(255, colorPickerColor.R, colorPickerColor.G, colorPickerColor.B);
                RaisePropertyChanged("MapColor");
                RaisePropertyChanged("WpfFillColor");
                RaisePropertyChanged("ColorPickerColor");
            }
        }

        public System.Windows.Media.SolidColorBrush WpfFillColor {
            get { return new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(mapcolor.A, mapcolor.R, mapcolor.G, mapcolor.B)); }
        }

        public int MapColorInt {
            get { return MapColor.ToArgb(); }
            set { MapColor = System.Drawing.Color.FromArgb(value); }
        }

        public Color MapColorFG {
            get { return mapcolorFG; }
            set {
                if (mapcolorFG == value) { return; }
                mapcolorFG = value;
                colorPickerColorFG = System.Windows.Media.Color.FromArgb(255, mapcolorFG.R, mapcolorFG.G, mapcolorFG.B);
                RaisePropertyChanged("MapColorFG");
                RaisePropertyChanged("WpfFillColorFG");
                RaisePropertyChanged("ColorPickerColorFG");
                if (parent != null && !isLoading) {
                    //parent.OnColorUpdated();
                }
            }
        }

        public System.Windows.Media.Color ColorPickerColorFG {
            get { return colorPickerColorFG; }
            set {
                if (colorPickerColorFG == value) { return; }
                colorPickerColorFG = value;
                MapColorFG = System.Drawing.Color.FromArgb(255, colorPickerColorFG.R, colorPickerColorFG.G, colorPickerColorFG.B);
                RaisePropertyChanged("MapColorFG");
                RaisePropertyChanged("WpfFillColorFG");
                RaisePropertyChanged("ColorPickerColorFG");
            }
        }

        public System.Windows.Media.SolidColorBrush WpfFillColorFG {
            get { return new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(mapcolorFG.A, mapcolorFG.R, mapcolorFG.G, mapcolorFG.B)); }
        }

        public int MapColorIntFG {
            get { return MapColorFG.ToArgb(); }
            set { MapColorFG = System.Drawing.Color.FromArgb(value); }
        }

        public IList<string> HatchTypeList {
            get { return hatchTypeList; }
            set {
                hatchTypeList = value;
                RaisePropertyChanged("HatchTypeList");
            }
        }

        public string HatchType {
            get { return hatchtype; }
            set {
                if (hatchtype == value) { return; }
                hatchtype = value;
                RaisePropertyChanged("HatchType");
                if (parent != null && !isLoading) {
                    parent.OnColorUpdated();
                }
            }
        }

        public bool Visible {
            get { return visible; }
            set {
                if (visible == value) { return; }
                visible = value;
                RaisePropertyChanged("Visible");
                if (parent != null && !isLoading) {
                    parent.OnColorUpdated();
                }
            }
        }

        public bool IsSelected {
            get { return isSelected; }
            set {
                if (isSelected == value) { return; }
                isSelected = value;
                RaisePropertyChanged("IsSelected");
            }
        }

        public bool ShowToggleButton {
            get { return showToggleButton; }
            set {
                if (showToggleButton == value) { return; }
                showToggleButton = value;
                RaisePropertyChanged("ShowColorPicker");
                RaisePropertyChanged("ShowToggleButton");
                RaisePropertyChanged("ToggleButtonVisibility");
                RaisePropertyChanged("ColorPickerVisibility");
            }
        }

        public bool ShowFilterItem {
            get { return showFilterItem; }
            set {
                if (showFilterItem == value) { return; }
                showFilterItem = value;
                RaisePropertyChanged("ShowFilterItem");
                //RaisePropertyChanged("FilterItemVisibility");
            }
        }

        public bool ShowColorPicker {
            get { return !showToggleButton; }
        }

        public System.Windows.Visibility ToggleButtonVisibility {
            get {
                if (showToggleButton) { return System.Windows.Visibility.Visible; }
                return System.Windows.Visibility.Collapsed;
            }
        }

        public System.Windows.Visibility ColorPickerVisibility {
            get {
                if (!showToggleButton) { return System.Windows.Visibility.Visible; }
                return System.Windows.Visibility.Collapsed;
            }
        }

        //public System.Windows.Visibility FilterItemVisibility {
        //    get {
        //        if (showFilterItem) { return System.Windows.Visibility.Visible; }
        //        return System.Windows.Visibility.Collapsed;
        //    }
        //}

        public bool IsLoading {
            get { return isLoading; }
            set { isLoading = value; }
        }

        public bool ColorPickerIsOpen {
            get { return colorPickerIsOpen; }
            set {
                if (colorPickerIsOpen == value) { return; }
                colorPickerIsOpen = value;
                RaisePropertyChanged("ColorPickerIsOpen");
                if (parent != null && !isLoading) {
                    parent.OnColorUpdated();
                }
            }
        }

        public bool ColorPickerFGIsOpen {
            get { return colorPickerFGIsOpen; }
            set {
                if (colorPickerFGIsOpen == value) { return; }
                colorPickerFGIsOpen = value;
                RaisePropertyChanged("ColorPickerFGIsOpen");
                if (parent != null && !isLoading) {
                    parent.OnColorUpdated();
                }
            }
        }
    }
}
