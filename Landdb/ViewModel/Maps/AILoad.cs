﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using AgC.UnitConversion;
using GalaSoft.MvvmLight;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Infrastructure;
using Landdb.Domain.ReadModels.Inventory;
using AgC.UnitConversion.MassAndWeight;
using AgC.UnitConversion.Area;
using AgC.UnitConversion.Volume;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using Landdb.Domain.ReadModels.ActiveIngredientSetting;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Fields;
using Landdb.Domain.ReadModels.Tree;
using System.Diagnostics;
using Landdb.Resources;

namespace Landdb.ViewModel.Maps {
    public class AILoad : ViewModelBase {
        readonly IClientEndpoint clientEndpoint;
        //readonly Dispatcher dispatcher;
        FertilizerFormulationViewModel fertilizerInformation;
        List<ActiveIngredientLineItemViewModel> activeIngredients;
        List<AnalysisModel> cropzonesAnalysis;

        public AILoad(IClientEndpoint clientEndpoint) {
            this.clientEndpoint = clientEndpoint;
            //this.dispatcher = dispatcher;
            SummaryItems = new ObservableCollection<ApplicationSummaryItem>();
        }

        public ObservableCollection<ApplicationSummaryItem> SummaryItems { get; private set; }
        public FertilizerFormulationViewModel FertilizerInformation {
            get { return fertilizerInformation; }
            set {
                fertilizerInformation = value;
                RaisePropertyChanged("FertilizerInformation");
            }
        }

        public List<ActiveIngredientLineItemViewModel> ActiveIngredients {
            get { return activeIngredients; }
            set {
                activeIngredients = value;
                RaisePropertyChanged("ActiveIngredients");
            }
        }

        public List<AnalysisModel> CropzonesAnalysis {
            get { return cropzonesAnalysis; }
            set {
                cropzonesAnalysis = value;
                RaisePropertyChanged("CropzonesAnalysis");
            }
        }

        public List<AnalysisModel> RefreshSummaryItems(List<TreeViewCropZoneItem> zones) {
            Stopwatch sw = Stopwatch.StartNew();
            Stopwatch cztime = Stopwatch.StartNew();
            double zonecount = 0;
            double aicount = 0;
            double j = 0;
            cropzonesAnalysis = new List<AnalysisModel>();
            var data = clientEndpoint.GetView<CropZoneApplicationDataView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear)).GetValue(new CropZoneApplicationDataView());
            foreach (TreeViewCropZoneItem treeModel in zones) {
                zonecount++;
                j++;
                SummaryItems.Clear();
                FertilizerInformation = null;

                FertilizerFormulationViewModel fertModel = new FertilizerFormulationViewModel();
                AnalysisModel model = new AnalysisModel() { FertilizerFormulation = fertModel };

                //var data = clientEndpoint.GetView<CropZoneApplicationDataView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear)).GetValue(new CropZoneApplicationDataView());
                //var inventory = clientEndpoint.GetView<InventoryListView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear)).GetValue(new InventoryListView());

                //IEnumerable<CropZoneApplicationDataItem> filtered = data.Items.Where(x => x.CropZoneId == treeModel.CropZoneId).OrderByDescending(x => x.StartDate);
                IEnumerable<CropZoneApplicationDataItem> filtered = data.Items.Where(x => x.CropZoneId == treeModel.CropZoneId).OrderByDescending(x => x.StartDate);

                if (filtered != null) {
                    foreach (CropZoneApplicationDataItem cadi in filtered) {
                        ApplicationSummaryItem asi = new ApplicationSummaryItem(clientEndpoint, new InventoryListView(), cadi);
                        SummaryItems.Add(asi);
                    }

                    //filtered.ForEach(x => SummaryItems.Add(new ApplicationSummaryItem(clientEndpoint, inventory, x)));

                    var analysis = CalculateFertilizerUsage(filtered, treeModel.GetArea(), treeModel.CropId);
                    FertilizerInformation = analysis.FertilizerFormulation;
                    ActiveIngredients = analysis.ActiveIngredients;
                    model = analysis;
                    model.Id = treeModel.CropZoneId.Id;
                    Measure itemareavalue = treeModel.GetArea();
                    if (itemareavalue.Value == 0) {
                        Measure czarea = GetCropZoneArea(new Guid(treeModel.CropZoneId.Id.ToString()));
                        itemareavalue = czarea;
                    }

                    model.Area = itemareavalue;
                    cropzonesAnalysis.Add(model);
                    aicount = aicount + analysis.ActiveIngredients.Count;
                }
                if (j >= 25) {
                    var swtotal = sw.Elapsed;
                    var cztotal = cztime.Elapsed;

                    string timestring = string.Format("Cropzones {0} of {1} | AI count: {2} | Time: {3}s | Interval: {4}ms", zonecount, zones.Count, aicount, swtotal.TotalSeconds.ToString("N0"), cztotal.TotalMilliseconds.ToString("N0"));
                    Console.WriteLine(timestring);
                    cztime.Stop();
                    cztime.Reset();
                    cztime.Start();
                    j = 0;
                }

            }
            return cropzonesAnalysis;
        }

        private Measure GetCropZoneArea(Guid Id) {
            string stringid = Id.ToString();
            Measure resultarea = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(0);
            var fieldMaybe = clientEndpoint.GetView<CropZoneDetailsView>(new CropZoneId(ApplicationEnvironment.CurrentDataSourceId, Id));
            fieldMaybe.IfValue(field => {
                if (field.ReportedArea != null && !string.IsNullOrWhiteSpace(field.ReportedAreaUnit)) {
                    resultarea = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(field.ReportedArea.Value);
                }
                else if (field.BoundaryArea != null && !string.IsNullOrWhiteSpace(field.BoundaryAreaUnit)) {
                    resultarea = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(field.BoundaryArea.Value);
                }
                else {
                    resultarea = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(0);
                }
            });
            return resultarea;
        }

        private AnalysisModel CalculateFertilizerUsage(IEnumerable<CropZoneApplicationDataItem> items, Measure czArea, CropId cropId) {
            FertilizerFormulationViewModel fertModel = new FertilizerFormulationViewModel();
            AnalysisModel model = new AnalysisModel() { FertilizerFormulation = fertModel };

            var ml = clientEndpoint.GetMasterlistService();
            var userSettings = clientEndpoint.GetUserSettings();
            double divisor = 1;
            double prodRate = 0;
            double areaRatio = 1;

            Dictionary<Guid, double> aiQuantitiesUsed = new Dictionary<Guid, double>();

            foreach (var item in items) {
                var product = ml.GetProduct(item.ProductId);
                // TODO FIX HACK - handle unknown products - MH
                if (product == null) { continue; }
                IUnit rateUnit = UnitFactory.GetUnitByName(item.RateUnit);
                Measure rateMeasure;
                rateMeasure = rateUnit.GetMeasure((double)item.RateValue, product.Density);

                areaRatio = 1;
                if (item.AreaUnit == czArea.Unit.Name) {
                    if (czArea.Value != 0) {
                        areaRatio = item.AreaValue / czArea.Value;
                    }
                }
                else if (item.AreaUnit == "acre" && czArea.Unit.Name == "acres") {
                    if (czArea.Value != 0) {
                        areaRatio = item.AreaValue / czArea.Value;
                    }
                }
                else {
                    var itemAreaUnit = UnitFactory.GetUnitByName(item.AreaUnit);
                    var itemArea = itemAreaUnit.GetMeasure(item.AreaValue);
                    var consistentItemArea = itemArea.GetValueAs(czArea.Unit);
                    if (czArea.Value != 0) {
                        areaRatio = consistentItemArea / czArea.Value;
                    }
                }

                if (rateMeasure is AreaMeasure) {
                    divisor = 1;
                    prodRate = rateMeasure.CreateAs(userSettings.UserAreaUnit).Value;
                }
                else if (rateMeasure is MassAndWeightMeasure || (rateMeasure is VolumeMeasure && rateMeasure.CanConvertTo(userSettings.UserMassUnit))) {
                    divisor = 100;
                    prodRate = rateMeasure.CreateAs(userSettings.UserMassUnit).Value;

                    foreach (var ai in product.ActiveIngredients ?? new List<ActiveIngredientContent>()) {
                        if (ai.Id == Guid.Empty) { continue; }
                        double massOfAiApplied = prodRate * areaRatio * (double)ai.Percent;

                        if (Landdb.Domain.ReadModels.Services.ActiveIngredientXRef.Self.IsGoodActiveIngredient(ai.Id)) {
                            var _id = Landdb.Domain.ReadModels.Services.ActiveIngredientXRef.Self.CrossReferenceActiveIngredient(ai.Id);
                            if (aiQuantitiesUsed.ContainsKey(_id)) {
                                aiQuantitiesUsed[_id] += massOfAiApplied;
                            }
                            else {
                                aiQuantitiesUsed.Add(_id, massOfAiApplied);
                            }
                        }
                    }
                }
                else { continue; }

                if (divisor == 0) { continue; }
                decimal multiplicand = (decimal)prodRate * (decimal)areaRatio / (decimal)divisor;

                #region Increment Fertilizer Counts
                if (product.Formulation != null) {
                    var f = product.Formulation;

                    if (f.N.HasValue) {
                        fertModel.N += f.N.Value * multiplicand;
                    }
                    if (f.P.HasValue) {
                        fertModel.P += f.P.Value * multiplicand;
                    }
                    if (f.K.HasValue) {
                        fertModel.K += f.K.Value * multiplicand;
                    }
                    if (f.Ca.HasValue) {
                        fertModel.Ca += f.Ca.Value * multiplicand;
                    }
                    if (f.Mg.HasValue) {
                        fertModel.Mg += f.Mg.Value * multiplicand;
                    }
                    if (f.S.HasValue) {
                        fertModel.S += f.S.Value * multiplicand;
                    }
                    if (f.B.HasValue) {
                        fertModel.B += f.B.Value * multiplicand;
                    }
                    if (f.Cl.HasValue) {
                        fertModel.Cl += f.Cl.Value * multiplicand;
                    }
                    if (f.Cu.HasValue) {
                        fertModel.Cu += f.Cu.Value * multiplicand;
                    }
                    if (f.Fe.HasValue) {
                        fertModel.Fe += f.Fe.Value * multiplicand;
                    }
                    if (f.Mn.HasValue) {
                        fertModel.Mn += f.Mn.Value * multiplicand;
                    }
                    if (f.Mo.HasValue) {
                        fertModel.Mo += f.Mo.Value * multiplicand;
                    }
                    if (f.Zn.HasValue) {
                        fertModel.Zn += f.Zn.Value * multiplicand;
                    }
                }
                #endregion
            }

            foreach (var activeIngredientId in aiQuantitiesUsed.Keys) {
                var aiId = new ActiveIngredientId(activeIngredientId);
                var aiName = ml.GetActiveIngredientDisplay(aiId);
                var usedQuantity = aiQuantitiesUsed[activeIngredientId];
                double? maxQuantity = null;
                var aiSetting = clientEndpoint.GetView<ActiveIngredientSettingsView>(new ActiveIngredientSettingId(ApplicationEnvironment.CurrentDataSourceId, aiId));
                aiSetting.IfValue(x => {

                    //added in an else statement to grab previous years value of max ai ~~ Shea
                    var cropYear = ApplicationEnvironment.CurrentCropYear;
                    var yearSettings = new List<ActiveIngredientSettingItem>();

                    if (x.ItemsByYear.ContainsKey(cropYear))
                    {
                        var item = x.ItemsByYear[cropYear].Where(y => y.CropId == cropId).FirstOrDefault();
                        if (item != null) {
                            maxQuantity = (double?)item.MaximumValue;
                        }
                    }
                    else
                    {
                        yearSettings = (from m in x.ItemsByYear where m.Key < cropYear select m).Any()
                            ? (from m in x.ItemsByYear where m.Key < cropYear select m).Last().Value : new List<ActiveIngredientSettingItem>();

                        //var item = yearSettings.Where(c => c.CropId == cropId).Last();
                        var item = yearSettings.Where(c => c.CropId == cropId).Any() ? yearSettings.Where(c => c.CropId == cropId).Last() : null;
                        if (item != null)
                        {
                            maxQuantity = (double?)item.MaximumValue;
                        }
                    }
                });

                string cropname = clientEndpoint.GetMasterlistService().GetCropDisplay(cropId);

                model.ActiveIngredients.Add(new ActiveIngredientLineItemViewModel(clientEndpoint, userSettings, aiId, aiName, usedQuantity, maxQuantity, cropId, cropname));
            }

            return model;
        }

    }

    public class AnalysisModel {
        public AnalysisModel() {
            ActiveIngredients = new List<ActiveIngredientLineItemViewModel>();
        }

        public Guid Id { get; set; }
        public Measure Area { get; set; }
        public FertilizerFormulationViewModel FertilizerFormulation { get; set; }
        public List<ActiveIngredientLineItemViewModel> ActiveIngredients { get; set; }
    }

    public class FertilizerFormulationViewModel : ViewModelBase {
        public decimal N { get; set; }
        public decimal P { get; set; }
        public decimal K { get; set; }
        public decimal Ca { get; set; }
        public decimal Mg { get; set; }
        public decimal S { get; set; }
        public decimal B { get; set; }
        public decimal Cl { get; set; }
        public decimal Cu { get; set; }
        public decimal Fe { get; set; }
        public decimal Mn { get; set; }
        public decimal Mo { get; set; }
        public decimal Zn { get; set; }

        public override string ToString() {
            var s = string.Format("{0} - {1} - {2}", N.ToString("n2"), P.ToString("n2"), K.ToString("n2"));
            return s;
        }
    }

    public class ActiveIngredientLineItemViewModel : ViewModelBase {
        UserSettings settings;
        CropId CropId;
        IClientEndpoint clientEndpoint;

        public ActiveIngredientLineItemViewModel(IClientEndpoint clientEndpoint, UserSettings settings, ActiveIngredientId activeIngredientId, string name, double usedQuantity, double? maxQuantity, CropId cropId, string cropname) {
            this.clientEndpoint = clientEndpoint;
            this.settings = settings;
            this.ActiveIngredientId = activeIngredientId;
            this.Name = name;
            this.UsedQuantity = usedQuantity;
            this.MaxQuantity = maxQuantity ?? 0.1;
            this.CropId = cropId;
            this.CropName = cropname;

            UpdatePercentAndText();

            UpdateMaximumUseCommand = new RelayCommand(UpdateMaximumUse);
        }

        private void UpdatePercentAndText() {
            if (MaxQuantity.HasValue && MaxQuantity > 0) {
                this.PercentRemaining = ((MaxQuantity.Value - UsedQuantity) / MaxQuantity.Value) * 100;
                if (this.PercentRemaining < 0) { this.PercentRemaining = 0; }
                this.PercentRemainingText = string.Format(Strings.PercentRemaining_Text, this.PercentRemaining);
                this.QuantityText = string.Format(Strings.AnalysisQuantity_Text, UsedQuantity, MaxQuantity.Value, "lb", ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit);
            } else {
                this.PercentRemaining = 0;
                this.PercentRemainingText = string.Empty;
                this.QuantityText = string.Format(Strings.AnalysisQuantityPercentRemainingZero_Text, UsedQuantity, "lb", ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit);
            }
            RaisePropertyChanged("PercentRemaining");
            RaisePropertyChanged("PercentRemainingText");
            RaisePropertyChanged("QuantityText");
        }

        public ICommand UpdateMaximumUseCommand { get; private set; }

        public ActiveIngredientId ActiveIngredientId { get; set; }
        public string Name { get; set; }
        public string CropName { get; set; }
        public double UsedQuantity { get; set; }
        public double? MaxQuantity { get; set; }

        public string QuantityText { get; set; }
        
        public double PercentRemaining { get; set; }
        public string PercentRemainingText { get; set; }

        public double Percentage {
            get {
                if (MaxQuantity == null || MaxQuantity == 0) { return double.NaN; }
                return UsedQuantity / MaxQuantity.Value;
            }
        }

        public string RatioText {
            get {
                //string rateText = string.Format(Landdb.Properties.Resources.RatePerAreaDisplayText, DataConfiguration.Instance.UserMassUnit.AbbreviatedDisplay, DataConfiguration.Instance.UserAreaUnit.FullDisplay);
                string rateText = string.Format(Strings.Per_Format_Text, settings.UserMassUnit.AbbreviatedDisplay, settings.UserAreaUnit.FullDisplay);
                return string.Format(Strings.CountOfCountName_Format_Text, UsedQuantity.ToString("n2"), MaxQuantity.GetValueOrDefault().ToString("n2"), rateText);
            }
        }

        public override string ToString() {
            return ActiveIngredientId.ToString() + " | " + UsedQuantity.ToString() + " | " + MaxQuantity.ToString() + " | " + Percentage.ToString() + "%";
        }

        void UpdateMaximumUse() {
            var vm = new Shared.Popups.EditActiveIngredientLoadViewModel((decimal)MaxQuantity.GetValueOrDefault(), x => {
                var domainCmd = new UpdateMaximumActiveIngredientUsePerCropYear(new ActiveIngredientSettingId(ApplicationEnvironment.CurrentDataSourceId, ActiveIngredientId),
                    new MessageMetadata(clientEndpoint.DeviceId, clientEndpoint.UserId), x.MaximumValue, Pound.Self.Name, CropId, ApplicationEnvironment.CurrentCropYear, string.Empty);
                clientEndpoint.SendOne(domainCmd);

                if (x.MaximumValue <= 0) {
                    MaxQuantity = null;
                } else {
                    MaxQuantity = (double)x.MaximumValue;
                }
                UpdatePercentAndText();
            });
            var descriptor = new ScreenDescriptor("Landdb.Views.Shared.Popups.EditActiveIngredientLoadView", "EditAiMax", vm);
            Messenger.Default.Send<ShowPopupMessage>(new ShowPopupMessage() { ScreenDescriptor = descriptor });
        }

    }

    public class ApplicationSummaryItem : ViewModelBase {
        readonly CropZoneApplicationDataItem item;
        readonly IClientEndpoint clientEndpoint;
        readonly InventoryListView inventory;

        double? costPerArea;
        double? totalCost;

        public ApplicationSummaryItem(IClientEndpoint clientEndpoint, InventoryListView inventory, CropZoneApplicationDataItem item) {
            this.clientEndpoint = clientEndpoint;
            this.inventory = inventory;
            this.item = item;

            if (inventory != null && inventory.Products.ContainsKey(item.ProductId)) {
                var ip = inventory.Products[item.ProductId];
                var costPerUnit = (decimal)ip.AveragePriceValue;
                var costUnit = ip.AveragePriceUnit ?? item.TotalProductUnit;

                try {
                    var costMeasure = clientEndpoint.GetProductUnitResolver().GetPackageSafeUnit(item.ProductId, costUnit).GetMeasure((double)costPerUnit); // TODO: Account for density?
                    var totalCostMeasure = costMeasure.GetValueAs(clientEndpoint.GetProductUnitResolver().GetPackageSafeUnit(item.ProductId, item.TotalProductUnit));
                    totalCost = totalCostMeasure * item.TotalGrowerProductValue;
                    costPerArea = totalCost / item.AreaValue;
                }
                catch {
                    // TODO FIX HACK - this bunch of code is a hack to get around incompatible measues - MH
                    MiniProduct miniprod = clientEndpoint.GetMasterlistService().GetProduct(item.ProductId);
                    double? proddensity = miniprod.Density;
                    var costMeasure = clientEndpoint.GetProductUnitResolver().GetPackageSafeUnit(item.ProductId, costUnit).GetMeasure((double)costPerUnit, proddensity);
                    var totalCostMeasure = costMeasure.GetValueAs(clientEndpoint.GetProductUnitResolver().GetPackageSafeUnit(item.ProductId, item.TotalProductUnit));
                    totalCost = totalCostMeasure * item.TotalGrowerProductValue;
                    costPerArea = totalCost / item.AreaValue;
                    //
                }

            } else {
                costPerArea = item.CostPerArea;
                totalCost = item.TotalCost;
            }

        }

        public ApplicationId ApplicationId { 
            get { return item.ApplicationId; } 
        }
        public DateTime StartDate {
            get { return item.StartDate; }
        }

        public string ProductDisplay {
            get {
                var display = Strings.UnknownProduct_Text;
                var ml = clientEndpoint.GetMasterlistService();
                if (ml != null) {
                    display = ml.GetProductDisplay(item.ProductId);
                }
                return display; 
            }
        }

        public string RateDisplay {
            get {
                var unit = UnitFactory.GetUnitByName(item.RateUnit);
                var areaUnit = UnitFactory.GetUnitByName(item.AreaUnit);
                return string.Format("{0:n2} {1} / {2} ({3} {4})", item.RateValue, unit.FullDisplay, areaUnit.AbbreviatedDisplay, item.AreaValue.ToString("n2"), areaUnit.AbbreviatedDisplay);
            }
        }

        public decimal RateValue {
            get { return item.RateValue; }
        }

        public string RateUnit {
            get { return item.RateUnit; }
        }

        public double AreaValue {
            get { return item.AreaValue; }
        }

        public string AreaUnit {
            get { return item.AreaUnit; }
        }

        public double CostPerArea {
            get {
                // Lazy-init this for perf
                if (costPerArea == null) {
                    CalculateCosts();
                }
                return costPerArea.GetValueOrDefault(); 
            }
        }

        public string TotalProductDisplay {
            get {
                var unit = UnitFactory.GetUnitByName(TotalProductUnit);
                return string.Format("{0:n2} {1}", item.TotalProductValue, unit.FullDisplay);
            }
        }

        public double TotalProductValue {
            get { return item.TotalProductValue; }
        }

        public string TotalProductUnit {
            get { return item.TotalProductUnit; }
        }

        public double TotalCost {
            get {
                // Lazy-init this for perf
                if (totalCost == null) {
                    CalculateCosts();
                }
                return totalCost.GetValueOrDefault(); 
            }
        }

        public double TotalShareOwnerQuantity {
            get { return item.TotalProductValue - item.TotalGrowerProductValue; }
        }

        public double TotalGrowerQuantity {
            get { return item.TotalGrowerProductValue; }
        }

        void CalculateCosts() {
            if (inventory != null && inventory.Products.ContainsKey(item.ProductId)) {
                var ip = inventory.Products[item.ProductId];
                var costPerUnit = (decimal)ip.AveragePriceValue;
                var costUnit = ip.AveragePriceUnit;

                var costMeasure = clientEndpoint.GetProductUnitResolver().GetPackageSafeUnit(item.ProductId, costUnit).GetMeasure((double)costPerUnit); // TODO: Account for density?
                var totalCostMeasure = costMeasure.GetValueAs(clientEndpoint.GetProductUnitResolver().GetPackageSafeUnit(item.ProductId, item.TotalProductUnit));
                totalCost = totalCostMeasure * item.TotalGrowerProductValue;
                costPerArea = totalCost / item.AreaValue;
            } else {
                costPerArea = item.CostPerArea;
                totalCost = item.TotalCost;
            }
        }
    }
}
