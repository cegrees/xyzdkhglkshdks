﻿using AgC.UnitConversion;
using AgC.UnitConversion.Area;
using GalaSoft.MvvmLight;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Map;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Landdb.Resources;
using ThinkGeo.MapSuite.Core;
using ThinkGeo.MapSuite.WpfDesktopEdition;

namespace Landdb.ViewModel.Maps {
    public class SelectedShapeExport {
        IClientEndpoint clientEndpoint;
        Proj4Projection projection;
        MapSettings mapsettings;
        string documentname;
        string selectionname;
        string timename;
        bool multifile;
        int areadecimals;
        String invalidChars = @"~!@#$%^&*()`[]{}|\/?'"".,<>-+=";

        public SelectedShapeExport(IClientEndpoint clientEndpoint, Proj4Projection projection, string documentname, string selectionname, bool multifile) {
            this.clientEndpoint = clientEndpoint;
            this.projection = projection;
            this.documentname = documentname;
            this.selectionname = selectionname;
            this.timename = @"_" + DateTime.Now.Ticks.ToString();
            this.multifile = multifile;

            mapsettings = clientEndpoint.GetMapSettings();
            areadecimals = mapsettings.AreaDecimals;
        }

        public void ExportShapeFile(IList<MapItem> clientLayerItems) {
            this.timename = @"_" + DateTime.Now.Ticks.ToString();
            var fulldirectoryname = Path.Combine(ApplicationEnvironment.ExportDirectory, documentname);
            if (!Directory.Exists(ApplicationEnvironment.ExportDirectory)) {
                Directory.CreateDirectory(ApplicationEnvironment.ExportDirectory);
            }
            if (!Directory.Exists(fulldirectoryname)) {
                Directory.CreateDirectory(fulldirectoryname);
            }
            //"shp files (*.shp)|*.shp|kml files (*.kml)|*.kml"
            //string filename = AddLayersBySaveFileDialog(fulldirectoryname, @"Export Shapes File", @"shp", @"shp files (*.shp)|*.shp");
            string filename = AddLayersBySaveFileDialog(fulldirectoryname, @"Export Shapes File", @"shp", @"shp files (*.shp)|*.shp|kml files (*.kml)|*.kml|geojson files (*.geojson)|*.geojson");
            if (string.IsNullOrEmpty(filename)) { return; }
            if (filename.EndsWith(".shp"))
            {

                List<DbfColumn> columns = new List<DbfColumn>();
                columns.Add(new DbfColumn(@"LdbLabel", DbfColumnType.Character, 64, 0));
                columns.Add(new DbfColumn(@"AreaLabel", DbfColumnType.Character, 64, 0));
                columns.Add(new DbfColumn(@"AreaOnlyLabel", DbfColumnType.Character, 64, 0));

                //string filename = selectionname + timename;
                ShapeFileFeatureLayer shapeFileFeatureLayer1 = new ShapeFileFeatureLayer();
                if (!multifile)
                {
                    var fullfilename = Path.Combine(fulldirectoryname, filename + @".shp");
                    shapeFileFeatureLayer1.FeatureSource.Projection = projection;
                    shapeFileFeatureLayer1 = CreateNewFeaturesShapeFile(fullfilename, columns, ShapeFileType.Polygon);
                    shapeFileFeatureLayer1.Open();
                    shapeFileFeatureLayer1.EditTools.BeginTransaction();
                }

                foreach (var m in clientLayerItems)
                {
                    string stringid = m.FieldId.ToString(); ;
                    if (m.MapData != null && !string.IsNullOrEmpty(m.MapData) && m.MapData != "MULTIPOLYGON EMPTY")
                    {
                        try
                        {
                            string customfilename = GetCustomFileName(m.FarmId, m.FieldId, m.CropZoneId, documentname);
                            //string customarealabel = GetCustomFieldName(m.FieldId, m.Name);
                            string customareaonlylabel = GetCustomAreaLabel(m.FieldId, true);
                            string customarealabel = DisplayLabelWithArea(m.Name, customareaonlylabel);
                            if (m.CropZoneId != null)
                            {
                                //customarealabel = GetCustomCropZoneName(new Guid(m.CropZoneId.ToString()), m.Name);
                                customareaonlylabel = GetCustomAreaLabel(new Guid(m.CropZoneId.ToString()), false);
                                customarealabel = DisplayLabelWithArea(m.Name, customareaonlylabel);
                            }
                            IDictionary<string, string> columnvalues = new Dictionary<string, string>();
                            columnvalues.Add(@"LdbLabel", m.Name);
                            columnvalues.Add(@"AreaLabel", customarealabel);
                            columnvalues.Add(@"AreaOnlyLabel", customareaonlylabel);

                            if (multifile)
                            {
                                filename = customfilename;
                                var fullfilename = Path.Combine(fulldirectoryname, filename + @".shp");
                                shapeFileFeatureLayer1 = new ShapeFileFeatureLayer();
                                shapeFileFeatureLayer1.FeatureSource.Projection = projection;
                                shapeFileFeatureLayer1 = CreateNewFeaturesShapeFile(fullfilename, columns, ShapeFileType.Polygon);
                                shapeFileFeatureLayer1.Open();
                                shapeFileFeatureLayer1.EditTools.BeginTransaction();
                            }
                            int nrecords = 0;
                            //MapItem
                            var newfeature = new Feature(m.MapData, m.FieldId.ToString(), columnvalues);
                            shapeFileFeatureLayer1.FeatureSource.AddFeature(newfeature);

                            if (multifile)
                            {
                                shapeFileFeatureLayer1.EditTools.CommitTransaction();
                                shapeFileFeatureLayer1.Close();
                            }
                        }
                        catch { }
                    }
                }

                if (!multifile)
                {
                    shapeFileFeatureLayer1.EditTools.CommitTransaction();
                    shapeFileFeatureLayer1.Close();
                }
                return;
            }
            if (filename.EndsWith(".kml"))
            {
                SharpKml.Dom.Document document = new SharpKml.Dom.Document();
                document.Name = filename;
                document.Open = false;
                foreach (var m in clientLayerItems)
                {
                    string stringid = m.FieldId.ToString(); ;
                    if (m.MapData != null && !string.IsNullOrEmpty(m.MapData) && m.MapData != "MULTIPOLYGON EMPTY")
                    {
                        try
                        {
                            string customfilename = GetCustomFileName(m.FarmId, m.FieldId, m.CropZoneId, documentname);
                            string customareaonlylabel = GetCustomAreaLabel(m.FieldId, true);
                            string customarealabel = DisplayLabelWithArea(m.Name, customareaonlylabel);
                            if (m.CropZoneId != null)
                            {
                                customareaonlylabel = GetCustomAreaLabel(new Guid(m.CropZoneId.ToString()), false);
                                customarealabel = DisplayLabelWithArea(m.Name, customareaonlylabel);
                            }
                            IDictionary<string, string> columnvalues = new Dictionary<string, string>();
                            columnvalues.Add(@"LdbLabel", m.Name);
                            columnvalues.Add(@"AreaLabel", customarealabel);
                            columnvalues.Add(@"AreaOnlyLabel", customareaonlylabel);

                            if (multifile)
                            {
                                //filename = customfilename;
                                //var fullfilename = Path.Combine(fulldirectoryname, filename + @".shp");
                                //shapeFileFeatureLayer1 = new ShapeFileFeatureLayer();
                                //shapeFileFeatureLayer1.FeatureSource.Projection = projection;
                                //shapeFileFeatureLayer1 = CreateNewFeaturesShapeFile(fullfilename, columns, ShapeFileType.Polygon);
                                //shapeFileFeatureLayer1.Open();
                                //shapeFileFeatureLayer1.EditTools.BeginTransaction();
                            }
                            int nrecords = 0;
                            //MapItem
                            var newfeature = new Feature(m.MapData, m.FieldId.ToString(), columnvalues);
                            SharpKml.Dom.Placemark placemark = TranslateMultipolygonFeatureToKML(newfeature);
                            document.AddFeature(placemark);

                            if (multifile)
                            {
                                //shapeFileFeatureLayer1.EditTools.CommitTransaction();
                                //shapeFileFeatureLayer1.Close();
                            }
                        }
                        catch { }
                    }
                }
                var kml = new SharpKml.Dom.Kml();
                kml.Feature = document;
                SharpKml.Engine.KmlFile kmlfile = SharpKml.Engine.KmlFile.Create(kml, false);
                using (var stream = System.IO.File.OpenWrite(filename))
                {
                    kmlfile.Save(stream);
                }
                return;
            }
            if (filename.EndsWith(".geojson"))
            {
                string ob = "{";
                string cb = "}";
                StringBuilder geojsonfeaturelist = new StringBuilder();
                foreach (var m in clientLayerItems)
                {
                    string stringid = m.FieldId.ToString(); ;
                    if (m.MapData != null && !string.IsNullOrEmpty(m.MapData) && m.MapData != "MULTIPOLYGON EMPTY")
                    {
                        try
                        {
                            string customfilename = GetCustomFileName(m.FarmId, m.FieldId, m.CropZoneId, documentname);
                            //string customarealabel = GetCustomFieldName(m.FieldId, m.Name);
                            string customareaonlylabel = GetCustomAreaLabel(m.FieldId, true);
                            string customarealabel = DisplayLabelWithArea(m.Name, customareaonlylabel);
                            if (m.CropZoneId != null)
                            {
                                //customarealabel = GetCustomCropZoneName(new Guid(m.CropZoneId.ToString()), m.Name);
                                customareaonlylabel = GetCustomAreaLabel(new Guid(m.CropZoneId.ToString()), false);
                                customarealabel = DisplayLabelWithArea(m.Name, customareaonlylabel);
                            }
                            IDictionary<string, string> columnvalues = new Dictionary<string, string>();
                            columnvalues.Add(@"LdbLabel", m.Name);
                            columnvalues.Add(@"AreaLabel", customarealabel);
                            columnvalues.Add(@"AreaOnlyLabel", customareaonlylabel);
                            var newfeature = new Feature(m.MapData, m.FieldId.ToString(), columnvalues);

                            if (multifile)
                            {
                                filename = customfilename;
                                var fullfilename = Path.Combine(fulldirectoryname, filename + @".geojson");
                                geojsonfeaturelist = new StringBuilder();
                            }
                            string geojsonfeature = newfeature.GetGeoJson();

                            if (geojsonfeaturelist.Length == 0)
                            {
                                geojsonfeaturelist = new StringBuilder(geojsonfeature);
                            }
                            else
                            {
                                geojsonfeaturelist.Append(",");
                                geojsonfeaturelist.Append(geojsonfeature);
                            }
                            if (multifile)
                            {
                                string jsoncontent1 = $"{ob}\"type\":\"FeatureCollection\",\"features\":[{geojsonfeaturelist}]{cb}";
                                File.WriteAllText(filename, jsoncontent1);
                            }

                        }
                        catch { }
                    }
                }
                if (!multifile)
                {
                    string jsoncontent = $"{ob}\"type\":\"FeatureCollection\",\"features\":[{geojsonfeaturelist}]{cb}";
                    File.WriteAllText(filename, jsoncontent);
                }
                return;
            }
        }

        public SharpKml.Dom.Placemark TranslateMultipolygonFeatureToKML(ThinkGeo.MapSuite.Core.Feature feature)
        {
            string name = feature.ColumnValues[@"LdbLabel"];
            MultipolygonShape mpoly = feature.GetShape() as MultipolygonShape;
            if (mpoly == null) { return null; }
            byte byte_Color_R = 150, byte_Color_G = 150, byte_Color_B = 150, byte_Color_A = 100; //you may get your own color by other method
            var style = new SharpKml.Dom.Style();
            style.Polygon = new SharpKml.Dom.PolygonStyle();
            style.Polygon.ColorMode = SharpKml.Dom.ColorMode.Normal;
            style.Polygon.Color = new SharpKml.Base.Color32(byte_Color_A, byte_Color_B, byte_Color_G, byte_Color_R);
            SharpKml.Dom.MultipleGeometry multiplegeometry = new SharpKml.Dom.MultipleGeometry();
            int i = 0;
            foreach (var poly in mpoly.Polygons)
            {
                i++;
                SharpKml.Dom.Polygon polygon = new SharpKml.Dom.Polygon();
                polygon.Extrude = true;
                polygon.AltitudeMode = SharpKml.Dom.AltitudeMode.ClampToGround;
                //Color Style Setting:
                SharpKml.Dom.OuterBoundary outerBoundary = new SharpKml.Dom.OuterBoundary();
                outerBoundary.LinearRing = new SharpKml.Dom.LinearRing();
                outerBoundary.LinearRing.Coordinates = new SharpKml.Dom.CoordinateCollection();
                foreach (Vertex vertex in poly.OuterRing.Vertices)
                {
                    outerBoundary.LinearRing.Coordinates.Add(new SharpKml.Base.Vector(vertex.Y, vertex.X));
                }
                polygon.OuterBoundary = outerBoundary;
                foreach (RingShape ringshape in poly.InnerRings)
                {
                    SharpKml.Dom.InnerBoundary innerboundary = new SharpKml.Dom.InnerBoundary();
                    innerboundary.LinearRing = new SharpKml.Dom.LinearRing();
                    innerboundary.LinearRing.Coordinates = new SharpKml.Dom.CoordinateCollection();
                    foreach (Vertex vertex in ringshape.Vertices)
                    {
                        innerboundary.LinearRing.Coordinates.Add(new SharpKml.Base.Vector(vertex.Y, vertex.X));
                    }
                    polygon.AddInnerBoundary(innerboundary);
                }
                multiplegeometry.AddGeometry(polygon);
            }
            SharpKml.Dom.Placemark placemark = new SharpKml.Dom.Placemark();
            placemark.Name = name;
            placemark.Geometry = multiplegeometry;
            placemark.AddStyle(style);
            return placemark;
        }

        internal string AddLayersBySaveFileDialog(string initialDirectory, string title, string defaultext, string filterstring) {
            SaveFileDialog savefiledialog = new SaveFileDialog();
            savefiledialog.InitialDirectory = initialDirectory;
            savefiledialog.OverwritePrompt = true;
            savefiledialog.Title = title;
            savefiledialog.DefaultExt = defaultext;
            savefiledialog.Filter = filterstring;
            bool? dialogresult = savefiledialog.ShowDialog();
            if (dialogresult.HasValue && dialogresult.Value) {
                return savefiledialog.FileName;
            }
            return string.Empty;
        }

        public ShapeFileFeatureLayer CreateNewFeaturesShapeFile(string newfilenamewithpath, List<DbfColumn> columns, ShapeFileType shapefiletype) {
            ShapeFileFeatureSource.CreateShapeFile(shapefiletype, newfilenamewithpath, columns);
            ShapeFileFeatureLayer shapeFileFeatureLayer1 = new ShapeFileFeatureLayer(newfilenamewithpath, ShapeFileReadWriteMode.ReadWrite);
            return shapeFileFeatureLayer1;
        }

        private string GetCustomFileName(Guid FarmId, Guid FieldId, Guid? CropzoneId, string name) {
            string stringfarmid = FarmId.ToString();
            string stringfieldid = FieldId.ToString();
            string stringcropzoneid = CropzoneId.ToString();
            string labelname = name;
            var farmMaybe = clientEndpoint.GetView<FarmDetailsView>(new FarmId(ApplicationEnvironment.CurrentDataSourceId, FarmId));
            farmMaybe.IfValue(farm => {
                labelname = farm.FarmNameByCropYear[ApplicationEnvironment.CurrentCropYear] + "__";
            });
            var fieldMaybe = clientEndpoint.GetView<FieldDetailsView>(new FieldId(ApplicationEnvironment.CurrentDataSourceId, FieldId));
            fieldMaybe.IfValue(field => {
                if (CropzoneId == null) {
                    labelname += field.FieldNameByCropYear[ApplicationEnvironment.CurrentCropYear]; //.Name;
                }
                else {
                    labelname += field.FieldNameByCropYear[ApplicationEnvironment.CurrentCropYear] + "__";
                }
            });

            if (CropzoneId != null) {
                var cropzoneMaybe = clientEndpoint.GetView<CropZoneDetailsView>(new CropZoneId(ApplicationEnvironment.CurrentDataSourceId, new Guid(CropzoneId.ToString())));
                cropzoneMaybe.IfValue(cropzone => {
                    labelname += cropzone.Name;
                });
            }
            return labelname;
        }

        //private string GetCustomFieldName(Guid Id, string name) {
        //    string stringid = Id.ToString();
        //    string labelname = name;
        //    var fieldMaybe = clientEndpoint.GetView<FieldDetailsView>(new FieldId(ApplicationEnvironment.CurrentDataSourceId, Id));
        //    fieldMaybe.IfValue(field => {
        //        if (field.ReportedArea != null && !string.IsNullOrWhiteSpace(field.ReportedAreaUnit)) {
        //            labelname = DisplaySelectedAreaDecimals(name, field.ReportedArea.Value, field.ReportedAreaUnit);
        //        }
        //        else if (field.BoundaryArea != null && !string.IsNullOrWhiteSpace(field.BoundaryAreaUnit)) {
        //            labelname = DisplaySelectedAreaDecimals(name, field.BoundaryArea.Value, field.BoundaryAreaUnit);
        //        }
        //        else {
        //            labelname = name + " " + UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(0);
        //        }

        //    });
        //    return labelname;
        //}

        //private string GetCustomCropZoneName(Guid Id, string name) {
        //    string stringid = Id.ToString();
        //    string labelname = name;
        //    var fieldMaybe = clientEndpoint.GetView<CropZoneDetailsView>(new CropZoneId(ApplicationEnvironment.CurrentDataSourceId, Id));
        //    fieldMaybe.IfValue(field => {
        //        if (field.ReportedArea != null && !string.IsNullOrWhiteSpace(field.ReportedAreaUnit)) {
        //            labelname = DisplaySelectedAreaDecimals(name, field.ReportedArea.Value, field.ReportedAreaUnit);
        //        }
        //        else if (field.BoundaryArea != null && !string.IsNullOrWhiteSpace(field.BoundaryAreaUnit)) {
        //            labelname = DisplaySelectedAreaDecimals(name, field.BoundaryArea.Value, field.BoundaryAreaUnit);
        //        }
        //        else {
        //            labelname = name + " " + UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(0);
        //        }

        //    });
        //    return labelname;
        //}

        //private string DisplaySelectedAreaDecimals(string name, double unitvalue, string unitname) {
        //    IUnit _unit = UnitFactory.GetUnitByName(unitname);
        //    string decimalstring = "N" + areadecimals.ToString("N0");
        //    //  + "\r\n" + 
        //    return name + "\r\n" + unitvalue.ToString(decimalstring);
        //    //return name + ": " + unitvalue.ToString(decimalstring) + " " + _unit.AbbreviatedDisplay;
        //}

        private string DisplayLabelWithArea(string name, string customarealabel) {
            return name + "\r\n" + customarealabel;
        }

        private string GetCustomAreaLabel(Guid Id, bool isfieldlabel) {
            string stringid = Id.ToString();
            string labelname = string.Empty;
            if (isfieldlabel) {
                var fieldMaybe = clientEndpoint.GetView<FieldDetailsView>(new FieldId(ApplicationEnvironment.CurrentDataSourceId, Id));
                fieldMaybe.IfValue(field => {
                    labelname = string.Empty;
                    if (field.ReportedArea != null && !string.IsNullOrWhiteSpace(field.ReportedAreaUnit)) {
                        labelname = DisplaySelectedAreaOnlyDecimals(field.ReportedArea.Value, field.ReportedAreaUnit);
                    }
                    else if (field.BoundaryArea != null && !string.IsNullOrWhiteSpace(field.BoundaryAreaUnit)) {
                        labelname = DisplaySelectedAreaOnlyDecimals(field.BoundaryArea.Value, field.BoundaryAreaUnit);
                    }
                });
            }
            else {
                var fieldMaybe = clientEndpoint.GetView<CropZoneDetailsView>(new CropZoneId(ApplicationEnvironment.CurrentDataSourceId, Id));
                fieldMaybe.IfValue(field => {
                    labelname = string.Empty;
                    if (field.ReportedArea != null && !string.IsNullOrWhiteSpace(field.ReportedAreaUnit)) {
                        labelname = DisplaySelectedAreaOnlyDecimals(field.ReportedArea.Value, field.ReportedAreaUnit);
                    }
                    else if (field.BoundaryArea != null && !string.IsNullOrWhiteSpace(field.BoundaryAreaUnit)) {
                        labelname = DisplaySelectedAreaOnlyDecimals(field.BoundaryArea.Value, field.BoundaryAreaUnit);
                    }
                });
            }
            return labelname;
        }

        private string DisplaySelectedAreaOnlyDecimals(double unitvalue, string unitname) {
            IUnit _unit = UnitFactory.GetUnitByName(unitname);
            string decimalstring = "N" + areadecimals.ToString("N0");
            return unitvalue.ToString(decimalstring);
        }

        //public void ExportSSurgoShapeFile(GeoCollection<Feature> selectedFeatures) {
        //    this.timename = @"_" + DateTime.Now.Ticks.ToString();
        //    var fulldirectoryname = Path.Combine(ApplicationEnvironment.ExportDirectory, documentname);
        //    if (!Directory.Exists(ApplicationEnvironment.ExportDirectory)) {
        //        Directory.CreateDirectory(ApplicationEnvironment.ExportDirectory);
        //    }
        //    if (!Directory.Exists(fulldirectoryname)) {
        //        Directory.CreateDirectory(fulldirectoryname);
        //    }

        //    List<DbfColumn> columns = new List<DbfColumn>();
        //    columns.Add(new DbfColumn(@"areasymbol", DbfColumnType.Character, 64, 0));
        //    columns.Add(new DbfColumn(@"spatialversion", DbfColumnType.Character, 64, 0));
        //    columns.Add(new DbfColumn(@"musym", DbfColumnType.Character, 64, 0));
        //    columns.Add(new DbfColumn(@"nationalmusym", DbfColumnType.Character, 64, 0));
        //    columns.Add(new DbfColumn(@"mukey", DbfColumnType.Character, 64, 0));
        //    columns.Add(new DbfColumn(@"muname", DbfColumnType.Character, 64, 0));
        //    columns.Add(new DbfColumn(@"mupolygonkey", DbfColumnType.Character, 64, 0));
        //    columns.Add(new DbfColumn(@"muareaacres", DbfColumnType.Character, 64, 0));
        //    columns.Add(new DbfColumn(@"description", DbfColumnType.Character, 64, 0));
        //    columns.Add(new DbfColumn(@"descriptionarea", DbfColumnType.Character, 64, 0));
        //    columns.Add(new DbfColumn(@"treeID", DbfColumnType.Character, 64, 0));

        //    //string filename = AddLayersBySaveFileDialog(fulldirectoryname, @"Export Shapes File", @"shp", @"shp files (*.shp)|*.shp");
        //    string filenamex = selectionname + timename;
        //    string filename = filenamex.Replace(" ", "_");
        //    ShapeFileFeatureLayer shapeFileFeatureLayer1 = new ShapeFileFeatureLayer();
        //    var fullfilename = Path.Combine(fulldirectoryname, filename + @".shp");
        //    shapeFileFeatureLayer1.FeatureSource.Projection = projection;
        //    shapeFileFeatureLayer1 = CreateNewFeaturesShapeFile(fullfilename, columns, ShapeFileType.Polygon);
        //    shapeFileFeatureLayer1.Open();
        //    shapeFileFeatureLayer1.EditTools.BeginTransaction();

        //    foreach (Feature feature in selectedFeatures) {
        //        IDictionary<string, string> columnvalues = new Dictionary<string, string>();
        //        columnvalues.Add(@"areasymbol", feature.ColumnValues[@"areasymbol"]);
        //        columnvalues.Add(@"spatialversion", feature.ColumnValues[@"spatialversion"]);
        //        columnvalues.Add(@"musym", feature.ColumnValues[@"musym"]);
        //        columnvalues.Add(@"nationalmusym", feature.ColumnValues[@"nationalmusym"]);
        //        columnvalues.Add(@"mukey", feature.ColumnValues[@"mukey"]);
        //        columnvalues.Add(@"muname", feature.ColumnValues[@"muname"]);
        //        columnvalues.Add(@"mupolygonkey", feature.ColumnValues[@"mupolygonkey"]);
        //        columnvalues.Add(@"muareaacres", feature.ColumnValues[@"muareaacres"]);
        //        columnvalues.Add(@"description", feature.ColumnValues[@"description"]);
        //        columnvalues.Add(@"descriptionarea", feature.ColumnValues[@"descriptionarea"]);
        //        columnvalues.Add(@"treeID", feature.ColumnValues[@"treeID"]);
        //        var newfeature = new Feature(feature.GetShape(), columnvalues);
        //        shapeFileFeatureLayer1.FeatureSource.AddFeature(newfeature);
        //    }
        //    shapeFileFeatureLayer1.EditTools.CommitTransaction();
        //    shapeFileFeatureLayer1.Close();
        //}

        public void ExportShapeSSurgoFile(InMemoryFeatureLayer currentLayer) {
            this.timename = @"_" + DateTime.Now.Ticks.ToString();
            var fulldirectoryname = Path.Combine(ApplicationEnvironment.ExportDirectory, documentname);
            if (!Directory.Exists(ApplicationEnvironment.ExportDirectory)) {
                Directory.CreateDirectory(ApplicationEnvironment.ExportDirectory);
            }
            if (!Directory.Exists(fulldirectoryname)) {
                Directory.CreateDirectory(fulldirectoryname);
            }

            string filenamex = selectionname + timename;
            //string filenamex = selectionname;
            string filename = filenamex.Replace(" ", "_");
            var fullfilename = Path.Combine(fulldirectoryname, filename + @".shp");

            string fullfilenamessurgo = GetNewShapeFileName(fulldirectoryname);
            if (string.IsNullOrEmpty(fullfilenamessurgo)) { return; }

            Collection<FeatureSourceColumn> layercolumns = currentLayer.GetColumns();
            List<DbfColumn> columns = new List<DbfColumn>();
            foreach (FeatureSourceColumn fsc in layercolumns) {
                DbfColumnType coltype = DbfColumnType.Character;
                if (fsc.TypeName == "Interger") { coltype = DbfColumnType.IntegerInBinary; }
                if (fsc.TypeName == "Double") { coltype = DbfColumnType.DoubleInBinary; }
                if (fsc.TypeName == "Date") { coltype = DbfColumnType.Date; }
                if (fsc.TypeName == "Logical") { coltype = DbfColumnType.Logical; }
                if (fsc.TypeName == "Memo") { coltype = DbfColumnType.Memo; }
                columns.Add(new DbfColumn(fsc.ColumnName, coltype, fsc.MaxLength, 0));
            }

            ShapeFileFeatureSource.CreateShapeFile(ShapeFileType.Polygon, fullfilenamessurgo, columns, Encoding.Default, OverwriteMode.Overwrite);
            ShapeFileFeatureLayer shapeFileFeatureLayer1 = new ShapeFileFeatureLayer(fullfilenamessurgo, ShapeFileReadWriteMode.ReadWrite);

            shapeFileFeatureLayer1.Open();
            int nrecords = 0;
            shapeFileFeatureLayer1.EditTools.BeginTransaction();

            GeoCollection<Feature> exportfeatures = new GeoCollection<Feature>();
            exportfeatures = currentLayer.InternalFeatures;

            foreach (Feature feature in exportfeatures) {
                nrecords++;
                BaseShape baseshape = feature.GetShape();
                Feature newfeat = new Feature(baseshape, feature.ColumnValues);
                shapeFileFeatureLayer1.FeatureSource.AddFeature(newfeat);
            }
            shapeFileFeatureLayer1.EditTools.CommitTransaction();
            shapeFileFeatureLayer1.Close();
        }

        public void ExportSSurgoXml(InMemoryFeatureLayer currentLayer) {
            this.timename = @"_" + DateTime.Now.Ticks.ToString();
            var fulldirectoryname = Path.Combine(ApplicationEnvironment.LayerCacheDirectory, documentname);
            if (!Directory.Exists(ApplicationEnvironment.LayerCacheDirectory)) {
                Directory.CreateDirectory(ApplicationEnvironment.LayerCacheDirectory);
            }
            if (!Directory.Exists(fulldirectoryname)) {
                Directory.CreateDirectory(fulldirectoryname);
            }

            string filenamex = selectionname + timename;
            //string filenamex = selectionname;
            string filename = filenamex.Replace(" ", "_");
            var fullfilename = Path.Combine(fulldirectoryname, filename + @".xml");
            if (string.IsNullOrEmpty(filename)) { return; }
            Collection<FeatureSourceColumn> layercolumns = currentLayer.GetColumns();
            GeoCollection<Feature> exportfeatures = currentLayer.InternalFeatures;
            InMemoryFeatureLayer shapeFileFeatureLayer1 = new InMemoryFeatureLayer();
            shapeFileFeatureLayer1.Open();
            foreach (FeatureSourceColumn fsc in layercolumns) {
                shapeFileFeatureLayer1.Columns.Add(fsc);
            }
            foreach (Feature feat in exportfeatures) {
                Feature newfeature = new Feature(feat.GetShape().GetWellKnownText(), feat.ColumnValues[@"RecID"], feat.ColumnValues);
                shapeFileFeatureLayer1.InternalFeatures.Add(newfeature);
            }

            //InMemoryFeatureLayer shapeFileFeatureLayer1 = new InMemoryFeatureLayer(layercolumns, exportfeatures);
            shapeFileFeatureLayer1.FeatureSource.Projection = currentLayer.FeatureSource.Projection;
            shapeFileFeatureLayer1.Close();
            GeoSerializer serializer = new GeoSerializer();
            serializer.Serialize(shapeFileFeatureLayer1, fullfilename);
        }

        public void SaveOverlayState(Overlay stateoverlay) {
            this.timename = @"_" + DateTime.Now.Ticks.ToString();
            var fulldirectoryname = Path.Combine(ApplicationEnvironment.LayerCacheDirectory, documentname);
            if (!Directory.Exists(ApplicationEnvironment.LayerCacheDirectory)) {
                Directory.CreateDirectory(ApplicationEnvironment.LayerCacheDirectory);
            }
            if (!Directory.Exists(fulldirectoryname)) {
                Directory.CreateDirectory(fulldirectoryname);
            }
            string filename = selectionname + timename;
            ShapeFileFeatureLayer shapeFileFeatureLayer1 = new ShapeFileFeatureLayer();
            var fullfilename = Path.Combine(fulldirectoryname, filename + @".dat");

            Byte[] savedStates = stateoverlay.SaveState();
            ByteArrayToFile(fullfilename, savedStates);
        }

        public void ByteArrayToFile(string fileName, byte[] byteArray) {
            FileStream fileStream = new FileStream(fileName, FileMode.Create, FileAccess.Write);
            fileStream.Write(byteArray, 0, byteArray.Length);
            fileStream.Close();
        }

        public string GetNewShapeFileName(string initialFileDirectory) {
            bool nameisgood = false;
            string filedialogfilename = string.Empty;
            string filedialogfilenameextension = "shp";
            string initialdirectory = string.Empty;
            SaveFileDialog filedialog = new SaveFileDialog();
            filedialog.AddExtension = true;
            filedialog.CheckFileExists = false;
            filedialog.OverwritePrompt = false;
            if (!string.IsNullOrEmpty(initialFileDirectory)) {
                if (!Directory.Exists(initialFileDirectory)) {
                    Directory.CreateDirectory(initialFileDirectory);
                }
                filedialog.InitialDirectory = initialFileDirectory;
                initialdirectory = initialFileDirectory;
            }
            filedialog.Filter = "Shape files (*.shp)|*.shp";
            while (!nameisgood) {
                if (filedialog.ShowDialog().Value) {
                    filedialogfilename = Path.GetFileNameWithoutExtension(filedialog.FileName);
                    initialdirectory = Path.GetDirectoryName(filedialog.FileName);
                    nameisgood = true;
                    foreach (Char invalidChar in invalidChars.ToCharArray()) {
                        if (filedialogfilename.Contains(invalidChar.ToString())) {
                            nameisgood = false;
                        }
                    }
                    //if (!string.IsNullOrEmpty(initialFileDirectory) && initialdirectory != initialFileDirectory) {
                    //    string newfilename = string.Format("{0}.{1}", filedialogfilename, filedialogfilenameextension);
                    //    string filenameandpath = Path.Combine(initialFileDirectory, newfilename);
                    //    filedialog.FileName = filenameandpath;
                    //    nameisgood = true;
                    //}
                    if (System.IO.File.Exists(filedialog.FileName)) {
                        nameisgood = false;
                    }

                    if (nameisgood) {
                        return filedialog.FileName;
                    }
                    else {
                        if (System.IO.File.Exists(filedialog.FileName)) {
                            System.Windows.Forms.MessageBox.Show(Strings.FileNameAlreadyExists_Text, Strings.Error_Text);
                            filedialog.FileName = string.Empty;
                            filedialog.InitialDirectory = initialdirectory;
                        }
                        else if (!string.IsNullOrEmpty(initialFileDirectory)) {
                            if (!Directory.Exists(initialFileDirectory)) {
                                Directory.CreateDirectory(initialFileDirectory);
                            }
                        }
                        else {
                            System.Windows.Forms.MessageBox.Show(Strings.InvalidFileNameCharacters_Text, Strings.Error_Text);
                            filedialog.FileName = string.Empty;
                            filedialog.InitialDirectory = initialdirectory;
                        }
                    }
                }
                else {
                    nameisgood = true;
                }
            }
            return string.Empty;
        }

    }
}

