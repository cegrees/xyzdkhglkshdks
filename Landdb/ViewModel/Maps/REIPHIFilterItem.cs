﻿using Landdb.Resources;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Map;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using NLog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Windows.Threading;
using AgC.UnitConversion;
using Landdb.Client.Infrastructure.DisplayItems;
using Landdb.MapStyles;
using ThinkGeo.MapSuite.Core;

namespace Landdb.ViewModel.Maps {
    public class REIPHIFilterItem : ViewModelBase, IMapFilter {
        MapsPageViewModel mapspage;
        readonly Logger log = LogManager.GetCurrentClassLogger();
        Guid currentDataSourceId = Guid.Empty;
        int currentCropYear = 0;
        string name = string.Empty;
        bool showthetoggle = true;
        ColorItem selectedcoloritem = null;
        ObservableCollection<ColorItem> coloritems = new ObservableCollection<ColorItem>();
        ObservableCollection<ColorItem> cropzonecoloritems = new ObservableCollection<ColorItem>();
        Dictionary<string, string> cropzonecrops = new Dictionary<string, string>();
        Dictionary<string, Color> cropzonecolors = new Dictionary<string, Color>();
        Dictionary<string, ColorItem> coloritemsindex = new Dictionary<string, ColorItem>();
        Dictionary<Guid, SummaryModel> ActiveIngredientsList = new Dictionary<Guid, SummaryModel>();
        List<SummaryModel> cropzonesAnalysis = new List<SummaryModel>();
        Dictionary<string, int> cropzonepercentages = new Dictionary<string, int>();
        Dictionary<string, string> cropzonelabels = new Dictionary<string, string>();
        Dictionary<string, string> CCp = new Dictionary<string, string>();
        ObservableCollection<string> croplist = new ObservableCollection<string>();
        string selectedcrop = "All Crops";
        IClientEndpoint clientEndpoint;
        MapSettings mapsettings;
        LegendAdornmentLayer legendLayer = new LegendAdornmentLayer();

        public REIPHIFilterItem(MapsPageViewModel mapspage, IClientEndpoint clientEndpoint) {
            this.mapspage = mapspage;
            this.clientEndpoint = clientEndpoint;
            this.currentDataSourceId = ApplicationEnvironment.CurrentDataSourceId;
            this.currentCropYear = ApplicationEnvironment.CurrentCropYear;
            InitializeFilter();
            InitializeLegend();
            UpdateSelectedIngredientCommand = new RelayCommand<ColorItem>(ToggleThisItem);
        }

        public ICommand UpdateSelectedIngredientCommand { get; private set; }

        public string Name => Strings.FilterType_ReiPhi;

        public FilterType Filter => FilterType.ReiPhi;

        public ObservableCollection<ColorItem> Legend { get; private set; }

        public ObservableCollection<ColorItem> ColorItems {
            get { return coloritems; }
            set {
                if (coloritems == value) { return; }
                coloritems = value;
                RaisePropertyChanged("ColorItem");
                RaisePropertyChanged("ColorItems");
                RaisePropertyChanged("CropzoneColors");
            }
        }

        public ColorItem SelectedColorItem {
            get { return selectedcoloritem; }
            set {
                if (selectedcoloritem == value) { return; }
                selectedcoloritem = value;
                OnColorUpdated();
                RaisePropertyChanged("SelectedColorItem");
                RaisePropertyChanged("ColorItems");
                RaisePropertyChanged("CropzoneColors");
            }
        }

        public ObservableCollection<string> Croplist {
            get { return croplist; }
            set {
                if (croplist == value) { return; }
                croplist = value;
                RaisePropertyChanged("Croplist");
            }
        }

        public string SelectedCrop {
            get { return selectedcrop; }
            set {
                if (selectedcrop == value) { return; }
                selectedcrop = value;
                OnColorUpdated();
                RaisePropertyChanged("SelectedCrop");
            }
        }

        public Dictionary<string, Color> CropzoneColors {
            get { return cropzonecolors; }
            set {
                if (cropzonecolors == value) { return; }
                cropzonecolors = value;
                RaisePropertyChanged("CropzoneColors");
            }
        }

        public List<SummaryModel> CropzonesAnalysis {
            get { return cropzonesAnalysis; }
            set {
                cropzonesAnalysis = value;
                RaisePropertyChanged("CropzonesAnalysis");
                RaisePropertyChanged("ColorItem");
                RaisePropertyChanged("ColorItems");
                RaisePropertyChanged("CropzoneColors");
            }
        }

        public Dictionary<string, int> CropzonePercentages {
            get { return cropzonepercentages; }
            set {
                cropzonepercentages = value;
                RaisePropertyChanged("CropzonePercentages");
            }
        }
        public IList<Color> UsedColors {
            get { return new List<Color>(); ; }
        }

        public IList<string> HatchTypeList {
            get { return HatchStyleFactory.GetHatchStyleList(); }
        }

        public REIPHISummary ApplicationSummary { get; private set; }

        public Dictionary<string, Color> Execute(IClientEndpoint clientEndpoint, Random rand) {
            ApplicationSummary = new REIPHISummary(clientEndpoint);
            InitializeFilter();
            selectedcrop = "All Crops";
            croplist.Add("All Crops");
            mapsettings = clientEndpoint.GetMapSettings();

            var years = clientEndpoint.GetView<FieldTreeView>(new DataSourceId(ApplicationEnvironment.CurrentDataSourceId));
            var yearItem = (years.HasValue && years.Value.CropYearList.ContainsKey(this.currentCropYear)) ? years.Value.CropYearList[this.currentCropYear] : new TreeViewYearItem();
            IList<TreeViewFarmItem> farms = yearItem.Farms != null ? yearItem.Farms : new List<TreeViewFarmItem>();

            var q = from f in farms
                    from fi in f.Fields
                    from cz in fi.CropZones
                    let itemMap = clientEndpoint.GetView<ItemMap>(cz.CropZoneId)
                    where itemMap.HasValue && itemMap.Value.MostRecentMapItem != null
                    select new { f.FarmId, fi.FieldId, cz.CropZoneId, cz.Name, cz, itemMap.Value.MostRecentMapItem.MapData, itemMap.Value.MostRecentMapItem.DataType };

            foreach (var item in q) {
                Guid czidguid = item.CropZoneId.Id;
                string czid = czidguid.ToString();

                string cropname = clientEndpoint.GetMasterlistService().GetCropDisplay(item.cz.CropId);
                CCp.Add(czid, cropname);
                if (!croplist.Contains(cropname)) {
                    croplist.Add(cropname);
                }
            }

            List<TreeViewCropZoneItem> zones = new List<TreeViewCropZoneItem>();
            foreach (var item in q) {
                zones.Add(item.cz);
            }
            CropzonesAnalysis = ApplicationSummary.RefreshSummaryItems(zones);
            //OnIngredientListUpdated();
            OnColorUpdated();
            return new Dictionary<string, Color>();
        }

        //public void OnIngredientListUpdated() {
        //    cropzonecrops.Clear();
        //    cropzonecrops.Add("All Ingredients", "All Ingredients");
        //    foreach (SummaryModel am in cropzonesAnalysis) {
        //        //foreach (ActiveIngredientLineItemViewModel ai in am.ActiveIngredients) {
        //        //    if (!cropzonecrops.ContainsKey(ai.ActiveIngredientId.Id.ToString())) {
        //        //        cropzonecrops.Add(ai.ActiveIngredientId.Id.ToString(), ai.Name);
        //        //    }
        //        //}
        //    }
        //    //selectedcoloritem = new ColorItem(this, "All Ingredients", "All Ingredients", "", Color.Black);
        //    coloritems.Clear();
        //    foreach (KeyValuePair<string, string> item in cropzonecrops) {
        //        coloritems.Add(new ColorItem(this, item.Key, item.Value, "", Color.Black));
        //    }
        //    RaisePropertyChanged("ColorItem");
        //    RaisePropertyChanged("ColorItems");
        //    RaisePropertyChanged("CropzoneColors");
        //}

        public void OnColorUpdated() {
            coloritemsindex.Clear();
            cropzonecolors.Clear();
            cropzonepercentages.Clear();
            cropzonecoloritems = new ObservableCollection<ColorItem>();
            cropzonelabels = new Dictionary<string, string>();
            foreach (SummaryModel am in cropzonesAnalysis) {
                string czid = am.Id.ToString();
                ColorItem aicoloritem = ChooseColor(am, czid);
                cropzonecoloritems.Add(aicoloritem);
                cropzonecolors.Add(czid, aicoloritem.MapColor);
            }
            mapspage.IsLegendVisible = false;
            mapspage.CropzoneLabels = cropzonelabels;
            mapspage.ColorItemList = new ObservableCollection<ColorItem>();
            mapspage.CropzoneColors = cropzonecolors;
        }

        private void InitializeFilter() {
            this.currentDataSourceId = ApplicationEnvironment.CurrentDataSourceId;
            this.currentCropYear = ApplicationEnvironment.CurrentCropYear;
            coloritems = new ObservableCollection<ColorItem>();
            cropzonecrops = new Dictionary<string, string>();
            cropzonecolors = new Dictionary<string, Color>();
            coloritemsindex = new Dictionary<string, ColorItem>();
            cropzonesAnalysis = new List<SummaryModel>();
            cropzonelabels = new Dictionary<string, string>();
            croplist = new ObservableCollection<string>();
            CCp = new Dictionary<string, string>();
            mapspage.IsLegendVisible = false;
        }

        void ToggleThisItem(ColorItem itemToSelect) {
            if (itemToSelect == null) { return; }
            SelectedColorItem = ColorItems.Where(x => x.Key == itemToSelect.Key).SingleOrDefault();
        }

        void ChangeTheColor() {
            showthetoggle = !showthetoggle;
            for (int i = 0; i < ColorItems.Count; i++) {
                ColorItems[i].ColorPickerColor = System.Windows.Media.Color.FromArgb(255, ColorItems[i].MapColor.R, ColorItems[i].MapColor.G, ColorItems[i].MapColor.B);
                ColorItems[i].ShowToggleButton = showthetoggle;
            }
            if (showthetoggle) {
                OnColorUpdated();
            }
            RaisePropertyChanged("ColorItems");
            RaisePropertyChanged("CropzoneColors");
        }

        private ColorItem ChooseColor(SummaryModel ai, string czid) {
            return new ColorItem(this, czid, ai.CropZoneIdString, ai.CropZoneIdString, ChooseColor(ai.Status));
        }

        private Color ChooseColor(double status) {
            if (status == 2) {
                return Color.FromArgb(255, 255, 0, 0);
            }
            else if (status == 1) {
                return Color.FromArgb(255, 255, 255, 0);
            }
            return Color.FromArgb(255, 0, 116, 0);
        }

        private void InitializeLegend() {
            ObservableCollection<ColorItem> legend = new ObservableCollection<ColorItem>();
            legend.Add(new ColorItem(null, "10", @"< 10%", "", Color.FromArgb(255, 255, 0, 0)));
            legend.Add(new ColorItem(null, "10", @"40 - 50%", "", Color.FromArgb(255, 255, 255, 0)));
            legend.Add(new ColorItem(null, "10", @"> 90%", "", Color.FromArgb(255, 0, 116, 0)));
            Legend = legend;
            RaisePropertyChanged("Legend");
        }

        private Measure GetCropZoneArea(Guid Id) {
            string stringid = Id.ToString();
            Measure resultarea = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(0);
            var fieldMaybe = clientEndpoint.GetView<CropZoneDetailsView>(new CropZoneId(ApplicationEnvironment.CurrentDataSourceId, Id));
            fieldMaybe.IfValue(field => {
                if (field.ReportedArea != null && !string.IsNullOrWhiteSpace(field.ReportedAreaUnit)) {
                    resultarea = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(field.ReportedArea.Value);
                }
                else if (field.BoundaryArea != null && !string.IsNullOrWhiteSpace(field.BoundaryAreaUnit)) {
                    resultarea = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(field.BoundaryArea.Value);
                }
                else {
                    resultarea = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(0);
                }
            });
            return resultarea;
        }

        public LegendAdornmentLayer BuildLegend(InMemoryFeatureLayer fieldsLayer, CropMapsFarmDisplayItem selectedFarm, bool isLegendVisible, Dictionary<string, string> unselectedFeatures) {
            legendLayer = new LegendAdornmentLayer();
            //float legendheight = 0;
            //legendLayer.BackgroundMask = AreaStyles.CreateLinearGradientStyle(new GeoColor(255, 255, 255, 255), new GeoColor(255, 230, 230, 230), 90, GeoColor.SimpleColors.Black);
            //CustomLegendItem title = new CustomLegendItem();
            //title.TextStyle = new TextStyle(Strings.MapLegend_Text, new GeoFont("Arial", 10, DrawingFontStyles.Bold), new GeoSolidBrush(GeoColor.SimpleColors.Black));
            //title.Height = 30;
            //legendheight += title.Height;
            //title.Width = 250;
            //DetermineLegendWitdh(legendLayer, title);
            //legendLayer.Title = title;
            //for (int i = 0; i < coloritems.Count; i++) {
            //    ColorItem ci = coloritems[i];
            //    coloritems[i].Area = 0;
            //}
            //foreach (Feature feeture in fieldsLayer.InternalFeatures) {
            //    string featureid = feeture.Id.ToString();
            //    string farmName = feeture.ColumnValues[@"Farm"];
            //    if (cropzonecolors.ContainsKey(featureid)) {
            //        if (selectedFarm == @"All Farms" || selectedFarm == string.Empty || selectedFarm == farmName) {
            //            for (int i = 0; i < coloritems.Count; i++) {
            //                ColorItem ci = coloritems[i];
            //                if (ci.MapColor.R == cropzonecolors[featureid].R && ci.MapColor.G == cropzonecolors[featureid].G && ci.MapColor.B == cropzonecolors[featureid].B) {
            //                    Measure czarea = GetCropZoneArea(new Guid(featureid));
            //                    coloritems[i].Area += czarea.Value;
            //                }
            //            }
            //        }
            //    }
            //}
            //foreach (var ci in coloritems) {
            //    if (ci.Visible) {
            //        Measure resultarea = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(ci.Area);
            //        CustomLegendItem legendItem4 = new CustomLegendItem();
            //        legendItem4.ImageStyle = AreaStyles.CreateSimpleAreaStyle(GeoColor.FromArgb(ci.MapColor.A, ci.MapColor.R, ci.MapColor.G, ci.MapColor.B));
            //        string featdesc3 = string.Empty;
            //        string string1 = string.Format("{0}:", ci.CropName);
            //        string string2 = string.Format("{0}", resultarea.AbbreviatedDisplay);
            //        featdesc3 = string.Format("{0}{1}", string1.PadRight(30), string2.PadRight(10));
            //        legendItem4.TextStyle = new TextStyle(featdesc3, new GeoFont("Consolas", 8), new GeoSolidBrush(GeoColor.SimpleColors.Black));
            //        legendItem4.Height = 25;
            //        legendheight += legendItem4.Height;
            //        legendItem4.Width = 325;
            //        DetermineLegendWitdh(legendLayer, legendItem4);
            //        legendLayer.LegendItems.Add(ci.Key, legendItem4);
            //    }
            //}
            //legendheight += 30;
            //if (legendheight < 200) {
            //    legendheight = 200;
            //}
            //legendLayer.Height = legendheight;
            ////legendLayer.Height = 325;
            //legendLayer.Width = 325;
            //legendLayer.ContentResizeMode = LegendContentResizeMode.Fixed;
            //legendLayer.Location = AdornmentLocation.UpperLeft;
            //legendLayer.YOffsetInPixel = 50;
            if (!isLegendVisible) { return null; }
            return legendLayer;
        }

        private void DetermineLegendWitdh(LegendAdornmentLayer legendLayer, CustomLegendItem legendItem) {
            SetLegendItemWidth(legendItem);

            if (legendItem.Width > legendLayer.Width) {
                legendLayer.Width = legendItem.Width;
            }
        }

        private static void SetLegendItemWidth(CustomLegendItem legendItem) {
            var fudgeFactor = 10;
            var lineWidth = (int)new GdiPlusGeoCanvas().MeasureText(legendItem.TextStyle.TextColumnName, legendItem.TextStyle.Font).Width;
            var totalImageLength = legendItem.ImageWidth + legendItem.ImageLeftPadding + legendItem.ImageRightPadding;
            var totalTextLength = legendItem.TextLeftPadding + legendItem.TextRightPadding + lineWidth;
            legendItem.Width = totalImageLength + totalTextLength + fudgeFactor;
        }

    }
}
