﻿using Landdb.Resources;
using AgC.UnitConversion;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using NLog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Windows.Threading;
using ThinkGeo.MapSuite.Core;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Map;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.MapStyles;
using System.Windows.Data;
using Landdb.Client.Infrastructure.DisplayItems;

namespace Landdb.ViewModel.Maps {
    public class CropFieldFsaFilterItem : ViewModelBase, IMapFilter {
        MapsPageViewModel mapspage;
        readonly Logger log = LogManager.GetCurrentClassLogger();
        Guid currentDataSourceId = Guid.Empty;
        int currentCropYear = 0;
        string name = string.Empty;
        bool showthetoggle = true;
        ColorItem selectedcoloritem = null;
        int colormultiplier = 50;
        int randomchoice = 5;
        IList<Color> usedcolors = new List<Color>();
        ObservableCollection<ColorItem> coloritems = new ObservableCollection<ColorItem>();
        Dictionary<string, string> cropzonefarm = new Dictionary<string, string>();
        Dictionary<string, string> cropzonecrops = new Dictionary<string, string>();
        Dictionary<string, Color> cropzonecolors = new Dictionary<string, Color>();
        Dictionary<string, ColorItem> coloritemsindex = new Dictionary<string, ColorItem>();
        Dictionary<string, ColorItem> cropzonecolordict = new Dictionary<string, ColorItem>();
        Dictionary<string, string> cropzonelabels = new Dictionary<string, string>();
        IClientEndpoint clientEndpoint;
        MapSettings mapsettings;
        LegendAdornmentLayer legendLayer = new LegendAdornmentLayer();
        int limitlegendlabellength = 0;

        public CropFieldFsaFilterItem(MapsPageViewModel mapspage, IClientEndpoint clientEndpoint) {
            this.mapspage = mapspage;
            this.clientEndpoint = clientEndpoint;
            InitializeFilter();
            ToggleCropColorCommand = new RelayCommand<ColorItem>(ToggleThisItem);
            ConfigureCropColorCommand = new RelayCommand(ChangeTheColor);
            RaisePropertyChanged("HatchTypeList");
        }

        public ICommand ToggleCropColorCommand { get; private set; }
        public ICommand ConfigureCropColorCommand { get; private set; }

        public string Name => Strings.FilterType_CropFieldFsa;

        public FilterType Filter => FilterType.CropFieldFSA;

        public ObservableCollection<ColorItem> ColorItems {
            get { return coloritems; }
            set {
                if (coloritems == value) { return; }
                coloritems = value;
                OnColorUpdated();
                RaisePropertyChanged("ColorItem");
                RaisePropertyChanged("ColorItems");
                RaisePropertyChanged("CropzoneColors");
            }
        }

        public ColorItem SelectedColorItem {
            get { return selectedcoloritem; }
            set {
                if (selectedcoloritem == value) { return; }
                selectedcoloritem = value;
                RaisePropertyChanged("SelectedColorItem");
                RaisePropertyChanged("ColorItems");
                RaisePropertyChanged("CropzoneColors");
            }
        }

        public Dictionary<string, Color> CropzoneColors {
            get { return cropzonecolors; }
            set {
                if (cropzonecolors == value) { return; }
                cropzonecolors = value;
                RaisePropertyChanged("CropzoneColors");
            }
        }
        public IList<Color> UsedColors {
            get { return usedcolors; }
        }

        public Dictionary<string, Color> Execute(IClientEndpoint clientEndpoint, Random rand) {
            //if (CropzoneColors.Count > 0) {
            //    //OnColorUpdated();
            //    return CropzoneColors; 
            //}
            if (cropzonecolordict.Count > 0) {
                mapspage.CropzoneColorDict = cropzonecolordict;
                return CropzoneColors;
            }

            InitializeFilter();
            ObservableCollection<ColorItem> CI = new ObservableCollection<ColorItem>();
            mapsettings = clientEndpoint.GetMapSettings();
            var tree = clientEndpoint.GetView<CropTreeView>(new CropYearId(this.currentDataSourceId, this.currentCropYear)).GetValue(new CropTreeView());
            if (tree == null) {
                return new Dictionary<string, Color>();
            }
            if (tree.Crops.Count == 0) {
                return new Dictionary<string, Color>();
            }
            foreach (var crop in tree.Crops) {
                if (crop.CropId == null) {
                    continue;
                }
                if (crop.CropId.Id == Guid.Empty) {
                    continue;
                }
                try {
                    CropId id = crop.CropId;
                    string cropname = clientEndpoint.GetMasterlistService().GetCropDisplay(id);
                    string idcompare = id.Id.ToString();
                    ColorItem colitem = new ColorItem(this, id.Id.ToString(), cropname, cropname, Color.Black);
                    if (mapsettings.CropFilterColors.ContainsKey(idcompare)) {
                        colitem.IsLoading = true;
                        colitem.MapColorInt = mapsettings.CropFilterColors[idcompare].MapColor;
                        colitem.MapColorIntFG = mapsettings.CropFilterColors[idcompare].MapColorFG;
                        colitem.HatchType = mapsettings.CropFilterColors[idcompare].HatchType;
                        colitem.Visible = mapsettings.CropFilterColors[idcompare].Visible;
                        colitem.IsLoading = false;
                        CI.Add(colitem);
                        if (!usedcolors.Contains(colitem.MapColor)) {
                            usedcolors.Add(colitem.MapColor);
                        }
                    }
                    else {
                        Color color = Color.Red;
                        bool colorused = true;
                        while (colorused) {
                            if (usedcolors.Count > 60) {
                                colormultiplier = 25;
                                randomchoice = 10;
                            }
                            color = Color.FromArgb(255, (rand.Next(0, randomchoice) * colormultiplier), (rand.Next(0, randomchoice) * colormultiplier), (rand.Next(0, randomchoice) * colormultiplier));
                            if (!usedcolors.Contains(color)) {
                                usedcolors.Add(color);
                                colorused = false;
                                break;
                            }
                        }
                        CI.Add(new ColorItem(this, id.Id.ToString(), cropname, cropname, color));
                    }
                }
                catch (Exception ex) {
                    int xxxxx = 0;
                }
            }

            CI = new ObservableCollection<ColorItem>(CI.OrderBy(x => x.CropName));

            var q = from c in tree.Crops
                    from f in c.Farms
                    from fi in f.Fields
                    from cz in fi.CropZones
                    let farmName = f.Name
                    let itemMap = clientEndpoint.GetView<ItemMap>(cz.CropZoneId)
                    where itemMap.HasValue && itemMap.Value.MostRecentMapItem != null
                    select new { c.CropId, f.FarmId, farmName, fi.FieldId, cz.CropZoneId, cz.Name, itemMap.Value.MostRecentMapItem.MapData, itemMap.Value.MostRecentMapItem.DataType };

            Dictionary<string, string> CC = new Dictionary<string, string>();
            cropzonefarm = new Dictionary<string, string>();
            foreach (var item in q) {
                if (item.CropId == null) {
                    continue;
                }
                if (item.CropId.Id == Guid.Empty) {
                    continue;
                }
                string czid = item.CropZoneId.Id.ToString();
                string crpid = item.CropId.Id.ToString();
                CC.Add(czid, crpid);
                if (!cropzonefarm.ContainsKey(czid)) {
                    cropzonefarm.Add(czid, item.farmName);
                }
            }

            foreach (var item in q) {
                Guid czidguid = item.CropZoneId.Id;
                string czid = czidguid.ToString();
                var fieldMaybe = clientEndpoint.GetView<FieldDetailsView>(item.FieldId);
                fieldMaybe.IfValue(field => {
                    string fsafarmnumber = field.FsaFarmNumber;
                    if (!string.IsNullOrEmpty(fsafarmnumber)) {
                        string fsatractnumber = field.FsaTractNumber;
                        string fsafieldnumber = field.FsaFieldNumber;
                        double? fsaarea = field.FsaArea;
                        string idcompare = field.FsaFarmNumber;
                        string fsatractnumber1 = string.Empty;
                        string fsafieldnumber1 = string.Empty;
                        if (!string.IsNullOrEmpty(fsatractnumber)) {
                            fsatractnumber1 = string.Format(":{0}", fsatractnumber);
                        }
                        if (!string.IsNullOrEmpty(fsafieldnumber)) {
                            fsafieldnumber1 = string.Format(":{0}", fsafieldnumber);
                        }
                        string name = string.Format("{0}{1}{2}", fsafarmnumber, fsatractnumber1, fsafieldnumber1);
                        if (!cropzonelabels.ContainsKey(czidguid.ToString()) && !string.IsNullOrEmpty(name)) {
                            cropzonelabels.Add(czidguid.ToString(), name);
                        }
                    }
                });
            }

            cropzonecrops = CC;
            ColorItems = CI;
            return CropzoneColors;
        }

        public void OnColorUpdated() {
            coloritemsindex.Clear();
            mapsettings = clientEndpoint.GetMapSettings();
            foreach (var ci in coloritems) {
                if (!coloritemsindex.ContainsKey(ci.Key)) {
                    coloritemsindex.Add(ci.Key, ci);
                }
                MapColorItem mci = new MapColorItem() { Key = ci.Key, Label = ci.Label, CropName = ci.CropName, MapColor = ci.MapColorInt, MapColorFG = ci.MapColorIntFG, HatchType = ci.HatchType, Visible = ci.Visible };
                //MapColorItem mci = new MapColorItem() { Key = ci.Key, Label = ci.Label, CropName = ci.CropName, MapColor = ci.MapColorInt, Visible = ci.Visible };
                if (mapsettings.CropFilterColors.ContainsKey(ci.Key)) {
                    mapsettings.CropFilterColors[ci.Key].MapColor = ci.MapColorInt;
                    mapsettings.CropFilterColors[ci.Key].MapColorFG = ci.MapColorIntFG;
                    mapsettings.CropFilterColors[ci.Key].HatchType = ci.HatchType;
                    mapsettings.CropFilterColors[ci.Key].Visible = ci.Visible;
                }
                else {
                    mapsettings.CropFilterColors.Add(ci.Key, mci);
                }
            }
            cropzonecolors.Clear();
            cropzonecolordict.Clear();
            foreach (var cc in cropzonecrops) {
                if (coloritemsindex[cc.Value].Visible) {
                    cropzonecolors.Add(cc.Key, coloritemsindex[cc.Value].MapColor);
                    cropzonecolordict.Add(cc.Key, coloritemsindex[cc.Value]);
                }
                //if (cropzonecolordict[cc.Key].Visible) {
                //    cropzonecolordict.Add(cc.Key, coloritemsindex[cc.Value]);
                //}
            }
            mapspage.IsLegendVisible = true;
            mapspage.CropzoneLabels = cropzonelabels;
            mapspage.CropzonePercentages = new Dictionary<string, int>();
            //mapspage.SSurgoLayersOverlay = new ThinkGeo.MapSuite.WpfDesktopEdition.LayerOverlay();
            mapspage.SetCropzoneColors(cropzonecolors);
            mapspage.ColorItems = ColorItems;
            mapspage.CropzoneColorDict = cropzonecolordict;
            //mapspage.CropzoneColors = cropzonecolors;
            clientEndpoint.SaveMapSettings(mapsettings);
        }

        private void InitializeFilter() {
            this.currentDataSourceId = ApplicationEnvironment.CurrentDataSourceId;
            this.currentCropYear = ApplicationEnvironment.CurrentCropYear;
            coloritems = new ObservableCollection<ColorItem>();
            cropzonecrops = new Dictionary<string, string>();
            cropzonecolors = new Dictionary<string, Color>();
            coloritemsindex = new Dictionary<string, ColorItem>();
            cropzonecolordict = new Dictionary<string, ColorItem>();
            cropzonelabels = new Dictionary<string, string>();
            usedcolors = new List<Color>();
            colormultiplier = 50;
            randomchoice = 5;
            mapspage.IsLegendVisible = false;
        }

        void ToggleThisItem(ColorItem itemToSelect) {
            if (itemToSelect == null) { return; }
            SelectedColorItem = ColorItems.Where(x => x.Key == itemToSelect.Key).SingleOrDefault();
        }

        void ChangeTheColor() {
            showthetoggle = !showthetoggle;
            for (int i = 0; i < ColorItems.Count; i++) {
                ColorItems[i].ColorPickerColor = System.Windows.Media.Color.FromArgb(255, ColorItems[i].MapColor.R, ColorItems[i].MapColor.G, ColorItems[i].MapColor.B);
                ColorItems[i].ColorPickerColorFG = System.Windows.Media.Color.FromArgb(255, ColorItems[i].MapColorFG.R, ColorItems[i].MapColorFG.G, ColorItems[i].MapColorFG.B);
                ColorItems[i].ShowToggleButton = showthetoggle;
            }
            if (showthetoggle) {
                OnColorUpdated();
            }
            RaisePropertyChanged("ColorItems");
            RaisePropertyChanged("CropzoneColors");
        }

        private Measure GetCropZoneArea(Guid Id) {
            string stringid = Id.ToString();
            Measure resultarea = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(0);
            var fieldMaybe = clientEndpoint.GetView<CropZoneDetailsView>(new CropZoneId(ApplicationEnvironment.CurrentDataSourceId, Id));
            fieldMaybe.IfValue(field => {
                if (field.ReportedArea != null && !string.IsNullOrWhiteSpace(field.ReportedAreaUnit)) {
                    resultarea = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(field.ReportedArea.Value);
                }
                else if (field.BoundaryArea != null && !string.IsNullOrWhiteSpace(field.BoundaryAreaUnit)) {
                    resultarea = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(field.BoundaryArea.Value);
                }
                else {
                    resultarea = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(0);
                }
            });
            return resultarea;
        }

        private string DisplayLegendSelectedAreaDecimals(string label, double unitvalue, string unitname, int labelpad, int areapad) {
            string featdesc3 = string.Empty;
            IUnit _unit = UnitFactory.GetUnitByName(unitname);
            string decimalstring = "N" + mapsettings.AreaDecimals.ToString("N0");
            string string1 = string.Format("{0}:", FormatLegendLabelLength(label));
            string string2 = string.Format("{0} {1} ", unitvalue.ToString(decimalstring), _unit.AbbreviatedDisplay);
            if (unitvalue == 0) {
                string2 = string.Format("0 {0} ", _unit.AbbreviatedDisplay);
            }
            featdesc3 = string.Format("{0}{1}", string1.PadRight(labelpad), string2.PadLeft(areapad));
            return featdesc3;
        }

        private string FormatLegendLabelLength(string labeltext) {
            string newlabel = labeltext;
            if (limitlegendlabellength > 10 && labeltext.Length > limitlegendlabellength) {
                newlabel = labeltext.Substring(0, limitlegendlabellength);
            }
            return newlabel;
        }

        public LegendAdornmentLayer BuildLegend(InMemoryFeatureLayer fieldsLayer, CropMapsFarmDisplayItem selectedFarm, bool isLegendVisible, Dictionary<string, string> unselectedFeatures) {
            legendLayer = new LegendAdornmentLayer();
            float legendheight = 0;
            float baselength = 50;
            float baselegendwidth = 100;
            int padrightname = 30;
            int padrightarea = 10;
            int padrightnamemin = 5;
            int padrightnamecalc = 0;
            float baseheight = 2;
            float baselegendheight = 20;
            string _unitname = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).AbbreviatedDisplay;
            IUnit _unit = UnitFactory.GetUnitByName(_unitname);
            string decimalstring = "N" + mapsettings.AreaDecimals.ToString("N0");

            legendLayer.BackgroundMask = AreaStyles.CreateLinearGradientStyle(new GeoColor(255, 255, 255, 255), new GeoColor(255, 230, 230, 230), 90, GeoColor.SimpleColors.Black);
            CustomLegendItem title = new CustomLegendItem();
            title.TextStyle = new TextStyle(Strings.MapLegend_Text, new GeoFont("Arial", 10, DrawingFontStyles.Bold), new GeoSolidBrush(GeoColor.SimpleColors.Black));
            SizeF TextStylelength = GetStringPixelLength(Strings.MapLegend_Text, "Arial", 10);
            //title.Height = 30;
            title.Height = TextStylelength.Height + 5;
            legendheight += title.Height;
            //title.Width = 200;
            title.Width = TextStylelength.Width;
            DetermineLegendWitdh(legendLayer, title);
            legendLayer.Title = title;
            for (int i = 0; i < coloritems.Count; i++) {
                ColorItem ci = coloritems[i];
                coloritems[i].Area = 0;
                coloritems[i].ShowFilterItem = false;
            }
            bool didnotfindacoloritem = true;
            foreach (Feature feeture in fieldsLayer.InternalFeatures) {
                string featureid = feeture.Id.ToString();
                //if (unselectedFeatures != null && unselectedFeatures.Count > 0 && unselectedFeatures.ContainsKey(featureid)) { continue; }
                if (unselectedFeatures == null || unselectedFeatures.Count == 0 || unselectedFeatures.ContainsKey(featureid))
                {
                    string farmName = feeture.ColumnValues[@"Farm"];
                    if (cropzonecolordict.ContainsKey(featureid))
                    {
                        if (selectedFarm.IsAllFarms || selectedFarm.DisplayText == string.Empty || selectedFarm.DisplayText == farmName)
                        {
                            for (int i = 0; i < coloritems.Count; i++)
                            {
                                ColorItem ci = coloritems[i];
                                if (ci.MapColor.R == cropzonecolordict[featureid].MapColor.R && ci.MapColor.G == cropzonecolordict[featureid].MapColor.G && ci.MapColor.B == cropzonecolordict[featureid].MapColor.B && ci.HatchType == cropzonecolordict[featureid].HatchType && ci.Label == cropzonecolordict[featureid].Label)
                                {
                                    Measure czarea = GetCropZoneArea(new Guid(featureid));
                                    coloritems[i].Area += czarea.Value;
                                    coloritems[i].ShowFilterItem = true;
                                    didnotfindacoloritem = false;
                                    if (coloritems[i].Label.Length > padrightnamecalc)
                                    {
                                        padrightnamecalc = coloritems[i].Label.Length;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (!selectedFarm.IsAllFarms) {
                var czcoloritems3 = from c in coloritems
                                    from cz in cropzonecrops
                                    from cf in cropzonefarm
                                    where (c.Key == cz.Value && cz.Key == cf.Key && cf.Value == selectedFarm.DisplayText)
                                    select new CropZoneColorItem() { CropZoneID = cz.Key, CropZonelLabel = c.CropName, ProductID = cz.Value, ProductName = c.Name, CropName = c.CropName, Area = c.Area, MapColor = c.MapColor, HatchType = c.HatchType, Visible = c.Visible };

                foreach (var item in czcoloritems3) {
                    for (int i = 0; i < coloritems.Count; i++) {
                        ColorItem ci = coloritems[i];
                        try {
                            if (ci.Key == item.ProductID) {
                                coloritems[i].ShowFilterItem = true;
                                didnotfindacoloritem = false;
                            }
                        }
                        catch { }
                    }
                }
            }
            else {
                for (int i = 0; i < coloritems.Count; i++) {
                    coloritems[i].ShowFilterItem = true;
                }
            }

            if (didnotfindacoloritem) {
                for (int i = 0; i < coloritems.Count; i++) {
                    coloritems[i].ShowFilterItem = true;
                }
            }
            padrightnamecalc += padrightnamemin;
            int baselabelwidth = 0;
            int baseareawidth = 0;
            foreach (var ci in coloritems) {
                if (ci.Visible && ci.Area > 0) {
                    string string1 = string.Format("{0}: ", FormatLegendLabelLength(ci.CropName));
                    int featlabellength = string1.Length;
                    if (featlabellength > baselabelwidth) {
                        baselabelwidth = featlabellength;
                    }

                    string string2 = string.Format(" {0} {1} ", ci.Area.ToString(decimalstring), _unit.AbbreviatedDisplay);
                    int featarealength = string2.Length;
                    if (featarealength > baseareawidth) {
                        baseareawidth = featarealength;
                    }
                }
            }
            baselabelwidth += 1;
            baseareawidth += 1;

            foreach (var ci in coloritems) {
                if (ci.Visible && ci.Area > 0) {
                    string featdesc3 = DisplayLegendSelectedAreaDecimals(ci.CropName, ci.Area, _unitname, baselabelwidth, baseareawidth);
                    SizeF featdesclength = GetStringPixelLength(featdesc3, "Consolas", 8);
                    if ((featdesclength.Width + baselength) > baselegendwidth) {
                        baselegendwidth = featdesclength.Width + baselength;
                    }
                    if ((featdesclength.Height + baseheight) > baselegendheight) {
                        baselegendheight = featdesclength.Height + baseheight;
                    }
                }
            }
            foreach (var ci in coloritems) {
                if (ci.Visible && ci.Area > 0) {
                    CustomLegendItem legendItem4 = new CustomLegendItem();
                    if (string.IsNullOrWhiteSpace(ci.HatchType) || ci.HatchType == "None" || ci.HatchType == "Solid") {
                        legendItem4.ImageStyle = AreaStyles.CreateSimpleAreaStyle(GeoColor.FromArgb(ci.MapColor.A, ci.MapColor.R, ci.MapColor.G, ci.MapColor.B));
                    }
                    else {
                        legendItem4.ImageStyle = AreaStyles.CreateHatchStyle(HatchStyleFactory.GetHatchStyle(ci.HatchType), GeoColor.FromArgb(ci.MapColorFG.A, ci.MapColorFG.R, ci.MapColorFG.G, ci.MapColorFG.B), GeoColor.FromArgb(ci.MapColor.A, ci.MapColor.R, ci.MapColor.G, ci.MapColor.B));
                    }
                    string featdesc3 = string.Empty;
                    featdesc3 = DisplayLegendSelectedAreaDecimals(ci.CropName, ci.Area, _unitname, baselabelwidth, baseareawidth);
                    legendItem4.TextStyle = new TextStyle(featdesc3, new GeoFont("Consolas", 8), new GeoSolidBrush(GeoColor.SimpleColors.Black));
                    //legendItem4.Height = 25;
                    legendItem4.Height = baselegendheight;
                    legendheight += legendItem4.Height;
                    //legendItem4.Width = 250;
                    legendLayer.Width = baselegendwidth;
                    DetermineLegendWitdh(legendLayer, legendItem4);
                    legendLayer.LegendItems.Add(ci.Key, legendItem4);
                }
            }
            legendheight += 20;
            if (legendheight < 100) {
                legendheight = 100;
            }
            legendLayer.Height = legendheight;
            //legendLayer.Height = 325;
            //legendLayer.Width = 325;
            legendLayer.Width = baselegendwidth;
            legendLayer.ContentResizeMode = LegendContentResizeMode.Fixed;
            legendLayer.Location = AdornmentLocation.UpperLeft;
            legendLayer.YOffsetInPixel = 50;
            _icv = CollectionViewSource.GetDefaultView(ColorItems);
            _icv.Filter = FarmFilter;
            _icv.Refresh();
            RaisePropertyChanged("ICV");
            if (!isLegendVisible) { return null; }
            return legendLayer;
        }

        private void DetermineLegendWitdh(LegendAdornmentLayer legendLayer, CustomLegendItem legendItem) {
            SetLegendItemWidth(legendItem);

            if (legendItem.Width > legendLayer.Width) {
                legendLayer.Width = legendItem.Width;
            }
        }

        private static void SetLegendItemWidth(CustomLegendItem legendItem) {
            var fudgeFactor = 10;
            var lineWidth = (int)new GdiPlusGeoCanvas().MeasureText(legendItem.TextStyle.TextColumnName, legendItem.TextStyle.Font).Width;
            var totalImageLength = legendItem.ImageWidth + legendItem.ImageLeftPadding + legendItem.ImageRightPadding;
            var totalTextLength = legendItem.TextLeftPadding + legendItem.TextRightPadding + lineWidth;
            legendItem.Width = totalImageLength + totalTextLength + fudgeFactor;
        }

        private static SizeF GetStringPixelLength(string text, string fontname, float fontsize) {
            SizeF size = new SizeF();
            using (System.Drawing.Graphics graphics = System.Drawing.Graphics.FromImage(new Bitmap(1, 1))) {
                size = graphics.MeasureString(text, new Font(fontname, fontsize, FontStyle.Regular, GraphicsUnit.Point));
            }
            return size;
        }

        private ICollectionView _icv;
        public ICollectionView ICV {
            get { return _icv; }
        }

        private bool FarmFilter(object item) {
            ColorItem _item = item as ColorItem;
            return _item.ShowFilterItem;
        }

    }
}
