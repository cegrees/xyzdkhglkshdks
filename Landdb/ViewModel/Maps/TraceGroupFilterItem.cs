﻿using Landdb.Resources;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Map;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.ViewModel.Fields;
using NLog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Windows.Threading;
using AgC.UnitConversion;
using Landdb.Client.Infrastructure.DisplayItems;
using Landdb.MapStyles;
using ThinkGeo.MapSuite.Core;

namespace Landdb.ViewModel.Maps {
    //TODO: For some reason, this file and the SeedVarietyFilterItem.cs file have almost totally identical code (the differences are at the end). It may be expedient to combine them into one file in the future to get rid of the copied code. 
    public class TraceGroupFilterItem : ViewModelBase, IMapFilter  {
        MapsPageViewModel mapspage;
        readonly Logger log = LogManager.GetCurrentClassLogger();
        Guid currentDataSourceId = Guid.Empty;
        int currentCropYear = 0;
        string name = string.Empty;
        bool showthetoggle = true;
        ColorItem selectedcoloritem = null;
        int colormultiplier = 50;
        int randomchoice = 5;
        IList<Color> usedcolors = new List<Color>();
        ObservableCollection<ColorItem> coloritems = new ObservableCollection<ColorItem>();
        Dictionary<string, string> cropzonecrops = new Dictionary<string, string>();
        Dictionary<string, Color> cropzonecolors = new Dictionary<string, Color>();
        Dictionary<string, ColorItem> coloritemsindex = new Dictionary<string, ColorItem>();
        ObservableCollection<string> croplist = new ObservableCollection<string>();
        string selectedcrop = "All Crops";
        IClientEndpoint clientEndpoint;
        MapSettings mapsettings;
        LegendAdornmentLayer legendLayer = new LegendAdornmentLayer();

        public TraceGroupFilterItem(MapsPageViewModel mapspage, IClientEndpoint clientEndpoint) {
            this.mapspage = mapspage;
            this.clientEndpoint = clientEndpoint;
            InitializeFilter();
            ToggleVarietyColorCommand = new RelayCommand<ColorItem>(ToggleThisItem);
            ConfigureVarietyColorCommand = new RelayCommand(ChangeTheColor);
        }

        public ICommand ToggleVarietyColorCommand { get; private set; }
        public ICommand ConfigureVarietyColorCommand { get; private set; }

        public string Name => Strings.FilterType_TraceGroup;

        public FilterType Filter => FilterType.TraceGroup;

        public ObservableCollection<ColorItem> ColorItems {
            get { return coloritems; }
            set {
                if (coloritems == value) { return; }
                coloritems = value;
                OnColorUpdated();
                RaisePropertyChanged("ColorItems");
                RaisePropertyChanged("CropzoneColors");
                RaisePropertyChanged("Croplist");
                RaisePropertyChanged("SelectedCrop");
            }
        }

        public ColorItem SelectedColorItem {
            get { return selectedcoloritem; }
            set {
                if (selectedcoloritem == value) { return; }
                selectedcoloritem = value;
                RaisePropertyChanged("SelectedColorItem");
                RaisePropertyChanged("ColorItems");
                RaisePropertyChanged("CropzoneColors");
            }
        }

        public ObservableCollection<string> Croplist {
            get { return croplist; }
            set {
                if (croplist == value) { return; }
                croplist = value;
                RaisePropertyChanged("Croplist");
            }
        }

        public string SelectedCrop {
            get { return selectedcrop; }
            set {
                if (selectedcrop == value) { return; }
                selectedcrop = value;
                for (int i = 0; i < ColorItems.Count; i++) {
                    if (selectedcrop == "All Crops" || selectedcrop == ColorItems[i].CropName) {
                        ColorItems[i].Visible = true;
                    }
                    else {
                        ColorItems[i].Visible = false;
                    }
                }
                OnColorUpdated();
                RaisePropertyChanged("SelectedCrop");
             }
        }

        public Dictionary<string, Color> CropzoneColors {
            get { return cropzonecolors; }
            set {
                if (cropzonecolors == value) { return; }
                cropzonecolors = value;
                RaisePropertyChanged("CropzoneColors");
            }
        }

        public IList<string> HatchTypeList {
            get { return HatchStyleFactory.GetHatchStyleList(); }
        }
        public IList<Color> UsedColors {
            get { return usedcolors; }
        }

        public Dictionary<string, Color> Execute(IClientEndpoint clientEndpoint, Random rand) {
            if (CropzoneColors.Count > 0) { return CropzoneColors; }
            InitializeFilter();
            mapsettings = clientEndpoint.GetMapSettings();

            selectedcrop = "All Crops";
            croplist.Add("All Crops");
            var tree = clientEndpoint.GetView<CropTreeView>(new CropYearId(this.currentDataSourceId, this.currentCropYear)).GetValue(new CropTreeView());

            var q = from c in tree.Crops
                    from f in c.Farms
                    from fi in f.Fields
                    from cz in fi.CropZones
                    let itemMap = clientEndpoint.GetView<ItemMap>(cz.CropZoneId)
                    where itemMap.HasValue && itemMap.Value.MostRecentMapItem != null
                    select new { c.CropId, f.FarmId, fi.FieldId, cz.CropZoneId, cz.Name, itemMap.Value.MostRecentMapItem.MapData, itemMap.Value.MostRecentMapItem.DataType };

            Dictionary<string, string> CCp = new Dictionary<string, string>();
            foreach (var item in q) {
                Guid czidguid = item.CropZoneId.Id;
                string czid = czidguid.ToString();

                string cropname = clientEndpoint.GetMasterlistService().GetCropDisplay(item.CropId);
                CCp.Add(czid, cropname);
                if (!croplist.Contains(cropname)) {
                    croplist.Add(cropname);
                }
            }
            croplist = new ObservableCollection<string>(croplist.OrderBy(x => x));
            for (int j = 1; j < croplist.Count; j++) {
                if (croplist[j] == "All Crops") {
                    croplist.Move(j, 0);
                    break;
                }
            }
            ObservableCollection<ColorItem> CI = new ObservableCollection<ColorItem>();
            Dictionary<string, string> CC = new Dictionary<string, string>();
            var data = clientEndpoint.GetView<CropZoneApplicationDataView>(new CropYearId(ApplicationEnvironment.CurrentDataSourceId, ApplicationEnvironment.CurrentCropYear)).GetValue(new CropZoneApplicationDataView());
            var ml = clientEndpoint.GetMasterlistService();
            foreach (var item in data.Items) {
                var product = ml.GetProduct(item.ProductId);
                // TODO FIX HACK - handle unknown products - MH
                if (product == null) { continue; }
                if (product.ProductType == GlobalStrings.ProductType_Seed) {
                    ProductId id = item.ProductId;
                    Guid prodid = id.Id;
                    string productname = product.Name;
                    CropZoneId czid = item.CropZoneId;
                    Guid czidguid = czid.Id;
                    string cropname = string.Empty;
                    if (CCp.ContainsKey(czidguid.ToString())) {
                        cropname = CCp[czidguid.ToString()];
                    }
                    //Color color = Color.FromArgb(255, rand.Next(0, 255), rand.Next(0, 255), rand.Next(0, 255));

                    Color color = Color.Red;
                    bool colorused = true;
                    while (colorused) {
                        if (usedcolors.Count > 60) {
                            colormultiplier = 25;
                            randomchoice = 10;
                        }
                        color = Color.FromArgb(255, (rand.Next(0, randomchoice) * colormultiplier), (rand.Next(0, randomchoice) * colormultiplier), (rand.Next(0, randomchoice) * colormultiplier));
                        if (!usedcolors.Contains(color)) {
                            usedcolors.Add(color);
                            colorused = false;
                            break;
                        }
                        if (usedcolors.Count > 500) {
                            colorused = false;
                            break;
                        }
                    }

                    ColorItem coloritem = new ColorItem(this, prodid.ToString(), productname, cropname, color);
                    if (!coloritemsindex.ContainsKey(prodid.ToString())) {
                        CI.Add(coloritem);
                        coloritemsindex.Add(prodid.ToString(), coloritem);
                    }
                    if (!CC.ContainsKey(czidguid.ToString())) {
                        CC.Add(czidguid.ToString(), prodid.ToString());
                        cropzonecolors.Add(czidguid.ToString(), coloritemsindex[prodid.ToString()].MapColor);
                    }
                }
            }
            cropzonecrops = CC;
            ColorItems = new ObservableCollection<ColorItem>(CI.OrderBy(x => x.CropName));
            return CropzoneColors;
        }

        public void OnColorUpdated() {
            coloritemsindex.Clear();
            foreach (var ci in coloritems) {
                coloritemsindex.Add(ci.Key, ci);
            }
            cropzonecolors.Clear();
            foreach (var cc in cropzonecrops) {
                if (selectedcrop == "All Crops") {
                    if (coloritemsindex[cc.Value].Visible) {
                        cropzonecolors.Add(cc.Key, coloritemsindex[cc.Value].MapColor);
                    }
                }
                else {
                    if (selectedcrop == coloritemsindex[cc.Value].CropName) {
                        if (coloritemsindex[cc.Value].Visible) {
                            cropzonecolors.Add(cc.Key, coloritemsindex[cc.Value].MapColor);
                        }
                    }
                }
            }
            mapspage.IsLegendVisible = false;
            mapspage.CropzoneLabels = new Dictionary<string, string>();
            mapspage.CropzonePercentages = new Dictionary<string, int>();
            mapspage.CropzoneColors = cropzonecolors;
        }

        private void InitializeFilter() {
            this.currentDataSourceId = ApplicationEnvironment.CurrentDataSourceId;
            this.currentCropYear = ApplicationEnvironment.CurrentCropYear;
            coloritems = new ObservableCollection<ColorItem>();
            cropzonecrops = new Dictionary<string, string>();
            cropzonecolors = new Dictionary<string, Color>();
            coloritemsindex = new Dictionary<string, ColorItem>();
            croplist = new ObservableCollection<string>();
            usedcolors = new List<Color>();
            colormultiplier = 50;
            randomchoice = 5;
            mapspage.IsLegendVisible = false;
        }

        void ToggleThisItem(ColorItem itemToSelect) {
            if (itemToSelect == null) { return; }
            SelectedColorItem = ColorItems.Where(x => x.Key == itemToSelect.Key).SingleOrDefault();
        }

        void ChangeTheColor() {
            showthetoggle = !showthetoggle;
            for (int i = 0; i < ColorItems.Count; i++) {
                ColorItems[i].ColorPickerColor = System.Windows.Media.Color.FromArgb(255, ColorItems[i].MapColor.R, ColorItems[i].MapColor.G, ColorItems[i].MapColor.B);
                ColorItems[i].ShowToggleButton = showthetoggle;
            }
            if (showthetoggle) {
                OnColorUpdated();
            }
            RaisePropertyChanged("ColorItems");
            RaisePropertyChanged("CropzoneColors");
        }

        private Measure GetCropZoneArea(Guid Id) {
            string stringid = Id.ToString();
            Measure resultarea = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(0);
            var fieldMaybe = clientEndpoint.GetView<CropZoneDetailsView>(new CropZoneId(ApplicationEnvironment.CurrentDataSourceId, Id));
            fieldMaybe.IfValue(field => {
                if (field.ReportedArea != null && !string.IsNullOrWhiteSpace(field.ReportedAreaUnit)) {
                    resultarea = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(field.ReportedArea.Value);
                }
                else if (field.BoundaryArea != null && !string.IsNullOrWhiteSpace(field.BoundaryAreaUnit)) {
                    resultarea = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(field.BoundaryArea.Value);
                }
                else {
                    resultarea = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(0);
                }
            });
            return resultarea;
        }

        public LegendAdornmentLayer BuildLegend(InMemoryFeatureLayer fieldsLayer, CropMapsFarmDisplayItem selectedFarm, bool isLegendVisible, Dictionary<string, string> unselectedFeatures) {
            legendLayer = new LegendAdornmentLayer();
            //float legendheight = 0;
            //legendLayer.BackgroundMask = AreaStyles.CreateLinearGradientStyle(new GeoColor(255, 255, 255, 255), new GeoColor(255, 230, 230, 230), 90, GeoColor.SimpleColors.Black);
            //CustomLegendItem title = new CustomLegendItem();
            //title.TextStyle = new TextStyle(Strings.MapLegend_Text, new GeoFont("Arial", 10, DrawingFontStyles.Bold), new GeoSolidBrush(GeoColor.SimpleColors.Black));
            //title.Height = 30;
            //legendheight += title.Height;
            //title.Width = 250;
            //DetermineLegendWitdh(legendLayer, title);
            //legendLayer.Title = title;
            //for (int i = 0; i < coloritems.Count; i++) {
            //    ColorItem ci = coloritems[i];
            //    coloritems[i].Area = 0;
            //}
            //foreach (Feature feeture in fieldsLayer.InternalFeatures) {
            //    string featureid = feeture.Id.ToString();
            //    string farmName = feeture.ColumnValues[@"Farm"];
            //    if (cropzonecolors.ContainsKey(featureid)) {
            //        if (selectedFarm == @"All Farms" || selectedFarm == string.Empty || selectedFarm == farmName) {
            //            for (int i = 0; i < coloritems.Count; i++) {
            //                ColorItem ci = coloritems[i];
            //                if (ci.MapColor.R == cropzonecolors[featureid].R && ci.MapColor.G == cropzonecolors[featureid].G && ci.MapColor.B == cropzonecolors[featureid].B) {
            //                    Measure czarea = GetCropZoneArea(new Guid(featureid));
            //                    coloritems[i].Area += czarea.Value;
            //                }
            //            }
            //        }
            //    }
            //}
            //foreach (var ci in coloritems) {
            //    if (ci.Visible) {
            //        Measure resultarea = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(ci.Area);
            //        CustomLegendItem legendItem4 = new CustomLegendItem();
            //        legendItem4.ImageStyle = AreaStyles.CreateSimpleAreaStyle(GeoColor.FromArgb(ci.MapColor.A, ci.MapColor.R, ci.MapColor.G, ci.MapColor.B));
            //        string featdesc3 = string.Empty;
            //        string string1 = string.Format("{0}:", ci.CropName);
            //        string string2 = string.Format("{0}", resultarea.AbbreviatedDisplay);
            //        featdesc3 = string.Format("{0}{1}", string1.PadRight(30), string2.PadRight(10));
            //        legendItem4.TextStyle = new TextStyle(featdesc3, new GeoFont("Consolas", 8), new GeoSolidBrush(GeoColor.SimpleColors.Black));
            //        legendItem4.Height = 25;
            //        legendheight += legendItem4.Height;
            //        legendItem4.Width = 325;
            //        DetermineLegendWitdh(legendLayer, legendItem4);
            //        legendLayer.LegendItems.Add(ci.Key, legendItem4);
            //    }
            //}
            //legendheight += 30;
            //if (legendheight < 200) {
            //    legendheight = 200;
            //}
            //legendLayer.Height = legendheight;
            ////legendLayer.Height = 325;
            //legendLayer.Width = 325;
            //legendLayer.ContentResizeMode = LegendContentResizeMode.Fixed;
            //legendLayer.Location = AdornmentLocation.UpperLeft;
            //legendLayer.YOffsetInPixel = 50;
            if (!isLegendVisible) { return null; }
            return legendLayer;
        }

        private void DetermineLegendWitdh(LegendAdornmentLayer legendLayer, CustomLegendItem legendItem) {
            SetLegendItemWidth(legendItem);

            if (legendItem.Width > legendLayer.Width) {
                legendLayer.Width = legendItem.Width;
            }
        }

        private static void SetLegendItemWidth(CustomLegendItem legendItem) {
            var fudgeFactor = 10;
            var lineWidth = (int)new GdiPlusGeoCanvas().MeasureText(legendItem.TextStyle.TextColumnName, legendItem.TextStyle.Font).Width;
            var totalImageLength = legendItem.ImageWidth + legendItem.ImageLeftPadding + legendItem.ImageRightPadding;
            var totalTextLength = legendItem.TextLeftPadding + legendItem.TextRightPadding + lineWidth;
            legendItem.Width = totalImageLength + totalTextLength + fudgeFactor;
        }

    }
}
