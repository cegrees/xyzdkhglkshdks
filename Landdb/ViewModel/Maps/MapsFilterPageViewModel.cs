﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AgC.UnitConversion;
using GalaSoft.MvvmLight;
using Landdb.Domain.ReadModels;
using Landdb.Domain.ReadModels.Inventory;
using Landdb.ViewModel.Secondary.Application;
using Landdb.ValueObjects;
using Landdb.Client.Infrastructure;
using Landdb.ViewModel.Fields.FieldDetails;
using System.Windows.Threading;
using NLog;
using Landdb.Infrastructure;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure.Messages;
using System.Windows.Controls;

namespace Landdb.ViewModel.Maps {
    public class MapsFilterPageViewModel : ViewModelBase {
        readonly MapsPageViewModel parent;
        readonly IClientEndpoint clientEndpoint;
        readonly Dispatcher dispatcher;
        readonly Logger log = LogManager.GetCurrentClassLogger();
        readonly IDetailsViewModelFactory detailsFactory;
        Guid currentDataSourceId;
        bool displaycropzones = false;
        int currentCropYear;
        IMapFilter selectedfilter = null;
        ObservableCollection<IMapFilter> filterelements;
        ObservableCollection<ColorItem> coloritemlist = new ObservableCollection<ColorItem>();
        ColorItem selectedcoloritem;

        public MapsFilterPageViewModel(MapsPageViewModel parent, IClientEndpoint clientEndpoint, IDetailsViewModelFactory detailsFactory, Dispatcher dispatcher) {
            this.parent = parent;
            this.dispatcher = dispatcher;
            this.clientEndpoint = clientEndpoint;
            this.detailsFactory = detailsFactory;
            this.currentDataSourceId = ApplicationEnvironment.CurrentDataSourceId;
            this.currentCropYear = ApplicationEnvironment.CurrentCropYear;
            this.selectedcoloritem = new ColorItem(null, "", "", "", System.Drawing.Color.Transparent);
            GalaSoft.MvvmLight.Messaging.Messenger.Default.Register<DataSourceChangedMessage>(this, OnDataSourceChanged);
            filterelements = new ObservableCollection<IMapFilter>();
            UpdateFilters();
        }

        public ObservableCollection<IMapFilter> FilterElements {
            get { return filterelements; }
            set {
                if (filterelements == value) { return; }
                filterelements = value;
                RaisePropertyChanged("FilterElements");
            }
        }

        public IMapFilter SelectedFilter {
            get { return selectedfilter; }
            set {
                if (selectedfilter == value) { return; }
                selectedfilter = value;
                RaisePropertyChanged("SelectedFilter");
                parent.SelectedFilter = selectedfilter;
            }
        }

        public ColorItem SelectedColorItem {
            get { return selectedcoloritem; }
            set {
                if (selectedcoloritem == value) { return; }
                selectedcoloritem = value;
                RaisePropertyChanged("SelectedColorItem");
            }
        }

        public ObservableCollection<ColorItem> ColorItemList {
            get { return coloritemlist; }
            set {
                if (coloritemlist == value) { return; }
                coloritemlist = value;
                RaisePropertyChanged("ColorItemList");
            }
        }

        void OnDataSourceChanged(DataSourceChangedMessage message) {
            this.currentDataSourceId = message.DataSourceId;
            this.currentCropYear = message.CropYear;
        }

        public string DisplayName {
            get {
                if (SelectedFilter == null) { return string.Empty; }
                return SelectedFilter.Name;
            }
        }

        void UpdateFilters() {
            ObservableCollection<IMapFilter> filterelements2 = new ObservableCollection<IMapFilter>();
            filterelements2.Add(new BlankFilterItem(parent, clientEndpoint));
            FilterElements = filterelements2;
            SelectedFilter = (IMapFilter)filterelements2;
        }
    }
}
