﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThinkGeo.MapSuite.Core;

namespace Landdb.ViewModel.Maps.ImageryOverlay {
    public class GoogleWebLayer : Layer {
        private const double qj4 = 0.010416666666666666;
        private const double qz4 = 85.0;
        private const int rD4 = 256;
        private const double rT4 = 4374754.0;
        private const double rj4 = 0.0254;
        private static readonly RectangleShape lio = new RectangleShape(-20037508.352, 20037508.352, 20037508.352, -20037508.352);
        private static System.Collections.ObjectModel.Collection<ZoomLevel> rz4;
        private int tileHeight = 512;
        private int tileWidth = 512;

        private Projection projectionFromSphericalMercator;
        private ManagedProj4Projection displayWgs84Projection;
        private ManagedProj4Projection googleWgs84Projection;
        private int zoomLevelsNumber;
        private double currentSphereResolution;
        private double currentRequestSphereResolution;

        public GoogleWebLayer() : base() {
        }

        public int ZoomLevelsNumber {
            get {
                return this.zoomLevelsNumber;
            }
            set {
                this.zoomLevelsNumber = value;
            }
        }
        public Projection ProjectionFromSphericalMercator {
            get {
                return this.projectionFromSphericalMercator;
            }
            set {
                this.projectionFromSphericalMercator = value;
                if (this.displayWgs84Projection == null) {
                    this.displayWgs84Projection = new ManagedProj4Projection();
                    this.displayWgs84Projection.ExternalProjectionParametersString = ManagedProj4Projection.GetWgs84ParametersString();
                    this.displayWgs84Projection.InternalProjectionParametersString = ManagedProj4Projection.GetWgs84ParametersString();
                }
                if (this.projectionFromSphericalMercator is Proj4Projection) {
                    this.displayWgs84Projection.InternalProjectionParametersString = ((Proj4Projection)this.projectionFromSphericalMercator).InternalProjectionParametersString;
                }
                else if (this.projectionFromSphericalMercator is ManagedProj4Projection) {
                    this.displayWgs84Projection.InternalProjectionParametersString = ((ManagedProj4Projection)this.projectionFromSphericalMercator).InternalProjectionParametersString;
                }
                else if (this.projectionFromSphericalMercator is UnmanagedProj4Projection) {
                    this.displayWgs84Projection.InternalProjectionParametersString = ((UnmanagedProj4Projection)this.projectionFromSphericalMercator).InternalProjectionParametersString;
                }
                //pj0.JhQ(this.displayWgs84Projection.ExternalProjectionParametersString, "ProjectionToSphericalMercator.ExternalProjectionParametersString");
                //pj0.JhQ(this.displayWgs84Projection.InternalProjectionParametersString, "ProjectionToSphericalMercator.InternalProjectionParametersString");
                this.displayWgs84Projection.Open();
            }
        }

        protected override void DrawCore(GeoCanvas canvas, System.Collections.ObjectModel.Collection<SimpleCandidate> labelsInAllLayers) {
            if (this.projectionFromSphericalMercator == null) {
                string googleMapParametersString = ManagedProj4Projection.GetGoogleMapParametersString();
                string googleMapParametersString2 = ManagedProj4Projection.GetGoogleMapParametersString();
                this.projectionFromSphericalMercator = new ManagedProj4Projection(googleMapParametersString, googleMapParametersString2);
                this.projectionFromSphericalMercator.Open();
            }
            try {
                double currentScale = ExtentHelper.GetScale(canvas.CurrentWorldExtent, canvas.Width, canvas.MapUnit);
                ZoomLevelsNumber = tD4(canvas);
                this.currentSphereResolution = tT4(this.zoomLevelsNumber, canvas.Dpi);
                this.currentRequestSphereResolution = tT4(this.zoomLevelsNumber, 96f);
                PointShape centerPoint = canvas.CurrentWorldExtent.GetCenterPoint();
                PointShape pointShape = (PointShape)this.projectionFromSphericalMercator.ConvertToInternalProjection(centerPoint);
                PointShape centerInWgs = (PointShape)this.googleWgs84Projection.ConvertToExternalProjection(pointShape);
                int num = (int)canvas.Height;
                int num2 = (int)canvas.Width;
                double snappedScale = wT4(this.zoomLevelsNumber);
                RectangleShape rectangleShape = canvas.CurrentWorldExtent;

                //gws = new GoogleWebService(640, 640, GoogleMapsMapType.Hybrid, GoogleMapsPictureFormat.Jpeg, 20, @"AIzaSyAdlNYBc_7sWnX0J-bU9hFik-bYPBuCaW0");
                //System.IO.Stream strem = gws.GetGoogleMapsImage(wgscenterpoint.Y, wgscenterpoint.X, zoomlevelnumber, extent, projextent, epsgcode);


                //Bitmap bm = GetGoogleMapsImage(centerInWgs.X, centerInWgs.Y, this.zoomLevelsNumber, (double)this.tileWidth, (double)this.tileHeight);
                //string bitmapfilename = string.Format("{0}.bmp", DateTime.Now.Ticks.ToString());

                //var fulldirectoryname = Path.Combine(ApplicationEnvironment.GoogleMapCacheDirectory, bitmapfilename);
                //if (!Directory.Exists(ApplicationEnvironment.GoogleMapCacheDirectory)) {
                //    Directory.CreateDirectory(ApplicationEnvironment.GoogleMapCacheDirectory);
                //}
                //bm.Save(fulldirectoryname, ImageFormat.Bmp);
                //GdiPlusGeoCanvas gdiPlusGeoCanvas = canvas as GdiPlusGeoCanvas;
                //if (gdiPlusGeoCanvas != null) {
                //    gdiPlusGeoCanvas.DrawScreenImageWithoutScaling(bm, canvas.Width * 0.5f, canvas.Height * 0.5f, DrawingLevel.LevelOne, 0f, 0f, 0f);
                //    bm.Dispose();
                //}
                //else {
                //    using (GeoImage geoImage2 = new GeoImage(currentstream)) {
                //        canvas.DrawScreenImageWithoutScaling(geoImage2, canvas.Width * 0.5f, canvas.Height * 0.5f, DrawingLevel.LevelOne, 0f, 0f, 0f);
                //    }
                //    currentstream.Dispose();
                //}
                //this.requestOneTile = false;
            }
            catch (Exception ex) {
                int werwerwer = 0;
            }

        }

        private static int tD4(GeoCanvas canvas) {
            int num = Bj8((double)canvas.Width, canvas.CurrentWorldExtent, canvas.MapUnit, canvas.Dpi);
            if (num >= 20) {
                num = 19;
            }
            else if (num == 0) {
                num = 1;
            }
            return num;
        }

        internal static int Bj8(double newWidth, RectangleShape newTileExtent, GeographyUnit mapUnit, float dpi) {
            OpenStreetMapsZoomLevelSet openStreetMapsZoomLevelSet = new OpenStreetMapsZoomLevelSet();
            ZoomLevel zoomLevel = openStreetMapsZoomLevelSet.GetZoomLevel(newTileExtent, newWidth, mapUnit, dpi);
            System.Collections.ObjectModel.Collection<ZoomLevel> zoomLevels = openStreetMapsZoomLevelSet.GetZoomLevels();
            int result = 0;
            for (int i = 0; i < zoomLevels.Count; i++) {
                if (zoomLevel.Scale == zoomLevels[i].Scale) {
                    result = i;
                    break;
                }
            }
            return result;
        }

        private static double tT4(int zoomLevelNumber, float dotPerInch) {
            double num = wT4(zoomLevelNumber);
            return num * 0.0254000508 / (double)dotPerInch;
        }

        private static double wT4(int zoomLevelNumber) {
            return wj4(zoomLevelNumber).Scale;
        }

        private static ZoomLevel wj4(int zoomLevelNumber) {
            return wz4()[zoomLevelNumber];
        }

        private static System.Collections.ObjectModel.Collection<ZoomLevel> wz4() {
            rz4 = (rz4 ?? new SphericalMercatorZoomLevelSet().GetZoomLevels());
            return rz4;
        }

    }
}

