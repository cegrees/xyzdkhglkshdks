﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Maps.ImageryOverlay
{
    public class MapCultureFactory
    {
        // Microsoft Supported Culture Codes
        //https://msdn.microsoft.com/en-us/library/hh441729.aspx
        //https://msdn.microsoft.com/en-us/library/ee825488(v=cs.20).aspx
        //http://azuliadesigns.com/list-net-culture-country-codes/
        // Google Language Codes
        //https://developers.google.com/maps/faq#languagesupport


        public string BingWpfMapControlCultureFactory()
        {
            string mapculture = "en-US";
            switch (Infrastructure.ApplicationEnvironment.CurrentDataSource.DatasourceCulture.ToLower())
            {
                case "ru-ru":
                    mapculture = "ru";
                    break;
                case "uk-ua":
                    mapculture = "ua";
                    break;
                case "pt-br":
                    mapculture = "pt-br";
                    break;
                case "ro-ro":
                    mapculture = "ro";
                    break;
                case "hi-in":
                    mapculture = "hi";
                    break;
                case "de-de":
                case "de-at":
                case "de-li":
                case "de-lu":
                case "de-ch":
                    mapculture = "de";
                    break;
                case "fr-fr":
                case "fr-ca":
                case "fr-lu":
                case "fr-be":
                case "fr-mc":
                case "fr-ch":
                    mapculture = "fr";
                    break;
                case "es-ar":
                case "es-bo":
                case "es-cl":
                case "es-co":
                case "es-cr":
                case "es-do":
                case "es-ec":
                case "es-sv":
                case "es-gt":
                case "es-hn":
                case "es-mx":
                case "es-ni":
                case "es-pa":
                case "es-py":
                case "es-pe":
                case "es-pr":
                case "es-es":
                case "es-uy":
                case "es-ve":
                    mapculture = "es";
                    break;
                default:
                    mapculture = "en-US";
                    break;
            }
            //mapculture = "ru";
            return mapculture;


            //af Afrikaans
            //am Amharic
            //ar - sa   Arabic(Saudi Arabia)
            //  as Assamese
            //az - Latn Azerbaijani(Latin)
            //be Belarusian
            //bg Bulgarian
            //bn - BD   Bangla(Bangladesh)
            //bn - IN   Bangla(India)
            //bs Bosnian(Latin)
            //ca Catalan Spanish
            //ca - ES - valencia  Valencian
            //    cs  Czech
            //    cy  Welsh
            //    da  Danish
            //    de  German(Germany)
            //de - de   German(Germany)
            //el Greek
            //en - GB   English(United Kingdom)
            //en - US   English(United States)
            //es Spanish(Spain)
            //es - ES   Spanish(Spain)
            //es - US   Spanish(United States)
            //es - MX   Spanish(Mexico)
            //et Estonian
            //eu Basque
            //fa Persian
            //fi Finnish
            //fil - Latn    Filipino
            //  fr  French(France)
            //fr - FR   French(France)
            //fr - CA   French(Canada)
            //ga Irish
            //gd - Latn Scottish Gaelic
            //gl Galician
            //gu Gujarati
            //ha - Latn Hausa(Latin)
            //he Hebrew
            //hi Hindi
            //hr Croatian
            //hu Hungarian
            //hy Armenian
            //id Indonesian
            //ig - Latn Igbo
            //  is Icelandic
            //  it  Italian(Italy)
            //it - it   Italian(Italy)
            //ja Japanese
            //ka Georgian
            //kk Kazakh
            //km Khmer
            //kn Kannada
            //ko Korean
            //kok Konkani
            //ku - Arab Central Kurdish
            //ky - Cyrl Kyrgyz
            //  lb  Luxembourgish
            //  lt  Lithuanian
            //  lv  Latvian
            //mi - Latn Maori
            //  mk  Macedonian
            //  ml  Malayalam
            //mn - Cyrl Mongolian(Cyrillic)
            //mr Marathi
            //ms Malay(Malaysia)
            //mt Maltese
            //nb Norwegian(Bokmål)
            //ne Nepali(Nepal)
            //nl Dutch(Netherlands)
            //nl - BE   Dutch(Netherlands)
            //nn Norwegian(Nynorsk)
            //nso Sesotho sa Leboa
            //or Odia
            //pa Punjabi(Gurmukhi)
            //pa - Arab Punjabi(Arabic)
            //pl Polish
            //prs - Arab    Dari
            //pt - BR   Portuguese(Brazil)
            //pt - PT   Portuguese(Portugal)
            //qut - Latn    K’iche’
            //quz Quechua(Peru)
            //ro Romanian(Romania)
            //ru Russian
            //rw Kinyarwanda
            //sd - Arab Sindhi(Arabic)
            //si Sinhala
            //sk Slovak
            //sl Slovenian
            //sq Albanian
            //sr - Cyrl - BA  Serbian(Cyrillic, Bosnia and Herzegovina)
            //sr - Cyrl - RS  Serbian(Cyrillic, Serbia)
            //sr - Latn - RS  Serbian(Latin, Serbia)
            //sv Swedish(Sweden)
            //sw Kiswahili
            //ta Tamil
            //te Telugu
            //tg - Cyrl Tajik(Cyrillic)
            //th Thai
            //ti Tigrinya
            //tk - Latn Turkmen(Latin)
            //tn Setswana
            //tr Turkish
            //tt - Cyrl Tatar(Cyrillic)
            //ug - Arab Uyghur
            //  uk  Ukrainian
            //  ur  Urdu
            //uz - Latn Uzbek(Latin)
            //vi Vietnamese
            //wo Wolof
            //xh isiXhosa
            //yo - Latn Yoruba
            //zh - Hans Chinese(Simplified)
            //zh - Hant Chinese(Traditional)
            //zu isiZulu


            //Language Culture Name	Display Name
            //af-ZA	Afrikaans - South Africa
            //sq-AL	Albanian - Albania
            //ar-DZ	Arabic - Algeria
            //ar-BH	Arabic - Bahrain
            //ar-EG	Arabic - Egypt
            //ar-IQ	Arabic - Iraq
            //ar-JO	Arabic - Jordan
            //ar-KW	Arabic - Kuwait
            //ar-LB	Arabic - Lebanon
            //ar-LY	Arabic - Libya
            //ar-MA	Arabic - Morocco
            //ar-OM	Arabic - Oman
            //ar-QA	Arabic - Qatar
            //ar-SA	Arabic - Saudi Arabia
            //ar-SY	Arabic - Syria
            //ar-TN	Arabic - Tunisia
            //ar-AE	Arabic - United Arab Emirates
            //ar-YE	Arabic - Yemen
            //hy-AM	Armenian - Armenia
            //Cy-az-AZ	Azeri (Cyrillic) - Azerbaijan
            //Lt-az-AZ	Azeri (Latin) - Azerbaijan
            //eu-ES	Basque - Basque
            //be-BY	Belarusian - Belarus
            //bg-BG	Bulgarian - Bulgaria
            //ca-ES	Catalan - Catalan
            //zh-CN	Chinese - China
            //zh-HK	Chinese - Hong Kong SAR
            //zh-MO	Chinese - Macau SAR
            //zh-SG	Chinese - Singapore
            //zh-TW	Chinese - Taiwan
            //zh-CHS	Chinese (Simplified)
            //zh-CHT	Chinese (Traditional)
            //hr-HR	Croatian - Croatia
            //cs-CZ	Czech - Czech Republic
            //da-DK	Danish - Denmark
            //div-MV	Dhivehi - Maldives
            //nl-BE	Dutch - Belgium
            //nl-NL	Dutch - The Netherlands
            //en-AU	English - Australia
            //en-BZ	English - Belize
            //en-CA	English - Canada
            //en-CB	English - Caribbean
            //en-IE	English - Ireland
            //en-JM	English - Jamaica
            //en-NZ	English - New Zealand
            //en-PH	English - Philippines
            //en-ZA	English - South Africa
            //en-TT	English - Trinidad and Tobago
            //en-GB	English - United Kingdom
            //en-US	English - United States
            //en-ZW	English - Zimbabwe
            //et-EE	Estonian - Estonia
            //fo-FO	Faroese - Faroe Islands
            //fa-IR	Farsi - Iran
            //fi-FI	Finnish - Finland
            //fr-BE	French - Belgium
            //fr-CA	French - Canada
            //fr-FR	French - France
            //fr-LU	French - Luxembourg
            //fr-MC	French - Monaco
            //fr-CH	French - Switzerland
            //gl-ES	Galician - Galician
            //ka-GE	Georgian - Georgia
            //de-AT	German - Austria
            //de-DE	German - Germany
            //de-LI	German - Liechtenstein
            //de-LU	German - Luxembourg
            //de-CH	German - Switzerland
            //el-GR	Greek - Greece
            //gu-IN	Gujarati - India
            //he-IL	Hebrew - Israel
            //hi-IN	Hindi - India
            //hu-HU	Hungarian - Hungary
            //is-IS	Icelandic - Iceland
            //id-ID	Indonesian - Indonesia
            //it-IT	Italian - Italy
            //it-CH	Italian - Switzerland
            //ja-JP	Japanese - Japan
            //kn-IN	Kannada - India
            //kk-KZ	Kazakh - Kazakhstan
            //kok-IN	Konkani - India
            //ko-KR	Korean - Korea
            //ky-KZ	Kyrgyz - Kazakhstan
            //lv-LV	Latvian - Latvia
            //lt-LT	Lithuanian - Lithuania
            //mk-MK	Macedonian (FYROM)
            //ms-BN	Malay - Brunei
            //ms-MY	Malay - Malaysia
            //mr-IN	Marathi - India
            //mn-MN	Mongolian - Mongolia
            //nb-NO	Norwegian (Bokmål) - Norway
            //nn-NO	Norwegian (Nynorsk) - Norway
            //pl-PL	Polish - Poland
            //pt-BR	Portuguese - Brazil
            //pt-PT	Portuguese - Portugal
            //pa-IN	Punjabi - India
            //ro-RO	Romanian - Romania
            //ru-RU	Russian - Russia
            //sa-IN	Sanskrit - India
            //Cy-sr-SP	Serbian (Cyrillic) - Serbia
            //Lt-sr-SP	Serbian (Latin) - Serbia
            //sk-SK	Slovak - Slovakia
            //sl-SI	Slovenian - Slovenia
            //es-AR	Spanish - Argentina
            //es-BO	Spanish - Bolivia
            //es-CL	Spanish - Chile
            //es-CO	Spanish - Colombia
            //es-CR	Spanish - Costa Rica
            //es-DO	Spanish - Dominican Republic
            //es-EC	Spanish - Ecuador
            //es-SV	Spanish - El Salvador
            //es-GT	Spanish - Guatemala
            //es-HN	Spanish - Honduras
            //es-MX	Spanish - Mexico
            //es-NI	Spanish - Nicaragua
            //es-PA	Spanish - Panama
            //es-PY	Spanish - Paraguay
            //es-PE	Spanish - Peru
            //es-PR	Spanish - Puerto Rico
            //es-ES	Spanish - Spain
            //es-UY	Spanish - Uruguay
            //es-VE	Spanish - Venezuela
            //sw-KE	Swahili - Kenya
            //sv-FI	Swedish - Finland
            //sv-SE	Swedish - Sweden
            //syr-SY	Syriac - Syria
            //ta-IN	Tamil - India
            //tt-RU	Tatar - Russia
            //te-IN	Telugu - India
            //th-TH	Thai - Thailand
            //tr-TR	Turkish - Turkey
            //uk-UA	Ukrainian - Ukraine
            //ur-PK	Urdu - Pakistan
            //Cy-uz-UZ	Uzbek (Cyrillic) - Uzbekistan
            //Lt-uz-UZ	Uzbek (Latin) - Uzbekistan
            //vi-VN	Vietnamese - Vietnam

        }

        public string GoogleMapCultureFactory()
        {
            string mapculture = "en-us";
            switch (Infrastructure.ApplicationEnvironment.CurrentDataSource.DatasourceCulture.ToLower())
            {
                case "ru-ru":
                    mapculture = "ru";
                    break;
                case "uk-ua":
                    mapculture = "ua";
                    break;
                case "pt-br":
                    mapculture = "pt-BR";
                    break;
                case "pt-pt":
                    mapculture = "pt-PT";
                    break;
                case "ro-ro":
                    mapculture = "ro";
                    break;
                case "hi-in":
                    mapculture = "hi";
                    break;
                case "de-de":
                case "de-at":
                case "de-li":
                case "de-lu":
                case "de-ch":
                    mapculture = "de";
                    break;
                case "fr-fr":
                case "fr-ca":
                case "fr-lu":
                case "fr-be":
                case "fr-mc":
                case "fr-ch":
                    mapculture = "fr";
                    break;
                case "es-ar":
                case "es-bo":
                case "es-cl":
                case "es-co":
                case "es-cr":
                case "es-do":
                case "es-ec":
                case "es-sv":
                case "es-gt":
                case "es-hn":
                case "es-mx":
                case "es-ni":
                case "es-pa":
                case "es-py":
                case "es-pe":
                case "es-pr":
                case "es-es":
                case "es-uy":
                case "es-ve":
                    mapculture = "es";
                    break;
                default:
                    mapculture = "en-us";
                    break;
            }
            return mapculture;
            //mapculture = "ru";
            //mapculture = "ar";
            //bg-BG 
            //mapculture = "bg";
            //mapculture = "bn";
            //ca-ES 
            //mapculture = "ca";
            //cs-CZ
            //mapculture = "cs";
            //da-DK
            //mapculture = "da";
            //da-DK
            //mapculture = "de";
            //el-GR 
            //mapculture = "el";
            //mapculture = "en";
            //"en-AU"
            //mapculture = "en-AU";
            //"en-GB"
            //mapculture = "en-GB";
            //mapculture = "es";
            //mapculture = "eu";
            //mapculture = "fa";
            //mapculture = "fi";
            //mapculture = "fil";
            //mapculture = "fr";
            //mapculture = "gl";
            //mapculture = "gu";
            //mapculture = "hi";
            //mapculture = "hr";
            //mapculture = "hu";
            //mapculture = "id";
            //mapculture = "it";
            //mapculture = "iw";
            //mapculture = "ja";
            //mapculture = "kn";
            //mapculture = "ko";
            //mapculture = "lt";
            //mapculture = "lv";
            //mapculture = "ml";
            //mapculture = "mr";
            //mapculture = "nl";
            //mapculture = "no";
            //mapculture = "pl";
            //mapculture = "pt";
            //mapculture = "pt-BR";
            //mapculture = "pt-PT";
            //mapculture = "ro";
            //mapculture = "ru";
            //mapculture = "sk";
            //mapculture = "sl";
            //mapculture = "sr";
            //mapculture = "sv";
            //mapculture = "ta";
            //mapculture = "te";
            //mapculture = "th";
            //mapculture = "tl";
            //mapculture = "tr";
            //mapculture = "uk";
            //mapculture = "vi";
            //mapculture = "zh-CN";
            //mapculture = "zh-TW";


            //ar Arabic     
            //bg Bulgarian  
            //bn Bengali    
            //ca Catalan    
            //cs Czech      
            //da Danish     
            //de German     
            //el Greek      
            //en English          
            //en - AU   English(Australian)    
            //en - GB   English(Great Britain) 
            //es Spanish                          
            //eu Basque     
            //eu Basque     
            //fa Farsi      
            //fi Finnish    
            //fil Filipino  
            //fr French     
            //gl Galician   
            //gu Gujarati   
            //hi Hindi      
            //hr Croatian   
            //hu Hungarian  
            //id Indonesian 
            //it Italian    
            //iw Hebrew     
            //ja Japanese      
            //kn Kannada
            // ko Korean
            // lt Lithuanian
            // lv Latvian
            //  ml Malayalam
            //  mr Marathi
            // nl Dutch
            // no Norwegian
            // pl Polish
            //pt Portuguese
            //pt - BR   Portuguese(Brazil)
            //pt - PT   Portuguese(Portugal)
            //ro Romanian
            //ru Russian
            //sk Slovak
            // sl Slovenian
            //  sr Serbian
            //sv Swedish
            // ta Tamil
            // te Telugu
            //th Thai
            // tl Tagalog
            // tr Turkish
            // uk Ukrainian
            // vi Vietnamese
            //    zh - CN   Chinese(Simplified)
            // zh - TW   Chinese(Traditional)        }




            //Language Culture Name	Display Name
            //af-ZA	Afrikaans - South Africa
            //sq-AL	Albanian - Albania
            //ar-DZ	Arabic - Algeria
            //ar-BH	Arabic - Bahrain
            //ar-EG	Arabic - Egypt
            //ar-IQ	Arabic - Iraq
            //ar-JO	Arabic - Jordan
            //ar-KW	Arabic - Kuwait
            //ar-LB	Arabic - Lebanon
            //ar-LY	Arabic - Libya
            //ar-MA	Arabic - Morocco
            //ar-OM	Arabic - Oman
            //ar-QA	Arabic - Qatar
            //ar-SA	Arabic - Saudi Arabia
            //ar-SY	Arabic - Syria
            //ar-TN	Arabic - Tunisia
            //ar-AE	Arabic - United Arab Emirates
            //ar-YE	Arabic - Yemen
            //hy-AM	Armenian - Armenia
            //Cy-az-AZ	Azeri (Cyrillic) - Azerbaijan
            //Lt-az-AZ	Azeri (Latin) - Azerbaijan
            //eu-ES	Basque - Basque
            //be-BY	Belarusian - Belarus
            //bg-BG	Bulgarian - Bulgaria
            //ca-ES	Catalan - Catalan
            //zh-CN	Chinese - China
            //zh-HK	Chinese - Hong Kong SAR
            //zh-MO	Chinese - Macau SAR
            //zh-SG	Chinese - Singapore
            //zh-TW	Chinese - Taiwan
            //zh-CHS	Chinese (Simplified)
            //zh-CHT	Chinese (Traditional)
            //hr-HR	Croatian - Croatia
            //cs-CZ	Czech - Czech Republic
            //da-DK	Danish - Denmark
            //div-MV	Dhivehi - Maldives
            //nl-BE	Dutch - Belgium
            //nl-NL	Dutch - The Netherlands
            //en-AU	English - Australia
            //en-BZ	English - Belize
            //en-CA	English - Canada
            //en-CB	English - Caribbean
            //en-IE	English - Ireland
            //en-JM	English - Jamaica
            //en-NZ	English - New Zealand
            //en-PH	English - Philippines
            //en-ZA	English - South Africa
            //en-TT	English - Trinidad and Tobago
            //en-GB	English - United Kingdom
            //en-US	English - United States
            //en-ZW	English - Zimbabwe
            //et-EE	Estonian - Estonia
            //fo-FO	Faroese - Faroe Islands
            //fa-IR	Farsi - Iran
            //fi-FI	Finnish - Finland
            //fr-BE	French - Belgium
            //fr-CA	French - Canada
            //fr-FR	French - France
            //fr-LU	French - Luxembourg
            //fr-MC	French - Monaco
            //fr-CH	French - Switzerland
            //gl-ES	Galician - Galician
            //ka-GE	Georgian - Georgia
            //de-AT	German - Austria
            //de-DE	German - Germany
            //de-LI	German - Liechtenstein
            //de-LU	German - Luxembourg
            //de-CH	German - Switzerland
            //el-GR	Greek - Greece
            //gu-IN	Gujarati - India
            //he-IL	Hebrew - Israel
            //hi-IN	Hindi - India
            //hu-HU	Hungarian - Hungary
            //is-IS	Icelandic - Iceland
            //id-ID	Indonesian - Indonesia
            //it-IT	Italian - Italy
            //it-CH	Italian - Switzerland
            //ja-JP	Japanese - Japan
            //kn-IN	Kannada - India
            //kk-KZ	Kazakh - Kazakhstan
            //kok-IN	Konkani - India
            //ko-KR	Korean - Korea
            //ky-KZ	Kyrgyz - Kazakhstan
            //lv-LV	Latvian - Latvia
            //lt-LT	Lithuanian - Lithuania
            //mk-MK	Macedonian (FYROM)
            //ms-BN	Malay - Brunei
            //ms-MY	Malay - Malaysia
            //mr-IN	Marathi - India
            //mn-MN	Mongolian - Mongolia
            //nb-NO	Norwegian (Bokmål) - Norway
            //nn-NO	Norwegian (Nynorsk) - Norway
            //pl-PL	Polish - Poland
            //pt-BR	Portuguese - Brazil
            //pt-PT	Portuguese - Portugal
            //pa-IN	Punjabi - India
            //ro-RO	Romanian - Romania
            //ru-RU	Russian - Russia
            //sa-IN	Sanskrit - India
            //Cy-sr-SP	Serbian (Cyrillic) - Serbia
            //Lt-sr-SP	Serbian (Latin) - Serbia
            //sk-SK	Slovak - Slovakia
            //sl-SI	Slovenian - Slovenia
            //es-AR	Spanish - Argentina
            //es-BO	Spanish - Bolivia
            //es-CL	Spanish - Chile
            //es-CO	Spanish - Colombia
            //es-CR	Spanish - Costa Rica
            //es-DO	Spanish - Dominican Republic
            //es-EC	Spanish - Ecuador
            //es-SV	Spanish - El Salvador
            //es-GT	Spanish - Guatemala
            //es-HN	Spanish - Honduras
            //es-MX	Spanish - Mexico
            //es-NI	Spanish - Nicaragua
            //es-PA	Spanish - Panama
            //es-PY	Spanish - Paraguay
            //es-PE	Spanish - Peru
            //es-PR	Spanish - Puerto Rico
            //es-ES	Spanish - Spain
            //es-UY	Spanish - Uruguay
            //es-VE	Spanish - Venezuela
            //sw-KE	Swahili - Kenya
            //sv-FI	Swedish - Finland
            //sv-SE	Swedish - Sweden
            //syr-SY	Syriac - Syria
            //ta-IN	Tamil - India
            //tt-RU	Tatar - Russia
            //te-IN	Telugu - India
            //th-TH	Thai - Thailand
            //tr-TR	Turkish - Turkey
            //uk-UA	Ukrainian - Ukraine
            //ur-PK	Urdu - Pakistan
            //Cy-uz-UZ	Uzbek (Cyrillic) - Uzbekistan
            //Lt-uz-UZ	Uzbek (Latin) - Uzbekistan
            //vi-VN	Vietnamese - Vietnam
        }

    }
}
