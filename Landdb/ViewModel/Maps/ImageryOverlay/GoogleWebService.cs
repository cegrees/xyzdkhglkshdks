﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Web;
using System.Xml;
using System.Xml.Linq; 
using System.Xml.XPath;
using System.Data; 
using ThinkGeo.MapSuite.Core;
using System.Drawing;

namespace Landdb.ViewModel.Maps.ImageryOverlay {
    public class GoogleWebService {
        private string clientId;
        private string privateKey;
        private string apiKey;
        private GoogleMapsMapType mapType;
        private GoogleMapsPictureFormat pictureFormat;
        private int tileHeight = 640;
        private int tileWidth = 640;
        private MapCultureFactory mapculturefactory = new MapCultureFactory();
        private WebProxy webProxy;
        private int timeoutInSeconds;

        private GdiPlusRasterLayer rasterLayer;

        [System.NonSerialized]
        private System.EventHandler<CreatingRequestGoogleMapsLayerEventArgs> _creatingrequest;

        [System.NonSerialized]
        private System.EventHandler<SendingWebRequestEventArgs> _sendingwebrequest;

        [System.NonSerialized]
        private System.EventHandler<SentWebRequestEventArgs> _sentwebrequest;

        //http://maps.googleapis.com/maps/api/staticmap?center=36.619932,-88.456417&zoom=15&size=2048x2048&scale=2&maptype=hybrid&format=jpg-baseline&sensor=false&key=AIzaSyAdlNYBc_7sWnX0J-bU9hFik-bYPBuCaW0

        public GoogleWebService() {
            this.timeoutInSeconds = 20;
            this.webProxy = new WebProxy();
        }

        public GoogleWebService(int tileHeight, int tileWidth, GoogleMapsMapType mapType, GoogleMapsPictureFormat pictureFormat, int timeoutInSeconds) {
            this.tileHeight = tileHeight;
            this.tileWidth = tileWidth;
            this.mapType = mapType;
            this.pictureFormat = pictureFormat;
            this.timeoutInSeconds = timeoutInSeconds;

            this.clientId = string.Empty;
            this.privateKey = string.Empty;
            this.apiKey = string.Empty;

            this.webProxy = new WebProxy();
        }

        public GoogleWebService(int tileHeight, int tileWidth, GoogleMapsMapType mapType, GoogleMapsPictureFormat pictureFormat, int timeoutInSeconds, string apikey) {

            this.tileHeight = tileHeight;
            this.tileWidth = tileWidth;
            this.mapType = mapType;
            this.pictureFormat = pictureFormat;
            this.timeoutInSeconds = timeoutInSeconds;

            this.clientId = string.Empty;
            this.privateKey = string.Empty;
            this.apiKey = apikey;

            this.webProxy = new WebProxy();
        }

        public GoogleWebService(int tileHeight, int tileWidth, GoogleMapsMapType mapType, GoogleMapsPictureFormat pictureFormat,  int timeoutInSeconds, string clientId, string privateKey) {

            this.tileHeight = tileHeight;
            this.tileWidth = tileWidth;
            this.mapType = mapType;
            this.pictureFormat = pictureFormat;
            this.timeoutInSeconds = timeoutInSeconds;

            this.clientId = clientId;
            this.privateKey = privateKey;
            this.apiKey = string.Empty;

            this.webProxy = new WebProxy();
        }

        public Layer GoogleWebLayer {
            get { return rasterLayer; }
        } 

        public Stream GetGoogleMapsImage(double latitude, double longitude, int zoomLevelNumber) {
            return GetGoogleMapsImage(latitude, longitude, zoomLevelNumber, null, null, 32716);
        }
        public Stream GetGoogleMapsImage(double latitude, double longitude, int zoomLevelNumber, RectangleShape extent, RectangleShape projextent, int epsgcode) {
            string uriString = this.BuildURI(longitude, latitude, zoomLevelNumber);
            Stream currentstream = null;
            rasterLayer = null;
            try {
                CreatingRequestGoogleMapsLayerEventArgs creatingRequestGoogleMapsLayerEventArgs = new CreatingRequestGoogleMapsLayerEventArgs(new Uri(uriString));
                this.OnCreatingRequest(creatingRequestGoogleMapsLayerEventArgs);
                WebRequest webRequest = WebRequest.Create(creatingRequestGoogleMapsLayerEventArgs.RequestUri);
                webRequest.Timeout = this.timeoutInSeconds * 1000;
                webRequest.Proxy = this.webProxy;
                WebResponse webResponse = this.SendWebRequest(webRequest);
                currentstream = webResponse.GetResponseStream();
                Bitmap bm = new Bitmap(currentstream);

                if (extent != null) {
                    string bitmapfilename = string.Format("{0}.bmp", DateTime.Now.Ticks.ToString());
                    var fulldirectoryname = Path.Combine(Infrastructure.ApplicationEnvironment.GoogleMapCacheDirectory, bitmapfilename);
                    if (!Directory.Exists(Infrastructure.ApplicationEnvironment.GoogleMapCacheDirectory)) {
                        Directory.CreateDirectory(Infrastructure.ApplicationEnvironment.GoogleMapCacheDirectory);
                    }
                    bm.Save(fulldirectoryname, System.Drawing.Imaging.ImageFormat.Bmp);

                    string tempFileBase = Path.Combine(Path.GetDirectoryName(fulldirectoryname), Path.GetFileNameWithoutExtension(fulldirectoryname));
                    string gnewimagerynamebpw = string.Format("{0}.bpw", tempFileBase);

                    //string bitmapworldfile = CreateWorldFile(fulldirectoryname);
                    //rasterLayer = new GdiPlusRasterLayer(fulldirectoryname, bitmapworldfile);

                    rasterLayer = new GdiPlusRasterLayer(fulldirectoryname, projextent);
                    rasterLayer.UpperThreshold = Double.MaxValue;
                    rasterLayer.LowerThreshold = 0;
                    rasterLayer.DrawingExceptionMode = DrawingExceptionMode.DrawException;
                    if (!rasterLayer.IsOpen) {
                        rasterLayer.Open();
                    }
                    //string worldfiletext = rasterLayer.ImageSource.GetWorldFileText();
                    //System.IO.File.WriteAllText(gnewimagerynamebpw, worldfiletext);
                }
            }
            catch (System.Exception ex) {
                int asdfsadf = 0;
            }
            return currentstream;
        }


        private readonly int GEOTIFF_MODELPIXELSCALETAG = 33550;
        private readonly int GEOTIFF_MODELTIEPOINTTAG = 33922;

        /// <summary>
        /// Create tfw - world file.
        /// </summary>
        /// <param name="filename">tif image full filename</param>
        private string CreateWorldFile(string filename) {
            string tfwFileName = Path.GetDirectoryName(filename) + "\\" + Path.GetFileNameWithoutExtension(filename) + ".tfw";
            if (File.Exists(tfwFileName)) {
                return tfwFileName;
            }

            using (var bitmapImage = new Bitmap(filename)) {
                System.Drawing.Imaging.PropertyItem[] imageProps = bitmapImage.PropertyItems;

                var modelscale = imageProps.First(a => a.Id == GEOTIFF_MODELPIXELSCALETAG);
                var tiepoint = imageProps.First(a => a.Id == GEOTIFF_MODELTIEPOINTTAG);

                double x = BitConverter.ToDouble(tiepoint.Value, 0 + 24);
                double y = BitConverter.ToDouble(tiepoint.Value, 0 + 32);

                double xscale = BitConverter.ToDouble(modelscale.Value, 0);
                double yscale = BitConverter.ToDouble(modelscale.Value, 0 + 8);


                using (System.IO.StreamWriter file = new System.IO.StreamWriter(tfwFileName)) {
                    file.WriteLine(xscale);
                    file.WriteLine("0.0");
                    file.WriteLine("0.0");
                    file.WriteLine(yscale);
                    file.WriteLine(x);
                    file.WriteLine(y);
                }

                return tfwFileName;
            }
        }

        private string BuildURI(double lon, double lat, int zoomLevelNumber) {
            string text = GetImageOrentation(zoomLevelNumber, lon, lat);
            System.Text.StringBuilder stringBuilder = new System.Text.StringBuilder("http://maps.googleapis.com/maps/api/staticmap?");
            stringBuilder.AppendFormat(System.Globalization.CultureInfo.InvariantCulture, "{0}&", new object[] { text });
            stringBuilder.AppendFormat(System.Globalization.CultureInfo.InvariantCulture, "maptype={0}&", new object[] { this.GetGoogleMapType() });
            stringBuilder.AppendFormat(System.Globalization.CultureInfo.InvariantCulture, "format={0}&", new object[] { this.GetPictureFormat() });
            stringBuilder.Append("sensor=false");
            if (!string.IsNullOrEmpty(this.clientId) && !string.IsNullOrEmpty(this.privateKey)) {
                stringBuilder.AppendFormat(System.Globalization.CultureInfo.InvariantCulture, "&client={0}", new object[] { this.clientId });
                stringBuilder.AppendFormat(System.Globalization.CultureInfo.InvariantCulture, "&signature={0}", new object[] { this.SignPrivateKey(stringBuilder.ToString()) });
            }
            if (!string.IsNullOrEmpty(this.apiKey) && string.IsNullOrEmpty(this.clientId)) {
                stringBuilder.AppendFormat(System.Globalization.CultureInfo.InvariantCulture, "&key={0}", new object[] { this.apiKey });
            }
            if (Infrastructure.ApplicationEnvironment.CurrentDataSource.DatasourceCulture.ToLower() != "en-us")
            {
                stringBuilder.AppendFormat(System.Globalization.CultureInfo.InvariantCulture, "&language={0}", new object[] { Infrastructure.ApplicationEnvironment.GoogleMapCulture });
            }
            return stringBuilder.ToString();
        }

        private string GetImageOrentation(int zoomLevelNumber, double longitude, double latitude) {
            System.Text.StringBuilder stringBuilder = new System.Text.StringBuilder();
            stringBuilder.AppendFormat(System.Globalization.CultureInfo.InvariantCulture, "center={0},{1}&", new object[] { System.Math.Round(latitude, 6), System.Math.Round(longitude, 6) });
            stringBuilder.AppendFormat(System.Globalization.CultureInfo.InvariantCulture, "zoom={0}&", new object[] { zoomLevelNumber });
            stringBuilder.AppendFormat(System.Globalization.CultureInfo.InvariantCulture, "size={0}x{1}", new object[] { this.tileWidth, this.tileHeight });
            return stringBuilder.ToString();
        }

        private string GetGoogleMapType() {
            switch (this.mapType) {
                case GoogleMapsMapType.RoadMap:
                    return "roadmap";
                case GoogleMapsMapType.Mobile:
                    return "mobile";
                case GoogleMapsMapType.Satellite:
                    return "satellite";
                case GoogleMapsMapType.Terrain:
                    return "terrain";
                case GoogleMapsMapType.Hybrid:
                    return "hybrid";
                default:
                    return "hybrid";
            }
        }

        public string GetPictureFormat() {
            GoogleMapsPictureFormat googleMapsPictureFormat = this.pictureFormat;
            switch (googleMapsPictureFormat) {
                case GoogleMapsPictureFormat.Jpeg: {
                        return "jpg-baseline";
                    }
                case GoogleMapsPictureFormat.Gif: {
                        return "gif";
                    }
                case GoogleMapsPictureFormat.Png8: {
                        return "png8";
                    }
                case GoogleMapsPictureFormat.Png32: {
                        return "Png32";
                    }
                default:
                    return "jpg-baseline";
            }
        }

        private string SignPrivateKey(string url) {
            string s = this.privateKey.Replace("-", "+").Replace("_", "/");
            byte[] key = System.Convert.FromBase64String(s);
            Uri uri = new Uri(url);
            byte[] buffer = EncodePrivateKey(uri.LocalPath + uri.Query);
            System.Security.Cryptography.HMACSHA1 hMACSHA = new System.Security.Cryptography.HMACSHA1(key);
            byte[] inArray = hMACSHA.ComputeHash(buffer);
            return System.Convert.ToBase64String(inArray).Replace("+", "-").Replace("/", "_");
        }

        private static byte[] EncodePrivateKey(string value) {
            byte[] bytes = System.Text.Encoding.Unicode.GetBytes(value);
            byte[] array = new byte[bytes.Length / 2];
            for (int i = 0; i < bytes.Length; i += 2) {
                array[i / 2] = bytes[i];
                if (array[i / 2] > 127) {
                    array[i / 2] = 63;
                }
            }
            return array;
        }


        public event System.EventHandler<CreatingRequestGoogleMapsLayerEventArgs> CreatingRequest {
            add {
                System.EventHandler<CreatingRequestGoogleMapsLayerEventArgs> eventHandler = this._creatingrequest;
                System.EventHandler<CreatingRequestGoogleMapsLayerEventArgs> eventHandler2;
                do {
                    eventHandler2 = eventHandler;
                    System.EventHandler<CreatingRequestGoogleMapsLayerEventArgs> combinedvalue = (System.EventHandler<CreatingRequestGoogleMapsLayerEventArgs>)System.Delegate.Combine(eventHandler2, value);
                    eventHandler = System.Threading.Interlocked.CompareExchange<System.EventHandler<CreatingRequestGoogleMapsLayerEventArgs>>(ref this._creatingrequest, combinedvalue, eventHandler2);
                }
                while (eventHandler != eventHandler2);
            }
            remove {
                System.EventHandler<CreatingRequestGoogleMapsLayerEventArgs> eventHandler = this._creatingrequest;
                System.EventHandler<CreatingRequestGoogleMapsLayerEventArgs> eventHandler2;
                do {
                    eventHandler2 = eventHandler;
                    System.EventHandler<CreatingRequestGoogleMapsLayerEventArgs> combinedvalue = (System.EventHandler<CreatingRequestGoogleMapsLayerEventArgs>)System.Delegate.Remove(eventHandler2, value);
                    eventHandler = System.Threading.Interlocked.CompareExchange<System.EventHandler<CreatingRequestGoogleMapsLayerEventArgs>>(ref this._creatingrequest, combinedvalue, eventHandler2);
                }
                while (eventHandler != eventHandler2);
            }
        }
        public event System.EventHandler<SendingWebRequestEventArgs> SendingWebRequest {
            add {
                System.EventHandler<SendingWebRequestEventArgs> eventHandler = this._sendingwebrequest;
                System.EventHandler<SendingWebRequestEventArgs> eventHandler2;
                do {
                    eventHandler2 = eventHandler;
                    System.EventHandler<SendingWebRequestEventArgs> combinedvalue = (System.EventHandler<SendingWebRequestEventArgs>)System.Delegate.Combine(eventHandler2, value);
                    eventHandler = System.Threading.Interlocked.CompareExchange<System.EventHandler<SendingWebRequestEventArgs>>(ref this._sendingwebrequest, combinedvalue, eventHandler2);
                }
                while (eventHandler != eventHandler2);
            }
            remove {
                System.EventHandler<SendingWebRequestEventArgs> eventHandler = this._sendingwebrequest;
                System.EventHandler<SendingWebRequestEventArgs> eventHandler2;
                do {
                    eventHandler2 = eventHandler;
                    System.EventHandler<SendingWebRequestEventArgs> combinedvalue = (System.EventHandler<SendingWebRequestEventArgs>)System.Delegate.Remove(eventHandler2, value);
                    eventHandler = System.Threading.Interlocked.CompareExchange<System.EventHandler<SendingWebRequestEventArgs>>(ref this._sendingwebrequest, combinedvalue, eventHandler2);
                }
                while (eventHandler != eventHandler2);
            }
        }

        public event System.EventHandler<SentWebRequestEventArgs> SentWebRequest {
            add {
                System.EventHandler<SentWebRequestEventArgs> eventHandler = this._sentwebrequest;
                System.EventHandler<SentWebRequestEventArgs> eventHandler2;
                do {
                    eventHandler2 = eventHandler;
                    System.EventHandler<SentWebRequestEventArgs> combinedvalue = (System.EventHandler<SentWebRequestEventArgs>)System.Delegate.Combine(eventHandler2, value);
                    eventHandler = System.Threading.Interlocked.CompareExchange<System.EventHandler<SentWebRequestEventArgs>>(ref this._sentwebrequest, combinedvalue, eventHandler2);
                }
                while (eventHandler != eventHandler2);
            }
            remove {
                System.EventHandler<SentWebRequestEventArgs> eventHandler = this._sentwebrequest;
                System.EventHandler<SentWebRequestEventArgs> eventHandler2;
                do {
                    eventHandler2 = eventHandler;
                    System.EventHandler<SentWebRequestEventArgs> combinedvalue = (System.EventHandler<SentWebRequestEventArgs>)System.Delegate.Remove(eventHandler2, value);
                    eventHandler = System.Threading.Interlocked.CompareExchange<System.EventHandler<SentWebRequestEventArgs>>(ref this._sentwebrequest, combinedvalue, eventHandler2);
                }
                while (eventHandler != eventHandler2);
            }
        }

        private WebResponse SendWebRequest(WebRequest webRequest) {
            SendingWebRequestEventArgs sendingWebRequestEventArgs = new SendingWebRequestEventArgs(webRequest);
            this.OnSendingWebRequest(sendingWebRequestEventArgs);
            WebResponse response = this.SendWebRequestCore(sendingWebRequestEventArgs.WebRequest);
            SentWebRequestEventArgs sentWebRequestEventArgs = new SentWebRequestEventArgs(response);
            this.OnSentWebRequest(sentWebRequestEventArgs);
            return sentWebRequestEventArgs.Response;
        }

        private WebResponse SendWebRequestCore(WebRequest webRequest) {
            return webRequest.GetResponse();
        }

        protected virtual void OnCreatingRequest(CreatingRequestGoogleMapsLayerEventArgs e) {
            System.EventHandler<CreatingRequestGoogleMapsLayerEventArgs> eventHandler = this._creatingrequest;
            if (eventHandler != null) {
                eventHandler(this, e);
            }
        }
        private  void OnSendingWebRequest(SendingWebRequestEventArgs e) {
            System.EventHandler<SendingWebRequestEventArgs> eventHandler = this._sendingwebrequest;
            if (eventHandler != null) {
                eventHandler(this, e);
            }
        }

        private void OnSentWebRequest(SentWebRequestEventArgs e) {
            System.EventHandler<SentWebRequestEventArgs> eventHandler = this._sentwebrequest;
            if (eventHandler != null) {
                eventHandler(this, e);
            }
        }

        public bool TempFileCleanUp(string tempFile) {
            bool successfuldelete = true;
            DirectoryInfo dir = new DirectoryInfo(Infrastructure.ApplicationEnvironment.GoogleMapCacheDirectory);
            if (!Directory.Exists(Infrastructure.ApplicationEnvironment.GoogleMapCacheDirectory)) {
                Directory.CreateDirectory(Infrastructure.ApplicationEnvironment.GoogleMapCacheDirectory);
            }

            if (dir.Exists) {
                FileInfo[] files = dir.GetFiles(@"*.bmp");
                foreach (FileInfo file in files) {
                    try {
                        if (File.Exists(file.FullName)) {
                            File.Delete(file.FullName);
                        }
                    }
                    catch {
                        successfuldelete = false;
                    }
                }
                FileInfo[] file2s = dir.GetFiles(@"*.bpw");
                foreach (FileInfo file in file2s) {
                    try {
                        if (File.Exists(file.FullName)) {
                            File.Delete(file.FullName);
                        }
                    }
                    catch {
                    }
                }
            }
            return successfuldelete;
        }

    }
}
