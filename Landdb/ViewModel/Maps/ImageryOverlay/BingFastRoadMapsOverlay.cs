﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ThinkGeo.MapSuite.WpfDesktopEdition;
using ThinkGeo.MapSuite.Core;
using System.Diagnostics;
using Landdb.Client.Infrastructure;
using Landdb.ViewModel.Fields.FieldDetails.MapModels;

namespace Landdb.ViewModel.Maps.ImageryOverlay {
    public class BingFastRoadMapsOverlay : IMapOverlay {
        private Overlay overlay;
        GeographyUnit geographyunit;
        RectangleShape extent;
        string projectionstring;
        bool visibility;
        bool onlineOnly;

        public BingFastRoadMapsOverlay() {
            SetMapInfoSelectorItemToRoadsOnline();
            extent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
            geographyunit = GeographyUnit.Meter;
            projectionstring = Proj4Projection.GetGoogleMapParametersString();
            visibility = true;
            onlineOnly = true;
            overlay = NewOverlay();
        }

        public BingFastRoadMapsOverlay(bool? allowofflinemap) {
            SetMapInfoSelectorItemToRoadsOnline();
            extent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
            geographyunit = GeographyUnit.Meter;
            projectionstring = Proj4Projection.GetGoogleMapParametersString();
            visibility = true;
            onlineOnly = true;
            if (!allowofflinemap.HasValue) {
                overlay = NewOverlay();
            }
            else if (allowofflinemap.HasValue && allowofflinemap.Value) {
                overlay = NewOverlay();
            }
            else {
                onlineOnly = true;
                overlay = NewOnlineOverlay();
            }
        }

        public string Name => MapInfoSelectorItem.MapTypeKeyText;
        public MapInfoSelectorItem MapInfoSelectorItem { get; set; }

        private void SetMapInfoSelectorItemToRoadsOnline() {
            MapInfoSelectorItem = new MapInfoSelectorItem(MapStatusType.RoadsOnline);
        }

        public Overlay Overlay {
            get { return overlay; }
            set { overlay = value; }
        }

        public GeographyUnit GeographyUnit {
            get { return geographyunit; }
            set { geographyunit = value; }
        }

        public RectangleShape Extent {
            get { return extent; }
            set { extent = value; }
        }

        public string ProjectionString {
            get { return projectionstring; }
            set { projectionstring = value; }
        }

        public bool Visibility {
            get { return visibility; }
            set { visibility = value; }
        }

        public bool OnlineOnly {
            get { return onlineOnly; }
            set { onlineOnly = value; }
        }

        public Overlay NewOverlay() {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            SetMapInfoSelectorItemToRoadsOnline();
            extent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
            geographyunit = GeographyUnit.Meter;
            projectionstring = Proj4Projection.GetGoogleMapParametersString();
            visibility = true;
            onlineOnly = true;

            BingMapsOverlay bingOverlay = new BingMapsOverlay();
            string bingpassword = "AgwFGldoH7RpgsCWgbL0pqvweosSbsuGvRinCPUPgJwGUlqwYBYsRY3O33dQHxAy";

            bingOverlay.ApplicationId = bingpassword;
            bingOverlay.MapType = BingMapsMapType.Road;
            bingOverlay.DrawingExceptionMode = DrawingExceptionMode.DrawException;
            bingOverlay.ImageFormat = TileImageFormat.Jpeg;
            bingOverlay.IsVisible = visibility;
            overlay = bingOverlay;
            sw.Stop();
            Console.WriteLine(sw.ElapsedMilliseconds);
            return bingOverlay;
        }

        public Overlay NewOnlineOverlay() {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            SetMapInfoSelectorItemToRoadsOnline();
            extent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
            geographyunit = GeographyUnit.Meter;
            projectionstring = Proj4Projection.GetGoogleMapParametersString();
            visibility = true;
            onlineOnly = true;

            BingMapsOverlay bingOverlay = new BingMapsOverlay();
            string bingpassword = "AgwFGldoH7RpgsCWgbL0pqvweosSbsuGvRinCPUPgJwGUlqwYBYsRY3O33dQHxAy";

            bingOverlay.ApplicationId = bingpassword;
            bingOverlay.MapType = BingMapsMapType.Road;
            bingOverlay.DrawingExceptionMode = DrawingExceptionMode.DrawException;
            bingOverlay.ImageFormat = TileImageFormat.Jpeg;
            bingOverlay.IsVisible = visibility;
            overlay = bingOverlay;
            sw.Stop();
            Console.WriteLine(sw.ElapsedMilliseconds);
            return bingOverlay;
        }
        //public string LoadAttribution(int zoomLevelsNumber) {
        //    return string.Empty;
        //}

        public ZoomLevelSet GetZoomLevelSet() {
            ZoomLevelFactory zoomlevelfactory = new ZoomLevelFactory("StandardPlus40");
            return zoomlevelfactory.ZoomLevelSET;
            //return null;
        }
        public Overlay ExtentHasChanged(int tileHeight, int tileWidth, RectangleShape extent, RectangleShape projextent, PointShape wgscenterpoint, int zoomlevelnumber, int epsgcode) {
            return null;
        }
    }
}
