﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ThinkGeo.MapSuite.WpfDesktopEdition;
using ThinkGeo.MapSuite.Core;
using System.Diagnostics;
using Landdb.Client.Infrastructure;
using Landdb.ViewModel.Fields.FieldDetails.MapModels;

namespace Landdb.ViewModel.Maps.ImageryOverlay {
    public class BingLayerOverlay : IMapOverlay {
        private Overlay overlay;
        GeographyUnit geographyunit;
        RectangleShape extent;
        string projectionstring;
        bool visibility;
        bool onlineOnly;

        public BingLayerOverlay() {
            SetMapInfoSelectorItemToBing();
            extent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
            geographyunit = GeographyUnit.Meter;
            projectionstring = Proj4Projection.GetGoogleMapParametersString();
            visibility = true;
            onlineOnly = false;
            overlay = NewOverlay();
        }

        public BingLayerOverlay(bool? allowofflinemap) {
            SetMapInfoSelectorItemToBing();
            extent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
            geographyunit = GeographyUnit.Meter;
            projectionstring = Proj4Projection.GetGoogleMapParametersString();
            visibility = true;
            onlineOnly = false;
            overlay = NewOverlay();
        }

        public string Name => MapInfoSelectorItem.MapTypeKeyText;
        public MapInfoSelectorItem MapInfoSelectorItem { get; set; }

        private void SetMapInfoSelectorItemToBing() {
            MapInfoSelectorItem = new MapInfoSelectorItem(MapStatusType.Bing);
        }

        public Overlay Overlay {
            get { return overlay; }
            set { overlay = value; }
        }

        public GeographyUnit GeographyUnit {
            get { return geographyunit; }
            set { geographyunit = value; }
        }

        public RectangleShape Extent {
            get { return extent; }
            set { extent = value; }
        }

        public string ProjectionString {
            get { return projectionstring; }
            set { projectionstring = value; }
        }

        public bool Visibility {
            get { return visibility; }
            set { visibility = value; }
        }

        public bool OnlineOnly {
            get { return onlineOnly; }
            set { onlineOnly = value; }
        }

        public Overlay NewOverlay() {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            SetMapInfoSelectorItemToBing();
            extent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
            geographyunit = GeographyUnit.Meter;
            projectionstring = Proj4Projection.GetGoogleMapParametersString();
            visibility = true;
            onlineOnly = false;

            overlay = NewOverlay();
            BingMapsLayer bingmapsLayer = new BingMapsLayer();
            //BingMapsOverlay bingOverlay = new BingMapsOverlay();
            string bingpassword = "AgwFGldoH7RpgsCWgbL0pqvweosSbsuGvRinCPUPgJwGUlqwYBYsRY3O33dQHxAy";

            bingmapsLayer.ApplicationId = bingpassword;
            bingmapsLayer.MapType = BingMapsMapType.AerialWithLabels;
            bingmapsLayer.DrawingExceptionMode = DrawingExceptionMode.DrawException;
            bingmapsLayer.TileCache = new InMemoryBitmapTileCache();
            //bingmapsLayer.TileType = TileType.SingleTile;
            //bingmapsLayer.ImageFormat = TileImageFormat.Jpeg;
            //bingmapsLayer.TileBuffer = 1;
            //bingmapsLayer.ProjectionFromSphericalMercator

            string gDirectory = Landdb.Infrastructure.ApplicationEnvironment.MapCacheDirectory;
            FileBitmapTileCache bitmapTileCache = new FileBitmapTileCache();
            bitmapTileCache.CacheDirectory = gDirectory;
            bitmapTileCache.CacheId = "BaseCachedTiles";
            bitmapTileCache.TileAccessMode = TileAccessMode.Default;
            bitmapTileCache.ImageFormat = TileImageFormat.Jpeg;
            //bingmapsLayer.CacheDirectory = gDirectory;
            bingmapsLayer.TileCache = bitmapTileCache;
            //overlay = bingOverlay;
            sw.Stop();
            Console.WriteLine(sw.ElapsedMilliseconds);
            return overlay;
        }

        public Overlay NewOnlineOverlay() {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            SetMapInfoSelectorItemToBing();
            extent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
            geographyunit = GeographyUnit.Meter;
            projectionstring = Proj4Projection.GetGoogleMapParametersString();
            visibility = true;
            onlineOnly = true;

            BingMapsOverlay bingOverlay = new BingMapsOverlay();
            string bingpassword = "AgwFGldoH7RpgsCWgbL0pqvweosSbsuGvRinCPUPgJwGUlqwYBYsRY3O33dQHxAy";

            bingOverlay.ApplicationId = bingpassword;
            bingOverlay.MapType = BingMapsMapType.AerialWithLabels;
            bingOverlay.DrawingExceptionMode = DrawingExceptionMode.DrawException;
            bingOverlay.ImageFormat = TileImageFormat.Jpeg;
            bingOverlay.IsVisible = visibility;
            overlay = bingOverlay;
            sw.Stop();
            Console.WriteLine(sw.ElapsedMilliseconds);
            return bingOverlay;
        }
        
        public ZoomLevelSet GetZoomLevelSet() {
            ZoomLevelFactory zoomlevelfactory = new ZoomLevelFactory("StandardPlus40");
            return zoomlevelfactory.ZoomLevelSET;
            //return null;
        }
        public Overlay ExtentHasChanged(int tileHeight, int tileWidth, RectangleShape extent, RectangleShape projextent, PointShape wgscenterpoint, int zoomlevelnumber, int epsgcode) {
            return null;
        }
    }
}
