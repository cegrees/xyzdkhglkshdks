﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Landdb.Client.Infrastructure;
using ThinkGeo.MapSuite.WpfDesktopEdition;
using ThinkGeo.MapSuite.Core;

namespace Landdb.ViewModel.Maps.ImageryOverlay {
    public interface IMapOverlay {
        string Name { get; }
        MapInfoSelectorItem MapInfoSelectorItem { get; set; }
        Overlay Overlay { get; set; }
        GeographyUnit GeographyUnit { get; set; }
        RectangleShape Extent { get; set; }
        string ProjectionString { get; set; }
        bool Visibility { get; set; }
        bool OnlineOnly { get; set; }
        //string LoadAttribution(int zoomLevelsNumber);
        ZoomLevelSet GetZoomLevelSet();
        Overlay NewOverlay();
        Overlay NewOnlineOverlay();
        Overlay ExtentHasChanged(int tileHeight, int tileWidth, RectangleShape extent, RectangleShape projextent, PointShape wgscenterpoint, int zoomlevelnumber, int epsgcode);

        }
    }