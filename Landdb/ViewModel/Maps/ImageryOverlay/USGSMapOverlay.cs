﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ThinkGeo.MapSuite.WpfDesktopEdition;
using ThinkGeo.MapSuite.Core;
using System.Diagnostics;
using Landdb.Client.Infrastructure;

namespace Landdb.ViewModel.Maps.ImageryOverlay {
    public class USGSMapOverlay : IMapOverlay {
        private Overlay overlay;
        GeographyUnit geographyunit;
        RectangleShape extent;
        string projectionstring;
        bool visibility;
        bool onlineOnly;

        public USGSMapOverlay() {
            SetMapInfoSelectorItemToUSGS();
            extent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
            geographyunit = GeographyUnit.Meter;
            projectionstring = Proj4Projection.GetGoogleMapParametersString();
            visibility = true;
            onlineOnly = true;
            overlay = NewOverlay();
        }

        public USGSMapOverlay(bool? allowofflinemap) {
            SetMapInfoSelectorItemToUSGS();
            extent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
            geographyunit = GeographyUnit.Meter;
            projectionstring = Proj4Projection.GetGoogleMapParametersString();
            visibility = true;
            onlineOnly = true;
            overlay = NewOverlay();
        }

        public string Name => MapInfoSelectorItem.MapTypeKeyText;
        public MapInfoSelectorItem MapInfoSelectorItem { get; set; }

        private void SetMapInfoSelectorItemToUSGS() {
            MapInfoSelectorItem = new MapInfoSelectorItem(MapStatusType.USGS);
        }

        public Overlay Overlay {
            get { return overlay; }
            set { overlay = value; }
        }

        public GeographyUnit GeographyUnit {
            get { return geographyunit; }
            set { geographyunit = value; }
        }

        public RectangleShape Extent {
            get { return extent; }
            set { extent = value; }
        }

        public string ProjectionString {
            get { return projectionstring; }
            set { projectionstring = value; }
        }

        public bool Visibility {
            get { return visibility; }
            set { visibility = value; }
        }

        public bool OnlineOnly {
            get { return onlineOnly; }
            set { onlineOnly = value; }
        }

        public Overlay NewOverlay() {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            SetMapInfoSelectorItemToUSGS();
            extent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
            geographyunit = GeographyUnit.Meter;
            projectionstring = Proj4Projection.GetGoogleMapParametersString();
            visibility = true;
            onlineOnly = true;

            WmsRasterLayer wmsImageLayer = new WmsRasterLayer(new Uri(@"http://raster.nationalmap.gov/arcgis/services/Orthoimagery/USGS_EDC_Ortho_NAIP/ImageServer/WMSServer"));
            wmsImageLayer.UpperThreshold = double.MaxValue;
            wmsImageLayer.LowerThreshold = 0;
            wmsImageLayer.Open();
            wmsImageLayer.ActiveLayerNames.Add("0");
            wmsImageLayer.Exceptions = "application/vnd.ogc.se_xml";
            wmsImageLayer.Parameters["REQUEST"] = "GetMap";
            wmsImageLayer.Parameters["FORMAT"] = "image/png";
            wmsImageLayer.Parameters["STYLES"] = "default";
            wmsImageLayer.Parameters["SRS"] = "EPSG:102113";
            wmsImageLayer.Parameters.Add("VERSION", "1.1.1");
            wmsImageLayer.Close();
            LayerOverlay layerWMSOverlay = new LayerOverlay();
            layerWMSOverlay.Layers.Add("wmsImageLayer", wmsImageLayer);
            layerWMSOverlay.TileCache = new InMemoryBitmapTileCache();
            string wmsDirectory = System.IO.Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "USGSCache");
            FileBitmapTileCache usgsTileCache = new FileBitmapTileCache();
            usgsTileCache.CacheDirectory = wmsDirectory;
            usgsTileCache.CacheId = "usgsCachedTiles";
            layerWMSOverlay.TileCache = usgsTileCache;
            layerWMSOverlay.IsVisible = visibility;
            layerWMSOverlay.DrawingExceptionMode = DrawingExceptionMode.DrawException;
            overlay = layerWMSOverlay;
            sw.Stop();
            Console.WriteLine(sw.ElapsedMilliseconds);
            return layerWMSOverlay;
        }

        public Overlay NewOnlineOverlay() {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            SetMapInfoSelectorItemToUSGS();
            extent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
            geographyunit = GeographyUnit.Meter;
            projectionstring = Proj4Projection.GetGoogleMapParametersString();
            visibility = true;
            onlineOnly = true;

            WmsRasterLayer wmsImageLayer = new WmsRasterLayer(new Uri(@"http://raster.nationalmap.gov/arcgis/services/Orthoimagery/USGS_EDC_Ortho_NAIP/ImageServer/WMSServer"));
            wmsImageLayer.UpperThreshold = double.MaxValue;
            wmsImageLayer.LowerThreshold = 0;
            wmsImageLayer.Open();
            wmsImageLayer.ActiveLayerNames.Add("0");
            wmsImageLayer.Exceptions = "application/vnd.ogc.se_xml";
            wmsImageLayer.Parameters["REQUEST"] = "GetMap";
            wmsImageLayer.Parameters["FORMAT"] = "image/png";
            wmsImageLayer.Parameters["STYLES"] = "default";
            wmsImageLayer.Parameters["SRS"] = "EPSG:102113";
            wmsImageLayer.Parameters.Add("VERSION", "1.1.1");
            wmsImageLayer.Close();
            LayerOverlay layerWMSOverlay = new LayerOverlay();
            layerWMSOverlay.Layers.Add("wmsImageLayer", wmsImageLayer);
            layerWMSOverlay.TileCache = new InMemoryBitmapTileCache();
            string wmsDirectory = System.IO.Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "USGSCache");
            FileBitmapTileCache usgsTileCache = new FileBitmapTileCache();
            usgsTileCache.CacheDirectory = wmsDirectory;
            usgsTileCache.CacheId = "usgsCachedTiles";
            layerWMSOverlay.TileCache = usgsTileCache;
            layerWMSOverlay.IsVisible = visibility;
            layerWMSOverlay.DrawingExceptionMode = DrawingExceptionMode.DrawException;
            overlay = layerWMSOverlay;
            sw.Stop();
            Console.WriteLine(sw.ElapsedMilliseconds);
            return layerWMSOverlay;
        }
        
        public ZoomLevelSet GetZoomLevelSet() {
            //ZoomLevelFactory zlf = new ZoomLevelFactory();
            //return zlf.GetGoogleMapsZoomLevelSet();
            return null;
        }
        public Overlay ExtentHasChanged(int tileHeight, int tileWidth, RectangleShape extent, RectangleShape projextent, PointShape wgscenterpoint, int zoomlevelnumber, int epsgcode) {
            return null;
        }
    }
}
