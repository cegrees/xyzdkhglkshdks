﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ThinkGeo.MapSuite.WpfDesktopEdition;
using ThinkGeo.MapSuite.Core;
using System.Diagnostics;
using Landdb.Client.Infrastructure;

namespace Landdb.ViewModel.Maps.ImageryOverlay {
    public class WorldMapKitOverlay : IMapOverlay {
        private Overlay overlay;
        GeographyUnit geographyunit;
        RectangleShape extent;
        string projectionstring;
        bool visibility;
        bool onlineOnly;

        public WorldMapKitOverlay() {
            SetMapInfoSelectorItemToWorldMapKit();
            extent = new RectangleShape(-126.4, 48.8, -67.0, 19.0);
            geographyunit = GeographyUnit.DecimalDegree;
            projectionstring = Proj4Projection.GetEpsgParametersString(4326);
            visibility = true;
            onlineOnly = false;
            overlay = NewOverlay();
        }

        public WorldMapKitOverlay(bool? allowofflinemap) {
            SetMapInfoSelectorItemToWorldMapKit();
            extent = new RectangleShape(-126.4, 48.8, -67.0, 19.0);
            geographyunit = GeographyUnit.DecimalDegree;
            projectionstring = Proj4Projection.GetEpsgParametersString(4326);
            visibility = true;
            onlineOnly = false;
            overlay = NewOverlay();
        }

        public string Name => MapInfoSelectorItem.MapTypeKeyText;
        public MapInfoSelectorItem MapInfoSelectorItem { get; set; }

        private void SetMapInfoSelectorItemToWorldMapKit() {
            MapInfoSelectorItem = new MapInfoSelectorItem(MapStatusType.WorldMapKit);
        }

        public Overlay Overlay {
            get { return overlay; }
            set { overlay = value; }
        }

        public GeographyUnit GeographyUnit {
            get { return geographyunit; }
            set { geographyunit = value; }
        }

        public RectangleShape Extent {
            get { return extent; }
            set { extent = value; }
        }

        public string ProjectionString {
            get { return projectionstring; }
            set { projectionstring = value; }
        }

        public bool Visibility {
            get { return visibility; }
            set { visibility = value; }
        }

        public bool OnlineOnly {
            get { return onlineOnly; }
            set { onlineOnly = value; }
        }

        public Overlay NewOverlay() {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            SetMapInfoSelectorItemToWorldMapKit();
            extent = new RectangleShape(-126.4, 48.8, -67.0, 19.0);
            geographyunit = GeographyUnit.DecimalDegree;
            projectionstring = Proj4Projection.GetEpsgParametersString(4326);
            visibility = true;
            onlineOnly = false;

            WorldMapKitWmsWpfOverlay worldoverlay = new WorldMapKitWmsWpfOverlay();
            worldoverlay.IsVisible = visibility;
            worldoverlay.DrawingExceptionMode = DrawingExceptionMode.DrawException;
            overlay = worldoverlay;
            sw.Stop();
            Console.WriteLine(sw.ElapsedMilliseconds);
            return worldoverlay;
        }

        public Overlay NewOnlineOverlay() {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            SetMapInfoSelectorItemToWorldMapKit();
            extent = new RectangleShape(-126.4, 48.8, -67.0, 19.0);
            geographyunit = GeographyUnit.DecimalDegree;
            projectionstring = Proj4Projection.GetEpsgParametersString(4326);
            visibility = true;
            onlineOnly = false;

            WorldMapKitWmsWpfOverlay worldoverlay = new WorldMapKitWmsWpfOverlay();
            worldoverlay.IsVisible = visibility;
            worldoverlay.DrawingExceptionMode = DrawingExceptionMode.DrawException;
            overlay = worldoverlay;
            sw.Stop();
            Console.WriteLine(sw.ElapsedMilliseconds);
            return worldoverlay;
        }
        //public string LoadAttribution(int zoomLevelsNumber) {
        //    return string.Empty;
        //}

        public ZoomLevelSet GetZoomLevelSet() {
            //ZoomLevelFactory zlf = new ZoomLevelFactory();
            //return zlf.GetGoogleMapsZoomLevelSet();
            return null;
        }
        public Overlay ExtentHasChanged(int tileHeight, int tileWidth, RectangleShape extent, RectangleShape projextent, PointShape wgscenterpoint, int zoomlevelnumber, int epsgcode) {
            return null;
        }
    }
}
