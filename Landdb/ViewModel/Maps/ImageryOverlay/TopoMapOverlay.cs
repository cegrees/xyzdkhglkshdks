﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ThinkGeo.MapSuite.WpfDesktopEdition;
using ThinkGeo.MapSuite.Core;
using System.Diagnostics;
using Landdb.Client.Infrastructure;

namespace Landdb.ViewModel.Maps.ImageryOverlay {
    public class TopoMapOverlay : IMapOverlay {
        private Overlay overlay;
        GeographyUnit geographyunit;
        RectangleShape extent;
        string projectionstring;
        bool visibility;
        bool onlineOnly;

        public TopoMapOverlay() {
            SetMapInfoSelectorItemToTopo();
            extent = new RectangleShape(-126.4, 48.8, -67.0, 19.0);
            geographyunit = GeographyUnit.DecimalDegree;
            projectionstring = Proj4Projection.GetEpsgParametersString(4326);
            //extent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
            //geographyunit = GeographyUnit.Meter;
            //projectionstring = Proj4Projection.GetGoogleMapParametersString();
            visibility = true;
            onlineOnly = true;
            overlay = NewOverlay();
        }

        public TopoMapOverlay(bool? allowofflinemap) {
            SetMapInfoSelectorItemToTopo();
            extent = new RectangleShape(-126.4, 48.8, -67.0, 19.0);
            geographyunit = GeographyUnit.DecimalDegree;
            projectionstring = Proj4Projection.GetEpsgParametersString(4326);
            //extent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
            //geographyunit = GeographyUnit.Meter;
            //projectionstring = Proj4Projection.GetGoogleMapParametersString();
            visibility = true;
            onlineOnly = true;
            overlay = NewOverlay();
        }

        public string Name => MapInfoSelectorItem.MapTypeKeyText;
        public MapInfoSelectorItem MapInfoSelectorItem { get; set; }

        void SetMapInfoSelectorItemToTopo() {
            MapInfoSelectorItem = new MapInfoSelectorItem(MapStatusType.Topo);
        }

        public Overlay Overlay {
            get { return overlay; }
            set { overlay = value; }
        }

        public GeographyUnit GeographyUnit {
            get { return geographyunit; }
            set { geographyunit = value; }
        }

        public RectangleShape Extent {
            get { return extent; }
            set { extent = value; }
        }

        public string ProjectionString {
            get { return projectionstring; }
            set { projectionstring = value; }
        }

        public bool Visibility {
            get { return visibility; }
            set { visibility = value; }
        }

        public bool OnlineOnly {
            get { return onlineOnly; }
            set { onlineOnly = value; }
        }

        public Overlay NewOverlay() {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            SetMapInfoSelectorItemToTopo();
            extent = new RectangleShape(-126.4, 48.8, -67.0, 19.0);
            geographyunit = GeographyUnit.DecimalDegree;
            projectionstring = Proj4Projection.GetEpsgParametersString(4326);
            visibility = true;
            onlineOnly = true;

            WmsRasterLayer wmsImageLayer = new WmsRasterLayer(new Uri("http://basemap.nationalmap.gov/arcgis/services/USGSTopo/MapServer/WmsServer?"));
            wmsImageLayer.UpperThreshold = double.MaxValue;
            wmsImageLayer.LowerThreshold = 0;
            wmsImageLayer.Open();
            System.Collections.ObjectModel.Collection<string> layernames = wmsImageLayer.GetServerLayerNames();

            foreach (string layerName in layernames) {
                wmsImageLayer.ActiveLayerNames.Add(layerName);
            }
            wmsImageLayer.Exceptions = "application/vnd.ogc.se_xml";
            wmsImageLayer.KeyColors.Add(new GeoColor(255, 255, 255, 255));
            wmsImageLayer.DrawingExceptionMode = DrawingExceptionMode.DrawException;
            wmsImageLayer.Close();
            LayerOverlay layerWMSOverlay = new LayerOverlay();
            layerWMSOverlay.Layers.Add("wmsImageLayer", wmsImageLayer);
            //layerWMSOverlay.TileCache = new InMemoryBitmapTileCache();
            //string wmsDirectory = System.IO.Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "TOPOCache");
            //FileBitmapTileCache topoTileCache = new FileBitmapTileCache();
            //topoTileCache.CacheDirectory = wmsDirectory;
            //topoTileCache.CacheId = "topoCachedTiles";
            //layerWMSOverlay.TileCache = topoTileCache;
            layerWMSOverlay.IsVisible = visibility;
            overlay = layerWMSOverlay;
            sw.Stop();
            Console.WriteLine(sw.ElapsedMilliseconds);
            return layerWMSOverlay;
        }

        public Overlay NewOnlineOverlay() {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            SetMapInfoSelectorItemToTopo();
            extent = new RectangleShape(-126.4, 48.8, -67.0, 19.0);
            geographyunit = GeographyUnit.DecimalDegree;
            projectionstring = Proj4Projection.GetEpsgParametersString(4326);
            visibility = true;
            onlineOnly = true;

            WmsRasterLayer wmsImageLayer = new WmsRasterLayer(new Uri("http://basemap.nationalmap.gov/arcgis/services/USGSTopo/MapServer/WmsServer?"));
            wmsImageLayer.UpperThreshold = double.MaxValue;
            wmsImageLayer.LowerThreshold = 0;
            wmsImageLayer.Open();
            System.Collections.ObjectModel.Collection<string> layernames = wmsImageLayer.GetServerLayerNames();

            foreach (string layerName in layernames) {
                wmsImageLayer.ActiveLayerNames.Add(layerName);
            }
            wmsImageLayer.Exceptions = "application/vnd.ogc.se_xml";
            wmsImageLayer.KeyColors.Add(new GeoColor(255, 255, 255, 255));
            wmsImageLayer.DrawingExceptionMode = DrawingExceptionMode.DrawException;
            wmsImageLayer.Close();
            LayerOverlay layerWMSOverlay = new LayerOverlay();
            layerWMSOverlay.Layers.Add("wmsImageLayer", wmsImageLayer);
            //layerWMSOverlay.TileCache = new InMemoryBitmapTileCache();
            //string wmsDirectory = System.IO.Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "TOPOCache");
            //FileBitmapTileCache topoTileCache = new FileBitmapTileCache();
            //topoTileCache.CacheDirectory = wmsDirectory;
            //topoTileCache.CacheId = "topoCachedTiles";
            //layerWMSOverlay.TileCache = topoTileCache;
            layerWMSOverlay.IsVisible = visibility;
            overlay = layerWMSOverlay;
            sw.Stop();
            Console.WriteLine(sw.ElapsedMilliseconds);
            return layerWMSOverlay;
        }
        //public string LoadAttribution(int zoomLevelsNumber) {
        //    return string.Empty;
        //}

        public ZoomLevelSet GetZoomLevelSet() {
            //ZoomLevelFactory zlf = new ZoomLevelFactory();
            //return zlf.GetGoogleMapsZoomLevelSet();
            return null;
        }
        public Overlay ExtentHasChanged(int tileHeight, int tileWidth, RectangleShape extent, RectangleShape projextent, PointShape wgscenterpoint, int zoomlevelnumber, int epsgcode) {
            return null;
        }
    }
}
