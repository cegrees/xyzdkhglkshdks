﻿using Landdb.Infrastructure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using ThinkGeo.MapSuite.Core;

namespace Landdb.ViewModel.Maps.ImageryOverlay {
    public class GoogleMapLayerControl : Layer {
        private const double qj4 = 0.010416666666666666;
        private const double qz4 = 85.0;
        private const int rD4 = 256;
        private const double rT4 = 4374754.0;
        private const double rj4 = 0.0254;
        private static readonly RectangleShape lio = new RectangleShape(-20037508.352, 20037508.352, 20037508.352, -20037508.352);
        private static System.Collections.ObjectModel.Collection<ZoomLevel> rz4;
        private int tileHeight = 512;
        private int tileWidth = 512;
        private string clientId;

        [System.NonSerialized]
        private System.Exception sD4;

        [System.NonSerialized]
        private System.Collections.ObjectModel.Collection<zD4> sT4;

        [System.NonSerialized]
        private System.Collections.Generic.Dictionary<int, Bitmap> sj4;

        private GoogleMapsMapType mapType;
        private Bitmap noDataTileImage;
        private GoogleMapsPictureFormat pictureFormat;
        private string privateKey;
        private string apiKey;
        private GoogleMapsTileMode tileMode;
        private int timeoutInSeconds;
        private int zoomLevelsNumber;
        private WebProxy webProxy;
        private System.TimeSpan expiration;
        private double maxCacheSizeInMegabytes;
        private Projection projectionFromSphericalMercator;
        private ManagedProj4Projection displayWgs84Projection;
        private ManagedProj4Projection googleWgs84Projection;
        private BitmapTileCache tileCache;
        private BitmapTileCache projectedTileCache;
        private GoogleMapsLayerImageCache requestedTileCache;
        private string cacheDirectory;
        private bool enableDefaultTileCache;
        private bool enableDefaultProjectedTileCache;
        private double currentSphereResolution;
        private double currentRequestSphereResolution;
        private bool requestOneTile;
        System.IO.Stream currentstream = null;

        [System.NonSerialized]
        private System.EventHandler<CreatingRequestGoogleMapsLayerEventArgs> sz4;

        [System.NonSerialized]
        private System.EventHandler<SendingWebRequestEventArgs> mCo;

        [System.NonSerialized]
        private System.EventHandler<SentWebRequestEventArgs> mSo;


        public event System.EventHandler<CreatingRequestGoogleMapsLayerEventArgs> CreatingRequest {
            add {
                System.EventHandler<CreatingRequestGoogleMapsLayerEventArgs> eventHandler = this.sz4;
                System.EventHandler<CreatingRequestGoogleMapsLayerEventArgs> eventHandler2;
                do {
                    eventHandler2 = eventHandler;
                    System.EventHandler<CreatingRequestGoogleMapsLayerEventArgs> value2 = (System.EventHandler<CreatingRequestGoogleMapsLayerEventArgs>)System.Delegate.Combine(eventHandler2, value);
                    eventHandler = System.Threading.Interlocked.CompareExchange<System.EventHandler<CreatingRequestGoogleMapsLayerEventArgs>>(ref this.sz4, value2, eventHandler2);
                }
                while (eventHandler != eventHandler2);
            }
            remove {
                System.EventHandler<CreatingRequestGoogleMapsLayerEventArgs> eventHandler = this.sz4;
                System.EventHandler<CreatingRequestGoogleMapsLayerEventArgs> eventHandler2;
                do {
                    eventHandler2 = eventHandler;
                    System.EventHandler<CreatingRequestGoogleMapsLayerEventArgs> value2 = (System.EventHandler<CreatingRequestGoogleMapsLayerEventArgs>)System.Delegate.Remove(eventHandler2, value);
                    eventHandler = System.Threading.Interlocked.CompareExchange<System.EventHandler<CreatingRequestGoogleMapsLayerEventArgs>>(ref this.sz4, value2, eventHandler2);
                }
                while (eventHandler != eventHandler2);
            }
        }

        public event System.EventHandler<SendingWebRequestEventArgs> SendingWebRequest {
            add {
                System.EventHandler<SendingWebRequestEventArgs> eventHandler = this.mCo;
                System.EventHandler<SendingWebRequestEventArgs> eventHandler2;
                do {
                    eventHandler2 = eventHandler;
                    System.EventHandler<SendingWebRequestEventArgs> value2 = (System.EventHandler<SendingWebRequestEventArgs>)System.Delegate.Combine(eventHandler2, value);
                    eventHandler = System.Threading.Interlocked.CompareExchange<System.EventHandler<SendingWebRequestEventArgs>>(ref this.mCo, value2, eventHandler2);
                }
                while (eventHandler != eventHandler2);
            }
            remove {
                System.EventHandler<SendingWebRequestEventArgs> eventHandler = this.mCo;
                System.EventHandler<SendingWebRequestEventArgs> eventHandler2;
                do {
                    eventHandler2 = eventHandler;
                    System.EventHandler<SendingWebRequestEventArgs> value2 = (System.EventHandler<SendingWebRequestEventArgs>)System.Delegate.Remove(eventHandler2, value);
                    eventHandler = System.Threading.Interlocked.CompareExchange<System.EventHandler<SendingWebRequestEventArgs>>(ref this.mCo, value2, eventHandler2);
                }
                while (eventHandler != eventHandler2);
            }
        }

        public event System.EventHandler<SentWebRequestEventArgs> SentWebRequest {
            add {
                System.EventHandler<SentWebRequestEventArgs> eventHandler = this.mSo;
                System.EventHandler<SentWebRequestEventArgs> eventHandler2;
                do {
                    eventHandler2 = eventHandler;
                    System.EventHandler<SentWebRequestEventArgs> value2 = (System.EventHandler<SentWebRequestEventArgs>)System.Delegate.Combine(eventHandler2, value);
                    eventHandler = System.Threading.Interlocked.CompareExchange<System.EventHandler<SentWebRequestEventArgs>>(ref this.mSo, value2, eventHandler2);
                }
                while (eventHandler != eventHandler2);
            }
            remove {
                System.EventHandler<SentWebRequestEventArgs> eventHandler = this.mSo;
                System.EventHandler<SentWebRequestEventArgs> eventHandler2;
                do {
                    eventHandler2 = eventHandler;
                    System.EventHandler<SentWebRequestEventArgs> value2 = (System.EventHandler<SentWebRequestEventArgs>)System.Delegate.Remove(eventHandler2, value);
                    eventHandler = System.Threading.Interlocked.CompareExchange<System.EventHandler<SentWebRequestEventArgs>>(ref this.mSo, value2, eventHandler2);
                }
                while (eventHandler != eventHandler2);
            }
        }

        public GoogleMapsTileMode TileMode {
            get {
                return this.tileMode;
            }
            set {
                this.tileMode = value;
            }
        }

        public int TileWidth {
            get {
                return this.tileWidth;
            }
            set {
                this.tileWidth = value;
            }
        }

        public int TileHeight {
            get {
                return this.tileHeight;
            }
            set {
                this.tileHeight = value;
            }
        }

        public GoogleMapsPictureFormat PictureFormat {
            get {
                return this.pictureFormat;
            }
            set {
                this.pictureFormat = value;
            }
        }

        public GoogleMapsMapType MapType {
            get {
                return this.mapType;
            }
            set {
                this.mapType = value;
            }
        }

        public int TimeoutInSeconds {
            get {
                return this.timeoutInSeconds;
            }
            set {
                this.timeoutInSeconds = value;
            }
        }

        public string ClientId {
            get {
                return this.clientId;
            }
            set {
                this.clientId = value;
            }
        }

        public string PrivateKey {
            get {
                return this.privateKey;
            }
            set {
                this.privateKey = value;
            }
        }

        public string ApiKey {
            get {
                return this.apiKey;
            }
            set {
                this.apiKey = value;
            }
        }

        public BitmapTileCache TileCache {
            get {
                return this.tileCache;
            }
            set {
                this.tileCache = value;
                this.enableDefaultTileCache = false;
            }
        }

        public int ZoomLevelsNumber {
            get {
                return this.zoomLevelsNumber;
            }
            set {
                this.zoomLevelsNumber = value;
            }
        }
        public Projection ProjectionFromSphericalMercator {
            get {
                return this.projectionFromSphericalMercator;
            }
            set {
                this.projectionFromSphericalMercator = value;
                if (this.displayWgs84Projection == null) {
                    this.displayWgs84Projection = new ManagedProj4Projection();
                    this.displayWgs84Projection.ExternalProjectionParametersString = ManagedProj4Projection.GetWgs84ParametersString();
                    this.displayWgs84Projection.InternalProjectionParametersString = ManagedProj4Projection.GetWgs84ParametersString();
                }
                if (this.projectionFromSphericalMercator is Proj4Projection) {
                    this.displayWgs84Projection.InternalProjectionParametersString = ((Proj4Projection)this.projectionFromSphericalMercator).InternalProjectionParametersString;
                }
                else if (this.projectionFromSphericalMercator is ManagedProj4Projection) {
                    this.displayWgs84Projection.InternalProjectionParametersString = ((ManagedProj4Projection)this.projectionFromSphericalMercator).InternalProjectionParametersString;
                }
                else if (this.projectionFromSphericalMercator is UnmanagedProj4Projection) {
                    this.displayWgs84Projection.InternalProjectionParametersString = ((UnmanagedProj4Projection)this.projectionFromSphericalMercator).InternalProjectionParametersString;
                }
                //pj0.JhQ(this.displayWgs84Projection.ExternalProjectionParametersString, "ProjectionToSphericalMercator.ExternalProjectionParametersString");
                //pj0.JhQ(this.displayWgs84Projection.InternalProjectionParametersString, "ProjectionToSphericalMercator.InternalProjectionParametersString");
                this.displayWgs84Projection.Open();
            }
        }


        public BitmapTileCache ProjectedTileCache {
            get {
                return this.projectedTileCache;
            }
            set {
                this.projectedTileCache = value;
                this.enableDefaultProjectedTileCache = false;
            }
        }

        public System.TimeSpan TileExpiration {
            get {
                return this.expiration;
            }
            set {
                this.expiration = value;
            }
        }

        public double MaxCacheSizeInMegabytes {
            get {
                return this.maxCacheSizeInMegabytes;
            }
            set {
                this.maxCacheSizeInMegabytes = value;
            }
        }

        public Bitmap NoDataTileImage {
            get {
                if (this.noDataTileImage == null) {
                    //this.noDataTileImage = hU8(XUQ.fUQ, this.tileWidth, this.tileHeight);
                }
                return (Bitmap)this.noDataTileImage.Clone();
            }
            set {
                this.noDataTileImage = hU8(value, this.tileWidth, this.tileHeight);
            }
        }

        public WebProxy WebProxy {
            get {
                return this.webProxy;
            }
            set {
                this.webProxy = value;
            }
        }

        public bool RequestOneTile {
            get {
                return this.requestOneTile;
            }
            set {
                this.requestOneTile = value;
            }
        }

        protected virtual System.Collections.Generic.Dictionary<int, Bitmap> BufferImages {
            get {
                return this.sj4;
            }
        }

        public GoogleMapLayerControl() : this(null, string.Empty, string.Empty, new WebProxy()) {
        }

        public GoogleMapLayerControl(string cacheDirectory, string clientId, string privateKey) : this(cacheDirectory, clientId, privateKey, null) {
        }

        public GoogleMapLayerControl(string cacheDirectory, string clientId, string privateKey, WebProxy webProxy) {
            this.clientId = clientId;
            this.privateKey = privateKey;
            this.googleWgs84Projection = new ManagedProj4Projection();
            this.googleWgs84Projection.InternalProjectionParametersString = ManagedProj4Projection.GetGoogleMapParametersString();
            this.googleWgs84Projection.ExternalProjectionParametersString = ManagedProj4Projection.GetEpsgParametersString(4326);
            if (webProxy != null && webProxy.Address != null && webProxy.Address != new Uri("")) {
                this.webProxy = webProxy;
            }
            this.timeoutInSeconds = 20;
            if (!string.IsNullOrEmpty(cacheDirectory)) {
                this.TileCache = new EncryptedFileBitmapTileCache(cacheDirectory);
            }
            this.enableDefaultTileCache = true;
            this.enableDefaultProjectedTileCache = true;
        }

        protected WebResponse SendWebRequest(WebRequest webRequest) {
            SendingWebRequestEventArgs sendingWebRequestEventArgs = new SendingWebRequestEventArgs(webRequest);
            this.OnSendingWebRequest(sendingWebRequestEventArgs);
            WebResponse response = this.SendWebRequestCore(sendingWebRequestEventArgs.WebRequest);
            SentWebRequestEventArgs sentWebRequestEventArgs = new SentWebRequestEventArgs(response);
            this.OnSentWebRequest(sentWebRequestEventArgs);
            return sentWebRequestEventArgs.Response;
        }

        protected virtual WebResponse SendWebRequestCore(WebRequest webRequest) {
            return webRequest.GetResponse();
        }

        public Bitmap GetGoogleMapsImage(double longitude, double latitude, int zoomLevelNumber, double tileWidth, double tileHeight) {
            return this.GetGoogleMapsImageCore(longitude, latitude, zoomLevelNumber, tileWidth, tileHeight);
        }

        protected virtual Bitmap GetGoogleMapsImageCore(double longitude, double latitude, int zoomLevelNumber, double tileWidth, double tileHeight) {
            string uriString = this.vD4(longitude, latitude, zoomLevelNumber, tileWidth, tileHeight);
            System.IO.Stream stream = null;
            currentstream = null;
            Bitmap bitmap = null;
            try {
                CreatingRequestGoogleMapsLayerEventArgs creatingRequestGoogleMapsLayerEventArgs = new CreatingRequestGoogleMapsLayerEventArgs(new Uri(uriString));
                this.OnCreatingRequest(creatingRequestGoogleMapsLayerEventArgs);
                WebRequest webRequest = WebRequest.Create(creatingRequestGoogleMapsLayerEventArgs.RequestUri);
                webRequest.Timeout = this.timeoutInSeconds * 1000;
                webRequest.Proxy = this.webProxy;
                WebResponse webResponse = this.SendWebRequest(webRequest);
                currentstream = webResponse.GetResponseStream();
                bitmap = new Bitmap(currentstream);
            }
            catch (System.Exception ex) {
                if (this.sD4 == null) {
                    this.sD4 = ex;
                }
            }
            finally {
                if (bitmap == null) {
                    bitmap = new Bitmap(1, 1);
                }
                if (stream != null) {
                    stream.Dispose();
                }
            }
            return bitmap;
        }


        protected virtual void OnCreatingRequest(CreatingRequestGoogleMapsLayerEventArgs e) {
            System.EventHandler<CreatingRequestGoogleMapsLayerEventArgs> eventHandler = this.sz4;
            if (eventHandler != null) {
                eventHandler(this, e);
            }
        }

        protected virtual void OnSendingWebRequest(SendingWebRequestEventArgs e) {
            System.EventHandler<SendingWebRequestEventArgs> eventHandler = this.mCo;
            if (eventHandler != null) {
                eventHandler(this, e);
            }
        }

        protected virtual void OnSentWebRequest(SentWebRequestEventArgs e) {
            System.EventHandler<SentWebRequestEventArgs> eventHandler = this.mSo;
            if (eventHandler != null) {
                eventHandler(this, e);
            }
        }

        protected override void OpenCore() {
            this.googleWgs84Projection.Open();
            if (this.displayWgs84Projection != null) {
                this.displayWgs84Projection.Open();
            }
            if (this.projectionFromSphericalMercator != null) {
                this.projectionFromSphericalMercator.Open();
            }
            base.OpenCore();
        }

        protected override void CloseCore() {
            this.googleWgs84Projection.Close();
            if (this.displayWgs84Projection != null) {
                this.displayWgs84Projection.Open();
            }
            if (this.projectionFromSphericalMercator != null) {
                this.projectionFromSphericalMercator.Open();
            }
            base.CloseCore();
        }

        public virtual void Draw(GeoCanvas canvas, System.Collections.ObjectModel.Collection<SimpleCandidate> labelsInAllLayers) {
            try {
                //canvas.DrawingProgressChanged += new System.EventHandler<DrawingProgressChangedEventArgs>(this.tBU =);
                //Stopwatch stopwatch = Stopwatch.StartNew();
                if (this.IsVisible) {
                    System.Collections.ObjectModel.Collection<GeoColor> collection = new System.Collections.ObjectModel.Collection<GeoColor>();
                    if (canvas.HasKeyColor) {
                        foreach (GeoColor current in canvas.KeyColors) {
                            collection.Add(current);
                        }
                        canvas.KeyColors.Clear();
                    }
                    try {
                        //if (this.transparency != 255f || this.redTranslation != 0f || this.blueTranslation != 0f || this.greenTranslation != 0f || this.isGrayscale || this.isNegative) {
                        Bitmap bitmap = null;
                        GeoImage geoImage = null;
                        try {
                            bitmap = new Bitmap((int)canvas.Width, (int)canvas.Height, PixelFormat.Format32bppPArgb);
                            bitmap.MakeTransparent();
                            GdiPlusGeoCanvas gdiPlusGeoCanvas = new GdiPlusGeoCanvas();
                            //foreach (GeoColor current2 in this.keyColors) {
                            //    gdiPlusGeoCanvas.KeyColors.Add(current2);
                            //}
                            gdiPlusGeoCanvas.BeginDrawing(bitmap, canvas.CurrentWorldExtent, canvas.MapUnit);
                            this.sxU(gdiPlusGeoCanvas, this.Attribution);
                            //this.DrawCore(gdiPlusGeoCanvas, labelsInAllLayers);
                            DrawMyImage(gdiPlusGeoCanvas, labelsInAllLayers);
                            gdiPlusGeoCanvas.EndDrawing();
                            geoImage = this.rhU(bitmap);
                            canvas.DrawScreenImageWithoutScaling(geoImage, canvas.Width / 2f, canvas.Height / 2f, DrawingLevel.LevelOne, 0f, 0f, 0f);
                        }
                        finally {
                            if (bitmap != null) {
                                bitmap.Dispose();
                            }
                            if (geoImage != null) {
                                geoImage.Dispose();
                            }
                        }
                        //}
                        //if (this.keyColors != null) {
                        //    foreach (GeoColor current3 in this.keyColors) {
                        //        canvas.KeyColors.Add(current3);
                        //    }
                        //}
                        this.sxU(canvas, this.Attribution);
                        //this.DrawCore(canvas, labelsInAllLayers);
                        DrawMyImage(canvas, labelsInAllLayers);
                    }
                    finally {
                        if (canvas.HasKeyColor) {
                            canvas.KeyColors.Clear();
                            foreach (GeoColor current4 in collection) {
                                canvas.KeyColors.Add(current4);
                            }
                        }
                    }
                }
                //stopwatch.Stop();
                //this.drawingTime = stopwatch.Elapsed;
            }
            finally {
                // canvas.DrawingProgressChanged -= new System.EventHandler<DrawingProgressChangedEventArgs>(this.tBU);
            }

        }

        private GeoImage rhU(Bitmap sourceBitmap) {
            GeoImage result = null;
            if (sourceBitmap.Width != 0 && sourceBitmap.Height != 0) {
                Graphics graphics = null;
                Bitmap bitmap = null;
                System.IO.Stream stream = null;
                ImageAttributes imageAttributes = null;
                try {
                    imageAttributes = new ImageAttributes();
                    bitmap = new Bitmap(sourceBitmap.Width, sourceBitmap.Height, PixelFormat.Format32bppPArgb);
                    bitmap.MakeTransparent();
                    graphics = Graphics.FromImage(bitmap);
                    Rectangle destRect = new Rectangle(0, 0, sourceBitmap.Width, sourceBitmap.Height);
                    graphics.DrawImage(sourceBitmap, destRect, 0, 0, sourceBitmap.Width, sourceBitmap.Height, GraphicsUnit.Pixel, imageAttributes);
                    System.IO.Stream stream2 = new System.IO.MemoryStream();
                    bitmap.Save(stream2, ImageFormat.Png);
                    stream2.Seek(0L, System.IO.SeekOrigin.Begin);
                    result = new GeoImage(stream2);
                }
                finally {
                    if (graphics != null) {
                        graphics.Dispose();
                    }
                    if (stream != null) {
                        stream.Dispose();
                    }
                    if (imageAttributes != null) {
                        imageAttributes.Dispose();
                    }
                    if (bitmap != null) {
                        bitmap.Dispose();
                    }
                }
            }
            return result;
        }


        private void sxU(GeoCanvas canvas, string attribution) {
            if (string.IsNullOrEmpty(this.Attribution)) {
                return;
            }
            DrawingAttributionLayerEventArgs drawingAttributionLayerEventArgs = new DrawingAttributionLayerEventArgs(canvas, attribution);
            this.OnDrawingAttribution(drawingAttributionLayerEventArgs);
            attribution = drawingAttributionLayerEventArgs.Attribution;
            if (drawingAttributionLayerEventArgs.Cancel) {
                return;
            }
            this.DrawAttributionCore(canvas, attribution);
            DrawnAttributionLayerEventArgs args = new DrawnAttributionLayerEventArgs(canvas, attribution);
            this.OnDrawnAttribution(args);
        }
  



    protected override void DrawCore(GeoCanvas canvas, System.Collections.ObjectModel.Collection<SimpleCandidate> labelsInAllLayers) {
            string asfd = string.Empty;
            //if (this.projectionFromSphericalMercator == null) {
            //    string googleMapParametersString = ManagedProj4Projection.GetGoogleMapParametersString();
            //    string googleMapParametersString2 = ManagedProj4Projection.GetGoogleMapParametersString();
            //    this.projectionFromSphericalMercator = new ManagedProj4Projection(googleMapParametersString, googleMapParametersString2);
            //    this.projectionFromSphericalMercator.Open();
            //}
            //try {
            //    //if (this.requestOneTile) {
            //        double currentScale = ExtentHelper.GetScale(canvas.CurrentWorldExtent, canvas.Width, canvas.MapUnit);
            //        ZoomLevelsNumber = tD4(canvas);
            //        this.currentSphereResolution = tT4(this.zoomLevelsNumber, canvas.Dpi);
            //        this.currentRequestSphereResolution = tT4(this.zoomLevelsNumber, 96f);
            //        //this.Attribution = LoadAttribution(this.zoomLevelsNumber);
            //        PointShape centerPoint = canvas.CurrentWorldExtent.GetCenterPoint();
            //        PointShape pointShape = (PointShape)this.projectionFromSphericalMercator.ConvertToInternalProjection(centerPoint);
            //        PointShape centerInWgs = (PointShape)this.googleWgs84Projection.ConvertToExternalProjection(pointShape);
            //        int num = (int)canvas.Height;
            //        int num2 = (int)canvas.Width;
            //        double snappedScale = wT4(this.zoomLevelsNumber);
            //        RectangleShape rectangleShape = canvas.CurrentWorldExtent;
            //        Bitmap bm = GetGoogleMapsImage(centerInWgs.X, centerInWgs.Y, this.zoomLevelsNumber, (double)this.tileWidth, (double)this.tileHeight);
            //        string bitmapfilename = string.Format("{0}.bmp", DateTime.Now.Ticks.ToString());

            //        var fulldirectoryname = Path.Combine(ApplicationEnvironment.GoogleMapCacheDirectory, bitmapfilename);
            //        if (!Directory.Exists(ApplicationEnvironment.GoogleMapCacheDirectory)) {
            //            Directory.CreateDirectory(ApplicationEnvironment.GoogleMapCacheDirectory);
            //        }
            //        bm.Save(fulldirectoryname, ImageFormat.Bmp);
            //        GdiPlusGeoCanvas gdiPlusGeoCanvas = canvas as GdiPlusGeoCanvas;
            //        if (gdiPlusGeoCanvas != null) {
            //            gdiPlusGeoCanvas.DrawScreenImageWithoutScaling(bm, canvas.Width * 0.5f, canvas.Height * 0.5f, DrawingLevel.LevelOne, 0f, 0f, 0f);
            //            bm.Dispose();
            //        }
            //        else {
            //            using (GeoImage geoImage2 = new GeoImage(currentstream)) {
            //                canvas.DrawScreenImageWithoutScaling(geoImage2, canvas.Width * 0.5f, canvas.Height * 0.5f, DrawingLevel.LevelOne, 0f, 0f, 0f);
            //            }
            //            currentstream.Dispose();
            //        }
            //        this.requestOneTile = false;
            //    //}
            //}
            //catch (Exception ex) {
            //    int werwerwer = 0;
            //}
        }

        void DrawMyImage(GeoCanvas canvas, System.Collections.ObjectModel.Collection<SimpleCandidate> labelsInAllLayers) {
            if (this.projectionFromSphericalMercator == null) {
                string googleMapParametersString = ManagedProj4Projection.GetGoogleMapParametersString();
                string googleMapParametersString2 = ManagedProj4Projection.GetGoogleMapParametersString();
                this.projectionFromSphericalMercator = new ManagedProj4Projection(googleMapParametersString, googleMapParametersString2);
                this.projectionFromSphericalMercator.Open();
            }
            try {
                double currentScale = ExtentHelper.GetScale(canvas.CurrentWorldExtent, canvas.Width, canvas.MapUnit);
                ZoomLevelsNumber = tD4(canvas);
                this.currentSphereResolution = tT4(this.zoomLevelsNumber, canvas.Dpi);
                this.currentRequestSphereResolution = tT4(this.zoomLevelsNumber, 96f);
                PointShape centerPoint = canvas.CurrentWorldExtent.GetCenterPoint();
                PointShape pointShape = (PointShape)this.projectionFromSphericalMercator.ConvertToInternalProjection(centerPoint);
                PointShape centerInWgs = (PointShape)this.googleWgs84Projection.ConvertToExternalProjection(pointShape);
                int num = (int)canvas.Height;
                int num2 = (int)canvas.Width;
                double snappedScale = wT4(this.zoomLevelsNumber);
                RectangleShape rectangleShape = canvas.CurrentWorldExtent;
                Bitmap bm = GetGoogleMapsImage(centerInWgs.X, centerInWgs.Y, this.zoomLevelsNumber, (double)this.tileWidth, (double)this.tileHeight);
                string bitmapfilename = string.Format("{0}.bmp", DateTime.Now.Ticks.ToString());

                var fulldirectoryname = Path.Combine(ApplicationEnvironment.GoogleMapCacheDirectory, bitmapfilename);
                if (!Directory.Exists(ApplicationEnvironment.GoogleMapCacheDirectory)) {
                    Directory.CreateDirectory(ApplicationEnvironment.GoogleMapCacheDirectory);
                }
                bm.Save(fulldirectoryname, ImageFormat.Bmp);
                GdiPlusGeoCanvas gdiPlusGeoCanvas = canvas as GdiPlusGeoCanvas;
                if (gdiPlusGeoCanvas != null) {
                    gdiPlusGeoCanvas.DrawScreenImageWithoutScaling(bm, canvas.Width * 0.5f, canvas.Height * 0.5f, DrawingLevel.LevelOne, 0f, 0f, 0f);
                    bm.Dispose();
                }
                else {
                    using (GeoImage geoImage2 = new GeoImage(currentstream)) {
                        canvas.DrawScreenImageWithoutScaling(geoImage2, canvas.Width * 0.5f, canvas.Height * 0.5f, DrawingLevel.LevelOne, 0f, 0f, 0f);
                    }
                    currentstream.Dispose();
                }
                this.requestOneTile = false;
            }
            catch (Exception ex) {
                int werwerwer = 0;
            }
        }


        private static double nyo(RectangleShape worldExtent, float screenWidth, float screenHeight, GeographyUnit worldExtentUnit) {
            ZoomLevelSet zoomLevelSet = (worldExtentUnit == GeographyUnit.Meter) ? new SphericalMercatorZoomLevelSet() : new ZoomLevelSet();
            System.Collections.ObjectModel.Collection<double> collection = new System.Collections.ObjectModel.Collection<double>();
            foreach (double item in from z in zoomLevelSet.GetZoomLevels()
                                    select z.Scale) {
                collection.Add(item);
            }
            double scale = ExtentHelper.GetScale(worldExtent, screenWidth, worldExtentUnit);
            int index = SEQ(scale, collection, -1.7976931348623157E+308, 1.7976931348623157E+308);
            return collection[index];
        }

        internal static int SEQ(double scale, System.Collections.ObjectModel.Collection<double> zoomLevelScales, double minimumScale, double maximumScale) {
            if (scale < minimumScale) {
                scale = minimumScale;
            }
            else if (scale > maximumScale) {
                scale = maximumScale;
            }
            int num = -1;
            if (zoomLevelScales.Count > 0) {
                foreach (double num2 in zoomLevelScales) {
                    if (num2 < scale && System.Math.Abs(num2 - scale) >= 0.1) {
                        break;
                    }
                    num++;
                }
            }
            if (num >= zoomLevelScales.Count) {
                num = zoomLevelScales.Count - 1;
            }
            if (num != -1) {
                return num;
            }
            return 0;
        }

        private static int tD4(GeoCanvas canvas) {
            int num = Bj8((double)canvas.Width, canvas.CurrentWorldExtent, canvas.MapUnit, canvas.Dpi);
            if (num >= 20) {
                num = 19;
            }
            else if (num == 0) {
                num = 1;
            }
            return num;
        }

        internal static int Bj8(double newWidth, RectangleShape newTileExtent, GeographyUnit mapUnit, float dpi) {
            OpenStreetMapsZoomLevelSet openStreetMapsZoomLevelSet = new OpenStreetMapsZoomLevelSet();
            ZoomLevel zoomLevel = openStreetMapsZoomLevelSet.GetZoomLevel(newTileExtent, newWidth, mapUnit, dpi);
            System.Collections.ObjectModel.Collection<ZoomLevel> zoomLevels = openStreetMapsZoomLevelSet.GetZoomLevels();
            int result = 0;
            for (int i = 0; i < zoomLevels.Count; i++) {
                if (zoomLevel.Scale == zoomLevels[i].Scale) {
                    result = i;
                    break;
                }
            }
            return result;
        }

        private static double tT4(int zoomLevelNumber, float dotPerInch) {
            double num = wT4(zoomLevelNumber);
            return num * 0.0254000508 / (double)dotPerInch;
        }

        private RectangleShape tj4(GeoCanvas canvas) {
            RectangleShape result;
            if (this.projectionFromSphericalMercator != null) {
                result = this.projectionFromSphericalMercator.ConvertToInternalProjection(canvas.CurrentWorldExtent);
            }
            else {
                //7ys./is(canvas.MapUnit, "canvas.MapUnit");
                result = canvas.CurrentWorldExtent.GetIntersection(lio);
            }
            return result;
        }

        private void tz4(GeoCanvas canvas) {
            Bitmap bitmap = null;
            try {
                //7ys=.JhQ=(canvas, "canvas");
                if (this.projectionFromSphericalMercator == null) {
                    string googleMapParametersString = ManagedProj4Projection.GetGoogleMapParametersString();
                    string googleMapParametersString2 = ManagedProj4Projection.GetGoogleMapParametersString();
                    this.projectionFromSphericalMercator = new ManagedProj4Projection(googleMapParametersString, googleMapParametersString2);
                    this.projectionFromSphericalMercator.Open();
                }
                this.zoomLevelsNumber = tD4(canvas);
                this.currentSphereResolution = tT4(this.zoomLevelsNumber, canvas.Dpi);
                this.currentRequestSphereResolution = tT4(this.zoomLevelsNumber, 96f);
                PointShape centerPoint = canvas.CurrentWorldExtent.GetCenterPoint();
                PointShape pointShape = (PointShape)this.projectionFromSphericalMercator.ConvertToInternalProjection(centerPoint);
                PointShape centerInWgs = (PointShape)this.googleWgs84Projection.ConvertToExternalProjection(pointShape);
                int num = (int)canvas.Height;
                int num2 = (int)canvas.Width;
                double snappedScale = wT4(this.zoomLevelsNumber);
                RectangleShape rectangleShape = canvas.CurrentWorldExtent;
                if (this.projectionFromSphericalMercator != null) {
                    rectangleShape = this.ProjectionFromSphericalMercator.ConvertToInternalProjection(canvas.CurrentWorldExtent);
                    num2 = (int)System.Math.Round(rectangleShape.Width / this.currentRequestSphereResolution);
                    num = (int)System.Math.Round(rectangleShape.Height / this.currentRequestSphereResolution);
                }
                //bitmap = this.vz4(num, num2, snappedScale, rectangleShape);
                if (bitmap == null) {
                    bitmap = this.uD4(bitmap, centerInWgs, num, num2);
                    this.wD4(bitmap, pointShape, snappedScale);
                }
                int width = bitmap.Width;
                int height = bitmap.Height;
                RectangleShape rectangleShape2 = this.tj4(canvas);
                if (this.projectionFromSphericalMercator != null) {
                    System.IO.MemoryStream memoryStream = new System.IO.MemoryStream();
                    bitmap.Save(memoryStream, ImageFormat.Png);
                    //xj4(bitmap, "s_before");
                    bitmap.Dispose();
                    bitmap = null;
                    this.projectionFromSphericalMercator.ConvertToExternalProjection(rectangleShape2);
                    GeoImage image = new GeoImage(memoryStream);
                    RasterProjectionResult rasterProjectionResult = this.projectionFromSphericalMercator.ConvertToExternalProjection(image, rectangleShape2);
                    GdiPlusGeoCanvas canvas2 = new GdiPlusGeoCanvas();
                    System.IO.Stream imageStream = rasterProjectionResult.Image.GetImageStream(canvas2);
                    bitmap = new Bitmap(imageStream);
                    //xj4(bitmap, "s_after");
                }
                int x = 0;
                int num3 = 0;
                if (num2 > width || num > height) {
                    x = (num2 - bitmap.Width) / 2;
                    num3 = (num - bitmap.Height) / 2;
                }
                int num4 = (int)(canvas.CurrentWorldExtent.Width / canvas.CurrentWorldExtent.Height * (double)canvas.Height);
                double num5 = (double)(canvas.Width / (float)num4);
                Bitmap bitmap2 = null;
                if (this.projectionFromSphericalMercator != null) {
                    int x2 = (int)System.Math.Round((double)((canvas.Width - (float)num2) / 2f));
                    int y = (int)System.Math.Round((double)((canvas.Height - (float)num) / 2f));
                    bitmap2 = new Bitmap((int)canvas.Width, (int)canvas.Height);
                    using (Graphics graphics = Graphics.FromImage(bitmap2)) {
                        graphics.DrawImage(bitmap, new Rectangle(x2, y, num2, num), 0, 0, bitmap.Width, bitmap.Height, GraphicsUnit.Pixel);
                        goto IL_328;
                    }
                }
                bitmap2 = new Bitmap(num2, (int)((double)num * num5));
                using (Graphics graphics2 = Graphics.FromImage(bitmap2)) {
                    graphics2.DrawImage(bitmap, new Rectangle(x, 0, bitmap2.Width, bitmap2.Height), 0, (int)((double)num - (double)num * num5) / 2 - num3, bitmap2.Width, bitmap2.Height, GraphicsUnit.Pixel);
                }
                IL_328:
                System.IO.MemoryStream memoryStream2 = new System.IO.MemoryStream();
                bitmap2.Save(memoryStream2, ImageFormat.Png);
                xj4(bitmap2, "s_google");
                bitmap2.Dispose();
                memoryStream2.Seek(0L, System.IO.SeekOrigin.Begin);
                using (GeoImage geoImage = new GeoImage(memoryStream2)) {
                    canvas.DrawScreenImage(geoImage, canvas.Width * 0.5f, canvas.Height * 0.5f, canvas.Width, canvas.Height, DrawingLevel.LevelOne, 0f, 0f, 0f);
                }
            }
            finally {
                if (bitmap != null) {
                    bitmap.Dispose();
                }
            }
        }

        private Bitmap uD4(Bitmap image, PointShape centerInWgs84, int drawingScreenHeight, int drawingScreenWidth) {
            string uriString = this.vD4(centerInWgs84.X, centerInWgs84.Y, this.zoomLevelsNumber, (double)drawingScreenWidth, (double)drawingScreenHeight);
            System.IO.Stream stream = null;
            try {
                CreatingRequestGoogleMapsLayerEventArgs creatingRequestGoogleMapsLayerEventArgs = new CreatingRequestGoogleMapsLayerEventArgs(new Uri(uriString));
                this.OnCreatingRequest(creatingRequestGoogleMapsLayerEventArgs);
                WebRequest webRequest = WebRequest.Create(creatingRequestGoogleMapsLayerEventArgs.RequestUri);
                WebResponse webResponse = this.SendWebRequest(webRequest);
                stream = webResponse.GetResponseStream();
                image = new Bitmap(stream);
            }
            finally {
                if (stream != null) {
                    stream.Dispose();
                }
            }
            return image;
        }

        private void uT4(GeoCanvas canvas) {
            //7ys=.JhQ=(canvas, "canvas");
            if (this.projectedTileCache == null && this.projectionFromSphericalMercator != null && this.enableDefaultProjectedTileCache) {
                this.projectedTileCache = new EncryptedFileBitmapTileCache(AxY() + "\\GoogleMapsProjectedTileCache");
                this.projectedTileCache.TileMatrix.BoundingBoxUnit = canvas.MapUnit;
            }
            if (this.tileCache == null && this.enableDefaultTileCache) {
                this.tileCache = new EncryptedFileBitmapTileCache(AxY() + "\\GoogleMapsTileCache");
                this.tileCache.TileMatrix.BoundingBoxUnit = canvas.MapUnit;
            }
            if (this.projectedTileCache != null && this.projectedTileCache.TileMatrix.BoundingBoxUnit != canvas.MapUnit) {
                this.projectedTileCache.TileMatrix.BoundingBoxUnit = canvas.MapUnit;
            }
            double scale = ExtentHelper.GetScale(canvas.CurrentWorldExtent, canvas.Width, canvas.MapUnit);
            if (this.projectedTileCache != null) {
                this.projectedTileCache.TileMatrix.Scale = scale;
            }
            if (this.tileCache != null) {
                this.TileCache.TileMatrix.TileWidth = this.TileWidth;
                this.TileCache.TileMatrix.TileHeight = this.TileHeight;
                this.tileCache.TileMatrix.Scale = scale;
            }
            this.sj4 = new System.Collections.Generic.Dictionary<int, Bitmap>();
            this.sT4 = new System.Collections.ObjectModel.Collection<zD4>();
            this.zoomLevelsNumber = tD4(canvas);
            wT4(this.zoomLevelsNumber);
            this.currentSphereResolution = tT4(this.zoomLevelsNumber, canvas.Dpi);
            this.currentRequestSphereResolution = tT4(this.zoomLevelsNumber, 96f);
            RectangleShape rectangleShape = canvas.CurrentWorldExtent;
            RectangleShape rectangleShape2;
            if (this.projectionFromSphericalMercator != null) {
                if (this.projectedTileCache != null && this.projectedTileCache.TileMatrix != null) {
                    System.Collections.ObjectModel.Collection<TileMatrixCell> intersectingCells = this.projectedTileCache.TileMatrix.GetIntersectingCells(canvas.CurrentWorldExtent);
                    if (intersectingCells.Count != 0) {
                        rectangleShape = intersectingCells[0].BoundingBox;
                        for (int j = 1; j < intersectingCells.Count; j++) {
                            rectangleShape.ExpandToInclude(intersectingCells[j].BoundingBox);
                        }
                    }
                }
                rectangleShape2 = this.projectionFromSphericalMercator.ConvertToInternalProjection(rectangleShape);
            }
            else {
                rectangleShape2 = canvas.CurrentWorldExtent.GetIntersection(lio);
            }
            double num = canvas.CurrentWorldExtent.Width / (double)canvas.Width;
            double num2 = canvas.CurrentWorldExtent.Height / (double)canvas.Height;
            System.Collections.ObjectModel.Collection<BitmapTile> collection = (this.ProjectedTileCache != null) ? this.ProjectedTileCache.GetTiles(canvas.CurrentWorldExtent) : null;
            GdiPlusGeoCanvas gdiPlusGeoCanvas = canvas as GdiPlusGeoCanvas;
            if (collection != null && collection.Any<BitmapTile>()) {
                if (collection.All((BitmapTile i) => i.Bitmap != null)) {
                    using (System.Collections.Generic.IEnumerator<BitmapTile> enumerator = collection.GetEnumerator()) {
                        while (enumerator.MoveNext()) {
                            BitmapTile current = enumerator.Current;
                            double num3 = (current.BoundingBox.UpperLeftPoint.X - canvas.CurrentWorldExtent.UpperLeftPoint.X) / num;
                            double num4 = (canvas.CurrentWorldExtent.UpperLeftPoint.Y - current.BoundingBox.UpperLeftPoint.Y) / num2;
                            if (gdiPlusGeoCanvas != null) {
                                gdiPlusGeoCanvas.DrawScreenImageWithoutScaling(current.Bitmap, (float)(num3 + (double)((float)current.Bitmap.Width / 2f)), (float)(num4 + (double)((float)current.Bitmap.Height / 2f)), DrawingLevel.LevelOne, 0f, 0f, 0f);
                            }
                            else {
                                System.IO.Stream stream = new System.IO.MemoryStream();
                                current.Bitmap.Save(stream, ImageFormat.Png);
                                GeoImage image = new GeoImage(stream);
                                canvas.DrawScreenImageWithoutScaling(image, (float)(num3 + (double)((float)current.Bitmap.Width / 2f)), (float)(num4 + (double)((float)current.Bitmap.Height / 2f)), DrawingLevel.LevelOne, 0f, 0f, 0f);
                            }
                        }
                        return;
                    }
                }
            }
            Bitmap bitmap = null;
            try {
                bitmap = this.uj4(rectangleShape2);
                RectangleShape rectangleShape3 = rectangleShape2;
                if (this.projectionFromSphericalMercator != null) {
                    this.projectionFromSphericalMercator.ConvertToExternalProjection(rectangleShape2);
                    MemoryStream stream = new MemoryStream();
                    bitmap.Save(stream, System.Drawing.Imaging.ImageFormat.Bmp);
                    GeoImage geoi = new GeoImage(stream);
                    //using (GeoImage geoImage = new GeoImage {

                    //    o0M = bitmap
                    //}) {
                    //    RasterProjectionResult rasterProjectionResult = this.projectionFromSphericalMercator.ConvertToExternalProjection(geoImage, rectangleShape2);
                    //    bitmap = ((rasterProjectionResult.Image.o0M as Bitmap) ?? new Bitmap(rasterProjectionResult.Image.GetImageStream(new GdiPlusGeoCanvas())));
                    //    rectangleShape3 = rasterProjectionResult.ImageExtent;
                    //}
                }
                double num5 = rectangleShape3.Width / (double)bitmap.Width;
                double num6 = rectangleShape3.Height / (double)bitmap.Height;
                double num7 = num5 / num;
                double num8 = num6 / num2;
                double num9 = (double)bitmap.Width * num7;
                double num10 = (double)bitmap.Height * num8;
                Bitmap bitmap2 = TkQ(bitmap, new Rectangle(0, 0, (int)num9, (int)num10), new RectangleF(0f, 0f, (float)bitmap.Width, (float)bitmap.Height));
                double num11 = (rectangleShape3.UpperLeftPoint.X - canvas.CurrentWorldExtent.UpperLeftPoint.X) / num;
                double num12 = -(rectangleShape3.UpperLeftPoint.Y - canvas.CurrentWorldExtent.UpperLeftPoint.Y) / num2;
                Bitmap bitmap3 = new Bitmap((int)canvas.Width, (int)canvas.Height);
                if (bitmap.Width > 0 && bitmap.Height > 0) {
                    using (Graphics graphics = Graphics.FromImage(bitmap3)) {
                        graphics.DrawImageUnscaled(bitmap2, (int)num11, (int)num12);
                    }
                    if (this.ProjectedTileCache != null && this.projectionFromSphericalMercator != null) {
                        double num13 = (rectangleShape3.UpperLeftPoint.X - rectangleShape.UpperLeftPoint.X) / num;
                        double num14 = -(rectangleShape3.UpperLeftPoint.Y - rectangleShape.UpperLeftPoint.Y) / num2;
                        RowColumnRange intersectingRowColumnRange = this.projectedTileCache.TileMatrix.GetIntersectingRowColumnRange(canvas.CurrentWorldExtent);
                        int height = (int)(intersectingRowColumnRange.MaxRowIndex - intersectingRowColumnRange.MinRowIndex + 1L) * this.projectedTileCache.TileMatrix.TileWidth;
                        int width = (int)(intersectingRowColumnRange.MaxColumnIndex - intersectingRowColumnRange.MinColumnIndex + 1L) * this.projectedTileCache.TileMatrix.TileHeight;
                        Bitmap bitmap4 = new Bitmap(width, height);
                        using (Graphics graphics2 = Graphics.FromImage(bitmap4)) {
                            graphics2.DrawImageUnscaled(bitmap2, (int)num13, (int)num14);
                        }
                        this.ProjectedTileCache.SaveTiles(bitmap4, rectangleShape, true);
                        bitmap4.Dispose();
                    }
                    bitmap2.Dispose();
                }
                if (gdiPlusGeoCanvas != null) {
                    gdiPlusGeoCanvas.DrawScreenImageWithoutScaling(bitmap3, canvas.Width * 0.5f, canvas.Height * 0.5f, DrawingLevel.LevelOne, 0f, 0f, 0f);
                    bitmap3.Dispose();
                }
                else {
                    System.IO.Stream stream2 = new System.IO.MemoryStream();
                    bitmap3.Save(stream2, ImageFormat.Png);
                    xj4(bitmap3, "m_google");
                    bitmap3.Dispose();
                    using (GeoImage geoImage2 = new GeoImage(stream2)) {
                        canvas.DrawScreenImageWithoutScaling(geoImage2, canvas.Width * 0.5f, canvas.Height * 0.5f, DrawingLevel.LevelOne, 0f, 0f, 0f);
                    }
                }
            }
            finally {
                if (bitmap != null) {
                    bitmap.Dispose();
                }
            }
        }

        internal static string AxY() {
            string text = string.Empty;
            if (string.IsNullOrEmpty(text)) {
                text = System.Environment.GetEnvironmentVariable("Temp");
            }
            if (string.IsNullOrEmpty(text)) {
                text = System.Environment.GetEnvironmentVariable("Tmp");
            }
            if (string.IsNullOrEmpty(text)) {
                text = "c:\\MapSuiteTemp";
            }
            else {
                text += "\\MapSuite";
            }
            return text;
        }

        internal static Bitmap TkQ(Bitmap originalBitmap, Rectangle destRect, RectangleF srcRect) {
            Bitmap bitmap = new Bitmap(destRect.Width, destRect.Height);
            Graphics graphics = Graphics.FromImage(bitmap);
            graphics.DrawImage(originalBitmap, destRect, srcRect, GraphicsUnit.Pixel);
            graphics.Dispose();
            return bitmap;
        }



        private Bitmap uj4(RectangleShape canvasExtentForGoogleDrawing) {
            int num = 0;
            this.sj4.Clear();
            this.sT4.Clear();
            Bitmap bitmap = null;
            double num2 = (double)this.tileWidth * this.currentRequestSphereResolution;
            double num3 = (double)this.tileHeight * this.currentRequestSphereResolution;
            int num4 = (int)System.Math.Floor((canvasExtentForGoogleDrawing.UpperLeftPoint.X - lio.UpperLeftPoint.X) / num2);
            int num5 = (int)System.Math.Ceiling((canvasExtentForGoogleDrawing.LowerRightPoint.X - lio.UpperLeftPoint.X) / num2);
            int num6 = (int)System.Math.Floor((lio.UpperLeftPoint.Y - canvasExtentForGoogleDrawing.UpperLeftPoint.Y) / num3);
            int num7 = (int)System.Math.Ceiling((lio.UpperLeftPoint.Y - canvasExtentForGoogleDrawing.LowerRightPoint.Y) / num3);
            System.Collections.Generic.List<Task> list = new System.Collections.Generic.List<Task>();
            for (int i = num4; i < num5; i++) {
                for (int j = num6; j < num7; j++) {
                    zD4 zD4 = new zD4(num, i, j);
                    this.sT4.Add(zD4);
                    Task task = new Task(new System.Action<object>(this.uz4), zD4);
                    list.Add(task);
                    task.Start();
                    num++;
                }
            }
            Task.WaitAll(list.ToArray());
            bitmap = new Bitmap(this.tileWidth * (num5 - num4), this.tileHeight * (num7 - num6));
            using (Graphics graphics = Graphics.FromImage(bitmap)) {
                foreach (zD4 current in this.sT4) {
                    graphics.DrawImageUnscaled(this.sj4[current.zz4], (current.zyo - num4) * this.tileWidth, (current.io0 - num6) * this.tileHeight);
                }
                canvasExtentForGoogleDrawing.UpperLeftPoint.X = lio.UpperLeftPoint.X + (double)(num4 * this.tileWidth) * this.currentRequestSphereResolution;
                canvasExtentForGoogleDrawing.UpperLeftPoint.Y = lio.UpperLeftPoint.Y - (double)(num6 * this.tileHeight) * this.currentRequestSphereResolution;
                canvasExtentForGoogleDrawing.LowerRightPoint.X = lio.UpperLeftPoint.X + (double)(num5 * this.tileWidth) * this.currentRequestSphereResolution;
                canvasExtentForGoogleDrawing.LowerRightPoint.Y = lio.UpperLeftPoint.Y - (double)(num7 * this.tileHeight) * this.currentRequestSphereResolution;
            }
            return bitmap;
        }



        private void uz4(object tileInfo) {
            bool flag = false;
            if (!string.IsNullOrEmpty(this.cacheDirectory) && this.ProjectionFromSphericalMercator == null && this.TileCache is EncryptedFileBitmapTileCache && ((EncryptedFileBitmapTileCache)this.TileCache).CacheDirectory.Equals(AxY() + "\\GoogleMapsTileCache") && (this.ProjectedTileCache == null || (this.ProjectedTileCache is EncryptedFileBitmapTileCache && ((EncryptedFileBitmapTileCache)this.ProjectedTileCache).CacheDirectory.Equals(AxY() + "\\GoogleMapsProjectedTileCache")))) {
                flag = true;
                if (this.requestedTileCache == null && this.tileCache != null) {
                    this.requestedTileCache = new GoogleMapsLayerImageCache();
                    this.requestedTileCache.So7 = ((FileBitmapTileCache)this.tileCache).CacheDirectory;
                }
            }
            Bitmap bitmap = null;
            zD4 zD4 = (zD4)tileInfo;
            if (flag) {
                bitmap = this.requestedTileCache.jc6(this.MapType, this.zoomLevelsNumber, zD4.zyo, zD4.io0, this.PictureFormat);
            }
            else if (this.tileCache != null) {
                TileMatrixCell cell = this.TileCache.TileMatrix.GetCell((long)zD4.io0, (long)zD4.zyo);
                BitmapTile tile = this.TileCache.GetTile(cell.BoundingBox);
                if (tile.Bitmap != null) {
                    bitmap = tile.Bitmap;
                }
            }
            if (bitmap != null) {
                lock (this.sj4) {
                    this.sj4.Add(zD4.zz4, bitmap);
                }
                return;
            }
            double num = (double)this.tileWidth * this.currentRequestSphereResolution;
            double num2 = (double)this.tileHeight * this.currentRequestSphereResolution;
            double num3 = lio.UpperLeftPoint.X + (double)zD4.zyo * num;
            double num4 = lio.UpperLeftPoint.Y - (double)zD4.io0 * num2;
            double maxX = num3 + num;
            double minY = num4 - num2;
            RectangleShape rectangleShape = new RectangleShape(num3, num4, maxX, minY);
            rectangleShape = rectangleShape.GetIntersection(lio);
            PointShape centerPoint = rectangleShape.GetCenterPoint();
            PointShape pointShape = this.googleWgs84Projection.ConvertToExternalProjection(centerPoint) as PointShape;
            string uriString = this.vD4(pointShape.X, pointShape.Y, this.zoomLevelsNumber, (double)this.tileWidth, (double)this.tileHeight);
            System.IO.Stream stream = null;
            try {
                CreatingRequestGoogleMapsLayerEventArgs creatingRequestGoogleMapsLayerEventArgs = new CreatingRequestGoogleMapsLayerEventArgs(new Uri(uriString));
                this.OnCreatingRequest(creatingRequestGoogleMapsLayerEventArgs);
                WebRequest webRequest = WebRequest.Create(creatingRequestGoogleMapsLayerEventArgs.RequestUri);
                webRequest.Proxy = this.webProxy;
                WebResponse webResponse = this.SendWebRequest(webRequest);
                stream = webResponse.GetResponseStream();
                bitmap = new Bitmap(stream);
                webResponse.Close();
                if (flag) {
                    this.requestedTileCache.yj4(bitmap, this.MapType, this.zoomLevelsNumber, zD4.zyo, zD4.io0, this.PictureFormat);
                }
                else if (this.TileCache != null) {
                    BitmapTile tile2 = new BitmapTile(bitmap, this.TileCache.TileMatrix.GetCell((long)zD4.io0, (long)zD4.zyo).BoundingBox, this.TileCache.TileMatrix.Scale);
                    this.TileCache.SaveTile(tile2);
                }
            }
            finally {
                if (stream != null) {
                    stream.Dispose();
                }
            }
            lock (this.sj4) {
                this.sj4.Add(zD4.zz4, bitmap);
            }
        }



        private string vD4(double lon, double lat, int zoomLevelNumber, double tileWidth, double tileHeight) {
            string text = vT4(tileWidth, tileHeight, zoomLevelNumber, lon, lat);
            System.Text.StringBuilder stringBuilder = new System.Text.StringBuilder("http://maps.googleapis.com/maps/api/staticmap?");
            stringBuilder.AppendFormat(System.Globalization.CultureInfo.InvariantCulture, "{0}&", new object[]
          {
                text
                    });
            stringBuilder.AppendFormat(System.Globalization.CultureInfo.InvariantCulture, "maptype={0}&", new object[]
          {
                this.Ays()
                    });
            stringBuilder.AppendFormat(System.Globalization.CultureInfo.InvariantCulture, "format={0}&", new object[]
          {
                this.GetPictureFormat()
                    });
            stringBuilder.Append("sensor=false");
            if (!string.IsNullOrEmpty(this.clientId) && !string.IsNullOrEmpty(this.privateKey)) {
                stringBuilder.AppendFormat(System.Globalization.CultureInfo.InvariantCulture, "&client={0}", new object[]
              {
                    this.clientId
                        });
                stringBuilder.AppendFormat(System.Globalization.CultureInfo.InvariantCulture, "&signature={0}", new object[]
              {
                    this.xD4(stringBuilder.ToString())
                        });
            }
            if (!string.IsNullOrEmpty(this.apiKey) && string.IsNullOrEmpty(this.clientId)) {
                stringBuilder.AppendFormat(System.Globalization.CultureInfo.InvariantCulture, "&key={0}", new object[]
              {
                    this.apiKey
                        });
            }
            return stringBuilder.ToString();
        }


        private static string vT4(double newWidth, double newHeight, int zoomLevelNumber, double longitude, double latitude) {
            System.Text.StringBuilder stringBuilder = new System.Text.StringBuilder();
            stringBuilder.AppendFormat(System.Globalization.CultureInfo.InvariantCulture, "center={0},{1}&", new object[]
          {
                System.Math.Round(latitude, 6),
                System.Math.Round(longitude, 6)
                    });
            stringBuilder.AppendFormat(System.Globalization.CultureInfo.InvariantCulture, "zoom={0}&", new object[]
          {
                zoomLevelNumber
                    });
            stringBuilder.AppendFormat(System.Globalization.CultureInfo.InvariantCulture, "size={0}x{1}", new object[]
          {
                (int)newWidth,
                (int)newHeight
                    });
            return stringBuilder.ToString();
        }

        private string Ays() {
            switch (this.mapType) {
                case GoogleMapsMapType.RoadMap:
                    return "roadmap";
                case GoogleMapsMapType.Mobile:
                    return "mobile";
                case GoogleMapsMapType.Satellite:
                    return "satellite";
                case GoogleMapsMapType.Terrain:
                    return "terrain";
                case GoogleMapsMapType.Hybrid:
                    return "hybrid";
                default:
                    return "roadmap";
            }
        }
        public string GetPictureFormat() {
            GoogleMapsPictureFormat googleMapsPictureFormat = this.pictureFormat;
            switch (googleMapsPictureFormat) {
                case GoogleMapsPictureFormat.Jpeg:
                    return "jpg-baseline";
                case GoogleMapsPictureFormat.Gif:
                    return "gif";
                case GoogleMapsPictureFormat.Png8:
                    return "png8";
                default:
                    if (googleMapsPictureFormat != GoogleMapsPictureFormat.Png32) {
                        return "jpg-baseline";
                    }
                    return "png32";
            }
        }

        private static Bitmap hU8(Image offScaleImage, int tileWidth, int tileHeight) {
            Bitmap bitmap = null;
            if (offScaleImage != null) {
                Graphics graphics = null;
                try {
                    Rectangle srcRect = new Rectangle(0, 0, offScaleImage.Width, offScaleImage.Height);
                    Rectangle destRect = new Rectangle(0, 0, tileWidth, tileHeight);
                    bitmap = new Bitmap(tileWidth, tileHeight);
                    graphics = Graphics.FromImage(bitmap);
                    graphics.DrawImage(offScaleImage, destRect, srcRect, GraphicsUnit.Pixel);
                }
                finally {
                    if (graphics != null) {
                        graphics.Dispose();
                    }
                    if (offScaleImage != null) {
                        offScaleImage.Dispose();
                    }
                }
            }
            return bitmap;
        }


        private bool vj4(RectangleShape utmExtent) {
            Projection projection = this.displayWgs84Projection ?? this.googleWgs84Projection;
            PointShape pointShape = (PointShape)projection.ConvertToExternalProjection(utmExtent.UpperLeftPoint);
            PointShape pointShape2 = (PointShape)projection.ConvertToExternalProjection(utmExtent.LowerRightPoint);
            return -180.0 > pointShape2.X || 180.0 < pointShape.X || 90.0 < pointShape2.Y || -90.0 > pointShape.Y;
        }


        private Bitmap vz4(int drawingScreenHeight, int drawingScreenWidth, double snappedScale, RectangleShape extentInSphericalMercator) {
            Bitmap bitmap = null;
            if (this.tileCache != null) {
                System.Collections.ObjectModel.Collection<BitmapTile> collection = null;
                lock (this.tileCache) {
                    this.tileCache.TileMatrix.BoundingBoxUnit = GeographyUnit.Meter;
                    this.tileCache.TileMatrix.Scale = snappedScale;
                    this.tileCache.TileMatrix.TileWidth = 256;
                    this.tileCache.TileMatrix.TileHeight = 256;
                    collection = this.tileCache.GetTiles(extentInSphericalMercator);
                }
                if (collection != null) {
                    if (collection.All((BitmapTile t) => t.Bitmap != null)) {
                        bitmap = new Bitmap(drawingScreenWidth, drawingScreenHeight);
                        using (Graphics graphics = Graphics.FromImage(bitmap)) {
                            foreach (BitmapTile current in from t in collection
                                                           where t.Bitmap != null
                                                           select t) {
                                double num = (current.BoundingBox.UpperLeftPoint.X - extentInSphericalMercator.UpperLeftPoint.X) / this.currentSphereResolution;
                                double num2 = (extentInSphericalMercator.UpperLeftPoint.Y - current.BoundingBox.UpperLeftPoint.Y) / this.currentSphereResolution;
                                graphics.DrawImageUnscaled(current.Bitmap, (int)num, (int)num2);
                            }
                        }
                    }
                }
            }
            return bitmap;
        }

        private void wD4(Bitmap image, PointShape centerInSphericalMercator, double snappedScale) {
            if (this.tileCache != null) {
                double minX = centerInSphericalMercator.X - this.currentSphereResolution * (double)image.Width * 0.5;
                double maxX = centerInSphericalMercator.X + this.currentSphereResolution * (double)image.Width * 0.5;
                double maxY = centerInSphericalMercator.Y + this.currentSphereResolution * (double)image.Height * 0.5;
                double minY = centerInSphericalMercator.Y - this.currentSphereResolution * (double)image.Height * 0.5;
                lock (this.tileCache) {
                    this.tileCache.TileMatrix.BoundingBoxUnit = GeographyUnit.Meter;
                    this.tileCache.TileMatrix.Scale = snappedScale;
                    this.tileCache.TileMatrix.TileWidth = 256;
                    this.tileCache.TileMatrix.TileHeight = 256;
                    this.tileCache.SaveTiles(image, new RectangleShape(minX, maxY, maxX, minY), true);
                }
            }
        }

        private static double wT4(int zoomLevelNumber) {
            return wj4(zoomLevelNumber).Scale;
        }

        private static ZoomLevel wj4(int zoomLevelNumber) {
            return wz4()[zoomLevelNumber];
        }

        private static System.Collections.ObjectModel.Collection<ZoomLevel> wz4() {
            rz4 = (rz4 ?? new SphericalMercatorZoomLevelSet().GetZoomLevels());
            return rz4;
        }



        private string xD4(string url) {
            //7ys=.JhQ=(this.PrivateKey, "PrivateKey");
            string s = this.PrivateKey.Replace("-", "+").Replace("_", "/");
            byte[] key = System.Convert.FromBase64String(s);
            Uri uri = new Uri(url);
            byte[] buffer = xT4(uri.LocalPath + uri.Query);
            System.Security.Cryptography.HMACSHA1 hMACSHA = new System.Security.Cryptography.HMACSHA1(key);
            byte[] inArray = hMACSHA.ComputeHash(buffer);
            return System.Convert.ToBase64String(inArray).Replace("+", "-").Replace("/", "_");
        }


        private static byte[] xT4(string value) {
            byte[] bytes = System.Text.Encoding.Unicode.GetBytes(value);
            byte[] array = new byte[bytes.Length / 2];
            for (int i = 0; i < bytes.Length; i += 2) {
                array[i / 2] = bytes[i];
                if (array[i / 2] > 127) {
                    array[i / 2] = 63;
                }
            }
            return array;
        }


        private static void xj4(Bitmap bitmap, string name) {
        }

        // ThinkGeo.MapSuite.Core.GeoImage
        //internal object o0M {
        //    get {
        //        return this.nativeImage;
        //    }
        //    set {
        //        this.nativeImage = value;
        //        Bitmap bitmap = this.nativeImage as Bitmap;
        //        if (bitmap != null) {
        //            this.width = bitmap.Width;
        //            this.height = bitmap.Height;
        //            if (this.imageStream != null) {
        //                this.imageStream.Dispose();
        //                this.imageStream = null;
        //            }
        //        }
        //    }
        //}



    }
    internal class zD4 {
        private int zT4;

        private int sCo;

        private int sSo;

        public int zz4 {
            get {
                return this.zT4;
            }

        }

        public int zyo {
            get {
                return this.sCo;
            }
        }

        public int io0 {
            get {
                return this.sSo;
            }
        }

        public zD4() : this(0, 0, 0) {
        }


        public zD4(int imageId, int column, int row) {
            this.zT4 = imageId;
            this.sCo = column;
            this.sSo = row;
        }
    }









    [System.Serializable]
    internal class GoogleMapsLayerImageCache {
        private string cacheDirectory;

        //7So
        public string So7 {
            get {
                return this.cacheDirectory;
            }
            set {
                this.cacheDirectory = value;
                if (!string.IsNullOrEmpty(this.cacheDirectory)) {
                    if (!System.IO.Directory.Exists(this.cacheDirectory)) {
                        System.IO.Directory.CreateDirectory(this.cacheDirectory);
                    }
                    this.cacheDirectory = value;
                    if (!this.cacheDirectory.EndsWith("\\", System.StringComparison.OrdinalIgnoreCase)) {
                        this.cacheDirectory += "\\";
                    }
                }
            }
        }


        public bool yD4 {
            get {
                return !string.IsNullOrEmpty(this.cacheDirectory);
            }
        }

        public string yT4(GoogleMapsMapType mapType, int zoomlevel, int tileX, int tileY, GoogleMapsPictureFormat pictureFormat) {
            string text = this.cacheDirectory + zoomlevel;
            if (!System.IO.Directory.Exists(text)) {
                System.IO.Directory.CreateDirectory(text);
            }
            text = text + "\\" + tileX;
            if (!System.IO.Directory.Exists(text)) {
                System.IO.Directory.CreateDirectory(text);
            }
            return string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0}\\{1}{2}-{3}-{4}.{5}", new object[]
          {
                      text,
                      GoogleMapsLayerImageCache.Ays(mapType),
                      zoomlevel,
                      tileX,
                      tileY,
                      GoogleMapsLayerImageCache.yz4(pictureFormat)
                    });
        }

        public void yj4(Bitmap image, GoogleMapsMapType mapType, int zoomlevel, int tileX, int tileY, GoogleMapsPictureFormat pictureFormat) {
            string text = this.yT4(mapType, zoomlevel, tileX, tileY, pictureFormat);
            text = text.Replace("." + GoogleMapsLayerImageCache.yz4(pictureFormat), ".tile");
            using (System.IO.MemoryStream memoryStream = new System.IO.MemoryStream()) {
                image.Save(memoryStream, GoogleMapsLayerImageCache.BSs(pictureFormat));
                System.Collections.Generic.List<byte> list = new System.Collections.Generic.List<byte>();
                list.AddRange(memoryStream.ToArray());
                for (int i = 0; i < list.Count; i++) {
                    list[i] ^= 255;
                }
                System.Collections.Generic.List<byte> arg_84_0 = list;
                int arg_84_1 = 0;
                byte[] collection = new byte[8];
                arg_84_0.InsertRange(arg_84_1, collection);
                System.IO.File.WriteAllBytes(text, list.ToArray());
            }
        }

        //6jc
        public Bitmap jc6(GoogleMapsMapType mapType, int zoomlevel, int tileX, int tileY, GoogleMapsPictureFormat pictureFormat) {
            string text = this.yT4(mapType, zoomlevel, tileX, tileY, pictureFormat);
            string text2 = text.Replace("." + GoogleMapsLayerImageCache.yz4(pictureFormat), ".tile");
            Bitmap result = null;
            string text3 = null;
            if (System.IO.File.Exists(text2)) {
                text3 = text2;
            }
            else if (System.IO.File.Exists(text)) {
                text3 = text;
            }
            if (text3 != null) {
                System.Collections.Generic.List<byte> list = new System.Collections.Generic.List<byte>();
                list.AddRange(System.IO.File.ReadAllBytes(text3));
                for (int i = 0; i < 8; i++) {
                    if (list[i] != 0) {
                        return new Bitmap(new System.IO.MemoryStream(list.ToArray()));
                    }
                }
                list.RemoveRange(0, 8);
                for (int j = 0; j < list.Count; j++) {
                    list[j] ^= 255;
                }
                result = new Bitmap(new System.IO.MemoryStream(list.ToArray()));
            }
            return result;
        }

        private static string Ays(GoogleMapsMapType mapType) {
            switch (mapType) {
                case GoogleMapsMapType.RoadMap:
                    return "roadmap";
                case GoogleMapsMapType.Mobile:
                    return "mobile";
                case GoogleMapsMapType.Satellite:
                    return "satellite";
                case GoogleMapsMapType.Terrain:
                    return "terrain";
                case GoogleMapsMapType.Hybrid:
                    return "hybrid";
                default:
                    return "roadmap";
            }
        }

        public static ImageFormat BSs(GoogleMapsPictureFormat pictureFormat) {
            switch (pictureFormat) {
                case GoogleMapsPictureFormat.Jpeg:
                    return ImageFormat.Jpeg;
                case GoogleMapsPictureFormat.Gif:
                    return ImageFormat.Gif;
                case GoogleMapsPictureFormat.Png8:
                    break;
                default:
                    if (pictureFormat != GoogleMapsPictureFormat.Png32) {
                    }
                    break;
            }
            return ImageFormat.Png;
        }

        public static string yz4(GoogleMapsPictureFormat pictureFormat) {
            switch (pictureFormat) {
                case GoogleMapsPictureFormat.Jpeg:
                    return "jpg";
                case GoogleMapsPictureFormat.Gif:
                    return "gif";
                case GoogleMapsPictureFormat.Png8:
                    break;
                default:
                    if (pictureFormat != GoogleMapsPictureFormat.Png32) {
                    }
                    break;
            }
            return "png";
        }
    }
}













