﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ThinkGeo.MapSuite.WpfDesktopEdition;
using ThinkGeo.MapSuite.Core;
using System.Diagnostics;
using System.IO;
using System.Drawing;
using System.Windows.Forms;
using Landdb.Client.Infrastructure;
using Landdb.Resources;

namespace Landdb.ViewModel.Maps.ImageryOverlay {
    public class PlanetImageOverlay : IMapOverlay {
        private Overlay overlay;
        GeographyUnit geographyunit;
        RectangleShape extent;
        string projectionstring;
        bool visibility;
        bool onlineOnly;
        string filename;
        int epsg;
        RectangleShape imageextent;
        Proj4Projection proj4;

        public PlanetImageOverlay() {
            SetMapInfoSelectorItemToPlanetLab();
            extent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
            geographyunit = GeographyUnit.Meter;
            projectionstring = Proj4Projection.GetEpsgParametersString(3857);
            this.visibility = true;
            this.onlineOnly = false;
            //this.overlay = NewOnlineOverlay();
            this.filename = string.Empty;
            this.epsg = 0;
            this.imageextent = new RectangleShape();
            //overlay = NewOverlay();

            proj4 = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetGoogleMapParametersString());
            if (!proj4.IsOpen)
            {
                proj4.Open();
            }
        }

        public PlanetImageOverlay(bool? allowofflinemap) {
            SetMapInfoSelectorItemToPlanetLab();
            extent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
            geographyunit = GeographyUnit.Meter;
            projectionstring = Proj4Projection.GetEpsgParametersString(3857);
            this.visibility = true;
            this.onlineOnly = false;
            //this.overlay = NewOnlineOverlay();
            this.filename = string.Empty;
            this.epsg = 0;
            this.imageextent = new RectangleShape();
            //overlay = NewOverlay();

            proj4 = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetGoogleMapParametersString());
            if (!proj4.IsOpen)
            {
                proj4.Open();
            }
        }

        public string Name => MapInfoSelectorItem.MapTypeKeyText;
        public MapInfoSelectorItem MapInfoSelectorItem { get; set; }
        private void SetMapInfoSelectorItemToPlanetLab() {
            MapInfoSelectorItem = new MapInfoSelectorItem(MapStatusType.PlanetLab);
        }

        public Overlay Overlay {
            get { return overlay; }
            set { overlay = value; }
        }

        public GeographyUnit GeographyUnit {
            get { return geographyunit; }
            set { geographyunit = value; }
        }

        public RectangleShape Extent {
            get { return extent; }
            set { extent = value; }
        }

        public string ProjectionString {
            get { return projectionstring; }
            set { projectionstring = value; }
        }

        public bool Visibility {
            get { return visibility; }
            set { visibility = value; }
        }

        public bool OnlineOnly {
            get { return onlineOnly; }
            set { onlineOnly = value; }
        }

        public Overlay NewOverlay() {
            try
            {
                Stopwatch sw = new Stopwatch();
                sw.Start();
                SetMapInfoSelectorItemToPlanetLab();
                extent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
                geographyunit = GeographyUnit.Meter;
                projectionstring = Proj4Projection.GetEpsgParametersString(3857);
                proj4 = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetGoogleMapParametersString());
                visibility = true;
                onlineOnly = false;
                if (!proj4.IsOpen)
                {
                    proj4.Open();
                }
                //if (filename.EndsWith(@"clip.tif"))
                //{
                //    this.epsg = 4326;
                //    extent = new RectangleShape(-126.4, 48.8, -67.0, 19.0);
                //    geographyunit = GeographyUnit.DecimalDegree;
                //    projectionstring = Proj4Projection.GetEpsgParametersString(4326);
                //}

                string worldfile = CreateWorldFile(filename);


                extent = imageextent;
                RectangleShape projextent = imageextent;
                //projextent = proj4.ConvertToExternalProjection(imageextent);

                LayerOverlay planetoverlay = new LayerOverlay();
                GeoTiffRasterLayer planetlayer = new GeoTiffRasterLayer();
                if (!string.IsNullOrEmpty(worldfile))
                {
                    planetlayer = new GeoTiffRasterLayer(filename, worldfile);
                }
                else
                {
                    planetlayer = new GeoTiffRasterLayer(filename, projextent);
                    projectionstring = Proj4Projection.GetEpsgParametersString(this.epsg);

                    //Proj4Projection proj4 = new Proj4Projection();
                    //proj4.InternalProjectionParametersString = Proj4Projection.GetEpsgParametersString(epsg);
                    //proj4.ExternalProjectionParametersString = Proj4Projection.GetGoogleMapParametersString();
                    //proj4.Open();
                    //planetlayer.ImageSource.Projection = proj4;
                }
                planetlayer.UpperThreshold = Double.MaxValue;
                planetlayer.LowerThreshold = 0;
                //planetlayer.DrawingExceptionMode = DrawingExceptionMode.DrawException;
                if (!planetlayer.IsOpen)
                {
                    planetlayer.Open();
                }
                planetlayer.ImageSource.Projection = proj4;

                string tempFileBase = Path.Combine(Path.GetDirectoryName(filename), Path.GetFileNameWithoutExtension(filename));
                string gnewimagerynamebpw = string.Format("{0}.tfw", tempFileBase);
                string worldfiletext = planetlayer.ImageSource.GetWorldFileText();
                //System.IO.File.WriteAllText(gnewimagerynamebpw, worldfiletext);

                planetoverlay.Layers.Add(Name, planetlayer);

                planetoverlay.IsVisible = visibility;
                planetoverlay.DrawingExceptionMode = DrawingExceptionMode.DrawException;
                overlay = planetoverlay;
                sw.Stop();
                Console.WriteLine(sw.ElapsedMilliseconds);

                //if (gws.GoogleWebLayer != null) {
                //    bingOverlay.Layers.Clear();
                //    bingOverlay.Layers.Add("GoogleMapsLayer", gws.GoogleWebLayer);
                //    bingOverlay.TileType = TileType.SingleTile;
                //    //bingOverlay.RenderMode = RenderMode.GdiPlus;
                //    bingOverlay.IsVisible = visibility;
                //    overlay = bingOverlay;
                //    return bingOverlay;
                //}
                return planetoverlay;
            }
            catch (Exception ex)
            {
                MessageBox.Show(Strings.ProblemProjectingImageFiles_Text);
            }
            return null;
        }

        public Overlay NewOnlineOverlay() {
            try
            {
                Stopwatch sw = new Stopwatch();
                sw.Start();
                SetMapInfoSelectorItemToPlanetLab();
                extent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
                geographyunit = GeographyUnit.Meter;
                projectionstring = Proj4Projection.GetEpsgParametersString(3857);
                proj4 = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetGoogleMapParametersString());
                //proj4 = new Proj4Projection(Proj4Projection.GetEpsgParametersString(3857), Proj4Projection.GetGoogleMapParametersString());
                visibility = true;
                onlineOnly = false;
                if (!proj4.IsOpen)
                {
                    proj4.Open();
                }
                //if (filename.EndsWith(@"clip.tif"))
                //{
                //    this.epsg = 4326;
                //    extent = new RectangleShape(-126.4, 48.8, -67.0, 19.0);
                //    geographyunit = GeographyUnit.DecimalDegree;
                //    projectionstring = Proj4Projection.GetEpsgParametersString(4326);
                //}
                string worldfile = string.Empty;
                //worldfile = CreateWorldFile(filename);


                extent = imageextent;
                RectangleShape projextent = imageextent;
                //projextent = proj4.ConvertToInternalProjection(imageextent);

                LayerOverlay planetoverlay = new LayerOverlay();
                if (filename.EndsWith(@".tif"))
                {
                    GeoTiffRasterLayer planetlayer = new GeoTiffRasterLayer();
                    planetlayer = new GeoTiffRasterLayer(filename, projextent);
                    planetlayer.UpperThreshold = Double.MaxValue;
                    planetlayer.LowerThreshold = 0;
                    planetlayer.KeyColors.Add(GeoColor.SimpleColors.Black);
                    //planetlayer.DrawingExceptionMode = DrawingExceptionMode.DrawException;
                    if (!planetlayer.IsOpen)
                    {
                        planetlayer.Open();
                    }
                    planetlayer.ImageSource.Projection = proj4;

                    //string tempFileBase = Path.Combine(Path.GetDirectoryName(filename), Path.GetFileNameWithoutExtension(filename));
                    //string gnewimagerynamebpw = string.Format("{0}.tfw", tempFileBase);
                    //string worldfiletext = planetlayer.ImageSource.GetWorldFileText();
                    //System.IO.File.WriteAllText(gnewimagerynamebpw, worldfiletext);

                    planetoverlay.Layers.Add(Name, planetlayer);

                    planetoverlay.IsVisible = visibility;
                    planetoverlay.DrawingExceptionMode = DrawingExceptionMode.DrawException;
                    overlay = planetoverlay;
                }
                else
                {
                    GdiPlusRasterLayer planetlayer = new GdiPlusRasterLayer();
                    planetlayer = new GdiPlusRasterLayer(filename, projextent);
                    planetlayer.UpperThreshold = Double.MaxValue;
                    planetlayer.LowerThreshold = 0;
                    planetlayer.KeyColors.Add(GeoColor.SimpleColors.Black);
                    //planetlayer.DrawingExceptionMode = DrawingExceptionMode.DrawException;
                    if (!planetlayer.IsOpen)
                    {
                        planetlayer.Open();
                    }
                    planetlayer.ImageSource.Projection = proj4;

                    //string tempFileBase = Path.Combine(Path.GetDirectoryName(filename), Path.GetFileNameWithoutExtension(filename));
                    //string gnewimagerynamebpw = string.Format("{0}.tfw", tempFileBase);
                    //string worldfiletext = planetlayer.ImageSource.GetWorldFileText();
                    //System.IO.File.WriteAllText(gnewimagerynamebpw, worldfiletext);

                    planetoverlay.Layers.Add(Name, planetlayer);

                    planetoverlay.IsVisible = visibility;
                    planetoverlay.DrawingExceptionMode = DrawingExceptionMode.DrawException;
                    overlay = planetoverlay;

                }
                sw.Stop();
                Console.WriteLine(sw.ElapsedMilliseconds);

                return planetoverlay;
            }
            catch (Exception ex)
            {
                MessageBox.Show(Strings.ProblemProjectingImageFiles_Text);
            }
            return null;
        }

        public ZoomLevelSet GetZoomLevelSet() {
            //ZoomLevelFactory zlf = new ZoomLevelFactory();
            //return zlf.GetGoogleMapsZoomLevelSet();
            return null;
        }
        public Overlay ExtentHasChanged(int tileHeight, int tileWidth, RectangleShape extent, RectangleShape projextent, PointShape wgscenterpoint, int zoomlevelnumber, int epsgcode) {
            return null;
        }

        public Overlay LoadImageOnOverlay(string filename, int epsg, RectangleShape imageextent, bool isclipped) {
            this.filename = filename;
            this.epsg = epsg;
            this.imageextent = imageextent;
            if (isclipped)
            {
                return NewOnlineOverlay(); ;
            } else
            {
                return NewOverlay(); ;
            }
        }

        /// <summary>
        /// Create tfw - world file.
        /// </summary>
        /// <param name="filename">tif image full filename</param>
        private string CreateWorldFile(string filename) {
            string tfwFileName = Path.GetDirectoryName(filename) + "\\" + Path.GetFileNameWithoutExtension(filename) + ".tfw";
            if(File.Exists(tfwFileName)) {
                return tfwFileName;
            }
            try
            {

                //string text = string.Empty;
                //text = text + Convert.ToDouble(worldExtent.Width / (double)imageWidth).ToString(CultureInfo.InvariantCulture) + Environment.NewLine;
                //text = text + < CliSecureRT >.cs("?(ÈØ¶") + Environment.NewLine;
                //text = text + < CliSecureRT >.cs("?(ÈØ¶") + Environment.NewLine;
                //text = text + Convert.ToDouble(worldExtent.Height / (double)imageHeight).ToString(CultureInfo.InvariantCulture) + Environment.NewLine;
                //text = text + worldExtent.UpperLeftPoint.X + Environment.NewLine;
                //return text + worldExtent.UpperLeftPoint.Y + Environment.NewLine;


                using (var bitmapImage = new Bitmap(filename))
                {
                    System.Drawing.Imaging.PropertyItem[] imageProps = bitmapImage.PropertyItems;

                    var modelscale = imageProps.First(a => a.Id == GEOTIFF_MODELPIXELSCALETAG);
                    var tiepoint = imageProps.First(a => a.Id == GEOTIFF_MODELTIEPOINTTAG);

                    double x = BitConverter.ToDouble(tiepoint.Value, 0 + 24);
                    double y = BitConverter.ToDouble(tiepoint.Value, 0 + 32);

                    double xscale = BitConverter.ToDouble(modelscale.Value, 0);
                    double yscale = BitConverter.ToDouble(modelscale.Value, 0 + 8);


                    using (System.IO.StreamWriter file = new System.IO.StreamWriter(tfwFileName))
                    {
                        file.WriteLine(xscale);
                        file.WriteLine("0.0");
                        file.WriteLine("0.0");
                        file.WriteLine(yscale);
                        file.WriteLine(x);
                        file.WriteLine(y);
                    }
                    return tfwFileName;
                }
            } catch (Exception ex)
            {
                System.Windows.MessageBox.Show(Strings.AnErrorOccurredGeoreferencingThisImage_Text);
            }
            return string.Empty;
        }

        private readonly int GEOTIFF_MODELPIXELSCALETAG = 33550;
        private readonly int GEOTIFF_MODELTIEPOINTTAG = 33922;

    }
}
