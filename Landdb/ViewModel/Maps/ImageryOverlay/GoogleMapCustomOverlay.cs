﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ThinkGeo.MapSuite.WpfDesktopEdition;
using ThinkGeo.MapSuite.Core;
using System.Diagnostics;
using Landdb.Client.Infrastructure;
using Landdb.ViewModel.Fields.FieldDetails.MapModels;
using Landdb.MapStyles.Overlays;

namespace Landdb.ViewModel.Maps.ImageryOverlay {
    public class GoogleMapCustomOverlay : IMapOverlay {
        private Overlay overlay;
        private LayerOverlay bingOverlay;
        GoogleWebService gws;
        GeographyUnit geographyunit;
        RectangleShape extent;
        string projectionstring;
        bool visibility;
        bool onlineOnly;
        int zoomLevelsNumber;

        public GoogleMapCustomOverlay() {
            SetMapInfoSelectorItemToGoogle();
            extent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
            geographyunit = GeographyUnit.Meter;
            projectionstring = Proj4Projection.GetEpsgParametersString(3857);
            this.visibility = true;
            this.onlineOnly = false;
            this.overlay = NewOnlineOverlay();

            TempFileCleanUp();
        }

        public string Name => MapInfoSelectorItem.MapTypeKeyText;
        public MapInfoSelectorItem MapInfoSelectorItem { get; set; }

        private void SetMapInfoSelectorItemToGoogle() {
            MapInfoSelectorItem = new MapInfoSelectorItem(MapStatusType.Google);
        }

        public Overlay Overlay {
            get { return overlay; }
            set { overlay = value; }
        }

        public GeographyUnit GeographyUnit {
            get { return geographyunit; }
            set { geographyunit = value; }
        }

        public RectangleShape Extent {
            get { return extent; }
            set { extent = value; }
        }

        public string ProjectionString {
            get { return projectionstring; }
            set { projectionstring = value; }
        }

        public bool Visibility {
            get { return visibility; }
            set { visibility = value; }
        }

        public bool OnlineOnly {
            get { return onlineOnly; }
            set { onlineOnly = value; }
        }

        public int ZoomLevelsNumber {
            get { return this.zoomLevelsNumber; }
            set { this.zoomLevelsNumber = value; }
        }

        public ZoomLevelSet GetZoomLevelSet() {
            ZoomLevelFactory zoomlevelfactory = new ZoomLevelFactory("Google40");
            return zoomlevelfactory.ZoomLevelSET;
        }

        public Overlay NewOverlay() {

            return new LayerOverlay();
        }

        public Overlay NewOnlineOverlay() {
            try {
                Stopwatch sw = new Stopwatch();
                sw.Start();
                SetMapInfoSelectorItemToGoogle();
                extent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
                geographyunit = GeographyUnit.Meter;
                projectionstring = Proj4Projection.GetEpsgParametersString(3857);
                visibility = true;
                onlineOnly = true;
                bingOverlay = new LayerOverlay();
                bingOverlay.DrawingExceptionMode = DrawingExceptionMode.DrawException;
                RectangleShape wgs84extent = new RectangleShape(-126.4, 48.8, -67.0, 19.0);
                PointShape wgscenterpoint = wgs84extent.GetCenterPoint();
                overlay = bingOverlay;
                return bingOverlay;
            }
            catch (Exception ex) {
                int i = 0;
                return null;
            }
        }

        public Overlay ExtentHasChanged(int tileHeight, int tileWidth, RectangleShape extent, RectangleShape projextent, PointShape wgscenterpoint, int zoomlevelnumber, int epsgcode) {
            return LoadGoogleImage(tileHeight, tileWidth, extent, projextent, wgscenterpoint, zoomlevelnumber, epsgcode);
        }

        public Overlay LoadGoogleImage(int tileHeight, int tileWidth, RectangleShape extent, RectangleShape projextent, PointShape wgscenterpoint, int zoomlevelnumber, int epsgcode) {
            try {
                gws = new GoogleWebService(tileHeight, tileWidth, GoogleMapsMapType.Hybrid, GoogleMapsPictureFormat.Jpeg, 20, @"gme-agconnectionsllc", @"mh_Fpie63AeVt1cL6ClW2oxR1iE=");

                System.IO.Stream strem = gws.GetGoogleMapsImage(wgscenterpoint.Y, wgscenterpoint.X, zoomlevelnumber, extent, projextent, epsgcode);


                if (gws.GoogleWebLayer != null) {
                    bingOverlay.Layers.Clear();
                    bingOverlay.Layers.Add("GoogleMapsLayer", gws.GoogleWebLayer);
                    bingOverlay.TileType = TileType.SingleTile;
                    //bingOverlay.RenderMode = RenderMode.GdiPlus;
                    bingOverlay.IsVisible = visibility;
                    overlay = bingOverlay;
                    return bingOverlay;
                }
            }
            catch (Exception ex) {
                int i = 0;
                return null;
            }
            return null;
        }
        public bool TempFileCleanUp() {
            bool successfuldelete = true;
            System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(Infrastructure.ApplicationEnvironment.GoogleMapCacheDirectory);
            if (!System.IO.Directory.Exists(Infrastructure.ApplicationEnvironment.GoogleMapCacheDirectory)) {
                System.IO.Directory.CreateDirectory(Infrastructure.ApplicationEnvironment.GoogleMapCacheDirectory);
            }
            if (System.IO.Directory.Exists(Infrastructure.ApplicationEnvironment.GoogleMapCacheDirectory)) {
                System.IO.FileInfo[] files = dir.GetFiles(@"*.bmp");
                foreach (System.IO.FileInfo file in files) {
                    try {
                        if (System.IO.File.Exists(file.FullName)) {
                            System.IO.File.Delete(file.FullName);
                        }
                    }
                    catch {
                        successfuldelete = false;
                    }
                }
                System.IO.FileInfo[] file2s = dir.GetFiles(@"*.bpw");
                foreach (System.IO.FileInfo file in file2s) {
                    try {
                        if (System.IO.File.Exists(file.FullName)) {
                            System.IO.File.Delete(file.FullName);
                        }
                    }
                    catch {
                        //successfuldelete = false;
                    }
                }
            }
            return successfuldelete;
        }
    }
}
