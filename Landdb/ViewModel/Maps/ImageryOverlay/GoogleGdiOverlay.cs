﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ThinkGeo.MapSuite.WpfDesktopEdition;
using ThinkGeo.MapSuite.Core;
using System.Diagnostics;
using Landdb.ViewModel.Fields.FieldDetails.MapModels;
using Landdb.MapStyles.Overlays;
using System.Drawing;
using System.Net;
using Landdb.Client.Infrastructure;

namespace Landdb.ViewModel.Maps.ImageryOverlay {
    public class GoogleGdiOverlay : IMapOverlay {
        private Overlay overlay;
        GeographyUnit geographyunit;
        RectangleShape extent;
        string projectionstring;
        bool visibility;
        bool onlineOnly;
        int zoomLevelsNumber;
        LayerOverlayGoogle bingOverlay2 = new LayerOverlayGoogle();
        private int tileHeight = 512;
        private int tileWidth = 512;
        private string clientId;
        private string privateKey;
        private string apiKey;
        private GoogleMapsPictureFormat pictureFormat;
        private GoogleMapsMapType mapType;

        public GoogleGdiOverlay() {
            SetMapInfoSelectorItemToGoogle();
            extent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
            geographyunit = GeographyUnit.Meter;
            projectionstring = Proj4Projection.GetGoogleMapParametersString();
            visibility = true;
            onlineOnly = false;
            //overlay = NewOverlay();
            overlay = NewOnlineAPIOverlay();
        }

        public GoogleGdiOverlay(bool? allowofflinemap) {
            SetMapInfoSelectorItemToGoogle();
            extent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
            geographyunit = GeographyUnit.Meter;
            projectionstring = Proj4Projection.GetGoogleMapParametersString();
            visibility = true;
            onlineOnly = false;
            overlay = NewOnlineAPIOverlay();
            //if (!allowofflinemap.HasValue) {
            //    overlay = NewOverlay();
            //    overlay = NewAPIOverlay();
            //}
            //else if (allowofflinemap.HasValue && allowofflinemap.Value) {
            //    overlay = NewOverlay();
            //    overlay = NewAPIOverlay();
            //}
            //else {
            //    onlineOnly = true;
            //    //overlay = NewOnlineOverlay();
            //    overlay = NewOnlineAPIOverlay();
            //}
        }

        public string Name => MapInfoSelectorItem.MapTypeKeyText;
        public MapInfoSelectorItem MapInfoSelectorItem { get; set; }
        private void SetMapInfoSelectorItemToGoogle() {
            MapInfoSelectorItem = new MapInfoSelectorItem(MapStatusType.Google);
        }

        public Overlay Overlay {
            get { return overlay; }
            set { overlay = value; }
        }

        public GeographyUnit GeographyUnit {
            get { return geographyunit; }
            set { geographyunit = value; }
        }

        public RectangleShape Extent {
            get { return extent; }
            set { extent = value; }
        }

        public string ProjectionString {
            get { return projectionstring; }
            set { projectionstring = value; }
        }

        public bool Visibility {
            get { return visibility; }
            set { visibility = value; }
        }

        public bool OnlineOnly {
            get { return onlineOnly; }
            set { onlineOnly = value; }
        }

        public string ClientId {
            get {
                return this.clientId;
            }
            set {
                this.clientId = value;
            }
        }

        public string PrivateKey {
            get {
                return this.privateKey;
            }
            set {
                this.privateKey = value;
            }
        }

        public string ApiKey {
            get {
                return this.apiKey;
            }
            set {
                this.apiKey = value;
            }
        }
        public int ZoomLevelsNumber {
            get { return this.zoomLevelsNumber; }
            set { this.zoomLevelsNumber = value; }
        }

        public ZoomLevelSet GetZoomLevelSet() {
            ZoomLevelFactory zlf = new ZoomLevelFactory();
            return zlf.GetGoogleMapsZoomLevelSet();
        }

        public Overlay NewOverlay() {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            SetMapInfoSelectorItemToGoogle();
            extent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
            geographyunit = GeographyUnit.Meter;
            projectionstring = Proj4Projection.GetGoogleMapParametersString();
            visibility = true;
            onlineOnly = false;

            //// method 1: using the default without the Api key or clientId, limitation: 1 000 Static Maps requests per IP address per 24 hour period.
            //GoogleMapsLayer googlelayer = new GoogleMapsLayer();
            //// method 2: using Api key, limitation: 25 000 Static Maps requests per 24 hour period.
            //googlelayer.CreatingRequest += new EventHandler(googlelayer_CreatingRequest);
            //// method 3: using the request for business with ClientId, No limitation
            ////googlelayer.ClientId = "xxx";
            ////googlelayer.PrivateKey = "xxx";

            //static void googlelayer_CreatingRequest(object sender, CreatingRequestGoogleMapsLayerEventArgs e)
            //{
            //    e.RequestUri = new Uri(e.RequestUri.AbsoluteUri + "&key={API_KEY}");
            //}



            LayerOverlay bingOverlay = new LayerOverlay();
            //
            // Summary:
            //     Gets or sets a value to access the special features of Google Maps API Premier,
            //     you must provide a client ID when accessing any of the Premier API libraries
            //     or services. When registering for Google Maps API Premier, you will receive this
            //     client ID from Google Enterprise Support. All client IDs begin with a gme- prefix.

            //GoogleMapsLayer googleLayer = new GoogleMapsLayer("../data/", "gme-xxxx", "AIzaSyDgAyQUWUkFMP95Nxxxxxxxxxxxxxx"); 
            GoogleMapsLayer googlemaplayer = new GoogleMapsLayer();
            //
            // Summary:
            //     Gets or sets a value to access the special features of Google Maps API Premier,
            //     you must provide a client ID when accessing any of the Premier API libraries
            //     or services. When registering for Google Maps API Premier, you will receive this
            //     client ID from Google Enterprise Support. All client IDs begin with a gme- prefix.
            //googlemaplayer.ClientId = "gme-[OUR ID]";

            //
            // Summary:
            //     Gets or sets a value that is unique to your client ID, please keep your key secure.
            //googlemaplayer.PrivateKey = "[OUR KEY]";

            googlemaplayer.MapType = GoogleMapsMapType.Hybrid;
            googlemaplayer.TileMode = GoogleMapsTileMode.SingleTile;
            googlemaplayer.DrawingExceptionMode = DrawingExceptionMode.DrawException;
            bingOverlay.Layers.Add(googlemaplayer);


            //bingOverlay.MapType = GoogleMapsMapType.Terrain;  //plus road
            //bingOverlay.MapType = GoogleMapsMapType.Satellite;
            //bingOverlay.MapType = GoogleMapsMapType.Hybrid;  //satellite and road
            //bingOverlay.MapType = GoogleMapsMapType.RoadMap;
            bingOverlay.TileCache = new InMemoryBitmapTileCache();
            bingOverlay.ImageFormat = TileImageFormat.Jpeg;
            bingOverlay.TileBuffer = 1;

            string gDirectory = Landdb.Infrastructure.ApplicationEnvironment.GoogleMapCacheDirectory;
            //FileBitmapTileCache bitmapTileCache = new FileBitmapTileCache();
            EncryptedFileBitmapTileCache bitmapTileCache = new EncryptedFileBitmapTileCache();
            bitmapTileCache.CacheDirectory = gDirectory;
            bitmapTileCache.CacheId = "BaseCachedTiles";
            bitmapTileCache.TileAccessMode = TileAccessMode.Default;
            bitmapTileCache.ImageFormat = TileImageFormat.Jpeg;
            //.CacheDirectory = gDirectory;
            bingOverlay.TileCache = bitmapTileCache;

            bingOverlay.IsVisible = visibility;

            overlay = bingOverlay;
            sw.Stop();
            Console.WriteLine(sw.ElapsedMilliseconds);
            return bingOverlay;
        }

        public Overlay NewOnlineOverlay() {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            SetMapInfoSelectorItemToGoogle();
            extent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
            geographyunit = GeographyUnit.Meter;
            projectionstring = Proj4Projection.GetGoogleMapParametersString();
            visibility = true;
            onlineOnly = true;



            //// method 1: using the default without the Api key or clientId, limitation: 1 000 Static Maps requests per IP address per 24 hour period.
            //GoogleMapsLayer googlelayer = new GoogleMapsLayer();
            //// method 2: using Api key, limitation: 25 000 Static Maps requests per 24 hour period.
            //googlelayer.CreatingRequest += new EventHandler(googlelayer_CreatingRequest);
            //// method 3: using the request for business with ClientId, No limitation
            ////googlelayer.ClientId = "xxx";
            ////googlelayer.PrivateKey = "xxx";

            //static void googlelayer_CreatingRequest(object sender, CreatingRequestGoogleMapsLayerEventArgs e)
            //{
            //    e.RequestUri = new Uri(e.RequestUri.AbsoluteUri + "&key={API_KEY}");
            //}


            LayerOverlay bingOverlay = new LayerOverlay();

            //GoogleMapsLayer googleLayer = new GoogleMapsLayer("../data/", "gme-xxxx", "AIzaSyDgAyQUWUkFMP95Nxxxxxxxxxxxxxx"); 
            GoogleMapsLayer googlemaplayer = new GoogleMapsLayer();
            //
            // Summary:
            //     Gets or sets a value to access the special features of Google Maps API Premier,
            //     you must provide a client ID when accessing any of the Premier API libraries
            //     or services. When registering for Google Maps API Premier, you will receive this
            //     client ID from Google Enterprise Support. All client IDs begin with a gme- prefix.
            //googlemaplayer.ClientId = "gme-[OUR ID]";

            //
            // Summary:
            //     Gets or sets a value that is unique to your client ID, please keep your key secure.
            //googlemaplayer.PrivateKey = "[OUR KEY]";

            googlemaplayer.MapType = GoogleMapsMapType.Hybrid;
            googlemaplayer.TileMode = GoogleMapsTileMode.SingleTile;
            googlemaplayer.DrawingExceptionMode = DrawingExceptionMode.DrawException;
            bingOverlay.Layers.Add(googlemaplayer);

            //googlemaplayer.DrawingExceptionMode = DrawingExceptionMode.DrawException;
            //googlemaplayer.MapType = GoogleMapsMapType.Hybrid;
            //googlemaplayer.MapType = GoogleMapsMapType.Satellite;
            bingOverlay.Layers.Add("GoogleMapsLayer", googlemaplayer);

            //GoogleMapsOverlay bingOverlay = new GoogleMapsOverlay();
            //string googleid = "AIzaSyBQmIjpJZDQ0UDJuw0NZXoY897U_XkrLWY ";
            //string googlepassword = "AIzaSyBQmIjpJZDQ0UDJuw0NZXoY897U_XkrLWY ";

            //bingOverlay.ApplicationId = bingpassword;

            //bingOverlay.MapType = GoogleMapsMapType.Terrain;  //plus road
            //bingOverlay.MapType = GoogleMapsMapType.Satellite;
            //bingOverlay.MapType = GoogleMapsMapType.Hybrid;  //satellite and road
            //bingOverlay.MapType = GoogleMapsMapType.RoadMap;
            //bingOverlay.DrawingExceptionMode = DrawingExceptionMode.DrawException;
            //bingOverlay.ImageFormat = TileImageFormat.Jpeg;
            bingOverlay.IsVisible = visibility;
            overlay = bingOverlay;
            sw.Stop();
            Console.WriteLine(sw.ElapsedMilliseconds);
            return bingOverlay;
        }

        public Overlay NewAPIOverlay() {
            try {
                Stopwatch sw = new Stopwatch();
                sw.Start();
                SetMapInfoSelectorItemToGoogle();
                extent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
                geographyunit = GeographyUnit.Meter;
                projectionstring = Proj4Projection.GetGoogleMapParametersString();
                visibility = true;
                onlineOnly = false;

                //// method 1: using the default without the Api key or clientId, limitation: 1 000 Static Maps requests per IP address per 24 hour period.
                //GoogleMapsLayer googlelayer = new GoogleMapsLayer();
                //// method 2: using Api key, limitation: 25 000 Static Maps requests per 24 hour period.
                //googlelayer.CreatingRequest += new EventHandler(googlelayer_CreatingRequest);
                //// method 3: using the request for business with ClientId, No limitation
                ////googlelayer.ClientId = "xxx";
                ////googlelayer.PrivateKey = "xxx";

                //static void googlelayer_CreatingRequest(object sender, CreatingRequestGoogleMapsLayerEventArgs e)
                //{
                //    e.RequestUri = new Uri(e.RequestUri.AbsoluteUri + "&key={API_KEY}");
                //}


                //
                // Summary:
                //     Gets or sets a value to access the special features of Google Maps API Premier,
                //     you must provide a client ID when accessing any of the Premier API libraries
                //     or services. When registering for Google Maps API Premier, you will receive this
                //     client ID from Google Enterprise Support. All client IDs begin with a gme- prefix.

                //GoogleMapsLayer googleLayer = new GoogleMapsLayer("../data/", "gme-xxxx", "AIzaSyDgAyQUWUkFMP95Nxxxxxxxxxxxxxx"); 

                //
                // Summary:
                //     Gets or sets a value to access the special features of Google Maps API Premier,
                //     you must provide a client ID when accessing any of the Premier API libraries
                //     or services. When registering for Google Maps API Premier, you will receive this
                //     client ID from Google Enterprise Support. All client IDs begin with a gme- prefix.
                //googlemaplayer.ClientId = "gme-[OUR ID]";

                //
                // Summary:
                //     Gets or sets a value that is unique to your client ID, please keep your key secure.
                //googlemaplayer.PrivateKey = "[OUR KEY]";

                LayerOverlay bingOverlay = new LayerOverlay();
                GoogleMapsLayer googlemaplayer = new GoogleMapsLayer();
                googlemaplayer.CreatingRequest += (sender, e) => {
                    e.RequestUri = new Uri(e.RequestUri.AbsoluteUri + "&key=AIzaSyAdlNYBc_7sWnX0J-bU9hFik-bYPBuCaW0");
                };
                //googlemaplayer.Attribution = 
                googlemaplayer.TileHeight = 1024;
                googlemaplayer.TileWidth = 1024;
                //googlemaplayer.TileHeight = 4096;
                //googlemaplayer.TileWidth = 4096;
                //googlemaplayer.TileHeight = 8192;
                //googlemaplayer.TileWidth = 8192;
                googlemaplayer.MapType = GoogleMapsMapType.Hybrid;
                googlemaplayer.TileMode = GoogleMapsTileMode.MultiTile;
                googlemaplayer.DrawingExceptionMode = DrawingExceptionMode.DrawException;

                bingOverlay.Layers.Add(googlemaplayer);

                bingOverlay.TileCache = new InMemoryBitmapTileCache();
                bingOverlay.ImageFormat = TileImageFormat.Jpeg;
                bingOverlay.TileBuffer = 1;

                string gDirectory = Landdb.Infrastructure.ApplicationEnvironment.GoogleMapCacheDirectory;
                //FileBitmapTileCache bitmapTileCache = new FileBitmapTileCache();
                EncryptedFileBitmapTileCache bitmapTileCache = new EncryptedFileBitmapTileCache();
                bitmapTileCache.CacheDirectory = gDirectory;
                bitmapTileCache.CacheId = "BaseCachedTiles";
                bitmapTileCache.TileAccessMode = TileAccessMode.Default;
                bitmapTileCache.ImageFormat = TileImageFormat.Jpeg;
                //.CacheDirectory = gDirectory;
                bingOverlay.TileCache = bitmapTileCache;

                bingOverlay.IsVisible = visibility;
                overlay = bingOverlay;
                sw.Stop();
                Console.WriteLine(sw.ElapsedMilliseconds);
                return bingOverlay;
            }
            catch (Exception ex) {
                int i = 0;
                return null;
            }
        }

        public Overlay NewOnlineAPIOverlay() {
            try {
                Stopwatch sw = new Stopwatch();
                sw.Start();
                SetMapInfoSelectorItemToGoogle();
                extent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
                geographyunit = GeographyUnit.Meter;
                projectionstring = Proj4Projection.GetGoogleMapParametersString();
                visibility = true;
                onlineOnly = true;

                //GoogleMapsLayer googleLayer = new GoogleMapsLayer("../data/", "gme-xxxx", "AIzaSyDgAyQUWUkFMP95Nxxxxxxxxxxxxxx"); 

                //
                // Summary:
                //     Gets or sets a value to access the special features of Google Maps API Premier,
                //     you must provide a client ID when accessing any of the Premier API libraries
                //     or services. When registering for Google Maps API Premier, you will receive this
                //     client ID from Google Enterprise Support. All client IDs begin with a gme- prefix.
                //googlemaplayer.ClientId = "gme-[OUR ID]";

                //
                // Summary:
                //     Gets or sets a value that is unique to your client ID, please keep your key secure.
                //googlemaplayer.PrivateKey = "[OUR KEY]";

                //GoogleMapsOverlay bingOverlay1 = new GoogleMapsOverlay();
                //bingOverlay1.MapType = GoogleMapsMapType.Hybrid;
                //bingOverlay1.DrawingExceptionMode = DrawingExceptionMode.DrawException;
                //bingOverlay1.SendingWebRequest += (sender, e) => {
                //    Uri absoluteuri = e.WebRequest.RequestUri;
                //    string uristring = absoluteuri.AbsoluteUri;
                //    //e.WebRequest.RequestUri = new Uri(absoluteuri + "&key=AIzaSyAdlNYBc_7sWnX0J-bU9hFik-bYPBuCaW0");
                //};


                //GoogleMapsOverlay bingOverlay = new GoogleMapsOverlay();
                //bingOverlay.MapType = GoogleMapsMapType.Hybrid;
                //bingOverlay.DrawingExceptionMode = DrawingExceptionMode.DrawException;

                LayerOverlayGoogle bingOverlay = new LayerOverlayGoogle();
                //LayerOverlay bingOverlay = new LayerOverlay();
                //bingOverlay.Attribution = string.Format("©{0} Google - Map data ©{0} Tele Atlas, Imagery ©{0} TerraMetrics", DateTime.Today.Year);
                bingOverlay.Attribution = @"Google";
                //GoogleMapsLayer googlemaplayer = new GoogleMapsLayer();
                GoogleMapLayerControl googlemaplayer = new GoogleMapLayerControl();

                googlemaplayer.TileHeight = 640;
                googlemaplayer.TileWidth = 640;
                //googlemaplayer.ApiKey = @"AIzaSyAdlNYBc_7sWnX0J-bU9hFik-bYPBuCaW0";
                googlemaplayer.MapType = GoogleMapsMapType.Hybrid;
                googlemaplayer.TileMode = GoogleMapsTileMode.MultiTile;
                googlemaplayer.DrawingExceptionMode = DrawingExceptionMode.DrawException;
                bool xxxxx = true;
                googlemaplayer.CreatingRequest += (sender, e) => {
                    string absoluteuri = e.RequestUri.AbsoluteUri;
                    //absoluteuri.Replace(@"size=512x512", @"size=640x640");
                    //e.RequestUri = new Uri(absoluteuri + "&key=AIzaSyAdlNYBc_7sWnX0J-bU9hFik-bYPBuCaW0");
                    e.RequestUri = new Uri(absoluteuri + "&key=AIzaSyAdlNYBc_7sWnX0J-bU9hFik-bYPBuCaW0");
                };


                bingOverlay.Layers.Add(googlemaplayer);
                bingOverlay.Layers.Add("GoogleMapsLayer", googlemaplayer);


                bingOverlay.IsVisible = visibility;
                overlay = bingOverlay;
                bingOverlay2 = bingOverlay;
                sw.Stop();
                Console.WriteLine(sw.ElapsedMilliseconds);
                return bingOverlay;
            }
            catch (Exception ex) {
                int i = 0;
                return null;
            }
        }

        public Overlay GetImageOverlay(RectangleShape imageExtent, PointShape centerPoint) {
            GdiPlusRasterLayer layer = new GdiPlusRasterLayer(@"C:\Temp", imageExtent);
            layer.DrawingExceptionMode = DrawingExceptionMode.DrawException;
            layer.StreamLoading += (sender, e) => {
                //e.AlternateStream =
            };



                bingOverlay2.Layers.Clear();
            bingOverlay2.Layers.Add(layer);
            bingOverlay2.Layers.Add("GoogleMapsLayer", layer);
            bingOverlay2.IsVisible = visibility;
            return bingOverlay2;
        }


        protected virtual Bitmap GetGoogleMapsImageCore(double longitude, double latitude, int zoomLevelNumber, double tileWidth, double tileHeight) {
            string uriString = this.vD4(longitude, latitude, zoomLevelNumber, tileWidth, tileHeight);
            System.IO.Stream stream = null;
            System.IO.Stream currentstream = null;
            Bitmap bitmap = null;
            try {
                //CreatingRequestGoogleMapsLayerEventArgs creatingRequestGoogleMapsLayerEventArgs = new CreatingRequestGoogleMapsLayerEventArgs(new Uri(uriString));
                //this.OnCreatingRequest(creatingRequestGoogleMapsLayerEventArgs);
                //WebRequest webRequest = WebRequest.Create(creatingRequestGoogleMapsLayerEventArgs.RequestUri);
                //webRequest.Timeout = this.timeoutInSeconds * 1000;
                //webRequest.Proxy = this.webProxy;
                //WebResponse webResponse = this.SendWebRequest(webRequest);
                ////stream = webResponse.GetResponseStream();
                //currentstream = webResponse.GetResponseStream();
                //bitmap = new Bitmap(currentstream);
            }
            catch (System.Exception ex) {
                //if (this.sD4 == null) {
                //    this.sD4 = ex;
                //}
            }
            finally {
                if (bitmap == null) {
                    bitmap = new Bitmap(1, 1);
                }
                if (stream != null) {
                    stream.Dispose();
                }
            }
            return bitmap;
        }

        private string vD4(double lon, double lat, int zoomLevelNumber, double tileWidth, double tileHeight) {
            string text = vT4(tileWidth, tileHeight, zoomLevelNumber, lon, lat);
            System.Text.StringBuilder stringBuilder = new System.Text.StringBuilder("http://maps.googleapis.com/maps/api/staticmap?");
            stringBuilder.AppendFormat(System.Globalization.CultureInfo.InvariantCulture, "{0}&", new object[]
          {
                text
                    });
            stringBuilder.AppendFormat(System.Globalization.CultureInfo.InvariantCulture, "maptype={0}&", new object[]
          {
                this.Ays()
                    });
            stringBuilder.AppendFormat(System.Globalization.CultureInfo.InvariantCulture, "format={0}&", new object[]
          {
                this.GetPictureFormat()
                    });
            stringBuilder.Append("sensor=false");
            if (!string.IsNullOrEmpty(this.clientId) && !string.IsNullOrEmpty(this.privateKey)) {
                stringBuilder.AppendFormat(System.Globalization.CultureInfo.InvariantCulture, "&client={0}", new object[]
              {
                    this.clientId
                        });
                stringBuilder.AppendFormat(System.Globalization.CultureInfo.InvariantCulture, "&signature={0}", new object[]
              {
                    this.xD4(stringBuilder.ToString())
                        });
            }
            if (!string.IsNullOrEmpty(this.apiKey) && string.IsNullOrEmpty(this.clientId)) {
                stringBuilder.AppendFormat(System.Globalization.CultureInfo.InvariantCulture, "&key={0}", new object[]
              {
                    this.clientId
                        });
            }
            return stringBuilder.ToString();
        }


        private static string vT4(double newWidth, double newHeight, int zoomLevelNumber, double longitude, double latitude) {
            System.Text.StringBuilder stringBuilder = new System.Text.StringBuilder();
            stringBuilder.AppendFormat(System.Globalization.CultureInfo.InvariantCulture, "center={0},{1}&", new object[]
          {
                System.Math.Round(latitude, 6),
                System.Math.Round(longitude, 6)
                    });
            stringBuilder.AppendFormat(System.Globalization.CultureInfo.InvariantCulture, "zoom={0}&", new object[]
          {
                zoomLevelNumber
                    });
            stringBuilder.AppendFormat(System.Globalization.CultureInfo.InvariantCulture, "size={0}x{1}", new object[]
          {
                (int)newWidth,
                (int)newHeight
                    });
            return stringBuilder.ToString();
        }

        private string Ays() {
            switch (this.mapType) {
                case GoogleMapsMapType.RoadMap:
                    return "roadmap";
                case GoogleMapsMapType.Mobile:
                    return "mobile";
                case GoogleMapsMapType.Satellite:
                    return "satellite";
                case GoogleMapsMapType.Terrain:
                    return "terrain";
                case GoogleMapsMapType.Hybrid:
                    return "hybrid";
                default:
                    return "roadmap";
            }
        }
        private static string Ays(GoogleMapsMapType mapType) {
            switch (mapType) {
                case GoogleMapsMapType.RoadMap:
                    return "roadmap";
                case GoogleMapsMapType.Mobile:
                    return "mobile";
                case GoogleMapsMapType.Satellite:
                    return "satellite";
                case GoogleMapsMapType.Terrain:
                    return "terrain";
                case GoogleMapsMapType.Hybrid:
                    return "hybrid";
                default:
                    return "roadmap";
            }
        }

        public string GetPictureFormat() {
            GoogleMapsPictureFormat googleMapsPictureFormat = this.pictureFormat;
            switch (googleMapsPictureFormat) {
                case GoogleMapsPictureFormat.Jpeg:
                    return "jpg-baseline";
                case GoogleMapsPictureFormat.Gif:
                    return "gif";
                case GoogleMapsPictureFormat.Png8:
                    return "png8";
                default:
                    if (googleMapsPictureFormat != GoogleMapsPictureFormat.Png32) {
                        return "jpg-baseline";
                    }
                    return "png32";
            }
        }
        private string xD4(string url) {
            //7ys=.JhQ=(this.PrivateKey, "PrivateKey");
            string s = this.PrivateKey.Replace("-", "+").Replace("_", "/");
            byte[] key = System.Convert.FromBase64String(s);
            Uri uri = new Uri(url);
            byte[] buffer = xT4(uri.LocalPath + uri.Query);
            System.Security.Cryptography.HMACSHA1 hMACSHA = new System.Security.Cryptography.HMACSHA1(key);
            byte[] inArray = hMACSHA.ComputeHash(buffer);
            return System.Convert.ToBase64String(inArray).Replace("+", "-").Replace("/", "_");
        }

        private static byte[] xT4(string value) {
            byte[] bytes = System.Text.Encoding.Unicode.GetBytes(value);
            byte[] array = new byte[bytes.Length / 2];
            for (int i = 0; i < bytes.Length; i += 2) {
                array[i / 2] = bytes[i];
                if (array[i / 2] > 127) {
                    array[i / 2] = 63;
                }
            }
            return array;
        }
        public Overlay ExtentHasChanged(int tileHeight, int tileWidth, RectangleShape extent, RectangleShape projextent, PointShape wgscenterpoint, int zoomlevelnumber, int epsgcode) {
            return null;
        }
    }
}
