﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ThinkGeo.MapSuite.WpfDesktopEdition;
using ThinkGeo.MapSuite.Core;
using System.Diagnostics;
using Landdb.ViewModel.Fields.FieldDetails.MapModels;
using Landdb.MapStyles.Overlays;
using Landdb.Client.Infrastructure;

namespace Landdb.ViewModel.Maps.ImageryOverlay {
    public class GoogleMapOverlay : IMapOverlay {
        private Overlay overlay;
        GeographyUnit geographyunit;
        RectangleShape extent;
        string projectionstring;
        bool visibility;
        bool onlineOnly;
        int zoomLevelsNumber;

        public GoogleMapOverlay() {
            SetMapInfoSelectorItemToGoogleOffline();
            extent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
            geographyunit = GeographyUnit.Meter;
            projectionstring = Proj4Projection.GetGoogleMapParametersString();
            //projectionstring = Proj4Projection.GetSphericalMercatorParametersString();
            //projectionstring = Proj4Projection.GetEpsgParametersString(3857);
            visibility = true;
            onlineOnly = false;
            //overlay = NewOverlay();
            overlay = NewOnlineAPIOverlay();
        }

        public GoogleMapOverlay(bool? allowofflinemap) {
            SetMapInfoSelectorItemToGoogleOffline();
            extent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
            geographyunit = GeographyUnit.Meter;
            projectionstring = Proj4Projection.GetGoogleMapParametersString();
            //projectionstring = Proj4Projection.GetSphericalMercatorParametersString();
            //projectionstring = Proj4Projection.GetEpsgParametersString(3857);
            visibility = true;
            onlineOnly = false;
            overlay = NewOnlineAPIOverlay();
            //if (!allowofflinemap.HasValue) {
            //    overlay = NewOverlay();
            //    overlay = NewAPIOverlay();
            //}
            //else if (allowofflinemap.HasValue && allowofflinemap.Value) {
            //    overlay = NewOverlay();
            //    overlay = NewAPIOverlay();
            //}
            //else {
            //    onlineOnly = true;
            //    //overlay = NewOnlineOverlay();
            //    overlay = NewOnlineAPIOverlay();
            //}
        }

        public string Name => MapInfoSelectorItem.MapTypeKeyText;
        public MapInfoSelectorItem MapInfoSelectorItem { get; set; }

        private void SetMapInfoSelectorItemToGoogleOffline() {
            MapInfoSelectorItem = new MapInfoSelectorItem(MapStatusType.GoogleOffline);
        }
        public Overlay Overlay {
            get { return overlay; }
            set { overlay = value; }
        }

        public GeographyUnit GeographyUnit {
            get { return geographyunit; }
            set { geographyunit = value; }
        }

        public RectangleShape Extent {
            get { return extent; }
            set { extent = value; }
        }

        public string ProjectionString {
            get { return projectionstring; }
            set { projectionstring = value; }
        }

        public bool Visibility {
            get { return visibility; }
            set { visibility = value; }
        }

        public bool OnlineOnly {
            get { return onlineOnly; }
            set { onlineOnly = value; }
        }

        public int ZoomLevelsNumber {
            get { return this.zoomLevelsNumber; }
            set { this.zoomLevelsNumber = value; }
        }

        public ZoomLevelSet GetZoomLevelSet() {
            //ZoomLevelFactory zlf = new ZoomLevelFactory();
            //return zlf.GetGoogleMapsZoomLevelSet();
            ZoomLevelFactory zoomlevelfactory = new ZoomLevelFactory("Google40");
            return zoomlevelfactory.ZoomLevelSET;
        }

        public Overlay NewOverlay() {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            SetMapInfoSelectorItemToGoogleOffline();
            extent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
            geographyunit = GeographyUnit.Meter;
            projectionstring = Proj4Projection.GetGoogleMapParametersString();
            //projectionstring = Proj4Projection.GetSphericalMercatorParametersString();
            //projectionstring = Proj4Projection.GetEpsgParametersString(3857);
            visibility = true;
            onlineOnly = false;

            //// method 1: using the default without the Api key or clientId, limitation: 1 000 Static Maps requests per IP address per 24 hour period.
            //GoogleMapsLayer googlelayer = new GoogleMapsLayer();
            //// method 2: using Api key, limitation: 25 000 Static Maps requests per 24 hour period.
            //googlelayer.CreatingRequest += new EventHandler(googlelayer_CreatingRequest);
            //// method 3: using the request for business with ClientId, No limitation
            ////googlelayer.ClientId = "xxx";
            ////googlelayer.PrivateKey = "xxx";

            //static void googlelayer_CreatingRequest(object sender, CreatingRequestGoogleMapsLayerEventArgs e)
            //{
            //    e.RequestUri = new Uri(e.RequestUri.AbsoluteUri + "&key={API_KEY}");
            //}



            LayerOverlay bingOverlay = new LayerOverlay();
            //
            // Summary:
            //     Gets or sets a value to access the special features of Google Maps API Premier,
            //     you must provide a client ID when accessing any of the Premier API libraries
            //     or services. When registering for Google Maps API Premier, you will receive this
            //     client ID from Google Enterprise Support. All client IDs begin with a gme- prefix.

            //GoogleMapsLayer googleLayer = new GoogleMapsLayer("../data/", "gme-xxxx", "AIzaSyDgAyQUWUkFMP95Nxxxxxxxxxxxxxx"); 
            GoogleMapsLayer googlemaplayer = new GoogleMapsLayer();
            //
            // Summary:
            //     Gets or sets a value to access the special features of Google Maps API Premier,
            //     you must provide a client ID when accessing any of the Premier API libraries
            //     or services. When registering for Google Maps API Premier, you will receive this
            //     client ID from Google Enterprise Support. All client IDs begin with a gme- prefix.
            //googlemaplayer.ClientId = "gme-[OUR ID]";

            //
            // Summary:
            //     Gets or sets a value that is unique to your client ID, please keep your key secure.
            //googlemaplayer.PrivateKey = "[OUR KEY]";

            googlemaplayer.MapType = GoogleMapsMapType.Hybrid;
            googlemaplayer.TileMode = GoogleMapsTileMode.SingleTile;
            googlemaplayer.DrawingExceptionMode = DrawingExceptionMode.DrawException;
            bingOverlay.Layers.Add(googlemaplayer);


            //bingOverlay.MapType = GoogleMapsMapType.Terrain;  //plus road
            //bingOverlay.MapType = GoogleMapsMapType.Satellite;
            //bingOverlay.MapType = GoogleMapsMapType.Hybrid;  //satellite and road
            //bingOverlay.MapType = GoogleMapsMapType.RoadMap;
            bingOverlay.TileCache = new InMemoryBitmapTileCache();
            bingOverlay.ImageFormat = TileImageFormat.Jpeg;
            bingOverlay.TileBuffer = 1;

            string gDirectory = Landdb.Infrastructure.ApplicationEnvironment.GoogleMapCacheDirectory;
            //FileBitmapTileCache bitmapTileCache = new FileBitmapTileCache();
            EncryptedFileBitmapTileCache bitmapTileCache = new EncryptedFileBitmapTileCache();
            bitmapTileCache.CacheDirectory = gDirectory;
            bitmapTileCache.CacheId = "BaseCachedTiles";
            bitmapTileCache.TileAccessMode = TileAccessMode.Default;
            bitmapTileCache.ImageFormat = TileImageFormat.Jpeg;
            //.CacheDirectory = gDirectory;
            bingOverlay.TileCache = bitmapTileCache;
            
            bingOverlay.IsVisible = visibility;

            overlay = bingOverlay;
            sw.Stop();
            Console.WriteLine(sw.ElapsedMilliseconds);
            return bingOverlay;
        }

        public Overlay NewOnlineOverlay() {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            SetMapInfoSelectorItemToGoogleOffline();
            extent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
            geographyunit = GeographyUnit.Meter;
            projectionstring = Proj4Projection.GetGoogleMapParametersString();
            //projectionstring = Proj4Projection.GetSphericalMercatorParametersString();
            //projectionstring = Proj4Projection.GetEpsgParametersString(3857);
            visibility = true;
            onlineOnly = true;



            //// method 1: using the default without the Api key or clientId, limitation: 1 000 Static Maps requests per IP address per 24 hour period.
            //GoogleMapsLayer googlelayer = new GoogleMapsLayer();
            //// method 2: using Api key, limitation: 25 000 Static Maps requests per 24 hour period.
            //googlelayer.CreatingRequest += new EventHandler(googlelayer_CreatingRequest);
            //// method 3: using the request for business with ClientId, No limitation
            ////googlelayer.ClientId = "xxx";
            ////googlelayer.PrivateKey = "xxx";

            //static void googlelayer_CreatingRequest(object sender, CreatingRequestGoogleMapsLayerEventArgs e)
            //{
            //    e.RequestUri = new Uri(e.RequestUri.AbsoluteUri + "&key={API_KEY}");
            //}
            

            LayerOverlay bingOverlay = new LayerOverlay();

            //GoogleMapsLayer googleLayer = new GoogleMapsLayer("../data/", "gme-xxxx", "AIzaSyDgAyQUWUkFMP95Nxxxxxxxxxxxxxx"); 
            GoogleMapsLayer googlemaplayer = new GoogleMapsLayer();
            //
            // Summary:
            //     Gets or sets a value to access the special features of Google Maps API Premier,
            //     you must provide a client ID when accessing any of the Premier API libraries
            //     or services. When registering for Google Maps API Premier, you will receive this
            //     client ID from Google Enterprise Support. All client IDs begin with a gme- prefix.
            //googlemaplayer.ClientId = "gme-[OUR ID]";

            //
            // Summary:
            //     Gets or sets a value that is unique to your client ID, please keep your key secure.
            //googlemaplayer.PrivateKey = "[OUR KEY]";

            googlemaplayer.MapType = GoogleMapsMapType.Hybrid;
            googlemaplayer.TileMode = GoogleMapsTileMode.SingleTile;
            googlemaplayer.DrawingExceptionMode = DrawingExceptionMode.DrawException;
            bingOverlay.Layers.Add(googlemaplayer);

            //googlemaplayer.DrawingExceptionMode = DrawingExceptionMode.DrawException;
            //googlemaplayer.MapType = GoogleMapsMapType.Hybrid;
            //googlemaplayer.MapType = GoogleMapsMapType.Satellite;
            bingOverlay.Layers.Add("GoogleMapsLayer", googlemaplayer);

            //GoogleMapsOverlay bingOverlay = new GoogleMapsOverlay();
            //string googleid = "AIzaSyBQmIjpJZDQ0UDJuw0NZXoY897U_XkrLWY ";
            //string googlepassword = "AIzaSyBQmIjpJZDQ0UDJuw0NZXoY897U_XkrLWY ";

            //bingOverlay.ApplicationId = bingpassword;
            
            //bingOverlay.MapType = GoogleMapsMapType.Terrain;  //plus road
            //bingOverlay.MapType = GoogleMapsMapType.Satellite;
            //bingOverlay.MapType = GoogleMapsMapType.Hybrid;  //satellite and road
            //bingOverlay.MapType = GoogleMapsMapType.RoadMap;
            //bingOverlay.DrawingExceptionMode = DrawingExceptionMode.DrawException;
            //bingOverlay.ImageFormat = TileImageFormat.Jpeg;
            bingOverlay.IsVisible = visibility;
            overlay = bingOverlay;
            sw.Stop();
            Console.WriteLine(sw.ElapsedMilliseconds);
            return bingOverlay;
        }

        public Overlay NewAPIOverlay() {
            try {
                Stopwatch sw = new Stopwatch();
                sw.Start();
                SetMapInfoSelectorItemToGoogleOffline();
                extent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
                geographyunit = GeographyUnit.Meter;
                projectionstring = Proj4Projection.GetGoogleMapParametersString();
                //projectionstring = Proj4Projection.GetSphericalMercatorParametersString();
                //projectionstring = Proj4Projection.GetEpsgParametersString(3857);
                visibility = true;
                onlineOnly = false;

                //// method 1: using the default without the Api key or clientId, limitation: 1 000 Static Maps requests per IP address per 24 hour period.
                //GoogleMapsLayer googlelayer = new GoogleMapsLayer();
                //// method 2: using Api key, limitation: 25 000 Static Maps requests per 24 hour period.
                //googlelayer.CreatingRequest += new EventHandler(googlelayer_CreatingRequest);
                //// method 3: using the request for business with ClientId, No limitation
                ////googlelayer.ClientId = "xxx";
                ////googlelayer.PrivateKey = "xxx";

                //static void googlelayer_CreatingRequest(object sender, CreatingRequestGoogleMapsLayerEventArgs e)
                //{
                //    e.RequestUri = new Uri(e.RequestUri.AbsoluteUri + "&key={API_KEY}");
                //}


                //
                // Summary:
                //     Gets or sets a value to access the special features of Google Maps API Premier,
                //     you must provide a client ID when accessing any of the Premier API libraries
                //     or services. When registering for Google Maps API Premier, you will receive this
                //     client ID from Google Enterprise Support. All client IDs begin with a gme- prefix.

                //GoogleMapsLayer googleLayer = new GoogleMapsLayer("../data/", "gme-xxxx", "AIzaSyDgAyQUWUkFMP95Nxxxxxxxxxxxxxx"); 

                //
                // Summary:
                //     Gets or sets a value to access the special features of Google Maps API Premier,
                //     you must provide a client ID when accessing any of the Premier API libraries
                //     or services. When registering for Google Maps API Premier, you will receive this
                //     client ID from Google Enterprise Support. All client IDs begin with a gme- prefix.
                //googlemaplayer.ClientId = "gme-[OUR ID]";

                //
                // Summary:
                //     Gets or sets a value that is unique to your client ID, please keep your key secure.
                //googlemaplayer.PrivateKey = "[OUR KEY]";

                LayerOverlay bingOverlay = new LayerOverlay();
                GoogleMapsLayer googlemaplayer = new GoogleMapsLayer();
                googlemaplayer.CreatingRequest += (sender, e) => {
                    e.RequestUri = new Uri(e.RequestUri.AbsoluteUri + "&key=AIzaSyAdlNYBc_7sWnX0J-bU9hFik-bYPBuCaW0");
                };
                //googlemaplayer.Attribution = 
                googlemaplayer.TileHeight = 640;
                googlemaplayer.TileWidth = 640;
                //googlemaplayer.TileHeight = 4096;
                //googlemaplayer.TileWidth = 4096;
                //googlemaplayer.TileHeight = 8192;
                //googlemaplayer.TileWidth = 8192;
                googlemaplayer.MapType = GoogleMapsMapType.Hybrid;
                googlemaplayer.TileMode = GoogleMapsTileMode.MultiTile;
                googlemaplayer.DrawingExceptionMode = DrawingExceptionMode.DrawException;

                bingOverlay.Layers.Add(googlemaplayer);

                bingOverlay.TileCache = new InMemoryBitmapTileCache();
                bingOverlay.ImageFormat = TileImageFormat.Jpeg;
                bingOverlay.TileBuffer = 1;

                string gDirectory = Landdb.Infrastructure.ApplicationEnvironment.GoogleMapCacheDirectory;
                //FileBitmapTileCache bitmapTileCache = new FileBitmapTileCache();
                EncryptedFileBitmapTileCache bitmapTileCache = new EncryptedFileBitmapTileCache();
                bitmapTileCache.CacheDirectory = gDirectory;
                bitmapTileCache.CacheId = "BaseCachedTiles";
                bitmapTileCache.TileAccessMode = TileAccessMode.Default;
                bitmapTileCache.ImageFormat = TileImageFormat.Jpeg;
                //.CacheDirectory = gDirectory;
                bingOverlay.TileCache = bitmapTileCache;

                bingOverlay.IsVisible = visibility;
                overlay = bingOverlay;
                sw.Stop();
                Console.WriteLine(sw.ElapsedMilliseconds);
                return bingOverlay;
            }
            catch (Exception ex) {
                int i = 0;
                return null;
            }
        }

        public Overlay NewOnlineAPIOverlay() {
            try {
                Stopwatch sw = new Stopwatch();
                sw.Start();
                SetMapInfoSelectorItemToGoogleOffline();
                extent = new RectangleShape(-13914223.6889783, 6341665.61942584, -7800525.36425821, 2894164.80692393);
                geographyunit = GeographyUnit.Meter;
                projectionstring = Proj4Projection.GetGoogleMapParametersString();
                //projectionstring = Proj4Projection.GetSphericalMercatorParametersString();
                //projectionstring = Proj4Projection.GetEpsgParametersString(3857);
                visibility = true;
                onlineOnly = true;

                //GoogleMapsLayer googleLayer = new GoogleMapsLayer("../data/", "gme-xxxx", "AIzaSyDgAyQUWUkFMP95Nxxxxxxxxxxxxxx"); 

                //
                // Summary:
                //     Gets or sets a value to access the special features of Google Maps API Premier,
                //     you must provide a client ID when accessing any of the Premier API libraries
                //     or services. When registering for Google Maps API Premier, you will receive this
                //     client ID from Google Enterprise Support. All client IDs begin with a gme- prefix.
                //googlemaplayer.ClientId = "gme-[OUR ID]";

                //
                // Summary:
                //     Gets or sets a value that is unique to your client ID, please keep your key secure.
                //googlemaplayer.PrivateKey = "[OUR KEY]";

                //GoogleMapsOverlay bingOverlay1 = new GoogleMapsOverlay();
                //bingOverlay1.MapType = GoogleMapsMapType.Hybrid;
                //bingOverlay1.DrawingExceptionMode = DrawingExceptionMode.DrawException;
                //bingOverlay1.SendingWebRequest += (sender, e) => {
                //    Uri absoluteuri = e.WebRequest.RequestUri;
                //    string uristring = absoluteuri.AbsoluteUri;
                //    //e.WebRequest.RequestUri = new Uri(absoluteuri + "&key=AIzaSyAdlNYBc_7sWnX0J-bU9hFik-bYPBuCaW0");
                //};


                //GoogleMapsOverlay bingOverlay = new GoogleMapsOverlay();
                //bingOverlay.MapType = GoogleMapsMapType.Hybrid;
                //bingOverlay.DrawingExceptionMode = DrawingExceptionMode.DrawException;

                //LayerOverlayGoogle bingOverlay = new LayerOverlayGoogle();
                LayerOverlay bingOverlay = new LayerOverlay();
                //bingOverlay.Attribution = string.Format("©{0} Google - Map data ©{0} Tele Atlas, Imagery ©{0} TerraMetrics", DateTime.Today.Year);
                //bingOverlay.Attribution = @"Google";
                GoogleMapsLayer googlemaplayer = new GoogleMapsLayer();
                //GoogleMapLayerControl googlemaplayer = new GoogleMapLayerControl();

                googlemaplayer.TileHeight = 640;
                googlemaplayer.TileWidth = 640;
                googlemaplayer.MapType = GoogleMapsMapType.Hybrid;
                googlemaplayer.TileMode = GoogleMapsTileMode.MultiTile;
                googlemaplayer.DrawingExceptionMode = DrawingExceptionMode.DrawException;
                googlemaplayer.CreatingRequest += (sender, e) => {
                    string absoluteuri = e.RequestUri.AbsoluteUri;
                    //absoluteuri.Replace(@"size=512x512", @"size=640x640");
                    //e.RequestUri = new Uri(absoluteuri + "&key=AIzaSyAdlNYBc_7sWnX0J-bU9hFik-bYPBuCaW0");
                     e.RequestUri = new Uri(absoluteuri + "&key=AIzaSyAdlNYBc_7sWnX0J-bU9hFik-bYPBuCaW0");
                };





                bingOverlay.Layers.Add(googlemaplayer);
                bingOverlay.Layers.Add("GoogleMapsLayer", googlemaplayer);
                bingOverlay.IsVisible = visibility;
                overlay = bingOverlay;
                sw.Stop();
                Console.WriteLine(sw.ElapsedMilliseconds);
                return bingOverlay;
            }
            catch (Exception ex) {
                int i = 0;
                return null;
            }
        }
        public Overlay ExtentHasChanged(int tileHeight, int tileWidth, RectangleShape extent, RectangleShape projextent, PointShape wgscenterpoint, int zoomlevelnumber, int epsgcode) {
            return null;
        }
    }
}
