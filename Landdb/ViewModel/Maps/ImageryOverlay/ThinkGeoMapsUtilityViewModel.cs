﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows.Shapes;
using Microsoft.Maps.MapControl.WPF;
using ThinkGeo.MapSuite.Core;
using ThinkGeo.MapSuite.WpfDesktopEdition;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using System.Collections.ObjectModel;
using Landdb.Infrastructure.Messages;

namespace Landdb.ViewModel.Maps.ImageryOverlay {
    public class ThinkGeoMapsUtilityViewModel : ViewModelBase {
        //static ThinkGeoMapsUtility defaultInstance = new ThinkGeoMapsUtility();
        //public static ThinkGeoMapsUtility Default { get { return defaultInstance; } }
        IMapOverlay selectedMap;
        ObservableCollection<IMapOverlay> mapoverlays;
        IDictionary<string, Func<IMapOverlay>> dict;

        public ThinkGeoMapsUtilityViewModel() {
            dict = new Dictionary<string, Func<IMapOverlay>>();

            //dict.Add("Bing", () => { return new BingMapOverlay(); });
            //dict.Add("Bing Online", () => { return new BingFastMapsOverlay(); });
            //dict.Add("Roads Only", () => { return new BingRoadMapsOverlay(); });
            //dict.Add("USGS", () => { return new USGSMapOverlay(); });
            //dict.Add("WorldMapKit", () => { return new WorldMapKitOverlay(); });

            mapoverlays = new ObservableCollection<IMapOverlay>();
            mapoverlays.Add(GetSelectedMapFromFactory("Bing"));
            //mapoverlays.Add(GetSelectedMapFromFactory("Bing Online"));
            mapoverlays.Add(GetSelectedMapFromFactory("Roads Only"));
            //mapoverlays.Add(GetSelectedMapFromFactory("USGS"));
            //mapoverlays.Add(GetSelectedMapFromFactory("WorldMapKit"));

            selectedMap = GetSelectedMapFromFactory("Bing");
        }

        public ObservableCollection<IMapOverlay> MapOverlays {
            get { return mapoverlays; }
            set {
                if (mapoverlays == value) { return; }
                mapoverlays = value;
                RaisePropertyChanged("MapOverlays");
            }
        }

        public IMapOverlay GetSelectedMapFromFactory(string key) {
            return dict[key].Invoke();
        }

        public IMapOverlay SelectedMap {
            get { return selectedMap; }
            set { 
                if (selectedMap == value) { return; }
                if (selectedMap == null) { return; }
                if (string.IsNullOrWhiteSpace(value.Name)) { return; }
                selectedMap = value;
                RaisePropertyChanged("SelectedMap");
                //Messenger.Default.Send<ImageryChangedMessage>(new ImageryChangedMessage() { MapOverlay = selectedMap });
            }
        }

        public Overlay SelectedOverlay {
            get {
                bool ShouldShowBingMaps = Landdb.Client.Infrastructure.NetworkStatus.IsInternetAvailableFast();
                if (selectedMap.OnlineOnly) {
                    if (ShouldShowBingMaps) {
                        return selectedMap.Overlay;
                    }
                    else {
                        return null;
                    }
                }
                return selectedMap.Overlay;
            }
        }

        public Overlay LoadThinkGeoBingMapsOverlay() {
            return SelectedOverlay;
        }
    }
}