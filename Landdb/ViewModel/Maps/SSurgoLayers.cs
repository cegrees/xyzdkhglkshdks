﻿using Landdb.Resources;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Map;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using Landdb.ViewModel.Secondary.Map;
using NLog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Windows.Threading;
using ThinkGeo.MapSuite.Core;
using AgC.UnitConversion;
using Landdb.Client.Infrastructure.DisplayItems;
using Landdb.MapStyles;
using ThinkGeo.MapSuite.Core;

namespace Landdb.ViewModel.Maps {
    public class SSurgoLayers : ViewModelBase, IMapFilter {
        MapsPageViewModel mapspage;
        readonly Logger log = LogManager.GetCurrentClassLogger();
        Guid currentDataSourceId = Guid.Empty;
        int currentCropYear = 0;
        string name = string.Empty;
        bool showthetoggle = true;
        ColorItem selectedcoloritem = null;
        int colormultiplier = 50;
        int randomchoice = 5;
        IList<Color> usedcolors = new List<Color>();
        ObservableCollection<ColorItem> coloritems = new ObservableCollection<ColorItem>();
        Dictionary<string, string> cropzonecrops = new Dictionary<string, string>();
        Dictionary<string, Color> cropzonecolors = new Dictionary<string, Color>();
        Dictionary<string, ColorItem> coloritemsindex = new Dictionary<string, ColorItem>();
        IClientEndpoint clientEndpoint;
        MapSettings mapsettings;
        LegendAdornmentLayer legendLayer = new LegendAdornmentLayer();
        int cmgopacity = 255;

        public SSurgoLayers(MapsPageViewModel mapspage, IClientEndpoint clientEndpoint) {
            this.mapspage = mapspage;
            this.clientEndpoint = clientEndpoint;
            InitializeFilter();

            ToggleSSurgoColorCommand = new RelayCommand<ColorItem>(ToggleThisItem);
            ConfigureSSurgoColorCommand = new RelayCommand(ChangeTheColor);
        }

        public ICommand ToggleSSurgoColorCommand { get; private set; }
        public ICommand ConfigureSSurgoColorCommand { get; private set; }

        public string Name => Strings.FilterType_SSurgo;

        public FilterType Filter => FilterType.SSurgoLayers;

        public ObservableCollection<ColorItem> ColorItems {
            get { return coloritems; }
            set {
                if (coloritems == value) { return; }
                coloritems = value;
                OnColorUpdated();
                RaisePropertyChanged("ColorItem");
                RaisePropertyChanged("ColorItems");
                RaisePropertyChanged("CropzoneColors");
            }
        }

        public ColorItem SelectedColorItem {
            get { return selectedcoloritem; }
            set {
                if (selectedcoloritem == value) { return; }
                selectedcoloritem = value;
                RaisePropertyChanged("SelectedColorItem");
                RaisePropertyChanged("ColorItems");
                RaisePropertyChanged("CropzoneColors");
            }
        }

        public Dictionary<string, Color> CropzoneColors {
            get { return cropzonecolors; }
            set {
                if (cropzonecolors == value) { return; }
                cropzonecolors = value;
                RaisePropertyChanged("CropzoneColors");
            }
        }

        public IList<string> HatchTypeList {
            get { return HatchStyleFactory.GetHatchStyleList(); }
        }
        public IList<Color> UsedColors {
            get { return usedcolors; }
        }

        public Dictionary<string, Color> Execute(IClientEndpoint clientEndpoint, Random rand) {
            if (CropzoneColors.Count > 0) { return CropzoneColors; }
            InitializeFilter();
            ObservableCollection<ColorItem> CI = new ObservableCollection<ColorItem>();
            mapsettings = clientEndpoint.GetMapSettings();
            cmgopacity = mapsettings.UnselectedOpacity;

            var fulldirectoryname = Path.Combine(ApplicationEnvironment.LayerCacheDirectory, @"SSurgo\" );
            if (!Directory.Exists(ApplicationEnvironment.LayerCacheDirectory)) {
                Directory.CreateDirectory(ApplicationEnvironment.LayerCacheDirectory);
            }
            if (!Directory.Exists(fulldirectoryname)) {
                Directory.CreateDirectory(fulldirectoryname);
            }

            DirectoryInfo dir = new DirectoryInfo(Path.GetDirectoryName(fulldirectoryname));
            //FileInfo[] files = dir.GetFiles("*.dat");
            //FileInfo[] files = dir.GetFiles("*.xml");
            FileInfo[] files = dir.GetFiles("*.xml");
            string[] filePaths = Directory.GetFiles(fulldirectoryname, "*.xml");
            foreach (FileInfo file in files) {
                string result = Path.GetFileNameWithoutExtension(file.FullName);
                ColorItem colitem = new ColorItem(this, result, result, file.FullName, Color.Black);
                colitem.IsLoading = true;
                colitem.Visible = false;
                colitem.IsLoading = false;
                CI.Add(colitem);
            }
            ColorItems = CI;
            return CropzoneColors;
        }

        public void OnColorUpdated() {
            coloritemsindex.Clear();
            cropzonecolors.Clear();
            //var mapsettings = clientEndpoint.GetMapSettings();
            GeoSerializer serializer = new GeoSerializer();
            ThinkGeo.MapSuite.WpfDesktopEdition.LayerOverlay layercollection = new ThinkGeo.MapSuite.WpfDesktopEdition.LayerOverlay();
            foreach (var ci in coloritems) {
                if (ci.Visible) {
                    //Byte[] savedStates = ByteArrayFromFile(ci.CropName);
                    //ThinkGeo.MapSuite.WpfDesktopEdition.LayerOverlay layeroverlay = new ThinkGeo.MapSuite.WpfDesktopEdition.LayerOverlay();
                    //layeroverlay.LoadState(savedStates);

                    InMemoryFeatureLayer deserializedLayer = (InMemoryFeatureLayer)serializer.Deserialize(ci.CropName, FileAccess.Read);
                    layercollection.Layers.Add(deserializedLayer);

                    //foreach(ThinkGeo.MapSuite.Core.Layer layer in layeroverlay.Layers) {
                    //    layercollection.Layers.Add(layer);
                    //}
                }
            }
            //mapspage.SSurgoLayersOverlay = layercollection;
            mapspage.IsLegendVisible = false;
            mapspage.RefreshTheMap();
        }

        private void InitializeFilter() {
            this.currentDataSourceId = ApplicationEnvironment.CurrentDataSourceId;
            this.currentCropYear = ApplicationEnvironment.CurrentCropYear;
            coloritems = new ObservableCollection<ColorItem>();
            cropzonecrops = new Dictionary<string, string>();
            cropzonecolors = new Dictionary<string, Color>();
            coloritemsindex = new Dictionary<string, ColorItem>();
            usedcolors = new List<Color>();
            colormultiplier = 50;
            randomchoice = 5;
            mapspage.IsLegendVisible = false;
        }

        void ToggleThisItem(ColorItem itemToSelect) {
            if (itemToSelect == null) { return; }
            SelectedColorItem = ColorItems.Where(x => x.Key == itemToSelect.Key).SingleOrDefault();
        }

        void ChangeTheColor() {
            //showthetoggle = !showthetoggle;
            //for (int i = 0; i < ColorItems.Count; i++) {
            //    ColorItems[i].ColorPickerColor = System.Windows.Media.Color.FromArgb(255, ColorItems[i].MapColor.R, ColorItems[i].MapColor.G, ColorItems[i].MapColor.B);
            //    ColorItems[i].ShowToggleButton = showthetoggle;
            //}
            RaisePropertyChanged("ColorItems");
            RaisePropertyChanged("CropzoneColors");
        }

        public byte[] ByteArrayFromFile(string fileName) {
            byte[] byteArray = null;
            FileStream fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
            BinaryReader binaryReader = new BinaryReader(fileStream);
            long numBytes = new FileInfo(fileName).Length;
            byteArray = binaryReader.ReadBytes((int)numBytes);
            return byteArray;
        }

        private Measure GetCropZoneArea(Guid Id) {
            string stringid = Id.ToString();
            Measure resultarea = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(0);
            var fieldMaybe = clientEndpoint.GetView<CropZoneDetailsView>(new CropZoneId(ApplicationEnvironment.CurrentDataSourceId, Id));
            fieldMaybe.IfValue(field => {
                if (field.ReportedArea != null && !string.IsNullOrWhiteSpace(field.ReportedAreaUnit)) {
                    resultarea = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(field.ReportedArea.Value);
                }
                else if (field.BoundaryArea != null && !string.IsNullOrWhiteSpace(field.BoundaryAreaUnit)) {
                    resultarea = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(field.BoundaryArea.Value);
                }
                else {
                    resultarea = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(0);
                }
            });
            return resultarea;
        }

        public LegendAdornmentLayer BuildLegend(InMemoryFeatureLayer fieldsLayer, CropMapsFarmDisplayItem selectedFarm, bool isLegendVisible, Dictionary<string, string> unselectedFeatures) {
            legendLayer = new LegendAdornmentLayer();
            //float legendheight = 0;
            //legendLayer.BackgroundMask = AreaStyles.CreateLinearGradientStyle(new GeoColor(255, 255, 255, 255), new GeoColor(255, 230, 230, 230), 90, GeoColor.SimpleColors.Black);
            //CustomLegendItem title = new CustomLegendItem();
            //title.TextStyle = new TextStyle(Strings.MapLegend_Text, new GeoFont("Arial", 10, DrawingFontStyles.Bold), new GeoSolidBrush(GeoColor.SimpleColors.Black));
            //title.Height = 30;
            //legendheight += title.Height;
            //title.Width = 250;
            //DetermineLegendWitdh(legendLayer, title);
            //legendLayer.Title = title;
            //for (int i = 0; i < coloritems.Count; i++) {
            //    ColorItem ci = coloritems[i];
            //    coloritems[i].Area = 0;
            //}
            //foreach (Feature feeture in fieldsLayer.InternalFeatures) {
            //    string featureid = feeture.Id.ToString();
            //    string farmName = feeture.ColumnValues[@"Farm"];
            //    if (cropzonecolors.ContainsKey(featureid)) {
            //        if (selectedFarm == @"All Farms" || selectedFarm == string.Empty || selectedFarm == farmName) {
            //            for (int i = 0; i < coloritems.Count; i++) {
            //                ColorItem ci = coloritems[i];
            //                if (ci.MapColor.R == cropzonecolors[featureid].R && ci.MapColor.G == cropzonecolors[featureid].G && ci.MapColor.B == cropzonecolors[featureid].B) {
            //                    Measure czarea = GetCropZoneArea(new Guid(featureid));
            //                    coloritems[i].Area += czarea.Value;
            //                }
            //            }
            //        }
            //    }
            //}
            //foreach (var ci in coloritems) {
            //    if (ci.Visible) {
            //        Measure resultarea = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(ci.Area);
            //        CustomLegendItem legendItem4 = new CustomLegendItem();
            //        legendItem4.ImageStyle = AreaStyles.CreateSimpleAreaStyle(GeoColor.FromArgb(ci.MapColor.A, ci.MapColor.R, ci.MapColor.G, ci.MapColor.B));
            //        string featdesc3 = string.Empty;
            //        string string1 = string.Format("{0}:", ci.CropName);
            //        string string2 = string.Format("{0}", resultarea.AbbreviatedDisplay);
            //        featdesc3 = string.Format("{0}{1}", string1.PadRight(30), string2.PadRight(10));
            //        legendItem4.TextStyle = new TextStyle(featdesc3, new GeoFont("Consolas", 8), new GeoSolidBrush(GeoColor.SimpleColors.Black));
            //        legendItem4.Height = 25;
            //        legendheight += legendItem4.Height;
            //        legendItem4.Width = 325;
            //        DetermineLegendWitdh(legendLayer, legendItem4);
            //        legendLayer.LegendItems.Add(ci.Key, legendItem4);
            //    }
            //}
            //legendheight += 30;
            //if (legendheight < 200) {
            //    legendheight = 200;
            //}
            //legendLayer.Height = legendheight;
            ////legendLayer.Height = 325;
            //legendLayer.Width = 325;
            //legendLayer.ContentResizeMode = LegendContentResizeMode.Fixed;
            //legendLayer.Location = AdornmentLocation.UpperLeft;
            //legendLayer.YOffsetInPixel = 50;
            if (!isLegendVisible) { return null; }
            return legendLayer;
        }

        private void DetermineLegendWitdh(LegendAdornmentLayer legendLayer, CustomLegendItem legendItem) {
            SetLegendItemWidth(legendItem);

            if (legendItem.Width > legendLayer.Width) {
                legendLayer.Width = legendItem.Width;
            }
        }

        private static void SetLegendItemWidth(CustomLegendItem legendItem) {
            var fudgeFactor = 10;
            var lineWidth = (int)new GdiPlusGeoCanvas().MeasureText(legendItem.TextStyle.TextColumnName, legendItem.TextStyle.Font).Width;
            var totalImageLength = legendItem.ImageWidth + legendItem.ImageLeftPadding + legendItem.ImageRightPadding;
            var totalTextLength = legendItem.TextLeftPadding + legendItem.TextRightPadding + lineWidth;
            legendItem.Width = totalImageLength + totalTextLength + fudgeFactor;
        }
    }
}
