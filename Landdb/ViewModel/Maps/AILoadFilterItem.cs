﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Map;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Infrastructure;
using NLog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Windows.Threading;
using AgC.UnitConversion;
using Landdb.MapStyles;
using ThinkGeo.MapSuite.Core;
using System.Diagnostics;
using Landdb.Client.Infrastructure.DisplayItems;
using Landdb.Resources;

namespace Landdb.ViewModel.Maps {
    public class AILoadFilterItem : ViewModelBase, IMapFilter  {
        MapsPageViewModel mapspage;
        readonly Logger log = LogManager.GetCurrentClassLogger();
        Guid currentDataSourceId = Guid.Empty;
        int currentCropYear = 0;
        string name = string.Empty;
        bool showthetoggle = true;
        ColorItem selectedcoloritem = null;
        ObservableCollection<ColorItem> coloritems = new ObservableCollection<ColorItem>();
        ObservableCollection<ColorItem> cropzonecoloritems = new ObservableCollection<ColorItem>();
        Dictionary<string, string> cropzonecrops = new Dictionary<string, string>();
        Dictionary<string, Color> cropzonecolors = new Dictionary<string,Color>();
        Dictionary<string, ColorItem> coloritemsindex = new Dictionary<string,ColorItem>();
        Dictionary<Guid, AnalysisModel> ActiveIngredientsList = new Dictionary<Guid, AnalysisModel>();
        List<AnalysisModel> cropzonesAnalysis = new List<AnalysisModel>();
        Dictionary<string, int> cropzonepercentages = new Dictionary<string, int>();
        Dictionary<string, string> cropzonelabels = new Dictionary<string, string>();
        Dictionary<string, string> CCp = new Dictionary<string, string>();
        ObservableCollection<string> croplist = new ObservableCollection<string>();
        string selectedcrop = Strings.AllIngredients_Text;
        IClientEndpoint clientEndpoint;
        MapSettings mapsettings;
        LegendAdornmentLayer legendLayer = new LegendAdornmentLayer();
        int limitlegendlabellength = 0;

        public AILoadFilterItem(MapsPageViewModel mapspage, IClientEndpoint clientEndpoint) {
            this.mapspage = mapspage;
            this.clientEndpoint = clientEndpoint;
            this.currentDataSourceId = ApplicationEnvironment.CurrentDataSourceId;
            this.currentCropYear = ApplicationEnvironment.CurrentCropYear;
            InitializeFilter();
            InitializeLegend();
            UpdateSelectedIngredientCommand = new RelayCommand<ColorItem>(ToggleThisItem);
            //ToggleCropColorpCommand = new RelayCommand<ColorItem>(ToggleThisItem);
            //ConfigureCropColorCommand = new RelayCommand(ChangeTheColor);
        }

        public ICommand UpdateSelectedIngredientCommand { get; private set; }
        //public ICommand ToggleCropColorpCommand { get; private set; }
        //public ICommand ConfigureCropColorCommand { get; private set; }

        public string Name => Strings.FilterType_ActiveIngredients; 

        public FilterType Filter => FilterType.ActiveIngredients;

        public ObservableCollection<ColorItem> Legend { get; private set; }

        public ObservableCollection<ColorItem> ColorItems {
            get { return coloritems; }
            set {
                if (coloritems == value) { return; }
                coloritems = value;
                RaisePropertyChanged("ColorItem");
                RaisePropertyChanged("ColorItems");
                RaisePropertyChanged("CropzoneColors");
            }
        }

        public ColorItem SelectedColorItem {
            get { return selectedcoloritem; }
            set {
                if (selectedcoloritem == value) { return; }
                selectedcoloritem = value;
                selectedcoloritem.IsSelected = true;
                OnColorUpdated();
                RaisePropertyChanged("SelectedColorItem");
                RaisePropertyChanged("ColorItems");
                RaisePropertyChanged("CropzoneColors");
            }
        }

        public ObservableCollection<string> Croplist {
            get { return croplist; }
            set {
                if (croplist == value) { return; }
                croplist = value;
                RaisePropertyChanged("Croplist");
            }
        }

        public string SelectedCrop {
            get { return selectedcrop; }
            set {
                if (selectedcrop == value) { return; }
                selectedcrop = value;
                OnColorUpdated();
                RaisePropertyChanged("SelectedCrop");
            }
        }

        public Dictionary<string, Color> CropzoneColors {
            get { return cropzonecolors; }
            set {
                if (cropzonecolors == value) { return; }
                cropzonecolors = value;
                RaisePropertyChanged("CropzoneColors");
            }
        }

        public List<AnalysisModel> CropzonesAnalysis {
            get { return cropzonesAnalysis; }
            set {
                cropzonesAnalysis = value;
                RaisePropertyChanged("CropzonesAnalysis");
                RaisePropertyChanged("ColorItem");
                RaisePropertyChanged("ColorItems");
                RaisePropertyChanged("CropzoneColors");
            }
        }

        public Dictionary<string, int> CropzonePercentages {
            get { return cropzonepercentages; }
            set {
                cropzonepercentages = value;
                RaisePropertyChanged("CropzonePercentages");
            }
        }
        public IList<Color> UsedColors {
            get { return new List<Color>(); ; }
        }

        public AILoad ApplicationSummary { get; private set; }

        public Dictionary<string, Color> Execute(IClientEndpoint clientEndpoint, Random rand) {
            ApplicationSummary = new AILoad(clientEndpoint);
            InitializeFilter();
            mapsettings = clientEndpoint.GetMapSettings();
            //Color color = Color.FromArgb(255, rand.Next(0, 255), rand.Next(0, 255), rand.Next(0, 255));
            //ColorItem selectedcolr = new ColorItem(this, "All Ingredients", "All Ingredients", "", color);
            //coloritems.Add(selectedcolr);
            //cropzonecrops.Add("All Ingredients", "All Ingredients");
            selectedcrop = Strings.AllIngredients_Text;
            croplist.Add(Strings.AllIngredients_Text);

            Stopwatch aiWatch = Stopwatch.StartNew();

            var years = clientEndpoint.GetView<FieldTreeView>(new DataSourceId(ApplicationEnvironment.CurrentDataSourceId));
            var yearItem = (years.HasValue && years.Value.CropYearList.ContainsKey(this.currentCropYear)) ? years.Value.CropYearList[this.currentCropYear] : new TreeViewYearItem();
            IList<TreeViewFarmItem> farms = yearItem.Farms != null ? yearItem.Farms : new List<TreeViewFarmItem>();

            var q = from f in farms
                    from fi in f.Fields
                    from cz in fi.CropZones
                    let itemMap = clientEndpoint.GetView<ItemMap>(cz.CropZoneId)
                    where itemMap.HasValue && itemMap.Value.MostRecentMapItem != null
                    select new { f.FarmId, fi.FieldId, cz.CropZoneId, cz.Name, cz, itemMap.Value.MostRecentMapItem.MapData, itemMap.Value.MostRecentMapItem.DataType };

            foreach (var item in q) {
                Guid czidguid = item.CropZoneId.Id;
                string czid = czidguid.ToString();

                string cropname = clientEndpoint.GetMasterlistService().GetCropDisplay(item.cz.CropId);
                CCp.Add(czid, cropname);
                if (!croplist.Contains(cropname)) {
                    croplist.Add(cropname);
                }
            }

            //ActiveIngredientsList = new Dictionary<Guid, AnalysisModel>();

            List<TreeViewCropZoneItem> zones = new List<TreeViewCropZoneItem>();
            foreach (var item in q) {
                zones.Add(item.cz);
            }
            var aiTimeTotal = aiWatch.ElapsedMilliseconds;
            log.Info("Time Elapsed: Active Ingredient - {0}ms | cropzones - {1}", aiTimeTotal, zones.Count);
            aiWatch.Stop();
            aiWatch.Reset();
            aiWatch.Start();

            CropzonesAnalysis = ApplicationSummary.RefreshSummaryItems(zones);
            aiTimeTotal = aiWatch.ElapsedMilliseconds;
            log.Info("Time Elapsed: RefreshSummaryItems - {0}ms | cropzones - {1}", aiTimeTotal, zones.Count);
            aiWatch.Stop();
            aiWatch.Reset();
            aiWatch.Start();

            OnIngredientListUpdated();
            aiTimeTotal = aiWatch.ElapsedMilliseconds;
            log.Info("Time Elapsed: OnIngredientListUpdated - {0}ms | cropzones - {1}", aiTimeTotal, zones.Count);
            aiWatch.Stop();
            aiWatch.Reset();
            aiWatch.Start();

            OnColorUpdated();
            aiTimeTotal = aiWatch.ElapsedMilliseconds;
            log.Info("Time Elapsed: OnColorUpdated - {0}ms | cropzones - {1}", aiTimeTotal, zones.Count);
            aiWatch.Stop();
            aiWatch.Reset();
            return cropzonecolors;
        }

        public void OnIngredientListUpdated() {
            cropzonecrops.Clear();
            cropzonecrops.Add(Strings.AllIngredients_Text, Strings.AllIngredients_Text);
            foreach (AnalysisModel am in cropzonesAnalysis) {
                foreach (ActiveIngredientLineItemViewModel ai in am.ActiveIngredients) {
                    if (!cropzonecrops.ContainsKey(ai.ActiveIngredientId.Id.ToString())) {
                        cropzonecrops.Add(ai.ActiveIngredientId.Id.ToString(), ai.Name);
                    }
                }
            }
            selectedcoloritem = new ColorItem(this, Strings.AllIngredients_Text, Strings.AllIngredients_Text, "", Color.Black);
            coloritems.Clear();
            coloritems.Add(selectedcoloritem);
            //List<KeyValuePair<string,string>> lst = cropzonecrops.ToList();
            
            foreach (KeyValuePair<string, string> item in cropzonecrops.OrderBy(x => x.Value)) {
                if (item.Value != Strings.AllIngredients_Text) {
                    coloritems.Add(new ColorItem(this, item.Key, item.Value, "", Color.Black));
                }
            }
            RaisePropertyChanged("ColorItem");
            RaisePropertyChanged("ColorItems");
            RaisePropertyChanged("CropzoneColors");
        }

        public void OnColorUpdated() {
            coloritemsindex.Clear();
            cropzonecolors.Clear();
            cropzonepercentages.Clear();
            cropzonecoloritems = new ObservableCollection<ColorItem>();
            cropzonelabels = new Dictionary<string, string>();
            foreach (AnalysisModel am in cropzonesAnalysis) {
                int percentage = 100;
                string czid = am.Id.ToString();
                double pcntage = 100;
                ActiveIngredientLineItemViewModel selectedai = null;
                foreach (ActiveIngredientLineItemViewModel ai in am.ActiveIngredients) {
                    if (ai.ActiveIngredientId.Id.ToString() == selectedcoloritem.Key) {
                        percentage = System.Convert.ToInt32(Math.Round(ai.PercentRemaining, 0));
                        selectedai = ai;
                    }
                    else if (selectedcoloritem.Key == Strings.AllIngredients_Text) {
                        if (ai.PercentRemaining < pcntage) {
                            pcntage = ai.PercentRemaining;
                            selectedai = ai;
                            if (cropzonelabels.ContainsKey(czid)) {
                                cropzonelabels[czid] = selectedai.Name;
                            }
                            else {
                                cropzonelabels.Add(czid, selectedai.Name);
                            }
                        }
                    }
                }
                if (selectedcoloritem.Key == Strings.AllIngredients_Text) {
                    percentage = System.Convert.ToInt32(Math.Round(pcntage, 0));
                }
                cropzonepercentages.Add(czid, percentage);
                if (selectedai != null) {
                    ColorItem aicoloritem = ChooseColor(selectedai, czid);
                    aicoloritem.Area = am.Area.Value;
                    cropzonecoloritems.Add(aicoloritem);
                    cropzonecolors.Add(czid, aicoloritem.MapColor);
                }
            }
            if (selectedcoloritem.Key == Strings.AllIngredients_Text) {
                mapspage.IsLegendVisible = false;
            }
            else {
                mapspage.IsLegendVisible = true;
            }
            mapspage.CropzoneLabels = cropzonelabels;
            mapspage.ColorItemList = new ObservableCollection<ColorItem>();
            mapspage.CropzonePercentages = cropzonepercentages;
            mapspage.CropzoneColors = cropzonecolors;
        }

        private void InitializeFilter() {
            this.currentDataSourceId = ApplicationEnvironment.CurrentDataSourceId;
            this.currentCropYear = ApplicationEnvironment.CurrentCropYear;
            coloritems = new ObservableCollection<ColorItem>();
            cropzonecrops = new Dictionary<string, string>();
            cropzonecolors = new Dictionary<string, Color>();
            coloritemsindex = new Dictionary<string, ColorItem>();
            cropzonesAnalysis = new List<AnalysisModel>();
            cropzonelabels = new Dictionary<string, string>();
            croplist = new ObservableCollection<string>();
            CCp = new Dictionary<string, string>();
            mapspage.IsLegendVisible = false;
        }

        void ToggleThisItem(ColorItem itemToSelect) {
            if (itemToSelect == null) { return; }
            foreach (ColorItem ci in ColorItems) {
                if(ci.IsSelected) {
                    ci.IsSelected = false;
                }
            }
            SelectedColorItem = ColorItems.Where(x => x.Key == itemToSelect.Key).SingleOrDefault();
        }

        void ChangeTheColor() {
            showthetoggle = !showthetoggle;
            for (int i = 0; i < ColorItems.Count; i++) {
                ColorItems[i].ColorPickerColor = System.Windows.Media.Color.FromArgb(255, ColorItems[i].MapColor.R, ColorItems[i].MapColor.G, ColorItems[i].MapColor.B);
                ColorItems[i].ShowToggleButton = showthetoggle;
            }
            if (showthetoggle) {
                OnColorUpdated();
            }
            RaisePropertyChanged("ColorItems");
            RaisePropertyChanged("CropzoneColors");
        }

        private ColorItem ChooseColor(ActiveIngredientLineItemViewModel ai, string czid) {
            return new ColorItem(this, czid, ai.Name, ai.CropName, ChooseColor(ai.PercentRemaining));
        }

        private Color ChooseColor(double percentage) {
            if (percentage < 10) {
                return Color.FromArgb(255, 255, 125, 0);
            }
            else if (percentage >= 10 && percentage < 20) {
                return Color.FromArgb(255, 255, 160, 0);
            }
            else if (percentage >= 20 && percentage < 30) {
                return Color.FromArgb(255, 255, 205, 0);
            }
            else if (percentage >= 30 && percentage < 40) {
                return Color.FromArgb(255, 255, 235, 0);
            }
            else if (percentage >= 40 && percentage < 50) {
                return Color.FromArgb(255, 255, 255, 0);
            }
            else if (percentage >= 50 && percentage < 60) {
                return Color.FromArgb(255, 177, 251, 23);
            }
            else if (percentage >= 60 && percentage < 70) {
                return Color.FromArgb(255, 95, 251, 23);
            }
            else if (percentage >= 70 && percentage < 80) {
                return Color.FromArgb(255, 76, 196, 23);
            }
            else if (percentage >= 80 && percentage < 90) {
                return Color.FromArgb(255, 26, 146, 0);
            }
            return Color.FromArgb(255, 0, 116, 0);
        }

        private void InitializeLegend() {
            ObservableCollection<ColorItem> legend = new ObservableCollection<ColorItem>();
            legend.Add(new ColorItem(null, "10", @"< 10%", "", Color.FromArgb(255, 255, 125, 0)));
            legend.Add(new ColorItem(null, "20", @"10 - 20%", "", Color.FromArgb(255, 255, 160, 0)));
            legend.Add(new ColorItem(null, "30", @"20 - 30%", "", Color.FromArgb(255, 255, 205, 0)));
            legend.Add(new ColorItem(null, "40", @"30 - 40%", "", Color.FromArgb(255, 255, 235, 0)));
            legend.Add(new ColorItem(null, "50", @"40 - 50%", "", Color.FromArgb(255, 255, 255, 0)));
            legend.Add(new ColorItem(null, "60", @"50 - 60%", "", Color.FromArgb(255, 177, 251, 23)));
            legend.Add(new ColorItem(null, "70", @"60 - 70%", "", Color.FromArgb(255, 95, 251, 23)));
            legend.Add(new ColorItem(null, "80", @"70 - 80%", "", Color.FromArgb(255, 76, 196, 23)));
            legend.Add(new ColorItem(null, "90", @"80 - 90%", "", Color.FromArgb(255, 26, 146, 0)));
            legend.Add(new ColorItem(null, "100", @"> 90%", "", Color.FromArgb(255, 0, 116, 0)));
            Legend = legend;
            RaisePropertyChanged("Legend");
        }

        private Measure GetCropZoneArea(Guid Id) {
            string stringid = Id.ToString();
            Measure resultarea = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(0);
            var fieldMaybe = clientEndpoint.GetView<CropZoneDetailsView>(new CropZoneId(ApplicationEnvironment.CurrentDataSourceId, Id));
            fieldMaybe.IfValue(field => {
                if (field.ReportedArea != null && !string.IsNullOrWhiteSpace(field.ReportedAreaUnit)) {
                    resultarea = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(field.ReportedArea.Value);
                }
                else if (field.BoundaryArea != null && !string.IsNullOrWhiteSpace(field.BoundaryAreaUnit)) {
                    resultarea = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(field.BoundaryArea.Value);
                }
                else {
                    resultarea = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).GetMeasure(0);
                }
            });
            return resultarea;
        }

        private string DisplayLegendSelectedAreaDecimals(string label, double unitvalue, string unitname, int labelpad, int areapad) {
            string featdesc3 = string.Empty;
            IUnit _unit = UnitFactory.GetUnitByName(unitname);
            string decimalstring = "N" + mapsettings.AreaDecimals.ToString("N0");
            string string1 = string.Format("{0}:", FormatLegendLabelLength(label));
            string string2 = string.Format("{0} {1} ", unitvalue.ToString(decimalstring), _unit.AbbreviatedDisplay);
            if (unitvalue == 0) {
                string2 = string.Format("0 {0} ", _unit.AbbreviatedDisplay);
            }
            featdesc3 = string.Format("{0}{1}", string1.PadRight(labelpad), string2.PadLeft(areapad));
            return featdesc3;
        }

        private string FormatLegendLabelLength(string labeltext) {
            string newlabel = labeltext;
            if (limitlegendlabellength > 10 && labeltext.Length > limitlegendlabellength) {
                newlabel = labeltext.Substring(0, limitlegendlabellength);
            }
            return newlabel;
        }

        public LegendAdornmentLayer BuildLegend(InMemoryFeatureLayer fieldsLayer, CropMapsFarmDisplayItem selectedFarm, bool isLegendVisible, Dictionary<string, string> unselectedFeatures) {
            legendLayer = new LegendAdornmentLayer();
            float legendheight = 0;
            float baselength = 50;
            float baselegendwidth = 100;
            int padrightname = 30;
            int padrightarea = 14;
            int padrightnamemin = 5;
            int padrightnamecalc = 12;
            float baseheight = 2;
            float baselegendheight = 20;

            legendLayer.BackgroundMask = AreaStyles.CreateLinearGradientStyle(new GeoColor(255, 255, 255, 255), new GeoColor(255, 230, 230, 230), 90, GeoColor.SimpleColors.Black);
            CustomLegendItem title = new CustomLegendItem();
            title.TextStyle = new TextStyle(Strings.MapLegend_Text, new GeoFont("Arial", 10, DrawingFontStyles.Bold), new GeoSolidBrush(GeoColor.SimpleColors.Black));
            SizeF TextStylelength = GetStringPixelLength(Strings.MapLegend_Text, "Arial", 10);
            title.Height = TextStylelength.Height;
            legendheight += title.Height;
            title.Width = TextStylelength.Width;
            DetermineLegendWitdh(legendLayer, title);
            legendLayer.Title = title;

            ObservableCollection<ColorItem> legendcalc = new ObservableCollection<ColorItem>();
            legendcalc = Legend;
            for (int i = 0; i < legendcalc.Count; i++) {
                legendcalc[i].Area = 0;
            }
            foreach (var ci in cropzonecoloritems) {
                //if (!fieldsLayer.InternalFeatures.Contains(ci.Key)) { continue; }
                if (fieldsLayer.InternalFeatures.Count > 0 && fieldsLayer.InternalFeatures.Contains(ci.Key)) {
                    Feature feeture = fieldsLayer.InternalFeatures[ci.Key];
                    string featureid = feeture.Id.ToString();
                    //if (unselectedFeatures != null && unselectedFeatures.Count > 0 && unselectedFeatures.ContainsKey(featureid)) { continue; }
                    if (unselectedFeatures == null || unselectedFeatures.Count == 0 || unselectedFeatures.ContainsKey(featureid))
                    {
                        string farmName = feeture.ColumnValues[@"Farm"];
                        if (selectedFarm.IsAllFarms || selectedFarm.DisplayText == string.Empty || selectedFarm.DisplayText == farmName)
                        {
                            if (ci.Area > 0)
                            {
                                for (int i = 0; i < legendcalc.Count; i++)
                                {
                                    ColorItem citem = legendcalc[i];
                                    if (ci.MapColor.R == citem.MapColor.R && ci.MapColor.G == citem.MapColor.G && ci.MapColor.B == citem.MapColor.B)
                                    {
                                        legendcalc[i].Area += ci.Area;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            foreach (var ci in legendcalc) {
                string _unitname = UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).AbbreviatedDisplay;
                CustomLegendItem legendItem4 = new CustomLegendItem();
                legendItem4.ImageStyle = AreaStyles.CreateSimpleAreaStyle(GeoColor.FromArgb(ci.MapColor.A, ci.MapColor.R, ci.MapColor.G, ci.MapColor.B));
                string featdesc3 = string.Empty;
                featdesc3 = DisplayLegendSelectedAreaDecimals(ci.Label, ci.Area, _unitname, padrightnamecalc, padrightarea);
                legendItem4.TextStyle = new TextStyle(featdesc3, new GeoFont("Consolas", 8), new GeoSolidBrush(GeoColor.SimpleColors.Black));
                legendItem4.Height = 20;
                legendheight += legendItem4.Height;
                legendItem4.Width = 200;
                DetermineLegendWitdh(legendLayer, legendItem4);
                legendLayer.LegendItems.Add(ci.Key, legendItem4);
            }
            legendheight += 20;
            if (legendheight < 100) {
                legendheight = 100;
            }
            legendLayer.Height = legendheight;
            legendLayer.Width = 200;
            legendLayer.ContentResizeMode = LegendContentResizeMode.Fixed;
            legendLayer.Location = AdornmentLocation.UpperLeft;
            legendLayer.YOffsetInPixel = 50;
            if (!isLegendVisible) { return null; }
            return legendLayer;
        }

        private void DetermineLegendWitdh(LegendAdornmentLayer legendLayer, CustomLegendItem legendItem) {
            SetLegendItemWidth(legendItem);

            if (legendItem.Width > legendLayer.Width) {
                legendLayer.Width = legendItem.Width;
            }
        }

        private static void SetLegendItemWidth(CustomLegendItem legendItem) {
            var fudgeFactor = 10;
            var lineWidth = (int)new GdiPlusGeoCanvas().MeasureText(legendItem.TextStyle.TextColumnName, legendItem.TextStyle.Font).Width;
            var totalImageLength = legendItem.ImageWidth + legendItem.ImageLeftPadding + legendItem.ImageRightPadding;
            var totalTextLength = legendItem.TextLeftPadding + legendItem.TextRightPadding + lineWidth;
            legendItem.Width = totalImageLength + totalTextLength + fudgeFactor;
        }

        private static SizeF GetStringPixelLength(string text, string fontname, float fontsize) {
            SizeF size = new SizeF();
            using (System.Drawing.Graphics graphics = System.Drawing.Graphics.FromImage(new Bitmap(1, 1))) {
                size = graphics.MeasureString(text, new Font(fontname, fontsize, FontStyle.Regular, GraphicsUnit.Point));
            }
            return size;
        }
    }
}
