﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Maps.JsonClass
{
    class ExportGeoJson
    {



        private JsonClass.Feature MakeAreaOfIntereest(ThinkGeo.MapSuite.Core.Feature feature, ThinkGeo.MapSuite.Core.Proj4Projection projection)
        {
            ThinkGeo.MapSuite.Core.BaseShape rs = feature.GetShape();
            ThinkGeo.MapSuite.Core.BaseShape rs2 = projection.ConvertToInternalProjection(rs);
            string psstring = rs2.GetGeoJson();



            JsonClass.Feature feat = new JsonClass.Feature();
            feat.type = "Feature";
            feat.properties = new JsonClass.Properties();
            JsonClass.Geometry geomtry = new JsonClass.Geometry();
            geomtry.type = "Polygon";
            geomtry.coordinates = Newtonsoft.Json.JsonConvert.DeserializeObject<float[][][]>(psstring);
            feat.geometry = geomtry;

            List<JsonClass.Feature> featlist = new List<JsonClass.Feature>();
            featlist.Add(feat);
            return feat;
        }
    }
}
