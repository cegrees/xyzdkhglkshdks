﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ViewModel.Maps.JsonClass
{

    public class FeatureCollection
    {
        public string type { get; set; }
        public Feature[] features { get; set; }
    }

    public class Feature
    {
        public string type { get; set; }
        public Properties properties { get; set; }
        public Geometry geometry { get; set; }
    }

    public class Properties
    {
        public string Name { get; set; }
        public string AreaLabel { get; set; }
        public string AreaOnlyLabel { get; set; }
    }

    public class Geometry
    {
        //public string coordinates { get; set; }
        public float[][][] coordinates { get; set; }
        public string type { get; set; }
    }
}

