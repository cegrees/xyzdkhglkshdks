﻿using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.ProductSetting;
using Landdb.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ReportServices
{
    class ProductServices
    {
        IClientEndpoint endpoint;
        public ProductServices(IClientEndpoint endpoint)
        {
            this.endpoint = endpoint;
        }

        public ReiPhi ReturnReiPhi(ProductId productId, CropId cropId)
        {
            var prodSettings = GetProductSettings(productId);
            var prodSettingForCrop = prodSettings.Any(j => j.CropId == cropId) ? prodSettings.FirstOrDefault(x => x.CropId == cropId) : null;

            //This will return a REI PHI value that the User Set
            //TO DO :: MAKE SURE WHEN THEY SET ONE VALUE THAT BOTH ARE RETURNED...EXAMPLE :: ALREADY HAS A VALUE OF R = 24 P = 7 USER SETS R = 30 ....P IS IN PRODUCT SETTING ALSO AS 7
            if (prodSettingForCrop != null)
            {
                ReiPhi reiPhi = new ReiPhi()
                {
                    Crop = prodSettingForCrop.CropId.Id,
                    Phi = (double?)prodSettingForCrop.PhiValue,
                    PhiU = prodSettingForCrop.PhiUnit,
                    Rei = (double?)prodSettingForCrop.ReiValue,
                    ReiU = prodSettingForCrop.ReiUnit,
                };

                return reiPhi;
            }
            else
            {
                //This will find the REI PHI for the Selected Crop if one does not exist it will find one for the Parent Crop
                var masterList = endpoint.GetMasterlistService();
                List<ReiPhi> reiPhis = new List<ReiPhi>();
                reiPhis.AddRange(masterList.GetProductReiPhis(productId));

                var crop = masterList.GetCrop(cropId);
                var parentId = crop.ParentId.HasValue ? new CropId(crop.ParentId.Value) : null;
                if (parentId != null) { }

                if (reiPhis.Any(id => id.Crop == cropId.Id))
                {
                    var rP = reiPhis.FirstOrDefault(id => id.Crop == cropId.Id);
                    return rP;
                }
                else if (parentId != null && reiPhis.Any(id => id.Crop == parentId.Id))
                {
                    var rP = reiPhis.FirstOrDefault(i => i.Crop == parentId.Id);
                    return rP;
                }
                else
                {
                    return null;
                }
            }
        }

        public ReiPhi ReturnLargestReiPhi(ProductId productId, List<CropId> cropIds)
        {
            List<ReiPhi> reiPhis = new List<ReiPhi>();
            foreach (var crop in cropIds)
            {
                var reiPhi = ReturnReiPhi(productId, crop);
                if (reiPhi != null) { reiPhis.Add(reiPhi); }
            }

            //Build New REI PHI with greatest Values
            if (reiPhis.Any())
            {
                var MaxREIobject = reiPhis.OrderByDescending(x => x.Rei).FirstOrDefault();
                var MaxPHIobject = reiPhis.OrderByDescending(j => j.Phi).FirstOrDefault();

                var newREIPHI = new ReiPhi()
                {
                    Phi = MaxPHIobject.Phi,
                    PhiU = MaxPHIobject.PhiU,
                    Rei = MaxREIobject.Rei,
                    ReiU = MaxREIobject.ReiU,
                };

                return newREIPHI;
            }
            else
            {
                return null;
            }
        }

        public List<ProductSettingItem> GetProductSettings(ProductId productId)
        {
            var prodSettingId = new ProductSettingId(ApplicationEnvironment.CurrentDataSourceId, productId);
            var prodSettingsMaybe = endpoint.GetView<ProductSettingsView>(prodSettingId);

            if (prodSettingsMaybe.HasValue && prodSettingsMaybe.Value.ItemsByYear.Any())
            {
                var value = prodSettingsMaybe.Value;
                var cropYearItem = value.ItemsByYear.ContainsKey(ApplicationEnvironment.CurrentCropYear) ? value.ItemsByYear[ApplicationEnvironment.CurrentCropYear] : value.ItemsByYear[ value.ItemsByYear.Max(y => y.Key) ]; //.OrderByDescending(x=>x.Key).FirstOrDefault().Value;

                return cropYearItem;
            }
            else
            {
                return new List<ProductSettingItem>();
            }
        }
    }
}
