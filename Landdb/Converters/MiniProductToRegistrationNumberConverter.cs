﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Landdb.Converters {
    public class MiniProductToRegistrationNumberConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            MiniProduct v = value as MiniProduct;
            if (v == null) { return value; }


            if (string.IsNullOrEmpty(v.RegistrationNumber)) { return string.Empty; }
            if (v.RegistrationNumber.ToLower() == "exempt") { return "Exempt"; }

            Regex r = new Regex("([1-9][0-9]*)");
            MatchCollection matches = r.Matches(v.RegistrationNumber);
            string regNumber = string.Empty;
            for (int i = 0; i < matches.Count; i++) {
                if (i == matches.Count - 1) {
                    regNumber += matches[i].Value;
                } else {
                    regNumber += matches[i].Value + "-";
                }

            }
            return regNumber;


        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            throw new NotImplementedException();
        }
    }
}
