﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Landdb.Converters {
    class MiniProductToSecondaryInfoDisplayTextConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            MiniProduct v = value as MiniProduct;
            if (v == null) { return value; }

            if (!string.IsNullOrWhiteSpace(v.Category)) {
                return string.Format("[{0}]", v.Category);
            } else if (!string.IsNullOrWhiteSpace(v.ProductType)) {
                return string.Format("[{0}]", v.ProductType);
            } else {
                return string.Empty;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            throw new NotImplementedException();
        }
    }
}
