﻿using Landdb.Infrastructure;
using Landdb.ViewModel.Reports;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.Converters.Reports
{
    public class ReportTemplateViewModelToFile
    {
        string fileLocation = Path.Combine(ApplicationEnvironment.TemplateDirectory, @"report_templates.txt");

        public ReportTemplateViewModelToFile()
        {

        }

        public void ReportTemplateService(ReportTemplateViewModel rtVM, bool createNew)
        {
            if (createNew)
            {
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(fileLocation, true))
                {
                    file.WriteLine(JsonConvert.SerializeObject(rtVM));
                }
            }
            else
            {
                //EDIT THE REPORT TEMPLATE FILE
                List<ReportTemplateViewModel> ReportTemplates = new List<ReportTemplateViewModel>();

                var data = File.ReadLines(fileLocation);

                foreach (var line in data)
                {
                    ReportTemplateViewModel templateItem = JsonConvert.DeserializeObject<ReportTemplateViewModel>(line);
                    ReportTemplates.Add(templateItem);
                }

                var selectedTemplate = (from t in ReportTemplates
                                        where t.TemplateName == rtVM.TemplateName && t.ReportType == rtVM.ReportType && t.DataSourceId == rtVM.DataSourceId
                                        select t).SingleOrDefault();
                ReportTemplates.Remove(selectedTemplate);

                ReportTemplates.Add(rtVM);
                File.WriteAllText(fileLocation, string.Empty);

                using (System.IO.StreamWriter file = new System.IO.StreamWriter(fileLocation, true))
                {
                    foreach (var temp in ReportTemplates)
                    {
                        file.WriteLine(JsonConvert.SerializeObject(temp));
                    }
                }
            }
        }

        public void SaveNewTemplate(ReportTemplateViewModel rtVM)
        {
            LimitTemplateTypes(rtVM.ReportType, rtVM.DataSourceId);

            using (System.IO.StreamWriter file = new System.IO.StreamWriter(fileLocation, true))
            {
                file.WriteLine(JsonConvert.SerializeObject(rtVM));
            }
        }

        public void RemoveTemplate(ReportTemplateViewModel template)
        {
            List<ReportTemplateViewModel> ReportTemplates = new List<ReportTemplateViewModel>();

            var data = File.ReadLines(fileLocation);

            foreach (var line in data)
            {
                ReportTemplateViewModel templateItem = JsonConvert.DeserializeObject<ReportTemplateViewModel>(line);
                ReportTemplates.Add(templateItem);
            }

            var selectedTemplate = (from t in ReportTemplates
                                    where t.TemplateName == template.TemplateName && t.ReportType == template.ReportType && t.DataSourceId == template.DataSourceId
                                    select t).FirstOrDefault();
            ReportTemplates.Remove(selectedTemplate);

            File.WriteAllText(fileLocation, string.Empty);

            using (System.IO.StreamWriter file = new System.IO.StreamWriter(fileLocation, true))
            {
                foreach (var temp in ReportTemplates)
                {
                    file.WriteLine(JsonConvert.SerializeObject(temp));
                }
            }
        }

        public void EditTemplate(ReportTemplateViewModel template)
        {
            List<ReportTemplateViewModel> ReportTemplates = new List<ReportTemplateViewModel>();

            var data = File.ReadLines(fileLocation);

            foreach (var line in data)
            {
                ReportTemplateViewModel templateItem = JsonConvert.DeserializeObject<ReportTemplateViewModel>(line);
                ReportTemplates.Add(templateItem);
            }

            var selectedTemplate = (from t in ReportTemplates
                                    where t.TemplateName == template.TemplateName && t.ReportType == template.ReportType && t.DataSourceId == template.DataSourceId
                                    select t).SingleOrDefault();
            ReportTemplates.Remove(selectedTemplate);

            ReportTemplates.Add(template);
            File.WriteAllText(fileLocation, string.Empty);

            using (System.IO.StreamWriter file = new System.IO.StreamWriter(fileLocation, true))
            {
                foreach (var temp in ReportTemplates)
                {
                    file.WriteLine(JsonConvert.SerializeObject(temp));
                }
            }
        }

        public void LimitTemplateTypes(string reportType, Guid DataSourceId)
        {
            List<ReportTemplateViewModel> ReportTemplates = new List<ReportTemplateViewModel>();
            var data = File.ReadLines(fileLocation);

            foreach (var line in data)
            {
                ReportTemplateViewModel templateItem = JsonConvert.DeserializeObject<ReportTemplateViewModel>(line);
                ReportTemplates.Add(templateItem);
            }

            var selectedTemplateTypes = (from t in ReportTemplates
                                         where t.ReportType == reportType && t.DataSourceId == DataSourceId
                                         select t).ToList();

            selectedTemplateTypes = selectedTemplateTypes.OrderByDescending(t => t.LastUsed).ToList();
            int cnt = selectedTemplateTypes.Count();
            while (cnt > 49)
            {
                ReportTemplates.Remove(selectedTemplateTypes.Last());
                cnt--;
            }

            File.WriteAllText(fileLocation, string.Empty);

            using (System.IO.StreamWriter file = new System.IO.StreamWriter(fileLocation, true))
            {
                foreach (var temp in ReportTemplates)
                {
                    file.WriteLine(JsonConvert.SerializeObject(temp));
                }
            }

        }
    }
}
