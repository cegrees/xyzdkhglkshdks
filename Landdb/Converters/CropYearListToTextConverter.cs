﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using Landdb.Resources;

namespace Landdb.Converters {
    public class CropYearListToTextConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            IEnumerable<int> list = value as IEnumerable<int>;

            if (list == null) { return string.Empty; }

            if (!list.Any()) { return Strings.NoCropYears_Text.ToLower(); }

            string s = string.Join(", ", list);
            return s;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            throw new NotImplementedException();
        }
    }
}
