﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Landdb.Converters {
    public class EnumToDisplayStringConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            if (value == null) { return null; }
            string itemString = value.ToString();
            Type enumType = parameter as Type;

            if (itemString == null || enumType == null) { return null; }

            FieldInfo field = enumType.GetField(itemString);
            if (field == null) { return null; }

            object[] attribs = field.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (null != attribs && attribs.Length > 0) {
                itemString = ((DescriptionAttribute)attribs[0]).Description;
            } else { // Check the DisplayAttribute in Sys.CM.DataAnnotations instead
                attribs = field.GetCustomAttributes(typeof(DisplayAttribute), false);
                if (null != attribs && attribs.Length > 0) {
                    itemString = ((DisplayAttribute)attribs[0]).Description;
                }
            }
            return itemString;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            throw new NotImplementedException();
        }
    }
}
