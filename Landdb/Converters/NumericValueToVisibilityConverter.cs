﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace Landdb.Converters {
    public class NumericValueToVisibilityConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            if (value == null) { return Visibility.Collapsed; }

            var val = System.Convert.ToDouble(value);

            if (double.IsNaN(val) || double.IsInfinity(val) || val <= 0) {
                return Visibility.Collapsed;
            } else { 
                return Visibility.Visible; 
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            throw new NotImplementedException();
        }
    }
}
