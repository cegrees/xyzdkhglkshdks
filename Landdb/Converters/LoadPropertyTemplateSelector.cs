﻿using Landdb.ViewModel.Yield;
using System.Windows;
using System.Windows.Controls;

namespace Landdb.Converters {
	public class LoadPropertyTemplateSelector : DataTemplateSelector {

		public DataTemplate LoadPropertyTextBoxTemplate { get; set; }
		public DataTemplate LoadPropertyComboBoxTemplate { get; set; }
		public DataTemplate LoadPropertyCheckBoxTemplate { get; set; }
		public DataTemplate LoadPropertyUnitSelectionTemplate { get; set; }

		public override DataTemplate SelectTemplate(object item, DependencyObject container) {
			if (item is LoadPropertyViewModel) {
				var lp = item as LoadPropertyViewModel;

				switch (lp.InputType) {
					case LoadPropertyInputTypes.TextBox:
						return LoadPropertyTextBoxTemplate;
					case LoadPropertyInputTypes.ComboBox:
						return LoadPropertyComboBoxTemplate;
					case LoadPropertyInputTypes.CheckBox:
						return LoadPropertyCheckBoxTemplate;
					case LoadPropertyInputTypes.PackageUnitSelection:
						return LoadPropertyUnitSelectionTemplate;
					default:
						break;
				}
			}

			return LoadPropertyTextBoxTemplate;
		}
	}
}