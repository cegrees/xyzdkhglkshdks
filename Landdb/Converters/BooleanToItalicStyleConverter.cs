﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Landdb.Converters {
    public sealed class InverseBooleanToItalicStyleConverter : BooleanConverter<FontStyle> {
        public InverseBooleanToItalicStyleConverter() : base(FontStyles.Normal, FontStyles.Italic) { }
    }
}
