﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Landdb.ViewModel.Fields;

namespace Landdb.Converters {
    public class FieldSelectionRootNodeTemplateSelector : DataTemplateSelector {
        public DataTemplate FarmDataTemplate { get; set; }
        public DataTemplate CropDataTemplate { get; set; }

        public override System.Windows.DataTemplate SelectTemplate(object item, System.Windows.DependencyObject container) {
            if (item is CropTreeItemViewModel) {
                return CropDataTemplate;
            } else {
                return FarmDataTemplate;
            }
            //return base.SelectTemplate(item, container);
        }
    }
}
