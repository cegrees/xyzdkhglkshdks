﻿using DocumentFormat.OpenXml.Office2013.PowerPoint;
using Landdb.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace Landdb.Converters {
    public class CultureToVisibilityConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            if (parameter == null) return Visibility.Visible;
            var visibility = Visibility.Collapsed;

            var thisCulture = ApplicationEnvironment.CurrentDataSource != null ?
                ApplicationEnvironment.CurrentDataSource.DatasourceCulture.ToLower() :
                Resources.Strings.Culture != null ?
                    Resources.Strings.Culture.Name :
                    Thread.CurrentThread.CurrentCulture.Name;

            var cultureParams = parameter.ToString().ToLower().Trim().Split(',');

            if (cultureParams[0].Equals("exclude")) {
                visibility = Visibility.Visible;
                if (cultureParams.Any(cultureParam => cultureParam.Trim().Equals(thisCulture.ToLower()))) {
                    visibility = Visibility.Collapsed;
                }
            } else {
                if (cultureParams.Contains(thisCulture.ToLower())) {
                    visibility = Visibility.Visible;
                }
            }

            return visibility;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            throw new NotImplementedException();
        }
    }
}