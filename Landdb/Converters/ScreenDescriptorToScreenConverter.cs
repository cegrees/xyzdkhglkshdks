﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using Landdb.Infrastructure;
using Landdb.ViewModel;
using ReactiveUI;

namespace Landdb.Converters {
    public class ScreenDescriptorToScreenConverter : IValueConverter {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            if (value == null) { return null; }

            var viewCreator = new ViewCreator();
            var sd = value as ScreenDescriptor;
            var view = viewCreator.CreateViewFromDescriptor(sd.Key);
            if (sd.ViewModel != null) {
                var viewType = view.GetType();
                
                if (IsInstanceOfGenericType(typeof(ReactiveUserControl<>), view)) {
                    // TODO: Should probably ensure that the parameter types match up, but for now, just have to make sure we do that appropriately ourselves.
                    var defType = typeof(ReactiveUserControl<>).MakeGenericType(sd.ViewModel.GetType());
                    defType.GetProperty("ViewModel").SetValue(view, sd.ViewModel);
                }
                view.DataContext = sd.ViewModel;
            }
            view.Tag = sd.Title;
            return view;
        }

        static bool IsInstanceOfGenericType(Type genericType, object instance) {
            Type type = instance.GetType();
            while (type != null) {
                if (type.IsGenericType &&
                    type.GetGenericTypeDefinition() == genericType) {
                    return true;
                }
                type = type.BaseType;
            }
            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            throw new NotImplementedException();
        }

        #endregion
    }
}
