﻿using Landdb.ViewModel.Production;
using Landdb.ViewModel.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Landdb.ViewModel.Production.Applications;
using Landdb.ViewModel.Production.Invoices;

namespace Landdb.Converters {

    public class DocumentDataTemplateSelector : DataTemplateSelector {
        static ResourceDictionary invoiceTemplateDictionary;
        static ResourceDictionary workOrderTemplateDictionary;
        static ResourceDictionary recommendationTemplateDictionary;
        static ResourceDictionary applicationTemplateDictionary;
        static ResourceDictionary planTemplateDictionary;
		static ResourceDictionary eCottonTemplateDictionary;
        static ResourceDictionary siteDataTemplateDictionary;

        static DocumentDataTemplateSelector() {
            invoiceTemplateDictionary = new ResourceDictionary();
            invoiceTemplateDictionary.Source = new Uri("/Landdb;component/Views/Templates/InvoiceTemplates.xaml", UriKind.RelativeOrAbsolute);

            workOrderTemplateDictionary = new ResourceDictionary();
            workOrderTemplateDictionary.Source = new Uri("/Landdb;component/Views/Templates/WorkOrderTemplates.xaml", UriKind.RelativeOrAbsolute);

            recommendationTemplateDictionary = new ResourceDictionary();
            recommendationTemplateDictionary.Source = new Uri("/Landdb;component/Views/Templates/RecommendationTemplates.xaml", UriKind.RelativeOrAbsolute);

            applicationTemplateDictionary = new ResourceDictionary();
            applicationTemplateDictionary.Source = new Uri("/Landdb;component/Views/Templates/ApplicationTemplates.xaml", UriKind.RelativeOrAbsolute);

            planTemplateDictionary = new ResourceDictionary();
            planTemplateDictionary.Source = new Uri("/Landdb;component/Views/Templates/PlanTemplates.xaml", UriKind.RelativeOrAbsolute);

			eCottonTemplateDictionary = new ResourceDictionary();
			eCottonTemplateDictionary.Source = new Uri("/Landdb;component/Views/Templates/eCottonDocumentTemplates.xaml", UriKind.RelativeOrAbsolute);

            siteDataTemplateDictionary = new ResourceDictionary();
            siteDataTemplateDictionary.Source = new Uri("/Landdb;component/Views/Templates/SiteDataTemplates.xaml", UriKind.RelativeOrAbsolute);
        }

        public override DataTemplate SelectTemplate(object item, DependencyObject container) {
            if (item is ApplicationDetailsViewModel) {
                // TODO: eventually we need some distinction between application and work order, either by type or property
                return workOrderTemplateDictionary["WorkOrderSourceDocumentTemplate"] as DataTemplate;
            } else if (item is InvoiceDetailsViewModel) {
                return invoiceTemplateDictionary["InvoiceSourceDocumentTemplate"] as DataTemplate;
            } else if (item is RecommendationDetailsViewModel) {
                return recommendationTemplateDictionary["RecSourceDocumentTemplate"] as DataTemplate;
			} else if (item is ECottonDocumentViewModel) {
				return eCottonTemplateDictionary["eCottonDocumentTemplate"] as DataTemplate;
            } else if (item is PlanDetailsViewModel) {
                return planTemplateDictionary["PlanSourceDocumentTemplate"] as DataTemplate;
            }
            else if (item is SiteDataDetailsViewModel)
            {
                return siteDataTemplateDictionary["SiteDataSourceDocumentTemplate"] as DataTemplate;
            }
            return base.SelectTemplate(item, container);
        }
    }
}
