﻿using Landdb.ViewModel.Yield;
using System.Windows;
using System.Windows.Controls;

namespace Landdb.Converters {
	public class LoadPropertyUnitSelectionTemplateSelector : DataTemplateSelector {

		public DataTemplate LoadPropertyWeightUnitSelectionTemplate { get; set; }

		public override DataTemplate SelectTemplate(object item, DependencyObject container) {
			if (item != null && item is LoadPropertyUnitSelectionTypes) {
				var ust = (LoadPropertyUnitSelectionTypes)item;

				switch (ust) {
					case LoadPropertyUnitSelectionTypes.None:
						break;
					case LoadPropertyUnitSelectionTypes.Weight:
						return LoadPropertyWeightUnitSelectionTemplate;
					case LoadPropertyUnitSelectionTypes.Volume:
						break;
					case LoadPropertyUnitSelectionTypes.Length:
						break;
					default:
						break;
				}
			}

			return null;
		}
	}
}