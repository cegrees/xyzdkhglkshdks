﻿using Landdb.Domain.ReadModels.Invoice;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Landdb.Converters {
    public class DocumentListItemTemplateSelector : DataTemplateSelector {
        static ResourceDictionary invoiceTemplateDictionary;

        public DocumentListItemTemplateSelector() {
            invoiceTemplateDictionary = new ResourceDictionary();
            invoiceTemplateDictionary.Source = new Uri("/Landdb;component/Views/Templates/InvoiceTemplates.xaml", UriKind.RelativeOrAbsolute);
        }

        public override DataTemplate SelectTemplate(object item, DependencyObject container) {
            if (item is InvoiceListItem) {
                return invoiceTemplateDictionary["InvoiceListItemTemplate"] as DataTemplate;
            }

            return base.SelectTemplate(item, container);
        }
    }
}
