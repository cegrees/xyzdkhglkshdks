﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace Landdb.Converters
{
    class ResolvedToBrushConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            if (value != null) {
                bool? resolved = value as bool?;
                if ((resolved.HasValue) && (!resolved.Value))
                    return (new SolidColorBrush(Colors.Red));
            }
            return (new SolidColorBrush(Colors.Black));
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

    }
}
