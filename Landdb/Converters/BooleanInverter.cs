﻿using System;
using System.Windows.Data;

namespace Landdb.Converters {
	public class BooleanInverter : IValueConverter {
		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
			return value is bool ? !(bool?)value : null;
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
			return value is bool ? !(bool?)value : null;
		}
	}
}
