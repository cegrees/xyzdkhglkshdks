﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Landdb.Converters {
    public class ReportedAreaToTextEntryConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            if (value == null) { return null; }

            if (value is double) {
                return ((double)value).ToString("n4");
            } else { return 0; }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            if (value == null) { return null; }

            if (value is string) {
                string sVal = value.ToString();
                if (string.IsNullOrWhiteSpace(sVal)) { return null; }

                double val = 0;

                sVal = sVal.Replace("..", ".");
                sVal = sVal.Replace(",,", ",");

                if (double.TryParse(sVal, out val)) {
                    return val;
                } else { return null; }

            } else { return null; }
        }
    }

    public class ReportedAreaToTextEntryNoFormatConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            if (value == null) { return null; }

            if (value is double) {
                return ((double)value).ToString("n4");
            } else { return 0; }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            if (value == null) { return null; }

            if (string.IsNullOrWhiteSpace(value.ToString())) { return null; }

            return value;
        }
    }

}
