﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Landdb.Converters {
	public class NumericToTextPluralityConverter : IValueConverter {
		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
			var parameterArray = parameter.ToString().Split(new char[] { ',' });
			var singularWord = parameterArray[0];
			var pluralWord = parameterArray[1];
			var formattingSpecifier = parameterArray[2];

			var convertedValue = System.Convert.ToDouble(value);

			return string.Format("{0} {1}", convertedValue.ToString(formattingSpecifier), convertedValue == 1 ? singularWord : pluralWord);
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
			throw new NotImplementedException();
		}
	}
}
