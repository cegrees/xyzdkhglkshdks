﻿using Landdb.ViewModel.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Landdb.ViewModel.Production.Invoices;

namespace Landdb.Converters {
    public class InvoiceDetailsTemplateSelector : DataTemplateSelector {
        static ResourceDictionary invoiceTemplateDictionary;

        public InvoiceDetailsTemplateSelector() {
            invoiceTemplateDictionary = new ResourceDictionary();
            invoiceTemplateDictionary.Source = new Uri("/Landdb;component/Views/Templates/InvoiceTemplates.xaml", UriKind.RelativeOrAbsolute);
        }

        public override DataTemplate SelectTemplate(object item, DependencyObject container) {
            if (item is ActualInvoiceDetailsViewModel) {
                return invoiceTemplateDictionary["InvoiceViewTemplate"] as DataTemplate;
            }

            if (item is PendingInvoiceDetailsViewModel) {
                return invoiceTemplateDictionary["PendingInvoiceViewTemplate"] as DataTemplate;
            }

            return base.SelectTemplate(item, container);
        }
    }
}
