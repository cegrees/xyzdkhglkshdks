﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Data;

namespace Landdb.Converters {
    public class DoubleToPercentTextConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            if (value is double) {
                return string.Format("{0:n2} %", (double)value * 100);
            } else {
                return "0.00 %";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            if (value is string) {
				string val = Regex.Replace((string)value, @"[^\d\.\-\+]", string.Empty, RegexOptions.None);

                if(string.IsNullOrEmpty(val)) {
                    return 0;
                }

                double outVal = 0;
                if (double.TryParse(val, out outVal)) {
                    return outVal / 100;
                } else {
                    return 0;
                }

            } else {
                return 0;
            }
        }
    }
}
