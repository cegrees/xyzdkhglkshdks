﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Landdb.Converters {
    public class MiniProductToStandardUnitTextConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            MiniProduct v = value as MiniProduct;
            if (v == null) { return value; }

            if (v.StdFactor == 1 && v.StdUnit == v.StdPackageUnit) { return v.StdPackageUnit; } else { return string.Format("{0} {1} {2}", v.StdFactor, v.StdUnit, v.StdPackageUnit); }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            throw new NotImplementedException();
        }
    }
}
