﻿using Landdb.ViewModel.Secondary.Reports.Specialty;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Landdb.Converters
{
    public class SpecialtyReportTemplateSelector : DataTemplateSelector
    {
        public DataTemplate PesticideUseDataTemplate { get; set; }
        public DataTemplate WSDADataTemplate { get; set; }
        public DataTemplate AerialApplicatorTemplate { get; set; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            if (item is PesticideUseViewModel)
            {
                return PesticideUseDataTemplate;
            }
            else if (item is AerialInfoViewModel)
            {
                return AerialApplicatorTemplate;
            }
            else
            {
                return WSDADataTemplate;
            }
        }
    }
}
