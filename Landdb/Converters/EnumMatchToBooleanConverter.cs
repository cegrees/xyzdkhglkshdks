﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace Landdb.Converters {
    public class EnumMatchToBooleanConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            if (parameter == null) { return false; }

            string parameterString = parameter.ToString();
            if (value == null || parameterString == null)
                return false;

            if(Enum.IsDefined(value.GetType(), value) == false)
                return false;

            object parameterValue = Enum.Parse(value.GetType(), parameterString);
            return parameterValue.Equals(value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            if (parameter == null) { return null; }

            string parameterString = parameter.ToString();
            if (value == null || parameterString == null)
                return null;

            bool useValue = (bool)value;
            if (useValue) {
                return Enum.Parse(targetType, parameterString);
            }

            return null;
        }
    }
}
