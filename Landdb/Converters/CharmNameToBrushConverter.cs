﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace Landdb.Converters {
    public class CharmNameToBrushConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            if (value != null) {
                string charmName = value.ToString();

                ResourceDictionary r = new ResourceDictionary();
                r.Source = new Uri(@"/Landdb;component/Resources/VectorIcons.xaml", UriKind.RelativeOrAbsolute);

                if (r.Contains(charmName) && r[charmName] is Visual) {
                    var vb = new VisualBrush((Visual)r[charmName]);
                    vb.Stretch = Stretch.Uniform;
                    return vb;
                } 
            }

            return Brushes.Transparent;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            throw new NotImplementedException();
        }
    }
}
