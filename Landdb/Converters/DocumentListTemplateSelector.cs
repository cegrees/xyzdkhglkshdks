﻿using Landdb.ViewModel.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Landdb.ViewModel.Production.Applications;
using Landdb.ViewModel.Production.Invoices;

namespace Landdb.Converters {
    public class DocumentListTemplateSelector : DataTemplateSelector {
        static ResourceDictionary invoiceTemplateDictionary;
        static ResourceDictionary workOrderTemplateDictionary;
        static ResourceDictionary recommendationTemplateDictionary;
        static ResourceDictionary applicationTemplateDictionary;
        static ResourceDictionary planTemplateDictionary;
        static ResourceDictionary siteDataTemplateDictionary;

        public DocumentListTemplateSelector() {
            invoiceTemplateDictionary = new ResourceDictionary();
            invoiceTemplateDictionary.Source = new Uri("/Landdb;component/Views/Templates/InvoiceTemplates.xaml", UriKind.RelativeOrAbsolute);

            workOrderTemplateDictionary = new ResourceDictionary();
            workOrderTemplateDictionary.Source = new Uri("/Landdb;component/Views/Templates/WorkOrderTemplates.xaml", UriKind.RelativeOrAbsolute);

            recommendationTemplateDictionary = new ResourceDictionary();
            recommendationTemplateDictionary.Source = new Uri("/Landdb;component/Views/Templates/RecommendationTemplates.xaml", UriKind.RelativeOrAbsolute);

            applicationTemplateDictionary = new ResourceDictionary();
            applicationTemplateDictionary.Source = new Uri("/Landdb;component/Views/Templates/ApplicationTemplates.xaml", UriKind.RelativeOrAbsolute);

            planTemplateDictionary = new ResourceDictionary();
            planTemplateDictionary.Source = new Uri("/Landdb;component/Views/Templates/PlanTemplates.xaml", UriKind.RelativeOrAbsolute);

            siteDataTemplateDictionary = new ResourceDictionary();
            siteDataTemplateDictionary.Source = new Uri("/Landdb;component/Views/Templates/SiteDataTemplates.xaml", UriKind.RelativeOrAbsolute);
        }

        public override DataTemplate SelectTemplate(object item, DependencyObject container) {
            if (item is InvoicesPageViewModel) {
                return invoiceTemplateDictionary["InvoiceListControlTemplate"] as DataTemplate;
            }
            if (item is WorkOrdersPageViewModel) {
                return workOrderTemplateDictionary["WorkOrderListControlTemplate"] as DataTemplate;
            }
            if (item is RecommendationsPageViewModel) {
                return recommendationTemplateDictionary["RecommendationListControlTemplate"] as DataTemplate;
            }
            if (item is ApplicationsPageViewModel) {
                return applicationTemplateDictionary["ApplicationListControlTemplate"] as DataTemplate;
            }
            if (item is PlansPageViewModel) {
                return planTemplateDictionary["PlanListControlTemplate"] as DataTemplate;
            }
            if (item is SiteDataPageViewModel)
            {
                return siteDataTemplateDictionary["ScoutingListControlTemplate"] as DataTemplate;
            }

            return base.SelectTemplate(item, container);
        }

    }
}
