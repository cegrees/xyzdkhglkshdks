﻿using Landdb.ViewModel.Resources.Contract.Production;
using Landdb.ViewModel.Resources.Contract.Rent;
using System.Windows;
using System.Windows.Controls;

namespace Landdb.Converters {
	public class ContractDetailsDataTemplateSelector : DataTemplateSelector {

		public DataTemplate RentContractDataTemplate { get; set; }
		public DataTemplate ProductionContractDataTemplate { get; set; }

		public override DataTemplate SelectTemplate(object item, DependencyObject container) {
			if (item is RentContractDetailsViewModel) {
				return RentContractDataTemplate;
			} else if (item is ProductionContractDetailsViewModel) {
				return ProductionContractDataTemplate;
			} else {
				return null;
			}
		}
	}
}
