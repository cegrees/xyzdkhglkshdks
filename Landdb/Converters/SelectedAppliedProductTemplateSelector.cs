﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Landdb.ViewModel.Secondary.Application;

namespace Landdb.Converters {
    public class SelectedAppliedProductTemplateSelector : DataTemplateSelector {
        public DataTemplate ByRatePerAreaTemplate { get; set; }
        public DataTemplate ByTotalProductTemplate { get; set; }
        public DataTemplate ByRatePerTankTemplate { get; set; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container) {
            if (item is AppliedProductViewModel) {
                var ap = item as AppliedProductViewModel;

                switch (ap.ApplyBy) {
                    case ApplicationTypes.RatePerArea:
                    default:
                        return ByRatePerAreaTemplate;
                    case ApplicationTypes.RatePerTank:
                        return ByRatePerTankTemplate;
                    case ApplicationTypes.TotalProduct:
                        return ByTotalProductTemplate;
                }
            } else {
                return ByRatePerAreaTemplate;
            }
        }
    }
}
