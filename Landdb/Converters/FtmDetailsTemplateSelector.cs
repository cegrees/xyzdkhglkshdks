﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Landdb.Converters {
    public class FtmDetailsTemplateSelector : DataTemplateSelector {
        public DataTemplate NullItemTemplate { get; set; }
        public DataTemplate DefaultTemplate { get; set; }

        public override System.Windows.DataTemplate SelectTemplate(object item, System.Windows.DependencyObject container) {
            if (item == null) {
                return NullItemTemplate;
            } else {
                return DefaultTemplate;
            }
        }
    }
}
