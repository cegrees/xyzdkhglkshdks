﻿using System;
using System.Collections;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Landdb.Converters {

	/// <summary>
	/// Returns Visibility.Visible if value received is numeric and not zero
	/// Useful for binding to List.Count so that UI items are only shown when the list has items
	/// </summary>
	public class NumericToVisibilityConverter : IValueConverter {
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
			decimal convertedVal = 0;

			var stringVal = value != null ? value.ToString() : string.Empty;

			if (decimal.TryParse(stringVal, out convertedVal) && convertedVal != 0) {
				return Visibility.Visible;
			}

			return Visibility.Collapsed;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
			throw new NotImplementedException();
		}
	}
}
