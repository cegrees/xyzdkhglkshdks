﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows;
using Landdb.ViewModel;
using Landdb.ViewModel.Fields;

namespace Landdb.Converters {
    public class AddItemPopupDataTemplateSelector : DataTemplateSelector {
        public override System.Windows.DataTemplate SelectTemplate(object item, DependencyObject container) {
            FrameworkElement element = container as FrameworkElement;
            if (element != null && item != null && item is AbstractTreeItemViewModel) {
                AbstractTreeItemViewModel treeItem = item as AbstractTreeItemViewModel;

                if (treeItem is FarmTreeItemViewModel) {
                    return element.FindResource("AddFieldPopupTemplate") as DataTemplate;
                }
                if (treeItem is FieldTreeItemViewModel) {
                    return element.FindResource("AddCropZonePopupTemplate") as DataTemplate;
                }
                if (treeItem is CropZoneTreeItemViewModel) {
                    return element.FindResource("AddCropZonePopupTemplate") as DataTemplate;
                }
            }

            return element.FindResource("AddFarmPopupTemplate") as DataTemplate;
        }
    }
}
