﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using Landdb.Resources;

namespace Landdb.Converters {
    public class ApplicationTypeToDisplayTextConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            if (value is ApplicationTypes) {
                switch ((ApplicationTypes)value) {
                    case ApplicationTypes.RatePerArea:
                        return Strings.RateArea_Text.ToLower();
                    case ApplicationTypes.TotalProduct:
                        return Strings.TotalProd_Text.ToLower();
                    case ApplicationTypes.RatePerTank:
                        return Strings.RateTank_Text.ToLower();
                    default:
                        return $"<{Strings.Unknown_Text.ToLower()}>";
                }
            } else {
                return $"<{Strings.Unknown_Text.ToLower()}>";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            throw new NotImplementedException();
        }
    }
}
