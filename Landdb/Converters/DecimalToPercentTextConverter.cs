﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Landdb.Converters {
    class DecimalToPercentTextConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            
            if (value is decimal)
            {                
                decimal decVal = (decimal)value;
                decimal newValue = decVal * 100;
                string newString = string.Format("{0:n2} %", newValue);
                return newString;
                
            }
            else
            {
                return "0.00 %";
            }

           
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            if (value is string) {
				string val = Regex.Replace((string)value, @"[^\d\.\-\,\+]", string.Empty, RegexOptions.None);

                if (string.IsNullOrEmpty(val)) {
                    return 0;
                }

                decimal outVal = 0;
                if (decimal.TryParse(val, out outVal)) {
                    return outVal / 100;
                } else {
                    return 0;
                }

            } else {
                return 0;
            }
        }
    }
}
