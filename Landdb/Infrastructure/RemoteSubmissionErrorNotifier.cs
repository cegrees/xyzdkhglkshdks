﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Landdb.Infrastructure {
    public class RemoteSubmissionErrorNotifier : ViewModelBase, IRemoteSubmissionErrorNotifier {

        List<IDomainCommand<AbstractDataSourceIdentity<Guid, Guid>>> errors;
        int unseenCount = 0;
        int seenCount = 0;

        public RemoteSubmissionErrorNotifier() {
            errors = new List<IDomainCommand<AbstractDataSourceIdentity<Guid, Guid>>>();
            ShowErrorsCommand = new RelayCommand(ShowErrors);
        }

        public ICommand ShowErrorsCommand { get; }

        public void AddNewError(IDomainCommand<AbstractDataSourceIdentity<Guid, Guid>> cmd) {
            errors.Insert(0,cmd);
            unseenCount++;
            SystemSounds.Exclamation.Play();

            RaiseTheWorld();
        }

        public void MarkErrorsAsSeen() {
            seenCount += unseenCount;
            unseenCount = 0;

            RaiseTheWorld();
        }

        void RaiseTheWorld() {
            RaisePropertyChanged(() => UnseenCount);
            RaisePropertyChanged(() => HasNewErrors);
            RaisePropertyChanged(() => HasErrors);
            RaisePropertyChanged(() => ErrorDisplayList);
        }

		public int UnseenCount => unseenCount;
		public bool HasNewErrors => unseenCount != 0;
		public bool HasErrors => unseenCount + seenCount > 0;
		public List<ErrorDisplayModel> ErrorDisplayList => GenerateErrorList();

		List<ErrorDisplayModel> GenerateErrorList() {
            List<ErrorDisplayModel> list = new List<ErrorDisplayModel>();
            int unseenCounter = unseenCount;
            foreach (var error in errors) {
                var displayText = Regex.Replace(error.GetType().Name, "([a-z](?=[A-Z0-9])|[A-Z](?=[A-Z][a-z]))", "$1 "); //This Regex de-Pascal-cases the type name
                list.Add(new ErrorDisplayModel() {
                    Type = displayText,
                    Timestamp = error.Metadata.Timestamp.ToLocalTime(),
                    Seen = unseenCounter > 0
                });
                unseenCounter--;
            }
            return list;
        }

        void ShowErrors() {
            var vm = new ViewModel.Overlays.RemoteErrorsListViewModel(GenerateErrorList());

            Messenger.Default.Send(new ShowOverlayMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(Views.Overlays.RemoteErrorsListView), "remotesubmitfail", vm)
			});

            MarkErrorsAsSeen();
        }
    }

    public class ErrorDisplayModel : ViewModelBase {
        string type;
        bool seen;
        DateTime timestamp;

        public string Type {
            get { return type; }
            set { 
                type = value;
                RaisePropertyChanged(() => Type);
            }
        }

        public bool Seen {
            get { return seen; }
            set {
                seen = value;
                RaisePropertyChanged(() => Seen);
            }
        }

        public DateTime Timestamp {
            get { return timestamp; }
            set {
                timestamp = value;
                RaisePropertyChanged(() => Timestamp);
            }
        }
    }
}