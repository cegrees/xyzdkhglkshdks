﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Landdb.Infrastructure {
    public interface IRouteConfigurator {
        void ConfigurizeAllTheRoutes();
    }
}
