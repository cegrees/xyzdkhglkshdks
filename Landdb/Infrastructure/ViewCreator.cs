﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Windows.Controls;

namespace Landdb.Infrastructure {
    public class ViewCreator : IViewCreator {
        static Dictionary<string, UserControl> history = new Dictionary<string, UserControl>();

        #region IViewCreator Members

        public UserControl CreateViewFromDescriptor(string descriptorKey) {
            try {
                if (history.ContainsKey(descriptorKey)) {
                    var item = history[descriptorKey];
                    history[descriptorKey].DataContext = null;
                    return history[descriptorKey];
                } else {
                    var type = Type.GetType(descriptorKey);
                    var typeConstructor = type.GetConstructor(Type.EmptyTypes);
                    var view = typeConstructor.Invoke(new object[] { });

                    if (view is UserControl) {
                        history.Add(descriptorKey, view as UserControl);
                        return view as UserControl;
                    } else {
                        throw new ApplicationException("The descriptor key provided did not resolve to a view.");
                    }
                }
            } catch (Exception ex) {
				var errorMsg = string.Format("The descriptor key [{0}] did not resolve to a view.", descriptorKey);
                throw new ApplicationException(errorMsg, ex);
            }
        }

        #endregion
    }
}
