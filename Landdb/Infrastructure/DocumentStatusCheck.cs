﻿using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Invoice;
using Landdb.Domain.ReadModels.Plan;
using Landdb.Domain.ReadModels.Recommendation;
using Landdb.Domain.ReadModels.WorkOrder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.Infrastructure {
    public class DocumentStatusCheck {
        IClientEndpoint clientEndpoint;

        public DocumentStatusCheck(IClientEndpoint clientEndpoint) {
            this.clientEndpoint = clientEndpoint;
        }

        public bool DocumentHasBeenDeleted(AbstractDataSourceIdentity<Guid, Guid> id) {
            if (id is WorkOrderId) {
                return DocumentHasBeenDeleted((WorkOrderId)id);
            }
            if (id is InvoiceId) {
                return DocumentHasBeenDeleted((InvoiceId)id);
            }
            if (id is RecommendationId) {
                return DocumentHasBeenDeleted((RecommendationId)id);
            }
            if (id is ApplicationId) {
                return DocumentHasBeenDeleted((ApplicationId)id);
            }
            if (id is CropPlanId) {
                return DocumentHasBeenDeleted((CropPlanId)id);
            }
            if (id is SiteDataId || id is ScoutingApplicationId)
            {
                return DocumentHasBeenDeleted((ScoutingApplicationId)id);
                //return id is SiteDataId ? DocumentHasBeenDeleted((SiteDataId)id) : DocumentHasBeenDeleted((ScoutingApplicationId)id);
            }

            return true;
        }

        public bool DocumentHasBeenDeleted(WorkOrderId id) {
            var woMaybe = clientEndpoint.GetView<WorkOrderView>(id);
            if (woMaybe.HasValue) {
                return woMaybe.Value.IsMarkedAsDeleted;
            } else {
                return true;
            }
        }

        public bool DocumentHasBeenDeleted(InvoiceId id) {
            var invMaybe = clientEndpoint.GetView<InvoiceView>(id);
            if (invMaybe.HasValue) {
                return invMaybe.Value.IsMarkedAsDeleted;
            } else { return true; }
        }

        public bool DocumentHasBeenDeleted(ApplicationId id) {
            var invMaybe = clientEndpoint.GetView<ApplicationView>(id);
            if (invMaybe.HasValue) {
                return invMaybe.Value.IsMarkedAsDeleted;
            } else { return true; }
        }

        public bool DocumentHasBeenDeleted(RecommendationId id) {
            var recMaybe = clientEndpoint.GetView<RecommendationView>(id);
            if (recMaybe.HasValue) {
                return recMaybe.Value.IsMarkedAsDeleted;
            } else { return true; }
        }

        public bool DocumentHasBeenDeleted(CropPlanId id) {
            var invMaybe = clientEndpoint.GetView<PlanView>(id);
            if (invMaybe.HasValue) {
                return invMaybe.Value.IsMarkedAsDeleted;
            } else { return true; }
        }

        public bool DocumentHasBeenDeleted(SiteDataId id)
        {
            return false;
        }

        public bool DocumentHasBeenDeleted(ScoutingApplicationId id)
        {
            return false;
        }
    }
}
