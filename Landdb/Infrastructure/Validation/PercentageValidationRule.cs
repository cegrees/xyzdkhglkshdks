﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Landdb.Infrastructure.Validation
{
    public class PercentageValidationRule : ValidationRule
    {
        private double _minPercentage = 0;
        private double _maxPercentage = 100;
        private string _errorMessage;

        public double MinimumPercentage
        {
            get { return _minPercentage; }
            set { _minPercentage = value; }
        }

        public double MaximumPercentage
        {
            get { return _maxPercentage; }
            set { _maxPercentage = value; }
        }

        public string ErrorMessage
        {
            get { return _errorMessage; }
            set { _errorMessage = value; }
        }

        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {
            ValidationResult result = new ValidationResult(true, null);
            try
            {
                double inputPercentage = double.Parse(value.ToString());
                if (inputPercentage > this.MaximumPercentage || inputPercentage < this.MinimumPercentage)
                {
                    result = new ValidationResult(false, this.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                var error = ex;
                result = new ValidationResult(false, "Improper Value");
            }
            return result;
        }
    }
}
