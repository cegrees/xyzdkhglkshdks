﻿using System;
using Landdb.Controls.OAuth2;
using Landdb.Client.Services.Web;
using Landdb.Client.Services.Credentials;

namespace Landdb.Infrastructure {
    public interface ICloudClientFactory {
        Guid DeviceId { get; }
        RequestSecurityTokenResponseStore TokenStore { get; }

        IStorageCredentials ResolveStorageCredentials();
        IRegistrationClient ResolveRegistrationClient();
        IDataSynchronizationClient ResolveDataSyncClient();
    }
}
