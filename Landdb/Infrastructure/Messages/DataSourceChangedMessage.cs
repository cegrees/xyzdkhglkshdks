﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Landdb.Infrastructure.Messages {
    public class DataSourceChangedMessage {
        public Guid DataSourceId { get; set; }
        public int CropYear { get; set; }
    }
}
