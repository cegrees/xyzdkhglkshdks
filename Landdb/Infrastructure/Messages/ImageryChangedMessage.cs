﻿using Landdb.ViewModel.Maps.ImageryOverlay;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Landdb.Infrastructure.Messages {
    public class ImageryChangedMessage {
        public IMapOverlay MapOverlay { get; set; }
    }
}
