﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Landdb.Web.ServiceContracts.Data;
using ThinkGeo.MapSuite.Core;

namespace Landdb.Infrastructure.Messages {
    public class CustomLabelMessage {
        public Dictionary<string, double> RotationAngles;
        public Dictionary<string, PointShape> LabelPositions;
    }
}

