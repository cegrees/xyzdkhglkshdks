﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.Infrastructure.Messages {

	public class PersonAddedMessage {
		public PersonId LastPersonAdded { get; set; }
	}

	public class CompanyAddedMessage {
		public CompanyId LastCompanyAdded { get; set; }
	}

	public class ContractAddedMessage {
		public ContractId LastContractAdded { get; set; }
	}

	public class EquipmentAddedMessage {
		public EquipmentId LastEquipmentAdded { get; set; }
	}

	public class ApplicationsAddedMessage {
		public ApplicationId LastApplicationAdded { get; set; }
	}

	public class InvoicesAddedMessage {
		public InvoiceId LastInvoiceAdded { get; set; }
	}

	public class PlansAddedMessage {
		public CropPlanId LastPlanAdded { get; set; }
	}

	public class RecommendationsAddedMessage {
		public RecommendationId LastRecommendationAdded { get; set; }
	}

	public class WorkOrdersAddedMessage {
		public WorkOrderId LastWorkOrderAdded { get; set; }
	}

	public class YieldLocationAddedMessage {
		public YieldLocationId LastYieldLocationAdded { get; set; }
	}

	public class YieldOperationAddedMessage {
		public YieldOperationId LastYieldOperationAdded { get; set; }
	}

    public class FtmItemChangedMessage
    {
        public CropZoneId LastFtmItemEdited { get; set; }
    }
    //public class InventoryItemEditedMessage
    //{
    //    public InventoryId LastInventoryItemEdited { get; set; }
    //}
}