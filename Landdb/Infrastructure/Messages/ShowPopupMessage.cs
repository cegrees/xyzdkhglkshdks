﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Landdb.ViewModel;

namespace Landdb.Infrastructure.Messages {
    public class ShowPopupMessage {
        public ScreenDescriptor ScreenDescriptor { get; set; }
    }
}
