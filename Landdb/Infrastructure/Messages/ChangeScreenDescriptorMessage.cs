﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Landdb.ViewModel;

namespace Landdb.Infrastructure.Messages {
    public class ChangeScreenDescriptorMessage {
        public ScreenDescriptor ScreenDescriptor { get; set; }
    }
}
