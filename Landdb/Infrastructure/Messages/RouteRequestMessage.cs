﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.Infrastructure.Messages {
    public class RouteRequestMessage {
        public object Route { get; set; }
        public RouteRequestMessage Next { get; set; }
    }
}
