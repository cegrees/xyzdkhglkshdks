﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Landdb.Domain;

namespace Landdb.Infrastructure.Messages {
    public class RemoteEventReceived {
        public IDomainEvent EventData { get; set; }
    }
}
