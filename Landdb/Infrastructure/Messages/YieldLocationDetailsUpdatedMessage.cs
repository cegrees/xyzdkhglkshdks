﻿using GalaSoft.MvvmLight;
using Landdb.ViewModel.Yield;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.Infrastructure.Messages {
	public class YieldLocationDetailsUpdatedMessage {
		public YieldLocationDetailsUpdatedMessage(YieldLocationDetailsViewModel details) {
			Details = details;
		}

		public YieldLocationDetailsViewModel Details { get; }
	}
}
