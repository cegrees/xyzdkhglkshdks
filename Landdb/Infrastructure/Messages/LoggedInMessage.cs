﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Landdb.Web.ServiceContracts.Data;

namespace Landdb.Infrastructure.Messages {
    public class LoggedInMessage {
        public UserAccount UserAccountInformation;
        public bool WorkingOffline;
    }
}
