﻿using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.Infrastructure {
    internal static class StartupTasks {
        static Logger log = LogManager.GetCurrentClassLogger();

        internal static void Perform() {
            MigrateDataToCultureAwareLocation();
            EnsureBrowserRegistryKeyExists();
        }

        private static void MigrateDataToCultureAwareLocation() {
            if(Directory.Exists(ApplicationEnvironment.BaseDataDirectory) && Directory.Exists(Path.Combine(ApplicationEnvironment.BaseDataDirectory, "landdb-tapes"))) {
                // Migrate
                var directoriesToMove = Directory.EnumerateDirectories(ApplicationEnvironment.BaseDataDirectory, "landdb*");
                Directory.CreateDirectory(ApplicationEnvironment.DataDirectory);
                foreach (var dir in directoriesToMove) {
                    var destDir = dir.Replace(ApplicationEnvironment.BaseDataDirectory, ApplicationEnvironment.DataDirectory);
                    if (!Directory.Exists(destDir)) {
                        Directory.Move(dir, destDir); // don't overwrite any existing directory
                    } else {
                        var errorText = string.Format("MigrateDataToCultureAwareLocation: Destination directory {0} exists. Skipping, but could indicate a problem.", destDir);
                        log.Error(errorText);
                    }
                }
            }
        }

        private static void EnsureBrowserRegistryKeyExists() {
            try {
                var key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Internet Explorer\\Main\\FeatureControl\\FEATURE_BROWSER_EMULATION", true);

                if (key == null) {
                    key = Microsoft.Win32.Registry.CurrentUser.CreateSubKey("SOFTWARE\\Microsoft\\Internet Explorer\\Main\\FeatureControl\\FEATURE_BROWSER_EMULATION");
                }

                var value = key.GetValue("Landdb.exe");
                if (value == null) {
                    key.SetValue("Landdb.exe", 0x2AF8, Microsoft.Win32.RegistryValueKind.DWord);
                }
            } catch (Exception ex) {
                log.ErrorException("Encountered an exception while checking/updating the browser registry key.", ex);
            }
        }
    }
}
