﻿using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Infrastructure;
using Landdb.Infrastructure.Messages;
using Landdb.Resources;
using Landdb.ViewModel;
using Landdb.ViewModel.Overlays;
using NLog;
using System;
using System.Collections.Generic;
using System.Deployment.Application;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.Infrastructure {
    public sealed class ApplicationUpdater : IApplicationUpdater {
        Logger log = LogManager.GetCurrentClassLogger();
        ApplicationUpdateStatusViewModel updateStatus;
        ApplicationPrereqUpdateStatusViewModel prereqUpdateStatus;

        public ApplicationUpdater() {
            updateStatus = new ApplicationUpdateStatusViewModel();
            prereqUpdateStatus = new ApplicationPrereqUpdateStatusViewModel();
        }

        public async Task<bool> CheckForUpdatesAsync() {
            if (!NetworkStatus.IsInternetAvailableFast()) {
                updateStatus.IsUpdateAvailable = false;
                return false;
            }

            if (System.Diagnostics.Debugger.IsAttached) { // Just to avoid annoying all the devs. ;)
                updateStatus.IsUpdateAvailable = false;
                return false;
            } 

            if (!ApplicationDeployment.IsNetworkDeployed) { // Don't try to update a non-ClickOnce copy
                updateStatus.IsUpdateAvailable = false;
                return false;
            }

            var info = await Task.Run(() => {
                var d = ApplicationDeployment.CurrentDeployment.CheckForDetailedUpdate(false);
                return d;
            });

            updateStatus.IsUpdateAvailable = info.UpdateAvailable;
            if(info.UpdateAvailable) {
                updateStatus.AvailableVersion = info.AvailableVersion;
            }
            return updateStatus.IsUpdateAvailable.GetValueOrDefault();
        }

        public async Task PerformUpdateAsync() {
            bool isCompleteInternal = false;
            updateStatus.IsUpdating = true;
            updateStatus.IsUpdateComplete = false;

            if (updateStatus.IsUpdateAvailable == null) {
                await CheckForUpdatesAsync();
            }

            /// !!! Test code
            //updateStatus.AvailableVersion = new Version(12, 14, 3, 7);
            //while (updateStatus.CurrentProgress < 100) {
            //    updateStatus.CurrentProgress += 10;

            //    int fakeByteTotal = 658293857;
            //    int currentFakeBytes = (int)((double)fakeByteTotal * updateStatus.CurrentProgress / 100);
            //    updateStatus.StatusText = $"{updateStatus.CurrentProgress}% complete. ({currentFakeBytes} bytes / {currentFakeBytes} bytes downloaded)";

            //    await Task.Delay(2000);
            //}
            //updateStatus.IsUpdating = false;
            //updateStatus.IsUpdateComplete = true;
            //updateStatus.HasErrors = true;
            //updateStatus.StatusText = $"Encountered an error while updating. NullReference exception, as always....";
            /// !!! End test code

            if (updateStatus.IsUpdateAvailable.GetValueOrDefault()) {
                updateStatus.MaxProgress = 100;
                updateStatus.IsUpdateComplete = false;

                ApplicationDeployment.CurrentDeployment.UpdateProgressChanged += (s, e) => {
                    updateStatus.CurrentProgress = e.ProgressPercentage;
                    updateStatus.StatusText = String.Format(Strings.UpdateStatus_Format_Text, e.ProgressPercentage, e.BytesCompleted, e.BytesTotal);
                };

                ApplicationDeployment.CurrentDeployment.UpdateCompleted += (s, e) => {
                    isCompleteInternal = true;
                    updateStatus.IsUpdating = false;
                    updateStatus.IsUpdateComplete = true;
                    //if (!e.Cancelled && e.Error == null) {
                    //    var vm = new ApplyUpdatesViewModel();
                    //    Messenger.Default.Send(new ShowOverlayMessage() {
                    //        ScreenDescriptor = new ScreenDescriptor(typeof(Views.Overlays.ApplicationUpdateView), "updateConfirm", vm)
                    //    });
                    //}
                    if (e.Error != null) {
                        log.ErrorException("Current deployment failed to update with an exception.", e.Error);
                        updateStatus.HasErrors = true;
                        updateStatus.StatusText = String.Format(Strings.UpdateStatus_Format_EncounteredAnError_Text, e.Error.Message);
                    }
                };

                updateStatus.IsUpdating = true;
                ApplicationDeployment.CurrentDeployment.UpdateAsync();
            } else { isCompleteInternal = true; }

            while(!isCompleteInternal) {
                await Task.Delay(500);
            }
        }

        public ApplicationUpdateStatusViewModel UpdateStatusModel {
            get { return updateStatus; }
        }

        public ApplicationPrereqUpdateStatusViewModel PreRequisiteStatusModel {
            get { return prereqUpdateStatus; }
        }

        public void GetOptionalFilegroup(string groupName, IApplicationUpdateStatus updateStatus, Action onComplete = null) {
            if (!NetworkStatus.IsInternetAvailableFast()) { return; }

            //if (System.Diagnostics.Debugger.IsAttached) { return; } // Just to avoid annoying all the devs. ;)
            if (!ApplicationDeployment.IsNetworkDeployed) { return; } // Don't try to update a non-ClickOnce copy

			Task.Run(() => {
                try {
					ApplicationDeployment deploy = ApplicationDeployment.CurrentDeployment;
                    if (!deploy.IsFileGroupDownloaded(groupName)) {
                        var updateVm = new OptionalUpdateDownloadViewModel() { MaxProgress = 100 };
						Messenger.Default.Send(new ShowOverlayMessage() {
							ScreenDescriptor = new ScreenDescriptor(typeof(Views.Overlays.OptionalUpdateDownloadView), "updateProgress", updateVm)
						});

                        deploy.DownloadFileGroupProgressChanged += (s, e) => {
                            if (updateStatus != null) {
                                updateStatus.CurrentProgress = e.ProgressPercentage;
                            }

                            updateVm.CurrentProgress = e.ProgressPercentage;
                        };
                        deploy.DownloadFileGroupCompleted += (s, e) => {
                            if (updateStatus != null) {
                                updateStatus.IsUpdating = false;
                            }

                            updateVm.IsUpdating = false;

                            if (!e.Cancelled && e.Error == null) {
								Messenger.Default.Send(new HideOverlayMessage());
                                //var vm = new ViewModel.Overlays.ApplyUpdatesViewModel();
                                //ScreenDescriptor sd = new ScreenDescriptor("Landdb.Views.Overlays.ApplicationUpdateView", "updateConfirm", vm);
                                //Messenger.Default.Send<ShowOverlayMessage>(new ShowOverlayMessage() { ScreenDescriptor = sd });
                            }

                            if (e.Error != null) {
								log.ErrorException("Current deployment failed to update with an exception.", e.Error);
								Messenger.Default.Send(new HideOverlayMessage());
								var vm = new UpdateErrorOverlayViewModel(e.Error);
								Messenger.Default.Send(new ShowOverlayMessage() {
                                    ScreenDescriptor = new ScreenDescriptor(typeof(Views.Overlays.UpdateErrorOverlayView), "appupdatefail", vm)
                                });
                            }

							onComplete?.Invoke();
						};

                        if (updateStatus != null) {
                            updateStatus.IsUpdating = true;
                        }
                        updateVm.IsUpdating = true;
                        deploy.DownloadFileGroupAsync(groupName, updateStatus);
                    }
                    
                } catch (Exception ex) {
					log.ErrorException("Caught an exception while trying to update the app.", ex);
					Messenger.Default.Send(new HideOverlayMessage());
					var vm = new UpdateErrorOverlayViewModel(ex);
					Messenger.Default.Send(new ShowOverlayMessage() {
                        ScreenDescriptor = new ScreenDescriptor(typeof(Views.Overlays.UpdateErrorOverlayView), "appupdatefail", vm)
                    });

					onComplete?.Invoke();
				}
            });
        }

        public bool OptionalFilegroupExists(string groupName) {
            throw new NotImplementedException();
        }

        public Task<bool> CheckForPrerequisiteUpdatesAsync() {
            bool result = false;

            if(!ThinkGeo9PrerequisitesExist()) {
                result = true;
                PreRequisiteStatusModel.AddPrereq(Strings.ThinkGeo_Info_Text, Strings.ThinkGeo_MoreInfo_Text, "http://landdb.blob.core.windows.net/public-dist/TG9PreReq/ThinkGeoPreReqInstallInstructions.html");
            }
            return Task.FromResult(result);
        }

        bool ThinkGeo9PrerequisitesExist() {
            try {
                string tg9UninstallKey = "{DDAC9121-6FCC-474B-BFA2-7BCBAFD5DDBD}";
                return IsUninstallRegistrySubKeyPresent(tg9UninstallKey);
            } catch (Exception ex) {
                log.ErrorException("Encountered an exception while checking for ThinkGeo 9 prerequisites.", ex);
            }
            return false;
        }

        static bool IsUninstallRegistrySubKeyPresent(string keyName) {
            Microsoft.Win32.RegistryKey key;
            bool found = false;

            // search in: CurrentUser
            key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey($"SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\{keyName}");
            if (key != null) { found = true; }

            // search in: LocalMachine_32
            key = Microsoft.Win32.Registry.LocalMachine.OpenSubKey($"SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\{keyName}");
            if (key != null) { found = true; }

            // search in: LocalMachine_64
            key = Microsoft.Win32.Registry.LocalMachine.OpenSubKey($"SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\{keyName}");
            if (key != null) { found = true; }

            return found;
        }
    }
}
