﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reactive.Linq;

namespace Landdb.Infrastructure {
    public static class ObservableEx {
        public static IObservable<T> OnTimeline<T>(this IObservable<T> source, TimeSpan period) {
            return source.Zip(Observable.Interval(period), (d, t) => d);
        }
    }
}
