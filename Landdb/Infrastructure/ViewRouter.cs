﻿using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.Infrastructure {
    public static class ViewRouter {
        public static void JumpToApplication(ApplicationId id) {
            RouteRequestMessage msg = new RouteRequestMessage() {
                Route = "Landdb.Views.ProductionPageView",
                Next = new RouteRequestMessage() {
                    Route = "Landdb.Views.Production.ApplicationsPageView",
                    Next = new RouteRequestMessage() {
                        Route = id
                    }
                }
            };

            Messenger.Default.Send<RouteRequestMessage>(msg);
        }

        public static void JumpToInvoice(InvoiceId id) {
            RouteRequestMessage msg = new RouteRequestMessage() {
                Route = "Landdb.Views.ProductionPageView",
                Next = new RouteRequestMessage() {
                    Route = "Landdb.Views.Production.InvoicesPageView",
                    Next = new RouteRequestMessage() {
                        Route = id
                    }
                }
            };

            Messenger.Default.Send<RouteRequestMessage>(msg);
        }

		public static void JumpToYieldLoad(LoadId id, bool isFromField) {
			var yieldPage = isFromField ? "Landdb.Views.Yield.HarvestLoadsPageView" : "Landdb.Views.Yield.StorageLoadsPageView";

			var msg = new RouteRequestMessage() {
				Route = "Landdb.Views.YieldPageView",
				Next = new RouteRequestMessage() {
					Route = yieldPage,
					Next = new RouteRequestMessage() {
						Route = id
					}
				}
			};

			Messenger.Default.Send<RouteRequestMessage>(msg);
		}

        public static void JumpToLandInfo()
        {
            RouteRequestMessage msg = new RouteRequestMessage()
            {
                Route = "Landdb.Views.FieldsPageView",
                Next = new RouteRequestMessage()
                {
                    Route = "Landdb.Views.FieldDetailsPageView",
                    //Next = new RouteRequestMessage()
                    //{
                    //    Route = id
                    //}
                }
            };

            Messenger.Default.Send<RouteRequestMessage>(msg);
        }

    }
}
