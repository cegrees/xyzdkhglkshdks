﻿using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.Infrastructure {

    public static class IsActivatedExtensions {
        public static IObservable<bool> GetIsActivated(this ISupportsActivation @this) {
            //Ensure.ArgumentNotNull(@this, nameof(@this));

            return Observable
                .Merge(
                    @this.Activator.Activated.Select(_ => true),
                    @this.Activator.Deactivated.Select(_ => false))
                .Replay(1)
                .RefCount();
        }
    }
}
