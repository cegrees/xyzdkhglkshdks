﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;
using ThinkGeo.MapSuite.Core;

namespace Landdb.Infrastructure.Mapping {
    public static class MapImaging {
        public static BitmapImage GetBitMap(IEnumerable<Layer> layers, RectangleShape currentExtent, GeographyUnit mapUnit, int width, int height) {
            Bitmap nativeImage = new Bitmap(width, height);
            Collection<SimpleCandidate> simpleCandidates = new Collection<SimpleCandidate>();
            GdiPlusGeoCanvas geoCanvas = new GdiPlusGeoCanvas();
            geoCanvas.BeginDrawing(nativeImage, currentExtent, mapUnit);
            foreach (Layer layer in layers) {
                layer.Draw(geoCanvas, simpleCandidates);
            }
            geoCanvas.EndDrawing();

            BitmapImage bmImage = null;

            using(MemoryStream memory = new MemoryStream()) {
                nativeImage.Save(memory, ImageFormat.Png);
                memory.Position = 0;
                bmImage = new BitmapImage();
                bmImage.BeginInit();
                bmImage.StreamSource = memory;
                bmImage.CacheOption = BitmapCacheOption.OnLoad;
                bmImage.EndInit();
            }

            nativeImage.Save(@"C:\temp\sampleFieldBitmap.bmp");

            return bmImage;
        }
    }
}
