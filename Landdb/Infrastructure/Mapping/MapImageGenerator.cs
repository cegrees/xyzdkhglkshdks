﻿using Landdb.Client.Infrastructure;
using Landdb.Client.Spatial;
using Landdb.Domain.ReadModels.Map;
using Microsoft.Maps.MapControl.WPF;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using Landdb.Resources;

namespace Landdb.Infrastructure.Mapping {
    public class MapImageGenerator {
        public Logger log = LogManager.GetCurrentClassLogger();

        public async Task<string> GenerateMap(IClientEndpoint clientEndpoint, Dispatcher dispatcher, IEnumerable<CropZoneId> cropZoneIds) {
            string result = string.Empty;
            Window previewWindow = null;
            Map bingMap = null;
            MapLayer bingMapsLayer;
            System.Windows.Shapes.Path cropzonePreview;
            var mapDataList = new List<string>();

            // Create the map on the UI thread
            await dispatcher.BeginInvoke(new Action(() => {
                previewWindow = new Window();
                bingMap = new Map();
                bingMap.Mode = new RoadMode();
                bingMap.CredentialsProvider = new ApplicationIdCredentialsProvider("AgwFGldoH7RpgsCWgbL0pqvweosSbsuGvRinCPUPgJwGUlqwYBYsRY3O33dQHxAy");
                bingMap.CacheMode = new BitmapCache();
                previewWindow.Width = 800;
                previewWindow.Height = 600;
                previewWindow.Title = Strings.GeneratingMapPreviewImage_Text;
                System.Windows.Controls.Grid grid = new System.Windows.Controls.Grid();
                previewWindow.Content = grid;
                grid.Children.Add(bingMap);
                bingMap.Width = 800;
                bingMap.Heading = 600;
            }));

            foreach (var c in cropZoneIds) {
                var mapItem = clientEndpoint.GetView<ItemMap>(c);
                string mapData = null;
                if (mapItem.HasValue && mapItem.Value.MostRecentMapItem != null) {
                    mapData = mapItem.Value.MostRecentMapItem.MapData;
                    mapDataList.Add(mapData);
                }
            }
            try {
                if (mapDataList.Any()) {
                    var network = NetworkStatus.IsInternetAvailableFast();
                    var ShouldShowWpfMock = !network;

                    await dispatcher.BeginInvoke(new Action(() => {
                        if (network) {
                            // gen image in Bing Maps
                            bingMapsLayer = BingMapsUtility.GetLayerForMapData(mapDataList);
                            previewWindow.Show();
                            foreach (var kid in bingMapsLayer.Children) {
                                MapPolygon poly = (MapPolygon)kid;
                                poly.Fill = System.Windows.Media.Brushes.Green;
                            }
                            bingMap.Children.Add(bingMapsLayer);

                            var box = bingMapsLayer.GetBoundingBox().Inflate(.8);
                            bingMap.SetView(box);
                            bingMap.UpdateLayout();
                        } else {
                            cropzonePreview = WpfTransforms.CreatePathFromMapData(mapDataList);
                        }
                    }));

                    await Task.Delay(500); // tweak this delay if map isn't properly rendering...

                    RenderTargetBitmap bmp = new RenderTargetBitmap((int)bingMap.ActualWidth, (int)bingMap.ActualHeight, 120, 96, PixelFormats.Pbgra32);
                    bmp.Render(bingMap);

                    PngBitmapEncoder png = new PngBitmapEncoder();
                    png.Frames.Add(BitmapFrame.Create(bmp));
                    var tempName = System.IO.Path.GetTempPath();
                    tempName += "Landdb_maptemp.png";

                    using (Stream stm = File.Create(tempName)) {
                        png.Save(stm);
                    }

                    result = tempName;
                }
            } catch (Exception ex) {
                log.WarnException("Error while genning a map image.", ex);
            } finally {
                if (previewWindow.IsVisible) {
                    dispatcher.BeginInvoke(new Action(() => {
                        previewWindow.Close(); // make sure the preview window is closed
                    }));
                }
            }

            return result;
        }
    }
}
