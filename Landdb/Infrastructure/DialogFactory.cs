﻿using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel;
using Landdb.ViewModel.Shared.Popups;
using Landdb.Views.Shared.Popups;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Landdb.Infrastructure {
	public static class DialogFactory {

		public static void ShowYesNoDialog(string title, string description, Action onYes, Action onNo) {
			YesNoConfirmationDialogViewModel vm = new YesNoConfirmationDialogViewModel(title, description, onYes, onNo);
			Messenger.Default.Send(new ShowPopupMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(YesNoConfirmationDialog), "yesNoDialog", vm)
			});
		}

		public static async Task ShowYesNoDialogAsync(string title, string description, Func<Task> onYes, Func<Task> onNo) {
			YesNoConfirmationDialogViewModel vm = new YesNoConfirmationDialogViewModel(title, description, () => onYes(), () => onNo());
			Messenger.Default.Send(new ShowPopupMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(YesNoConfirmationDialog), "yesNoDialog", vm)
			});
		}

		public static async Task ShowTwoOptionDialog(string title, string description, string action1Text, Action onAction1, string action2Text, Action onAction2) {
			Task task1 = new Task(onAction1);
			Task task2 = new Task(onAction2);

			MultiOptionConfirmationDialogViewModel vm = new MultiOptionConfirmationDialogViewModel(title, description, action1Text, task1, action2Text, task2);
			Messenger.Default.Send(new ShowPopupMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(MultiOptionConfirmationDialog), "yesNoDialog", vm)
			});
			await Task.WhenAny(task1, task2);
		}

		public static void ShowMessage(string title, string description, string dismissButtonName = null, Action dismissAction = null) {
			MessageDialogViewModel vm = new MessageDialogViewModel(title, description, dismissButtonName, dismissAction);
			Messenger.Default.Send(new ShowPopupMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(MessageDialog), "messageDialog", vm)
			});
		}

		public static void ShowNameChangeDialog(string entityDescription, string currentName, Action<string> newNameAction) {
			var vm = new ChangeEntityNameViewModel(entityDescription, currentName, changeVm => {
				if (changeVm != null && changeVm.HasChanges) {
					newNameAction(changeVm.EntityName);
				}

				Messenger.Default.Send(new HidePopupMessage());
			});

			Messenger.Default.Send(new ShowPopupMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(ChangeEntityNameView), $"change {entityDescription} name", vm)
			});
		}

		public static void ShowNotesChangeDialog(string currentNotes, Action<string> newNotesAction) {
			var vm = new UpdateEntityNotesViewModel(currentNotes, changeVm => {
				if (changeVm != null && changeVm.HasChanges) {
					newNotesAction(changeVm.Notes);
				}

				Messenger.Default.Send(new HidePopupMessage());
			});

			Messenger.Default.Send(new ShowPopupMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(UpdateEntityNotesView), "update notes", vm)
			});
		}

		public static void ShowDateTimeChangeDialog(DateTime currentDateTime, ChangeEntityDateTimeViewModel.EntityDateTimeDisplayMode displayMode, Action<DateTime> newDateTimeAction) {
			var vm = new ChangeEntityDateTimeViewModel(currentDateTime, displayMode, changeVm => {
				if (changeVm != null && changeVm.HasChanges) {
					newDateTimeAction(changeVm.GetStartDateTime());
				}

				Messenger.Default.Send(new HidePopupMessage());
			});

			Messenger.Default.Send(new ShowPopupMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(ChangeEntityDateTimeView), "update date", vm)
			});
		}

		public static void ShowNumericChangeDialog(string valueDescription, decimal currentValue, string stringFormat, string warningText, Action<decimal> newValue) {
			var vm = new ChangeEntityNumericViewModel(valueDescription, currentValue, stringFormat, warningText, changeVm => {
				if (changeVm != null && changeVm.HasChanges) {
					newValue(changeVm.NewValue);
				}

				Messenger.Default.Send(new HidePopupMessage());
			});

			Messenger.Default.Send(new ShowPopupMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(ChangeEntityNumericView), $"change {valueDescription}", vm)
			});
		}

		public static void ShowStreetAddressChangeDialog(StreetAddress currentAddress, Action<StreetAddress> newAddress) {
			var vm = new UpdateEntityStreetAddressViewModel(currentAddress, changeVm => {
				if (changeVm != null && changeVm.HasChanges) {
					newAddress(changeVm.StreetAddress.GetValueObject());
				}

				Messenger.Default.Send(new HidePopupMessage());
			});

			Messenger.Default.Send(new ShowPopupMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(UpdateEntityStreetAddressView), "update street address", vm)
			});
		}

		public static void ShowComboBoxSelectionChangeDialog<TEquatableListItem>(TEquatableListItem currentSelectedItem, IEnumerable<TEquatableListItem> availableListItems, string entityDescription, Action<TEquatableListItem> newSelection) where TEquatableListItem : IEquatable<TEquatableListItem> {
			var vm = new ChangeEntityComboBoxSelection<TEquatableListItem>(currentSelectedItem, availableListItems, entityDescription, changeVm => {
				if (changeVm != null && changeVm.HasChanges) {
					newSelection(changeVm.SelectedListItem);
				}

				Messenger.Default.Send(new HidePopupMessage());
			});

			Messenger.Default.Send(new ShowPopupMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(ChangeEntityComboBoxSelectionView), "update street address", vm)
			});
		}

		public static void ShowContactInfoChangeDialog(string currentName, string currentPhoneNumber, string currentEmailAddress, Action<Tuple<string, PhoneNumber, Email>> newInfo) {
			var vm = new UpdateEntityContactInfoViewModel(currentName, currentPhoneNumber, currentEmailAddress, changeVm => {
				if (changeVm != null && changeVm.HasChanges) {

					newInfo(new Tuple<string, PhoneNumber, Email>(
						changeVm.Name,
						changeVm.GetPhoneNumberValueObject(),
						changeVm.GetEmailAddressValueObject()
					));
				}

				Messenger.Default.Send(new HidePopupMessage());
			});

			Messenger.Default.Send(new ShowPopupMessage() {
				ScreenDescriptor = new ScreenDescriptor(typeof(UpdateEntityContactInfoView), "update contact info", vm)
			});
		}
	}
}