﻿using Landdb.Client.Account;
using Microsoft.Win32;
using System;
using System.IO;
using System.Text.RegularExpressions;
namespace Landdb.Infrastructure {
	internal static class ApplicationEnvironment {
		static string dataDirectoryOverride = null;
		static string uriOverride = null;

		public static string CompanyName => "AgConnections";
		public static string ApplicationName => "Landdb";

		public static string LogDirectory => Path.Combine(UserAppDataPath, "logs");
		public static string TemplateDirectory => Path.Combine(UserAppDataPath, "templates");
		public static string ExportDirectory => Path.Combine(UserAppDataPath, "exports");
		public static string MigratorDataDirectory => Path.Combine(UserAppDataPath, "mdata");
		public static string SettingsDirectory => Path.Combine(UserAppDataPath, "settings");
		public static string MapCacheDirectory => Path.Combine(UserAppDataPath, "map_cache");
		public static string RoadCacheDirectory => Path.Combine(UserAppDataPath, "road_cache");
        public static string GoogleMapCacheDirectory => Path.Combine(UserAppDataPath, "google_map_cache");
        public static string GoogleRoadCacheDirectory => Path.Combine(UserAppDataPath, "google_road_cache");
        public static string PlanetLabsImageCacheDirectory => Path.Combine(UserAppDataPath, "planetlabs_image_cache");
        public static string LayerCacheDirectory => Path.Combine(UserAppDataPath, "layer_cache");
		public static string MasterlistDirectory => Path.Combine(UserAppDataPath, "masterlist");
		public static string ApplicationExecutableDirectory => AppDomain.CurrentDomain.BaseDirectory;
        public static string UserAppDataPath {
			get {
				var path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), string.Format("{0}\\{1}", CompanyName, ApplicationName));
				return path;
			}
		}

		public static string BaseDataDirectory {
			get {
				if (string.IsNullOrWhiteSpace(dataDirectoryOverride)) {
					var dataPath = uriOverride == null ? "data" : string.Format("data_{0}", uriOverride);
					return Path.Combine(UserAppDataPath, dataPath);
				} else {
					return dataDirectoryOverride;
				}
			}
		}

		public static string DataDirectory {
			get {
				var baseDirectory = BaseDataDirectory;
				var culture = string.Format("{0}", Client.Properties.Settings.Default.MasterlistGroup);
				return Path.Combine(baseDirectory, culture);
			}
		}

		public static Guid CurrentDataSourceId { get; internal set; }
		public static int CurrentCropYear { get; internal set; }
		public static string CurrentDataSourceName { get; internal set; }
	    private static IDataSource _currentDataSource;
		public static IDataSource CurrentDataSource {
		    get => _currentDataSource;
		    internal set {
		        _currentDataSource = value;
		        Client.Services.MasterlistService.DefaultAreaUnit = value.DefaultAreaUnit;
		    } }

        private static Landdb.ViewModel.Maps.ImageryOverlay.MapCultureFactory mapcultureFactory = new Landdb.ViewModel.Maps.ImageryOverlay.MapCultureFactory();
        public static string BingWpfMapCulture
        {
            get { return mapcultureFactory.BingWpfMapControlCultureFactory(); }
        }

        public static string GoogleMapCulture
        {
            get { return mapcultureFactory.GoogleMapCultureFactory(); }
        }

        public static bool ShowSustainability { get; internal set; }
		public static bool SkipDefaultWindowPlacement { get; internal set; }

		internal static IDataSource FakeLocalDataSource { get; set; }
		public static string FieldToMarketRemoteUri { get; set; }

        private static string masterlistRemoteUri = "https://masterlist.agconnections.com/api";
        public static string MasterlistRemoteUri {
            get { return masterlistRemoteUri; }
            set { masterlistRemoteUri = value; }
        }
        internal static string GetSystemDefaultBrowserPath() {
			string name = string.Empty;
			RegistryKey regKey = null;

			try {
				//set the registry key we want to open
				regKey = Registry.ClassesRoot.OpenSubKey("HTTP\\shell\\open\\command", false);

				//get rid of the enclosing quotes
				name = regKey.GetValue(null).ToString().ToLower().Replace("" + (char)34, "");

				//check to see if the value ends with .exe (this way we can remove any command line arguments)
				if (!name.EndsWith("exe"))
					//get rid of all command line arguments (anything after the .exe must go)
					name = name.Substring(0, name.LastIndexOf(".exe") + 4);

			} catch (Exception) {
				// TODO: Log exception
				name = string.Empty; //string.Format("ERROR: An exception of type: {0} occurred in method: {1} in the following module: {2}", ex.GetType(), ex.TargetSite, this.GetType());
			} finally {
				//check and see if the key is still open, if so
				//then close it
				if (regKey != null)
					regKey.Close();
			}
			//return the value
			return name;
		}

		/// <summary>
		/// Used to override the path data is pulled from. Passing a null or whitespace
		/// argument will reset the data directory to the default.
		/// </summary>
		/// <param name="dataPath">Full path of where data should be read/written</param>
		internal static void OverrideDataDirectory(string dataPath) {
			dataDirectoryOverride = dataPath;
		}

		internal static void SetCustomUri(string uri) {
			if (string.IsNullOrWhiteSpace(uri)) { return; }

			string regexPattern = @"^(?<s1>(?<s0>[^:/\?#]+):)?(?<a1>"
				+ @"//(?<a0>[^/\?#]*))?(?<p0>[^\?#]*)"
				+ @"(?<q1>\?(?<q0>[^#]*))?"
				+ @"(?<f1>#(?<f0>.*))?";
			Regex re = new Regex(regexPattern, RegexOptions.ExplicitCapture);
			Match m = re.Match(uri);

			if (uri.StartsWith("http")) {
				var v = m.Groups["a0"].Value;
				if (v.Contains(":")) {
					v = v.Split(':')[0];
				}
				uriOverride = v;
			} else {
				uriOverride = uri;
			}
		}
        public static bool SearchFieldHistory { get; internal set; }
        public static bool ShowPremiumMap { get; internal set; }
        public static bool ShowPlanetLabsImagery { get; internal set; }
        public static bool ShowPlanetPlanetPageLoaded { get; internal set; }
        public static bool ShowElluminateTab { get; internal set; }

        private static double planetlabsMaximumAOI = 320;
        public static double PlanetLabsMaximumAOI
        {
            get { return planetlabsMaximumAOI; }
            set { planetlabsMaximumAOI = value; }
        }

        private static bool planetPageLoaded = false;
        public static bool PlanetPageLoaded
        {
            get { return planetPageLoaded; }
            set { planetPageLoaded = value; }
        }
    }
}
