﻿using Landdb.Client.Infrastructure;
using Landdb.Models;
using Landdb.Web.ServiceContracts.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.Infrastructure {
    public class AuthMigrationCheck {
        IClientEndpoint clientEndpoint;

        public AuthMigrationCheck(IClientEndpoint clientEndpoint) {
            this.clientEndpoint = clientEndpoint;
        }

        public bool ShouldPromptForAuthMigration() {
            var oldUserAccountInfo = GetOldMostRecentUser();
            if (oldUserAccountInfo == null) { return false; }

            var lastUser = GetLastSignedInUser();
            return lastUser == null;
        }

        UserAccount GetOldMostRecentUser() {
            var settingsId = new NullId();
            var account = clientEndpoint.GetPersistentView<UserAccount>(settingsId);

            return account.HasValue ? account.Value : null;
        }

        LastUserSignInInformation GetLastSignedInUser() {
            var settingsId = new NullId();
            var info = clientEndpoint.GetPersistentView<LastUserSignInInformation>(settingsId);

            return info.HasValue ? info.Value : null;
        }
    }
}
