﻿namespace Landdb.Infrastructure {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using NLog.Config;
    using NLog.Targets;
    using NLog;
    using System.IO;
    using System.Reflection;
    using System.Net.NetworkInformation;

    public static class LogConfiguration {

        internal static void ConfigureLogging() {
            // Step 1. Create configuration object 
            LoggingConfiguration config = new LoggingConfiguration();

            
            // Step 2. Create targets and add them to the configuration 
            ColoredConsoleTarget consoleTarget = new ColoredConsoleTarget();
            config.AddTarget("console", consoleTarget);
#if DEBUG
            ConsoleManager.Show();
#endif

            FileTarget fileTarget = new FileTarget();
            config.AddTarget("file", fileTarget);

            ApplicationInsightsTarget aiTarget = new ApplicationInsightsTarget(App.CurrentApp.Telemetry);
            config.AddTarget("app_insights", aiTarget);

            // Step 3. Set target properties 
            consoleTarget.Layout = "${date:format=HH\\:MM\\:ss} ${logger} ${message} ${exception:innerFormat=ToString,StackTrace:maxInnerExceptionLevel=5:format=ToString,StackTrace}";

            var logFileName = string.Format("{0}.log", ApplicationEnvironment.ApplicationName);
            fileTarget.FileName = Path.Combine(ApplicationEnvironment.LogDirectory, logFileName);
            fileTarget.ArchiveFileName = Path.Combine(ApplicationEnvironment.LogDirectory, logFileName.Replace(".log", "{##}.log"));
            fileTarget.ArchiveNumbering = ArchiveNumberingMode.Rolling;
            fileTarget.ArchiveEvery = FileArchivePeriod.Day;
            fileTarget.MaxArchiveFiles = 14;
            fileTarget.Layout = "${longdate} ${logger} ${message} ${exception:innerFormat=ToString,StackTrace:maxInnerExceptionLevel=5:format=ToString,StackTrace}";

            
            // Step 4. Define rules
            LoggingRule rule1 = new LoggingRule("Landdb*", LogLevel.Debug, consoleTarget);
            config.LoggingRules.Add(rule1);

            LoggingRule rule2 = new LoggingRule("Landdb*", LogLevel.Debug, fileTarget);
            config.LoggingRules.Add(rule2);

            LoggingRule rule3 = new LoggingRule("Landdb*", LogLevel.Trace, aiTarget);
            config.LoggingRules.Add(rule3);

            
            // Step 5. Activate the configuration
            LogManager.Configuration = config;

            Logger log = LogManager.GetCurrentClassLogger();
            log.Debug("Logging engine started");
        }

        internal static void LogStartupInformation() {
            Logger log = LogManager.GetCurrentClassLogger();

            AssemblyName entryAssemblyName = Assembly.GetEntryAssembly().GetName();
            log.Info("*** {0}", entryAssemblyName.FullName);
            log.Info("*** Executable path: {0}", entryAssemblyName.CodeBase);

            var nis = NetworkInterface.GetAllNetworkInterfaces();

            var upQ = from ni in nis
                      where ni.OperationalStatus == OperationalStatus.Up &&
                            ni.NetworkInterfaceType != NetworkInterfaceType.Loopback &&
                            ni.NetworkInterfaceType != NetworkInterfaceType.Tunnel
                      select ni;

            foreach (var ni in upQ) {
                log.Debug("Available Network Interface [{0}]: {1}", ni.Name, ni.NetworkInterfaceType.ToString());
            }
        }
    }

    [Target("AppInsights")]
    public class ApplicationInsightsTarget : Target {
        readonly Microsoft.ApplicationInsights.TelemetryClient telemetry;

        public ApplicationInsightsTarget(Microsoft.ApplicationInsights.TelemetryClient telemetry) {
            this.telemetry = telemetry;
        }

        protected override void Write(LogEventInfo logEvent) {
            if (logEvent.Level == LogLevel.Fatal) { return; } // We're handling fatal exceptions differently.
            if (logEvent.Exception != null) {
                telemetry.TrackException(logEvent.Exception);
            } else {
                telemetry.TrackTrace(logEvent.FormattedMessage, ConvertNLogSeverity(logEvent.Level));
            } 
        }

        public static Microsoft.ApplicationInsights.DataContracts.SeverityLevel ConvertNLogSeverity(LogLevel level) {
            if (level == LogLevel.Info) {
                return Microsoft.ApplicationInsights.DataContracts.SeverityLevel.Information;
            }
            if (level == LogLevel.Error) {
                return Microsoft.ApplicationInsights.DataContracts.SeverityLevel.Error;
            }
            if (level == LogLevel.Warn) {
                return Microsoft.ApplicationInsights.DataContracts.SeverityLevel.Warning;
            }
            if (level == LogLevel.Fatal) {
                return Microsoft.ApplicationInsights.DataContracts.SeverityLevel.Critical;
            }
            if (level == LogLevel.Trace || level == LogLevel.Debug) {
                return Microsoft.ApplicationInsights.DataContracts.SeverityLevel.Verbose;
            }
            return Microsoft.ApplicationInsights.DataContracts.SeverityLevel.Verbose;
            
        }
    }
}
