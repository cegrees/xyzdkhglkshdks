﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization;
using System.Net;

namespace Landdb.Infrastructure {
    public static class Extensions {
        private const long ExpirationBuffer = 10;

        public static string Serialize<T>(T obj) {
            using (var ms = new MemoryStream()) {
                var serializer = new DataContractSerializer(typeof(T));
                serializer.WriteObject(ms, obj);
                if (ms.Length > int.MaxValue) {
                    throw new Exception("Message too long to serialize");
                } else {
                    var enc = new UTF8Encoding();
                    return enc.GetString(ms.ToArray(), 0, (int)ms.Length);
                }
            }
        }

        public static T Deserialize<T>(string xml) {
            using (var ms = new MemoryStream(Encoding.UTF8.GetBytes(xml))) {
                var serializer = new DataContractSerializer(typeof(T));
                return (T)serializer.ReadObject(ms);
            }
        }

        public static WebHeaderCollection ParseQueryString(string queryString) {
            var res = new WebHeaderCollection();
            int num = (queryString != null) ? queryString.Length : 0;
            for (int i = 0; i < num; i++) {
                int startIndex = i;
                int num4 = -1;
                while (i < num) {
                    char ch = queryString[i];
                    if (ch == '=') {
                        if (num4 < 0) {
                            num4 = i;
                        }
                    } else if (ch == '&') {
                        break;
                    }

                    i++;
                }

                var str = string.Empty;
                var str2 = string.Empty;
                if (num4 >= 0) {
                    str = queryString.Substring(startIndex, num4 - startIndex);
                    str2 = queryString.Substring(num4 + 1, (i - num4) - 1);
                } else {
                    str2 = queryString.Substring(startIndex, i - startIndex);
                }

                res[str.Replace("?", string.Empty)] = Uri.UnescapeDataString(str2);
            }

            return res;
        }

        public static string KiloFormat(this double num) {
            if (double.IsInfinity(num) || double.IsNaN(num) || double.IsNegativeInfinity(num) || double.IsPositiveInfinity(num)) { return num.ToString(); }

            if (num >= 100000000) {
                return (num / 1000000D).ToString("0.#M");
            }
            if (num >= 1000000) {
                return (num / 1000000D).ToString("0.##M");
            }
            if (num >= 100000) {
                return (num / 1000D).ToString("0.#k");
            }
            if (num >= 10000) {
                return (num / 1000D).ToString("0.##k");
            }

            return num.ToString("#,0");
        }
    }
}
