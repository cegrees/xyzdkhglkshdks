﻿using System;
using System.Windows.Threading;
using System.Windows;
using System.Collections;
using Landdb.Controls.OAuth2;
using Landdb.Client.Services.Credentials;
using Landdb.Client.Services.Web;
using Landdb.Client.Infrastructure;

namespace Landdb.Infrastructure {
    public class CloudClientFactory : ICloudClientFactory {
        private readonly IDictionary appResources;
        private readonly Dispatcher dispatcher;
        private readonly Guid deviceId;

        public CloudClientFactory() : this(Application.Current.Resources, Application.Current.Dispatcher, CloudClientFactory.GetDeviceId()) { }

        public CloudClientFactory(IDictionary appResources, Dispatcher dispatcher, Guid deviceId) {
            this.appResources = appResources;
            this.dispatcher = dispatcher;
            this.deviceId = deviceId;
        }

        public Guid DeviceId {
            get { return deviceId; }
        }

        public RequestSecurityTokenResponseStore TokenStore {
            get { return this.appResources["rstrStore"] as RequestSecurityTokenResponseStore; }
        }

        public IStorageCredentials ResolveStorageCredentials() {
            return this.TokenStore.Credentials;
        }

        public Client.Services.Web.IRegistrationClient ResolveRegistrationClient() {
            // TODO: Get the reg service endpoint from somewhere more configurable
            return new RegistrationClient(RemoteConfigurationSettings.GetRegistrationServicesUri(), this.ResolveStorageCredentials());
        }

        public Client.Services.Web.IDataSynchronizationClient ResolveDataSyncClient() {
            return new DataSynchronizationClient(RemoteConfigurationSettings.GetDataSynchronizationServicesUri(), this.ResolveStorageCredentials());
        }

        static Guid GetDeviceId() {
            var deviceId = Landdb.Client.Properties.Settings.Default.DeviceId;

            // Set the device ID if one doesn't already exist.
            // TODO: Probably need to write a custom store for storing this data as opposed to user settings, for security reasons
            if (deviceId == Guid.Empty) {
                deviceId = Guid.NewGuid();
                Landdb.Client.Properties.Settings.Default.DeviceId = deviceId;
                Properties.Settings.Default.Save();
            }

            return deviceId;
        }
    }
}