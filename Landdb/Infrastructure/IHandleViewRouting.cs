﻿using Landdb.Infrastructure.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.Infrastructure {
    public interface IHandleViewRouting {
        void HandleRouteRequest(RouteRequestMessage message);
    }
}
