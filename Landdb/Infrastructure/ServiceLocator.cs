﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ninject.Modules;
using Ninject;

namespace Landdb.Infrastructure {
    public static class ServiceLocator {
        static IKernel kernel;

        public static void Initialize(params INinjectModule[] modules) {
            kernel = new StandardKernel(modules);
        }

        public static T Get<T>() {
            return kernel.Get<T>();
        }
    }
}
