﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ninject.Modules;
using Landdb.Web.ServiceContracts;
using Landdb.Client.Services.Web;
using Landdb.Client.Account;
using Landdb.Client.Services;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Tree;
using Landdb.Domain;
using Landdb.Wires;
using Lokad.Cqrs;
using Lokad.Cqrs.AtomicStorage;
using Lokad.Cqrs.Evil;
using Landdb.Domain.ReadModels;
using Landdb.Client.Wires.MemoryStorage;
using Landdb.Domain.ReadModels.Product;
using Landdb.Services;
using Landdb.Client.Services.Credentials;

namespace Landdb.Infrastructure {
    public class LanddbModule : NinjectModule {
        public int? EventStoreOperationMax = null;
        public int? EventStoreOperationTimeout = null;

        public override void Load() {
            Bind<IViewCreator>().To<ViewCreator>();

            var accountStatus = new AccountStatus();
            Bind<IAccountStatus>().ToConstant<AccountStatus>(accountStatus).InSingletonScope();

            var remoteErrorNotifier = new RemoteSubmissionErrorNotifier();
            Bind<IRemoteSubmissionErrorNotifier>().ToConstant(remoteErrorNotifier).InSingletonScope();

            // Needed for synchronization
            Bind<ISynchronizationManager>().To<SynchronizationManager>() 
                .InSingletonScope()
                .WithParameter(new Ninject.Parameters.ConstructorArgument("serviceUri", RemoteConfigurationSettings.GetDataSynchronizationServicesUri()))
                .WithParameter(new Ninject.Parameters.ConstructorArgument("operationCountOverride", EventStoreOperationMax))
                .WithParameter(new Ninject.Parameters.ConstructorArgument("operationTimeoutOverride", EventStoreOperationTimeout));

            Bind<IRouteConfigurator>().To<RouteConfigurator>();
            //Bind<IMessageObserver>().To<MessageObserver>();
            Bind<INameAutoGenerationService>().To<NameAutoGenerationService>();

            var config = FileStorage.CreateConfig(ApplicationEnvironment.DataDirectory);  
            var memConfig = FastMemoryStorage.CreateConfig();
            var engineSetup = new EngineSetup();
            var appUpdater = new ApplicationUpdater();
            
            engineSetup.ApplicationUpdater = appUpdater;

            engineSetup.Streaming = config.CreateStreaming();
            engineSetup.PersistentDocumentStoreFactory = config.CreateDocumentStore;
            engineSetup.DocumentStoreFactory = x => memConfig.CreateNuclear(x).Container;

            engineSetup.QueueReaderFactory = s => config.CreateInbox(s, DecayEvil.BuildExponentialDecay(500));
            engineSetup.QueueWriterFactory = config.CreateQueueWriter;

            //engineSetup.FastQueueReaderFactory = s => memConfig.CreateInbox();
            //engineSetup.FastQueueWriterFactory = config.CreateQueueWriter;

            engineSetup.AppendOnlyStoreFactory = config.CreateAppendOnlyStore;

            engineSetup.DeviceId = GetDeviceId();
            engineSetup.AccountStatus = accountStatus;
            engineSetup.ResolveRemoteSubmissionClient = App.CloudClientFactory.ResolveDataSyncClient;
            engineSetup.RemoteSubmissionErrorNotifier = remoteErrorNotifier;

            engineSetup.ConfigureQueues(1, 1);

            var currentUiCulture = System.Globalization.CultureInfo.CurrentUICulture.Name;
            var masterlistGroup = Landdb.Client.Properties.Settings.Default.MasterlistGroup ?? currentUiCulture;
            engineSetup.MasterlistServiceFactory = d => new MasterlistService(ApplicationEnvironment.ApplicationExecutableDirectory, ApplicationEnvironment.MasterlistDirectory,d.GetReader<DataSourceId, UserCreatedProductList>(), appUpdater, masterlistGroup, currentUiCulture); 
            engineSetup.ProductUnitResolverFactory = ml => new ProductUnitResolver(ml);
            engineSetup.InventoryCalculationServiceFactory = () => new InventoryCalculationService();

            var components = engineSetup.Build();

            var datasourceCultureIsenUS = masterlistGroup.Equals("en-US");
            var Client = new ClientEndpoint(components, accountStatus, ApplicationEnvironment.DataDirectory, ApplicationEnvironment.SettingsDirectory, GetDeviceId(), appUpdater, App.CloudClientFactory.ResolveStorageCredentials, datasourceCultureIsenUS);

            Bind<IClientEndpoint>().ToConstant<ClientEndpoint>(Client).InSingletonScope();
            Bind<IEventStore>().ToConstant(components.EventStore).InSingletonScope();
            Bind<IApplicationUpdater>().ToConstant(appUpdater).InSingletonScope();
            Bind<IMessageProcessor>().To<MessageProcessor>().WithConstructorArgument("deviceId", GetDeviceId()).WithConstructorArgument("pendingCommands", components.PendingCommands);
        }

        Guid GetDeviceId() {
            var deviceId = Landdb.Client.Properties.Settings.Default.DeviceId;

            // Set the device ID if one doesn't already exist.
            // TODO: Probably need to write a custom store for storing this data as opposed to user settings, for security reasons
            if (deviceId == Guid.Empty) {
                deviceId = Guid.NewGuid();
                Landdb.Client.Properties.Settings.Default.DeviceId = deviceId;
                Properties.Settings.Default.Save();
            }

            return deviceId;
        }
    }
}
