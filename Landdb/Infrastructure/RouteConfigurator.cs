﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Landdb.Domain;
using Landdb.Domain.ReadModels.Tree;

namespace Landdb.Infrastructure {
    public class RouteConfigurator : IRouteConfigurator {
        public void ConfigurizeAllTheRoutes() {
            ConfigurizeEventHandlers();
            ConfigurizeCommandHandlers();
            ValidateRoutes();
        }

        void ConfigurizeEventHandlers() {
            //var eventPublisher = ServiceLocator.Get<IEventPublisher>();

            //// TODO: Configure based on reflection in the future?

            //var farmListRepository = ServiceLocator.Get<IFarmListRepository>();
            //eventPublisher.RegisterEventHandler<FarmCreatedEvent>(farmListRepository.Handle);
            //eventPublisher.RegisterEventHandler<FarmRenamedEvent>(farmListRepository.Handle);


            //var uiMessageObserver = ServiceLocator.Get<IMessageObserver>();
            //eventPublisher.RegisterEventHandler<Event>(uiMessageObserver.Handle);
        }

        void ConfigurizeCommandHandlers() {
            //var commandProcessor = ServiceLocator.Get<ICommandSender>();

            //var farmCommandHandler = new FarmCommandHandlers(ServiceLocator.Get<IRepository<Farm>>());
            //commandProcessor.RegisterCommandHandler<CreateFarmCommand>(farmCommandHandler.Handle);
        }

        void ValidateRoutes() { }
    }
}
