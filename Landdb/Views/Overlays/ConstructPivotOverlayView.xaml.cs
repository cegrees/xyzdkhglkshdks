﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Landdb.Views.Overlays {
    /// <summary>
    /// Interaction logic for ConstructPivotOverlayView.xaml
    /// </summary>
    public partial class ConstructPivotOverlayView : UserControl {
        public ConstructPivotOverlayView() {
            InitializeComponent();
            this.IsVisibleChanged += new DependencyPropertyChangedEventHandler(ConstructPivotOverlayView_IsVisibleChanged);
        }

        void ConstructPivotOverlayView_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e) {
            if ((bool)e.NewValue == false) {
                MapContainer.Content = null;
            }
        }
    }
}
