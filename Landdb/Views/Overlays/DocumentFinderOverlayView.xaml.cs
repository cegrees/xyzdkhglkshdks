﻿using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Landdb.Views.Overlays {
    /// <summary>
    /// Interaction logic for DocumentFinderOverlayView.xaml
    /// </summary>
    public partial class DocumentFinderOverlayView : UserControl {
        public DocumentFinderOverlayView() {
            InitializeComponent();
        }

        //private void ChooseButton_Click_1(object sender, RoutedEventArgs e) {
        //    GalaSoft.MvvmLight.Messaging.Messenger.Default.Send<Landdb.Infrastructure.Messages.HideOverlayMessage>(new Landdb.Infrastructure.Messages.HideOverlayMessage());
        //}

        //private void Button_Click(object sender, RoutedEventArgs e) {
        //    ScreenDescriptor descriptor = new ScreenDescriptor("Landdb.Views.Overlays.DocumentFinderOverlayView", "choose document");
        //    Messenger.Default.Send<ShowOverlayMessage>(new ShowOverlayMessage() { ScreenDescriptor = descriptor });

        //}
    }
}
