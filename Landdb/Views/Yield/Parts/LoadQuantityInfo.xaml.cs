﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Landdb.Views.Yield.Parts {
	/// <summary>
	/// Interaction logic for QuantityInfo.xaml
	/// </summary>
	public partial class LoadQuantityInfo : UserControl {
		public LoadQuantityInfo() {
			//http://stackoverflow.com/a/3534181
			if (System.ComponentModel.DesignerProperties.GetIsInDesignMode(this)) { return; }

			InitializeComponent();
		}
	}
}
