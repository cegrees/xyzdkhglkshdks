﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Landdb.Views.Yield.Popups {
	/// <summary>
	/// Interaction logic for ChangeLoadPricesView.xaml
	/// </summary>
	public partial class ChangeLoadPricesView : UserControl {
		public ChangeLoadPricesView() {
			InitializeComponent();
		}

		private void ComboBox_PreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e) {
			var cb = sender as ComboBox;
			cb.IsDropDownOpen = true;
		}
	}
}
