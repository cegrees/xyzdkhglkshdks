﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Landdb.Views.Yield.Popups {
	/// <summary>
	/// Interaction logic for ManageSubLocationsView.xaml
	/// </summary>
	public partial class ManageSubLocationsView : UserControl {
		public ManageSubLocationsView() {
			InitializeComponent();
		}

		private void SetButton_Click(object sender, RoutedEventArgs e) {
			if (sender is Button) {
				((Button)sender).Focus();
			}
		}
	}
}
