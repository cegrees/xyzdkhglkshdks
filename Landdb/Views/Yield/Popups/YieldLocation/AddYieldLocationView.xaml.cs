﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Landdb.Views.Yield.Popups
{
    /// <summary>
    /// Interaction logic for AddYieldLocationView.xaml
    /// </summary>
    public partial class AddYieldLocationView : UserControl
    {
        public AddYieldLocationView()
        {
            InitializeComponent();
        }

        private void SetButton_Click(object sender, RoutedEventArgs e)
        {
            if (sender is Button)
            {
                ((Button)sender).Focus();
            }
        }

        private void RoundGeometry_Checked(object sender, RoutedEventArgs e)
        {
            RoundView.Visibility = Visibility.Visible;
            FlatView.Visibility = Visibility.Collapsed;
        }

        private void FlatGeometry_Checked(object sender, RoutedEventArgs e)
        {
            FlatView.Visibility = Visibility.Visible;
            RoundView.Visibility = Visibility.Collapsed;
        }

        private void Owned_Checked(object sender, RoutedEventArgs e)
        {
            LeasedView.Visibility = Visibility.Collapsed;
            //RoundView.Visibility = Visibility.Collapsed;
        }

        private void Leased_Checked(object sender, RoutedEventArgs e)
        {
            LeasedView.Visibility = Visibility.Visible;
            //RoundView.Visibility = Visibility.Collapsed;
        }
    }
}
