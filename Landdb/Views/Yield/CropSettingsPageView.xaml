﻿<UserControl x:Class="Landdb.Views.Yield.CropSettingsPageView"
             xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
             xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
             xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" 
             xmlns:d="http://schemas.microsoft.com/expression/blend/2008" 
             xmlns:mtkc="clr-namespace:MetroToolkit.Controls;assembly=MetroToolkit.Controls"
             xmlns:cnv="clr-namespace:Landdb.Converters"
             xmlns:e="clr-namespace:System.Windows.Interactivity;assembly=System.Windows.Interactivity"
             xmlns:b="clr-namespace:Landdb.Behaviors"
             xmlns:lex="http://wpflocalizeextension.codeplex.com"
             lex:LocalizeDictionary.DesignCulture="qps-ploc"
             lex:ResxLocalizationProvider.DefaultAssembly="Landdb"
             lex:ResxLocalizationProvider.DefaultDictionary="Strings"
             mc:Ignorable="d" 
             DataContext="{Binding Source={StaticResource Locator}, Path=CropSettingsPage}"
             d:DesignHeight="600" 
             d:DesignWidth="800">

    <UserControl.Resources>
        <ResourceDictionary>
            <ResourceDictionary.MergedDictionaries>
                <ResourceDictionary Source="pack://application:,,,/MahApps.Metro;component/Styles/Controls.xaml" />
            </ResourceDictionary.MergedDictionaries>

            <cnv:CharmNameToBrushConverter x:Key="CharmNameToBrushConverter" />

            <DataTemplate x:Key="CropSettingsListItemTemplate">
                <Grid Margin="5">
                    <Grid.RowDefinitions>
                        <RowDefinition Height="Auto" />
                        <RowDefinition Height="Auto" />
                        <RowDefinition Height="Auto" />
                    </Grid.RowDefinitions>

                    <Grid.ColumnDefinitions>
                        <ColumnDefinition Width="*" />
                        <ColumnDefinition Width="Auto" />
                    </Grid.ColumnDefinitions>

                    <TextBlock Grid.ColumnSpan="2" Text="{Binding CropName}" FontWeight="Normal" Style="{StaticResource MetroSubHeader1TextBlock}" />
                </Grid>
            </DataTemplate>

            <DataTemplate x:Key="CropSettingsViewTemplate">
                <Grid Margin="5">
                    <Grid.RowDefinitions>
                        <RowDefinition Height="Auto" />
                        <RowDefinition Height="*" />
                    </Grid.RowDefinitions>

                    <Grid>
                        <Grid.ColumnDefinitions>
                            <ColumnDefinition Width="3*" />
                            <ColumnDefinition Width="*" />
                        </Grid.ColumnDefinitions>

                        <TextBlock Text="{Binding CropName}" Style="{StaticResource MetroPageHeaderTextBlock}" FontSize="30" />
                    </Grid>

                    <StackPanel Grid.Row="1" Margin="0,20,0,0" HorizontalAlignment="Left">
                        <StackPanel>
                            <TextBlock Text="{lex:LocTextUpper DefaultUnit_Text}" Style="{StaticResource MetroContentLabelTextBlock}" />
                            <ComboBox ItemsSource="{Binding AllowedUnits}" SelectedItem="{Binding SelectedUnit}" Width="200" Height="25" HorizontalAlignment="Left" />
                        </StackPanel>
                        
                        <StackPanel Margin="0,40,0,0" Width="350">
                            <TextBlock Text="{lex:LocTextUpper ImportantNotePleaseRead_Text}" Style="{StaticResource MetroContentLabelTextBlock}" />
                            <TextBlock Text="{lex:LocText CropSettingsPage_FollowingCropSettingsCanBe_Text}" TextWrapping="Wrap" Background="Gainsboro" Padding="5" />
                            <TextBlock Text="{lex:LocText CropSettingsPage_KeepFollowingValues_Text}" TextWrapping="Wrap" Background="Gainsboro" Padding="5" />

                            <TextBlock Text="{lex:LocText StandardTargetDryMoisturePercentage_Text}" Style="{StaticResource MetroContentLabelTextBlock}" Visibility="{Binding HasStandardDryMoisturePercentage, Converter={StaticResource BooleanToVisibilityConverter}}" Background="Gainsboro" Padding="5,10,0,0" />
                            <TextBlock TextWrapping="Wrap" Visibility="{Binding HasStandardDryMoisturePercentage, Converter={StaticResource BooleanToVisibilityConverter}}" Background="Gainsboro" Padding="5">
                                <TextBlock.Text>
                                    <MultiBinding StringFormat="{}{0}: {1:P2}">
                                        <Binding Path="CropName" />
                                        <Binding Path="StandardDryMoisturePercentage" />
                                    </MultiBinding>
                                </TextBlock.Text>
                            </TextBlock>
                        </StackPanel>

                        <TextBlock Text="{lex:LocTextLower HandlingShrink_Text}" Style="{StaticResource MetroSubHeader2TextBlock}" Margin="0,10,0,0" />

                        <Grid>
                            <Grid.ColumnDefinitions>
                                <ColumnDefinition Width="Auto" />
                                <ColumnDefinition Width="Auto" />
                            </Grid.ColumnDefinitions>
                            
                            <StackPanel Margin="0,10,0,0">
                                <TextBlock Text="{lex:LocTextUpper ShrinkPercentPerMoistureLost_Text}" Style="{StaticResource MetroContentLabelTextBlock}" />
                                <TextBox Text="{Binding ShrinkPercentage, StringFormat={}{0:P5}}" Width="200" Height="25">
                                    <e:Interaction.Behaviors>
                                        <b:HighlightTextOnFocusBehavior />
                                    </e:Interaction.Behaviors>
                                </TextBox>
                            </StackPanel>
                        </Grid>

                        <StackPanel Margin="0,10,0,0">
                            <TextBlock Text="{lex:LocTextUpper TargetDryMoisturePercentage_Text}" Style="{StaticResource MetroContentLabelTextBlock}" />
                            <TextBox Text="{Binding TargetMoisturePercentage, StringFormat={}{0:P5}}" Width="200" Height="25" HorizontalAlignment="Left">
                                <e:Interaction.Behaviors>
                                    <b:HighlightTextOnFocusBehavior />
                                </e:Interaction.Behaviors>
                            </TextBox>
                        </StackPanel>
                    </StackPanel>
                </Grid>
            </DataTemplate>
        </ResourceDictionary>
    </UserControl.Resources>

    <mtkc:MetroContentControl HorizontalAlignment="Stretch" VerticalAlignment="Stretch">
        <Grid>
            <Grid.ColumnDefinitions>
                <ColumnDefinition Width="*" MinWidth="230" />
                <ColumnDefinition Width="3*" />
            </Grid.ColumnDefinitions>

            <Grid Grid.Column="0" Margin="0,25,15,0" >
                <Grid.ColumnDefinitions>
                    <ColumnDefinition Width="*" />
                    <ColumnDefinition Width="*" />
                </Grid.ColumnDefinitions>
                <Grid.RowDefinitions>
                    <RowDefinition Height="*" />
                    <RowDefinition Height="Auto" />
                </Grid.RowDefinitions>

                <ListBox x:Name="CropSettingsListBox" Grid.Row="0" Grid.ColumnSpan="2" BorderThickness="0" Background="Transparent"
                         ItemsSource="{Binding ListItems}" SelectedItem="{Binding SelectedListItem, Mode=TwoWay}" 
                         ScrollViewer.HorizontalScrollBarVisibility="Disabled" ItemTemplate="{StaticResource CropSettingsListItemTemplate}">
                    <ListBox.ItemContainerStyle>
                        <Style TargetType="ListBoxItem">
                            <Setter Property="HorizontalContentAlignment" Value="Stretch" />
                        </Style>
                    </ListBox.ItemContainerStyle>
                </ListBox>

                <!--<Button Command="{Binding AddCommand}" x:Name="AddButton" Grid.Row="1" Grid.Column="0" Content="Add" Margin="0,5,5,5" Style="{DynamicResource MetroButtonStyle}" />
				<Button Command="{Binding RemoveCommand}" Grid.Row="1" Grid.Column="1" Content="Remove" Margin="5,5,0,5" Style="{DynamicResource MetroButtonStyle}" />-->
            </Grid>

            <GridSplitter Grid.Column="0" Width="10" Style="{DynamicResource MetroSplitterStyle}" Margin="0,25,0,5" />

            <Grid Grid.Column="1" Margin="0,25,0,0" Visibility="{Binding DetailsVisible}">
                <Grid.RowDefinitions>
                    <RowDefinition Height="*" />
                    <RowDefinition Height="Auto" />
                </Grid.RowDefinitions>

                <ContentControl Content="{Binding DetailsModel}" ContentTemplate="{StaticResource CropSettingsViewTemplate}"/>

                <Grid Grid.Row="1">
                    <Button Command="{Binding PrintCommand}" Content="{lex:LocText Print_Text}" Visibility="Hidden" MinWidth="100" Margin="5,5,0,5" Style="{DynamicResource MetroButtonStyle}" HorizontalAlignment="Right" />

                    <StackPanel Orientation="Horizontal">
                        <!--<Button x:Name="SetCharmButton" Click="SetCharmButton_Click" Content="Set Charm" MinWidth="100" Margin="5,5,0,5" Style="{DynamicResource MetroButtonStyle}" HorizontalAlignment="Left" />-->
                    </StackPanel>
                </Grid>
            </Grid>

        </Grid>
    </mtkc:MetroContentControl>
</UserControl>