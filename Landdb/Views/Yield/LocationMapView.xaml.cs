﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Landdb.ViewModel.Secondary.Application;
using Microsoft.Maps.MapControl.WPF;
using Landdb.ViewModel.Yield;
using Landdb.Client.Spatial;


namespace Landdb.Views.Yield {
    /// <summary>
    /// Interaction logic for LocationMapView.xaml
    /// </summary>
    public partial class LocationMapView : UserControl {

        public LocationMapView() {
            InitializeComponent();
            this.DataContextChanged += EnvironmentalView_DataContextChanged;
            BingMap.IsVisibleChanged += BingMap_IsVisibleChanged;
        }

        void BingMap_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e) {
            if (BingMap.IsVisible) {
                UpdateMap();
            }
        }

        void EnvironmentalView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            UpdateMap();
        }

        void UpdateMap() {
            if(this.DataContext is LocationMapViewModel) {
                var vm = this.DataContext as LocationMapViewModel;

                if (vm == null || BingMap == null || !BingMap.IsLoaded) { return; }

                try {
                    if (vm.ShouldShowBingMaps && vm.BingMapsLayer != null) {
                        BingMap.Children.Clear();

                        BingMap.Children.Add(vm.BingMapsLayer);
                        var box = vm.BingMapsLayer.GetBoundingBox().Inflate(4);
                        BingMap.SetView(box);
                    }
                }
                catch { }  // TODO: Eat for now; probably need to bubble something to the user
            }
            else if(this.DataContext is YieldLocationDetailsViewModel) {
                //var vm = this.DataContext as YieldLocationDetailsViewModel;

                //if (vm == null || BingMap == null || !BingMap.IsLoaded) { return; }

                //try {
                //    if (vm.LocationMapVM.ShouldShowBingMaps && vm.LocationMapVM.BingMapsLayer != null) {
                //        BingMap.Children.Clear();

                //        BingMap.Children.Add(vm.LocationMapVM.BingMapsLayer);
                //        var box = vm.LocationMapVM.BingMapsLayer.GetBoundingBox().Inflate(0.5);
                //        BingMap.SetView(box);
                //    }
                //}
                //catch { }  // TODO: Eat for now; probably need to bubble something to the user
            }
        }

        private void BingMap_MouseDown(object sender, MouseButtonEventArgs e) {
            ((MainWindow)System.Windows.Application.Current.MainWindow).Draggable = false;
        }

        private void BingMap_MouseUp(object sender, MouseButtonEventArgs e) {
            ((MainWindow)System.Windows.Application.Current.MainWindow).Draggable = true;
        }

        private void BingMap_Loaded(object sender, RoutedEventArgs e) {
            UpdateMap();
        }
    }
}
