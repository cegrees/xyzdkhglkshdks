﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Landdb.ViewModel;
using Landdb.Client.Account;
using Landdb.Infrastructure;
using Landdb.Web.ServiceContracts;
using Landdb.Client.Services;

namespace Landdb.Views {
    public partial class MainPage : UserControl {
        public MainPage() {
            InitializeComponent();
        }

        // TODO: Make this more MVVM brah! :)
        private void CropYearBorder_MouseLeftButtonUp(object sender, MouseButtonEventArgs e) {
            var vm = this.DataContext as MainPageViewModel;
            if (vm != null) {
                vm.DataSourceSelectionViewModel.IsDataSourceListVisible = !vm.DataSourceSelectionViewModel.IsDataSourceListVisible;
            }
        }

        //private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e) {
        //    var vm = this.DataContext as MainPageViewModel;
        //    if (vm != null) {
        //        if (tabs.SelectedContent != null && tabs.SelectedContent is FrameworkElement) {
        //            vm.SelectedViewModel = ((FrameworkElement)tabs.SelectedContent).DataContext;
        //        } else {
        //            vm.SelectedViewModel = null;
        //        }
        //    }
        //}

    }
}
