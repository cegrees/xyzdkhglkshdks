﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Landdb.Views.Secondary.Application {
    /// <summary>
    /// Interaction logic for UpdateSelectedCropZoneAreaView.xaml
    /// </summary>
    public partial class UpdateSelectedCropZoneAreaView : UserControl {
        public UpdateSelectedCropZoneAreaView() {
            InitializeComponent();
        }

        private void SetButton_Click(object sender, RoutedEventArgs e) {
            if (sender is Button) {
                ((Button)sender).Focus();
            }
        }
    }
}
