﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Landdb.Views.Secondary.Application {
    /// <summary>
    /// Interaction logic for ApplicatorView.xaml
    /// </summary>
    public partial class ApplicatorView : UserControl {
        public ApplicatorView() {
            InitializeComponent();

            this.PreviewKeyDown += ApplicatorView_PreviewKeyDown;
            ApplicatorTabs.SelectedIndex = 0;
        }

        void ApplicatorView_PreviewKeyDown(object sender, KeyEventArgs e) {
            if (e.Key == Key.D1 && ((Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control)) {
                if (ApplicatorTabs.Items.Count >= 1) {
                    ApplicatorTabs.SelectedIndex = 0;
                    e.Handled = true;
                }
            }
            if (e.Key == Key.D2 && ((Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control)) {
                if (ApplicatorTabs.Items.Count >= 2) {
                    ApplicatorTabs.SelectedIndex = 1;
                    e.Handled = true;
                }
            }

            if (e.Key == Key.D3 && ((Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control)) {
                if (ApplicatorTabs.Items.Count >= 3) {
                    ApplicatorTabs.SelectedIndex = 2;
                    e.Handled = true;
                }
            }

            if (e.Key == Key.D4 && ((Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control)) {
                if (ApplicatorTabs.Items.Count >= 4) {
                    ApplicatorTabs.SelectedIndex = 3;
                    e.Handled = true;
                }
            }

            if (e.Key == Key.D5 && ((Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control)) {
                if (ApplicatorTabs.Items.Count >= 5) {
                    ApplicatorTabs.SelectedIndex = 4;
                    e.Handled = true;
                }
            }

            if (e.Key == Key.Return)
            {
                //do nothing
                e.Handled = true;
            }
        }

    }
}
