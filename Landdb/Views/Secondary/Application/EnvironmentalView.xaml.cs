﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Landdb.ViewModel.Secondary.Application;
using Microsoft.Maps.MapControl.WPF;

namespace Landdb.Views.Secondary.Application {
    /// <summary>
    /// Interaction logic for EnvironmentalView.xaml
    /// </summary>
    public partial class EnvironmentalView : UserControl {
        LocationRect pinLocationRectangle;

        public EnvironmentalView() {
            InitializeComponent();

            this.DataContextChanged += EnvironmentalView_DataContextChanged;
            BingMap.SizeChanged += BingMap_SizeChanged;
        }

        private void BingMap_SizeChanged(object sender, SizeChangedEventArgs e) {
            if (BingMap.ActualHeight > 0 && BingMap.ActualWidth > 0 && pinLocationRectangle != null) {
                BingMap.SetView(pinLocationRectangle);
                BingMap.SizeChanged -= BingMap_SizeChanged;
            }
        }

        void EnvironmentalView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            if (e.OldValue != null && e.OldValue is ApplicationConditionsViewModel) {
                ((ApplicationConditionsViewModel)e.OldValue).PropertyChanged -= EnvironmentalView_PropertyChanged;
            }

            if (e.NewValue != null && e.NewValue is ApplicationConditionsViewModel) {
                ((ApplicationConditionsViewModel)e.NewValue).PropertyChanged += EnvironmentalView_PropertyChanged;
            }
        }

        void EnvironmentalView_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e) {
            if (e.PropertyName == "WeatherStations") {
                WeatherStationCombo_SourceUpdated();
            }
        }

        ApplicationConditionsViewModel ViewModel {
            get { return DataContext as ApplicationConditionsViewModel; }
        }

        void WeatherStationCombo_SourceUpdated() {
            if (BingMap == null || ViewModel?.EnvironmentConditions.WeatherStations == null) { return; }
            BingMap.Children.Clear();

            List<Location> pinLocations = new List<Location>();

            foreach (var ws in ViewModel.EnvironmentConditions.WeatherStations) {
                if (ws.IsSummary) { continue; }

                Pushpin p = new Pushpin();
                p.Location = new Location(ws.Latitude, ws.Longitude);
                pinLocations.Add(p.Location);
                p.Content = ws.Number;
                p.Name = ws.StationId;
                BingMap.Children.Add(p);
            }
            pinLocationRectangle = new LocationRect(pinLocations);

            if (BingMap.IsLoaded && BingMap.IsVisible) {
                BingMap.SetView(pinLocationRectangle);
            }
        }

        private void BingMap_MouseDown(object sender, MouseButtonEventArgs e) {
            ((MainWindow)System.Windows.Application.Current.MainWindow).Draggable = false;
        }

        private void BingMap_MouseUp(object sender, MouseButtonEventArgs e) {
            ((MainWindow)System.Windows.Application.Current.MainWindow).Draggable = true;
        }

    }
}
