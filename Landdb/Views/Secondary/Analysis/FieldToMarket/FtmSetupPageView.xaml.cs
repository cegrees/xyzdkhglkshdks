﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Landdb.Views.Secondary.Analysis.FieldToMarket {
    /// <summary>
    /// Interaction logic for FtmSetupPageView.xaml
    /// </summary>
    public partial class FtmSetupPageView : UserControl {
        public FtmSetupPageView() {
            InitializeComponent();
        }

        private void Rusle2AbbreviationLink_Click(object sender, RoutedEventArgs e) {
            System.Diagnostics.Process.Start("http://www.nrcs.usda.gov/Internet/FSE_DOCUMENTS/nrcs142p2_022753.pdf");
        }
    }
}
