﻿using System.Windows.Controls;

namespace Landdb.Views.Secondary.YieldOperation {
	/// <summary>
	/// Interaction logic for PostYieldOperationOverlay.xaml
	/// </summary>
	public partial class PostYieldOperationOverlay : UserControl {
		public PostYieldOperationOverlay() {
			InitializeComponent();
		}
	}
}
