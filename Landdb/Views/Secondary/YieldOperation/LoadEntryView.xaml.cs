﻿using NLog;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Landdb.Views.Secondary.YieldOperation {
	/// <summary>
	/// Interaction logic for LoadEntryView.xaml
	/// </summary>
	public partial class LoadEntryView : UserControl {

		private readonly Logger log = LogManager.GetCurrentClassLogger();

		public LoadEntryView() {
			InitializeComponent();
		}

		private void ComboBox_PreviewKeyDown(object sender, KeyEventArgs e) {
			try {
				var cb = sender as ComboBox;

				if (cb != null) {
					cb.IsDropDownOpen = true;
				} else {
					log.Warn("Warning code 28895. Value 'cb' is null.");
				}
			} catch (Exception ex) {
				// trying to catch details on fb 28895 without crashing the app.
				// ref: https://fogbugz.agconnections.com/fogbugz/default.asp?28895

				// possibly a bug in .net
				// ref: http://stackoverflow.com/questions/29306589/crash-in-combobox-coerce-not-my-code
				// ref: https://connect.microsoft.com/VisualStudio/feedback/details/1660886/system-windows-controls-combobox-coerceisselectionboxhighlighted-bug
				log.ErrorException("Error code 28895 detected.", ex);
			}
		}

		private void StackPanel_GotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e) {
			// i don't event want to disclose the time i spent using trial and error to figure this out
			var sp = sender as StackPanel;
			var size = sp.RenderSize;
			size.Height += 300;
			var origin = new Point(0, -100);
			sp.BringIntoView(new Rect(origin, size));
		}
	}
}