﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Landdb.Views.Secondary.YieldOperation {
	/// <summary>
	/// Interaction logic for SourceLocationView.xaml
	/// </summary>
	public partial class SourceLocationView : UserControl {
		public SourceLocationView() {
			InitializeComponent();
		}

		private void ComboBox_PreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e) {
			var cb = sender as ComboBox;
			cb.IsDropDownOpen = true;
		}
	}
}
