﻿using Landdb.ViewModel.Secondary.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Landdb.Views.Secondary.PopUp
{
    /// <summary>
    /// Interaction logic for AppliedProductSpecialityCalculator.xaml
    /// </summary>
    public partial class AppliedProductSpecialityCalculator : UserControl
    {
        public AppliedProductSpecialityCalculator()
        {
            InitializeComponent();
            
        }

        private void UserControl_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            
        }

        private void UserControl_Initialized(object sender, EventArgs e)
        {
            //this.CalculatorTabs.TabIndex = 0;
        }

        private void CalculatorTabs_Loaded(object sender, RoutedEventArgs e)
        {
            this.CalculatorTabs.SelectedIndex = 0;
        }
    }
}
