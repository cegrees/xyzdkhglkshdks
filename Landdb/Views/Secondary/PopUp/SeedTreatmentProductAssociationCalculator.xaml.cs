﻿using Landdb.ViewModel.Secondary.PopUp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Landdb.Views.Secondary.PopUp
{
    /// <summary>
    /// Interaction logic for SeedTreatmentProductAssociationCalculator.xaml
    /// </summary>
    public partial class SeedTreatmentProductAssociationCalculator : UserControl
    {
        public SeedTreatmentProductAssociationCalculator()
        {
            InitializeComponent();

            var vm = this.DataContext != null ? this.DataContext as AssociatedSeedTreatmentViewModel : null;
            this.CalculatorTabs.TabIndex = vm != null ? vm.SelectedTabIndex : 0;
            //this.CalculatorTabs.SelectedIndex = 0;
        }

        private void CalculatorTabs_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //if(this.DataContext == null) { return; }
            //var vm = this.DataContext as AssociatedSeedTreatmentViewModel;
            //vm.SelectedTabIndex = CalculatorTabs.SelectedIndex;
        }

        private void CalculatorTabs_Initialized(object sender, EventArgs e)
        {
            var vm = this.DataContext != null ? this.DataContext as AssociatedSeedTreatmentViewModel : null;
            CalculatorTabs.SelectedIndex = vm != null ? vm.SelectedTabIndex : 0;
        }

        private void ProductListBox_Initialized(object sender, EventArgs e)
        {

        }

        private void ProductListBox_Loaded(object sender, RoutedEventArgs e)
        {
            var autoCompleteBox = sender as AutoCompleteBox;
            var vm = DataContext as AssociatedSeedTreatmentViewModel;
        }

        private void CalculatorTabs_Loaded(object sender, RoutedEventArgs e)
        {
            var vm = this.DataContext != null ? this.DataContext as AssociatedSeedTreatmentViewModel : null;
            CalculatorTabs.SelectedIndex = vm != null ? vm.SelectedTabIndex : 0;
        }
    }
}
