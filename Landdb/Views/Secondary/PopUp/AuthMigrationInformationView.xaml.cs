﻿using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure.Messages;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Landdb.Views.Secondary.PopUp {
    /// <summary>
    /// Interaction logic for AuthMigrationInformationView.xaml
    /// </summary>
    public partial class AuthMigrationInformationView : Window {
        public AuthMigrationInformationView() {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e) {
            this.Close();
        }

        private void OpenMigrationPageLink_Click(object sender, RoutedEventArgs e) {
            Process.Start(@"https://landdb-authmigration.landdb.com/account/beginmigration");
        }
    }
}
