namespace Landdb.Views.Secondary.Reports.Invoice
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;

    /// <summary>
    /// Summary description for StandardInvoice.
    /// </summary>
    public partial class StandardInvoice : Telerik.Reporting.Report
    {
        public StandardInvoice()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }
    }
}