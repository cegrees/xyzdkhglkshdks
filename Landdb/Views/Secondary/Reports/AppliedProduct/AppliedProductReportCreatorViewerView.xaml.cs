﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Landdb.ViewModel;

namespace Landdb.Views.Secondary.Reports.AppliedProduct {
    /// <summary>
    /// Interaction logic for AppliedProductReportCreatorViewerView.xaml
    /// </summary>
    public partial class AppliedProductReportCreatorViewerView : UserControl {
        public AppliedProductReportCreatorViewerView() {
            InitializeComponent();

            this.IsVisibleChanged += AppliedProductReportCreatorViewerView_IsVisibleChanged;
        }

        void AppliedProductReportCreatorViewerView_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e) {
            if (this.IsVisible && ViewModel != null) {
                ViewModel.Update();
            }
        }

        Updatable ViewModel => this.DataContext as Updatable;

        private void Button_MouseEnter(object sender, MouseEventArgs e)
        {
            
        }
    }
}
