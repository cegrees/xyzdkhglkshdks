namespace Landdb.Views.Secondary.Reports.AppliedProduct
{
    partial class AppliedProductDetailsReport
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			Telerik.Reporting.Group group1 = new Telerik.Reporting.Group();
			Telerik.Reporting.Group group2 = new Telerik.Reporting.Group();
			Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
			this.groupFooterSection = new Telerik.Reporting.GroupFooterSection();
			this.textBox18 = new Telerik.Reporting.TextBox();
			this.textBox20 = new Telerik.Reporting.TextBox();
			this.textBox22 = new Telerik.Reporting.TextBox();
			this.textBox24 = new Telerik.Reporting.TextBox();
			this.groupHeaderSection = new Telerik.Reporting.GroupHeaderSection();
			this.textBox19 = new Telerik.Reporting.TextBox();
			this.textBox23 = new Telerik.Reporting.TextBox();
			this.groupFooterSection1 = new Telerik.Reporting.GroupFooterSection();
			this.textBox16 = new Telerik.Reporting.TextBox();
			this.textBox21 = new Telerik.Reporting.TextBox();
			this.textBox17 = new Telerik.Reporting.TextBox();
			this.textBox3 = new Telerik.Reporting.TextBox();
			this.groupHeaderSection1 = new Telerik.Reporting.GroupHeaderSection();
			this.textBox5 = new Telerik.Reporting.TextBox();
			this.textBox6 = new Telerik.Reporting.TextBox();
			this.textBox7 = new Telerik.Reporting.TextBox();
			this.textBox8 = new Telerik.Reporting.TextBox();
			this.textBox9 = new Telerik.Reporting.TextBox();
			this.textBox10 = new Telerik.Reporting.TextBox();
			this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
			this.textBox93 = new Telerik.Reporting.TextBox();
			this.textBox4 = new Telerik.Reporting.TextBox();
			this.txtDataSourceLabel = new Telerik.Reporting.TextBox();
			this.textBox100 = new Telerik.Reporting.TextBox();
			this.detail = new Telerik.Reporting.DetailSection();
			this.textBox11 = new Telerik.Reporting.TextBox();
			this.textBox12 = new Telerik.Reporting.TextBox();
			this.textBox13 = new Telerik.Reporting.TextBox();
			this.textBox14 = new Telerik.Reporting.TextBox();
			this.textBox15 = new Telerik.Reporting.TextBox();
			this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
			this.textBox61 = new Telerik.Reporting.TextBox();
			this.pageInfoTextBox = new Telerik.Reporting.TextBox();
			this.textBox65 = new Telerik.Reporting.TextBox();
			this.AppliedProductDetailData = new Telerik.Reporting.ObjectDataSource();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// groupFooterSection
			// 
			this.groupFooterSection.Height = Telerik.Reporting.Drawing.Unit.Inch(0.40035352110862732D);
			this.groupFooterSection.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox18,
            this.textBox20,
            this.textBox22,
            this.textBox24});
			this.groupFooterSection.Name = "groupFooterSection";
			this.groupFooterSection.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
			// 
			// textBox18
			// 
			this.textBox18.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9354959881165996E-05D), Telerik.Reporting.Drawing.Unit.Inch(0.09988199919462204D));
			this.textBox18.Name = "textBox18";
			this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.5999214649200439D), Telerik.Reporting.Drawing.Unit.Inch(0.20000003278255463D));
			this.textBox18.Style.Color = System.Drawing.Color.Black;
			this.textBox18.Style.Font.Bold = true;
			this.textBox18.Style.Font.Name = "Segoe UI";
			this.textBox18.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
			this.textBox18.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox18.Value = "Total Product:";
			// 
			// textBox20
			// 
			this.textBox20.Format = "{0:N2}";
			this.textBox20.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.4007868766784668D), Telerik.Reporting.Drawing.Unit.Inch(0.09988199919462204D));
			this.textBox20.Name = "textBox20";
			this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.89956694841384888D), Telerik.Reporting.Drawing.Unit.Inch(0.20000003278255463D));
			this.textBox20.Style.Color = System.Drawing.Color.Black;
			this.textBox20.Style.Font.Bold = true;
			this.textBox20.Style.Font.Name = "Segoe UI";
			this.textBox20.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
			this.textBox20.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox20.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox20.Value = "=Sum(Fields.TotalProductValue)";
			// 
			// textBox22
			// 
			this.textBox22.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(7.3000783920288086D), Telerik.Reporting.Drawing.Unit.Inch(0.09988199919462204D));
			this.textBox22.Name = "textBox22";
			this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.69988232851028442D), Telerik.Reporting.Drawing.Unit.Inch(0.20000003278255463D));
			this.textBox22.Style.Color = System.Drawing.Color.Black;
			this.textBox22.Style.Font.Bold = true;
			this.textBox22.Style.Font.Name = "Segoe UI";
			this.textBox22.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
			this.textBox22.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox22.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox22.Value = "=Fields.TotalUnit.AbbreviatedDisplay";
			// 
			// textBox24
			// 
			this.textBox24.Format = "{0:N2}";
			this.textBox24.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.2003941535949707D), Telerik.Reporting.Drawing.Unit.Inch(0.09988199919462204D));
			this.textBox24.Name = "textBox24";
			this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2003146409988403D), Telerik.Reporting.Drawing.Unit.Inch(0.20000003278255463D));
			this.textBox24.Style.Color = System.Drawing.Color.Black;
			this.textBox24.Style.Font.Bold = true;
			this.textBox24.Style.Font.Name = "Segoe UI";
			this.textBox24.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
			this.textBox24.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox24.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox24.Value = "=Sum(Fields.AreaValue)";
			// 
			// groupHeaderSection
			// 
			this.groupHeaderSection.Height = Telerik.Reporting.Drawing.Unit.Inch(0.40000003576278687D);
			this.groupHeaderSection.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox19,
            this.textBox23});
			this.groupHeaderSection.Name = "groupHeaderSection";
			this.groupHeaderSection.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			// 
			// textBox19
			// 
			this.textBox19.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D));
			this.textBox19.Name = "textBox19";
			this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.9999210834503174D), Telerik.Reporting.Drawing.Unit.Inch(0.29996061325073242D));
			this.textBox19.Style.Color = System.Drawing.Color.Black;
			this.textBox19.Style.Font.Name = "Segoe UI Light";
			this.textBox19.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(18D);
			this.textBox19.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox19.Value = "=Fields.ProductName";
			// 
			// textBox23
			// 
			this.textBox23.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.0000400543212891D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D));
			this.textBox23.Name = "textBox23";
			this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.9999210834503174D), Telerik.Reporting.Drawing.Unit.Inch(0.29996061325073242D));
			this.textBox23.Style.Color = System.Drawing.Color.Black;
			this.textBox23.Style.Font.Name = "Segoe UI Light";
			this.textBox23.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(18D);
			this.textBox23.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox23.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox23.Value = "=Fields.Manufacturer";
			// 
			// groupFooterSection1
			// 
			this.groupFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.39964628219604492D);
			this.groupFooterSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox16,
            this.textBox21,
            this.textBox17,
            this.textBox3});
			this.groupFooterSection1.Name = "groupFooterSection1";
			// 
			// textBox16
			// 
			this.textBox16.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.10004018247127533D), Telerik.Reporting.Drawing.Unit.Inch(0D));
			this.textBox16.Name = "textBox16";
			this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.5999214649200439D), Telerik.Reporting.Drawing.Unit.Inch(0.20000003278255463D));
			this.textBox16.Style.Color = System.Drawing.Color.Black;
			this.textBox16.Style.Font.Name = "Segoe UI Light";
			this.textBox16.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
			this.textBox16.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox16.Value = "Total Farm:";
			// 
			// textBox21
			// 
			this.textBox21.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(7.3000783920288086D), Telerik.Reporting.Drawing.Unit.Inch(3.9672850107308477E-05D));
			this.textBox21.Name = "textBox21";
			this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.69988232851028442D), Telerik.Reporting.Drawing.Unit.Inch(0.20000003278255463D));
			this.textBox21.Style.Color = System.Drawing.Color.Black;
			this.textBox21.Style.Font.Name = "Segoe UI Light";
			this.textBox21.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
			this.textBox21.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox21.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox21.Value = "=Fields.TotalUnit.AbbreviatedDisplay";
			// 
			// textBox17
			// 
			this.textBox17.Format = "{0:N2}";
			this.textBox17.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.4007868766784668D), Telerik.Reporting.Drawing.Unit.Inch(0D));
			this.textBox17.Name = "textBox17";
			this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.89921301603317261D), Telerik.Reporting.Drawing.Unit.Inch(0.20000003278255463D));
			this.textBox17.Style.Color = System.Drawing.Color.Black;
			this.textBox17.Style.Font.Name = "Segoe UI Light";
			this.textBox17.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
			this.textBox17.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox17.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox17.Value = "=Sum(Fields.TotalProductValue)";
			// 
			// textBox3
			// 
			this.textBox3.Format = "{0:N2}";
			this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.2003936767578125D), Telerik.Reporting.Drawing.Unit.Inch(3.9672850107308477E-05D));
			this.textBox3.Name = "textBox3";
			this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2003146409988403D), Telerik.Reporting.Drawing.Unit.Inch(0.20000003278255463D));
			this.textBox3.Style.Color = System.Drawing.Color.Black;
			this.textBox3.Style.Font.Name = "Segoe UI Light";
			this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
			this.textBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox3.Value = "=Sum(Fields.AreaValue)";
			// 
			// groupHeaderSection1
			// 
			this.groupHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.50000005960464478D);
			this.groupHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox5,
            this.textBox6,
            this.textBox7,
            this.textBox8,
            this.textBox9,
            this.textBox10});
			this.groupHeaderSection1.Name = "groupHeaderSection1";
			// 
			// textBox5
			// 
			this.textBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.10004018247127533D), Telerik.Reporting.Drawing.Unit.Inch(0D));
			this.textBox5.Name = "textBox5";
			this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.8999214172363281D), Telerik.Reporting.Drawing.Unit.Inch(0.19992133975028992D));
			this.textBox5.Style.Color = System.Drawing.Color.Gray;
			this.textBox5.Style.Font.Bold = false;
			this.textBox5.Style.Font.Name = "Segoe UI";
			this.textBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
			this.textBox5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox5.Value = "=Fields.FarmName";
			// 
			// textBox6
			// 
			this.textBox6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D), Telerik.Reporting.Drawing.Unit.Inch(0.30000001192092896D));
			this.textBox6.Name = "textBox6";
			this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.1999607086181641D), Telerik.Reporting.Drawing.Unit.Inch(0.19992133975028992D));
			this.textBox6.Style.Color = System.Drawing.Color.Gray;
			this.textBox6.Style.Font.Bold = true;
			this.textBox6.Style.Font.Name = "Segoe UI";
			this.textBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox6.Value = "Field: Crop Zone";
			// 
			// textBox7
			// 
			this.textBox7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.6000397205352783D), Telerik.Reporting.Drawing.Unit.Inch(0.30000001192092896D));
			this.textBox7.Name = "textBox7";
			this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3998813629150391D), Telerik.Reporting.Drawing.Unit.Inch(0.19992133975028992D));
			this.textBox7.Style.Color = System.Drawing.Color.Gray;
			this.textBox7.Style.Font.Bold = true;
			this.textBox7.Style.Font.Name = "Segoe UI";
			this.textBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox7.Value = "Application Date";
			// 
			// textBox8
			// 
			this.textBox8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4D), Telerik.Reporting.Drawing.Unit.Inch(0.30000001192092896D));
			this.textBox8.Name = "textBox8";
			this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2003147602081299D), Telerik.Reporting.Drawing.Unit.Inch(0.19992133975028992D));
			this.textBox8.Style.Color = System.Drawing.Color.Gray;
			this.textBox8.Style.Font.Bold = true;
			this.textBox8.Style.Font.Name = "Segoe UI";
			this.textBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox8.Value = "Rate";
			// 
			// textBox9
			// 
			this.textBox9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.2003936767578125D), Telerik.Reporting.Drawing.Unit.Inch(0.30000001192092896D));
			this.textBox9.Name = "textBox9";
			this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2003147602081299D), Telerik.Reporting.Drawing.Unit.Inch(0.19992133975028992D));
			this.textBox9.Style.Color = System.Drawing.Color.Gray;
			this.textBox9.Style.Font.Bold = true;
			this.textBox9.Style.Font.Name = "Segoe UI";
			this.textBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox9.Value = "Area Applied";
			// 
			// textBox10
			// 
			this.textBox10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.4000000953674316D), Telerik.Reporting.Drawing.Unit.Inch(0.30000001192092896D));
			this.textBox10.Name = "textBox10";
			this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5999608039855957D), Telerik.Reporting.Drawing.Unit.Inch(0.19992133975028992D));
			this.textBox10.Style.Color = System.Drawing.Color.Gray;
			this.textBox10.Style.Font.Bold = true;
			this.textBox10.Style.Font.Name = "Segoe UI";
			this.textBox10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox10.Value = "Total Product";
			// 
			// pageHeaderSection1
			// 
			this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.800000011920929D);
			this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox93,
            this.textBox4,
            this.txtDataSourceLabel,
            this.textBox100});
			this.pageHeaderSection1.Name = "pageHeaderSection1";
			// 
			// textBox93
			// 
			this.textBox93.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.5000004768371582D), Telerik.Reporting.Drawing.Unit.Inch(0.20007884502410889D));
			this.textBox93.Name = "textBox93";
			this.textBox93.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.49996018409729D), Telerik.Reporting.Drawing.Unit.Inch(0.29996061325073242D));
			this.textBox93.Style.Color = System.Drawing.Color.DimGray;
			this.textBox93.Style.Font.Name = "Segoe UI Light";
			this.textBox93.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(18D);
			this.textBox93.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox93.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox93.TextWrap = false;
			this.textBox93.Value = "Applied Product Details";
			// 
			// textBox4
			// 
			this.textBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D), Telerik.Reporting.Drawing.Unit.Inch(0.5001180768013D));
			this.textBox4.Name = "textBox4";
			this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.3999605178833008D), Telerik.Reporting.Drawing.Unit.Inch(0.19996063411235809D));
			this.textBox4.Style.Color = System.Drawing.Color.DimGray;
			this.textBox4.Style.Font.Name = "Segoe UI";
			this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox4.TextWrap = false;
			this.textBox4.Value = "=Fields.DateRangeDisplay";
			// 
			// txtDataSourceLabel
			// 
			this.txtDataSourceLabel.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(3.9378803194267675E-05D));
			this.txtDataSourceLabel.Name = "txtDataSourceLabel";
			this.txtDataSourceLabel.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.0312089920043945D), Telerik.Reporting.Drawing.Unit.Inch(0.50000005960464478D));
			this.txtDataSourceLabel.Style.Font.Name = "Segoe UI Light";
			this.txtDataSourceLabel.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(24D);
			this.txtDataSourceLabel.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.txtDataSourceLabel.Value = "=Fields.DataSourceName";
			// 
			// textBox100
			// 
			this.textBox100.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.0000777244567871D), Telerik.Reporting.Drawing.Unit.Inch(0.50011825561523438D));
			this.textBox100.Name = "textBox100";
			this.textBox100.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9998817443847656D), Telerik.Reporting.Drawing.Unit.Inch(0.19996063411235809D));
			this.textBox100.Style.Color = System.Drawing.Color.DimGray;
			this.textBox100.Style.Font.Name = "Segoe UI";
			this.textBox100.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox100.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox100.TextWrap = false;
			this.textBox100.Value = "=Fields.CropYear";
			// 
			// detail
			// 
			this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D);
			this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox11,
            this.textBox12,
            this.textBox13,
            this.textBox14,
            this.textBox15});
			this.detail.Name = "detail";
			// 
			// textBox11
			// 
			this.textBox11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.4000000953674316D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
			this.textBox11.Name = "textBox11";
			this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5999608039855957D), Telerik.Reporting.Drawing.Unit.Inch(0.19992133975028992D));
			this.textBox11.Style.Color = System.Drawing.Color.Black;
			this.textBox11.Style.Font.Bold = false;
			this.textBox11.Style.Font.Name = "Segoe UI";
			this.textBox11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox11.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox11.Value = "=Fields.TotalProductDisplay";
			// 
			// textBox12
			// 
			this.textBox12.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.1999998092651367D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
			this.textBox12.Name = "textBox12";
			this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2003147602081299D), Telerik.Reporting.Drawing.Unit.Inch(0.19992133975028992D));
			this.textBox12.Style.Color = System.Drawing.Color.Black;
			this.textBox12.Style.Font.Bold = false;
			this.textBox12.Style.Font.Name = "Segoe UI";
			this.textBox12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox12.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox12.Value = "=Fields.AreaDisplay";
			// 
			// textBox13
			// 
			this.textBox13.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
			this.textBox13.Name = "textBox13";
			this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2003147602081299D), Telerik.Reporting.Drawing.Unit.Inch(0.19992133975028992D));
			this.textBox13.Style.Color = System.Drawing.Color.Black;
			this.textBox13.Style.Font.Bold = false;
			this.textBox13.Style.Font.Name = "Segoe UI";
			this.textBox13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox13.Value = "=Fields.RateDisplay";
			// 
			// textBox14
			// 
			this.textBox14.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.5917065143585205D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
			this.textBox14.Name = "textBox14";
			this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3998813629150391D), Telerik.Reporting.Drawing.Unit.Inch(0.19992133975028992D));
			this.textBox14.Style.Color = System.Drawing.Color.Black;
			this.textBox14.Style.Font.Bold = false;
			this.textBox14.Style.Font.Name = "Segoe UI";
			this.textBox14.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox14.Value = "=Fields.ApplicationDate";
			// 
			// textBox15
			// 
			this.textBox15.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
			this.textBox15.Name = "textBox15";
			this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.1999607086181641D), Telerik.Reporting.Drawing.Unit.Inch(0.19992133975028992D));
			this.textBox15.Style.Color = System.Drawing.Color.Black;
			this.textBox15.Style.Font.Bold = false;
			this.textBox15.Style.Font.Name = "Segoe UI";
			this.textBox15.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox15.Value = "=Fields.FieldCropZone";
			// 
			// pageFooterSection1
			// 
			this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.30003932118415833D);
			this.pageFooterSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox61,
            this.pageInfoTextBox,
            this.textBox65});
			this.pageFooterSection1.Name = "pageFooterSection1";
			// 
			// textBox61
			// 
			this.textBox61.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
			this.textBox61.Name = "textBox61";
			this.textBox61.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8790887594223023D), Telerik.Reporting.Drawing.Unit.Inch(0.29996046423912048D));
			this.textBox61.Style.Color = System.Drawing.Color.DimGray;
			this.textBox61.Style.Font.Bold = true;
			this.textBox61.Style.Font.Italic = false;
			this.textBox61.Style.Font.Name = "Segoe UI Light";
			this.textBox61.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(16D);
			this.textBox61.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox61.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox61.Value = "AG CONNECTIONS";
			// 
			// pageInfoTextBox
			// 
			this.pageInfoTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.3000006675720215D), Telerik.Reporting.Drawing.Unit.Inch(0D));
			this.pageInfoTextBox.Name = "pageInfoTextBox";
			this.pageInfoTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.6999607086181641D), Telerik.Reporting.Drawing.Unit.Inch(0.29999962449073792D));
			this.pageInfoTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.pageInfoTextBox.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.pageInfoTextBox.StyleName = "PageInfo";
			this.pageInfoTextBox.Value = "=PageNumber";
			// 
			// textBox65
			// 
			this.textBox65.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.1145446300506592D), Telerik.Reporting.Drawing.Unit.Inch(0D));
			this.textBox65.Name = "textBox65";
			this.textBox65.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0937893390655518D), Telerik.Reporting.Drawing.Unit.Inch(0.29996046423912048D));
			this.textBox65.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox65.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox65.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox65.Value = "generated on {Now()}";
			// 
			// AppliedProductDetailData
			// 
			this.AppliedProductDetailData.DataSource = typeof(Landdb.ReportModels.AppliedProduct.AppliedProductDetail);
			this.AppliedProductDetailData.Name = "AppliedProductDetailData";
			// 
			// AppliedProductDetailsReport
			// 
			this.DataSource = this.AppliedProductDetailData;
			group1.GroupFooter = this.groupFooterSection;
			group1.GroupHeader = this.groupHeaderSection;
			group1.Groupings.Add(new Telerik.Reporting.Grouping("=Fields.ProductId"));
			group1.GroupKeepTogether = Telerik.Reporting.GroupKeepTogether.FirstDetail;
			group1.Name = "group";
			group2.GroupFooter = this.groupFooterSection1;
			group2.GroupHeader = this.groupHeaderSection1;
			group2.Groupings.Add(new Telerik.Reporting.Grouping("=Fields.FarmName"));
			group2.GroupKeepTogether = Telerik.Reporting.GroupKeepTogether.All;
			group2.Name = "group1";
			this.Groups.AddRange(new Telerik.Reporting.Group[] {
            group1,
            group2});
			this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.groupHeaderSection,
            this.groupFooterSection,
            this.groupHeaderSection1,
            this.groupFooterSection1,
            this.pageHeaderSection1,
            this.detail,
            this.pageFooterSection1});
			this.Name = "AppliedProductDetailsReport";
			this.PageSettings.Landscape = false;
			this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(0.25D), Telerik.Reporting.Drawing.Unit.Inch(0.25D), Telerik.Reporting.Drawing.Unit.Inch(0.25D), Telerik.Reporting.Drawing.Unit.Inch(0.25D));
			this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
			styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
			styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
			this.Width = Telerik.Reporting.Drawing.Unit.Inch(8D);
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PageFooterSection pageFooterSection1;
        private Telerik.Reporting.TextBox textBox93;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.GroupHeaderSection groupHeaderSection;
        private Telerik.Reporting.GroupFooterSection groupFooterSection;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.GroupHeaderSection groupHeaderSection1;
        private Telerik.Reporting.GroupFooterSection groupFooterSection1;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox61;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.ObjectDataSource AppliedProductDetailData;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox24;
		private Telerik.Reporting.TextBox pageInfoTextBox;
		private Telerik.Reporting.TextBox textBox65;
		private Telerik.Reporting.TextBox txtDataSourceLabel;
		private Telerik.Reporting.TextBox textBox100;
	}
}