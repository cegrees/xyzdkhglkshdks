namespace Landdb.Views.Secondary.Reports.AppliedProduct
{
    partial class DicambaForm
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup17 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.textBox25 = new Telerik.Reporting.TextBox();
            this.textBox27 = new Telerik.Reporting.TextBox();
            this.textBox29 = new Telerik.Reporting.TextBox();
            this.textBox31 = new Telerik.Reporting.TextBox();
            this.textBox39 = new Telerik.Reporting.TextBox();
            this.textBox51 = new Telerik.Reporting.TextBox();
            this.textBox96 = new Telerik.Reporting.TextBox();
            this.textBox97 = new Telerik.Reporting.TextBox();
            this.textBox98 = new Telerik.Reporting.TextBox();
            this.textBox99 = new Telerik.Reporting.TextBox();
            this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            this.txtDataSourceLabel = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox93 = new Telerik.Reporting.TextBox();
            this.textBox100 = new Telerik.Reporting.TextBox();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.panel1 = new Telerik.Reporting.Panel();
            this.panel3 = new Telerik.Reporting.Panel();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.panel2 = new Telerik.Reporting.Panel();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.textBox79 = new Telerik.Reporting.TextBox();
            this.textBox80 = new Telerik.Reporting.TextBox();
            this.panel7 = new Telerik.Reporting.Panel();
            this.textBox55 = new Telerik.Reporting.TextBox();
            this.textBox57 = new Telerik.Reporting.TextBox();
            this.textBox59 = new Telerik.Reporting.TextBox();
            this.textBox50 = new Telerik.Reporting.TextBox();
            this.textBox52 = new Telerik.Reporting.TextBox();
            this.textBox47 = new Telerik.Reporting.TextBox();
            this.textBox54 = new Telerik.Reporting.TextBox();
            this.textBox46 = new Telerik.Reporting.TextBox();
            this.textBox48 = new Telerik.Reporting.TextBox();
            this.panel6 = new Telerik.Reporting.Panel();
            this.textBox49 = new Telerik.Reporting.TextBox();
            this.textBox56 = new Telerik.Reporting.TextBox();
            this.textBox58 = new Telerik.Reporting.TextBox();
            this.textBox60 = new Telerik.Reporting.TextBox();
            this.textBox61 = new Telerik.Reporting.TextBox();
            this.textBox53 = new Telerik.Reporting.TextBox();
            this.textBox63 = new Telerik.Reporting.TextBox();
            this.textBox64 = new Telerik.Reporting.TextBox();
            this.textBox65 = new Telerik.Reporting.TextBox();
            this.panel8 = new Telerik.Reporting.Panel();
            this.textBox66 = new Telerik.Reporting.TextBox();
            this.textBox67 = new Telerik.Reporting.TextBox();
            this.textBox69 = new Telerik.Reporting.TextBox();
            this.textBox70 = new Telerik.Reporting.TextBox();
            this.textBox71 = new Telerik.Reporting.TextBox();
            this.textBox73 = new Telerik.Reporting.TextBox();
            this.textBox74 = new Telerik.Reporting.TextBox();
            this.textBox68 = new Telerik.Reporting.TextBox();
            this.textBox72 = new Telerik.Reporting.TextBox();
            this.textBox77 = new Telerik.Reporting.TextBox();
            this.textBox76 = new Telerik.Reporting.TextBox();
            this.textBox75 = new Telerik.Reporting.TextBox();
            this.panel9 = new Telerik.Reporting.Panel();
            this.table4 = new Telerik.Reporting.Table();
            this.textBox91 = new Telerik.Reporting.TextBox();
            this.textBox92 = new Telerik.Reporting.TextBox();
            this.textBox94 = new Telerik.Reporting.TextBox();
            this.textBox95 = new Telerik.Reporting.TextBox();
            this.textBox78 = new Telerik.Reporting.TextBox();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.panel5 = new Telerik.Reporting.Panel();
            this.table1 = new Telerik.Reporting.Table();
            this.textBox26 = new Telerik.Reporting.TextBox();
            this.textBox28 = new Telerik.Reporting.TextBox();
            this.textBox30 = new Telerik.Reporting.TextBox();
            this.textBox32 = new Telerik.Reporting.TextBox();
            this.textBox40 = new Telerik.Reporting.TextBox();
            this.textBox62 = new Telerik.Reporting.TextBox();
            this.textBox43 = new Telerik.Reporting.TextBox();
            this.shape1 = new Telerik.Reporting.Shape();
            this.textBox44 = new Telerik.Reporting.TextBox();
            this.textBox45 = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
            this.table3 = new Telerik.Reporting.Table();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.textBox33 = new Telerik.Reporting.TextBox();
            this.textBox34 = new Telerik.Reporting.TextBox();
            this.textBox35 = new Telerik.Reporting.TextBox();
            this.textBox38 = new Telerik.Reporting.TextBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox25
            // 
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.921875D), Telerik.Reporting.Drawing.Unit.Inch(0.2083333283662796D));
            this.textBox25.Style.Color = System.Drawing.Color.Black;
            this.textBox25.Style.Font.Bold = true;
            this.textBox25.Style.Font.Name = "Segoe UI";
            this.textBox25.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox25.Style.Font.Underline = true;
            this.textBox25.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox25.Value = "Field";
            // 
            // textBox27
            // 
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.78645837306976318D), Telerik.Reporting.Drawing.Unit.Inch(0.2083333283662796D));
            this.textBox27.Style.Color = System.Drawing.Color.Black;
            this.textBox27.Style.Font.Bold = true;
            this.textBox27.Style.Font.Name = "Segoe UI";
            this.textBox27.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox27.Style.Font.Underline = true;
            this.textBox27.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox27.Value = "Crop Zone";
            // 
            // textBox29
            // 
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.58854168653488159D), Telerik.Reporting.Drawing.Unit.Inch(0.2083333283662796D));
            this.textBox29.Style.Color = System.Drawing.Color.Black;
            this.textBox29.Style.Font.Bold = true;
            this.textBox29.Style.Font.Name = "Segoe UI";
            this.textBox29.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox29.Style.Font.Underline = true;
            this.textBox29.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox29.Value = "Area";
            // 
            // textBox31
            // 
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.2083333283662796D));
            this.textBox31.Style.Color = System.Drawing.Color.Black;
            this.textBox31.Style.Font.Bold = true;
            this.textBox31.Style.Font.Name = "Segoe UI";
            this.textBox31.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox31.Style.Font.Underline = true;
            this.textBox31.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox31.StyleName = "";
            this.textBox31.Value = "Buffer";
            // 
            // textBox39
            // 
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.6927084922790527D), Telerik.Reporting.Drawing.Unit.Inch(0.2083333283662796D));
            this.textBox39.Style.Color = System.Drawing.Color.Black;
            this.textBox39.Style.Font.Bold = true;
            this.textBox39.Style.Font.Name = "Segoe UI";
            this.textBox39.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox39.Style.Font.Underline = true;
            this.textBox39.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox39.StyleName = "";
            this.textBox39.Value = "Crop";
            // 
            // textBox51
            // 
            this.textBox51.Name = "textBox51";
            this.textBox51.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1302083730697632D), Telerik.Reporting.Drawing.Unit.Inch(0.2083333283662796D));
            this.textBox51.Style.Color = System.Drawing.Color.Black;
            this.textBox51.Style.Font.Bold = true;
            this.textBox51.Style.Font.Name = "Segoe UI";
            this.textBox51.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox51.Style.Font.Underline = true;
            this.textBox51.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox51.StyleName = "";
            this.textBox51.Value = "Planting Date";
            // 
            // textBox96
            // 
            this.textBox96.Name = "textBox96";
            this.textBox96.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.421875D), Telerik.Reporting.Drawing.Unit.Inch(0.21000000834465027D));
            this.textBox96.Style.Color = System.Drawing.Color.Black;
            this.textBox96.Style.Font.Bold = true;
            this.textBox96.Style.Font.Name = "Segoe UI";
            this.textBox96.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox96.Style.Font.Underline = true;
            this.textBox96.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox96.Value = "Product(s)";
            // 
            // textBox97
            // 
            this.textBox97.Name = "textBox97";
            this.textBox97.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3020833730697632D), Telerik.Reporting.Drawing.Unit.Inch(0.2099999338388443D));
            this.textBox97.Style.Color = System.Drawing.Color.Black;
            this.textBox97.Style.Font.Bold = true;
            this.textBox97.Style.Font.Name = "Segoe UI";
            this.textBox97.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox97.Style.Font.Underline = true;
            this.textBox97.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox97.Value = "Reg #";
            // 
            // textBox98
            // 
            this.textBox98.Name = "textBox98";
            this.textBox98.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4133331775665283D), Telerik.Reporting.Drawing.Unit.Inch(0.20999994874000549D));
            this.textBox98.Style.Color = System.Drawing.Color.Black;
            this.textBox98.Style.Font.Bold = true;
            this.textBox98.Style.Font.Name = "Segoe UI";
            this.textBox98.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox98.Style.Font.Underline = true;
            this.textBox98.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox98.Value = "Rate / Area";
            // 
            // textBox99
            // 
            this.textBox99.Name = "textBox99";
            this.textBox99.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4687495231628418D), Telerik.Reporting.Drawing.Unit.Inch(0.2099999338388443D));
            this.textBox99.Style.Color = System.Drawing.Color.Black;
            this.textBox99.Style.Font.Bold = true;
            this.textBox99.Style.Font.Name = "Segoe UI";
            this.textBox99.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox99.Style.Font.Underline = true;
            this.textBox99.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox99.StyleName = "";
            this.textBox99.Value = "Total Product";
            // 
            // pageHeaderSection1
            // 
            this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.828164279460907D);
            this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.txtDataSourceLabel,
            this.textBox3,
            this.textBox93,
            this.textBox100,
            this.textBox21});
            this.pageHeaderSection1.Name = "pageHeaderSection1";
            // 
            // txtDataSourceLabel
            // 
            this.txtDataSourceLabel.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.0105743408203125D), Telerik.Reporting.Drawing.Unit.Inch(0.028125008568167686D));
            this.txtDataSourceLabel.Name = "txtDataSourceLabel";
            this.txtDataSourceLabel.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.0312089920043945D), Telerik.Reporting.Drawing.Unit.Inch(0.27187496423721313D));
            this.txtDataSourceLabel.Style.Font.Name = "Segoe UI Light";
            this.txtDataSourceLabel.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(18D);
            this.txtDataSourceLabel.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.txtDataSourceLabel.Value = "=Fields.DataSourceName";
            // 
            // textBox3
            // 
            this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.900118350982666D), Telerik.Reporting.Drawing.Unit.Inch(0.52820366621017456D));
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9998817443847656D), Telerik.Reporting.Drawing.Unit.Inch(0.29996061325073242D));
            this.textBox3.Style.Color = System.Drawing.Color.DimGray;
            this.textBox3.Style.Font.Name = "Segoe UI Light";
            this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox3.TextWrap = false;
            this.textBox3.Value = "=Fields.TotalArea";
            // 
            // textBox93
            // 
            this.textBox93.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.4313278198242188D), Telerik.Reporting.Drawing.Unit.Inch(0.028125008568167686D));
            this.textBox93.Name = "textBox93";
            this.textBox93.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.4687116146087646D), Telerik.Reporting.Drawing.Unit.Inch(0.29996061325073242D));
            this.textBox93.Style.Color = System.Drawing.Color.DimGray;
            this.textBox93.Style.Font.Name = "Segoe UI Light";
            this.textBox93.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(18D);
            this.textBox93.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox93.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox93.TextWrap = false;
            this.textBox93.Value = "Dicamba Form";
            // 
            // textBox100
            // 
            this.textBox100.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.900118350982666D), Telerik.Reporting.Drawing.Unit.Inch(0.32816436886787415D));
            this.textBox100.Name = "textBox100";
            this.textBox100.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9998817443847656D), Telerik.Reporting.Drawing.Unit.Inch(0.19996063411235809D));
            this.textBox100.Style.Color = System.Drawing.Color.DimGray;
            this.textBox100.Style.Font.Name = "Segoe UI";
            this.textBox100.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox100.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox100.TextWrap = false;
            this.textBox100.Value = "=Fields.CropYear";
            // 
            // textBox21
            // 
            this.textBox21.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D), Telerik.Reporting.Drawing.Unit.Inch(0.32816433906555176D));
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.9894258975982666D), Telerik.Reporting.Drawing.Unit.Inch(0.29996061325073242D));
            this.textBox21.Style.Color = System.Drawing.Color.DimGray;
            this.textBox21.Style.Font.Name = "Segoe UI";
            this.textBox21.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox21.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox21.TextWrap = false;
            this.textBox21.Value = "=Fields.ApplicationName";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(8.8000001907348633D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.panel1,
            this.panel3,
            this.panel2,
            this.panel7,
            this.panel6,
            this.panel8,
            this.panel9,
            this.panel5,
            this.table3,
            this.textBox1});
            this.detail.Name = "detail";
            // 
            // panel1
            // 
            this.panel1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(double.NaN), Telerik.Reporting.Drawing.Unit.Inch(double.NaN));
            this.panel1.Name = "panel1";
            this.panel1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.9894652366638184D), Telerik.Reporting.Drawing.Unit.Inch(0.70003938674926758D));
            this.panel1.Style.BorderColor.Bottom = System.Drawing.Color.Gray;
            this.panel1.Style.BorderColor.Top = System.Drawing.Color.Gray;
            this.panel1.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel1.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel1.Style.Color = System.Drawing.Color.Gray;
            // 
            // panel3
            // 
            this.panel3.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox8,
            this.textBox9,
            this.textBox11,
            this.textBox10,
            this.textBox20,
            this.textBox22,
            this.textBox23,
            this.textBox18});
            this.panel3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(1.5000787973403931D));
            this.panel3.Name = "panel3";
            this.panel3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.9105749130249023D), Telerik.Reporting.Drawing.Unit.Inch(0.999921441078186D));
            // 
            // textBox8
            // 
            this.textBox8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(7.8519187809433788E-05D), Telerik.Reporting.Drawing.Unit.Inch(0.10003948211669922D));
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.199921727180481D), Telerik.Reporting.Drawing.Unit.Inch(0.79992103576660156D));
            this.textBox8.Style.BackgroundColor = System.Drawing.Color.Silver;
            this.textBox8.Style.Color = System.Drawing.Color.Black;
            this.textBox8.Style.Font.Bold = true;
            this.textBox8.Style.Font.Name = "Segoe UI";
            this.textBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox8.Value = "SUSCEPTIBLE CROP AWARENESS";
            // 
            // textBox9
            // 
            this.textBox9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.1897014379501343D), Telerik.Reporting.Drawing.Unit.Inch(0.20003986358642578D));
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.3073320388793945D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox9.Style.Color = System.Drawing.Color.Black;
            this.textBox9.Style.Font.Bold = true;
            this.textBox9.Style.Font.Name = "Segoe UI";
            this.textBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox9.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox9.Value = "Name and Date of the Sensitive Crop Registry Consulted:";
            // 
            // textBox11
            // 
            this.textBox11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.2897014617919922D), Telerik.Reporting.Drawing.Unit.Inch(0.39996084570884705D));
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.7000000476837158D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBox11.Style.Font.Bold = true;
            this.textBox11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox11.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox11.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox11.Value = "OR";
            // 
            // textBox10
            // 
            this.textBox10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.1897014379501343D), Telerik.Reporting.Drawing.Unit.Inch(0.6000397801399231D));
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.4102988243103027D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox10.Style.Color = System.Drawing.Color.Black;
            this.textBox10.Style.Font.Bold = true;
            this.textBox10.Style.Font.Name = "Segoe UI";
            this.textBox10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox10.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox10.Value = "Date Neighboring Fields Were Surveyed for Susceptible Crops:";
            // 
            // textBox20
            // 
            this.textBox20.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.60007905960083D), Telerik.Reporting.Drawing.Unit.Inch(0.60003995895385742D));
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1891887187957764D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox20.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox20.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox20.Value = "   /       / ";
            // 
            // textBox22
            // 
            this.textBox22.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.8106536865234375D), Telerik.Reporting.Drawing.Unit.Inch(0.19093132019042969D));
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0893077850341797D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox22.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox22.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox22.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox22.Value = "";
            // 
            // textBox23
            // 
            this.textBox23.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.8106536865234375D), Telerik.Reporting.Drawing.Unit.Inch(0.590931236743927D));
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0893864631652832D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox23.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox23.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox23.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox23.Value = "";
            // 
            // textBox18
            // 
            this.textBox18.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.5999999046325684D), Telerik.Reporting.Drawing.Unit.Inch(0.20003969967365265D));
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1892681121826172D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox18.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox18.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox18.Value = "   /       / ";
            // 
            // panel2
            // 
            this.panel2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox19,
            this.textBox5,
            this.textBox6,
            this.textBox7,
            this.textBox17,
            this.textBox79,
            this.textBox80});
            this.panel2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.0105349225923419D), Telerik.Reporting.Drawing.Unit.Inch(0.59999996423721313D));
            this.panel2.Name = "panel2";
            this.panel2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.9894657135009766D), Telerik.Reporting.Drawing.Unit.Inch(0.89999991655349731D));
            this.panel2.Style.BorderColor.Bottom = System.Drawing.Color.Gray;
            this.panel2.Style.BorderColor.Top = System.Drawing.Color.Gray;
            this.panel2.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel2.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            // 
            // textBox19
            // 
            this.textBox19.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D), Telerik.Reporting.Drawing.Unit.Inch(0.1000000610947609D));
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1999212503433228D), Telerik.Reporting.Drawing.Unit.Inch(0.699999988079071D));
            this.textBox19.Style.BackgroundColor = System.Drawing.Color.Silver;
            this.textBox19.Style.Color = System.Drawing.Color.Black;
            this.textBox19.Style.Font.Bold = true;
            this.textBox19.Style.Font.Name = "Segoe UI";
            this.textBox19.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox19.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox19.Value = "REQUIRED APPLICATOR TRAINING";
            // 
            // textBox5
            // 
            this.textBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.2000396251678467D), Telerik.Reporting.Drawing.Unit.Inch(0.1541178971529007D));
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.089425802230835D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox5.Style.Color = System.Drawing.Color.Black;
            this.textBox5.Style.Font.Bold = true;
            this.textBox5.Style.Font.Name = "Segoe UI";
            this.textBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox5.Style.Font.Underline = false;
            this.textBox5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox5.Value = "Applicator Name (if different than certified applicator):";
            // 
            // textBox6
            // 
            this.textBox6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.200079083442688D), Telerik.Reporting.Drawing.Unit.Inch(0.50000029802322388D));
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0893862247467041D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox6.Style.Color = System.Drawing.Color.Black;
            this.textBox6.Style.Font.Bold = true;
            this.textBox6.Style.Font.Name = "Segoe UI";
            this.textBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox6.Value = "Date Completed (MM/DD/YY):";
            // 
            // textBox7
            // 
            this.textBox7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.6894650459289551D), Telerik.Reporting.Drawing.Unit.Inch(0.50000029802322388D));
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.90000033378601074D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox7.Style.Font.Bold = true;
            this.textBox7.Style.Font.Name = "Segoe UI";
            this.textBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox7.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox7.Value = "Provider:";
            // 
            // textBox17
            // 
            this.textBox17.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.2895443439483643D), Telerik.Reporting.Drawing.Unit.Inch(0.50000029802322388D));
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2999206781387329D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox17.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox17.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox17.Value = "   /       / ";
            // 
            // textBox79
            // 
            this.textBox79.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.40000057220459D), Telerik.Reporting.Drawing.Unit.Inch(0.1450093537569046D));
            this.textBox79.Name = "textBox79";
            this.textBox79.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.4541275501251221D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox79.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox79.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox79.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox79.Value = "";
            // 
            // textBox80
            // 
            this.textBox80.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.5895442962646484D), Telerik.Reporting.Drawing.Unit.Inch(0.50000029802322388D));
            this.textBox80.Name = "textBox80";
            this.textBox80.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.2645835876464844D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox80.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox80.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox80.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox80.Value = "";
            // 
            // panel7
            // 
            this.panel7.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox55,
            this.textBox57,
            this.textBox59,
            this.textBox50,
            this.textBox52,
            this.textBox47,
            this.textBox54,
            this.textBox46,
            this.textBox48});
            this.panel7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(4.0000791549682617D));
            this.panel7.Name = "panel7";
            this.panel7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.90000057220459D), Telerik.Reporting.Drawing.Unit.Inch(0.99992102384567261D));
            this.panel7.Style.BorderColor.Bottom = System.Drawing.Color.Gray;
            this.panel7.Style.BorderColor.Top = System.Drawing.Color.Gray;
            this.panel7.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel7.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            // 
            // textBox55
            // 
            this.textBox55.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(7.8519187809433788E-05D), Telerik.Reporting.Drawing.Unit.Inch(0.10003948211669922D));
            this.textBox55.Name = "textBox55";
            this.textBox55.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.199921727180481D), Telerik.Reporting.Drawing.Unit.Inch(0.79992103576660156D));
            this.textBox55.Style.BackgroundColor = System.Drawing.Color.Silver;
            this.textBox55.Style.Color = System.Drawing.Color.Black;
            this.textBox55.Style.Font.Bold = true;
            this.textBox55.Style.Font.Name = "Segoe UI";
            this.textBox55.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox55.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox55.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox55.Value = "NOZZLE INFO";
            // 
            // textBox57
            // 
            this.textBox57.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.1897014379501343D), Telerik.Reporting.Drawing.Unit.Inch(0.6000397801399231D));
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4427865743637085D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox57.Style.Color = System.Drawing.Color.Black;
            this.textBox57.Style.Font.Bold = true;
            this.textBox57.Style.Font.Name = "Segoe UI";
            this.textBox57.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox57.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox57.Value = "Orifice Size:";
            // 
            // textBox59
            // 
            this.textBox59.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.9000792503356934D), Telerik.Reporting.Drawing.Unit.Inch(0.590931236743927D));
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9999605417251587D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox59.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox59.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox59.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox59.Value = "";
            // 
            // textBox50
            // 
            this.textBox50.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.800079345703125D), Telerik.Reporting.Drawing.Unit.Inch(0.20004017651081085D));
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0540091991424561D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox50.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox50.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox50.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox50.Value = "";
            // 
            // textBox52
            // 
            this.textBox52.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.3000402450561523D), Telerik.Reporting.Drawing.Unit.Inch(0.20914840698242188D));
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.49995997548103333D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox52.Style.Color = System.Drawing.Color.Black;
            this.textBox52.Style.Font.Bold = true;
            this.textBox52.Style.Font.Name = "Segoe UI";
            this.textBox52.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox52.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox52.Value = "Type:";
            // 
            // textBox47
            // 
            this.textBox47.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.200079083442688D), Telerik.Reporting.Drawing.Unit.Inch(0.20004017651081085D));
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4427865743637085D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox47.Style.Color = System.Drawing.Color.Black;
            this.textBox47.Style.Font.Bold = true;
            this.textBox47.Style.Font.Name = "Segoe UI";
            this.textBox47.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox47.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox47.Value = "Manufacturer/Brand:";
            // 
            // textBox54
            // 
            this.textBox54.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.6429445743560791D), Telerik.Reporting.Drawing.Unit.Inch(0.20004017651081085D));
            this.textBox54.Name = "textBox54";
            this.textBox54.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8540886640548706D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox54.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox54.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox54.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox54.Value = "";
            // 
            // textBox46
            // 
            this.textBox46.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.6429450511932373D), Telerik.Reporting.Drawing.Unit.Inch(0.59093093872070312D));
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8540886640548706D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox46.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox46.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox46.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox46.Value = "";
            // 
            // textBox48
            // 
            this.textBox48.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.5000004768371582D), Telerik.Reporting.Drawing.Unit.Inch(0.59093093872070312D));
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3999999761581421D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox48.Style.Color = System.Drawing.Color.Black;
            this.textBox48.Style.Font.Bold = true;
            this.textBox48.Style.Font.Name = "Segoe UI";
            this.textBox48.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox48.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox48.Value = "Operating Pressure:";
            // 
            // panel6
            // 
            this.panel6.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox49,
            this.textBox56,
            this.textBox58,
            this.textBox60,
            this.textBox61,
            this.textBox53,
            this.textBox63,
            this.textBox64,
            this.textBox65});
            this.panel6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.0105743408203125D), Telerik.Reporting.Drawing.Unit.Inch(5.0000791549682617D));
            this.panel6.Name = "panel6";
            this.panel6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.90000057220459D), Telerik.Reporting.Drawing.Unit.Inch(0.99992102384567261D));
            this.panel6.Style.BorderColor.Bottom = System.Drawing.Color.Gray;
            this.panel6.Style.BorderColor.Top = System.Drawing.Color.Gray;
            this.panel6.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel6.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            // 
            // textBox49
            // 
            this.textBox49.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(7.8519187809433788E-05D), Telerik.Reporting.Drawing.Unit.Inch(0.10003948211669922D));
            this.textBox49.Name = "textBox49";
            this.textBox49.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.199921727180481D), Telerik.Reporting.Drawing.Unit.Inch(0.79988223314285278D));
            this.textBox49.Style.BackgroundColor = System.Drawing.Color.Silver;
            this.textBox49.Style.Color = System.Drawing.Color.Black;
            this.textBox49.Style.Font.Bold = true;
            this.textBox49.Style.Font.Name = "Segoe UI";
            this.textBox49.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox49.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox49.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox49.Value = "APPLICATION TIMING";
            // 
            // textBox56
            // 
            this.textBox56.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.7895050048828125D), Telerik.Reporting.Drawing.Unit.Inch(0.20902951061725617D));
            this.textBox56.Name = "textBox56";
            this.textBox56.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0540091991424561D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox56.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox56.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox56.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox56.Value = "=Fields.TimingEvent";
            // 
            // textBox58
            // 
            this.textBox58.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.5894260406494141D), Telerik.Reporting.Drawing.Unit.Inch(0.20902951061725617D));
            this.textBox58.Name = "textBox58";
            this.textBox58.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2105741500854492D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox58.Style.Color = System.Drawing.Color.Black;
            this.textBox58.Style.Font.Bold = true;
            this.textBox58.Style.Font.Name = "Segoe UI";
            this.textBox58.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox58.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox58.Value = "Timing Event:";
            // 
            // textBox60
            // 
            this.textBox60.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.200079083442688D), Telerik.Reporting.Drawing.Unit.Inch(0.19992129504680634D));
            this.textBox60.Name = "textBox60";
            this.textBox60.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4427865743637085D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox60.Style.Color = System.Drawing.Color.Black;
            this.textBox60.Style.Font.Bold = true;
            this.textBox60.Style.Font.Name = "Segoe UI";
            this.textBox60.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox60.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox60.Value = "Date of Application:";
            // 
            // textBox61
            // 
            this.textBox61.Format = "{0:d}";
            this.textBox61.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.6429445743560791D), Telerik.Reporting.Drawing.Unit.Inch(0.19992129504680634D));
            this.textBox61.Name = "textBox61";
            this.textBox61.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8540886640548706D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox61.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox61.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox61.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox61.Value = "=Fields.StartDate";
            // 
            // textBox53
            // 
            this.textBox53.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.200079083442688D), Telerik.Reporting.Drawing.Unit.Inch(0.599920928478241D));
            this.textBox53.Name = "textBox53";
            this.textBox53.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4427865743637085D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox53.Style.Font.Bold = true;
            this.textBox53.Style.Font.Name = "Segoe UI";
            this.textBox53.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox53.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox53.Value = "Start Time:";
            // 
            // textBox63
            // 
            this.textBox63.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.5894255638122559D), Telerik.Reporting.Drawing.Unit.Inch(0.599920928478241D));
            this.textBox63.Name = "textBox63";
            this.textBox63.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2105748653411865D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox63.Style.Color = System.Drawing.Color.Black;
            this.textBox63.Style.Font.Bold = true;
            this.textBox63.Style.Font.Name = "Segoe UI";
            this.textBox63.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox63.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox63.Value = "End Time:";
            // 
            // textBox64
            // 
            this.textBox64.Format = "{0:t}";
            this.textBox64.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.6429450511932373D), Telerik.Reporting.Drawing.Unit.Inch(0.599920928478241D));
            this.textBox64.Name = "textBox64";
            this.textBox64.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8540886640548706D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox64.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox64.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox64.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox64.Value = "=Fields.StartDate";
            // 
            // textBox65
            // 
            this.textBox65.Format = "{0:t}";
            this.textBox65.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.800079345703125D), Telerik.Reporting.Drawing.Unit.Inch(0.599920928478241D));
            this.textBox65.Name = "textBox65";
            this.textBox65.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0540091991424561D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox65.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox65.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox65.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox65.Value = "=Fields.EndDate";
            // 
            // panel8
            // 
            this.panel8.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox66,
            this.textBox67,
            this.textBox69,
            this.textBox70,
            this.textBox71,
            this.textBox73,
            this.textBox74,
            this.textBox68,
            this.textBox72,
            this.textBox77,
            this.textBox76,
            this.textBox75});
            this.panel8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(6.0000786781311035D));
            this.panel8.Name = "panel8";
            this.panel8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.90000057220459D), Telerik.Reporting.Drawing.Unit.Inch(1.799920916557312D));
            this.panel8.Style.BorderColor.Bottom = System.Drawing.Color.Gray;
            this.panel8.Style.BorderColor.Top = System.Drawing.Color.Gray;
            this.panel8.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel8.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            // 
            // textBox66
            // 
            this.textBox66.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(7.8519187809433788E-05D), Telerik.Reporting.Drawing.Unit.Inch(0.10003948211669922D));
            this.textBox66.Name = "textBox66";
            this.textBox66.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.199921727180481D), Telerik.Reporting.Drawing.Unit.Inch(1.5998821258544922D));
            this.textBox66.Style.BackgroundColor = System.Drawing.Color.Silver;
            this.textBox66.Style.Color = System.Drawing.Color.Black;
            this.textBox66.Style.Font.Bold = true;
            this.textBox66.Style.Font.Name = "Segoe UI";
            this.textBox66.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox66.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox66.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox66.Value = "WEATHER INFO";
            // 
            // textBox67
            // 
            this.textBox67.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.5D), Telerik.Reporting.Drawing.Unit.Inch(0.49081358313560486D));
            this.textBox67.Name = "textBox67";
            this.textBox67.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0540091991424561D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox67.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox67.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox67.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox67.Value = "=Fields.EndTemperature";
            // 
            // textBox69
            // 
            this.textBox69.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.2106534242630005D), Telerik.Reporting.Drawing.Unit.Inch(0.49992179870605469D));
            this.textBox69.Name = "textBox69";
            this.textBox69.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4427865743637085D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox69.Style.Font.Bold = true;
            this.textBox69.Style.Font.Name = "Segoe UI";
            this.textBox69.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox69.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox69.Value = "Air Temp:";
            // 
            // textBox70
            // 
            this.textBox70.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.045832633972168D), Telerik.Reporting.Drawing.Unit.Inch(0.49992179870605469D));
            this.textBox70.Name = "textBox70";
            this.textBox70.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8540886640548706D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox70.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox70.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox70.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox70.Value = "=Fields.Temperature";
            // 
            // textBox71
            // 
            this.textBox71.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.2106534242630005D), Telerik.Reporting.Drawing.Unit.Inch(0.82478457689285278D));
            this.textBox71.Name = "textBox71";
            this.textBox71.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4427865743637085D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox71.Style.Font.Bold = true;
            this.textBox71.Style.Font.Name = "Segoe UI";
            this.textBox71.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox71.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox71.Value = "Wind Speed:";
            // 
            // textBox73
            // 
            this.textBox73.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.0614578723907471D), Telerik.Reporting.Drawing.Unit.Inch(0.81567639112472534D));
            this.textBox73.Name = "textBox73";
            this.textBox73.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8540886640548706D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox73.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox73.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox73.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox73.Value = "=Fields.WindSpeed";
            // 
            // textBox74
            // 
            this.textBox74.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.5D), Telerik.Reporting.Drawing.Unit.Inch(0.81567639112472534D));
            this.textBox74.Name = "textBox74";
            this.textBox74.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0540091991424561D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox74.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox74.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox74.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox74.Value = "=Fields.EndWindSpeed";
            // 
            // textBox68
            // 
            this.textBox68.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.0458328723907471D), Telerik.Reporting.Drawing.Unit.Inch(0.10003916174173355D));
            this.textBox68.Name = "textBox68";
            this.textBox68.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8435142040252686D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox68.Style.Color = System.Drawing.Color.Gray;
            this.textBox68.Style.Font.Bold = true;
            this.textBox68.Style.Font.Name = "Segoe UI";
            this.textBox68.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox68.Value = "APPLICATION START";
            // 
            // textBox72
            // 
            this.textBox72.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.5D), Telerik.Reporting.Drawing.Unit.Inch(0.10003916174173355D));
            this.textBox72.Name = "textBox72";
            this.textBox72.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0540091991424561D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox72.Style.Color = System.Drawing.Color.Gray;
            this.textBox72.Style.Font.Bold = true;
            this.textBox72.Style.Font.Name = "Segoe UI";
            this.textBox72.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox72.Value = "APPLICATION END";
            // 
            // textBox77
            // 
            this.textBox77.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.2106534242630005D), Telerik.Reporting.Drawing.Unit.Inch(1.1999212503433228D));
            this.textBox77.Name = "textBox77";
            this.textBox77.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4427865743637085D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox77.Style.Font.Bold = true;
            this.textBox77.Style.Font.Name = "Segoe UI";
            this.textBox77.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox77.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox77.Value = "Wind Direction:";
            // 
            // textBox76
            // 
            this.textBox76.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.0439865589141846D), Telerik.Reporting.Drawing.Unit.Inch(1.1895046234130859D));
            this.textBox76.Name = "textBox76";
            this.textBox76.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8540886640548706D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox76.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox76.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox76.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox76.Value = "=Fields.WindDirection";
            // 
            // textBox75
            // 
            this.textBox75.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.5023198127746582D), Telerik.Reporting.Drawing.Unit.Inch(1.1895046234130859D));
            this.textBox75.Name = "textBox75";
            this.textBox75.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0540091991424561D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox75.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox75.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox75.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox75.Value = "=Fields.EndWindDirection";
            // 
            // panel9
            // 
            this.panel9.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.table4,
            this.textBox78,
            this.textBox24});
            this.panel9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(7.8519187809433788E-05D), Telerik.Reporting.Drawing.Unit.Inch(7.8000788688659668D));
            this.panel9.Name = "panel9";
            this.panel9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.9000396728515625D), Telerik.Reporting.Drawing.Unit.Inch(0.99992114305496216D));
            this.panel9.Style.BorderColor.Bottom = System.Drawing.Color.Gray;
            this.panel9.Style.BorderColor.Top = System.Drawing.Color.Gray;
            this.panel9.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel9.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            // 
            // table4
            // 
            this.table4.Bindings.Add(new Telerik.Reporting.Binding("DataSource", "=Fields.Products"));
            this.table4.Bindings.Add(new Telerik.Reporting.Binding("Visible", "=Fields.Products.Count > 0"));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.4218744039535523D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.3020832538604736D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.4133332967758179D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.4687491655349731D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.20999999344348908D)));
            this.table4.Body.SetCellContent(0, 0, this.textBox91);
            this.table4.Body.SetCellContent(0, 1, this.textBox92);
            this.table4.Body.SetCellContent(0, 2, this.textBox94);
            this.table4.Body.SetCellContent(0, 3, this.textBox95);
            tableGroup1.Name = "tableGroup3";
            tableGroup1.ReportItem = this.textBox96;
            tableGroup2.Name = "tableGroup4";
            tableGroup2.ReportItem = this.textBox97;
            tableGroup3.Name = "tableGroup5";
            tableGroup3.ReportItem = this.textBox98;
            tableGroup4.Name = "group2";
            tableGroup4.ReportItem = this.textBox99;
            this.table4.ColumnGroups.Add(tableGroup1);
            this.table4.ColumnGroups.Add(tableGroup2);
            this.table4.ColumnGroups.Add(tableGroup3);
            this.table4.ColumnGroups.Add(tableGroup4);
            this.table4.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox91,
            this.textBox92,
            this.textBox94,
            this.textBox95,
            this.textBox96,
            this.textBox97,
            this.textBox98,
            this.textBox99});
            this.table4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.2106536626815796D), Telerik.Reporting.Drawing.Unit.Inch(0.29992231726646423D));
            this.table4.Name = "table4";
            tableGroup5.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup5.Name = "detailTableGroup1";
            this.table4.RowGroups.Add(tableGroup5);
            this.table4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(5.6060400009155273D), Telerik.Reporting.Drawing.Unit.Inch(0.41999998688697815D));
            // 
            // textBox91
            // 
            this.textBox91.Name = "textBox91";
            this.textBox91.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4218745231628418D), Telerik.Reporting.Drawing.Unit.Inch(0.21000000834465027D));
            this.textBox91.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox91.Value = "=Fields.ProductName";
            // 
            // textBox92
            // 
            this.textBox92.Name = "textBox92";
            this.textBox92.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3020833730697632D), Telerik.Reporting.Drawing.Unit.Inch(0.21000003814697266D));
            this.textBox92.Value = "=Fields.EpaNumber";
            // 
            // textBox94
            // 
            this.textBox94.Name = "textBox94";
            this.textBox94.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4133331775665283D), Telerik.Reporting.Drawing.Unit.Inch(0.21000003814697266D));
            this.textBox94.Value = "=Fields.RateDisplay";
            // 
            // textBox95
            // 
            this.textBox95.Name = "textBox95";
            this.textBox95.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4687495231628418D), Telerik.Reporting.Drawing.Unit.Inch(0.21000003814697266D));
            this.textBox95.StyleName = "";
            this.textBox95.Value = "=Fields.TotalDisplay";
            // 
            // textBox78
            // 
            this.textBox78.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.099921546876430511D));
            this.textBox78.Name = "textBox78";
            this.textBox78.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.199921727180481D), Telerik.Reporting.Drawing.Unit.Inch(0.79988223314285278D));
            this.textBox78.Style.BackgroundColor = System.Drawing.Color.Silver;
            this.textBox78.Style.Color = System.Drawing.Color.Black;
            this.textBox78.Style.Font.Bold = true;
            this.textBox78.Style.Font.Name = "Segoe UI";
            this.textBox78.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox78.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox78.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox78.Value = "TANK PRODUCTS";
            // 
            // textBox24
            // 
            this.textBox24.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.2106536626815796D), Telerik.Reporting.Drawing.Unit.Inch(0.099921546876430511D));
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.6893858909606934D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox24.Style.Font.Italic = true;
            this.textBox24.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox24.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox24.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox24.Value = "Retain receipt of each purchase of each application. Retain copy of all product l" +
    "abels, including state labels where applicable.";
            // 
            // panel5
            // 
            this.panel5.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.table1,
            this.textBox43,
            this.shape1,
            this.textBox44,
            this.textBox45,
            this.textBox12});
            this.panel5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.010457356460392475D), Telerik.Reporting.Drawing.Unit.Inch(2.5000002384185791D));
            this.panel5.Name = "panel5";
            this.panel5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.90011739730835D), Telerik.Reporting.Drawing.Unit.Inch(1.5D));
            this.panel5.Style.BorderColor.Bottom = System.Drawing.Color.Gray;
            this.panel5.Style.BorderColor.Top = System.Drawing.Color.Gray;
            this.panel5.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel5.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            // 
            // table1
            // 
            this.table1.Bindings.Add(new Telerik.Reporting.Binding("DataSource", "=Fields.Fields"));
            this.table1.Bindings.Add(new Telerik.Reporting.Binding("Visible", "=Fields.Fields.Count > 0"));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.92187529802322388D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.78645837306976318D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.58854186534881592D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.99999988079071045D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.6927083730697632D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.1302086114883423D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2083333283662796D)));
            this.table1.Body.SetCellContent(0, 0, this.textBox26);
            this.table1.Body.SetCellContent(0, 1, this.textBox28);
            this.table1.Body.SetCellContent(0, 2, this.textBox30);
            this.table1.Body.SetCellContent(0, 3, this.textBox32);
            this.table1.Body.SetCellContent(0, 4, this.textBox40);
            this.table1.Body.SetCellContent(0, 5, this.textBox62);
            tableGroup6.Name = "tableGroup";
            tableGroup6.ReportItem = this.textBox25;
            tableGroup7.Name = "tableGroup1";
            tableGroup7.ReportItem = this.textBox27;
            tableGroup8.Name = "tableGroup2";
            tableGroup8.ReportItem = this.textBox29;
            tableGroup9.Name = "group";
            tableGroup9.ReportItem = this.textBox31;
            tableGroup10.Name = "group1";
            tableGroup10.ReportItem = this.textBox39;
            tableGroup11.Name = "group3";
            tableGroup11.ReportItem = this.textBox51;
            this.table1.ColumnGroups.Add(tableGroup6);
            this.table1.ColumnGroups.Add(tableGroup7);
            this.table1.ColumnGroups.Add(tableGroup8);
            this.table1.ColumnGroups.Add(tableGroup9);
            this.table1.ColumnGroups.Add(tableGroup10);
            this.table1.ColumnGroups.Add(tableGroup11);
            this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox26,
            this.textBox28,
            this.textBox30,
            this.textBox32,
            this.textBox40,
            this.textBox62,
            this.textBox25,
            this.textBox27,
            this.textBox29,
            this.textBox31,
            this.textBox39,
            this.textBox51});
            this.table1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.1926676034927368D), Telerik.Reporting.Drawing.Unit.Inch(0.19999980926513672D));
            this.table1.Name = "table1";
            tableGroup12.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup12.Name = "detailTableGroup";
            this.table1.RowGroups.Add(tableGroup12);
            this.table1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.1197924613952637D), Telerik.Reporting.Drawing.Unit.Inch(0.4166666567325592D));
            // 
            // textBox26
            // 
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.921875D), Telerik.Reporting.Drawing.Unit.Inch(0.2083333432674408D));
            this.textBox26.Value = "=Fields.Field";
            // 
            // textBox28
            // 
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.78645837306976318D), Telerik.Reporting.Drawing.Unit.Inch(0.2083333283662796D));
            this.textBox28.Value = "=Fields.CropZone";
            // 
            // textBox30
            // 
            this.textBox30.Format = "{0:N2}";
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.58854168653488159D), Telerik.Reporting.Drawing.Unit.Inch(0.2083333432674408D));
            this.textBox30.Value = "=Fields.Area";
            // 
            // textBox32
            // 
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.2083333432674408D));
            this.textBox32.StyleName = "";
            this.textBox32.Value = "";
            // 
            // textBox40
            // 
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.6927084922790527D), Telerik.Reporting.Drawing.Unit.Inch(0.2083333283662796D));
            this.textBox40.StyleName = "";
            this.textBox40.Value = "=Fields.Crop";
            // 
            // textBox62
            // 
            this.textBox62.Name = "textBox62";
            this.textBox62.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1302083730697632D), Telerik.Reporting.Drawing.Unit.Inch(0.2083333283662796D));
            this.textBox62.StyleName = "";
            this.textBox62.Value = "=Fields.PlantingDate";
            // 
            // textBox43
            // 
            this.textBox43.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.2107309103012085D), Telerik.Reporting.Drawing.Unit.Inch(0.76571559906005859D));
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.7894258499145508D), Telerik.Reporting.Drawing.Unit.Inch(0.3999994695186615D));
            this.textBox43.Style.Color = System.Drawing.Color.Black;
            this.textBox43.Style.Font.Bold = true;
            this.textBox43.Style.Font.Name = "Segoe UI";
            this.textBox43.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox43.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox43.Value = "Check to Confirm Spray System Equipment is Cleaned Prior to Using:";
            // 
            // shape1
            // 
            this.shape1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.1001567840576172D), Telerik.Reporting.Drawing.Unit.Inch(0.86571568250656128D));
            this.shape1.Name = "shape1";
            this.shape1.ShapeType = new Telerik.Reporting.Drawing.Shapes.PolygonShape(4, 45D, 0);
            this.shape1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.25644651055336D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            // 
            // textBox44
            // 
            this.textBox44.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.9001564979553223D), Telerik.Reporting.Drawing.Unit.Inch(0.86571568250656128D));
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.9999604225158691D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox44.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox44.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox44.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox44.Value = "";
            // 
            // textBox45
            // 
            this.textBox45.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.4000778198242188D), Telerik.Reporting.Drawing.Unit.Inch(0.87482422590255737D));
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.5D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox45.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox45.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox45.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox45.Value = "(notes)";
            // 
            // textBox12
            // 
            this.textBox12.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.10003948211669922D));
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2000001668930054D), Telerik.Reporting.Drawing.Unit.Inch(1.2999604940414429D));
            this.textBox12.Style.BackgroundColor = System.Drawing.Color.Silver;
            this.textBox12.Style.Color = System.Drawing.Color.Black;
            this.textBox12.Style.Font.Bold = true;
            this.textBox12.Style.Font.Name = "Segoe UI";
            this.textBox12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox12.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox12.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox12.Value = "PRE - APPLICATION INFO";
            // 
            // pageFooterSection1
            // 
            this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.10000038146972656D);
            this.pageFooterSection1.Name = "pageFooterSection1";
            // 
            // table3
            // 
            this.table3.Bindings.Add(new Telerik.Reporting.Binding("DataSource", "=Fields.Applicators"));
            this.table3.Bindings.Add(new Telerik.Reporting.Binding("Visible", "=Fields.Applicators.Count > 0"));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.8333333730697632D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.9013851881027222D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.5694092512130737D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.18833324313163757D)));
            this.table3.Body.SetCellContent(0, 1, this.textBox13);
            this.table3.Body.SetCellContent(0, 2, this.textBox14);
            this.table3.Body.SetCellContent(0, 0, this.textBox38);
            tableGroup13.Name = "group4";
            tableGroup13.ReportItem = this.textBox35;
            tableGroup14.Name = "tableGroup6";
            tableGroup14.ReportItem = this.textBox33;
            tableGroup15.Name = "tableGroup7";
            tableGroup15.ReportItem = this.textBox34;
            this.table3.ColumnGroups.Add(tableGroup13);
            this.table3.ColumnGroups.Add(tableGroup14);
            this.table3.ColumnGroups.Add(tableGroup15);
            this.table3.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox13,
            this.textBox14,
            this.textBox33,
            this.textBox34,
            this.textBox35,
            this.textBox38});
            this.table3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.2211881875991821D), Telerik.Reporting.Drawing.Unit.Inch(0.1000000610947609D));
            this.table3.Name = "table3";
            tableGroup17.Name = "group11";
            tableGroup16.ChildGroups.Add(tableGroup17);
            tableGroup16.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup16.Name = "detailTableGroup2";
            this.table3.RowGroups.Add(tableGroup16);
            this.table3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(5.3041281700134277D), Telerik.Reporting.Drawing.Unit.Inch(0.35499989986419678D));
            // 
            // textBox13
            // 
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9013861417770386D), Telerik.Reporting.Drawing.Unit.Inch(0.18833325803279877D));
            this.textBox13.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox13.Value = "=Fields.Name";
            // 
            // textBox14
            // 
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5694091320037842D), Telerik.Reporting.Drawing.Unit.Inch(0.18833325803279877D));
            this.textBox14.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox14.Value = "=Fields.LicenseNumber";
            // 
            // textBox33
            // 
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9013861417770386D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666567325592D));
            this.textBox33.Style.Font.Bold = true;
            this.textBox33.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox33.Style.Font.Underline = true;
            this.textBox33.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox33.Value = "Name/Company";
            // 
            // textBox34
            // 
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5694091320037842D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666567325592D));
            this.textBox34.Style.Font.Bold = true;
            this.textBox34.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox34.Style.Font.Underline = true;
            this.textBox34.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox34.Value = "License No.";
            // 
            // textBox35
            // 
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8333333730697632D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666567325592D));
            this.textBox35.Style.Font.Bold = true;
            this.textBox35.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox35.Style.Font.Underline = true;
            this.textBox35.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox35.StyleName = "";
            this.textBox35.Value = "Certified Applicator";
            // 
            // textBox38
            // 
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8333333730697632D), Telerik.Reporting.Drawing.Unit.Inch(0.18833325803279877D));
            this.textBox38.Style.Font.Italic = false;
            this.textBox38.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox38.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox38.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox38.StyleName = "";
            this.textBox38.Value = "=Fields.Company";
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1999212503433228D), Telerik.Reporting.Drawing.Unit.Inch(0.5D));
            this.textBox1.Style.BackgroundColor = System.Drawing.Color.Silver;
            this.textBox1.Style.Color = System.Drawing.Color.Black;
            this.textBox1.Style.Font.Bold = true;
            this.textBox1.Style.Font.Name = "Segoe UI";
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox1.Value = "Applicator";
            // 
            // DicambaForm
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pageHeaderSection1,
            this.detail,
            this.pageFooterSection1});
            this.Name = "DicambaForm";
            this.PageSettings.ContinuousPaper = false;
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(8D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PageFooterSection pageFooterSection1;
        private Telerik.Reporting.TextBox txtDataSourceLabel;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox93;
        private Telerik.Reporting.TextBox textBox100;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.Panel panel1;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.Panel panel2;
        private Telerik.Reporting.Panel panel3;
        private Telerik.Reporting.Panel panel5;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.Table table1;
        private Telerik.Reporting.TextBox textBox26;
        private Telerik.Reporting.TextBox textBox28;
        private Telerik.Reporting.TextBox textBox30;
        private Telerik.Reporting.TextBox textBox32;
        private Telerik.Reporting.TextBox textBox25;
        private Telerik.Reporting.TextBox textBox27;
        private Telerik.Reporting.TextBox textBox29;
        private Telerik.Reporting.TextBox textBox31;
        private Telerik.Reporting.TextBox textBox40;
        private Telerik.Reporting.TextBox textBox39;
        private Telerik.Reporting.TextBox textBox43;
        private Telerik.Reporting.Shape shape1;
        private Telerik.Reporting.TextBox textBox44;
        private Telerik.Reporting.TextBox textBox45;
        private Telerik.Reporting.TextBox textBox62;
        private Telerik.Reporting.TextBox textBox51;
        private Telerik.Reporting.Panel panel7;
        private Telerik.Reporting.TextBox textBox55;
        private Telerik.Reporting.TextBox textBox57;
        private Telerik.Reporting.TextBox textBox59;
        private Telerik.Reporting.TextBox textBox50;
        private Telerik.Reporting.TextBox textBox52;
        private Telerik.Reporting.TextBox textBox47;
        private Telerik.Reporting.TextBox textBox54;
        private Telerik.Reporting.TextBox textBox46;
        private Telerik.Reporting.TextBox textBox48;
        private Telerik.Reporting.Panel panel6;
        private Telerik.Reporting.TextBox textBox49;
        private Telerik.Reporting.TextBox textBox56;
        private Telerik.Reporting.TextBox textBox58;
        private Telerik.Reporting.TextBox textBox60;
        private Telerik.Reporting.TextBox textBox61;
        private Telerik.Reporting.TextBox textBox53;
        private Telerik.Reporting.TextBox textBox63;
        private Telerik.Reporting.TextBox textBox64;
        private Telerik.Reporting.TextBox textBox65;
        private Telerik.Reporting.Panel panel8;
        private Telerik.Reporting.TextBox textBox66;
        private Telerik.Reporting.TextBox textBox67;
        private Telerik.Reporting.TextBox textBox69;
        private Telerik.Reporting.TextBox textBox70;
        private Telerik.Reporting.TextBox textBox71;
        private Telerik.Reporting.TextBox textBox73;
        private Telerik.Reporting.TextBox textBox74;
        private Telerik.Reporting.TextBox textBox68;
        private Telerik.Reporting.TextBox textBox72;
        private Telerik.Reporting.TextBox textBox77;
        private Telerik.Reporting.TextBox textBox76;
        private Telerik.Reporting.TextBox textBox75;
        private Telerik.Reporting.Panel panel9;
        private Telerik.Reporting.Table table4;
        private Telerik.Reporting.TextBox textBox91;
        private Telerik.Reporting.TextBox textBox92;
        private Telerik.Reporting.TextBox textBox94;
        private Telerik.Reporting.TextBox textBox95;
        private Telerik.Reporting.TextBox textBox96;
        private Telerik.Reporting.TextBox textBox97;
        private Telerik.Reporting.TextBox textBox98;
        private Telerik.Reporting.TextBox textBox99;
        private Telerik.Reporting.TextBox textBox78;
        private Telerik.Reporting.TextBox textBox79;
        private Telerik.Reporting.TextBox textBox80;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.Table table3;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox38;
        private Telerik.Reporting.TextBox textBox35;
        private Telerik.Reporting.TextBox textBox33;
        private Telerik.Reporting.TextBox textBox34;
        private Telerik.Reporting.TextBox textBox1;
    }
}