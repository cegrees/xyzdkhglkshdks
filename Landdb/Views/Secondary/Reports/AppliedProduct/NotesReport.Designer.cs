namespace Landdb.Views.Secondary.Reports.AppliedProduct
{
    partial class NotesReport
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			Telerik.Reporting.Group group1 = new Telerik.Reporting.Group();
			Telerik.Reporting.Group group2 = new Telerik.Reporting.Group();
			Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
			this.groupFooterSection = new Telerik.Reporting.GroupFooterSection();
			this.groupHeaderSection = new Telerik.Reporting.GroupHeaderSection();
			this.textBox5 = new Telerik.Reporting.TextBox();
			this.groupFooterSection1 = new Telerik.Reporting.GroupFooterSection();
			this.groupHeaderSection1 = new Telerik.Reporting.GroupHeaderSection();
			this.textBox11 = new Telerik.Reporting.TextBox();
			this.textBox10 = new Telerik.Reporting.TextBox();
			this.textBox9 = new Telerik.Reporting.TextBox();
			this.textBox8 = new Telerik.Reporting.TextBox();
			this.textBox6 = new Telerik.Reporting.TextBox();
			this.textBox3 = new Telerik.Reporting.TextBox();
			this.textBox7 = new Telerik.Reporting.TextBox();
			this.textBox12 = new Telerik.Reporting.TextBox();
			this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
			this.textBox15 = new Telerik.Reporting.TextBox();
			this.txtDataSourceLabel = new Telerik.Reporting.TextBox();
			this.textBox100 = new Telerik.Reporting.TextBox();
			this.textBox1 = new Telerik.Reporting.TextBox();
			this.detail = new Telerik.Reporting.DetailSection();
			this.textBox13 = new Telerik.Reporting.TextBox();
			this.textBox14 = new Telerik.Reporting.TextBox();
			this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
			this.textBox99 = new Telerik.Reporting.TextBox();
			this.pageInfoTextBox = new Telerik.Reporting.TextBox();
			this.textBox65 = new Telerik.Reporting.TextBox();
			this.ApplicationNotesData = new Telerik.Reporting.ObjectDataSource();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// groupFooterSection
			// 
			this.groupFooterSection.Height = Telerik.Reporting.Drawing.Unit.Inch(0.0416666679084301D);
			this.groupFooterSection.Name = "groupFooterSection";
			// 
			// groupHeaderSection
			// 
			this.groupHeaderSection.Height = Telerik.Reporting.Drawing.Unit.Inch(0.30000001192092896D);
			this.groupHeaderSection.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox5});
			this.groupHeaderSection.Name = "groupHeaderSection";
			// 
			// textBox5
			// 
			this.textBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D));
			this.textBox5.Name = "textBox5";
			this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.9499611854553223D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
			this.textBox5.Style.BackgroundColor = System.Drawing.Color.Gainsboro;
			this.textBox5.Style.Color = System.Drawing.Color.Black;
			this.textBox5.Style.Font.Bold = true;
			this.textBox5.Style.Font.Name = "Segoe UI";
			this.textBox5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox5.Value = "=Fields.FarmName";
			// 
			// groupFooterSection1
			// 
			this.groupFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.0416666679084301D);
			this.groupFooterSection1.Name = "groupFooterSection1";
			// 
			// groupHeaderSection1
			// 
			this.groupHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.800000011920929D);
			this.groupHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox11,
            this.textBox10,
            this.textBox9,
            this.textBox8,
            this.textBox6,
            this.textBox3,
            this.textBox7,
            this.textBox12});
			this.groupHeaderSection1.Name = "groupHeaderSection1";
			// 
			// textBox11
			// 
			this.textBox11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.4709115028381348D), Telerik.Reporting.Drawing.Unit.Inch(0.099921159446239471D));
			this.textBox11.Name = "textBox11";
			this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4790496826171875D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
			this.textBox11.Style.BackgroundColor = System.Drawing.Color.Transparent;
			this.textBox11.Style.Color = System.Drawing.Color.Gray;
			this.textBox11.Style.Font.Bold = true;
			this.textBox11.Style.Font.Name = "Segoe UI";
			this.textBox11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox11.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox11.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox11.Value = "AREA";
			// 
			// textBox10
			// 
			this.textBox10.Format = "{0:N2}";
			this.textBox10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.4709115028381348D), Telerik.Reporting.Drawing.Unit.Inch(0.29992115497589111D));
			this.textBox10.Name = "textBox10";
			this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.479049563407898D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
			this.textBox10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox10.Value = "=Fields.Area";
			// 
			// textBox9
			// 
			this.textBox9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.0209901332855225D), Telerik.Reporting.Drawing.Unit.Inch(0.29992115497589111D));
			this.textBox9.Name = "textBox9";
			this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.4579803943634033D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
			this.textBox9.Value = "=Fields.CropZoneName";
			// 
			// textBox8
			// 
			this.textBox8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.0209901332855225D), Telerik.Reporting.Drawing.Unit.Inch(0.099921159446239471D));
			this.textBox8.Name = "textBox8";
			this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.4579803943634033D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
			this.textBox8.Style.BackgroundColor = System.Drawing.Color.Transparent;
			this.textBox8.Style.Color = System.Drawing.Color.Gray;
			this.textBox8.Style.Font.Bold = true;
			this.textBox8.Style.Font.Name = "Segoe UI";
			this.textBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox8.Value = "CROP ZONE";
			// 
			// textBox6
			// 
			this.textBox6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.020911216735839844D), Telerik.Reporting.Drawing.Unit.Inch(0.099921159446239471D));
			this.textBox6.Name = "textBox6";
			this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
			this.textBox6.Style.BackgroundColor = System.Drawing.Color.Transparent;
			this.textBox6.Style.Color = System.Drawing.Color.Gray;
			this.textBox6.Style.Font.Bold = true;
			this.textBox6.Style.Font.Name = "Segoe UI";
			this.textBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox6.Value = "FIELD";
			// 
			// textBox3
			// 
			this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.020911216735839844D), Telerik.Reporting.Drawing.Unit.Inch(0.29992115497589111D));
			this.textBox3.Name = "textBox3";
			this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
			this.textBox3.Value = "=Fields.FieldName";
			// 
			// textBox7
			// 
			this.textBox7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.020911216735839844D), Telerik.Reporting.Drawing.Unit.Inch(0.5999605655670166D));
			this.textBox7.Name = "textBox7";
			this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7999999523162842D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
			this.textBox7.Style.Color = System.Drawing.Color.Black;
			this.textBox7.Style.Font.Name = "Segoe UI";
			this.textBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox7.Value = "DATE";
			// 
			// textBox12
			// 
			this.textBox12.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.8209900856018066D), Telerik.Reporting.Drawing.Unit.Inch(0.5999605655670166D));
			this.textBox12.Name = "textBox12";
			this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.1289710998535156D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
			this.textBox12.Style.Color = System.Drawing.Color.Black;
			this.textBox12.Style.Font.Name = "Segoe UI";
			this.textBox12.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox12.Value = "NOTES";
			// 
			// pageHeaderSection1
			// 
			this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.80000007152557373D);
			this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox15,
            this.txtDataSourceLabel,
            this.textBox100,
            this.textBox1});
			this.pageHeaderSection1.Name = "pageHeaderSection1";
			// 
			// textBox15
			// 
			this.textBox15.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.50007873773574829D));
			this.textBox15.Name = "textBox15";
			this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9998817443847656D), Telerik.Reporting.Drawing.Unit.Inch(0.19996063411235809D));
			this.textBox15.Style.Color = System.Drawing.Color.DimGray;
			this.textBox15.Style.Font.Name = "Segoe UI";
			this.textBox15.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox15.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox15.TextWrap = false;
			this.textBox15.Value = "=Fields.DateRangeDisplay";
			// 
			// txtDataSourceLabel
			// 
			this.txtDataSourceLabel.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
			this.txtDataSourceLabel.Name = "txtDataSourceLabel";
			this.txtDataSourceLabel.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.0312089920043945D), Telerik.Reporting.Drawing.Unit.Inch(0.50000005960464478D));
			this.txtDataSourceLabel.Style.Font.Name = "Segoe UI Light";
			this.txtDataSourceLabel.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(24D);
			this.txtDataSourceLabel.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.txtDataSourceLabel.Value = "=Fields.DataSourceName";
			// 
			// textBox100
			// 
			this.textBox100.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.0001969337463379D), Telerik.Reporting.Drawing.Unit.Inch(0.5001181960105896D));
			this.textBox100.Name = "textBox100";
			this.textBox100.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9998817443847656D), Telerik.Reporting.Drawing.Unit.Inch(0.19996063411235809D));
			this.textBox100.Style.Color = System.Drawing.Color.DimGray;
			this.textBox100.Style.Font.Name = "Segoe UI";
			this.textBox100.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox100.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox100.TextWrap = false;
			this.textBox100.Value = "=Fields.CropYear";
			// 
			// textBox1
			// 
			this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.5312886238098145D), Telerik.Reporting.Drawing.Unit.Inch(0.20003946125507355D));
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.4687116146087646D), Telerik.Reporting.Drawing.Unit.Inch(0.29996061325073242D));
			this.textBox1.Style.Color = System.Drawing.Color.DimGray;
			this.textBox1.Style.Font.Name = "Segoe UI Light";
			this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(18D);
			this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox1.TextWrap = false;
			this.textBox1.Value = "Application Notes Report";
			// 
			// detail
			// 
			this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(0.20003941655158997D);
			this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox13,
            this.textBox14});
			this.detail.Name = "detail";
			this.detail.Style.BorderColor.Bottom = System.Drawing.Color.Gainsboro;
			this.detail.Style.BorderColor.Top = System.Drawing.Color.Gainsboro;
			this.detail.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.detail.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
			// 
			// textBox13
			// 
			this.textBox13.Format = "{0:d}";
			this.textBox13.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.020911216735839844D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
			this.textBox13.Name = "textBox13";
			this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7999999523162842D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
			this.textBox13.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox13.Value = "=Fields.StartDate";
			// 
			// textBox14
			// 
			this.textBox14.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.8209900856018066D), Telerik.Reporting.Drawing.Unit.Inch(0D));
			this.textBox14.Name = "textBox14";
			this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.1289710998535156D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
			this.textBox14.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox14.Value = "=Fields.Notes";
			// 
			// pageFooterSection1
			// 
			this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.41662725806236267D);
			this.pageFooterSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox99,
            this.pageInfoTextBox,
            this.textBox65});
			this.pageFooterSection1.Name = "pageFooterSection1";
			// 
			// textBox99
			// 
			this.textBox99.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.02091115340590477D), Telerik.Reporting.Drawing.Unit.Inch(0.11662724614143372D));
			this.textBox99.Name = "textBox99";
			this.textBox99.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8790887594223023D), Telerik.Reporting.Drawing.Unit.Inch(0.29996046423912048D));
			this.textBox99.Style.Color = System.Drawing.Color.DimGray;
			this.textBox99.Style.Font.Bold = true;
			this.textBox99.Style.Font.Italic = false;
			this.textBox99.Style.Font.Name = "Segoe UI Light";
			this.textBox99.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(16D);
			this.textBox99.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox99.Value = "AG CONNECTIONS";
			// 
			// pageInfoTextBox
			// 
			this.pageInfoTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.6999993324279785D), Telerik.Reporting.Drawing.Unit.Inch(0.11662724614143372D));
			this.pageInfoTextBox.Name = "pageInfoTextBox";
			this.pageInfoTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.2999222278594971D), Telerik.Reporting.Drawing.Unit.Inch(0.29999962449073792D));
			this.pageInfoTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.pageInfoTextBox.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.pageInfoTextBox.StyleName = "PageInfo";
			this.pageInfoTextBox.Value = "=PageNumber";
			// 
			// textBox65
			// 
			this.textBox65.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.1000001430511475D), Telerik.Reporting.Drawing.Unit.Inch(0.11662724614143372D));
			this.textBox65.Name = "textBox65";
			this.textBox65.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0937893390655518D), Telerik.Reporting.Drawing.Unit.Inch(0.29996046423912048D));
			this.textBox65.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox65.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox65.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox65.Value = "generated on {Now()}";
			// 
			// ApplicationNotesData
			// 
			this.ApplicationNotesData.DataSource = typeof(Landdb.ReportModels.AppliedProduct.ApplicationNotesData);
			this.ApplicationNotesData.Name = "ApplicationNotesData";
			// 
			// NotesReport
			// 
			this.DataSource = this.ApplicationNotesData;
			group1.GroupFooter = this.groupFooterSection;
			group1.GroupHeader = this.groupHeaderSection;
			group1.Groupings.Add(new Telerik.Reporting.Grouping("=Fields.FarmName"));
			group1.Name = "group";
			group2.GroupFooter = this.groupFooterSection1;
			group2.GroupHeader = this.groupHeaderSection1;
			group2.Groupings.Add(new Telerik.Reporting.Grouping("=Fields.FieldName"));
			group2.Name = "group1";
			this.Groups.AddRange(new Telerik.Reporting.Group[] {
            group1,
            group2});
			this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.groupHeaderSection,
            this.groupFooterSection,
            this.groupHeaderSection1,
            this.groupFooterSection1,
            this.pageHeaderSection1,
            this.detail,
            this.pageFooterSection1});
			this.Name = "NotesReport";
			this.PageSettings.Landscape = false;
			this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(0.25D), Telerik.Reporting.Drawing.Unit.Inch(0.25D), Telerik.Reporting.Drawing.Unit.Inch(0.25D), Telerik.Reporting.Drawing.Unit.Inch(0.25D));
			this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
			styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
			styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
			this.Width = Telerik.Reporting.Drawing.Unit.Inch(8.0000791549682617D);
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PageFooterSection pageFooterSection1;
        private Telerik.Reporting.GroupHeaderSection groupHeaderSection;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.GroupFooterSection groupFooterSection;
        private Telerik.Reporting.GroupHeaderSection groupHeaderSection1;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.GroupFooterSection groupFooterSection1;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox99;
        private Telerik.Reporting.TextBox pageInfoTextBox;
        private Telerik.Reporting.TextBox textBox65;
		private Telerik.Reporting.TextBox textBox15;
		private Telerik.Reporting.TextBox txtDataSourceLabel;
		private Telerik.Reporting.TextBox textBox100;
		private Telerik.Reporting.TextBox textBox1;
		private Telerik.Reporting.ObjectDataSource ApplicationNotesData;
	}
}