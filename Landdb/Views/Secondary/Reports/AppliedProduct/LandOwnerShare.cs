namespace Landdb.Views.Secondary.Reports.AppliedProduct {
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;

    /// <summary>
    /// Summary description for AppliedByFieldDetailReport.
    /// </summary>
    public partial class LandOwnerShare : Telerik.Reporting.Report {
        public LandOwnerShare() {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();
        }
    }
}