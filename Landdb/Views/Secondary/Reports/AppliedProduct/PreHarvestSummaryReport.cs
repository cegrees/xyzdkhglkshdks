namespace Landdb.Views.Secondary.Reports.AppliedProduct
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;

    /// <summary>
    /// Summary description for PreHarvestSummaryReport.
    /// </summary>
    public partial class PreHarvestSummaryReport : Telerik.Reporting.Report
    {
        public PreHarvestSummaryReport()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }
    }
}