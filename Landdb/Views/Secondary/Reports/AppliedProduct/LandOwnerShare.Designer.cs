namespace Landdb.Views.Secondary.Reports.AppliedProduct {
    partial class LandOwnerShare {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
			Telerik.Reporting.Drawing.FormattingRule formattingRule1 = new Telerik.Reporting.Drawing.FormattingRule();
			Telerik.Reporting.Drawing.FormattingRule formattingRule2 = new Telerik.Reporting.Drawing.FormattingRule();
			Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.Drawing.FormattingRule formattingRule3 = new Telerik.Reporting.Drawing.FormattingRule();
			Telerik.Reporting.Drawing.FormattingRule formattingRule4 = new Telerik.Reporting.Drawing.FormattingRule();
			Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup17 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup18 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup19 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup20 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.Drawing.FormattingRule formattingRule5 = new Telerik.Reporting.Drawing.FormattingRule();
			Telerik.Reporting.Drawing.FormattingRule formattingRule6 = new Telerik.Reporting.Drawing.FormattingRule();
			Telerik.Reporting.TableGroup tableGroup21 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup22 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup23 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup24 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup25 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup26 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup27 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup28 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup29 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup30 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.Drawing.FormattingRule formattingRule7 = new Telerik.Reporting.Drawing.FormattingRule();
			Telerik.Reporting.TableGroup tableGroup31 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup32 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup33 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup34 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup35 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup36 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup37 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup38 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup39 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup40 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.Drawing.FormattingRule formattingRule8 = new Telerik.Reporting.Drawing.FormattingRule();
			Telerik.Reporting.Drawing.FormattingRule formattingRule9 = new Telerik.Reporting.Drawing.FormattingRule();
			Telerik.Reporting.Drawing.FormattingRule formattingRule10 = new Telerik.Reporting.Drawing.FormattingRule();
			Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
			this.textBox5 = new Telerik.Reporting.TextBox();
			this.textBox11 = new Telerik.Reporting.TextBox();
			this.textBox6 = new Telerik.Reporting.TextBox();
			this.textBox7 = new Telerik.Reporting.TextBox();
			this.textBox13 = new Telerik.Reporting.TextBox();
			this.textBox15 = new Telerik.Reporting.TextBox();
			this.textBox17 = new Telerik.Reporting.TextBox();
			this.textBox100 = new Telerik.Reporting.TextBox();
			this.textBox20 = new Telerik.Reporting.TextBox();
			this.textBox28 = new Telerik.Reporting.TextBox();
			this.textBox29 = new Telerik.Reporting.TextBox();
			this.textBox30 = new Telerik.Reporting.TextBox();
			this.textBox31 = new Telerik.Reporting.TextBox();
			this.textBox32 = new Telerik.Reporting.TextBox();
			this.textBox33 = new Telerik.Reporting.TextBox();
			this.textBox103 = new Telerik.Reporting.TextBox();
			this.textBox35 = new Telerik.Reporting.TextBox();
			this.textBox43 = new Telerik.Reporting.TextBox();
			this.textBox44 = new Telerik.Reporting.TextBox();
			this.textBox45 = new Telerik.Reporting.TextBox();
			this.textBox46 = new Telerik.Reporting.TextBox();
			this.textBox47 = new Telerik.Reporting.TextBox();
			this.textBox48 = new Telerik.Reporting.TextBox();
			this.textBox106 = new Telerik.Reporting.TextBox();
			this.textBox50 = new Telerik.Reporting.TextBox();
			this.textBox58 = new Telerik.Reporting.TextBox();
			this.textBox59 = new Telerik.Reporting.TextBox();
			this.textBox60 = new Telerik.Reporting.TextBox();
			this.textBox61 = new Telerik.Reporting.TextBox();
			this.textBox62 = new Telerik.Reporting.TextBox();
			this.textBox63 = new Telerik.Reporting.TextBox();
			this.textBox109 = new Telerik.Reporting.TextBox();
			this.detail = new Telerik.Reporting.DetailSection();
			this.textBox19 = new Telerik.Reporting.TextBox();
			this.table1 = new Telerik.Reporting.Table();
			this.textBox8 = new Telerik.Reporting.TextBox();
			this.textBox9 = new Telerik.Reporting.TextBox();
			this.textBox10 = new Telerik.Reporting.TextBox();
			this.textBox12 = new Telerik.Reporting.TextBox();
			this.textBox14 = new Telerik.Reporting.TextBox();
			this.textBox16 = new Telerik.Reporting.TextBox();
			this.textBox18 = new Telerik.Reporting.TextBox();
			this.textBox66 = new Telerik.Reporting.TextBox();
			this.textBox67 = new Telerik.Reporting.TextBox();
			this.textBox68 = new Telerik.Reporting.TextBox();
			this.textBox69 = new Telerik.Reporting.TextBox();
			this.textBox71 = new Telerik.Reporting.TextBox();
			this.textBox72 = new Telerik.Reporting.TextBox();
			this.textBox95 = new Telerik.Reporting.TextBox();
			this.textBox101 = new Telerik.Reporting.TextBox();
			this.textBox102 = new Telerik.Reporting.TextBox();
			this.table2 = new Telerik.Reporting.Table();
			this.textBox21 = new Telerik.Reporting.TextBox();
			this.textBox22 = new Telerik.Reporting.TextBox();
			this.textBox23 = new Telerik.Reporting.TextBox();
			this.textBox24 = new Telerik.Reporting.TextBox();
			this.textBox25 = new Telerik.Reporting.TextBox();
			this.textBox26 = new Telerik.Reporting.TextBox();
			this.textBox27 = new Telerik.Reporting.TextBox();
			this.textBox73 = new Telerik.Reporting.TextBox();
			this.textBox74 = new Telerik.Reporting.TextBox();
			this.textBox75 = new Telerik.Reporting.TextBox();
			this.textBox80 = new Telerik.Reporting.TextBox();
			this.textBox78 = new Telerik.Reporting.TextBox();
			this.textBox70 = new Telerik.Reporting.TextBox();
			this.textBox90 = new Telerik.Reporting.TextBox();
			this.textBox104 = new Telerik.Reporting.TextBox();
			this.textBox105 = new Telerik.Reporting.TextBox();
			this.textBox34 = new Telerik.Reporting.TextBox();
			this.table3 = new Telerik.Reporting.Table();
			this.textBox36 = new Telerik.Reporting.TextBox();
			this.textBox37 = new Telerik.Reporting.TextBox();
			this.textBox38 = new Telerik.Reporting.TextBox();
			this.textBox39 = new Telerik.Reporting.TextBox();
			this.textBox40 = new Telerik.Reporting.TextBox();
			this.textBox41 = new Telerik.Reporting.TextBox();
			this.textBox42 = new Telerik.Reporting.TextBox();
			this.textBox79 = new Telerik.Reporting.TextBox();
			this.textBox81 = new Telerik.Reporting.TextBox();
			this.textBox82 = new Telerik.Reporting.TextBox();
			this.textBox87 = new Telerik.Reporting.TextBox();
			this.textBox85 = new Telerik.Reporting.TextBox();
			this.textBox76 = new Telerik.Reporting.TextBox();
			this.textBox77 = new Telerik.Reporting.TextBox();
			this.textBox107 = new Telerik.Reporting.TextBox();
			this.textBox108 = new Telerik.Reporting.TextBox();
			this.textBox49 = new Telerik.Reporting.TextBox();
			this.textBox64 = new Telerik.Reporting.TextBox();
			this.table4 = new Telerik.Reporting.Table();
			this.textBox51 = new Telerik.Reporting.TextBox();
			this.textBox52 = new Telerik.Reporting.TextBox();
			this.textBox53 = new Telerik.Reporting.TextBox();
			this.textBox54 = new Telerik.Reporting.TextBox();
			this.textBox55 = new Telerik.Reporting.TextBox();
			this.textBox56 = new Telerik.Reporting.TextBox();
			this.textBox57 = new Telerik.Reporting.TextBox();
			this.textBox86 = new Telerik.Reporting.TextBox();
			this.textBox88 = new Telerik.Reporting.TextBox();
			this.textBox89 = new Telerik.Reporting.TextBox();
			this.textBox94 = new Telerik.Reporting.TextBox();
			this.textBox92 = new Telerik.Reporting.TextBox();
			this.textBox83 = new Telerik.Reporting.TextBox();
			this.textBox84 = new Telerik.Reporting.TextBox();
			this.textBox110 = new Telerik.Reporting.TextBox();
			this.textBox111 = new Telerik.Reporting.TextBox();
			this.textBox91 = new Telerik.Reporting.TextBox();
			this.textBox96 = new Telerik.Reporting.TextBox();
			this.textBox97 = new Telerik.Reporting.TextBox();
			this.textBox98 = new Telerik.Reporting.TextBox();
			this.textBox112 = new Telerik.Reporting.TextBox();
			this.reportDataSource = new Telerik.Reporting.ObjectDataSource();
			this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
			this.textBox65 = new Telerik.Reporting.TextBox();
			this.pageInfoTextBox = new Telerik.Reporting.TextBox();
			this.textBox99 = new Telerik.Reporting.TextBox();
			this.reportHeaderSection1 = new Telerik.Reporting.ReportHeaderSection();
			this.textBox2 = new Telerik.Reporting.TextBox();
			this.textBox1 = new Telerik.Reporting.TextBox();
			this.textBox3 = new Telerik.Reporting.TextBox();
			this.txtDataSourceLabel = new Telerik.Reporting.TextBox();
			this.textBox113 = new Telerik.Reporting.TextBox();
			this.textBox93 = new Telerik.Reporting.TextBox();
			this.textBox4 = new Telerik.Reporting.TextBox();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// textBox5
			// 
			this.textBox5.Name = "textBox5";
			this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.762500524520874D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666716337204D));
			this.textBox5.Style.Color = System.Drawing.Color.Gray;
			this.textBox5.Style.Font.Bold = true;
			this.textBox5.Style.Font.Name = "Segoe UI";
			this.textBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox5.Value = "DATE";
			// 
			// textBox11
			// 
			this.textBox11.Name = "textBox11";
			this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1979166269302368D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666716337204D));
			this.textBox11.Style.Color = System.Drawing.Color.Gray;
			this.textBox11.Style.Font.Bold = true;
			this.textBox11.Style.Font.Name = "Segoe UI";
			this.textBox11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox11.StyleName = "";
			this.textBox11.Value = "APPLICATION CO";
			// 
			// textBox6
			// 
			this.textBox6.Name = "textBox6";
			this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.970833420753479D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666716337204D));
			this.textBox6.Style.Color = System.Drawing.Color.Gray;
			this.textBox6.Style.Font.Bold = true;
			this.textBox6.Style.Font.Name = "Segoe UI";
			this.textBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox6.Value = "PRODUCT";
			// 
			// textBox7
			// 
			this.textBox7.Name = "textBox7";
			this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.887499988079071D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666716337204D));
			this.textBox7.Style.Color = System.Drawing.Color.Gray;
			this.textBox7.Style.Font.Bold = true;
			this.textBox7.Style.Font.Name = "Segoe UI";
			this.textBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox7.Value = "RATE";
			// 
			// textBox13
			// 
			this.textBox13.Name = "textBox13";
			this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.87499970197677612D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666716337204D));
			this.textBox13.Style.Color = System.Drawing.Color.Gray;
			this.textBox13.Style.Font.Bold = true;
			this.textBox13.Style.Font.Name = "Segoe UI";
			this.textBox13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox13.StyleName = "";
			this.textBox13.Value = "AREA";
			// 
			// textBox15
			// 
			this.textBox15.Name = "textBox15";
			this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.80902719497680664D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666716337204D));
			this.textBox15.Style.Color = System.Drawing.Color.Gray;
			this.textBox15.Style.Font.Bold = true;
			this.textBox15.Style.Font.Name = "Segoe UI";
			this.textBox15.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox15.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox15.StyleName = "";
			this.textBox15.Value = "TOTAL";
			// 
			// textBox17
			// 
			formattingRule1.Filters.Add(new Telerik.Reporting.Filter("=Fields.IncludeCost", Telerik.Reporting.FilterOperator.Equal, "false"));
			formattingRule1.Style.Visible = false;
			this.textBox17.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule1});
			this.textBox17.Name = "textBox17";
			this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.87499958276748657D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666716337204D));
			this.textBox17.Style.Color = System.Drawing.Color.Gray;
			this.textBox17.Style.Font.Bold = true;
			this.textBox17.Style.Font.Name = "Segoe UI";
			this.textBox17.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox17.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox17.StyleName = "";
			this.textBox17.Value = "COST";
			// 
			// textBox100
			// 
			this.textBox100.Name = "textBox100";
			this.textBox100.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.22916679084300995D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666716337204D));
			this.textBox100.Style.Color = System.Drawing.Color.Gray;
			this.textBox100.Style.Font.Bold = true;
			this.textBox100.Style.Font.Name = "Segoe UI";
			this.textBox100.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox100.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox100.StyleName = "";
			// 
			// textBox20
			// 
			this.textBox20.Name = "textBox20";
			this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.76250040531158447D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666716337204D));
			this.textBox20.Style.Color = System.Drawing.Color.Gray;
			this.textBox20.Style.Font.Bold = true;
			this.textBox20.Style.Font.Name = "Segoe UI";
			this.textBox20.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox20.Value = "DATE";
			// 
			// textBox28
			// 
			this.textBox28.Name = "textBox28";
			this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2083337306976318D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666716337204D));
			this.textBox28.Style.Color = System.Drawing.Color.Gray;
			this.textBox28.Style.Font.Bold = true;
			this.textBox28.Style.Font.Name = "Segoe UI";
			this.textBox28.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox28.StyleName = "";
			this.textBox28.Value = "APPLICATION CO";
			// 
			// textBox29
			// 
			this.textBox29.Name = "textBox29";
			this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9916677474975586D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666716337204D));
			this.textBox29.Style.Color = System.Drawing.Color.Gray;
			this.textBox29.Style.Font.Bold = true;
			this.textBox29.Style.Font.Name = "Segoe UI";
			this.textBox29.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox29.Value = "PRODUCT";
			// 
			// textBox30
			// 
			this.textBox30.Name = "textBox30";
			this.textBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.88750046491622925D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666716337204D));
			this.textBox30.Style.Color = System.Drawing.Color.Gray;
			this.textBox30.Style.Font.Bold = true;
			this.textBox30.Style.Font.Name = "Segoe UI";
			this.textBox30.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox30.Value = "RATE";
			// 
			// textBox31
			// 
			this.textBox31.Name = "textBox31";
			this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.87499988079071045D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666716337204D));
			this.textBox31.Style.Color = System.Drawing.Color.Gray;
			this.textBox31.Style.Font.Bold = true;
			this.textBox31.Style.Font.Name = "Segoe UI";
			this.textBox31.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox31.StyleName = "";
			this.textBox31.Value = "AREA";
			// 
			// textBox32
			// 
			this.textBox32.Name = "textBox32";
			this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.77430564165115356D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666716337204D));
			this.textBox32.Style.Color = System.Drawing.Color.Gray;
			this.textBox32.Style.Font.Bold = true;
			this.textBox32.Style.Font.Name = "Segoe UI";
			this.textBox32.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox32.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox32.StyleName = "";
			this.textBox32.Value = "TOTAL";
			// 
			// textBox33
			// 
			formattingRule2.Filters.Add(new Telerik.Reporting.Filter("=Fields.IncludeCost", Telerik.Reporting.FilterOperator.Equal, "false"));
			formattingRule2.Style.Visible = false;
			this.textBox33.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule2});
			this.textBox33.Name = "textBox33";
			this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.88541620969772339D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666716337204D));
			this.textBox33.Style.Color = System.Drawing.Color.Gray;
			this.textBox33.Style.Font.Bold = true;
			this.textBox33.Style.Font.Name = "Segoe UI";
			this.textBox33.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox33.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox33.StyleName = "";
			this.textBox33.Value = "COST";
			// 
			// textBox103
			// 
			this.textBox103.Name = "textBox103";
			this.textBox103.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.23958340287208557D), Telerik.Reporting.Drawing.Unit.Inch(0.166666641831398D));
			this.textBox103.Style.Color = System.Drawing.Color.Gray;
			this.textBox103.Style.Font.Bold = true;
			this.textBox103.Style.Font.Name = "Segoe UI";
			this.textBox103.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox103.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox103.StyleName = "";
			// 
			// textBox35
			// 
			this.textBox35.Name = "textBox35";
			this.textBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.76250016689300537D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666716337204D));
			this.textBox35.Style.Color = System.Drawing.Color.Gray;
			this.textBox35.Style.Font.Bold = true;
			this.textBox35.Style.Font.Name = "Segoe UI";
			this.textBox35.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox35.Value = "DATE";
			// 
			// textBox43
			// 
			this.textBox43.Name = "textBox43";
			this.textBox43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2083331346511841D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666716337204D));
			this.textBox43.Style.Color = System.Drawing.Color.Gray;
			this.textBox43.Style.Font.Bold = true;
			this.textBox43.Style.Font.Name = "Segoe UI";
			this.textBox43.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox43.StyleName = "";
			this.textBox43.Value = "APPLICATION CO";
			// 
			// textBox44
			// 
			this.textBox44.Name = "textBox44";
			this.textBox44.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0020835399627686D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666716337204D));
			this.textBox44.Style.Color = System.Drawing.Color.Gray;
			this.textBox44.Style.Font.Bold = true;
			this.textBox44.Style.Font.Name = "Segoe UI";
			this.textBox44.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox44.Value = "PRODUCT";
			// 
			// textBox45
			// 
			this.textBox45.Name = "textBox45";
			this.textBox45.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.88750004768371582D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666716337204D));
			this.textBox45.Style.Color = System.Drawing.Color.Gray;
			this.textBox45.Style.Font.Bold = true;
			this.textBox45.Style.Font.Name = "Segoe UI";
			this.textBox45.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox45.Value = "RATE";
			// 
			// textBox46
			// 
			this.textBox46.Name = "textBox46";
			this.textBox46.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.87499958276748657D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666716337204D));
			this.textBox46.Style.Color = System.Drawing.Color.Gray;
			this.textBox46.Style.Font.Bold = true;
			this.textBox46.Style.Font.Name = "Segoe UI";
			this.textBox46.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox46.StyleName = "";
			this.textBox46.Value = "AREA";
			// 
			// textBox47
			// 
			this.textBox47.Name = "textBox47";
			this.textBox47.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.73958349227905273D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666716337204D));
			this.textBox47.Style.Color = System.Drawing.Color.Gray;
			this.textBox47.Style.Font.Bold = true;
			this.textBox47.Style.Font.Name = "Segoe UI";
			this.textBox47.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox47.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox47.StyleName = "";
			this.textBox47.Value = "TOTAL";
			// 
			// textBox48
			// 
			this.textBox48.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule2});
			this.textBox48.Name = "textBox48";
			this.textBox48.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.90972161293029785D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666716337204D));
			this.textBox48.Style.Color = System.Drawing.Color.Gray;
			this.textBox48.Style.Font.Bold = true;
			this.textBox48.Style.Font.Name = "Segoe UI";
			this.textBox48.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox48.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox48.StyleName = "";
			this.textBox48.Value = "COST";
			// 
			// textBox106
			// 
			this.textBox106.Name = "textBox106";
			this.textBox106.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.25000008940696716D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666716337204D));
			this.textBox106.Style.Color = System.Drawing.Color.Gray;
			this.textBox106.Style.Font.Bold = true;
			this.textBox106.Style.Font.Name = "Segoe UI";
			this.textBox106.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox106.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox106.StyleName = "";
			// 
			// textBox50
			// 
			this.textBox50.Name = "textBox50";
			this.textBox50.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.76250004768371582D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666716337204D));
			this.textBox50.Style.Color = System.Drawing.Color.Gray;
			this.textBox50.Style.Font.Bold = true;
			this.textBox50.Style.Font.Name = "Segoe UI";
			this.textBox50.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox50.Value = "DATE";
			// 
			// textBox58
			// 
			this.textBox58.Name = "textBox58";
			this.textBox58.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2083330154418945D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666716337204D));
			this.textBox58.Style.Color = System.Drawing.Color.Gray;
			this.textBox58.Style.Font.Bold = true;
			this.textBox58.Style.Font.Name = "Segoe UI";
			this.textBox58.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox58.StyleName = "";
			this.textBox58.Value = "APPLICATION CO";
			// 
			// textBox59
			// 
			this.textBox59.Name = "textBox59";
			this.textBox59.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9916665554046631D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666716337204D));
			this.textBox59.Style.Color = System.Drawing.Color.Gray;
			this.textBox59.Style.Font.Bold = true;
			this.textBox59.Style.Font.Name = "Segoe UI";
			this.textBox59.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox59.Value = "PRODUCT";
			// 
			// textBox60
			// 
			this.textBox60.Name = "textBox60";
			this.textBox60.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.88749980926513672D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666716337204D));
			this.textBox60.Style.Color = System.Drawing.Color.Gray;
			this.textBox60.Style.Font.Bold = true;
			this.textBox60.Style.Font.Name = "Segoe UI";
			this.textBox60.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox60.Value = "RATE";
			// 
			// textBox61
			// 
			this.textBox61.Name = "textBox61";
			this.textBox61.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.87499970197677612D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666716337204D));
			this.textBox61.Style.Color = System.Drawing.Color.Gray;
			this.textBox61.Style.Font.Bold = true;
			this.textBox61.Style.Font.Name = "Segoe UI";
			this.textBox61.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox61.StyleName = "";
			this.textBox61.Value = "AREA";
			// 
			// textBox62
			// 
			this.textBox62.Name = "textBox62";
			this.textBox62.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.75347179174423218D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666716337204D));
			this.textBox62.Style.Color = System.Drawing.Color.Gray;
			this.textBox62.Style.Font.Bold = true;
			this.textBox62.Style.Font.Name = "Segoe UI";
			this.textBox62.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox62.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox62.StyleName = "";
			this.textBox62.Value = "TOTAL";
			// 
			// textBox63
			// 
			this.textBox63.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule2});
			this.textBox63.Name = "textBox63";
			this.textBox63.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.90624940395355225D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666716337204D));
			this.textBox63.Style.Color = System.Drawing.Color.Gray;
			this.textBox63.Style.Font.Bold = true;
			this.textBox63.Style.Font.Name = "Segoe UI";
			this.textBox63.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox63.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox63.StyleName = "";
			this.textBox63.Value = "COST";
			// 
			// textBox109
			// 
			this.textBox109.Name = "textBox109";
			this.textBox109.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.22916677594184876D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666716337204D));
			this.textBox109.Style.Color = System.Drawing.Color.Gray;
			this.textBox109.Style.Font.Bold = true;
			this.textBox109.Style.Font.Name = "Segoe UI";
			this.textBox109.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox109.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox109.StyleName = "";
			// 
			// detail
			// 
			this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(4.5D);
			this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox19,
            this.table1,
            this.table2,
            this.textBox34,
            this.table3,
            this.textBox49,
            this.textBox64,
            this.table4,
            this.textBox91,
            this.textBox96,
            this.textBox97,
            this.textBox98,
            this.textBox112});
			this.detail.KeepTogether = false;
			this.detail.Name = "detail";
			this.detail.Style.BackgroundColor = System.Drawing.Color.Empty;
			// 
			// textBox19
			// 
			this.textBox19.Bindings.Add(new Telerik.Reporting.Binding("Visible", "=Fields.CropProtectionLineItems.Count > 0"));
			this.textBox19.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D), Telerik.Reporting.Drawing.Unit.Inch(0D));
			this.textBox19.Name = "textBox19";
			this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0603783130645752D), Telerik.Reporting.Drawing.Unit.Inch(0.3999999463558197D));
			this.textBox19.Style.Color = System.Drawing.Color.Gray;
			this.textBox19.Style.Font.Name = "Segoe UI Light";
			this.textBox19.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(18D);
			this.textBox19.Value = "crop protection";
			// 
			// table1
			// 
			this.table1.Bindings.Add(new Telerik.Reporting.Binding("DataSource", "=Fields.CropProtectionLineItems"));
			this.table1.Bindings.Add(new Telerik.Reporting.Binding("Visible", "=Fields.CropProtectionLineItems.Count > 0"));
			this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.76250070333480835D)));
			this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.197914719581604D)));
			this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.9708327054977417D)));
			this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.88749957084655762D)));
			this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.87499982118606567D)));
			this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.80902743339538574D)));
			this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.87499982118606567D)));
			this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.22916677594184876D)));
			this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.20833340287208557D)));
			this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.20999999344348908D)));
			this.table1.Body.SetCellContent(0, 0, this.textBox8);
			this.table1.Body.SetCellContent(0, 2, this.textBox9);
			this.table1.Body.SetCellContent(0, 3, this.textBox10);
			this.table1.Body.SetCellContent(0, 1, this.textBox12);
			this.table1.Body.SetCellContent(0, 4, this.textBox14);
			this.table1.Body.SetCellContent(0, 5, this.textBox16);
			this.table1.Body.SetCellContent(0, 6, this.textBox18);
			this.table1.Body.SetCellContent(1, 0, this.textBox66);
			this.table1.Body.SetCellContent(1, 1, this.textBox67);
			this.table1.Body.SetCellContent(1, 2, this.textBox68);
			this.table1.Body.SetCellContent(1, 3, this.textBox69);
			this.table1.Body.SetCellContent(1, 5, this.textBox71);
			this.table1.Body.SetCellContent(1, 6, this.textBox72);
			this.table1.Body.SetCellContent(1, 4, this.textBox95);
			this.table1.Body.SetCellContent(0, 7, this.textBox101);
			this.table1.Body.SetCellContent(1, 7, this.textBox102);
			tableGroup1.ReportItem = this.textBox5;
			tableGroup2.Name = "Group1";
			tableGroup2.ReportItem = this.textBox11;
			tableGroup3.ReportItem = this.textBox6;
			tableGroup4.ReportItem = this.textBox7;
			tableGroup5.Name = "Group2";
			tableGroup5.ReportItem = this.textBox13;
			tableGroup6.Name = "Group3";
			tableGroup6.ReportItem = this.textBox15;
			tableGroup7.Name = "Group4";
			tableGroup7.ReportItem = this.textBox17;
			tableGroup8.Name = "group4";
			tableGroup8.ReportItem = this.textBox100;
			this.table1.ColumnGroups.Add(tableGroup1);
			this.table1.ColumnGroups.Add(tableGroup2);
			this.table1.ColumnGroups.Add(tableGroup3);
			this.table1.ColumnGroups.Add(tableGroup4);
			this.table1.ColumnGroups.Add(tableGroup5);
			this.table1.ColumnGroups.Add(tableGroup6);
			this.table1.ColumnGroups.Add(tableGroup7);
			this.table1.ColumnGroups.Add(tableGroup8);
			this.table1.DataSource = null;
			this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox8,
            this.textBox12,
            this.textBox9,
            this.textBox10,
            this.textBox14,
            this.textBox16,
            this.textBox18,
            this.textBox101,
            this.textBox66,
            this.textBox67,
            this.textBox68,
            this.textBox69,
            this.textBox95,
            this.textBox71,
            this.textBox72,
            this.textBox102,
            this.textBox5,
            this.textBox11,
            this.textBox6,
            this.textBox7,
            this.textBox13,
            this.textBox15,
            this.textBox17,
            this.textBox100});
			this.table1.KeepTogether = false;
			this.table1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.10000038146972656D), Telerik.Reporting.Drawing.Unit.Inch(0.40007877349853516D));
			this.table1.Name = "table1";
			tableGroup9.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup9.Name = "DetailGroup";
			tableGroup10.Name = "group";
			this.table1.RowGroups.Add(tableGroup9);
			this.table1.RowGroups.Add(tableGroup10);
			this.table1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6069416999816895D), Telerik.Reporting.Drawing.Unit.Inch(0.58500009775161743D));
			this.table1.Sortings.Add(new Telerik.Reporting.Sorting("=Fields.StartDateTime", Telerik.Reporting.SortDirection.Asc));
			// 
			// textBox8
			// 
			this.textBox8.Name = "textBox8";
			this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.76250040531158447D), Telerik.Reporting.Drawing.Unit.Inch(0.20833344757556915D));
			this.textBox8.Style.Font.Name = "Segoe UI";
			this.textBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox8.Value = "=Fields.StartDate";
			// 
			// textBox9
			// 
			this.textBox9.Name = "textBox9";
			this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9708333015441895D), Telerik.Reporting.Drawing.Unit.Inch(0.20833344757556915D));
			this.textBox9.Style.Font.Name = "Segoe UI";
			this.textBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox9.Value = "=Fields.ProductDisplay";
			// 
			// textBox10
			// 
			this.textBox10.Name = "textBox10";
			this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.88749974966049194D), Telerik.Reporting.Drawing.Unit.Inch(0.20833344757556915D));
			this.textBox10.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
			this.textBox10.Style.Font.Name = "Segoe UI";
			this.textBox10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox10.Value = "=Fields.RateDisplay";
			// 
			// textBox12
			// 
			this.textBox12.Name = "textBox12";
			this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1979165077209473D), Telerik.Reporting.Drawing.Unit.Inch(0.20833344757556915D));
			this.textBox12.Style.Font.Name = "Segoe UI";
			this.textBox12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox12.StyleName = "";
			this.textBox12.Value = "=Fields.ApplicatorDisplay";
			// 
			// textBox14
			// 
			this.textBox14.Name = "textBox14";
			this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.87499964237213135D), Telerik.Reporting.Drawing.Unit.Inch(0.20833344757556915D));
			this.textBox14.Style.Font.Name = "Segoe UI";
			this.textBox14.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox14.StyleName = "";
			this.textBox14.Value = "=Fields.AreaDisplay";
			// 
			// textBox16
			// 
			this.textBox16.Name = "textBox16";
			this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.80902719497680664D), Telerik.Reporting.Drawing.Unit.Inch(0.20833344757556915D));
			this.textBox16.Style.Font.Name = "Segoe UI";
			this.textBox16.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox16.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox16.StyleName = "";
			this.textBox16.Value = "=Fields.TotalProductDisplay";
			// 
			// textBox18
			// 
			this.textBox18.Format = "{0:C2}";
			this.textBox18.Name = "textBox18";
			this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.87499958276748657D), Telerik.Reporting.Drawing.Unit.Inch(0.20833344757556915D));
			this.textBox18.Style.Font.Name = "Segoe UI";
			this.textBox18.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox18.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox18.StyleName = "";
			this.textBox18.Value = "=Fields.TotalCost";
			// 
			// textBox66
			// 
			this.textBox66.Name = "textBox66";
			this.textBox66.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.76250046491622925D), Telerik.Reporting.Drawing.Unit.Inch(0.21000000834465027D));
			this.textBox66.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox66.Style.Font.Name = "Segoe UI";
			this.textBox66.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox66.StyleName = "";
			// 
			// textBox67
			// 
			this.textBox67.Name = "textBox67";
			this.textBox67.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1979159116744995D), Telerik.Reporting.Drawing.Unit.Inch(0.21000000834465027D));
			this.textBox67.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox67.Style.Font.Name = "Segoe UI";
			this.textBox67.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox67.StyleName = "";
			// 
			// textBox68
			// 
			this.textBox68.Name = "textBox68";
			this.textBox68.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9708319902420044D), Telerik.Reporting.Drawing.Unit.Inch(0.21000000834465027D));
			this.textBox68.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox68.Style.Font.Name = "Segoe UI";
			this.textBox68.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox68.StyleName = "";
			// 
			// textBox69
			// 
			formattingRule3.Filters.Add(new Telerik.Reporting.Filter("Fields.TotalCost Is Null", Telerik.Reporting.FilterOperator.Equal, "= True"));
			formattingRule3.Style.Color = System.Drawing.Color.White;
			formattingRule3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			formattingRule3.Style.Visible = true;
			this.textBox69.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule3});
			this.textBox69.Name = "textBox69";
			this.textBox69.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.88749939203262329D), Telerik.Reporting.Drawing.Unit.Inch(0.21000000834465027D));
			this.textBox69.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox69.Style.Font.Bold = true;
			this.textBox69.Style.Font.Name = "Segoe UI";
			this.textBox69.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox69.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox69.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox69.StyleName = "";
			this.textBox69.Value = "$ / Area:";
			// 
			// textBox71
			// 
			formattingRule4.Filters.Add(new Telerik.Reporting.Filter("Fields.TotalCost Is Null", Telerik.Reporting.FilterOperator.Equal, "= True"));
			formattingRule4.Style.Color = System.Drawing.Color.White;
			formattingRule4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			formattingRule4.Style.Visible = true;
			this.textBox71.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule4});
			this.textBox71.Name = "textBox71";
			this.textBox71.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.80902731418609619D), Telerik.Reporting.Drawing.Unit.Inch(0.21000000834465027D));
			this.textBox71.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox71.Style.Font.Bold = true;
			this.textBox71.Style.Font.Name = "Segoe UI";
			this.textBox71.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox71.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox71.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox71.StyleName = "";
			this.textBox71.Value = "Total Cost:";
			// 
			// textBox72
			// 
			this.textBox72.Format = "{0:C2}";
			this.textBox72.Name = "textBox72";
			this.textBox72.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.874999463558197D), Telerik.Reporting.Drawing.Unit.Inch(0.21000000834465027D));
			this.textBox72.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox72.Style.Font.Name = "Segoe UI";
			this.textBox72.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox72.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox72.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox72.StyleName = "";
			this.textBox72.Value = "=Sum(Fields.TotalCost)";
			// 
			// textBox95
			// 
			this.textBox95.Format = "{0:C2}";
			this.textBox95.Name = "textBox95";
			this.textBox95.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.874999463558197D), Telerik.Reporting.Drawing.Unit.Inch(0.21000000834465027D));
			this.textBox95.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox95.Style.Font.Name = "Segoe UI";
			this.textBox95.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox95.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox95.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox95.StyleName = "";
			this.textBox95.Value = "=Sum(Fields.TotalCost) / Fields.CropZoneArea";
			// 
			// textBox101
			// 
			this.textBox101.Name = "textBox101";
			this.textBox101.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.22916679084300995D), Telerik.Reporting.Drawing.Unit.Inch(0.20833340287208557D));
			this.textBox101.Style.Font.Name = "Segoe UI";
			this.textBox101.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox101.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox101.StyleName = "";
			this.textBox101.Value = "=Fields.IsAverage";
			// 
			// textBox102
			// 
			this.textBox102.Name = "textBox102";
			this.textBox102.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.22916679084300995D), Telerik.Reporting.Drawing.Unit.Inch(0.20999999344348908D));
			this.textBox102.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox102.Style.Font.Name = "Segoe UI";
			this.textBox102.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox102.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox102.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox102.StyleName = "";
			// 
			// table2
			// 
			this.table2.Bindings.Add(new Telerik.Reporting.Binding("DataSource", "=Fields.FertilizerLineItems"));
			this.table2.Bindings.Add(new Telerik.Reporting.Binding("Visible", "=Fields.FertilizerLineItems.Count > 0"));
			this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.762500524520874D)));
			this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.2083337306976318D)));
			this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.9916682243347168D)));
			this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.88749980926513672D)));
			this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.87499922513961792D)));
			this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.77430593967437744D)));
			this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.885416567325592D)));
			this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.23958340287208557D)));
			this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.20999996364116669D)));
			this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.20999996364116669D)));
			this.table2.Body.SetCellContent(0, 0, this.textBox21);
			this.table2.Body.SetCellContent(0, 2, this.textBox22);
			this.table2.Body.SetCellContent(0, 3, this.textBox23);
			this.table2.Body.SetCellContent(0, 1, this.textBox24);
			this.table2.Body.SetCellContent(0, 4, this.textBox25);
			this.table2.Body.SetCellContent(0, 5, this.textBox26);
			this.table2.Body.SetCellContent(0, 6, this.textBox27);
			this.table2.Body.SetCellContent(1, 0, this.textBox73);
			this.table2.Body.SetCellContent(1, 1, this.textBox74);
			this.table2.Body.SetCellContent(1, 2, this.textBox75);
			this.table2.Body.SetCellContent(1, 5, this.textBox80);
			this.table2.Body.SetCellContent(1, 6, this.textBox78);
			this.table2.Body.SetCellContent(1, 3, this.textBox70);
			this.table2.Body.SetCellContent(1, 4, this.textBox90);
			this.table2.Body.SetCellContent(0, 7, this.textBox104);
			this.table2.Body.SetCellContent(1, 7, this.textBox105);
			tableGroup11.ReportItem = this.textBox20;
			tableGroup12.Name = "Group1";
			tableGroup12.ReportItem = this.textBox28;
			tableGroup13.ReportItem = this.textBox29;
			tableGroup14.ReportItem = this.textBox30;
			tableGroup15.Name = "Group2";
			tableGroup15.ReportItem = this.textBox31;
			tableGroup16.Name = "Group3";
			tableGroup16.ReportItem = this.textBox32;
			tableGroup17.Name = "Group4";
			tableGroup17.ReportItem = this.textBox33;
			tableGroup18.Name = "group5";
			tableGroup18.ReportItem = this.textBox103;
			this.table2.ColumnGroups.Add(tableGroup11);
			this.table2.ColumnGroups.Add(tableGroup12);
			this.table2.ColumnGroups.Add(tableGroup13);
			this.table2.ColumnGroups.Add(tableGroup14);
			this.table2.ColumnGroups.Add(tableGroup15);
			this.table2.ColumnGroups.Add(tableGroup16);
			this.table2.ColumnGroups.Add(tableGroup17);
			this.table2.ColumnGroups.Add(tableGroup18);
			this.table2.DataSource = null;
			this.table2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox21,
            this.textBox24,
            this.textBox22,
            this.textBox23,
            this.textBox25,
            this.textBox26,
            this.textBox27,
            this.textBox104,
            this.textBox73,
            this.textBox74,
            this.textBox75,
            this.textBox70,
            this.textBox90,
            this.textBox80,
            this.textBox78,
            this.textBox105,
            this.textBox20,
            this.textBox28,
            this.textBox29,
            this.textBox30,
            this.textBox31,
            this.textBox32,
            this.textBox33,
            this.textBox103});
			this.table2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.093789421021938324D), Telerik.Reporting.Drawing.Unit.Inch(1.3852361440658569D));
			this.table2.Name = "table2";
			tableGroup19.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup19.Name = "DetailGroup";
			tableGroup20.Name = "group1";
			this.table2.RowGroups.Add(tableGroup19);
			this.table2.RowGroups.Add(tableGroup20);
			this.table2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6243076324462891D), Telerik.Reporting.Drawing.Unit.Inch(0.58666658401489258D));
			this.table2.Sortings.Add(new Telerik.Reporting.Sorting("=Fields.StartDateTime", Telerik.Reporting.SortDirection.Asc));
			// 
			// textBox21
			// 
			this.textBox21.Name = "textBox21";
			this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.76250022649765015D), Telerik.Reporting.Drawing.Unit.Inch(0.20999999344348908D));
			this.textBox21.Style.Font.Name = "Segoe UI";
			this.textBox21.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox21.Value = "=Fields.StartDate";
			// 
			// textBox22
			// 
			this.textBox22.Name = "textBox22";
			this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9916678667068481D), Telerik.Reporting.Drawing.Unit.Inch(0.20999999344348908D));
			this.textBox22.Style.Font.Name = "Segoe UI";
			this.textBox22.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox22.Value = "=Fields.ProductDisplay";
			// 
			// textBox23
			// 
			this.textBox23.Name = "textBox23";
			this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.887500524520874D), Telerik.Reporting.Drawing.Unit.Inch(0.20999999344348908D));
			this.textBox23.Style.Font.Name = "Segoe UI";
			this.textBox23.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox23.Value = "=Fields.RateDisplay";
			// 
			// textBox24
			// 
			this.textBox24.Name = "textBox24";
			this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2083337306976318D), Telerik.Reporting.Drawing.Unit.Inch(0.20999999344348908D));
			this.textBox24.Style.Font.Name = "Segoe UI";
			this.textBox24.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox24.StyleName = "";
			this.textBox24.Value = "=Fields.ApplicatorDisplay";
			// 
			// textBox25
			// 
			this.textBox25.Name = "textBox25";
			this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.8749997615814209D), Telerik.Reporting.Drawing.Unit.Inch(0.20999999344348908D));
			this.textBox25.Style.Font.Name = "Segoe UI";
			this.textBox25.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox25.StyleName = "";
			this.textBox25.Value = "=Fields.AreaDisplay";
			// 
			// textBox26
			// 
			this.textBox26.Name = "textBox26";
			this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.77430576086044312D), Telerik.Reporting.Drawing.Unit.Inch(0.20999999344348908D));
			this.textBox26.Style.Font.Name = "Segoe UI";
			this.textBox26.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox26.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox26.StyleName = "";
			this.textBox26.Value = "=Fields.TotalProductDisplay";
			// 
			// textBox27
			// 
			this.textBox27.Format = "{0:C2}";
			this.textBox27.Name = "textBox27";
			this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.88541620969772339D), Telerik.Reporting.Drawing.Unit.Inch(0.20999999344348908D));
			this.textBox27.Style.Font.Name = "Segoe UI";
			this.textBox27.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox27.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox27.StyleName = "";
			this.textBox27.Value = "=Fields.TotalCost";
			// 
			// textBox73
			// 
			this.textBox73.Name = "textBox73";
			this.textBox73.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.76250022649765015D), Telerik.Reporting.Drawing.Unit.Inch(0.20999999344348908D));
			this.textBox73.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox73.Style.Font.Name = "Segoe UI";
			this.textBox73.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox73.StyleName = "";
			// 
			// textBox74
			// 
			this.textBox74.Name = "textBox74";
			this.textBox74.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2083337306976318D), Telerik.Reporting.Drawing.Unit.Inch(0.20999999344348908D));
			this.textBox74.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox74.Style.Font.Name = "Segoe UI";
			this.textBox74.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox74.StyleName = "";
			// 
			// textBox75
			// 
			this.textBox75.Name = "textBox75";
			this.textBox75.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9916679859161377D), Telerik.Reporting.Drawing.Unit.Inch(0.20999999344348908D));
			this.textBox75.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox75.Style.Font.Name = "Segoe UI";
			this.textBox75.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox75.StyleName = "";
			// 
			// textBox80
			// 
			formattingRule5.Filters.Add(new Telerik.Reporting.Filter("Fields.TotalCost Is Null", Telerik.Reporting.FilterOperator.Equal, "= True"));
			formattingRule5.Style.Color = System.Drawing.Color.White;
			formattingRule5.Style.Visible = true;
			this.textBox80.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule5});
			this.textBox80.Name = "textBox80";
			this.textBox80.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.77430570125579834D), Telerik.Reporting.Drawing.Unit.Inch(0.20999999344348908D));
			this.textBox80.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox80.Style.Font.Bold = true;
			this.textBox80.Style.Font.Name = "Segoe UI";
			this.textBox80.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox80.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox80.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox80.StyleName = "";
			this.textBox80.Value = "Total Cost:";
			// 
			// textBox78
			// 
			this.textBox78.Format = "{0:C2}";
			this.textBox78.Name = "textBox78";
			this.textBox78.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.88541615009307861D), Telerik.Reporting.Drawing.Unit.Inch(0.20999999344348908D));
			this.textBox78.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox78.Style.Font.Name = "Segoe UI";
			this.textBox78.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox78.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox78.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox78.StyleName = "";
			this.textBox78.Value = "=Sum(Fields.TotalCost)";
			// 
			// textBox70
			// 
			formattingRule6.Filters.Add(new Telerik.Reporting.Filter("Fields.TotalCost Is Null", Telerik.Reporting.FilterOperator.Equal, "= True"));
			formattingRule6.Style.Color = System.Drawing.Color.White;
			formattingRule6.Style.Visible = true;
			this.textBox70.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule6});
			this.textBox70.Name = "textBox70";
			this.textBox70.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.88749939203262329D), Telerik.Reporting.Drawing.Unit.Inch(0.21000000834465027D));
			this.textBox70.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox70.Style.Font.Bold = true;
			this.textBox70.Style.Font.Name = "Segoe UI";
			this.textBox70.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox70.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox70.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox70.StyleName = "";
			this.textBox70.Value = "$ / Area:";
			// 
			// textBox90
			// 
			this.textBox90.Format = "{0:C2}";
			this.textBox90.Name = "textBox90";
			this.textBox90.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.874999463558197D), Telerik.Reporting.Drawing.Unit.Inch(0.21000000834465027D));
			this.textBox90.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox90.Style.Font.Name = "Segoe UI";
			this.textBox90.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox90.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox90.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox90.StyleName = "";
			this.textBox90.Value = "=Sum(Fields.TotalCost) / Fields.CropZoneArea";
			// 
			// textBox104
			// 
			this.textBox104.Name = "textBox104";
			this.textBox104.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.23958340287208557D), Telerik.Reporting.Drawing.Unit.Inch(0.20999996364116669D));
			this.textBox104.Style.Font.Name = "Segoe UI";
			this.textBox104.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox104.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox104.StyleName = "";
			this.textBox104.Value = "=Fields.IsAverage";
			// 
			// textBox105
			// 
			this.textBox105.Name = "textBox105";
			this.textBox105.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.23958340287208557D), Telerik.Reporting.Drawing.Unit.Inch(0.20999997854232788D));
			this.textBox105.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox105.Style.Font.Name = "Segoe UI";
			this.textBox105.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox105.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox105.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox105.StyleName = "";
			// 
			// textBox34
			// 
			this.textBox34.Bindings.Add(new Telerik.Reporting.Binding("Visible", "=Fields.FertilizerLineItems.Count > 0"));
			this.textBox34.KeepTogether = false;
			this.textBox34.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D), Telerik.Reporting.Drawing.Unit.Inch(0.98515748977661133D));
			this.textBox34.Name = "textBox34";
			this.textBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0603783130645752D), Telerik.Reporting.Drawing.Unit.Inch(0.3999999463558197D));
			this.textBox34.Style.Color = System.Drawing.Color.Gray;
			this.textBox34.Style.Font.Name = "Segoe UI Light";
			this.textBox34.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(18D);
			this.textBox34.Value = "fertilizer";
			// 
			// table3
			// 
			this.table3.Bindings.Add(new Telerik.Reporting.Binding("DataSource", "=Fields.SeedLineItems"));
			this.table3.Bindings.Add(new Telerik.Reporting.Binding("Visible", "=Fields.SeedLineItems.Count > 0"));
			this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.76250046491622925D)));
			this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.208332896232605D)));
			this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.0020837783813477D)));
			this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.88750004768371582D)));
			this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.87499958276748657D)));
			this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.73958325386047363D)));
			this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.90972143411636353D)));
			this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.25000002980232239D)));
			this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.21000003814697266D)));
			this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.21000003814697266D)));
			this.table3.Body.SetCellContent(0, 0, this.textBox36);
			this.table3.Body.SetCellContent(0, 2, this.textBox37);
			this.table3.Body.SetCellContent(0, 3, this.textBox38);
			this.table3.Body.SetCellContent(0, 1, this.textBox39);
			this.table3.Body.SetCellContent(0, 4, this.textBox40);
			this.table3.Body.SetCellContent(0, 5, this.textBox41);
			this.table3.Body.SetCellContent(0, 6, this.textBox42);
			this.table3.Body.SetCellContent(1, 0, this.textBox79);
			this.table3.Body.SetCellContent(1, 1, this.textBox81);
			this.table3.Body.SetCellContent(1, 2, this.textBox82);
			this.table3.Body.SetCellContent(1, 5, this.textBox87);
			this.table3.Body.SetCellContent(1, 6, this.textBox85);
			this.table3.Body.SetCellContent(1, 3, this.textBox76);
			this.table3.Body.SetCellContent(1, 4, this.textBox77);
			this.table3.Body.SetCellContent(0, 7, this.textBox107);
			this.table3.Body.SetCellContent(1, 7, this.textBox108);
			tableGroup21.ReportItem = this.textBox35;
			tableGroup22.Name = "Group1";
			tableGroup22.ReportItem = this.textBox43;
			tableGroup23.ReportItem = this.textBox44;
			tableGroup24.ReportItem = this.textBox45;
			tableGroup25.Name = "Group2";
			tableGroup25.ReportItem = this.textBox46;
			tableGroup26.Name = "Group3";
			tableGroup26.ReportItem = this.textBox47;
			tableGroup27.Name = "Group4";
			tableGroup27.ReportItem = this.textBox48;
			tableGroup28.Name = "group6";
			tableGroup28.ReportItem = this.textBox106;
			this.table3.ColumnGroups.Add(tableGroup21);
			this.table3.ColumnGroups.Add(tableGroup22);
			this.table3.ColumnGroups.Add(tableGroup23);
			this.table3.ColumnGroups.Add(tableGroup24);
			this.table3.ColumnGroups.Add(tableGroup25);
			this.table3.ColumnGroups.Add(tableGroup26);
			this.table3.ColumnGroups.Add(tableGroup27);
			this.table3.ColumnGroups.Add(tableGroup28);
			this.table3.DataSource = null;
			this.table3.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox36,
            this.textBox39,
            this.textBox37,
            this.textBox38,
            this.textBox40,
            this.textBox41,
            this.textBox42,
            this.textBox107,
            this.textBox79,
            this.textBox81,
            this.textBox82,
            this.textBox76,
            this.textBox77,
            this.textBox87,
            this.textBox85,
            this.textBox108,
            this.textBox35,
            this.textBox43,
            this.textBox44,
            this.textBox45,
            this.textBox46,
            this.textBox47,
            this.textBox48,
            this.textBox106});
			this.table3.KeepTogether = false;
			this.table3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.087539829313755035D), Telerik.Reporting.Drawing.Unit.Inch(2.3720600605010986D));
			this.table3.Name = "table3";
			tableGroup29.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup29.Name = "DetailGroup";
			tableGroup30.Name = "group2";
			this.table3.RowGroups.Add(tableGroup29);
			this.table3.RowGroups.Add(tableGroup30);
			this.table3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6347217559814453D), Telerik.Reporting.Drawing.Unit.Inch(0.5866667628288269D));
			this.table3.Sortings.Add(new Telerik.Reporting.Sorting("=Fields.StartDateTime", Telerik.Reporting.SortDirection.Asc));
			// 
			// textBox36
			// 
			this.textBox36.Name = "textBox36";
			this.textBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.76250046491622925D), Telerik.Reporting.Drawing.Unit.Inch(0.20999999344348908D));
			this.textBox36.Style.Font.Name = "Segoe UI";
			this.textBox36.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox36.Value = "=Fields.StartDate";
			// 
			// textBox37
			// 
			this.textBox37.Name = "textBox37";
			this.textBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0020830631256104D), Telerik.Reporting.Drawing.Unit.Inch(0.20999999344348908D));
			this.textBox37.Style.Font.Name = "Segoe UI";
			this.textBox37.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox37.Value = "=Fields.ProductDisplay";
			// 
			// textBox38
			// 
			this.textBox38.Name = "textBox38";
			this.textBox38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.88750016689300537D), Telerik.Reporting.Drawing.Unit.Inch(0.20999999344348908D));
			this.textBox38.Style.Font.Name = "Segoe UI";
			this.textBox38.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox38.Value = "=Fields.RateDisplay";
			// 
			// textBox39
			// 
			this.textBox39.Name = "textBox39";
			this.textBox39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2083337306976318D), Telerik.Reporting.Drawing.Unit.Inch(0.20999999344348908D));
			this.textBox39.Style.Font.Name = "Segoe UI";
			this.textBox39.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox39.StyleName = "";
			this.textBox39.Value = "=Fields.ApplicatorDisplay";
			// 
			// textBox40
			// 
			this.textBox40.Name = "textBox40";
			this.textBox40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.8749997615814209D), Telerik.Reporting.Drawing.Unit.Inch(0.20999999344348908D));
			this.textBox40.Style.Font.Name = "Segoe UI";
			this.textBox40.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox40.StyleName = "";
			this.textBox40.Value = "=Fields.AreaDisplay";
			// 
			// textBox41
			// 
			this.textBox41.Name = "textBox41";
			this.textBox41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.73958349227905273D), Telerik.Reporting.Drawing.Unit.Inch(0.20999999344348908D));
			this.textBox41.Style.Font.Name = "Segoe UI";
			this.textBox41.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox41.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox41.StyleName = "";
			this.textBox41.Value = "=Fields.TotalProductDisplay";
			// 
			// textBox42
			// 
			this.textBox42.Format = "{0:C2}";
			this.textBox42.Name = "textBox42";
			this.textBox42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.9097217321395874D), Telerik.Reporting.Drawing.Unit.Inch(0.20999999344348908D));
			this.textBox42.Style.Font.Name = "Segoe UI";
			this.textBox42.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox42.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox42.StyleName = "";
			this.textBox42.Value = "=Fields.TotalCost";
			// 
			// textBox79
			// 
			this.textBox79.Name = "textBox79";
			this.textBox79.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.76250046491622925D), Telerik.Reporting.Drawing.Unit.Inch(0.20999999344348908D));
			this.textBox79.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox79.Style.Font.Name = "Segoe UI";
			this.textBox79.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox79.StyleName = "";
			// 
			// textBox81
			// 
			this.textBox81.Name = "textBox81";
			this.textBox81.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2083337306976318D), Telerik.Reporting.Drawing.Unit.Inch(0.20999999344348908D));
			this.textBox81.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox81.Style.Font.Name = "Segoe UI";
			this.textBox81.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox81.StyleName = "";
			// 
			// textBox82
			// 
			this.textBox82.Name = "textBox82";
			this.textBox82.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0020830631256104D), Telerik.Reporting.Drawing.Unit.Inch(0.20999999344348908D));
			this.textBox82.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox82.Style.Font.Name = "Segoe UI";
			this.textBox82.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox82.StyleName = "";
			// 
			// textBox87
			// 
			formattingRule7.Filters.Add(new Telerik.Reporting.Filter("Fields.TotalCost Is Null", Telerik.Reporting.FilterOperator.Equal, "= True"));
			formattingRule7.Style.Color = System.Drawing.Color.White;
			formattingRule7.Style.Visible = true;
			this.textBox87.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule7});
			this.textBox87.Name = "textBox87";
			this.textBox87.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.73958349227905273D), Telerik.Reporting.Drawing.Unit.Inch(0.20999999344348908D));
			this.textBox87.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox87.Style.Font.Bold = true;
			this.textBox87.Style.Font.Name = "Segoe UI";
			this.textBox87.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox87.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox87.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox87.StyleName = "";
			this.textBox87.Value = "Total Cost:";
			// 
			// textBox85
			// 
			this.textBox85.Format = "{0:C2}";
			this.textBox85.Name = "textBox85";
			this.textBox85.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.90972167253494263D), Telerik.Reporting.Drawing.Unit.Inch(0.20999999344348908D));
			this.textBox85.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox85.Style.Font.Name = "Segoe UI";
			this.textBox85.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox85.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox85.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox85.StyleName = "";
			this.textBox85.Value = "=Sum(Fields.TotalCost)";
			// 
			// textBox76
			// 
			this.textBox76.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule7});
			this.textBox76.Name = "textBox76";
			this.textBox76.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.88749939203262329D), Telerik.Reporting.Drawing.Unit.Inch(0.21000000834465027D));
			this.textBox76.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox76.Style.Font.Bold = true;
			this.textBox76.Style.Font.Name = "Segoe UI";
			this.textBox76.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox76.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox76.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox76.StyleName = "";
			this.textBox76.Value = "$ / Area:";
			// 
			// textBox77
			// 
			this.textBox77.Format = "{0:C2}";
			this.textBox77.Name = "textBox77";
			this.textBox77.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.874999463558197D), Telerik.Reporting.Drawing.Unit.Inch(0.21000000834465027D));
			this.textBox77.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox77.Style.Font.Name = "Segoe UI";
			this.textBox77.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox77.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox77.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox77.StyleName = "";
			this.textBox77.Value = "=Sum(Fields.TotalCost) / Fields.CropZoneArea";
			// 
			// textBox107
			// 
			this.textBox107.Name = "textBox107";
			this.textBox107.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.25000008940696716D), Telerik.Reporting.Drawing.Unit.Inch(0.21000003814697266D));
			this.textBox107.Style.Font.Name = "Segoe UI";
			this.textBox107.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox107.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox107.StyleName = "";
			this.textBox107.Value = "=Fields.IsAverage";
			// 
			// textBox108
			// 
			this.textBox108.Name = "textBox108";
			this.textBox108.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.25000008940696716D), Telerik.Reporting.Drawing.Unit.Inch(0.21000003814697266D));
			this.textBox108.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox108.Style.Font.Name = "Segoe UI";
			this.textBox108.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox108.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox108.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox108.StyleName = "";
			// 
			// textBox49
			// 
			this.textBox49.Bindings.Add(new Telerik.Reporting.Binding("Visible", "=Fields.SeedLineItems.Count > 0"));
			this.textBox49.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(1.9719816446304321D));
			this.textBox49.Name = "textBox49";
			this.textBox49.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0603783130645752D), Telerik.Reporting.Drawing.Unit.Inch(0.3999999463558197D));
			this.textBox49.Style.Color = System.Drawing.Color.Gray;
			this.textBox49.Style.Font.Name = "Segoe UI Light";
			this.textBox49.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(18D);
			this.textBox49.Value = "seed";
			// 
			// textBox64
			// 
			this.textBox64.Bindings.Add(new Telerik.Reporting.Binding("Visible", "=Fields.ServiceLineItems.Count > 0"));
			this.textBox64.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(2.9588057994842529D));
			this.textBox64.Name = "textBox64";
			this.textBox64.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0603783130645752D), Telerik.Reporting.Drawing.Unit.Inch(0.3999999463558197D));
			this.textBox64.Style.Color = System.Drawing.Color.Gray;
			this.textBox64.Style.Font.Name = "Segoe UI Light";
			this.textBox64.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(18D);
			this.textBox64.Value = "service";
			// 
			// table4
			// 
			this.table4.Bindings.Add(new Telerik.Reporting.Binding("DataSource", "=Fields.ServiceLineItems"));
			this.table4.Bindings.Add(new Telerik.Reporting.Binding("Visible", "=Fields.ServiceLineItems.Count > 0"));
			this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.7625001072883606D)));
			this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.208332896232605D)));
			this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.9916661977767944D)));
			this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.88749963045120239D)));
			this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.87499934434890747D)));
			this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.7534719705581665D)));
			this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.90624934434890747D)));
			this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.22916677594184876D)));
			this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.21000003814697266D)));
			this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.21000003814697266D)));
			this.table4.Body.SetCellContent(0, 0, this.textBox51);
			this.table4.Body.SetCellContent(0, 2, this.textBox52);
			this.table4.Body.SetCellContent(0, 3, this.textBox53);
			this.table4.Body.SetCellContent(0, 1, this.textBox54);
			this.table4.Body.SetCellContent(0, 4, this.textBox55);
			this.table4.Body.SetCellContent(0, 5, this.textBox56);
			this.table4.Body.SetCellContent(0, 6, this.textBox57);
			this.table4.Body.SetCellContent(1, 0, this.textBox86);
			this.table4.Body.SetCellContent(1, 1, this.textBox88);
			this.table4.Body.SetCellContent(1, 2, this.textBox89);
			this.table4.Body.SetCellContent(1, 5, this.textBox94);
			this.table4.Body.SetCellContent(1, 6, this.textBox92);
			this.table4.Body.SetCellContent(1, 3, this.textBox83);
			this.table4.Body.SetCellContent(1, 4, this.textBox84);
			this.table4.Body.SetCellContent(0, 7, this.textBox110);
			this.table4.Body.SetCellContent(1, 7, this.textBox111);
			tableGroup31.ReportItem = this.textBox50;
			tableGroup32.Name = "Group1";
			tableGroup32.ReportItem = this.textBox58;
			tableGroup33.ReportItem = this.textBox59;
			tableGroup34.ReportItem = this.textBox60;
			tableGroup35.Name = "Group2";
			tableGroup35.ReportItem = this.textBox61;
			tableGroup36.Name = "Group3";
			tableGroup36.ReportItem = this.textBox62;
			tableGroup37.Name = "Group4";
			tableGroup37.ReportItem = this.textBox63;
			tableGroup38.Name = "group7";
			tableGroup38.ReportItem = this.textBox109;
			this.table4.ColumnGroups.Add(tableGroup31);
			this.table4.ColumnGroups.Add(tableGroup32);
			this.table4.ColumnGroups.Add(tableGroup33);
			this.table4.ColumnGroups.Add(tableGroup34);
			this.table4.ColumnGroups.Add(tableGroup35);
			this.table4.ColumnGroups.Add(tableGroup36);
			this.table4.ColumnGroups.Add(tableGroup37);
			this.table4.ColumnGroups.Add(tableGroup38);
			this.table4.DataSource = null;
			this.table4.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox51,
            this.textBox54,
            this.textBox52,
            this.textBox53,
            this.textBox55,
            this.textBox56,
            this.textBox57,
            this.textBox110,
            this.textBox86,
            this.textBox88,
            this.textBox89,
            this.textBox83,
            this.textBox84,
            this.textBox94,
            this.textBox92,
            this.textBox111,
            this.textBox50,
            this.textBox58,
            this.textBox59,
            this.textBox60,
            this.textBox61,
            this.textBox62,
            this.textBox63,
            this.textBox109});
			this.table4.KeepTogether = false;
			this.table4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.087539829313755035D), Telerik.Reporting.Drawing.Unit.Inch(3.3588845729827881D));
			this.table4.Name = "table4";
			tableGroup39.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup39.Name = "DetailGroup";
			tableGroup40.Name = "group3";
			this.table4.RowGroups.Add(tableGroup39);
			this.table4.RowGroups.Add(tableGroup40);
			this.table4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.61388635635376D), Telerik.Reporting.Drawing.Unit.Inch(0.5866667628288269D));
			this.table4.Sortings.Add(new Telerik.Reporting.Sorting("=Fields.StartDateTime", Telerik.Reporting.SortDirection.Asc));
			// 
			// textBox51
			// 
			this.textBox51.Name = "textBox51";
			this.textBox51.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.7625001072883606D), Telerik.Reporting.Drawing.Unit.Inch(0.20999999344348908D));
			this.textBox51.Style.Font.Name = "Segoe UI";
			this.textBox51.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox51.Value = "=Fields.StartDate";
			// 
			// textBox52
			// 
			this.textBox52.Name = "textBox52";
			this.textBox52.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9916657209396362D), Telerik.Reporting.Drawing.Unit.Inch(0.20999999344348908D));
			this.textBox52.Style.Font.Name = "Segoe UI";
			this.textBox52.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox52.Value = "=Fields.ProductDisplay";
			// 
			// textBox53
			// 
			this.textBox53.Name = "textBox53";
			this.textBox53.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.88749974966049194D), Telerik.Reporting.Drawing.Unit.Inch(0.20999999344348908D));
			this.textBox53.Style.Font.Name = "Segoe UI";
			this.textBox53.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox53.Value = "=Fields.RateDisplay";
			// 
			// textBox54
			// 
			this.textBox54.Name = "textBox54";
			this.textBox54.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2083325386047363D), Telerik.Reporting.Drawing.Unit.Inch(0.20999999344348908D));
			this.textBox54.Style.Font.Name = "Segoe UI";
			this.textBox54.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox54.StyleName = "";
			this.textBox54.Value = "=Fields.ApplicatorDisplay";
			// 
			// textBox55
			// 
			this.textBox55.Name = "textBox55";
			this.textBox55.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.87499922513961792D), Telerik.Reporting.Drawing.Unit.Inch(0.20999999344348908D));
			this.textBox55.Style.Font.Name = "Segoe UI";
			this.textBox55.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox55.StyleName = "";
			this.textBox55.Value = "=Fields.AreaDisplay";
			// 
			// textBox56
			// 
			this.textBox56.Name = "textBox56";
			this.textBox56.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.75347167253494263D), Telerik.Reporting.Drawing.Unit.Inch(0.20999999344348908D));
			this.textBox56.Style.Font.Name = "Segoe UI";
			this.textBox56.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox56.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox56.StyleName = "";
			this.textBox56.Value = "=Fields.TotalProductDisplay";
			// 
			// textBox57
			// 
			this.textBox57.Format = "{0:C2}";
			this.textBox57.Name = "textBox57";
			this.textBox57.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.90624940395355225D), Telerik.Reporting.Drawing.Unit.Inch(0.20999999344348908D));
			this.textBox57.Style.Font.Name = "Segoe UI";
			this.textBox57.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox57.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox57.StyleName = "";
			this.textBox57.Value = "=Fields.TotalCost";
			// 
			// textBox86
			// 
			this.textBox86.Name = "textBox86";
			this.textBox86.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.7625001072883606D), Telerik.Reporting.Drawing.Unit.Inch(0.20999999344348908D));
			this.textBox86.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox86.Style.Font.Name = "Segoe UI";
			this.textBox86.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox86.StyleName = "";
			// 
			// textBox88
			// 
			this.textBox88.Name = "textBox88";
			this.textBox88.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2083324193954468D), Telerik.Reporting.Drawing.Unit.Inch(0.20999999344348908D));
			this.textBox88.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox88.Style.Font.Name = "Segoe UI";
			this.textBox88.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox88.StyleName = "";
			// 
			// textBox89
			// 
			this.textBox89.Name = "textBox89";
			this.textBox89.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9916654825210571D), Telerik.Reporting.Drawing.Unit.Inch(0.20999999344348908D));
			this.textBox89.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox89.Style.Font.Name = "Segoe UI";
			this.textBox89.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox89.StyleName = "";
			// 
			// textBox94
			// 
			this.textBox94.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule7});
			this.textBox94.Name = "textBox94";
			this.textBox94.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.75347167253494263D), Telerik.Reporting.Drawing.Unit.Inch(0.20999999344348908D));
			this.textBox94.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox94.Style.Font.Bold = true;
			this.textBox94.Style.Font.Name = "Segoe UI";
			this.textBox94.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox94.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox94.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox94.StyleName = "";
			this.textBox94.Value = "Total Cost:";
			// 
			// textBox92
			// 
			this.textBox92.Format = "{0:C2}";
			this.textBox92.Name = "textBox92";
			this.textBox92.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.90624934434890747D), Telerik.Reporting.Drawing.Unit.Inch(0.20999997854232788D));
			this.textBox92.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox92.Style.Font.Name = "Segoe UI";
			this.textBox92.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox92.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox92.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox92.StyleName = "";
			this.textBox92.Value = "=Sum(Fields.TotalCost)";
			// 
			// textBox83
			// 
			this.textBox83.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule7});
			this.textBox83.Name = "textBox83";
			this.textBox83.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.88749939203262329D), Telerik.Reporting.Drawing.Unit.Inch(0.21000000834465027D));
			this.textBox83.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox83.Style.Font.Bold = true;
			this.textBox83.Style.Font.Name = "Segoe UI";
			this.textBox83.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox83.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox83.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox83.StyleName = "";
			this.textBox83.Value = "$ / Area:";
			// 
			// textBox84
			// 
			this.textBox84.Format = "{0:C2}";
			this.textBox84.Name = "textBox84";
			this.textBox84.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.874999463558197D), Telerik.Reporting.Drawing.Unit.Inch(0.21000000834465027D));
			this.textBox84.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox84.Style.Font.Name = "Segoe UI";
			this.textBox84.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox84.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox84.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox84.StyleName = "";
			this.textBox84.Value = "=Sum(Fields.TotalCost) / Fields.CropZoneArea";
			// 
			// textBox110
			// 
			this.textBox110.Name = "textBox110";
			this.textBox110.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.22916677594184876D), Telerik.Reporting.Drawing.Unit.Inch(0.21000003814697266D));
			this.textBox110.Style.Font.Name = "Segoe UI";
			this.textBox110.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox110.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox110.StyleName = "";
			this.textBox110.Value = "=Fields.IsAverage";
			// 
			// textBox111
			// 
			this.textBox111.Name = "textBox111";
			this.textBox111.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.22916677594184876D), Telerik.Reporting.Drawing.Unit.Inch(0.21000003814697266D));
			this.textBox111.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox111.Style.Font.Name = "Segoe UI";
			this.textBox111.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox111.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox111.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox111.StyleName = "";
			// 
			// textBox91
			// 
			formattingRule8.Filters.Add(new Telerik.Reporting.Filter("=Fields.TotalCost Is Null", Telerik.Reporting.FilterOperator.Equal, "= True"));
			formattingRule8.Style.Visible = false;
			this.textBox91.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule8});
			this.textBox91.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.46592903137207D), Telerik.Reporting.Drawing.Unit.Inch(4.0999608039855957D));
			this.textBox91.Name = "textBox91";
			this.textBox91.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.93399238586425781D), Telerik.Reporting.Drawing.Unit.Inch(0.20003955066204071D));
			this.textBox91.Style.Font.Bold = true;
			this.textBox91.Value = "Total Cost:";
			// 
			// textBox96
			// 
			this.textBox96.Format = "{0:C2}";
			this.textBox96.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.4000000953674316D), Telerik.Reporting.Drawing.Unit.Inch(4.0999608039855957D));
			this.textBox96.Name = "textBox96";
			this.textBox96.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0722579956054688D), Telerik.Reporting.Drawing.Unit.Inch(0.20003955066204071D));
			this.textBox96.Style.Font.Bold = false;
			this.textBox96.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox96.Value = "=Fields.TotalCost";
			// 
			// textBox97
			// 
			this.textBox97.Format = "{0:C2}";
			this.textBox97.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.3763504028320312D), Telerik.Reporting.Drawing.Unit.Inch(4.0999608039855957D));
			this.textBox97.Name = "textBox97";
			this.textBox97.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0722579956054688D), Telerik.Reporting.Drawing.Unit.Inch(0.20003955066204071D));
			this.textBox97.Style.Font.Bold = false;
			this.textBox97.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox97.Value = "=Fields.TotalCostPerArea";
			// 
			// textBox98
			// 
			formattingRule9.Filters.Add(new Telerik.Reporting.Filter("=Fields.TotalCostPerArea Is Null", Telerik.Reporting.FilterOperator.Equal, "= True"));
			formattingRule9.Style.Visible = false;
			this.textBox98.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule9});
			this.textBox98.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.6471836566925049D), Telerik.Reporting.Drawing.Unit.Inch(4.0999608039855957D));
			this.textBox98.Name = "textBox98";
			this.textBox98.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.72423458099365234D), Telerik.Reporting.Drawing.Unit.Inch(0.20003955066204071D));
			this.textBox98.Style.Font.Bold = true;
			this.textBox98.Value = "$ / Area:";
			// 
			// textBox112
			// 
			formattingRule10.Filters.Add(new Telerik.Reporting.Filter("= Fields.CostType", Telerik.Reporting.FilterOperator.Equal, "Average"));
			formattingRule10.Style.Visible = false;
			this.textBox112.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule10});
			this.textBox112.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.093789421021938324D), Telerik.Reporting.Drawing.Unit.Inch(4.3000006675720215D));
			this.textBox112.Name = "textBox112";
			this.textBox112.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9645830392837524D), Telerik.Reporting.Drawing.Unit.Inch(0.19999949634075165D));
			this.textBox112.Style.Font.Italic = true;
			this.textBox112.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox112.Value = "* average cost";
			// 
			// reportDataSource
			// 
			this.reportDataSource.DataSource = typeof(Landdb.ReportModels.AppliedProductByFieldDetail.ReportData);
			this.reportDataSource.Name = "reportDataSource";
			// 
			// pageFooterSection1
			// 
			this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.40000024437904358D);
			this.pageFooterSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox65,
            this.pageInfoTextBox,
            this.textBox99});
			this.pageFooterSection1.Name = "pageFooterSection1";
			// 
			// textBox65
			// 
			this.textBox65.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.1000001430511475D), Telerik.Reporting.Drawing.Unit.Inch(0.10000038146972656D));
			this.textBox65.Name = "textBox65";
			this.textBox65.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9999998807907105D), Telerik.Reporting.Drawing.Unit.Inch(0.29996046423912048D));
			this.textBox65.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox65.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox65.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox65.Value = "generated on {Now()}";
			// 
			// pageInfoTextBox
			// 
			this.pageInfoTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.6000003814697266D), Telerik.Reporting.Drawing.Unit.Inch(0.10000038146972656D));
			this.pageInfoTextBox.Name = "pageInfoTextBox";
			this.pageInfoTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.3998813629150391D), Telerik.Reporting.Drawing.Unit.Inch(0.29999962449073792D));
			this.pageInfoTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.pageInfoTextBox.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.pageInfoTextBox.StyleName = "PageInfo";
			this.pageInfoTextBox.Value = "=PageNumber";
			// 
			// textBox99
			// 
			this.textBox99.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.10000038146972656D));
			this.textBox99.Name = "textBox99";
			this.textBox99.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8790887594223023D), Telerik.Reporting.Drawing.Unit.Inch(0.29996046423912048D));
			this.textBox99.Style.Color = System.Drawing.Color.DimGray;
			this.textBox99.Style.Font.Bold = true;
			this.textBox99.Style.Font.Italic = false;
			this.textBox99.Style.Font.Name = "Segoe UI Light";
			this.textBox99.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(16D);
			this.textBox99.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox99.Value = "AG CONNECTIONS";
			// 
			// reportHeaderSection1
			// 
			this.reportHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(1.2000000476837158D);
			this.reportHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox2,
            this.textBox1,
            this.textBox3,
            this.txtDataSourceLabel,
            this.textBox113,
            this.textBox93,
            this.textBox4});
			this.reportHeaderSection1.Name = "reportHeaderSection1";
			// 
			// textBox2
			// 
			this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9339065551757812E-05D), Telerik.Reporting.Drawing.Unit.Inch(0.50003939867019653D));
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.0311698913574219D), Telerik.Reporting.Drawing.Unit.Inch(0.19996063411235809D));
			this.textBox2.Style.Font.Bold = true;
			this.textBox2.Style.Font.Name = "Segoe UI";
			this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox2.Value = "=Fields.FieldDisplay";
			// 
			// textBox1
			// 
			this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D), Telerik.Reporting.Drawing.Unit.Inch(0.70007878541946411D));
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.0312094688415527D), Telerik.Reporting.Drawing.Unit.Inch(0.19996063411235809D));
			this.textBox1.Style.Font.Bold = false;
			this.textBox1.Style.Font.Name = "Segoe UI";
			this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox1.Value = "=Fields.CropDisplay";
			// 
			// textBox3
			// 
			this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6D), Telerik.Reporting.Drawing.Unit.Inch(0.5D));
			this.textBox3.Name = "textBox3";
			this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9998817443847656D), Telerik.Reporting.Drawing.Unit.Inch(0.29996061325073242D));
			this.textBox3.Style.Color = System.Drawing.Color.DimGray;
			this.textBox3.Style.Font.Name = "Segoe UI Light";
			this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(18D);
			this.textBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox3.TextWrap = false;
			this.textBox3.Value = "=Fields.AreaDisplay";
			// 
			// txtDataSourceLabel
			// 
			this.txtDataSourceLabel.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
			this.txtDataSourceLabel.Name = "txtDataSourceLabel";
			this.txtDataSourceLabel.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.0312089920043945D), Telerik.Reporting.Drawing.Unit.Inch(0.50000005960464478D));
			this.txtDataSourceLabel.Style.Font.Name = "Segoe UI Light";
			this.txtDataSourceLabel.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(24D);
			this.txtDataSourceLabel.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.txtDataSourceLabel.Value = "=Fields.DataSourceName";
			// 
			// textBox113
			// 
			this.textBox113.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.53533411026001D), Telerik.Reporting.Drawing.Unit.Inch(0D));
			this.textBox113.Name = "textBox113";
			this.textBox113.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.4687116146087646D), Telerik.Reporting.Drawing.Unit.Inch(0.29996061325073242D));
			this.textBox113.Style.Color = System.Drawing.Color.DimGray;
			this.textBox113.Style.Font.Name = "Segoe UI Light";
			this.textBox113.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(18D);
			this.textBox113.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox113.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox113.TextWrap = false;
			this.textBox113.Value = "Land Owner Share";
			// 
			// textBox93
			// 
			this.textBox93.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.90011817216873169D));
			this.textBox93.Name = "textBox93";
			this.textBox93.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.0312089920043945D), Telerik.Reporting.Drawing.Unit.Inch(0.19996063411235809D));
			this.textBox93.Style.Color = System.Drawing.Color.DimGray;
			this.textBox93.Style.Font.Name = "Segoe UI";
			this.textBox93.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox93.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox93.TextWrap = false;
			this.textBox93.Value = "=Fields.DateRangeDisplay";
			// 
			// textBox4
			// 
			this.textBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.0041642189025879D), Telerik.Reporting.Drawing.Unit.Inch(0.2999606728553772D));
			this.textBox4.Name = "textBox4";
			this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9998817443847656D), Telerik.Reporting.Drawing.Unit.Inch(0.19996063411235809D));
			this.textBox4.Style.Color = System.Drawing.Color.DimGray;
			this.textBox4.Style.Font.Name = "Segoe UI";
			this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox4.TextWrap = false;
			this.textBox4.Value = "=Fields.CropYear";
			// 
			// LandOwnerShare
			// 
			this.DataSource = this.reportDataSource;
			this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail,
            this.pageFooterSection1,
            this.reportHeaderSection1});
			this.Name = "LandOwnerShare";
			this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(0.25D), Telerik.Reporting.Drawing.Unit.Inch(0.25D), Telerik.Reporting.Drawing.Unit.Inch(0.25D), Telerik.Reporting.Drawing.Unit.Inch(0.25D));
			this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
			this.Style.BackgroundColor = System.Drawing.Color.White;
			styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
			styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
			this.Width = Telerik.Reporting.Drawing.Unit.Inch(8.0040464401245117D);
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PageFooterSection pageFooterSection1;
        private Telerik.Reporting.ReportHeaderSection reportHeaderSection1;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.ObjectDataSource reportDataSource;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.Table table1;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.Table table2;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.TextBox textBox25;
        private Telerik.Reporting.TextBox textBox26;
        private Telerik.Reporting.TextBox textBox27;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.TextBox textBox28;
        private Telerik.Reporting.TextBox textBox29;
        private Telerik.Reporting.TextBox textBox30;
        private Telerik.Reporting.TextBox textBox31;
        private Telerik.Reporting.TextBox textBox32;
        private Telerik.Reporting.TextBox textBox33;
        private Telerik.Reporting.TextBox textBox34;
        private Telerik.Reporting.Table table3;
        private Telerik.Reporting.TextBox textBox36;
        private Telerik.Reporting.TextBox textBox37;
        private Telerik.Reporting.TextBox textBox38;
        private Telerik.Reporting.TextBox textBox39;
        private Telerik.Reporting.TextBox textBox40;
        private Telerik.Reporting.TextBox textBox41;
        private Telerik.Reporting.TextBox textBox42;
        private Telerik.Reporting.TextBox textBox35;
        private Telerik.Reporting.TextBox textBox43;
        private Telerik.Reporting.TextBox textBox44;
        private Telerik.Reporting.TextBox textBox45;
        private Telerik.Reporting.TextBox textBox46;
        private Telerik.Reporting.TextBox textBox47;
        private Telerik.Reporting.TextBox textBox48;
        private Telerik.Reporting.TextBox textBox49;
        private Telerik.Reporting.TextBox textBox64;
        private Telerik.Reporting.Table table4;
        private Telerik.Reporting.TextBox textBox51;
        private Telerik.Reporting.TextBox textBox52;
        private Telerik.Reporting.TextBox textBox53;
        private Telerik.Reporting.TextBox textBox54;
        private Telerik.Reporting.TextBox textBox55;
        private Telerik.Reporting.TextBox textBox56;
        private Telerik.Reporting.TextBox textBox57;
        private Telerik.Reporting.TextBox textBox50;
        private Telerik.Reporting.TextBox textBox58;
        private Telerik.Reporting.TextBox textBox59;
        private Telerik.Reporting.TextBox textBox60;
        private Telerik.Reporting.TextBox textBox61;
        private Telerik.Reporting.TextBox textBox62;
        private Telerik.Reporting.TextBox textBox63;
        private Telerik.Reporting.TextBox textBox65;
        private Telerik.Reporting.TextBox textBox66;
        private Telerik.Reporting.TextBox textBox67;
        private Telerik.Reporting.TextBox textBox68;
        private Telerik.Reporting.TextBox textBox69;
        private Telerik.Reporting.TextBox textBox71;
        private Telerik.Reporting.TextBox textBox72;
        private Telerik.Reporting.TextBox textBox73;
        private Telerik.Reporting.TextBox textBox74;
        private Telerik.Reporting.TextBox textBox75;
        private Telerik.Reporting.TextBox textBox80;
        private Telerik.Reporting.TextBox textBox78;
        private Telerik.Reporting.TextBox textBox79;
        private Telerik.Reporting.TextBox textBox81;
        private Telerik.Reporting.TextBox textBox82;
        private Telerik.Reporting.TextBox textBox87;
        private Telerik.Reporting.TextBox textBox85;
        private Telerik.Reporting.TextBox textBox86;
        private Telerik.Reporting.TextBox textBox88;
        private Telerik.Reporting.TextBox textBox89;
        private Telerik.Reporting.TextBox textBox94;
        private Telerik.Reporting.TextBox textBox92;
        private Telerik.Reporting.TextBox textBox95;
        private Telerik.Reporting.TextBox textBox70;
        private Telerik.Reporting.TextBox textBox90;
        private Telerik.Reporting.TextBox textBox76;
        private Telerik.Reporting.TextBox textBox77;
        private Telerik.Reporting.TextBox textBox83;
        private Telerik.Reporting.TextBox textBox84;
        private Telerik.Reporting.TextBox textBox91;
        private Telerik.Reporting.TextBox textBox96;
        private Telerik.Reporting.TextBox pageInfoTextBox;
        private Telerik.Reporting.TextBox textBox97;
        private Telerik.Reporting.TextBox textBox98;
        private Telerik.Reporting.TextBox textBox99;
        private Telerik.Reporting.TextBox textBox101;
        private Telerik.Reporting.TextBox textBox102;
        private Telerik.Reporting.TextBox textBox100;
        private Telerik.Reporting.TextBox textBox104;
        private Telerik.Reporting.TextBox textBox105;
        private Telerik.Reporting.TextBox textBox103;
        private Telerik.Reporting.TextBox textBox107;
        private Telerik.Reporting.TextBox textBox108;
        private Telerik.Reporting.TextBox textBox106;
        private Telerik.Reporting.TextBox textBox110;
        private Telerik.Reporting.TextBox textBox111;
        private Telerik.Reporting.TextBox textBox109;
        private Telerik.Reporting.TextBox textBox112;
		private Telerik.Reporting.TextBox txtDataSourceLabel;
		private Telerik.Reporting.TextBox textBox113;
		private Telerik.Reporting.TextBox textBox93;
		private Telerik.Reporting.TextBox textBox4;
	}
}