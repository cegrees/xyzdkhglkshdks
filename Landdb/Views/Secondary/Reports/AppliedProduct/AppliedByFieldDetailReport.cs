namespace Landdb.Views.Secondary.Reports.AppliedProduct {
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;

    /// <summary>
    /// Summary description for AppliedByFieldDetailReport.
    /// </summary>
    public partial class AppliedByFieldDetailReport : Telerik.Reporting.Report {
        public AppliedByFieldDetailReport() {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();
        }
    }
}