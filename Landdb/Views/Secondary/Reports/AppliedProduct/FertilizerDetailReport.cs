namespace Landdb.Views.Secondary.Reports.AppliedProduct
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;

    /// <summary>
    /// Summary description for FertilizerDetailReport.
    /// </summary>
    public partial class FertilizerDetailReport : Telerik.Reporting.Report
    {
        public FertilizerDetailReport()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }
    }
}