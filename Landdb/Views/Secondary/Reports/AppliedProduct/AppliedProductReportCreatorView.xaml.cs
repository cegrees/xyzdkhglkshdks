﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Landdb.Views.Secondary.Reports.AppliedProduct {
    /// <summary>
    /// Interaction logic for AppliedProductReportCreatorView.xaml
    /// </summary>
    public partial class AppliedProductReportCreatorView : UserControl {
        public AppliedProductReportCreatorView() {
            InitializeComponent();
        }

        private void ApplicatorTabs_SelectionChanged_1(object sender, SelectionChangedEventArgs e) {
            ApplicatorTabs.UpdateLayout();
        }
    }
}
