namespace Landdb.Views.Secondary.Reports.AppliedProduct
{
    partial class ReEntrySummaryReport
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			Telerik.Reporting.Drawing.FormattingRule formattingRule1 = new Telerik.Reporting.Drawing.FormattingRule();
			Telerik.Reporting.Drawing.FormattingRule formattingRule2 = new Telerik.Reporting.Drawing.FormattingRule();
			Telerik.Reporting.Drawing.FormattingRule formattingRule3 = new Telerik.Reporting.Drawing.FormattingRule();
			Telerik.Reporting.Group group1 = new Telerik.Reporting.Group();
			Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
			this.groupFooterSection = new Telerik.Reporting.GroupFooterSection();
			this.groupHeaderSection = new Telerik.Reporting.GroupHeaderSection();
			this.textBox19 = new Telerik.Reporting.TextBox();
			this.textBox48 = new Telerik.Reporting.TextBox();
			this.textBox1 = new Telerik.Reporting.TextBox();
			this.textBox2 = new Telerik.Reporting.TextBox();
			this.textBox3 = new Telerik.Reporting.TextBox();
			this.textBox5 = new Telerik.Reporting.TextBox();
			this.textBox6 = new Telerik.Reporting.TextBox();
			this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
			this.detail = new Telerik.Reporting.DetailSection();
			this.textBox7 = new Telerik.Reporting.TextBox();
			this.textBox9 = new Telerik.Reporting.TextBox();
			this.textBox10 = new Telerik.Reporting.TextBox();
			this.textBox11 = new Telerik.Reporting.TextBox();
			this.textBox12 = new Telerik.Reporting.TextBox();
			this.shape1 = new Telerik.Reporting.Shape();
			this.textBox8 = new Telerik.Reporting.TextBox();
			this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
			this.pageInfoTextBox = new Telerik.Reporting.TextBox();
			this.textBox65 = new Telerik.Reporting.TextBox();
			this.textBox61 = new Telerik.Reporting.TextBox();
			this.ReiEntryData = new Telerik.Reporting.ObjectDataSource();
			this.textBox13 = new Telerik.Reporting.TextBox();
			this.txtDataSourceLabel = new Telerik.Reporting.TextBox();
			this.textBox4 = new Telerik.Reporting.TextBox();
			this.textBox100 = new Telerik.Reporting.TextBox();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// groupFooterSection
			// 
			this.groupFooterSection.Height = Telerik.Reporting.Drawing.Unit.Inch(0.0520833320915699D);
			this.groupFooterSection.Name = "groupFooterSection";
			// 
			// groupHeaderSection
			// 
			this.groupHeaderSection.Height = Telerik.Reporting.Drawing.Unit.Inch(0.800000011920929D);
			this.groupHeaderSection.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox19,
            this.textBox48,
            this.textBox1,
            this.textBox2,
            this.textBox3,
            this.textBox5,
            this.textBox6});
			this.groupHeaderSection.Name = "groupHeaderSection";
			// 
			// textBox19
			// 
			this.textBox19.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.099999986588954926D));
			this.textBox19.Name = "textBox19";
			this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.2998428344726562D), Telerik.Reporting.Drawing.Unit.Inch(0.29996061325073242D));
			this.textBox19.Style.Color = System.Drawing.Color.Gray;
			this.textBox19.Style.Font.Name = "Segoe UI Light";
			this.textBox19.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(18D);
			this.textBox19.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox19.Value = "=Fields.Farm";
			// 
			// textBox48
			// 
			this.textBox48.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.5999605655670166D));
			this.textBox48.Name = "textBox48";
			this.textBox48.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3000000715255737D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
			this.textBox48.Style.Color = System.Drawing.Color.Gray;
			this.textBox48.Style.Font.Bold = true;
			this.textBox48.Style.Font.Name = "Segoe UI";
			this.textBox48.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox48.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox48.Value = "Field ";
			// 
			// textBox1
			// 
			this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.3000005483627319D), Telerik.Reporting.Drawing.Unit.Inch(0.5999605655670166D));
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4999210834503174D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
			this.textBox1.Style.Color = System.Drawing.Color.Gray;
			this.textBox1.Style.Font.Bold = true;
			this.textBox1.Style.Font.Name = "Segoe UI";
			this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox1.Value = "Crop Zone";
			// 
			// textBox2
			// 
			this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.8000004291534424D), Telerik.Reporting.Drawing.Unit.Inch(0.5999605655670166D));
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.499842643737793D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
			this.textBox2.Style.Color = System.Drawing.Color.Gray;
			this.textBox2.Style.Font.Bold = true;
			this.textBox2.Style.Font.Name = "Segoe UI";
			this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox2.Value = "Crop";
			// 
			// textBox3
			// 
			this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.2999224662780762D), Telerik.Reporting.Drawing.Unit.Inch(0.5999605655670166D));
			this.textBox3.Name = "textBox3";
			this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.49999937415122986D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
			this.textBox3.Style.Color = System.Drawing.Color.Gray;
			this.textBox3.Style.Font.Bold = true;
			this.textBox3.Style.Font.Name = "Segoe UI";
			this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox3.Value = "REI (h)";
			// 
			// textBox5
			// 
			this.textBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.8000006675720215D), Telerik.Reporting.Drawing.Unit.Inch(0.59996050596237183D));
			this.textBox5.Name = "textBox5";
			this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5998426675796509D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
			this.textBox5.Style.Color = System.Drawing.Color.Gray;
			this.textBox5.Style.Font.Bold = true;
			this.textBox5.Style.Font.Name = "Segoe UI";
			this.textBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox5.Value = "REI Expiration";
			// 
			// textBox6
			// 
			this.textBox6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.3999218940734863D), Telerik.Reporting.Drawing.Unit.Inch(0.59996038675308228D));
			this.textBox6.Name = "textBox6";
			this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0999990701675415D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
			this.textBox6.Style.Color = System.Drawing.Color.Gray;
			this.textBox6.Style.Font.Bold = true;
			this.textBox6.Style.Font.Name = "Segoe UI";
			this.textBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox6.Value = "Status";
			// 
			// pageHeaderSection1
			// 
			this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.800000011920929D);
			this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox13,
            this.txtDataSourceLabel,
            this.textBox4,
            this.textBox100});
			this.pageHeaderSection1.Name = "pageHeaderSection1";
			// 
			// detail
			// 
			formattingRule1.Filters.Add(new Telerik.Reporting.Filter("= RowNumber()%2", Telerik.Reporting.FilterOperator.Equal, "1"));
			formattingRule1.Style.BackgroundColor = System.Drawing.Color.Gainsboro;
			this.detail.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule1});
			this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(0.2001185417175293D);
			this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox7,
            this.textBox9,
            this.textBox10,
            this.textBox11,
            this.textBox12,
            this.shape1,
            this.textBox8});
			this.detail.Name = "detail";
			// 
			// textBox7
			// 
			this.textBox7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.8000006675720215D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
			this.textBox7.Name = "textBox7";
			this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5998426675796509D), Telerik.Reporting.Drawing.Unit.Inch(0.20007912814617157D));
			this.textBox7.Value = "=Fields.ReiExpiration";
			// 
			// textBox9
			// 
			this.textBox9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.2999224662780762D), Telerik.Reporting.Drawing.Unit.Inch(0D));
			this.textBox9.Name = "textBox9";
			this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.49999937415122986D), Telerik.Reporting.Drawing.Unit.Inch(0.2001185417175293D));
			this.textBox9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox9.Value = "=Fields.Rei";
			// 
			// textBox10
			// 
			this.textBox10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.8000004291534424D), Telerik.Reporting.Drawing.Unit.Inch(0D));
			this.textBox10.Name = "textBox10";
			this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.499842643737793D), Telerik.Reporting.Drawing.Unit.Inch(0.2001185417175293D));
			this.textBox10.Value = "=Fields.Crop";
			// 
			// textBox11
			// 
			this.textBox11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.3000005483627319D), Telerik.Reporting.Drawing.Unit.Inch(3.9577484130859375E-05D));
			this.textBox11.Name = "textBox11";
			this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4999212026596069D), Telerik.Reporting.Drawing.Unit.Inch(0.20007896423339844D));
			this.textBox11.Value = "=Fields.CropZone";
			// 
			// textBox12
			// 
			this.textBox12.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(3.9577484130859375E-05D));
			this.textBox12.Name = "textBox12";
			this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3000000715255737D), Telerik.Reporting.Drawing.Unit.Inch(0.20007896423339844D));
			this.textBox12.Value = "=Fields.Field";
			// 
			// shape1
			// 
			formattingRule2.Filters.Add(new Telerik.Reporting.Filter("=Fields.IsAllowed", Telerik.Reporting.FilterOperator.Equal, "true"));
			formattingRule2.Style.BackgroundColor = System.Drawing.Color.LimeGreen;
			formattingRule3.Filters.Add(new Telerik.Reporting.Filter("=Fields.IsAllowed", Telerik.Reporting.FilterOperator.Equal, "false"));
			formattingRule3.Style.BackgroundColor = System.Drawing.Color.Red;
			this.shape1.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule2,
            formattingRule3});
			this.shape1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.3999218940734863D), Telerik.Reporting.Drawing.Unit.Inch(3.9577484130859375E-05D));
			this.shape1.Name = "shape1";
			this.shape1.ShapeType = new Telerik.Reporting.Drawing.Shapes.EllipseShape(0D);
			this.shape1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.299999862909317D), Telerik.Reporting.Drawing.Unit.Inch(0.20007896423339844D));
			this.shape1.Style.BackgroundColor = System.Drawing.Color.Transparent;
			this.shape1.Style.BorderColor.Default = System.Drawing.Color.Transparent;
			this.shape1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			// 
			// textBox8
			// 
			this.textBox8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.7000007629394531D), Telerik.Reporting.Drawing.Unit.Inch(3.9577484130859375E-05D));
			this.textBox8.Name = "textBox8";
			this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.799920380115509D), Telerik.Reporting.Drawing.Unit.Inch(0.20007896423339844D));
			this.textBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox8.Value = "=Fields.Status";
			// 
			// pageFooterSection1
			// 
			this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.5479167103767395D);
			this.pageFooterSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pageInfoTextBox,
            this.textBox65,
            this.textBox61});
			this.pageFooterSection1.Name = "pageFooterSection1";
			// 
			// pageInfoTextBox
			// 
			this.pageInfoTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.1582546234130859D), Telerik.Reporting.Drawing.Unit.Inch(0.1458333283662796D));
			this.pageInfoTextBox.Name = "pageInfoTextBox";
			this.pageInfoTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.3416664600372314D), Telerik.Reporting.Drawing.Unit.Inch(0.29999962449073792D));
			this.pageInfoTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.pageInfoTextBox.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.pageInfoTextBox.StyleName = "PageInfo";
			this.pageInfoTextBox.Value = "=PageNumber";
			// 
			// textBox65
			// 
			this.textBox65.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.65625D), Telerik.Reporting.Drawing.Unit.Inch(0.1458333283662796D));
			this.textBox65.Name = "textBox65";
			this.textBox65.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0937893390655518D), Telerik.Reporting.Drawing.Unit.Inch(0.29996046423912048D));
			this.textBox65.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox65.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox65.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox65.Value = "generated on {Now()}";
			// 
			// textBox61
			// 
			this.textBox61.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D), Telerik.Reporting.Drawing.Unit.Inch(0.09087371826171875D));
			this.textBox61.Name = "textBox61";
			this.textBox61.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8790887594223023D), Telerik.Reporting.Drawing.Unit.Inch(0.29996046423912048D));
			this.textBox61.Style.Color = System.Drawing.Color.DimGray;
			this.textBox61.Style.Font.Bold = true;
			this.textBox61.Style.Font.Italic = false;
			this.textBox61.Style.Font.Name = "Segoe UI Light";
			this.textBox61.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(16D);
			this.textBox61.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox61.Value = "AG CONNECTIONS";
			// 
			// ReiEntryData
			// 
			this.ReiEntryData.DataSource = "Landdb.ReportModels.REIPHI.CropZoneItem, Landdb, Version=12.0.2.0, Culture=neutra" +
    "l, PublicKeyToken=null";
			this.ReiEntryData.Name = "ReiEntryData";
			// 
			// textBox13
			// 
			this.textBox13.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.5312886238098145D), Telerik.Reporting.Drawing.Unit.Inch(0.20003950595855713D));
			this.textBox13.Name = "textBox13";
			this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.4687116146087646D), Telerik.Reporting.Drawing.Unit.Inch(0.29996061325073242D));
			this.textBox13.Style.Color = System.Drawing.Color.DimGray;
			this.textBox13.Style.Font.Name = "Segoe UI Light";
			this.textBox13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(18D);
			this.textBox13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox13.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox13.TextWrap = false;
			this.textBox13.Value = "REI Re-Entry Summary Report";
			// 
			// txtDataSourceLabel
			// 
			this.txtDataSourceLabel.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
			this.txtDataSourceLabel.Name = "txtDataSourceLabel";
			this.txtDataSourceLabel.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.0312089920043945D), Telerik.Reporting.Drawing.Unit.Inch(0.50000005960464478D));
			this.txtDataSourceLabel.Style.Font.Name = "Segoe UI Light";
			this.txtDataSourceLabel.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(24D);
			this.txtDataSourceLabel.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.txtDataSourceLabel.Value = "=Fields.DataSourceName";
			// 
			// textBox4
			// 
			this.textBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.50007873773574829D));
			this.textBox4.Name = "textBox4";
			this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.0312089920043945D), Telerik.Reporting.Drawing.Unit.Inch(0.19996063411235809D));
			this.textBox4.Style.Color = System.Drawing.Color.DimGray;
			this.textBox4.Style.Font.Name = "Segoe UI";
			this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox4.TextWrap = false;
			this.textBox4.Value = "=Fields.DateRangeDisplay";
			// 
			// textBox100
			// 
			this.textBox100.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.0000786781311035D), Telerik.Reporting.Drawing.Unit.Inch(0.50007891654968262D));
			this.textBox100.Name = "textBox100";
			this.textBox100.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9998817443847656D), Telerik.Reporting.Drawing.Unit.Inch(0.19996063411235809D));
			this.textBox100.Style.Color = System.Drawing.Color.DimGray;
			this.textBox100.Style.Font.Name = "Segoe UI";
			this.textBox100.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox100.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox100.TextWrap = false;
			this.textBox100.Value = "=Fields.CropYear";
			// 
			// ReEntrySummaryReport
			// 
			this.DataSource = this.ReiEntryData;
			group1.GroupFooter = this.groupFooterSection;
			group1.GroupHeader = this.groupHeaderSection;
			group1.Groupings.Add(new Telerik.Reporting.Grouping("=Fields.Farm"));
			group1.Name = "group";
			this.Groups.AddRange(new Telerik.Reporting.Group[] {
            group1});
			this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.groupHeaderSection,
            this.groupFooterSection,
            this.pageHeaderSection1,
            this.detail,
            this.pageFooterSection1});
			this.Name = "ReEntrySummaryReport";
			this.PageSettings.Landscape = false;
			this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(0.25D), Telerik.Reporting.Drawing.Unit.Inch(0.25D), Telerik.Reporting.Drawing.Unit.Inch(0.25D), Telerik.Reporting.Drawing.Unit.Inch(0.25D));
			this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
			this.Style.BackgroundColor = System.Drawing.Color.White;
			styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
			styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
			this.Width = Telerik.Reporting.Drawing.Unit.Inch(8D);
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PageFooterSection pageFooterSection1;
        private Telerik.Reporting.GroupHeaderSection groupHeaderSection;
        private Telerik.Reporting.GroupFooterSection groupFooterSection;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox48;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox pageInfoTextBox;
        private Telerik.Reporting.TextBox textBox65;
        private Telerik.Reporting.ObjectDataSource ReiEntryData;
        private Telerik.Reporting.Shape shape1;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox61;
		private Telerik.Reporting.TextBox textBox13;
		private Telerik.Reporting.TextBox txtDataSourceLabel;
		private Telerik.Reporting.TextBox textBox4;
		private Telerik.Reporting.TextBox textBox100;
	}
}