namespace Landdb.Views.Secondary.Reports.AppliedProduct
{
    partial class FertilizerDetailReport
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
			this.textBox20 = new Telerik.Reporting.TextBox();
			this.textBox29 = new Telerik.Reporting.TextBox();
			this.textBox30 = new Telerik.Reporting.TextBox();
			this.textBox11 = new Telerik.Reporting.TextBox();
			this.textBox13 = new Telerik.Reporting.TextBox();
			this.textBox6 = new Telerik.Reporting.TextBox();
			this.textBox35 = new Telerik.Reporting.TextBox();
			this.textBox44 = new Telerik.Reporting.TextBox();
			this.textBox45 = new Telerik.Reporting.TextBox();
			this.textBox46 = new Telerik.Reporting.TextBox();
			this.textBox47 = new Telerik.Reporting.TextBox();
			this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
			this.textBox93 = new Telerik.Reporting.TextBox();
			this.textBox3 = new Telerik.Reporting.TextBox();
			this.textBox1 = new Telerik.Reporting.TextBox();
			this.textBox2 = new Telerik.Reporting.TextBox();
			this.txtDataSourceLabel = new Telerik.Reporting.TextBox();
			this.textBox4 = new Telerik.Reporting.TextBox();
			this.textBox100 = new Telerik.Reporting.TextBox();
			this.detail = new Telerik.Reporting.DetailSection();
			this.textBox34 = new Telerik.Reporting.TextBox();
			this.table2 = new Telerik.Reporting.Table();
			this.textBox21 = new Telerik.Reporting.TextBox();
			this.textBox22 = new Telerik.Reporting.TextBox();
			this.textBox23 = new Telerik.Reporting.TextBox();
			this.textBox5 = new Telerik.Reporting.TextBox();
			this.textBox12 = new Telerik.Reporting.TextBox();
			this.textBox7 = new Telerik.Reporting.TextBox();
			this.textBox49 = new Telerik.Reporting.TextBox();
			this.table3 = new Telerik.Reporting.Table();
			this.textBox36 = new Telerik.Reporting.TextBox();
			this.textBox37 = new Telerik.Reporting.TextBox();
			this.textBox38 = new Telerik.Reporting.TextBox();
			this.textBox40 = new Telerik.Reporting.TextBox();
			this.textBox41 = new Telerik.Reporting.TextBox();
			this.textBox9 = new Telerik.Reporting.TextBox();
			this.textBox10 = new Telerik.Reporting.TextBox();
			this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
			this.textBox65 = new Telerik.Reporting.TextBox();
			this.pageInfoTextBox = new Telerik.Reporting.TextBox();
			this.textBox61 = new Telerik.Reporting.TextBox();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// textBox20
			// 
			this.textBox20.Name = "textBox20";
			this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.81671684980392456D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666716337204D));
			this.textBox20.Style.Color = System.Drawing.Color.Gray;
			this.textBox20.Style.Font.Bold = true;
			this.textBox20.Style.Font.Name = "Segoe UI";
			this.textBox20.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox20.Value = "DATE";
			// 
			// textBox29
			// 
			this.textBox29.Name = "textBox29";
			this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8320367336273193D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666716337204D));
			this.textBox29.Style.Color = System.Drawing.Color.Gray;
			this.textBox29.Style.Font.Bold = true;
			this.textBox29.Style.Font.Name = "Segoe UI";
			this.textBox29.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox29.Value = "PRODUCT";
			// 
			// textBox30
			// 
			this.textBox30.Name = "textBox30";
			this.textBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.63820075988769531D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666716337204D));
			this.textBox30.Style.Color = System.Drawing.Color.Gray;
			this.textBox30.Style.Font.Bold = true;
			this.textBox30.Style.Font.Name = "Segoe UI";
			this.textBox30.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox30.Value = "RATE";
			// 
			// textBox11
			// 
			this.textBox11.Name = "textBox11";
			this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0153181552886963D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666716337204D));
			this.textBox11.Style.Color = System.Drawing.Color.Gray;
			this.textBox11.Style.Font.Bold = true;
			this.textBox11.Style.Font.Name = "Segoe UI";
			this.textBox11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox11.StyleName = "";
			this.textBox11.Value = "AREA";
			// 
			// textBox13
			// 
			this.textBox13.Name = "textBox13";
			this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.74754196405410767D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666716337204D));
			this.textBox13.Style.Color = System.Drawing.Color.Gray;
			this.textBox13.Style.Font.Bold = true;
			this.textBox13.Style.Font.Name = "Segoe UI";
			this.textBox13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox13.StyleName = "";
			this.textBox13.Value = "TOTAL";
			// 
			// textBox6
			// 
			this.textBox6.Name = "textBox6";
			this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.8562793731689453D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666716337204D));
			this.textBox6.Style.Color = System.Drawing.Color.Gray;
			this.textBox6.Style.Font.Bold = true;
			this.textBox6.Style.Font.Name = "Segoe UI";
			this.textBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox6.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox6.StyleName = "";
			this.textBox6.Value = "Units / Area";
			// 
			// textBox35
			// 
			this.textBox35.Name = "textBox35";
			this.textBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.93288981914520264D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666716337204D));
			this.textBox35.Style.Color = System.Drawing.Color.Gray;
			this.textBox35.Style.Font.Bold = true;
			this.textBox35.Style.Font.Name = "Segoe UI";
			this.textBox35.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox35.Value = "DATE";
			// 
			// textBox44
			// 
			this.textBox44.Name = "textBox44";
			this.textBox44.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8357453346252441D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666716337204D));
			this.textBox44.Style.Color = System.Drawing.Color.Gray;
			this.textBox44.Style.Font.Bold = true;
			this.textBox44.Style.Font.Name = "Segoe UI";
			this.textBox44.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox44.Value = "Variety";
			// 
			// textBox45
			// 
			this.textBox45.Name = "textBox45";
			this.textBox45.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7776604890823364D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666716337204D));
			this.textBox45.Style.Color = System.Drawing.Color.Gray;
			this.textBox45.Style.Font.Bold = true;
			this.textBox45.Style.Font.Name = "Segoe UI";
			this.textBox45.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox45.Value = "RATE";
			// 
			// textBox46
			// 
			this.textBox46.Name = "textBox46";
			this.textBox46.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2044322490692139D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666716337204D));
			this.textBox46.Style.Color = System.Drawing.Color.Gray;
			this.textBox46.Style.Font.Bold = true;
			this.textBox46.Style.Font.Name = "Segoe UI";
			this.textBox46.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox46.StyleName = "";
			this.textBox46.Value = "AREA";
			// 
			// textBox47
			// 
			this.textBox47.Name = "textBox47";
			this.textBox47.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.1657812595367432D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666716337204D));
			this.textBox47.Style.Color = System.Drawing.Color.Gray;
			this.textBox47.Style.Font.Bold = true;
			this.textBox47.Style.Font.Name = "Segoe UI";
			this.textBox47.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox47.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox47.StyleName = "";
			this.textBox47.Value = "TOTAL";
			// 
			// pageHeaderSection1
			// 
			this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(1.1000000238418579D);
			this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox93,
            this.textBox3,
            this.textBox1,
            this.textBox2,
            this.txtDataSourceLabel,
            this.textBox4,
            this.textBox100});
			this.pageHeaderSection1.Name = "pageHeaderSection1";
			// 
			// textBox93
			// 
			this.textBox93.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.3273601531982422D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
			this.textBox93.Name = "textBox93";
			this.textBox93.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.6726005077362061D), Telerik.Reporting.Drawing.Unit.Inch(0.29996061325073242D));
			this.textBox93.Style.Color = System.Drawing.Color.DimGray;
			this.textBox93.Style.Font.Name = "Segoe UI Light";
			this.textBox93.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(18D);
			this.textBox93.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox93.TextWrap = false;
			this.textBox93.Value = "Fertilizer Detail Report";
			// 
			// textBox3
			// 
			this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.0000004768371582D), Telerik.Reporting.Drawing.Unit.Inch(0.699999988079071D));
			this.textBox3.Name = "textBox3";
			this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9998817443847656D), Telerik.Reporting.Drawing.Unit.Inch(0.29996061325073242D));
			this.textBox3.Style.Color = System.Drawing.Color.DimGray;
			this.textBox3.Style.Font.Name = "Segoe UI Light";
			this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(18D);
			this.textBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox3.TextWrap = false;
			this.textBox3.Value = "=Fields.AreaDisplay";
			// 
			// textBox1
			// 
			this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.0042448043823242188D), Telerik.Reporting.Drawing.Unit.Inch(0.90000003576278687D));
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(5.399960994720459D), Telerik.Reporting.Drawing.Unit.Inch(0.19996063411235809D));
			this.textBox1.Style.Font.Bold = false;
			this.textBox1.Style.Font.Name = "Segoe UI";
			this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox1.Value = "=Fields.CropDisplay";
			// 
			// textBox2
			// 
			this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.0042448043823242188D), Telerik.Reporting.Drawing.Unit.Inch(0.5D));
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(5.399960994720459D), Telerik.Reporting.Drawing.Unit.Inch(0.19996063411235809D));
			this.textBox2.Style.Font.Bold = true;
			this.textBox2.Style.Font.Name = "Segoe UI";
			this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox2.Value = "=Fields.FieldDisplay";
			// 
			// txtDataSourceLabel
			// 
			this.txtDataSourceLabel.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(3.9378803194267675E-05D));
			this.txtDataSourceLabel.Name = "txtDataSourceLabel";
			this.txtDataSourceLabel.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.0312089920043945D), Telerik.Reporting.Drawing.Unit.Inch(0.50000005960464478D));
			this.txtDataSourceLabel.Style.Font.Name = "Segoe UI Light";
			this.txtDataSourceLabel.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(24D);
			this.txtDataSourceLabel.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.txtDataSourceLabel.Value = "=Fields.DataSourceName";
			// 
			// textBox4
			// 
			this.textBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.70000004768371582D));
			this.textBox4.Name = "textBox4";
			this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.5D), Telerik.Reporting.Drawing.Unit.Inch(0.19996063411235809D));
			this.textBox4.Style.Color = System.Drawing.Color.DimGray;
			this.textBox4.Style.Font.Name = "Segoe UI";
			this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox4.TextWrap = false;
			this.textBox4.Value = "=Fields.DateRangeDisplay";
			// 
			// textBox100
			// 
			this.textBox100.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.0000004768371582D), Telerik.Reporting.Drawing.Unit.Inch(0.50000005960464478D));
			this.textBox100.Name = "textBox100";
			this.textBox100.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9998817443847656D), Telerik.Reporting.Drawing.Unit.Inch(0.19996063411235809D));
			this.textBox100.Style.Color = System.Drawing.Color.DimGray;
			this.textBox100.Style.Font.Name = "Segoe UI";
			this.textBox100.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox100.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox100.TextWrap = false;
			this.textBox100.Value = "=Fields.CropYear";
			// 
			// detail
			// 
			this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(2.2999999523162842D);
			this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox34,
            this.table2,
            this.textBox49,
            this.table3,
            this.textBox9,
            this.textBox10});
			this.detail.Name = "detail";
			// 
			// textBox34
			// 
			this.textBox34.Bindings.Add(new Telerik.Reporting.Binding("Visible", "=Fields.FertilizerLineItems.Count > 0"));
			this.textBox34.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D), Telerik.Reporting.Drawing.Unit.Inch(0.89999991655349731D));
			this.textBox34.Name = "textBox34";
			this.textBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0603783130645752D), Telerik.Reporting.Drawing.Unit.Inch(0.3999999463558197D));
			this.textBox34.Style.Color = System.Drawing.Color.Gray;
			this.textBox34.Style.Font.Name = "Segoe UI Light";
			this.textBox34.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(18D);
			this.textBox34.Value = "fertilizer";
			// 
			// table2
			// 
			this.table2.Bindings.Add(new Telerik.Reporting.Binding("DataSource", "=Fields.FertilizerLineItems"));
			this.table2.Bindings.Add(new Telerik.Reporting.Binding("Visible", "=Fields.FertilizerLineItems.Count > 0"));
			this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.81671690940856934D)));
			this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.8320366144180298D)));
			this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.63820075988769531D)));
			this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.0153181552886963D)));
			this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.74754202365875244D)));
			this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.8562793731689453D)));
			this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.20999991893768311D)));
			this.table2.Body.SetCellContent(0, 0, this.textBox21);
			this.table2.Body.SetCellContent(0, 1, this.textBox22);
			this.table2.Body.SetCellContent(0, 2, this.textBox23);
			this.table2.Body.SetCellContent(0, 3, this.textBox5);
			this.table2.Body.SetCellContent(0, 5, this.textBox12);
			this.table2.Body.SetCellContent(0, 4, this.textBox7);
			tableGroup1.ReportItem = this.textBox20;
			tableGroup2.ReportItem = this.textBox29;
			tableGroup3.ReportItem = this.textBox30;
			tableGroup4.Name = "group";
			tableGroup4.ReportItem = this.textBox11;
			tableGroup5.Name = "group1";
			tableGroup5.ReportItem = this.textBox13;
			tableGroup6.Name = "Group2";
			tableGroup6.ReportItem = this.textBox6;
			this.table2.ColumnGroups.Add(tableGroup1);
			this.table2.ColumnGroups.Add(tableGroup2);
			this.table2.ColumnGroups.Add(tableGroup3);
			this.table2.ColumnGroups.Add(tableGroup4);
			this.table2.ColumnGroups.Add(tableGroup5);
			this.table2.ColumnGroups.Add(tableGroup6);
			this.table2.DataSource = null;
			this.table2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox21,
            this.textBox22,
            this.textBox23,
            this.textBox5,
            this.textBox7,
            this.textBox12,
            this.textBox20,
            this.textBox29,
            this.textBox30,
            this.textBox11,
            this.textBox13,
            this.textBox6});
			this.table2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.093789421021938324D), Telerik.Reporting.Drawing.Unit.Inch(1.2958331108093262D));
			this.table2.Name = "table2";
			tableGroup7.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup7.Name = "DetailGroup";
			this.table2.RowGroups.Add(tableGroup7);
			this.table2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.9060935974121094D), Telerik.Reporting.Drawing.Unit.Inch(0.3766666054725647D));
			// 
			// textBox21
			// 
			this.textBox21.Name = "textBox21";
			this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.81671684980392456D), Telerik.Reporting.Drawing.Unit.Inch(0.20999991893768311D));
			this.textBox21.Style.Font.Name = "Segoe UI";
			this.textBox21.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox21.Value = "=Fields.StartDate";
			// 
			// textBox22
			// 
			this.textBox22.Name = "textBox22";
			this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8320367336273193D), Telerik.Reporting.Drawing.Unit.Inch(0.20999991893768311D));
			this.textBox22.Style.Font.Name = "Segoe UI";
			this.textBox22.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox22.Value = "=Fields.ProductDisplay";
			// 
			// textBox23
			// 
			this.textBox23.Name = "textBox23";
			this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.63820075988769531D), Telerik.Reporting.Drawing.Unit.Inch(0.20999991893768311D));
			this.textBox23.Style.Font.Name = "Segoe UI";
			this.textBox23.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox23.Value = "=Fields.RateDisplay";
			// 
			// textBox5
			// 
			this.textBox5.Name = "textBox5";
			this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0153181552886963D), Telerik.Reporting.Drawing.Unit.Inch(0.20999991893768311D));
			this.textBox5.Style.Font.Name = "Segoe UI";
			this.textBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox5.StyleName = "";
			this.textBox5.Value = "=Fields.AreaDisplay";
			// 
			// textBox12
			// 
			this.textBox12.Name = "textBox12";
			this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.8562793731689453D), Telerik.Reporting.Drawing.Unit.Inch(0.20999991893768311D));
			this.textBox12.Style.Font.Name = "Segoe UI";
			this.textBox12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox12.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox12.StyleName = "";
			this.textBox12.Value = "=Fields.FertilizerAnalysis";
			// 
			// textBox7
			// 
			this.textBox7.Name = "textBox7";
			this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.74754196405410767D), Telerik.Reporting.Drawing.Unit.Inch(0.20999991893768311D));
			this.textBox7.Style.Font.Name = "Segoe UI";
			this.textBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox7.StyleName = "";
			this.textBox7.Value = "=Fields.TotalProductDisplay";
			// 
			// textBox49
			// 
			this.textBox49.Bindings.Add(new Telerik.Reporting.Binding("Visible", "=Fields.SeedLineItems.Count > 0"));
			this.textBox49.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.0042448043823242188D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
			this.textBox49.Name = "textBox49";
			this.textBox49.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0603783130645752D), Telerik.Reporting.Drawing.Unit.Inch(0.3999999463558197D));
			this.textBox49.Style.Color = System.Drawing.Color.Gray;
			this.textBox49.Style.Font.Name = "Segoe UI Light";
			this.textBox49.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(18D);
			this.textBox49.Value = "planting";
			// 
			// table3
			// 
			this.table3.Bindings.Add(new Telerik.Reporting.Binding("DataSource", "=Fields.SeedLineItems"));
			this.table3.Bindings.Add(new Telerik.Reporting.Binding("Visible", "=Fields.SeedLineItems.Count > 0"));
			this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.93288981914520264D)));
			this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.8357453346252441D)));
			this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.777660608291626D)));
			this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.2044322490692139D)));
			this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.1657814979553223D)));
			this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.20999991893768311D)));
			this.table3.Body.SetCellContent(0, 0, this.textBox36);
			this.table3.Body.SetCellContent(0, 1, this.textBox37);
			this.table3.Body.SetCellContent(0, 2, this.textBox38);
			this.table3.Body.SetCellContent(0, 3, this.textBox40);
			this.table3.Body.SetCellContent(0, 4, this.textBox41);
			tableGroup8.ReportItem = this.textBox35;
			tableGroup9.ReportItem = this.textBox44;
			tableGroup10.ReportItem = this.textBox45;
			tableGroup11.Name = "Group2";
			tableGroup11.ReportItem = this.textBox46;
			tableGroup12.Name = "Group3";
			tableGroup12.ReportItem = this.textBox47;
			this.table3.ColumnGroups.Add(tableGroup8);
			this.table3.ColumnGroups.Add(tableGroup9);
			this.table3.ColumnGroups.Add(tableGroup10);
			this.table3.ColumnGroups.Add(tableGroup11);
			this.table3.ColumnGroups.Add(tableGroup12);
			this.table3.DataSource = null;
			this.table3.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox36,
            this.textBox37,
            this.textBox38,
            this.textBox40,
            this.textBox41,
            this.textBox35,
            this.textBox44,
            this.textBox45,
            this.textBox46,
            this.textBox47});
			this.table3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.083373703062534332D), Telerik.Reporting.Drawing.Unit.Inch(0.40011820197105408D));
			this.table3.Name = "table3";
			tableGroup13.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup13.Name = "DetailGroup";
			this.table3.RowGroups.Add(tableGroup13);
			this.table3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.91650915145874D), Telerik.Reporting.Drawing.Unit.Inch(0.3766666054725647D));
			// 
			// textBox36
			// 
			this.textBox36.Name = "textBox36";
			this.textBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.93288981914520264D), Telerik.Reporting.Drawing.Unit.Inch(0.2099999338388443D));
			this.textBox36.Style.Font.Name = "Segoe UI";
			this.textBox36.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox36.Value = "=Fields.StartDate";
			// 
			// textBox37
			// 
			this.textBox37.Name = "textBox37";
			this.textBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8357453346252441D), Telerik.Reporting.Drawing.Unit.Inch(0.2099999338388443D));
			this.textBox37.Style.Font.Name = "Segoe UI";
			this.textBox37.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox37.Value = "=Fields.ProductDisplay";
			// 
			// textBox38
			// 
			this.textBox38.Name = "textBox38";
			this.textBox38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7776604890823364D), Telerik.Reporting.Drawing.Unit.Inch(0.2099999338388443D));
			this.textBox38.Style.Font.Name = "Segoe UI";
			this.textBox38.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox38.Value = "=Fields.RateDisplay";
			// 
			// textBox40
			// 
			this.textBox40.Name = "textBox40";
			this.textBox40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2044322490692139D), Telerik.Reporting.Drawing.Unit.Inch(0.2099999338388443D));
			this.textBox40.Style.Font.Name = "Segoe UI";
			this.textBox40.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox40.StyleName = "";
			this.textBox40.Value = "=Fields.AreaDisplay";
			// 
			// textBox41
			// 
			this.textBox41.Name = "textBox41";
			this.textBox41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.1657812595367432D), Telerik.Reporting.Drawing.Unit.Inch(0.2099999338388443D));
			this.textBox41.Style.Font.Name = "Segoe UI";
			this.textBox41.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox41.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox41.StyleName = "";
			this.textBox41.Value = "=Fields.TotalProductDisplay";
			// 
			// textBox9
			// 
			this.textBox9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(1.8000000715255737D));
			this.textBox9.Name = "textBox9";
			this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2000001668930054D), Telerik.Reporting.Drawing.Unit.Inch(0.3999999463558197D));
			this.textBox9.Style.Color = System.Drawing.Color.Gray;
			this.textBox9.Style.Font.Name = "Segoe UI Light";
			this.textBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(18D);
			this.textBox9.Value = "Summary:";
			// 
			// textBox10
			// 
			this.textBox10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.2000789642333984D), Telerik.Reporting.Drawing.Unit.Inch(1.8000000715255737D));
			this.textBox10.Name = "textBox10";
			this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.2645459175109863D), Telerik.Reporting.Drawing.Unit.Inch(0.3999999463558197D));
			this.textBox10.Style.Color = System.Drawing.Color.Gray;
			this.textBox10.Style.Font.Name = "Segoe UI Light";
			this.textBox10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(18D);
			this.textBox10.Value = "=Fields.TotalFertilizer";
			// 
			// pageFooterSection1
			// 
			this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.5D);
			this.pageFooterSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox65,
            this.pageInfoTextBox,
            this.textBox61});
			this.pageFooterSection1.Name = "pageFooterSection1";
			// 
			// textBox65
			// 
			this.textBox65.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.1000001430511475D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
			this.textBox65.Name = "textBox65";
			this.textBox65.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0937893390655518D), Telerik.Reporting.Drawing.Unit.Inch(0.29996046423912048D));
			this.textBox65.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox65.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox65.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox65.Value = "generated on {Now()}";
			// 
			// pageInfoTextBox
			// 
			this.pageInfoTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.4352574348449707D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
			this.pageInfoTextBox.Name = "pageInfoTextBox";
			this.pageInfoTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.5646247863769531D), Telerik.Reporting.Drawing.Unit.Inch(0.29996052384376526D));
			this.pageInfoTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.pageInfoTextBox.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.pageInfoTextBox.StyleName = "PageInfo";
			this.pageInfoTextBox.Value = "=PageNumber";
			// 
			// textBox61
			// 
			this.textBox61.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
			this.textBox61.Name = "textBox61";
			this.textBox61.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8790887594223023D), Telerik.Reporting.Drawing.Unit.Inch(0.29996046423912048D));
			this.textBox61.Style.Color = System.Drawing.Color.DimGray;
			this.textBox61.Style.Font.Bold = true;
			this.textBox61.Style.Font.Italic = false;
			this.textBox61.Style.Font.Name = "Segoe UI Light";
			this.textBox61.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(16D);
			this.textBox61.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox61.Value = "AG CONNECTIONS";
			// 
			// FertilizerDetailReport
			// 
			this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pageHeaderSection1,
            this.detail,
            this.pageFooterSection1});
			this.Name = "FertilizerDetailReport";
			this.PageSettings.Landscape = false;
			this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(0.25D), Telerik.Reporting.Drawing.Unit.Inch(0.25D), Telerik.Reporting.Drawing.Unit.Inch(0.25D), Telerik.Reporting.Drawing.Unit.Inch(0.25D));
			this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
			this.Style.BackgroundColor = System.Drawing.Color.White;
			styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
			styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
			this.Width = Telerik.Reporting.Drawing.Unit.Inch(8D);
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PageFooterSection pageFooterSection1;
        private Telerik.Reporting.TextBox textBox93;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox34;
        private Telerik.Reporting.Table table2;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.TextBox textBox29;
        private Telerik.Reporting.TextBox textBox30;
        private Telerik.Reporting.TextBox textBox49;
        private Telerik.Reporting.Table table3;
        private Telerik.Reporting.TextBox textBox36;
        private Telerik.Reporting.TextBox textBox37;
        private Telerik.Reporting.TextBox textBox38;
        private Telerik.Reporting.TextBox textBox40;
        private Telerik.Reporting.TextBox textBox41;
        private Telerik.Reporting.TextBox textBox35;
        private Telerik.Reporting.TextBox textBox44;
        private Telerik.Reporting.TextBox textBox45;
        private Telerik.Reporting.TextBox textBox46;
        private Telerik.Reporting.TextBox textBox47;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox65;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox pageInfoTextBox;
        private Telerik.Reporting.TextBox textBox61;
		private Telerik.Reporting.TextBox txtDataSourceLabel;
		private Telerik.Reporting.TextBox textBox4;
		private Telerik.Reporting.TextBox textBox100;
	}
}