namespace Landdb.Views.Secondary.Reports.AppliedProduct
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;

    /// <summary>
    /// Summary description for AppliedProductSummary.
    /// </summary>
    public partial class AppliedProductSummary : Telerik.Reporting.Report
    {
        public AppliedProductSummary()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }
    }
}