namespace Landdb.Views.Secondary.Reports.Application
{
    partial class PortraitApplication
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.Reporting.Drawing.FormattingRule formattingRule1 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.InstanceReportSource instanceReportSource1 = new Telerik.Reporting.InstanceReportSource();
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.Drawing.FormattingRule formattingRule2 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.Drawing.FormattingRule formattingRule3 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.portraitProductsSubReport1 = new Landdb.Views.Secondary.Reports.Application.PortraitProductsSubReport();
            this.textBox70 = new Telerik.Reporting.TextBox();
            this.textBox72 = new Telerik.Reporting.TextBox();
            this.textBox55 = new Telerik.Reporting.TextBox();
            this.textBox57 = new Telerik.Reporting.TextBox();
            this.textBox59 = new Telerik.Reporting.TextBox();
            this.textBox63 = new Telerik.Reporting.TextBox();
            this.textBox61 = new Telerik.Reporting.TextBox();
            this.textBox65 = new Telerik.Reporting.TextBox();
            this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            this.AuthorizeDateTXT = new Telerik.Reporting.TextBox();
            this.AuthorizerTXT = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.txtDataSourceLabel = new Telerik.Reporting.TextBox();
            this.textBox93 = new Telerik.Reporting.TextBox();
            this.textBox100 = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.subReport1 = new Telerik.Reporting.SubReport();
            this.textBox90 = new Telerik.Reporting.TextBox();
            this.textBox89 = new Telerik.Reporting.TextBox();
            this.textBox83 = new Telerik.Reporting.TextBox();
            this.textBox84 = new Telerik.Reporting.TextBox();
            this.textBox82 = new Telerik.Reporting.TextBox();
            this.textBox81 = new Telerik.Reporting.TextBox();
            this.textBox80 = new Telerik.Reporting.TextBox();
            this.textBox85 = new Telerik.Reporting.TextBox();
            this.textBox86 = new Telerik.Reporting.TextBox();
            this.textBox87 = new Telerik.Reporting.TextBox();
            this.textBox88 = new Telerik.Reporting.TextBox();
            this.textBox68 = new Telerik.Reporting.TextBox();
            this.textBox62 = new Telerik.Reporting.TextBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox29 = new Telerik.Reporting.TextBox();
            this.textBox28 = new Telerik.Reporting.TextBox();
            this.textBox27 = new Telerik.Reporting.TextBox();
            this.textBox26 = new Telerik.Reporting.TextBox();
            this.textBox25 = new Telerik.Reporting.TextBox();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.table3 = new Telerik.Reporting.Table();
            this.textBox71 = new Telerik.Reporting.TextBox();
            this.textBox73 = new Telerik.Reporting.TextBox();
            this.textBox76 = new Telerik.Reporting.TextBox();
            this.textBox75 = new Telerik.Reporting.TextBox();
            this.pictureBox2 = new Telerik.Reporting.PictureBox();
            this.textBox69 = new Telerik.Reporting.TextBox();
            this.textBox74 = new Telerik.Reporting.TextBox();
            this.textBox67 = new Telerik.Reporting.TextBox();
            this.table2 = new Telerik.Reporting.Table();
            this.textBox52 = new Telerik.Reporting.TextBox();
            this.textBox50 = new Telerik.Reporting.TextBox();
            this.textBox56 = new Telerik.Reporting.TextBox();
            this.textBox58 = new Telerik.Reporting.TextBox();
            this.textBox60 = new Telerik.Reporting.TextBox();
            this.textBox64 = new Telerik.Reporting.TextBox();
            this.textBox54 = new Telerik.Reporting.TextBox();
            this.textBox53 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.TimingEventTXB = new Telerik.Reporting.TextBox();
            this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
            this.textBox104 = new Telerik.Reporting.TextBox();
            this.pageInfoTextBox = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.portraitProductsSubReport1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // portraitProductsSubReport1
            // 
            this.portraitProductsSubReport1.Name = "PortraitProductsSubReport";
            // 
            // textBox70
            // 
            this.textBox70.Name = "textBox70";
            this.textBox70.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9013861417770386D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666567325592D));
            this.textBox70.Style.Font.Bold = true;
            this.textBox70.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox70.Style.Font.Underline = true;
            this.textBox70.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox70.Value = "Name/Company";
            // 
            // textBox72
            // 
            this.textBox72.Name = "textBox72";
            this.textBox72.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5694091320037842D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666567325592D));
            this.textBox72.Style.Font.Bold = true;
            this.textBox72.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox72.Style.Font.Underline = true;
            this.textBox72.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox72.Value = "License No.";
            // 
            // textBox55
            // 
            this.textBox55.Name = "textBox55";
            this.textBox55.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3999998569488525D), Telerik.Reporting.Drawing.Unit.Inch(0.21000002324581146D));
            this.textBox55.Style.Font.Bold = true;
            this.textBox55.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox55.Value = "Farm";
            // 
            // textBox57
            // 
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4729170799255371D), Telerik.Reporting.Drawing.Unit.Inch(0.21000002324581146D));
            this.textBox57.Style.Font.Bold = true;
            this.textBox57.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox57.Value = "Field";
            // 
            // textBox59
            // 
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5354166030883789D), Telerik.Reporting.Drawing.Unit.Inch(0.21000002324581146D));
            this.textBox59.Style.Font.Bold = true;
            this.textBox59.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox59.Value = "Crop Zone";
            // 
            // textBox63
            // 
            this.textBox63.Name = "textBox63";
            this.textBox63.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4083338975906372D), Telerik.Reporting.Drawing.Unit.Inch(0.21000002324581146D));
            this.textBox63.Style.Font.Bold = true;
            this.textBox63.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox63.StyleName = "";
            this.textBox63.Value = "Crop";
            // 
            // textBox61
            // 
            this.textBox61.Name = "textBox61";
            this.textBox61.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5000001192092896D), Telerik.Reporting.Drawing.Unit.Inch(0.21000002324581146D));
            this.textBox61.Style.Font.Bold = true;
            this.textBox61.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox61.StyleName = "";
            this.textBox61.Value = "Center Lat/Long";
            // 
            // textBox65
            // 
            this.textBox65.Name = "textBox65";
            this.textBox65.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.67500025033950806D), Telerik.Reporting.Drawing.Unit.Inch(0.21000002324581146D));
            this.textBox65.Style.Font.Bold = true;
            this.textBox65.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox65.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox65.StyleName = "";
            this.textBox65.Value = "App Area";
            // 
            // pageHeaderSection1
            // 
            this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(1.3000000715255737D);
            this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.AuthorizeDateTXT,
            this.AuthorizerTXT,
            this.textBox9,
            this.textBox3,
            this.txtDataSourceLabel,
            this.textBox93,
            this.textBox100});
            this.pageHeaderSection1.Name = "pageHeaderSection1";
            // 
            // AuthorizeDateTXT
            // 
            formattingRule1.Filters.Add(new Telerik.Reporting.Filter("=Fields.AuthorizerDate", Telerik.Reporting.FilterOperator.LessThan, "1901-01-01"));
            formattingRule1.Style.Visible = false;
            this.AuthorizeDateTXT.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule1});
            this.AuthorizeDateTXT.Format = "{0:d}";
            this.AuthorizeDateTXT.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.6097664833068848D), Telerik.Reporting.Drawing.Unit.Inch(1.004324197769165D));
            this.AuthorizeDateTXT.Name = "AuthorizeDateTXT";
            this.AuthorizeDateTXT.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.3935921192169189D), Telerik.Reporting.Drawing.Unit.Inch(0.21025960147380829D));
            this.AuthorizeDateTXT.Style.Color = System.Drawing.Color.DimGray;
            this.AuthorizeDateTXT.Style.Font.Name = "Segoe UI";
            this.AuthorizeDateTXT.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.AuthorizeDateTXT.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.AuthorizeDateTXT.Value = "=Fields.AuthorizerDate";
            // 
            // AuthorizerTXT
            // 
            this.AuthorizerTXT.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.2521531581878662D), Telerik.Reporting.Drawing.Unit.Inch(0.70011806488037109D));
            this.AuthorizerTXT.Name = "AuthorizerTXT";
            this.AuthorizerTXT.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.764589786529541D), Telerik.Reporting.Drawing.Unit.Inch(0.30412733554840088D));
            this.AuthorizerTXT.Style.Color = System.Drawing.Color.DimGray;
            this.AuthorizerTXT.Style.Font.Name = "Segoe UI Light";
            this.AuthorizerTXT.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(18D);
            this.AuthorizerTXT.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.AuthorizerTXT.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.AuthorizerTXT.Value = "=Fields.Authorizer";
            // 
            // textBox9
            // 
            this.textBox9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D), Telerik.Reporting.Drawing.Unit.Inch(0.50783973932266235D));
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.0812108516693115D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox9.Style.Font.Bold = true;
            this.textBox9.Style.Font.Name = "Segoe UI";
            this.textBox9.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox9.Value = "=Fields.ApplicationName";
            // 
            // textBox3
            // 
            this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D), Telerik.Reporting.Drawing.Unit.Inch(0.70791846513748169D));
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.0812108516693115D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox3.Style.Font.Name = "Segoe UI";
            this.textBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox3.Value = "=Fields.ApplicationDate";
            // 
            // txtDataSourceLabel
            // 
            this.txtDataSourceLabel.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(3.9378803194267675E-05D));
            this.txtDataSourceLabel.Name = "txtDataSourceLabel";
            this.txtDataSourceLabel.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.0312089920043945D), Telerik.Reporting.Drawing.Unit.Inch(0.50000005960464478D));
            this.txtDataSourceLabel.Style.Font.Name = "Segoe UI Light";
            this.txtDataSourceLabel.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(24D);
            this.txtDataSourceLabel.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.txtDataSourceLabel.Value = "=Fields.DataSourceName";
            // 
            // textBox93
            // 
            this.textBox93.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.5480313301086426D), Telerik.Reporting.Drawing.Unit.Inch(0.20003946125507355D));
            this.textBox93.Name = "textBox93";
            this.textBox93.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.4687116146087646D), Telerik.Reporting.Drawing.Unit.Inch(0.29996061325073242D));
            this.textBox93.Style.Color = System.Drawing.Color.DimGray;
            this.textBox93.Style.Font.Name = "Segoe UI Light";
            this.textBox93.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(18D);
            this.textBox93.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox93.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox93.TextWrap = false;
            this.textBox93.Value = "Application";
            // 
            // textBox100
            // 
            this.textBox100.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.0001182556152344D), Telerik.Reporting.Drawing.Unit.Inch(0.50007873773574829D));
            this.textBox100.Name = "textBox100";
            this.textBox100.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9998817443847656D), Telerik.Reporting.Drawing.Unit.Inch(0.19996063411235809D));
            this.textBox100.Style.Color = System.Drawing.Color.DimGray;
            this.textBox100.Style.Font.Name = "Segoe UI";
            this.textBox100.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox100.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox100.TextWrap = false;
            this.textBox100.Value = "=Fields.CropYear";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(6.3000006675720215D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.subReport1,
            this.textBox90,
            this.textBox89,
            this.textBox83,
            this.textBox84,
            this.textBox82,
            this.textBox81,
            this.textBox80,
            this.textBox85,
            this.textBox86,
            this.textBox87,
            this.textBox88,
            this.textBox68,
            this.textBox62,
            this.textBox10,
            this.textBox8,
            this.textBox7,
            this.textBox29,
            this.textBox28,
            this.textBox27,
            this.textBox26,
            this.textBox25,
            this.textBox24,
            this.textBox23,
            this.textBox22,
            this.textBox14,
            this.textBox13,
            this.textBox12,
            this.textBox11,
            this.table3,
            this.pictureBox2,
            this.textBox69,
            this.textBox74,
            this.textBox67,
            this.table2,
            this.textBox54,
            this.textBox53,
            this.textBox5,
            this.textBox19,
            this.textBox6,
            this.TimingEventTXB});
            this.detail.Name = "detail";
            // 
            // subReport1
            // 
            this.subReport1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(4.1999998092651367D));
            this.subReport1.Name = "subReport1";
            instanceReportSource1.ReportDocument = this.portraitProductsSubReport1;
            this.subReport1.ReportSource = instanceReportSource1;
            this.subReport1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.9660720825195312D), Telerik.Reporting.Drawing.Unit.Inch(0.299999475479126D));
            // 
            // textBox90
            // 
            this.textBox90.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.013303597457706928D), Telerik.Reporting.Drawing.Unit.Inch(1.8000001907348633D));
            this.textBox90.Name = "textBox90";
            this.textBox90.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.4707953929901123D), Telerik.Reporting.Drawing.Unit.Inch(0.2022802084684372D));
            this.textBox90.Style.BackgroundColor = System.Drawing.Color.Silver;
            this.textBox90.Style.Color = System.Drawing.Color.Black;
            this.textBox90.Style.Font.Bold = true;
            this.textBox90.Style.Font.Name = "Segoe UI";
            this.textBox90.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox90.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox90.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox90.Value = "Environmental";
            // 
            // textBox89
            // 
            this.textBox89.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.013304074294865131D), Telerik.Reporting.Drawing.Unit.Inch(2.2000789642333984D));
            this.textBox89.Name = "textBox89";
            this.textBox89.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0790879726409912D), Telerik.Reporting.Drawing.Unit.Inch(0.19992123544216156D));
            this.textBox89.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox89.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox89.Style.Font.Bold = true;
            this.textBox89.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox89.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox89.Value = "Soil Condition:";
            // 
            // textBox83
            // 
            this.textBox83.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.013303915970027447D), Telerik.Reporting.Drawing.Unit.Inch(2.0336093902587891D));
            this.textBox83.Name = "textBox83";
            this.textBox83.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0790880918502808D), Telerik.Reporting.Drawing.Unit.Inch(0.177719384431839D));
            this.textBox83.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox83.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox83.Style.Font.Bold = true;
            this.textBox83.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox83.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox83.Value = "Sky Condition:";
            // 
            // textBox84
            // 
            this.textBox84.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.694908618927002D), Telerik.Reporting.Drawing.Unit.Inch(3.4979171752929688D));
            this.textBox84.Name = "textBox84";
            this.textBox84.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.936177670955658D), Telerik.Reporting.Drawing.Unit.Inch(0.297640323638916D));
            this.textBox84.Style.Font.Bold = true;
            this.textBox84.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox84.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox84.Value = "Carrier/Area:";
            // 
            // textBox82
            // 
            this.textBox82.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(3.4979171752929688D));
            this.textBox82.Name = "textBox82";
            this.textBox82.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.40933555364608765D), Telerik.Reporting.Drawing.Unit.Inch(0.29764065146446228D));
            this.textBox82.Style.Font.Bold = true;
            this.textBox82.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox82.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox82.Value = "Size:";
            // 
            // textBox81
            // 
            this.textBox81.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.39066442847251892D), Telerik.Reporting.Drawing.Unit.Inch(3.4979171752929688D));
            this.textBox81.Name = "textBox81";
            this.textBox81.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.88525092601776123D), Telerik.Reporting.Drawing.Unit.Inch(0.29764050245285034D));
            this.textBox81.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox81.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox81.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox81.Value = "=Fields.TankSizeDisplay";
            // 
            // textBox80
            // 
            this.textBox80.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.6428253650665283D), Telerik.Reporting.Drawing.Unit.Inch(3.4979171752929688D));
            this.textBox80.Name = "textBox80";
            this.textBox80.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.81887310743331909D), Telerik.Reporting.Drawing.Unit.Inch(0.29764050245285034D));
            this.textBox80.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox80.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox80.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox80.Value = "=Fields.CarrierPerAreaDisplay";
            // 
            // textBox85
            // 
            this.textBox85.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.91574239730835D), Telerik.Reporting.Drawing.Unit.Inch(3.5023586750030518D));
            this.textBox85.Name = "textBox85";
            this.textBox85.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0852508544921875D), Telerik.Reporting.Drawing.Unit.Inch(0.29764050245285034D));
            this.textBox85.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox85.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox85.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox85.Value = "=Fields.TotalCarrierDisplay";
            // 
            // textBox86
            // 
            this.textBox86.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.8949089050292969D), Telerik.Reporting.Drawing.Unit.Inch(3.5023586750030518D));
            this.textBox86.Name = "textBox86";
            this.textBox86.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0145909786224365D), Telerik.Reporting.Drawing.Unit.Inch(0.297640323638916D));
            this.textBox86.Style.Font.Bold = true;
            this.textBox86.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox86.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox86.Value = "Total Carrier:";
            // 
            // textBox87
            // 
            this.textBox87.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.3949089050292969D), Telerik.Reporting.Drawing.Unit.Inch(3.4979171752929688D));
            this.textBox87.Name = "textBox87";
            this.textBox87.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.87917429208755493D), Telerik.Reporting.Drawing.Unit.Inch(0.297640323638916D));
            this.textBox87.Style.Font.Bold = true;
            this.textBox87.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox87.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox87.Value = "Tank Count:";
            // 
            // textBox88
            // 
            this.textBox88.Format = "{0:N2}";
            this.textBox88.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(7.2699089050292969D), Telerik.Reporting.Drawing.Unit.Inch(3.4979171752929688D));
            this.textBox88.Name = "textBox88";
            this.textBox88.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.70216244459152222D), Telerik.Reporting.Drawing.Unit.Inch(0.29764050245285034D));
            this.textBox88.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox88.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox88.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox88.Value = "=Fields.TankCount";
            // 
            // textBox68
            // 
            this.textBox68.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.013304074294865131D), Telerik.Reporting.Drawing.Unit.Inch(2.4000790119171143D));
            this.textBox68.Name = "textBox68";
            this.textBox68.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0207966566085815D), Telerik.Reporting.Drawing.Unit.Inch(0.20700450241565704D));
            this.textBox68.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox68.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox68.Style.Font.Bold = true;
            this.textBox68.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox68.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox68.Value = "Wind Speed:";
            // 
            // textBox62
            // 
            this.textBox62.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.013938267715275288D), Telerik.Reporting.Drawing.Unit.Inch(3.0000002384185791D));
            this.textBox62.Name = "textBox62";
            this.textBox62.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1701598167419434D), Telerik.Reporting.Drawing.Unit.Inch(0.19992092251777649D));
            this.textBox62.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox62.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox62.Style.Font.Bold = true;
            this.textBox62.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox62.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox62.Value = "Humidity:";
            // 
            // textBox10
            // 
            this.textBox10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.013304233551025391D), Telerik.Reporting.Drawing.Unit.Inch(2.7896623611450195D));
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0207966566085815D), Telerik.Reporting.Drawing.Unit.Inch(0.21025906503200531D));
            this.textBox10.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox10.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox10.Style.Font.Bold = true;
            this.textBox10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox10.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox10.Value = "Temperature:";
            // 
            // textBox8
            // 
            this.textBox8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.013304074294865131D), Telerik.Reporting.Drawing.Unit.Inch(2.6104171276092529D));
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1707941293716431D), Telerik.Reporting.Drawing.Unit.Inch(0.18958309292793274D));
            this.textBox8.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox8.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox8.Style.Font.Bold = true;
            this.textBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox8.Value = "Wind Direction:";
            // 
            // textBox7
            // 
            this.textBox7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(3.3000001907348633D));
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.9951577186584473D), Telerik.Reporting.Drawing.Unit.Inch(0.2022802084684372D));
            this.textBox7.Style.BackgroundColor = System.Drawing.Color.Silver;
            this.textBox7.Style.Color = System.Drawing.Color.Black;
            this.textBox7.Style.Font.Bold = true;
            this.textBox7.Style.Font.Name = "Segoe UI";
            this.textBox7.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox7.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox7.Value = "Tank Information";
            // 
            // textBox29
            // 
            this.textBox29.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.2604566812515259D), Telerik.Reporting.Drawing.Unit.Inch(2.2000789642333984D));
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.1986808776855469D), Telerik.Reporting.Drawing.Unit.Inch(0.1999211460351944D));
            this.textBox29.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox29.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox29.Style.Font.Bold = false;
            this.textBox29.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox29.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox29.Value = "=Fields.SoilCondition";
            // 
            // textBox28
            // 
            this.textBox28.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.2604566812515259D), Telerik.Reporting.Drawing.Unit.Inch(2.0023593902587891D));
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.1986804008483887D), Telerik.Reporting.Drawing.Unit.Inch(0.19764059782028198D));
            this.textBox28.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox28.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox28.Style.Font.Bold = false;
            this.textBox28.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox28.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox28.Value = "=Fields.SkyCondition";
            // 
            // textBox27
            // 
            this.textBox27.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.0174312591552734D), Telerik.Reporting.Drawing.Unit.Inch(0.42803493142127991D));
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.466667652130127D), Telerik.Reporting.Drawing.Unit.Inch(0.23216201364994049D));
            this.textBox27.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox27.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox27.Style.Font.Bold = false;
            this.textBox27.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox27.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox27.Value = "=Fields.EndDate";
            // 
            // textBox26
            // 
            this.textBox26.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.0174312591552734D), Telerik.Reporting.Drawing.Unit.Inch(0.19795608520507813D));
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.466667652130127D), Telerik.Reporting.Drawing.Unit.Inch(0.23000000417232513D));
            this.textBox26.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox26.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox26.Style.Font.Bold = false;
            this.textBox26.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox26.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox26.Value = "=Fields.StartDate";
            // 
            // textBox25
            // 
            this.textBox25.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.2604564428329468D), Telerik.Reporting.Drawing.Unit.Inch(3.0000791549682617D));
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.1986806392669678D), Telerik.Reporting.Drawing.Unit.Inch(0.19992092251777649D));
            this.textBox25.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox25.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox25.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox25.Value = "=Fields.Humidity";
            // 
            // textBox24
            // 
            this.textBox24.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.2604566812515259D), Telerik.Reporting.Drawing.Unit.Inch(2.8000791072845459D));
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.1986806392669678D), Telerik.Reporting.Drawing.Unit.Inch(0.19992123544216156D));
            this.textBox24.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox24.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox24.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox24.Value = "=Fields.Temperature";
            // 
            // textBox23
            // 
            this.textBox23.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.2604566812515259D), Telerik.Reporting.Drawing.Unit.Inch(2.600078821182251D));
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.1986804008483887D), Telerik.Reporting.Drawing.Unit.Inch(0.19992116093635559D));
            this.textBox23.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox23.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox23.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox23.Value = "=Fields.WindDirection";
            // 
            // textBox22
            // 
            this.textBox22.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.2604566812515259D), Telerik.Reporting.Drawing.Unit.Inch(2.4000790119171143D));
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.198681116104126D), Telerik.Reporting.Drawing.Unit.Inch(0.199921116232872D));
            this.textBox22.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox22.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox22.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox22.Value = "=Fields.WindSpeed";
            // 
            // textBox14
            // 
            this.textBox14.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.013938267715275288D), Telerik.Reporting.Drawing.Unit.Inch(1.0000001192092896D));
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.4707949161529541D), Telerik.Reporting.Drawing.Unit.Inch(0.19783779978752136D));
            this.textBox14.Style.BackgroundColor = System.Drawing.Color.Silver;
            this.textBox14.Style.Color = System.Drawing.SystemColors.WindowText;
            this.textBox14.Style.Font.Bold = true;
            this.textBox14.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox14.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox14.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox14.Value = "Applicators";
            // 
            // textBox13
            // 
            this.textBox13.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.42803493142127991D));
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.89775913953781128D), Telerik.Reporting.Drawing.Unit.Inch(0.23216201364994049D));
            this.textBox13.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox13.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox13.Style.Font.Bold = true;
            this.textBox13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox13.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox13.Value = "Finish Time:";
            // 
            // textBox12
            // 
            this.textBox12.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.19795608520507813D));
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.89575451612472534D), Telerik.Reporting.Drawing.Unit.Inch(0.23000000417232513D));
            this.textBox12.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox12.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox12.Style.Font.Bold = true;
            this.textBox12.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox12.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox12.Value = "Start Time:";
            // 
            // textBox11
            // 
            this.textBox11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.4707949161529541D), Telerik.Reporting.Drawing.Unit.Inch(0.19783779978752136D));
            this.textBox11.Style.BackgroundColor = System.Drawing.Color.Silver;
            this.textBox11.Style.Color = System.Drawing.SystemColors.WindowText;
            this.textBox11.Style.Font.Bold = true;
            this.textBox11.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox11.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox11.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox11.Value = "Application Date & Time";
            // 
            // table3
            // 
            this.table3.Bindings.Add(new Telerik.Reporting.Binding("DataSource", "=Fields.Applicators"));
            this.table3.Bindings.Add(new Telerik.Reporting.Binding("Visible", "=Fields.Applicators.Count > 0"));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.9013854265213013D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.5694096088409424D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.18833325803279877D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.17708323895931244D)));
            this.table3.Body.SetCellContent(0, 0, this.textBox71);
            this.table3.Body.SetCellContent(0, 1, this.textBox73);
            this.table3.Body.SetCellContent(1, 0, this.textBox76);
            this.table3.Body.SetCellContent(1, 1, this.textBox75);
            tableGroup1.Name = "tableGroup6";
            tableGroup1.ReportItem = this.textBox70;
            tableGroup2.Name = "tableGroup7";
            tableGroup2.ReportItem = this.textBox72;
            this.table3.ColumnGroups.Add(tableGroup1);
            this.table3.ColumnGroups.Add(tableGroup2);
            this.table3.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox71,
            this.textBox73,
            this.textBox76,
            this.textBox75,
            this.textBox70,
            this.textBox72});
            this.table3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.013938267715275288D), Telerik.Reporting.Drawing.Unit.Inch(1.1979168653488159D));
            this.table3.Name = "table3";
            tableGroup4.Name = "group11";
            tableGroup5.Name = "group12";
            tableGroup3.ChildGroups.Add(tableGroup4);
            tableGroup3.ChildGroups.Add(tableGroup5);
            tableGroup3.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup3.Name = "detailTableGroup2";
            this.table3.RowGroups.Add(tableGroup3);
            this.table3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.4707949161529541D), Telerik.Reporting.Drawing.Unit.Inch(0.53208315372467041D));
            // 
            // textBox71
            // 
            this.textBox71.Name = "textBox71";
            this.textBox71.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9013861417770386D), Telerik.Reporting.Drawing.Unit.Inch(0.18833325803279877D));
            this.textBox71.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox71.Value = "=Fields.Name";
            // 
            // textBox73
            // 
            this.textBox73.Name = "textBox73";
            this.textBox73.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5694091320037842D), Telerik.Reporting.Drawing.Unit.Inch(0.18833325803279877D));
            this.textBox73.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox73.Value = "=Fields.LicenseNumber";
            // 
            // textBox76
            // 
            this.textBox76.Name = "textBox76";
            this.textBox76.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9013861417770386D), Telerik.Reporting.Drawing.Unit.Inch(0.17708323895931244D));
            this.textBox76.Style.Font.Italic = true;
            this.textBox76.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox76.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox76.StyleName = "";
            this.textBox76.Value = "=Fields.Company";
            // 
            // textBox75
            // 
            this.textBox75.Format = "{0:d}";
            this.textBox75.Name = "textBox75";
            this.textBox75.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5694091320037842D), Telerik.Reporting.Drawing.Unit.Inch(0.17708323895931244D));
            this.textBox75.Style.Font.Italic = true;
            this.textBox75.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox75.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox75.Value = "=Fields.ExpirationDate";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.08970832824707D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.pictureBox2.MimeType = "";
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.2000000476837158D), Telerik.Reporting.Drawing.Unit.Inch(3.2000000476837158D));
            this.pictureBox2.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.Stretch;
            this.pictureBox2.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.pictureBox2.Value = "=Fields.MapImage";
            // 
            // textBox69
            // 
            this.textBox69.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.3146572113037109D), Telerik.Reporting.Drawing.Unit.Inch(6D));
            this.textBox69.Name = "textBox69";
            this.textBox69.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.7020852565765381D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBox69.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox69.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox69.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox69.Style.Font.Bold = true;
            this.textBox69.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox69.Value = "Date";
            // 
            // textBox74
            // 
            this.textBox74.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.011051177978515625D), Telerik.Reporting.Drawing.Unit.Inch(6D));
            this.textBox74.Name = "textBox74";
            this.textBox74.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.899921178817749D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBox74.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox74.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox74.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox74.Style.Font.Bold = true;
            this.textBox74.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox74.Value = "Signature";
            // 
            // textBox67
            // 
            this.textBox67.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9783611297607422D), Telerik.Reporting.Drawing.Unit.Inch(4.6958327293396D));
            this.textBox67.Name = "textBox67";
            this.textBox67.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.0249977111816406D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox67.Style.BackgroundColor = System.Drawing.Color.Silver;
            this.textBox67.Style.Color = System.Drawing.Color.Black;
            this.textBox67.Style.Font.Bold = true;
            this.textBox67.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox67.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox67.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox67.Value = "=Fields.TotalArea";
            // 
            // table2
            // 
            this.table2.Bindings.Add(new Telerik.Reporting.Binding("DataSource", "=Fields.Fields"));
            this.table2.Bindings.Add(new Telerik.Reporting.Binding("Visible", "=Fields.Fields.Count > 0"));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.4000002145767212D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.4729161262512207D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.5354169607162476D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.4083336591720581D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.5000013113021851D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.675000011920929D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.23000000417232513D)));
            this.table2.Body.SetCellContent(0, 0, this.textBox52);
            this.table2.Body.SetCellContent(0, 1, this.textBox50);
            this.table2.Body.SetCellContent(0, 2, this.textBox56);
            this.table2.Body.SetCellContent(0, 4, this.textBox58);
            this.table2.Body.SetCellContent(0, 5, this.textBox60);
            this.table2.Body.SetCellContent(0, 3, this.textBox64);
            tableGroup6.Name = "tableGroup3";
            tableGroup6.ReportItem = this.textBox55;
            tableGroup7.Name = "tableGroup4";
            tableGroup7.ReportItem = this.textBox57;
            tableGroup8.Name = "tableGroup5";
            tableGroup8.ReportItem = this.textBox59;
            tableGroup9.Name = "group10";
            tableGroup9.ReportItem = this.textBox63;
            tableGroup10.Name = "group7";
            tableGroup10.ReportItem = this.textBox61;
            tableGroup11.Name = "group8";
            tableGroup11.ReportItem = this.textBox65;
            this.table2.ColumnGroups.Add(tableGroup6);
            this.table2.ColumnGroups.Add(tableGroup7);
            this.table2.ColumnGroups.Add(tableGroup8);
            this.table2.ColumnGroups.Add(tableGroup9);
            this.table2.ColumnGroups.Add(tableGroup10);
            this.table2.ColumnGroups.Add(tableGroup11);
            this.table2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox52,
            this.textBox50,
            this.textBox56,
            this.textBox64,
            this.textBox58,
            this.textBox60,
            this.textBox55,
            this.textBox57,
            this.textBox59,
            this.textBox63,
            this.textBox61,
            this.textBox65});
            this.table2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.011051337234675884D), Telerik.Reporting.Drawing.Unit.Inch(4.895911693572998D));
            this.table2.Name = "table2";
            tableGroup12.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup12.Name = "detailTableGroup1";
            this.table2.RowGroups.Add(tableGroup12);
            this.table2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.9916682243347168D), Telerik.Reporting.Drawing.Unit.Inch(0.4400000274181366D));
            // 
            // textBox52
            // 
            formattingRule2.Filters.Add(new Telerik.Reporting.Filter("=RowNumber() % 2", Telerik.Reporting.FilterOperator.Equal, "1"));
            formattingRule2.Style.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.textBox52.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule2});
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3999998569488525D), Telerik.Reporting.Drawing.Unit.Inch(0.23000000417232513D));
            this.textBox52.Value = "=Fields.Farm";
            // 
            // textBox50
            // 
            formattingRule3.Filters.Add(new Telerik.Reporting.Filter("=RowNumber() % 2", Telerik.Reporting.FilterOperator.Equal, "1"));
            formattingRule3.Style.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.textBox50.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule3});
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4729170799255371D), Telerik.Reporting.Drawing.Unit.Inch(0.23000000417232513D));
            this.textBox50.Value = "=Fields.Field";
            // 
            // textBox56
            // 
            this.textBox56.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule3});
            this.textBox56.Name = "textBox56";
            this.textBox56.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5354166030883789D), Telerik.Reporting.Drawing.Unit.Inch(0.23000000417232513D));
            this.textBox56.Value = "=Fields.CropZone";
            // 
            // textBox58
            // 
            this.textBox58.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule3});
            this.textBox58.Name = "textBox58";
            this.textBox58.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5000001192092896D), Telerik.Reporting.Drawing.Unit.Inch(0.23000000417232513D));
            this.textBox58.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox58.StyleName = "";
            this.textBox58.Value = "=Fields.CenterLatLong";
            // 
            // textBox60
            // 
            this.textBox60.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule3});
            this.textBox60.Format = "{0:N2}";
            this.textBox60.Name = "textBox60";
            this.textBox60.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.67500025033950806D), Telerik.Reporting.Drawing.Unit.Inch(0.23000000417232513D));
            this.textBox60.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox60.StyleName = "";
            this.textBox60.Value = "=Fields.Area";
            // 
            // textBox64
            // 
            this.textBox64.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule3});
            this.textBox64.Name = "textBox64";
            this.textBox64.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4083338975906372D), Telerik.Reporting.Drawing.Unit.Inch(0.23000000417232513D));
            this.textBox64.StyleName = "";
            this.textBox64.Value = "=Fields.Crop";
            // 
            // textBox54
            // 
            this.textBox54.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.011051337234675884D), Telerik.Reporting.Drawing.Unit.Inch(4.6958327293396D));
            this.textBox54.Name = "textBox54";
            this.textBox54.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.069312572479248D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox54.Style.BackgroundColor = System.Drawing.Color.Silver;
            this.textBox54.Style.Color = System.Drawing.Color.Black;
            this.textBox54.Style.Font.Bold = true;
            this.textBox54.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox54.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox54.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox54.Value = "Fields";
            // 
            // textBox53
            // 
            this.textBox53.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.013303597457706928D), Telerik.Reporting.Drawing.Unit.Inch(3.8999998569488525D));
            this.textBox53.Name = "textBox53";
            this.textBox53.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.9667458534240723D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox53.Style.BackgroundColor = System.Drawing.Color.Silver;
            this.textBox53.Style.Color = System.Drawing.Color.Black;
            this.textBox53.Style.Font.Bold = true;
            this.textBox53.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox53.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox53.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox53.Value = "Products";
            // 
            // textBox5
            // 
            this.textBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(5.4000000953674316D));
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.9771623611450195D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox5.Style.BackgroundColor = System.Drawing.Color.Silver;
            this.textBox5.Style.Color = System.Drawing.Color.Black;
            this.textBox5.Style.Font.Bold = true;
            this.textBox5.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox5.Value = "Notes";
            // 
            // textBox19
            // 
            this.textBox19.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(5.5979170799255371D));
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.987696647644043D), Telerik.Reporting.Drawing.Unit.Inch(0.17712275683879852D));
            this.textBox19.Value = "=Fields.Notes";
            // 
            // textBox6
            // 
            this.textBox6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.66027575731277466D));
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.98958337306976318D), Telerik.Reporting.Drawing.Unit.Inch(0.23216201364994049D));
            this.textBox6.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox6.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox6.Style.Font.Bold = true;
            this.textBox6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox6.Style.Visible = true;
            this.textBox6.Value = "Timing Event:";
            // 
            // TimingEventTXB
            // 
            this.TimingEventTXB.Angle = 0D;
            this.TimingEventTXB.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.0174312591552734D), Telerik.Reporting.Drawing.Unit.Inch(0.66027575731277466D));
            this.TimingEventTXB.Name = "TimingEventTXB";
            this.TimingEventTXB.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.4673020839691162D), Telerik.Reporting.Drawing.Unit.Inch(0.2321619987487793D));
            this.TimingEventTXB.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.TimingEventTXB.Style.Visible = true;
            this.TimingEventTXB.Value = "=Fields.TimingEvent";
            // 
            // pageFooterSection1
            // 
            this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.40000090003013611D);
            this.pageFooterSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox104,
            this.pageInfoTextBox,
            this.textBox2});
            this.pageFooterSection1.Name = "pageFooterSection1";
            // 
            // textBox104
            // 
            this.textBox104.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.1000010147690773D));
            this.textBox104.Name = "textBox104";
            this.textBox104.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8790887594223023D), Telerik.Reporting.Drawing.Unit.Inch(0.29996046423912048D));
            this.textBox104.Style.Color = System.Drawing.Color.DimGray;
            this.textBox104.Style.Font.Bold = true;
            this.textBox104.Style.Font.Italic = false;
            this.textBox104.Style.Font.Name = "Segoe UI Light";
            this.textBox104.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(16D);
            this.textBox104.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox104.Value = "AG CONNECTIONS";
            // 
            // pageInfoTextBox
            // 
            this.pageInfoTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.0584907531738281D), Telerik.Reporting.Drawing.Unit.Inch(0.089509963989257812D));
            this.pageInfoTextBox.Name = "pageInfoTextBox";
            this.pageInfoTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9186718463897705D), Telerik.Reporting.Drawing.Unit.Inch(0.30003905296325684D));
            this.pageInfoTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.pageInfoTextBox.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.pageInfoTextBox.StyleName = "PageInfo";
            this.pageInfoTextBox.Value = "=PageNumber";
            // 
            // textBox2
            // 
            this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.8534829616546631D), Telerik.Reporting.Drawing.Unit.Inch(0.1000010147690773D));
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.1978371143341064D), Telerik.Reporting.Drawing.Unit.Inch(0.29996046423912048D));
            this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox2.Value = "generated on {Now()}";
            // 
            // PortraitApplication
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pageHeaderSection1,
            this.detail,
            this.pageFooterSection1});
            this.Name = "PortraitApplication";
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(0.25D), Telerik.Reporting.Drawing.Unit.Inch(0.25D), Telerik.Reporting.Drawing.Unit.Inch(0.25D), Telerik.Reporting.Drawing.Unit.Inch(0.25D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(8.0167427062988281D);
            ((System.ComponentModel.ISupportInitialize)(this.portraitProductsSubReport1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PageFooterSection pageFooterSection1;
        private Telerik.Reporting.TextBox AuthorizeDateTXT;
        private Telerik.Reporting.TextBox AuthorizerTXT;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox90;
        private Telerik.Reporting.TextBox textBox89;
        private Telerik.Reporting.TextBox textBox83;
        private Telerik.Reporting.TextBox textBox84;
        private Telerik.Reporting.TextBox textBox82;
        private Telerik.Reporting.TextBox textBox81;
        private Telerik.Reporting.TextBox textBox80;
        private Telerik.Reporting.TextBox textBox85;
        private Telerik.Reporting.TextBox textBox86;
        private Telerik.Reporting.TextBox textBox87;
        private Telerik.Reporting.TextBox textBox88;
        private Telerik.Reporting.TextBox textBox68;
        private Telerik.Reporting.TextBox textBox62;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox29;
        private Telerik.Reporting.TextBox textBox28;
        private Telerik.Reporting.TextBox textBox27;
        private Telerik.Reporting.TextBox textBox26;
        private Telerik.Reporting.TextBox textBox25;
        private Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.Table table3;
        private Telerik.Reporting.TextBox textBox71;
        private Telerik.Reporting.TextBox textBox73;
        private Telerik.Reporting.TextBox textBox76;
        private Telerik.Reporting.TextBox textBox75;
        private Telerik.Reporting.TextBox textBox70;
        private Telerik.Reporting.TextBox textBox72;
        private Telerik.Reporting.PictureBox pictureBox2;
        private Telerik.Reporting.TextBox textBox69;
        private Telerik.Reporting.TextBox textBox74;
        private Telerik.Reporting.TextBox textBox67;
        private Telerik.Reporting.Table table2;
        private Telerik.Reporting.TextBox textBox52;
        private Telerik.Reporting.TextBox textBox50;
        private Telerik.Reporting.TextBox textBox56;
        private Telerik.Reporting.TextBox textBox58;
        private Telerik.Reporting.TextBox textBox60;
        private Telerik.Reporting.TextBox textBox64;
        private Telerik.Reporting.TextBox textBox55;
        private Telerik.Reporting.TextBox textBox57;
        private Telerik.Reporting.TextBox textBox59;
        private Telerik.Reporting.TextBox textBox63;
        private Telerik.Reporting.TextBox textBox61;
        private Telerik.Reporting.TextBox textBox65;
        private Telerik.Reporting.TextBox textBox54;
        private Telerik.Reporting.TextBox textBox104;
        private Telerik.Reporting.TextBox pageInfoTextBox;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox textBox53;
        private Telerik.Reporting.SubReport subReport1;
        private PortraitProductsSubReport portraitProductsSubReport1;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox TimingEventTXB;
        private Telerik.Reporting.TextBox txtDataSourceLabel;
        private Telerik.Reporting.TextBox textBox93;
        private Telerik.Reporting.TextBox textBox100;
    }
}