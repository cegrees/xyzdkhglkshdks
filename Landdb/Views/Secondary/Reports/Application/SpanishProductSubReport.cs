namespace Landdb.Views.Secondary.Reports.Application
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;

    /// <summary>
    /// Summary description for SpanishProductSubReport.
    /// </summary>
    public partial class SpanishProductSubReport : Telerik.Reporting.Report
    {
        public SpanishProductSubReport()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }
    }
}