namespace Landdb.Views.Secondary.Reports.Application
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;

    /// <summary>
    /// Summary description for PortraitProductsSubReport.
    /// </summary>
    public partial class PortraitProductsSubReport : Telerik.Reporting.Report
    {
        public PortraitProductsSubReport()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }
    }
}