namespace Landdb.Views.Secondary.Reports.Application
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;

    /// <summary>
    /// Summary description for PortraitApplication.
    /// </summary>
    public partial class PortraitApplication : Telerik.Reporting.Report
    {
        public PortraitApplication()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }
    }
}