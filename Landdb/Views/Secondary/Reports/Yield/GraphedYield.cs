namespace Landdb.Views.Secondary.Reports.Yield
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;

    /// <summary>
    /// Summary description for GraphedYield.
    /// </summary>
    public partial class GraphedYield : Telerik.Reporting.Report
    {
        public GraphedYield()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }
    }
}