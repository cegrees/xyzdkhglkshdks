namespace Landdb.Views.Secondary.Reports.Yield
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;

    /// <summary>
    /// Summary description for CommoditySummaryReport.
    /// </summary>
    public partial class CommoditySummaryReport : Telerik.Reporting.Report
    {
        public CommoditySummaryReport()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }
    }
}