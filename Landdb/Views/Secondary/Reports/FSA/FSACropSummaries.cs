namespace Landdb.Views.Secondary.Reports.FSA
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;

    /// <summary>
    /// Summary description for FSACropSummaries.
    /// </summary>
    public partial class FSACropSummaries : Telerik.Reporting.Report
    {
        public FSACropSummaries()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }
    }
}