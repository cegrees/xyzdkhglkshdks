﻿using GalaSoft.MvvmLight.Messaging;
using Landdb.ViewModel.Secondary.Reports.FSA;
using Microsoft.Maps.MapControl.WPF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Landdb.Client.Spatial;

namespace Landdb.Views.Secondary.Reports.FSA
{
    /// <summary>
    /// Interaction logic for FSAReportInfoView.xaml
    /// </summary>
    public partial class FSAReportInfoView : UserControl
    {
        bool bingMapsHasSize = false;

        public FSAReportInfoView()
        {
            InitializeComponent();
            BingMap.SizeChanged += BingMap_SizeChanged;
            Messenger.Default.Register<PropertyChangedMessage<MapLayer>>(this, UpdateMap);
        }

        private void ComboBox_Selected(object sender, RoutedEventArgs e)
        {

        }

        void BingMap_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (e.NewSize.Height > 0 && e.NewSize.Width > 0)
            {
                if (!bingMapsHasSize)
                {
                    bingMapsHasSize = true;
                }
            }
            else
            {
                bingMapsHasSize = false;
            }
        }

        private void BingMap_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ((MainWindow)System.Windows.Application.Current.MainWindow).Draggable = false;
        }

        private void BingMap_MouseUp(object sender, MouseButtonEventArgs e)
        {
            ((MainWindow)System.Windows.Application.Current.MainWindow).Draggable = true;
        }

        void UpdateMap(PropertyChangedMessage<MapLayer> mapChanged)
        {
            //var vm = this.DataContext as FSAReportInfoViewModel;

            if (BingMap == null || !BingMap.IsLoaded || !bingMapsHasSize) { return; }

            try
            {
                if (mapChanged.NewValue != null)
                {
                    BingMap.Children.Clear();

                    BingMap.Children.Add(mapChanged.NewValue);
                    var box = mapChanged.NewValue.GetBoundingBox().Inflate(0.1);

                    if (box != null)
                    {
                        BingMap.SetView(box);
                    }
                }
            }
            catch { }  // TODO: Eat for now; probably need to bubble something to the user
        }
        
    }
}
