namespace Landdb.Views.Secondary.Reports.FSA
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;

    /// <summary>
    /// Summary description for FSAMultiplePlantings.
    /// </summary>
    public partial class FSAMultiplePlantings : Telerik.Reporting.Report
    {
        public FSAMultiplePlantings()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            checkBox1.Value = false;
        }
    }
}