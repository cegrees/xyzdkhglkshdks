namespace Landdb.Views.Secondary.Reports.Contracts
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;

    /// <summary>
    /// Summary description for RentContractSummary.
    /// </summary>
    public partial class RentContractSummary : Telerik.Reporting.Report
    {
        public RentContractSummary()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }
    }
}