namespace Landdb.Views.Secondary.Reports.Contracts
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;

    /// <summary>
    /// Summary description for ProductionContractSummary.
    /// </summary>
    public partial class ProductionContractSummary : Telerik.Reporting.Report
    {
        public ProductionContractSummary()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }

        private void graph1_NeedDataSource(object sender, EventArgs e)
        {
            //Telerik.Reporting.Processing.Chart chart = sender as Telerik.Reporting.Processing.Chart;
            //chart.DataSource = this.pieChartData;
        }
    }
}