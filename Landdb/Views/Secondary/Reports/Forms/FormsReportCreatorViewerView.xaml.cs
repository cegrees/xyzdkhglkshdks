﻿using Landdb.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Landdb.Views.Secondary.Reports.Forms
{
    /// <summary>
    /// Interaction logic for FormsReportCreatorViewerView.xaml
    /// </summary>
    public partial class FormsReportCreatorViewerView : UserControl
    {
        public FormsReportCreatorViewerView()
        {
            InitializeComponent();

            this.IsVisibleChanged += FormsReportCreatorViewerView_IsVisibleChanged;
        }
        void FormsReportCreatorViewerView_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (this.IsVisible && ViewModel != null)
            {
                ViewModel.Update();
            }
        }

        Updatable ViewModel => this.DataContext as Updatable;

        private void Button_MouseEnter(object sender, MouseEventArgs e)
        {

        }
    }
}
