namespace Landdb.Views.Secondary.Reports.Specialty
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;

    /// <summary>
    /// Summary description for AerialApplicatorOverview.
    /// </summary>
    public partial class AerialApplicatorOverview : Telerik.Reporting.Report
    {
        public AerialApplicatorOverview()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }
    }
}