﻿using Landdb.ViewModel.Secondary.Reports.Specialty;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Landdb.Views.Secondary.Reports.Specialty
{
    /// <summary>
    /// Interaction logic for SpecialtyReportCreatorView.xaml
    /// </summary>
    public partial class SpecialtyReportCreatorView : UserControl
    {
        public SpecialtyReportCreatorView()
        {
            InitializeComponent();
        }

        private void ApplicatorTabs_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ApplicatorTabs.UpdateLayout();

            if (ApplicatorTabs.SelectedItem == Viewer)
            {
                //fire off report generator
                var context = DataContext as SpecialtyReportCreatorViewModel;
                context.UpdateReport();
            }
        }
    }
}
