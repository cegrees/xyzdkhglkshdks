namespace Landdb.Views.Secondary.Reports.Specialty
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;

    /// <summary>
    /// Summary description for Arizona1080.
    /// </summary>
    public partial class Arizona1080 : Telerik.Reporting.Report
    {
        public Arizona1080()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            checkBox1.Value = false;
            checkBox2.Value = false;
            checkBox3.Value = false;
            checkBox4.Value = false;
            checkBox5.Value = false;
            checkBox6.Value = false;
            checkBox7.Value = false;
            checkBox8.Value = false;
            checkBox9.Value = false;
            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }
    }
}