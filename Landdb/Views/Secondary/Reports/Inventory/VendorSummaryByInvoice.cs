namespace Landdb.Views.Secondary.Reports.Inventory
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;

    /// <summary>
    /// Summary description for VendorSummaryByInvoice.
    /// </summary>
    public partial class VendorSummaryByInvoice : Telerik.Reporting.Report
    {
        public VendorSummaryByInvoice()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }
    }
}