namespace Landdb.Views.Secondary.Reports.Inventory
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;

    /// <summary>
    /// Summary description for ProductSummaryByVendor.
    /// </summary>
    public partial class VendorSummaryByProduct : Telerik.Reporting.Report
    {
        public VendorSummaryByProduct()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }
    }
}