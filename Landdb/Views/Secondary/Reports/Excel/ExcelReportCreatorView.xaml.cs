﻿using Landdb.ViewModel.Secondary.Reports.ExcelDump;
using Landdb.ViewModel.Secondary.Reports.ExcelDump.Generators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Landdb.Views.Secondary.Reports.Excel
{
    /// <summary>
    /// Interaction logic for ExcelReportCreatorView.xaml
    /// </summary>
    public partial class ExcelReportCreatorView : UserControl
    {
        public ExcelReportCreatorView()
        {
            InitializeComponent();
        }

        private void ApplicatorTabs_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ApplicatorTabs.UpdateLayout();
            if (this.DataContext != null)
            {
                var vm = this.DataContext as AppliedProductExcelDumpCreatorViewModel;
                vm.SelectedReportGenerator = null;
            }
        }
    }
}
