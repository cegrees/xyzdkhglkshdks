namespace Landdb.Views.Secondary.Reports.Scouting
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;

    /// <summary>
    /// Summary description for ScoutingReport.
    /// </summary>
    public partial class ScoutingReport : Telerik.Reporting.Report
    {
        public ScoutingReport()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }
    }
}