﻿using Landdb.ViewModel.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Landdb.Client.Spatial;
using Landdb.ViewModel.Secondary.Reports.WorkOrder.Generators;
using Landdb.ViewModel.Secondary.Reports.WorkOrder;
using System.IO;
using Telerik.ReportViewer;
using Telerik.Reporting.Processing;
using Microsoft.Win32;
using Telerik.Reporting;
using Landdb.ViewModel.Secondary.Reports.Application;
using Landdb.ViewModel.Secondary.Reports.Invoice;
using Landdb.ViewModel.Secondary.Reports.Plan;

namespace Landdb.Views.Secondary.Reports.Work_Order
{
    /// <summary>
    /// Interaction logic for SingleWorkOrderView.xaml
    /// </summary>
    public partial class SingleWorkOrderView : UserControl
    {
        public SingleWorkOrderView()
        {
            InitializeComponent();
            ReportViewer.ZoomPercent = 75;
            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ReportSource reportExport;
            if (this.DataContext is SingleWorkOrderViewModel)
            {
                var vm = this.DataContext as SingleWorkOrderViewModel;
                reportExport = vm.ReportSource;
            }
            else if (this.DataContext is SingleApplicationViewModel)
            {
                var vm = this.DataContext as SingleApplicationViewModel;
                reportExport = vm.ReportSource;
            }
            else if (this.DataContext is SinglePlanViewModel)
            {
                var vm = this.DataContext as SinglePlanViewModel;
                reportExport = vm.ReportSource;
            }
            else
            {
                var vm = this.DataContext as SingleInvoiceViewModel;
                reportExport = vm.ReportSource;
            }

            ReportProcessor reportProcessor = new ReportProcessor();
            System.Collections.Hashtable deviceInfo =
            new System.Collections.Hashtable();
            RenderingResult result = reportProcessor.RenderReport("PDF", reportExport, deviceInfo);

            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "Adobe Reader|*.pdf";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;

            Nullable<bool> resultSaved = saveFileDialog1.ShowDialog();

            // Process save file dialog box results 
            if (resultSaved == true)
            {
                // Save document 
                string filename = saveFileDialog1.FileName;

                using (System.IO.FileStream fs = new System.IO.FileStream(filename, System.IO.FileMode.Create))
                {
                    fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
                }

            }

        }
    }
}
