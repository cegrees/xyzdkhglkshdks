namespace Landdb.Views.Secondary.Reports.Work_Order
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;

    /// <summary>
    /// Summary description for WorkOrderLargeMap.
    /// </summary>
    public partial class WorkOrderLargeMap : Telerik.Reporting.Report
    {
        public WorkOrderLargeMap()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }
    }
}