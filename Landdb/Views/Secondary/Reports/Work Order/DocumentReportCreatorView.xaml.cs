﻿using Landdb.ViewModel.Secondary.Reports.Document;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Landdb.Views.Secondary.Reports.Work_Order
{
    /// <summary>
    /// Interaction logic for DocumentReportCreatorView.xaml
    /// </summary>
    public partial class DocumentReportCreatorView : UserControl
    {
        DocumentReportCreatorViewModel viewModel;
        public DocumentReportCreatorView()
        {
            InitializeComponent();
        }

        private void ApplicatorTabs_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ApplicatorTabs.UpdateLayout();
            if (viewModel != null)
            {
                viewModel = this.DataContext as DocumentReportCreatorViewModel;
                viewModel.SelectedReportGenerator = null;
                viewModel.ReportSource = null;
            }
        }
    }
}
