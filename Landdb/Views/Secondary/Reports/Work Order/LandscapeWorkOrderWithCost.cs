namespace Landdb.Views.Secondary.Reports.Work_Order
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;

    /// <summary>
    /// Summary description for LandscapeWorkOrderWithCost.
    /// </summary>
    public partial class LandscapeWorkOrderWithCost : Telerik.Reporting.Report
    {
        public LandscapeWorkOrderWithCost()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }
    }
}