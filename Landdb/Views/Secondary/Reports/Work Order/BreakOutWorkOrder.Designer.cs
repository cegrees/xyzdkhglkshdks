namespace Landdb.Views.Secondary.Reports.Work_Order
{
    partial class BreakOutWorkOrder
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            var cultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture.Name;
            
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup23 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup24 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup25 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup26 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup27 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup28 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup29 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup30 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup31 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup32 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup33 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup34 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup35 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup36 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup37 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup38 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup39 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup40 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup41 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup42 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup43 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup44 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.Drawing.FormattingRule formattingRule1 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup17 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup18 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup19 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup20 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup21 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup22 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.textBox35 = new Telerik.Reporting.TextBox();
            this.textBox95 = new Telerik.Reporting.TextBox();
            this.textBox100 = new Telerik.Reporting.TextBox();
            this.textBox51 = new Telerik.Reporting.TextBox();
            this.textBox53 = new Telerik.Reporting.TextBox();
            this.textBox55 = new Telerik.Reporting.TextBox();
            this.textBox63 = new Telerik.Reporting.TextBox();
            this.textBox57 = new Telerik.Reporting.TextBox();
            this.textBox59 = new Telerik.Reporting.TextBox();
            this.textBox61 = new Telerik.Reporting.TextBox();
            this.textBox70 = new Telerik.Reporting.TextBox();
            this.textBox72 = new Telerik.Reporting.TextBox();
            this.textBox112 = new Telerik.Reporting.TextBox();
            this.textBox113 = new Telerik.Reporting.TextBox();
            this.textBox116 = new Telerik.Reporting.TextBox();
            this.textBox117 = new Telerik.Reporting.TextBox();
            this.textBox118 = new Telerik.Reporting.TextBox();
            this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            this.detail = new Telerik.Reporting.DetailSection();
            this.textBox30 = new Telerik.Reporting.TextBox();
            this.pictureBox1 = new Telerik.Reporting.PictureBox();
            this.table4 = new Telerik.Reporting.Table();
            this.textBox40 = new Telerik.Reporting.TextBox();
            this.textBox82 = new Telerik.Reporting.TextBox();
            this.textBox43 = new Telerik.Reporting.TextBox();
            this.textBox90 = new Telerik.Reporting.TextBox();
            this.textBox91 = new Telerik.Reporting.TextBox();
            this.textBox96 = new Telerik.Reporting.TextBox();
            this.textBox97 = new Telerik.Reporting.TextBox();
            this.textBox101 = new Telerik.Reporting.TextBox();
            this.textBox102 = new Telerik.Reporting.TextBox();
            this.textBox45 = new Telerik.Reporting.TextBox();
            this.textBox89 = new Telerik.Reporting.TextBox();
            this.textBox34 = new Telerik.Reporting.TextBox();
            this.textBox31 = new Telerik.Reporting.TextBox();
            this.panel1 = new Telerik.Reporting.Panel();
            this.textBox37 = new Telerik.Reporting.TextBox();
            this.textBox47 = new Telerik.Reporting.TextBox();
            this.textBox48 = new Telerik.Reporting.TextBox();
            this.textBox84 = new Telerik.Reporting.TextBox();
            this.textBox85 = new Telerik.Reporting.TextBox();
            this.textBox86 = new Telerik.Reporting.TextBox();
            this.textBox87 = new Telerik.Reporting.TextBox();
            this.textBox88 = new Telerik.Reporting.TextBox();
            this.textBox50 = new Telerik.Reporting.TextBox();
            this.table2 = new Telerik.Reporting.Table();
            this.textBox52 = new Telerik.Reporting.TextBox();
            this.textBox54 = new Telerik.Reporting.TextBox();
            this.textBox56 = new Telerik.Reporting.TextBox();
            this.textBox58 = new Telerik.Reporting.TextBox();
            this.textBox60 = new Telerik.Reporting.TextBox();
            this.textBox62 = new Telerik.Reporting.TextBox();
            this.textBox64 = new Telerik.Reporting.TextBox();
            this.textBox65 = new Telerik.Reporting.TextBox();
            this.textBox66 = new Telerik.Reporting.TextBox();
            this.textBox67 = new Telerik.Reporting.TextBox();
            this.textBox68 = new Telerik.Reporting.TextBox();
            this.table3 = new Telerik.Reporting.Table();
            this.textBox71 = new Telerik.Reporting.TextBox();
            this.textBox73 = new Telerik.Reporting.TextBox();
            this.textBox76 = new Telerik.Reporting.TextBox();
            this.textBox75 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.textBox25 = new Telerik.Reporting.TextBox();
            this.textBox32 = new Telerik.Reporting.TextBox();
            this.textBox46 = new Telerik.Reporting.TextBox();
            this.table5 = new Telerik.Reporting.Table();
            this.textBox49 = new Telerik.Reporting.TextBox();
            this.textBox74 = new Telerik.Reporting.TextBox();
            this.textBox98 = new Telerik.Reporting.TextBox();
            this.textBox106 = new Telerik.Reporting.TextBox();
            this.textBox107 = new Telerik.Reporting.TextBox();
            this.textBox69 = new Telerik.Reporting.TextBox();
            this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
            this.txtDataSourceLabel = new Telerik.Reporting.TextBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox99 = new Telerik.Reporting.TextBox();
            this.textBox103 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.pageInfoTextBox = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox38 = new Telerik.Reporting.TextBox();
            this.textBox41 = new Telerik.Reporting.TextBox();
            this.textBox36 = new Telerik.Reporting.TextBox();
            this.textBox26 = new Telerik.Reporting.TextBox();
            this.textBox28 = new Telerik.Reporting.TextBox();
            this.textBox29 = new Telerik.Reporting.TextBox();
            this.ProductName = new Telerik.Reporting.TextBox();
            this.textBox78 = new Telerik.Reporting.TextBox();
            this.textBox77 = new Telerik.Reporting.TextBox();
            this.textBox83 = new Telerik.Reporting.TextBox();
            this.textBox94 = new Telerik.Reporting.TextBox();
            this.textBox93 = new Telerik.Reporting.TextBox();
            this.textBox92 = new Telerik.Reporting.TextBox();
            this.textBox81 = new Telerik.Reporting.TextBox();
            this.textBox39 = new Telerik.Reporting.TextBox();
            this.textBox42 = new Telerik.Reporting.TextBox();
            this.textBox80 = new Telerik.Reporting.TextBox();
            this.textBox33 = new Telerik.Reporting.TextBox();
            this.textBox44 = new Telerik.Reporting.TextBox();
            this.textBox79 = new Telerik.Reporting.TextBox();
            this.textBox27 = new Telerik.Reporting.TextBox();
            this.table1 = new Telerik.Reporting.Table();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox35
            // 
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.1473491191864014D), Telerik.Reporting.Drawing.Unit.Inch(0.20000006258487701D));
            this.textBox35.Style.BackgroundColor = System.Drawing.Color.Silver;
            this.textBox35.Style.Font.Bold = true;
            this.textBox35.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox35.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox35.Value = "Worker Protection";
            // 
            // textBox95
            // 
            this.textBox95.Name = "textBox95";
            this.textBox95.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0697999000549316D), Telerik.Reporting.Drawing.Unit.Inch(0.20000006258487701D));
            this.textBox95.Style.BackgroundColor = System.Drawing.Color.Silver;
            this.textBox95.Style.Font.Bold = true;
            this.textBox95.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox95.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox95.StyleName = "";
            // 
            // textBox100
            // 
            this.textBox100.Name = "textBox100";
            this.textBox100.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.7828118801116943D), Telerik.Reporting.Drawing.Unit.Inch(0.20000006258487701D));
            this.textBox100.Style.BackgroundColor = System.Drawing.Color.Silver;
            this.textBox100.Style.Font.Bold = true;
            this.textBox100.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox100.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox100.StyleName = "";
            // 
            // textBox51
            // 
            this.textBox51.Name = "textBox51";
            this.textBox51.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.135344386100769D), Telerik.Reporting.Drawing.Unit.Inch(0.20999999344348908D));
            this.textBox51.Style.Font.Bold = true;
            this.textBox51.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox51.Value = Landdb.Resources.Strings.Farm_Text;
            // 
            // textBox53
            // 
            this.textBox53.CanGrow = false;
            this.textBox53.Name = "textBox53";
            this.textBox53.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.135344386100769D), Telerik.Reporting.Drawing.Unit.Inch(0.20999999344348908D));
            this.textBox53.Style.Font.Bold = true;
            this.textBox53.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox53.Value = Landdb.Resources.Strings.Field_Text;
            // 
            // textBox55
            // 
            this.textBox55.CanGrow = false;
            this.textBox55.Name = "textBox55";
            this.textBox55.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1770075559616089D), Telerik.Reporting.Drawing.Unit.Inch(0.20999999344348908D));
            this.textBox55.Style.Font.Bold = true;
            this.textBox55.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox55.Value = Landdb.Resources.Strings.CropZone_Text;
            // 
            // textBox63
            // 
            this.textBox63.CanGrow = false;
            this.textBox63.Name = "textBox63";
            this.textBox63.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3019999265670776D), Telerik.Reporting.Drawing.Unit.Inch(0.20999999344348908D));
            this.textBox63.Style.Font.Bold = true;
            this.textBox63.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox63.StyleName = "";
            this.textBox63.Value = Landdb.Resources.Strings.Crop_Text;
            // 
            // textBox57
            // 
            this.textBox57.CanGrow = false;
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1900005340576172D), Telerik.Reporting.Drawing.Unit.Inch(0.20999999344348908D));
            this.textBox57.Style.Font.Bold = true;
            this.textBox57.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox57.StyleName = "";
            if (cultureInfo == "en-US")
            {
                this.textBox57.Value = Landdb.Resources.Strings.CenterLatLong_Text;
            }
            else {
                this.textBox57.Value = "";
            }
            // 
            // textBox59
            // 
            this.textBox59.CanGrow = false;
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.87494438886642456D), Telerik.Reporting.Drawing.Unit.Inch(0.20999999344348908D));
            this.textBox59.Style.Font.Bold = true;
            this.textBox59.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox59.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox59.StyleName = "";
            this.textBox59.Value = Landdb.Resources.Strings.WOArea_Text;
            // 
            // textBox61
            // 
            this.textBox61.CanGrow = false;
            this.textBox61.Name = "textBox61";
            this.textBox61.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.88535970449447632D), Telerik.Reporting.Drawing.Unit.Inch(0.20999999344348908D));
            this.textBox61.Style.Font.Bold = true;
            this.textBox61.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox61.StyleName = "";
            this.textBox61.Value = Landdb.Resources.Strings.ActualArea_Text;
            // 
            // textBox70
            // 
            this.textBox70.Name = "textBox70";
            this.textBox70.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.30883526802063D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666567325592D));
            this.textBox70.Style.Font.Bold = true;
            this.textBox70.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox70.Style.Font.Underline = true;
            this.textBox70.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            if (cultureInfo == "en-US")
            {
                this.textBox70.Value = Landdb.Resources.Strings.NameCompany_Text;
            }
            else {
                this.textBox70.Value = "";
            }
            // 
            // textBox72
            // 
            this.textBox72.Name = "textBox72";
            this.textBox72.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8658847808837891D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666716337204D));
            this.textBox72.Style.Font.Bold = true;
            this.textBox72.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox72.Style.Font.Underline = true;
            this.textBox72.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            if (cultureInfo == "en-US")
            {
                this.textBox72.Value = Landdb.Resources.Strings.LicenseNumber_Text;
            }
            else {
                this.textBox72.Value = "";
            }

            // 
            // textBox112
            // 
            this.textBox112.CanGrow = false;
            this.textBox112.Name = "textBox112";
            this.textBox112.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.718120813369751D), Telerik.Reporting.Drawing.Unit.Inch(0.22657833993434906D));
            this.textBox112.Style.Font.Bold = true;
            this.textBox112.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox112.Style.Font.Underline = true;
            this.textBox112.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.textBox112.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox112.Value = "Name";
            // 
            // textBox113
            // 
            this.textBox113.Name = "textBox113";
            this.textBox113.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7075183391571045D), Telerik.Reporting.Drawing.Unit.Inch(0.22657833993434906D));
            this.textBox113.Style.Font.Bold = true;
            this.textBox113.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox113.Style.Font.Underline = true;
            this.textBox113.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D);
            this.textBox113.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox113.StyleName = "";
            this.textBox113.Value = "Reg Number";
            // 
            // textBox116
            // 
            this.textBox116.Bindings.Add(new Telerik.Reporting.Binding("Visible", "=Fields.RateType = \"Rate/Tank\""));
            this.textBox116.Name = "textBox116";
            this.textBox116.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2672187089920044D), Telerik.Reporting.Drawing.Unit.Inch(0.22657833993434906D));
            this.textBox116.Style.Font.Bold = true;
            this.textBox116.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox116.Style.Font.Underline = true;
            this.textBox116.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D);
            this.textBox116.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox116.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox116.Style.Visible = true;
            this.textBox116.StyleName = "";
            this.textBox116.Value = "Rate/Tank";
            // 
            // textBox117
            // 
            this.textBox117.Name = "textBox117";
            this.textBox117.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1126120090484619D), Telerik.Reporting.Drawing.Unit.Inch(0.22657833993434906D));
            this.textBox117.Style.Font.Bold = true;
            this.textBox117.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox117.Style.Font.Underline = true;
            this.textBox117.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D);
            this.textBox117.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox117.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox117.StyleName = "";
            this.textBox117.Value = "Rate/Area";
            // 
            // textBox118
            // 
            this.textBox118.Name = "textBox118";
            this.textBox118.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1857826709747315D), Telerik.Reporting.Drawing.Unit.Inch(0.22657833993434906D));
            this.textBox118.Style.Font.Bold = true;
            this.textBox118.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox118.Style.Font.Underline = true;
            this.textBox118.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D);
            this.textBox118.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox118.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox118.StyleName = "";
            this.textBox118.Value = "Total";
            // 
            // pageHeaderSection1
            // 
            this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(1.2000000476837158D);
            this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.txtDataSourceLabel,
            this.textBox1,
            this.textBox2,
            this.textBox3,
            this.textBox4,
            this.textBox99,
            this.textBox103});
            this.pageHeaderSection1.Name = "pageHeaderSection1";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(8.1000003814697266D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox30,
            this.pictureBox1,
            this.table4,
            this.textBox34,
            this.textBox31,
            this.panel1,
            this.textBox50,
            this.table2,
            this.textBox65,
            this.textBox66,
            this.textBox67,
            this.textBox68,
            this.table3,
            this.textBox11,
            this.textBox12,
            this.textBox13,
            this.textBox14,
            this.textBox15,
            this.textBox16,
            this.textBox17,
            this.textBox18,
            this.textBox19,
            this.textBox20,
            this.textBox21,
            this.textBox22,
            this.textBox23,
            this.textBox24,
            this.textBox25,
            this.textBox32,
            this.textBox46,
            this.table5,
            this.textBox69,
            this.textBox37});
            this.detail.Name = "detail";
            // 
            // textBox30
            // 
            this.textBox30.CanGrow = false;
            this.textBox30.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9106178283691406D), Telerik.Reporting.Drawing.Unit.Inch(4.4999213218688965D));
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.0893430709838867D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox30.Style.BackgroundColor = System.Drawing.Color.Silver;
            this.textBox30.Style.Color = System.Drawing.Color.Black;
            this.textBox30.Style.Font.Bold = true;
            this.textBox30.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox30.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox30.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox30.Value = "=Fields.TotalArea";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.6176190376281738D), Telerik.Reporting.Drawing.Unit.Inch(0.09992130845785141D));
            this.pictureBox1.MimeType = "";
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.0199999809265137D), Telerik.Reporting.Drawing.Unit.Inch(3.0188684463500977D));
            this.pictureBox1.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.Stretch;
            this.pictureBox1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.pictureBox1.Value = "=Fields.MapImage";
            // 
            // table4
            // 
            this.table4.Bindings.Add(new Telerik.Reporting.Binding("DataSource", "=Fields.WorkerProtectionItems"));
            this.table4.Bindings.Add(new Telerik.Reporting.Binding("Visible", "=Fields.WorkerProtectionItems.Count > 0"));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.0804983377456665D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.0668506622314453D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.0697999000549316D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(3.7828118801116943D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.20208343863487244D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.208333358168602D)));
            this.table4.Body.SetCellContent(2, 0, this.textBox40);
            this.table4.Body.SetCellContent(3, 0, this.textBox82);
            this.table4.Body.SetCellContent(0, 0, this.textBox43, 1, 2);
            this.table4.Body.SetCellContent(1, 0, this.textBox90);
            this.table4.Body.SetCellContent(1, 1, this.textBox91);
            this.table4.Body.SetCellContent(0, 2, this.textBox96);
            this.table4.Body.SetCellContent(1, 2, this.textBox97);
            this.table4.Body.SetCellContent(0, 3, this.textBox101);
            this.table4.Body.SetCellContent(1, 3, this.textBox102);
            this.table4.Body.SetCellContent(2, 1, this.textBox45, 1, 3);
            this.table4.Body.SetCellContent(3, 1, this.textBox89, 1, 3);
            tableGroup2.Name = "group2";
            tableGroup3.Name = "tableGroup8";
            tableGroup1.ChildGroups.Add(tableGroup2);
            tableGroup1.ChildGroups.Add(tableGroup3);
            tableGroup1.Name = "tableGroup1";
            tableGroup1.ReportItem = this.textBox35;
            tableGroup5.Name = "group20";
            tableGroup4.ChildGroups.Add(tableGroup5);
            tableGroup4.Name = "group19";
            tableGroup4.ReportItem = this.textBox95;
            tableGroup7.Name = "group22";
            tableGroup6.ChildGroups.Add(tableGroup7);
            tableGroup6.Name = "group21";
            tableGroup6.ReportItem = this.textBox100;
            this.table4.ColumnGroups.Add(tableGroup1);
            this.table4.ColumnGroups.Add(tableGroup4);
            this.table4.ColumnGroups.Add(tableGroup6);
            this.table4.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox43,
            this.textBox96,
            this.textBox101,
            this.textBox90,
            this.textBox91,
            this.textBox97,
            this.textBox102,
            this.textBox40,
            this.textBox45,
            this.textBox82,
            this.textBox89,
            this.textBox35,
            this.textBox95,
            this.textBox100});
            this.table4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(6.9000000953674316D));
            this.table4.Name = "table4";
            tableGroup9.Name = "group15";
            tableGroup10.Name = "group18";
            tableGroup11.Name = "group16";
            tableGroup12.Name = "group17";
            tableGroup8.ChildGroups.Add(tableGroup9);
            tableGroup8.ChildGroups.Add(tableGroup10);
            tableGroup8.ChildGroups.Add(tableGroup11);
            tableGroup8.ChildGroups.Add(tableGroup12);
            tableGroup8.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup8.Name = "detailTableGroup3";
            this.table4.RowGroups.Add(tableGroup8);
            this.table4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.9999608993530273D), Telerik.Reporting.Drawing.Unit.Inch(1.0104169845581055D));
            // 
            // textBox40
            // 
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0804983377456665D), Telerik.Reporting.Drawing.Unit.Inch(0.20208343863487244D));
            this.textBox40.Style.Color = System.Drawing.Color.Gray;
            this.textBox40.Style.Font.Bold = true;
            this.textBox40.Style.Font.Name = "Segoe UI";
            this.textBox40.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox40.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(15D);
            this.textBox40.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox40.StyleName = "";
            this.textBox40.Value = "PPE";
            // 
            // textBox82
            // 
            this.textBox82.Name = "textBox82";
            this.textBox82.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0804983377456665D), Telerik.Reporting.Drawing.Unit.Inch(0.208333358168602D));
            this.textBox82.Style.Color = System.Drawing.Color.Gray;
            this.textBox82.Style.Font.Bold = true;
            this.textBox82.Style.Font.Name = "Segoe UI";
            this.textBox82.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox82.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(15D);
            this.textBox82.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox82.StyleName = "";
            this.textBox82.Value = "ReEntry";
            // 
            // textBox43
            // 
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.1473491191864014D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBox43.Style.Font.Underline = true;
            this.textBox43.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox43.Value = "=Fields.ProductName";
            // 
            // textBox90
            // 
            this.textBox90.Name = "textBox90";
            this.textBox90.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0804983377456665D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox90.Style.Color = System.Drawing.Color.Gray;
            this.textBox90.Style.Font.Bold = true;
            this.textBox90.Style.Font.Name = "Segoe UI";
            this.textBox90.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox90.Style.Font.Underline = false;
            this.textBox90.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(15D);
            this.textBox90.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox90.StyleName = "";
            this.textBox90.Value = "Signal Word";
            // 
            // textBox91
            // 
            this.textBox91.Name = "textBox91";
            this.textBox91.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0668506622314453D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox91.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox91.Style.Font.Underline = false;
            this.textBox91.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox91.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox91.StyleName = "";
            this.textBox91.Value = "=Fields.SignalWord";
            // 
            // textBox96
            // 
            this.textBox96.Name = "textBox96";
            this.textBox96.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0697999000549316D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBox96.Style.Font.Underline = true;
            this.textBox96.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox96.StyleName = "";
            // 
            // textBox97
            // 
            this.textBox97.Name = "textBox97";
            this.textBox97.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0697999000549316D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox97.Style.Color = System.Drawing.Color.Gray;
            this.textBox97.Style.Font.Bold = true;
            this.textBox97.Style.Font.Name = "Segoe UI";
            this.textBox97.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox97.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(15D);
            this.textBox97.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox97.StyleName = "";
            this.textBox97.Value = "RUP";
            // 
            // textBox101
            // 
            this.textBox101.Name = "textBox101";
            this.textBox101.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.7828118801116943D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBox101.Style.Font.Underline = true;
            this.textBox101.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox101.StyleName = "";
            // 
            // textBox102
            // 
            this.textBox102.Name = "textBox102";
            this.textBox102.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.7828118801116943D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox102.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox102.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox102.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox102.StyleName = "";
            this.textBox102.Value = "=Fields.RestrictedUse";
            // 
            // textBox45
            // 
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.91946268081665D), Telerik.Reporting.Drawing.Unit.Inch(0.20208343863487244D));
            this.textBox45.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox45.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox45.StyleName = "";
            this.textBox45.Value = "=Fields.PPE";
            // 
            // textBox89
            // 
            this.textBox89.Name = "textBox89";
            this.textBox89.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.91946268081665D), Telerik.Reporting.Drawing.Unit.Inch(0.208333358168602D));
            this.textBox89.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox89.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox89.StyleName = "";
            this.textBox89.Value = "=Fields.ReEntry";
            // 
            // textBox34
            // 
            this.textBox34.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(1.2082549333572388D));
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.1685433387756348D), Telerik.Reporting.Drawing.Unit.Inch(0.29992121458053589D));
            this.textBox34.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox34.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox34.Style.Font.Bold = true;
            this.textBox34.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox34.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox34.Value = Landdb.Resources.Strings.AdditionalPests_Text;
            // 
            // textBox31
            // 
            this.textBox31.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.90825462341308594D));
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.179161548614502D), Telerik.Reporting.Drawing.Unit.Inch(0.29992121458053589D));
            this.textBox31.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox31.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox31.Style.Font.Bold = true;
            this.textBox31.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox31.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox31.Value = Landdb.Resources.Strings.AppMethod_Text;
            // 
            // panel1
            // 
            this.panel1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox47,
            this.textBox48,
            this.textBox84,
            this.textBox85,
            this.textBox86,
            this.textBox87,
            this.textBox88});
            this.panel1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.11670621484518051D), Telerik.Reporting.Drawing.Unit.Inch(3.408254861831665D));
            this.panel1.Name = "panel1";
            this.panel1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.8832545280456543D), Telerik.Reporting.Drawing.Unit.Inch(0.3030761182308197D));
            // 
            // textBox37
            // 
            this.textBox37.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D), Telerik.Reporting.Drawing.Unit.Inch(3.4996464252471924D));
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.79996055364608765D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox37.Style.Font.Bold = true;
            this.textBox37.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox37.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox37.Value = Landdb.Resources.Strings.TankSize_Text;
            // 
            // textBox47
            // 
            this.textBox47.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.68329381942749023D), Telerik.Reporting.Drawing.Unit.Inch(0.10050074011087418D));
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.88525092601776123D), Telerik.Reporting.Drawing.Unit.Inch(0.19089095294475555D));
            this.textBox47.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox47.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox47.Value = "=Fields.TankSizeDisplay";
            // 
            // textBox48
            // 
            this.textBox48.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.781048059463501D), Telerik.Reporting.Drawing.Unit.Inch(0.10050010681152344D));
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.81887310743331909D), Telerik.Reporting.Drawing.Unit.Inch(0.19089095294475555D));
            this.textBox48.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox48.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox48.Value = "=Fields.CarrierPerAreaDisplay";
            // 
            // textBox84
            // 
            this.textBox84.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.6937500238418579D), Telerik.Reporting.Drawing.Unit.Inch(0.10050010681152344D));
            this.textBox84.Name = "textBox84";
            this.textBox84.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0872193574905396D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox84.Style.Font.Bold = true;
            this.textBox84.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox84.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox84.Value = Landdb.Resources.Strings.CarrierArea_Text;
            // 
            // textBox85
            // 
            this.textBox85.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.6146702766418457D), Telerik.Reporting.Drawing.Unit.Inch(0.1096089705824852D));
            this.textBox85.Name = "textBox85";
            this.textBox85.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0852508544921875D), Telerik.Reporting.Drawing.Unit.Inch(0.19089095294475555D));
            this.textBox85.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox85.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox85.Value = "=Fields.TotalCarrierDisplay";
            // 
            // textBox86
            // 
            this.textBox86.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.6000001430511475D), Telerik.Reporting.Drawing.Unit.Inch(0.091351829469203949D));
            this.textBox86.Name = "textBox86";
            this.textBox86.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0145909786224365D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox86.Style.Font.Bold = true;
            this.textBox86.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox86.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox86.Value = Landdb.Resources.Strings.TotalCarrier_Text;
            // 
            // textBox87
            // 
            this.textBox87.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.7979168891906738D), Telerik.Reporting.Drawing.Unit.Inch(0.091351829469203949D));
            this.textBox87.Name = "textBox87";
            this.textBox87.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0979630947113037D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox87.Style.Font.Bold = true;
            this.textBox87.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox87.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox87.Value = Landdb.Resources.Strings.TankCount_Text;
            // 
            // textBox88
            // 
            this.textBox88.Format = "{0:N2}";
            this.textBox88.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.8187503814697266D), Telerik.Reporting.Drawing.Unit.Inch(0.11218515783548355D));
            this.textBox88.Name = "textBox88";
            this.textBox88.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0645040273666382D), Telerik.Reporting.Drawing.Unit.Inch(0.19089095294475555D));
            this.textBox88.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox88.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox88.Value = "=Fields.TankCount";
            // 
            // textBox50
            // 
            this.textBox50.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(4.5D));
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.9105391502380371D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox50.Style.BackgroundColor = System.Drawing.Color.Silver;
            this.textBox50.Style.Color = System.Drawing.Color.Black;
            this.textBox50.Style.Font.Bold = true;
            this.textBox50.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox50.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox50.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox50.Value = Landdb.Resources.Strings.Fields_Text;
            // 
            // table2
            // 
            this.table2.Bindings.Add(new Telerik.Reporting.Binding("DataSource", "=Fields.Fields"));
            this.table2.Bindings.Add(new Telerik.Reporting.Binding("Visible", "=Fields.Fields.Count > 0"));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.135344386100769D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.135344386100769D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.1770075559616089D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.3019999265670776D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.1900005340576172D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.87494432926177979D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.88535964488983154D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.591666579246521D)));
            this.table2.Body.SetCellContent(0, 0, this.textBox52);
            this.table2.Body.SetCellContent(0, 1, this.textBox54);
            this.table2.Body.SetCellContent(0, 2, this.textBox56);
            this.table2.Body.SetCellContent(0, 4, this.textBox58);
            this.table2.Body.SetCellContent(0, 5, this.textBox60);
            this.table2.Body.SetCellContent(0, 6, this.textBox62);
            this.table2.Body.SetCellContent(0, 3, this.textBox64);
            this.table2.Body.SetCellContent(1, 0, this.table1, 1, 7);
            tableGroup23.Name = "tableGroup3";
            tableGroup23.ReportItem = this.textBox51;
            tableGroup24.Name = "tableGroup4";
            tableGroup24.ReportItem = this.textBox53;
            tableGroup25.Name = "tableGroup5";
            tableGroup25.ReportItem = this.textBox55;
            tableGroup26.Name = "group10";
            tableGroup26.ReportItem = this.textBox63;
            tableGroup27.Name = "group7";
            tableGroup27.ReportItem = this.textBox57;
            tableGroup28.Name = "group8";
            tableGroup28.ReportItem = this.textBox59;
            tableGroup29.Name = "group9";
            tableGroup29.ReportItem = this.textBox61;
            this.table2.ColumnGroups.Add(tableGroup23);
            this.table2.ColumnGroups.Add(tableGroup24);
            this.table2.ColumnGroups.Add(tableGroup25);
            this.table2.ColumnGroups.Add(tableGroup26);
            this.table2.ColumnGroups.Add(tableGroup27);
            this.table2.ColumnGroups.Add(tableGroup28);
            this.table2.ColumnGroups.Add(tableGroup29);
            this.table2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox52,
            this.textBox54,
            this.textBox56,
            this.textBox64,
            this.textBox58,
            this.textBox60,
            this.textBox62,
            this.table1,
            this.textBox51,
            this.textBox53,
            this.textBox55,
            this.textBox63,
            this.textBox57,
            this.textBox59,
            this.textBox61});
            this.table2.KeepTogether = false;
            this.table2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(4.7000002861022949D));
            this.table2.Name = "table2";
            tableGroup31.Name = "group23";
            tableGroup32.Name = "group24";
            tableGroup30.ChildGroups.Add(tableGroup31);
            tableGroup30.ChildGroups.Add(tableGroup32);
            tableGroup30.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup30.Name = "detailTableGroup1";
            this.table2.RowGroups.Add(tableGroup30);
            this.table2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.7000002861022949D), Telerik.Reporting.Drawing.Unit.Inch(1.00166654586792D));
            // 
            // textBox52
            // 
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.135344386100769D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox52.Style.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.textBox52.Value = "=Fields.Farm";
            // 
            // textBox54
            // 
            this.textBox54.CanGrow = false;
            this.textBox54.Name = "textBox54";
            this.textBox54.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.135344386100769D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox54.Style.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.textBox54.Value = "=Fields.Field";
            // 
            // textBox56
            // 
            this.textBox56.CanGrow = false;
            this.textBox56.Name = "textBox56";
            this.textBox56.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1770075559616089D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox56.Style.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.textBox56.Value = "=Fields.CropZone";
            // 
            // textBox58
            // 
            this.textBox58.CanGrow = false;
            this.textBox58.Name = "textBox58";
            this.textBox58.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1900005340576172D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox58.Style.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.textBox58.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox58.StyleName = "";
            this.textBox58.Value = "=Fields.CenterLatLong";
            // 
            // textBox60
            // 
            this.textBox60.CanGrow = false;
            this.textBox60.Format = "{0:N2}";
            this.textBox60.Name = "textBox60";
            this.textBox60.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.87494438886642456D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox60.Style.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.textBox60.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox60.StyleName = "";
            this.textBox60.Value = "=Fields.Area";
            // 
            // textBox62
            // 
            this.textBox62.CanGrow = false;
            this.textBox62.Name = "textBox62";
            this.textBox62.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.88535970449447632D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox62.Style.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.textBox62.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox62.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox62.Style.Font.Bold = true;
            this.textBox62.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox62.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox62.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox62.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox62.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox62.StyleName = "";
            // 
            // textBox64
            // 
            this.textBox64.CanGrow = false;
            this.textBox64.Name = "textBox64";
            this.textBox64.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3019999265670776D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox64.Style.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.textBox64.StyleName = "";
            this.textBox64.Value = "=Fields.Crop";
            // 
            // textBox65
            // 
            this.textBox65.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(-4.9670538793122887E-09D), Telerik.Reporting.Drawing.Unit.Inch(5.8000006675720215D));
            this.textBox65.Name = "textBox65";
            this.textBox65.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.9999608993530273D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox65.Style.BackgroundColor = System.Drawing.Color.Silver;
            this.textBox65.Style.Color = System.Drawing.Color.Black;
            this.textBox65.Style.Font.Bold = true;
            this.textBox65.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox65.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox65.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox65.Value = Landdb.Resources.Strings.Notes_Text;
            // 
            // textBox66
            // 
            this.textBox66.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(6.00007963180542D));
            this.textBox66.Name = "textBox66";
            this.textBox66.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.9999608993530273D), Telerik.Reporting.Drawing.Unit.Inch(0.20000013709068298D));
            this.textBox66.Value = "=Fields.Notes";
            // 
            // textBox67
            // 
            this.textBox67.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(6.4916667938232422D));
            this.textBox67.Name = "textBox67";
            this.textBox67.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.0397238731384277D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBox67.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox67.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox67.Style.Font.Bold = true;
            this.textBox67.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox67.Value = Landdb.Resources.Strings.Signature_Text;
            // 
            // textBox68
            // 
            this.textBox68.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.248136043548584D), Telerik.Reporting.Drawing.Unit.Inch(6.4916667938232422D));
            this.textBox68.Name = "textBox68";
            this.textBox68.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.7518250942230225D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBox68.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox68.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox68.Style.Font.Bold = true;
            this.textBox68.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox68.Value = Landdb.Resources.Strings.Date_Text;
            // 
            // table3
            // 
            this.table3.Bindings.Add(new Telerik.Reporting.Binding("DataSource", "=Fields.Applicators"));
            this.table3.Bindings.Add(new Telerik.Reporting.Binding("Visible", "=Fields.Applicators.Count > 0"));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.3088357448577881D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.8658846616744995D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.18833327293395996D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.17708322405815125D)));
            this.table3.Body.SetCellContent(0, 0, this.textBox71);
            this.table3.Body.SetCellContent(0, 1, this.textBox73);
            this.table3.Body.SetCellContent(1, 0, this.textBox76);
            this.table3.Body.SetCellContent(1, 1, this.textBox75);
            tableGroup33.Name = "tableGroup6";
            tableGroup33.ReportItem = this.textBox70;
            tableGroup34.Name = "tableGroup7";
            tableGroup34.ReportItem = this.textBox72;
            this.table3.ColumnGroups.Add(tableGroup33);
            this.table3.ColumnGroups.Add(tableGroup34);
            this.table3.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox71,
            this.textBox73,
            this.textBox76,
            this.textBox75,
            this.textBox70,
            this.textBox72});
            this.table3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(1.6999214887619019D));
            this.table3.Name = "table3";
            tableGroup36.Name = "group11";
            tableGroup37.Name = "group12";
            tableGroup35.ChildGroups.Add(tableGroup36);
            tableGroup35.ChildGroups.Add(tableGroup37);
            tableGroup35.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup35.Name = "detailTableGroup2";
            this.table3.RowGroups.Add(tableGroup35);
            this.table3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.174720287322998D), Telerik.Reporting.Drawing.Unit.Inch(0.53208315372467041D));
            // 
            // textBox71
            // 
            this.textBox71.Name = "textBox71";
            this.textBox71.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.30883526802063D), Telerik.Reporting.Drawing.Unit.Inch(0.18833325803279877D));
            this.textBox71.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox71.Value = "=Fields.Name";
            // 
            // textBox73
            // 
            this.textBox73.Name = "textBox73";
            this.textBox73.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8658847808837891D), Telerik.Reporting.Drawing.Unit.Inch(0.18833327293395996D));
            this.textBox73.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox73.Value = "=Fields.LicenseNumber";
            // 
            // textBox76
            // 
            this.textBox76.Name = "textBox76";
            this.textBox76.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.30883526802063D), Telerik.Reporting.Drawing.Unit.Inch(0.17708323895931244D));
            this.textBox76.Style.Font.Italic = true;
            this.textBox76.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox76.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox76.StyleName = "";
            this.textBox76.Value = "=Fields.Company";
            // 
            // textBox75
            // 
            this.textBox75.Format = "{0:d}";
            this.textBox75.Name = "textBox75";
            this.textBox75.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8658847808837891D), Telerik.Reporting.Drawing.Unit.Inch(0.17708325386047363D));
            this.textBox75.Style.Font.Italic = true;
            this.textBox75.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox75.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox75.Value = "=Fields.ExpirationDate";
            // 
            // textBox11
            // 
            this.textBox11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.09992130845785141D));
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.1727099418640137D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox11.Style.BackgroundColor = System.Drawing.Color.Silver;
            this.textBox11.Style.Color = System.Drawing.SystemColors.WindowText;
            this.textBox11.Style.Font.Bold = true;
            this.textBox11.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox11.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox11.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox11.Value = Landdb.Resources.Strings.ApplicationDateAndTime;
            // 
            // textBox12
            // 
            this.textBox12.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.29992127418518066D));
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.89575463533401489D), Telerik.Reporting.Drawing.Unit.Inch(0.29992121458053589D));
            this.textBox12.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox12.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox12.Style.Font.Bold = true;
            this.textBox12.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox12.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox12.Value = Landdb.Resources.Strings.StartTime_Text;
            // 
            // textBox13
            // 
            this.textBox13.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.608254611492157D));
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.89775925874710083D), Telerik.Reporting.Drawing.Unit.Inch(0.29992121458053589D));
            this.textBox13.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox13.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox13.Style.Font.Bold = true;
            this.textBox13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox13.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox13.Value = Landdb.Resources.Strings.FinishTime_Text;
            // 
            // textBox14
            // 
            this.textBox14.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(1.499921441078186D));
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.179161548614502D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox14.Style.BackgroundColor = System.Drawing.Color.Silver;
            this.textBox14.Style.Color = System.Drawing.Color.Black;
            this.textBox14.Style.Font.Bold = true;
            this.textBox14.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox14.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox14.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox14.Value = Landdb.Resources.Strings.Applicators_Text;
            // 
            // textBox15
            // 
            this.textBox15.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(2.2999215126037598D));
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.178886890411377D), Telerik.Reporting.Drawing.Unit.Inch(0.2022802084684372D));
            this.textBox15.Style.BackgroundColor = System.Drawing.Color.Silver;
            this.textBox15.Style.Color = System.Drawing.Color.Black;
            this.textBox15.Style.Font.Bold = true;
            this.textBox15.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox15.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox15.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox15.Value = Landdb.Resources.Strings.Environmental_Text;
            // 
            // textBox16
            // 
            this.textBox16.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(2.8165881633758545D));
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.178886890411377D), Telerik.Reporting.Drawing.Unit.Inch(0.30220144987106323D));
            this.textBox16.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox16.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox16.Style.Font.Bold = true;
            this.textBox16.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox16.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox16.Value = Landdb.Resources.Strings.SoilCondition_Text;
            // 
            // textBox17
            // 
            this.textBox17.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(2.5082547664642334D));
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.178886890411377D), Telerik.Reporting.Drawing.Unit.Inch(0.30220144987106323D));
            this.textBox17.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox17.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox17.Style.Font.Bold = true;
            this.textBox17.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox17.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox17.Value = Landdb.Resources.Strings.SkyCondition_Text;
            // 
            // textBox18
            // 
            this.textBox18.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(7.9472862068996619E-08D), Telerik.Reporting.Drawing.Unit.Inch(3.2006187438964844D));
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.89575445652008057D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox18.Style.Font.Bold = true;
            this.textBox18.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox18.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox18.Value = Landdb.Resources.Strings.WindSpeed_Text;
            // 
            // textBox19
            // 
            this.textBox19.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.8083728551864624D), Telerik.Reporting.Drawing.Unit.Inch(3.1999213695526123D));
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0977592468261719D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox19.Style.Font.Bold = true;
            this.textBox19.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox19.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox19.Value = Landdb.Resources.Strings.WindDirection_Text;
            // 
            // textBox20
            // 
            this.textBox20.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.7167060375213623D), Telerik.Reporting.Drawing.Unit.Inch(3.1999213695526123D));
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.99999994039535522D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox20.Style.Font.Bold = true;
            this.textBox20.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox20.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox20.Value = Landdb.Resources.Strings.Temperature_Text;
            // 
            // textBox21
            // 
            this.textBox21.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.9167065620422363D), Telerik.Reporting.Drawing.Unit.Inch(3.1999213695526123D));
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.88113147020339966D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox21.Style.Font.Bold = true;
            this.textBox21.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox21.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox21.Value = Landdb.Resources.Strings.HumidityText;
            // 
            // textBox22
            // 
            this.textBox22.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.90000003576278687D), Telerik.Reporting.Drawing.Unit.Inch(3.21728515625D));
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.70216244459152222D), Telerik.Reporting.Drawing.Unit.Inch(0.19089095294475555D));
            this.textBox22.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox22.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox22.Value = "";
            // 
            // textBox23
            // 
            this.textBox23.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.9167060852050781D), Telerik.Reporting.Drawing.Unit.Inch(3.1999213695526123D));
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.80208337306976318D), Telerik.Reporting.Drawing.Unit.Inch(0.19089095294475555D));
            this.textBox23.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox23.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox23.Value = "";
            // 
            // textBox24
            // 
            this.textBox24.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.7167062759399414D), Telerik.Reporting.Drawing.Unit.Inch(3.1999213695526123D));
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0999215841293335D), Telerik.Reporting.Drawing.Unit.Inch(0.19089095294475555D));
            this.textBox24.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox24.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox24.Value = "";
            // 
            // textBox25
            // 
            this.textBox25.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.7167067527771D), Telerik.Reporting.Drawing.Unit.Inch(3.1999213695526123D));
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2832934856414795D), Telerik.Reporting.Drawing.Unit.Inch(0.19089095294475555D));
            this.textBox25.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox25.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox25.Value = "";
            // 
            // textBox32
            // 
            this.textBox32.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.89031726121902466D), Telerik.Reporting.Drawing.Unit.Inch(0.29992127418518066D));
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.2705543041229248D), Telerik.Reporting.Drawing.Unit.Inch(0.29992121458053589D));
            this.textBox32.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox32.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox32.Style.Font.Bold = true;
            this.textBox32.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox32.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox32.Value = "=Fields.StartDate";
            // 
            // textBox46
            // 
            this.textBox46.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.88611102104187012D), Telerik.Reporting.Drawing.Unit.Inch(0.60825467109680176D));
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.2747600078582764D), Telerik.Reporting.Drawing.Unit.Inch(0.29992121458053589D));
            this.textBox46.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox46.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox46.Style.Font.Bold = true;
            this.textBox46.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox46.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox46.Value = "=Fields.EndDate";
            // 
            // table5
            // 
            this.table5.Bindings.Add(new Telerik.Reporting.Binding("DataSource", "=Fields.Products"));
            this.table5.Bindings.Add(new Telerik.Reporting.Binding("Visible", "=Fields.Products.Count > 0"));
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.718120813369751D)));
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.7075183391571045D)));
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.2672187089920044D)));
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.1126120090484619D)));
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.185782790184021D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.18850404024124146D)));
            this.table5.Body.SetCellContent(0, 0, this.textBox49);
            this.table5.Body.SetCellContent(0, 4, this.textBox74);
            this.table5.Body.SetCellContent(0, 3, this.textBox98);
            this.table5.Body.SetCellContent(0, 1, this.textBox106);
            this.table5.Body.SetCellContent(0, 2, this.textBox107);
            tableGroup38.Name = "tableGroup";
            tableGroup38.ReportItem = this.textBox112;
            tableGroup39.Name = "group14";
            tableGroup39.ReportItem = this.textBox113;
            tableGroup40.Name = "group13";
            tableGroup40.ReportItem = this.textBox116;
            tableGroup41.Name = "group4";
            tableGroup41.ReportItem = this.textBox117;
            tableGroup42.Name = "group3";
            tableGroup42.ReportItem = this.textBox118;
            this.table5.ColumnGroups.Add(tableGroup38);
            this.table5.ColumnGroups.Add(tableGroup39);
            this.table5.ColumnGroups.Add(tableGroup40);
            this.table5.ColumnGroups.Add(tableGroup41);
            this.table5.ColumnGroups.Add(tableGroup42);
            this.table5.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox49,
            this.textBox106,
            this.textBox107,
            this.textBox98,
            this.textBox74,
            this.textBox112,
            this.textBox113,
            this.textBox116,
            this.textBox117,
            this.textBox118});
            this.table5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.008707679808139801D), Telerik.Reporting.Drawing.Unit.Inch(3.9999992847442627D));
            this.table5.Name = "table5";
            tableGroup44.Name = "group1";
            tableGroup43.ChildGroups.Add(tableGroup44);
            tableGroup43.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup43.Name = "detailTableGroup";
            this.table5.RowGroups.Add(tableGroup43);
            this.table5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.9912524223327637D), Telerik.Reporting.Drawing.Unit.Inch(0.41508236527442932D));
            this.table5.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D);
            // 
            // textBox49
            // 
            this.textBox49.Name = "textBox49";
            this.textBox49.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.718120813369751D), Telerik.Reporting.Drawing.Unit.Inch(0.18850404024124146D));
            this.textBox49.Style.Font.Bold = true;
            this.textBox49.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.textBox49.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.textBox49.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox49.Value = "=Fields.ProductName";
            // 
            // textBox74
            // 
            this.textBox74.CanGrow = false;
            this.textBox74.Name = "textBox74";
            this.textBox74.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1857826709747315D), Telerik.Reporting.Drawing.Unit.Inch(0.18850404024124146D));
            this.textBox74.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D);
            this.textBox74.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox74.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox74.StyleName = "";
            this.textBox74.Value = "=Fields.TotalDisplay";
            // 
            // textBox98
            // 
            this.textBox98.CanGrow = false;
            this.textBox98.Name = "textBox98";
            this.textBox98.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1126120090484619D), Telerik.Reporting.Drawing.Unit.Inch(0.18850404024124146D));
            this.textBox98.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D);
            this.textBox98.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox98.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox98.StyleName = "";
            this.textBox98.Value = "=Fields.RateDisplay";
            // 
            // textBox106
            // 
            this.textBox106.Name = "textBox106";
            this.textBox106.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7075183391571045D), Telerik.Reporting.Drawing.Unit.Inch(0.18850404024124146D));
            this.textBox106.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D);
            this.textBox106.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.textBox106.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox106.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox106.StyleName = "";
            this.textBox106.Value = "=Fields.EPANumber";
            // 
            // textBox107
            // 
            this.textBox107.Bindings.Add(new Telerik.Reporting.Binding("Visible", "=Fields.RateType = \"Rate/Tank\""));
            this.textBox107.Name = "textBox107";
            this.textBox107.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2672187089920044D), Telerik.Reporting.Drawing.Unit.Inch(0.18850404024124146D));
            this.textBox107.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D);
            this.textBox107.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox107.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox107.StyleName = "";
            this.textBox107.Value = "=Fields.RatePerTankDisplay";
            // 
            // textBox69
            // 
            this.textBox69.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.0087076826021075249D), Telerik.Reporting.Drawing.Unit.Inch(3.7999217510223389D));
            this.textBox69.Name = "textBox69";
            this.textBox69.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.9912528991699219D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox69.Style.BackgroundColor = System.Drawing.Color.Silver;
            this.textBox69.Style.Color = System.Drawing.Color.Black;
            this.textBox69.Style.Font.Bold = true;
            this.textBox69.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox69.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox69.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox69.Value = Landdb.Resources.Strings.Products_Text;
            // 
            // pageFooterSection1
            // 
            this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.41043153405189514D);
            this.pageFooterSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox5,
            this.pageInfoTextBox,
            this.textBox6});
            this.pageFooterSection1.Name = "pageFooterSection1";
            // 
            // txtDataSourceLabel
            // 
            this.txtDataSourceLabel.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.0087076826021075249D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.txtDataSourceLabel.Name = "txtDataSourceLabel";
            this.txtDataSourceLabel.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.0312089920043945D), Telerik.Reporting.Drawing.Unit.Inch(0.50000005960464478D));
            this.txtDataSourceLabel.Style.Font.Name = "Segoe UI Light";
            this.txtDataSourceLabel.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(24D);
            this.txtDataSourceLabel.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.txtDataSourceLabel.Value = "=Fields.DataSourceName";
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.5312881469726562D), Telerik.Reporting.Drawing.Unit.Inch(0.20003946125507355D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.4687116146087646D), Telerik.Reporting.Drawing.Unit.Inch(0.29996061325073242D));
            this.textBox1.Style.Color = System.Drawing.Color.DimGray;
            this.textBox1.Style.Font.Name = "Segoe UI Light";
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(18D);
            this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox1.TextWrap = false;
            this.textBox1.Value = "=Fields.Title";
            // 
            // textBox2
            // 
            this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.0001182556152344D), Telerik.Reporting.Drawing.Unit.Inch(0.50007873773574829D));
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9998817443847656D), Telerik.Reporting.Drawing.Unit.Inch(0.19996063411235809D));
            this.textBox2.Style.Color = System.Drawing.Color.DimGray;
            this.textBox2.Style.Font.Name = "Segoe UI";
            this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox2.TextWrap = false;
            this.textBox2.Value = "=Fields.CropYear";
            // 
            // textBox3
            // 
            this.textBox3.CanGrow = false;
            this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.1937496662139893D), Telerik.Reporting.Drawing.Unit.Inch(0.70791840553283691D));
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.8062500953674316D), Telerik.Reporting.Drawing.Unit.Inch(0.18942584097385407D));
            this.textBox3.Style.Color = System.Drawing.Color.DimGray;
            this.textBox3.Style.Font.Name = "Segoe UI Light";
            this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox3.Value = "=Fields.Authorizer";
            // 
            // textBox4
            // 
            formattingRule1.Filters.Add(new Telerik.Reporting.Filter("=Fields.AuthorizerDate", Telerik.Reporting.FilterOperator.LessThan, "1901-01-01"));
            formattingRule1.Style.Visible = false;
            this.textBox4.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule1});
            this.textBox4.Format = "{0:d}";
            this.textBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.6063685417175293D), Telerik.Reporting.Drawing.Unit.Inch(0.89742302894592285D));
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.3935921192169189D), Telerik.Reporting.Drawing.Unit.Inch(0.21025960147380829D));
            this.textBox4.Style.Color = System.Drawing.Color.DimGray;
            this.textBox4.Style.Font.Name = "Segoe UI";
            this.textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox4.Value = "=Fields.AuthorizerDate";
            // 
            // textBox99
            // 
            this.textBox99.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.0087076826021075249D), Telerik.Reporting.Drawing.Unit.Inch(0.70791840553283691D));
            this.textBox99.Name = "textBox99";
            this.textBox99.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.0812106132507324D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox99.Style.Font.Bold = true;
            this.textBox99.Style.Font.Name = "Segoe UI";
            this.textBox99.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox99.Value = "=Fields.WorkOrderName";
            // 
            // textBox103
            // 
            this.textBox103.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.50783973932266235D));
            this.textBox103.Name = "textBox103";
            this.textBox103.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.0812108516693115D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox103.Style.Font.Name = "Segoe UI";
            this.textBox103.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox103.Value = "=Fields.WorkOrderDate";
            // 
            // textBox5
            // 
            this.textBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.8000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.11047109216451645D));
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0937893390655518D), Telerik.Reporting.Drawing.Unit.Inch(0.29996046423912048D));
            this.textBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox5.Value = "generated on {Now()}";
            // 
            // pageInfoTextBox
            // 
            this.pageInfoTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.61527681350708D), Telerik.Reporting.Drawing.Unit.Inch(0.099999748170375824D));
            this.pageInfoTextBox.Name = "pageInfoTextBox";
            this.pageInfoTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.38468337059021D), Telerik.Reporting.Drawing.Unit.Inch(0.29999962449073792D));
            this.pageInfoTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.pageInfoTextBox.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.pageInfoTextBox.StyleName = "PageInfo";
            this.pageInfoTextBox.Value = "=PageNumber";
            // 
            // textBox6
            // 
            this.textBox6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.11047109216451645D));
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8790887594223023D), Telerik.Reporting.Drawing.Unit.Inch(0.29996046423912048D));
            this.textBox6.Style.Color = System.Drawing.Color.DimGray;
            this.textBox6.Style.Font.Bold = true;
            this.textBox6.Style.Font.Italic = false;
            this.textBox6.Style.Font.Name = "Segoe UI Light";
            this.textBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(16D);
            this.textBox6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox6.Value = "AG CONNECTIONS";
            // 
            // textBox38
            // 
            this.textBox38.CanGrow = false;
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.95561796426773071D), Telerik.Reporting.Drawing.Unit.Inch(0.22657832503318787D));
            this.textBox38.Style.Font.Bold = true;
            this.textBox38.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox38.Style.Font.Underline = true;
            this.textBox38.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D);
            this.textBox38.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox38.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox38.StyleName = "";
            this.textBox38.Value = "Total";
            // 
            // textBox41
            // 
            this.textBox41.CanGrow = false;
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.82069802284240723D), Telerik.Reporting.Drawing.Unit.Inch(0.22657832503318787D));
            this.textBox41.Style.Font.Bold = true;
            this.textBox41.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox41.Style.Font.Underline = true;
            this.textBox41.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D);
            this.textBox41.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox41.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox41.StyleName = "";
            this.textBox41.Value = "Rate/Area";
            // 
            // textBox36
            // 
            this.textBox36.Bindings.Add(new Telerik.Reporting.Binding("Visible", "=Fields.RateType = \"Rate/Tank\""));
            this.textBox36.CanGrow = false;
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.90967828035354614D), Telerik.Reporting.Drawing.Unit.Inch(0.22657832503318787D));
            this.textBox36.Style.Font.Bold = true;
            this.textBox36.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox36.Style.Font.Underline = true;
            this.textBox36.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D);
            this.textBox36.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox36.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox36.Style.Visible = true;
            this.textBox36.StyleName = "";
            this.textBox36.Value = "Rate/Tank";
            // 
            // textBox26
            // 
            this.textBox26.CanGrow = false;
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.61849462985992432D), Telerik.Reporting.Drawing.Unit.Inch(0.22657832503318787D));
            this.textBox26.Style.Font.Bold = true;
            this.textBox26.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox26.Style.Font.Underline = true;
            this.textBox26.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D);
            this.textBox26.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox26.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox26.StyleName = "";
            this.textBox26.Value = "PHI(d)";
            // 
            // textBox28
            // 
            this.textBox28.CanGrow = false;
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.607997715473175D), Telerik.Reporting.Drawing.Unit.Inch(0.22657832503318787D));
            this.textBox28.Style.Font.Bold = true;
            this.textBox28.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox28.Style.Font.Underline = true;
            this.textBox28.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D);
            this.textBox28.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox28.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox28.StyleName = "";
            this.textBox28.Value = "REI(h)";
            // 
            // textBox29
            // 
            this.textBox29.CanGrow = false;
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.520967960357666D), Telerik.Reporting.Drawing.Unit.Inch(0.22657832503318787D));
            this.textBox29.Style.Font.Bold = true;
            this.textBox29.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox29.Style.Font.Underline = true;
            this.textBox29.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D);
            this.textBox29.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox29.StyleName = "";
            this.textBox29.Value = "EPA Number / Pest";
            // 
            // ProductName
            // 
            this.ProductName.CanGrow = false;
            this.ProductName.Name = "ProductName";
            this.ProductName.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.2665462493896484D), Telerik.Reporting.Drawing.Unit.Inch(0.22657832503318787D));
            this.ProductName.Style.Font.Bold = true;
            this.ProductName.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.ProductName.Style.Font.Underline = true;
            this.ProductName.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D);
            this.ProductName.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.ProductName.Value = "Name / Active";
            // 
            // textBox78
            // 
            this.textBox78.CanGrow = false;
            this.textBox78.Name = "textBox78";
            this.textBox78.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.95561796426773071D), Telerik.Reporting.Drawing.Unit.Inch(0.176584392786026D));
            this.textBox78.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox78.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox78.Style.Font.Bold = true;
            this.textBox78.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox78.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D);
            this.textBox78.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox78.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox78.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox78.StyleName = "";
            // 
            // textBox77
            // 
            this.textBox77.CanGrow = false;
            this.textBox77.Name = "textBox77";
            this.textBox77.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.82069802284240723D), Telerik.Reporting.Drawing.Unit.Inch(0.176584392786026D));
            this.textBox77.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox77.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox77.Style.Font.Bold = true;
            this.textBox77.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox77.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D);
            this.textBox77.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox77.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox77.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox77.StyleName = "";
            // 
            // textBox83
            // 
            this.textBox83.CanGrow = false;
            this.textBox83.Name = "textBox83";
            this.textBox83.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.90967828035354614D), Telerik.Reporting.Drawing.Unit.Inch(0.176584392786026D));
            this.textBox83.Style.Font.Bold = true;
            this.textBox83.Style.Font.Italic = true;
            this.textBox83.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox83.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D);
            this.textBox83.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox83.StyleName = "";
            // 
            // textBox94
            // 
            this.textBox94.CanGrow = false;
            this.textBox94.Name = "textBox94";
            this.textBox94.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.61849462985992432D), Telerik.Reporting.Drawing.Unit.Inch(0.176584392786026D));
            this.textBox94.Style.Font.Italic = true;
            this.textBox94.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox94.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D);
            this.textBox94.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox94.StyleName = "";
            // 
            // textBox93
            // 
            this.textBox93.CanGrow = false;
            this.textBox93.Name = "textBox93";
            this.textBox93.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.607997715473175D), Telerik.Reporting.Drawing.Unit.Inch(0.176584392786026D));
            this.textBox93.Style.Font.Italic = true;
            this.textBox93.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox93.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D);
            this.textBox93.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox93.StyleName = "";
            // 
            // textBox92
            // 
            this.textBox92.CanGrow = false;
            this.textBox92.Name = "textBox92";
            this.textBox92.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.520967960357666D), Telerik.Reporting.Drawing.Unit.Inch(0.176584392786026D));
            this.textBox92.Style.Font.Italic = true;
            this.textBox92.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox92.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D);
            this.textBox92.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox92.StyleName = "";
            this.textBox92.Value = "=Fields.Pest";
            // 
            // textBox81
            // 
            this.textBox81.CanGrow = false;
            this.textBox81.Name = "textBox81";
            this.textBox81.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.2665462493896484D), Telerik.Reporting.Drawing.Unit.Inch(0.176584392786026D));
            this.textBox81.Style.Font.Italic = true;
            this.textBox81.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox81.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D);
            this.textBox81.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox81.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox81.StyleName = "";
            this.textBox81.Value = "=Fields.Active";
            // 
            // textBox39
            // 
            this.textBox39.CanGrow = false;
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.95561796426773071D), Telerik.Reporting.Drawing.Unit.Inch(0.1885039210319519D));
            this.textBox39.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D);
            this.textBox39.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox39.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox39.StyleName = "";
            this.textBox39.Value = "=Fields.TotalDisplay";
            // 
            // textBox42
            // 
            this.textBox42.CanGrow = false;
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.82069802284240723D), Telerik.Reporting.Drawing.Unit.Inch(0.1885039210319519D));
            this.textBox42.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D);
            this.textBox42.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox42.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox42.StyleName = "";
            this.textBox42.Value = "=Fields.RateDisplay";
            // 
            // textBox80
            // 
            this.textBox80.Bindings.Add(new Telerik.Reporting.Binding("Visible", "=Fields.RateType = \"Rate/Tank\""));
            this.textBox80.CanGrow = false;
            this.textBox80.Name = "textBox80";
            this.textBox80.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.90967828035354614D), Telerik.Reporting.Drawing.Unit.Inch(0.1885039210319519D));
            this.textBox80.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D);
            this.textBox80.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox80.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox80.StyleName = "";
            this.textBox80.Value = "=Fields.RatePerTankDisplay";
            // 
            // textBox33
            // 
            this.textBox33.CanGrow = false;
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.61849462985992432D), Telerik.Reporting.Drawing.Unit.Inch(0.1885039210319519D));
            this.textBox33.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D);
            this.textBox33.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox33.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox33.StyleName = "";
            this.textBox33.Value = "=Fields.PHI";
            // 
            // textBox44
            // 
            this.textBox44.CanGrow = false;
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.607997715473175D), Telerik.Reporting.Drawing.Unit.Inch(0.1885039210319519D));
            this.textBox44.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D);
            this.textBox44.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.textBox44.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox44.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox44.Value = "=Fields.REI";
            // 
            // textBox79
            // 
            this.textBox79.CanGrow = false;
            this.textBox79.Name = "textBox79";
            this.textBox79.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.520967960357666D), Telerik.Reporting.Drawing.Unit.Inch(0.1885039210319519D));
            this.textBox79.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D);
            this.textBox79.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.textBox79.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox79.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox79.StyleName = "";
            this.textBox79.Value = "=Fields.EPANumber";
            // 
            // textBox27
            // 
            this.textBox27.CanGrow = false;
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.2665462493896484D), Telerik.Reporting.Drawing.Unit.Inch(0.1885039210319519D));
            this.textBox27.Style.Font.Bold = true;
            this.textBox27.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D);
            this.textBox27.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.textBox27.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox27.Value = "=Fields.ProductName";
            // 
            // table1
            // 
            this.table1.Bindings.Add(new Telerik.Reporting.Binding("DataSource", "=Fields.Products"));
            this.table1.Bindings.Add(new Telerik.Reporting.Binding("Visible", "=Fields.Products.Count > 0"));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.2665462493896484D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.520967960357666D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.607997715473175D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.61849462985992432D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.90967828035354614D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.82069802284240723D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.95561802387237549D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.1885039210319519D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.17658437788486481D)));
            this.table1.Body.SetCellContent(0, 0, this.textBox27);
            this.table1.Body.SetCellContent(0, 3, this.textBox33);
            this.table1.Body.SetCellContent(0, 6, this.textBox39);
            this.table1.Body.SetCellContent(0, 5, this.textBox42);
            this.table1.Body.SetCellContent(0, 2, this.textBox44);
            this.table1.Body.SetCellContent(1, 5, this.textBox77);
            this.table1.Body.SetCellContent(1, 6, this.textBox78);
            this.table1.Body.SetCellContent(1, 4, this.textBox83);
            this.table1.Body.SetCellContent(0, 1, this.textBox79);
            this.table1.Body.SetCellContent(0, 4, this.textBox80);
            this.table1.Body.SetCellContent(1, 0, this.textBox81);
            this.table1.Body.SetCellContent(1, 1, this.textBox92);
            this.table1.Body.SetCellContent(1, 2, this.textBox93);
            this.table1.Body.SetCellContent(1, 3, this.textBox94);
            tableGroup13.Name = "tableGroup";
            tableGroup13.ReportItem = this.ProductName;
            tableGroup14.Name = "group14";
            tableGroup14.ReportItem = this.textBox29;
            tableGroup15.Name = "group5";
            tableGroup15.ReportItem = this.textBox28;
            tableGroup16.Name = "group";
            tableGroup16.ReportItem = this.textBox26;
            tableGroup17.Name = "group13";
            tableGroup17.ReportItem = this.textBox36;
            tableGroup18.Name = "group4";
            tableGroup18.ReportItem = this.textBox41;
            tableGroup19.Name = "group3";
            tableGroup19.ReportItem = this.textBox38;
            this.table1.ColumnGroups.Add(tableGroup13);
            this.table1.ColumnGroups.Add(tableGroup14);
            this.table1.ColumnGroups.Add(tableGroup15);
            this.table1.ColumnGroups.Add(tableGroup16);
            this.table1.ColumnGroups.Add(tableGroup17);
            this.table1.ColumnGroups.Add(tableGroup18);
            this.table1.ColumnGroups.Add(tableGroup19);
            this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox27,
            this.textBox79,
            this.textBox44,
            this.textBox33,
            this.textBox80,
            this.textBox42,
            this.textBox39,
            this.textBox81,
            this.textBox92,
            this.textBox93,
            this.textBox94,
            this.textBox83,
            this.textBox77,
            this.textBox78,
            this.ProductName,
            this.textBox29,
            this.textBox28,
            this.textBox26,
            this.textBox36,
            this.textBox41,
            this.textBox38});
            this.table1.Name = "table1";
            tableGroup21.Name = "group1";
            tableGroup22.Name = "group6";
            tableGroup20.ChildGroups.Add(tableGroup21);
            tableGroup20.ChildGroups.Add(tableGroup22);
            tableGroup20.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup20.Name = "detailTableGroup";
            this.table1.RowGroups.Add(tableGroup20);
            this.table1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.7000007629394531D), Telerik.Reporting.Drawing.Unit.Inch(0.591666579246521D));
            this.table1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D);
            // 
            // BreakOutWorkOrder
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pageHeaderSection1,
            this.detail,
            this.pageFooterSection1});
            this.Name = "BreakOutWorkOrder";
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(0.25D), Telerik.Reporting.Drawing.Unit.Inch(0.25D), Telerik.Reporting.Drawing.Unit.Inch(0.25D), Telerik.Reporting.Drawing.Unit.Inch(0.25D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(8D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PageFooterSection pageFooterSection1;
        private Telerik.Reporting.TextBox textBox30;
        private Telerik.Reporting.PictureBox pictureBox1;
        private Telerik.Reporting.Table table4;
        private Telerik.Reporting.TextBox textBox40;
        private Telerik.Reporting.TextBox textBox82;
        private Telerik.Reporting.TextBox textBox43;
        private Telerik.Reporting.TextBox textBox90;
        private Telerik.Reporting.TextBox textBox91;
        private Telerik.Reporting.TextBox textBox96;
        private Telerik.Reporting.TextBox textBox97;
        private Telerik.Reporting.TextBox textBox101;
        private Telerik.Reporting.TextBox textBox102;
        private Telerik.Reporting.TextBox textBox45;
        private Telerik.Reporting.TextBox textBox89;
        private Telerik.Reporting.TextBox textBox35;
        private Telerik.Reporting.TextBox textBox95;
        private Telerik.Reporting.TextBox textBox100;
        private Telerik.Reporting.TextBox textBox34;
        private Telerik.Reporting.TextBox textBox31;
        private Telerik.Reporting.Panel panel1;
        private Telerik.Reporting.TextBox textBox37;
        private Telerik.Reporting.TextBox textBox47;
        private Telerik.Reporting.TextBox textBox48;
        private Telerik.Reporting.TextBox textBox84;
        private Telerik.Reporting.TextBox textBox85;
        private Telerik.Reporting.TextBox textBox86;
        private Telerik.Reporting.TextBox textBox87;
        private Telerik.Reporting.TextBox textBox88;
        private Telerik.Reporting.TextBox textBox50;
        private Telerik.Reporting.Table table2;
        private Telerik.Reporting.TextBox textBox52;
        private Telerik.Reporting.TextBox textBox54;
        private Telerik.Reporting.TextBox textBox56;
        private Telerik.Reporting.TextBox textBox58;
        private Telerik.Reporting.TextBox textBox60;
        private Telerik.Reporting.TextBox textBox62;
        private Telerik.Reporting.TextBox textBox64;
        private Telerik.Reporting.TextBox textBox51;
        private Telerik.Reporting.TextBox textBox53;
        private Telerik.Reporting.TextBox textBox55;
        private Telerik.Reporting.TextBox textBox63;
        private Telerik.Reporting.TextBox textBox57;
        private Telerik.Reporting.TextBox textBox59;
        private Telerik.Reporting.TextBox textBox61;
        private Telerik.Reporting.TextBox textBox65;
        private Telerik.Reporting.TextBox textBox66;
        private Telerik.Reporting.TextBox textBox67;
        private Telerik.Reporting.TextBox textBox68;
        private Telerik.Reporting.Table table3;
        private Telerik.Reporting.TextBox textBox71;
        private Telerik.Reporting.TextBox textBox73;
        private Telerik.Reporting.TextBox textBox76;
        private Telerik.Reporting.TextBox textBox75;
        private Telerik.Reporting.TextBox textBox70;
        private Telerik.Reporting.TextBox textBox72;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.TextBox textBox25;
        private Telerik.Reporting.TextBox textBox32;
        private Telerik.Reporting.TextBox textBox46;
        private Telerik.Reporting.Table table5;
        private Telerik.Reporting.TextBox textBox49;
        private Telerik.Reporting.TextBox textBox74;
        private Telerik.Reporting.TextBox textBox98;
        private Telerik.Reporting.TextBox textBox106;
        private Telerik.Reporting.TextBox textBox107;
        private Telerik.Reporting.TextBox textBox112;
        private Telerik.Reporting.TextBox textBox113;
        private Telerik.Reporting.TextBox textBox116;
        private Telerik.Reporting.TextBox textBox117;
        private Telerik.Reporting.TextBox textBox118;
        private Telerik.Reporting.TextBox textBox69;
        private Telerik.Reporting.TextBox txtDataSourceLabel;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox99;
        private Telerik.Reporting.TextBox textBox103;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox pageInfoTextBox;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.Table table1;
        private Telerik.Reporting.TextBox textBox27;
        private Telerik.Reporting.TextBox textBox33;
        private Telerik.Reporting.TextBox textBox39;
        private Telerik.Reporting.TextBox textBox42;
        private Telerik.Reporting.TextBox textBox44;
        private Telerik.Reporting.TextBox textBox77;
        private Telerik.Reporting.TextBox textBox78;
        private Telerik.Reporting.TextBox textBox83;
        private Telerik.Reporting.TextBox textBox79;
        private Telerik.Reporting.TextBox textBox80;
        private Telerik.Reporting.TextBox textBox81;
        private Telerik.Reporting.TextBox textBox92;
        private Telerik.Reporting.TextBox textBox93;
        private Telerik.Reporting.TextBox textBox94;
        private Telerik.Reporting.TextBox ProductName;
        private Telerik.Reporting.TextBox textBox29;
        private Telerik.Reporting.TextBox textBox28;
        private Telerik.Reporting.TextBox textBox26;
        private Telerik.Reporting.TextBox textBox36;
        private Telerik.Reporting.TextBox textBox41;
        private Telerik.Reporting.TextBox textBox38;
    }
}