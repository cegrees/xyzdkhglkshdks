namespace Landdb.Views.Secondary.Reports.Work_Order
{
    partial class StandardWorkOrder
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StandardWorkOrder));
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.Drawing.FormattingRule formattingRule1 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.Drawing.FormattingRule formattingRule2 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup17 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup18 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup19 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup20 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup21 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup22 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup23 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup24 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup25 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.InstanceReportSource instanceReportSource1 = new Telerik.Reporting.InstanceReportSource();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.textBox70 = new Telerik.Reporting.TextBox();
            this.textBox72 = new Telerik.Reporting.TextBox();
            this.textBox51 = new Telerik.Reporting.TextBox();
            this.textBox53 = new Telerik.Reporting.TextBox();
            this.textBox55 = new Telerik.Reporting.TextBox();
            this.textBox63 = new Telerik.Reporting.TextBox();
            this.textBox57 = new Telerik.Reporting.TextBox();
            this.textBox59 = new Telerik.Reporting.TextBox();
            this.textBox61 = new Telerik.Reporting.TextBox();
            this.textBox35 = new Telerik.Reporting.TextBox();
            this.textBox95 = new Telerik.Reporting.TextBox();
            this.textBox100 = new Telerik.Reporting.TextBox();
            this.portraitProductsSubReport1 = new Landdb.Views.Secondary.Reports.Application.PortraitProductsSubReport();
            this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            this.textBox93 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.txtDataSourceLabel = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
            this.textBox69 = new Telerik.Reporting.TextBox();
            this.pageInfoTextBox = new Telerik.Reporting.TextBox();
            this.textBox104 = new Telerik.Reporting.TextBox();
            this.reportHeaderSection1 = new Telerik.Reporting.ReportHeaderSection();
            this.textBox25 = new Telerik.Reporting.TextBox();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.table3 = new Telerik.Reporting.Table();
            this.textBox71 = new Telerik.Reporting.TextBox();
            this.textBox73 = new Telerik.Reporting.TextBox();
            this.textBox76 = new Telerik.Reporting.TextBox();
            this.textBox75 = new Telerik.Reporting.TextBox();
            this.textBox68 = new Telerik.Reporting.TextBox();
            this.textBox67 = new Telerik.Reporting.TextBox();
            this.textBox66 = new Telerik.Reporting.TextBox();
            this.textBox65 = new Telerik.Reporting.TextBox();
            this.table2 = new Telerik.Reporting.Table();
            this.textBox52 = new Telerik.Reporting.TextBox();
            this.textBox54 = new Telerik.Reporting.TextBox();
            this.textBox56 = new Telerik.Reporting.TextBox();
            this.textBox58 = new Telerik.Reporting.TextBox();
            this.textBox60 = new Telerik.Reporting.TextBox();
            this.textBox62 = new Telerik.Reporting.TextBox();
            this.textBox64 = new Telerik.Reporting.TextBox();
            this.textBox50 = new Telerik.Reporting.TextBox();
            this.textBox32 = new Telerik.Reporting.TextBox();
            this.panel1 = new Telerik.Reporting.Panel();
            this.textBox37 = new Telerik.Reporting.TextBox();
            this.textBox47 = new Telerik.Reporting.TextBox();
            this.textBox48 = new Telerik.Reporting.TextBox();
            this.textBox84 = new Telerik.Reporting.TextBox();
            this.textBox85 = new Telerik.Reporting.TextBox();
            this.textBox86 = new Telerik.Reporting.TextBox();
            this.textBox87 = new Telerik.Reporting.TextBox();
            this.textBox88 = new Telerik.Reporting.TextBox();
            this.textBox31 = new Telerik.Reporting.TextBox();
            this.textBox34 = new Telerik.Reporting.TextBox();
            this.table4 = new Telerik.Reporting.Table();
            this.textBox40 = new Telerik.Reporting.TextBox();
            this.textBox82 = new Telerik.Reporting.TextBox();
            this.textBox43 = new Telerik.Reporting.TextBox();
            this.textBox90 = new Telerik.Reporting.TextBox();
            this.textBox91 = new Telerik.Reporting.TextBox();
            this.textBox96 = new Telerik.Reporting.TextBox();
            this.textBox97 = new Telerik.Reporting.TextBox();
            this.textBox101 = new Telerik.Reporting.TextBox();
            this.textBox102 = new Telerik.Reporting.TextBox();
            this.textBox45 = new Telerik.Reporting.TextBox();
            this.textBox89 = new Telerik.Reporting.TextBox();
            this.pictureBox1 = new Telerik.Reporting.PictureBox();
            this.textBox30 = new Telerik.Reporting.TextBox();
            this.subReport1 = new Telerik.Reporting.SubReport();
            this.textBox26 = new Telerik.Reporting.TextBox();
            this.textBox27 = new Telerik.Reporting.TextBox();
            this.workOrderDataSource = new Telerik.Reporting.ObjectDataSource();
            ((System.ComponentModel.ISupportInitialize)(this.portraitProductsSubReport1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox70
            // 
            this.textBox70.Name = "textBox70";
            this.textBox70.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.30883526802063D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666567325592D));
            this.textBox70.Style.Font.Bold = true;
            this.textBox70.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox70.Style.Font.Underline = true;
            this.textBox70.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            resources.ApplyResources(this.textBox70, "textBox70");
            // 
            // textBox72
            // 
            this.textBox72.Name = "textBox72";
            this.textBox72.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8658847808837891D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666716337204D));
            this.textBox72.Style.Font.Bold = true;
            this.textBox72.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox72.Style.Font.Underline = true;
            this.textBox72.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            resources.ApplyResources(this.textBox72, "textBox72");
            // 
            // textBox51
            // 
            this.textBox51.Name = "textBox51";
            this.textBox51.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1354167461395264D), Telerik.Reporting.Drawing.Unit.Inch(0.21000002324581146D));
            this.textBox51.Style.Font.Bold = true;
            this.textBox51.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            resources.ApplyResources(this.textBox51, "textBox51");
            // 
            // textBox53
            // 
            this.textBox53.Name = "textBox53";
            this.textBox53.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1354169845581055D), Telerik.Reporting.Drawing.Unit.Inch(0.21000005304813385D));
            this.textBox53.Style.Font.Bold = true;
            this.textBox53.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            resources.ApplyResources(this.textBox53, "textBox53");
            // 
            // textBox55
            // 
            this.textBox55.Name = "textBox55";
            this.textBox55.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1770834922790527D), Telerik.Reporting.Drawing.Unit.Inch(0.21000005304813385D));
            this.textBox55.Style.Font.Bold = true;
            this.textBox55.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            resources.ApplyResources(this.textBox55, "textBox55");
            // 
            // textBox63
            // 
            this.textBox63.Name = "textBox63";
            this.textBox63.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3020832538604736D), Telerik.Reporting.Drawing.Unit.Inch(0.21000003814697266D));
            this.textBox63.Style.Font.Bold = true;
            this.textBox63.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox63.StyleName = "";
            resources.ApplyResources(this.textBox63, "textBox63");
            // 
            // textBox57
            // 
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2916673421859741D), Telerik.Reporting.Drawing.Unit.Inch(0.21000002324581146D));
            this.textBox57.Style.Font.Bold = true;
            this.textBox57.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox57.StyleName = "";
            resources.ApplyResources(this.textBox57, "textBox57");
            // 
            // textBox59
            // 
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.87500065565109253D), Telerik.Reporting.Drawing.Unit.Inch(0.21000002324581146D));
            this.textBox59.Style.Font.Bold = true;
            this.textBox59.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox59.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox59.StyleName = "";
            resources.ApplyResources(this.textBox59, "textBox59");
            // 
            // textBox61
            // 
            this.textBox61.Name = "textBox61";
            this.textBox61.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.96874970197677612D), Telerik.Reporting.Drawing.Unit.Inch(0.21000002324581146D));
            this.textBox61.Style.Font.Bold = true;
            this.textBox61.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox61.StyleName = "";
            resources.ApplyResources(this.textBox61, "textBox61");
            // 
            // textBox35
            // 
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.0645821094512939D), Telerik.Reporting.Drawing.Unit.Inch(0.20000006258487701D));
            this.textBox35.Style.BackgroundColor = System.Drawing.Color.Silver;
            this.textBox35.Style.Font.Bold = true;
            this.textBox35.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox35.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            resources.ApplyResources(this.textBox35, "textBox35");
            // 
            // textBox95
            // 
            this.textBox95.Name = "textBox95";
            this.textBox95.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0416667461395264D), Telerik.Reporting.Drawing.Unit.Inch(0.20000006258487701D));
            this.textBox95.Style.BackgroundColor = System.Drawing.Color.Silver;
            this.textBox95.Style.Font.Bold = true;
            this.textBox95.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox95.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox95.StyleName = "";
            // 
            // textBox100
            // 
            this.textBox100.Name = "textBox100";
            this.textBox100.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.8083338737487793D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.textBox100.Style.BackgroundColor = System.Drawing.Color.Silver;
            this.textBox100.Style.Font.Bold = true;
            this.textBox100.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox100.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox100.StyleName = "";
            // 
            // portraitProductsSubReport1
            // 
            this.portraitProductsSubReport1.Name = "PortraitProductsSubReport";
            // 
            // pageHeaderSection1
            // 
            this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(1D);
            this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox93,
            this.textBox4,
            this.textBox1,
            this.txtDataSourceLabel,
            this.textBox2,
            this.textBox3});
            this.pageHeaderSection1.Name = "pageHeaderSection1";
            // 
            // textBox93
            // 
            this.textBox93.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.5D), Telerik.Reporting.Drawing.Unit.Inch(0.20003943145275116D));
            this.textBox93.Name = "textBox93";
            this.textBox93.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5050491094589233D), Telerik.Reporting.Drawing.Unit.Inch(0.29996061325073242D));
            this.textBox93.Style.Color = System.Drawing.Color.DimGray;
            this.textBox93.Style.Font.Name = "Segoe UI Light";
            this.textBox93.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(18D);
            this.textBox93.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox93.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox93.TextWrap = false;
            resources.ApplyResources(this.textBox93, "textBox93");
            // 
            // textBox4
            // 
            this.textBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.0102958679199219D), Telerik.Reporting.Drawing.Unit.Inch(0.50212281942367554D));
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9998817443847656D), Telerik.Reporting.Drawing.Unit.Inch(0.19996063411235809D));
            this.textBox4.Style.Color = System.Drawing.Color.DimGray;
            this.textBox4.Style.Font.Name = "Segoe UI";
            this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox4.TextWrap = false;
            resources.ApplyResources(this.textBox4, "textBox4");
            // 
            // textBox1
            // 
            this.textBox1.CanGrow = false;
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.1050887107849121D), Telerik.Reporting.Drawing.Unit.Inch(0.70216220617294312D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.8999602794647217D), Telerik.Reporting.Drawing.Unit.Inch(0.18942584097385407D));
            this.textBox1.Style.Color = System.Drawing.Color.DimGray;
            this.textBox1.Style.Font.Name = "Segoe UI Light";
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            resources.ApplyResources(this.textBox1, "textBox1");
            // 
            // txtDataSourceLabel
            // 
            this.txtDataSourceLabel.CanGrow = false;
            this.txtDataSourceLabel.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.txtDataSourceLabel.Name = "txtDataSourceLabel";
            this.txtDataSourceLabel.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.4998817443847656D), Telerik.Reporting.Drawing.Unit.Inch(0.50000005960464478D));
            this.txtDataSourceLabel.Style.Font.Name = "Segoe UI Light";
            this.txtDataSourceLabel.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(20D);
            this.txtDataSourceLabel.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            resources.ApplyResources(this.txtDataSourceLabel, "txtDataSourceLabel");
            // 
            // textBox2
            // 
            this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D), Telerik.Reporting.Drawing.Unit.Inch(0.70015740394592285D));
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.0812106132507324D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox2.Style.Font.Bold = true;
            this.textBox2.Style.Font.Name = "Segoe UI";
            this.textBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            resources.ApplyResources(this.textBox2, "textBox2");
            // 
            // textBox3
            // 
            this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D), Telerik.Reporting.Drawing.Unit.Inch(0.50007873773574829D));
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.0812108516693115D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox3.Style.Font.Name = "Segoe UI";
            this.textBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            resources.ApplyResources(this.textBox3, "textBox3");
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(0.0520833320915699D);
            this.detail.Name = "detail";
            // 
            // pageFooterSection1
            // 
            this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.41037240624427795D);
            this.pageFooterSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox69,
            this.pageInfoTextBox,
            this.textBox104});
            this.pageFooterSection1.Name = "pageFooterSection1";
            // 
            // textBox69
            // 
            this.textBox69.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.3000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.11041196435689926D));
            this.textBox69.Name = "textBox69";
            this.textBox69.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.1978371143341064D), Telerik.Reporting.Drawing.Unit.Inch(0.29996046423912048D));
            this.textBox69.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox69.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox69.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            resources.ApplyResources(this.textBox69, "textBox69");
            // 
            // pageInfoTextBox
            // 
            this.pageInfoTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.0915055274963379D), Telerik.Reporting.Drawing.Unit.Inch(0.099920906126499176D));
            this.pageInfoTextBox.Name = "pageInfoTextBox";
            this.pageInfoTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9186718463897705D), Telerik.Reporting.Drawing.Unit.Inch(0.30003905296325684D));
            this.pageInfoTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.pageInfoTextBox.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.pageInfoTextBox.StyleName = "PageInfo";
            resources.ApplyResources(this.pageInfoTextBox, "pageInfoTextBox");
            // 
            // textBox104
            // 
            this.textBox104.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.11041196435689926D));
            this.textBox104.Name = "textBox104";
            this.textBox104.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8790887594223023D), Telerik.Reporting.Drawing.Unit.Inch(0.29996046423912048D));
            this.textBox104.Style.Color = System.Drawing.Color.DimGray;
            this.textBox104.Style.Font.Bold = true;
            this.textBox104.Style.Font.Italic = false;
            this.textBox104.Style.Font.Name = "Segoe UI Light";
            this.textBox104.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(16D);
            this.textBox104.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            resources.ApplyResources(this.textBox104, "textBox104");
            // 
            // reportHeaderSection1
            // 
            this.reportHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(8.3060531616210938D);
            this.reportHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox25,
            this.textBox24,
            this.textBox23,
            this.textBox22,
            this.textBox21,
            this.textBox20,
            this.textBox19,
            this.textBox18,
            this.textBox17,
            this.textBox16,
            this.textBox15,
            this.textBox14,
            this.textBox13,
            this.textBox12,
            this.textBox11,
            this.table3,
            this.textBox68,
            this.textBox67,
            this.textBox66,
            this.textBox65,
            this.table2,
            this.textBox50,
            this.textBox32,
            this.panel1,
            this.textBox31,
            this.textBox34,
            this.table4,
            this.pictureBox1,
            this.textBox30,
            this.subReport1,
            this.textBox26,
            this.textBox27});
            this.reportHeaderSection1.Name = "reportHeaderSection1";
            // 
            // textBox25
            // 
            this.textBox25.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.7791657447814941D), Telerik.Reporting.Drawing.Unit.Inch(3.6000001430511475D));
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1999213695526123D), Telerik.Reporting.Drawing.Unit.Inch(0.19089095294475555D));
            this.textBox25.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox25.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            resources.ApplyResources(this.textBox25, "textBox25");
            // 
            // textBox24
            // 
            this.textBox24.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.7041301727294922D), Telerik.Reporting.Drawing.Unit.Inch(3.6000001430511475D));
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0999215841293335D), Telerik.Reporting.Drawing.Unit.Inch(0.19089095294475555D));
            this.textBox24.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox24.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            resources.ApplyResources(this.textBox24, "textBox24");
            // 
            // textBox23
            // 
            this.textBox23.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.9020469188690186D), Telerik.Reporting.Drawing.Unit.Inch(3.6000001430511475D));
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.80208337306976318D), Telerik.Reporting.Drawing.Unit.Inch(0.19089095294475555D));
            this.textBox23.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox23.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            resources.ApplyResources(this.textBox23, "textBox23");
            // 
            // textBox22
            // 
            this.textBox22.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.99579685926437378D), Telerik.Reporting.Drawing.Unit.Inch(3.6104171276092529D));
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.70216244459152222D), Telerik.Reporting.Drawing.Unit.Inch(0.19089095294475555D));
            this.textBox22.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox22.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            resources.ApplyResources(this.textBox22, "textBox22");
            // 
            // textBox21
            // 
            this.textBox21.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.9770827293396D), Telerik.Reporting.Drawing.Unit.Inch(3.6000001430511475D));
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.79775935411453247D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox21.Style.Font.Bold = true;
            this.textBox21.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox21.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            resources.ApplyResources(this.textBox21, "textBox21");
            // 
            // textBox20
            // 
            this.textBox20.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.7041301727294922D), Telerik.Reporting.Drawing.Unit.Inch(3.6000001430511475D));
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.99999994039535522D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox20.Style.Font.Bold = true;
            this.textBox20.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox20.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            resources.ApplyResources(this.textBox20, "textBox20");
            // 
            // textBox19
            // 
            this.textBox19.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.7978801727294922D), Telerik.Reporting.Drawing.Unit.Inch(3.6000001430511475D));
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0977592468261719D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox19.Style.Font.Bold = true;
            this.textBox19.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox19.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            resources.ApplyResources(this.textBox19, "textBox19");
            // 
            // textBox18
            // 
            this.textBox18.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.099963508546352386D), Telerik.Reporting.Drawing.Unit.Inch(3.6000001430511475D));
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.89575445652008057D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox18.Style.Font.Bold = true;
            this.textBox18.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox18.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            resources.ApplyResources(this.textBox18, "textBox18");
            // 
            // textBox17
            // 
            this.textBox17.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.099963508546352386D), Telerik.Reporting.Drawing.Unit.Inch(2.8937499523162842D));
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.178886890411377D), Telerik.Reporting.Drawing.Unit.Inch(0.30220144987106323D));
            this.textBox17.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox17.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox17.Style.Font.Bold = true;
            this.textBox17.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox17.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            resources.ApplyResources(this.textBox17, "textBox17");
            // 
            // textBox16
            // 
            this.textBox16.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.099963508546352386D), Telerik.Reporting.Drawing.Unit.Inch(3.1958332061767578D));
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.178886890411377D), Telerik.Reporting.Drawing.Unit.Inch(0.30220144987106323D));
            this.textBox16.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox16.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox16.Style.Font.Bold = true;
            this.textBox16.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox16.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            resources.ApplyResources(this.textBox16, "textBox16");
            // 
            // textBox15
            // 
            this.textBox15.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.099963508546352386D), Telerik.Reporting.Drawing.Unit.Inch(2.6854164600372314D));
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.178886890411377D), Telerik.Reporting.Drawing.Unit.Inch(0.2022802084684372D));
            this.textBox15.Style.BackgroundColor = System.Drawing.Color.Silver;
            this.textBox15.Style.Color = System.Drawing.Color.Black;
            this.textBox15.Style.Font.Bold = true;
            this.textBox15.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox15.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox15.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            resources.ApplyResources(this.textBox15, "textBox15");
            // 
            // textBox14
            // 
            this.textBox14.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.099963508546352386D), Telerik.Reporting.Drawing.Unit.Inch(1.8000001907348633D));
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.179161548614502D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox14.Style.BackgroundColor = System.Drawing.Color.Silver;
            this.textBox14.Style.Color = System.Drawing.Color.Black;
            this.textBox14.Style.Font.Bold = true;
            this.textBox14.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox14.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox14.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            resources.ApplyResources(this.textBox14, "textBox14");
            // 
            // textBox13
            // 
            this.textBox13.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.095833458006381989D), Telerik.Reporting.Drawing.Unit.Inch(0.50019693374633789D));
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.1727099418640137D), Telerik.Reporting.Drawing.Unit.Inch(0.29992121458053589D));
            this.textBox13.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox13.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox13.Style.Font.Bold = true;
            this.textBox13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox13.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            resources.ApplyResources(this.textBox13, "textBox13");
            // 
            // textBox12
            // 
            this.textBox12.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.095833458006381989D), Telerik.Reporting.Drawing.Unit.Inch(0.19811360538005829D));
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.1727099418640137D), Telerik.Reporting.Drawing.Unit.Inch(0.29992121458053589D));
            this.textBox12.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox12.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox12.Style.Font.Bold = true;
            this.textBox12.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox12.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            resources.ApplyResources(this.textBox12, "textBox12");
            // 
            // textBox11
            // 
            this.textBox11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.095833458006381989D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.1727099418640137D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox11.Style.BackgroundColor = System.Drawing.Color.Silver;
            this.textBox11.Style.Color = System.Drawing.SystemColors.WindowText;
            this.textBox11.Style.Font.Bold = true;
            this.textBox11.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox11.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox11.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            resources.ApplyResources(this.textBox11, "textBox11");
            // 
            // table3
            // 
            this.table3.Bindings.Add(new Telerik.Reporting.Binding("DataSource", "=Fields.Applicators"));
            this.table3.Bindings.Add(new Telerik.Reporting.Binding("Visible", "=Fields.Applicators.Count > 0"));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.3088364601135254D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.8658840656280518D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.18833327293395996D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.17708323895931244D)));
            this.table3.Body.SetCellContent(0, 0, this.textBox71);
            this.table3.Body.SetCellContent(0, 1, this.textBox73);
            this.table3.Body.SetCellContent(1, 0, this.textBox76);
            this.table3.Body.SetCellContent(1, 1, this.textBox75);
            tableGroup1.Name = "tableGroup6";
            tableGroup1.ReportItem = this.textBox70;
            tableGroup2.Name = "tableGroup7";
            tableGroup2.ReportItem = this.textBox72;
            this.table3.ColumnGroups.Add(tableGroup1);
            this.table3.ColumnGroups.Add(tableGroup2);
            this.table3.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox71,
            this.textBox73,
            this.textBox76,
            this.textBox75,
            this.textBox70,
            this.textBox72});
            this.table3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.10413011163473129D), Telerik.Reporting.Drawing.Unit.Inch(2.0000791549682617D));
            this.table3.Name = "table3";
            tableGroup4.Name = "group11";
            tableGroup5.Name = "group12";
            tableGroup3.ChildGroups.Add(tableGroup4);
            tableGroup3.ChildGroups.Add(tableGroup5);
            tableGroup3.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup3.Name = "detailTableGroup2";
            this.table3.RowGroups.Add(tableGroup3);
            this.table3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.174720287322998D), Telerik.Reporting.Drawing.Unit.Inch(0.53208315372467041D));
            // 
            // textBox71
            // 
            this.textBox71.Name = "textBox71";
            this.textBox71.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.30883526802063D), Telerik.Reporting.Drawing.Unit.Inch(0.18833325803279877D));
            this.textBox71.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            resources.ApplyResources(this.textBox71, "textBox71");
            // 
            // textBox73
            // 
            this.textBox73.Name = "textBox73";
            this.textBox73.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8658847808837891D), Telerik.Reporting.Drawing.Unit.Inch(0.18833327293395996D));
            this.textBox73.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            resources.ApplyResources(this.textBox73, "textBox73");
            // 
            // textBox76
            // 
            this.textBox76.Name = "textBox76";
            this.textBox76.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.30883526802063D), Telerik.Reporting.Drawing.Unit.Inch(0.17708323895931244D));
            this.textBox76.Style.Font.Italic = true;
            this.textBox76.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox76.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox76.StyleName = "";
            resources.ApplyResources(this.textBox76, "textBox76");
            // 
            // textBox75
            // 
            this.textBox75.Format = "{0:d}";
            this.textBox75.Name = "textBox75";
            this.textBox75.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8658847808837891D), Telerik.Reporting.Drawing.Unit.Inch(0.17708325386047363D));
            this.textBox75.Style.Font.Italic = true;
            this.textBox75.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox75.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            resources.ApplyResources(this.textBox75, "textBox75");
            // 
            // textBox68
            // 
            this.textBox68.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.2727141380310059D), Telerik.Reporting.Drawing.Unit.Inch(6.4916672706604D));
            this.textBox68.Name = "textBox68";
            this.textBox68.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.7020852565765381D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBox68.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox68.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox68.Style.Font.Bold = true;
            this.textBox68.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            resources.ApplyResources(this.textBox68, "textBox68");
            // 
            // textBox67
            // 
            this.textBox67.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.089381851255893707D), Telerik.Reporting.Drawing.Unit.Inch(6.4916672706604D));
            this.textBox67.Name = "textBox67";
            this.textBox67.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.899921178817749D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBox67.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox67.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox67.Style.Font.Bold = true;
            this.textBox67.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            resources.ApplyResources(this.textBox67, "textBox67");
            // 
            // textBox66
            // 
            this.textBox66.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.089381851255893707D), Telerik.Reporting.Drawing.Unit.Inch(5.9916672706604D));
            this.textBox66.Name = "textBox66";
            this.textBox66.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.9143819808959961D), Telerik.Reporting.Drawing.Unit.Inch(0.20000013709068298D));
            resources.ApplyResources(this.textBox66, "textBox66");
            // 
            // textBox65
            // 
            this.textBox65.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.089381851255893707D), Telerik.Reporting.Drawing.Unit.Inch(5.7937502861022949D));
            this.textBox65.Name = "textBox65";
            this.textBox65.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.91057825088501D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox65.Style.BackgroundColor = System.Drawing.Color.Silver;
            this.textBox65.Style.Color = System.Drawing.Color.Black;
            this.textBox65.Style.Font.Bold = true;
            this.textBox65.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox65.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox65.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            resources.ApplyResources(this.textBox65, "textBox65");
            // 
            // table2
            // 
            this.table2.Bindings.Add(new Telerik.Reporting.Binding("DataSource", "=Fields.Fields"));
            this.table2.Bindings.Add(new Telerik.Reporting.Binding("Visible", "=Fields.Fields.Count > 0"));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.1354168653488159D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.1354168653488159D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.1770830154418945D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.3020830154418945D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.2916669845581055D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.87500029802322388D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.96874970197677612D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.23000000417232513D)));
            this.table2.Body.SetCellContent(0, 0, this.textBox52);
            this.table2.Body.SetCellContent(0, 1, this.textBox54);
            this.table2.Body.SetCellContent(0, 2, this.textBox56);
            this.table2.Body.SetCellContent(0, 4, this.textBox58);
            this.table2.Body.SetCellContent(0, 5, this.textBox60);
            this.table2.Body.SetCellContent(0, 6, this.textBox62);
            this.table2.Body.SetCellContent(0, 3, this.textBox64);
            tableGroup6.Name = "tableGroup3";
            tableGroup6.ReportItem = this.textBox51;
            tableGroup7.Name = "tableGroup4";
            tableGroup7.ReportItem = this.textBox53;
            tableGroup8.Name = "tableGroup5";
            tableGroup8.ReportItem = this.textBox55;
            tableGroup9.Name = "group10";
            tableGroup9.ReportItem = this.textBox63;
            tableGroup10.Name = "group7";
            tableGroup10.ReportItem = this.textBox57;
            tableGroup11.Name = "group8";
            tableGroup11.ReportItem = this.textBox59;
            tableGroup12.Name = "group9";
            tableGroup12.ReportItem = this.textBox61;
            this.table2.ColumnGroups.Add(tableGroup6);
            this.table2.ColumnGroups.Add(tableGroup7);
            this.table2.ColumnGroups.Add(tableGroup8);
            this.table2.ColumnGroups.Add(tableGroup9);
            this.table2.ColumnGroups.Add(tableGroup10);
            this.table2.ColumnGroups.Add(tableGroup11);
            this.table2.ColumnGroups.Add(tableGroup12);
            this.table2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox52,
            this.textBox54,
            this.textBox56,
            this.textBox64,
            this.textBox58,
            this.textBox60,
            this.textBox62,
            this.textBox51,
            this.textBox53,
            this.textBox55,
            this.textBox63,
            this.textBox57,
            this.textBox59,
            this.textBox61});
            this.table2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.089381851255893707D), Telerik.Reporting.Drawing.Unit.Inch(5.2937502861022949D));
            this.table2.Name = "table2";
            tableGroup13.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup13.Name = "detailTableGroup1";
            this.table2.RowGroups.Add(tableGroup13);
            this.table2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.8854165077209473D), Telerik.Reporting.Drawing.Unit.Inch(0.4400000274181366D));
            // 
            // textBox52
            // 
            formattingRule1.Filters.Add(new Telerik.Reporting.Filter("=RowNumber() % 2", Telerik.Reporting.FilterOperator.Equal, "1"));
            formattingRule1.Style.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.textBox52.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule1});
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1354173421859741D), Telerik.Reporting.Drawing.Unit.Inch(0.23000000417232513D));
            resources.ApplyResources(this.textBox52, "textBox52");
            // 
            // textBox54
            // 
            formattingRule2.Filters.Add(new Telerik.Reporting.Filter("=RowNumber() % 2", Telerik.Reporting.FilterOperator.Equal, "1"));
            formattingRule2.Style.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.textBox54.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule2});
            this.textBox54.Name = "textBox54";
            this.textBox54.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1354173421859741D), Telerik.Reporting.Drawing.Unit.Inch(0.23000000417232513D));
            resources.ApplyResources(this.textBox54, "textBox54");
            // 
            // textBox56
            // 
            this.textBox56.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule2});
            this.textBox56.Name = "textBox56";
            this.textBox56.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1770831346511841D), Telerik.Reporting.Drawing.Unit.Inch(0.23000000417232513D));
            resources.ApplyResources(this.textBox56, "textBox56");
            // 
            // textBox58
            // 
            this.textBox58.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule2});
            this.textBox58.Name = "textBox58";
            this.textBox58.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2916673421859741D), Telerik.Reporting.Drawing.Unit.Inch(0.23000000417232513D));
            this.textBox58.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox58.StyleName = "";
            resources.ApplyResources(this.textBox58, "textBox58");
            // 
            // textBox60
            // 
            this.textBox60.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule2});
            this.textBox60.Format = "{0:N2}";
            this.textBox60.Name = "textBox60";
            this.textBox60.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.87500065565109253D), Telerik.Reporting.Drawing.Unit.Inch(0.23000000417232513D));
            this.textBox60.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox60.StyleName = "";
            resources.ApplyResources(this.textBox60, "textBox60");
            // 
            // textBox62
            // 
            this.textBox62.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule2});
            this.textBox62.Name = "textBox62";
            this.textBox62.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.96874970197677612D), Telerik.Reporting.Drawing.Unit.Inch(0.23000000417232513D));
            this.textBox62.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox62.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox62.Style.Font.Bold = true;
            this.textBox62.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox62.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox62.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox62.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox62.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox62.StyleName = "";
            // 
            // textBox64
            // 
            this.textBox64.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule2});
            this.textBox64.Name = "textBox64";
            this.textBox64.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3020832538604736D), Telerik.Reporting.Drawing.Unit.Inch(0.23000000417232513D));
            this.textBox64.StyleName = "";
            resources.ApplyResources(this.textBox64, "textBox64");
            // 
            // textBox50
            // 
            this.textBox50.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.089381851255893707D), Telerik.Reporting.Drawing.Unit.Inch(5.0958333015441895D));
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.9105391502380371D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox50.Style.BackgroundColor = System.Drawing.Color.Silver;
            this.textBox50.Style.Color = System.Drawing.Color.Black;
            this.textBox50.Style.Font.Bold = true;
            this.textBox50.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox50.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox50.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            resources.ApplyResources(this.textBox50, "textBox50");
            // 
            // textBox32
            // 
            this.textBox32.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(4.2000002861022949D));
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.9999604225158691D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox32.Style.BackgroundColor = System.Drawing.Color.Silver;
            this.textBox32.Style.Color = System.Drawing.Color.Black;
            this.textBox32.Style.Font.Bold = true;
            this.textBox32.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox32.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox32.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            resources.ApplyResources(this.textBox32, "textBox32");
            // 
            // panel1
            // 
            this.panel1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox37,
            this.textBox47,
            this.textBox48,
            this.textBox84,
            this.textBox85,
            this.textBox86,
            this.textBox87,
            this.textBox88});
            this.panel1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.10413011163473129D), Telerik.Reporting.Drawing.Unit.Inch(3.8013865947723389D));
            this.panel1.Name = "panel1";
            this.panel1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.8749566078186035D), Telerik.Reporting.Drawing.Unit.Inch(0.3030761182308197D));
            // 
            // textBox37
            // 
            this.textBox37.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D), Telerik.Reporting.Drawing.Unit.Inch(0.10046068578958511D));
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.79996055364608765D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox37.Style.Font.Bold = true;
            this.textBox37.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox37.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            resources.ApplyResources(this.textBox37, "textBox37");
            // 
            // textBox47
            // 
            this.textBox47.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.8000788688659668D), Telerik.Reporting.Drawing.Unit.Inch(0.10046068578958511D));
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.88525092601776123D), Telerik.Reporting.Drawing.Unit.Inch(0.19089095294475555D));
            this.textBox47.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox47.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            resources.ApplyResources(this.textBox47, "textBox47");
            // 
            // textBox48
            // 
            this.textBox48.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.781048059463501D), Telerik.Reporting.Drawing.Unit.Inch(0.10050010681152344D));
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.81887310743331909D), Telerik.Reporting.Drawing.Unit.Inch(0.19089095294475555D));
            this.textBox48.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox48.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            resources.ApplyResources(this.textBox48, "textBox48");
            // 
            // textBox84
            // 
            this.textBox84.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.6937500238418579D), Telerik.Reporting.Drawing.Unit.Inch(0.10050010681152344D));
            this.textBox84.Name = "textBox84";
            this.textBox84.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0872193574905396D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox84.Style.Font.Bold = true;
            this.textBox84.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox84.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            resources.ApplyResources(this.textBox84, "textBox84");
            // 
            // textBox85
            // 
            this.textBox85.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.6146702766418457D), Telerik.Reporting.Drawing.Unit.Inch(0.1096089705824852D));
            this.textBox85.Name = "textBox85";
            this.textBox85.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0852508544921875D), Telerik.Reporting.Drawing.Unit.Inch(0.19089095294475555D));
            this.textBox85.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox85.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            resources.ApplyResources(this.textBox85, "textBox85");
            // 
            // textBox86
            // 
            this.textBox86.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.6000001430511475D), Telerik.Reporting.Drawing.Unit.Inch(0.091351829469203949D));
            this.textBox86.Name = "textBox86";
            this.textBox86.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0145909786224365D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox86.Style.Font.Bold = true;
            this.textBox86.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox86.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            resources.ApplyResources(this.textBox86, "textBox86");
            // 
            // textBox87
            // 
            this.textBox87.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.1581244468688965D), Telerik.Reporting.Drawing.Unit.Inch(0.091351829469203949D));
            this.textBox87.Name = "textBox87";
            this.textBox87.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0145909786224365D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox87.Style.Font.Bold = true;
            this.textBox87.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox87.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            resources.ApplyResources(this.textBox87, "textBox87");
            // 
            // textBox88
            // 
            this.textBox88.Format = "{0:N2}";
            this.textBox88.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(7.1727943420410156D), Telerik.Reporting.Drawing.Unit.Inch(0.11218515783548355D));
            this.textBox88.Name = "textBox88";
            this.textBox88.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.70216244459152222D), Telerik.Reporting.Drawing.Unit.Inch(0.19089095294475555D));
            this.textBox88.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox88.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            resources.ApplyResources(this.textBox88, "textBox88");
            // 
            // textBox31
            // 
            this.textBox31.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.095833458006381989D), Telerik.Reporting.Drawing.Unit.Inch(1.1001969575881958D));
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.179161548614502D), Telerik.Reporting.Drawing.Unit.Inch(0.29992121458053589D));
            this.textBox31.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox31.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox31.Style.Font.Bold = true;
            this.textBox31.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox31.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            resources.ApplyResources(this.textBox31, "textBox31");
            // 
            // textBox34
            // 
            this.textBox34.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.095833458006381989D), Telerik.Reporting.Drawing.Unit.Inch(1.4001966714859009D));
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.1685433387756348D), Telerik.Reporting.Drawing.Unit.Inch(0.29992121458053589D));
            this.textBox34.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox34.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox34.Style.Font.Bold = true;
            this.textBox34.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox34.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            resources.ApplyResources(this.textBox34, "textBox34");
            // 
            // table4
            // 
            this.table4.Bindings.Add(new Telerik.Reporting.Binding("DataSource", "=Fields.WorkerProtectionItems"));
            this.table4.Bindings.Add(new Telerik.Reporting.Binding("Visible", "=Fields.WorkerProtectionItems.Count > 0"));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.05208420753479D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.0124983787536621D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.0416667461395264D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(3.8083338737487793D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.20000007748603821D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.19999983906745911D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.20208348333835602D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.20833338797092438D)));
            this.table4.Body.SetCellContent(2, 0, this.textBox40);
            this.table4.Body.SetCellContent(3, 0, this.textBox82);
            this.table4.Body.SetCellContent(0, 0, this.textBox43, 1, 2);
            this.table4.Body.SetCellContent(1, 0, this.textBox90);
            this.table4.Body.SetCellContent(1, 1, this.textBox91);
            this.table4.Body.SetCellContent(0, 2, this.textBox96);
            this.table4.Body.SetCellContent(1, 2, this.textBox97);
            this.table4.Body.SetCellContent(0, 3, this.textBox101);
            this.table4.Body.SetCellContent(1, 3, this.textBox102);
            this.table4.Body.SetCellContent(2, 1, this.textBox45, 1, 3);
            this.table4.Body.SetCellContent(3, 1, this.textBox89, 1, 3);
            tableGroup15.Name = "group2";
            tableGroup16.Name = "tableGroup8";
            tableGroup14.ChildGroups.Add(tableGroup15);
            tableGroup14.ChildGroups.Add(tableGroup16);
            tableGroup14.Name = "tableGroup1";
            tableGroup14.ReportItem = this.textBox35;
            tableGroup18.Name = "group20";
            tableGroup17.ChildGroups.Add(tableGroup18);
            tableGroup17.Name = "group19";
            tableGroup17.ReportItem = this.textBox95;
            tableGroup20.Name = "group22";
            tableGroup19.ChildGroups.Add(tableGroup20);
            tableGroup19.Name = "group21";
            tableGroup19.ReportItem = this.textBox100;
            this.table4.ColumnGroups.Add(tableGroup14);
            this.table4.ColumnGroups.Add(tableGroup17);
            this.table4.ColumnGroups.Add(tableGroup19);
            this.table4.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox43,
            this.textBox96,
            this.textBox101,
            this.textBox90,
            this.textBox91,
            this.textBox97,
            this.textBox102,
            this.textBox40,
            this.textBox45,
            this.textBox82,
            this.textBox89,
            this.textBox35,
            this.textBox95,
            this.textBox100});
            this.table4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.089180313050746918D), Telerik.Reporting.Drawing.Unit.Inch(6.8958330154418945D));
            this.table4.Name = "table4";
            tableGroup22.Name = "group15";
            tableGroup23.Name = "group18";
            tableGroup24.Name = "group16";
            tableGroup25.Name = "group17";
            tableGroup21.ChildGroups.Add(tableGroup22);
            tableGroup21.ChildGroups.Add(tableGroup23);
            tableGroup21.ChildGroups.Add(tableGroup24);
            tableGroup21.ChildGroups.Add(tableGroup25);
            tableGroup21.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup21.Name = "detailTableGroup3";
            this.table4.RowGroups.Add(tableGroup21);
            this.table4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.9145832061767578D), Telerik.Reporting.Drawing.Unit.Inch(1.0104168653488159D));
            // 
            // textBox40
            // 
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0520833730697632D), Telerik.Reporting.Drawing.Unit.Inch(0.20208361744880676D));
            this.textBox40.Style.Color = System.Drawing.Color.Gray;
            this.textBox40.Style.Font.Bold = true;
            this.textBox40.Style.Font.Name = "Segoe UI";
            this.textBox40.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox40.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(15D);
            this.textBox40.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox40.StyleName = "";
            resources.ApplyResources(this.textBox40, "textBox40");
            // 
            // textBox82
            // 
            this.textBox82.Name = "textBox82";
            this.textBox82.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0520833730697632D), Telerik.Reporting.Drawing.Unit.Inch(0.2083333283662796D));
            this.textBox82.Style.Color = System.Drawing.Color.Gray;
            this.textBox82.Style.Font.Bold = true;
            this.textBox82.Style.Font.Name = "Segoe UI";
            this.textBox82.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox82.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(15D);
            this.textBox82.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox82.StyleName = "";
            resources.ApplyResources(this.textBox82, "textBox82");
            // 
            // textBox43
            // 
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.0645821094512939D), Telerik.Reporting.Drawing.Unit.Inch(0.20000007748603821D));
            this.textBox43.Style.Font.Underline = true;
            this.textBox43.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            resources.ApplyResources(this.textBox43, "textBox43");
            // 
            // textBox90
            // 
            this.textBox90.Name = "textBox90";
            this.textBox90.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0520837306976318D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox90.Style.Color = System.Drawing.Color.Gray;
            this.textBox90.Style.Font.Bold = true;
            this.textBox90.Style.Font.Name = "Segoe UI";
            this.textBox90.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox90.Style.Font.Underline = false;
            this.textBox90.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(15D);
            this.textBox90.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox90.StyleName = "";
            resources.ApplyResources(this.textBox90, "textBox90");
            // 
            // textBox91
            // 
            this.textBox91.Name = "textBox91";
            this.textBox91.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0124979019165039D), Telerik.Reporting.Drawing.Unit.Inch(0.20000001788139343D));
            this.textBox91.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox91.Style.Font.Underline = false;
            this.textBox91.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox91.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox91.StyleName = "";
            resources.ApplyResources(this.textBox91, "textBox91");
            // 
            // textBox96
            // 
            this.textBox96.Name = "textBox96";
            this.textBox96.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0416667461395264D), Telerik.Reporting.Drawing.Unit.Inch(0.20000007748603821D));
            this.textBox96.Style.Font.Underline = true;
            this.textBox96.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox96.StyleName = "";
            // 
            // textBox97
            // 
            this.textBox97.Name = "textBox97";
            this.textBox97.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0416667461395264D), Telerik.Reporting.Drawing.Unit.Inch(0.20000001788139343D));
            this.textBox97.Style.Color = System.Drawing.Color.Gray;
            this.textBox97.Style.Font.Bold = true;
            this.textBox97.Style.Font.Name = "Segoe UI";
            this.textBox97.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox97.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(15D);
            this.textBox97.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox97.StyleName = "";
            resources.ApplyResources(this.textBox97, "textBox97");
            // 
            // textBox101
            // 
            this.textBox101.Name = "textBox101";
            this.textBox101.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.8083338737487793D), Telerik.Reporting.Drawing.Unit.Inch(0.2000001072883606D));
            this.textBox101.Style.Font.Underline = true;
            this.textBox101.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox101.StyleName = "";
            // 
            // textBox102
            // 
            this.textBox102.Name = "textBox102";
            this.textBox102.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.8083338737487793D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox102.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox102.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox102.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox102.StyleName = "";
            resources.ApplyResources(this.textBox102, "textBox102");
            // 
            // textBox45
            // 
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.8624987602233887D), Telerik.Reporting.Drawing.Unit.Inch(0.20208342373371124D));
            this.textBox45.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox45.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox45.StyleName = "";
            resources.ApplyResources(this.textBox45, "textBox45");
            // 
            // textBox89
            // 
            this.textBox89.Name = "textBox89";
            this.textBox89.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.8624987602233887D), Telerik.Reporting.Drawing.Unit.Inch(0.2083333283662796D));
            this.textBox89.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox89.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox89.StyleName = "";
            resources.ApplyResources(this.textBox89, "textBox89");
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.4747986793518066D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.pictureBox1.MimeType = "";
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.5D), Telerik.Reporting.Drawing.Unit.Inch(3.5D));
            this.pictureBox1.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.Stretch;
            this.pictureBox1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.pictureBox1.Value = "=Fields.MapImage";
            // 
            // textBox30
            // 
            this.textBox30.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9935123920440674D), Telerik.Reporting.Drawing.Unit.Inch(5.0936713218688965D));
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.0064482688903809D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox30.Style.BackgroundColor = System.Drawing.Color.Silver;
            this.textBox30.Style.Color = System.Drawing.Color.Black;
            this.textBox30.Style.Font.Bold = true;
            this.textBox30.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox30.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox30.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            resources.ApplyResources(this.textBox30, "textBox30");
            // 
            // subReport1
            // 
            this.subReport1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(4.4000792503356934D));
            this.subReport1.Name = "subReport1";
            instanceReportSource1.ReportDocument = this.portraitProductsSubReport1;
            this.subReport1.ReportSource = instanceReportSource1;
            this.subReport1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.9660720825195312D), Telerik.Reporting.Drawing.Unit.Inch(0.299999475479126D));
            // 
            // textBox26
            // 
            this.textBox26.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.089381851255893707D), Telerik.Reporting.Drawing.Unit.Inch(0.80019694566726685D));
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.98941820859909058D), Telerik.Reporting.Drawing.Unit.Inch(0.29992121458053589D));
            this.textBox26.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox26.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox26.Style.Font.Bold = true;
            this.textBox26.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox26.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            resources.ApplyResources(this.textBox26, "textBox26");
            // 
            // textBox27
            // 
            this.textBox27.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.0788788795471191D), Telerik.Reporting.Drawing.Unit.Inch(0.80019694566726685D));
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.1684646606445312D), Telerik.Reporting.Drawing.Unit.Inch(0.29992121458053589D));
            this.textBox27.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox27.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox27.Style.Font.Bold = true;
            this.textBox27.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox27.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            resources.ApplyResources(this.textBox27, "textBox27");
            // 
            // workOrderDataSource
            // 
            this.workOrderDataSource.DataSource = typeof(Landdb.ReportModels.WorkOrder.WorkOrderData);
            this.workOrderDataSource.Name = "workOrderDataSource";
            // 
            // StandardWorkOrder
            // 
            this.DataSource = this.workOrderDataSource;
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pageHeaderSection1,
            this.detail,
            this.pageFooterSection1,
            this.reportHeaderSection1});
            this.Name = "StandardWorkOrder";
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(0.25D), Telerik.Reporting.Drawing.Unit.Inch(0.25D), Telerik.Reporting.Drawing.Unit.Inch(0.25D), Telerik.Reporting.Drawing.Unit.Inch(0.25D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            this.Style.BackgroundColor = System.Drawing.Color.White;
            this.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0D);
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(8.0101776123046875D);
            ((System.ComponentModel.ISupportInitialize)(this.portraitProductsSubReport1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PageFooterSection pageFooterSection1;
        private Telerik.Reporting.ReportHeaderSection reportHeaderSection1;
        private Telerik.Reporting.TextBox textBox25;
        private Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox69;
        private Telerik.Reporting.TextBox pageInfoTextBox;
        private Telerik.Reporting.Table table3;
        private Telerik.Reporting.TextBox textBox71;
        private Telerik.Reporting.TextBox textBox73;
        private Telerik.Reporting.TextBox textBox70;
        private Telerik.Reporting.TextBox textBox72;
        private Telerik.Reporting.ObjectDataSource workOrderDataSource;
        private Telerik.Reporting.TextBox textBox76;
        private Telerik.Reporting.TextBox textBox75;
        private Telerik.Reporting.TextBox textBox68;
        private Telerik.Reporting.TextBox textBox67;
        private Telerik.Reporting.TextBox textBox66;
        private Telerik.Reporting.TextBox textBox65;
        private Telerik.Reporting.Table table2;
        private Telerik.Reporting.TextBox textBox52;
        private Telerik.Reporting.TextBox textBox54;
        private Telerik.Reporting.TextBox textBox56;
        private Telerik.Reporting.TextBox textBox58;
        private Telerik.Reporting.TextBox textBox60;
        private Telerik.Reporting.TextBox textBox62;
        private Telerik.Reporting.TextBox textBox64;
        private Telerik.Reporting.TextBox textBox51;
        private Telerik.Reporting.TextBox textBox53;
        private Telerik.Reporting.TextBox textBox55;
        private Telerik.Reporting.TextBox textBox63;
        private Telerik.Reporting.TextBox textBox57;
        private Telerik.Reporting.TextBox textBox59;
        private Telerik.Reporting.TextBox textBox61;
        private Telerik.Reporting.TextBox textBox50;
        private Telerik.Reporting.TextBox textBox32;
        private Telerik.Reporting.Panel panel1;
        private Telerik.Reporting.TextBox textBox37;
        private Telerik.Reporting.TextBox textBox47;
        private Telerik.Reporting.TextBox textBox48;
        private Telerik.Reporting.TextBox textBox84;
        private Telerik.Reporting.TextBox textBox85;
        private Telerik.Reporting.TextBox textBox86;
        private Telerik.Reporting.TextBox textBox87;
        private Telerik.Reporting.TextBox textBox88;
        private Telerik.Reporting.TextBox textBox31;
        private Telerik.Reporting.TextBox textBox34;
        private Telerik.Reporting.Table table4;
        private Telerik.Reporting.TextBox textBox40;
        private Telerik.Reporting.TextBox textBox45;
        private Telerik.Reporting.TextBox textBox82;
        private Telerik.Reporting.TextBox textBox89;
        private Telerik.Reporting.TextBox textBox43;
        private Telerik.Reporting.TextBox textBox35;
        private Telerik.Reporting.PictureBox pictureBox1;
        private Telerik.Reporting.TextBox textBox30;
        private Telerik.Reporting.TextBox textBox90;
        private Telerik.Reporting.TextBox textBox91;
        private Telerik.Reporting.TextBox textBox96;
        private Telerik.Reporting.TextBox textBox97;
        private Telerik.Reporting.TextBox textBox101;
        private Telerik.Reporting.TextBox textBox102;
        private Telerik.Reporting.TextBox textBox95;
        private Telerik.Reporting.TextBox textBox100;
        private Telerik.Reporting.TextBox textBox104;
        private Telerik.Reporting.SubReport subReport1;
        private Application.PortraitProductsSubReport portraitProductsSubReport1;
        private Telerik.Reporting.TextBox textBox26;
        private Telerik.Reporting.TextBox textBox27;
        private Telerik.Reporting.TextBox textBox93;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox txtDataSourceLabel;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox3;
    }
}