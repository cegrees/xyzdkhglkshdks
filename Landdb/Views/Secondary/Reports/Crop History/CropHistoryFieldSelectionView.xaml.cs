﻿using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Secondary.Reports.Crop_History;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Landdb.Client.Spatial;
using Microsoft.Maps.MapControl.WPF;

namespace Landdb.Views.Secondary.Reports.Crop_History
{
    /// <summary>
    /// Interaction logic for CropHistoryFieldSelectionView.xaml
    /// </summary>
    public partial class CropHistoryFieldSelectionView : UserControl
    {
        bool bingMapsHasSize = false;
        CropHistoryFieldSelectionViewModel viewModel;

        public CropHistoryFieldSelectionView()
        {
            InitializeComponent();
            BingMap.IsVisibleChanged += BingMap_IsVisibleChanged;
            BingMap.SizeChanged += BingMap_SizeChanged;
            BingMap.Loaded += BingMap_Loaded;
            this.DataContextChanged += FieldsPageView_DataContextChanged;
            var dataContext = this.DataContext as CropHistoryFieldSelectionView;

        }

        void BingMap_Loaded(object sender, RoutedEventArgs e)
        {
            UpdateMap();
        }

        private void FieldsPageView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (this.DataContext != null)
            {
                ViewModel = this.DataContext as CropHistoryFieldSelectionViewModel;
            }
        }

        private CropHistoryFieldSelectionViewModel ViewModel
        {
            get { return viewModel; }
            set
            {
                viewModel = value;
                viewModel.MapUpdated += dataContext_MapUpdated;
                Messenger.Default.Register<ShowMainViewMessage>(this, UnHookMapEvent);
            }
        }

        private void dataContext_MapUpdated(object sender, EventArgs e)
        {
            UpdateMap();
        }

        void BingMap_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (e.NewSize.Height > 0 && e.NewSize.Width > 0)
            {
                if (!bingMapsHasSize)
                {
                    bingMapsHasSize = true;
                    UpdateMap();
                }
            }
            else
            {
                bingMapsHasSize = false;
            }
        }

        private void BingMap_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ((MainWindow)System.Windows.Application.Current.MainWindow).Draggable = false;
        }

        private void BingMap_MouseUp(object sender, MouseButtonEventArgs e)
        {
            ((MainWindow)System.Windows.Application.Current.MainWindow).Draggable = true;
        }

        void BingMap_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (BingMap.IsVisible)
            {
                UpdateMap();
            }
        }

        void UpdateMap()
        {
            var vm = this.DataContext as CropHistoryFieldSelectionViewModel;

            if (vm == null || BingMap == null || !BingMap.IsLoaded || !bingMapsHasSize) { return; }

            try
            {
                if (vm.BingMapsLayer != null)
                {
                    BingMap.Children.Clear();

                    BingMap.Children.Add(vm.BingMapsLayer);
                    var box = vm.BingMapsLayer.GetBoundingBox().Inflate(0.1);

                    if (box != null)
                    {
                        BingMap.SetView(box);
                    }
                }
            }
            catch { }  // TODO: Eat for now; probably need to bubble something to the user
        }

        public void UnHookMapEvent(ShowMainViewMessage e)
        {
            if (ViewModel != null)
            {
                ViewModel.MapUpdated -= dataContext_MapUpdated;
                Messenger.Default.Unregister<ShowMainViewMessage>(this);
            }
        }

        private void ToggleBTN_Click(object sender, RoutedEventArgs e)
        {
            if (BingMap.Mode.ToString() == "Microsoft.Maps.MapControl.WPF.RoadMode")
            {
                //Set the map mode to Aerial with labels
                BingMap.Mode = new AerialMode(true);
            }
            else if (BingMap.Mode.ToString() == "Microsoft.Maps.MapControl.WPF.AerialMode")
            {
                //Set the map mode to RoadMode
                BingMap.Mode = new RoadMode();
            }
        }

    }
}
