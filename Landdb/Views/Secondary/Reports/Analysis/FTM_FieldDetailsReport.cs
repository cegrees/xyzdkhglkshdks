namespace Landdb.Views.Secondary.Reports.Analysis
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;

    /// <summary>
    /// Summary description for FTM_FieldDetailsReport.
    /// </summary>
    public partial class FTM_FieldDetailsReport : Telerik.Reporting.Report
    {
        public FTM_FieldDetailsReport()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }
    }
}