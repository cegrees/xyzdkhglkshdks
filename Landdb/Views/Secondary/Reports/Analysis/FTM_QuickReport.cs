namespace Landdb.Views.Secondary.Reports.Analysis
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;

    /// <summary>
    /// Summary description for FTM_QuickReport.
    /// </summary>
    public partial class FTM_QuickReport : Telerik.Reporting.Report
    {
        public FTM_QuickReport()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }
    }
}