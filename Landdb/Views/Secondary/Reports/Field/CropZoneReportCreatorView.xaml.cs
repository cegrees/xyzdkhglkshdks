﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Landdb.Views.Secondary.Reports.Field
{
    /// <summary>
    /// Interaction logic for CropZoneReportCreatorView.xaml
    /// </summary>
    public partial class CropZoneReportCreatorView : UserControl
    {
        public CropZoneReportCreatorView()
        {
            InitializeComponent();
        }

        private void ApplicatorTabs_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ApplicatorTabs.UpdateLayout();
        }
    }
}
