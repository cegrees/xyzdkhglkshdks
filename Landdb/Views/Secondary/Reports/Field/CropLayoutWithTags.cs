namespace Landdb.Views.Secondary.Reports.Field
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;

    /// <summary>
    /// Summary description for CropLayoutWithTags.
    /// </summary>
    public partial class CropLayoutWithTags : Telerik.Reporting.Report
    {
        public CropLayoutWithTags()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }
    }
}