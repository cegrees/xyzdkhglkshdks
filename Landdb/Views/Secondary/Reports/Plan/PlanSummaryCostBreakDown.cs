namespace Landdb.Views.Secondary.Reports.Plan
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;

    /// <summary>
    /// Summary description for PlanSummaryCostBreakDown.
    /// </summary>
    public partial class PlanSummaryCostBreakDown : Telerik.Reporting.Report
    {
        public PlanSummaryCostBreakDown()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }
    }
}