﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Landdb.Views.Secondary.Map {
    /// <summary>
    /// Interaction logic for SimpleDrawView.xaml
    /// </summary>
    public partial class SimpleDrawView : UserControl {

        public SimpleDrawView() {
            InitializeComponent();
        }


        private void CropZone_MouseUp(object sender, MouseButtonEventArgs e) {
            if (sender is FrameworkElement) {
                var fe = sender as FrameworkElement;
                //CropZone_Manipulated(fe);
            }
        }

        private void CropZone_TouchUp(object sender, TouchEventArgs e) {
            if (sender is FrameworkElement) {
                var fe = sender as FrameworkElement;
                //CropZone_Manipulated(fe);
            }
        }
    }
}
