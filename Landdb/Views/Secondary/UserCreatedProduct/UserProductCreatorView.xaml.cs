﻿using Landdb.ViewModel.Secondary.UserCreatedProduct;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Landdb.Views.Secondary.UserCreatedProduct {
    /// <summary>
    /// Interaction logic for UserProductCreatorView.xaml
    /// </summary>
    public partial class UserProductCreatorView : UserControl {

        Dictionary<string, decimal> FertValues;

        public UserProductCreatorView() {
            InitializeComponent();
            FertValues = new Dictionary<string, decimal>();
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (this.DataContext != null)
            {
                var dataContext = this.DataContext as UserProductCreatorViewModel;
                var newFormulation = new FertilizerFormulation();

                var textBox = sender as TextBox;
                decimal formulation = 0m;
                var seeThis = decimal.TryParse(textBox.Text, out formulation);

                if (decimal.TryParse(textBox.Text, out formulation))
                {
                    if (FertValues.ContainsKey(textBox.Name))
                    {
                        FertValues[textBox.Name] = formulation;
                    }
                    else
                    {
                        FertValues.Add(textBox.Name, formulation);
                    }
                }

                //dataContext.IsFormulationNull = false;
                dataContext.IsFormulationNull = true;
                foreach (var item in FertValues)
                {
                    if (item.Value > 0)
                    {
                        dataContext.IsFormulationNull = false;
                    }
                }
            }
        }
    }
}
