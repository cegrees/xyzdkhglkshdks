﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Landdb.Views.Shared.Popups {
    /// <summary>
    /// Interaction logic for EditActiveIngredientLoadView.xaml
    /// </summary>
    public partial class EditActiveIngredientLoadView : UserControl {
        public EditActiveIngredientLoadView() {
            InitializeComponent();

            this.Loaded += EditActiveIngredientLoadView_Loaded;
        }

        void EditActiveIngredientLoadView_Loaded(object sender, RoutedEventArgs e) {
            maxValueTextBox.Focus();
            Keyboard.Focus(maxValueTextBox);
        }

        private void Button_Click(object sender, RoutedEventArgs e) {
            if (sender is Button) {
                ((Button)sender).Focus();
            }
        }
    }
}
