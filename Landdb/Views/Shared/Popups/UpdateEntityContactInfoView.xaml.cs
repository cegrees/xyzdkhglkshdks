﻿using System.Windows;
using System.Windows.Controls;

namespace Landdb.Views.Shared.Popups {
	/// <summary>
	/// Interaction logic for EditCompanyInfo.xaml
	/// </summary>
	public partial class UpdateEntityContactInfoView : UserControl {
		public UpdateEntityContactInfoView() {
			InitializeComponent();
		}

		private void SaveButton_Click(object sender, RoutedEventArgs e) {
			if (sender is Button) {
				((Button)sender).Focus();
			}
		}
	}
}
