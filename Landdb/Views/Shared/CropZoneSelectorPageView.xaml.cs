﻿using Landdb.ViewModel.Secondary.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Maps.MapControl.WPF;
using Landdb.Client.Spatial;
using Landdb.ViewModel.Shared;
using GalaSoft.MvvmLight.Messaging;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel;

namespace Landdb.Views.Shared {
    /// <summary>
    /// Interaction logic for FieldsPageView.xaml
    /// </summary>
    public partial class CropZoneSelectorPageView : UserControl {
        BaseFieldSelectionViewModel viewModel;

        public CropZoneSelectorPageView() {
            InitializeComponent();
            //BingMap.IsVisibleChanged += BingMap_IsVisibleChanged;
            //BingMap.SizeChanged += BingMap_SizeChanged;
            //BingMap.Loaded += BingMap_Loaded;
            this.DataContextChanged += FieldsPageView_DataContextChanged;
            var dataContext = this.DataContext as BaseFieldSelectionViewModel;

        }

        void BingMap_Loaded(object sender, RoutedEventArgs e)
        {
            //UpdateMap();
        }

        private void FieldsPageView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (this.DataContext != null)
            {
                ViewModel = this.DataContext as BaseFieldSelectionViewModel;
            }
        }

        private BaseFieldSelectionViewModel ViewModel
        {
            get { return viewModel; }
            set 
            {
                viewModel = value;
                viewModel.MapUpdated += dataContext_MapUpdated;           
                //Messenger.Default.Register<ShowMainViewMessage>(this, UnHookMapEvent);
            }
        }

        private void dataContext_MapUpdated(object sender, EventArgs e)
        {
            //UpdateMap();
        }

        void BingMap_SizeChanged(object sender, SizeChangedEventArgs e) {
            //if (e.NewSize.Height > 0 && e.NewSize.Width > 0) {
            //    if (!bingMapsHasSize) {
            //        bingMapsHasSize = true;
            //        UpdateMap();
            //    }
            //} else {
            //    bingMapsHasSize = false;
            //}
        }

        private void BingMap_MouseDown(object sender, MouseButtonEventArgs e) {
            ((MainWindow)System.Windows.Application.Current.MainWindow).Draggable = false;
        }

        private void BingMap_MouseUp(object sender, MouseButtonEventArgs e) {
            ((MainWindow)System.Windows.Application.Current.MainWindow).Draggable = true;
        }

        private void CropZone_MouseUp(object sender, MouseButtonEventArgs e) {
            //if (sender is FrameworkElement) {
            //    var fe = sender as FrameworkElement;
            //    CropZone_Manipulated(fe);
            //}
        }

        private void CropZone_TouchUp(object sender, TouchEventArgs e) {
            //if (sender is FrameworkElement) {
            //    var fe = sender as FrameworkElement;
            //    CropZone_Manipulated(fe);
            //}
        }

        void CropZone_Manipulated(FrameworkElement fe) {
            var dc = fe.DataContext as CropZoneSelectionViewModel;

            if (dc != null)
            {
                var vm = dc.CreateEditModel();

                //UpdateAreaView.DataContext = vm;

                //UpdateAreaPopup.PlacementTarget = fe;
                //UpdateAreaPopup.Placement = System.Windows.Controls.Primitives.PlacementMode.Top;
                //UpdateAreaPopup.IsOpen = true;
            }
        }

        void BingMap_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e) {
            //if (BingMap.IsVisible) {
            //    UpdateMap();
            //}
        }

        //void UpdateMap() {
        //    var vm = this.DataContext as BaseFieldSelectionViewModel;

        //    if (vm == null || BingMap == null || !BingMap.IsLoaded || !bingMapsHasSize) { return; }

        //    try {
        //        if (vm.BingMapsLayer != null) {
        //            BingMap.Children.Clear();

        //            BingMap.Children.Add(vm.BingMapsLayer);
        //            var box = vm.BingMapsLayer.GetBoundingBox().Inflate(0.1);

        //            if (box != null) {
        //                BingMap.SetView(box);
        //            }
        //        }
        //    } catch { }  // TODO: Eat for now; probably need to bubble something to the user
        //}

        public void UnHookMapEvent(ShowMainViewMessage e)
        {
            //if (ViewModel != null)
            //{
            //    ViewModel.MapUpdated -= dataContext_MapUpdated;
            //    Messenger.Default.Unregister<ShowMainViewMessage>(this);
            //}
        }
    }
}