﻿using Landdb.ViewModel.Reports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Landdb.Views.Reports {
    /// <summary>
    /// Interaction logic for ReportSelectionPageView.xaml
    /// </summary>
    public partial class ReportSelectionPageView : UserControl {
        public ReportSelectionPageView() {
            InitializeComponent();
        }

        private void TemplatesList_MouseDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void TemplatesList_Selected(object sender, RoutedEventArgs e)
        {

        }

        private void TemplatesList_TouchDown(object sender, TouchEventArgs e)
        {
            var vm = this.DataContext as ReportSelectionPageViewModel;
            vm.LaunchSelectedTemplate.Execute(vm.SelectedTemplate);
        }

    }
}
