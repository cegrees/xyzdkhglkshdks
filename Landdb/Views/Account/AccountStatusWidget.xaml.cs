﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Landdb.ViewModel.Account;

namespace Landdb.Views.Account {
    /// <summary>
    /// Interaction logic for AccountStatusWidget.xaml
    /// </summary>
    public partial class AccountStatusWidget : UserControl {
        public AccountStatusWidget() {
            InitializeComponent();
        }

        private void AccountButton_Click(object sender, RoutedEventArgs e) {
            AccountPopup.IsOpen = !AccountPopup.IsOpen;
        }

    }
}
