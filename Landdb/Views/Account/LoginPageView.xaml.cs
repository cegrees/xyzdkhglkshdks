﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NLog;
using Landdb.Client.Infrastructure;
using Landdb.ViewModel;

namespace Landdb.Views.Account {
    /// <summary>
    /// Interaction logic for LoginPageView.xaml
    /// </summary>
    public partial class LoginPageView : UserControl {
        Logger log = LogManager.GetCurrentClassLogger();

        public LoginPageView() {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e) {
            SignInControl.StartAuth(response => {
                if (response == null) {
                    // offline
                    log.Info("Signed on in offline mode");
                } else {
                    log.Info("Signed in with {0}", response.Token);
                    var tokenStore = App.CloudClientFactory.TokenStore as Controls.OAuth2.RequestSecurityTokenResponseStore;
                    tokenStore.Credentials = response;
                }

                this.ViewModel.OnLoginComplete(response);
            });
        }

        public StartupViewModel ViewModel {
            get { return this.DataContext as StartupViewModel; }
        }
    }
}
