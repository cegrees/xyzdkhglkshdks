﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Landdb.Views.Production {
    /// <summary>
    /// Interaction logic for WorkOrdersPageView.xaml
    /// </summary>
    public partial class WorkOrdersPageView : UserControl {
        public WorkOrdersPageView() {
            InitializeComponent();
        }

        private void SetCharmButton_Click(object sender, RoutedEventArgs e) {
            CharmPopup.IsOpen = !CharmPopup.IsOpen;
        }
    }
}
