﻿using Landdb.ViewModel.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Landdb.Client.Spatial;

namespace Landdb.Views.Production
{
    /// <summary>
    /// Interaction logic for SiteDataDetailsView.xaml
    /// </summary>
    public partial class SiteDataDetailsView : UserControl
    {
        public SiteDataDetailsView()
        {
            InitializeComponent();
            this.DataContextChanged += CropZoneDetailsView_DataContextChanged;
            BingMap.IsVisibleChanged += BingMap_IsVisibleChanged;
        }

        void BingMap_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (BingMap.IsVisible)
            {
                UpdateMap();
            }
        }

        private void BingMap_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ((MainWindow)Application.Current.MainWindow).Draggable = false;
        }

        private void BingMap_MouseUp(object sender, MouseButtonEventArgs e)
        {
            ((MainWindow)Application.Current.MainWindow).Draggable = true;
        }

        void CropZoneDetailsView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            UpdateMap();
        }

        void BingMap_Loaded_1(object sender, RoutedEventArgs e)
        {
            UpdateMap();
        }

        void UpdateMap()
        {
            var vm = this.DataContext as SiteDataDetailsViewModel;

            if (vm == null || BingMap == null || !BingMap.IsLoaded) { return; }

            try
            {
                if (vm.ShouldShowBingMaps && vm.BingMapsLayer != null)
                {
                    BingMap.Children.Clear();

                    BingMap.Children.Add(vm.BingMapsLayer);
                    var box = vm.BingMapsLayer.GetBoundingBox().Inflate(0.1);
                    BingMap.SetView(box);

                    BingMap.Children.Add(vm.ObservationsLayer);
                }
            }
            catch { }  // TODO: Eat for now; probably need to bubble something to the user
        }

    }
}
