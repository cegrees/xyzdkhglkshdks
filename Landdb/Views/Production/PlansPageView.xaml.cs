﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Landdb.Views.Production {
    /// <summary>
    /// Interaction logic for PlansPageView.xaml
    /// </summary>
    public partial class PlansPageView : UserControl {
        public PlansPageView() {
            InitializeComponent();
        }

        private void SetCharmButton_Click(object sender, RoutedEventArgs e) {
            CharmPopup.IsOpen = !CharmPopup.IsOpen;
        }

        private void SetEditorBtn_Click(object sender, RoutedEventArgs e)
        {
            MassCropPopup.IsOpen = !MassCropPopup.IsOpen;
        }

        private void SetMergeBtn_Click(object sender, RoutedEventArgs e) {
            MergePopup.IsOpen = !MergePopup.IsOpen;
        }
    }
}
