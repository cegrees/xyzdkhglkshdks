﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Landdb.Views.Production.Popups
{
    /// <summary>
    /// Interaction logic for IntegrationsPageInfo.xaml
    /// </summary>
    public partial class IntegrationsPageInfo : UserControl
    {
        public IntegrationsPageInfo()
        {
            InitializeComponent();
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Hyperlink_Click(object sender, RoutedEventArgs e)
        {
            var hyperLink = sender as System.Windows.Documents.Hyperlink;
            Process.Start(hyperLink.NavigateUri.ToString());
        }
    }
}
