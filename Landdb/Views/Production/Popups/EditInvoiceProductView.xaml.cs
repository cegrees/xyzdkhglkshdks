﻿using System.Windows;
using System.Windows.Controls;

namespace Landdb.Views.Production.Popups {
	/// <summary>
	/// Interaction logic for EditInvoiceProductView.xaml
	/// </summary>
	public partial class EditInvoiceProductView : UserControl {
		public EditInvoiceProductView() {
			InitializeComponent();
		}

		private void SaveButton_Click(object sender, RoutedEventArgs e) {
			if (sender is Button) {
				((Button)sender).Focus();
			}
		}
	}
}