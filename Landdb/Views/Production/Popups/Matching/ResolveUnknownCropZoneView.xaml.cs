﻿using Landdb.ViewModel.Secondary.Recommendation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Maps.MapControl.WPF;
using Landdb.Client.Spatial;

namespace Landdb.Views.Production.Popups.Matching
{
    /// <summary>
    /// Interaction logic for ResolveUnknownCropZone.xaml
    /// </summary>
    public partial class ResolveUnknownCropZoneView : UserControl
    {
        bool bingMapsHasSize = false;

        public ResolveUnknownCropZoneView()
        {
            InitializeComponent();
            BingMap.IsVisibleChanged += BingMap_IsVisibleChanged;
            BingMap.SizeChanged += BingMap_SizeChanged;
        }

        void BingMap_SizeChanged(object sender, SizeChangedEventArgs e) {
            if (e.NewSize.Height > 0 && e.NewSize.Width > 0) {
                if (!bingMapsHasSize) {
                    bingMapsHasSize = true;
                    UpdateMap();
                }
            } else {
                bingMapsHasSize = false;
            }
        }

        private void BingMap_MouseDown(object sender, MouseButtonEventArgs e) {
            ((MainWindow)System.Windows.Application.Current.MainWindow).Draggable = false;
        }

        private void BingMap_MouseUp(object sender, MouseButtonEventArgs e) {
            ((MainWindow)System.Windows.Application.Current.MainWindow).Draggable = true;
        }
 
        void BingMap_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e) {
            if (BingMap.IsVisible) {
                UpdateMap();
            }
        }


        void UpdateMap() {
            var vm = this.DataContext as RecommendedFieldSelectionViewModel;

            if (vm == null || BingMap == null || !BingMap.IsLoaded || !bingMapsHasSize) { return; }

            try {
                if (vm.BingMapsLayer != null) {
                    BingMap.Children.Clear();

                    BingMap.Children.Add(vm.BingMapsLayer);
                    var box = vm.BingMapsLayer.GetBoundingBox().Inflate(0.1);
                    BingMap.SetView(box);
                }
            } catch { }  // TODO: Eat for now; probably need to bubble something to the user
        }

    }
}
