﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Landdb.Views.Production.Popups {
    /// <summary>
    /// Interaction logic for ChangeTimeView.xaml
    /// </summary>
    public partial class ChangeTimeView : UserControl {
        public ChangeTimeView() {
            InitializeComponent();
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void StackPanel_TouchDown(object sender, TouchEventArgs e)
        {
            if ((bool)UpdateWeatherCHK.IsChecked) { UpdateWeatherCHK.IsChecked = false; }
            else { UpdateWeatherCHK.IsChecked = true; }
        }

        private void StackPanel_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if ((bool)UpdateWeatherCHK.IsChecked) { UpdateWeatherCHK.IsChecked = false; }
            else { UpdateWeatherCHK.IsChecked = true; }
        }
    }
}
