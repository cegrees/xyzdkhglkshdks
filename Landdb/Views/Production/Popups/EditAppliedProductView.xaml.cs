﻿using Landdb.ViewModel.Production.Popups;
using System.Windows;
using System.Windows.Controls;

namespace Landdb.Views.Production.Popups {
	/// <summary>
	/// Interaction logic for EditAppliedProductView.xaml
	/// </summary>
	public partial class EditAppliedProductView : UserControl {
		public EditAppliedProductView() {
			InitializeComponent();
		}

		private void SaveButton_Click(object sender, RoutedEventArgs e) {
			if (sender is Button) {
				((Button)sender).Focus();
			}
		}

        private void ProductListBox_Loaded(object sender, RoutedEventArgs e)
        {
            if (this.DataContext is EditAppliedProductViewModel)
            {
                var vm = this.DataContext as EditAppliedProductViewModel;
                var autoCompleteBox = sender as AutoCompleteBox;
                if (vm.SelectedPest == null)
                {
                    autoCompleteBox.Text = string.Empty;
                }
            }
            else if (this.DataContext is EditWorkOrderProductViewModel)
            {
                var vm = this.DataContext as EditWorkOrderProductViewModel;

                if (vm.SelectedPest == null)
                {
                    var autoCompleteBox = sender as AutoCompleteBox;
                    autoCompleteBox.Text = string.Empty;
                }
            }
        }
    }
}
