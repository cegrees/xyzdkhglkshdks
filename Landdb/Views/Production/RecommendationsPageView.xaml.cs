﻿using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Person;
using Landdb.Domain.ReadModels.Product;
using Landdb.Domain.ReadModels.Recommendation;
using Landdb.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using Landdb.Resources;

namespace Landdb.Views.Production {
    /// <summary>
    /// Interaction logic for RecommendationsPageView.xaml
    /// </summary>
    public partial class RecommendationsPageView : UserControl {
        public RecommendationsPageView() {
            InitializeComponent();
        }

        private void SetCharmButton_Click(object sender, RoutedEventArgs e) {
            CharmPopup.IsOpen = !CharmPopup.IsOpen;
        }

        string FormatEPANumber(string epaNum) {
            string regNumber = epaNum;
            if (string.IsNullOrEmpty(regNumber)) {
                regNumber = string.Empty;
            }
            if (regNumber.ToLower() == "exempt") {
                regNumber = "Exempt";
            }
            Regex r = new Regex("([1-9][0-9]*)");
            MatchCollection matches = r.Matches(regNumber);
            regNumber = string.Empty;
            for (int i = 0; i < matches.Count; i++) {
                if (i == matches.Count - 1) {
                    regNumber += matches[i].Value;
                } else {
                    regNumber += matches[i].Value + "-";
                }

            }
            return regNumber;
        }

        string ResolveAppliedUnit(string inUnits) {
            string appliedUnit;

            switch (inUnits) {
                case ("seeds"):
                    appliedUnit = Strings.Unit_Seed_Text.ToLower();
                    break;
                case ("ton"):
                    appliedUnit = Strings.Unit_ShortTon_Text.ToLower();
                    break;
                case ("t"):
                    appliedUnit = Strings.Unit_MetricTon_Text.ToLower();
                    break;
                case ("lb"):
                    appliedUnit = Strings.Unit_Pound_Text.ToLower();
                    break;
                case ("oz"):
                    appliedUnit = Strings.Unit_Ounce_Text.ToLower();
                    break;
                case ("ozm"):
                    appliedUnit = Strings.Unit_Ounce_Text.ToLower();
                    break;
                case ("kg"):
                    appliedUnit = Strings.Unit_Kilogram_Text.ToLower();
                    break;
                case ("g"):
                    appliedUnit = Strings.Unit_Gram_Text.ToLower();
                    break;
                case ("floz"):
                    appliedUnit = Strings.Unit_FlOz_Text.ToLower();
                    break;
                case ("qt"):
                    appliedUnit = Strings.Unit_Quart_Text.ToLower();
                    break;
                case ("pt"):
                    appliedUnit = Strings.Unit_Pint_Text.ToLower();
                    break;
                case ("ml"):
                    appliedUnit = Strings.Unit_Milliliter_Text.ToLower();
                    break;
                case ("l"):
                    appliedUnit = Strings.Unit_Liter_Text.ToLower();
                    break;
                case ("ga"):
                    appliedUnit = Strings.Unit_Gallon_Text.ToLower();
                    break;
                case ("gal"):
                    appliedUnit = Strings.Unit_Gallon_Text.ToLower();
                    break;
                case ("cl"):
                    appliedUnit = Strings.Unit_Centiliter_Text.ToLower();
                    break;
                default:
                    appliedUnit = inUnits;
                    // FIXME - need to get in here and interconvert the JD units that have no correlary LDB unit into a standard unit for volume or mass
                    break;
            }
            return (appliedUnit);
        }

    }
}