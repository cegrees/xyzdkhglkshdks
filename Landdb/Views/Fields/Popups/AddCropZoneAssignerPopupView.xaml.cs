﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Landdb.Views.Fields.Popups {
    /// <summary>
    /// Interaction logic for AddCropZonePopupView.xaml
    /// </summary>
    public partial class AddCropZoneAssignerPopupView : UserControl {
        public AddCropZoneAssignerPopupView() {
            InitializeComponent();

            this.Loaded += AddCropZoneAssignerPopupView_Loaded;
        }

        void AddCropZoneAssignerPopupView_Loaded(object sender, RoutedEventArgs e) {
            CropComboBox.Focus();
            Keyboard.Focus(CropComboBox);

        }

        private void CompleteButton_Click(object sender, RoutedEventArgs e) {
            CropComboBox.Focus();
        }
    }
}
