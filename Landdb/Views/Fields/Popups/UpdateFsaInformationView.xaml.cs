﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Landdb.Views.Fields.Popups {
    /// <summary>
    /// Interaction logic for UpdateFsaInformationView.xaml
    /// </summary>
    public partial class UpdateFsaInformationView : UserControl {
        public UpdateFsaInformationView() {
            InitializeComponent();

            this.PreviewKeyDown += UpdateFsaInformationView_PreviewKeyDown;
        }

        private void ComboBox_Selected_1(object sender, RoutedEventArgs e)
        {

        }

        void UpdateFsaInformationView_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                //do nothing
                e.Handled = true;
            }
        }
    }
}
