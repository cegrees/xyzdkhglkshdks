﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Landdb.Views.Fields.Popups {
    /// <summary>
    /// Interaction logic for AddItemPopupView.xaml
    /// </summary>
    public partial class AddFarmPopupView : UserControl {
        public AddFarmPopupView() {
            InitializeComponent();

            this.Loaded += AddFarmPopupView_Loaded;
        }

        void AddFarmPopupView_Loaded(object sender, RoutedEventArgs e) {
            NewItemComboBox.Focus();
            Keyboard.Focus(NewItemComboBox);
        }

        private void CompleteButton_Click(object sender, RoutedEventArgs e) {
            NewItemComboBox.Focus();
        }

    }
}
