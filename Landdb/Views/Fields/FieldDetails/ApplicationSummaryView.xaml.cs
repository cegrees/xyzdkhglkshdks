﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Landdb.Views.Fields.FieldDetails {
    /// <summary>
    /// Interaction logic for ApplicationSummaryView.xaml
    /// </summary>
    public partial class ApplicationSummaryView : UserControl {
        public ApplicationSummaryView() {
            InitializeComponent();
        }

        private void summaryGrid_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            DataGrid grid = sender as DataGrid;
            if(grid != null) {
                foreach(var col in grid.Columns) {
                    col.SetValue(FrameworkElement.DataContextProperty, e.NewValue);
                }
            }
        }
    }
}
