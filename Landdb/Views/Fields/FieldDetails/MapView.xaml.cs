﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Landdb.Views.Fields.FieldDetails {
    /// <summary>
    /// Interaction logic for MapView.xaml
    /// </summary>
    public partial class MapView : UserControl {
        public MapView() {
            InitializeComponent();
        }

        private void MapToolsButton_Click(object sender, RoutedEventArgs e) {
            MapToolsPopup.IsOpen = !MapToolsPopup.IsOpen;

            SearchMapPopup.IsOpen = false;
            OptionsMapPopup.IsOpen = false;
            ImportToolsPopup.IsOpen = false;
        }

        private void SearchMapButton_Click(object sender, RoutedEventArgs e) {
            SearchMapPopup.IsOpen = !SearchMapPopup.IsOpen;

            MapToolsPopup.IsOpen = false;
            OptionsMapPopup.IsOpen = false;
            ImportToolsPopup.IsOpen = false;
        }

        private void OptionsMapButton_Click_1(object sender, RoutedEventArgs e) {
            OptionsMapPopup.IsOpen = !OptionsMapPopup.IsOpen;

            SearchMapPopup.IsOpen = false;
            MapToolsPopup.IsOpen = false;
            ImportToolsPopup.IsOpen = false;
        }

        private void ImportToolsButton_Click(object sender, RoutedEventArgs e) {
            ImportToolsPopup.IsOpen = !ImportToolsPopup.IsOpen;

            MapToolsPopup.IsOpen = false;
            SearchMapPopup.IsOpen = false;
            OptionsMapPopup.IsOpen = false;

        }

        private void ApexImportToolsButton_Click(object sender, RoutedEventArgs e) {
            ApexImportToolsPopup.IsOpen = !ApexImportToolsPopup.IsOpen;

            MapToolsPopup.IsOpen = false;
            SearchMapPopup.IsOpen = false;
            OptionsMapPopup.IsOpen = false;

        }

        private void CLUImportToolsButton_Click(object sender, RoutedEventArgs e) {
            ImportCLUToolsPopup.IsOpen = !ImportCLUToolsPopup.IsOpen;

            MapToolsPopup.IsOpen = false;
            SearchMapPopup.IsOpen = false;
            OptionsMapPopup.IsOpen = false;

        }

        private void DeleteToolsButton_Click(object sender, RoutedEventArgs e) {
            DeleteToolsPopup.IsOpen = !DeleteToolsPopup.IsOpen;

            MapToolsPopup.IsOpen = false;
            SearchMapPopup.IsOpen = false;
            OptionsMapPopup.IsOpen = false;

        }

        private void MapOptionsPopupView_Loaded(object sender, RoutedEventArgs e)
        {

        }
        private void Hyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
            e.Handled = true;
        }
    }
}
