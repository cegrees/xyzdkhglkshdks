﻿using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Spatial;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Fields.MassEditorAssigner;
using Microsoft.Maps.MapControl.WPF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Landdb.Views.Fields.MassEditorAssigner {
	/// <summary>
	/// Interaction logic for MassEditorAssigner.xaml
	/// </summary>
	public partial class MassEditorAssignerView : UserControl {
		public MassEditorAssignerView() {
			InitializeComponent();
		}
	}
}
