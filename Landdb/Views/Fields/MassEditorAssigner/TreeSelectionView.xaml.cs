﻿using GalaSoft.MvvmLight.Messaging;
using Landdb.Client.Spatial;
using Landdb.Infrastructure.Messages;
using Landdb.ViewModel.Fields.MassEditorAssigner;
using Microsoft.Maps.MapControl.WPF;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;

namespace Landdb.Views.Fields.MassEditorAssigner {
	/// <summary>
	/// Interaction logic for TreeSelectionView.xaml
	/// </summary>
	public partial class TreeSelectionView : UserControl {
		public TreeSelectionView() {
			InitializeComponent();
			BingMap.IsVisibleChanged += BingMap_IsVisibleChanged;
			BingMap.SizeChanged += BingMap_SizeChanged;
			BingMap.Loaded += BingMap_Loaded;
			this.DataContextChanged += FieldsPageView_DataContextChanged;
			var dataContext = this.DataContext as TreeSelectionViewModel;
		}

		bool bingMapsHasSize = false;
		TreeSelectionViewModel viewModel;

		void BingMap_Loaded(object sender, RoutedEventArgs e) {
			UpdateMap();
		}

		private void FieldsPageView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
			if (this.DataContext != null) {
				ViewModel = this.DataContext as TreeSelectionViewModel;
			}
		}

		private TreeSelectionViewModel ViewModel {
			get { return viewModel; }
			set {
				if (value != null) {
					viewModel = value;
					viewModel.MapUpdated += dataContext_MapUpdated;
					Messenger.Default.Register<ShowMainViewMessage>(this, UnHookMapEvent);
				}
			}
		}

		private void dataContext_MapUpdated(object sender, EventArgs e) {
			UpdateMap();
		}

		void BingMap_SizeChanged(object sender, SizeChangedEventArgs e) {
			if (e.NewSize.Height > 0 && e.NewSize.Width > 0) {
				if (!bingMapsHasSize) {
					bingMapsHasSize = true;
					UpdateMap();
				}
			} else {
				bingMapsHasSize = false;
			}
		}

		private void CropZone_MouseUp(object sender, MouseButtonEventArgs e) {
			//if (sender is FrameworkElement) {
			//    var fe = sender as FrameworkElement;
			//    CropZone_Manipulated(fe);
			//}
		}

		private void BingMap_MouseDown(object sender, MouseButtonEventArgs e) {
			((MainWindow)System.Windows.Application.Current.MainWindow).Draggable = false;
		}

		private void BingMap_MouseUp(object sender, MouseButtonEventArgs e) {
			((MainWindow)System.Windows.Application.Current.MainWindow).Draggable = true;
		}

		private void CropZone_TouchUp(object sender, TouchEventArgs e) {
			//if (sender is FrameworkElement) {
			//    var fe = sender as FrameworkElement;
			//    CropZone_Manipulated(fe);
			//}
		}

		void CropZone_Manipulated(FrameworkElement fe) {
			var dc = fe.DataContext;

			//if (dc != null)
			//{
			//    var vm = dc.CreateEditModel();

			//    UpdateAreaView.DataContext = vm;

			//    UpdateAreaPopup.PlacementTarget = fe;
			//    UpdateAreaPopup.Placement = System.Windows.Controls.Primitives.PlacementMode.Top;
			//    UpdateAreaPopup.IsOpen = true;
			//}
		}

		void BingMap_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e) {
			if (BingMap.IsVisible) {
				UpdateMap();
			}
		}

		void UpdateMap() {
			var vms = this.DataContext;
			var vm = vms as TreeSelectionViewModel;

			if (vm == null || BingMap == null || !BingMap.IsLoaded || !bingMapsHasSize) { return; }

			try {
				if (vm.BingMapsLayer != null) {
					BingMap.Children.Clear();

					BingMap.Children.Add(vm.BingMapsLayer);
					var box = vm.BingMapsLayer.GetBoundingBox().Inflate(0.1);

					if (box != null) {
						BingMap.SetView(box);
					}
				}
				if (vm.LabelsLayer != null) {
					BingMap.Children.Add(vm.LabelsLayer);
				}
			} catch { }  // TODO: Eat for now; probably need to bubble something to the user
		}

		public void UnHookMapEvent(ShowMainViewMessage e) {
			if (ViewModel != null) {
				ViewModel.MapUpdated -= dataContext_MapUpdated;
				Messenger.Default.Unregister<ShowMainViewMessage>(this);
			}
		}

		private void ToggleBTN_TouchUp(object sender, TouchEventArgs e) {
			if (BingMap.Mode.ToString() == "Microsoft.Maps.MapControl.WPF.RoadMode") {
				//Set the map mode to Aerial with labels
				BingMap.Mode = new AerialMode(true);
			} else if (BingMap.Mode.ToString() == "Microsoft.Maps.MapControl.WPF.AerialMode") {
				//Set the map mode to RoadMode
				BingMap.Mode = new RoadMode();
			}
		}

		private void ToggleBTN_Click(object sender, RoutedEventArgs e) {
			if (BingMap.Mode.ToString() == "Microsoft.Maps.MapControl.WPF.RoadMode") {
				//Set the map mode to Aerial with labels
				BingMap.Mode = new AerialMode(true);
			} else if (BingMap.Mode.ToString() == "Microsoft.Maps.MapControl.WPF.AerialMode") {
				//Set the map mode to RoadMode
				BingMap.Mode = new RoadMode();
			}
		}
	}
}
