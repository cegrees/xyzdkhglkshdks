﻿using System.Windows;
using System.Windows.Controls;

namespace Landdb.Views.Fields.MassAssigner {
	/// <summary>
	/// Interaction logic for CropZoneDetailEditor.xaml
	/// </summary>
	public partial class EditCropZoneDetailPopupView : UserControl {
		public EditCropZoneDetailPopupView() {
			InitializeComponent();
		}

		private void SaveButton_Click(object sender, RoutedEventArgs e) {
			if (sender is Button) {
				((Button)sender).Focus();
			}
		}
	}
}