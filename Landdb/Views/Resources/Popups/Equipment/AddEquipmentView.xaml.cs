﻿using System.Windows;
using System.Windows.Controls;

namespace Landdb.Views.Resources.Popups.Equipment {
	/// <summary>
	/// Interaction logic for AddEquipmentView.xaml
	/// </summary>
	public partial class AddEquipmentView : UserControl {
		public AddEquipmentView() {
			InitializeComponent();
		}

		private void SetButton_Click(object sender, RoutedEventArgs e) {
			if (sender is Button) {
				((Button)sender).Focus();
			}
		}
	}
}