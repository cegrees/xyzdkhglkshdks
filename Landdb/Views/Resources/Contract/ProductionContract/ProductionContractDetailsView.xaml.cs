﻿using Landdb.ViewModel.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Maps.MapControl.WPF;
using Landdb.Client.Spatial;

namespace Landdb.Views.Resources.Contract.Production {
	/// <summary>
	/// Interaction logic for ProductionView.xaml
	/// </summary>
	public partial class ProductionContractDetailsView : UserControl {
		public ProductionContractDetailsView() {
			InitializeComponent();
		}
	}
}