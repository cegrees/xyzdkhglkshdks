﻿using Landdb.ViewModel.Maps;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Landdb.Views.Maps {
    /// <summary>
    /// Interaction logic for MapsPageView.xaml
    /// </summary>
    public partial class MapsPageView : UserControl {
        public MapsPageView() {
            InitializeComponent();
        }

        private void OptionsMapButton_Click_1(object sender, RoutedEventArgs e) {
            OptionsMapPopup.IsOpen = !OptionsMapPopup.IsOpen;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //MapContainer Create Image and Save
            var difWidth = MapContainer.Width;
            var difHeight = MapContainer.Height;

            var width = MapContainer.ActualWidth;
            var height = MapContainer.ActualHeight;
            RenderTargetBitmap bmp = new RenderTargetBitmap((int)width, (int)height, 96, 96, PixelFormats.Pbgra32);
            bmp.Render(MapContainer);
            PngBitmapEncoder png = new PngBitmapEncoder();
            png.Frames.Add(BitmapFrame.Create(bmp));

            try
            {
                //open up file dialog to save file....
                //then call createexcelfile to create the excel...
                string filename = string.Empty;
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();

                saveFileDialog1.Filter = "JPEG|*.jpg";
                saveFileDialog1.FilterIndex = 2;
                saveFileDialog1.RestoreDirectory = true;

                Nullable<bool> resultSaved = saveFileDialog1.ShowDialog();

                // Process save file dialog box results 
                if (resultSaved == true)
                {
                    // Save document 
                    filename = saveFileDialog1.FileName;

                    using (Stream stm = File.Create(filename))
                    {
                        png.Save(stm);
                    }
                }

                //now open file....
                System.Diagnostics.Process.Start(filename);
            }
            catch (Exception ex)
            {
                return;
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            //var vm = DataContext as MapsPageViewModel;
            //var width = MapContainer.ActualWidth;
            //var height = MapContainer.ActualHeight;
            //RenderTargetBitmap bmp = new RenderTargetBitmap((int)width, (int)height, 96, 96, PixelFormats.Pbgra32);
            //bmp.Render(MapContainer);
            //PngBitmapEncoder png = new PngBitmapEncoder();
            //png.Frames.Add(BitmapFrame.Create(bmp));

            //try
            //{

            //    MemoryStream fs = new MemoryStream();
            //    png.Save(fs);

            //    vm.Print(new Bitmap(fs));
            //}
            //catch (Exception ex)
            //{
            //    return;
            //}
        }
        private void Hyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
            e.Handled = true;
        }

    }
}
