﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows;
using Landdb.ViewModel.Maps;

namespace Landdb.Views.Maps {
    public class FilterDataTemplateSelector : DataTemplateSelector {
        public override System.Windows.DataTemplate SelectTemplate(object item, DependencyObject container) {
            FrameworkElement element = container as FrameworkElement;
            if (element != null && item != null && item is IMapFilter) {
                //AbstractTreeItemViewModel treeItem = item as AbstractTreeItemViewModel;

                if (item is CropFilterItem) {
                    return element.FindResource("CropFilterItemTemplate") as DataTemplate;
                }

                if (item is SeedVarietyFilterItem) {
                    return element.FindResource("VarietyFilterItemTemplate") as DataTemplate;
                }

                if (item is CropProtectionFilterItem) {
                    return element.FindResource("CropProtectionFilterItemTemplate") as DataTemplate;
                }

                if (item is FertilizerFilterItem) {
                    return element.FindResource("FertilizerFilterItemTemplate") as DataTemplate;
                }

                if (item is ServiceFilterItem) {
                    return element.FindResource("ServiceFilterItemTemplate") as DataTemplate;
                }

                if (item is AILoadFilterItem) {
                    return element.FindResource("AILoadFilterItemTemplate") as DataTemplate;
                }

                if (item is SSurgoLayers) {
                    return element.FindResource("SSurgoLayersTemplate") as DataTemplate;
                }

                if (item is REIPHIFilterItem) {
                    return element.FindResource("REIPHIFilterItemTemplate") as DataTemplate;
                }

                if (item is TagFilterItem) {
                    return element.FindResource("TagFilterItemTemplate") as DataTemplate;
                }

                if (item is TagOrderedFilterItem) {
                    return element.FindResource("TagOrderedFilterItemTemplate") as DataTemplate;
                }

                if (item is TagVarietyFilterItem) {
                    return element.FindResource("VarietyTagFilterItemTemplate") as DataTemplate;
                }

                if (item is RentContractFilterItem) {
                    return element.FindResource("ContractFilterItemTemplate") as DataTemplate;
                }

                if (item is ProductionContractFilterItem) {
                    return element.FindResource("ProductionContractFilterItemTemplate") as DataTemplate;
                }

                if (item is FieldFsaFilterItem) {
                    return element.FindResource("FieldFsaFilterItemTemplate") as DataTemplate;
                }

                if (item is CropFieldFsaFilterItem) {
                    return element.FindResource("CropFieldFsaFilterItemTemplate") as DataTemplate;
                }
            }

            return element.FindResource("BlankFilterItemTemplate") as DataTemplate;
        }
    }
}
