﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Media;

namespace Landdb.Behaviors {
    public class HighlightTextOnFocusBehavior : Behavior<TextBox> {
        //public bool HighlightTextOnFocus {
        //    get { return (bool)GetValue(HighlightTextOnFocusProperty); }
        //    set { SetValue(HighlightTextOnFocusProperty, value); }
        //}

        //public static readonly DependencyProperty HighlightTextOnFocusProperty =
        //    DependencyProperty.Register("HighlightTextOnFocus", typeof(bool), typeof(HighlightTextOnFocusBehavior), new UIPropertyMetadata(true));

        protected override void OnAttached() {
            base.OnAttached();

            this.AssociatedObject.GotKeyboardFocus += AssociatedObject_GotKeyboardFocus;
            this.AssociatedObject.PreviewMouseLeftButtonDown += AssociatedObject_PreviewMouseLeftButtonDown;
        }

        protected override void OnDetaching() {
            base.OnDetaching();

            this.AssociatedObject.GotKeyboardFocus -= AssociatedObject_GotKeyboardFocus;
            this.AssociatedObject.PreviewMouseLeftButtonDown -= AssociatedObject_PreviewMouseLeftButtonDown;
        }

        void AssociatedObject_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e) {
            TextBox tb = FindAncestor<TextBox>((DependencyObject)e.OriginalSource);

            if (tb == null)
                return;

            if (!tb.IsKeyboardFocusWithin) {
                tb.Focus();
                e.Handled = true;
            }
        }

        void AssociatedObject_GotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e) {
            var textBox = e.OriginalSource as TextBox;
            if (textBox != null) {
                textBox.SelectAll();
            }
        }

        static T FindAncestor<T>(DependencyObject current)
            where T : DependencyObject {
            current = VisualTreeHelper.GetParent(current);

            while (current != null) {
                if (current is T) {
                    return (T)current;
                }
                current = VisualTreeHelper.GetParent(current);
            };
            return null;
        }

    }
}
