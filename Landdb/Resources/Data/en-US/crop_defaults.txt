{
  "CropDefaults" : [
        {
          "CropId": "6ef1467e-7fe0-4c34-a5e9-5baded575c44",
          "_cropName" : "alfalfa",
          "DryMoisturePercentage": null,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 60,
                  "PartUnitName": "pound"
              }
          ]
      },      
        {
          "CropId": "4810aced-7ca3-4d7b-b6e0-51caae711478",
          "_cropName" : "alfalfa seed",
          "DryMoisturePercentage": null,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 60,
                  "PartUnitName": "pound"
              }
          ]
      },
      {
          "CropId": "01b2da1a-cfa2-4730-a804-30be8f3c8d4a",
          "_cropName" : "barley",
          "DryMoisturePercentage": 0.145,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 48,
                  "PartUnitName": "pound"
              }
          ]
      },
      {
          "CropId": "f722ee38-f6ae-4935-9bbb-031b872e41f2",
          "_cropName" : "feed barley",
          "DryMoisturePercentage": 0.145,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 48,
                  "PartUnitName": "pound"
              }
          ]
      },
      {
          "CropId": "417753ca-e852-4af9-b273-5662f5608429",
          "_cropName" : "malt barley",
          "DryMoisturePercentage": 0.145,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 48,
                  "PartUnitName": "pound"
              }
          ]
      },
      {
          "CropId": "b83f6d89-0fc8-42c2-b1da-d8c82b9c7a99",
          "_cropName" : "seed barley",
          "DryMoisturePercentage": 0.145,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 48,
                  "PartUnitName": "pound"
              }
          ]
      },
      {
          "CropId": "fe4b523f-47d1-4e00-aee6-e4819f9ce036",
          "_cropName" : "clover",
          "DryMoisturePercentage": null,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 60,
                  "PartUnitName": "pound"
              }
          ]
      },
      {
          "CropId": "f88f4e8d-8eb0-41db-b828-bc63d8d3ca48",
          "_cropName" : "arrowleaf clover",
          "DryMoisturePercentage": null,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 60,
                  "PartUnitName": "pound"
              }
          ]
      },
      {
          "CropId": "7570b736-39fe-4066-acd3-c3f9e0eadecf",
          "_cropName" : "crimson clover",
          "DryMoisturePercentage": null,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 60,
                  "PartUnitName": "pound"
              }
          ]
      },
      {
          "CropId": "7c606e7a-c20c-4c2f-999f-9fac6fbe4fd5",
          "_cropName" : "red clover",
          "DryMoisturePercentage": null,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 60,
                  "PartUnitName": "pound"
              }
          ]
      },
      {
          "CropId": "77072b39-4084-405b-b696-eb6c36360f6d",
          "_cropName" : "white clover",
          "DryMoisturePercentage": null,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 60,
                  "PartUnitName": "pound"
              }
          ]
      },
      {
          "CropId": "ae8eabd4-68a1-4867-9589-104dc09eb996",
          "_cropName" : "corn",
          "DryMoisturePercentage": 0.155,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 56,
                  "PartUnitName": "pound"
              }
          ]
      },
      {
          "CropId": "6dcc25e0-2cd9-44ce-b683-e2d446ce5a59",
          "_cropName" : "sweet corn",
          "DryMoisturePercentage": null,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 70,
                  "PartUnitName": "pound"
              }
          ]
      },
      {
          "CropId": "14835d04-bf9c-44fa-8c7c-147c9984f99d",
          "_cropName" : "field corn",
          "DryMoisturePercentage": 0.155,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 56,
                  "PartUnitName": "pound"
              }
          ]
      },
      {
          "CropId": "0faaeabe-11d5-4c47-934c-fdc591d06ee0",
          "_cropName" : "indian corn",
          "DryMoisturePercentage": 0.155,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 56,
                  "PartUnitName": "pound"
              }
          ]
      },
      {
          "CropId": "c6057e31-d48a-4511-8184-9fca429b39d8",
          "_cropName" : "popcorn",
          "DryMoisturePercentage": 0.155,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 56,
                  "PartUnitName": "pound"
              }
          ]
      },
      {
          "CropId": "08c6032b-309b-4ea7-88d0-e91c09cd2816",
          "_cropName" : "seed corn",
          "DryMoisturePercentage": 0.155,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 56,
                  "PartUnitName": "pound"
              }
          ]
      },
      {
          "CropId": "769325c0-86dd-4b15-adc6-aa4bb8624a2c",
          "_cropName" : "white corn",
          "DryMoisturePercentage": 0.155,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 56,
                  "PartUnitName": "pound"
              }
          ]
      },
      {
          "CropId": "bcaf4f4a-3e7c-4d6a-8143-db2467c27938",
          "_cropName" : "yellow corn",
          "DryMoisturePercentage": 0.155,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 56,
                  "PartUnitName": "pound"
              }
          ]
      },
      {
          "CropId": "128cb010-f6ea-49df-be3e-7e41c0e7da39",
          "_cropName" : "cotton",
          "DryMoisturePercentage": null,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 32,
                  "PartUnitName": "pound"
              }
          ]
      },
      {
          "CropId": "e36cee22-f6c7-4808-bc3a-276f8282854f",
          "_cropName" : "acala cotton",
          "DryMoisturePercentage": null,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 32,
                  "PartUnitName": "pound"
              }
          ]
      },
      {
          "CropId": "46a72002-9aa6-445c-af70-f7d4d0e1b803",
          "_cropName" : "upland cotton",
          "DryMoisturePercentage": null,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 32,
                  "PartUnitName": "pound"
              }
          ]
      },
      {
          "CropId": "34d92bf3-7bd9-4e81-aaf7-d51c0c28f0f2",
          "_cropName" : "cowpea",
          "DryMoisturePercentage": null,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 60,
                  "PartUnitName": "pound"
              }
          ]
      },
      {
          "CropId": "c026ddcd-b98b-448c-b6e6-5827c7adb2ab",
          "_cropName" : "flax",
          "DryMoisturePercentage": null,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 60,
                  "PartUnitName": "pound"
              }
          ]
      },
      {
          "CropId": "e0c2d768-5ad4-4947-a5ef-133b346f6d2e",
          "_cropName" : "tall fescue",
          "DryMoisturePercentage": null,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 14,
                  "PartUnitName": "pound"
              }
          ]
      },
      {
          "CropId": "dfed5082-5c76-4f81-83e2-69409bf72edf",
          "_cropName" : "millet",
          "DryMoisturePercentage": null,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 50,
                  "PartUnitName": "pound"
              }
          ]
      },
      {
          "CropId": "e69d631c-fd52-4daa-8ba5-891b085e6273",
          "_cropName" : "oats",
          "DryMoisturePercentage": 0.14,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 32,
                  "PartUnitName": "pound"
              }
          ]
      },
      {
          "CropId": "fc3a8346-0ebc-4c5a-84a3-cd803bd899c4",
          "_cropName" : "rape",
          "DryMoisturePercentage": null,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 60,
                  "PartUnitName": "pound"
              }
          ]
      },
      {
          "CropId": "04ad9135-3548-49ad-8fcd-564496c16566",
          "_cropName" : "rye",
          "DryMoisturePercentage": null,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 56,
                  "PartUnitName": "pound"
              }
          ]
      },
      {
          "CropId": "8a3143eb-17a2-47fc-80d8-7a480b767f01",
          "_cropName" : "grain sorghum",
          "DryMoisturePercentage": null,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 56,
                  "PartUnitName": "pound"
              }
          ]
      },
      {
          "CropId": "302289b0-c4d4-48ae-9bec-f32ed603b261",
          "_cropName" : "soybeans",
          "DryMoisturePercentage": 0.13,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 60,
                  "PartUnitName": "pound"
              }
          ]
      },
      {
          "CropId": "faef5270-36c9-4991-8210-5a85d8cf094b",
          "_cropName" : "food grade soybeans",
          "DryMoisturePercentage": 0.13,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 60,
                  "PartUnitName": "pound"
              }
          ]
      },
      {
          "CropId": "252f8a12-c2f8-43f9-be41-46323573851a",
          "_cropName" : "soybean seed",
          "DryMoisturePercentage": 0.13,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 60,
                  "PartUnitName": "pound"
              }
          ]
      },
      {
          "CropId": "67edc1ba-a8ea-431b-99d5-e9bd673750a1",
          "_cropName" : "sunflower",
          "DryMoisturePercentage": 0.1,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 24,
                  "PartUnitName": "pound"
              },
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 30,
                  "PartUnitName": "pound"
              }
          ]
      },
      {
          "CropId": "0aa2e116-7c08-42fb-a20d-bb749c4d21bf",
          "_cropName" : "timothy",
          "DryMoisturePercentage": null,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 45,
                  "PartUnitName": "pound"
              }
          ]
      },
      {
          "CropId": "e051dee4-c024-4cf6-a0b4-2a1c4d75c8e6",
          "_cropName" : "wheat",
          "DryMoisturePercentage": 0.135,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 60,
                  "PartUnitName": "pound"
              }
          ]
      },
      {
          "CropId": "a3d7ded8-0c96-4e3f-96f2-7f86333e1c8c",
          "_cropName" : "club wheat",
          "DryMoisturePercentage": 0.135,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 60,
                  "PartUnitName": "pound"
              }
          ]
      },
      {
          "CropId": "6ff43069-cabe-4b31-9d08-258959879981",
          "_cropName" : "dark northern spring wheat",
          "DryMoisturePercentage": 0.135,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 60,
                  "PartUnitName": "pound"
              }
          ]
      },
      {
          "CropId": "9733a875-6437-49a0-819a-ee07ebdae18a",
          "_cropName" : "durum wheat",
          "DryMoisturePercentage": 0.135,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 60,
                  "PartUnitName": "pound"
              }
          ]
      },
      {
          "CropId": "d7254d16-dca0-480a-8df2-19ac3252b041",
          "_cropName" : "hard red wheat",
          "DryMoisturePercentage": 0.135,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 60,
                  "PartUnitName": "pound"
              }
          ]
      },
      {
          "CropId": "9bbae152-1ca6-4c93-b276-02c2620a711c",
          "_cropName" : "hard red spring wheat",
          "DryMoisturePercentage": 0.135,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 60,
                  "PartUnitName": "pound"
              }
          ]
      },
      {
          "CropId": "16f4bb3c-e9a5-4582-947c-c697187645e2",
          "_cropName" : "hard red winter wheat",
          "DryMoisturePercentage": 0.135,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 60,
                  "PartUnitName": "pound"
              }
          ]
      },
      {
          "CropId": "e5f397a1-f030-4d7c-9512-d48bedf743d8",
          "_cropName" : "hard white wheat",
          "DryMoisturePercentage": 0.135,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 60,
                  "PartUnitName": "pound"
              }
          ]
      },
      {
          "CropId": "59daa43f-c9b7-40ef-9e6d-aaa59528db20",
          "_cropName" : "hard white spring wheat",
          "DryMoisturePercentage": 0.135,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 60,
                  "PartUnitName": "pound"
              }
          ]
      },
      {
          "CropId": "480d2628-36e9-4fbf-bb01-30cc362cecc4",
          "_cropName" : "hard white winter wheat",
          "DryMoisturePercentage": 0.135,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 60,
                  "PartUnitName": "pound"
              }
          ]
      },
      {
          "CropId": "ca3fdab8-b9e5-40b5-b964-5c6b337ce0f1",
          "_cropName" : "soft red wheat",
          "DryMoisturePercentage": 0.135,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 60,
                  "PartUnitName": "pound"
              }
          ]
      },
      {
          "CropId": "f2b1bd80-67e1-4652-963a-7d84d8c1c70c",
          "_cropName" : "soft red winter wheat",
          "DryMoisturePercentage": 0.135,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 60,
                  "PartUnitName": "pound"
              }
          ]
      },
      {
          "CropId": "d34e7921-ba6d-4205-a657-544f87ab41d8",
          "_cropName" : "soft white spring wheat",
          "DryMoisturePercentage": 0.135,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 60,
                  "PartUnitName": "pound"
              }
          ]
      },
      {
          "CropId": "12dd8cff-0296-49c5-9de4-3c0fe62ac794",
          "_cropName" : "soft white winter wheat",
          "DryMoisturePercentage": 0.135,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 60,
                  "PartUnitName": "pound"
              }
          ]
      },
      {
          "CropId": "222732ea-1b45-461b-a304-86ec74510a07",
          "_cropName" : "sudan grass",
          "DryMoisturePercentage": null,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 28,
                  "PartUnitName": "pound"
              }
          ]
      },
      {
          "CropId": "5e9b758a-c409-4eec-823f-5d8f700fadb6",
          "_cropName" : "vetch",
          "DryMoisturePercentage": null,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 60,
                  "PartUnitName": "pound"
              }
          ]
      },
	  {
          "CropId": "79227b6f-bf42-418a-be5e-bdf0bae0f292",
          "_cropName" : "rice",
          "DryMoisturePercentage": 0.12,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 45,
                  "PartUnitName": "pound"
              }
          ]
      },
	  {
          "CropId": "94796dca-7386-40e5-8137-0ae8c23bb59e",
          "_cropName" : "hybrid rice",
          "DryMoisturePercentage": 0.12,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 45,
                  "PartUnitName": "pound"
              }
          ]
      },
	  {
          "CropId": "dc023491-6dde-4cb1-be1d-c70eac90d93d",
          "_cropName" : "wild rice",
          "DryMoisturePercentage": 0.12,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 45,
                  "PartUnitName": "pound"
              }
          ]
      },
	  {
          "CropId": "dfe44d8d-a1e1-4c4b-8a60-3b7634e31fe9",
          "_cropName" : "zucchini squash",
          "DryMoisturePercentage": null,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 40,
                  "PartUnitName": "pound"
              },
              {
                  "PackageUnitName": "half bushel",
                  "ConversionFactor": 20,
                  "PartUnitName": "pound"
              }			
          ]
      },
	  {
          "CropId": "2c1d8c79-7064-4676-a753-f5f37643fede",
          "_cropName" : "green zucchini squash",
          "DryMoisturePercentage": null,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 40,
                  "PartUnitName": "pound"
              },
              {
                  "PackageUnitName": "half bushel",
                  "ConversionFactor": 20,
                  "PartUnitName": "pound"
              }			
          ]
      },
	  {
          "CropId": "f54c1d22-9648-42bd-ba71-5ca6f6c456b6",
          "_cropName" : "grey zucchini squash",
          "DryMoisturePercentage": null,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 40,
                  "PartUnitName": "pound"
              },
              {
                  "PackageUnitName": "half bushel",
                  "ConversionFactor": 20,
                  "PartUnitName": "pound"
              }			
          ]
      },
	  {
          "CropId": "a896b784-a0a3-43ce-a505-c1537d45c27e",
          "_cropName" : "yellow zucchini squash",
          "DryMoisturePercentage": null,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 40,
                  "PartUnitName": "pound"
              },
              {
                  "PackageUnitName": "half bushel",
                  "ConversionFactor": 20,
                  "PartUnitName": "pound"
              }			
          ]
      },
	  {
          "CropId": "B50ABDFE-FD59-4071-B3F4-A2E100FA3B57",
          "_cropName" : "field peas",
          "DryMoisturePercentage": null,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 25,
                  "PartUnitName": "pound"
              }		
          ]
      },
	  {
          "CropId": "3f251dde-83c4-4ca4-b48e-7e76fac3521a",
          "_cropName" : "lentils",
          "DryMoisturePercentage": null,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 60,
                  "PartUnitName": "pound"
              }		
          ]
      },
	  {
          "CropId": "951c95a8-00cf-49ba-814b-0b2c36903858",
          "_cropName" : "canola",
          "DryMoisturePercentage": null,
          "PackageUnits": [
              {
                  "PackageUnitName": "bushel",
                  "ConversionFactor": 50,
                  "PartUnitName": "pound"
              }		
          ]
      }
  ]
}