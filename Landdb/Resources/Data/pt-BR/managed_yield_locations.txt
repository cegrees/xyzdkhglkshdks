{
    "Locations" : [
        {
            "Id" : "f248f013-6377-4d1a-9da9-e22b78ee4ce4",
            "Name" : "GreenStar"
        },
        {
            "Id" : "e8c7d6e2-dca5-4872-9dc1-6f1e3b12b0ee",
            "Name" : "CNH"
        },
        {
            "Id" : "aa5f9cf3-86fa-4b5f-9bf6-f9d27a071602",
            "Name" : "Avery"
        },
        {
            "Id" : "fe8765b3-e2ee-458b-8142-f664760c4cc9",
            "Name" : "Digi-Star"
        },
        {
            "Id" : "d9768223-c2b4-4dfa-ba82-7730336700a5",
            "Name" : "Rice-Lake"
        },
        {
            "Id" : "9d9b5a65-8d93-4e94-9dc1-ebd09a7c05ff",
            "Name" : "R&R"
        }
    ]
}