﻿using AgC.UnitConversion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Landdb.Resources;

namespace Landdb.ReportModels.Invoice
{
    public class InvoiceData
    {
        public InvoiceData()
        {
            Products = new List<InvoiceProductLineItem>();
        }

        public string Vendor { get; set; }
        public DateTime InvoiceDate { get; set; }
        public int CropYear { get; set; }
        public string InvoiceNumber { get; set; }
        public string Notes { get; set; }
        public string DataSource { get; set; }
        public List<InvoiceProductLineItem> Products { get; set; }


        public class InvoiceProductLineItem
        {
            public string ProductName { get; set; }
            public string RegistrationNumber { get; set; }
            public string Manufacturer { get; set; }
            public double TotalQuantity { get; set; }
            public string TotalUnit { get; set; }
            public string TotalDisplay { get { return string.Format("{0} {1}", TotalQuantity.ToString("N2"), UnitFactory.GetUnitByName(TotalUnit).AbbreviatedDisplay); } }
            public double TotalPrice { get; set; }
            public double PricePer { get; set; }
            public string PricePerUnit { get; set; }
            public string PricePerDisplay { get { return string.Format(Strings.Per_Format_Text, PricePer.ToString("C"), UnitFactory.GetUnitByName(PricePerUnit).AbbreviatedDisplay); } }
        }
    }
}
