﻿using AgC.UnitConversion;
using Landdb.Client.Services.Fertilizer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ReportModels.Plan
{
    public class PlanData
    {

        public PlanData()
        {
            Products = new List<PlanProducts>();
            CropZones = new List<IncludedPlanCropZones>();
        }

        //Budget Items//
        public double Rent { get; set; }
        public double Equipment { get; set; }
        public double Insurance { get; set; }
        public double Labor { get; set; }
        public double Repairs { get; set; }
        public double Taxes { get; set; }
        public double ReturnToMgt { get; set; }
        //
        
        //Details
        public double EstimatedArea { get; set; }
        public List<PlanProducts> Products { get; set; }
        public List<IncludedPlanCropZones> CropZones { get; set; }
        public string PlanName { get; set; }
        public int CropYear { get; set; }
        public string DataSourceName { get; set; }
        public string CropDisplay { get; set; }
        public FertilizerFormulationModel TotalFertilizer { get; set; }
        public string Notes { get; set; }
        //

        //Revenue//
        public double YieldPerArea { get; set; }
        public double PricePerUnit { get; set; }
        public double FSAPayment { get; set; }
        public double CropShare { get; set; }
        //

        //CalculatedFields
        public double InputCost { get; set; }
        public double Expense { get; set; }
        public double TotalRevenue { get; set; }
        public double TotalExpense { get; set; }
        public double TotalFixedCost { get; set; }
        public double NetRevenue { get; set; }
        public double RevenuePerArea { get; set; }
        public double ExpensePerArea { get; set; }
        public double VariableCostPerArea { get; set; }
        //

        public class PlanProducts
        {
            public string PlanName { get; set; }
            public int CropYear { get; set; }
            public string DataSourceName { get; set; }
            public string CropDisplay { get; set; }
            public string ProductName { get; set; }
            public string Manufacturer { get; set; }
            public double Rate { get; set; }
            public string RateUnit { get; set; }
            public double TotalProduct { get; set; }
            public string TotalProductUnit { get; set; }
            public int ApplicationCount { get; set; }
            public double PercentApplied { get; set; }
            public string TimingEvent { get; set; }
            public int TimingEventOrder { get; set; }
            public string TimingEventTag { get; set; }
            public string RateDisplay { get { return string.Format("{0} {1}", Rate.ToString("N2"), UnitFactory.GetUnitByName(RateUnit).AbbreviatedDisplay); } }
            public string TotalDisplay { get { return string.Format("{0} {1}", TotalProduct.ToString("N2"), UnitFactory.GetUnitByName(TotalProductUnit).AbbreviatedDisplay); } }
            public double TotalCost { get; set; }
            public double CostPerArea { get; set; }
            public string CostPerAreaDisplay { get { return string.Format("{0} /{1}", CostPerArea.ToString("C2"), AreaUnit.AbbreviatedDisplay); } }
            public double Area { get; set; }
            public IUnit AreaUnit { get; set; }
            public DateTime? TargetDate { get; set; }
        }

        public class IncludedPlanCropZones
        {
            public string Farm { get; set; }
            public string Field { get; set; }
            public string CropZone { get; set; }
            public double Area { get; set; }
        }
    }

    
}
