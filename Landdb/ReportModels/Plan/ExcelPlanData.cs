﻿using AgC.UnitConversion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ReportModels.Plan
{
    public class ExcelPlanData
    {
        //Budget Items//
        public decimal? Rent { get; set; }
        public decimal? Equipment { get; set; }
        public decimal? Insurance { get; set; }
        public decimal? Labor { get; set; }
        public decimal? Repairs { get; set; }
        public decimal? Taxes { get; set; }
        public decimal? ReturnToMgt { get; set; }
        //

        //Details
        public decimal? EstimatedArea { get; set; }
        public CropPlanId PlanId { get; set; }
        public string PlanName { get; set; }
        public int CropYear { get; set; }
        public string Notes { get; set; }
        //

        //Revenue//
        public decimal? YieldPerArea { get; set; }
        public decimal? PricePerUnit { get; set; }
        public decimal? FSAPayment { get; set; }
        public decimal? CropShare { get; set; }
        //

        //CalculatedFields
        //public decimal? InputCost { get; set; }
        //public decimal? Expense { get; set; }
        //public decimal? TotalRevenue { get; set; }
        //public decimal? TotalExpense { get; set; }
        //public decimal? NetRevenue { get; set; }
        //public decimal? RevenuePerArea { get; set; }
        //public decimal? ExpensePerArea { get; set; }
    }

    public class DetailProducts
    {
        //Product
        public CropPlanId PlanId { get; set; }
        public string PlanName { get; set; }
        public int CropYear { get; set; }
        public string Crop { get; set; }
        public Guid ProductId { get; set; }
        public string ProductName { get; set; }
        public decimal? Rate { get; set; }
        public string RateUnit { get; set; }
        public decimal? TotalProduct { get; set; }
        public string TotalProductUnit { get; set; }
        public decimal? TotalCost { get; set; }
        public decimal? CostPerArea { get; set; }
        public int ApplicationCount { get; set; }
        public decimal? PercentApplied { get; set; }
        public string TimingEvent { get; set; }
        public int TimingEventOrder { get; set; }
        public string TimingEventTag { get; set; }
        public DateTime? TargetDate { get; set; }
        //Crop Zone
        public Guid FarmId { get; set; }
        public string Farm { get; set; }
        public Guid FieldId { get; set; }
        public string Field { get; set; }
        public Guid CropZoneId { get; set; }
        public string CropZone { get; set; }
        public decimal? Area { get; set; }
        public string Tags { get; set; }
    }
}
