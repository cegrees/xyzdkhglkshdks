﻿using AgC.UnitConversion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ReportModels.Plan
{
    public class ProductSummaryData
    {
        public ProductSummaryData()
        {

        }

        public string CropYear { get; set; }
        public string ReportName { get; set; }
        public ProductId ProductId { get; set; }
        public string ProductName { get; set; }
        public string DocumentName { get; set; }
        public string DataSourceName { get; set; }
        public string TargetDateDisplay { get; set; }
        public decimal TotalProductValue { get; set; }
        public IUnit TotalProductUnit { get; set; }
        public string TotalProduct { get { return string.Format("{0} {1}", TotalProductValue.ToString("N2"), TotalProductUnit.AbbreviatedDisplay); } }
        public decimal CostPerArea { get; set; }
        public decimal TotalCost { get; set; }
        public string TimingEvent { get; set; }
        public decimal TotalArea { get; set; }
    }
}
