﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ReportModels.Plan
{
    public class PlanSummaryCostBreakOut
    {
        public string DatasourceName { get; set; }
        public int CropYear { get; set; }

        public CropPlanId PlanId { get; set; }
        public string PlanName { get; set; }
        
        public FarmId FarmId { get; set; }
        public string Farm { get; set; }
        public FieldId FieldId { get; set; }
        public string Field { get; set; }
        public CropZoneId CropZoneId { get; set; }
        public string CropZone { get; set; }
        public string LandOwner { get; set; }
        public ContractId RentContractId { get; set; }
        public string ContractName { get; set; }
        public string Contract { get; set; }
        public string County { get; set; }
        public string Crop { get; set; }
        public decimal Area { get; set; }
        public decimal GrowerShareCost { get; set; }
        public decimal GrowerShareCostPerArea { get; set; }
        public decimal LandOwnerShareTotalCost { get; set; }
        public decimal LandOwnerShareCostPerArea { get; set; }
        public decimal CropZonePlanFixedCost { get; set; }
        public decimal CropZonePlanVariableCost { get; set; }
        public double CropZonePlannedYieldTotal { get; set; }
        public double CropZonePlannedYieldPricePerUnit { get; set; }
        public double CropZoneTotalFSAPayment { get; set; }
    }
}
