﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ReportModels.ExcelDump
{
    public class ExcelDumpData
    {
        public ExcelDumpData()
        {
            AppliedProducts = new List<AppliedProductDumpData>();
            WorkOrderProducts = new List<WorkOrderProductDumpData>();
            Fields = new List<FieldLineItemData>();
            Plans = new List<PlannedProductDumpData>();
            Loads = new List<CropZoneYieldItemData>();
            SourceDocuments = new List<SourceDocumentItemData>();
            Invoices = new List<InvoiceItemData>();

            StorageToSaleItems = new List<StorageToSale>();
            FieldToSaleItems = new List<FieldToSale>();
            StorageToStorageItems = new List<StorageToStorage>();
        }

        public List<AppliedProductDumpData> AppliedProducts;
        public List<WorkOrderProductDumpData> WorkOrderProducts;
        public List<FieldLineItemData> Fields;
        public List<PlannedProductDumpData> Plans;
        public List<CropZoneYieldItemData> Loads;
        public List<StorageToStorage> StorageToStorageItems;
        public List<FieldToSale> FieldToSaleItems;
        public List<StorageToSale> StorageToSaleItems;
        public List<SourceDocumentItemData> SourceDocuments;
        public List<InvoiceItemData> Invoices;

        public class AppliedProductDumpData
        {
            public Guid ClientId { get; set; }
            public Guid FarmId { get; set; }
            public string FarmName { get; set; }
            public Guid FieldId { get; set; }
            public string FieldName { get; set; }
            public Guid CropZoneId { get; set; }
            public string CropZoneName { get; set; }
            public string Tags { get; set; }
            public Guid CropId { get; set; }
            public string Crop { get; set; }
            public int CropYear { get; set; }
            public Guid ApplicationId { get; set; }
            public string ApplicationName { get; set; }
            public string StartDate { get; set; }
            public string EndDate { get; set; }
            public string Duration { get; set; }
            public Guid ProductId { get; set; }
            public string Product { get; set; }
            public string Type { get; set; }
            public string Category { get; set; }
            public string Manufacturer { get; set; }
            public string RegNumber { get; set; }
            public string ApplicationMethod { get; set; }
            public decimal RateValue { get; set; }
            public string RateUnit { get; set; }
            public decimal CropZoneArea { get; set; }
            public decimal PercentOfCropZoneArea { get; set; }
            public decimal AreaApplied { get; set; }
            public decimal TotalProductQty { get; set; }
            public string TotalProductUnit { get; set; }
            public decimal TotalAverageCost { get; set; }
            public decimal TotalInvoiceCost { get; set; }
            public decimal TotalSpecificCost { get; set; }
            public decimal CostPerCropZoneArea { get; set; }
            public decimal CostPerAppliedArea { get; set; }
            public decimal AverageCostPerTotalProductUnit { get; set; }
            public string AverageTotalProductUnit { get; set; }
            //< add in the AverageCostPerTotalProductUnit and TotalProductUnit>
            public decimal InvoiceCostPerArea { get; set; }
            public decimal SpecificCostPerArea { get; set; }
            public decimal GrowerSharePercent { get; set; }
            public decimal GrowerShareQty { get; set; }
            public decimal OwnerShareQty { get; set; }
            public string ProductShareUnit { get; set; }
            public decimal GrowerCost { get; set; }
            public decimal OwnerCost { get; set; }
            public string TimingEvent { get; set; }
            public int TimingEventOrder { get; set; }
            public Guid AuthorizerId { get; set; }
            public string Authorizer { get; set; }
            public DateTime AuthorizedDate { get; set; }
            public string Applicators { get; set; }
            public decimal Temp { get; set; }
            public decimal Wind { get; set; }
            public string Direction { get; set; }
            public decimal Humidity { get; set; }
            public string SkyCondition { get; set; }
            public string SoilCondition { get; set; }
            public bool Rup { get; set; }
            public string Phi { get; set; }
            public string Rei { get; set; }
            public string PhiDate { get; set; }
            public string ReiDate { get; set; }
            public string TargetPest { get; set; }
            public decimal N { get; set; }
            public decimal P { get; set; }
            public decimal K { get; set; }
            public decimal Ca { get; set; }
            public decimal Mg { get; set; }
            public decimal S { get; set; }
            public decimal B { get; set; }
            public decimal Cl { get; set; }
            public decimal Cu { get; set; }
            public decimal Fe { get; set; }
            public decimal Mn { get; set; }
            public decimal Mo { get; set; }
            public decimal Zn { get; set; }
            public string ActiveIngredient { get; set; }
        }

            public class WorkOrderProductDumpData
        {
            public Guid ClientId { get; set; }
            public Guid FarmId { get; set; }
            public string FarmName { get; set; }
            public Guid FieldId { get; set; }
            public string FieldName { get; set; }   
            public Guid CropZoneId { get; set; }
            public string CropZoneName { get; set; }
            //public string Tags { get; set; }
            public Guid CropId { get; set; }
            public string Crop { get; set; }
            public int CropYear { get; set; }
            public Guid WorkOrderId { get; set; }
            public string WorkOrderName { get; set; }
            public string StartDate { get; set; }
            public Guid ProductId { get; set; }
            public string Product { get; set; }
            public string Type { get; set; }
            public string Category { get; set; }
            public string Manufacturer { get; set; }
            public string RegNumber { get; set; }
            public string ApplicationMethod { get; set; }
            public decimal RateValue { get; set; }
            public string RateUnit { get; set; }
            public decimal AreaApplied { get; set; }
            public decimal CropZoneArea { get; set; }
            public decimal PercentOfCropZoneArea { get; set; }
            public decimal CostPerAppliedArea { get; set; }
            public decimal CostPerCropZoneArea { get; set; }
            public decimal TotalProductQty { get; set; }
            public string TotalProductUnit { get; set; }
            public decimal TotalAverageCost { get; set; }
            //public decimal TotalInvoiceCost { get; set; }
            public decimal TotalSpecificCost { get; set; }
            //public decimal InvoiceCostPerArea { get; set; }
            public decimal SpecificCostPerArea { get; set; }
            public decimal GrowerSharePercent { get; set; }
            public decimal GrowerShareQty { get; set; }
            public decimal OwnerShareQty { get; set; }
            //public string ProductShareUnit { get; set; }
            public decimal GrowerCost { get; set; }
            public decimal OwnerCost { get; set; }
            public string TimingEvent { get; set; }
            public int TimingEventOrder { get; set; }
            public Guid AuthorizerId { get; set; }
            public string Authorizer { get; set; }
            public DateTime AuthorizedDate { get; set; }
            //public string Applicators { get; set; }
            //public bool Rup { get; set; }
            public string Phi { get; set; }
            public string Rei { get; set; }
            public string PhiDate { get; set; }
            public string ReiDate { get; set; }
            public string TargetPest { get; set; }
            public decimal N { get; set; }
            public decimal P { get; set; }
            public decimal K { get; set; }
            public decimal Ca { get; set; }
            public decimal Mg { get; set; }
            public decimal S { get; set; }
            public decimal B { get; set; }
            public decimal Cl { get; set; }
            public decimal Cu { get; set; }
            public decimal Fe { get; set; }
            public decimal Mn { get; set; }
            public decimal Mo { get; set; }
            public decimal Zn { get; set; }
            public string ActiveIngredient { get; set; }
        }
        public class PlannedProductDumpData
        {
            //public Guid ClientId { get; set; }
            //public Guid FarmId { get; set; }
            //public string FarmName { get; set; }
            //public Guid FieldId { get; set; }
            //public string FieldName { get; set; }
            //public Guid CropZoneId { get; set; }
            //public string CropZoneName { get; set; }
            public Guid CropId { get; set; }
            public string Crop { get; set; }
            public int CropYear { get; set; }
            public Guid PlanId { get; set; }
            public string PlanName { get; set; }
            //public decimal AnnualCost { get; set; }
            //public decimal Revenue { get; set; }
            public Guid ProductId { get; set; }
            public string Product { get; set; }
            public string Type { get; set; }
            public string Category { get; set; }
            public string Manufacturer { get; set; }
            public string RegNumber { get; set; }
            public decimal RateValue { get; set; }
            public string RateUnit { get; set; }
            public decimal Area { get; set; }
            public decimal CostPerArea { get; set; }
            public decimal TotalProductQty { get; set; }
            public string TotalProductUnit { get; set; }
            public decimal? TotalSpecificCost { get; set; }
            public bool Rup { get; set; }
            public string TimingEvent { get; set; }
            public int TimingEventOrder { get; set; }
            public string TimingEventTag { get; set; }
            public DateTime? TargetDate { get; set; }
        }

        public class FieldLineItemData
        {
            public Guid FarmId { get; set; }
            public string Farm { get; set; }
            public Guid FieldId { get; set; }
            public string Field { get; set; }
            public decimal FieldArea { get; set; }
            public decimal FieldBoundaryArea { get; set; }
            public string CustomFieldId { get; set; }
            public string FieldCenterLatLong { get; set; }
            public int CropYear { get; set; }
            public string LandOwner { get; set; }
            public string RentType { get; set; }
            public decimal LandOwnerPercent { get; set; }
            public decimal TenantPercent { get; set; }
            public string State { get; set; }
            public string County { get; set; }
            public string FSAFarm { get; set; }
            public string FSATract { get; set; }
            public string FSAField { get; set; }
            public decimal FSAArea { get; set; }
            public string CluId { get; set; }
            public string Meridian { get; set; }
            public string Township { get; set; }
            public string Range { get; set; }
            public string Section { get; set; }
            public string TaxId { get; set; }
            public string ShortDesc { get; set; }
            public string LegalDesc { get; set; }
            public decimal SlopePercent { get; set; }
            public decimal SlopeLength { get; set; }
            public string SoilTexture { get; set; }
            public bool? Irrigated { get; set; }
            public string WaterSource { get; set; }
            public string WaterOrigin { get; set; }
            public string IrrigationSystem { get; set; }
            public double? PumpPressure { get; set; }
            public double? PumpLift { get; set; }
            public string PumpEnergySource { get; set; }
            public string PermitId { get; set; }
            public string SiteId { get; set; }
            public string FieldNotes { get; set; }
            public Guid CropZoneId { get; set; }
            public string CropZone { get; set; }
            public string Tags { get; set; }
            public string Crop { get; set; }
            public string CropNotes { get; set; }
            public string CenterLatLong { get; set; }
            public decimal CzArea { get; set; }
            public string SeedLotId { get; set; }
            public string SeedSource { get; set; }
            public string InsuranceId { get; set; }
            public string CropZoneSiteId { get; set; }
            public string CropZonePermitId { get; set; }
            public string ContractId { get; set; }
            public decimal N { get; set; }
            public decimal P { get; set; }
            public decimal K { get; set; }
            public decimal Ca { get; set; }
            public decimal Mg { get; set; }
            public decimal S { get; set; }
            public decimal B { get; set; }
            public decimal Cl { get; set; }
            public decimal Cu { get; set; }
            public decimal Fe { get; set; }
            public decimal Mn { get; set; }
            public decimal Mo { get; set; }
            public decimal Zn { get; set; }
        }

        public class CropZoneYieldItemData
        {
            public Guid CropZoneId { get; set; }
            public Guid LoadId { get; set; }
            public DateTime LoadDate { get; set; }
            public string LoadNumber { get; set; }

            //new stuff..
            public string Farm { get; set; }
            public string Field { get; set; }
            public string CropZone { get; set; }
            public string Crop { get; set; }
            public decimal CropZoneArea { get; set; }
            //

            public string DestinationName { get; set; }
            public string DestinationType { get; set; }
            public Guid OperationId { get; set; }
            public string OperationType { get; set; }

            public decimal WeightedFinalDeliveredQuantity { get; set; }
            public decimal WeightedFinalDryQuantity { get; set; }

            public decimal GrowerQtyShare { get; set; }
            public decimal LandOwnerQtyShare { get; set; }
            public string FinalUnit { get; set; }
            public decimal Gross { get; set; }
            public decimal Tare { get; set; }
            public decimal Net { get; set; }
            public string Unit { get; set; }
            public Guid DriverId { get; set; }
            public string DriverName { get; set; }
            public Guid TruckId { get; set; }
            public string TruckName { get; set; }
            public string Notes { get; set; }
            //public decimal DeliveredQty { get { return TotalLoadQty + MoistureShrinkQty + ShrinkAdjustmentQty + HandlingShrinkQty; } }
            //public decimal TotalLoadQty { get; set; }
            //public string TotalQtyUnit { get; set; }
            public decimal InitialMoisturePercent { get; set; }
            public decimal MoistureShrinkPercent { get; set; }
            public decimal MoistureShrinkQty { get; set; }
            public decimal TargetMoisturePercent { get; set; }
            public decimal ShrinkAdjustmentPercent { get; set; }
            public decimal ShrinkAdjustmentQty { get; set; }
            public decimal HandlingShrinkPercent { get; set; }
            public decimal HandlingShrinkQty { get; set; }

            //new stuff
            public decimal? LargeSquareBaleCount { get; set; }
            public decimal? RoundBaleCount { get; set; }
            public decimal? SquareBaleCount { get; set; }
            public decimal? BillofLadingCount { get; set; }
            public decimal? BoxCount { get; set; }
            public decimal? LintWeight { get; set; }
            public string LintWeightUnit { get; set; }
            public decimal? SeedWeight { get; set; }
            public string SeedWeightUnit { get; set; }
            public decimal? MiniModuleCount { get; set; }
            public decimal? ModuleCount { get; set; }
            public decimal? DamagePercent { get; set; }
            public decimal? ForiegnMaterialPercent { get; set; }
            public decimal? PlumpPercent { get; set; }
            public decimal? ProteinPercent { get; set; }
            public decimal? SLMPercent { get; set; }
            public decimal? SugarPercent { get; set; }
            public decimal? TestWeight { get; set; }
            public string TestWeightUnit { get; set; }
            //     
        }

        public class StorageToStorage
        {
            public Guid LoadId { get; set; }
            public DateTime LoadDate { get; set; }
            public string LoadNumber { get; set; }
            public string Crop { get; set; }
            public string StorageLocation { get; set; }
            public string DestinationName { get; set; }
            public string DestinationType { get; set; }
            public string OperationType { get; set; }
            public decimal WeightedTotalQuantity { get; set; }
            public decimal TotalQuantity { get; set; }
            public string TotalUnit { get; set; }
            public decimal Gross { get; set; }
            public decimal Tare { get; set; }
            public decimal Net { get; set; }
            public decimal WeightedGross { get; set; }
            public decimal WeightedTare { get; set; }
            public decimal WeightedNet { get; set; }
            public string Unit { get; set; }
            public Guid DriverId { get; set; }
            public string DriverName { get; set; }
            public Guid TruckId { get; set; }
            public string TruckName { get; set; }
            public string Notes { get; set; }
            public decimal InitialMoisturePercent { get; set; }
			public decimal TargetMoisturePercent { get; set; }
			public decimal MoistureShrinkPercent { get; set; }
            public decimal MoistureShrinkQty { get; set; }
            public decimal ShrinkAdjustmentPercent { get; set; }
            public decimal ShrinkAdjustmentQty { get; set; }
			public decimal HandlingShrinkPercent { get; set; }
			public decimal HandlingShrinkQty { get; set; }
            public decimal? LargeSquareBaleCount { get; set; }
            public decimal? RoundBaleCount { get; set; }
            public decimal? SquareBaleCount { get; set; }
            public decimal? BillofLadingCount { get; set; }
            public decimal? BoxCount { get; set; }
            public decimal? LintWeight { get; set; }
            public string LintWeightUnit { get; set; }
            public decimal? SeedWeight { get; set; }
            public string SeedWeightUnit { get; set; }
            public decimal? MiniModuleCount { get; set; }
            public decimal? ModuleCount { get; set; }
            public decimal? DamagePercent { get; set; }
            public decimal? ForiegnMaterialPercent { get; set; }
            public decimal? PlumpPercent { get; set; }
            public decimal? ProteinPercent { get; set; }
            public decimal? SLMPercent { get; set; }
            public decimal? SugarPercent { get; set; }
            public decimal? TestWeight { get; set; }
            public string TestWeightUnit { get; set; }
        }

        public class FieldToSale
        {
            public Guid CropZoneId { get; set; }
            public Guid LoadId { get; set; }
            public DateTime LoadDate { get; set; }
            public string LoadNumber { get; set; }
            public string Farm { get; set; }
            public string Field { get; set; }
            public string CropZone { get; set; }
            public string Crop { get; set; }
            public decimal CropZoneArea { get; set; }
            public string RentShares { get; set; }
            public string DestinationName { get; set; }
            public string DestinationType { get; set; }
            public Guid OperationId { get; set; }
            public string OperationType { get; set; }
            public decimal WeightedFinalDeliveredQty { get { return WeightedFinalDryQuantity + MoistureShrinkQty + ShrinkAdjustmentQty + HandlingShrinkQty; } }
            public decimal WeightedFinalDryQuantity { get; set; }
            public decimal GrowerQtyShare { get; set; }
            public decimal LandOwnerQtyShare { get; set; }
            public string FinalUnit { get; set; }
            public decimal WeightedFinalPrice { get; set; }
            public decimal Gross { get; set; }
            public decimal Tare { get; set; }
            public decimal Net { get; set; }
            public string Unit { get; set; }
            public Guid DriverId { get; set; }
            public string DriverName { get; set; }
            public Guid TruckId { get; set; }
            public string TruckName { get; set; }
            public string Notes { get; set; }
            public decimal DeliveredQty { get { return FinalQty + MoistureShrinkQty + ShrinkAdjustmentQty + HandlingShrinkQty; } }
            public string Buyer { get; set; }
            public string ContractId { get; set; }
            public string GrowerId { get; set; }
            public decimal GrossSalePrice { get; set; }
            public decimal GrossUnitPrice { get; set; }
            public decimal SalePriceAdjustment { get; set; }
            public decimal FinalUnitPrice { get; set; }
            public decimal FinalSalePrice { get; set; }
            public decimal FinalQty { get; set; }
            public string FinalQtyUnit { get; set; }

            public decimal InitialMoisturePercent { get; set; }
            public decimal MoistureShrinkPercent { get; set; }
            public decimal MoistureShrinkQty { get; set; }
            public decimal TargetMoisturePercent { get; set; }
            public decimal ShrinkAdjustmentPercent { get; set; }
            public decimal ShrinkAdjustmentQty { get; set; }
            public decimal HandlingShrinkPercent { get; set; }
            public decimal HandlingShrinkQty { get; set; }
            public decimal? LargeSquareBaleCount { get; set; }
            public decimal? RoundBaleCount { get; set; }
            public decimal? SquareBaleCount { get; set; }
            public decimal? BillofLadingCount { get; set; }
            public decimal? BoxCount { get; set; }
            public decimal? LintWeight { get; set; }
            public string LintWeightUnit { get; set; }
            public decimal? SeedWeight { get; set; }
            public string SeedWeightUnit { get; set; }
            public decimal? MiniModuleCount { get; set; }
            public decimal? ModuleCount { get; set; }
            public decimal? DamagePercent { get; set; }
            public decimal? ForiegnMaterialPercent { get; set; }
            public decimal? PlumpPercent { get; set; }
            public decimal? ProteinPercent { get; set; }
            public decimal? SLMPercent { get; set; }
            public decimal? SugarPercent { get; set; }
            public decimal? TestWeight { get; set; }
            public string TestWeightUnit { get; set; }
        }

        public class StorageToSale
        {
            public Guid LoadId { get; set; }
            public DateTime LoadDate { get; set; }
            public string LoadNumber { get; set; }
            public string Crop { get; set; }
            public string RentShares { get; set; }
            public string StorageLocation { get; set; }
            public string DestinationName { get; set; }
            public string DestinationType { get; set; }
            public string OperationType { get; set; }
            public decimal WeightedFinalDryQuantity { get; set; }

            public string FinalUnit { get; set; }
            public decimal WeightedFinalPrice { get; set; }
            public decimal Gross { get; set; }
            public decimal Tare { get; set; }
            public decimal Net { get; set; }
            public string Unit { get; set; }

            public Guid DriverId { get; set; }
            public string DriverName { get; set; }
            public Guid TruckId { get; set; }
            public string TruckName { get; set; }
            public string Notes { get; set; }

            public string Buyer { get; set; }
            public string ContractId { get; set; }
            public string GrowerId { get; set; }
            public decimal GrossSalePrice { get; set; }
            public decimal GrossUnitPrice { get; set; }
            public decimal SalePriceAdjustment { get; set; }
            public decimal FinalUnitPrice { get; set; }
            public decimal FinalSalePrice { get; set; }
            public decimal FinalQty { get; set; }
            public string FinalQtyUnit { get; set; }

            public decimal InitialMoisturePercent { get; set; }
            public decimal MoistureShrinkPercent { get; set; }
            public decimal MoistureShrinkQty { get; set; }
            public decimal TargetMoisturePercent { get; set; }
            public decimal ShrinkAdjustmentPercent { get; set; }
            public decimal ShrinkAdjustmentQty { get; set; }
            public decimal HandlingShrinkPercent { get; set; }
            public decimal HandlingShrinkQty { get; set; }
            public decimal? LargeSquareBaleCount { get; set; }
            public decimal? RoundBaleCount { get; set; }
            public decimal? SquareBaleCount { get; set; }
            public decimal? BillofLadingCount { get; set; }
            public decimal? BoxCount { get; set; }
            public decimal? LintWeight { get; set; }
            public string LintWeightUnit { get; set; }
            public decimal? SeedWeight { get; set; }
            public string SeedWeightUnit { get; set; }
            public decimal? MiniModuleCount { get; set; }
            public decimal? ModuleCount { get; set; }
            public decimal? DamagePercent { get; set; }
            public decimal? ForiegnMaterialPercent { get; set; }
            public decimal? PlumpPercent { get; set; }
            public decimal? ProteinPercent { get; set; }
            public decimal? SLMPercent { get; set; }
            public decimal? SugarPercent { get; set; }
            public decimal? TestWeight { get; set; }
            public string TestWeightUnit { get; set; }
        }

        public class SourceDocumentItemData : IComparable
        {
            public AbstractDataSourceIdentity<Guid, Guid> SourceId { get; set; }
            public string SourceType { get; set; }
            public ApplicationId ApplicationId { get; set; }



            public int CompareTo(object obj)
            {
                SourceDocumentItemData s = (SourceDocumentItemData)obj;
                return String.Compare(this.ApplicationId.ToString(), s.ApplicationId.ToString());
            }
        }

        public class InvoiceItemData
        {
            public Guid Id { get; set; }
            public string InvoiceNumber { get; set; }
            public DateTime InvoiceDate { get; set; }
            public DateTime? InvoiceDueDate { get; set; }
            public string Vendor { get; set; }
            public Guid ProductId { get; set; }
            public string Product { get; set; }
            public string Manufacturer { get; set; }
            public string ProductType { get; set; }
            public decimal TotalProductValue { get; set; }
            public string TotalProductUnit { get; set; }
            public decimal CostPerUnit { get; set; }
            public decimal TotalCost { get; set; }
            public string Notes { get; set; }
        }

        public class ExcelPlanInfoData
        {
            public CropPlanId PlanId { get; set; }
            public string PlanName { get; set; }

            //Crop Zone
            public Guid FarmId { get; set; }
            public string Farm { get; set; }
            public Guid FieldId { get; set; }
            public string Field { get; set; }
            public Guid CropZoneId { get; set; }
            public string CropZone { get; set; }
            public decimal? CzArea { get; set; }
            public string Tags { get; set; }

            //Budget Items//
            public decimal? Rent { get; set; }
            public decimal? Equipment { get; set; }
            public decimal? Insurance { get; set; }
            public decimal? Labor { get; set; }
            public decimal? Repairs { get; set; }
            public decimal? Taxes { get; set; }
            public decimal? ReturnToMgt { get; set; }
            //

            //Details
            public decimal? EstimatedArea { get; set; }
            public int CropYear { get; set; }
            public string Notes { get; set; }
            //

            //Revenue//
            public decimal? YieldPerArea { get; set; }
            public decimal? PricePerUnit { get; set; }
            public decimal? FSAPayment { get; set; }
            public decimal? CropShare { get; set; }
            //
        }
    }
}
