﻿using AgC.UnitConversion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ReportModels.REIPHI
{
    class REIPHIEntryData
    {
        public REIPHIEntryData()
        {
            CropZones = new List<CropZoneItem>();
        }

        public List<CropZoneItem> CropZones { get; set; } 

    }

    public class CropZoneItem
    {
        public CropZoneItem() { Products = new List<ProductItem>(); }

	    public string CropYear { get; set; }
        public string DateRangeDisplay { get; set; }
        public string DataSourceName { get; set; }
        public string Farm { get; set; }
        public string Field { get; set; }
        public string CropZone { get; set; }
        public string Crop { get; set; }
        public decimal Rei { get; set; }
        public DateTime ReiExpiration { get; set; }
        public decimal Phi { get; set; }
        public DateTime PhiExpiration { get; set; }
        public bool IsAllowed { get; set; }
        public string Status { get; set; }
        public List<ProductItem> Products { get; set; }
    }

    public class ProductItem 
    {
        public string Product { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal Rei { get; set; }
        public decimal Phi { get; set; }
        public DateTime ReiExpiration { get; set; }
        public DateTime PhiExpiration { get; set; }
        public decimal RateValue { get; set; }
        public IUnit RateUnit { get; set; }
        public decimal TotalValue { get; set; }
        public IUnit TotalUnit { get; set; }
        public bool ReiStatus { get; set; }
        public bool PhiStatus { get; set; }
    }
}
