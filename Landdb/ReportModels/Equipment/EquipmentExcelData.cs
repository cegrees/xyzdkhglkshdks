﻿using System;
using Landdb.ViewModel.Resources.Equipment;

namespace Landdb.ReportModels.Equipment
{
    public class EquipmentExcelData
    {
        public int CropYear { get; }
        public string Name { get; }
        public string Make { get; }
        public string Model { get; }
        public int? EquipmentYear { get; }
        public string Type { get; }
        public string CurrentStatus { get; }
        public string CurrentStatusDate { get; }
        public string SerialNumber { get; }
        public string LicenseNumber { get; }
        public string UnitId { get; }
        public double? PurchasePrice { get; }
        public string PurchaseDate { get; }
        public double? CurrentValue { get; }
        public string CurrentValueDate { get; }
        public string Notes { get; }

        public EquipmentExcelData(EquipmentSummaryViewModel equipmentSummaryViewModel)
        {
            Name = equipmentSummaryViewModel.BasicInfoViewModel.Name;
            Make = equipmentSummaryViewModel.BasicInfoViewModel.Make;
            Model = equipmentSummaryViewModel.BasicInfoViewModel.Model;
            EquipmentYear = int.TryParse(equipmentSummaryViewModel.BasicInfoViewModel.YearAsString, out int result) ? result : (int?) null;
            CurrentStatus = equipmentSummaryViewModel.BasicInfoViewModel.CurrentStatusInfo.EquipmentStatusDisplayText;
            CurrentStatusDate = equipmentSummaryViewModel.BasicInfoViewModel.CurrentStatusInfoDateDisplayText;
            Type = equipmentSummaryViewModel.BasicInfoViewModel.SelectedEquipmentType.EquipmentTypeString;
            SerialNumber = equipmentSummaryViewModel.BasicInfoViewModel.SerialNumber;
            LicenseNumber = equipmentSummaryViewModel.BasicInfoViewModel.LicenseNumber;
            UnitId = equipmentSummaryViewModel.BasicInfoViewModel.UnitId;
            Notes = equipmentSummaryViewModel.BasicInfoViewModel.Notes;
            PurchasePrice = equipmentSummaryViewModel.HistoryInfoViewModel.PurchasePrice;
            PurchaseDate = equipmentSummaryViewModel.HistoryInfoViewModel.DisplayPurchaseDate;
            CurrentValue = equipmentSummaryViewModel.HistoryInfoViewModel.CurrentValue;
            CurrentValueDate = equipmentSummaryViewModel.HistoryInfoViewModel.DisplayCurrentDate;
            CropYear = equipmentSummaryViewModel.currentCropYearId.Id;
        }
    }
}