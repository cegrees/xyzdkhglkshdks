﻿using Landdb.ViewModel.Resources.Popups.Equipment.Tabs;

namespace Landdb.ReportModels.Equipment
{
	public class ServiceItem
	{
		public string Service { get; }
		public string Cost { get; }
		public string Unit { get; }

		private ServiceItem(ServiceInfoViewModel serviceInfoViewModel)
		{
			Service = serviceInfoViewModel.ServiceName;
			Cost = serviceInfoViewModel.OperationCostAsString;
			Unit = serviceInfoViewModel.ProductUnit;
		}

		public static ServiceItem CreateServiceItem(ServiceInfoViewModel serviceInfoViewModel)
		{
			return new ServiceItem(serviceInfoViewModel);
		}
	}
}