﻿using System.Collections.Generic;
using System.Linq;
using Landdb.Infrastructure;
using Landdb.ViewModel.Resources.Equipment;

namespace Landdb.ReportModels.Equipment
{
    public class EquipmentSummaryData
    {
	    public string Name { get; }
	    public string Make { get; }
	    public string Model { get; }
	    public string Year { get; }
		public string YearMakeModel { get; }
	    public string Type { get; }
		public string CurrentStatus { get; }
		public string CurrentStatusDate { get; }
		public string CurrentStatusAndDate { get; }
	    public string SerialNumber { get; }
	    public string LicenseNumber { get; }
	    public string UnitId { get; }
	    public string Notes { get; }
	    public string PurchasePrice { get; }
	    public string PurchaseDate { get; }
		public string PurchasePriceAndDate { get; }
	    public string CurrentValue { get; }
	    public string CurrentValueDate { get; }
		public string CurrentValueAndDate { get; }
	    public bool IsSubSet { get; }
	    public string DataSourceName { get; }
		public string CropYear { get; }
		public List<StatusItem> Statuses { get; }
		public List<ServiceItem> Services { get; }

        public EquipmentSummaryData(EquipmentSummaryViewModel equipmentSummaryViewModel)
        {
	        Name = equipmentSummaryViewModel.BasicInfoViewModel.Name;
	        Make = equipmentSummaryViewModel.BasicInfoViewModel.Make;
	        Model = equipmentSummaryViewModel.BasicInfoViewModel.Model;
	        Year = equipmentSummaryViewModel.BasicInfoViewModel.YearAsString;
	        CurrentStatus = equipmentSummaryViewModel.BasicInfoViewModel.CurrentStatusInfo.EquipmentStatusDisplayText;
	        CurrentStatusDate = equipmentSummaryViewModel.BasicInfoViewModel.CurrentStatusInfoDateDisplayText;
	        CurrentStatusAndDate = $"{CurrentStatus} {CurrentStatusDate}";
	        YearMakeModel = equipmentSummaryViewModel.BasicInfoViewModel.YearMakeModelString;
	        Type = equipmentSummaryViewModel.BasicInfoViewModel.SelectedEquipmentType.EquipmentTypeString;
			SerialNumber = equipmentSummaryViewModel.BasicInfoViewModel.SerialNumber;
	        LicenseNumber = equipmentSummaryViewModel.BasicInfoViewModel.LicenseNumber;
	        UnitId = equipmentSummaryViewModel.BasicInfoViewModel.UnitId;
	        Notes = equipmentSummaryViewModel.BasicInfoViewModel.Notes;
	        PurchasePrice = equipmentSummaryViewModel.HistoryInfoViewModel.PurchasePriceAsString;
	        PurchaseDate = equipmentSummaryViewModel.HistoryInfoViewModel.DisplayPurchaseDate;
	        PurchasePriceAndDate = $"{PurchasePrice} {PurchaseDate}";
	        CurrentValue = equipmentSummaryViewModel.HistoryInfoViewModel.CurrentValueAsString;
	        CurrentValueDate = equipmentSummaryViewModel.HistoryInfoViewModel.DisplayCurrentDate;
	        CurrentValueAndDate = $"{CurrentValue} {CurrentValueDate}";
	        IsSubSet = false;
            DataSourceName = ApplicationEnvironment.CurrentDataSourceName;
	        CropYear = equipmentSummaryViewModel.currentCropYearId.Id.ToString();
			Statuses =  new List<StatusItem>();
	        equipmentSummaryViewModel.StatusInfoCollectionViewModel.Statuses
				.ForEach(statusInfoViewModel => {Statuses.Add(StatusItem.CreateStatusItem(statusInfoViewModel));});
	        Services = new List<ServiceItem>();
			equipmentSummaryViewModel.ServicesInfoCollectionViewModel.Services
				.ForEach(service => { Services.Add(ServiceItem.CreateServiceItem(service));});
        }
    }
}
