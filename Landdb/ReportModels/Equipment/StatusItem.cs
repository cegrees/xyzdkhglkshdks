﻿using Landdb.ViewModel.Resources.Popups.Equipment.Tabs;

namespace Landdb.ReportModels.Equipment
{
	public class StatusItem
	{
		public string Status { get; }
		public string StatusDate { get; }

		private StatusItem(StatusInfoViewModel statusInfoViewModel)
		{
			Status = statusInfoViewModel.EquipmentStatusDisplayText;
			StatusDate = statusInfoViewModel.DisplayStatusDate;
		}

		public static StatusItem CreateStatusItem(StatusInfoViewModel statusInfoViewModel)
		{
			return new StatusItem(statusInfoViewModel);
		}
	}
}
