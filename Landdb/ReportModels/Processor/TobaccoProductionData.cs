﻿using AgC.UnitConversion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ReportModels.Processor
{
    public class TobaccoProductionData
    {
        public TobaccoProductionData()
        {

        }

        public string CropYear { get; set; }
        public string ContractName { get; set; }
        public string ContractNumber { get; set; }
        public string ContractWeight { get; set; }
        public string DataSourceName { get; set; }
        public decimal AreaValue { get; set; }
        public IUnit AreaUnit { get; set; }
        public string AreaDisplay { get { return string.Format("{0} {1}", AreaValue.ToString("N2"), AreaUnit.AbbreviatedDisplay ); } }
        public string FarmDisplay { get; set; }
        public string FieldDisplay { get; set; }
        public string CropDisplay { get; set; }
        public string FarmFieldCz { get; set; }
        public string FSAFarm { get; set; }
        public string CountyState { get; set; }
        public string DateRangeDisplay { get; set; }
        public string Variety { get; set; }
        public string LotNumber { get; set; }
        public string SeedingDate { get; set; }
        public string Transplant { get; set; }
        public string Topping { get; set; }
        public string Harvest { get; set; }
        public List<ProcessorProduct> Products { get; set; }
    }

    //public class ProcessorProduct
    //{
    //    public string Applicators { get; set; }
    //    public DateTime StartDate { get; set; }
    //    public string EpaNumber { get; set; }
    //    public string Rei { get; set; }
    //    public string Analysis { get; set; }
    //    public double AppliedArea { get; set; }
    //    public string Product { get; set; }
    //    public string Pests { get; set; }
    //    public decimal TotalValue { get; set; }
    //    public IUnit TotalUnit { get; set; }
    //    public decimal RateValue { get; set; }
    //    public IUnit RateUnit { get; set; }
    //    public string TotalDisplay { get { return string.Format("{0} {1}", TotalValue.ToString("N2"), TotalUnit.AbbreviatedDisplay); } }
    //    public string RateDisplay { get { return string.Format("{0} {1}",RateValue.ToString("N2"), RateUnit.AbbreviatedDisplay); } }
    //}
}
