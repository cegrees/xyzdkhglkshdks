﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ReportModels.Processor
{
    public class MillerCoorsData
    {
        public string GrowerName { get; set; }
        public string IsYourFarmLessThan200Acres { get; set; }
        public string FieldLocation { get; set; }
        public string FieldSoilSampled { get; set; }
        public string PreviousCrop { get; set; }
        public double Acres { get; set; }
        public string Variety { get; set; }
        public string SeedingRate { get; set; }
        public decimal NitrogenApplied { get; set; }
        public decimal PhosphorusApplied { get; set; }
        public decimal PotassiumApplied { get; set; }
        public string Section3HerbicideApplied { get; set; }
        public string Section3HerbicideRateApplied { get; set; }
        public string Section3HerbicideDateApplied { get; set; }

        public string Section3_1HerbicideApplied { get; set; }
        public string Section3_1HerbicideRateApplied { get; set; }
        public string Section3_1HerbicideDateApplied { get; set; }

        public string Section3_2HerbicideApplied { get; set; }
        public string Section3_2HerbicideRateApplied { get; set; }
        public string Section3_2HerbicideDateApplied { get; set; }

        public string Section3_3HerbicideApplied { get; set; }
        public string Section3_3HerbicideRateApplied { get; set; }
        public string Section3_3HerbicideDateApplied { get; set; }

        public string Section4InsecticideApplied { get; set; }
        public string Section4InsecticideRateApplied { get; set; }
        public string Section4InsecticideDateApplied { get; set; }

        public string Section4_1InsecticideApplied { get; set; }
        public string Section4_1InsecticideRateApplied { get; set; }
        public string Section4_1InsecticideDateApplied { get; set; }

        public string PlantGrowthRegulatorApplied { get; set; }
        public string PlantGrowthRegulatorRateApplied { get; set; }
        public string PlantGrowthRegulatorDateApplied { get; set; }

        public string Section6FungicideApplied { get; set; }
        public string Section6FungicideRateApplied { get; set; }
        public string Section6FungicideDateApplied { get; set; }

        public string Section6_1FungicideApplied { get; set; }
        public string Section6_1FungicideRateApplied { get; set; }
        public string Section6_1FungicideDateApplied { get; set; }

        public string TillageOperation { get; set; }
        public string NumberOfTillagePasses { get; set; }

        public string IrrigationSystemType { get; set; }
        public string EstimatedWaterApplied { get; set; }
        public string WhatYearDidYouLastRenozzleYourSprinklerSystem { get; set; }
        public string TypeOfNozzleUsedOnThisPivot { get; set; }
        public string IsThereAWaterFlowMeterUsedOnThisField { get; set; }
        public string DoYouUseAWaterSchedulingProtocol { get; set; }
        public string DoesThisPivotUseAnEndGun { get; set; }
    }
}
