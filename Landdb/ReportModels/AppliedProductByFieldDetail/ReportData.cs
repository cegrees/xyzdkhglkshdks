﻿using Landdb.Client.Services.Fertilizer;
using Landdb.Properties;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ReportModels.AppliedProductByFieldDetail
{
    public class ReportData
    {
        public ReportData()
        {
            CropProtectionLineItems = new List<ReportLineItemData>();
            FertilizerLineItems = new List<ReportLineItemData>();
            SeedLineItems = new List<ReportLineItemData>();
            ServiceLineItems = new List<ReportLineItemData>();
            LineItems = new List<ReportLineItemData>();
            ActiveItems = new List<ActiveIngredientItemData>();
            CropZones = new List<IncludedCropZoneItemData>();
        }

        public List<ReportLineItemData> CropProtectionLineItems { get; set; }
        public List<ReportLineItemData> FertilizerLineItems { get; set; }
        public List<ReportLineItemData> SeedLineItems { get; set; }
        public List<ReportLineItemData> ServiceLineItems { get; set; }
        public List<ReportLineItemData> LineItems { get; set; }
        public List<ActiveIngredientItemData> ActiveItems { get; set; }

        public List<IncludedCropZoneItemData> CropZones { get; set; }

        public string DataSourceName { get; set; }
        public string AddressDisplay { get; set; }
        public string FieldDisplay { get; set; }
        public string CropDisplay { get; set; }
        public double Area { get; set; }
        public string AreaDisplay { get; set; }
        public string DateRangeDisplay { get; set; }
        public double? TotalCost { get; set; }
        public double? TotalCostPerArea { get; set; }
        public FertilizerFormulationModel TotalFertilizer { get; set; }
        public Bitmap logo { get { return Landdb.Properties.Resources.AgC_Logo; } }
        public string CostType { get; set; }
	    public string CropYear { get; set; }
        //CropProtectionLineItems.Add(new ReportLineItemData() { ApplicatorDisplay = "Andrew Ryan Coop", StartDate = "Mar. 20", EndDate = "Mar. 20", ProductDisplay = "Verdict Herbicide", RateDisplay = "5 fl oz", AreaDisplay = "2.42 ac", TotalProductDisplay = "0.09 gal", TotalCost = 5000 });
        //CropProtectionLineItems.Add(new ReportLineItemData() { ApplicatorDisplay = "Andrew Ryan Coop", StartDate = "Mar. 20", EndDate = "Mar. 20", ProductDisplay = "Traxion", RateDisplay = "24 fl oz", AreaDisplay = "2.42 ac", TotalProductDisplay = "0.45 gal", TotalCost = 1024.76m });
        //CropProtectionLineItems.Add(new ReportLineItemData() { ApplicatorDisplay = "Contoso Farms", StartDate = "Mar. 20", EndDate = "Mar. 20", ProductDisplay = "Resolve DF Herbicide", RateDisplay = "1 oz", AreaDisplay = "2.42 ac", TotalProductDisplay = "0.15 lb", TotalCost = 709.21m });
        //CropProtectionLineItems.Add(new ReportLineItemData() { ApplicatorDisplay = "Contoso Farms", StartDate = "Mar. 20", EndDate = "Mar. 20", ProductDisplay = "Atrazine 90DF", RateDisplay = "1 lb", AreaDisplay = "2.42 ac", TotalProductDisplay = "2.42 lb", TotalCost = 1234.98m });
        //CropProtectionLineItems.Add(new ReportLineItemData() { ApplicatorDisplay = "Contoso Farms", StartDate = "Mar. 20", EndDate = "Mar. 20", ProductDisplay = "Lexar", RateDisplay = "1.5 qt", AreaDisplay = "2.42 ac", TotalProductDisplay = "0.91 gal", TotalCost = 897.24m });
        //CropProtectionLineItems.Add(new ReportLineItemData() { ApplicatorDisplay = "Contoso Farms", StartDate = "Mar. 20", EndDate = "Mar. 20", ProductDisplay = "Touchdown Total", RateDisplay = "22 fl oz", AreaDisplay = "2.42 ac", TotalProductDisplay = "0.42 gal", TotalCost = 1412.88m });

        //FertilizerLineItems.Add(new ReportLineItemData() { ApplicatorDisplay = "Comstock Spraying", StartDate = "Mar. 20", EndDate = "Mar. 20", ProductDisplay = "18-46-0", RateDisplay = "200 lb", AreaDisplay = "2.42 ac", TotalProductDisplay = "0.24 tons", TotalCost = 1814.23m });
        //FertilizerLineItems.Add(new ReportLineItemData() { ApplicatorDisplay = "Comstock Spraying", StartDate = "Mar. 20", EndDate = "Mar. 20", ProductDisplay = "Magnesium 36%", RateDisplay = "50 lb", AreaDisplay = "2.42 ac", TotalProductDisplay = "121.15 lb", TotalCost = 881.34m });
        //FertilizerLineItems.Add(new ReportLineItemData() { ApplicatorDisplay = "Comstock Spraying", StartDate = "Mar. 20", EndDate = "Mar. 20", ProductDisplay = "0-0-60", RateDisplay = "100 lb", AreaDisplay = "2.42 ac", TotalProductDisplay = "0.12 tons", TotalCost = 2061.03m });
        //FertilizerLineItems.Add(new ReportLineItemData() { ApplicatorDisplay = "Comstock Spraying", StartDate = "Mar. 20", EndDate = "Mar. 20", ProductDisplay = "82-0-0", RateDisplay = "157 lb", AreaDisplay = "2.42 ac", TotalProductDisplay = "0.21 tons", TotalCost = 1466.29m });

        //SeedLineItems.Add(new ReportLineItemData() { ApplicatorDisplay = "Contoso Farms", StartDate = "Mar. 20", EndDate = "Mar. 20", ProductDisplay = "Annual Ryegrass", RateDisplay = "15 lb", AreaDisplay = "2.42 ac", TotalProductDisplay = "36.34 lb", TotalCost = 477.12m });
        //SeedLineItems.Add(new ReportLineItemData() { ApplicatorDisplay = "Contoso Farms", StartDate = "Mar. 20", EndDate = "Mar. 20", ProductDisplay = "33N58", RateDisplay = "31,000 kernel", AreaDisplay = "3.07 ac", TotalProductDisplay = "1.19 bags", TotalCost = 501.84m });

        //ServiceLineItems.Add(new ReportLineItemData() { ApplicatorDisplay = "Contoso Farms", StartDate = "Mar. 20", EndDate = "Mar. 20", ProductDisplay = "Planting", RateDisplay = "1 ac", AreaDisplay = "2.42 ac", TotalProductDisplay = "2.42 ac", TotalCost = 307.2m });
        //ServiceLineItems.Add(new ReportLineItemData() { ApplicatorDisplay = "Contoso Farms", StartDate = "Mar. 20", EndDate = "Mar. 20", ProductDisplay = "Spraying", RateDisplay = "1 ac", AreaDisplay = "2.42 ac", TotalProductDisplay = "2.42 ac", TotalCost = 277.14m });
        //ServiceLineItems.Add(new ReportLineItemData() { ApplicatorDisplay = "Contoso Farms", StartDate = "Mar. 20", EndDate = "Mar. 20", ProductDisplay = "Spreading", RateDisplay = "1 ac", AreaDisplay = "2.42 ac", TotalProductDisplay = "2.42 ac", TotalCost = 289.6m });
        //ServiceLineItems.Add(new ReportLineItemData() { ApplicatorDisplay = "Contoso Farms", StartDate = "Mar. 20", EndDate = "Mar. 20", ProductDisplay = "Sidedress 82 N", RateDisplay = "1 ac", AreaDisplay = "2.42 ac", TotalProductDisplay = "2.42 ac", TotalCost = 289.6m });

        //DataSourceName = "Contoso Farms";
    }

    public class ReportLineItemData
    {
        public string ApplicatorDisplay { get; set; }
        public double ApplicationArea { get; set; }
        public string StartDate { get; set; }
        public DateTime StartDateTime { get; set; }
        public string EndDate { get; set; }
        public string ProductDisplay { get; set; }
        public string EPANumber { get; set; }
        public string RateDisplay { get; set; }
        public double Rate { get; set; }
        public string RateUnit { get; set; }
        public string AreaDisplay { get; set; }
        public string TotalProductDisplay { get; set; }
        public double TotalProduct { get; set; }
        public string TotalUnit { get; set; }
        public double? TotalCost { get; set; }
        public string REI { get; set; }
        public string REIDate { get; set; }
        public string PHI { get; set; }
        public string PHIDate { get; set; }
        public string RUP { get; set; }
        public bool UserEditedReiPhi { get; set; }
        public double CropZoneArea { get; set; }
        public string FertilizerAnalysis { get; set; }
        public string Manufacturer { get; set; }
        public string AppMethod { get; set; }
        public string Active { get; set; }
        public bool IncludeCost { get; set; }
        public int RowCount { get; set; }
        public string IsAverage { get; set; }
        public string Pest { get; set; }
        public string ProductType { get; set; }
    }

    public class ActiveIngredientItemData
    {
        public Guid ActiveIngredientID { get; set; }
        public string Name { get; set; }
        public double TotalLoad { get; set; }
    }

    public class IncludedCropZoneItemData
    {
        public string FarmName { get; set; }
        public string FieldName { get; set; }
        public string CropZoneName { get; set; }
        public double Area { get; set; }
        public string DisplayArea { get; set; }
    }
}
