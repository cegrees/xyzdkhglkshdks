﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ReportModels.Scouting
{
    public class ScoutingData
    {
        public string DatasourceName { get; set; }
        public int CropYear { get; set; }
        public string DocumentName { get; set; }
        public DateTime ScoutingDate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string SkyCondition { get; set; }
        public string SoilCondition { get; set; }
        public string WindSpeed { get; set; }
        public string WindDirection { get; set; }
        public string Temperature { get; set; }
        public decimal? Humidity { get; set; }
        public Bitmap MapImage { get; set; }
        public string Notes { get; set; }
        public List<Observation> Observations { get; set; }

    }

    public class Observation
    {
        public string ObservationName { get; set; }
        public int ObservationNumber { get; set; }
        public string Notes { get; set; }
        public GeoPositionDetails GeoPosition { get; set; }

        public List<ObservationProperty> ObservationProperties { get; set; }
    }

    public class ObservationProperty
    {
        public string Name { get; set; }
        public string FullName { get; set; }
        public string DisplayValue { get; set; }
        public string Value { get; set; }
        public string Unit { get; set; }
        public GeoPositionDetails GeoPosition { get; set; }
        public string Notes { get; set; }
    }

    public class GeoPositionDetails
    {
        public string Accuracy { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public DateTime? GeoTimestamp { get; set; }
        public DateTime? SysTimestamp { get; set; }
    }
}
