﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ReportModels.CropZone
{
    public class FSAData
    {
        public FSAData()
        {
            FSAFarms = new List<FSAFarmItem>();
            CropsSummary = new List<CropInfo>();
        }


        public string FSAFarmNumber { get; set; }
        public List<FSAFarmItem> FSAFarms { get; set; }

        public List<CropInfo> CropsSummary { get; set; }
    }

    public class FSAFarmItem
    {
        public string DataSourceName { get; set; }
        public string DisplayName { get; set; }
        public int CropYear { get; set; }
        public string FSAFarmNumber { get; set; }
        public string FSATractNumber { get; set; }
        public string FSAFieldNumber { get; set; }
        public decimal FSAArea { get; set; }
        public decimal TurnRow { get; set; }
        public string County { get; set; }
        public Bitmap Map { get; set; }
        public Image Map2
        {
            get
            {
                if (Map != null)
                {
                    using (MemoryStream stream = new MemoryStream())
                    {
                        Map.Save(stream, ImageFormat.Png);
                        return Image.FromStream(stream);
                    }
                }
                else
                {
                    return null;
                }
            }
        }
        public string Meridian { get; set; }
        public string Township { get; set; }
        public string Section { get; set; }
        public string Range { get; set; }
        public string CashRent { get; set; }
        public string CropShare { get; set; }
        public decimal LandLordPercent { get; set; }
        public decimal TenantPercent { get; set; }
        public string CropInsuranceId { get; set; }
        public List<VarietyItem> Varieties { get; set; }
        /// <summary>
        /// /////////////////////////////////////////////
        /// added for fsa report
        /// </summary>
        public string Variety { get; set; }
        public decimal ReportedArea { get; set; }
        public decimal PlantingArea { get; set; }
        public string PlantingDate { get; set; }
        public string FarmField { get; set; }
        public string Crop { get; set; }
        ///////////////////////////////////////////////

        public bool? Irrigated { get; set; }

        public override string ToString()
        {
            return this.FSAFarmNumber;
        }
    }

    public class VarietyItem
    {
        public DateTime PlantingDate { get; set; }
        public string Variety { get; set; }
        public string Crop { get; set; }
        public decimal Area { get; set; }
        public decimal TurnRow { get; set; }
    }

    public class CropInfo
    {
        public CropId CropID { get; set; }
        public string CropDisplay { get; set; }
        public decimal Area { get; set; }

    }
}
