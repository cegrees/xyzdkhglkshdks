﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ReportModels.CropZone
{
    public class MapInfoData
    {
        public MapInfoData()
        {

        }

        public int CropYear { get; set; }
        public string DataSourceName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string CityStateZip { get { return string.Format("{0}, {1}  {2}", City, State, ZipCode); } }

        public string Farm { get; set; }
        public string Field { get; set; }
        public string CropZone { get; set; }
        public string FarmFieldCZ { get { return string.Format("{0} : {1} : {2}", Farm, Field, CropZone); } }
        public string CenterLatLon { get; set; }
        public string FSANumber { get; set; }
        public string FSATract { get; set; }
        public string FieldNumber { get; set; }

        public string Township { get; set; }
        public string Range { get; set; }
        public string Section { get; set; }
        public string County { get; set; }
        public string Baseline { get; set; }
        public string Merdian { get; set; }

        public string Crop { get; set; }
        public string SubCrop { get; set; }
        public string VarietyDisplay { get; set; }
        public string Area { get; set; }
        public string ContractId { get; set; }
        public string GrowerId { get; set; }

        public string LegalDescription { get; set; }

        public Bitmap MapImage { get; set; }
    }
}
