﻿using AgC.UnitConversion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Landdb.Infrastructure;

namespace Landdb.ReportModels.CropZone
{
    public class CropZoneData
    {
        public CropZoneData()
        {
            CropZoneLineItems = new List<CropZoneLineItemData>();
            CropSummaries = new List<CropSummary>();
        }

        public string DatasourceName { get; set; }
        public string StreetAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string CropYear { get; set; }

        public List<CropZoneLineItemData> CropZoneLineItems { get; set; }
        public List<CropSummary> CropSummaries { get; set; }

        public class CropZoneLineItemData
        {
            //Add Variety and Planting Date
            public string Farm { get; set; }
            public string Field { get; set; }
            public string CropZone { get; set; }
            public string Tags { get; set; }
            public string Crop { get; set; }
            public double BoundaryArea { get; set; }
            public double ReportedArea { get; set; }
            public bool? Irrigation { get; set; }
            public string WaterSource { get; set; }
            public List<ContractLineItemData> Contracts { get; set; }
            public bool Rented { get; set; }
            public string PlantingDates { get; set; }
            public string Varieties { get; set; }
            public int CropYear { get; set; }
	        public string DataSourceName { get; set; } = ApplicationEnvironment.CurrentDataSourceName;
        }

        public class ContractLineItemData
        {
            public string ContractName { get; set; }
            public string Company { get; set; }
            public DateTime Created { get; set; }
            public DateTime DeliveryDate { get; set; }
            public double ContractAmount { get; set; }
            public string Unit { get; set; }
            public string AmountDisplay { get { return string.Format("{0} {1}", ContractAmount.ToString("N2"), UnitFactory.GetUnitByName(Unit).AbbreviatedDisplay); } }
            public double PricePer { get; set; }
        }

        public class CropSummary
        {
            public Guid CropId { get; set; }
            public string Name { get; set; }
            public decimal AreaValue { get; set; }
            public string AreaUnit { get; set; }
        }
    }
}
