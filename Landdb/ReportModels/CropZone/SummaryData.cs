﻿using AgC.UnitConversion;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ReportModels.CropZone
{
    public class SummaryData
    {
        public SummaryData()
        {
            Varieties = new List<Variety>();
            YieldItems = new List<GenericListItem>();
            InputItems = new List<GenericListItem>();
            CropHistorys = new List<FieldCropHistoryItem>();
        }

        public string DataSourceName { get; set; }
        public int CropYear { get; set; }
        public string FieldDisplay { get; set; }
        public string CropDisplay { get; set; }
        public string AreaDisplay { get; set; }
        public List<Variety> Varieties { get; set; }
        public string FertilizerAnalysis { get; set; }
        public List<GenericListItem> YieldItems { get; set; }
        public List<GenericListItem> InputItems { get; set; }
        public List<FieldCropHistoryItem> CropHistorys { get; set; }
        public Bitmap MapImage { get; set; }
    }


    public class Variety
    {
        public Variety (string name, string plantingDateText)
        {
            VarietyName = name;
            PlantingDate = plantingDateText;
        }
        public string VarietyName { get; set; }
        public string PlantingDate { get; set; }
    }

    public class GenericListItem
    {
        public GenericListItem(string header, string valueA, string valueB)
        {
            Header = header;
            ValueA = valueA;
            ValueB = valueB;
        }

        public string Header { get; set; }
        public string ValueA { get; set; }
        public string ValueB { get; set; }
    }

    public class FieldCropHistoryItem
    {
        public CropZoneId CropZoneID { get; set; }
        public int CropYear { get; set; }
        public CropId CropID { get; set; }
        public string Crop { get; set; }
        public decimal Area { get; set; }
        public IUnit AreaUnit { get; set; }
        public string AreaDisplay { get { return string.Format("{0} {1}", Area.ToString("N2"), AreaUnit.AbbreviatedDisplay ); } }
        public decimal AverageYieldPerArea { get; set; }
        public CompositeUnit YieldUnit { get; set; }
        public string AvgYieldDisplay { get { return YieldUnit != null ? string.Format("{0} {1}", AverageYieldPerArea.ToString("N2"), YieldUnit.AbbreviatedDisplay) : string.Empty; } }
    }
}
