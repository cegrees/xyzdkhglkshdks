﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ReportModels.CropZone
{
    public class FSAValidationData
    {
        public string DatasourceName { get; set; }
        public int CropYear { get; set; }
        public string DisplayName { get; set; }
        public string FsaFarmNumber { get; set; }
        public string FsaTractNumber { get; set; }
        public string FsaFieldNumber { get; set; }
        public decimal FsaArea { get; set; }
        public string County { get; set; }
        public string Crop { get; set; }
        public bool? Irrigated { get; set; }
        public string Meridian { get; set; }
        public string Township { get; set; }
        public string Section { get; set; }
        public string Range { get; set; }
        public string CashRent { get; set; }
        public string CropShare { get; set; }
        public decimal LandLordPercent { get; set; }
        public decimal TenantPercent { get; set; }
        public string CropInsuranceId { get; set; }
        public string Variety { get; set; }
        public decimal ReportedArea { get; set; }
        public decimal PlantingArea { get; set; }
        public string Replant { get; set; }
        public string PlantingDate { get; set; }
        public decimal TurnRow { get; set; }
    }
}
