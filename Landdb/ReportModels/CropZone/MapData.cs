﻿using AgC.UnitConversion;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ReportModels.CropZone
{
    public class MapData
    {
        public MapData()
        {
            LegendItems = new List<LegendItem>();
        }

        public string DataSourceName { get; set; }
        public string Title { get; set; }
        public string SubHeader { get; set; }
        public Bitmap MapImage { get; set; }
        public List<LegendItem> LegendItems { get; set; }
        public string CropYear { get; set; }
        public string DisplayCoordinates { get; set; }
    }

    public class LegendItem 
    {
        public string Name { get; set; }
        public decimal AreaValue { get; set; }
        public IUnit AreaUnit { get; set; }
        public string AreaDisplay { get { return AreaValue.ToString("n2"); } }
    }
}
