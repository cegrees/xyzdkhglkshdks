﻿using Landdb.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ReportModels.Crop_History
{
    public class CropHistoryData
    {
        public string DateRangeDisplay { get; set; }
        public int CropYear { get; set; }
        public Guid FarmId { get; set; }
        public Guid FieldId { get; set; }
        public string FarmName { get; set; }
        public string FieldName { get; set; }
        public string CropZoneName { get; set; }
        public string Crop { get; set; }
        public string FertSummary { get; set; }
        public decimal Area { get; set; }
        public string AvgYieldDisplay { get; set; }
        public string PlantingDate { get; set; }
        public string Variety { get; set; }
        public string Tillage { get; set; }
        public int CurrentCropYear { get { return ApplicationEnvironment.CurrentCropYear; } }
        public string Township { get; set; }
        public string Range { get; set; }
        public string Section { get; set; }
 
        public string DataSourceName { get { return ApplicationEnvironment.CurrentDataSourceName; } }
        public decimal N { get; set; }
        public decimal P { get; set; }
        public decimal K { get; set; }
        public decimal Ca { get; set; }
        public decimal Mg { get; set; }
        public decimal S { get; set; }
        public decimal B { get; set; }
        public decimal Cl { get; set; }
        public decimal Cu { get; set; }
        public decimal Fe { get; set; }
        public decimal Mn { get; set; }
        public decimal Mo { get; set; }
        public decimal Zn { get; set; }
    }
}
