﻿using AgC.UnitConversion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ReportModels.Inventory
{
    public class ProductSummaryData
    {
        public string DataSourceName { get; set; }
        public int CropYear { get; set; }
        public string Vendor { get; set; }
        public ProductId ProductID { get; set; }
        public string Product { get; set; }
        public string Manufacturer { get; set; }
        public decimal TotalQuantity { get; set; }
        public IUnit TotalUnit { get; set; }
        public string TotalQuantityDisplay { get { return string.Format("{0} {1}", TotalQuantity.ToString("N2"), TotalUnit.AbbreviatedDisplay); } }
        public decimal AvgCostPerUnit{ get { return TotalQuantity != 0 ? TotalCost / TotalQuantity : 0; } }
        public decimal TotalCost { get; set; }
        public string InvoiceNumber { get; set; }
        public string DateRangeDisplay { get; set; }
    }
}
