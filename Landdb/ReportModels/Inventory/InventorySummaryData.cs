﻿using AgC.UnitConversion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Landdb.Resources;

namespace Landdb.ReportModels.Inventory
{
    public class InventorySummaryData
    {
        public InventorySummaryData()
        {
            InventoryItems = new List<ProductLineItem>();
            IsSubSet = false;
        }

        public string CropYear { get; set; }
        public string DataSourceName { get; set; }
        public bool IsSubSet { get; set; }
        public List<ProductLineItem> InventoryItems { get; set; }
        public string DateRangeDisplay { get; set; }
    }

    public class ProductLineItem
    {
        public string Product { get; set; }
        public string Manufacturer { get; set; }
        public decimal PurchasedValue { get; set; }
        public IUnit PurchasedUnit { get; set; }
        public string Purchased { get { return  PurchasedUnit != null ? string.Format("{0} {1}", PurchasedValue.ToString("N2"), PurchasedUnit.AbbreviatedDisplay) : string.Empty; } }
        public decimal PurchasedCost { get; set; }
        public decimal AppliedValue { get; set; }
        public IUnit AppliedUnit { get; set; }
        public string Applied { get { return AppliedUnit != null ? string.Format("{0} {1}", AppliedValue.ToString("N2"), AppliedUnit.AbbreviatedDisplay) : string.Empty; } }
        public decimal AppliedCost { get; set; }
        public decimal RemainingValue { get; set; }
        public IUnit RemainingUnit { get; set; }
        public string Remaining { get { return RemainingUnit != null ? string.Format("{0} {1}", RemainingValue.ToString("N2"), RemainingUnit.AbbreviatedDisplay) : string.Empty; } }
        public decimal InventoryCost { get; set; }
        public decimal AveragePriceValue { get; set; }
        public IUnit AveragePriceUnit { get; set; }
        public string Average { get { return AveragePriceUnit != null ? string.Format(Strings.Per_Format_Text, AveragePriceValue.ToString("N2"), AveragePriceUnit.AbbreviatedDisplay) : string.Empty; } }
        public decimal TotalCostValue { get; set; }
        public IUnit TotalCostUnit { get; set; }
        public string Total { get { return string.Format(Strings.Dollars_Format_Text, TotalCostValue.ToString("N2")); } }
    }
}
