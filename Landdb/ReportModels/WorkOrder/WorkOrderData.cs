﻿using AgC.UnitConversion;
using Landdb.Infrastructure;
using Microsoft.Maps.MapControl.WPF;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Landdb.Resources;

namespace Landdb.ReportModels.WorkOrder {
    public class WorkOrderData {
        public WorkOrderData() {
            Products = new List<ProductLineItemData>();
            Fields = new List<FieldLineItemData>();
            Applicators = new List<ApplicatorLineItemData>();
            WorkerProtectionItems = new List<WorkerProtectionItem>();
        }

        public List<ProductLineItemData> Products { get; set; }
        public List<FieldLineItemData> Fields { get; set; }
        public List<ApplicatorLineItemData> Applicators { get; set; }
        public List<WorkerProtectionItem> WorkerProtectionItems { get; set; }
        public string DatasourceName { get; set; }
        public string StreetAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string Authorizer { get; set; }
        public DateTime AuthorizerDate { get; set; }
        public string WorkOrderName { get; set; }
        public string WorkOrderDate { get; set; }
        public int CropYear { get; set; }
        public string TimingEvent { get; set; }
        public string Notes { get; set; }
        public MapLayer Layer { get; set; }
        public Bitmap MapImage { get; set; }
        public string MapLocation { get; set; }
        public string WorkOrderType { get; set; }
        public string TotalArea { get; set; }
        public double TotalCost { get; set; }

        public bool TankInformationVisibility { get; set; }
        public decimal TankSizeValue { get; set; }
        public string TankSizeUnit { get; set; }
        public decimal CarrierPerAreaValue { get; set; }
        public string CarrierPerAreaUnit { get; set; }
        public decimal TotalCarrierValue { get; set; }
        public string TotalCarrierUnit { get; set; }
        public decimal? TankCount { get; set; }

        public string TankSizeDisplay { get { return string.IsNullOrEmpty(TankSizeUnit) || TankSizeValue == 0m ? string.Empty : string.Format("{0} {1}", TankSizeValue.ToString("N2"), UnitFactory.GetUnitByName(TankSizeUnit).AbbreviatedDisplay); } }
        public string CarrierPerAreaDisplay { get { return string.IsNullOrEmpty(CarrierPerAreaUnit) || CarrierPerAreaValue == 0m ? string.Empty : string.Format("{0} {1}", CarrierPerAreaValue.ToString("N2"), UnitFactory.GetUnitByName(CarrierPerAreaUnit).AbbreviatedDisplay); } }
        public string TotalCarrierDisplay { get { return string.IsNullOrEmpty(TotalCarrierUnit) || TotalCarrierValue == 0m ? string.Empty : string.Format("{0} {1}", TotalCarrierValue.ToString("N2"), UnitFactory.GetUnitByName(TotalCarrierUnit).AbbreviatedDisplay); } }

        public class ProductLineItemData {
            public ProductLineItemData() {
                AssociatedProducts = new List<AssociatedProductItem>();
            }
            public string ProductName { get; set; }
            public string Active { get; set; }
            public string EPANumber { get; set; }
            public string REI { get; set; }
            public string PHI { get; set; }
            public double RateValue { get; set; }
            public string RateUnit { get; set; }
            public double TotalValue { get; set; }
            public string TotalUnit { get; set; }
            public string ApplicationMethod { get; set; }
            public string Pest { get; set; }
            public string RateDisplay { get { return string.Format("{0} {1}", RateValue.ToString("N2"), UnitFactory.GetUnitByName(RateUnit).AbbreviatedDisplay); } }
            public string TotalDisplay { get { return string.Format("{0} {1}", TotalValue.ToString("N2"), UnitFactory.GetUnitByName(TotalUnit).AbbreviatedDisplay); } }
            public double RatePer100 { get; set; }
            public string RatePer100Unit { get; set; }
            public string RatePer100Display { get { return RatePer100 > 0 ? string.Format("{0} {1}", RatePer100.ToString("N2"), UnitFactory.GetUnitByName(RatePer100Unit).AbbreviatedDisplay) : string.Empty; } }
            public double RatePerTank { get; set; }
            public string RatePerTankUnit { get; set; }
            public string RatePerTankDisplay { get { return RatePerTank > 0 ? string.Format("{0} {1}", RatePerTank.ToString("N2"), UnitFactory.GetUnitByName(RatePerTankUnit).AbbreviatedDisplay) : string.Empty; } }
            public string RateType { get; set; }
            public decimal PercentApplied { get; set; }
            public decimal TotalArea { get; set; }
            public IUnit TotalAreaUnit { get; set; }
            public string AppliedAreaDisplay { get { return string.Format("{0} {1}", (TotalArea * PercentApplied).ToString("N2"), TotalAreaUnit.AbbreviatedDisplay); } }
            public int RowCount { get; set; }
            public double TotalCost { get; set; }
            public double CostPerArea { get; set; }
            public string CostPerAreaDisplay { get { return string.Format(Strings.Per_Format_Text, CostPerArea.ToString("C2"), TotalAreaUnit.AbbreviatedDisplay); } }
            public string DefaultAreaUnit { get; set; }
            public List<AssociatedProductItem> AssociatedProducts { get; set; }
        }

        public class FieldLineItemData {
            public string Farm { get; set; }
            public string Field { get; set; }
            public string CropZone { get; set; }
            public string Crop { get; set; }
            public string CenterLatLong { get; set; }
            public double Area { get; set; }
        }

        public class ApplicatorLineItemData {
            public string Name { get; set; }
            public string Company { get; set; }
            public string LicenseNumber { get; set; }
            public DateTime ExpirationDate { get; set; }
            public string DisplayName { get; set; }
        }

        public class WorkerProtectionItem {
            public string ProductName { get; set; }
            public string PPE { get; set; }
            public string ReEntry { get; set; }
            public string SignalWord { get; set; }
            public string RestrictedUse { get; set; }
        }

        public class AssociatedProductItem {
            public string ProductName { get; set; }
            public decimal RatePerAreaValue { get; set; }
            public IUnit RatePerAreaUnit { get; set; }
            public decimal TotalProductValue { get; set; }
            public IUnit TotalProductUnit { get; set; }
            public string CustomRateType { get; set; }
            public decimal CustomRateValue { get; set; }
            public bool HasCost { get; set; }
            public decimal CostPerUnitValue { get; set; }
            public IUnit CostPerUnitUnit { get; set; }
            public CompositeUnit DisplayCustomUnit { get; set; }
            public string CustomRateDisplay { get { return string.Format("{0} {1}", CustomRateValue.ToString("N2"), DisplayCustomUnit.FullDisplay); } }
            public string RateDisplay { get { return string.Format("{0} {1} / {2}", RatePerAreaValue.ToString("N2"), RatePerAreaUnit.AbbreviatedDisplay, ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit); } }
            public string TotalDisplay { get { return string.Format("{0} {1}", TotalProductValue.ToString("N2"), TotalProductUnit.AbbreviatedDisplay); } }
            public decimal TotalCost { get; set; }
        }
    }
}