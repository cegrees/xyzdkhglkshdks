﻿using AgC.UnitConversion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Landdb.Resources;

namespace Landdb.ReportModels.SpecialtyReportsModel
{
    public class Form1080Data
    {
        public Form1080Data()
        {
            WorkerProtectionItems = new List<WorkerProtectionItem>();
            Products = new List<ProductItem>();
            CropZones = new List<CropZoneItem>();
        }

        public string DataSourceDisplay { get; set; }
        public decimal TotalArea { get; set; }
        public decimal CarrierPerArea { get; set; }
        public DateTime ApplicationDate { get; set; }

        public List<ProductItem> Products { get; set; }
        public List<CropZoneItem> CropZones { get; set; }
        public List<WorkerProtectionItem> WorkerProtectionItems { get; set; }
    }

    public class ProductItem
    {
        public string Product { get; set; }
        public string EpaNumber { get; set; }
        public string Active { get; set; }
        public string RateDisplay { get { return string.Format(Strings.ValueUnitPerArea_Format_Text, RateValue.ToString("N2"), RateUnit.AbbreviatedDisplay); } }
        public decimal RateValue { get; set; }
        public IUnit RateUnit { get; set; }
        public string DilutionPer100 { get; set; }
        public string TotalProductDisplay { get { return string.Format("{0} {1}", TotalValue.ToString("N2"), TotalUnit.AbbreviatedDisplay); } }
        public decimal TotalValue { get; set; }
        public IUnit TotalUnit { get; set; }
    }

    public class CropZoneItem
    {
        public string Crop { get; set; }
        public string Section { get; set; }
        public string Township { get; set; }
        public string Range { get; set; }
        public decimal Acres { get; set; }
    }

    public class WorkerProtectionItem
    {
        public string ProductName { get; set; }
        public string PPE { get; set; }
        public string ReEntry { get; set; }
    }
}
