﻿using AgC.UnitConversion;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ReportModels.SpecialtyReportsModel
{
    public class WSDAData
    {
        public WSDAData()
        {
            Products = new List<ProductLineItem>();
            Applicators = new List<ApplicatorItem>();
        }

        public Bitmap Map { get; set; }

        public int CropYear { get; set; }
        public string DataSourceName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string CityStateZip { get { return string.Format("{0}, {1}  {2}", City, State, ZipCode); } } 

        public string Farm { get; set; }
        public string Field { get; set; }
        public string CropZone { get; set; }
        public string FarmFieldCZ { get { return string.Format("{0} : {1} : {2}", Farm, Field, CropZone); } }
        public string CenterLatLon { get; set; }

        public string Township { get; set; }
        public string Range { get; set; }
        public string Section { get; set; }
        public string County { get; set; }

        public string Crop { get; set; }
        public string SubCrop { get; set; }
        public string VarietyDisplay { get; set; }
        public string Area { get; set; }

        public List<ProductLineItem> Products { get; set; }
        public List<ApplicatorItem> Applicators { get; set; }
    }

    public class ProductLineItem
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string ProductName { get; set; }
        public string EpaNumber { get; set; }
        public string Active { get; set; }
        public string Area { get; set; }
        public double RateValue { get; set; }
        public IUnit RateUnit { get; set; }
        public string Rate { get { return string.Format("{0} {1}", RateValue.ToString("N2"), RateUnit.AbbreviatedDisplay); } }
        public double TotalValue { get; set; }
        public IUnit TotalUnit { get; set; }
        public string TotalProduct { get { return string.Format("{0} {1}", TotalValue.ToString("N2"), TotalUnit.AbbreviatedDisplay); } }
        public decimal CarrierPerArea { get; set; }
        public decimal? Temp { get; set; }
        public decimal? Humidity { get; set; }
        public decimal? WindSpeed { get; set; }
        public string WindDirection { get; set; }
        public string SkyCondition { get; set; }
        public string SoilCondition{ get; set; }
        public string AppMethod { get; set; }
        public string Applicator { get; set; }
        public int ItemCount { get; set; }
    }

    public class ApplicatorItem 
    {
        public string Applicator { get; set; }
        public string LicenseNumber { get; set; }
        public string Company { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
    }
}
