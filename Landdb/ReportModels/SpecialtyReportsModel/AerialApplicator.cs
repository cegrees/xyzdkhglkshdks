﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ReportModels.SpecialtyReportsModel
{
    public class AerialApplicator
    {
        public string DataSourceName { get; set; }
        public string CropYear { get; set; }
        public string Farm { get; set; }
        public string Field { get; set; }
        public string CropZone { get; set; }
        public string AreaDisplay { get; set; }
        public string Crop { get; set; }
        public string ApplicationCompany { get; set; }
        public string Applicator { get; set; }
        public string LicenseNumber { get; set; }
        public string Vendor { get; set; }
        public Bitmap MapImage { get; set; }
        public string CenterLatLong { get; set; }
        public string TotalAreaDisplay { get; set; }
        public string Notes { get; set; }
    }
}
