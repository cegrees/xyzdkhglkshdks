﻿using AgC.UnitConversion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ReportModels.SpecialtyReportsModel
{
    public class PesticideUseData
    {
        public PesticideUseData()
        {
            CropProtectionItems = new List<ReportLineItemData>();
        }

        public string Month { get; set; }
        public string Year { get; set; }
        public string DataSourceName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        public int? CountyNumber { get; set; }
        public string CropZoneDisplay { get; set; }
        public string FieldName { get; set; }
        public string AreaDisplay { get; set; }
        public string SiteId { get; set; }
        public string Section { get; set; }
        public string TownshipDisplay { get; set; }
        public string RangeDisplay { get; set; }
        public string MeridianDisplay { get; set; }
        public DateTime SigningDate { get; set; }
        public string SignedBy { get; set; }
        public string ReportTitle { get; set; }
        public string PermitNumber { get; set; }
        public List<ReportLineItemData> CropProtectionItems { get; set; }
    }

    public class ReportLineItemData
    {
        public string ProductDisplay { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        //public string Active { get; set; }
        public string EPANumber { get; set; }
        public double Rate { get; set; }
        public string RateUnit { get; set; }
        public string RateDisplay { get { return string.Format("{0} {1}", Rate.ToString("N3"), UnitFactory.GetUnitByName(RateUnit).AbbreviatedDisplay); } }
        //public string AreaDisplay { get; set; }
        public double TotalProduct { get; set; }
        public string TotalUnit { get; set; }
        public string TotalProductDisplay { get { return string.Format("{0} {1}", TotalProduct.ToString("N2"), UnitFactory.GetUnitByName(TotalUnit).AbbreviatedDisplay); } }
        //public double TotalCost { get; set; }
        public string REI { get; set; }
        public string REIDate { get; set; }
        //public string PHI { get; set; }
        //public string PHIDate { get; set; }
        //public string RUP { get; set; }
        public string Area { get; set; }
        public string ApplicationMethod { get; set; }
        public string TargetPest { get; set; }
        public double CarrierPerArea { get; set; }
    }
}
