﻿using AgC.UnitConversion;
using Landdb.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ReportModels.AppliedProduct
{
    public class AppliedProductDetail
    {
        public string DatasourceName { get; set; }
        public string CropYear { get; set; }
        public string DateRangeDisplay { get; set; }
        public ProductId ProductId { get; set; }
        public string ProductName { get; set; }
        public string Manufacturer { get; set; }
        public string FarmName { get; set; }
        public string FieldCropZone { get; set; }
        public DateTime ApplicationDate { get; set; }
        public double RateValue { get; set; }
        public IUnit RateUnit { get; set; }
        public string RateDisplay { get { return string.Format("{0} {1}", RateValue.ToString("N2"), RateUnit.AbbreviatedDisplay); } }
        public double AreaValue { get; set; }
        public string AreaUnit { get; set; }
        public string AreaDisplay { get { return string.Format("{0} {1}", AreaValue.ToString("N2"), AreaUnit); } }
        public double TotalProductValue { get; set; }
        public IUnit TotalUnit { get; set; }
        public string TotalProductDisplay { get { return string.Format("{0} {1}", TotalProductValue.ToString("N2"), TotalUnit.AbbreviatedDisplay); } }
    }
}
