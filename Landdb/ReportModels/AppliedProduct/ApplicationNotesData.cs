﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ReportModels.AppliedProduct
{
    public class ApplicationNotesData
    {
        public string FarmName { get; set; }
        public string FieldName { get; set; }
        public string CropZoneName { get; set; }
        public string DataSourceName { get; set; }
        public string CropYear { get; set; }
        public string DateRangeDisplay { get; set; }
        public double Area { get; set; }
        public DateTime StartDate { get; set; }
        public string Notes { get; set; }
    }
}
