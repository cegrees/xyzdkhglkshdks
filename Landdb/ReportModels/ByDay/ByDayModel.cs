﻿using AgC.UnitConversion;
using Landdb.Client.Services.Fertilizer;
using Landdb.Properties;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ReportModels.ByDay
{
    public class ByDayModel
    {
        public ByDayModel()
        {
            LineItems = new List<ReportLineItemData>();
            ActiveItems = new List<ActiveIngredientItemData>();
            CropZoneItems = new List<CropZoneLineItemData>();
            ApplicatorItems = new List<ApplicatorLineItemData>();
        }

        public List<ReportLineItemData> LineItems { get; set; }
        public List<ActiveIngredientItemData> ActiveItems { get; set; }
        public List<CropZoneLineItemData> CropZoneItems { get; set; }
        public List<ApplicatorLineItemData> ApplicatorItems { get; set; }
        public string ApplicationId { get; set; }
        public string CropYear { get; set; }
        public string DataSourceName { get; set; }
        public string AddressDisplay { get; set; }
        public string ApplicationType { get; set; }
        public string AuthorizedBy { get; set; }
        public string AuthorizeDisplay { get; set; }
        public DateTime? AuthorizedOn { get; set; }
        public string Notes { get; set; }
        public double Area { get; set; }
        public string AreaDisplay { get; set; }
        public string DateRangeDisplay { get; set; }
        public double TotalCost { get; set; }

        public decimal? WindSpeed { get; set; }
        public string WindDirection { get; set; }
        public decimal? Temp { get; set; }
        public decimal? Humidity { get; set; }
        public string SoilCondition { get; set; }
        public string SkyCondition { get; set; }

        public FertilizerFormulationModel TotalFertilizer { get; set; }
        public Bitmap logo { get { return Landdb.Properties.Resources.AgC_Logo; } }
    }

    public class ReportLineItemData {
        public string ProductDisplay { get; set; }
        public string Manufacturer { get; set; }
        public string Active { get; set; }
        public string EPANumber { get; set; }
        public double Rate { get; set; }
        public string RateUnit { get; set; }
        public string RateDisplay { get { return string.Format("{0} {1}", Rate.ToString("N2"), UnitFactory.GetUnitByName(RateUnit).AbbreviatedDisplay); } }
        public string AreaDisplay { get; set; }
        public double TotalProduct { get; set; }
        public string TotalUnit { get; set; }
        public string TotalProductDisplay { get { return string.Format("{0} {1}", TotalProduct.ToString("N2"), UnitFactory.GetUnitByName(TotalUnit).AbbreviatedDisplay); } }
        public double TotalCost { get; set; }
        public string REI { get; set; }
        public string REIDate { get; set; }
        public string PHI { get; set; }
        public string PHIDate { get; set; }
        public string RUP { get; set; }
        public double CropZoneArea { get; set; }
        public string ApplicationMethod { get; set; }
        public string TargetPest { get; set; }
    }

    public class ActiveIngredientItemData
    {
        public Guid ActiveIngredientID { get; set; }
        public string Name { get; set; }
        public double TotalLoad { get; set; } 
    }

    public class CropZoneLineItemData
    {            
        public string Farm { get; set; }
        public string Field { get; set; }
        public string CustomFieldId { get; set; }
        public string FieldCenterLatLong { get; set; }
        public string CropZone { get; set; }
        public string LocationDisplay { get { return string.Format("{0}:{1}:{2}", Farm, Field, CropZone); } }
        public string Crop { get; set; }
        public string CenterLatLong { get; set; }
        public decimal Area { get; set; }
        public decimal BoundaryArea { get; set; }
        public decimal REI{get;set;}
        public decimal PHI{get;set;}
        public DateTime REIExpiration{get;set;}
        public DateTime PHIExpiration{get;set;}
    }

    public class ApplicatorLineItemData
    {
        public string Name { get; set; }
        public string Company { get; set; }
        public string LicenseNumber { get; set; }
        public DateTime ExpirationDate { get; set; }
        public string DisplayName { get; set; }
    }
    
}
