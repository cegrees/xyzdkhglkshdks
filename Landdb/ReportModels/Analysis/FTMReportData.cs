﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ReportModels.Analysis
{
    public class FTMReportData
    {
        public FTMReportData()
        {
            ConservationPractices = new List<string>();
            EfficiencyValues = new List<EfficiencyValue>();
        }

        public List<EfficiencyValue> EfficiencyValues { get; set; }
        public string DataSourceName { get; set; }
        public string FieldDisplay { get; set; }
        public string CropDisplay { get; set; }
        public string VarietyDisplay { get; set; }
        public string AreaDisplay { get; set; }
        public string PreviousCropDisplay { get; set; }
        public DateTime PlantingDate { get; set; }
        public DateTime HarvestDate { get; set; }
        public decimal? EstYield { get; set; }
        public string Slope { get; set; }
        public string Irrigated { get; set; }
        public string LandUseState { get; set; }
        public string LandUseField { get; set; }
        public string LandUseDiff { get; set; }
        public string ErosionState { get; set; }
        public string ErosionField { get; set; }
        public string ErosionDiff { get; set; }
        public string SoilCarbonState { get; set; }
        public string SoilCarbonField { get; set; }
        public string SoilCarbonDiff { get; set; }
        public string EnergyState { get; set; }
        public string EnergyField { get; set; }
        public string EnergyDiff { get; set; }
        public string GreenhouseGasState { get; set; }
        public string GreenhouseGasField { get; set; }
        public string GreenhouseGasDiff { get; set; }
        public List<string> ConservationPractices { get; set; }
        public Bitmap Map { get; set; }
        public Bitmap Graph { get; set; }
        public string TillageProfile { get; set; }

        //Biotic Pressure
        public string SelectedWeedPressure { get; set; }
        public string SelectedInsectPressure { get; set; }
        public string SelectedDiseasePressure { get; set; }

        //Crop Management
        public string HydrologicSoilGroup { get; set; }
        public string NitrogenCredit { get; set; }
        public string ResidualNitrogen { get; set; }
        public string IPMStrategy { get; set; }
        public string RelativeAppRate { get; set; }
        public string SoilCondition { get; set; }

        public string JanVegCover { get; set; }
        public string FebVegCover { get; set; }
        public string MarVegCover { get; set; }
        public string AprVegCover { get; set; }
        public string MayVegCover { get; set; }
        public string JunVegCover { get; set; }
        public string JulVegCover { get; set; }
        public string AugVegCover { get; set; }
        public string SepVegCover { get; set; }
        public string OctVegCover { get; set; }
        public string NovVegCover { get; set; }
        public string DecVegCover { get; set; }

    }

    public class EfficiencyValue
    {
        public string Type { get; set; }
        public decimal Value { get; set; }
        public decimal CropYear { get; set; }
    }
}
