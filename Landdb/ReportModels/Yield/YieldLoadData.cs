﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ReportModels.Yield
{
    public class YieldLoadData
    {
        public string DataSourceName { get; set; }
        public string DateRange { get; set; }
        public CropZoneId CropZoneId { get; set; }
        public string FarmFieldCZ { get; set; }
        public string CropZoneArea { get; set; }
        public string Commodity { get; set; }
        public int CropYear { get; set; }
        public DateTime LoadDate { get; set; }
        public string LoadName { get; set; }
        public string LocationName { get; set; }
        public decimal Moisture { get; set; }
        public decimal GrossWeight { get; set; }
        public decimal TareWeight { get; set; }
        public decimal NetWeight { get; set; }
        public decimal DeliveredQty { get; set; }
        public decimal DryQty { get; set; }
        public string TotalDisplay { get; set; }
        public string PerAreaDisplay { get; set; }
        public decimal Area { get; set; }
        public string DryQtyUnit { get; set; }
    }
}
