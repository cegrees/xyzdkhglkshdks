﻿using AgC.UnitConversion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ReportModels.Yield
{
    public class ProfitabilityData
    {
        public ProfitabilityData()
        {
            ProfitabilityItems = new List<ProfitabilityItem>();
        }

        public List<ProfitabilityItem> ProfitabilityItems { get; set; }
    }

    public class ProfitabilityItem
    {
        public string Farm { get; set; }
        public string Field { get; set; }
        public string CZ { get; set; }
        public string Crop { get; set; }
        public string Variety { get; set; }
        public string DataSourceName { get; set; }
        public string DateRange { get; set; }
        public string CropYear { get; set; }
        public decimal Area { get; set; }
        public decimal AvgYieldValue { get; set; }
        public CompositeUnit AvgYieldUnit { get; set; }
        public string AvgYield { get { return AvgYieldUnit != null ? string.Format("{0} {1}", AvgYieldValue.ToString("N2"), AvgYieldUnit.AbbreviatedDisplay) : string.Empty; } }
        public decimal TotalYieldValue { get; set; }
        public CompositeUnit TotalYieldUnit { get; set; }
        public string TotalUnitDisplay { get { return TotalYieldUnit.AbbreviatedDisplay; } }
        public string TotalYield { get { return TotalYieldUnit != null ? string.Format("{0} {1}", TotalYieldValue.ToString("N2"), TotalYieldUnit.AbbreviatedDisplay) : string.Empty; } }
        public decimal Income { get; set; }
        public decimal Cost { get; set; }
        public decimal NetIncome { get; set; }
        public decimal NetIncomePerArea { get; set; }
        public string FSAInfo { get; set; }
    }

    public class ProfitabilityExcelItem
    {
        public string DataSourceName { get; set; }
        public string Farm { get; set; }
        public string Field { get; set; }
        public string CropZone { get; set; }
        public string Crop { get; set; }
        public string Variety { get; set; }
        public string Tags { get; set; }
        public string CropYear { get; set; }
        public decimal Area { get; set; }
        public decimal AvgYieldValue { get; set; }
        public CompositeUnit AvgYieldUnit { get; set; }
        public decimal TotalYieldValue { get; set; }
        public CompositeUnit TotalYieldUnit { get; set; }
        public decimal Income { get; set; }
        public decimal Cost { get; set; }
        public decimal NetIncome { get; set; }
        public decimal NetIncomePerArea { get; set; }
    }
}
