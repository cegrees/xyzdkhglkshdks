﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ReportModels.Yield
{
    public class ContributionMarginData
    {
        public string Farm { get; set; }
        public string Field { get; set; }
        public string CropZone { get; set; }
        public string Crop { get; set; }
        public int CropYear { get; set; }
        public decimal CropZoneArea { get; set; }
        public decimal ContributionMarginTotal { get; set; }
        public decimal ContributionMarginPerArea { get; set; }
        public decimal TotalYield { get; set; }
        public decimal YieldPerArea { get; set; }
        public decimal AverageSalePrice { get; set; }
        public decimal GrowerShares { get; set; }
        public decimal GrowerShare { get; set; }
        public decimal GrowerRevenue { get; set; }
        public decimal GrowerRevenuePerArea { get; set; }
        public decimal GrowerSeedCosts { get; set; }
        public decimal GrowerSeedCostPerArea { get; set; }
        public decimal GrowerFertilizerCosts { get; set; }
        public decimal GrowerFertilizerCostPerArea { get; set; }
        public decimal GrowerCropProtectionCosts { get; set; }
        public decimal GrowerCropProtectionCostPerArea { get; set; }
        public decimal GrowerServicesCosts { get; set; }
        public decimal GrowerServicesCostPerArea { get; set; }
        public decimal GrowerTotalInputCosts { get; set; }
        public decimal GrowerTotalInputCostPerArea { get; set; }
        public decimal CashRentTotal { get; set; }
        public decimal CashRentPerArea { get; set; }

    }
}
