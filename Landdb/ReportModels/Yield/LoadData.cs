﻿using Landdb.ViewModel.Secondary.YieldOperation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ReportModels.Yield
{
    public class LoadData
    {
        public LoadData()
        {
            LoadProperties = new List<LoadPropertyItem>();
            IncludedFields = new List<LoadFieldItem>();
        }

        public string CropYear { get; set; }
        public string DataSourceName { get; set; }
        public string DateRange { get; set; }
        public string Commodity { get; set; }
        public string LoadNumber { get; set; }
        public DateTime LoadDate { get; set; }
        public string Destination { get; set; }
        public string Truck { get; set; }
        public string Driver { get; set; }
        public decimal MoisturePercent { get; set; }
        public string MoistureShrinkDisplay { get; set; }
        public string AdditionalShrinkDisplay { get; set; }
        public string TotalShrinkDisplay { get; set; }
        public string GrossWeightDisplay { get; set; }
        public string TareWeightDisplay { get; set; }
        public string NetWeightDisplay { get; set; }
        public string FinalWeightDisplay { get; set; }
        public string Notes { get; set; }
        public List<LoadPropertyItem> LoadProperties { get; set; }
        public List<LoadFieldItem> IncludedFields { get; set; }
    }

    public class LoadPropertyItem
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }

    public class LoadFieldItem
    {
        public string Farm { get; set; }
        public string Field { get; set; }
        public string Crop { get; set; }
        public string FSAFarm { get; set; }
        public string FSATract { get; set; }
        public string FSAField { get; set; }
        public string Area { get; set; }
    }
}
