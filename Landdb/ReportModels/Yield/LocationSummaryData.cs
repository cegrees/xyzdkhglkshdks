﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ReportModels.Yield
{
    public class LocationSummaryData
    {
        public string DataSourceName { get; set; }
        public int CropYear { get; set; }
        public Guid LocationID { get; set; }
        public string LocationName { get; set; }
        public string LocationType { get; set; }
        
        public string Commodity { get; set; }
        public decimal RemainingValue { get; set; }
        public string RemainingUnit { get; set; }
    }
}
