﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ReportModels.Yield
{
    public class CommoditySummaryData
    {
        public string DataSourceName { get; set; }
        public int CropYear { get; set; }
        public Guid CommodityID { get; set; }
        public string Commodity { get; set; }
        public decimal HarvestedValue { get; set; }
        public string HarvestedUnit { get; set; }
        public decimal SoldValue { get; set; }
        public string SoldUnit { get; set; }
        public decimal StoredValue { get; set; }
        public string StoredUnit { get; set; }
        public string LoadsDisplay { get; set; }
    }
}
