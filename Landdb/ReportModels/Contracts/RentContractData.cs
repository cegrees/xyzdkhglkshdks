﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Landdb.Resources;

namespace Landdb.ReportModels.Contracts
{
    public class RentContractData
    {
        public RentContractData()
        {
            Fields = new List<FieldLineItem>();
            FlexItems = new List<FlexInformationItem>();
			YieldShares = new List<YieldShare>();
        }

        public string CropYear { get; set; }
        public string AreaDisplay { get; set; }
        public string DataSourceName { get; set; }
        public string ContractName { get; set; }
        public string LandOwner { get; set; }
        public string Crop { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime DueDate { get; set; }
        public string DueDateDisplay { get { return string.Format(Strings.DueOn_Format_Text, DueDate.ToShortDateString()); } }

        public double CropShare { get; set; }
        public double TotalHarvested { get; set; }
        public string HarvestedUnit { get; set; }
        public double LandOwnerShare { get; set; }
        public double LandOwnerShareSold { get; set; }
        public double LandOwnerSale { get; set; }
        public double AverageSalePrice { get; set; }

        public double PerAcreRent { get; set; }
        public double TotalRent { get; set; }
        public double TotalArea { get; set; }

        public double CPPercent { get; set; }
        public double FertPercent { get; set; }
        public double SeedPercent { get; set; }
        public double ServicePercent { get; set; }
        public double TotalCPShare { get; set; }
        public double TotalFertShare { get; set; }
        public double TotalSeedShare { get; set; }
        public double TotalServiceShare { get; set; }
        public double TotalShare { get; set; }
        public bool SharesVisible { get { return CPPercent > 0 || FertPercent > 0 || SeedPercent > 0 || ServicePercent > 0; } }

        public List<FlexInformationItem> FlexItems { get; set; }
        public double FlexPercent { get; set; }
        public string FlexPercentSource { get; set; }
        public string FlexPercentDisplay { get { return string.Format("{0} {1}", FlexPercent, FlexPercentSource); } }

        public List<FieldLineItem> Fields { get; set; }
        public List<YieldShare> YieldShares { get; set; }

        public class FlexInformationItem
        {
            public double BonusPercent { get; set; }
            public double PerAcreValue { get; set; }
            public string PerAcreUnit { get; set; }
        }

        public class FieldLineItem
        {
            public string Farm { get; set; }
            public string Field { get; set; }
            public string CropZone { get; set; }
            public double AreaDisplay { get; set; }
        }

        public class YieldShare
        {
            public double CropShare { get; set; }
            public double TotalHarvested { get; set; }
            public string HarvestedUnit { get; set; }
            public string TotalHarvestDisplay { get { return string.Format("{0} {1}", TotalHarvested.ToString("N2"), HarvestedUnit); } }
            public double LandOwnerShares { get; set; }
            public double LandOwnerSharesSold { get; set; }
            public double LandOwnerSales { get; set; }
            public double AverageSalePrice { get; set; }
            public double TotalSales { get; set; }
        }
    }
}
