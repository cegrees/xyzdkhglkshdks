﻿using AgC.UnitConversion;
using Landdb.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ReportModels.Contracts
{
    public class ProductionContractData
    {
        public ProductionContractData()
        {
            IncludedCropZones = new List<CropZoneItems>();
            Fulfillment = new List<FulfilmentItem>();
        }

        public List<FulfilmentItem> Fulfillment { get; set; }
        public string CropYear { get; set; }
        public string DataSourceName { get; set; }
        public double Area { get; set; }
        public string AreaDisplay { get; set; }
        public string ContractName { get; set; }
        public string ContractNumber { get; set; }
        public string Buyer { get; set; }
        public string Crop { get; set; }
        public DateTime StartDate  { get; set; }
        public DateTime DueDate{ get; set; }
        public string GrowerId { get; set; }
        public double ContractedPrice{ get; set; }
        public double FuturesPrice { get; set; }
        public double ContractedAmount { get; set; }
        public string ContractedAmountDisplay { get; set; }
        public double FulfilledAmount { get; set; }
        public string FulfilledDisplay { get; set; }
        public decimal PercentFulfilled { get; set; }
        public List<CropZoneItems> IncludedCropZones { get; set; }
    }

    public class CropZoneItems
    {
        public string Farm { get; set; }
        public string Field { get; set; }
        public string CropZone { get; set; }
        public double Area { get; set; }
        public string AreaDisplay { get { return string.Format("{0} {1}", Area.ToString("N2"), UnitFactory.GetUnitByName(ApplicationEnvironment.CurrentDataSource.DefaultAreaUnit).AbbreviatedDisplay); } }
    }

    public class FulfilmentItem
    {
        public string Name { get; set; }
        public double Value { get; set; }
    }
}
