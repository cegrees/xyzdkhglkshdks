﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.ReportModels.ShipperPacker
{
    public class ShipperPackerData
    {
        public ShipperPackerData()
        {
            LineItems = new List<ReportLineItemData>();
            ActiveItems = new List<ActiveIngredientItemData>();
        }

        public int CropYear { get; set; }
        public string DataSourceName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string CityStateZip { get { return !string.IsNullOrEmpty(City) ? string.Format("{0}, {1}  {2}", City, State, ZipCode) : string.Empty; } }
	    public string DateRangeDisplay { get; set; }
        public string Farm { get; set; }
        public string Field { get; set; }
        public string CropZone { get; set; }
        public string FarmFieldCZ { get { return string.Format("{0} : {1} : {2}", Farm, Field, CropZone); } }
        public string CenterLatLon { get; set; }
        public string PermitId { get; set; }
        public string SiteId { get; set; }
        public string BlockId { get; set; }

        public string Township { get; set; }
        public string Range { get; set; }
        public string Section { get; set; }
        public string County { get; set; }
        public string Baseline { get; set; }
        public string Merdian { get; set; }

        public string Crop { get; set; }
        public string SubCrop { get; set; }
        public string VarietyDisplay { get; set; }
        public string Area { get; set; }
        public string ContractId { get; set; }
        public string GrowerId { get; set; }

        public List<ReportLineItemData> LineItems { get; set; }
        public List<ActiveIngredientItemData> ActiveItems { get; set; }
    }

    public class ReportLineItemData
    {
        public string ApplicatorDisplay { get; set; }
        public double ApplicationArea { get; set; }
        public string StartDate { get; set; }
        public DateTime StartDateTime { get; set; }
        public string EndDate { get; set; }
        public string ProductDisplay { get; set; }
        public string EPANumber { get; set; }
        public string RateDisplay { get; set; }
        public double Rate { get; set; }
        public string RateUnit { get; set; }
        public string AreaDisplay { get; set; }
        public string TotalProductDisplay { get; set; }
        public double TotalProduct { get; set; }
        public string TotalUnit { get; set; }
        public double TotalCost { get; set; }
        public string REI { get; set; }
        public string REIDate { get; set; }
        public string PHI { get; set; }
        public string PHIDate { get; set; }
        public string PHIStatus { get; set; }
        public string RUP { get; set; }
        public double CropZoneArea { get; set; }
        public string FertilizerAnalysis { get; set; }
        public string Manufacturer { get; set; }
        public string AppMethod { get; set; }
        public string Pest { get; set; }
        public string Active { get; set; }
        public int ItemCount { get; set; }
        public string CarrierPerAreaDisplay { get; set; }
    }

    public class ActiveIngredientItemData
    {
        public Guid ActiveIngredientID { get; set; }
        public string Name { get; set; }
        public double Acres { get; set; }
        public double TotalLoad { get; set; }
        public double TotalPerAcre { get { return TotalLoad / Acres; } }
        public double TotalPerHectare { get { return TotalPerAcre * 1.1208511; } }
        public double TotalAllowedLoad { get; set; }
        public double TotalKilogramsAllowedLoad { get; set; }
        public double AIPercentRemaining { 
            get 
            {
                var allowed = TotalAllowedLoad == 0 ? 1 : (TotalAllowedLoad - TotalPerAcre) / TotalAllowedLoad;
                return allowed < 0 ? 0 : allowed;
            } 
        }
        public string Status { get { return AIPercentRemaining > 0 ? "Allowed" : "Denied"; } }
        public int ItemCount { get; set; }
    }
}
