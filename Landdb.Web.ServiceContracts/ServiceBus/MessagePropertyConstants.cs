﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Landdb.Web.ServiceContracts.ServiceBus {
    public static class MessagePropertyConstants {
        public static readonly string Type = "Type";
        public static readonly string DataSourceId = "DataSourceId";
        public static readonly string AggregateId = "AggregateId";
        public static readonly string AggregateVersion = "AggregateVersion";
        public static readonly string AggregateIdentityType = "AggregateIdentityType";
        public static readonly string OriginatingDeviceId = "OriginatingDeviceId";
        public static readonly string NumberOfEvents = "NumberOfEvents";
        public static readonly string FailedCommandId = "FailedCommandId";
    }
}
