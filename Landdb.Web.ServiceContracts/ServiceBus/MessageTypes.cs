﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Landdb.Web.ServiceContracts.ServiceBus {
    public enum MessageTypes {
        AggregateModified,
        CommandFailed,
    }
}
