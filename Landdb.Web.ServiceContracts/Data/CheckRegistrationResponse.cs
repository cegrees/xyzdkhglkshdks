﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Landdb.Web.ServiceContracts.Data {
    [DataContract]
    public class CheckRegistrationResponse {
        [DataMember(Order = 1)]
        public bool IsRegistered { get; set; }

        [DataMember(Order = 2)]
        public UserAccount AccountInformation { get; set; }
    }
}
