﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.Web.ServiceContracts.Data {
    [DataContract]
    public class DataSourceMessage {
        [DataMember(Order = 1)]
        public string Message { get; set; }

        [DataMember(Order = 2)]
        public string Url { get; set; }
    }
}
