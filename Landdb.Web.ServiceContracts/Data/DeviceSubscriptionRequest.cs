﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Landdb.Web.ServiceContracts.Data {
    [DataContract]
    public class DeviceSubscriptionRequest {
        [DataMember]
        public Guid DataSourceId { get; set; }
        [DataMember]
        public Guid DeviceId { get; set; }
    }
}
