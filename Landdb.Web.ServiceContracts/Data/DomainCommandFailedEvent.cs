﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Landdb.Web.ServiceContracts.Data {
    public class DomainCommandFailedEvent {
        public Guid DataSourceId { get; set; }
        public Guid AggregateId { get; set; }
        public string AggregateIdentityType { get; set; }
        public Guid CommandId { get; set; }
        public Guid OriginatingSource { get; set; }
        public string Reason { get; set; }
        public string OriginalMessage { get; set; }
        public string OriginalMetadata { get; set; }
    }
}
