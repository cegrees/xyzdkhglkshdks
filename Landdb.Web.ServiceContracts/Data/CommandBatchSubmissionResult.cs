﻿namespace Landdb.Web.ServiceContracts.Data {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
using System.Runtime.Serialization;

    [DataContract]
    public class CommandBatchSubmissionResult {
        public bool WasSuccessful { get; set; }
        public string Message { get; set; }

        // TODO: Add some more stuff to this?
    }
}
