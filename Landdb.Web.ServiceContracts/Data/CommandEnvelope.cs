﻿namespace Landdb.Web.ServiceContracts.Data {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
using System.Runtime.Serialization;

    [DataContract]
    public class CommandEnvelope {
        [DataMember]
        public Guid CommandId { get; set; }
        [DataMember]
        public Guid DataSourceId { get; set; }
        [DataMember]
        public int OriginalVersion { get; set; }
        [DataMember]
        public string CommandType { get; set; }
        [DataMember]
        public string Payload { get; set; }
    }

}
