﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Landdb.Web.ServiceContracts.Data {
    [DataContract]
    public class EventStoreCredentials {
        [DataMember]
        public string Username { get; set; }
        [DataMember]
        public string Password { get; set; }
        [DataMember]
        public bool HasValidCredentials { get; set; }
        [DataMember]
        public string InvalidCredentialReason { get; set; }
        [DataMember]
        public string EventStoreAddress { get; set; }
        [DataMember]
        public int EventStorePort { get; set; }
    }
}
