﻿namespace Landdb.Web.ServiceContracts.Data {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Runtime.Serialization;

    [DataContract]
    public class UserAccount {
        [DataMember(Order = 1)]
        public Guid Id { get; set; }
        [DataMember(Order = 2)]
        public string AuthenticationId { get; set; }
        [DataMember(Order = 3)]
        public string Email { get; set; }
        [DataMember(Order = 4)]
        public string GivenName { get; set; }
        [DataMember(Order = 5)]
        public string Surname { get; set; }
        [DataMember(Order = 6)]
        public DateTime CreatedOn { get; set; }

        [DataMember(Order = 7)]
        public IList<DataSourceDescriptor> SubscribableDataSources;
    }
}
