﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Landdb.Web.ServiceContracts.Data {
    [DataContract]
    public class CommandSubmissionRequest {
        [DataMember]
        public string SerializedCommand { get; set; }
        [DataMember]
        public string CommandType { get; set; }
        [DataMember]
        public Guid DeviceId { get; set; }
        [DataMember]
        public string DataSourceId { get; set; }
    }
}
