﻿namespace Landdb.Web.ServiceContracts.Data {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Runtime.Serialization;

    [DataContract]
    public class SubscriptionDetails {
        [DataMember]
        public Guid UserId { get; set; }
        [DataMember]
        public Guid DataSourceId { get; set; }
        [DataMember]
        public Guid DeviceId { get; set; }
        [DataMember]
        public bool ShouldResynchronize { get; set; }
        [DataMember]
        public DateTime CreatedOn { get; set; }

        [DataMember]
        public bool IsSubscriptionValid { get; set; }
        [DataMember]
        public string InvalidSubscriptionMessage { get; set; }
    }
}
