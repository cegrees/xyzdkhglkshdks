﻿namespace Landdb.Web.ServiceContracts.Data {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
using System.Runtime.Serialization;

    [DataContract]
    public class DataSourceDescriptor {
        // TODO: This should be renamed and can serve as the perm sheet structure sent to the client. Needs to be signed.
        [DataMember(Order = 1)]
        public Guid DataSourceId { get; set; }

        [DataMember(Order = 2)]
        public string Name { get; set; }

        [DataMember(Order = 3)]
        public IList<int> AvailableCropYears { get; set; }

        [DataMember(Order = 4)]
        public string Password { get; set; }

        [DataMember(Order = 5)]
        public string MasterlistGroup { get; set; }

        [DataMember(Order = 6)]
        public string DatasourceCulture { get; set; }

        [DataMember(Order = 7)]
        public string DefaultAreaUnit { get; set; }

        [DataMember(Order = 8)]
        public DataSourceMessage DataSourceMessage { get; set; }
    }
}
