﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Landdb.Web.ServiceContracts.Data {
    [DataContract]
    public class EventSubmissionResult {
        [DataMember]
        public bool CloudIsOfflineForMaintenance { get; set; }

        [DataMember]
        public bool DataSourceWasLockedForMaintenance { get; set; }

        [DataMember]
        public bool EventsWereReceived { get; set; }
    }
}
