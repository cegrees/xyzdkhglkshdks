﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Landdb.Web.ServiceContracts.Data {
    //[DataContract]
    //public class CommandSubmissionResponse_v1 {
    //    [DataMember]
    //    public bool CommandWasReceived { get; set; }

    //    [DataMember]
    //    public CommandSubmissionFailureReasons CommandSubmissionFailureReason { get; set; }

    //    [DataMember]
    //    public string AdditionalData { get; set; }
    //}

    [DataContract]
    public class CommandSubmissionResponse {
        [DataMember]
        public bool CommandWasSuccessful { get; set; }

        [DataMember]
        public CommandSubmissionFailureReasons CommandSubmissionFailureReason { get; set; }

        [DataMember]
        public string AdditionalData { get; set; }

        [DataMember]
        public IList<IDomainEvent> GeneratedEvents { get; set; }

        [DataMember]
        public DomainError DomainError { get; set; }
    }

    public enum CommandSubmissionFailureReasons {
        None,
        CloudIsOffline,
        DatasourceWasLocked,
        AuthenticationFailed,
        InvalidRequest,
        AggregateRebirth,
        AggregateZombie,
        AggregatePremature,
        AggregateDomainException
    }
}
