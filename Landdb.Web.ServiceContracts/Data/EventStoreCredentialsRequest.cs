﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Landdb.Web.ServiceContracts.Data {
    [DataContract]
    public class EventStoreCredentialsRequest {
        [DataMember]
        public Guid DataSourceId { get; set; }
    }
}
