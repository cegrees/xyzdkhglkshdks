﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Landdb.Web.ServiceContracts {
    public class EventStoreRecordSlice {
        public EventStoreRecordSlice(RecordedEventStoreData[] events, int fromEventNumber, bool isEndOfStream, int lastEventNumber, int nextEventNumber) {
            this.Events = events;
            this.FromEventNumber = fromEventNumber;
            this.IsEndOfStream = isEndOfStream;
            this.LastEventNumber = lastEventNumber;
            this.NextEventNumber = nextEventNumber;
        }

        public readonly RecordedEventStoreData[] Events;
        public readonly int FromEventNumber;
        public readonly bool IsEndOfStream;
        public readonly int LastEventNumber;
        public readonly int NextEventNumber;
    }
}
