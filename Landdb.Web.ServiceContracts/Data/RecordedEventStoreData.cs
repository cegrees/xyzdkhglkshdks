﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Landdb.Web.ServiceContracts {
    public class RecordedEventStoreData {
        public RecordedEventStoreData(byte[] data, Guid eventId, int eventNumber, string eventStreamId, string eventType, byte[] metadata, int originalEventNumber) {
            Data = data;
            EventId = eventId;
            EventNumber = eventNumber;
            EventStreamId = eventStreamId;
            EventType = eventType;
            Metadata = metadata;
            OriginalEventNumber = originalEventNumber;
        }

        public readonly byte[] Data;
        public readonly Guid EventId;
        public readonly int EventNumber;
        public readonly string EventStreamId;
        public readonly string EventType;
        public readonly byte[] Metadata;
        public readonly int OriginalEventNumber;
    }
}