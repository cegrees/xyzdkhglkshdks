﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Landdb.Web.ServiceContracts.Exceptions {
    public class AuthenticationRequiredException : Exception {
        public AuthenticationRequiredException(string message)
            : base(message) {
        }

        public AuthenticationRequiredException(string message, Exception inner)
            : base(message, inner) {
        }
    }
}
