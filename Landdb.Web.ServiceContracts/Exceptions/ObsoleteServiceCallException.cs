﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Landdb.Web.ServiceContracts.Exceptions {
    /// <summary>
    /// Thrown when an attempt is made to call a service method that has been obsoleted.
    /// </summary>
    [Serializable]
    public class ObsoleteServiceCallException : Exception {
        public ObsoleteServiceCallException() { }
        public ObsoleteServiceCallException(string message) : base(message) { }
        public ObsoleteServiceCallException(string message, Exception inner) : base(message, inner) { }
        protected ObsoleteServiceCallException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
