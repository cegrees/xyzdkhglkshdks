﻿namespace Landdb.Web.ServiceContracts {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.ServiceModel;
    using Landdb.Web.ServiceContracts.Data;
    using System.Runtime.Serialization;
    using System.ServiceModel.Web;

    [ServiceContract]
    public interface IDataSynchronizationServices {
        [Obsolete]
        [OperationContract]
        [WebInvoke(Method = "POST",
             UriTemplate = "/submit",
             RequestFormat = WebMessageFormat.Xml,
             ResponseFormat = WebMessageFormat.Xml,
             BodyStyle = WebMessageBodyStyle.Bare)]
        EventSubmissionResult SubmitEvents(EventSubmissionRequest request);

        [OperationContract]
        [WebInvoke(Method = "POST",
             UriTemplate = "/submit_command",
             RequestFormat = WebMessageFormat.Xml,
             ResponseFormat = WebMessageFormat.Xml,
             BodyStyle = WebMessageBodyStyle.Bare)]
        CommandSubmissionResponse SubmitCommands(CommandSubmissionRequest request);
    }
}

