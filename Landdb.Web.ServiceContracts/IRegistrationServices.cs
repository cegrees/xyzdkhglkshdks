﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Landdb.Web.ServiceContracts.Data;
using System.ServiceModel.Web;

namespace Landdb.Web.ServiceContracts {
    [ServiceContract]
    public interface IRegistrationServices {
        [OperationContract]
        [WebInvoke(Method = "GET",
            UriTemplate = "/validate",
            RequestFormat = WebMessageFormat.Xml,
            ResponseFormat = WebMessageFormat.Xml,
            BodyStyle = WebMessageBodyStyle.Bare)]
        CheckRegistrationResponse CheckRegistration();

        // TODO: Need to accept device information, like name, type, etc. so that we can store a Device record alongside the subscription if the device ID doesn't already exist
        [OperationContract]
        [WebInvoke(Method = "POST",
            UriTemplate = "/ensure_subscription",
            RequestFormat = WebMessageFormat.Xml,
            ResponseFormat = WebMessageFormat.Xml,
            BodyStyle = WebMessageBodyStyle.Bare)]
        SubscriptionDetails GetOrCreateSubscription(DeviceSubscriptionRequest request);

        [OperationContract]
        [WebInvoke(Method = "POST",
            UriTemplate = "/get_eventstore_creds",
            RequestFormat = WebMessageFormat.Xml,
            ResponseFormat = WebMessageFormat.Xml,
            BodyStyle = WebMessageBodyStyle.Bare)]
        EventStoreCredentials GetEventStoreCreds(EventStoreCredentialsRequest request);

    }
}
