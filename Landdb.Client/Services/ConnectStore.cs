﻿using System;
using System.Collections.Generic;

namespace Landdb.Client.Services
{
    public class ConnectStore<StoreData> where StoreData : IEquatable<StoreData>
    {
        private readonly List<Tuple<Action, Action>> subscribers;
        private List<StoreData> store;

        public ConnectStore()
        {
            subscribers = new List<Tuple<Action, Action>>();
            store = new List<StoreData>();
        }

        public Guid DatasourceId { get; private set; }

        public List<StoreData> GetData()
        {
            return new List<StoreData>(store);
        }

        public void UpdatedData(List<StoreData> possibleNewState)
        {
            if (SendData(possibleNewState))
            {
                subscribers.ForEach(subscriber => subscriber.Item1.Invoke());
            }
        }

        private bool SendData(List<StoreData> possibleNewState)
        {
            if (ListsAreDifferent(store, possibleNewState))
            {
                store = new List<StoreData>(possibleNewState);
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool ListsAreDifferent(List<StoreData> store, List<StoreData> possibleNewState)
        {
            if (store.Count != possibleNewState.Count)
            {
                return true;
            }
            else
            {
                bool isNewData = false;

                foreach (StoreData possibleState in possibleNewState)
                {
                    if (IsNewData(store, possibleState))
                    {
                        //If we find one item that is new. Then they lists are different.
                        //And we can exit the loop.
                        isNewData = true;
                        break;
                    }
                }

                return isNewData;
            }
        }

        private static bool IsNewData(List<StoreData> store, StoreData inputData)
        {
            bool isNewData = true;

            store.ForEach(storeData =>
            {
                if (inputData.Equals(storeData))
                {
                    isNewData = false;
                }
            });

            return isNewData;
        }

        public void Subscribe(Action UpdateDataChanged, Action UpdateDataWasRemoved)
        {
            subscribers.Add(new Tuple<Action, Action>(UpdateDataChanged, UpdateDataWasRemoved));
        }

        public void UpdateDatasourceId(Guid datasourceId)
        {
            DatasourceId = datasourceId;
            store = new List<StoreData>();
        }

        public void Remove(StoreData storeData)
        {
            if (SendData(store.FindAll(data => !data.Equals(storeData))))
            {
                subscribers.ForEach(subscriber => subscriber.Item2.Invoke());    
            }
        }
    }
}