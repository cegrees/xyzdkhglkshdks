﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Landdb.Web.ServiceContracts.Data;

namespace Landdb.Client.Services.Web {
    public class DeviceSubscriptionResults {
        public DeviceSubscriptionResults(SubscriptionDetails details, bool hasError, Exception exception) {
            Details = details;
            HasError = hasError;
            Exception = exception;
        }

        public SubscriptionDetails Details { get; private set; }
        public Exception Exception { get; private set; }
        public bool HasError { get; private set; }
    }
}
