﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Landdb.Web.ServiceContracts.Data;

namespace Landdb.Client.Services.Web {
    public class RegistrationResults {
        public RegistrationResults(UserAccount user, bool isRegistered, bool hasError = false, Exception exception = null) {
            User = user;
            IsRegistered = isRegistered;
            HasError = hasError;
            Exception = exception;
        }

        public UserAccount User { get; private set; }
        public bool IsRegistered { get; private set; }

        public Exception Exception { get; private set; }
        public bool HasError { get; private set; }
    }
}
