﻿using Landdb.Web.ServiceContracts.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.Client.Services.Web {
    public interface IRegistrationClient {
        Task<RegistrationResults> CheckUserRegistration();
        Task<DeviceSubscriptionResults> ConfirmDeviceSubscription(Guid dataSourceId, Guid deviceId);
    }
}
