﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Landdb.Client.Services.Credentials;
using System.Net;
using System.Globalization;
using System.IO;
using System.Runtime.Serialization;
using Landdb.Web.ServiceContracts.Data;
using Landdb.Client.Infrastructure;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace Landdb.Client.Services.Web {
    public class RegistrationClient : IRegistrationClient {
        private const string RegisterSignatureOperation = "checkregistration";
        private const string EnsureSubscriptionOperation = "ensure_subscription";
        private const string GetEventStoreCredsSubscriptionOperation = "eventstorecreds";

        private readonly IStorageCredentials storageCredentials;

        private readonly string serviceUri;

        public RegistrationClient(string serviceUri, IStorageCredentials storageCredentials) {
            this.serviceUri = serviceUri;
            this.storageCredentials = storageCredentials;
        }

        public async Task<RegistrationResults> CheckUserRegistration() {
            try {
                var client = new HttpClient {
                    BaseAddress = new Uri(serviceUri)
                };

                await this.storageCredentials.SetBearerTokenAsync(client);
                var response = await client.GetAsync(RegisterSignatureOperation);

                if (response.StatusCode == HttpStatusCode.OK) {
                    var json = await response.Content.ReadAsStringAsync();
                    var registration = JsonConvert.DeserializeObject<CheckRegistrationResponse>(json);

                    if (registration.IsRegistered) {
                        return new RegistrationResults(registration.AccountInformation, true);
                    }
                    else {
                        return new RegistrationResults(null, false);
                    }
                }
                else {
                    return new RegistrationResults(null, false, true, new ApplicationException(string.Format("Received a response of {0} from the registration service.", response.StatusCode)));
                }
            }
            catch (Exception ex) {
                throw new Exception("An exception was thrown from within the registration service", ex);
            }

        }

        public async Task<DeviceSubscriptionResults> ConfirmDeviceSubscription(Guid dataSourceId, Guid deviceId) {
            if(dataSourceId == Guid.Empty || deviceId == Guid.Empty) {
                return new DeviceSubscriptionResults(null, true, new ArgumentException("dataSourceId and deviceId are required"));
            }

            var client = new HttpClient {
                BaseAddress = new Uri(serviceUri)
            };

            await this.storageCredentials.SetBearerTokenAsync(client);
            var data = new DeviceSubscriptionRequest { DeviceId = deviceId, DataSourceId = dataSourceId };
            var response = await client.PostAsJsonAsync<DeviceSubscriptionRequest>(EnsureSubscriptionOperation, data);

            if (response.StatusCode == HttpStatusCode.OK) {
                var json = await response.Content.ReadAsStringAsync();
                var details = JsonConvert.DeserializeObject<SubscriptionDetails>(json);

                if (details.IsSubscriptionValid) {
                    return new DeviceSubscriptionResults(details, false, null);
                } else {
                    return new DeviceSubscriptionResults(null, true, new HttpWebException(details.InvalidSubscriptionMessage));
                }

            } else {
                return new DeviceSubscriptionResults(null, true, new ApplicationException(string.Format("Received a response of {0} from the registration service.", response.StatusCode)));
            }
        }


        public async Task GetEventStoreCredentials(Guid dataSourceId, Action<EventStoreCredentials> onResponse) {
            if (dataSourceId == Guid.Empty) {
                return;
            }

            var client = new HttpClient {
                BaseAddress = new Uri(serviceUri)
            };

            await this.storageCredentials.SetBearerTokenAsync(client);
            var data = new EventStoreCredentialsRequest { DataSourceId = dataSourceId };
            var response = await client.PostAsJsonAsync<EventStoreCredentialsRequest>(GetEventStoreCredsSubscriptionOperation, data);

            EventStoreCredentials creds = null;
            if (response.StatusCode == HttpStatusCode.OK) {
                var json = await response.Content.ReadAsStringAsync();
                creds = JsonConvert.DeserializeObject<EventStoreCredentials>(json);
            }

            onResponse(creds);
        }
    }
}
