﻿using Landdb.Web.ServiceContracts.Data;
using System;
using System.Threading.Tasks;
namespace Landdb.Client.Services.Web {
    public interface IDataSynchronizationClient {
        Task<CommandSubmissionResponse> SubmitCommand(string serializedCommand, string commandType, Guid deviceId, string dataSourceId);
    }
}
