﻿namespace Landdb.Client.Services.Web {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Landdb.Web.ServiceContracts;
    using Landdb.Web.ServiceContracts.Data;
    using System.ServiceModel;
    using System.ServiceModel.Web;
    using Landdb.Client.Account;
    using Landdb.Client.Infrastructure;
    using System.Runtime.Serialization;
    using Landdb.Client.Services.Credentials;
    using System.Net;
    using System.IO;
using NLog;
    using System.Net.Http;
    using System.Threading.Tasks;
    using Newtonsoft.Json;
    using Landdb.Client.Wires;

    public class DataSynchronizationClient : IDataSynchronizationClient {
        private const string SubmitOperation = "submit";
        private readonly IStorageCredentials storageCredentials;

        private readonly string serviceUri;
        readonly Logger log = LogManager.GetCurrentClassLogger();
        

        public DataSynchronizationClient(string serviceUri, IStorageCredentials storageCredentials) {
            this.serviceUri = serviceUri;
            this.storageCredentials = storageCredentials;
        }

        public async Task<CommandSubmissionResponse> SubmitCommand(string serializedCommand, string commandType, Guid deviceId, string dataSourceId) {
            try {
            var client = new HttpClient {
                BaseAddress = new Uri(serviceUri + @"v2/")
            };

            await this.storageCredentials.SetBearerTokenAsync(client);
            var data = new CommandSubmissionRequest { SerializedCommand = serializedCommand, CommandType = commandType, DeviceId = deviceId, DataSourceId = dataSourceId };

            var response = await client.PostAsJsonAsync<CommandSubmissionRequest>(SubmitOperation, data);

            if (response.StatusCode == HttpStatusCode.OK) {
                var json = await response.Content.ReadAsStringAsync();
                var settings = new JsonSerializerSettings() { TypeNameHandling = TypeNameHandling.All };
                var details = JsonConvert.DeserializeObject<CommandSubmissionResponse>(json, settings);
                log.Debug("Submitted command of type {0} to server. Received: {1}  Maintenance: {2}", commandType, details.CommandWasSuccessful.ToString(), details.CommandSubmissionFailureReason.ToString());

                return details;
            } else {
                string responseContent = "<unknown content>";
                try {
                    responseContent = await response.Content.ReadAsStringAsync();
                } catch { }

                string message = string.Format("Couldn't communicate with the server to submit a command. Response code: {0} {1}, Content: {2}", response.StatusCode, response.ReasonPhrase, responseContent);
                log.Warn(message);

                throw new CloudIsUnreachableException();
            }

            } catch (HttpRequestException ex) {
                string message = string.Format("Caught an HttpRequestException while trying to submit a command. Message: {0}", ex.Message);
                log.WarnException(message, ex);

                throw new CloudIsUnreachableException();
        }
    }
}
}
