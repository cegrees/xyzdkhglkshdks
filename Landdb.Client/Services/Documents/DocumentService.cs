﻿using Landdb.Client.Infrastructure;
using Landdb.Client.Models;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace Landdb.Client.Services.Documents {
    public class DocumentServiceEndpoint : IDocumentService {
        readonly string localDocumentFolderPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "AgConnections\\Labels");
        readonly string localCatalogFileName = "Catalog.xml";
        Logger log = LogManager.GetCurrentClassLogger();

        // Find this fucker and kill it and any refs in app.configs
        // DocumentService.Data.Model.IDocumentService docServiceClient = new DocumentService.Data.Model.DocumentServiceClient();
        RemoteDocumentServiceClient documentServiceClient = new RemoteDocumentServiceClient();

        public IList<ProductDocument> GetLocalDocuments(Guid productId) {
            var documents = from d in Catalog.Descendants("Document")
                            where d.Attribute("ProductId").Value == productId.ToString()
                            select new ProductDocument {
                                Id = new Guid(d.Attribute("Id").Value),
                                ProductId = new Guid(d.Attribute("ProductId").Value),
                                Type = d.Attribute("Description").Value,
                                Description = d.Attribute("Description").Value,
                                Filename = d.Attribute("Filename").Value,
                                LastUpdated = DateTime.Parse(d.Attribute("LastUpdated").Value),
                                Path = d.Attribute("Path").Value
                            };

            return documents.ToList();
        }

        public bool DocumentExistsLocally(ProductDocument document) {
            var doc = from d in Catalog.Descendants("Document")
                      where d.Attribute("Id").Value == document.Id.ToString()
                      select d;

            return doc.SingleOrDefault() == null ? false : true;
        }

        public Uri GetLocalDocumentLocation(ProductDocument document) {
            var doc = from d in Catalog.Descendants("Document")
                      where d.Attribute("Id").Value == document.Id.ToString()
                      select d.Attribute("Path").Value;

            return new Uri(doc.Single());
        }

        /// <summary>
        /// Stores the provided byte[] array to the filesystem according to the document metadata and product name.
        /// </summary>
        /// <param name="document"></param>
        /// <param name="productName"></param>
        /// <param name="documentData"></param>
        /// <returns>The full path of the stored file.</returns>
        public string StoreDocumentLocally(ProductDocument document, string productName, byte[] documentData) {
            var catalog = Catalog;

            var doc = (from d in catalog.Descendants("Document")
                       where d.Attribute("Id").Value == document.Id.ToString()
                       select d).SingleOrDefault();

            string path = Path.Combine(localDocumentFolderPath, string.Format("{0} - ({1})\\{2}", productName, document.ProductId, document.Filename));

            if (path.Length > 260) {
                // too big, give it a new filename
                var count = GetLocalDocuments(document.ProductId).Count;
                path = Path.Combine(localDocumentFolderPath,
                                    string.Format("{0} - ({1})\\{2}",
                                    productName,
                                    document.ProductId,
                                    string.Format("{0}.pdf", ++count)));
            }

            if (doc == null) {
                doc = new XElement("Document",
                        new XAttribute("Id", document.Id),
                        new XAttribute("ProductId", document.ProductId),
                        new XAttribute("Description", document.Description),
                        new XAttribute("Filename", document.Filename),
                        new XAttribute("LastUpdated", document.LastUpdated),
                        new XAttribute("Path", path)
                      );

                catalog.Root.Add(doc);
            } else {
                doc.Attribute("Description").Value = document.Description;
                doc.Attribute("Filename").Value = document.Filename;
                doc.Attribute("LastUpdated").Value = DateTime.UtcNow.ToString();

                path = doc.Attribute("Path").Value;
            }

            // save the Catalog...
            catalog.Save(Path.Combine(localDocumentFolderPath, localCatalogFileName));

            // use the path created for the Document
            if (!Directory.Exists(Path.GetDirectoryName(path))) {
                Directory.CreateDirectory(Path.GetDirectoryName(path));
            }

            FileStream fs = new FileStream(path, FileMode.Create, FileAccess.ReadWrite);
            fs.Write(documentData, 0, documentData.Length);
            fs.Close();
            fs.Dispose();

            return path;
        }

        public async Task<IList<ProductDocument>> GetRemoteDocuments(Guid productId) {
            IList<ProductDocument> documents = new List<ProductDocument>();

            try {
                var docs = await documentServiceClient.GetDocuments(productId);

                foreach (var d in docs) {
                    documents.Add(new ProductDocument() { Description = d.Description, Filename = d.Filename, Id = d.Id, LastUpdated = d.LastUpdated, ProductId = d.ProductId, Type = d.Type });
                }
            } catch (Exception ex) {
                log.ErrorException("There was a problem getting remote documents.", ex);
            }
            return documents;
        }

        public DateTime GetRemoteDocumentRevisionDate(Models.ProductDocument document) {
            return document.LastUpdated;
        }

        public async Task<byte[]> GetRemoteDocumentData(Models.ProductDocument document) {
            try {
                var encodedData = await documentServiceClient.GetDocumentData(document.Id);
                var data = Convert.FromBase64String(encodedData);

                return data;
            } catch (Exception ex) {
                log.ErrorException("There was a problem getting remote document data.", ex);

                return null;
            }
        }

        XDocument Catalog
        {
            get
            {
                var catalogPath = Path.Combine(localDocumentFolderPath, localCatalogFileName);

                if (!File.Exists(catalogPath)) {
                    string baseXml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n" +
                                     "<Documents>\r\n" +
                                     "</Documents>";

                    if (!Directory.Exists(localDocumentFolderPath)) {
                        Directory.CreateDirectory(localDocumentFolderPath);
                    }

                    StreamWriter file = new StreamWriter(catalogPath);
                    file.WriteLine(baseXml);
                    file.Close();
                    file.Dispose();
                }

                using (XmlReader xReader = XmlReader.Create(catalogPath)) {
                    return XDocument.Load(xReader);
                }
            }
        }

        [DataContract]
        public class RemoteDocumentsContainer {
            [DataMember]
            public IList<ProductDocument> Documents { get; set; }
        }

        [DataContract]
        public class RemoteDocumentContainer {
            [DataMember]
            public ProductDocument Document { get; set; }
        }

        private class RemoteDocumentServiceClient {
            public async Task<ProductDocument[]> GetDocuments(Guid productId) {
                var getDocumentDescriptorsByProductIdUrl = string.Format("{0}/products/{1}/documentdescriptors?format=json", Infrastructure.RemoteConfigurationSettings.MasterlistRemoteUri, productId);
                var webClient = new WebClient();

                webClient.Headers.Add("Authorization", "basic QWdDQVBJOlJhbmRvbU1vbmtleVN1cmZzT25MYXZhIQ==");

                var documentContainerJson = await webClient.DownloadStringTaskAsync(new Uri(getDocumentDescriptorsByProductIdUrl));
                var documents = JsonConvert.DeserializeObject<RemoteDocumentsContainer>(documentContainerJson).Documents;

                return documents.ToArray();
            }

            public async Task<string> GetDocumentData(Guid documentId) {
                var getDocumentByIdUrl = string.Format("{0}/documents/{1}?format=json", Infrastructure.RemoteConfigurationSettings.MasterlistRemoteUri, documentId);
                var webClient = new WebClient();

                webClient.Headers.Add("Authorization", "basic QWdDQVBJOlJhbmRvbU1vbmtleVN1cmZzT25MYXZhIQ==");

                var documentContainerJson = await webClient.DownloadStringTaskAsync(new Uri(getDocumentByIdUrl));
                var d = JsonConvert.DeserializeObject<RemoteDocumentContainer>(documentContainerJson);

                return d.Document.Data;
            }
        }
    }
}
