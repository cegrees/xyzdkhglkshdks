﻿using Landdb.Client.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.Client.Services.Documents {
    public interface IDocumentService {
        // Local
        IList<ProductDocument> GetLocalDocuments(Guid productId);
        bool DocumentExistsLocally(ProductDocument document);
        Uri GetLocalDocumentLocation(ProductDocument document);

        string StoreDocumentLocally(ProductDocument document, string productName, byte[] documentData);

        // Remote
        Task<IList<ProductDocument>> GetRemoteDocuments(Guid productId);
        DateTime GetRemoteDocumentRevisionDate(ProductDocument document);
        Task<byte[]> GetRemoteDocumentData(ProductDocument document);
    }
}
