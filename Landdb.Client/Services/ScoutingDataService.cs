﻿using Landdb.Client.Infrastructure;
using Landdb.Client.Models;
using Landdb.Infrastructure;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using NLog;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Landdb.Client.Services.Credentials;
using Landdb.Client.Services.Web;

namespace Landdb.Client.Services {
    public class ScoutingDataService {

        readonly string scoutingStorageLocation;

        bool isListLoaded = false;
        Task scoutingLoadTask;

        string baseCachedScoutingFileLocation;
        string scoutinglistGroup;

        DataSourceId currentDataSourceId;
        int cropYear;
        HttpClient client;
        Logger log = LogManager.GetCurrentClassLogger();

        public ScoutingDataService(string applicationDirectory, DataSourceId dsId, int cropYear, HttpClient client) {
            this.client = client;
            this.currentDataSourceId = dsId;
            this.cropYear = cropYear;
            this.scoutinglistGroup = "ScoutingData";
            //this.baseCachedScoutingFileLocation = Path.Combine(applicationDirectory, @"Resources\\Data\\", scoutinglistGroup);
            this.baseCachedScoutingFileLocation = Path.Combine(applicationDirectory, scoutinglistGroup);
            this.scoutingStorageLocation = applicationDirectory; //Path.Combine(baseCachedScoutingFileLocation, scoutinglistGroup);

            //scoutingStorageLocation = Path.Combine(this.scoutingStorageLocation, "scouting");
            scoutingList = new ScoutingList();

            scoutingLoadTask = new Task(LoadScoutingData);
            scoutingLoadTask.Start();

            if (!isListLoaded) {
                if (scoutingLoadTask.Status == TaskStatus.Running) {
                    scoutingLoadTask.Wait();
                }
            }

            GetData();
        }

        public DataSourceId CurrentDataSourceId {
            get { return currentDataSourceId; }
        }

        public ScoutingList scoutingList { get; set; }

        public async void GetData() {
            //TO DO :: ADD REVISION NUMBER OR DATE...
            var action = string.Format("crm?dataSourceId={0}&cropYear={1}", CurrentDataSourceId.Id, cropYear);

            var results = scoutingList.Results != null ? scoutingList.Results.ToList() : new List<ScoutingListItemDetails>(); // list.Results.ToList();
            ScoutingList list = await GetScoutingData<ScoutingList>(action, client);
            if (list != null) { results.AddRange(list.Results); }
            scoutingList.Results = results;

            WriteScoutingData();
        }

        private async Task<T> GetScoutingData<T>(string scoutingDocumentUri, HttpClient client) {
            try {
                var response = await client.GetAsync(scoutingDocumentUri);

                if (response.StatusCode == HttpStatusCode.OK) {
                    var json = await response.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<T>(json);
                    return result;
                } else {
                    var fake = string.Empty;
                    var result = JsonConvert.DeserializeObject<T>(fake);
                    return result;
                }
            } catch (Exception ex) {
                throw new ApplicationException("Scouting Service Info Retrieval Failed.");
            }
        }

        void LoadScoutingData() {
            log.Debug("Loading scouting...");
            var filePath = Path.Combine(scoutingStorageLocation, "scouting.txt");
            if (File.Exists(filePath)) {
                var data = File.ReadAllText(filePath);
                scoutingList = JsonConvert.DeserializeObject<ScoutingList>(data);
                //((List<MiniProduct>)scoutingList.Results).Sort((x, y) => { return x.Name.CompareTo(y.Name); });

                isListLoaded = true;
                log.Debug("Finished loading scouting.");
            }
        }

        void WriteScoutingData() {
            try {
                var filePath = Path.Combine(scoutingStorageLocation, "scouting.txt");

                JsonSerializerSettings settings = new JsonSerializerSettings() { NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore, };
                var listData = JsonConvert.SerializeObject(scoutingList, settings);
                File.WriteAllText(filePath, listData);

            } catch (Exception ex) {
                log.Debug(ex);
            }
        }
    }
}