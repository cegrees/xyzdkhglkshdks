﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Landdb.Client.Services {
    public interface IProgressTracker {
        int CurrentRecord { get; set; }
        string StatusMessage { get; set; }
        int TotalRecords { get; set; }
    }
}
