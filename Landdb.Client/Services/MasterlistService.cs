﻿using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Product;
using Landdb.Infrastructure;
using Lokad.Cqrs.AtomicStorage;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Landdb.Client.Resources;
using Landdb.Client.Resources.LocalizedLists;

namespace Landdb.Client.Services {

    // TODO: This class is getting to be a bit of a god-class.
    // Need to refactor not only to reduce the massive duplication taking place around the different
    // masterlist data types, but also to reduce the scope of this class overall. Most likely needs
    // to delegate out to a few child objects that can handle the common synchronization logic.
    public class MasterlistService : IMasterlistService {
        readonly IDocumentReader<DataSourceId, UserCreatedProductList> userProductReader;
        readonly string baseCachedMasterlistFileLocation;
        readonly string masterlistStorageLocation;
        readonly string productStorageLocation;
        readonly string cropStorageLocation;
        readonly string ingredientStorageLocation;
        readonly string reiphiStorageLocation;
        readonly string pestStorageLocation;
        readonly string dataStorageLocation;
        readonly string cropDefaultsStorageLocation;
        readonly string loadPropertiesStorageLocation;
        readonly string yieldLocationSegmentTypesStorageLocation;
        readonly string managedYieldLocationsStorageLocation;

        const string mlwProductQuery = @"{0}/products?format=json&c={1}&last={2}";
        const string mlwCropQuery = @"{0}/crops?format=json&c={1}&last={2}";
        const string mlwIngredientQuery = @"{0}/activeingredients?format=json&c={1}&last={2}";
        const string mlwReiPhiQuery = @"{0}/reiphi?format=json&c={1}&last={2}";
        const string mlwPestQuery = @"{0}/pests/?format=json&c={1}&last={2}";

        const string apiCropDefaultsQuery = @"https://www.landdb.com/api/cropdefaults/sync?culture={0}&revision={1}";
        const string apiLoadPropertiesQuery = @"https://www.landdb.com/api/loadproperties/sync?culture={0}&revision={1}";
        const string apiYieldLocationSegmentTypesQuery = @"https://www.landdb.com/api/yieldlocations/segmenttypes/sync?culture={0}&revision={1}";
        const string apiManagedYieldLocationsQuery = @"https://www.landdb.com/api/yieldlocations/managed/sync?culture={0}&revision={1}";

        CropList cropList = null;
        ProductList productList = null;
        ActiveIngredientList ingredientList = null;
        ReiPhiList reiphiList = null;
        PestList pestList = null;
        List<Pest> pestCache = null;

        StateList stateList = null;
        MeridianList meridianList = null;
        DLSList dlsList = null;
        private SoilTextureList soilTextureList;
        private WaterOriginList waterOriginList;
        private WaterSourceList waterSourceList;
        private IrrigationSystemList irrigationSystemList;
        private ApplicationMethodList applicationMethodList;
        private TimingEventList timingEventList;
        private TillageSystemList tillageSystemList;
        private EnergySourceList energySourceList;
        CropDefaultsList cropDefaultsList = null;
        LoadPropertyDefinitions loadPropertyDefinitions = null;
        YieldLocationSegmentTypesList yieldLocationSegmentTypesList = null;
        ManagedYieldLocationsList managedYieldLocationsList = null;
        Dictionary<ProductId, MiniProduct> productCache = new Dictionary<ProductId, MiniProduct>();
        Dictionary<ProductId, string> productDisplayCache = new Dictionary<ProductId, string>();
        Dictionary<CropId, string> cropDisplayCache = new Dictionary<CropId, string>();
        Dictionary<ActiveIngredientId, string> ingredientDisplayCache = new Dictionary<ActiveIngredientId, string>();

        IApplicationUpdater appUpdater;

        private string CultureToDisplay { get; set; }
        public static string DefaultAreaUnit { get; set; }

        bool isProductListLoaded = false;
        Task productLoadTask;

        bool isCropListLoaded = false;
        Task cropLoadTask;

        bool isIngredientListLoaded = false;
        Task ingredientLoadTask;

        bool isReiPhiListLoaded = false;
        Task reiPhiLoadTask;

        bool isPestListLoaded = false;
        Task pestLoadTask;

        bool isCropDefaultsListLoaded = false;
        Task cropDefaultsLoadTask;

        bool isLoadPropertiesListLoaded = false;
        Task loadPropertiesLoadTask;

        bool isYieldLocationSegmentTypesListLoaded = false;
        Task yieldLocationSegmentTypesLoadTask;

        bool isManagedYieldLocationsListLoaded = false;
        Task managedYieldLocationsLoadTask;

        Logger log = LogManager.GetCurrentClassLogger();

        string masterlistGroup;

        public MasterlistService(string applicationDirectory, string masterlistStorageLocation, IDocumentReader<DataSourceId, UserCreatedProductList> userProductReader, IApplicationUpdater appUpdater, string masterlistGroup, string currentUiCulture) {
            this.userProductReader = userProductReader;
            this.appUpdater = appUpdater;
            this.masterlistGroup = masterlistGroup;
            this.baseCachedMasterlistFileLocation = Path.Combine(applicationDirectory, @"Resources\\Data\\", masterlistGroup);
            this.dataStorageLocation = Path.Combine(applicationDirectory, @"Resources\\Data\\");
            this.masterlistStorageLocation = Path.Combine(masterlistStorageLocation, masterlistGroup);

            productStorageLocation = Path.Combine(this.masterlistStorageLocation, "products");
            cropStorageLocation = Path.Combine(this.masterlistStorageLocation, "crops");
            ingredientStorageLocation = Path.Combine(this.masterlistStorageLocation, "ingredients");
            reiphiStorageLocation = Path.Combine(this.masterlistStorageLocation, "reiphi");
            pestStorageLocation = Path.Combine(this.masterlistStorageLocation, "pest");
            cropDefaultsStorageLocation = Path.Combine(this.masterlistStorageLocation, "crop_defaults");
            loadPropertiesStorageLocation = Path.Combine(this.masterlistStorageLocation, "load_properties");
            yieldLocationSegmentTypesStorageLocation = Path.Combine(this.masterlistStorageLocation, "yield_location_segment_types");
            managedYieldLocationsStorageLocation = Path.Combine(this.masterlistStorageLocation, "managed_yield_locations");

            EnsureStorageLocationExists(this.masterlistStorageLocation);

            ScheduleLoaderTasks();
            FindAndSetCultureToDisplay(currentUiCulture);
            SetSustainabilityLists();
            SetLegalLists();
        }

        void ScheduleLoaderTasks() {
            productLoadTask = new Task(LoadProductList);
            productLoadTask.Start();

            cropLoadTask = new Task(LoadCropList);
            cropLoadTask.Start();

            ingredientLoadTask = new Task(LoadIngredientList);
            ingredientLoadTask.Start();

            reiPhiLoadTask = new Task(LoadReiPhiList);
            reiPhiLoadTask.Start();

            pestLoadTask = new Task(LoadPestList);
            pestLoadTask.Start();

            cropDefaultsLoadTask = new Task(LoadCropDefaultsList);
            cropDefaultsLoadTask.Start();

            loadPropertiesLoadTask = new Task(LoadLoadPropertiesList);
            loadPropertiesLoadTask.Start();

            yieldLocationSegmentTypesLoadTask = new Task(LoadYieldLocationSegmentTypesList);
            yieldLocationSegmentTypesLoadTask.Start();

            managedYieldLocationsLoadTask = new Task(LoadManagedYieldLocationsList);
            managedYieldLocationsLoadTask.Start();

            if (NetworkStatus.IsInternetAvailableFast()) {
                var productSyncTask = SynchronizeProductListAsync();
                var cropSyncTask = SynchronizeCropListAsync();
                var ingredientSyncTask = SynchronizeIngredientListAsync();
                var reiphiSyncTask = SynchronizeReiPhiListAsync();

                var cropDefaultsSyncTask = SynchronizeCropDefaultsListAsync();
                var loadPropsSyncTask = SynchronizeLoadPropertiesListAsync();
                var segmentTypesSyncTask = SynchronizeYieldLocationSegmentTypeListAsync();
                var managedLocsSyncTask = SynchronizeManagedYieldLocationsListAsync();

                var pestSyncTask = SynchronizePestList();
            }
        }

        public void FindAndSetCultureToDisplay(string forcedCulture = "en-US") {
            CultureToDisplay = forcedCulture;
            System.Threading.Thread.CurrentThread.CurrentUICulture = new CultureInfo(forcedCulture);
            System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo(forcedCulture);
            Strings.Culture = new CultureInfo(forcedCulture);
        }

        public void SetSustainabilityLists() {
            var sustainabilityLists = new SustainabilityLists(CultureToDisplay);

            //TODO: if (soilTextureList == null || soilTextureList.SoilTextures == null || soilTextureList.SoilTextures.Any()) {
            soilTextureList = new SoilTextureList { SoilTextures = sustainabilityLists.GetSoilTextureListForCulture() };
            waterOriginList = new WaterOriginList { WaterOrigins = sustainabilityLists.GetWaterOriginListForCulture() };
            irrigationSystemList = new IrrigationSystemList { IrrigationSystems = sustainabilityLists.GetIrrigationSystemListForCulture()};
            tillageSystemList = new TillageSystemList { TillageSystems = sustainabilityLists.GetTillageSystemListForCulture()};
            waterSourceList = new WaterSourceList { WaterSources = sustainabilityLists.GetWaterSourceListForCulture() };
            applicationMethodList = new ApplicationMethodList { ApplicationMethods = sustainabilityLists.GetApplicationMethodListForCulture()};
            timingEventList = new TimingEventList { TimingEvents = sustainabilityLists.GetTimingEventListForCulture()};
            energySourceList = new EnergySourceList { EnergySources = sustainabilityLists.GetEnergySourceListForCulture()};
        }

        public void SetLegalLists() {
            var legalLists = new LegalLists(CultureToDisplay);

            //TODO: Null checking, similar to the way it's done in SetSustainabilityLists()
            meridianList = new MeridianList {Meridians = legalLists.GetMeridianListForCulture()};
            dlsList = new DLSList{Dominions = legalLists.GetDominionListForCultureAsMeridians()}; 
            //for some reason, in the metadata, "DLSList" contains a list of type "Meridian" called "Dominions" - even though there is a "Dominion" data type...
            //once we change that to be a list of type Dominion, then we can just change the dlsList assignment to to "GetDominionListForCulture()" and delete all of the 
            //"DominionListAsMeridians" code in LegalLists.cs
        }

        public IEnumerable<MiniProduct> GetProductList() {
            EnsureProductListIsLoaded();
            return productList.Products.Where(x => x.Status.ToLower() == "active");
        }

        public IEnumerable<Pest> GetPestList() {
            EnsurePestListIsLoaded();
            return pestCache;
            //var q = from mlp in pestList.Pests
            //        let laName = mlp.LatinNames.OrderBy(x => x.Id).FirstOrDefault() ?? new MasterlistPestTranslationItem() { Id = 0, Name = null }
            //        let genNames = mlp.GeneralNames.Any() ? mlp.GeneralNames : new List<MasterlistPestTranslationItem>() { new MasterlistPestTranslationItem() { Id = laName.Id, Name = laName.Name } }
            //        from genName in genNames
            //        select new Pest() { Id = genName.Id, Name = genName.Name, LatinName = laName.Name };

            //return q;
        }

        public IEnumerable<MiniCrop> GetCropList() {
            EnsureCropListIsLoaded();
            return cropList.Crops.Where(x => x.Status.ToLower() == "active");
        }

        public IEnumerable<ActiveIngredient> GetActiveIngredientList() {
            EnsureIngredientListIsLoaded();
            return ingredientList.ActiveIngredients;
        }

        public IEnumerable<State> GetStateList() {
            EnsureStateListIsLoaded();
            return stateList.USStates;
        }

        public IEnumerable<Meridian> GetMeridianList() {
            return meridianList.Meridians;
        }

        public IEnumerable<Meridian> GetDominionList()
        {
            return dlsList.Dominions;
        }

        public IEnumerable<SoilTexture> GetSoilTextureList() {
            return soilTextureList.SoilTextures;
        }

        public IEnumerable<WaterSource> GetWaterSourceList() {
            return waterSourceList.WaterSources;
        }

        public IEnumerable<WaterOrigin> GetWaterOriginList() {
            return waterOriginList.WaterOrigins;
        }

        public IEnumerable<IrrigationSystem> GetIrrigationSystemList() {
            return irrigationSystemList.IrrigationSystems;
        }

        public IEnumerable<ApplicationMethod> GetApplicationMethodList() {
            return applicationMethodList.ApplicationMethods;
        }

        public IEnumerable<TimingEvent> GetTimingEventList() {
            return timingEventList.TimingEvents;
        }

        public IEnumerable<TillageSystem> GetTillageSystemList() {
            return tillageSystemList.TillageSystems;
        }

        public IEnumerable<EnergySource> GetEnergySourceList() {
            return energySourceList.EnergySources;
        }

        public LoadPropertyDefinitions GetLoadPropertyDefinitions() {
            EnsureLoadPropertyDefinitionsAreLoaded();
            return loadPropertyDefinitions;
        }

        public YieldLocationSegmentTypesList GetYieldLocationSegmentTypes() {
            EnsureYieldLocationSegmentTypesListIsLoaded();
            return yieldLocationSegmentTypesList;
        }

        public ManagedYieldLocationsList GetManagedYieldLocations() {
            EnsureManagedYieldLocationsListIsLoaded();
            return managedYieldLocationsList;
        }

        public IEnumerable<ReiPhi> GetProductReiPhis(ProductId productId) {
            EnsureReiPhiListIsLoaded();
            var q = from r in reiphiList.ProductReiPhi
                    from i in r.ReiPhi
                    where r.Prod == productId.Id
                    select i;
            return q;
        }

        private MiniProduct GetProduct(Guid productId) {
            EnsureProductListIsLoaded();
            lock (productList.Products) {
                var pId = new ProductId(productId);
                if (productCache.ContainsKey(pId)) {
                    return productCache[pId];
                }

                var q = from p in productList.Products
                        where p.Id == productId
                        select p;
                var product = q.FirstOrDefault();
                if (product != null) {
                    productCache.Add(pId, product);
                }
                return product;
            }
        }

        public MiniProduct GetProduct(ProductId productId) {
            if (productCache.ContainsKey(productId)) {
                return productCache[productId];
            }

            if (productId.DataSourceId == GlobalIdentifiers.GetMasterlistDataSourceId()) {
                var p = GetProduct(productId.Id);
                if (p != null) { return p; }
            }

            // fallback to user-created products if not in cache, and not retrievable via masterlist ID
            // this should handle the case where someone boned up and passed a UCP with a ML-DS identifier -bs
            var list = userProductReader.Get(new DataSourceId(productId.DataSourceId)).GetValue(new UserCreatedProductList());
            var mp = list.UserCreatedProducts.FirstOrDefault(x => x.Id == productId.Id);
            if (mp != null) {
                productCache.Add(productId, mp);
            }
            return mp;
        }

        public MiniCrop GetCrop(CropId cropId) {
            if (cropId == null) { return null; }

            EnsureCropListIsLoaded();

            var c = cropList.Crops.Where(x => x.Id == cropId.Id).FirstOrDefault();
            return c;
        }

        public Pest GetPest(uint pestId) {
            EnsurePestListIsLoaded();

            var pest = (from p in GetPestList()
                        where p.Id == pestId
                        select p).FirstOrDefault();

            return pest;
        }

        public MiniPest GetPest(uint pestId, string commonName, string latinName)
        {
            EnsurePestListIsLoaded();
            commonName = string.IsNullOrEmpty(commonName) ? string.Empty : commonName;
            latinName = string.IsNullOrEmpty(latinName) ? string.Empty : latinName;

            var pests = (from p in GetPestList()
                        where p.Id == pestId || p.Name == commonName || p.LatinName == latinName
                        select new MiniPest() { id = p.Id, LatinList = p.LatinName, Name = p.Name });

            var fullMatch = (from p in pests
                             where p.id == pestId && p.Name == commonName && p.LatinList == latinName
                             select p);

            var namesMatch = (from p in pests
                              where p.Name == commonName && p.LatinList == latinName
                              select p);

            var singleNameMatch = (from p in pests
                                   where p.Name == commonName || p.LatinList == latinName
                                   select p);
            if (fullMatch.Any()) { return fullMatch.FirstOrDefault(); }
            if (namesMatch.Any()) { return namesMatch.FirstOrDefault(); }
            if(singleNameMatch.Any()) { return singleNameMatch.FirstOrDefault(); }

            return new MiniPest();
        }


        /// <summary>
        /// GetStateByName will handle lookups by abbreviation.
        /// </summary>
        /// <param name="stateAbbreviation"></param>
        /// <returns></returns>
        [Obsolete]
		public State GetStateByAbbreviation(string stateAbbreviation) {
			if (string.IsNullOrWhiteSpace(stateAbbreviation)) { return null; }

			EnsureStateListIsLoaded();

			var state = from p in stateList.USStates
						where p.Abbreviation == stateAbbreviation
						select p;
			return state.FirstOrDefault();
		}

		public State GetStateByName(string stateName) {
			if (string.IsNullOrWhiteSpace(stateName)) { return null; }

			EnsureStateListIsLoaded();

			var state = from p in stateList.USStates
						where p.Name.ToLower() == stateName.ToLower()
							|| p.Abbreviation.ToLower() == stateName.ToLower()
						select p;
			return state.FirstOrDefault();
		}

		public County GetCountyByStateAndName(string countyName, string stateName) {
			if (string.IsNullOrWhiteSpace(countyName) || string.IsNullOrWhiteSpace(stateName)) { return null; }

			EnsureStateListIsLoaded();

			var county = from p in stateList.USStates
						 where p.Name.ToLower() == stateName.ToLower()
							 || p.Abbreviation.ToLower() == stateName.ToLower()
						 from j in p.Counties
						 where j.Name.ToLower() == countyName.ToLower()
						 select j;

			return county.FirstOrDefault();
		}

		public Meridian GetMeridianByName(string name) {

			var meridian = from p in meridianList.Meridians
						   where p.Name == name
						   select p;

			return meridian.FirstOrDefault();
		}

        public Meridian GetDominionByName(string name)
        {
            var meridian = from p in dlsList.Dominions
                           where p.Name == name
                           select p;

            return meridian.FirstOrDefault();
        }

		public WaterSource GetWaterSourceByName(string source) {
			var waterSource = from p in waterSourceList.WaterSources
							  where p.Name == source
							  select p;

			return waterSource.FirstOrDefault();
		}

		public WaterOrigin GetWaterOriginByName(string origin) {
			var waterOrigin = from p in waterOriginList.WaterOrigins
							  where p.Name == origin
							  select p;

			return waterOrigin.FirstOrDefault();
		}

		public IrrigationSystem GetIrrigationSystemByName(string system) {
			var irrigation = from p in irrigationSystemList.IrrigationSystems
							 where p.Name == system
							 select p;

			return irrigation.FirstOrDefault();
		}

		public SoilTexture GetSoilTextureByName(string textureName) {
            var texture = from p in soilTextureList.SoilTextures
						  where p.Name == textureName
						  select p;

			return texture.FirstOrDefault();
		}

		public TillageSystem GetTillageSystemByName(string name) {
			var tillage = from p in tillageSystemList.TillageSystems
						  where p.Name == name
						  select p;

			return tillage.FirstOrDefault();
		}

		public EnergySource GetEnergySourceByName(string source) {
			var energySource = from p in energySourceList.EnergySources
							   where p.Name == source
							   select p;

			return energySource.FirstOrDefault();
		}

		public string GetProductDisplay(ProductId productId) {
			if (productDisplayCache.ContainsKey(productId)) {
				return productDisplayCache[productId];
			}

			EnsureProductListIsLoaded();

			string name = null;
			var p = GetProduct(productId);
			if (p != null) {
				name = string.Format("{0} ({1})", p.Name, p.Manufacturer);
			} else {
				name = "-Unknown Product-";
			}

			//Added try / catch to solve crash when adding plan to a new datasource.  
			try { productDisplayCache.Add(productId, name); }// This might be premature optimization, but seems like it'd help since most growers only use a handful of products
			catch (Exception ex) { }

			return name;
		}

		public string GetProductManufacturer(ProductId productId) {
			EnsureProductListIsLoaded();

			string name = null;
			var p = GetProduct(productId);
			if (p != null) {
				name = p.Manufacturer;
			} else {
				name = "-Unknown Manufacturer-";
			}
			return name;
		}

		public string GetProductName(ProductId productId) {
			EnsureProductListIsLoaded();

			string name = null;
			var p = GetProduct(productId);
			if (p != null) {
				name = p.Name;
			} else {
				name = "-Unknown Product-";
			}

			return name;
		}

		public string GetCropDisplay(CropId cropId) {
			if (cropId == null) { return "-Unknown Crop-"; }

			if (cropDisplayCache.ContainsKey(cropId)) {
				return cropDisplayCache[cropId];
			}

			EnsureCropListIsLoaded();

			string name = string.Empty;
			// NOTE: Need a better way to handle duplicate IDs in the future, if they crop up. Current problem is with Almond and Cotton : Pima -bs 4/12/2013
			try {
				name = (from c in cropList.Crops
						where c.Id == cropId.Id
						select c.Name).SingleOrDefault();
			} catch (InvalidOperationException ex) {
				log.ErrorException("Selecting crop with SingleOrDefault failed. Likely a duplicate ID. Please let a developer know!", ex);
				name = "**" + (from c in cropList.Crops
							   where c.Id == cropId.Id
							   select c.Name).FirstOrDefault();
			}
			if (string.IsNullOrWhiteSpace(name)) { name = "-Unknown Crop-"; }
			cropDisplayCache.Add(cropId, name);  // This might be premature optimization, but seems like it'd help since most growers only use a handful of crops
			return name;
		}

		public string GetActiveIngredientDisplay(ActiveIngredientId ingredientId) {
			if (ingredientId == null) { return "-Unknown Ingredient-"; }

			if (ingredientDisplayCache.ContainsKey(ingredientId)) {
				return ingredientDisplayCache[ingredientId];
			}

			EnsureIngredientListIsLoaded();

			string name = (from ai in ingredientList.ActiveIngredients
						   where ai.Id == ingredientId.Id
						   select ai.Name).SingleOrDefault();

			if (string.IsNullOrWhiteSpace(name)) { name = "-Unknown Ingredient-"; }
			ingredientDisplayCache.Add(ingredientId, name);
			return name;
		}

		public CropDefault GetCropDefaultsForCrop(CropId cropId) {
			if (cropId == null) { return null; }

			EnsureCropDefaultsListIsLoaded();
			return cropDefaultsList.CropDefaults.SingleOrDefault(x => x.CropId == cropId.Id);
		}

		void EnsureCropListIsLoaded() {
			if (!isCropListLoaded) {
				if (cropLoadTask.Status == TaskStatus.Running) {
					cropLoadTask.Wait();
				}
			}
		}

		void LoadCropList() {
			var filePath = Path.Combine(cropStorageLocation, "crops.txt");
			var data = File.ReadAllText(filePath);

			cropList = JsonConvert.DeserializeObject<CropList>(data);
			((List<MiniCrop>)cropList.Crops).Sort((x, y) => { return x.Name.CompareTo(y.Name); });
			isCropListLoaded = true;
		}

		void EnsureIngredientListIsLoaded() {
			if (!isIngredientListLoaded) {
				if (ingredientLoadTask.Status == TaskStatus.Running) {
					ingredientLoadTask.Wait();
				}
			}
		}

		void LoadIngredientList() {
			var filePath = Path.Combine(ingredientStorageLocation, "activeingredients.txt");
			var data = File.ReadAllText(filePath);

			ingredientList = JsonConvert.DeserializeObject<ActiveIngredientList>(data);
			((List<ActiveIngredient>)ingredientList.ActiveIngredients).Sort((x, y) => { return x.Name.CompareTo(y.Name); });
			isIngredientListLoaded = true;
		}

		void EnsureProductListIsLoaded() {
			if (!isProductListLoaded) {
				if (productLoadTask.Status == TaskStatus.Running) {
					productLoadTask.Wait();
				}
			}
		}

		void EnsureReiPhiListIsLoaded() {
			if (!isReiPhiListLoaded) {
				if (reiPhiLoadTask.Status == TaskStatus.Running) {
					reiPhiLoadTask.Wait();
				}
			}
		}

		void EnsurePestListIsLoaded() {
			if (!isPestListLoaded) {
				if (pestLoadTask.Status == TaskStatus.Running) {
					pestLoadTask.Wait();
				}
			}
            if (pestCache == null) {
                //pestCache = (from mlp in pestList.Pests
                //             let laName = mlp.LatinNames.OrderBy(x => x.Id).FirstOrDefault() ?? new MasterlistPestTranslationItem() { Id = 0, Name = null }
                //             let genNames = mlp.GeneralNames.Any() ? mlp.GeneralNames : new List<MasterlistPestTranslationItem>() { new MasterlistPestTranslationItem() { Id = laName.Id, Name = laName.Name } }
                //             from genName in genNames
                //             select new Pest() { Id = genName.Id, Name = genName.Name, LatinName = laName.Name }).ToList();
                pestCache = (from mlp in pestList.Pests
                             let laNames = mlp.LatinNames.Any() ? mlp.LatinNames.OrderBy(x => x.Id).ToList() : new List<MasterlistPestTranslationItem>() { new MasterlistPestTranslationItem() { Id = 0, Name = string.Empty } }
                             let genNames = mlp.GeneralNames.Any() ? mlp.GeneralNames : new List<MasterlistPestTranslationItem>() { new MasterlistPestTranslationItem() { Id = 0, Name = string.Empty } }
                             from genName in genNames
                             from laName in laNames
                             select new Pest() { Id = mlp.Id, Name = genName.Name, LatinName = laName.Name }).ToList();
            }
		}

		void EnsureCropDefaultsListIsLoaded() {
			if (!isCropDefaultsListLoaded) {
				if (cropDefaultsLoadTask.Status == TaskStatus.Running) {
					cropDefaultsLoadTask.Wait();
				}
			}
		}

		void EnsureLoadPropertyDefinitionsAreLoaded() {
			if (!isLoadPropertiesListLoaded) {
				if (loadPropertiesLoadTask.Status == TaskStatus.Running) {
					loadPropertiesLoadTask.Wait();
				}
			}
		}

		void EnsureYieldLocationSegmentTypesListIsLoaded() {
			if (!isYieldLocationSegmentTypesListLoaded) {
				if (yieldLocationSegmentTypesLoadTask.Status == TaskStatus.Running) {
					yieldLocationSegmentTypesLoadTask.Wait();
				}
			}
		}

		void EnsureManagedYieldLocationsListIsLoaded() {
			if (!isManagedYieldLocationsListLoaded) {
				if (managedYieldLocationsLoadTask.Status == TaskStatus.Running) {
					managedYieldLocationsLoadTask.Wait();
				}
			}
		}

		void LoadProductList() {
			log.Debug("Loading products...");
			var filePath = Path.Combine(productStorageLocation, "products.txt");
			var data = File.ReadAllText(filePath);
			productList = JsonConvert.DeserializeObject<ProductList>(data);
			((List<MiniProduct>)productList.Products).Sort((x, y) => { return x.Name.CompareTo(y.Name); });
            foreach (var x in productList.Products)
            {
                if (x.RegistrationNumber == null) { }
                else
                {
                    Regex epaNumberRegex = new Regex("([1-9][0-9]*)");

                    var matches = from m in epaNumberRegex.Matches(x.RegistrationNumber).Cast<Match>()
                                  select m.Value;
                    x.RegistrationNumber = string.Join("-", matches.ToArray());
                }
            }

			isProductListLoaded = true;
			log.Debug("Finished loading products.");
		}

		void LoadReiPhiList() {
			var filePath = Path.Combine(reiphiStorageLocation, "reiphi.txt");
			var data = File.ReadAllText(filePath);

			reiphiList = JsonConvert.DeserializeObject<ReiPhiList>(data);
			isReiPhiListLoaded = true;

		}

		void LoadPestList() {
			var filePath = Path.Combine(pestStorageLocation, "pests.txt");
			var data = File.ReadAllText(filePath);

			pestList = JsonConvert.DeserializeObject<PestList>(data);
			isPestListLoaded = true;
		}

		void LoadCropDefaultsList() {
			var filePath = Path.Combine(cropDefaultsStorageLocation, "crop_defaults.txt");
			var data = File.ReadAllText(filePath);

			cropDefaultsList = JsonConvert.DeserializeObject<CropDefaultsList>(data);
			isCropDefaultsListLoaded = true;
		}

		void LoadLoadPropertiesList() {
			var filePath = Path.Combine(loadPropertiesStorageLocation, "load_properties.txt");
			var data = File.ReadAllText(filePath);

			loadPropertyDefinitions = JsonConvert.DeserializeObject<LoadPropertyDefinitions>(data);
			isLoadPropertiesListLoaded = true;
		}

		void LoadYieldLocationSegmentTypesList() {
			var filePath = Path.Combine(yieldLocationSegmentTypesStorageLocation, "yield_location_segment_types.txt");
			var data = File.ReadAllText(filePath);

			yieldLocationSegmentTypesList = JsonConvert.DeserializeObject<YieldLocationSegmentTypesList>(data);
			isYieldLocationSegmentTypesListLoaded = true;
		}

		void LoadManagedYieldLocationsList() {
			var filePath = Path.Combine(managedYieldLocationsStorageLocation, "managed_yield_locations.txt");
			var data = File.ReadAllText(filePath);

			managedYieldLocationsList = JsonConvert.DeserializeObject<ManagedYieldLocationsList>(data);
			isManagedYieldLocationsListLoaded = true;
		}

		void EnsureStateListIsLoaded() {
			if (stateList == null || stateList.USStates == null || !stateList.USStates.Any()) {
                //var filePath = Path.Combine(dataStorageLocation, "State_County_min.txt");
                var filePath = Path.Combine(baseCachedMasterlistFileLocation, "states.txt");
				var data = File.ReadAllText(filePath);

				stateList = JsonConvert.DeserializeObject<StateList>(data);
				((List<State>)stateList.USStates).Sort((x, y) => { return x.Name.CompareTo(y.Name); });
			}
		}

		void EnsureStorageLocationExists(string baseStorageLocation) {
			if (!Directory.Exists(baseStorageLocation)) {
				Directory.CreateDirectory(baseStorageLocation);
			}

			if (!Directory.Exists(productStorageLocation)) {
				Directory.CreateDirectory(productStorageLocation);
			}

			if (!Directory.Exists(cropStorageLocation)) {
				Directory.CreateDirectory(cropStorageLocation);
			}

			if (!Directory.Exists(ingredientStorageLocation)) {
				Directory.CreateDirectory(ingredientStorageLocation);
			}

			if (!Directory.Exists(reiphiStorageLocation)) {
				Directory.CreateDirectory(reiphiStorageLocation);
			}

			if (!Directory.Exists(pestStorageLocation)) {
				Directory.CreateDirectory(pestStorageLocation);
			}

			if (!Directory.Exists(cropDefaultsStorageLocation)) {
				Directory.CreateDirectory(cropDefaultsStorageLocation);
			}

			if (!Directory.Exists(loadPropertiesStorageLocation)) {
				Directory.CreateDirectory(loadPropertiesStorageLocation);
			}

			if (!Directory.Exists(yieldLocationSegmentTypesStorageLocation)) {
				Directory.CreateDirectory(yieldLocationSegmentTypesStorageLocation);
			}

			if (!Directory.Exists(managedYieldLocationsStorageLocation)) {
				Directory.CreateDirectory(managedYieldLocationsStorageLocation);
			}

			if (!Directory.Exists(baseCachedMasterlistFileLocation) || !File.Exists(Path.Combine(baseCachedMasterlistFileLocation, "products_rev.txt"))) {
				if (appUpdater != null) {
					log.Info("Masterlist directory doesn't exist. Getting optional filegroup.");
					bool updated = false;
					string masterlistFilegroup = string.Format("Masterlist_{0}", masterlistGroup);
					appUpdater.GetOptionalFilegroup(masterlistFilegroup, null, () => { updated = true; });

					while (!updated) {
						System.Threading.Thread.Sleep(500);
					}
				} else {
					log.Warn("Application updater object was null. Can't retrieve optional masterlist packages.");
				}
			}

			// Setup products if they don't exist
			InitializeLocalCache(productStorageLocation, "products");
			InitializeLocalCache(cropStorageLocation, "crops");
			InitializeLocalCache(ingredientStorageLocation, "activeingredients");
			InitializeLocalCache(reiphiStorageLocation, "reiphi");
			InitializeLocalCache(pestStorageLocation, "pests");
			InitializeLocalCache(cropDefaultsStorageLocation, "crop_defaults");
			InitializeLocalCache(loadPropertiesStorageLocation, "load_properties");
			InitializeLocalCache(yieldLocationSegmentTypesStorageLocation, "yield_location_segment_types");
			InitializeLocalCache(managedYieldLocationsStorageLocation, "managed_yield_locations");
		}

		void InitializeLocalCache(string storageLocation, string cacheName) {
			var dest = Path.Combine(storageLocation, $"{cacheName}.txt");
			if (!File.Exists(dest)) {
				CopyListFromResourceData(storageLocation, cacheName);
			} else {
				var revisionFileName = $"{cacheName}_rev.txt";
				var currentVersionPath = Path.Combine(storageLocation, revisionFileName);
				if (!File.Exists(currentVersionPath)) {
					CopyListFromResourceData(storageLocation, cacheName, true);
				} else {
					var currentVersionData = ListRevisionData.ReadFromFile(currentVersionPath);
					int currentVersion = currentVersionData.DataRevision;
					int currentStructureVersion = currentVersionData.StructureRevision;

					var resourceVersionPath = Path.Combine(baseCachedMasterlistFileLocation, revisionFileName);
					var resourceVersionData = ListRevisionData.ReadFromFile(resourceVersionPath);
					int resourceVersion = resourceVersionData.DataRevision;
					int resourceStructureVersion = resourceVersionData.StructureRevision;

					if (resourceStructureVersion > currentStructureVersion || resourceVersion > currentVersion) { CopyListFromResourceData(storageLocation, cacheName, true); }
				}
			}
		}

		#region Synchronization
		void CopyListFromResourceData(string storageLocation, string cacheName, bool overwrite = false) {
			var cacheFileName = $"{cacheName}.txt";
			var resourcePath = Path.Combine(baseCachedMasterlistFileLocation, cacheFileName);
			var dest = Path.Combine(storageLocation, cacheFileName);
			File.Copy(resourcePath, dest, overwrite);

			cacheFileName = $"{cacheName}_rev.txt";
			resourcePath = Path.Combine(baseCachedMasterlistFileLocation, cacheFileName);
			dest = Path.Combine(storageLocation, cacheFileName);
			File.Copy(resourcePath, dest, overwrite);
		}

		async Task SynchronizeProductListAsync() {
			try {
				WebClient client = new WebClient();
                client.Headers.Add("Authorization", "basic QWdDQVBJOlJhbmRvbU1vbmtleVN1cmZzT25MYXZhIQ==");
                client.Headers.Add("Content-Type", "application/json; charset=utf-8");
                client.Encoding = System.Text.Encoding.UTF8;
                var versionPath = Path.Combine(productStorageLocation, "products_rev.txt");
				var versionData = ListRevisionData.ReadFromFile(versionPath);
				int version = versionData.DataRevision;

                //string url = string.Format(mlwProductQueryBeta, version);
                string url = string.Format(mlwProductQuery, RemoteConfigurationSettings.MasterlistRemoteUri, masterlistGroup, version);
				var newData = await client.DownloadStringTaskAsync(new Uri(url));

				var newProductList = JsonConvert.DeserializeObject<ProductList>(newData);
				if (!newProductList.Products.Any()) { return; } // No products, nothing to do.

				EnsureProductListIsLoaded();
				var pList = (List<MiniProduct>)productList.Products;

				lock (pList) {
					foreach (var newProduct in newProductList.Products) {
						int removedCount = pList.RemoveAll(x => x.Id == newProduct.Id);
						pList.Add(newProduct);
					}
				}
				((List<MiniProduct>)productList.Products).Sort((x, y) => { return x.Name.CompareTo(y.Name); });

				var productFileLocation = Path.Combine(productStorageLocation, "products.txt");
				productList.CurrentRevision = newProductList.CurrentRevision;

				JsonSerializerSettings settings = new JsonSerializerSettings() { NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore, };
				var listData = JsonConvert.SerializeObject(productList, settings);
				File.WriteAllText(productFileLocation, listData);


				versionData.DataRevision = newProductList.CurrentRevision;
				versionData.SaveToFile(versionPath);
			} catch (Exception ex) {
				log.ErrorException("Encountered a problem while synchronizing masterlist products", ex);
			}
		}

        async Task SynchronizeCropListAsync() {
			try {
				WebClient client = new WebClient();
				client.Headers.Add("Authorization", "basic QWdDQVBJOlJhbmRvbU1vbmtleVN1cmZzT25MYXZhIQ==");
                client.Headers.Add("Content-Type", "application/json; charset=utf-8");
                client.Encoding = System.Text.Encoding.UTF8;
                var versionPath = Path.Combine(cropStorageLocation, "crops_rev.txt");
				var versionData = ListRevisionData.ReadFromFile(versionPath);
				int version = versionData.DataRevision;

                string url = string.Format(mlwCropQuery, RemoteConfigurationSettings.MasterlistRemoteUri, masterlistGroup, version);
				var newData = await client.DownloadStringTaskAsync(new Uri(url));

				var newCropList = JsonConvert.DeserializeObject<CropList>(newData);
				if (!newCropList.Crops.Any()) { return; } // No crops, nothing to do.

				EnsureCropListIsLoaded();
				var cList = (List<MiniCrop>)cropList.Crops;

				int updatedCount = 0;
				foreach (var newCrop in newCropList.Crops) {
					int removedCount = cList.RemoveAll(x => x.Id == newCrop.Id);
					updatedCount += removedCount;
					cList.Add(newCrop);
				}
				log.Info("Added {0} crops. Updated {1} crops.", newCropList.Crops.Count() - updatedCount, updatedCount);

				((List<MiniCrop>)cropList.Crops).Sort((x, y) => { return x.Name.CompareTo(y.Name); });

				var cropFileLocation = Path.Combine(cropStorageLocation, "crops.txt");
				cropList.CurrentRevision = newCropList.CurrentRevision;

				JsonSerializerSettings settings = new JsonSerializerSettings() { NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore };
				var listData = JsonConvert.SerializeObject(cropList, settings);
				File.WriteAllText(cropFileLocation, listData);

				versionData.DataRevision = newCropList.CurrentRevision;
				versionData.SaveToFile(versionPath);
			} catch (Exception ex) {
				log.ErrorException("Encountered a problem while synchronizing masterlist crops", ex);
			}
		}

        async Task SynchronizeIngredientListAsync() {
			try {
				WebClient client = new WebClient();
				client.Headers.Add("Authorization", "basic QWdDQVBJOlJhbmRvbU1vbmtleVN1cmZzT25MYXZhIQ==");
                client.Headers.Add("Content-Type", "application/json; charset=utf-8");
                client.Encoding = System.Text.Encoding.UTF8;
                var versionPath = Path.Combine(ingredientStorageLocation, "activeingredients_rev.txt");
				var versionData = ListRevisionData.ReadFromFile(versionPath);
				int version = versionData.DataRevision;

				string url = string.Format(mlwIngredientQuery, RemoteConfigurationSettings.MasterlistRemoteUri, masterlistGroup, version);
				var newData = await client.DownloadStringTaskAsync(new Uri(url));

				var newIngredientList = JsonConvert.DeserializeObject<ActiveIngredientList>(newData);
				if (!newIngredientList.ActiveIngredients.Any()) { return; } // No ingredients, nothing to do.

				log.Info("Found {0} new/updated ingredients. Syncing now...", newIngredientList.ActiveIngredients.Count());

				EnsureIngredientListIsLoaded();
				var iList = (List<ActiveIngredient>)ingredientList.ActiveIngredients;

				int updatedCount = 0;
				foreach (var newIngredient in newIngredientList.ActiveIngredients) {
					int removedCount = iList.RemoveAll(x => x.Id == newIngredient.Id);
					updatedCount += removedCount;
					iList.Add(newIngredient);
				}
				log.Info("Added {0} ingredients. Updated {1} ingredients.", newIngredientList.ActiveIngredients.Count() - updatedCount, updatedCount);

				((List<ActiveIngredient>)ingredientList.ActiveIngredients).Sort((x, y) => { return x.Name.CompareTo(y.Name); });

				var ingredientFileLocation = Path.Combine(ingredientStorageLocation, "activeingredients.txt");
				ingredientList.CurrentRevision = newIngredientList.CurrentRevision;

				JsonSerializerSettings settings = new JsonSerializerSettings() { NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore };
				var listData = JsonConvert.SerializeObject(ingredientList, settings);
				File.WriteAllText(ingredientFileLocation, listData);

				versionData.DataRevision = newIngredientList.CurrentRevision;
				versionData.SaveToFile(versionPath);

				log.Info("Wrote new ingredients list to {0}", ingredientFileLocation);
			} catch (Exception ex) {
				log.ErrorException("Encountered a problem while synchronizing masterlist active ingredients", ex);
			}
		}

        async Task SynchronizeReiPhiListAsync() {
			try {
				WebClient client = new WebClient();
				client.Headers.Add("Authorization", "basic QWdDQVBJOlJhbmRvbU1vbmtleVN1cmZzT25MYXZhIQ==");
                client.Headers.Add("Content-Type", "application/json; charset=utf-8");
                client.Encoding = System.Text.Encoding.UTF8;
                var versionPath = Path.Combine(reiphiStorageLocation, "reiphi_rev.txt");
				var versionData = File.ReadAllText(versionPath);
				int version = JsonConvert.DeserializeObject<int>(versionData);

				string url = string.Format(mlwReiPhiQuery, RemoteConfigurationSettings.MasterlistRemoteUri, masterlistGroup, version);
				var newData = await client.DownloadStringTaskAsync(new Uri(url));

				var newReiPhiList = JsonConvert.DeserializeObject<ReiPhiList>(newData);
				if (!newReiPhiList.ProductReiPhi.Any()) { return; } // No rei/phi, nothing to do.

				EnsureReiPhiListIsLoaded();
				var iList = (List<ProductReiPhi>)reiphiList.ProductReiPhi;

				int updatedCount = 0;
				foreach (var newReiPhi in newReiPhiList.ProductReiPhi) {
					int removedCount = iList.RemoveAll(x => x.Prod == newReiPhi.Prod);
					updatedCount += removedCount;
					iList.Add(newReiPhi);
				}
				log.Info("Added {0} REI/PHI records. Updated {1} REI/PHI records.", newReiPhiList.ProductReiPhi.Count() - updatedCount, updatedCount);

				var reiphiFileLocation = Path.Combine(reiphiStorageLocation, "reiphi.txt");
				reiphiList.CurrentRevision = newReiPhiList.CurrentRevision;

				JsonSerializerSettings settings = new JsonSerializerSettings() { NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore };
				var listData = JsonConvert.SerializeObject(reiphiList, settings);
				File.WriteAllText(reiphiFileLocation, listData);

				versionData = JsonConvert.SerializeObject(newReiPhiList.CurrentRevision);
				File.WriteAllText(versionPath, versionData);
			} catch (Exception ex) {
				log.ErrorException("Encountered a problem while synchronizing masterlist rei/phi records", ex);
			}
		}

        async Task SynchronizePestList() {
			try {
				WebClient client = new WebClient();
				client.Headers.Add("Authorization", "basic QWdDQVBJOlJhbmRvbU1vbmtleVN1cmZzT25MYXZhIQ==");
                client.Headers.Add("Content-Type", "application/json; charset=utf-8");
                client.Encoding = System.Text.Encoding.UTF8;
                var versionPath = Path.Combine(pestStorageLocation, "pests_rev.txt");
                var versionData = ListRevisionData.ReadFromFile(versionPath);
                int version = versionData.DataRevision;

				string url = string.Format(mlwPestQuery, RemoteConfigurationSettings.MasterlistRemoteUri, masterlistGroup, version);
				var newData = await client.DownloadStringTaskAsync(new Uri(url));

				var newPestList = JsonConvert.DeserializeObject<PestList>(newData);
				if (!newPestList.Pests.Any()) {
					return;
				} // No pests, nothing to do.

				EnsurePestListIsLoaded();
				var iList = (List<MasterlistPestItem>)pestList.Pests;

				int updatedCount = 0;
				foreach (var newPest in newPestList.Pests) {
					int removedCount = iList.RemoveAll(x => x.Id == newPest.Id);
					updatedCount += removedCount;
					iList.Add(newPest);
				}
				log.Info("Added {0} pests. Updated {1} pests.", newPestList.Pests.Count() - updatedCount, updatedCount);

				var pestFileLocation = Path.Combine(pestStorageLocation, "pests.txt");
				pestList.CurrentRevision = newPestList.CurrentRevision;

				JsonSerializerSettings settings = new JsonSerializerSettings() {
					NullValueHandling = NullValueHandling.Ignore
				};

				var listData = JsonConvert.SerializeObject(pestList, settings);
				File.WriteAllText(pestFileLocation, listData);

				versionData.DataRevision = newPestList.CurrentRevision;
				versionData.SaveToFile(versionPath);
			} catch (Exception ex) {
				log.ErrorException("Encountered a problem while synchronizing masterlist pests", ex);
			}
		}

        async Task SynchronizeCropDefaultsListAsync() {
			try {
				var client = new WebClient();
                client.Headers.Add("Content-Type", "application/json; charset=utf-8");
                client.Encoding = System.Text.Encoding.UTF8;
                var versionPath = Path.Combine(cropDefaultsStorageLocation, "crop_defaults_rev.txt");
				var versionData = File.ReadAllText(versionPath);
				int version = JsonConvert.DeserializeObject<int>(versionData);

				var url = string.Format(apiCropDefaultsQuery, masterlistGroup, versionData);
				var newData = await client.DownloadStringTaskAsync(new Uri(url));

				var newCropDefaults = JsonConvert.DeserializeAnonymousType(newData, new {
					Results = new CropDefaultsList()
				}).Results;

				if (version >= newCropDefaults.CurrentRevision) { return; }

				EnsureCropDefaultsListIsLoaded();

				cropDefaultsList = newCropDefaults;

				var cropDefaultsFileLocation = Path.Combine(cropDefaultsStorageLocation, "crop_defaults.txt");
				cropDefaultsList.CurrentRevision = newCropDefaults.CurrentRevision;

				JsonSerializerSettings settings = new JsonSerializerSettings() {
					NullValueHandling = NullValueHandling.Ignore
				};

				var listData = JsonConvert.SerializeObject(cropDefaultsList, settings);
				File.WriteAllText(cropDefaultsFileLocation, listData);

				versionData = JsonConvert.SerializeObject(newCropDefaults.CurrentRevision);
				File.WriteAllText(versionPath, versionData);
			} catch (Exception ex) {
				log.ErrorException("Encountered a problem while synchronizing masterlist crop defaults", ex);
			}
		}

		async Task SynchronizeLoadPropertiesListAsync() {
			try {
				var client = new WebClient();
                client.Headers.Add("Content-Type", "application/json; charset=utf-8");
                client.Encoding = System.Text.Encoding.UTF8;
                var versionPath = Path.Combine(loadPropertiesStorageLocation, "load_properties_rev.txt");
				var versionData = File.ReadAllText(versionPath);
				int version = JsonConvert.DeserializeObject<int>(versionData);

				var url = string.Format(apiLoadPropertiesQuery, masterlistGroup, versionData);
				var newData = await client.DownloadStringTaskAsync(new Uri(url));

				var newDefinitionsList = JsonConvert.DeserializeAnonymousType(newData, new {
					Results = new LoadPropertyDefinitions()
				}).Results;

				// second condition is to repair a bad sync that got through in production
				if (version >= newDefinitionsList.CurrentRevision && loadPropertyDefinitions.LoadPropertySets != null) { return; }

				EnsureLoadPropertyDefinitionsAreLoaded();

				loadPropertyDefinitions = newDefinitionsList;

				var loadPropertiesFileLocation = Path.Combine(loadPropertiesStorageLocation, "load_properties.txt");
				loadPropertyDefinitions.CurrentRevision = newDefinitionsList.CurrentRevision;

				JsonSerializerSettings settings = new JsonSerializerSettings() {
					NullValueHandling = NullValueHandling.Ignore
				};

				var listData = JsonConvert.SerializeObject(newDefinitionsList, settings);
				File.WriteAllText(loadPropertiesFileLocation, listData);

				versionData = JsonConvert.SerializeObject(newDefinitionsList.CurrentRevision);
				File.WriteAllText(versionPath, versionData);
			} catch (Exception ex) {
				log.ErrorException("Encountered a problem while synchronizing masterlist load properties", ex);
			}
		}

		async Task SynchronizeYieldLocationSegmentTypeListAsync() {
			try {
				var client = new WebClient();
                client.Headers.Add("Content-Type", "application/json; charset=utf-8");
                client.Encoding = System.Text.Encoding.UTF8;
                var versionPath = Path.Combine(yieldLocationSegmentTypesStorageLocation, "yield_location_segment_types_rev.txt");
				var versionData = File.ReadAllText(versionPath);
				int version = JsonConvert.DeserializeObject<int>(versionData);

				var url = string.Format(apiYieldLocationSegmentTypesQuery, masterlistGroup, versionData);
				var newData = await client.DownloadStringTaskAsync(new Uri(url));

				var newSegmentTypes = JsonConvert.DeserializeAnonymousType(newData, new {
					Results = new YieldLocationSegmentTypesList()
				}).Results;

				if (version >= newSegmentTypes.CurrentRevision) { return; }

				EnsureCropDefaultsListIsLoaded();

				yieldLocationSegmentTypesList = newSegmentTypes;

				var segmentTypesFileLocation = Path.Combine(yieldLocationSegmentTypesStorageLocation, "yield_location_segment_types.txt");
				yieldLocationSegmentTypesList.CurrentRevision = newSegmentTypes.CurrentRevision;

				JsonSerializerSettings settings = new JsonSerializerSettings() {
					NullValueHandling = NullValueHandling.Ignore
				};

				var listData = JsonConvert.SerializeObject(yieldLocationSegmentTypesList, settings);
				File.WriteAllText(segmentTypesFileLocation, listData);

				versionData = JsonConvert.SerializeObject(newSegmentTypes.CurrentRevision);
				File.WriteAllText(versionPath, versionData);
			} catch (Exception ex) {
				log.ErrorException("Encountered a problem while synchronizing masterlist yield location segment types", ex);
			}
		}

		async Task SynchronizeManagedYieldLocationsListAsync() {
			try {
				var client = new WebClient();
                client.Headers.Add("Content-Type", "application/json; charset=utf-8");
                client.Encoding = System.Text.Encoding.UTF8;
                var versionPath = Path.Combine(managedYieldLocationsStorageLocation, "managed_yield_locations_rev.txt");
				var versionData = File.ReadAllText(versionPath);
				int version = JsonConvert.DeserializeObject<int>(versionData);

				var url = string.Format(apiManagedYieldLocationsQuery, masterlistGroup, versionData);
				var newData = await client.DownloadStringTaskAsync(new Uri(url));

				var newLocations = JsonConvert.DeserializeAnonymousType(newData, new {
					Results = new ManagedYieldLocationsList()
				}).Results;

				if (version >= newLocations.CurrentRevision) { return; }

				EnsureCropDefaultsListIsLoaded();

				managedYieldLocationsList = newLocations;

				var managedLocationsFileLocation = Path.Combine(managedYieldLocationsStorageLocation, "managed_yield_locations.txt");
				managedYieldLocationsList.CurrentRevision = newLocations.CurrentRevision;

				JsonSerializerSettings settings = new JsonSerializerSettings() {
					NullValueHandling = NullValueHandling.Ignore
				};

				var listData = JsonConvert.SerializeObject(managedYieldLocationsList, settings);
				File.WriteAllText(managedLocationsFileLocation, listData);

				versionData = JsonConvert.SerializeObject(newLocations.CurrentRevision);
				File.WriteAllText(versionPath, versionData);
			} catch (Exception ex) {
				log.ErrorException("Encountered a problem while synchronizing masterlist yield location segment types", ex);
			}
		}
		#endregion

		#region Land.db Web / Not Implemented
		public IEnumerable<MiniPest> PestListSearch(string searchTerm) {
			throw new NotImplementedException();
		}

		public IEnumerable<ActiveIngredient> GetActiveIngredientList(ProductId productId) {
			throw new NotImplementedException();
		}

		public IEnumerable<MiniCrop> GetCropList(string culture) {
			throw new NotImplementedException();
		}

		public ProductSummariesWrapper GetProductList(long revision, string type, string status, string culture, int? skip, int? take) {
			throw new NotImplementedException();
		}

		public ProductSummariesWrapper GetProductList(DateTime since, string type, string status, string culture) {
			throw new NotImplementedException();
		}

		public WebMiniProduct GetWebMiniProduct(ProductId productId) {
			throw new NotImplementedException("Not intended for client use");
		}

		public MiniProduct GetProductByAgrianId(string productId) {
			throw new NotImplementedException();
		}

		public CropDefaultsList GetCropDefaultsList() {
			throw new NotImplementedException();
		}

		public CropDefaultsList GetCropDefaultsList(string culture) {
			throw new NotImplementedException();
		}

		public LoadPropertyDefinitions GetLoadPropertyDefinitions(string culture) {
			throw new NotImplementedException();
		}

        public List<MiniProduct> GetProducts() {
            throw new NotImplementedException();
        }

		public YieldLocationSegmentTypesList GetYieldLocationSegmentTypes(string culture) {
			throw new NotImplementedException();
		}

		public ManagedYieldLocationsList GetManagedYieldLocations(string culture) {
			throw new NotImplementedException();
		}
		#endregion

	}

	public sealed class ListRevisionData {
		public int DataRevision { get; set; }
		public int StructureRevision { get; set; }

		public static ListRevisionData ReadFromFile(string filePath) {
			var versionData = File.ReadAllText(filePath);
			string[] versionValues = versionData.Split(':');
			int dataVersion = int.Parse(versionValues[0]);
			int structureVersion = 0;
			if (versionValues.Length > 1) {
				structureVersion = int.Parse(versionValues[1]);
			}
			return new ListRevisionData() { DataRevision = dataVersion, StructureRevision = structureVersion };
		}

		public bool SaveToFile(string filePath) {
			string data = string.Format("{0}:{1}", this.DataRevision.ToString(), this.StructureRevision.ToString());
			File.WriteAllText(filePath, data);
			return true;
		}
	}
}