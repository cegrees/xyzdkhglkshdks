﻿using System;
namespace Landdb.Client.Services
{
    public interface INameAutoGenerationService
    {
        string DisplayName { get; }
        string Initials { get; }
        DateTime Now { get; }
        string UserName { get; }
    }
}
