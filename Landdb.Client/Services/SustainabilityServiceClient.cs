﻿using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.Client.Services {
    public class SustainabilityServiceClient {
        readonly string baseUri;
        readonly string referenceApiUri;
        readonly string usleReferenceApiUri;
        readonly string fieldApiUri;
        readonly string clientApiUri;
        readonly string creds;
        readonly string source;
        readonly Logger log = LogManager.GetCurrentClassLogger();

        public SustainabilityServiceClient(string baseUri = null,
                                      string group = "AgConnections",
                                      string creds = "rc89yP5e4xtjljySRb3BjvXxQ8VW8G27+RO4x9zfO4QU3YIS40uRxC1CqCO7Ku3MJ3H+CvYPfsw96hHr+Swhbha6mrR+ENmI2FUCScJYksY5Cqi9/xiTquFk4vgCghA5D10x/6qRIhFsadImIlOgccpAE8ad22UC9SH7AOX0iGc=") {
            if (baseUri == null) {
                this.baseUri = "https://sustainability.landdb.com/sustainability/api/";
            } else {
                this.baseUri = baseUri;
            }
            this.creds = creds;
            this.source = group;

            this.referenceApiUri = this.baseUri + "reference/";
            this.usleReferenceApiUri = this.referenceApiUri + "usle/";
            this.fieldApiUri = this.baseUri + "field/";
            this.clientApiUri = this.baseUri + "client/";

            log.Info("FTM: Initialized service client library. Uri: {0} Group: {1}", this.baseUri, group);
        }

        public async Task<FieldDetails> GetFieldDetails(string clientid, string crop, string fieldid) {
            var action = $"{source}/{clientid}/{fieldid}";
            try {
                var data = await GetData<FieldDetails>(fieldApiUri, action);
                return data;
            } catch {
                return null;
            }
        }

        public async Task<ValidationSummary> GetFieldValidationSummary(string clientid, string crop, string fieldid) {
            var action = $"{source}/{clientid}/{fieldid}/validation"; // string.Format("{0}/{1}/{2}/{3}/", group, clientid, crop, fieldid);
            try {
                var data = await GetData<ValidationSummary>(fieldApiUri, action);
                return data;
            } catch {
                return null;
            }
        }

        public async Task<FtmFieldCreationStatus> RegisterField(string clientid, string crop, string fieldid, int year, string historicid, FieldInputs inputs = null) {
            var client = CreateHttpClient(fieldApiUri);
            var action = $"{source}/{clientid}/{fieldid}/{year}/{crop}/{historicid}";

            HttpResponseMessage response = null;
            if (inputs == null) {
                response = await client.PostAsync(action, null);
            } else {
                response = await client.PostAsJsonAsync(action, inputs);
            }

            if (response.StatusCode == HttpStatusCode.Created) {
                return FtmFieldCreationStatus.Created;
            }

            switch (response.StatusCode) {
                case HttpStatusCode.Created:
                    return FtmFieldCreationStatus.Created;
                case HttpStatusCode.Conflict:
                    return FtmFieldCreationStatus.AlreadyExists;
                default:
                    return FtmFieldCreationStatus.Error;
            }
        }

        public async Task<bool> ReplaceInputs(string clientid, string crop, string fieldid, FieldInputs inputs) {
            var action = $"{source}/{clientid}/{fieldid}/inputs";
            return await PutData(fieldApiUri, action, inputs);
        }

        public async Task<bool> DeleteField(string clientid, string crop, string fieldid) {
            var client = CreateHttpClient(fieldApiUri);
            var action = $"{source}/{clientid}/{fieldid}";
            var response = await client.DeleteAsync(action);

            switch (response.StatusCode) {
                case HttpStatusCode.OK:
                    return true;
                default:
                    return false;
            }
        }

        public async Task<bool> SetBoundary(string clientid, string crop, string fieldid, string boundary) {
            var action = $"{source}/{clientid}/{fieldid}/boundary";
            return await PutData<string>(fieldApiUri, action, boundary);
        }

        public async Task<bool> SetCustomData(string clientid, string crop, string fieldid, string customData) {
            var action = $"{source}/{clientid}/{fieldid}/custom"; 
            return await PutData(fieldApiUri, action, customData);
        }

        public async Task<bool> SetTillageManagementTemplate(string clientid, string crop, string fieldid, string tillageManagementTemplate) {
            var action = $"{source}/{clientid}/{fieldid}/nrcs/tillagetemplate";
            return await PutData(fieldApiUri, action, tillageManagementTemplate);
        }

        public async Task<bool> SetTillageClass(string clientid, string crop, string fieldid, string tillageClass) {
            var action = $"{source}/{clientid}/{fieldid}/tillagepractice";
            return await PutData(fieldApiUri, action, tillageClass);
        }

        public async Task<bool> SetState(string clientid, string crop, string fieldid, string state) {
            var action = $"{source}/{clientid}/{fieldid}/state";
            return await PutData(fieldApiUri, action, state);
        }

        public async Task<bool> SetYield(string clientid, string crop, string fieldid, double yield) {
            var action = $"{source}/{clientid}/{fieldid}/yield";
            return await PutData(fieldApiUri, action, yield);
        }

        public async Task<bool> SetArea(string clientid, string crop, string fieldid, double area) {
            var action = $"{source}/{clientid}/{fieldid}/area";
            return await PutData(fieldApiUri, action, area);
        }

        public async Task<bool> SetSlope(string clientid, string crop, string fieldid, double slopeGrade, double slopeLengthInFeet) {
            var slopeInformation = new {
                Grade = slopeGrade, Length = slopeLengthInFeet
            };
            var action = $"{source}/{clientid}/{fieldid}/slope";
            return await PutData(fieldApiUri, action, slopeInformation);
        }

        public async Task<bool> SetCropProtection(string clientid, string crop, string fieldid, IList<CropProtectionApplication> cropProtections) {
            var action = $"{source}/{clientid}/{fieldid}/cropprotections";
            return await PutData(fieldApiUri, action, cropProtections);
        }

        public async Task<bool> SetFertilizer(string clientid, string crop, string fieldid, IList<FertilizerApplication> fertilizers) {
            var action = $"{source}/{clientid}/{fieldid}/fertilizers";
            return await PutData(fieldApiUri, action, fertilizers);
        }

        public async Task<bool> SetIrrigation(string clientid, string crop, string fieldid, FieldToMarket.Contracts.Partial.IrrigationInformation irrigation) {
            var action = $"{source}/{clientid}/{fieldid}/irrigation";
            return await PutData(fieldApiUri, action, irrigation);
        }

        public async Task<bool> SetDrying(string clientid, string crop, string fieldid, FieldToMarket.Contracts.Partial.DryingInformation dryingInformation) {
            var action = $"{source}/{clientid}/{fieldid}/drying";
            return await PutData(fieldApiUri, action, dryingInformation);
        }

        public async Task<bool> SetDeliveries(string clientid, string crop, string fieldid, IList<DeliveryItem> deliveries) {
            var action = $"{source}/{clientid}/{fieldid}/deliveries";
            return await PutData(fieldApiUri, action, deliveries);
        }

        public async Task<bool> SetWaterQualityDoc(string clientid, string crop, string fieldid, FieldToMarket.Contracts.FieldWaterQualityDocument waterQualityDoc) {
            var action = $"{source}/{clientid}/{fieldid}/watersurvey"; 
            return await PutData(fieldApiUri, action, waterQualityDoc);
        }

        public async Task<bool> SetUSLEInputs(string clientid, string crop, string fieldid, UsleInputs usleInputs) {
            var action = $"{source}/{clientid}/{fieldid}/usle";
            return await PutData(fieldApiUri, action, usleInputs);
        }

        public async Task<bool> RefreshNrcsData(string clientid, string fieldid) {
            var action = $"{source}/{clientid}/{fieldid}/refresh_nrcs";
            try {
                var client = CreateHttpClient(fieldApiUri);
                var response = await client.PostAsync(action, null);

                if (response.StatusCode == HttpStatusCode.Accepted || response.IsSuccessStatusCode) {
                    return true;
                } else {
                    throw new ApplicationException(string.Format("Couldn't post for action: {0} Response: {1}", action, response.StatusCode.ToString()));
                }
            } catch (Exception ex) {
                throw new ApplicationException(string.Format("Couldn't get data for action: {0}", action), ex);
            }
        }

        public async Task<bool> ShareField(string clientid, string crop, string fieldid) {
            var action = $"{source}/{clientid}/{fieldid}/share";

            try {
                var client = CreateHttpClient(fieldApiUri);
                var response = await client.PutAsync(action, null);

                if (response.StatusCode == HttpStatusCode.Accepted) {
                    return true;
                } else {
                    throw new ApplicationException(string.Format("Couldn't put data for action: {0} Response: {1}", action, response.StatusCode.ToString()));
                }
            } catch (Exception ex) {
                throw new ApplicationException(string.Format("Couldn't put data for action: {0}", action), ex);
            }
        }

        public async Task<bool> UnshareField(string clientid, string crop, string fieldid) {
            var action = $"{source}/{clientid}/{fieldid}/unshare";

            try {
                var client = CreateHttpClient(fieldApiUri);
                var response = await client.PutAsync(action, null);

                if (response.StatusCode == HttpStatusCode.Accepted) {
                    return true;
                } else {
                    throw new ApplicationException(string.Format("Couldn't put data for action: {0} Response: {1}", action, response.StatusCode.ToString()));
                }
            } catch (Exception ex) {
                throw new ApplicationException(string.Format("Couldn't put data for action: {0}", action), ex);
            }
        }

        public async Task<IEnumerable<FieldPointer_v2>> GetFieldList(string clientid, int year) {
            var action = $"{source}/{clientid}/fields/{year}/summary/";
            return await GetData<IEnumerable<FieldPointer_v2>>(clientApiUri, action);
        }

        async Task<bool> PutData<T>(string serviceUri, string action, T data) {
            try {
                var client = CreateHttpClient(serviceUri);
                var response = await client.PutAsJsonAsync<T>(action, data);

                if (response.StatusCode == HttpStatusCode.Accepted || response.IsSuccessStatusCode) {
                    log.Info($"FTM API: Called {action} successfully.");
                    return true;
                } else {
                    log.Error($"FTM API: Call to {action} failed. Response code: {response.StatusCode.ToString()}");
                    throw new ApplicationException(string.Format("Couldn't get data for action: {0} Response: {1}", action, response.StatusCode.ToString()));
                }
            } catch (Exception ex) {
                throw new ApplicationException(string.Format("Couldn't get data for action: {0}", action), ex);
            }
        }

        async Task<T> GetData<T>(string serviceUri, string action) {
            try {
                var client = CreateHttpClient(serviceUri);
                var response = await client.GetAsync(action);

                if (response.StatusCode == HttpStatusCode.OK) {
                    var json = await response.Content.ReadAsStringAsync();
                    var result = await JsonConvert.DeserializeObjectAsync<T>(json);
                    return result;
                } else {
                    throw new ApplicationException(string.Format("Couldn't get data for action: {0}", action));
                }
            } catch {
                throw new ApplicationException(string.Format("Couldn't get data for action: {0}", action));
            }
        }

        HttpClient CreateHttpClient(string baseAddress) {
            var client = new HttpClient {
                BaseAddress = new Uri(baseAddress),
            };
            client.DefaultRequestHeaders.Add("Authorization-Token", creds);
            return client;
        }

    }

    public class FieldPointer_v2 {
        public FieldPointer_v2() { }

        public string CropName { get; set; }
        public string FieldId { get; set; }
        public string HistoricId { get; set; }
        public bool IsShared { get; set; }
        public int Year { get; set; }
    }

    [DataContract]
    public class FieldDetails {


        [DataMember]
        public FieldRegistration Registration { get; set; }
        [DataMember]
        public FieldInputs Inputs { get; set; }
        [DataMember]
        public FieldResults Results { get; set; }
        [DataMember]
        public FieldIntermediateCalculations IntermediateCalculations { get; set; }
        [DataMember]
        public NrcsResults NRCSResponseValues { get; set; }
        [DataMember]
        public CalculationStatus CalculationStatus { get; set; }
        [DataMember]
        public List<CalculationMessage> Messages { get; set; }
        [DataMember]
        public FieldToMarket.Contracts.FieldWaterQualityDocument WaterQuality { get; set; }
        [DataMember]
        public DateTime Timestamp { get; set; }

        [DataMember]
        public bool IsShared { get; set; }

    }

    [DataContract]
    public class FieldRegistration {
        [DataMember]
        public FieldActorId Id { get; set; }
        [DataMember]
        public string Crop { get; set; }
        [DataMember]
        public int Year { get; set; }
        [DataMember]
        public string HistoricId { get; set; }
    }

    [DataContract]
    public class FieldActorId {
        public FieldActorId() { }

        public FieldActorId(string source, string clientid, string fieldid) {
            Source = source;
            ClientId = clientid;
            FieldId = fieldid;
        }

        [DataMember]
        public string Source { get; set; }
        [DataMember]
        public string ClientId { get; set; }
        [DataMember]
        public string FieldId { get; set; }

        public override string ToString() {
            return $"{Source}~{ClientId}~{FieldId}";
        }

        public static FieldActorId FromSerializedString(string serializedString) {
            var idParts = serializedString.Split('~');
            if (idParts.Length != 3) { throw new ArgumentException($"Provided string ({serializedString}) was not formatted correctly as a serialized FieldActorId.", nameof(serializedString)); }
            return new FieldActorId(idParts[0], idParts[1], idParts[2]);
        }
    }

    [DataContract]
    public class FieldInputs {
        // Field information
        [DataMember]
        public string State { get; set; }
        [DataMember]
        public double? Yield { get; set; }
        [DataMember]
        public double? Area { get; set; }
        [DataMember]
        public string BoundaryWkt { get; set; }

        // Soil and tillage
        [DataMember]
        public string SoilTexture { get; set; }
        [DataMember]
        public double? SlopeGrade { get; set; }
        [DataMember]
        public double? SlopeLength { get; set; }
        [DataMember]
        public string TillagePractice { get; set; }
        [DataMember]
        public string NRCS_TillageManagementProfile { get; set; }

        // USLE inputs
        [DataMember]
        public string USLE_SoilTexture { get; set; }
        [DataMember]
        public string USLE_TillageSystem { get; set; }
        [DataMember]
        public string USLE_WindErosionSeverity { get; set; }
        [DataMember]
        public string USLE_PreviousCrop { get; set; }
        [DataMember]
        public double? USLE_DieselUsePerArea { get; set; }
        [DataMember]
        public List<string> USLE_ConservationPractices { get; set; }


        // Irrigation
        [DataMember]
        public bool? IsIrrigated { get; set; }
        [DataMember]
        public double? IrrigationAmount { get; set; }
        [DataMember]
        public string WaterSource { get; set; }
        [DataMember]
        public string IrrigationSystem { get; set; }
        [DataMember]
        public double? PumpDepth { get; set; }
        [DataMember]
        public double? PumpPressure { get; set; }
        [DataMember]
        public string WaterOrigin { get; set; }
        [DataMember]
        public string PumpEnergySource { get; set; }
        [DataMember]
        public List<FuelUseInput> ActualIrrigationFuel { get; set; }


        // Drying
        [DataMember]
        public string DryerType { get; set; }
        [DataMember]
        public double? PercentOfYieldToDry { get; set; }
        [DataMember]
        public double? PointsOfMoistureToRemove { get; set; }
        [DataMember]
        public List<FuelUseInput> ActualDryingFuel { get; set; }
        [DataMember]
        public double? CottonLintPercentage { get; set; }
        [DataMember]
        public double? CottonThermalEnergyInBtu { get; set; }


        // Production
        [DataMember]
        public List<FertilizerApplication> FertilizerApplications { get; set; }
        [DataMember]
        public List<CropProtectionApplication> CropProtectionApplications { get; set; }
        [DataMember]
        public List<DeliveryItem> DeliveryItems { get; set; }

        [DataMember]
        public string CustomData { get; set; }
    }

    [DataContract]
    public class FertilizerApplication {
        [DataMember]
        public string CustomIdentifier { get; set; }
        [DataMember]
        public string FertilizerId { get; set; }
        [DataMember]
        public double RateInPoundsPerAcre { get; set; }
        [DataMember]
        public double Nitrogen { get; set; }
        [DataMember]
        public double Phosphorus { get; set; }
        [DataMember]
        public double Potassium { get; set; }
        [DataMember]
        public bool IsManure { get; set; }
    }

    [DataContract]
    public class CropProtectionApplication {
        [DataMember]
        public string CustomIdentifier { get; set; }
        [DataMember]
        public int HerbicideCount { get; set; }
        [DataMember]
        public int InsecticideCount { get; set; }
        [DataMember]
        public int FungicideCount { get; set; }
        [DataMember]
        public int SeedTreatmentCount { get; set; }
        [DataMember]
        public int GrowthRegulatorCount { get; set; }
        [DataMember]
        public int FumigantCount { get; set; }
    }

    [DataContract]
    public class DeliveryItem {
        [DataMember]
        public string FuelType { get; set; }
        [DataMember]
        public double TotalTransportDistance { get; set; }
        [DataMember]
        public double AverageMilesPerGallon { get; set; }
    }

    [DataContract]
    public class FuelUseInput {
        public FuelUseInput() { }

        public FuelUseInput(string fuelName, double totalFuelUsed) {
            FuelName = fuelName;
            TotalFuelUsed = totalFuelUsed;
        }

        [DataMember]
        public string FuelName { get; set; }
        [DataMember]
        public double TotalFuelUsed { get; set; }
    }

    [DataContract]
    public class FieldResults {
        [DataMember]
        public double? LandUseValue { get; set; }
        [DataMember]
        public double? SoilConservationValue { get; set; }
        [DataMember]
        public double? SoilCarbonValue { get; set; }
        [DataMember]
        public double? WaterUseValue { get; set; }
        [DataMember]
        public double? EnergyUseValue { get; set; }
        [DataMember]
        public double? GreenhouseGasEmissionsValue { get; set; }

        // TODO: State values ?

        [DataMember]
        public DateTime? LastCalculationTimestamp { get; set; }
    }

    [DataContract]
    public class FieldIntermediateCalculations {
        [DataMember]
        public double? TillageAndOperationsDieselEquivalent { get; set; }
        [DataMember]
        public double? ManureLoadingDieselEquivalent { get; set; }
        [DataMember]
        public double? IrrigationDieselEquivalent { get; set; }
        [DataMember]
        public double? TransportationDieselEquivalent { get; set; }
        [DataMember]
        public double? DryingDieselEquivalent { get; set; }

        [DataMember]
        public double? TillageAndOperationsBTUValue { get; set; }
        [DataMember]
        public double? ManureLoadingBTUValue { get; set; }
        [DataMember]
        public double? IrrigationBTUValue { get; set; }
        [DataMember]
        public double? TransportationBTUValue { get; set; }
        [DataMember]
        public double? DryingBTUValue { get; set; }

        [DataMember]
        public double? TillageAndOperationsCO2Equivalent { get; set; }
        [DataMember]
        public double? ManureLoadingCO2Equivalent { get; set; }
        [DataMember]
        public double? IrrigationCO2Equivalent { get; set; }
        [DataMember]
        public double? TransportationCO2Equivalent { get; set; }
        [DataMember]
        public double? DryingCO2Equivalent { get; set; }

        [DataMember]
        public double? EmbeddedEnergy_BTU_Fertilizer_N { get; set; }
        [DataMember]
        public double? EmbeddedEnergy_BTU_Fertilizer_P { get; set; }
        [DataMember]
        public double? EmbeddedEnergy_BTU_Fertilizer_K { get; set; }
        [DataMember]
        public double? EmbeddedEnergy_BTU_Fertilizer_Lime { get; set; }
        [DataMember]
        public double? EmbeddedEnergy_BTU_CropProtection_Herbicide { get; set; }
        [DataMember]
        public double? EmbeddedEnergy_BTU_CropProtection_Insecticide { get; set; }
        [DataMember]
        public double? EmbeddedEnergy_BTU_CropProtection_Fungicide { get; set; }
        [DataMember]
        public double? EmbeddedEnergy_BTU_CropProtection_SeedTreatment { get; set; }
        [DataMember]
        public double? EmbeddedEnergy_BTU_CropProtection_GrowthRegulator { get; set; }
        [DataMember]
        public double? EmbeddedEnergy_BTU_CropProtection_Fumigant { get; set; }

        [DataMember]
        public double? EmbeddedGHG_CO2Equivalent_Fertilizer_N { get; set; }
        [DataMember]
        public double? EmbeddedGHG_CO2Equivalent_Fertilizer_P { get; set; }
        [DataMember]
        public double? EmbeddedGHG_CO2Equivalent_Fertilizer_K { get; set; }
        [DataMember]
        public double? EmbeddedGHG_CO2Equivalent_Fertilizer_Lime { get; set; }
        [DataMember]
        public double? EmbeddedGHG_CO2Equivalent_CropProtection_Herbicide { get; set; }
        [DataMember]
        public double? EmbeddedGHG_CO2Equivalent_CropProtection_Insecticide { get; set; }
        [DataMember]
        public double? EmbeddedGHG_CO2Equivalent_CropProtection_Fungicide { get; set; }
        [DataMember]
        public double? EmbeddedGHG_CO2Equivalent_CropProtection_SeedTreatment { get; set; }
        [DataMember]
        public double? EmbeddedGHG_CO2Equivalent_CropProtection_GrowthRegulator { get; set; }
        [DataMember]
        public double? EmbeddedGHG_CO2Equivalent_CropProtection_Fumigant { get; set; }

        [DataMember]
        public double? EmissionGHG_CO2Equivalent_NitrousOxide { get; set; }
        [DataMember]
        public double? EmissionGHG_CO2Equivalent_Crop { get; set; }
    }

    [DataContract]
    public class NrcsResults {
        [DataMember]
        public double? SlopeDelivery { get; set; }
        [DataMember]
        public double? SlopeTValue { get; set; }
        [DataMember]
        public double? SlopeDegrade { get; set; }
        [DataMember]
        public double? EquivalentDieselUsePerArea { get; set; }
        [DataMember]
        public string ClimatePointer { get; set; }
        [DataMember]
        public string SoilPointer { get; set; }
        [DataMember]
        public double? RUSLE2_SoilConditioningIndex { get; set; }
        [DataMember]
        public double? RUSLE2_StirValue { get; set; }

        [DataMember]
        public double? WEPS_SoilConditioningIndex { get; set; }
        [DataMember]
        public double? WEPS_WindErosion { get; set; }

        [DataMember]
        public bool IsSuccessful { get; set; }
        [DataMember]
        public string Rusle2Message { get; set; }
        [DataMember]
        public string WepsMessage { get; set; }
    }

    [DataContract]
    public class CalculationStatus {
        [DataMember]
        public bool? IsDataInputComplete { get; set; }
        [DataMember]
        public bool? IsCalculationComplete { get; set; }
        [DataMember]
        public DateTime? LastCalculationTimestamp { get; set; }
        [DataMember]
        public DateTime? LastNRCSPullTimestamp { get; set; }
        [DataMember]
        public string NRCSDataStatus { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public bool? HasNRCSErrors { get; set; }
        [DataMember]
        public string NRCSMessages { get; set; }
        [DataMember]
        public bool? HasCalculationErrors { get; set; }
        [DataMember]
        public string CalculationMessages { get; set; }
    }

    [DataContract]
    public class CalculationMessage {
        public CalculationMessage(string message, string relatedResult, string relatedInput = null, CalculationMessageSeverity severity = CalculationMessageSeverity.Info) {
            Message = message;
            RelatedResult = relatedResult;
            RelatedInput = relatedInput;
            Severity = severity;
        }

        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public string RelatedResult { get; set; }
        [DataMember]
        public string RelatedInput { get; set; }
        [DataMember]
        public CalculationMessageSeverity Severity { get; set; }
    }

    [DataContract]
    public enum CalculationMessageSeverity {
        [EnumMember]
        Info = 0,
        [EnumMember]
        Warning = 1,
        [EnumMember]
        Error = 2
    }

    [DataContract]
    public class UsleInputs {
        [DataMember]
        public List<string> ConservationPractices { get; set; }
        [DataMember]
        public string PreviousCrop { get; set; }
        [DataMember]
        public string WindErosionSeverity { get; set; }
        [DataMember]
        public double OperationalDieselUsePerArea { get; set; }
        [DataMember]
        public string SoilTexture { get; set; }
        [DataMember]
        public string TillageSystem { get; set; }
    }

    [DataContract]
    public class ValidationSummary {

        [DataMember]
        public FieldRegistration Registration { get; set; }

        [DataMember]
        public bool IsYieldValid { get; set; }
        [DataMember]
        public bool IsFieldInformationValid { get; set; }
        [DataMember]
        public bool IsIrrigationValid { get; set; }
        [DataMember]
        public bool IsFertilizerValid { get; set; }
        [DataMember]
        public bool IsCropProtectionValid { get; set; }
        [DataMember]
        public bool IsDryingValid { get; set; }
        [DataMember]
        public bool IsDeliveryValid { get; set; }
        [DataMember]
        public bool IsConservationValid { get; set; }
        [DataMember]
        public bool IsSurveyValid { get; set; }
        [DataMember]
        public bool IsFieldShared { get; set; }
        [DataMember]
        public DateTime? Timestamp { get; set; }

        // Messages
        [DataMember]
        public string YieldMessage { get; set; }
        [DataMember]
        public string FieldInformationMessage { get; set; }
        [DataMember]
        public string IrrigationMessage { get; set; }
        [DataMember]
        public string FertilizerMessage { get; set; }
        [DataMember]
        public string CropProtectionMessage { get; set; }
        [DataMember]
        public string DryingMessage { get; set; }
        [DataMember]
        public string DeliveryMessage { get; set; }
        [DataMember]
        public string ConservationMessage { get; set; }
        [DataMember]
        public string SurveyMessage { get; set; }

        // logic
        public bool HasValidationWarnings {
            get {
                return !IsYieldValid || !IsFieldInformationValid || !IsIrrigationValid || !IsFertilizerValid || !IsCropProtectionValid
                    || !IsDryingValid || !IsDeliveryValid || !IsConservationValid || !IsSurveyValid;
            }
        }

        public List<string> ValidationWarnings {
            get {
                List<string> warnings = new List<string>();
                if (!IsYieldValid) {
                    if (string.IsNullOrWhiteSpace(YieldMessage)) {
                        //warnings.Add("");
                    } else {
                        warnings.AddRange(YieldMessage.Split(';'));
                    }
                }
                if (!IsFieldInformationValid) {
                    if (string.IsNullOrWhiteSpace(FieldInformationMessage)) {
                        //warnings.Add("");
                    } else {
                        warnings.AddRange(FieldInformationMessage.Split(';'));
                    }
                }
                if (!IsIrrigationValid) {
                    if (string.IsNullOrWhiteSpace(IrrigationMessage)) {
                        //warnings.Add("");
                    } else {
                        warnings.AddRange(IrrigationMessage.Split(';'));
                    }
                }
                if (!IsFertilizerValid) {
                    if (string.IsNullOrWhiteSpace(FertilizerMessage)) {
                        //warnings.Add("");
                    } else {
                        warnings.AddRange(FertilizerMessage.Split(';'));
                    }
                }
                if (!IsCropProtectionValid) {
                    if (string.IsNullOrWhiteSpace(CropProtectionMessage)) {
                        //warnings.Add("");
                    } else {
                        warnings.AddRange(CropProtectionMessage.Split(';'));
                    }
                }
                if (!IsDryingValid) {
                    if (string.IsNullOrWhiteSpace(DryingMessage)) {
                        //warnings.Add("");
                    } else {
                        warnings.AddRange(DryingMessage.Split(';'));
                    }
                }
                if (!IsDeliveryValid) {
                    if (string.IsNullOrWhiteSpace(DeliveryMessage)) {
                        //warnings.Add("");
                    } else {
                        warnings.AddRange(DeliveryMessage.Split(';'));
                    }
                }
                if (!IsConservationValid) {
                    if (string.IsNullOrWhiteSpace(ConservationMessage)) {
                        //warnings.Add("");
                    } else {
                        warnings.AddRange(ConservationMessage.Split(';'));
                    }
                }
                if (!IsSurveyValid) {
                    if (string.IsNullOrWhiteSpace(SurveyMessage)) {
                        //warnings.Add("");
                    } else {
                        warnings.AddRange(SurveyMessage.Split(';'));
                    }
                }

                return warnings;
            }
        }
    }

}
