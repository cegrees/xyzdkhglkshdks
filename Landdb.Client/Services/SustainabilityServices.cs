﻿using ftmc = FieldToMarket.Contracts;
using FieldToMarket.Contracts.Partial;
using FieldToMarket.Contracts.Reference;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Landdb.Client.Services {
    public class SustainabilityServices {
        readonly string baseUri;
        readonly string referenceApiUri;
        readonly string usleReferenceApiUri;
        readonly string fieldApiUri;
        readonly string creds;
        readonly string group;
        readonly Logger log = LogManager.GetCurrentClassLogger();

        //  --FTMUri=http://localhost:48015/api/
        public SustainabilityServices(string baseUri,
                                      string group = "AgConnections",
                                      string creds = "rc89yP5e4xtjljySRb3BjvXxQ8VW8G27+RO4x9zfO4QU3YIS40uRxC1CqCO7Ku3MJ3H+CvYPfsw96hHr+Swhbha6mrR+ENmI2FUCScJYksY5Cqi9/xiTquFk4vgCghA5D10x/6qRIhFsadImIlOgccpAE8ad22UC9SH7AOX0iGc=") {
            if (baseUri == null) {
                this.baseUri = "https://sustainabilityservices.azurewebsites.net/api/";
            } else {
                this.baseUri = baseUri;
            }
            this.creds = creds;
            this.group = group;

            this.referenceApiUri = this.baseUri + "reference/";
            this.usleReferenceApiUri = this.referenceApiUri + "usle/";
            this.fieldApiUri = this.baseUri + "fields/";

            log.Info("FTM: Initialized service library. Uri: {0} Group: {1}", this.baseUri, group);
        }

        public async Task<IEnumerable<Crop>> GetCrops() {
            return await GetData<List<Crop>>(referenceApiUri, "crops");
        }

        public async Task<Crop> GetCrop(string name) {
            return await GetData<Crop>(referenceApiUri, $"crops/{name}");
        }

        public async Task<IEnumerable<FuelType>> GetFuelTypes() {
            return await GetData<List<FuelType>>(referenceApiUri, "fueltypes");
        }

        public async Task<IEnumerable<string>> GetTillageClasses() {
            return await GetData<List<string>>(usleReferenceApiUri, "tillageclasses");
        }

        public async Task<IEnumerable<DryingSystem>> GetDryingSystems() {
            return await GetData<List<DryingSystem>>(referenceApiUri, "dryingsystems");
        }

        public async Task<IEnumerable<string>> GetSoilTextures() {
            return await GetData<List<string>>(usleReferenceApiUri, "soiltextures");
        }

        public async Task<IEnumerable<string>> GetConservationPractices() {
            return await GetData<List<string>>(usleReferenceApiUri, "conservationpractices");
        }

        public async Task<IEnumerable<string>> GetPreviousCrops() {
            return await GetData<List<string>>(usleReferenceApiUri, "previouscrops");
        }

        public async Task<IEnumerable<string>> GetWindSeverities() {
            return await GetData<List<string>>(usleReferenceApiUri, "windseverities");
        }

        public async Task<string> LookupFtmCropByAgCId(string state, Guid cropId, Guid subcropId) {
            string action;
            if (subcropId == Guid.Empty) {
                action = string.Format("ftmcropbyagcid/{0}/{1}", state, cropId.ToString());
            } else {
                action = string.Format("ftmcropbyagcid/{0}/{1}/{2}", state, cropId.ToString(), subcropId.ToString());
            }
            
            return await GetData<string>(referenceApiUri, action);
        }

        public async Task<IEnumerable<TillageManagementPractice>> GetTillageManagementPractices(string crop, double latitude, double longitude) {
            var queryParams = new NameValueCollection() { {"crop", crop}, {"latitude", latitude.ToString()}, {"longitude", longitude.ToString()} };
            var action = string.Format("tillagemanagementpractices/{0}?{1}", crop, queryParams.CreateQueryString());
            return await GetData<List<TillageManagementPractice>>(referenceApiUri, action);
        }

        public async Task<CropStateValue> GetStateValuesForCrop(string crop, string state) {
            var action = string.Format("statevalues/{0}/{1}", crop, state);
            return await GetData<CropStateValue>(referenceApiUri, action);
        }

        //public async Task<ftmc.FieldDetailsDocument> GetFieldDetails(string clientId, string crop, string fieldId) {
        //    var action = string.Format("{0}/{1}/{2}/{3}/", group, clientId, crop, fieldId);
        //    try {
        //        var data = await GetData<ftmc.FieldDetailsDocument>(fieldApiUri, action);
        //        return data;
        //    } catch {
        //        return null;
        //    }
        //}

        //public async Task<FtmFieldCreationStatus> RegisterField(string clientId, string crop, string fieldId, int cropYear, string historicId, ftmc.FieldInputDocument inputs = null) {
        //    var client = CreateHttpClient(fieldApiUri);
        //    var action = string.Format("{0}/{1}/{2}/{3}/{4}/{5}", group, clientId, crop, fieldId, cropYear, historicId);
        //    HttpResponseMessage response = null;
        //    if (inputs == null) {
        //        response = await client.PostAsync(action, null);
        //    } else {
        //        response = await client.PostAsJsonAsync(action, inputs);
        //    }

        //    if (response.StatusCode == HttpStatusCode.Created) {
        //        return FtmFieldCreationStatus.Created;
        //    }

        //    switch (response.StatusCode) {
        //        case HttpStatusCode.Created:
        //            return FtmFieldCreationStatus.Created;
        //        case HttpStatusCode.Conflict:
        //            return FtmFieldCreationStatus.AlreadyExists;
        //        default:
        //            return FtmFieldCreationStatus.Error;
        //    }
        //}

        //public async Task<bool> ReplaceInputs(string clientId, string crop, string fieldId, ftmc.FieldInputDocument inputs) {
        //    var action = string.Format("{0}/{1}/{2}/{3}/replaceinputs/", group, clientId, crop, fieldId);
        //    return await PutData(fieldApiUri, action, inputs);
        //}

        //public async Task<bool> DeleteField(string clientId, string crop, string fieldId) {
        //    var client = CreateHttpClient(fieldApiUri);
        //    var action = string.Format("{0}/{1}/{2}/{3}/", group, clientId, crop, fieldId);
        //    var response = await client.DeleteAsync(action);

        //    switch (response.StatusCode) {
        //        case HttpStatusCode.OK:
        //            return true;
        //        default:
        //            return false;
        //    }
        //}

        //public async Task<bool> SetBoundary(string clientId, string crop, string fieldId, string boundary) {
        //    var action = string.Format("{0}/{1}/{2}/{3}/boundary/", group, clientId, crop, fieldId);
        //    return await PutData(fieldApiUri, action, boundary);
        //}

        //public async Task<bool> SetCustomData(string clientId, string crop, string fieldId, string customData) {
        //    var action = string.Format("{0}/{1}/{2}/{3}/customData/", group, clientId, crop, fieldId);
        //    return await PutData(fieldApiUri, action, customData);
        //}

        //public async Task<bool> SetTillageManagementTemplate(string clientId, string crop, string fieldId, string tillageManagementTemplate) {
        //    var action = string.Format("{0}/{1}/{2}/{3}/nrcstillagemanagementpointer/", group, clientId, crop, fieldId);
        //    return await PutData(fieldApiUri, action, tillageManagementTemplate);
        //}

        //public async Task<bool> SetTillageClass(string clientId, string crop, string fieldId, string tillageClass) {
        //    var action = string.Format("{0}/{1}/{2}/{3}/tillageclass/", group, clientId, crop, fieldId);
        //    return await PutData(fieldApiUri, action, tillageClass);
        //}
        
        //public async Task<bool> SetState(string clientId, string crop, string fieldId, string state) {
        //    var action = string.Format("{0}/{1}/{2}/{3}/state/", group, clientId, crop, fieldId);
        //    return await PutData(fieldApiUri, action, state);
        //}

        //public async Task<bool> SetYield(string clientId, string crop, string fieldId, double yield) {
        //    var action = string.Format("{0}/{1}/{2}/{3}/yield/", group, clientId, crop, fieldId);
        //    return await PutData(fieldApiUri, action, yield);
        //}

        //public async Task<bool> SetArea(string clientId, string crop, string fieldId, double area) {
        //    var action = string.Format("{0}/{1}/{2}/{3}/area/", group, clientId, crop, fieldId);
        //    return await PutData(fieldApiUri, action, area);
        //}

        //public async Task<bool> SetSlope(string clientId, string crop, string fieldId, double slopeGrade, double slopeLengthInFeet) {
        //    SlopeInformation slopeInformation = new SlopeInformation() {
        //        SlopeGrade = slopeGrade, SlopeLength = slopeLengthInFeet
        //    };
        //    var action = string.Format("{0}/{1}/{2}/{3}/slope/", group, clientId, crop, fieldId);
        //    return await PutData(fieldApiUri, action, slopeInformation);
        //}

        //public async Task<bool> SetCropProtection(string clientId, string crop, string fieldId, IList<ftmc.Partial.CropProtectionApplication> cropProtections) {
        //    var action = string.Format("{0}/{1}/{2}/{3}/cropprotection/", group, clientId, crop, fieldId);
        //    return await PutData(fieldApiUri, action, cropProtections);
        //}

        //public async Task<bool> SetFertilizer(string clientId, string crop, string fieldId, IList<ftmc.Partial.FertilizerApplication> fertilizers) {
        //    var action = string.Format("{0}/{1}/{2}/{3}/fertilizer/", group, clientId, crop, fieldId);
        //    return await PutData(fieldApiUri, action, fertilizers);
        //}

        //public async Task<bool> SetIrrigation(string clientId, string crop, string fieldId, IrrigationInformation irrigation) {
        //    var action = string.Format("{0}/{1}/{2}/{3}/irrigation/", group, clientId, crop, fieldId);
        //    return await PutData(fieldApiUri, action, irrigation);
        //}

        //public async Task<bool> SetDrying(string clientId, string crop, string fieldId, DryingInformation dryingInformation) {
        //    var action = string.Format("{0}/{1}/{2}/{3}/drying/", group, clientId, crop, fieldId);
        //    return await PutData(fieldApiUri, action, dryingInformation);
        //}

        //public async Task<bool> SetDeliveries(string clientId, string crop, string fieldId, IList<ftmc.Partial.DeliveryItem> deliveries) {
        //    var action = string.Format("{0}/{1}/{2}/{3}/deliveries/", group, clientId, crop, fieldId);
        //    return await PutData(fieldApiUri, action, deliveries);
        //}

        //public async Task<bool> SetWaterQualityDoc(string clientId, string crop, string fieldId, ftmc.FieldWaterQualityDocument waterQualityDoc) {
        //    var action = string.Format("{0}/{1}/{2}/{3}/waterquality/", group, clientId, crop, fieldId);
        //    return await PutData(fieldApiUri, action, waterQualityDoc);
        //}

        //public async Task<bool> SetUSLEInputs(string clientId, string crop, string fieldId, UsleInputs usleInputs) {
        //    var action = string.Format("{0}/{1}/{2}/{3}/usleinputs/", group, clientId, crop, fieldId);
        //    return await PutData(fieldApiUri, action, usleInputs);            
        //}

        //public async Task<bool> ShareField(string clientId, string crop, string fieldId) {
        //    var action = string.Format("{0}/{1}/{2}/{3}/share/", group, clientId, crop, fieldId);

        //    try {
        //        var client = CreateHttpClient(fieldApiUri);
        //        var response = await client.PutAsync(action, null);

        //        if (response.StatusCode == HttpStatusCode.Accepted) {
        //            return true;
        //        } else {
        //            throw new ApplicationException(string.Format("Couldn't put data for action: {0} Response: {1}", action, response.StatusCode.ToString()));
        //        }
        //    } catch (Exception ex) {
        //        throw new ApplicationException(string.Format("Couldn't put data for action: {0}", action), ex);
        //    }
        //}

        //public async Task<bool> UnshareField(string clientId, string crop, string fieldId) {
        //    var action = string.Format("{0}/{1}/{2}/{3}/unshare/", group, clientId, crop, fieldId);

        //    try {
        //        var client = CreateHttpClient(fieldApiUri);
        //        var response = await client.PutAsync(action, null);

        //        if (response.StatusCode == HttpStatusCode.Accepted) {
        //            return true;
        //        } else {
        //            throw new ApplicationException(string.Format("Couldn't put data for action: {0} Response: {1}", action, response.StatusCode.ToString()));
        //        }
        //    } catch (Exception ex) {
        //        throw new ApplicationException(string.Format("Couldn't put data for action: {0}", action), ex);
        //    }
        //}

        //public async Task<IEnumerable<ftmc.FieldPointer>> GetFieldList(string clientId, int year) {
        //    var action = string.Format("{0}/{1}/{2}/availablefields/", group, clientId, year);
        //    return await GetData<IEnumerable<ftmc.FieldPointer>>(fieldApiUri, action);
        //}

        async Task<bool> PutData<T>(string serviceUri, string action, T data) {
            try {
                var client = CreateHttpClient(serviceUri);
                var response = await client.PutAsJsonAsync<T>(action, data);

                if (response.StatusCode == HttpStatusCode.Accepted) {
                    return true;
                } else {
                    throw new ApplicationException(string.Format("Couldn't get data for action: {0} Response: {1}", action, response.StatusCode.ToString()));
                }
            } catch (Exception ex) {
                throw new ApplicationException(string.Format("Couldn't get data for action: {0}", action), ex);
            }
        }

        async Task<T> GetData<T>(string serviceUri, string action) {
            try {
                var client = CreateHttpClient(serviceUri);
                var response = await client.GetAsync(action);

                if (response.StatusCode == HttpStatusCode.OK) {
                    var json = await response.Content.ReadAsStringAsync();
                    var result = await JsonConvert.DeserializeObjectAsync<T>(json);
                    return result;
                } else {
                    throw new ApplicationException(string.Format("Couldn't get data for action: {0}", action));
                }
            } catch {
                throw new ApplicationException(string.Format("Couldn't get data for action: {0}", action));
            }
        }

        HttpClient CreateHttpClient(string baseAddress) {
            var client = new HttpClient {
                BaseAddress = new Uri(baseAddress),
            };
            client.DefaultRequestHeaders.Add("Authorization-Token", creds);
            return client;
        }
    }

    public enum FtmFieldCreationStatus {
        Unknown,
        Created,
        AlreadyExists,
        Error
    }

    public static class FtmServiceExtensions {
        public static string CreateQueryString(this NameValueCollection parameters) {
            var query = HttpUtility.ParseQueryString(string.Empty);
            foreach (var pk in parameters.AllKeys) {
                query[pk] = parameters[pk];
            }

            string queryString = query.ToString();
            return queryString;
        }
    }
}
