﻿using Landdb.Client.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Landdb.Infrastructure;
using Landdb.Client.Account;

namespace Landdb.Client.Services {
	public class NameAutoGenerationService : INameAutoGenerationService {

		string userName;

		public NameAutoGenerationService(IAccountStatus status) {
			var accountStatus = status;
			userName = status.UserName;
		}

		public DateTime Now { get { return DateTime.Now; } }
		public string UserName { get { return userName; } }

		public string DisplayName {
			get { return string.Format("{0} {1} {2}", Initials, Now.ToString("yyMMdd"), Now.ToLocalTime().ToString("HHmm")); }
		}

		public string Initials {
			get {
				string initials = string.Empty;

				var parts = UserName.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
				foreach (string part in parts) {
					initials += part[0];
				}

				return initials;
			}
		}
	}
}
