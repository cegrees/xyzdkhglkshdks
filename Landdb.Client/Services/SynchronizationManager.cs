﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Landdb.Client.Account;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;
using Landdb.Web.ServiceContracts;
using Landdb.Web.ServiceContracts.Data;
using Landdb.Domain;
using NLog;
using Landdb.Client.Infrastructure;
using System.Net;
using Newtonsoft.Json;
using Microsoft.AspNet.SignalR.Client.Hubs;
using System.Collections.Concurrent;
using Landdb.Client.Services.Credentials;
using System.Net.Http;
using Landdb.Client.Resources;

namespace Landdb.Client.Services {

    public class SynchronizationManager : ISynchronizationManager {
        IMessageProcessor messageProcessor;
        IEventStore eventStore;
        IClientEndpoint clientEndpoint;
        readonly string serviceUri;

        Logger log = LogManager.GetCurrentClassLogger();

        DataSourceId currentDataSourceId;
        CancellationTokenSource cts;

        int eventStoreOperationsMax = 50;
        int eventStoreOperationTimeoutSeconds = 60;

        Task currentSyncTask;

        public SynchronizationManager(IMessageProcessor messageProcessor, IEventStore eventStore, IClientEndpoint clientEndpoint, string serviceUri, int? operationCountOverride = null, int? operationTimeoutOverride = null) {
            this.messageProcessor = messageProcessor;
            this.eventStore = eventStore;
            this.clientEndpoint = clientEndpoint;
            this.serviceUri = serviceUri;

            if (operationCountOverride.HasValue) { eventStoreOperationsMax = operationCountOverride.Value; }
            if (operationTimeoutOverride.HasValue) { eventStoreOperationTimeoutSeconds = operationTimeoutOverride.Value; }

        }


        public DataSourceId CurrentDataSourceId {
            get { return currentDataSourceId; }
        }

        public async Task BeginSynchronizationTasks(Guid dataSourceId, IAccountStatus accountStatus, IProgressTracker progress, Action AfterCatchup) {
            if (!NetworkStatus.IsInternetAvailableFast()) {
                AfterCatchup();
                return;
            }

            currentDataSourceId = new DataSourceId(dataSourceId);

            // cancel any existing sync operation
            if (cts != null) {
                using (cts) {
                    cts.Cancel();

                    if (currentSyncTask != null) {
                        try {
                            currentSyncTask.Wait();
                        } catch (AggregateException ex) {
                            ex.Flatten().Handle(e => e is OperationCanceledException || e is TaskCanceledException);
                        }
                    }
                }
            }

            cts = new CancellationTokenSource();

            var cancellationToken = cts.Token;

            currentSyncTask = Task.Factory.StartNew(async () => {
                bool isCatchingUp = true;

                JsonSerializerSettings settings = new JsonSerializerSettings();
                settings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;

                try {
                    // Setup a client to be used
                    var client = new HttpClient {
                        BaseAddress = new Uri(serviceUri)
                    };
                    var storageCreds = clientEndpoint.ResolveStorageCredentials();
                    await storageCreds.SetBearerTokenAsync(client);
                    cancellationToken.ThrowIfCancellationRequested();

                    // First message to the user
                    UpdateProgressMessage(progress, Strings.Progress_SynchronizingRecords);
                    UpdateProgressTotalRecords(progress, 100);
                    UpdateProgressCurrentRecord(progress, 0);

                    var dss = clientEndpoint.GetPersistentView<DataSourceSettings>(currentDataSourceId);
                    var dataSourceSettings = dss.HasValue ? dss.Value : new DataSourceSettings() { };
                    int start = (int)dataSourceSettings.NextEventNumberToReceive;
                    if (start < 0) { start = 0; }

                    log.Info("Beginning event sync of {0}, starting with v{1}", currentDataSourceId.Id.ToString(), start);

                    // Initial call to the service will let us setup some things, and determine what the last event number is going to be, so we know when to call AfterCatchup()
                    string operation = string.Format("events/{0}/{1}/{2}", dataSourceId.ToString(), start, eventStoreOperationsMax);
                    var firstResponse = await client.GetAsync(operation, cancellationToken);
                    if (firstResponse.StatusCode == HttpStatusCode.OK) {
                        var firstResponseJson = await firstResponse.Content.ReadAsStringAsync();
                        EventStoreRecordSlice currentSlice = JsonConvert.DeserializeObject<EventStoreRecordSlice>(firstResponseJson, settings);
                        firstResponse.Dispose();

                        // Set target catchup revision
                        int catchupRevision = currentSlice.LastEventNumber;
                        UpdateProgressTotalRecords(progress, catchupRevision);

                        if (start >= catchupRevision) { // Handle the scenario where nothing's been entered since we were away.
                            AfterCatchup();
                            isCatchingUp = false;
                        }
                        
                        // Message pump
                        while (true) {
                            if (cancellationToken.IsCancellationRequested) { break; }

                            bool processedEvents = false;
                            foreach(var pendingEvent in currentSlice.Events) {
                                messageProcessor.ProcessEvent(pendingEvent);
                                processedEvents = true;

                                if (isCatchingUp) { // If still catching up, update the UI and check whether we can complete
                                    UpdateProgressCurrentRecord(progress, pendingEvent.OriginalEventNumber);

                                    if (pendingEvent.OriginalEventNumber >= catchupRevision) {
                                        AfterCatchup();
                                        isCatchingUp = false;
                                    }
                                }
                            }

                            if (processedEvents) {
                                log.Info("Processed an event slice for {0}. Next event will be v{1}", currentDataSourceId.Id, currentSlice.NextEventNumber);
                                dataSourceSettings.NextEventNumberToReceive = currentSlice.NextEventNumber;
                                clientEndpoint.SavePersistentEntity<DataSourceSettings>(currentDataSourceId, dataSourceSettings);
                            }

                            bool nextSliceIsReady = false;
                            operation = string.Format("events/{0}/{1}/{2}", dataSourceId.ToString(), dataSourceSettings.NextEventNumberToReceive, eventStoreOperationsMax);
                            
                            while (!nextSliceIsReady) {
                                using (var response = await client.GetAsync(operation, cancellationToken)) {
                                    if (response.StatusCode == HttpStatusCode.OK) {
                                        var json = await response.Content.ReadAsStringAsync();
                                        currentSlice = JsonConvert.DeserializeObject<EventStoreRecordSlice>(json, settings);

                                        if (currentSlice.Events.Any()) {
                                            nextSliceIsReady = true;
                                        } else {
                                            var cancelled = cancellationToken.WaitHandle.WaitOne(TimeSpan.FromSeconds(15));
                                            if (cancelled) { break; }
                                        }
                                    } else {
                                        if (storageCreds.IsExpired) {
                                            await storageCreds.SetBearerTokenAsync(client);
                                        } else {
                                            log.Warn("Couldn't retrieve data from the server. (Response code: {0} {1}) Trying again in 15 seconds.", ((int)response.StatusCode).ToString(), response.ReasonPhrase);

                                            var cancelled = cancellationToken.WaitHandle.WaitOne(TimeSpan.FromSeconds(15));
                                            if (cancelled) { break; }
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        string firstResponseContent = string.Empty;
                        if (firstResponse.Content != null) {
                            firstResponseContent = await firstResponse.Content.ReadAsStringAsync();
                        }
                        log.Error("Couldn't retrieve sync initialization data from the server. {0} {1}", firstResponse.StatusCode, firstResponseContent);

                        if (isCatchingUp) {
                            AfterCatchup();
                        }
                        // TODO: Notify of problem.
                    }

                } catch (Exception ex) {
                    log.ErrorException(string.Format("Caught an exception while syncing datasource {0}", currentDataSourceId.Id), ex);
                    if (isCatchingUp) {
                        AfterCatchup();
                    }
                    throw;
                }
            }, cancellationToken).Unwrap();
            
        }

        void UpdateProgressMessage(IProgressTracker progress, string message) {
            if (progress != null) {
                progress.StatusMessage = message;
            }
        }

        void UpdateProgressTotalRecords(IProgressTracker progress, int totalRecords) {
            if (progress != null) {
                progress.TotalRecords = totalRecords;
            }
        }

        void UpdateProgressCurrentRecord(IProgressTracker progress, long currentRecord) {
            if (progress != null) {
                progress.CurrentRecord = (int)currentRecord;
            }
        }

    }
}