﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Landdb.Client.Infrastructure;
using Landdb.Client.Models.Connect;
using Newtonsoft.Json;

namespace Landdb.Client.Services
{
    public class ConnectApplicationService
    {
        private readonly ConnectStore<ConnectApplication> connectStore;
        private readonly IClientEndpoint clientEndpoint;
        private readonly string connectHostUrl;
        private readonly HttpClient httpClient;
        private bool isRunning;

        public ConnectApplicationService(ConnectStore<ConnectApplication> connectStore, 
            IClientEndpoint clientEndpoint, string connectHostUrl)
        {
            this.connectStore = connectStore;
            this.clientEndpoint = clientEndpoint;
            this.connectHostUrl = connectHostUrl;
            httpClient = new HttpClient();
        }

        public async Task<List<ConnectApplication>> GetPendingApplications(Guid datasourceId)
        {
            return await GetDataAsync<List<ConnectApplication>>($"{connectHostUrl}/application/pending/{datasourceId}");
        }

        private async Task<T> GetDataAsync<T>(string uri)
        {
            HttpRequestMessage message = new HttpRequestMessage(HttpMethod.Get, uri);

            await clientEndpoint.ResolveStorageCredentials().SignHttpRequestMessageAsync(message);

            var response = await httpClient.SendAsync(message);
            string json = await response.Content.ReadAsStringAsync();

            var result = JsonConvert.DeserializeObject<T>(json);
            return result;
        }

        public async Task<bool> Remove(ConnectApplication storeData)
        {
            HttpRequestMessage message = new HttpRequestMessage(HttpMethod.Put,
                $"{connectHostUrl}/application/pending/{storeData.DatasourceId}/{storeData.ApplicationId.Id}/remove");

            await clientEndpoint.ResolveStorageCredentials().SignHttpRequestMessageAsync(message);

            httpClient.SendAsync(message);

            connectStore.Remove(storeData);

            return true;
        }

        public async Task Start()
        {
            isRunning = await HasIntegrationAccount(connectStore.DatasourceId);
            Run();
        }

        private async Task Run()
        {
            while (isRunning)
            {
                List<ConnectApplication> applications = await GetPendingApplications(connectStore.DatasourceId);

                connectStore.UpdatedData(applications ?? new List<ConnectApplication>());

                await Task.Delay(TimeSpan.FromSeconds(60));
            }
        }

        public void Stop()
        {
            isRunning = false;
        }

        public async Task<bool> HasIntegrationAccount(Guid datasourceId)
        {
            HttpResponseMessage httpResponse = await httpClient.GetAsync(
                    $"https://landdb-connect.azurewebsites.net/api/GetRegistrationsByDatasourceId?datasourceId={datasourceId}&code=j2DQEVuHCQ0RpeXuW26mZcoUEm3DUeGWCDyc21HZt9um/ULFh1kzFA==");

            string json = httpResponse.Content.ReadAsStringAsync().Result;

            List<IntegrationAccount> integrationAccounts = JsonConvert.DeserializeObject<List<IntegrationAccount>>(json);

            return integrationAccounts.Count > 0;
        }

        public async Task UpdateDatasourceId(Guid datasourceId)
        {
            connectStore.UpdateDatasourceId(datasourceId);
            Stop();
            await Task.Delay(TimeSpan.FromSeconds(1));
            Start();
        }
    }
}