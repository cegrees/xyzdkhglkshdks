﻿using AgC.UnitConversion;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.Client.Services.Fertilizer
{
    public interface IFertilizerAccumulator
    {
        FertilizerFormulationModel CalculateTotalPlanFertilizer(List<IncludedProduct> Products, Measure estArea, IClientEndpoint endPoint, Guid dataSourceId, int currentCropYear);
    }
}
