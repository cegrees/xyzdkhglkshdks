﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.Client.Services.Fertilizer
{
    public class FertilizerFormulationModel
    {
        public decimal N { get; set; }
        public decimal P { get; set; }
        public decimal K { get; set; }
        public decimal Ca { get; set; }
        public decimal Mg { get; set; }
        public decimal S { get; set; }
        public decimal B { get; set; }
        public decimal Cl { get; set; }
        public decimal Cu { get; set; }
        public decimal Fe { get; set; }
        public decimal Mn { get; set; }
        public decimal Mo { get; set; }
        public decimal Zn { get; set; }

        public override string ToString()
        {
            StringBuilder formulation = new StringBuilder(30);
            formulation.Append(String.Format("{0:0#.##}", N)); formulation.Append("N - ");
            formulation.Append(String.Format("{0:0#.##}", P)); formulation.Append("P - ");
            formulation.Append(String.Format("{0:0#.##}", K)); formulation.Append("K");

            if (Ca != 0)
            {
                formulation.Append(" - "); formulation.Append(String.Format("{0:0#.##}", Ca)); formulation.Append("Ca");
            }
            if (Mg != 0)
            {
                formulation.Append(" - "); formulation.Append(String.Format("{0:0#.##}", Mg)); formulation.Append("Mg");
            }
            if (S != 0)
            {
                formulation.Append(" - "); formulation.Append(String.Format("{0:0#.##}", S)); formulation.Append("S");
            }
            if (B != 0)
            {
                formulation.Append(" - "); formulation.Append(String.Format("{0:0#.##}", B)); formulation.Append("B");
            }
            if (Cl != 0)
            {
                formulation.Append(" - "); formulation.Append(String.Format("{0:0#.##}", Cl)); formulation.Append("Cl");
            }
            if (Cu != 0)
            {
                formulation.Append(" - "); formulation.Append(String.Format("{0:0#.##}", Cu)); formulation.Append("Cu");
            }
            if (Fe != 0)
            {
                formulation.Append(" - "); formulation.Append(String.Format("{0:0#.##}", Fe)); formulation.Append("Fe");
            }
            if (Mn != 0)
            {
                formulation.Append(" - "); formulation.Append(String.Format("{0:0#.##}", Mn)); formulation.Append("Mn");
            }
            if (Mo != 0)
            {
                formulation.Append(" - "); formulation.Append(String.Format("{0:0#.##}", Mo)); formulation.Append("Mo");
            }
            if (Zn != 0)
            {
                formulation.Append(" - "); formulation.Append(String.Format("{0:0#.##}", Zn)); formulation.Append("Zn");
            }
            return formulation.ToString();
            //var s = string.Format("{0} - {1} - {2}", N.ToString("n2"), P.ToString("n2"), K.ToString("n2"));
            //return s;
        }
    }
}
