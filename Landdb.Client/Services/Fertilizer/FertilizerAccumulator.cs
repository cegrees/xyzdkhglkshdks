﻿using AgC.UnitConversion;
using AgC.UnitConversion.Area;
using AgC.UnitConversion.MassAndWeight;
using AgC.UnitConversion.Package;
using AgC.UnitConversion.Volume;
using Landdb.Client.Infrastructure;
using Landdb.Domain.ReadModels.Application;
using Landdb.Domain.ReadModels.Product;
using Landdb.Domain.ReadModels.Shared;
using Landdb.Domain.ReadModels.Tree;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.Client.Services.Fertilizer
{
    public class FertilizerAccumulator
    {
        IClientEndpoint clientEndpoint;
        Guid currentDataSourceID;
        int currentCropYear;
        IMasterlistService ml;
        UserSettings userSettings;

        public FertilizerAccumulator(IClientEndpoint endPoint, Guid dataSourceId, int currentCropYear)
        {
            this.clientEndpoint = endPoint;
            this.currentDataSourceID = dataSourceId;
            this.currentCropYear = currentCropYear;

            ml = clientEndpoint.GetMasterlistService();
            userSettings = clientEndpoint.GetUserSettings();

            //var analysis = await CalculateFertilizerUsage(filtered, ((CropZoneTreeItemViewModel)treeModel).Area, cropId);
        }

        public async Task<FertilizerFormulationModel> CalculateFertilizerAnalysis(ProductId prodId, Measure czArea, double rate, string unit)
        {
            FertilizerFormulationModel fertModel = new FertilizerFormulationModel();
            double divisor = 1;
            double prodRate = 0;
            double areaRatio = 1;

            var product = ml.GetProduct(prodId);

            if (product == null)
            {
                //TO DO :: BE SURE TO GET THE DATASOURCE ID
                var userCreatedList = clientEndpoint.GetView<UserCreatedProductList>(new DataSourceId(currentDataSourceID)).GetValue(new UserCreatedProductList());
                var userCreated = userCreatedList.UserCreatedProducts.Where(x => x.Id == prodId.Id).FirstOrDefault();
                product = userCreated;
            }

            IUnit rateUnit = UnitFactory.GetPackageSafeUnit(unit, product.StdUnit, product.StdFactor, product.StdPackageUnit);
            Measure rateMeasure;
            rateMeasure = rateUnit.GetMeasure(rate, product.Density);

            areaRatio = 1;

            if (rateMeasure is AreaMeasure)
            {
                divisor = 1;
                prodRate = rateMeasure.CreateAs(userSettings.UserAreaUnit).Value;
            }
            else if (rateMeasure is MassAndWeightMeasure || (rateMeasure is VolumeMeasure && rateMeasure.CanConvertTo(userSettings.UserMassUnit)))
            {
                divisor = 100;
                var prodRateNew = rateMeasure.CanConvertTo(userSettings.UserMassUnit) ? rateMeasure.CreateAs(userSettings.UserMassUnit).Value : rateMeasure.Value;
                prodRate = prodRateNew;
            }
            else if(rateMeasure is PackageMeasure)
            {
                divisor = 100;
                var prodRateNew = rateMeasure.CanConvertTo(userSettings.UserMassUnit) ? rateMeasure.GetValueAs(userSettings.UserMassUnit) : 0.0;
                prodRate = prodRateNew;
            }


            decimal multiplicand = (decimal)prodRate * (decimal)areaRatio / (decimal)divisor;

            #region Increment Fertilizer Counts
            if (product.Formulation != null)
            {
                var f = product.Formulation;

                if (f.N.HasValue)
                {
                    fertModel.N += f.N.Value * multiplicand;
                }
                if (f.P.HasValue)
                {
                    fertModel.P += f.P.Value * multiplicand;
                }
                if (f.K.HasValue)
                {
                    fertModel.K += f.K.Value * multiplicand;
                }
                if (f.Ca.HasValue)
                {
                    fertModel.Ca += f.Ca.Value * multiplicand;
                }
                if (f.Mg.HasValue)
                {
                    fertModel.Mg += f.Mg.Value * multiplicand;
                }
                if (f.S.HasValue)
                {
                    fertModel.S += f.S.Value * multiplicand;
                }
                if (f.B.HasValue)
                {
                    fertModel.B += f.B.Value * multiplicand;
                }
                if (f.Cl.HasValue)
                {
                    fertModel.Cl += f.Cl.Value * multiplicand;
                }
                if (f.Cu.HasValue)
                {
                    fertModel.Cu += f.Cu.Value * multiplicand;
                }
                if (f.Fe.HasValue)
                {
                    fertModel.Fe += f.Fe.Value * multiplicand;
                }
                if (f.Mn.HasValue)
                {
                    fertModel.Mn += f.Mn.Value * multiplicand;
                }
                if (f.Mo.HasValue)
                {
                    fertModel.Mo += f.Mo.Value * multiplicand;
                }
                if (f.Zn.HasValue)
                {
                    fertModel.Zn += f.Zn.Value * multiplicand;
                }
            }
            #endregion
            return fertModel;
        }

        public async Task<FertilizerFormulationModel> CalculateFertilizerAnalysis(ProductId prodId, Measure czArea, Measure appArea, double rate, string unit)
        {
            FertilizerFormulationModel fertModel = new FertilizerFormulationModel();
            double divisor = 1;
            double prodRate = 0;
            double areaRatio = 1;

            var product = ml.GetProduct(prodId);

            if (product == null)
            {
                //TO DO :: BE SURE TO GET THE DATASOURCE ID
                var userCreatedList = clientEndpoint.GetView<UserCreatedProductList>(new DataSourceId(currentDataSourceID)).GetValue(new UserCreatedProductList());
                var userCreated = userCreatedList.UserCreatedProducts.Where(x => x.Id == prodId.Id).FirstOrDefault();
                product = userCreated;
            }

            IUnit rateUnit = UnitFactory.GetPackageSafeUnit(unit, product.StdUnit, product.StdFactor, product.StdPackageUnit);
            Measure rateMeasure;
            rateMeasure = rateUnit.GetMeasure(rate, product.Density);

            areaRatio = appArea.Value / czArea.Value;

            if (rateMeasure is AreaMeasure)
            {
                divisor = 1;
                prodRate = rateMeasure.CreateAs(userSettings.UserAreaUnit).Value;
            }
            else if (rateMeasure is MassAndWeightMeasure || (rateMeasure is VolumeMeasure && rateMeasure.CanConvertTo(userSettings.UserMassUnit)))
            {
                divisor = 100;
                var prodRateNew = rateMeasure.CanConvertTo(userSettings.UserMassUnit) ? rateMeasure.CreateAs(userSettings.UserMassUnit).Value : rateMeasure.Value;
                prodRate = prodRateNew;
            }
            else if (rateMeasure is PackageMeasure)
            {
                divisor = 100;
                var prodRateNew = rateMeasure.CanConvertTo(userSettings.UserMassUnit) ? rateMeasure.GetValueAs(userSettings.UserMassUnit) : 0.0;
                prodRate = prodRateNew;
            }


            decimal multiplicand = (decimal)prodRate * (decimal)areaRatio / (decimal)divisor;

            #region Increment Fertilizer Counts
            if (product.Formulation != null)
            {
                var f = product.Formulation;

                if (f.N.HasValue)
                {
                    fertModel.N += f.N.Value * multiplicand;
                }
                if (f.P.HasValue)
                {
                    fertModel.P += f.P.Value * multiplicand;
                }
                if (f.K.HasValue)
                {
                    fertModel.K += f.K.Value * multiplicand;
                }
                if (f.Ca.HasValue)
                {
                    fertModel.Ca += f.Ca.Value * multiplicand;
                }
                if (f.Mg.HasValue)
                {
                    fertModel.Mg += f.Mg.Value * multiplicand;
                }
                if (f.S.HasValue)
                {
                    fertModel.S += f.S.Value * multiplicand;
                }
                if (f.B.HasValue)
                {
                    fertModel.B += f.B.Value * multiplicand;
                }
                if (f.Cl.HasValue)
                {
                    fertModel.Cl += f.Cl.Value * multiplicand;
                }
                if (f.Cu.HasValue)
                {
                    fertModel.Cu += f.Cu.Value * multiplicand;
                }
                if (f.Fe.HasValue)
                {
                    fertModel.Fe += f.Fe.Value * multiplicand;
                }
                if (f.Mn.HasValue)
                {
                    fertModel.Mn += f.Mn.Value * multiplicand;
                }
                if (f.Mo.HasValue)
                {
                    fertModel.Mo += f.Mo.Value * multiplicand;
                }
                if (f.Zn.HasValue)
                {
                    fertModel.Zn += f.Zn.Value * multiplicand;
                }
            }
            #endregion
            return fertModel;
        }


        public async Task<FertilizerFormulationModel> CalculateTotalPlanFertilizer(List<IncludedProduct> Products, Measure estArea)
        {
            FertilizerFormulationModel fertModel = new FertilizerFormulationModel();
            double divisor = 1;
            double prodRate = 0;
            double areaRatio = 1;

            foreach (var prod in Products)
            {
                var product = ml.GetProduct(prod.Id);

                if (product == null)
                {
                    //TO DO :: BE SURE TO GET THE DATASOURCE ID
                    var userCreatedList = clientEndpoint.GetView<UserCreatedProductList>(new DataSourceId(currentDataSourceID)).GetValue(new UserCreatedProductList());
                    var userCreated = userCreatedList.UserCreatedProducts.Where(x => x.Id == prod.Id.Id).FirstOrDefault();
                    product = userCreated;
                }

                IUnit rateUnit = UnitFactory.GetPackageSafeUnit(prod.RateUnit, product.StdUnit, product.StdFactor, product.StdPackageUnit);
                Measure rateMeasure;
                rateMeasure = rateUnit.GetMeasure((double)prod.RateValue, product.Density);
                var prodAreaApplied = ((double)prod.PercentApplied * (int)prod.ApplicationCount * estArea.Value);
                if (prod.AreaUnit == estArea.Unit.Name)
                {
                    areaRatio = prodAreaApplied / estArea.Value;
                }
                else
                {
                    var itemAreaUnit = UnitFactory.GetUnitByName(prod.AreaUnit);
                    var itemArea = itemAreaUnit.GetMeasure(prodAreaApplied);
                    var consistentItemArea = itemArea.GetValueAs(estArea.Unit);
                    areaRatio = consistentItemArea / estArea.Value;
                }

                if (rateMeasure is AreaMeasure)
                {
                    divisor = 1;
                    prodRate = rateMeasure.CreateAs(userSettings.UserAreaUnit).Value;
                }
                else if (rateMeasure is MassAndWeightMeasure || (rateMeasure is VolumeMeasure && rateMeasure.CanConvertTo(userSettings.UserMassUnit)))
                {
                    divisor = 100;
                    prodRate = rateMeasure.CreateAs(userSettings.UserMassUnit).Value;
                }
                else if (rateMeasure is PackageMeasure)
                {
                    divisor = 100;
                    var prodRateNew = rateMeasure.CanConvertTo(userSettings.UserMassUnit) ? rateMeasure.GetValueAs(userSettings.UserMassUnit) : 0.0;
                    prodRate = prodRateNew;
                }
                else
                {
                    continue;
                }

                decimal multiplicand = (decimal)prodRate * (decimal)areaRatio / (decimal)divisor;

                #region Increment Fertilizer Counts
                if (product.Formulation != null)
                {
                    var f = product.Formulation;

                    if (f.N.HasValue)
                    {
                        fertModel.N += f.N.Value * multiplicand;
                    }
                    if (f.P.HasValue)
                    {
                        fertModel.P += f.P.Value * multiplicand;
                    }
                    if (f.K.HasValue)
                    {
                        fertModel.K += f.K.Value * multiplicand;
                    }
                    if (f.Ca.HasValue)
                    {
                        fertModel.Ca += f.Ca.Value * multiplicand;
                    }
                    if (f.Mg.HasValue)
                    {
                        fertModel.Mg += f.Mg.Value * multiplicand;
                    }
                    if (f.S.HasValue)
                    {
                        fertModel.S += f.S.Value * multiplicand;
                    }
                    if (f.B.HasValue)
                    {
                        fertModel.B += f.B.Value * multiplicand;
                    }
                    if (f.Cl.HasValue)
                    {
                        fertModel.Cl += f.Cl.Value * multiplicand;
                    }
                    if (f.Cu.HasValue)
                    {
                        fertModel.Cu += f.Cu.Value * multiplicand;
                    }
                    if (f.Fe.HasValue)
                    {
                        fertModel.Fe += f.Fe.Value * multiplicand;
                    }
                    if (f.Mn.HasValue)
                    {
                        fertModel.Mn += f.Mn.Value * multiplicand;
                    }
                    if (f.Mo.HasValue)
                    {
                        fertModel.Mo += f.Mo.Value * multiplicand;
                    }
                    if (f.Zn.HasValue)
                    {
                        fertModel.Zn += f.Zn.Value * multiplicand;
                    }
                }
                #endregion
            }

            return fertModel;
        }

        public async Task<FertilizerFormulationModel> CalculateTotalCropZoneFertilizer(CropZoneId czId, DateTime? startDate, DateTime? endDate)
        {

            var cropYearId = new CropYearId(currentDataSourceID, currentCropYear);
            var value = clientEndpoint.GetView<CropZoneApplicationDataView>(cropYearId).GetValue(new CropZoneApplicationDataView());
            var allApplied = (from t in value.Items
                             where t.CropZoneId == czId
                             select t).ToList();
            List<CropZoneApplicationDataItem> dataItems = new List<CropZoneApplicationDataItem>();

            var czDetails = clientEndpoint.GetView<CropZoneDetailsView>(czId).Value;
            var areaValue = czDetails.ReportedArea.HasValue ? czDetails.ReportedArea.Value : czDetails.BoundaryArea.HasValue ? czDetails.BoundaryArea.Value : 0;
            var areaUnitString = czDetails.ReportedArea != null ? czDetails.ReportedAreaUnit : czDetails.BoundaryAreaUnit;
            var areaMeasure = UnitFactory.GetUnitByName(areaUnitString ?? Acre.Self.Name).GetMeasure(areaValue);

            if (startDate != null || endDate != null)
            {
                dataItems = allApplied.Where(x => x.EndDate >= startDate && x.EndDate <= endDate).ToList();
            }
            else
            {
                dataItems.AddRange(allApplied);
            }

            Task<FertilizerFormulationModel> getFormulation = CalculateFertilizerUsage(dataItems, areaMeasure);

            return await getFormulation;
        }

        //CHEAP EXAMPLE
        //public async void CalculateFert()
        //{
        //    List<CropZoneApplicationDataItem> filtered = new List<CropZoneApplicationDataItem>();
        //    Measure area = UnitFactory.GetUnitByName("Acre").GetMeasure(1.0);
        //    CropId cropId = new CropId();
        //    var analysis = await CalculateFertilizerUsage(filtered, area, cropId);
        //}

        public async Task<FertilizerFormulationModel> CalculateFertilizerUsage(List<CropZoneApplicationDataItem> items, Measure czArea)
        {
            FertilizerFormulationModel fertModel = new FertilizerFormulationModel();

            if (czArea.Value == 0) { return fertModel; }

            double divisor = 1;
            double prodRate = 0;
            double areaRatio = 1;

            foreach (var item in items)
            {
                var product = ml.GetProduct(item.ProductId);

                if (product == null)
                {
                    //TO DO :: BE SURE TO GET THE DATASOURCE ID
                    var userCreatedList = clientEndpoint.GetView<UserCreatedProductList>(new DataSourceId(currentDataSourceID)).GetValue(new UserCreatedProductList());
                    var userCreated = userCreatedList.UserCreatedProducts.Where(x => x.Id == item.ProductId.Id).FirstOrDefault();
                    product = userCreated;
                }

                IUnit rateUnit = UnitFactory.GetPackageSafeUnit(item.RateUnit, product.StdUnit, product.StdFactor, product.StdPackageUnit);
                Measure rateMeasure;
                rateMeasure = rateUnit.GetMeasure((double)item.RateValue, product.Density);
                var coveragePercent = item.CoveragePercent;

                if (item.AreaUnit == czArea.Unit.Name)
                {
                    areaRatio = (item.AreaValue * coveragePercent) / czArea.Value;
                }
                else
                {
                    var itemAreaUnit = UnitFactory.GetUnitByName(item.AreaUnit);
                    var itemArea = itemAreaUnit.GetMeasure(item.AreaValue);
                    var consistentItemArea = itemArea.GetValueAs(czArea.Unit);
                    areaRatio = (consistentItemArea * coveragePercent) / czArea.Value;
                }

                if (rateMeasure is AreaMeasure)
                {
                    divisor = 1;
                    prodRate = rateMeasure.CreateAs(userSettings.UserAreaUnit).Value;
                }
                else if (rateMeasure is MassAndWeightMeasure || (rateMeasure is VolumeMeasure && rateMeasure.CanConvertTo(userSettings.UserMassUnit)))
                {
                    divisor = 100;
                    prodRate = rateMeasure.CreateAs(userSettings.UserMassUnit).Value;
                }
                else if (rateMeasure is PackageMeasure)
                {
                    divisor = 100;
                    var prodRateNew = rateMeasure.CanConvertTo(userSettings.UserMassUnit) ? rateMeasure.GetValueAs(userSettings.UserMassUnit) : 0.0;
                    prodRate = prodRateNew;
                }
                else
                {
                    continue;
                }

                decimal multiplicand = (decimal)prodRate * (decimal)areaRatio / (decimal)divisor;

                #region Increment Fertilizer Counts
                if (product.Formulation != null)
                {
                    var f = product.Formulation;

                    if (f.N.HasValue)
                    {
                        fertModel.N += f.N.Value * multiplicand;
                    }
                    if (f.P.HasValue)
                    {
                        fertModel.P += f.P.Value * multiplicand;
                    }
                    if (f.K.HasValue)
                    {
                        fertModel.K += f.K.Value * multiplicand;
                    }
                    if (f.Ca.HasValue)
                    {
                        fertModel.Ca += f.Ca.Value * multiplicand;
                    }
                    if (f.Mg.HasValue)
                    {
                        fertModel.Mg += f.Mg.Value * multiplicand;
                    }
                    if (f.S.HasValue)
                    {
                        fertModel.S += f.S.Value * multiplicand;
                    }
                    if (f.B.HasValue)
                    {
                        fertModel.B += f.B.Value * multiplicand;
                    }
                    if (f.Cl.HasValue)
                    {
                        fertModel.Cl += f.Cl.Value * multiplicand;
                    }
                    if (f.Cu.HasValue)
                    {
                        fertModel.Cu += f.Cu.Value * multiplicand;
                    }
                    if (f.Fe.HasValue)
                    {
                        fertModel.Fe += f.Fe.Value * multiplicand;
                    }
                    if (f.Mn.HasValue)
                    {
                        fertModel.Mn += f.Mn.Value * multiplicand;
                    }
                    if (f.Mo.HasValue)
                    {
                        fertModel.Mo += f.Mo.Value * multiplicand;
                    }
                    if (f.Zn.HasValue)
                    {
                        fertModel.Zn += f.Zn.Value * multiplicand;
                    }
                }
                #endregion
            }

            return fertModel;
        }
    }
}
