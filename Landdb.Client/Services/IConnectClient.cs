﻿using Landdb.Client.Models.Connect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.Client.Services {
    public interface IConnectClient {

        Task<bool?> CanGetInvoicesAsync(Guid datasourceId);

        Task<IEnumerable<InvoiceView>> GetPendingInvoicesAsync(Guid datasourceId);
        Task<InvoiceBlockHeaderList> GetPendingInvoiceHeadersAsync(Guid datasourceId);
        Task<InvoiceView> GetInvoiceAsync(InvoiceId invoiceId);
        Task<bool> RemoveInvoiceAsync(InvoiceId invoiceId);
        string GetInvoiceEditExperienceUri(Guid datasourceId, Guid invoiceId, int cropYear);
    }
}
