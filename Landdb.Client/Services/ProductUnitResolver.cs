﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AgC.UnitConversion;
using Landdb.Domain.ReadModels;
using Landdb.Domain.ReadModels.Product;
using Lokad.Cqrs.AtomicStorage;

namespace Landdb.Client.Services {
    // TODO: Refactor such that this and the Landdb.Azure.WebFramework.Services.ProductUnitResolver are merged to a common library.
    public class ProductUnitResolver : IProductUnitResolver {
        readonly IMasterlistService masterlist;

        public ProductUnitResolver(IMasterlistService masterlist) {
            this.masterlist = masterlist;
        }

        public IUnit GetProductPackageUnit(ProductId productId) {
            MiniProduct mp = masterlist.GetProduct(productId); 
            if (mp == null) { return new NullUnit(); }
            return UnitFactory.GetUnitByPackageInformation(mp.StdUnit, mp.StdFactor, mp.StdPackageUnit);
        }

        public IUnit GetPackageSafeUnit(ProductId productId, string desiredUnitName) {
            MiniProduct mp = masterlist.GetProduct(productId);
            if (mp == null) { return new NullUnit(); }
            return UnitFactory.GetPackageSafeUnit(desiredUnitName, mp.StdUnit, mp.StdFactor, mp.StdPackageUnit);
        }

        public double? GetDensity(ProductId productId) {
            MiniProduct mp = masterlist.GetProduct(productId);
            if (mp == null) { return null; }
            return mp.Density;
        }
    }
}
