﻿namespace Landdb.Client.Services {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Landdb.Client.Account;
using Landdb.Web.ServiceContracts.Data;
    using System.Threading.Tasks;

    public interface ISynchronizationManager {
        DataSourceId CurrentDataSourceId { get; }
        Task BeginSynchronizationTasks(Guid dataSourceId, IAccountStatus accountStatus, IProgressTracker progress,
            Action AfterCatchup);
    }
}
