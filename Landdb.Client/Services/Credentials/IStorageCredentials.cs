﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Landdb.Client.Services.Credentials {
    public interface IStorageCredentials {
        Task SetBearerTokenAsync(HttpClient client);

        Task SignHttpRequestMessageAsync(HttpRequestMessage message);
        bool IsExpired { get; }
        [Obsolete]
		Task SignWebClientAsync(WebClient webClient);
    }
}
