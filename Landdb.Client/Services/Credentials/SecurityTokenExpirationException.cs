﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Landdb.Client.Services.Credentials {
    public class SecurityTokenExpirationException : Exception {
        public SecurityTokenExpirationException(string message)
            : base(message) {
        }

        public SecurityTokenExpirationException(string message, Exception inner)
            : base(message, inner) {
        }
    }
}
