﻿using System;
using System.Runtime.Serialization;

namespace Landdb.Client.Services
{
    [DataContract]
    class IntegrationAccount
    {
        [DataMember]
        public string IntegrationAccountId { get; set; }

        [DataMember]
        public string AgentName { get; set; }

        [DataMember]
        public string IntegrationType { get; set; }

        [DataMember]
        public string Status { get; set; }

        [DataMember]
        public DateTime CreatedOnUtc { get; set; }
    }
}
