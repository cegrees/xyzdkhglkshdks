﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Landdb.Client.Models.Connect;
using System.Net.Http;
using Landdb.Client.Infrastructure;
using Newtonsoft.Json;
using Polly;
using NLog;
using System.Net;

namespace Landdb.Client.Services {
    public class ConnectClient : IConnectClient {
        IClientEndpoint clientEndpoint;
        string connectHostUrl;
        string connectWebUrl;
        HttpClient http = new HttpClient() { Timeout = TimeSpan.FromSeconds(10) };
        Policy policy;
        Logger log = LogManager.GetCurrentClassLogger();

        public ConnectClient(IClientEndpoint clientEndpoint, string connectHostUrl, string connectWebUrl) {
            this.clientEndpoint = clientEndpoint;
            this.connectHostUrl = connectHostUrl;
            this.connectWebUrl = connectWebUrl;

            policy = Policy
                .Handle<Exception>()
                .RetryAsync(5, (ex, count) => {
                    log.WarnException($"Polly Failure #{count}", ex);
                });
        }

        public async Task<bool?> CanGetInvoicesAsync(Guid datasourceId) {
            if(datasourceId == Guid.Empty) { return false; }

            var uri = $"{connectHostUrl}/invoices/pending/{datasourceId}";
            HttpRequestMessage message = new HttpRequestMessage(HttpMethod.Get, uri);
            await clientEndpoint.ResolveStorageCredentials().SignHttpRequestMessageAsync(message);
            var response = await http.SendAsync(message);

            if(response.IsSuccessStatusCode) { return true; }
            if(response.StatusCode == HttpStatusCode.Unauthorized ||
                response.StatusCode == HttpStatusCode.Forbidden) { return false; }

            return null;
        }

        public async Task<IEnumerable<InvoiceView>> GetPendingInvoicesAsync(Guid datasourceId) {
            if(datasourceId == Guid.Empty) { return new List<InvoiceView>(); }

            var policyResult = await policy.ExecuteAsync(async () => {
                var result = await GetDataAsync<List<InvoiceView>>($"{connectHostUrl}/invoices/pending/{datasourceId}");
                return result;
            });
            return policyResult;
        }

        public async Task<InvoiceBlockHeaderList> GetPendingInvoiceHeadersAsync(Guid datasourceId) {
            if (datasourceId == Guid.Empty) { return new InvoiceBlockHeaderList(); }

            var policyResult = await policy.ExecuteAsync(async () => {
                var result = await GetDataAsync<InvoiceBlockHeaderList>($"{connectHostUrl}/invoices/pending/{datasourceId}/headers");
                return result;
            });
            return policyResult;
        }

        public async Task<InvoiceView> GetInvoiceAsync(InvoiceId invoiceId) {
            if(invoiceId == null) {
                log.Debug($"{nameof(GetInvoiceAsync)}: invoiceId was null");
                return null;
            }

            var policyResult = await policy.ExecuteAsync(async () => {
                var result = await GetDataAsync<InvoiceView>($"{connectHostUrl}/invoices/pending/{invoiceId.DataSourceId}/{invoiceId.Id}");
                return result;
            });
            return policyResult;
        }

        public async Task<bool> RemoveInvoiceAsync(InvoiceId invoiceId) {
            if (invoiceId == null) {
                log.Debug($"{nameof(GetInvoiceAsync)}: invoiceId was null");
                return false;
            }

            var policyResult = await policy.ExecuteAsync(async () => {
                var uri = $"{connectHostUrl}/invoices/pending/{invoiceId.DataSourceId}/{invoiceId.Id}";
                HttpRequestMessage message = new HttpRequestMessage(HttpMethod.Delete, uri);
                await clientEndpoint.ResolveStorageCredentials().SignHttpRequestMessageAsync(message);
                var response = await http.SendAsync(message);
                var json = await response.Content.ReadAsStringAsync();

                var result = JsonConvert.DeserializeObject<bool>(json);
                return result;
            });
            return policyResult;
        }

        async Task<T> GetDataAsync<T>(string uri) {
            HttpRequestMessage message = new HttpRequestMessage(HttpMethod.Get, uri);

            await clientEndpoint.ResolveStorageCredentials().SignHttpRequestMessageAsync(message);

            var response = await http.SendAsync(message);
            var json = await response.Content.ReadAsStringAsync();

            var result = JsonConvert.DeserializeObject<T>(json);
            return result;
        }

        public string GetInvoiceEditExperienceUri(Guid datasourceId, Guid invoiceId, int cropYear)
        {
            return $"{connectWebUrl}/?type=invoices&dataSourceId={datasourceId}&productionId={invoiceId}&cropYear={cropYear}";
        }
    }
}
