﻿using Landdb.Client.Infrastructure;
using System;
using System.Threading.Tasks;

namespace Landdb.Client.Services {
    public interface IConnectEngine {

        Task StartSynchronization(Guid datasourceId);
        IConnectInvoiceRepository InvoiceRepository { get; }
        void Subscribe(Action subscriberAction);
    }
}
