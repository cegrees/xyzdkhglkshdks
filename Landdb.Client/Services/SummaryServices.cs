﻿using Landdb.Client.Infrastructure;
using Landdb.Client.Models;
using Landdb.Domain.ReadModels.Yield;
using Landdb.Domain.ReadModels.YieldLocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.Client.Services {
    public static class SummaryServices {
        public static IDictionary<CompositeUnit, YieldQuantity> GetYieldData(IClientEndpoint clientEndpoint, CropYearId cropYearId, CropZoneId cropZoneId) {
            var yieldSummaryItems = new Dictionary<CompositeUnit, YieldQuantity>();
            var yieldData = clientEndpoint.GetView<CropZoneLoadDataView>(cropYearId).GetValue(new CropZoneLoadDataView()).Items.Where(x => x.CropZoneId == cropZoneId);
			var yieldLocations = clientEndpoint.GetView<YieldLocationListView>(new DataSourceId(cropYearId.DataSourceId)).GetValue(() => new YieldLocationListView()).YieldLocations;

			var locationsExcludedFromInventory = from loc in yieldLocations
												 where loc.IsActiveInCropYear(cropYearId.Id) && loc.IsQuantityExcludedFromInventory
												 select loc.Id.Id;

			var sortedUnitFrequencyList = from item in yieldData
                                          where item.CropZoneId == cropZoneId
                                          where item.FinalQuantityUnit != null
                                          group item by item.FinalQuantityUnit into g
                                          orderby g.Count() descending
                                          select g.Key;

            foreach (var unit in sortedUnitFrequencyList) {
				//var itemsWithThisUnit = yieldData.Where(x => x.FinalQuantityUnit == unit);

				// grab only items with this unit that were not delivered to an inventory-exluded location
				var nonExcludedItemsWithThisUnit = from item in yieldData
												   from loc in item.DestinationLocations
												   where item.FinalQuantityUnit == unit
														&& !locationsExcludedFromInventory.Contains(loc.Id.Id)
												   select item;

				// weight the area weight quantity additionally by the location weight
                var totalQuantity = (double)nonExcludedItemsWithThisUnit.Sum(x => (1m / x.DestinationLocations.Count) * x.AreaWeightedFinalQuantity);

                yieldSummaryItems.Add(unit, new YieldQuantity() {
                    TotalQuantity = totalQuantity,
                    QuantityUnit = unit
                });
            }

            return yieldSummaryItems;
        }
    }
}
