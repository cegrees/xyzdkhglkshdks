﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.Infrastructure {
    public interface IApplicationUpdateStatus {
        bool IsUpdating { get; set; }
        string StatusText { get; set; }
        double MaxProgress { get; set; }
        double CurrentProgress { get; set; }
        bool? IsUpdateAvailable { get; set; } 
    }
}
