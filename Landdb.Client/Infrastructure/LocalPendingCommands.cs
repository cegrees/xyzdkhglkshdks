﻿using Landdb.Client.Account;
using Lokad.Cqrs;
using Lokad.Cqrs.AtomicStorage;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.Client.Infrastructure {
    public class LocalPendingCommands {
        NuclearStorage persistentDocumentStore;
        IMalleableEventStore clientEventStore;
        List<IDomainCommand> pendingCommands = new List<IDomainCommand>();
        RedirectToCommand serviceCommands;
        IAccountStatus accountStatus;
        ConcurrentDictionary<Guid, int> recentlyRemovedCommands = new ConcurrentDictionary<Guid, int>();
        readonly string pendingCommandName = "pending_commands";
        Logger log = LogManager.GetCurrentClassLogger();

        object serializationLock = new object();
        DateTime ts = DateTime.UtcNow;

        public LocalPendingCommands(RedirectToCommand serviceCommands, NuclearStorage persistentDocumentStore, IMalleableEventStore clientEventStore, IAccountStatus accountStatus) {
            this.serviceCommands = serviceCommands;
            this.persistentDocumentStore = persistentDocumentStore;
            this.clientEventStore = clientEventStore;
            this.accountStatus = accountStatus;

            Initialize();
        }

        public List<IDomainCommand> PendingCommands {
            get { return pendingCommands; }
            set { pendingCommands = value; }
        }

        public void Initialize() {
            lock (serializationLock) {
                var m = persistentDocumentStore.GetEntity<PendingCommandSerializedState>(pendingCommandName);
                var state = m.HasValue ? m.Value : new PendingCommandSerializedState();
                PendingCommands = state.Deserialize();
            }
        }

        public void PersistState() {
            //if ((DateTime.UtcNow - ts).TotalSeconds > 5) {
                ts = DateTime.UtcNow;
                var state = PendingCommandSerializedState.Capture(PendingCommands);
                persistentDocumentStore.UpdateEntityEnforcingNew<PendingCommandSerializedState>(pendingCommandName, x => { x.Data = state.Data; });
            //}
        }

        public void AddCommand(IDomainCommand cmd) {
            lock (serializationLock) {
                if (cmd == null) { return; }
                PendingCommands.Add(cmd);
                PersistState();
            }
        }

        public bool RemoveCommandById(IIdentity aggregateId, Guid commandId) {
            lock (serializationLock) {
                int removed = PendingCommands.RemoveAll(x => x.Metadata.MessageId == commandId);
                clientEventStore.RemoveEventsSourcedFromCommand(aggregateId, commandId);
                PersistState();

                if (removed == 0 && recentlyRemovedCommands.ContainsKey(commandId)) {
                    removed = 1;
                }

                recentlyRemovedCommands.AddOrUpdate(commandId, removed, (k, v) => v + removed);

                return removed > 0;
            }
        }

        public void ReplayLocalCommands() {
            var watch = Stopwatch.StartNew();
            List<Tuple<IIdentity, Guid>> failedCommands = new List<Tuple<IIdentity, Guid>>();

            foreach (var cmd in PendingCommands) {
                try {
                    serviceCommands.Invoke(cmd);
                } catch (Exception ex) {
                    log.ErrorException("Failed to process a pending command.", ex);
                    failedCommands.Add(new Tuple<IIdentity, Guid>(((dynamic)cmd).Id as IIdentity, cmd.Metadata.MessageId));
                }
            }

            foreach (var cmd in failedCommands) {
                RemoveCommandById(cmd.Item1, cmd.Item2);
            }

            watch.Stop();

            var seconds = watch.Elapsed.TotalSeconds;
            if (seconds > 10) {
                SystemObserver.Notify("[Warn]: Pending command replay took {0:0.0} seconds", seconds);
            }
        }

        [DataContract]
        public class PendingCommandSerializedState {
            [DataMember(Order = 1)]
            public string Data { get; set; }

            internal List<IDomainCommand> Deserialize() {
                if (string.IsNullOrWhiteSpace(Data)) { return new List<IDomainCommand>(); }

                JsonSerializerSettings settings = new JsonSerializerSettings() { TypeNameHandling = Newtonsoft.Json.TypeNameHandling.All };
                var list = JsonConvert.DeserializeObject<List<IDomainCommand>>(Data, settings);
                return list;
            }

            internal static PendingCommandSerializedState Capture(List<IDomainCommand> commands) {
                JsonSerializerSettings settings = new JsonSerializerSettings() { TypeNameHandling = Newtonsoft.Json.TypeNameHandling.All };
                return new PendingCommandSerializedState() { Data = JsonConvert.SerializeObject(commands, settings) };
            }
        }
    }
}
