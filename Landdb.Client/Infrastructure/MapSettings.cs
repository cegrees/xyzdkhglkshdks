﻿using System.Runtime.CompilerServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Landdb.Client.Localization;
using Landdb.Client.Services;
using Landdb.Web.ServiceContracts.Data;

namespace Landdb.Client.Infrastructure {
    [DataContract]
    public class MapSettings {
        public MapSettings() {
            BingMapVisible = true;
            BingMapType = "Bing";
            CMGMapType = "Roads Only";
            DisplayCoordinateFormat = "Decimal Degree";
            MapInfoSelectorItem = new MapInfoSelectorItem(BingMapType);
            CMGMapInfoSelectorItem = new MapInfoSelectorItem(CMGMapType);
            MapCoordinateFormatDisplayItem = new MapCoordinateFormatDisplayItem(DisplayCoordinateFormat);
            MapAreaUnit = DataSourceCultureInformation.DataSourceCulture.Name.Equals("en-US") ? "acres" : "hectares";
            AreaDecimals = 2;
            DisplayAreaInFieldLabel = false;
            DisplayAreaOnlyLabel = false;
            CMGFieldOrCropzones = true;
            FieldsOpacity = 100;
            FieldsVisible = true;
            UnselectedOpacity = 100;
            UnselectedVisible = true;
            MapAnnotationOpacity = 100;
            MapAnnotationsVisible = true;
            CMGMapAnnotationsVisible = true;
            MapAnnotationLabelsVisible = false;
            ScalingTextVisible = true;
            ScalingTextMinimumSize = 4;
            ScalingTextMaximumSize = 24;
            MapAnnotationCategories = new Dictionary<string, MapAnnotationCategory>();
            CropFilterColors = new Dictionary<string, MapColorItem>();
            VarietyFilterColors = new Dictionary<string, MapColorItem>();
            SSurgoColors = new Dictionary<string, MapColorItem>();
            TagFilterColors = new Dictionary<string, MapColorItem>();
            VarietyTagFilterColors = new Dictionary<string, MapColorItem>();
            ContractFilterColors = new Dictionary<string, MapColorItem>();
            ProductionContractFilterColors = new Dictionary<string, MapColorItem>();
            DisplayFillColor = true;
            DisplayCMGLegend = true;
            BorderSize = 2;
            AllowLabelOverlapping = true;
            AllowOfflineMap = true;
            FittingPolygon = true;
            FittingPolygonFactor = 2;
            BorderRowWidth = 30;
            //ZoomLevel = "Custom40";
            ZoomLevel = "StandardPlus40";
            DisplayMapZoomBar = false;

        }

        [DataMember(Order = 1)]
        public bool BingMapVisible { get; set; }

        [DataMember(Order = 2)]
        public string BingMapType { get; set; }

        [DataMember(Order = 3)]
        public string CMGMapType { get; set; }

        [DataMember(Order = 4)]
        public string DisplayCoordinateFormat { get; set; }

        [DataMember(Order = 5)]
        public string MapAreaUnit { get; set; }

        [DataMember(Order = 6)]
        public int AreaDecimals { get; set; }

        [DataMember(Order = 7)]
        public int FieldsOpacity { get; set; }

        [DataMember(Order = 8)]
        public bool FieldsVisible { get; set; }

        [DataMember(Order = 9)]
        public int UnselectedOpacity { get; set; }

        [DataMember(Order = 9)]
        public bool UnselectedVisible { get; set; }

        [DataMember(Order = 10)]
        public int MapAnnotationOpacity { get; set; }

        [DataMember(Order = 11)]
        public bool MapAnnotationsVisible { get; set; }

        [DataMember(Order = 12)]
        public bool ScalingTextVisible { get; set; }

        [DataMember(Order = 13)]
        public int ScalingTextMinimumSize { get; set; }

        [DataMember(Order = 14)]
        public int ScalingTextMaximumSize { get; set; }

        [DataMember(Order = 15)]
        public string FieldsSelectedFillColor { get; set; }

        [DataMember(Order = 16)]
        public string FieldsUnSelectedFillColor { get; set; }

        [DataMember(Order = 17)]
        public Dictionary<string, MapAnnotationCategory> MapAnnotationCategories { get; set; }

        [DataMember(Order = 18)]
        public Dictionary<string, MapColorItem> CropFilterColors { get; set; }

        [DataMember(Order = 19)]
        public Dictionary<string, MapColorItem> VarietyFilterColors { get; set; }

        [DataMember(Order = 20)]
        public bool? CMGMapAnnotationsVisible { get; set; }

        [DataMember(Order = 21)]
        public bool? MapAnnotationLabelsVisible { get; set; }

        [DataMember(Order = 22)]
        public bool? DisplayAreaInFieldLabel { get; set; }

        [DataMember(Order = 23)]
        public Dictionary<string, MapColorItem> SSurgoColors { get; set; }

        [DataMember(Order = 24)]
        public bool? DisplayFillColor { get; set; }

        [DataMember(Order = 25)]
        public int BorderSize { get; set; }

        [DataMember(Order = 26)]
        public bool? DisplayCMGLegend { get; set; }

        [DataMember(Order = 27)]
        public bool? CMGFieldOrCropzones { get; set; }

        [DataMember(Order = 28)]
        public Dictionary<string, MapColorItem> TagFilterColors { get; set; }

        [DataMember(Order = 29)]
        public bool? AllowLabelOverlapping { get; set; }

        [DataMember(Order = 30)]
        public bool? FittingPolygon { get; set; }

        [DataMember(Order = 31)]
        public int FittingPolygonFactor { get; set; }

        [DataMember(Order = 32)]
        public Dictionary<string, MapColorItem> VarietyTagFilterColors { get; set; }

        [DataMember(Order = 33)]
        public Dictionary<string, MapColorItem> ContractFilterColors { get; set; }

        [DataMember(Order = 34)]
        public Dictionary<string, MapColorItem> ProductionContractFilterColors { get; set; }

        [DataMember(Order = 35)]
        public string ZoomLevel { get; set; }

        [DataMember(Order = 36)]
        public bool? AllowOfflineMap { get; set; }

        [DataMember(Order = 37)]
        public Dictionary<string, MapColorItem> FSAFilterColors { get; set; }

        [DataMember(Order = 38)]
        public bool? DisplayAreaOnlyLabel { get; set; }

        [DataMember(Order = 39)]
        public string FieldsSelectedBorderColor { get; set; }

        [DataMember(Order = 40)]
        public string FieldsUnSelectedBorderColor { get; set; }

        [DataMember(Order = 41)]
        public bool? DisplayMapZoomBar { get; set; }

        [DataMember(Order = 42)]
        public double? BorderRowWidth { get; set; }

        [IgnoreDataMember]
        public MapInfoSelectorItem MapInfoSelectorItem {
            get => new MapInfoSelectorItem(BingMapType);
            set => BingMapType = value.MapTypeKeyText;
        }

        [IgnoreDataMember]
        public MapInfoSelectorItem CMGMapInfoSelectorItem {
            get => new MapInfoSelectorItem(CMGMapType);
            set => CMGMapType = value.MapTypeKeyText;
        }

        [IgnoreDataMember]
        public MapCoordinateFormatDisplayItem MapCoordinateFormatDisplayItem {
            get => new MapCoordinateFormatDisplayItem(DisplayCoordinateFormat);
            set => DisplayCoordinateFormat = value.KeyText;
        }
    }

    [DataContract]
    public class MapAnnotationCategory {
        [DataMember(Order = 1)]
        public string Name { get; set; }

        [DataMember(Order = 2)]
        public string ShapeType { get; set; }

        [DataMember(Order = 3)]
        public string StyleName { get; set; }

        [DataMember(Order = 4)]
        public bool Visible { get; set; }

        [IgnoreDataMember]
        public string DisplayText => AnnotationsDisplayItem.GetAnnotationStyleDisplayTextFor(Name);

        public override string ToString() {
            return Name;
        }
    }

    [DataContract]
    public class MapColorItem {
        [DataMember(Order = 1)]
        public string Key { get; set; }

        [DataMember(Order = 2)]
        public string Label { get; set; }

        [DataMember(Order = 3)]
        public string CropName { get; set; }

        [DataMember(Order = 4)]
        public int MapColor { get; set; }

        [DataMember(Order = 5)]
        public bool Visible { get; set; }

        [DataMember(Order = 6)]
        public int MapColorFG { get; set; }

        [DataMember(Order = 7)]
        public string HatchType { get; set; }

        public override string ToString() {
            return CropName;
        }
    }

}
