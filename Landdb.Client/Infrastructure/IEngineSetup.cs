﻿using System;
namespace Landdb.Client.Infrastructure {
    public interface IEngineSetup {
        Landdb.Client.Account.AccountStatus AccountStatus { get; set; }
        Container Build();
        void ConfigureQueues(int serviceQueueCount, int adapterQueueCount);
    }
}
