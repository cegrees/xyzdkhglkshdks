﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Landdb.Client.Infrastructure {
    public static class RemoteConfigurationSettings {
        static string overriddenBaseUri = null;

        public static string GetBaseUri() {
            if (string.IsNullOrEmpty(overriddenBaseUri)) {
                return Properties.Settings.Default.RemoteBaseUri;
            } else { return overriddenBaseUri; }
        }

        public static void OverrideBaseUri(string uri) {
            overriddenBaseUri = uri;
        }
        public static string MasterlistRemoteUri { get; set; } = "https://masterlist.agconnections.com/api";
        public static string ConnectRemoteApiUri { get; set; } = "https://connect.landdb.com/api/ext";
        public static string ConnectRemoteWebAppUri { get; set; } = "https://connect.landdb.com";
        public static string ConnectApiKey { get; set; } = "ff799ffc486444b398a04c01319f7767";

        public static string GetRegistrationServicesUri() {
            return string.Format(@"{0}api/registration/", GetBaseUri());
        }

        public static string GetDataSynchronizationServicesUri() {
            return string.Format(@"{0}api/datasynchronization/", GetBaseUri());
        }
        public static string PlanetLabsRemoteUri { get; set; } = "http://agc-jd-connect-t0117.eastus2.cloudapp.azure.com/planet-labs/api";
    }
}
