﻿using AgC.UnitConversion;
using Landdb.Domain.ReadModels.ProductSetting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.Client.Infrastructure {
    public static class DisplayMeasureConverter {
        public static Measure ConvertToRateDisplayMeasure(ProductId productId, decimal originalMeasureValue, string originalMeasureUnit, double? productDensity, IClientEndpoint clientEndpoint, Guid dsId) {
            var calculatedRate = clientEndpoint.GetProductUnitResolver()
                .GetPackageSafeUnit(productId, originalMeasureUnit)
                .GetMeasure((double)originalMeasureValue, productDensity);

            return ConvertToRateDisplayMeasure(productId, calculatedRate, clientEndpoint, dsId);
        }

        public static Measure ConvertToRateDisplayMeasure(ProductId productId, Measure originalMeasure, IClientEndpoint clientEndpoint, Guid dataSourceId) {
            var product = clientEndpoint.GetMasterlistService().GetProduct(productId);
            if (product == null) { return originalMeasure; }
            var density = product.Density;
            var displayRate = originalMeasure.Unit.GetMeasure(originalMeasure.Value, density);
            string displayRateUnit = null;
            var productSettingMaybe = clientEndpoint.GetView<ProductSettingsView>(new ProductSettingId(dataSourceId, productId));
            if (productSettingMaybe.HasValue)
            {
                displayRateUnit = string.IsNullOrEmpty(productSettingMaybe.Value.RatePerAreaUnit) ? null : productSettingMaybe.Value.RatePerAreaUnit;
                if (displayRateUnit != null && displayRate.Unit.Name != displayRateUnit)
                {
                    displayRate = displayRate.CreateAs(clientEndpoint.GetProductUnitResolver().GetPackageSafeUnit(productId, displayRateUnit));
                }
            }
            //clientEndpoint.GetUserSettings().PreferredRateUnits.TryGetValue(productId.Id, out displayRateUnit);
            //if (displayRateUnit != null && displayRate.Unit.Name != displayRateUnit) {
            //    displayRate = displayRate.CreateAs(clientEndpoint.GetProductUnitResolver().GetPackageSafeUnit(productId, displayRateUnit));
            //}

            return displayRate;
        }

        public static Measure ConvertToRatePer100DisplayMeasure(ProductId productId, decimal originalMeasureValue, string originalMeasureUnit, double? productDensity, IClientEndpoint clientEndpoint, Guid dsId)
        {
            var calculatedRate = clientEndpoint.GetProductUnitResolver()
                .GetPackageSafeUnit(productId, originalMeasureUnit)
                .GetMeasure((double)originalMeasureValue, productDensity);

            return ConvertToRatePer100DisplayMeasure(productId, calculatedRate, clientEndpoint, dsId);
        }

        public static Measure ConvertToRatePer100DisplayMeasure(ProductId productId, Measure originalMeasure, IClientEndpoint clientEndpoint, Guid dataSourceId)
        {
            var product = clientEndpoint.GetMasterlistService().GetProduct(productId);

            if (product == null) { return originalMeasure; }

            var density = product.Density;

            var displayRatePer100 = originalMeasure.Unit.GetMeasure(originalMeasure.Value, density);
            string displayRateUnit = null;
            //try
            //{
            var productSettingMaybe = clientEndpoint.GetView<ProductSettingsView>(new ProductSettingId(dataSourceId, productId));
            if (productSettingMaybe.HasValue)
            {
                displayRateUnit = string.IsNullOrEmpty(productSettingMaybe.Value.RatePer100Unit) ? null : productSettingMaybe.Value.RatePer100Unit;
                if (displayRateUnit != null && displayRatePer100.Unit.Name != displayRateUnit)
                {
                    displayRatePer100 = displayRatePer100.CreateAs(clientEndpoint.GetProductUnitResolver().GetPackageSafeUnit(productId, displayRateUnit));
                }
            }

            return displayRatePer100;
        }

        public static Measure ConvertToRatePerTankDisplayMeasure(ProductId productId, decimal originalMeasureValue, string originalMeasureUnit, double? productDensity, IClientEndpoint clientEndpoint, Guid dsId)
        {
            var calculatedRate = clientEndpoint.GetProductUnitResolver()
                .GetPackageSafeUnit(productId, originalMeasureUnit)
                .GetMeasure((double)originalMeasureValue, productDensity);

            return ConvertToRatePerTankDisplayMeasure(productId, calculatedRate, clientEndpoint, dsId);
        }

        public static Measure ConvertToRatePerTankDisplayMeasure(ProductId productId, Measure originalMeasure, IClientEndpoint clientEndpoint, Guid dataSourceId)
        {
			var product = clientEndpoint.GetMasterlistService().GetProduct(productId);

			if (product == null) { return originalMeasure; }

			var density = product.Density;

            var displayRatePerTank = originalMeasure.Unit.GetMeasure(originalMeasure.Value, density);
            string displayRateUnit = null;
            //try
            //{
            var productSettingMaybe = clientEndpoint.GetView<ProductSettingsView>(new ProductSettingId(dataSourceId, productId));
            if (productSettingMaybe.HasValue)
            {
                displayRateUnit = string.IsNullOrEmpty(productSettingMaybe.Value.RatePerTankUnit) ? null : productSettingMaybe.Value.RatePerTankUnit;
                if (displayRateUnit != null && displayRatePerTank.Unit.Name != displayRateUnit)
                {

                    displayRatePerTank = displayRatePerTank.CreateAs(clientEndpoint.GetProductUnitResolver().GetPackageSafeUnit(productId, displayRateUnit));
                }
            }

            return displayRatePerTank;

                //clientEndpoint.GetUserSettings().PreferredRatePerTankUnits.TryGetValue(productId.Id, out displayRateUnit);
                //if (displayRateUnit != null && displayRatePerTank.Unit.Name != displayRateUnit)
                //{
                //    displayRatePerTank = displayRatePerTank.CreateAs(clientEndpoint.GetProductUnitResolver().GetPackageSafeUnit(productId, displayRateUnit));
                //}

            //}
            //catch {
            //    if (clientEndpoint.GetUserSettings().PreferredRatePerTankUnits == null)
            //    {
            //        clientEndpoint.GetUserSettings().PreferredRatePerTankUnits = new Dictionary<Guid, string>();
            //        clientEndpoint.SaveUserSettings();
            //    }
            //}
            
        }

        public static Measure ConvertToTotalDisplayMeasure(ProductId productId, decimal originalMeasureValue, string originalMeasureUnit, double? productDensity, IClientEndpoint clientEndpoint, Guid dsId) {
            return ConvertToTotalDisplayMeasure(productId, (double)originalMeasureValue, originalMeasureUnit, productDensity, clientEndpoint, dsId);
        }

        public static Measure ConvertToTotalDisplayMeasure(ProductId productId, double originalMeasureValue, string originalMeasureUnit, double? productDensity, IClientEndpoint clientEndpoint, Guid dsId) {
            var calculatedRate = clientEndpoint.GetProductUnitResolver()
                .GetPackageSafeUnit(productId, originalMeasureUnit)
                .GetMeasure(originalMeasureValue, productDensity);

            return ConvertToTotalDisplayMeasure(productId, calculatedRate, clientEndpoint, dsId);
        }

        public static Measure ConvertToTotalDisplayMeasure(ProductId productId, Measure originalMeasure, IClientEndpoint clientEndpoint, Guid dataSourceId) {
            var displayTotal = originalMeasure;
            string displayTotalUnit = null;

            var productSettingMaybe = clientEndpoint.GetView<ProductSettingsView>(new ProductSettingId(dataSourceId, productId));
            if (productSettingMaybe.HasValue)
            {
                displayTotalUnit = string.IsNullOrEmpty(productSettingMaybe.Value.TotalUnit) ? null : productSettingMaybe.Value.TotalUnit;
                if (displayTotalUnit != null && displayTotal.Unit.Name != displayTotalUnit)
                {
                    displayTotal = displayTotal.CreateAs(clientEndpoint.GetProductUnitResolver().GetPackageSafeUnit(productId, displayTotalUnit));
                }
            }
            //clientEndpoint.GetUserSettings().PreferredTotalUnits.TryGetValue(productId.Id, out displayRateUnit);
            //if (displayRateUnit != null && displayRate.Unit.Name != displayRateUnit) {
            //    displayRate = displayRate.CreateAs(clientEndpoint.GetProductUnitResolver().GetPackageSafeUnit(productId, displayRateUnit));
            //}

            return displayTotal;
        }
    }
}
