﻿using System;
using System.Threading.Tasks;

namespace Landdb.Infrastructure {
    public interface IApplicationUpdater {
        Task<bool> CheckForUpdatesAsync();
        Task PerformUpdateAsync();
        void GetOptionalFilegroup(string groupName, IApplicationUpdateStatus updateStatus, Action onComplete = null);
        bool OptionalFilegroupExists(string groupName);

        Task<bool> CheckForPrerequisiteUpdatesAsync();
    }
}
