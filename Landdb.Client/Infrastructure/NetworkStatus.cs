﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices;

namespace Landdb.Client.Infrastructure {
    public static class NetworkStatus {
        static bool wasOnlineEarlier;
        static bool? overrideValue;
        static bool? allowForVirtualNetworks;

        [DllImport("wininet.dll", SetLastError = true)]
        private static extern bool InternetCheckConnection(string lpszUrl, int dwFlags, int dwReserved);

        /// <summary> 
        /// Indicates whether any network connection is available 
        /// Filter connections below a specified speed, as well as virtual network cards. 
        /// </summary> 
        /// <returns> 
        ///     <c>true</c> if a network connection is available; otherwise, <c>false</c>. 
        /// </returns> 
        public static bool IsNetworkAvailable() {
            return IsNetworkAvailable(0);
        }

        /// <summary> 
        /// Indicates whether any network connection is available. 
        /// Filter connections below a specified speed, as well as virtual network cards. 
        /// </summary> 
        /// <param name="minimumSpeed">The minimum speed required. Passing 0 will not filter connection using speed.</param> 
        /// <returns> 
        ///     <c>true</c> if a network connection is available; otherwise, <c>false</c>. 
        /// </returns> 
        public static bool IsNetworkAvailable(long minimumSpeed) {
            if (overrideValue.HasValue) { return overrideValue.Value; }

            if (!NetworkInterface.GetIsNetworkAvailable())
                return false;

            foreach (NetworkInterface ni in NetworkInterface.GetAllNetworkInterfaces()) {
                // discard because of standard reasons 
                if ((ni.OperationalStatus != OperationalStatus.Up) ||
                    (ni.NetworkInterfaceType == NetworkInterfaceType.Loopback) ||
                    (ni.NetworkInterfaceType == NetworkInterfaceType.Tunnel))
                    continue;

                // this allow to filter modems, serial, etc. 
                // I use 10000000 as a minimum speed for most cases 
                if (ni.Speed < minimumSpeed)
                    continue;

                // discard virtual cards (virtual box, virtual pc, parallels, etc.) 
                if (!allowForVirtualNetworks.HasValue || (allowForVirtualNetworks.HasValue && !allowForVirtualNetworks.Value)) {
                    if ((ni.Description.IndexOf("virtual", StringComparison.OrdinalIgnoreCase) >= 0) ||
                        (ni.Name.IndexOf("virtual", StringComparison.OrdinalIgnoreCase) >= 0)) {
                        // my virtual network needs to return true;
                        continue;
                    }
                }
                return true;
            }
            return false;
        }

        public static bool IsInternetAvailable() {
            if (overrideValue.HasValue) { return overrideValue.Value; }

            bool network = IsNetworkAvailable();
            bool internet = InternetCheckConnection("http://www.msftncsi.com/ncsi.txt", 1, 0);
            wasOnlineEarlier = network && internet;
            return network && internet;
        }

        public static bool IsInternetAvailableFast() {
            if (overrideValue.HasValue) { return overrideValue.Value; }

            if (wasOnlineEarlier) { return true; }
            return IsInternetAvailable();
        }

        public static void SetOverride(bool? isOnline) {
            overrideValue = isOnline;
        }

        public static void SetAllowForVirtualNetworks(bool? allowForVNetworks) {
            allowForVirtualNetworks = allowForVNetworks;
        }
    }
}
