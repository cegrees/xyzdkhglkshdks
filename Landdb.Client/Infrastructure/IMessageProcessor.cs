﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Landdb.Web.ServiceContracts;
using Landdb.Web.ServiceContracts.Data;

namespace Landdb.Client.Infrastructure {


    public interface IMessageProcessor {
        void ProcessEvent(RecordedEventStoreData e);
        void ProcessSlice(EventStoreRecordSlice slice);
    }
}
