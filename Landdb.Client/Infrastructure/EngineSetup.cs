﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lokad.Cqrs.StreamingStorage;
using Lokad.Cqrs.TapeStorage;
using Lokad.Cqrs.AtomicStorage;
using Lokad.Cqrs.Partition;
using Lokad.Cqrs;
using Landdb.Wires;
using Lokad.Cqrs.Build;
using Landdb.Domain;
using Landdb.Client.Services.Web;
using Landdb.Web.ServiceContracts;
using Landdb.Client.Account;
using System.Threading;
using ServiceStack.Text;
using Newtonsoft.Json;
using Lokad.Cqrs.Envelope;
using Landdb.Client.Wires;
using Lokad.Cqrs.Dispatch;
using System.Diagnostics;
using System.Threading.Tasks;
using Landdb.Domain.ReadModels;
using Landdb.Client.Wires.MemoryStorage;
using Landdb.Services;
using Landdb.Infrastructure;

namespace Landdb.Client.Infrastructure {


    public sealed class EngineSetup : Landdb.Client.Infrastructure.IEngineSetup {

        static readonly string FunctionalRecorderQueueName = Conventions.FunctionalEventRecorderQueue;
        static readonly string RouterQueueName = Conventions.DefaultRouterQueue;
        static readonly string ErrorsContainerName = Conventions.DefaultErrorsFolder;
        static readonly string RemoteSubmissionErrorsContainerName = Conventions.DefaultRemoteSubmissionErrorsFolder;

        const string EventProcessingQueueName = Conventions.Prefix + "-handle-events";
        const string AggregateHandlerQueueName = Conventions.Prefix + "-handle-cmd-entity";
        string[] _serviceQueues;


        public void ConfigureQueues(int serviceQueueCount, int adapterQueueCount) {
            _serviceQueues = Enumerable
                .Range(0, serviceQueueCount)
                .Select((s, i) => Conventions.Prefix + "-handle-cmd-service-" + i)
                .ToArray();
        }

        public static string TapesContainer = Conventions.Prefix + "-tapes";
        //public const string TapesContainer = Conventions.Prefix + "-tapes-pb";

        public static /*readonly*/ EnvelopeStreamer Streamer = Contracts.CreateStreamer();
        public static readonly IDocumentStrategy ViewStrategy = new ViewStrategy();
        public static readonly IDocumentStrategy DocStrategy = new DocumentStrategy();

        public IStreamRoot Streaming;

        public Func<string, IQueueWriter> QueueWriterFactory;
        public Func<string, IQueueWriter> FastQueueWriterFactory;
        public Func<string, IQueueReader> QueueReaderFactory;
        public Func<string, IQueueReader> FastQueueReaderFactory;
        public Func<string, IAppendOnlyStore> AppendOnlyStoreFactory;
        public Func<IDocumentStrategy, IDocumentStore> DocumentStoreFactory;
        public Func<IDocumentStrategy, IDocumentStore> PersistentDocumentStoreFactory;

        public Func<IMasterlistService, IProductUnitResolver> ProductUnitResolverFactory;
        public Func<IDocumentStore, IMasterlistService> MasterlistServiceFactory;
        public Func<IInventoryCalculationService> InventoryCalculationServiceFactory;

        public Guid DeviceId;
        public Account.AccountStatus AccountStatus { get; set; }
        public Func<IDataSynchronizationClient> ResolveRemoteSubmissionClient;
        public IRemoteSubmissionErrorNotifier RemoteSubmissionErrorNotifier;
        public IApplicationUpdater ApplicationUpdater { get; set; }

        public Container Build() {
            var appendOnlyStore = AppendOnlyStoreFactory(TapesContainer);
            var messageStore = new MessageStore(appendOnlyStore, Streamer.MessageSerializer);

            var sendToRouterQueue = new MessageSender(Streamer, QueueWriterFactory(RouterQueueName));
            var sendToFunctionalRecorderQueue = new MessageSender(Streamer, QueueWriterFactory(FunctionalRecorderQueueName));
            var sendToEventProcessingQueue = new MessageSender(Streamer, QueueWriterFactory(EventProcessingQueueName));

            var sendSmart = new TypedMessageSender(sendToRouterQueue, sendToFunctionalRecorderQueue);

            var remoteStore = new Landdb.Wires.EventStore(messageStore, sendToEventProcessingQueue, DeviceId);
            var localStore = new HybridEventStore(remoteStore, sendToEventProcessingQueue);

            var quarantine = new EnvelopeQuarantine(Streamer, sendSmart, Streaming.GetContainer(ErrorsContainerName));

            var builder = new CqrsEngineBuilder(Streamer, quarantine);

            var events = new RedirectToDynamicEvent();
            var commands = new RedirectToCommand();
            var funcs = new RedirectToCommand();

            var persistentDocs = PersistentDocumentStoreFactory(DocStrategy);
            var stateDocs = new NuclearStorage(persistentDocs);

            LocalPendingCommands pendingCommandStore = new LocalPendingCommands(commands, stateDocs, localStore, AccountStatus);

            // Handle domain events
            builder.Handle(QueueReaderFactory(EventProcessingQueueName), aem => CallHandlers(events, aem), "watch");

            // Handle domain commands
            builder.Handle(QueueReaderFactory(AggregateHandlerQueueName), aem => CallHandlers(commands, aem, QueueWriterFactory(Conventions.RemoteCommandSubmissionQueue), pendingCommandStore));

            // Handle functional commands.
            builder.Handle(QueueReaderFactory(RouterQueueName), MakeRouter(messageStore), "watch");

            // multiple service queues
            _serviceQueues.For_Each(s => builder.Handle(QueueReaderFactory(s), aem => CallHandlers(funcs, aem, QueueWriterFactory(Conventions.RemoteCommandSubmissionQueue), pendingCommandStore)));

            builder.Handle(QueueReaderFactory(FunctionalRecorderQueueName), aem => RecordFunctionalEvent(aem, messageStore));
            var viewDocs = DocumentStoreFactory(ViewStrategy);
            //var persistentDocs = PersistentDocumentStoreFactory(ViewStrategy);

            var vector = new DomainIdentityGenerator(stateDocs);
            //var ops = new StreamOps(Streaming);
            var projections = new ProjectionsConsumingOneBoundedContext();

            // Create the unit resolver, using the projection documents for user-created products
            var masterlist = MasterlistServiceFactory(viewDocs);
            var unitResolver = ProductUnitResolverFactory(masterlist);
            var inventoryCalc = InventoryCalculationServiceFactory();

            // Domain Bounded Context
            DomainBoundedContext.ApplicationServices(localStore, unitResolver, DeviceId.ToString()).For_Each(commands.WireToWhen);
            //DomainBoundedContext.EntityApplicationServices(viewDocs, store, vector).For_Each(commands.WireToWhen);
            //DomainBoundedContext.FuncApplicationServices().For_Each(funcs.WireToWhen);
            //DomainBoundedContext.Ports(sender).For_Each(events.WireToWhen);
            //DomainBoundedContext.Tasks(sender, viewDocs, true).For_Each(builder.AddTask);
            projections.RegisterFactory(Landdb.Domain.ReadModels.BoundedContext.Projections);

            // Client Bounded Context
            //projections.RegisterFactory(Landdb.Client.Projections.ClientBoundedContext.Projections);

            // wire all projections
            projections.BuildFor(viewDocs, unitResolver, masterlist, inventoryCalc).For_Each(events.WireToWhen);


            if (NetworkStatus.IsInternetAvailable()) { // only wire this up if a network is available. Otherwise, we're in offline mode.
                // wire in remote event submission publisher
                var remoteSubmitter = new RemoteSubmissionPublisher(QueueReaderFactory(Conventions.RemoteCommandSubmissionQueue), Streamer, builder.Duplication, ResolveRemoteSubmissionClient, AccountStatus, DeviceId, Streaming.GetContainer(RemoteSubmissionErrorsContainerName), pendingCommandStore, RemoteSubmissionErrorNotifier);
                builder.AddTask(c => Task.Factory.StartNew(() => remoteSubmitter.Run(c)));
            } else {
                NetworkStatus.SetOverride(false); // Force global offline mode if internet is not available here.
            }

            

            return new Container {
                Builder = builder,
                Setup = this,
                SendToCommandRouter = sendToRouterQueue,
                MessageStore = messageStore,
                ProjectionFactories = projections,
                ViewDocs = viewDocs,
                PersistentDocs = persistentDocs,
                AppendOnlyStore = appendOnlyStore,
                ProductUnitResolver = unitResolver,
                MasterlistService = masterlist,
                InventoryCalculationService = inventoryCalc,
                DirectCommandInvoker = commands,
                EventStore = remoteStore,
                LocalEventStore = localStore,
                PendingCommands = pendingCommandStore
            };
        }

        bool RecordShouldBePublished(StoreRecord storeRecord) {
            return storeRecord.Key != "audit";
        }

        void RecordFunctionalEvent(ImmutableEnvelope envelope, MessageStore store) {
            if (envelope.Message is IFunctionalEvent) store.RecordMessage("func", envelope);
            else throw new InvalidOperationException("Non-functional event {0} landed to queue for tracking stateless events");
        }

        Action<ImmutableEnvelope> MakeRouter(MessageStore tape) {
            var entities = QueueWriterFactory(AggregateHandlerQueueName);
            var processing = _serviceQueues.Select(QueueWriterFactory).ToArray();

            return envelope => {
                var message = envelope.Message;
                if (message is IDomainCommand) {
                    // all commands are recorded to audit stream, as they go through router
                    tape.RecordMessage("audit", envelope);
                }

                if (message is IDomainEvent) {
                    throw new InvalidOperationException("Events are not expected in command router queue");
                }

                var data = Streamer.SaveEnvelopeData(envelope);

                if (message is IDomainCommand<IIdentity>) {
                    entities.PutMessage(data);
                    return;
                }
                if (message is IFunctionalCommand) {
                    // randomly distribute between queues
                    var i = Environment.TickCount % processing.Length;
                    processing[i].PutMessage(data);
                    return;
                }
                throw new InvalidOperationException("Unknown queue format");
            };
        }

        /// <summary>
        /// Helper class that merely makes the concept explicit
        /// </summary>
        public sealed class ProjectionsConsumingOneBoundedContext {
            public delegate IEnumerable<object> FactoryForWhenProjections(IDocumentStore store, IProductUnitResolver productUnitResolver, IMasterlistService masterlistService, IInventoryCalculationService inventoryCalculationService);

            readonly IList<FactoryForWhenProjections> _factories = new List<FactoryForWhenProjections>();

            public void RegisterFactory(FactoryForWhenProjections factory) {
                _factories.Add(factory);
            }

            public IEnumerable<object> BuildFor(IDocumentStore store, IProductUnitResolver productUnitResolver, IMasterlistService masterlistService, IInventoryCalculationService inventoryCalculationService) {
                return _factories.SelectMany(factory => factory(store, productUnitResolver, masterlistService, inventoryCalculationService));
            }
        }

        static void CallHandlers(RedirectToDynamicEvent functions, ImmutableEnvelope aem) {
            var e = aem.Message as IDomainEvent;

            if (e != null) {
                functions.InvokeEvent(e);
            }
        }

        static void CallHandlers(RedirectToCommand serviceCommands, ImmutableEnvelope aem, IQueueWriter remoteCommandSubmissionQueue, LocalPendingCommands pendingCommandStore) {
            var content = aem.Message;
            var watch = Stopwatch.StartNew();
            serviceCommands.Invoke(content);
            watch.Stop();

            var seconds = watch.Elapsed.TotalSeconds;
            if (seconds > 10) {
                SystemObserver.Notify("[Warn]: {0} took {1:0.0} seconds", content.GetType().Name, seconds);
            }

            remoteCommandSubmissionQueue.PutMessage(Streamer.SaveEnvelopeData(aem));

            try {
                pendingCommandStore.AddCommand(content as IDomainCommand);
            } catch {
                // eat the exception. If command is successful remotely, the data will eventually make it back here.
            }
        }
    }

    public sealed class Container : IDisposable {
        public IEngineSetup Setup;
        public CqrsEngineBuilder Builder;
        public MessageSender SendToCommandRouter;
        public MessageStore MessageStore;
        public IAppendOnlyStore AppendOnlyStore;
        public IDocumentStore ViewDocs;
        public IDocumentStore PersistentDocs;
        public EngineSetup.ProjectionsConsumingOneBoundedContext ProjectionFactories;
        public IProductUnitResolver ProductUnitResolver;
        public IMasterlistService MasterlistService;
        public IInventoryCalculationService InventoryCalculationService;
        public RedirectToCommand DirectCommandInvoker;
        public IClientEventStore EventStore;
        public IClientEventStore LocalEventStore;
        public LocalPendingCommands PendingCommands;

        public CqrsEngineHost BuildEngine(CancellationToken token) {
            return Builder.Build(token);
        }

        public void ExecuteStartupTasks(CancellationToken token) {

            // we run S2 projections from 3 different BCs against one domain log
            StartupProjectionRebuilder.Rebuild(
                token,
                ViewDocs as FastMemoryDocumentStore,
                EventStore,
                store => ProjectionFactories.BuildFor(store, ProductUnitResolver, MasterlistService, InventoryCalculationService));

            if (PendingCommands != null) {
                PendingCommands.ReplayLocalCommands();
            }
        }

        public void Dispose() {
            using (AppendOnlyStore) {
                AppendOnlyStore = null;
            }
        }
    }
    public static class ExtendArrayEvil {
        public static void For_Each<T>(this IEnumerable<T> self, Action<T> action) {
            foreach (var variable in self) {
                action(variable);
            }
        }
    }
}
