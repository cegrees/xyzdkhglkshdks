﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace Landdb.Client.Infrastructure
{
    internal class EnumTools {
        public static string ReadDescription(Enum value) {
            return value.GetType().GetMember(value.ToString()).FirstOrDefault()?.GetCustomAttribute<DescriptionAttribute>()?.Description;

            #region Something I found online that could be used to replace the method string

            //FieldInfo fi = source.GetType().GetField(source.ToString());
            //    DescriptionAttribute[] attributes = (DescriptionAttribute[]) fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
            //    if (attributes != null && attributes.Length > 0) return attributes[0].Description;
            //    else return source.ToString();

            #endregion
        }
    }
}