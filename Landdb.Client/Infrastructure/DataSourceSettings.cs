﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Landdb.Client.Infrastructure {
    [DataContract]
    public class DataSourceSettings {
        [DataMember(Order = 1)]
        public Guid DataSourceId { get; set; }
        [DataMember(Order = 2)]
        public bool NeedsFullSynchronization { get; set; }
        [DataMember(Order = 3)]
        public long NextEventNumberToReceive { get; set; }
        [DataMember(Order = 4)]
        public long? NextFailedCommandNumberToReceive { get; set; }
    }
}
