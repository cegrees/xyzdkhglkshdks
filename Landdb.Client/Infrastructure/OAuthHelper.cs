﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Landdb.Client.Infrastructure {
    public static class OAuthHelper {
        public static string GetWrapAuthorizationHeader(string authToken) {
            string authorizationHeader =
                    string.Format("WRAP access_token=\"{0}\"",
                    authToken);

            return authorizationHeader;
        }
    }
}
