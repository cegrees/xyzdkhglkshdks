﻿using AgC.UnitConversion;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Landdb.Client.Infrastructure {
	[DataContract]
	public class UserSettings {
		public UserSettings() {
			PreferredRateUnits = new Dictionary<Guid, string>();
			PreferredTotalUnits = new Dictionary<Guid, string>();
			PreferredRatePerTankUnits = new Dictionary<Guid, string>();
			UserDataSourcesInfo = new List<DataSourceCompanyInfo>();
			UserMassUnitName = AgC.UnitConversion.MassAndWeight.Pound.Self.Name; // default mass unit
			UserAreaUnitName = AgC.UnitConversion.Area.Acre.Self.Name; // default area unit
			IncludeWeatherByDefault = true;
		}

		[DataMember]
		public Guid LastDataSourceId { get; set; }

		[DataMember]
		public int LastCropYear { get; set; }

		[DataMember]
		public bool AreFieldsSortedAlphabetically { get; set; }

		[DataMember]
		public bool IncludeWeatherByDefault { get; set; }

		[DataMember]
		public string UserMassUnitName { get; set; }

		[DataMember]
		public string UserAreaUnitName { get; set; }

		[DataMember]
		public Dictionary<Guid, string> PreferredRateUnits { get; set; }

		[DataMember]
		public Dictionary<Guid, string> PreferredTotalUnits { get; set; }

		[DataMember]
		public Dictionary<Guid, string> PreferredRatePerTankUnits { get; set; }

		[DataMember]
		public List<DataSourceCompanyInfo> UserDataSourcesInfo { get; set; }

		[DataMember]
		public Guid LastReportHeaderEntityId { get; set; }

		[DataMember]
		public double? LastUsedDuration { get; set; }

		[DataMember]
		public string LastUsedWeatherStationId { get; set; }

		[DataMember]
		public DateTime LastUsedReportStartDate { get; set; }

		[DataMember]
		public DateTime LastUsedReportEndDate { get; set; }

		public AgC.UnitConversion.MassAndWeight.MassAndWeightUnit UserMassUnit {
			get { return UnitFactory.GetUnitByName(UserMassUnitName) as AgC.UnitConversion.MassAndWeight.MassAndWeightUnit; }
			set {
				if (value == null) {
					UserMassUnitName = AgC.UnitConversion.MassAndWeight.Pound.Self.Name;
				} else {
					UserMassUnitName = value.Name;
				}
			}
		}

		public AgC.UnitConversion.Area.AreaUnit UserAreaUnit {
			get { return UnitFactory.GetUnitByName(UserAreaUnitName) as AgC.UnitConversion.Area.AreaUnit; }
			set {
				if (value == null) {
					UserAreaUnitName = AgC.UnitConversion.Area.Acre.Self.Name;
				} else {
					UserAreaUnitName = value.Name;
				}
			}
		}
	}

	public class DataSourceCompanyInfo {
		public DataSourceId DataSourceId { get; set; }
		public string DataSourceName { get; set; }
		public string Address { get; set; }
		public string City { get; set; }
		public string State { get; set; }
		public string ZipCode { get; set; }
	}
}
