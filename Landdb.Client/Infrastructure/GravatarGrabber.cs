﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Landdb.Client.Infrastructure {
    public static class GravatarGrabber {
        public static string GetGravatarUri(string emailAddress, int size) {
            if (string.IsNullOrWhiteSpace(emailAddress)) {
                return string.Empty;
            }

            // Reference: http://en.gravatar.com/site/implement/url
            StringBuilder sb = new StringBuilder();

            sb.Append("http://www.gravatar.com/avatar/");
            sb.Append(Md5EncodeText(emailAddress.Trim().ToLowerInvariant()));
            sb.Append(".jpg");

            // Size
            sb.Append("?s=");
            sb.Append(size);

            return sb.ToString();
        }

        /// <summary>
        /// Retrieves the MD5 encoded hash string of the provided input.
        /// </summary>
        /// <param name="text">The input text.</param>
        /// <returns>Returns the MD5 hash string of the input string.</returns>
        private static string Md5EncodeText(string text) {
            StringBuilder sb = new StringBuilder();

            byte[] ss = MD5.Create()
                        .ComputeHash(Encoding.UTF8.GetBytes(text));

            foreach (byte b in ss) {
                sb.Append(b.ToString("X2"));
            }
            return sb.ToString().ToLower();
        }
    }
}
