﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace Landdb.Client.Infrastructure {
    public class HttpWebException : Exception {
        public HttpWebException() {
        }

        public HttpWebException(string message)
            : base(message) {
        }

        public HttpWebException(string message, Exception innerException)
            : base(message, innerException) {
        }

        public HttpWebException(string message, HttpStatusCode statusCode)
            : this(message, statusCode, null) {
        }

        public HttpWebException(string message, HttpStatusCode statusCode, Exception innerException)
            : base(message, innerException) {
            this.StatusCode = statusCode;
        }

        public HttpStatusCode StatusCode { get; private set; }
    }
}
