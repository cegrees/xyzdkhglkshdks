﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lokad.Cqrs.AtomicStorage;
using Lokad.Cqrs;
using Lokad.Cqrs.Partition;
using Lokad.Cqrs.Envelope;
using Lokad.Cqrs.Build;
using System.Threading;
using Landdb.Wires;
using Landdb.Client.Services;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using Landdb.Client.Account;
using Landdb.Domain.ReadModels;
using Landdb.Infrastructure;
using Landdb.Client.Services.Credentials;
using NLog;
using System.Net.Http;
using AgC.UnitConversion;
using Landdb.Client.Models.Connect;

namespace Landdb.Client.Infrastructure {
    public class ClientEndpoint : Landdb.Client.Infrastructure.IClientEndpoint, IDisposable {
        readonly NuclearStorage _store;
        readonly NuclearStorage _persistentStorage;
        readonly Container components;
        readonly CqrsEngineHost engine;
        CancellationTokenSource cts;
        Task engineTask = null;
        bool isDisposed = false;
        bool isEngineRunning = false;
        IAccountStatus accountStatus;
        string dataDirectory;
        string settingsDirectory;
        Guid deviceId;
        IApplicationUpdater applicationUpdater;
        Func<IStorageCredentials> resolveStorageCredentials;
        readonly Logger log = LogManager.GetCurrentClassLogger();
        readonly IConnectClient connectClient;
        readonly IConnectEngine connectEngine;
        private readonly ConnectStore<ConnectApplication> applicationStore;
        private readonly ConnectApplicationService connectApplicationService; 

        private bool datasourceCultureIsenUS = true;

        public ClientEndpoint(Container components, IAccountStatus accountStatus, string dataDirectory, string settingsDirectory, Guid deviceId, IApplicationUpdater applicationUpdater, Func<IStorageCredentials> resolveStorageCredentials, bool datasourceCultureIsenUS) {
            this.components = components;
            this.accountStatus = accountStatus;
            this.dataDirectory = dataDirectory;
            this.settingsDirectory = settingsDirectory;
            this.deviceId = deviceId;
            this.applicationUpdater = applicationUpdater;
            this.resolveStorageCredentials = resolveStorageCredentials;
            this.datasourceCultureIsenUS = datasourceCultureIsenUS;

            _store = new NuclearStorage(components.ViewDocs);
            _persistentStorage = new NuclearStorage(components.PersistentDocs);
            cts = new CancellationTokenSource();

            components.ExecuteStartupTasks(cts.Token);

            engine = this.components.BuildEngine(cts.Token);
            engineTask = engine.Start(cts.Token);
            isEngineRunning = true;

            connectClient = new ConnectClient(this, RemoteConfigurationSettings.ConnectRemoteApiUri, RemoteConfigurationSettings.ConnectRemoteWebAppUri);
            connectEngine = new ConnectEngine(connectClient);

            applicationStore = new ConnectStore<ConnectApplication>();
            connectApplicationService = new ConnectApplicationService(applicationStore, this, RemoteConfigurationSettings.ConnectRemoteApiUri);
        }

        public void Stop(Action OnStopped) {
            cts.Cancel();

            try {
                engineTask.Wait(5000, cts.Token);
            } catch (OperationCanceledException) {
            } catch (Exception ex) {
                log.WarnException("Caught an exception while trying to stop the ES engine.", ex);
            }

            if (engineTask.IsCanceled || engineTask.IsCompleted) {
                components.EventStore.Close();
                isEngineRunning = false;
                OnStopped();
            }
        }

        public void Restart() {
            if (!isEngineRunning) {
                if (cts != null) {
                    cts.Dispose();
                }
                cts = new CancellationTokenSource();

                components.ExecuteStartupTasks(cts.Token);
                engine.Start(cts.Token);
                isEngineRunning = true;
            }
        }

        public void SendOne(IDomainCommand command, string optionalId = null) {
            components.SendToCommandRouter.Send(command);
        }

        public TSingleton GetSingleton<TSingleton>() where TSingleton : new() {
            return _store.GetSingletonOrNew<TSingleton>();
        }

        public Maybe<TEntity> GetView<TEntity>(object key) {
            var optional = _store.GetEntity<TEntity>(key);
            return optional.HasValue ? optional.Value : Maybe<TEntity>.Empty;
        }

        public TEntity GetViewOrThrow<TEntity>(object key) {
            var v = GetView<TEntity>(key);
            if (!v.HasValue) {
                throw new InvalidOperationException(string.Format("Failed to locate view {0} by key {1}", typeof(TEntity), key));
            }
            return v.Value;
        }

        public void SavePersistentEntity<TEntity>(object key, TEntity e) {
            _persistentStorage.AddOrUpdateEntity<TEntity>(key, e);
        }

        public Maybe<TEntity> GetPersistentView<TEntity>(object key) {
            var optional = _persistentStorage.GetEntity<TEntity>(key);
            return optional.HasValue ? optional.Value : Maybe<TEntity>.Empty;
        }

        public IMasterlistService GetMasterlistService() {
            return components.MasterlistService;
        }

        public IProductUnitResolver GetProductUnitResolver() {
            return components.ProductUnitResolver;
        }

        UserSettings settings = null;

        public string SettingsDirectory {
            get { return settingsDirectory; }
        }

        public UserSettings GetUserSettings() {
            if (settings == null) {
                var settingsId = new UserSettingsId(accountStatus.UserId);
                var userSettingsFile = Path.Combine(settingsDirectory, string.Format("UserSettings_{0}.json", settingsId.ToString()));

                if (File.Exists(userSettingsFile)) {
                    using (FileStream stream = new FileStream(userSettingsFile, FileMode.Open)) {
                        using (StreamReader reader = new StreamReader(stream)) {
                            var json = reader.ReadToEnd();
                            settings = JsonConvert.DeserializeObject<UserSettings>(json);
                        }
                    }
                }
            }

            if (settings == null) { settings = new UserSettings(); }

            var areaUnit = datasourceCultureIsenUS ? AgC.UnitConversion.Area.Acre.Self.Name : AgC.UnitConversion.Area.Hectare.Self.Name;
            var weightUnit = datasourceCultureIsenUS ? AgC.UnitConversion.MassAndWeight.Pound.Self.Name : AgC.UnitConversion.MassAndWeight.Kilogram.Self.Name;

            settings.UserAreaUnit = UnitFactory.GetUnitByName(areaUnit) as AgC.UnitConversion.Area.AreaUnit;
            settings.UserMassUnit = UnitFactory.GetUnitByName(weightUnit) as AgC.UnitConversion.MassAndWeight.MassAndWeightUnit;
            return settings;
        }

        public void SaveUserSettings() {
            if (settings == null) {
                GetUserSettings();
            }

            var settingsId = new UserSettingsId(accountStatus.UserId);
            var settingsJson = JsonConvert.SerializeObject(settings);

            DirectoryInfo di = new DirectoryInfo(settingsDirectory);
            if (!di.Exists) {
                di.Create();
            }

            var userSettingsFile = Path.Combine(settingsDirectory, string.Format("UserSettings_{0}.json", settingsId.ToString()));

            using (FileStream stream = new FileStream(userSettingsFile, FileMode.Create)) {
                using (StreamWriter writer = new StreamWriter(stream)) {
                    writer.Write(settingsJson);
                }
            }
        }

        MapSettings mapsettings = null;

        public MapSettings GetMapSettings() {
            if (mapsettings == null) {
                var settingsId = new UserSettingsId(accountStatus.UserId);
                var userSettingsFile = Path.Combine(settingsDirectory, string.Format("MapSettings_{0}.json", settingsId.ToString()));

                mapsettings = new MapSettings();
                mapsettings.FieldsSelectedBorderColor = "#FFFF00";
                mapsettings.FieldsSelectedFillColor = "#FFFF00";
                mapsettings.FieldsUnSelectedBorderColor = "#808080";
                mapsettings.FieldsUnSelectedFillColor = "#808080";
                if (File.Exists(userSettingsFile))
                {
                    using (FileStream stream = new FileStream(userSettingsFile, FileMode.Open))
                    {
                        using (StreamReader reader = new StreamReader(stream))
                        {
                            var json = reader.ReadToEnd();
                            mapsettings = JsonConvert.DeserializeObject<MapSettings>(json);
                        }
                    }
                }                
            }

            return mapsettings ?? new MapSettings();
        }

        public void SaveMapSettings(MapSettings mapsettings) {
            if (mapsettings == null) {
                GetMapSettings();
            }

            var settingsId = new UserSettingsId(accountStatus.UserId);
            var settingsJson = JsonConvert.SerializeObject(mapsettings);

            DirectoryInfo di = new DirectoryInfo(settingsDirectory);
            if (!di.Exists) {
                di.Create();
            }

            var userSettingsFile = Path.Combine(settingsDirectory, string.Format("MapSettings_{0}.json", settingsId.ToString()));

            using (FileStream stream = new FileStream(userSettingsFile, FileMode.Create)) {
                using (StreamWriter writer = new StreamWriter(stream)) {
                    writer.Write(settingsJson);
                }
            }
        }

        public Guid DeviceId {
            get { return deviceId; }
        }

        public Guid UserId {
            get {
                if (accountStatus != null) {
                    return accountStatus.UserId;
                } else { return Guid.Empty; }
            }
        }

        public Func<IStorageCredentials> ResolveStorageCredentials {
            get { return resolveStorageCredentials; }
        }

        public IConnectClient GetConnectClient() {
            return connectClient;
        }

        public IConnectEngine GetConnectEngine() {
            return connectEngine;
        }

        public ConnectStore<ConnectApplication> GetApplicationStore()
        {
            return applicationStore;
        }

        public ConnectApplicationService GetConnectApplicationService()
        {
            return connectApplicationService;
        }

        public void Dispose() {
            if (!isDisposed) {
                if (engine != null) {
                    engine.Dispose();
                }
                if (cts != null) {
                    cts.Dispose();
                }
                isDisposed = true;
            }
        }

        public MessageMetadata GenerateNewMetadata() {
            return new MessageMetadata(Guid.NewGuid(), DateTime.UtcNow, DeviceId, UserId);
        }
    }
}