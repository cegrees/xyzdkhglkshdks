﻿using Lokad.Cqrs;
using Lokad.Cqrs.Dispatch.Events;
using Lokad.Cqrs.Partition;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Landdb.Client.Infrastructure {
    public sealed class AwaitableDispatcherProcess : IEngineProcess {
        readonly Func<byte[], Task> _dispatcher;
        readonly IQueueReader _inbox;

        public AwaitableDispatcherProcess(Func<byte[], Task> dispatcher, IQueueReader inbox) {
            _dispatcher = dispatcher;
            _inbox = inbox;
        }

        public void Dispose() {
            _disposal.Dispose();
        }

        public void Initialize(CancellationToken token) {
            _inbox.InitIfNeeded();
        }

        readonly CancellationTokenSource _disposal = new CancellationTokenSource();

        public Task Start(CancellationToken token) {
            return Task.Factory.StartNew(async () => {
                try {
                    await ReceiveMessages(token);
                } catch (ObjectDisposedException) {
                    // suppress
                }
            }, token);
        }

        async Task ReceiveMessages(CancellationToken outer) {
            using (var source = CancellationTokenSource.CreateLinkedTokenSource(_disposal.Token, outer)) {
                while (true) {
                    MessageTransportContext context;
                    try {
                        if (!_inbox.TakeMessage(source.Token, out context)) {
                            // we didn't retrieve queue within the token lifetime.
                            // it's time to shutdown the server
                            break;
                        }
                    } catch (Exception) {
                        continue;
                    }

                    try {
                        await ProcessMessage(context);
                    } catch (ThreadAbortException) {
                        // Nothing. we are being shutdown
                    } catch (Exception ex) {
                        var e = new DispatchRecoveryFailed(ex, context, context.QueueName);
                        SystemObserver.Notify(e);
                    }
                }
            }
        }

        async Task ProcessMessage(MessageTransportContext context) {
            var dispatched = false;
            try {
                await _dispatcher(context.Unpacked);

                dispatched = true;
            } catch (ThreadAbortException) {
                // we are shutting down. Stop immediately
                return;
            } catch (Exception dispatchEx) {
                // if the code below fails, it will just cause everything to be reprocessed later,
                // which is OK (duplication manager will handle this)

                SystemObserver.Notify(new MessageDispatchFailed(context, context.QueueName, dispatchEx));
                // quarantine is atomic with the processing
                _inbox.TryNotifyNack(context);
            }
            if (!dispatched)
                return;
            try {
                _inbox.AckMessage(context);
                // 3rd - notify.
                SystemObserver.Notify(new MessageAcked(context));

            } catch (ThreadAbortException) {
                // nothing. We are going to sleep
            } catch (Exception ex) {
                // not a big deal. Message will be processed again.
                SystemObserver.Notify(new MessageAckFailed(ex, context));
            }
        }
    }
}
