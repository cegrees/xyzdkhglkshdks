﻿using System;
using Landdb.Client.Models.Connect;
using Landdb.Client.Services;
using Lokad.Cqrs;
using Landdb.Domain.ReadModels;
using Landdb.Client.Services.Credentials;
namespace Landdb.Client.Infrastructure {
    public interface IClientEndpoint {
        TSingleton GetSingleton<TSingleton>() where TSingleton : new();
        Maybe<TEntity> GetView<TEntity>(object key);
        TEntity GetViewOrThrow<TEntity>(object key);
        void SendOne(Landdb.IDomainCommand command, string optionalId = null);
        void SavePersistentEntity<TEntity>(object key, TEntity e);
        Maybe<TEntity> GetPersistentView<TEntity>(object key);
        IMasterlistService GetMasterlistService();
        IProductUnitResolver GetProductUnitResolver();
        Func<IStorageCredentials> ResolveStorageCredentials { get; }
        IConnectClient GetConnectClient();
        IConnectEngine GetConnectEngine();
        ConnectStore<ConnectApplication> GetApplicationStore();
        ConnectApplicationService GetConnectApplicationService();

        void Stop(Action OnStopped);
        void Restart();

        string SettingsDirectory { get; }
        UserSettings GetUserSettings();
        void SaveUserSettings();
        MapSettings GetMapSettings();
        void SaveMapSettings(MapSettings mapsettings);
        Guid DeviceId { get; }
        Guid UserId { get; }
        MessageMetadata GenerateNewMetadata();
    }
}
