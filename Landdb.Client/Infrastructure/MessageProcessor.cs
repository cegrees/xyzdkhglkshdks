﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Landdb.Domain;
using Landdb.Web.ServiceContracts.ServiceBus;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using Landdb.Web.ServiceContracts;
using Lokad.Cqrs;
using System.Reflection;
using Landdb.Converters;
using Landdb.Web.ServiceContracts.Data;
using NLog;

namespace Landdb.Client.Infrastructure {
    public class MessageProcessor : IMessageProcessor {
        LocalPendingCommands pendingCommands;
        IEventStore eventStore;
        Guid deviceId;
        Assembly contractAssembly = typeof(FarmCreated).Assembly; // have a more base event for getting this?
        JsonSerializerSettings deserializeSettings;
        Logger log = LogManager.GetCurrentClassLogger();

        public MessageProcessor(LocalPendingCommands pendingCommands, IEventStore eventStore, Guid deviceId) {
            this.pendingCommands = pendingCommands;
            this.eventStore = eventStore;
            this.deviceId = deviceId;

            deserializeSettings = new JsonSerializerSettings();
            deserializeSettings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;
            deserializeSettings.Converters.Add(new DocumentDescriptorConverter());
        }

        public void ProcessEvent(RecordedEventStoreData e) {
            var metadataString = Encoding.UTF8.GetString(e.Metadata);
            if (string.IsNullOrWhiteSpace(metadataString)) { return; } // no metadata means we can't parse this
            List<MessageAttribute> messageAttributes = JsonConvert.DeserializeObject<List<MessageAttribute>>(metadataString);
            if (messageAttributes != null && messageAttributes.Where(x => x.Key == "NETType").Any()) {
                Type eType = contractAssembly.GetType(messageAttributes.Where(x => x.Key == "NETType").FirstOrDefault().Value);

                if (eType != null) {
                    var dataString = Encoding.UTF8.GetString(e.Data);
                    var o = JsonConvert.DeserializeObject(dataString, eType, deserializeSettings);
                    var de = o as IDomainEvent;
                    if (de != null) {
                        var identity = ((dynamic)de).Id;

                        var localEventStream = eventStore.LoadEventStream(identity);
                        long localAggregateVersion = localEventStream.StreamVersion;

                        bool shouldPublish = true;

                        // Remove from pending commands
                        if (de.Metadata.Source == deviceId && de.Metadata.SourceMessageId.HasValue) {
                            shouldPublish = !pendingCommands.RemoveCommandById(((dynamic)de).Id, de.Metadata.SourceMessageId.Value);
                        }

                        // Make sure not to persist any events we already have.
                        // TODO: Verify that this doesn't cause off-by-one errors
                        if (e.EventNumber < localAggregateVersion) { return; }

                        eventStore.AppendEventsToStream(identity, localAggregateVersion, new[] { de }, de.Metadata.Source.ToString(), identity.DataSourceId.ToString(), shouldPublish);
                    }
                }
            }
        }

        public void ProcessSlice(EventStoreRecordSlice slice) {
            foreach (var e in slice.Events) {
                ProcessEvent(e);
            }
        }

        private void ProcessCommandFailedMessage(DomainCommandFailedEvent cmdFailedEvent) {
            try {
                Guid dataSourceId = cmdFailedEvent.DataSourceId;
                Guid aggregateId = cmdFailedEvent.AggregateId;
                string aggregateIdentityTypeString = cmdFailedEvent.AggregateIdentityType;
                Guid originatingDeviceId = cmdFailedEvent.OriginatingSource;
                Guid commandId = cmdFailedEvent.CommandId;

                var identity = typeof(DataSourceId).Assembly.GetType(aggregateIdentityTypeString).GetConstructor(new Type[] { typeof(Guid), typeof(Guid) }).Invoke(new object[] { dataSourceId, aggregateId }) as IIdentity;

                if (deviceId == originatingDeviceId) {
                    // TODO: Inform the user that the command has failed somehow.
                    pendingCommands.RemoveCommandById(identity, commandId);

                    // TODO: Force a projection rebuild.
                }
            } catch (Exception ex) {
                log.ErrorException("Couldn't process a command failed message from the server.", ex);
            }
        }

    }
}
