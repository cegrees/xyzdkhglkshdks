﻿using System;
namespace Landdb.Client.Infrastructure {
    public interface IRemoteSubmissionErrorNotifier {
        void AddNewError(Landdb.IDomainCommand<Landdb.AbstractDataSourceIdentity<Guid, Guid>> cmd);
        void MarkErrorsAsSeen();
    }
}
