﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Landdb.Client.Infrastructure {
    public class UserSettingsId : AbstractIdentity<Guid> {
        public const string TagValue = "usersettings";

        public UserSettingsId() {}

        public UserSettingsId(Guid id) {
            Id = id;
        }

        [DataMember(Order = 1)]
        public override Guid Id { get; protected set; }

        public override string GetTag() {
            return TagValue;
        }

        public override string ToString() {
            return string.Format("usersettings-" + Id.ToString().ToLowerInvariant().Substring(0, 6));
        }
    }
}
