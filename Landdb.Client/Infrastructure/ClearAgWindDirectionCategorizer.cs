﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Landdb.Client.Infrastructure.DisplayItems;

namespace Landdb.Client.Infrastructure {
    public class ClearAgWindDirectionCategorizer {
        private static Dictionary<string, ClearAgWindType> WindDirectionMappingDictionary =>
            new Dictionary<string, ClearAgWindType> {
                //{"Calm", ClearAgWindType.Calm},
                //{"Becoming Calm", ClearAgWindType.Calm},
                //{"Calm Conditions Continuing", ClearAgWindType.Calm},
                //{"Light Winds", ClearAgWindType.LightWinds},
                //{"Light Winds Continuing", ClearAgWindType.LightWinds},
                //{"Light Breeze", ClearAgWindType.LightBreeze},
                //{"Light Breeze Continuing", ClearAgWindType.LightBreeze},
                //{"Windy", ClearAgWindType.Windy},
                //{"Becoming Windy", ClearAgWindType.Windy},
                //{"Turning Windy", ClearAgWindType.Windy},
                //{"Windy Conditions Continuing", ClearAgWindType.Windy},
                //{"Windy Conditions Developing", ClearAgWindType.Windy},
                //{"Windy Conditions Diminishing", ClearAgWindType.Windy},
                //{"Breezy", ClearAgWindType.Breezy},
                //{"Becoming Breezy", ClearAgWindType.Breezy},
                //{"Turning Breezy", ClearAgWindType.Breezy},
                //{"Breezy Conditions Continuing", ClearAgWindType.Breezy},
                //{"Breezy Conditions Developing", ClearAgWindType.Breezy},
                //{"Breezy Conditions Diminishing", ClearAgWindType.Breezy},
                //{"Gusty Winds", ClearAgWindType.GustyWinds},
                //{"Gusty Winds Developing", ClearAgWindType.GustyWinds},
                //{"Gusty Winds Diminishing", ClearAgWindType.GustyWinds},
                //{"Gusty Winds Continuing", ClearAgWindType.GustyWinds},
                {"North", ClearAgWindType.North},
                {"Becoming North", ClearAgWindType.North},
                {"Turning North", ClearAgWindType.North},
                {"North-Northeast", ClearAgWindType.NorthNorthEast},
                {"NNE", ClearAgWindType.NorthNorthEast},
                {"Becoming North-Northeast", ClearAgWindType.NorthNorthEast},
                {"Turning North-Northeast", ClearAgWindType.NorthNorthEast},
                {"Northeast", ClearAgWindType.NorthEast},
                {"NE", ClearAgWindType.NorthEast},
                {"Becoming Northeast", ClearAgWindType.NorthEast},
                {"Turning Northeast", ClearAgWindType.NorthEast},
                {"East-Northeast", ClearAgWindType.EastNorthEast},
                {"ENE", ClearAgWindType.EastNorthEast},
                {"Becoming East-Northeast", ClearAgWindType.EastNorthEast},
                {"Turning East-Northeast", ClearAgWindType.EastNorthEast},
                {"East", ClearAgWindType.East},
                {"E", ClearAgWindType.East},
                {"Becoming East", ClearAgWindType.East},
                {"Turning East", ClearAgWindType.East},
                {"East-Southeast", ClearAgWindType.EastSouthEast},
                {"ESE", ClearAgWindType.EastSouthEast},
                {"Becoming East-Southeast", ClearAgWindType.EastSouthEast},
                {"Turning East-Southeast", ClearAgWindType.EastSouthEast},
                {"Southeast", ClearAgWindType.SouthEast},
                {"SE", ClearAgWindType.SouthEast},
                {"Becoming Southeast", ClearAgWindType.SouthEast},
                {"Turning Southeast", ClearAgWindType.SouthEast},
                {"South-Southeast", ClearAgWindType.SouthSouthEast},
                {"SSE", ClearAgWindType.SouthSouthEast},
                {"Becoming South-Southeast", ClearAgWindType.SouthSouthEast},
                {"Turning South-Southeast", ClearAgWindType.SouthSouthEast},
                {"South", ClearAgWindType.South},
                {"S", ClearAgWindType.South},
                {"Becoming South", ClearAgWindType.South},
                {"Turning South", ClearAgWindType.South},
                {"South-Southwest", ClearAgWindType.SouthSouthWest},
                {"SSW", ClearAgWindType.SouthSouthWest},
                {"Becoming South-Southwest", ClearAgWindType.SouthSouthWest},
                {"Turning South-Southwest", ClearAgWindType.SouthSouthWest},
                {"Southwest", ClearAgWindType.SouthWest},
                {"SW", ClearAgWindType.SouthWest},
                {"Becoming Southwest", ClearAgWindType.SouthWest},
                {"Turning Southwest", ClearAgWindType.SouthWest},
                {"West-Southwest", ClearAgWindType.WestSouthWest},
                {"WSW", ClearAgWindType.WestSouthWest},
                {"Becoming West-Southwest", ClearAgWindType.WestSouthWest},
                {"Turning West-Southwest", ClearAgWindType.WestSouthWest},
                {"West", ClearAgWindType.West},
                {"W", ClearAgWindType.West},
                {"Becoming West", ClearAgWindType.West},
                {"Turning West", ClearAgWindType.West},
                {"Northwest", ClearAgWindType.NorthWest},
                {"NW", ClearAgWindType.NorthWest},
                {"Becoming Northwest", ClearAgWindType.NorthWest},
                {"Turning Northwest", ClearAgWindType.NorthWest},
                {"West-Northwest", ClearAgWindType.WestNorthWest},
                {"WNW", ClearAgWindType.WestNorthWest},
                {"Becoming West-Northwest", ClearAgWindType.WestNorthWest},
                {"Turning West-Northwest", ClearAgWindType.WestNorthWest},
                {"North-Northwest", ClearAgWindType.NorthNorthWest},
                {"NNW", ClearAgWindType.NorthNorthWest},
                {"Becoming North-Northwest", ClearAgWindType.NorthNorthWest},
                {"Turning North-Northwest", ClearAgWindType.NorthNorthWest},
                {"Variable", ClearAgWindType.VariableWinds},
                {"Becoming Variable", ClearAgWindType.VariableWinds},
                {"Turning Variable", ClearAgWindType.VariableWinds},
                {"Variable Winds", ClearAgWindType.VariableWinds}
            };

        public static ClearAgWindType Categorize(string valueFromApi) {
            if (!String.IsNullOrEmpty(valueFromApi))
                foreach (var windDirection in WindDirectionMappingDictionary) {
                    if (windDirection.Key.ToLower().Equals(valueFromApi.ToLower())) {
                        return windDirection.Value;
                    }
                }
            
            return ClearAgWindType.Unknown;
        }
    }
}