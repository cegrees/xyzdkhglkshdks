﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lokad.Cqrs.TapeStorage;
using Lokad.Cqrs;
using System.Runtime.Serialization;
using Mono.Cecil;
using System.Diagnostics;
using Landdb.Wires;
using Lokad.Cqrs.AtomicStorage;
using System.Threading;
using Landdb.Client.Wires.MemoryStorage;
using NLog;

namespace Landdb.Client.Infrastructure {
    public static class StartupProjectionRebuilder {
        

        public static void Rebuild(CancellationToken token, FastMemoryDocumentStore memoryStore, IClientEventStore stream, Func<IDocumentStore, IEnumerable<object>> projectors) {
            Logger log = LogManager.GetCurrentClassLogger();
            var strategy = memoryStore.Strategy;
            memoryStore.ResetAll();

            var tracked = new ProjectionInspectingStore(memoryStore);

            var projections = new List<object>();
            projections.AddRange(projectors(tracked));

    //*******************************************
    //TODO: Need to figure out why this was here originally. Is it a bad idea to have multiple writers in a single projection? Should we switch back to one-writer/one-projection?
    //*******************************************
            //if (tracked.Projections.Count != projections.Count())
            //    throw new InvalidOperationException("Count mismatch");
            tracked.ValidateSanity();

            var wireWatch = Stopwatch.StartNew();

            var wire = new RedirectToDynamicEvent();
            projections.ForEach(x => wire.WireToWhen(x));
            var wireTimeTotal = wireWatch.ElapsedMilliseconds;

            var eventEnumerationWatch = Stopwatch.StartNew();
            var events = stream.EnumerateAllEvents().Events;
            var enumTimeTotal = eventEnumerationWatch.ElapsedMilliseconds;

            var handlersWatch = Stopwatch.StartNew();

            ObserveWhileCan(events, wire, token, log);

            if (token.IsCancellationRequested) {
                SystemObserver.Notify("[warn] Aborting projections before anything was changed");
                return;
            }

            
            var handlerTicks = handlersWatch.ElapsedTicks;
            var timeInHandlers = Math.Round(TimeSpan.FromTicks(handlerTicks).TotalMilliseconds, 1);
            log.Debug("Time Elapsed: Wires - {0}ms | Event Enum - {1}ms | Handlers - {2}", wireTimeTotal, enumTimeTotal, timeInHandlers);
        }



        [DataContract]
        public sealed class ProjectionHash {
            [DataMember(Order = 1)]
            public Dictionary<string, string> BucketHashes { get; set; }

            public ProjectionHash() {
                BucketHashes = new Dictionary<string, string>();
            }
        }


        sealed class ProjectionInspectingStore : IDocumentStore {
            readonly IDocumentStore _real;

            public ProjectionInspectingStore(IDocumentStore real) {
                _real = real;
            }

            public readonly List<Projection> Projections = new List<Projection>();


            public sealed class Projection {
                public Type EntityType;
                public string StoreBucket;
            }

            public void ValidateSanity() {
                if (Projections.Count == 0)
                    throw new InvalidOperationException("There were no projections registered");

                // TODO: Is this bad?

                //var viewsWithMultipleProjections = Projections.GroupBy(e => e.EntityType).Where(g => g.Count() > 1).ToList();
                //if (viewsWithMultipleProjections.Count > 0) {
                //    var builder = new StringBuilder();
                //    builder.AppendLine("Please, define only one projection per view. These views were referenced more than once:");
                //    foreach (var projection in viewsWithMultipleProjections) {
                //        builder.AppendLine("  " + projection.Key);
                //    }
                //    builder.AppendLine("NB: you can use partials or dynamics in edge cases");
                //    throw new InvalidOperationException(builder.ToString());
                //}

                //var viewsWithSimilarBuckets = Projections
                //    .GroupBy(e => e.StoreBucket.ToLowerInvariant())
                //    .Where(g => g.Count() > 1)
                //    .ToArray();

                //if (viewsWithSimilarBuckets.Length > 0) {
                //    var builder = new StringBuilder();
                //    builder.AppendLine("Following views will be stored in same location, which will cause problems:");
                //    foreach (var i in viewsWithSimilarBuckets) {
                //        var @join = string.Join(",", i.Select(x => x.EntityType));
                //        builder.AppendFormat(" {0} : {1}", i.Key, @join).AppendLine();
                //    }
                //    throw new InvalidOperationException(builder.ToString());
                //}

            }

            public IDocumentWriter<TKey, TEntity> GetWriter<TKey, TEntity>() {
                Projections.Add(new Projection() {
                    EntityType = typeof(TEntity),
                    StoreBucket = _real.Strategy.GetEntityBucket<TEntity>()
                });

                return _real.GetWriter<TKey, TEntity>();
            }

            public IDocumentReader<TKey, TEntity> GetReader<TKey, TEntity>() {
                return _real.GetReader<TKey, TEntity>();
            }

            public IDocumentStrategy Strategy {
                get { return _real.Strategy; }
            }

            public IEnumerable<DocumentRecord> EnumerateContents(string bucket) {
                return _real.EnumerateContents(bucket);
            }

            public void WriteContents(string bucket, IEnumerable<DocumentRecord> records) {
                _real.WriteContents(bucket, records);
            }

            public void Reset(string bucket) {
                _real.Reset(bucket);
            }
        }



        static string GetClassHash(Type type1) {
            var location = type1.Assembly.Location;
            var mod = ModuleDefinition.ReadModule(location);
            var builder = new StringBuilder();
            var type = type1;


            var typeDefinition = mod.GetType(type.FullName);
            builder.AppendLine(typeDefinition.Name);
            ProcessMembers(builder, typeDefinition);

            // we include nested types
            foreach (var nested in typeDefinition.NestedTypes) {
                ProcessMembers(builder, nested);
            }

            return builder.ToString();
        }

        static void ProcessMembers(StringBuilder builder, TypeDefinition typeDefinition) {
            foreach (var md in typeDefinition.Methods.OrderBy(m => m.ToString())) {
                builder.AppendLine("  " + md);

                foreach (var instruction in md.Body.Instructions) {
                    // we don't care about offsets
                    instruction.Offset = 0;
                    builder.AppendLine("    " + instruction);
                }
            }
            foreach (var field in typeDefinition.Fields.OrderBy(f => f.ToString())) {
                builder.AppendLine("  " + field);
            }
        }


        static void ObserveWhileCan(IEnumerable<IDomainEvent> records, RedirectToDynamicEvent wire, CancellationToken token, Logger log) {
            var watch = Stopwatch.StartNew();
            int count = 0;
            foreach (var record in records) {
                count += 1;

                if (token.IsCancellationRequested)
                    return;
                if (count % 50000 == 0) {
                    log.Debug("Observing {0} {1}", count,
                        Math.Round(watch.Elapsed.TotalSeconds, 2));

                    SystemObserver.Notify("Observing {0} {1}", count,
                        Math.Round(watch.Elapsed.TotalSeconds, 2));
                    watch.Restart();
                }
                try {
                    wire.InvokeEvent(record);
                } catch (Exception ex) {
                    log.ErrorException("Encountered an error while replaying events into read models", ex);
                }
            }
        }
    }

}