﻿using System.Windows.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using Landdb.Client.Resources;
using Mono.CompilerServices.SymbolWriter;

namespace Landdb.Client.Infrastructure.DisplayItems {
    public enum ClearAgWindType {
        //Calm,
        //LightWinds,
        //LightBreeze,
        //Windy,
        //Breezy,
        //GustyWinds,
        VariableWinds,
        North,
        NorthEast,
        NorthNorthEast,
        NorthWest,
        NorthNorthWest,
        South,
        SouthEast,
        SouthSouthEast,
        SouthWest,
        SouthSouthWest,
        East,
        EastNorthEast,
        EastSouthEast,
        West,
        WestSouthWest,
        WestNorthWest,
        Unknown,
    }

    public class ClearAgWindDirectionDisplayItem {
        static ClearAgWindDirectionDisplayItem() {
            CreateClearAgWindDirectionDictionary();
        }

        public ClearAgWindDirectionDisplayItem(string clearAgWindDirectionName) {
            ClearAgWindDirection = GetClearAgWindTypeFor(clearAgWindDirectionName);
        }

        public ClearAgWindDirectionDisplayItem(ClearAgWindType type) {
            ClearAgWindDirection = type;
        }

        public ClearAgWindType ClearAgWindDirection { get; }
        public string DisplayText => GetClearAgWindDirectionDisplayTextFor(ClearAgWindDirection);
        public string KeyText => GetClearAgWindDirectionKeyTextFor(ClearAgWindDirection);

        public static Dictionary<ClearAgWindType, (string untranslatedKey, string translatedValue)> ClearAgWindDirectionDictionary { get; private set; }

        public static ClearAgWindType GetClearAgWindTypeFor(string clearAgWindDirectionName) {
            try {
                return ClearAgWindDirectionDictionary.First(x => x.Value.untranslatedKey == clearAgWindDirectionName).Key;
            } catch (Exception e) {
                Console.WriteLine(e);
                throw e;
            }
        }

        public static string GetClearAgWindDirectionDisplayTextFor(ClearAgWindType clearAgWindDirectionType) {
            try {
                return ClearAgWindDirectionDictionary[clearAgWindDirectionType].translatedValue;
            } catch (Exception e) {
                Console.WriteLine(e);
                throw new ArgumentOutOfRangeException(nameof(clearAgWindDirectionType), clearAgWindDirectionType, null);
            }
        }

        public static string GetClearAgWindDirectionDisplayTextFor(string clearAgWindDirectionKeyText) {
            try {
                var thisDisplayItem = new ClearAgWindDirectionDisplayItem(clearAgWindDirectionKeyText);
                return thisDisplayItem.DisplayText;
            } catch (Exception e) {
                Console.WriteLine(e);
                throw e;
            }
        }

        public static string GetClearAgWindDirectionKeyTextFor(ClearAgWindType clearAgWindDirectionType) {
            try {
                return ClearAgWindDirectionDictionary[clearAgWindDirectionType].untranslatedKey;
            } catch (Exception e) {
                Console.WriteLine(e);
                throw new ArgumentOutOfRangeException(nameof(clearAgWindDirectionType), clearAgWindDirectionType, null);
            }
        }

        private static void CreateClearAgWindDirectionDictionary() {
            ClearAgWindDirectionDictionary = new Dictionary<ClearAgWindType, (string, string)> {
                //{ClearAgWindType.Calm, ("Calm", Strings.WindDirection_Calm)},
                //{ClearAgWindType.LightWinds, ("Light Winds", Strings.WindDirection_LightWinds)},
                //{ClearAgWindType.LightBreeze, ("Light Breeze", Strings.WindDirection_LightBreeze)},
                //{ClearAgWindType.Windy, ("Windy", Strings.WindDirection_Windy)},
                //{ClearAgWindType.Breezy, ("Breezy", Strings.WindDirection_Breezy)},
                //{ClearAgWindType.GustyWinds, ("Gusty Winds", Strings.WindDirection_GustyWinds)},
                {ClearAgWindType.VariableWinds, ("Variable Winds", Strings.WindDirection_VariableWinds)},
                {ClearAgWindType.North, ("North", "N")},
                {ClearAgWindType.NorthEast, ("Northeast", "NE")},
                {ClearAgWindType.NorthNorthEast, ("North-Northeast", "NNE")},
                {ClearAgWindType.NorthWest, ("Northwest", "NW")},
                {ClearAgWindType.NorthNorthWest, ("North-Northwest", "NNW")},
                {ClearAgWindType.South, ("South", "S")},
                {ClearAgWindType.SouthEast, ("Southeast", "SE")},
                {ClearAgWindType.SouthSouthEast, ("South-Southeast", "SSE")},
                {ClearAgWindType.SouthWest, ("Southwest", "SW")},
                {ClearAgWindType.SouthSouthWest, ("South-Southwest", "SSW")},
                {ClearAgWindType.East, ("East", "E")},
                {ClearAgWindType.EastNorthEast, ("East-Northeast", "ENE")},
                {ClearAgWindType.EastSouthEast, ("East-Southeast", "ESE")},
                {ClearAgWindType.West, ("West", "W")},
                {ClearAgWindType.WestSouthWest, ("West-Southwest", "WSW")},
                {ClearAgWindType.WestNorthWest, ("West-Northwest", "WNW")},
                {ClearAgWindType.Unknown, ("Unknown", "")},
            };
        }

        public static List<ClearAgWindDirectionDisplayItem> AllAsList() {
            var theListOfDisplayItems = ClearAgWindDirectionDictionary.Keys.Select(x => new ClearAgWindDirectionDisplayItem(x)).ToList();
            //TODO: Use LINQ to sort alphabetically in a way that is dependent upon culture (i.e. implement IComparable):    theListOfDisplayItems.Sort();

            return theListOfDisplayItems;
        }

        public override bool Equals(object toCompare) {
            switch (toCompare) {
                case ClearAgWindDirectionDisplayItem displayCompareItem:
                    return ClearAgWindDirection.Equals(displayCompareItem.ClearAgWindDirection);
                case ClearAgWindType typeCompareItem:
                    return ClearAgWindDirection.Equals(typeCompareItem);
                case string stringCompareItem:
                    return ClearAgWindDirectionDictionary.Any(x => x.Value.untranslatedKey == stringCompareItem);
                default:
                    return false;
            }
        }

        public override string ToString() {
            return DisplayText;
        }
    }
}