﻿using System.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Landdb.Client.Resources;

namespace Landdb.Client.Infrastructure {

    public class TreeDisplayModeDisplayItem {
        public enum TreeDisplayModeType {
            Farms,
            Crops
        }

        static TreeDisplayModeDisplayItem() {
            CreateTreeDisplayModeDictionary();
        }

        public TreeDisplayModeDisplayItem(string treeDisplayModeName) {
            TreeDisplayMode = GetTreeDisplayModeFor(treeDisplayModeName);
        }

        public TreeDisplayModeType TreeDisplayMode { get; }
        public string DisplayText => GetTreeDisplayModeDisplayTextFor(TreeDisplayMode);
        public string KeyText => GetTreeDisplayModeKeyTextFor(TreeDisplayMode);
        
        private static Dictionary<TreeDisplayModeType, (string untranslatedKey, string translatedValue)> TreeDisplayModeDictionary { get; set; }

        public static TreeDisplayModeType GetTreeDisplayModeFor(string treeDisplayModeName) {
            try {
                return TreeDisplayModeDictionary.First(x => x.Value.untranslatedKey == treeDisplayModeName).Key;
            } catch (Exception e) {
                Console.WriteLine(e);
                throw e;
            }
        }

        public bool IsFarms => TreeDisplayModeEquals(TreeDisplayModeType.Farms);

        public bool IsCrops => TreeDisplayModeEquals(TreeDisplayModeType.Crops);

        public static bool IsSameTypeAs(object o) {
            return o is TreeDisplayModeType;
        }

        public static string GetTreeDisplayModeDisplayTextFor(TreeDisplayModeType treeDisplayMode) {
            try {
                return TreeDisplayModeDictionary[treeDisplayMode].translatedValue;
            } catch (Exception e) {
                Console.WriteLine(e);
                throw new ArgumentOutOfRangeException(nameof(TreeDisplayMode), treeDisplayMode, null);
            }
        }

        public static string GetTreeDisplayModeDisplayTextFor(string treeDisplayModeKeyText) {
            try {
                var thisDisplayItem = new TreeDisplayModeDisplayItem(treeDisplayModeKeyText);
                return thisDisplayItem.DisplayText;
            } catch (Exception e) {
                Console.WriteLine(e);
                throw e;
            }
        }

        public static string GetTreeDisplayModeKeyTextFor(TreeDisplayModeType treeDisplayMode) {
            try {
                return TreeDisplayModeDictionary[treeDisplayMode].untranslatedKey;
            } catch (Exception e) {
                Console.WriteLine(e);
                throw new ArgumentOutOfRangeException(nameof(treeDisplayMode), treeDisplayMode, null);
            }
        }

        public static string GetTreeDisplayModeKeyTextFor(string displayText) {
            try {
                var thisFormat = TreeDisplayModeDictionary.First(x => x.Value.translatedValue == displayText);
                return thisFormat.Value.untranslatedKey;
            } catch (Exception e) {
                Console.WriteLine(e);
                throw e;
            }
        }

        private static void CreateTreeDisplayModeDictionary() {
            TreeDisplayModeDictionary = new Dictionary<TreeDisplayModeType, (string, string)> {
                {TreeDisplayModeType.Farms, ("Farms", Strings.TreeDisplayMode_Farms)},
                {TreeDisplayModeType.Crops, ("Crops", Strings.TreeDisplayMode_Crops)}
            };
        }

        private bool TreeDisplayModeEquals(TreeDisplayModeType toCompare) {
            if (TreeDisplayMode.Equals(toCompare))
                return true;
            else
                return false;
        }

        //public override bool Equals(object toCompare) {
        //    var compareItem = toCompare as TreeDisplayModeDisplayItem;
        //    return TreeDisplayModeEquals(compareItem.TreeDisplayMode);
        //}

        public override string ToString() {
            return DisplayText;
        }
    }
}