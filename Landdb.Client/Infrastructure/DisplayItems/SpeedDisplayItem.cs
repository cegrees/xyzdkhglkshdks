﻿using System;
using System.Collections.Generic;
using System.Linq;
using Landdb.Client.Resources;
using System.ComponentModel;

namespace Landdb.Client.Infrastructure.DisplayItems {
    public enum SpeedDisplayType {
        [Description("Mile")] MPH,
        [Description("Kilometer")] KMPH,
        [Description("Unknown")] Unknown
    }


    public class SpeedDisplayItem {
        static SpeedDisplayItem() {
            CreateSpeedDictionary();
        }

        public SpeedDisplayItem(string speedName) {
            SpeedType = GetSpeedTypeFor(speedName);
        }

        public SpeedDisplayItem(SpeedDisplayType type) {
            SpeedType = type;
        }

        public SpeedDisplayType SpeedType { get; }
        public string DisplayText => $"{GetSpeedDisplayTextFor(SpeedType)}";
        public string KeyText => GetSpeedKeyTextFor(SpeedType);

        public static Dictionary<SpeedDisplayType, (string untranslatedKey, string translatedValue)> SpeedDictionary { get; private set; }

        public static SpeedDisplayType GetSpeedTypeFor(string speedName) {
            //TODO: clean up, look through the list of translated values instead of just the untranslated keys to make it localization compatible
            try {
                var displayType = SpeedDisplayType.MPH;
                speedName = speedName.ToLower();
                var simplifiedName = speedName.Contains(Strings.Distance_Mile.ToLower()) || 
                                     speedName.Contains("mph") ? "MPH" : 
                    speedName.Contains("km") || speedName.Contains("kilometer") ? "KM/H" : 
                    null;
                if (!string.IsNullOrEmpty(simplifiedName)) {
                    displayType = SpeedDictionary.FirstOrDefault(x => x.Value.untranslatedKey.Equals(simplifiedName)).Key;
                } else {
                    displayType = SpeedDictionary.FirstOrDefault(x => x.Value.translatedValue.Equals(simplifiedName)).Key;
                }

                return displayType;
            } catch (Exception e) {
                Console.WriteLine(e);
                throw e;
            }
        }

        public static string GetSpeedDisplayTextFor(SpeedDisplayType speedDisplayType) {
            try {
                return SpeedDictionary[speedDisplayType].translatedValue;
            } catch (Exception e) {
                Console.WriteLine(e);
                throw new ArgumentOutOfRangeException(nameof(speedDisplayType), speedDisplayType, null);
            }
        }

        public static string GetSpeedDisplayTextFor(string speedKeyText) {
            try {
                var thisDisplayItem = new SpeedDisplayItem(speedKeyText);
                return thisDisplayItem.DisplayText;
            } catch (Exception e) {
                Console.WriteLine(e);
                throw e;
            }
        }

        public static string GetSpeedKeyTextFor(SpeedDisplayType speedDisplayType) {
            try {
                return SpeedDictionary[speedDisplayType].untranslatedKey;
            } catch (Exception e) {
                Console.WriteLine(e);
                throw new ArgumentOutOfRangeException(nameof(speedDisplayType), speedDisplayType, null);
            }
        }

        private static void CreateSpeedDictionary() {
            SpeedDictionary = new Dictionary<SpeedDisplayType, (string, string)> {
                {SpeedDisplayType.MPH, ("MPH", Strings.Speed_MilesPerHour)},
                {SpeedDisplayType.KMPH, ("KM/H", Strings.Speed_KilometersPerHour)},
            };
        }

        public static List<SpeedDisplayItem> AllAsList() {
            var theListOfDisplayItems = SpeedDictionary.Keys.Select(x => new SpeedDisplayItem(x)).ToList();
            //TODO: Use LINQ to sort alphabetically in a way that is dependent upon culture (i.e. implement IComparable):    theListOfDisplayItems.Sort();

            return theListOfDisplayItems;
        }

        /// <summary>
        /// Compares <see cref="SpeedDisplayItem"/>, <see cref="SpeedDisplayType"/>, and <see cref="string"/> arguments.
        /// </summary>
        /// <param name="toCompare"></param>
        /// <returns></returns>
        public override bool Equals(object toCompare) {
            if (toCompare is SpeedDisplayItem displayCompareItem) {
                return SpeedType.Equals(displayCompareItem.SpeedType);
            } else if (toCompare is SpeedDisplayType typeCompareItem) {
                return SpeedType.Equals(typeCompareItem);
            } else if (toCompare is string stringCompareItem) {
                stringCompareItem = stringCompareItem.ToLower();
                var simplifiedName = stringCompareItem.Contains(Strings.Distance_Mile.ToLower()) || stringCompareItem.Contains("mph") ? "MPH" :
                    stringCompareItem.Contains("km") || stringCompareItem.Contains("kilometer") ? "KM/H" : null;
                return SpeedDictionary.Any(x => x.Value.untranslatedKey == simplifiedName);
            } else {
                return false;
            }
        }

        public override string ToString() {
            return DisplayText;
        }
    }
}
