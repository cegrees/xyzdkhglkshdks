﻿using Landdb.Client.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThinkGeo.MapSuite.Core;

namespace Landdb.Client.Infrastructure.DisplayItems {
    public class AdornmentLocationDisplayItem {
        static AdornmentLocationDisplayItem() {
            CreateAdornmentLocationDictionary();
        }

        public AdornmentLocationDisplayItem(string AdornmentLocationName) {
            AdornmentLocation = GetAdornmentLocationFor(AdornmentLocationName);
        }

        public AdornmentLocationDisplayItem(AdornmentLocation adornmentLocation) {
            AdornmentLocation = adornmentLocation;
        }

        public AdornmentLocation AdornmentLocation { get; }
        public string DisplayText => GetAdornmentLocationDisplayTextFor(AdornmentLocation);
        public string KeyText => GetAdornmentLocationKeyTextFor(AdornmentLocation);

        public static Dictionary<AdornmentLocation, (string untranslatedKey, string translatedValue)> AdornmentLocationDictionary {
            get;
            private set;
        }

        public static AdornmentLocation GetAdornmentLocationFor(string adornmentLocationName) {
            try {
                return AdornmentLocationDictionary.First(x => x.Value.untranslatedKey == adornmentLocationName).Key;
            } catch (Exception e) {
                Console.WriteLine(e);
                throw e;
            }
        }

        public static string GetAdornmentLocationDisplayTextFor(AdornmentLocation adornmentLocation) {
            try {
                return AdornmentLocationDictionary[adornmentLocation].translatedValue;
            } catch (Exception e) {
                Console.WriteLine(e);
                throw new ArgumentOutOfRangeException(nameof(adornmentLocation), adornmentLocation, null);
            }
        }

        public static string GetAdornmentLocationDisplayTextFor(string adornmentLocationKeyText) {
            try {
                var thisDisplayItem = new AdornmentLocationDisplayItem(adornmentLocationKeyText);
                return thisDisplayItem.DisplayText;
            } catch (Exception e) {
                Console.WriteLine(e);
                throw e;
            }
        }

        public static string GetAdornmentLocationKeyTextFor(AdornmentLocation adornmentLocation) {
            try {
                return AdornmentLocationDictionary[adornmentLocation].untranslatedKey;
            } catch (Exception e) {
                Console.WriteLine(e);
                throw new ArgumentOutOfRangeException(nameof(adornmentLocation), adornmentLocation, null);
            }
        }

        private static void CreateAdornmentLocationDictionary() {
            AdornmentLocationDictionary =
                new Dictionary<AdornmentLocation, (string, string)> {
                    {AdornmentLocation.Center, ("Center", Strings.AdornmentLocation_Center)},
                    {AdornmentLocation.CenterLeft, ("Center Left", Strings.AdornmentLocation_CenterLeft)},
                    {AdornmentLocation.CenterRight, ("Center Right", Strings.AdornmentLocation_CenterRight)},
                    {AdornmentLocation.LowerCenter, ("Lower Center", Strings.AdornmentLocation_LowerCenter)},
                    {AdornmentLocation.LowerLeft, ("Lower Left", Strings.AdornmentLocation_LowerLeft)},
                    {AdornmentLocation.LowerRight, ("Lower Right", Strings.AdornmentLocation_LowerRight)},
                    {AdornmentLocation.UpperCenter, ("Upper Center", Strings.AdornmentLocation_UpperCenter)},
                    {AdornmentLocation.UpperLeft, ("Upper Left", Strings.AdornmentLocation_UpperLeft)},
                    {AdornmentLocation.UpperRight, ("Upper Right", Strings.AdornmentLocation_UpperRight)},
                    {AdornmentLocation.UseOffsets, ("Use Offsets", Strings.AdornmentLocation_UseOffsets)},
                };
        }

        public bool AdornmentLocationEquals(AdornmentLocation toCompare) {
            return AdornmentLocation.Equals(toCompare);
        }

        public override bool Equals(object toCompare) {
            return 
                toCompare is AdornmentLocation location? AdornmentLocation == location: 
                toCompare is AdornmentLocationDisplayItem displayItem ? AdornmentLocation == displayItem.AdornmentLocation: 
                false;
        }

        public override string ToString() {
            return DisplayText;
        }
    }
}