﻿using System.Windows.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using Landdb.Client.Resources;
using System.ComponentModel;

namespace Landdb.Client.Infrastructure.DisplayItems
{

    public enum DocumentTypeDisplayType
    {
        [Description("Application")] 
        Application,
        [Description("Invoice")]
        Invoice,
        [Description("Recommendation")] 
        Recommendation,
        [Description("ScoutingApplication")] 
        ScoutingApplication,
        [Description("WorkOrder")] 
        WorkOrder,
        [Description("Plan")]
        Plan,
        [Description("SourceDocument")]
        Unknown
    }


    public class DocumentTypeDisplayItem
    {
        static DocumentTypeDisplayItem()
        {
            CreateDocumentTypeDictionary();
        }

        public DocumentTypeDisplayItem(string documentTypeName)
        {
            DocumentType = GetDocumentTypeFor(documentTypeName);
        }

        public DocumentTypeDisplayItem(DocumentTypeDisplayType type)
        {
            DocumentType = type;
        }

        public DocumentTypeDisplayType DocumentType { get; }
        public string DisplayText => $"{GetDocumentTypeDisplayTextFor(DocumentType)}";
        public string KeyText => GetDocumentTypeKeyTextFor(DocumentType);

        public static Dictionary<DocumentTypeDisplayType, string> DocumentTypeDictionary { get; private set; }

        public static DocumentTypeDisplayType GetDocumentTypeFor(string documentTypeName)
        {
            try
            {
                var displayType = DocumentTypeDisplayType.Unknown;
                if (!string.IsNullOrEmpty(documentTypeName)) {
                    displayType = DocumentTypeDictionary.FirstOrDefault(x => x.Key.ToString() == documentTypeName).Key;
                }
                return displayType;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw e;
            }
        }

        public static string GetDocumentTypeDisplayTextFor(DocumentTypeDisplayType documentTypeDisplayType)
        {
            try
            {
                return DocumentTypeDictionary[documentTypeDisplayType];
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw new ArgumentOutOfRangeException(nameof(documentTypeDisplayType), documentTypeDisplayType, null);
            }
        }

        public static string GetDocumentTypeDisplayTextFor(string documentTypeKeyText)
        {
            try
            {
                var thisDisplayItem = new DocumentTypeDisplayItem(documentTypeKeyText);
                return thisDisplayItem.DisplayText;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw e;
            }
        }

        public static string GetDocumentTypeKeyTextFor(DocumentTypeDisplayType documentTypeDisplayType)
        {
            try {
                return EnumTools.ReadDescription(documentTypeDisplayType);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw new ArgumentOutOfRangeException(nameof(documentTypeDisplayType), documentTypeDisplayType, null);
            }
        }

        private static void CreateDocumentTypeDictionary()
        {
            DocumentTypeDictionary =
                new Dictionary<DocumentTypeDisplayType, string> {
                    {DocumentTypeDisplayType.Application, Strings.DocumentType_Application},
                    {DocumentTypeDisplayType.Invoice, Strings.DocumentType_Invoice},
                    {DocumentTypeDisplayType.Recommendation, Strings.DocumentType_Recommendation},
                    {DocumentTypeDisplayType.ScoutingApplication, Strings.DocumentType_ScoutingApplication},
                    {DocumentTypeDisplayType.WorkOrder, Strings.DocumentType_WorkOrder},
                    {DocumentTypeDisplayType.Plan, Strings.DocumentType_Plan },
                    {DocumentTypeDisplayType.Unknown, "Unknown Document Type"},
                };
        }

        public static List<DocumentTypeDisplayItem> AllAsList()
        {
            var theListOfDisplayItems = DocumentTypeDictionary.Keys.Select(x => new DocumentTypeDisplayItem(x)).ToList();
            //TODO: Use LINQ to sort alphabetically in a way that is dependent upon culture (i.e. implement IComparable):    theListOfDisplayItems.Sort();

            return theListOfDisplayItems;
        }

        /// <summary>
        /// Compares <see cref="DocumentTypeDisplayType"/>, <see cref="DocumentTypeDisplayType"/>, and <see cref="string"/> arguments.
        /// </summary>
        /// <param name="toCompare"></param>
        /// <returns></returns>
        public override bool Equals(object toCompare)
        {
            if (toCompare is DocumentTypeDisplayItem displayCompareItem)
            {
                return DocumentType.Equals(displayCompareItem.DocumentType);
            }
            else if (toCompare is DocumentTypeDisplayType typeCompareItem)
            {
                return DocumentType.Equals(typeCompareItem);
            }
            else if (toCompare is string stringCompareItem)
            {
                var simplifiedName = stringCompareItem.Contains("F") ? "F" : stringCompareItem.Contains("C") ? "C" : null;
                return DocumentTypeDictionary.Any(x => EnumTools.ReadDescription(x.Key).Equals(simplifiedName));
            }
            else
            {
                return false;
            }
        }

        public override string ToString()
        {
            return DisplayText;
        }
    }
}