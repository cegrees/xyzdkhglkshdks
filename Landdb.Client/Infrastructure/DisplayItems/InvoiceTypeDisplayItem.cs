﻿using System.Windows.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using Landdb.Client.Resources;
using System.ComponentModel;

namespace Landdb.Client.Infrastructure.DisplayItems
{

    public enum InvoiceTypeDisplayType {
        [Description("Non-Pending Invoices")] 
        Actual,
        [Description("All Invoices")]
        All,
        [Description("Pending Invoices")]
        Pending,
        [Description("Unknown Status")] 
        Unknown,
    }


    public class InvoiceTypeDisplayItem
    {
        static InvoiceTypeDisplayItem()
        {
            CreateInvoiceTypeDictionary();
        }

        public InvoiceTypeDisplayItem(string invoiceTypeName)
        {
            InvoiceType = GetInvoiceTypeFor(invoiceTypeName);
        }

        public InvoiceTypeDisplayItem(InvoiceTypeDisplayType type)
        {
            InvoiceType = type;
        }

        public InvoiceTypeDisplayType InvoiceType { get; }
        public string DisplayText => $"{GetInvoiceTypeDisplayTextFor(InvoiceType)}";
        public string KeyText => GetInvoiceTypeKeyTextFor(InvoiceType);

        public static Dictionary<InvoiceTypeDisplayType, string> InvoiceTypeDictionary { get; private set; }

        public static InvoiceTypeDisplayType GetInvoiceTypeFor(string invoiceTypeName)
        {
            try
            {
                var displayType = InvoiceTypeDisplayType.Unknown;
                if (!string.IsNullOrEmpty(invoiceTypeName)) {
                    var invoiceType = InvoiceTypeDictionary.FirstOrDefault(x => EnumTools.ReadDescription(x.Key).Equals(invoiceTypeName));
                    if (invoiceType.Equals(null)) {
                        invoiceType = InvoiceTypeDictionary.FirstOrDefault(x => x.Value.Equals(invoiceTypeName));
                    }

                    displayType = invoiceType.Equals(null)? InvoiceTypeDisplayType.Unknown : invoiceType.Key;
                }
                return displayType;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw e;
            }
        }

        public static string GetInvoiceTypeDisplayTextFor(InvoiceTypeDisplayType invoiceTypeDisplayType)
        {
            try
            {
                return InvoiceTypeDictionary[invoiceTypeDisplayType];
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw new ArgumentOutOfRangeException(nameof(invoiceTypeDisplayType), invoiceTypeDisplayType, null);
            }
        }

        public static string GetInvoiceTypeDisplayTextFor(string invoiceTypeKeyText)
        {
            try
            {
                var thisDisplayItem = new InvoiceTypeDisplayItem(invoiceTypeKeyText);
                return thisDisplayItem.DisplayText;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw e;
            }
        }

        public static string GetInvoiceTypeKeyTextFor(InvoiceTypeDisplayType invoiceTypeDisplayType)
        {
            try
            {
                return EnumTools.ReadDescription(invoiceTypeDisplayType);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw new ArgumentOutOfRangeException(nameof(invoiceTypeDisplayType), invoiceTypeDisplayType, null);
            }
        }

        private static void CreateInvoiceTypeDictionary()
        {
            InvoiceTypeDictionary =
                new Dictionary<InvoiceTypeDisplayType, string> {
                    {InvoiceTypeDisplayType.Actual, Strings.InvoiceType_Actual},
                    {InvoiceTypeDisplayType.All, Strings.InvoiceType_All},
                    {InvoiceTypeDisplayType.Pending, Strings.InvoiceType_Pending},
                    {InvoiceTypeDisplayType.Unknown, Strings.InvoiceType_Unknown},
                };
        }

        public static List<InvoiceTypeDisplayItem> AllAsList()
        {
            var theListOfDisplayItems = InvoiceTypeDictionary.Keys.Select(x => new InvoiceTypeDisplayItem(x)).ToList();
            //TODO: Use LINQ to sort alphabetically in a way that is dependent upon culture (i.e. implement IComparable):    theListOfDisplayItems.Sort();

            return theListOfDisplayItems;
        }

        /// <summary>
        /// Compares <see cref="InvoiceTypeDisplayType"/>, <see cref="InvoiceTypeDisplayType"/>, and <see cref="string"/> arguments.
        /// </summary>
        /// <param name="toCompare"></param>
        /// <returns></returns>
        public override bool Equals(object toCompare)
        {
            if (toCompare is InvoiceTypeDisplayItem displayCompareItem)
            {
                return InvoiceType.Equals(displayCompareItem.InvoiceType);
            }
            else if (toCompare is InvoiceTypeDisplayType typeCompareItem)
            {
                return InvoiceType.Equals(typeCompareItem);
            }
            else if (toCompare is string stringCompareItem)
            {
                var simplifiedName = stringCompareItem.Contains("F") ? "F" : stringCompareItem.Contains("C") ? "C" : null;
                return InvoiceTypeDictionary.Any(x => EnumTools.ReadDescription(x.Key).Equals(simplifiedName));
            }
            else
            {
                return false;
            }
        }

        public override string ToString()
        {
            return DisplayText;
        }
    }
}