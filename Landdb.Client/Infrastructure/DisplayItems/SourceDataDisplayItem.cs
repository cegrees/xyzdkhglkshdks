﻿using System.Windows.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using Landdb.Client.Resources;
using System.ComponentModel;

namespace Landdb.Client.Infrastructure.DisplayItems
{

    public enum SourceDataDisplayType
    {
        [Description("Not used")]
        NotUsed,
        [Description(" ")]
        Null,
        [Description("Used in an application")]
        UsedInApplication,
        [Description("Used in a recommendation")]
        UsedInRecommendation,
        [Description("Used in a work order")]
        UsedInWorkOrder,
        [Description("Unknown")]
        Unknown,
    }


    public class SourceDataDisplayItem
    {
        static SourceDataDisplayItem()
        {
            CreateSourceDataDictionary();
        }

        public SourceDataDisplayItem(string sourceDataName)
        {
            SourceDataType = GetSourceDataFor(sourceDataName);
        }

        public SourceDataDisplayItem(SourceDataDisplayType type)
        {
            SourceDataType = type;
        }

        public SourceDataDisplayType SourceDataType { get; }
        public string DisplayText => $"{GetSourceDataDisplayTextFor(SourceDataType)}";
        public string KeyText => GetSourceDataKeyTextFor(SourceDataType);

        public static Dictionary<SourceDataDisplayType, string> SourceDataDictionary { get; private set; }

        public static SourceDataDisplayType GetSourceDataFor(string sourceDataName)
        {
            try
            {
                var displayType = SourceDataDisplayType.Unknown;
                if (!string.IsNullOrEmpty(sourceDataName))
                {
                    var sourceData = SourceDataDictionary.FirstOrDefault(x => EnumTools.ReadDescription(x.Key).Equals(sourceDataName));
                    if (sourceData.Equals(null))
                    {
                        sourceData = SourceDataDictionary.FirstOrDefault(x => x.Value.Equals(sourceDataName));
                    }

                    displayType = sourceData.Equals(null) ? SourceDataDisplayType.Unknown : sourceData.Key;
                }
                return displayType;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw e;
            }
        }

        public static string GetSourceDataDisplayTextFor(SourceDataDisplayType sourceDataDisplayType)
        {
            try
            {
                return SourceDataDictionary[sourceDataDisplayType];
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw new ArgumentOutOfRangeException(nameof(sourceDataDisplayType), sourceDataDisplayType, null);
            }
        }

        public static string GetSourceDataDisplayTextFor(string sourceDataKeyText)
        {
            try
            {
                var thisDisplayItem = new SourceDataDisplayItem(sourceDataKeyText);
                return thisDisplayItem.DisplayText;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw e;
            }
        }

        public static string GetSourceDataKeyTextFor(SourceDataDisplayType sourceDataDisplayType)
        {
            try
            {
                return EnumTools.ReadDescription(sourceDataDisplayType);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw new ArgumentOutOfRangeException(nameof(sourceDataDisplayType), sourceDataDisplayType, null);
            }
        }

        private static void CreateSourceDataDictionary()
        {
            SourceDataDictionary =
                new Dictionary<SourceDataDisplayType, string> {
                    {SourceDataDisplayType.Null, Strings.SourceData_Null},
                    {SourceDataDisplayType.NotUsed, Strings.SourceData_NotUsed},
                    {SourceDataDisplayType.UsedInApplication, Strings.SourceData_UsedInApplication},
                    {SourceDataDisplayType.UsedInRecommendation, Strings.SourceData_UsedInRecommendation},
                    {SourceDataDisplayType.UsedInWorkOrder, Strings.SourceData_UsedInWorkOrder},
                    {SourceDataDisplayType.Unknown, Strings.SourceData_Unknown},
                };
            //TODO: Use LINQ to sort alphabetically in a way that is dependent upon culture (i.e. implement IComparable):    theListOfDisplayItems.Sort();

        }

        public static List<SourceDataDisplayItem> AllAsList()
        {
            var theListOfDisplayItems = SourceDataDictionary.Keys.Select(x => new SourceDataDisplayItem(x)).ToList();

            return theListOfDisplayItems;
        }

        public static List<SourceDataDisplayItem> Sources_Invoice() {
            var theListOfDisplayItems = SourceDataDictionary
                                        .Where(x => x.Key.Equals(SourceDataDisplayType.Null) || x.Key.Equals(SourceDataDisplayType.UsedInApplication) || x.Key.Equals(SourceDataDisplayType.NotUsed))
                                        .ToDictionary(k => k.Key, v => v.Value)
                                        .Keys
                                        .Select(x => new SourceDataDisplayItem(x)).ToList();

            return theListOfDisplayItems;
        }

        public static List<SourceDataDisplayItem> Sources_WorkOrder() {
            var theListOfDisplayItems = SourceDataDictionary
                                        .Where(x => x.Key.Equals(SourceDataDisplayType.Null) || 
                                                    x.Key.Equals(SourceDataDisplayType.NotUsed) ||
                                                    x.Key.Equals(SourceDataDisplayType.UsedInApplication) || 
                                                    x.Key.Equals(SourceDataDisplayType.UsedInWorkOrder))
                                        .ToDictionary(k => k.Key, v => v.Value).Keys.Select(x => new SourceDataDisplayItem(x)).ToList();
            return theListOfDisplayItems;
        }

        /// <summary>
        /// Compares <see cref="SourceDataDisplayType"/>, <see cref="SourceDataDisplayType"/>, and <see cref="string"/> arguments.
        /// </summary>
        /// <param name="toCompare"></param>
        /// <returns></returns>
        public override bool Equals(object toCompare)
        {
            if (toCompare is SourceDataDisplayItem displayCompareItem)
            {
                return SourceDataType.Equals(displayCompareItem.SourceDataType);
            }
            else if (toCompare is SourceDataDisplayType typeCompareItem)
            {
                return SourceDataType.Equals(typeCompareItem);
            }
            else if (toCompare is string stringCompareItem)
            {
                var simplifiedName = stringCompareItem.Contains("F") ? "F" : stringCompareItem.Contains("C") ? "C" : null;
                return SourceDataDictionary.Any(x => EnumTools.ReadDescription(x.Key).Equals(simplifiedName));
            }
            else
            {
                return false;
            }
        }

        public override string ToString()
        {
            return DisplayText;
        }
    }
}