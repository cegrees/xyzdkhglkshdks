﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using Landdb.Client.Resources;

namespace Landdb.Client.Infrastructure.DisplayItems {
    public enum SoilMoistureDisplayType {
        [Description("Dry")] Dry,
        [Description("Moist")] Moist,
        [Description("Wet")] Wet,
        [Description("Unknown")] Unknown,
    }


    public class SoilMoistureDisplayItem {
        static SoilMoistureDisplayItem() {
            CreateSoilMoistureDictionary();
        }

        public SoilMoistureDisplayItem(string soilMoistureName) {
            SoilMoistureType = GetSoilMoistureTypeFor(soilMoistureName);
        }

        public SoilMoistureDisplayItem(SoilMoistureDisplayType type) {
            SoilMoistureType = type;
        }

        public SoilMoistureDisplayType SoilMoistureType { get; }
        public string DisplayText => $"{GetSoilMoistureDisplayTextFor(SoilMoistureType)}";
        public string KeyText => GetSoilMoistureKeyTextFor(SoilMoistureType);

        public static Dictionary<SoilMoistureDisplayType, (string untranslatedKey, string translatedValue)> SoilMoistureDictionary { get; private set; }

        public static SoilMoistureDisplayType GetSoilMoistureTypeFor(string soilMoistureName) {
            //TODO: clean up
            try {
                if (string.IsNullOrEmpty(soilMoistureName)) return SoilMoistureDisplayType.Unknown;

                var displayType = SoilMoistureDisplayType.Unknown;
                soilMoistureName = soilMoistureName.ToLower();
                var simplifiedName = soilMoistureName.Contains(Strings.SoilMoisture_Dry.ToLower()) ? "Dry" :
                    soilMoistureName.Contains(Strings.SoilMoisture_Moist.ToLower()) ? "Moist" :
                    soilMoistureName.Contains(Strings.SoilMoisture_Wet.ToLower()) ? "Wet" :
                    soilMoistureName.Contains(Strings.SoilMoisture_Unknown.ToLower()) ? "Unknown" :
                    null;
                if (!string.IsNullOrEmpty(simplifiedName)) {
                    displayType = SoilMoistureDictionary.FirstOrDefault(x => x.Value.untranslatedKey.Equals(simplifiedName)).Key;
                } else {
                    displayType = SoilMoistureDictionary.FirstOrDefault(x => x.Value.translatedValue.Equals(simplifiedName)).Key;
                }

                return displayType;
            } catch (Exception e) {
                Console.WriteLine(e);
                throw e;
            }
        }

        public static string GetSoilMoistureDisplayTextFor(SoilMoistureDisplayType soilMoistureDisplayType) {
            try {
                return SoilMoistureDictionary[soilMoistureDisplayType].translatedValue;
            } catch (Exception e) {
                Console.WriteLine(e);
                throw new ArgumentOutOfRangeException(nameof(soilMoistureDisplayType), soilMoistureDisplayType, null);
            }
        }

        public static string GetSoilMoistureDisplayTextFor(string soilMoistureKeyText) {
            try {
                var thisDisplayItem = new SoilMoistureDisplayItem(soilMoistureKeyText);
                return thisDisplayItem.DisplayText;
            } catch (Exception e) {
                Console.WriteLine(e);
                throw e;
            }
        }

        public static string GetSoilMoistureKeyTextFor(SoilMoistureDisplayType soilMoistureDisplayType) {
            try {
                return SoilMoistureDictionary[soilMoistureDisplayType].untranslatedKey;
            } catch (Exception e) {
                Console.WriteLine(e);
                throw new ArgumentOutOfRangeException(nameof(soilMoistureDisplayType), soilMoistureDisplayType, null);
            }
        }

        private static void CreateSoilMoistureDictionary() {
            SoilMoistureDictionary = new Dictionary<SoilMoistureDisplayType, (string, string)> {
                {SoilMoistureDisplayType.Dry, ("Dry", Strings.SoilMoisture_Dry)},
                {SoilMoistureDisplayType.Moist, ("Moist", Strings.SoilMoisture_Moist)},
                {SoilMoistureDisplayType.Wet, ("Wet", Strings.SoilMoisture_Wet)},
                {SoilMoistureDisplayType.Unknown, ("Unknown", Strings.SoilMoisture_Unknown)},
            };
        }

        public static List<SoilMoistureDisplayItem> AllAsList() {
            var theListOfDisplayItems = SoilMoistureDictionary.Keys.Select(x => new SoilMoistureDisplayItem(x)).ToList();
            //TODO: Use LINQ to sort alphabetically in a way that is dependent upon culture (i.e. implement IComparable):    theListOfDisplayItems.Sort();

            return theListOfDisplayItems;
        }

        /// <summary>
        /// Compares <see cref="SoilMoistureDisplayItem"/>, <see cref="SoilMoistureDisplayType"/>, and <see cref="string"/> arguments.
        /// </summary>
        /// <param name="toCompare"></param>
        /// <returns></returns>
        public override bool Equals(object toCompare) {
            if (toCompare is SoilMoistureDisplayItem displayCompareItem) {
                return SoilMoistureType.Equals(displayCompareItem.SoilMoistureType);
            } else if (toCompare is SoilMoistureDisplayType typeCompareItem) {
                return SoilMoistureType.Equals(typeCompareItem);
            } else if (toCompare is string stringCompareItem) {
                stringCompareItem = stringCompareItem.ToLower();
                var simplifiedName = stringCompareItem.Contains(Strings.SoilMoisture_Dry.ToLower()) ? "Dry" :
                    stringCompareItem.Contains(Strings.SoilMoisture_Moist.ToLower()) ? "Moist" :
                    stringCompareItem.Contains(Strings.SoilMoisture_Wet.ToLower()) ? "Wet" : null;
                return SoilMoistureDictionary.Any(x => x.Value.untranslatedKey == simplifiedName);
            } else {
                return false;
            }
        }

        public override string ToString() {
            return DisplayText;
        }
    }
}