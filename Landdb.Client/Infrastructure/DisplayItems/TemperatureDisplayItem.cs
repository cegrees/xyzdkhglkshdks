﻿using System.Windows.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using Landdb.Client.Resources;
using System.ComponentModel;

namespace Landdb.Client.Infrastructure.DisplayItems {

    public enum TemperatureDisplayType {
        [Description("Fahrenheit")]
        F,
        [Description("Celsius")]
        C,
        [Description("Unknown")]
        Unknown
    }


    public class TemperatureDisplayItem {
        static TemperatureDisplayItem() {
            CreateTemperatureDictionary();
        }

        public TemperatureDisplayItem(string temperatureName) {
            TemperatureType = GetTemperatureTypeFor(temperatureName);
        }

        public TemperatureDisplayItem(TemperatureDisplayType type) {
            TemperatureType = type;
        }

        public TemperatureDisplayType TemperatureType { get; }
        public string DisplayText => $"°{GetTemperatureDisplayTextFor(TemperatureType)}"; //should override DisplayItemBase so we get the lil' degrees symbol
        public string KeyText => GetTemperatureKeyTextFor(TemperatureType);

        public static Dictionary<TemperatureDisplayType, (string untranslatedKey, string translatedValue)> TemperatureDictionary { get; private set; }

        public static TemperatureDisplayType GetTemperatureTypeFor(string temperatureName) {
            try {
                var displayType = TemperatureDisplayType.Unknown;
                if (string.IsNullOrEmpty(temperatureName)) return TemperatureDisplayType.Unknown;

                var simplifiedName = temperatureName.Contains("F") ? "F" : temperatureName.Contains("C") ? "C" : null;
                if (!string.IsNullOrEmpty(simplifiedName)) {
                    displayType = TemperatureDictionary.FirstOrDefault(x => x.Value.untranslatedKey.Equals(simplifiedName)).Key;
                } else {
                    displayType = TemperatureDictionary.FirstOrDefault(x => x.Value.translatedValue.Equals(simplifiedName)).Key;
                }

                return displayType;
            } catch (Exception e) {
                Console.WriteLine(e);
                throw e;
            }
        }

        public static string GetTemperatureDisplayTextFor(TemperatureDisplayType temperatureDisplayType) {
            try {
                return TemperatureDictionary[temperatureDisplayType].translatedValue;
            } catch (Exception e) {
                Console.WriteLine(e);
                throw new ArgumentOutOfRangeException(nameof(temperatureDisplayType), temperatureDisplayType, null);
            }
        }

        public static string GetTemperatureDisplayTextFor(string temperatureKeyText) {
            try {
                var thisDisplayItem = new TemperatureDisplayItem(temperatureKeyText);
                return thisDisplayItem.DisplayText;
            } catch (Exception e) {
                Console.WriteLine(e);
                throw e;
            }
        }

        public static string GetTemperatureKeyTextFor(TemperatureDisplayType temperatureDisplayType) {
            try {
                return TemperatureDictionary[temperatureDisplayType].untranslatedKey;
            } catch (Exception e) {
                Console.WriteLine(e);
                throw new ArgumentOutOfRangeException(nameof(temperatureDisplayType), temperatureDisplayType, null);
            }
        }

        private static void CreateTemperatureDictionary() {
            TemperatureDictionary =
                new Dictionary<TemperatureDisplayType, (string, string)> {
                    {TemperatureDisplayType.F, ("F", Strings.Temperature_Fahrenheit)},
                    {TemperatureDisplayType.C, ("C", Strings.Temperature_Celsius)},
                    {TemperatureDisplayType.Unknown, ("Unknown Unit", "")},
                };
        }

        public static List<TemperatureDisplayItem> AllAsList() {
            var theListOfDisplayItems = TemperatureDictionary.Keys.Select(x => new TemperatureDisplayItem(x)).ToList();
            //TODO: Use LINQ to sort alphabetically in a way that is dependent upon culture (i.e. implement IComparable):    theListOfDisplayItems.Sort();

            return theListOfDisplayItems;
        }

        /// <summary>
        /// Compares <see cref="TemperatureDisplayItem"/>, <see cref="TemperatureDisplayType"/>, and <see cref="string"/> arguments.
        /// </summary>
        /// <param name="toCompare"></param>
        /// <returns></returns>
        public override bool Equals(object toCompare) {
            if (toCompare is TemperatureDisplayItem displayCompareItem) {
                return TemperatureType.Equals(displayCompareItem.TemperatureType);
            } else if (toCompare is TemperatureDisplayType typeCompareItem) {
                return TemperatureType.Equals(typeCompareItem);
            } else if (toCompare is string stringCompareItem) {
                var simplifiedName = stringCompareItem.Contains("F") ? "F" : stringCompareItem.Contains("C") ? "C" : null;
                return TemperatureDictionary.Any(x => x.Value.untranslatedKey == simplifiedName);
            } else {
                return false;
            }
        }

        public override string ToString() {
            return DisplayText;
        }
    }
}