﻿using Landdb.Client.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Landdb.Client.Infrastructure.DisplayItems {
    public enum AreaUnitDisplayType {
        //TODO: Remove the secondary key in the dictionary and use these descriptions instead
        //TODO: Include abbreviations
        [Description("acre")] Acre,
        [Description("hectare")] Hectare,
        [Description("Unknown")] Unknown //if anything ever shows up as Unknown, we have a *serious* problem in the datasource...lol
    }

    public class AreaUnitDisplayItem {
        static AreaUnitDisplayItem() {
            CreateAreaUnitDictionary();
        }

        public AreaUnitDisplayItem(string areaUnitName) {
            AreaUnitType = GetAreaUnitTypeFor(areaUnitName);
        }

        public AreaUnitDisplayItem(AreaUnitDisplayType type) {
            AreaUnitType = type;
        }

        public AreaUnitDisplayType AreaUnitType { get; }
        public string DisplayText => $"{GetAreaUnitDisplayTextFor(AreaUnitType)}";
        public string KeyText => GetAreaUnitKeyTextFor(AreaUnitType);
        public static Dictionary<AreaUnitDisplayType, (string untranslatedKey, string translatedValue)> AreaUnitDictionary { get; private set; }

        public static AreaUnitDisplayType GetAreaUnitTypeFor(string areaUnitName) {
            try {
                if (string.IsNullOrEmpty(areaUnitName)) return AreaUnitDisplayType.Unknown;
                var simplifiedName = areaUnitName.Contains("acre") || areaUnitName.Contains("ac") ? "acre" :
                    areaUnitName.Contains("hectare") || areaUnitName.Contains("ha") ? "hectare" : null;
                return AreaUnitDictionary.First(x => x.Value.untranslatedKey == simplifiedName).Key; //intentionally did not use "FirstOrDefault" 
            } catch (Exception e) {
                Console.WriteLine(e);
                throw e;
            }
        }

        public static string GetAreaUnitDisplayTextFor(AreaUnitDisplayType areaUnitDisplayType) {
            try {
                return AreaUnitDictionary[areaUnitDisplayType].translatedValue;
            } catch (Exception e) {
                Console.WriteLine(e);
                throw new ArgumentOutOfRangeException(nameof(areaUnitDisplayType), areaUnitDisplayType, null);
            }
        }

        public static string GetAreaUnitDisplayTextFor(string areaUnitKeyText) {
            try {
                var thisDisplayItem = new AreaUnitDisplayItem(areaUnitKeyText);
                return thisDisplayItem.DisplayText;
            } catch (Exception e) {
                Console.WriteLine(e);
                throw e;
            }
        }

        public static string GetAreaUnitKeyTextFor(AreaUnitDisplayType areaUnitDisplayType) {
            try {
                return AreaUnitDictionary[areaUnitDisplayType].untranslatedKey;
            } catch (Exception e) {
                Console.WriteLine(e);
                throw new ArgumentOutOfRangeException(nameof(areaUnitDisplayType), areaUnitDisplayType, null);
            }
        }

        private static void CreateAreaUnitDictionary() {
            AreaUnitDictionary = new Dictionary<AreaUnitDisplayType, (string, string)> {
                {AreaUnitDisplayType.Acre, ("acre", Strings.AreaUnit_American)},
                {AreaUnitDisplayType.Hectare, ("hectare", Strings.AreaUnit_Standard)},
                {AreaUnitDisplayType.Unknown, ("Unknown Unit", "")},
            };
        }

        public static List<AreaUnitDisplayItem> AllAsList() {
            var theListOfDisplayItems = AreaUnitDictionary.Keys.Select(x => new AreaUnitDisplayItem(x)).ToList();
            //TODO: Use LINQ to sort alphabetically in a way that is dependent upon culture (i.e. implement IComparable):    theListOfDisplayItems.Sort();

            return theListOfDisplayItems;
        }

        /// <summary>
        /// Compares <see cref="AreaUnitDisplayItem"/>, <see cref="AreaUnitDisplayType"/>, and <see cref="string"/> arguments.
        /// </summary>
        /// <param name="toCompare"></param>
        /// <returns></returns>
        public override bool Equals(object toCompare) {
            if (toCompare is AreaUnitDisplayItem displayCompareItem) {
                return AreaUnitType.Equals(displayCompareItem.AreaUnitType);
            } else if (toCompare is AreaUnitDisplayType typeCompareItem) {
                return AreaUnitType.Equals(typeCompareItem);
            } else if (toCompare is string stringCompareItem) {
                var simplifiedName = stringCompareItem.Contains("F") ? "F" :
                    stringCompareItem.Contains("C") ? "C" : null;
                return AreaUnitDictionary.Any(x => x.Value.untranslatedKey == simplifiedName);
            } else {
                return false;
            }
        }

        public override string ToString() {
            return DisplayText;
        }
    }
}