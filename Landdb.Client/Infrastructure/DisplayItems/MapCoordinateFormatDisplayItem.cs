﻿using Landdb.Client.Resources;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Landdb.Client.Infrastructure
{
    public class MapCoordinateFormatDisplayItem {
        static MapCoordinateFormatDisplayItem() {
            CreateMapCoordinateFormatDictionary();
        }

        public enum MapCoordinateFormatType {
            DecimalDegree,
            DegreesMinutesSeconds,
            DecimalSeconds
        }

        public MapCoordinateFormatDisplayItem(string mapCoordinateFormatName) {
            MapCoordinateFormat = GetMapCoordinateFormatTypeFor(mapCoordinateFormatName);
        }

        public MapCoordinateFormatType MapCoordinateFormat { get; }
        public string DisplayText => GetMapCoordinateDisplayTextFor(MapCoordinateFormat);
        public string KeyText => GetMapCoordinateKeyTextFor(MapCoordinateFormat);

        public static Dictionary<MapCoordinateFormatType, (string untranslatedKey, string translatedValue)> MapCoordinateFormatDictionary {
            get;
            private set;
        }

        public static MapCoordinateFormatType GetMapCoordinateFormatTypeFor(string mapCoordinateFormatName) {
            try {
                return MapCoordinateFormatDictionary.First(x => x.Value.untranslatedKey == mapCoordinateFormatName).Key;
            } catch (Exception e) {
                Console.WriteLine(e);
                throw e;
            }
        }

        public static string GetMapCoordinateDisplayTextFor(MapCoordinateFormatType mapCoordinateFormatType) {
            try {
                return MapCoordinateFormatDictionary[mapCoordinateFormatType].translatedValue;
            } catch (Exception e) {
                Console.WriteLine(e);
                throw new ArgumentOutOfRangeException(nameof(mapCoordinateFormatType), mapCoordinateFormatType, null);
            }
        }

        public static string GetMapCoordinateDisplayTextFor(string mapCoordinateFormatKeyText) {
            try {
                var thisDisplayItem = new MapCoordinateFormatDisplayItem(mapCoordinateFormatKeyText);
                return thisDisplayItem.DisplayText;
            } catch (Exception e) {
                Console.WriteLine(e);
                throw e;
            }
        }

        public static string GetMapCoordinateKeyTextFor(MapCoordinateFormatType mapCoordinateFormatType) {
            try {
                return MapCoordinateFormatDictionary[mapCoordinateFormatType].untranslatedKey;
            } catch (Exception e) {
                Console.WriteLine(e);
                throw new ArgumentOutOfRangeException(nameof(mapCoordinateFormatType), mapCoordinateFormatType, null);
            }
        }

        public static string GetMapCoordinateKeyTextFor(string displayText) {
            try {
                var thisFormat = MapCoordinateFormatDictionary.First(x => x.Value.translatedValue == displayText);
                return thisFormat.Value.untranslatedKey;
            } catch (Exception e) {
                Console.WriteLine(e);
                throw e;
            }
        }

        private static void CreateMapCoordinateFormatDictionary() {
            MapCoordinateFormatDictionary = new Dictionary<MapCoordinateFormatType, (string, string)> {
                {MapCoordinateFormatType.DecimalDegree, ("Decimal Degree", Strings.DisplayCoordinates_DecimalDegree)},
                {MapCoordinateFormatType.DegreesMinutesSeconds, ("Degrees Minutes Seconds", Strings.DisplayCoordinates_DegreesMinutesSeconds)},
                {MapCoordinateFormatType.DecimalSeconds, ("Decimal Seconds", Strings.DisplayCoordinates_DecimalSeconds)}
            };
        }

        private bool MapCoordinateFormatEquals(MapCoordinateFormatType toCompare) {
            if (MapCoordinateFormat.Equals(toCompare))
                return true;
            else
                return false;
        }

        public override bool Equals(object toCompare) {
            var compareItem = toCompare as MapCoordinateFormatDisplayItem;
            return MapCoordinateFormatEquals(compareItem.MapCoordinateFormat);
        }

        public override string ToString() {
            return DisplayText;
        }
    }
}