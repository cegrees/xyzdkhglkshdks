﻿using System.Windows.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using Landdb.Client.Resources;

namespace Landdb.Client.Infrastructure.DisplayItems {

    //TODO: Pare this down the same way as the ClearAgWindDirectionDisplayItem enum via the ClearAgWindDirectionCategorizer.
    public enum ClearAgSkyCoverType {
        Clear,
        BecomingClear,
        MostlyClear,
        BecomingMostlyClear,
        PartlyCloudy,
        BecomingPartlyCloudy,
        PartlyClear,
        BecomingPartlyClear,
        MostlyCloudy,
        BecomingMostlyCloudy,
        Cloudy,
        BecomingCloudy,
        Fair,
        BecomingFair,
        FewClouds,
        FewCloudsDeveloping,
        ScatteredClouds,
        ScatteredCloudsDeveloping,
        BrokenOvercast,
        BecomingBrokenOvercast,
        Overcast,
        BecomingOvercast,
        Clouds,
        DecreasingClouds,
        IncreasingClouds,
        VariableClouds,
        NumerousClouds,
        NumerousCloudsDeveloping,
        Sunny,
        BecomingSunny,
        MostlySunny,
        BecomingMostlySunny,
        PartlySunny,
        BecomingPartlySunny,
        ConsiderableCloudiness,
        BecomingConsiderableCloudiness,
        ConsiderableCloudinessDeveloping,
        Unknown
    }

    public class ClearAgSkyCoverDisplayItem {
        static ClearAgSkyCoverDisplayItem() {
            CreateClearAgSkyCoverDictionary();
        }

        public ClearAgSkyCoverDisplayItem(string clearAgSkyCoverName) {
            ClearAgSkyCover = GetClearAgSkyCoverTypeFor(clearAgSkyCoverName);
        }

        public ClearAgSkyCoverDisplayItem(ClearAgSkyCoverType type) {
            ClearAgSkyCover = type;
        }

        public ClearAgSkyCoverType ClearAgSkyCover { get; }
        public string DisplayText => GetClearAgSkyCoverDisplayTextFor(ClearAgSkyCover);
        public string KeyText => GetClearAgSkyCoverKeyTextFor(ClearAgSkyCover);

        public static Dictionary<ClearAgSkyCoverType, (string untranslatedKey, string translatedValue)> ClearAgSkyCoverDictionary {
            get;
            private set;
        }

        public static ClearAgSkyCoverType GetClearAgSkyCoverTypeFor(string clearAgSkyCoverName) {
            try {
                if (!String.IsNullOrEmpty(clearAgSkyCoverName)) {
                    foreach (var skyCover in ClearAgSkyCoverDictionary) {
                        if (skyCover.Value.translatedValue.ToLower().Equals(clearAgSkyCoverName.ToLower()) || 
                            skyCover.Value.untranslatedKey.ToLower().Equals(clearAgSkyCoverName.ToLower())) {
                            return skyCover.Key;
                        }
                    }
                }

                return ClearAgSkyCoverType.Unknown;
            } catch (Exception e) {
                Console.WriteLine(e);
                throw e;
            }
        }

        public static string GetClearAgSkyCoverDisplayTextFor(ClearAgSkyCoverType clearAgSkyCoverType) {
            try {
                return ClearAgSkyCoverDictionary[clearAgSkyCoverType].translatedValue;
            } catch (Exception e) {
                Console.WriteLine(e);
                throw new ArgumentOutOfRangeException(nameof(clearAgSkyCoverType), clearAgSkyCoverType, null);
            }
        }

        public static string GetClearAgSkyCoverDisplayTextFor(string clearAgSkyCoverKeyText) {
            try {
                var thisDisplayItem = new ClearAgSkyCoverDisplayItem(clearAgSkyCoverKeyText);
                return thisDisplayItem.DisplayText;
            } catch (Exception e) {
                Console.WriteLine(e);
                throw e;
            }
        }

        public static string GetClearAgSkyCoverKeyTextFor(ClearAgSkyCoverType clearAgSkyCoverType) {
            try {
                return ClearAgSkyCoverDictionary[clearAgSkyCoverType].untranslatedKey;
            } catch (Exception e) {
                Console.WriteLine(e);
                throw new ArgumentOutOfRangeException(nameof(clearAgSkyCoverType), clearAgSkyCoverType, null);
            }
        }

        private static void CreateClearAgSkyCoverDictionary() {
            ClearAgSkyCoverDictionary = new Dictionary<ClearAgSkyCoverType, (string, string)> {
                {ClearAgSkyCoverType.Clear, ("Clear", Strings.SkyCover_Clear)},
                {ClearAgSkyCoverType.BecomingClear, ("Becoming Clear", Strings.SkyCover_BecomingClear)},
                {ClearAgSkyCoverType.MostlyClear, ("Mostly Clear", Strings.SkyCover_MostlyClear)},
                {ClearAgSkyCoverType.BecomingMostlyClear, ("Becoming Mostly Clear", Strings.SkyCover_BecomingMostlyClear)},
                {ClearAgSkyCoverType.PartlyCloudy, ("Partly Cloudy", Strings.SkyCover_PartlyCloudy)},
                {ClearAgSkyCoverType.BecomingPartlyCloudy, ("Becoming Partly Cloudy", Strings.SkyCover_BecomingPartlyCloudy)},
                {ClearAgSkyCoverType.PartlyClear, ("Partly Clear", Strings.SkyCover_PartlyClear)},
                {ClearAgSkyCoverType.BecomingPartlyClear, ("Becoming Partly Clear", Strings.SkyCover_BecomingPartlyClear)},
                {ClearAgSkyCoverType.MostlyCloudy, ("Mostly Cloudy", Strings.SkyCover_MostlyCloudy)},
                {ClearAgSkyCoverType.BecomingMostlyCloudy, ("Becoming Mostly Cloudy", Strings.SkyCover_BecomingMostlyCloudy)},
                {ClearAgSkyCoverType.Cloudy, ("Cloudy", Strings.SkyCover_Cloudy)},
                {ClearAgSkyCoverType.BecomingCloudy, ("Becoming Cloudy", Strings.SkyCover_BecomingCloudy)},
                {ClearAgSkyCoverType.Fair, ("Fair", Strings.SkyCover_Fair)},
                {ClearAgSkyCoverType.BecomingFair, ("Becoming Fair", Strings.SkyCover_BecomingFair)},
                {ClearAgSkyCoverType.FewClouds, ("Few Clouds", Strings.SkyCover_FewClouds)},
                {ClearAgSkyCoverType.FewCloudsDeveloping, ("Few Clouds Developing", Strings.SkyCover_FewCloudsDeveloping)},
                {ClearAgSkyCoverType.ScatteredClouds, ("Scattered Clouds", Strings.SkyCover_ScatteredClouds)},
                {ClearAgSkyCoverType.ScatteredCloudsDeveloping, ("Scattered Clouds Developing", Strings.SkyCover_ScatteredCloudsDeveloping)},
                {ClearAgSkyCoverType.BrokenOvercast, ("Broken Overcast", Strings.SkyCover_BrokenOvercast)},
                {ClearAgSkyCoverType.BecomingBrokenOvercast, ("Becoming Broken Overcast", Strings.SkyCover_BecomingBrokenOvercast)},
                {ClearAgSkyCoverType.Overcast, ("Overcast", Strings.SkyCover_Overcast)},
                {ClearAgSkyCoverType.BecomingOvercast, ("Becoming Overcast", Strings.SkyCover_BecomingOvercast)},
                {ClearAgSkyCoverType.Clouds, ("Clouds", Strings.SkyCover_Clouds)},
                {ClearAgSkyCoverType.DecreasingClouds, ("Decreasing Clouds", Strings.SkyCover_DecreasingClouds)},
                {ClearAgSkyCoverType.IncreasingClouds, ("Increasing Clouds", Strings.SkyCover_IncreasingClouds)},
                {ClearAgSkyCoverType.VariableClouds, ("Variable Clouds", Strings.SkyCover_VariableClouds)},
                {ClearAgSkyCoverType.NumerousClouds, ("Numerous Clouds", Strings.SkyCover_NumerousClouds)},
                {ClearAgSkyCoverType.NumerousCloudsDeveloping, ("Numerous Clouds Developing", Strings.SkyCover_NumerousCloudsDeveloping)},
                {ClearAgSkyCoverType.Sunny, ("Sunny", Strings.SkyCover_Sunny)},
                {ClearAgSkyCoverType.BecomingSunny, ("Becoming Sunny", Strings.SkyCover_BecomingSunny)},
                {ClearAgSkyCoverType.MostlySunny, ("Mostly Sunny", Strings.SkyCover_MostlySunny)},
                {ClearAgSkyCoverType.BecomingMostlySunny, ("Becoming Mostly Sunny", Strings.SkyCover_BecomingMostlySunny)},
                {ClearAgSkyCoverType.PartlySunny, ("Partly Sunny", Strings.SkyCover_PartlySunny)},
                {ClearAgSkyCoverType.BecomingPartlySunny, ("Becoming Partly Sunny", Strings.SkyCover_BecomingPartlySunny)},
                {ClearAgSkyCoverType.ConsiderableCloudiness, ("Considerable Cloudiness", Strings.SkyCover_ConsiderableCloudiness)},
                {ClearAgSkyCoverType.BecomingConsiderableCloudiness, ("Becoming Considerable Cloudiness", Strings.SkyCover_BecomingConsiderableCloudiness)},
                {ClearAgSkyCoverType.ConsiderableCloudinessDeveloping, ("Considerable Cloudiness Developing", Strings.SkyCover_ConsiderableCloudinessDeveloping)},
                {ClearAgSkyCoverType.Unknown, ("Unknown", "")},
            };
        }

        public static List<ClearAgSkyCoverDisplayItem> AllAsList() {
            var theListOfDisplayItems = ClearAgSkyCoverDictionary.Keys.Select(x => new ClearAgSkyCoverDisplayItem(x)).ToList();
            //TODO: Use LINQ to sort alphabetically in a way that is dependent upon culture (i.e. implement IComparable):    theListOfDisplayItems.Sort();

            return theListOfDisplayItems;
        }

        public override bool Equals(object toCompare) {
            switch (toCompare) {
                case ClearAgSkyCoverDisplayItem displayCompareItem:
                    return ClearAgSkyCover.Equals(displayCompareItem.ClearAgSkyCover);
                case ClearAgSkyCoverType typeCompareItem:
                    return ClearAgSkyCover.Equals(typeCompareItem);
                case string stringCompareItem:
                    return ClearAgSkyCoverDictionary.Any(x => x.Value.untranslatedKey == stringCompareItem);
                default:
                    return false;
            }
        }

        public override string ToString() {
            return DisplayText;
        }
    }
}