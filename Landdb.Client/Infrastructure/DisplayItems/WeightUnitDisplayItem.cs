﻿using Landdb.Client.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Landdb.Client.Infrastructure.DisplayItems {
    public enum WeightUnitDisplayType {
        [Description("lb")] Pound,
        [Description("kg")] Kilogram,
        [Description("Unknown")] Unknown //if anything ever shows up as Unknown, we have a *serious* problem in the datasource...lol
    }

    public class WeightUnitDisplayItem {
        static WeightUnitDisplayItem() {
            CreateWeightUnitDictionary();
        }

        public WeightUnitDisplayItem(string weightUnitName) {
            WeightUnitType = GetWeightUnitTypeFor(weightUnitName);
        }

        public WeightUnitDisplayItem(WeightUnitDisplayType type) {
            WeightUnitType = type;
        }

        public WeightUnitDisplayType WeightUnitType { get; }
        public string DisplayText => $"°{GetWeightUnitDisplayTextFor(WeightUnitType)}";
        public string KeyText => GetWeightUnitKeyTextFor(WeightUnitType);
        public static Dictionary<WeightUnitDisplayType, (string untranslatedKey, string translatedValue)> WeightUnitDictionary { get; private set; }

        public static WeightUnitDisplayType GetWeightUnitTypeFor(string weightUnitName) {
            try {
                if (string.IsNullOrEmpty(weightUnitName)) return WeightUnitDisplayType.Unknown;
                //TODO: May want to reverse this logic so that it checks to see if the description contains the weight unit name...that way the description can have the name and the acronym
                var key = weightUnitName.Contains(EnumTools.ReadDescription(WeightUnitDisplayType.Pound)) ? WeightUnitDisplayType.Pound :
                    weightUnitName.Contains(EnumTools.ReadDescription(WeightUnitDisplayType.Kilogram)) ? WeightUnitDisplayType.Kilogram : WeightUnitDisplayType.Unknown;
                return WeightUnitDictionary.First(x => x.Key == key).Key; //intentionally did not use "FirstOrDefault" 
            } catch (Exception e) {
                Console.WriteLine(e);
                throw e;
            }
        }

        public static string GetWeightUnitDisplayTextFor(WeightUnitDisplayType weightUnitDisplayType) {
            try {
                return WeightUnitDictionary[weightUnitDisplayType].translatedValue;
            } catch (Exception e) {
                Console.WriteLine(e);
                throw new ArgumentOutOfRangeException(nameof(weightUnitDisplayType), weightUnitDisplayType, null);
            }
        }

        public static string GetWeightUnitDisplayTextFor(string weightUnitKeyText) {
            try {
                var thisDisplayItem = new WeightUnitDisplayItem(weightUnitKeyText);
                return thisDisplayItem.DisplayText;
            } catch (Exception e) {
                Console.WriteLine(e);
                throw e;
            }
        }

        public static string GetWeightUnitKeyTextFor(WeightUnitDisplayType weightUnitDisplayType) {
            try {
                return WeightUnitDictionary[weightUnitDisplayType].untranslatedKey;
            } catch (Exception e) {
                Console.WriteLine(e);
                throw new ArgumentOutOfRangeException(nameof(weightUnitDisplayType), weightUnitDisplayType, null);
            }
        }

        private static void CreateWeightUnitDictionary() {
            WeightUnitDictionary = new Dictionary<WeightUnitDisplayType, (string, string)> {
                {WeightUnitDisplayType.Pound, ("pound", Strings.WeightUnit_American)},
                {WeightUnitDisplayType.Kilogram, ("kilogram", Strings.WeightUnit_Standard)},
                {WeightUnitDisplayType.Unknown, ("Unknown Unit", "")},
            };
        }

        public static List<WeightUnitDisplayItem> AllAsList() {
            var theListOfDisplayItems = WeightUnitDictionary.Keys.Select(x => new WeightUnitDisplayItem(x)).ToList();
            //TODO: Use LINQ to sort alphabetically in a way that is dependent upon culture (i.e. implement IComparable):    theListOfDisplayItems.Sort();

            return theListOfDisplayItems;
        }

        /// <summary>
        /// Compares <see cref="WeightUnitDisplayItem"/>, <see cref="WeightUnitDisplayType"/>, and <see cref="string"/> arguments.
        /// </summary>
        /// <param name="toCompare"></param>
        /// <returns></returns>
        public override bool Equals(object toCompare) {
            if (toCompare is WeightUnitDisplayItem displayCompareItem) {
                return WeightUnitType.Equals(displayCompareItem.WeightUnitType);
            } else if (toCompare is WeightUnitDisplayType typeCompareItem) {
                return WeightUnitType.Equals(typeCompareItem);
            } else if (toCompare is string stringCompareItem) {
                var simplifiedName = stringCompareItem.Contains("F") ? "F" :
                    stringCompareItem.Contains("C") ? "C" : null;
                return WeightUnitDictionary.Any(x => x.Value.untranslatedKey == simplifiedName);
            } else {
                return false;
            }
        }

        public override string ToString() {
            return DisplayText;
        }
    }
}