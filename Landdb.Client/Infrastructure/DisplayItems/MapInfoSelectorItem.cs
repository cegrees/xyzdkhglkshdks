﻿using System;
using Landdb.Client.Resources;

namespace Landdb.Client.Infrastructure {
    public enum MapStatusType {
        Bing,
        BingOnline,
        Google,
        GoogleOffline,
        RoadsOnline,
        RoadsOnly,
        None,
        PlanetLab,
        Topo,
        USGS,
        WorldMapKit
    }

    public class MapInfoSelectorItem {
        public MapInfoSelectorItem(MapStatusType mapStatus) {
            MapStatus = mapStatus;
        }

        /// <summary>
        /// This constructor is designed only to be used in the direst of situations. It has been created specifically for backwards compatibility. 
        /// The <see cref="MapStatusType"/> enum has replaced the usage of string key values.
        /// </summary>
        /// <param name="mapTypeKeyText"></param>
        public MapInfoSelectorItem(string mapTypeKeyText) {
            MapStatus = GetMapStatusTypeFor(mapTypeKeyText);
        }

        public MapStatusType MapStatus { get; }
        public string DisplayText => GetMapStatusDisplayText(MapStatus);

        public bool IsBing() {
            return MapStatusEquals(MapStatusType.Bing);
        }

        public bool IsGoogle() {
            return MapStatusEquals(MapStatusType.Google);
        }

        public bool IsGoogleOffline() {
            return MapStatusEquals(MapStatusType.GoogleOffline);
        }

        public bool IsRoadsOnly() {
            return MapStatusEquals(MapStatusType.RoadsOnly);
        }

        private static string GetMapStatusDisplayText(MapStatusType mapStatusType) {
            switch (mapStatusType) {
                case MapStatusType.Bing:
                    return Strings.MapInfoSelectorItem_Bing;
                case MapStatusType.BingOnline:
                    return Strings.MapInfoSelectorItem_BingOnline;
                case MapStatusType.Google:
                    return Strings.MapInfoSelectorItem_Google;
                case MapStatusType.GoogleOffline:
                    return Strings.MapInfoSelectorItem_GoogleOffline;
                case MapStatusType.None:
                    return Strings.MapInfoSelectorItem_None;
                case MapStatusType.PlanetLab:
                    return Strings.MapInfoSelectorItem_PlanetLab;
                case MapStatusType.RoadsOnline:
                    return Strings.MapInfoSelectorItem_RoadOnline;
                case MapStatusType.RoadsOnly:
                    return Strings.MapInfoSelectorItem_RoadsOnly;
                case MapStatusType.Topo:
                    return Strings.MapInfoSelectorItem_Topo;
                case MapStatusType.USGS:
                    return Strings.MapInfoSelectorItem_USGS;
                case MapStatusType.WorldMapKit:
                    return Strings.MapInfoSelectorItem_WorldMapKit;
                default:
                    throw new ArgumentOutOfRangeException(nameof(mapStatusType), mapStatusType, null);
            }
        }

        public string MapTypeKeyText => GetMapTypeKeyTextFor(MapStatus);

        public static string GetMapTypeKeyTextFor(MapStatusType mapStatusType) {
            switch (mapStatusType) {
                case MapStatusType.Bing:
                    return "Bing";
                case MapStatusType.BingOnline:
                    return "Bing Online";
                case MapStatusType.Google:
                    return "Google";
                case MapStatusType.GoogleOffline:
                    return "Google1";
                case MapStatusType.RoadsOnline:
                    return "Roads Online";
                case MapStatusType.RoadsOnly:
                    return "Roads Only";
                case MapStatusType.None:
                    return "None";
                case MapStatusType.PlanetLab:
                    return "PlanetLab";
                case MapStatusType.Topo:
                    return "Topo";
                case MapStatusType.USGS:
                    return "USGS";
                case MapStatusType.WorldMapKit:
                    return "WorldMapKit";
                default:
                    throw new ArgumentOutOfRangeException(nameof(mapStatusType), mapStatusType, null);
            }
        }

        public static MapStatusType GetMapStatusTypeFor(string mapTypeKeyText) {
            switch (mapTypeKeyText) {
                case "Bing":
                    return MapStatusType.Bing;
                case "Bing Online":
                    return MapStatusType.BingOnline;
                case "Google":
                    return MapStatusType.Google;
                case "Google1":
                    return MapStatusType.GoogleOffline;
                case "Roads Online":
                    return MapStatusType.RoadsOnline;
                case "Roads Only":
                    return MapStatusType.RoadsOnly;
                case "None":
                    return MapStatusType.None;
                case "PlanetLab":
                    return MapStatusType.PlanetLab;
                case "Topo":
                    return MapStatusType.Topo;
                case "USGS":
                    return MapStatusType.USGS;
                case "WorldMapKit":
                    return MapStatusType.WorldMapKit;
                default:
                    throw new ArgumentOutOfRangeException(nameof(mapTypeKeyText), mapTypeKeyText, null);
            }
        }

        public bool MapStatusEquals(MapStatusType toCompare) {
            if (MapStatus.Equals(toCompare))
                return true;
            return false;
        }

        public bool Equals(MapInfoSelectorItem toCompare) {
            return MapStatusEquals(toCompare.MapStatus);
        }

        public override string ToString() {
            return DisplayText;
        }
    }
}