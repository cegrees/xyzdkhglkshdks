﻿using System;
using System.Collections.Generic;
using Landdb.Client.Resources;

namespace Landdb.Client.Infrastructure.DisplayItems {
    public class CropMapsFarmDisplayItem {
        static CropMapsFarmDisplayItem() {
            CreateFarmDisplayDictionary();
        }

        public CropMapsFarmDisplayItem(string farmDisplayName) {
            FarmDisplay = GetFarmDisplayTypeFor(farmDisplayName);
            if (farmDisplayName.ToLower().Equals("all farms"))
                DisplayText = FarmDisplayDictionary[FarmDisplayType.AllFarms];
            else {
                DisplayText = farmDisplayName;
            }
        }

        private FarmDisplayType FarmDisplay { get; }
        public string DisplayText { get; set; }
        public static Dictionary<FarmDisplayType, string> FarmDisplayDictionary { get; private set; }

        public static FarmDisplayType GetFarmDisplayTypeFor(string farmDisplayName) {
            return farmDisplayName.ToLower().Equals("all farms") ? FarmDisplayType.AllFarms : FarmDisplayType.UserDefinedFarm;
        }

        private static void CreateFarmDisplayDictionary() {
            FarmDisplayDictionary = new Dictionary<FarmDisplayType, string> {{FarmDisplayType.AllFarms, Strings.CropMapsFarm_AllFarms},};
        }

        private bool FarmDisplayEquals(FarmDisplayType toCompare) {
            return FarmDisplay == toCompare;
        }

        public bool IsAllFarms => FarmDisplayEquals(FarmDisplayType.AllFarms);

        public override bool Equals(object toCompare) {
            var compareItem = toCompare as CropMapsFarmDisplayItem;
            
            return compareItem == null ? false : FarmDisplayEquals(compareItem.FarmDisplay);
        }

        public override string ToString() {
            return DisplayText;
        }
    }

    public enum FarmDisplayType {
        AllFarms,
        UserDefinedFarm
    }
}