﻿using Landdb.Client.Models.Connect;
using Landdb.Client.Services;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Landdb.Client.Infrastructure {
    public class ConnectEngine : IConnectEngine {
        IConnectClient connectClient;
        CancellationTokenSource currentCancellationTokenSource;
        Logger log = LogManager.GetCurrentClassLogger();

        IConnectInvoiceRepository invoiceRepository;
        private readonly List<Action> subscribers;

        public ConnectEngine(IClientEndpoint clientEndpoint, string connectHostUrl, string connectWebUrl) 
            : this(new ConnectClient(clientEndpoint, connectHostUrl, connectWebUrl)) { }

        public ConnectEngine(IConnectClient connectClient) {
            this.connectClient = connectClient;
            invoiceRepository = new ConnectInvoiceRepository(this.connectClient);
            subscribers = new List<Action>();
        }

        public Task StartSynchronization(Guid datasourceId) {
            CancelCurrentSyncOperation();

            currentCancellationTokenSource = new CancellationTokenSource();
            return Task.Factory.StartNew(() => {
                try {
                    InternalSyncLoop(datasourceId, currentCancellationTokenSource.Token).Wait();
                } catch (Exception ex) {
                    log.ErrorException("Caught an exception in the Connect engine sync loop", ex);
                }
            });
        }

        public void CancelCurrentSyncOperation() {
            if(currentCancellationTokenSource != null) {
                currentCancellationTokenSource.Cancel();
            }
        }

        public IConnectInvoiceRepository InvoiceRepository { get { return invoiceRepository; } }

        async Task InternalSyncLoop(Guid datasourceId, CancellationToken cancellationToken) {
            bool? canSyncInvoices = null;

            while(!cancellationToken.IsCancellationRequested) {
                if (!canSyncInvoices.HasValue) { // If we don't know whether we can sync invoices, check
                    canSyncInvoices = await connectClient.CanGetInvoicesAsync(datasourceId);
                }

                var refreshTasks = new List<Task>();

                if(canSyncInvoices.GetValueOrDefault()) { // If we definitively can sync invoices, then include this task in the refresh
                    refreshTasks.Add(invoiceRepository.RefreshAsync(datasourceId));
                }

                await Task.WhenAll(refreshTasks);
                subscribers.ForEach(action => action.Invoke());

                await Task.Delay(TimeSpan.FromSeconds(60), cancellationToken);
            }
        }

        public void Subscribe(Action subscriberAction)
        {
            subscribers.Add(subscriberAction);
        }
    }

    public class ConnectInvoiceRepository : IConnectInvoiceRepository {
        object lockObject = new object();
        List<InvoiceBlockHeader> backingList;
        IConnectClient client;
        public event EventHandler InvoicesRefreshed;

        public ConnectInvoiceRepository(IConnectClient client) {
            this.client = client;
            this.backingList = new List<InvoiceBlockHeader>();
        }

        public async Task RefreshAsync(Guid datasourceId) {
            var newInvoices = await client.GetPendingInvoiceHeadersAsync(datasourceId);
            lock(lockObject) {
                backingList.Clear();
                if (newInvoices != null && newInvoices.InvoiceBlocks != null) {
                    backingList.AddRange(newInvoices.InvoiceBlocks);
                }
                OnListRefreshed(EventArgs.Empty);
            }
        }

        public Task<IEnumerable<InvoiceBlockHeader>> GetInvoiceHeadersAsync() {
            lock (lockObject) {
                var readonlyList = backingList.AsReadOnly();
                return Task.FromResult(readonlyList.AsEnumerable());
            }
        }

        public async Task<InvoiceView> GetInvoiceAsync(InvoiceId invoiceId) {
            return await client.GetInvoiceAsync(invoiceId);
        }

        public async Task<bool> RemoveInvoiceAsync(InvoiceId invoiceId) {
            return await client.RemoveInvoiceAsync(invoiceId);
        }

        protected virtual void OnListRefreshed(EventArgs e) {
            var handler = InvoicesRefreshed;
            if(handler != null) { handler(this, e); }
        }
    }

    public interface IConnectInvoiceRepository {
        event EventHandler InvoicesRefreshed;
        Task RefreshAsync(Guid datasourceId);
        Task<IEnumerable<InvoiceBlockHeader>> GetInvoiceHeadersAsync();
        Task<InvoiceView> GetInvoiceAsync(InvoiceId invoiceId);
        Task<bool> RemoveInvoiceAsync(InvoiceId invoiceId);
    }
}
