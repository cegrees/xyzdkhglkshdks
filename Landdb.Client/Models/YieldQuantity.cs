﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.Client.Models {
    public class YieldQuantity {
        public CompositeUnit QuantityUnit { get; set; }
        public double TotalQuantity { get; set; }
    }
}
