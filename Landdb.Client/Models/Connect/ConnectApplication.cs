﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace Landdb.Client.Models.Connect
{
    [DataContract]
    public class ConnectApplication : IEquatable<ConnectApplication> 
    {
        [DataMember]
        public string id { get; set; }

        [DataMember]
        public ApplicationId ApplicationId { get; set; }

        [DataMember]
        public string DatasourceId { get; set; }

        [DataMember]
        public int? CropYear { get; set; }

        [DataMember]
        public BlockStatus Status { get; set; }

        [DataMember]
        public DateTime? ImportTimestamp { get; set; }

        [DataMember]
        public string DocumentType { get; set; }

        [DataMember]
        public string SourceId { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public DateTime? DocumentDate { get; set; }

        [DataMember]
        public List<LandUse> Land = new List<LandUse>();

        [DataMember]
        public Dictionary<string, DateTime> TimeScopes = new Dictionary<string, DateTime>();

        [DataMember]
        public List<ProductUse> Products = new List<ProductUse>();

        [DataMember]
        public List<DataAttribute> DataAttributes = new List<DataAttribute>();

        [DataMember]
        public UnitMeasure TotalArea { get; set; }

        [DataMember]
        public string Notes { get; set; }

        [DataMember]
        public List<ArtifactLink> ArtifactLinks = new List<ArtifactLink>();

        [DataMember]
        public double[] BoundingBox = new double[4];


        public bool Equals(ConnectApplication other)
        {
            return id == other?.id;
        }

        [DataContract]
        public class ArtifactLink
        {
            [DataMember]
            public string Name { get; set; }
            [DataMember]
            public string Type { get; set; }
            [DataMember]
            public string URI { get; set; }
        }

        public enum BlockStatus
        {
            Unknown = 0,
            Active = 1,
            Completed = 2,
            Archived = 3,
            Removed = 4,
        }

        [DataContract]
        public class UnitMeasure
        {
            [DataMember]
            public double Value { get; set; }
            [DataMember]
            public string Unit { get; set; }
        }

        [DataContract]
        public abstract class ResourceUse
        {
            [DataMember]
            public string ResourceId { get; set; }
            [DataMember]
            public string ResourceDescription { get; set; }
            [DataMember]
            public Dictionary<string, string> ResourceProperties { get; set; } = new Dictionary<string, string>();
            [DataMember]
            public Guid TrackingId { get; set; } = Guid.NewGuid();
        }

        [DataContract]
        public enum ResourceMatchType
        {
            Unknown,
            System,
            User
        }

        [DataContract]
        public class ResourceMatch
        {
            [DataMember]
            public CompositeId Id { get; set; }
            [DataMember]
            public Guid TrackingId { get; set; } = Guid.NewGuid();
            [DataMember]
            public DateTime MatchedTimestamp { get; set; }
            [DataMember]
            public string CorrelationId { get; set; }
            [DataMember]
            public ResourceMatchType MatchType { get; set; }
        }

        [DataContract]
        public class ProductMatch
        {
            [DataMember]
            public ProductId Id { get; set; }
            [DataMember]
            public Guid TrackingId { get; set; }
            [DataMember]
            public double? ProductDensity { get; set; }

            [DataMember]
            public DateTime MatchedTimestamp { get; set; }
            [DataMember]
            public string CorrelationId { get; set; }
            [DataMember]
            public ResourceMatchType MatchType { get; set; }
            [DataMember]
            public bool IsIgnored { get; set; }
        }

        [DataContract]
        public sealed class CompositeId
        {
            public CompositeId(Guid dataSourceId, Guid id)
            {
                DataSourceId = dataSourceId;
                Id = id;
            }

            [DataMember]
            public Guid DataSourceId { get; private set; }

            [DataMember]
            public Guid Id { get; private set; }
        }

        [DataContract]
        public class LandUse : ResourceUse
        {
            [DataMember]
            public UnitMeasure AreaAllocated { get; set; }
            [DataMember]
            public ResourceMatch Match { get; set; }
            [DataMember]
            public List<LandCandidate> Candidates { get; set; } = new List<LandCandidate>();

        }

        [DataContract]
        public class LandCandidate
        {
            [DataMember]
            public CompositeId Id { get; set; }
            [DataMember]
            public string Description { get; set; }
            [DataMember]
            public int CropYear { get; set; }
            [DataMember]
            public Dictionary<string, string> Properties { get; set; } = new Dictionary<string, string>();
            [DataMember]
            public string Shape { get; set; }

        }

        [DataContract]
        public class PersonUse : ResourceUse
        {
            [DataMember]
            public UnitMeasure AreaAllocated { get; set; }
            [DataMember]
            public ResourceMatch Match { get; set; }
        }


        [DataContract]
        public class ProductUse : ResourceUse
        {
            [DataMember]
            public bool IsCarrier { get; set; }
            [DataMember]
            public bool IsTankMix { get; set; }
            // keys: per-area, total, per-tank, etc.
            [DataMember]
            public Dictionary<string, UnitMeasure> Measurements = new Dictionary<string, UnitMeasure>();
            [DataMember]
            public List<ProductUse> Components = new List<ProductUse>();
            [DataMember]
            public ProductMatch Match { get; set; }
            [DataMember]
            public string SourceId { get; set; }
        }

        [DataContract]
        public class DataAttribute
        {
            [DataMember]
            public string Name { get; set; }
            [DataMember]
            public string Description { get; set; }
            [DataMember]
            public string Unit { get; set; }
        }

    }
}