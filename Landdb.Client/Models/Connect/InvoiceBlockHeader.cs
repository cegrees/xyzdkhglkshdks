﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.Client.Models.Connect {
    [DataContract]
    public class InvoiceBlockHeaderList {
        [DataMember]
        public int TotalInvoiceBlocks { get; set; } // If they ever have more than int.MaxValue pending invoices, that's their own fault...
        [DataMember]
        public List<InvoiceBlockHeader> InvoiceBlocks { get; set; }
        [DataMember]
        public string ContinuationToken { get; set; }
    }
    [DataContract]
    public class InvoiceBlockHeader {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public InvoiceId InvoiceId { get; set; }
        [DataMember]
        public string InvoiceNumber { get; set; }
        [DataMember]
        public DateTime? InvoiceDate { get; set; }
        [DataMember]
        public int? CropYear { get; set; }
        [DataMember]
        public int ProductCount { get; set; }
        [DataMember]
        public decimal TotalCostValue { get; set; }
        [DataMember]
        public string TotalCostCurrency { get; set; }
        [DataMember]
        public string Source { get; set; }
    }
}
