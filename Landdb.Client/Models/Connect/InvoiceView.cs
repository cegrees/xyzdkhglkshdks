﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.Client.Models.Connect {
    [DataContract]
    public class InvoiceView {

        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public InvoiceId InvoiceId { get; set; }
        [DataMember]
        public DateTime? InvoiceDate { get; set; }
        [DataMember]
        public string InvoiceNumber { get; set; }
        [DataMember]
        public List<InvoiceProduct> Products { get; set; }
        [DataMember]
        public string Vendor { get; set; }
        [DataMember]
        public string Notes { get; set; }
        [DataMember]
        public DateTime? InvoiceDueDate { get; set; }
    }

    [DataContract]
    public class InvoiceProduct {
        [DataMember]
        public ProductId ProductId { get; set; }
        [DataMember]
        [JsonProperty("SourceName")]
        public string ProductName { get; set; }
        [DataMember]
        public Guid TrackingId { get; set; }
        [DataMember]
        [JsonProperty("TotalCostValue")]
        public decimal TotalCost { get; set; }
        [DataMember]
        public string TotalCostCurrency { get; set; }
        [DataMember]
        [JsonProperty("TotalQuantityValue")]
        public decimal TotalProductValue { get; set; }
        [DataMember]
        [JsonProperty("TotalQuantityUnit")]
        public string TotalProductUnit { get; set; }
        [DataMember]
        public double? Density { get; set; }
    }
}
