﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.Client.Models {
    [DataContract]
    public class ProductDocument {
        [DataMember]
        public Guid Id { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public string Data { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string Filename { get; set; }
        [DataMember]
        public DateTime LastUpdated { get; set; }
        [DataMember]
        public Guid ProductId { get; set; }
        public string Path { get; set; }
    }
}
