﻿using Landdb.Domain.ReadModels.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.Client.Models
{
    public class ScoutingListItemDetails
    {
        public ScoutingListItemDetails()
        {
        }

        public ScoutingApplicationId DocumentId { get; set; }
        public string Name { get; set; }
        public DateTime? StartDateTime { get; set; }
        public DateTime? EndDateTime { get; set; }
        public int CropYear { get; set; }
        public string DocumentType { get; set; }
        public string Offering { get; set; }
        public string Notes { get; set; }
        public List<ApplicationApplicator> Applicators { get; set; }
        public string Authorization { get; set; }
        public string Centroid { get; set; }
        public ApplicationConditions Conditions { get; set; }
        public List<ObservationSet> ReportedSets { get; set; }
        public List<IncludedCropZone> CropZones { get; set; } //DO SOMETHING ABOUT THIS ONE
        //public List<IncludedProduct> Products { get; set; } //DO SOMETHING ABOUT THIS ONE

    }

    public class ObservationSet
    {
        public string Notes { get; set; }
        public GeoPositionDetails GeoPosition { get; set; }
        public ObservationSetSpec ReportableSetSpec { get; set; }
        public List<ObservationProperty> ReportedProperties { get; set; }
    }

    public class ObservationSetSpec
    {
        public string ReturnToShape { get; set; }
        public string Name { get; set; }
        public Guid ReportableSetId { get; set; }
        public int ReqCntMax { get; set; }
        public int ReqCntMin { get; set; }
        public string ScoutingCategory { get; set; }
    }

    public class ObservationProperty
    {
        public ObservationPropertySpecs ReportablePropertySpec { get; set; }
        public CompositeUnit ReportedUnit { get; set; }
        public string ReportedValue { get; set; }
    }

    public class ObservationPropertySpecs
    {
        public string DataType { get; set; }
        public string Label { get; set; }
        public string Name { get; set; }
        public List<ObservableListItem> ReportableListItems { get; set; }
        public ReportedPest ReportablePestCombo { get; set; }
        public Guid ReportablePropertyId { get; set; }
    }

    public class ReportedPest
    {
        public int? CommonId { get; set; }
        public string CommonName { get; set; }
        public int LatinId { get; set; }
        public string LatinName { get; set; }
        public int PestId { get; set; }
    }

    public class GeoPositionDetails
    {
        public string Accuracy { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public DateTime GeoTimestamp { get; set; }
        public DateTime SysTimestamp { get; set; }
    }

    public class ObservableListItem
    {
        public string Label { get; set; }
        public double Value { get; set; }
    }
}
