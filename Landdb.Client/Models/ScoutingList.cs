﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.Client.Models {
	public class ScoutingList {
		public ScoutingList() {
			Results = new List<ScoutingListItemDetails>();
		}

		public IEnumerable<ScoutingListItemDetails> Results { get; set; }
		public int Revision { get; set; }
		public int TotalCount { get; set; }
	}
}