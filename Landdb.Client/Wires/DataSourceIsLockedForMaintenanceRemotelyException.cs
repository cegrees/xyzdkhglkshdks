﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.Client.Wires {
    [Serializable]
    public class DataSourceIsLockedForMaintenanceRemotelyException : Exception {
        public DataSourceIsLockedForMaintenanceRemotelyException() { }
        public DataSourceIsLockedForMaintenanceRemotelyException(string message) : base(message) { }
        public DataSourceIsLockedForMaintenanceRemotelyException(string message, Exception inner) : base(message, inner) { }
        protected DataSourceIsLockedForMaintenanceRemotelyException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
