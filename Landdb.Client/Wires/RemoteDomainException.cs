﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.Client.Wires {
    [Serializable]
    public class RemoteDomainException : Exception {
        public RemoteDomainException() { }
        public RemoteDomainException(string message) : base(message) { }
        public RemoteDomainException(string message, Exception inner) : base(message, inner) { }
        protected RemoteDomainException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }

        public RemoteDomainExceptionReasons Reason { get; set; }
        public DomainError RemoteError { get; set; }
    }

    public enum RemoteDomainExceptionReasons {
        unknown,
        premature,
        rebirth,
        zombie
    }
}
