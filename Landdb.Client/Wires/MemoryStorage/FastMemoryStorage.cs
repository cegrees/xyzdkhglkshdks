﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lokad.Cqrs;
using Lokad.Cqrs.AtomicStorage;
using Lokad.Cqrs.Partition;

namespace Landdb.Client.Wires.MemoryStorage {
    public static class FastMemoryStorage {
        public static FastMemoryStorageConfig CreateConfig() {
            return new FastMemoryStorageConfig();
        }

        /// <summary>
        /// Creates the simplified nuclear storage wrapper around Atomic storage.
        /// </summary>
        /// <param name="dictionary">The dictionary.</param>
        /// <param name="strategy">The atomic storage strategy.</param>
        /// <returns></returns>
        public static NuclearStorage CreateNuclear(this FastMemoryStorageConfig dictionary, IDocumentStrategy strategy) {
            var container = new FastMemoryDocumentStore(dictionary.Data, strategy);
            return new NuclearStorage(container);
        }


        public static MemoryQueueReader CreateInbox(this FastMemoryStorageConfig storageConfig, params string[] queueNames) {
            var queues = queueNames
                .Select(n => storageConfig.Queues.GetOrAdd(n, s => new BlockingCollection<byte[]>()))
                .ToArray();

            return new MemoryQueueReader(queues, queueNames);
        }

        public static IQueueWriter CreateQueueWriter(this FastMemoryStorageConfig storageConfig, string queueName) {
            var collection = storageConfig.Queues.GetOrAdd(queueName, s => new BlockingCollection<byte[]>());
            return new MemoryQueueWriter(collection, queueName);
        }

        public static MessageSender CreateMessageSender(this FastMemoryStorageConfig storageConfig, IEnvelopeStreamer streamer, string queueName) {
            return new MessageSender(streamer, CreateQueueWriter(storageConfig, queueName));
        }

    }
}
