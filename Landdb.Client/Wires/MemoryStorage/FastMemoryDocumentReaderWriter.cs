﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lokad.Cqrs.AtomicStorage;

namespace Landdb.Client.Wires.MemoryStorage {
    public class FastMemoryDocumentReaderWriter<TKey, TEntity> : IDocumentReader<TKey, TEntity>, IDocumentWriter<TKey, TEntity> {
        readonly IDocumentStrategy _strategy;
        readonly ConcurrentDictionary<string, object> _store;

        public FastMemoryDocumentReaderWriter(IDocumentStrategy strategy, ConcurrentDictionary<string, object> store) {
            _store = store;
            _strategy = strategy;
        }

        string GetName(TKey key) {
            return _strategy.GetEntityLocation<TEntity>(key);
        }

        public bool TryGet(TKey key, out TEntity view) {
            var name = GetName(key);
            object o;
            if (_store.TryGetValue(name, out o)) {
                view = (TEntity)o;
                return true;
            }
            view = default(TEntity);
            return false;
        }

        public TEntity AddOrUpdate(TKey key, Func<TEntity> addFactory, Func<TEntity, TEntity> update, AddOrUpdateHint hint = AddOrUpdateHint.ProbablyExists) {
            var result = default(TEntity);
            _store.AddOrUpdate(GetName(key), s => {
                result = addFactory();
                return result;
            }, (s2, o) => {
                TEntity entity = (TEntity)o;
                result = update(entity);
                return result;
            });
            return result;
        }

        public bool TryDelete(TKey key) {
            object o;
            return _store.TryRemove(GetName(key), out o);
        }
    }
}
