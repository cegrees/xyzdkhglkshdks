﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lokad.Cqrs;

namespace Landdb.Client.Wires.MemoryStorage {
    public sealed class FastMemoryStorageConfig : HideObjectMembersFromIntelliSense {
        public readonly ConcurrentDictionary<string, BlockingCollection<byte[]>> Queues =
            new ConcurrentDictionary<string, BlockingCollection<byte[]>>();

        public readonly ConcurrentDictionary<string, ConcurrentDictionary<string, object>> Data = new ConcurrentDictionary<string, ConcurrentDictionary<string, object>>();

        public readonly ConcurrentDictionary<string, List<object>> Tapes =
            new ConcurrentDictionary<string, List<object>>();
    }
}
