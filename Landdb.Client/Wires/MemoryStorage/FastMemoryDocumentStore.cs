﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Lokad.Cqrs.AtomicStorage;

namespace Landdb.Client.Wires.MemoryStorage {
    public sealed class FastMemoryDocumentStore : IDocumentStore {
        ConcurrentDictionary<string, ConcurrentDictionary<string, object>> _store;
        readonly IDocumentStrategy _strategy;

        public FastMemoryDocumentStore(ConcurrentDictionary<string, ConcurrentDictionary<string, object>> store, IDocumentStrategy strategy) {
            _store = store;
            _strategy = strategy;
        }

        public IDocumentWriter<TKey, TEntity> GetWriter<TKey, TEntity>() {
            var bucket = _strategy.GetEntityBucket<TEntity>();
            var store = _store.GetOrAdd(bucket, s => new ConcurrentDictionary<string, object>());
            return new FastMemoryDocumentReaderWriter<TKey, TEntity>(_strategy, store);
        }

        public IDocumentReader<TKey, TEntity> GetReader<TKey, TEntity>() {
            var bucket = _strategy.GetEntityBucket<TEntity>();
            var store = _store.GetOrAdd(bucket, s => new ConcurrentDictionary<string, object>());
            return new FastMemoryDocumentReaderWriter<TKey, TEntity>(_strategy, store);
        }

        public IDocumentStrategy Strategy {
            get { return _strategy; }
        }

        public IEnumerable<DocumentRecord> EnumerateContents(string bucket) {
            var store = _store.GetOrAdd(bucket, s => new ConcurrentDictionary<string, object>());
            
                return store.Select(p => new DocumentRecord(p.Key, () => {
                    using (var memory = new MemoryStream()) {
                        _strategy.Serialize(p.Value, memory);
                        return memory.ToArray();
                    }
                }));
        }

        public void WriteContents(string bucket, IEnumerable<DocumentRecord> records) {
            throw new NotImplementedException();
        }

        public void ResetAll() {
            foreach (var bucket in _store.Keys) {
                Reset(bucket);
            }
        }

        public void Reset(string bucket) {
            _store[bucket].Clear();
        }
    }
}
