﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Landdb.Client.Account;
using Landdb.Client.Services.Web;
using Lokad.Cqrs;
using Lokad.Cqrs.Dispatch;
using Lokad.Cqrs.Envelope;
using Lokad.Cqrs.Partition;
using Newtonsoft.Json;
using Landdb.Web.ServiceContracts.Data;
using System.Threading.Tasks;
using Landdb.Client.Infrastructure;
using Lokad.Cqrs.StreamingStorage;
using NLog;

namespace Landdb.Client.Wires {
    public sealed class RemoteSubmissionPublisher {
        readonly IQueueReader remoteQueueInbox;
        readonly AccountStatus accountStatus;
        readonly Guid deviceId;
        readonly EnvelopeStreamer streamer;
        readonly DuplicationManager duplication;
        readonly Func<IDataSynchronizationClient> ResolveRemoteSubmissionClient;
        readonly IStreamContainer quarantineContainer;
        readonly LocalPendingCommands pendingCommands;
        readonly Logger log = LogManager.GetCurrentClassLogger();
        readonly IRemoteSubmissionErrorNotifier remoteErrorNotifier;

        public RemoteSubmissionPublisher(IQueueReader remoteQueueInbox, EnvelopeStreamer streamer, DuplicationManager duplication, Func<IDataSynchronizationClient> ResolveRemoteSubmissionClient, AccountStatus accountStatus, Guid deviceId, IStreamContainer quarantineContainer, LocalPendingCommands pendingCommands, IRemoteSubmissionErrorNotifier remoteErrorNotifier) {
            this.remoteQueueInbox = remoteQueueInbox;
            this.streamer = streamer;
            this.duplication = duplication;
            this.ResolveRemoteSubmissionClient = ResolveRemoteSubmissionClient;
            this.accountStatus = accountStatus;
            this.deviceId = deviceId;
            this.quarantineContainer = quarantineContainer;
            this.pendingCommands = pendingCommands;
            this.remoteErrorNotifier = remoteErrorNotifier;
        }

        public void Run(CancellationToken token) {
            remoteQueueInbox.InitIfNeeded();
            bool isConnected = !string.IsNullOrWhiteSpace(accountStatus.AuthorizationToken);

            accountStatus.PropertyChanged += (s, e) => {
                if (e.PropertyName == "AuthorizationToken") {
                    isConnected = !string.IsNullOrWhiteSpace(accountStatus.AuthorizationToken);
                }
            };

            while (!token.IsCancellationRequested) {
                if (isConnected) {
                    var client = ResolveRemoteSubmissionClient();
                    //var dispatcher = new EnvelopeDispatcher(env => SubmitEvents(client, env), streamer, new RemoteSubmissionQuarantine(), duplication, "remoteDispatch");
                    
                    //var dispatcher = new EnvelopeDispatcher(env => SubmitCommand(client, env), streamer, new RemoteSubmissionQuarantine(), duplication, "remoteDispatch");
                    //var dispatchProcess = new DispatcherProcess(dispatcher.Dispatch, remoteQueueInbox);

                    var dispatcher = new AwaitableEnvelopeDispatcher(env => SubmitCommand(client, env), streamer, new RemoteSubmissionQuarantine(streamer, quarantineContainer, pendingCommands, remoteErrorNotifier), duplication, "remoteDispatch");
                    var dispatchProcess = new AwaitableDispatcherProcess(dispatcher.Dispatch, remoteQueueInbox);

                    dispatchProcess.Start(token);
                    break;
                } else {
                    Thread.Sleep(5000); // poll every 5 seconds for submission
                }
            }
        }

        private async Task SubmitCommand(IDataSynchronizationClient client, ImmutableEnvelope env) {
            var cmd = env.Message as IDomainCommand<AbstractDataSourceIdentity<Guid, Guid>>;

            var dataSourceId = cmd.Id.DataSourceId;

            var settings = new JsonSerializerSettings();
            settings.TypeNameHandling = TypeNameHandling.Auto;
            var serializedCommand = JsonConvert.SerializeObject(cmd, settings);
            var cmdType = cmd.GetType().FullName;
            var submissionResult = await client.SubmitCommand(serializedCommand, cmdType, deviceId, dataSourceId.ToString());

            if (!submissionResult.CommandWasSuccessful) {
                switch (submissionResult.CommandSubmissionFailureReason) {
                    case CommandSubmissionFailureReasons.CloudIsOffline:
                        throw new CloudIsOfflineForMaintenanceException();
                    case CommandSubmissionFailureReasons.DatasourceWasLocked:
                        throw new DataSourceIsLockedForMaintenanceRemotelyException();
                    case CommandSubmissionFailureReasons.AuthenticationFailed:
                    case CommandSubmissionFailureReasons.InvalidRequest:
                        throw new CloudIsUnreachableException(string.Format("Command wasn't processed remotely. Reason: {0}", submissionResult.CommandSubmissionFailureReason));
                    
                    case CommandSubmissionFailureReasons.AggregatePremature:
                        throw new RemoteDomainException() { Reason = RemoteDomainExceptionReasons.premature, RemoteError = submissionResult.DomainError };
                    case CommandSubmissionFailureReasons.AggregateRebirth:
                        throw new RemoteDomainException() { Reason = RemoteDomainExceptionReasons.rebirth, RemoteError = submissionResult.DomainError };
                    case CommandSubmissionFailureReasons.AggregateZombie:
                        throw new RemoteDomainException() { Reason = RemoteDomainExceptionReasons.zombie, RemoteError = submissionResult.DomainError };
                    case CommandSubmissionFailureReasons.AggregateDomainException:
                        throw new RemoteDomainException() { Reason = RemoteDomainExceptionReasons.unknown, RemoteError = submissionResult.DomainError };

                    case CommandSubmissionFailureReasons.None:
                    default:
                        throw new ApplicationException("Server reported that command was not received, but no valid reason was given.");
                }
            }
        }
    }
}
