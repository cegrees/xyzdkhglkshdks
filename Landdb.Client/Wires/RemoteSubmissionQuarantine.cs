﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lokad.Cqrs.Envelope;
using System.Collections.Concurrent;
using Lokad.Cqrs;
using System.ServiceModel;
using System.Threading;
using NLog;
using Lokad.Cqrs.StreamingStorage;
using Newtonsoft.Json;
using System.IO;
using System.Globalization;
using Landdb.Client.Infrastructure;

namespace Landdb.Client.Wires {
    public class RemoteSubmissionQuarantine : IEnvelopeQuarantine {
        readonly IStreamContainer _root;
        readonly ConcurrentDictionary<string, int> _failures = new ConcurrentDictionary<string, int>();
        readonly Logger log = LogManager.GetCurrentClassLogger();
        readonly EnvelopeStreamer streamer;
        readonly LocalPendingCommands pendingCommands;
        readonly IRemoteSubmissionErrorNotifier errorNotifier;

        public RemoteSubmissionQuarantine(EnvelopeStreamer streamer, IStreamContainer root, LocalPendingCommands pendingCommands, IRemoteSubmissionErrorNotifier errorNotifier) {
            this.streamer = streamer;
            this._root = root;
            _root.Create();
            this.pendingCommands = pendingCommands;
            this.errorNotifier = errorNotifier;
        }

        public bool TryToQuarantine(ImmutableEnvelope envelope, Exception ex) {
            int maxRetries = 4;

            // serialization problem
            if (envelope == null)
                return true;

            log.WarnException("Trying to quarantine a message.", ex);

            if (ex is DataSourceIsLockedForMaintenanceRemotelyException) {
                Thread.Sleep(45 * 1000); // Sleep for 45 seconds before trying again. This issue should resolve itself eventually.
                return false;
            }

            if (ex is CloudIsOfflineForMaintenanceException) {
                Thread.Sleep(45 * 1000); // Sleep for 45 seconds before trying again. It'll come back.
                return false;
            }

            if (ex is CloudIsUnreachableException) {
                Thread.Sleep(60 * 1000); // Sleep for 60 seconds before trying again. It'll come back.
                return false;
            }

            if (ex is RemoteDomainException) {
                switch (((RemoteDomainException)ex).Reason) {
                    case RemoteDomainExceptionReasons.premature:
                        Thread.Sleep(5 * 1000); // Sleep 5 seconds on premature. Hopefully it will be there when we try again. If not, we'll eventually quarantine.
                        break;
                    case RemoteDomainExceptionReasons.rebirth: // Go ahead and quarantine on rebirth/zombie. There's no recovery for these at the moment.
                        log.Warn("Tried to create an already existing aggregate. Quarantining...");
                        SoftQuarantine(envelope, ex);
                        return true;
                    case RemoteDomainExceptionReasons.zombie:
                        log.Warn("Tried to issue a command on a deleted aggregate. Quarantining...");
                        SoftQuarantine(envelope, ex);
                        return true;
                    case RemoteDomainExceptionReasons.unknown:
                    default:
                        // If unknown, we'll briefly sleep, & default to standard quarantine logic below
                        Thread.Sleep(500);
                        break;
                }
            }

            if (ex != null) {
                var current = _failures.AddOrUpdate(envelope.EnvelopeId, s => 1, (s1, i) => i + 1);
                if (current < maxRetries) {
                    return false;
                }
                var quarantineMessage = string.Format("A message has encountered {0} submission exceptions. Quarantining...", current);
                log.Warn(quarantineMessage);
                SoftQuarantine(envelope, ex);

                // accept and forget
                int forget;
                _failures.TryRemove(envelope.EnvelopeId, out forget);
                return true;
            }

            
            return false;
        }

        void SoftQuarantine(ImmutableEnvelope envelope, Exception ex) {
            try {
                var cmd = envelope.Message as IDomainCommand<AbstractDataSourceIdentity<Guid, Guid>>;
                if (cmd != null) {
                    pendingCommands.RemoveCommandById(cmd.Id, cmd.Metadata.MessageId);

                    errorNotifier.AddNewError(cmd);
                }

                // Write exception text to a file
                var file = string.Format("{0:yyyy-MM-dd}-{1}-engine.txt",
                    DateTime.UtcNow,
                    envelope.EnvelopeId.ToLowerInvariant());

                var data = "";
                if (_root.Exists(file)) {
                    using (var stream = _root.OpenRead(file))
                    using (var reader = new StreamReader(stream)) {
                        data = reader.ReadToEnd();
                    }
                }

                var builder = new StringBuilder(data);
                if (builder.Length == 0) {
                    DescribeMessage(builder, envelope);
                }

                builder.AppendLine("[Exception]");
                builder.AppendLine(DateTime.UtcNow.ToString(CultureInfo.InvariantCulture));
                builder.AppendLine(ex.ToString());
                builder.AppendLine();

                var text = builder.ToString();

                using (var stream = _root.OpenWrite(file))
                using (var writer = new StreamWriter(stream)) {
                    writer.Write(text);
                }

            } catch (Exception qEx) {
                log.Error("Caught an exception while trying to quarantine a message", qEx);
            }
        }

        public void Quarantine(byte[] message, Exception ex) {
            // yum, yum
        }

        public void TryRelease(Lokad.Cqrs.ImmutableEnvelope context) {
            if (null != context) {
                int value;
                _failures.TryRemove(context.EnvelopeId, out value);
            }
        }

        static void DescribeMessage(StringBuilder builder, ImmutableEnvelope context) {
            builder.AppendLine(context.PrintToString(o => JsonConvert.SerializeObject(o)));
        }
    }
}