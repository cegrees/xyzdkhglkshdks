﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lokad.Cqrs;

namespace Landdb.Client.Wires {
    public sealed class TypedMessageSender : ICommandSender {
        readonly MessageSender _commandRouter;
        readonly MessageSender _functionalRecorder;


        public TypedMessageSender(MessageSender commandRouter, MessageSender functionalRecorder) {
            _commandRouter = commandRouter;
            _functionalRecorder = functionalRecorder;
        }

        public void SendCommand(IDomainCommand commands, bool idFromContent = false) {
            if (idFromContent) {
                _commandRouter.SendHashed(commands);
            } else {
                _commandRouter.Send(commands);
            }
        }

        public void SendFromClient(IDomainCommand e, string id, MessageAttribute[] attributes) {
            _commandRouter.Send(e, id, attributes);
        }

        public void PublishFromClientHashed(IFunctionalEvent e, MessageAttribute[] attributes) {
            _functionalRecorder.SendHashed(e, attributes);
        }

        public void Publish(IFunctionalEvent @event) {
            _functionalRecorder.Send(@event);
        }

        public void Send(IFunctionalCommand command) {
            _commandRouter.Send(command);
        }

        public void SendHashed(IFunctionalCommand command) {
            _commandRouter.SendHashed(command);
        }


    }
}