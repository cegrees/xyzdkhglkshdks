﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.Client.Wires {
    [Serializable]
    public class CloudIsOfflineForMaintenanceException : Exception {
        public CloudIsOfflineForMaintenanceException() { }
        public CloudIsOfflineForMaintenanceException(string message) : base(message) { }
        public CloudIsOfflineForMaintenanceException(string message, Exception inner) : base(message, inner) { }
        protected CloudIsOfflineForMaintenanceException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
