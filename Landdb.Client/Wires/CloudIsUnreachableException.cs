﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.Client.Wires {
    [Serializable]
    public class CloudIsUnreachableException : Exception {
        public CloudIsUnreachableException() { }
        public CloudIsUnreachableException(string message) : base(message) { }
        public CloudIsUnreachableException(string message, Exception inner) : base(message, inner) { }
        protected CloudIsUnreachableException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
