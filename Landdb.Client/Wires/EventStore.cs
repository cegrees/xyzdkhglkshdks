﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Lokad.Cqrs;
using Lokad.Cqrs.TapeStorage;

namespace Landdb.Wires {
    public sealed class EventStore : IClientEventStore {
        readonly MessageStore _store;
        MessageSender publisher;
        Guid deviceToIgnore;

        public EventStore(MessageStore store, MessageSender publisher, Guid deviceToIgnore) {
            _store = store;
            this.publisher = publisher;
            this.deviceToIgnore = deviceToIgnore;
        }

        public void AppendEventsToStream(IIdentity id, long originalVersion, ICollection<IDomainEvent> events, string deviceId, string dataSourceId, bool shouldPublish = true) {
            if (events.Count == 0) return;
            // functional events don't have an identity
            var name = IdentityToKey(id);

            List<MessageAttribute> messageAttributes = new List<MessageAttribute>();

            if (!string.IsNullOrEmpty(deviceId)) {
                messageAttributes.Add(new MessageAttribute("deviceId", deviceId));
            }

            if (!string.IsNullOrEmpty(dataSourceId)) {
                messageAttributes.Add(new MessageAttribute("dataSourceId", dataSourceId));
            }

            
            try {
                _store.AppendToStore(name, messageAttributes, originalVersion, events.Cast<object>().ToArray());

                if (publisher != null && shouldPublish) { // Guid.Parse(deviceId) != deviceToIgnore) {
                    List<MessageAttribute> globalMessageAttributes = new List<MessageAttribute>();

                    if (!string.IsNullOrEmpty(deviceId)) { globalMessageAttributes.Add(new MessageAttribute("deviceId", deviceId)); }
                    if (!string.IsNullOrEmpty(dataSourceId)) { globalMessageAttributes.Add(new MessageAttribute("dataSourceId", dataSourceId)); }

                    foreach (var e in events) {
                        publisher.Send(e, globalMessageAttributes.ToArray());
                    }
                }

            } catch (AppendOnlyStoreConcurrencyException e) {
                // load server events
                var server = LoadEventStream(id);
                // throw a real problem
                throw OptimisticConcurrencyException.Create(server.StreamVersion, e.ExpectedStreamVersion, id, server.Events);
            }
        }

        static string IdentityToKey(IIdentity id) {
            return id == null ? "func" : (id.GetTag() + ":" + id.GetId());
        }

        public EventStream LoadEventStream(IIdentity id) {
            var key = IdentityToKey(id);

            // TODO: make this lazy somehow?
            var stream = new EventStream();
            foreach (var record in _store.EnumerateMessages(key, 0, int.MaxValue)) {
                stream.Events.AddRange(record.Items.Cast<IDomainEvent>());
                stream.StreamVersion = record.StreamVersion;
            }
            return stream;
        }

        public EventStream EnumerateAllEvents() {
            var stream = new EventStream();
            foreach (var record in _store.EnumerateAllItems(0, int.MaxValue)) {
                stream.Events.AddRange(from r in record.Items where r is IDomainEvent select (IDomainEvent)r);
            }
            stream.StreamVersion = stream.Events.Count;
            return stream;
        }

        public void Close() {
            _store.CloseStore();
        }
    }

    public sealed class HybridEventStore : IClientEventStore, IMalleableEventStore {
        IClientEventStore persistedStore;
        MessageSender publisher;
        ConcurrentDictionary<IIdentity, ICollection<IDomainEvent>> localStore;

        public HybridEventStore(IClientEventStore persistedStore, MessageSender publisher) {
            this.persistedStore = persistedStore;
            localStore = new ConcurrentDictionary<IIdentity,ICollection<IDomainEvent>>();
            this.publisher = publisher;
        }

        public EventStream LoadEventStream(IIdentity id) {
            var stream = persistedStore.LoadEventStream(id);

            if(localStore.ContainsKey(id)) {
                foreach (var e in localStore[id]) {
                    // TODO: de-duplication
                    stream.Events.Add(e);
                    stream.StreamVersion++;
                }
            }

            return stream;
        }

        public void AppendEventsToStream(IIdentity id, long originalVersion, ICollection<IDomainEvent> events, string deviceId, string dataSourceId, bool shouldPublish = true) {
            localStore.AddOrUpdate(id, new List<IDomainEvent>(events), (key, val) => {
                foreach (var e in events) { val.Add(e); }
                return val;
            });


            if (publisher != null) {
                List<MessageAttribute> globalMessageAttributes = new List<MessageAttribute>();

                if (!string.IsNullOrEmpty(deviceId)) { globalMessageAttributes.Add(new MessageAttribute("deviceId", deviceId)); }
                if (!string.IsNullOrEmpty(dataSourceId)) { globalMessageAttributes.Add(new MessageAttribute("dataSourceId", dataSourceId)); }

                foreach (var e in events) {
                    publisher.Send(e, globalMessageAttributes.ToArray());
                }
            }
        }

        public EventStream EnumerateAllEvents() {
            // This could be inefficient for large local stores...
            var stream = persistedStore.EnumerateAllEvents();
            var q = from ec in localStore.Values
                    from e in ec
                    orderby e.Metadata.Timestamp
                    select e;
            foreach (var e in q) {
                stream.Events.Add(e);
            }
            stream.StreamVersion = stream.Events.Count;
            return stream;
        }

        public void RemoveEventsSourcedFromCommand(IIdentity id, Guid commandId) {
            // Hopefully this works....

            var q = from x in localStore.GetOrAdd(id, new List<IDomainEvent>())
                    where x.Metadata.SourceMessageId == commandId
                    select x;

            var modified = localStore[id];
            foreach(var e in q.ToList()) {
                modified.Remove(e);
            }

            localStore.TryUpdate(id, modified, localStore[id]);
        }

        public void Close() {
            if (localStore is IClientEventStore) {
                ((IClientEventStore)localStore).Close();
            }
        }
    }
}