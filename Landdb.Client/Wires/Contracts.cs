﻿#region (c) 2010-2012 Lokad - CQRS- New BSD License

// Copyright (c) Lokad 2010-2012, http://www.lokad.com
// This code is released as Open Source under the terms of the New BSD Licence

#endregion

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Lokad.Cqrs;
using Lokad.Cqrs.AtomicStorage;
using Lokad.Cqrs.Envelope;
using Lokad.Cqrs.Evil;
using ServiceStack.Text;
using ProtoBuf.Meta;
using ProtoBuf;
//using Newtonsoft.Json;

namespace Landdb.Wires {
    public static class Contracts {
        static Type[] LoadMessageContracts() {
            var messages = new[] { typeof(FarmCreated) }
                .SelectMany(t => t.Assembly.GetExportedTypes())
                .Where(t => typeof(IMessage).IsAssignableFrom(t))
                .Where(t => !t.IsAbstract)
                .ToArray();
            return messages;

        }

        public static EnvelopeStreamer CreateStreamer() {
            return new EnvelopeStreamer(new DataSerializer(LoadMessageContracts()));
        }

        public static EnvelopeStreamer CreateExperimentalBinaryStreamer() {
            return new EnvelopeStreamer(new BinaryDataSerializer(LoadMessageContracts()));
        }

        [ProtoContract]
        class ProtoDocumentDescriptor {
            [ProtoMember(1)]
            public string DocumentType;
            [ProtoMember(2)]
            public string IdentityType;
            [ProtoMember(3)]
            public Guid DatasourceId;
            [ProtoMember(4)]
            public Guid DocumentId;
            [ProtoMember(5)]
            public string Location;
            [ProtoMember(6)]
            public string Name;

            public static implicit operator DocumentDescriptor(ProtoDocumentDescriptor d) {
                if (d == null) { return null; }
                
                var identity = typeof(DataSourceId).Assembly.GetType(d.IdentityType).GetConstructor(new Type[] { typeof(Guid), typeof(Guid) }).Invoke(new object[] { d.DatasourceId, d.DocumentId }) as AbstractDataSourceIdentity<Guid,Guid>;
                return new DocumentDescriptor() {
                    DocumentType = d.DocumentType,
                    Identity = identity,
                    Location = d.Location,
                    Name = d.Name
                };
            }

            public static implicit operator ProtoDocumentDescriptor(DocumentDescriptor d) {
                return d == null ? null : new ProtoDocumentDescriptor {
                    DocumentType = d.DocumentType,
                    IdentityType = d.Identity.GetType().FullName,
                    DatasourceId = d.Identity.DataSourceId,
                    DocumentId = d.Identity.Id,
                    Location = d.Location,
                    Name = d.Name
                };
            }
        }

        class BinaryDataSerializer : AbstractMessageSerializer {
            public BinaryDataSerializer(ICollection<Type> knownTypes)
                : base(knownTypes) {
                RuntimeTypeModel.Default[typeof(DateTimeOffset)].Add("m_dateTime", "m_offsetMinutes");
                RuntimeTypeModel.Default.Add(typeof(DocumentDescriptor), false).SetSurrogate(typeof(ProtoDocumentDescriptor));
            }

            protected override Formatter PrepareFormatter(Type type) {
                var name = ContractEvil.GetContractReference(type);
                var formatter = RuntimeTypeModel.Default.CreateFormatter(type);
                return new Formatter(name, type, formatter.Deserialize, (o, stream) => formatter.Serialize(stream, o));
            }
        }

        class DataSerializer : AbstractMessageSerializer {
            public DataSerializer(ICollection<Type> knownTypes)
                : base(knownTypes) {
                RuntimeTypeModel.Default[typeof(DateTimeOffset)].Add("m_dateTime", "m_offsetMinutes");
                //settings.Converters.Add(new Landdb.Converters.DocumentDescriptorConverter());
            }
            //JsonSerializerSettings settings = new JsonSerializerSettings() { TypeNameHandling = Newtonsoft.Json.TypeNameHandling.All };

            protected override Formatter PrepareFormatter(Type type) {
                var name = ContractEvil.GetContractReference(type);
                return new Formatter(name, type, s => JsonSerializer.DeserializeFromStream(type, s), (o, s) => {
                    using (var writer = new StreamWriter(s)) {
                        writer.WriteLine();
                        // Losing the JsvFormatter for now. It's the thing that's been adding those /r/n/ts to our WKTs. -bs
                        //writer.WriteLine(JsvFormatter.Format(JsonSerializer.SerializeToString(o, type)));
                        writer.WriteLine(JsonSerializer.SerializeToString(o, type));
                    }

                });

                //return new Formatter(name, type,
                //    s => {
                //        using (var sr = new StreamReader(s)) {
                //            var b = sr.ReadToEnd();
                //            return JsonConvert.DeserializeObject(b, type, settings);
                //        }
                //    },
                //    (o, s) => {
                //        using (var writer = new StreamWriter(s)) {
                //            writer.WriteLine();
                //            // Losing the JsvFormatter for now. It's the thing that's been adding those /r/n/ts to our WKTs. -bs
                //            //writer.WriteLine(JsvFormatter.Format(JsonSerializer.SerializeToString(o, type)));
                //            writer.WriteLine(JsonConvert.SerializeObject(o, settings));
                //        }

                //    });
            }
        }
    }
}