﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.Client.Resources.LocalizedLists
{
    class SustainabilityLists
    {
        public SustainabilityLists(string currentUiCulture) {
            CultureName = currentUiCulture;
        }

        //TODO: make them into arrays to speed up loading...?

        public string CultureName { get; set; }

        private readonly List<SoilTexture> en_US_SoilTextureList = new List<SoilTexture> {
            new SoilTexture{Name=Strings.SoilTexture_Clay},
            new SoilTexture{Name=Strings.SoilTexture_Silt},
            new SoilTexture{Name=Strings.SoilTexture_Sand},
            new SoilTexture{Name=Strings.SoilTexture_Loam},
            new SoilTexture{Name=Strings.SoilTexture_SandyClay},
            new SoilTexture{Name=Strings.SoilTexture_SiltyClay},
            new SoilTexture{Name=Strings.SoilTexture_SandyClayLoam},
            new SoilTexture{Name=Strings.SoilTexture_SiltyClayLoam},
            new SoilTexture{Name=Strings.SoilTexture_ClayLoam},
            new SoilTexture{Name=Strings.SoilTexture_SiltLoam},
            new SoilTexture{Name=Strings.SoilTexture_SandyLoam},
            new SoilTexture{Name=Strings.SoilTexture_LoamySand}
        };
        private readonly List<SoilTexture> ru_SoilTextureList = new List<SoilTexture> {
            new SoilTexture{Name=Strings.SoilTexture_Clay},
            new SoilTexture{Name=Strings.SoilTexture_Silt},
            new SoilTexture{Name=Strings.SoilTexture_Sand},
            new SoilTexture{Name=Strings.SoilTexture_Loam},
            new SoilTexture{Name=Strings.SoilTexture_SandyClay},
            new SoilTexture{Name=Strings.SoilTexture_SiltyClay},
            new SoilTexture{Name=Strings.SoilTexture_SandyClayLoam},
            new SoilTexture{Name=Strings.SoilTexture_SiltyClayLoam},
            new SoilTexture{Name=Strings.SoilTexture_ClayLoam},
            new SoilTexture{Name=Strings.SoilTexture_SiltLoam},
            new SoilTexture{Name=Strings.SoilTexture_SandyLoam},
            new SoilTexture{Name=Strings.SoilTexture_LoamySand}
        };
        private readonly List<SoilTexture> pt_BR_SoilTextureList = new List<SoilTexture> {
            new SoilTexture{Name=Strings.SoilTexture_Clay},
            new SoilTexture{Name=Strings.SoilTexture_Silt},
            new SoilTexture{Name=Strings.SoilTexture_Sand},
            new SoilTexture{Name=Strings.SoilTexture_Loam},
            new SoilTexture{Name=Strings.SoilTexture_SandyClay},
            new SoilTexture{Name=Strings.SoilTexture_SiltyClay},
            new SoilTexture{Name=Strings.SoilTexture_SandyClayLoam},
            new SoilTexture{Name=Strings.SoilTexture_SiltyClayLoam},
            new SoilTexture{Name=Strings.SoilTexture_ClayLoam},
            new SoilTexture{Name=Strings.SoilTexture_SiltLoam},
            new SoilTexture{Name=Strings.SoilTexture_SandyLoam},
            new SoilTexture{Name=Strings.SoilTexture_LoamySand}
        };
        private readonly List<SoilTexture> uk_SoilTextureList = new List<SoilTexture> {
            new SoilTexture{Name=Strings.SoilTexture_Clay},
            new SoilTexture{Name=Strings.SoilTexture_Silt},
            new SoilTexture{Name=Strings.SoilTexture_Sand},
            new SoilTexture{Name=Strings.SoilTexture_Loam},
            new SoilTexture{Name=Strings.SoilTexture_SandyClay},
            new SoilTexture{Name=Strings.SoilTexture_SiltyClay},
            new SoilTexture{Name=Strings.SoilTexture_SandyClayLoam},
            new SoilTexture{Name=Strings.SoilTexture_SiltyClayLoam},
            new SoilTexture{Name=Strings.SoilTexture_ClayLoam},
            new SoilTexture{Name=Strings.SoilTexture_SiltLoam},
            new SoilTexture{Name=Strings.SoilTexture_SandyLoam},
            new SoilTexture{Name=Strings.SoilTexture_LoamySand}
        };


        private readonly List<WaterOrigin> en_US_WaterOriginList = new List<WaterOrigin> {
            new WaterOrigin{Name=Strings.WaterOrigins_Aquifer},
            new WaterOrigin{Name=Strings.WaterOrigins_Watershed},
        };
        private readonly List<WaterOrigin> ru_WaterOriginList = new List<WaterOrigin>();
        private readonly List<WaterOrigin> pt_BR_WaterOriginList = new List<WaterOrigin>();
        private readonly List<WaterOrigin> uk_WaterOriginList = new List<WaterOrigin>();

        private readonly List<IrrigationSystem> en_US_IrrigationSystemList = new List<IrrigationSystem> {
            new IrrigationSystem{Name=Strings.IrrigationSystems_Drip},
            new IrrigationSystem{Name=Strings.IrrigationSystems_Flood},
            new IrrigationSystem{Name=Strings.IrrigationSystems_Furrow},
            new IrrigationSystem{Name=Strings.IrrigationSystems_Linear},
            new IrrigationSystem{Name=Strings.IrrigationSystems_Microsprinkler},
            new IrrigationSystem{Name=Strings.IrrigationSystems_Pivot},
            new IrrigationSystem{Name=Strings.IrrigationSystems_Subsurface},
            new IrrigationSystem{Name=Strings.IrrigationSystems_TravelingGun},
            new IrrigationSystem{Name=Strings.IrrigationSystems_WheelLines}
        };
        private readonly List<IrrigationSystem> ru_IrrigationSystemList = new List<IrrigationSystem>();
        private readonly List<IrrigationSystem> pt_BR_IrrigationSystemList = new List<IrrigationSystem>();
        private readonly List<IrrigationSystem> uk_IrrigationSystemList = new List<IrrigationSystem>();


        private readonly List<TillageSystem> en_US_TillageSystemList = new List<TillageSystem> {
            new TillageSystem{Name=Strings.TillageSystems_ConventionalTill},
            new TillageSystem{Name=Strings.TillageSystems_IntensiveTill},
            new TillageSystem{Name=Strings.TillageSystems_MulchTill},
            new TillageSystem{Name=Strings.TillageSystems_NoTill},
            new TillageSystem{Name=Strings.TillageSystems_ReducedTill},
            new TillageSystem{Name=Strings.TillageSystems_StripTill}
        };
        private readonly List<TillageSystem> ru_TillageSystemList = new List<TillageSystem>();
        private readonly List<TillageSystem> pt_BR_TillageSystemList = new List<TillageSystem>();
        private readonly List<TillageSystem> uk_TillageSystemList = new List<TillageSystem>();


        private readonly List<WaterSource> en_US_WaterSourceList = new List<WaterSource> {
            new WaterSource{Name=Strings.WaterSources_IrrigationDistrict},
            new WaterSource{Name=Strings.WaterSources_Municipal},
            new WaterSource{Name=Strings.WaterSources_OnFarmPond},
            new WaterSource{Name=Strings.WaterSources_Reservoir},
            new WaterSource{Name=Strings.WaterSources_River},
            new WaterSource{Name=Strings.WaterSources_Stream},
            new WaterSource{Name=Strings.WaterSources_Tailwater},
            new WaterSource{Name=Strings.WaterSources_Well}
        };
        private readonly List<WaterSource> ru_WaterSourceList = new List<WaterSource>();
        private readonly List<WaterSource> pt_BR_WaterSourceList = new List<WaterSource>();
        private readonly List<WaterSource> uk_WaterSourceList = new List<WaterSource>();

        private static readonly Func<List<ApplicationMethod>> SortedApplicationMethods = () => {
            var applicationList = new List<ApplicationMethod> {
                new ApplicationMethod{Name=Strings.ApplicationMethods_Aerial},
                new ApplicationMethod{Name=Strings.ApplicationMethods_AirBlast},
                new ApplicationMethod{Name=Strings.ApplicationMethods_Banded},
                new ApplicationMethod{Name=Strings.ApplicationMethods_Broadcast},
                new ApplicationMethod{Name=Strings.ApplicationMethods_Chemigation},
                new ApplicationMethod{Name=Strings.ApplicationMethods_Fertigation},
                new ApplicationMethod{Name=Strings.ApplicationMethods_Fumigation},
                new ApplicationMethod{Name=Strings.ApplicationMethods_Ground},
                new ApplicationMethod{Name=Strings.ApplicationMethods_GroundApplication},
                new ApplicationMethod{Name=Strings.ApplicationMethods_Hooded},
                new ApplicationMethod{Name=Strings.ApplicationMethods_Incorporated},
                new ApplicationMethod{Name=Strings.ApplicationMethods_InFurrow},
                new ApplicationMethod{Name=Strings.ApplicationMethods_Planting},
                new ApplicationMethod{Name=Strings.ApplicationMethods_RePlanting},
                new ApplicationMethod{Name=Strings.ApplicationMethods_SeedTreatment},
                new ApplicationMethod{Name=Strings.ApplicationMethods_Sidedress},
                new ApplicationMethod{Name=Strings.ApplicationMethods_Storage},
                new ApplicationMethod{Name=Strings.ApplicationMethods_Topdress},
                new ApplicationMethod{Name=Strings.ApplicationMethods_TreeInjection},
                new ApplicationMethod{Name=Strings.ApplicationMethods_VariableRate},
                new ApplicationMethod{Name=Strings.ApplicationMethods_WaterRun}
            };

            bool ComesBefore(ApplicationMethod toCompare, ApplicationMethod comparingWith) => String.Compare(toCompare.Name, comparingWith.Name, StringComparison.OrdinalIgnoreCase) > 0;

            var methodArray = applicationList.ToArray();

            for (int i = 0; i < methodArray.Length; i++) {
                for (int j = i + 1; j < methodArray.Length; j++) {
                    if (ComesBefore(methodArray[i], methodArray[j])) {
                        var temp = methodArray[i];
                        methodArray[i] = methodArray[j];
                        methodArray[j] = temp;
                    }
                }
            }

            return methodArray.ToList();
        };

        private readonly List<ApplicationMethod> en_US_ApplicationMethodList = SortedApplicationMethods.Invoke();
        private readonly List<ApplicationMethod> ru_ApplicationMethodList = new List<ApplicationMethod>();
        private readonly List<ApplicationMethod> pt_BR_ApplicationMethodList = new List<ApplicationMethod>();
        private readonly List<ApplicationMethod> uk_ApplicationMethodList = new List<ApplicationMethod>();

        private readonly List<TimingEvent> en_US_TimingEventList = new List<TimingEvent> {
            new TimingEvent{Name=Strings.TimingEvents_Annual, Order=5},
            new TimingEvent{Name=Strings.TimingEvents_Growing, Order=2},
            new TimingEvent{Name=Strings.TimingEvents_Harvest, Order=3},
            new TimingEvent{Name=Strings.TimingEvents_Planting, Order=1},
            new TimingEvent{Name=Strings.TimingEvents_PrePlant, Order=0},
            new TimingEvent{Name=Strings.TimingEvents_Storage, Order=4}
        };
        private readonly List<TimingEvent> ru_TimingEventList = new List<TimingEvent>();
        private readonly List<TimingEvent> pt_BR_TimingEventList = new List<TimingEvent>();
        private readonly List<TimingEvent> uk_TimingEventList = new List<TimingEvent>();

        private readonly List<EnergySource> en_US_EnergySourceList = new List<EnergySource> {
            new EnergySource{Name=Strings.EnergySources_Diesel},
            new EnergySource{Name=Strings.EnergySources_Electric},
            new EnergySource{Name=Strings.EnergySources_NaturalGas},
            new EnergySource{Name=Strings.EnergySources_Propane}
        };
        private readonly List<EnergySource> ru_EnergySourceList = new List<EnergySource>();
        private readonly List<EnergySource> pt_BR_EnergySourceList = new List<EnergySource>();
        private readonly List<EnergySource> uk_EnergySourceList = new List<EnergySource>();

        public IEnumerable<SoilTexture> GetSoilTextureListForCulture() {
            var returnList = new List<SoilTexture>();

            if (CultureName.Equals("ru")) returnList = ru_SoilTextureList;
            else if (CultureName.Equals("pt-BR") || CultureName.Equals("pt")) returnList = pt_BR_SoilTextureList;
            else if (CultureName.Equals("uk-UA") || CultureName.Equals("uk")) returnList = uk_SoilTextureList;

            returnList.Sort((x, y) => string.Compare(x.Name, y.Name, StringComparison.Ordinal));
            return returnList.Count == 0 ? en_US_SoilTextureList : returnList;
        }

        public IEnumerable<WaterOrigin> GetWaterOriginListForCulture() {
            var returnList = new List<WaterOrigin>();

            if (CultureName.Equals("ru")) returnList = ru_WaterOriginList;
            else if (CultureName.Equals("pt-BR") || CultureName.Equals("pt")) returnList = pt_BR_WaterOriginList;
            else if (CultureName.Equals("uk-UA") || CultureName.Equals("uk")) returnList = uk_WaterOriginList;

            returnList.Sort((x, y) => string.Compare(x.Name, y.Name, StringComparison.Ordinal));
            return returnList.Count == 0 ? en_US_WaterOriginList : returnList;
        }

        public IEnumerable<IrrigationSystem> GetIrrigationSystemListForCulture() {
            var returnList = new List<IrrigationSystem>();

            if (CultureName.Equals("ru")) returnList = ru_IrrigationSystemList;
            else if (CultureName.Equals("pt-BR") || CultureName.Equals("pt")) returnList = pt_BR_IrrigationSystemList;
            else if (CultureName.Equals("uk-UA") || CultureName.Equals("uk")) returnList = uk_IrrigationSystemList;

            returnList.Sort((x, y) => string.Compare(x.Name, y.Name, StringComparison.Ordinal));
            return returnList.Count == 0 ? en_US_IrrigationSystemList : returnList;
        }

        public IEnumerable<TillageSystem> GetTillageSystemListForCulture() {
            var returnList = new List<TillageSystem>();

            if (CultureName.Equals("ru")) returnList = ru_TillageSystemList;
            else if (CultureName.Equals("pt-BR") || CultureName.Equals("pt")) returnList = pt_BR_TillageSystemList;
            else if (CultureName.Equals("uk-UA") || CultureName.Equals("uk")) returnList = uk_TillageSystemList;

            returnList.Sort((x, y) => string.Compare(x.Name, y.Name, StringComparison.Ordinal));
            return returnList.Count == 0 ? en_US_TillageSystemList : returnList;
        }

        public IEnumerable<WaterSource> GetWaterSourceListForCulture() {
            var returnList = new List<WaterSource>();

            if (CultureName.Equals("ru")) returnList = ru_WaterSourceList;
            else if (CultureName.Equals("pt-BR") || CultureName.Equals("pt")) returnList = pt_BR_WaterSourceList;
            else if (CultureName.Equals("uk-UA") || CultureName.Equals("uk")) returnList = uk_WaterSourceList;

            returnList.Sort((x, y) => string.Compare(x.Name, y.Name, StringComparison.Ordinal));
            return returnList.Count == 0 ? en_US_WaterSourceList : returnList;
        }

        public IEnumerable<ApplicationMethod> GetApplicationMethodListForCulture() {
            var returnList = new List<ApplicationMethod>();

            if (CultureName.Equals("ru")) returnList = ru_ApplicationMethodList;
            else if (CultureName.Equals("pt-BR") || CultureName.Equals("pt")) returnList = pt_BR_ApplicationMethodList;
            else if (CultureName.Equals("uk-UA") || CultureName.Equals("uk")) returnList = uk_ApplicationMethodList;

            returnList.Sort((x, y) => string.Compare(x.Name, y.Name, StringComparison.Ordinal));
            return returnList.Count == 0 ? en_US_ApplicationMethodList : returnList;
        }

        public IEnumerable<TimingEvent> GetTimingEventListForCulture() {
            var returnList = new List<TimingEvent>();

            if (CultureName.Equals("ru")) returnList = ru_TimingEventList;
            else if (CultureName.Equals("pt-BR") || CultureName.Equals("pt")) returnList = pt_BR_TimingEventList;
            else if (CultureName.Equals("uk-UA") || CultureName.Equals("uk")) returnList = uk_TimingEventList;

            returnList.Sort((x, y) => string.Compare(x.Name, y.Name, StringComparison.Ordinal));
            return returnList.Count == 0 ? en_US_TimingEventList : returnList;
        }

        public IEnumerable<EnergySource> GetEnergySourceListForCulture() {
            var returnList = new List<EnergySource>();

            if (CultureName.Equals("ru")) returnList = ru_EnergySourceList;
            else if (CultureName.Equals("pt-BR") || CultureName.Equals("pt")) returnList = pt_BR_EnergySourceList;
            else if (CultureName.Equals("uk-UA") || CultureName.Equals("uk")) returnList = uk_EnergySourceList;

            returnList.Sort((x, y) => string.Compare(x.Name, y.Name, StringComparison.Ordinal));
            return returnList.Count == 0 ? en_US_EnergySourceList : returnList;
        }
    }
}
