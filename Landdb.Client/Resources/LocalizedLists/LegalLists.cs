﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.Client.Resources.LocalizedLists
{
    class LegalLists {

        public LegalLists(string currentUiCulture) {
            CultureName = currentUiCulture;
        }

        public string CultureName { get; set; }

        private readonly List<Meridian> en_US_MeridianList = new List<Meridian> {
            new Meridian {Id = 001, Name = Strings.Meridians_BlackHills, Adopted = 1878, InitialPoint = "43°59'44?N 104°03'16?W"},
            new Meridian {Id = 002, Name = Strings.Meridians_FifthPrincipal, Adopted = 1815, InitialPoint = "34°38'45?N 91°03'07?W"},
            new Meridian {Id = 003, Name = Strings.Meridians_Boise, Adopted = 1867, InitialPoint = "43°22'21?N 116°23'35?W"},
            new Meridian {Id = 004, Name = Strings.Meridians_Chickasaw, Adopted = 1833, InitialPoint = "35°01'58?N 89°14'47?W"},
            new Meridian {Id = 005, Name = Strings.Meridians_Choctaw, Adopted = 1821, InitialPoint = "31°52'32?N 90°14'41?W"},
            new Meridian {Id = 006, Name = Strings.Meridians_Cimarron, Adopted = 1881, InitialPoint = "36°30'05?N 103°00'07?W"},
            new Meridian {Id = 007, Name = Strings.Meridians_CopperRiver, Adopted = 1905, InitialPoint = "61°49'04?N 145°18'37?W"},
            new Meridian {Id = 008, Name = Strings.Meridians_Fairbanks, Adopted = 1910, InitialPoint = "4°51'50.048?N 147°38'25.949?W"},
            new Meridian {Id = 009, Name = Strings.Meridians_FirstPrincipal, Adopted = 1819, InitialPoint = "40°59'22?N 84°48'11?W"},
            new Meridian {Id = 010, Name = Strings.Meridians_FourthPrincipalExtended, Adopted = 1831, InitialPoint = "42°30'27?N 90°25'37?W"},
            new Meridian {Id = 011, Name = Strings.Meridians_FourthPrincipal, Adopted = 1815, InitialPoint = "40°00'50?N 90°27'11?W"},
            new Meridian {Id = 012, Name = Strings.Meridians_GilaAndSaltRiver, Adopted = 1865, InitialPoint = "33°22'38?N 112°18'19?W"},
            new Meridian {Id = 013, Name = Strings.Meridians_Humboldt, Adopted = 1853, InitialPoint = "40°25'02?N 124°07'10?W"},
            new Meridian {Id = 014, Name = Strings.Meridians_Huntsville, Adopted = 1807, InitialPoint = "34°59'27?N 86°34'16?W"},
            new Meridian {Id = 015, Name = Strings.Meridians_Indian, Adopted = 1807, InitialPoint = "34°29'32?N 97°14'49?W"},
            new Meridian {Id = 016, Name = Strings.Meridians_KateelRiver, Adopted = 1956, InitialPoint = "65°26'16.374?N 158°45'31.014?W"},
            new Meridian {Id = 017, Name = Strings.Meridians_Louisiana, Adopted = 1807, InitialPoint = "31°00'31?N 92°24'55?W"},
            new Meridian {Id = 018, Name = Strings.Meridians_Michigan, Adopted = 1815, InitialPoint = "42°25'28?N 84°21'53?W"},
            new Meridian {Id = 019, Name = Strings.Meridians_MontanaPrincipal, Adopted = 1867, InitialPoint = "45°47'13?N 111°39'33?W"},
            new Meridian {Id = 020, Name = Strings.Meridians_MountDiablo, Adopted = 1851, InitialPoint = "37°52'54?N 121°54'47?W"},
            new Meridian {Id = 021, Name = Strings.Meridians_Navajo, Adopted = 1869, InitialPoint = "35°44'56?N 108°31'59?W"},
            new Meridian {Id = 022, Name = Strings.Meridians_NewMexicoPrincipal, Adopted = 1855, InitialPoint = "34°15'35?N 106°53'12?W"},
            new Meridian {Id = 023, Name = Strings.Meridians_SaintHelena, Adopted = 1819, InitialPoint = "30°59'56?N 91°09'36?W"},
            new Meridian {Id = 024, Name = Strings.Meridians_SaintStephens, Adopted = 1805, InitialPoint = "30°59'51?N 88°01'20?W"},
            new Meridian {Id = 025, Name = Strings.Meridians_SaltLake, Adopted = 1855, InitialPoint = "40°46'11?N 111°53'27?W"},
            new Meridian {Id = 026, Name = Strings.Meridians_SanBernardino, Adopted = 1852, InitialPoint = "34°07'13?N 116°55'48?W"},
            new Meridian {Id = 027, Name = Strings.Meridians_SecondPrincipal, Adopted = 1805, InitialPoint = "38°28'14?N 86°27'21?W"},
            new Meridian {Id = 028, Name = Strings.Meridians_Seward, Adopted = 1911, InitialPoint = "60°07'37?N 149°21'26?W"},
            new Meridian {Id = 029, Name = Strings.Meridians_SixthPrincipal, Adopted = 1855, InitialPoint = "40°00'07?N 97°22'08?W"},
            new Meridian {Id = 030, Name = Strings.Meridians_Tallahassee, Adopted = 1824, InitialPoint = "30°26'03?N 84°16'38?W"},
            new Meridian {Id = 031, Name = Strings.Meridians_ThirdPrincipal, Adopted = 1805, InitialPoint = "38°28'27?N 89°08'54?W"},
            new Meridian {Id = 032, Name = Strings.Meridians_Uintah, Adopted = 1875, InitialPoint = "40°25'59?N 109°56'06?W"},
            new Meridian {Id = 033, Name = Strings.Meridians_Umiat, Adopted = 1956, InitialPoint = "69°23'29.654?N 152°00'04.551?W"},
            new Meridian {Id = 034, Name = Strings.Meridians_Ute, Adopted = 1880, InitialPoint = "39°06'23?N 108°31'59?W"},
            new Meridian {Id = 035, Name = Strings.Meridians_Washington, Adopted = 1803, InitialPoint = "30°59'56?N 91°09'36?W"},
            new Meridian {Id = 036, Name = Strings.Meridians_Willamette, Adopted = 1851, InitialPoint = "45°31'11?N 122°44'34?W"},
            new Meridian {Id = 037, Name = Strings.Meridians_WindRiver, Adopted = 1875, InitialPoint = "43°00'41?N 108°48'49?W"}
        };
        private readonly List<Meridian> ru_MeridianList = new List<Meridian>();
        private readonly List<Meridian> pt_BR_MeridianList = new List<Meridian>();
        private readonly List<Meridian> uk_MeridianList = new List<Meridian>();

        private readonly List<Dominion> en_US_DominionList = new List<Dominion> {
            new Dominion {Id = 001, Name = Strings.Dominions_First, Adopted = 1, InitialPoint = ""},
            new Dominion {Id = 002, Name = Strings.Dominions_Second, Adopted = 2, InitialPoint = ""},
            new Dominion {Id = 003, Name = Strings.Dominions_Third, Adopted = 3, InitialPoint = ""},
            new Dominion {Id = 004, Name = Strings.Dominions_Fourth, Adopted = 4, InitialPoint = ""},
            new Dominion {Id = 005, Name = Strings.Dominions_Fifth, Adopted = 5, InitialPoint = ""},
            new Dominion {Id = 006, Name = Strings.Dominions_Sixth, Adopted = 6, InitialPoint = ""},
            new Dominion {Id = 007, Name = Strings.Dominions_Seventh, Adopted = 7, InitialPoint = ""},
            new Dominion {Id = 008, Name = Strings.Dominions_Coast, Adopted = 8, InitialPoint = ""}
        };
        private readonly List<Dominion> ru_DominionList = new List<Dominion>();
        private readonly List<Dominion> pt_BR_DominionList = new List<Dominion>();
        private readonly List<Dominion> uk_DominionList = new List<Dominion>();

        private readonly List<Meridian> en_US_DominionListAsMeridians = new List<Meridian> {
            new Meridian {Id = 001, Name = Strings.Dominions_First, Adopted = 1, InitialPoint = ""},
            new Meridian {Id = 002, Name = Strings.Dominions_Second, Adopted = 2, InitialPoint = ""},
            new Meridian {Id = 003, Name = Strings.Dominions_Third, Adopted = 3, InitialPoint = ""},
            new Meridian {Id = 004, Name = Strings.Dominions_Fourth, Adopted = 4, InitialPoint = ""},
            new Meridian {Id = 005, Name = Strings.Dominions_Fifth, Adopted = 5, InitialPoint = ""},
            new Meridian {Id = 006, Name = Strings.Dominions_Sixth, Adopted = 6, InitialPoint = ""},
            new Meridian {Id = 007, Name = Strings.Dominions_Seventh, Adopted = 7, InitialPoint = ""},
            new Meridian {Id = 008, Name = Strings.Dominions_Coast, Adopted = 8, InitialPoint = ""}
        };
        private readonly List<Meridian> ru_DominionListAsMeridians = new List<Meridian>();
        private readonly List<Meridian> pt_BR_DominionListAsMeridians = new List<Meridian>();
        private readonly List<Meridian> uk_DominionListAsMeridians = new List<Meridian>();

        public IEnumerable<Meridian> GetMeridianListForCulture() {
            var returnList = new List<Meridian>();

            if (CultureName.Equals("ru")) returnList = ru_MeridianList;
            else if (CultureName.Equals("pt-BR") || CultureName.Equals("pt")) returnList = pt_BR_MeridianList;
            else if (CultureName.Equals("uk-UA") || CultureName.Equals("uk")) returnList = uk_MeridianList;

            returnList.Sort((x, y) => string.Compare(x.Name, y.Name, StringComparison.Ordinal));
            return returnList.Count == 0 ? en_US_MeridianList : returnList;
        }

        public IEnumerable<Dominion> GetDominionListForCulture() {
            var returnList = new List<Dominion>();

            if (CultureName.Equals("ru")) returnList = ru_DominionList;
            else if (CultureName.Equals("pt-BR") || CultureName.Equals("pt")) returnList = pt_BR_DominionList;
            else if (CultureName.Equals("uk-UA") || CultureName.Equals("uk")) returnList = uk_DominionList;

            returnList.Sort((x, y) => string.Compare(x.Name, y.Name, StringComparison.Ordinal));
            return returnList.Count == 0 ? en_US_DominionList : returnList;
        }

        public IEnumerable<Meridian> GetDominionListForCultureAsMeridians()
        {
            var returnList = new List<Meridian>();

            if (CultureName.Equals("ru")) returnList = ru_DominionListAsMeridians;
            else if (CultureName.Equals("pt-BR") || CultureName.Equals("pt")) returnList = pt_BR_DominionListAsMeridians;
            else if (CultureName.Equals("uk-UA") || CultureName.Equals("uk")) returnList = uk_DominionListAsMeridians;

            returnList.Sort((x, y) => string.Compare(x.Name, y.Name, StringComparison.Ordinal));
            return returnList.Count == 0 ? en_US_DominionListAsMeridians : returnList;
        }

    }
}
