﻿using System.Globalization;

namespace Landdb.Client.Localization
{
    /// <summary>
    /// Intended to statically contain the culture of the current DataSource for use anywhere in the client where that information might be useful. 
    /// </summary>
    public static class DataSourceCultureInformation
    {
        /// <summary>
        /// The culture of the masterlist group which contains the currently selected DataSource.
        /// </summary>
        public static CultureInfo DataSourceCulture {
            get {
                var masterlistGroup = Landdb.Client.Properties.Settings.Default.DatasourceCulture;
                return new CultureInfo(masterlistGroup);
            }
        }

        /// <summary>
        /// Returns whether or not the currently selected DataSource uses Imperial Units. 
        /// </summary>
        public static bool UsesImperialUnits {
            get { return DataSourceCulture.Name.Equals("en-US"); }
        }
    }
}
