﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Landdb.Client.Localization
{
    public class DetermineDatasourceUnits
    {
        public static bool ShouldThisUnitShowInThisCulture(string unitString)
        {
            var culture = DataSourceCultureInformation.DataSourceCulture.Name;
            if (culture == "en-US") { return true; }
            else
            {
                var imperialUnits = new List<string> {
                 "pint",
                 "quart",
                 "gallon",
                 "ounce",
                 "pound",
                 "long ton",
                 "short ton",
                 "foot",
                 "mile",
                 "acre",
                 "inch",
            };                
                bool isValidUnit = !imperialUnits.Any(u => unitString.Contains(u));
                return isValidUnit;
            }
            
        }



    }
}
