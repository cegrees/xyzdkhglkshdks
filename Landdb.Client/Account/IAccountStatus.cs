﻿namespace Landdb.Client.Account {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Landdb.Web.ServiceContracts;
    using System.ComponentModel;

    // TODO: Consider not using PropertyChanged, and relying on a custom event for data source update notification
    public interface IAccountStatus : INotifyPropertyChanged {
        // TODO: Add other relevant account information here

        IEnumerable<IDataSource> AvailableDataSources { get; }
        
        string AuthorizationToken { get; set; }
        Guid DeviceId { get; }
        Guid UserId { get; }
        string UserName { get; }
        bool IsWorkingOffline { get; set; }

        void ParseUserAccountData(Landdb.Web.ServiceContracts.Data.UserAccount account);
        
    }
}
