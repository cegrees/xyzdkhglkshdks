﻿namespace Landdb.Client.Account {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Landdb.Web.ServiceContracts;
    using System.ComponentModel;

    public class AccountStatus : IAccountStatus, INotifyPropertyChanged {
        List<IDataSource> availableDataSources = new List<IDataSource>();
        Guid deviceId;
        Guid userId;
        string userName;
        string authorizationToken = string.Empty;

        public AccountStatus() {
            deviceId = Properties.Settings.Default.DeviceId;

            // Set the device ID if one doesn't already exist.
            // TODO: Probably need to write a custom store for storing this data as opposed to user settings, for security reasons
            if (deviceId == Guid.Empty) {
                deviceId = Guid.NewGuid();
                Properties.Settings.Default.DeviceId = deviceId;
                Properties.Settings.Default.Save();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public string AuthorizationToken { 
            get { return authorizationToken; }
            set {
                if (authorizationToken != value) {
                    authorizationToken = value;
                    RaisePropertyChanged("AuthorizationToken");
                }
            }
        }

        public IEnumerable<IDataSource> AvailableDataSources {
            get { return availableDataSources; }
        }

        public Guid DeviceId {
            get { return deviceId; }
        }

        public Guid UserId {
            get { return userId; }
        }

        public string UserName { get { return userName; } }

        public bool IsWorkingOffline { get; set; }

        public void ParseUserAccountData(Landdb.Web.ServiceContracts.Data.UserAccount account) {
            if (account == null) {
                userId = Guid.Empty;
                userName = "- unknown user -";
            } else {
                userId = account.Id;
                userName = account.GivenName + " " + account.Surname;
                foreach (var sds in account.SubscribableDataSources) {
                    IDataSource ds = new DataSource {
                        DataSourceId = sds.DataSourceId,
                        DataSourceMessage = sds.DataSourceMessage,
                        Name = sds.Name,
                        AvailableYears = sds.AvailableCropYears,
                        DefaultAreaUnit = sds.DefaultAreaUnit,
                        DatasourceCulture = sds.DatasourceCulture,
                        MasterlistGroup = sds.MasterlistGroup
                    };
                    availableDataSources.Add(ds);
                }
            }
            RaisePropertyChanged("AvailableDataSources");
        }

        void RaisePropertyChanged(string propertyName) {
            var handler = PropertyChanged;
            if (handler != null) {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

    }
}
