﻿using Landdb.Web.ServiceContracts.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Landdb.Client.Account {
    public class DataSource : IDataSource {
        public DataSource() {
            AvailableYears = new List<int>();
        }

        public Guid DataSourceId {get; set;}
        public DataSourceMessage DataSourceMessage { get; set; }
        public string Name { get; set; }
        public IEnumerable<int> AvailableYears { get; set; }
        public bool IsSubscribed { get; internal set; }

        public string MasterlistGroup { get; set; }
        public string DatasourceCulture { get; set; }
        public string DefaultAreaUnit { get; set; }
    }
}
