﻿using Landdb.Web.ServiceContracts.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Landdb.Client.Account {
    public interface IDataSource {
        Guid DataSourceId {get; set;}
        DataSourceMessage DataSourceMessage { get; set; }
        string Name { get; set; }
        IEnumerable<int> AvailableYears { get; set; }
        string MasterlistGroup { get; set; }
        string DatasourceCulture { get; set; }
        string DefaultAreaUnit { get; set; }
        bool IsSubscribed { get; }
    }
}
