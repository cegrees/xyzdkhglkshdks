﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Landdb.Client.Spatial {
    /// <summary>
    ///  Converts a Well-known Text representation to a <see cref="Geometry"/> instance.
    /// </summary>
    /// <remarks>
    /// <para>The Well-Known Text (WKT) representation of Geometry is designed to exchange geometry data in ASCII form.</para>
    /// Examples of WKT representations of geometry objects are:
    /// <list type="table">
    /// <listheader><term>Geometry </term><description>WKT Representation</description></listheader>
    /// <item><term>A Point</term>
    /// <description>POINT(15 20)<br/> Note that point coordinates are specified with no separating comma.</description></item>
    /// <item><term>A LineString with four points:</term>
    /// <description>LINESTRING(0 0, 10 10, 20 25, 50 60)</description></item>
    /// <item><term>A Polygon with one exterior ring and one interior ring:</term>
    /// <description>POLYGON((0 0,10 0,10 10,0 10,0 0),(5 5,7 5,7 7,5 7, 5 5))</description></item>
    /// <item><term>A MultiPoint with three Point values:</term>
    /// <description>MULTIPOINT(0 0, 20 20, 60 60)</description></item>
    /// <item><term>A MultiLineString with two LineString values:</term>
    /// <description>MULTILINESTRING((10 10, 20 20), (15 15, 30 15))</description></item>
    /// <item><term>A MultiPolygon with two Polygon values:</term>
    /// <description>MULTIPOLYGON(((0 0,10 0,10 10,0 10,0 0)),((5 5,7 5,7 7,5 7, 5 5)))</description></item>
    /// <item><term>A GeometryCollection consisting of two Point values and one LineString:</term>
    /// <description>GEOMETRYCOLLECTION(POINT(10 10), POINT(30 30), LINESTRING(15 15, 20 20))</description></item>
    /// </list>
    /// </remarks>
    public class GeometryFromWKT {
        /// <summary>
        /// Converts a Well-known text representation to a <see cref="Geometry"/>.
        /// </summary>
        /// <param name="wellKnownText">A <see cref="Geometry"/> tagged text string ( see the OpenGIS Simple Features Specification.</param>
        /// <returns>Returns a <see cref="Geometry"/> specified by wellKnownText.  Throws an exception if there is a parsing problem.</returns>
        public static Geometry Parse(string wellKnownText) {
            if (string.IsNullOrWhiteSpace(wellKnownText)) { return Geometry.Empty; }

            // throws a parsing exception is there is a problem.
            StringReader reader = new StringReader(wellKnownText);
            return Parse(reader);
        }

        /// <summary>
        /// Converts a Well-known Text representation to a <see cref="Geometry"/>.
        /// </summary>
        /// <param name="reader">A Reader which will return a Geometry Tagged Text
        /// string (see the OpenGIS Simple Features Specification)</param>
        /// <returns>Returns a <see cref="Geometry"/> read from StreamReader. 
        /// An exception will be thrown if there is a parsing problem.</returns>
        public static Geometry Parse(TextReader reader) {
            WktStreamTokenizer tokenizer = new WktStreamTokenizer(reader);

            return ReadGeometryTaggedText(tokenizer);
        }

        public static Point GetCenterPoint(string wellKnownText) {
            if (string.IsNullOrWhiteSpace(wellKnownText)) { return new Point(); }

            // throws a parsing exception is there is a problem.
            StringReader reader = new StringReader(wellKnownText);
            var geo = Parse(reader);

            if (geo != null) {
                return new Point(geo.Bounds.Left + geo.Bounds.Width / 2, geo.Bounds.Bottom + geo.Bounds.Height / 2);
            } else { return new Point(); }
        }

        /// <summary>
        /// Returns the next array of Coordinates in the stream.
        /// </summary>
        /// <param name="tokenizer">Tokenizer over a stream of text in Well-known Text format.  The
        /// next element returned by the stream should be "(" (the beginning of "(x1 y1, x2 y2, ..., xn yn)" or
        /// "EMPTY".</param>
        /// <returns>The next array of Coordinates in the stream, or an empty array of "EMPTY" is the
        /// next element returned by the stream.</returns>
        private static PointCollection GetCoordinates(WktStreamTokenizer tokenizer) {
            PointCollection coordinates = new PointCollection();
            string nextToken = GetNextEmptyOrOpener(tokenizer);
            if (nextToken == "EMPTY")
                return coordinates;

            Point externalCoordinate = new Point { X = GetNextNumber(tokenizer), Y = GetNextNumber(tokenizer) };
            coordinates.Add(externalCoordinate);
            nextToken = GetNextCloserOrComma(tokenizer);
            while (nextToken == ",") {
                Point internalCoordinate = new Point { X = GetNextNumber(tokenizer), Y = GetNextNumber(tokenizer) };
                coordinates.Add(internalCoordinate);
                nextToken = GetNextCloserOrComma(tokenizer);
            }
            return coordinates;
        }


        /// <summary>
        /// Returns the next number in the stream.
        /// </summary>
        /// <param name="tokenizer">Tokenizer over a stream of text in Well-known text format.  The next token
        /// must be a number.</param>
        /// <returns>Returns the next number in the stream.</returns>
        /// <remarks>
        /// ParseException is thrown if the next token is not a number.
        /// </remarks>
        private static double GetNextNumber(WktStreamTokenizer tokenizer) {
            tokenizer.NextToken();
            return tokenizer.GetNumericValue();
        }

        /// <summary>
        /// Returns the next "EMPTY" or "(" in the stream as uppercase text.
        /// </summary>
        /// <param name="tokenizer">Tokenizer over a stream of text in Well-known Text
        /// format. The next token must be "EMPTY" or "(".</param>
        /// <returns>the next "EMPTY" or "(" in the stream as uppercase
        /// text.</returns>
        /// <remarks>
        /// ParseException is thrown if the next token is not "EMPTY" or "(".
        /// </remarks>
        private static string GetNextEmptyOrOpener(WktStreamTokenizer tokenizer) {
            tokenizer.NextToken();
            string nextWord = tokenizer.GetStringValue();
            if (nextWord == "EMPTY" || nextWord == "(")
                return nextWord;

            throw new Exception("Expected 'EMPTY' or '(' but encountered '" + nextWord + "'");
        }

        /// <summary>
        /// Returns the next ")" or "," in the stream.
        /// </summary>
        /// <param name="tokenizer">tokenizer over a stream of text in Well-known Text
        /// format. The next token must be ")" or ",".</param>
        /// <returns>Returns the next ")" or "," in the stream.</returns>
        /// <remarks>
        /// ParseException is thrown if the next token is not ")" or ",".
        /// </remarks>
        private static string GetNextCloserOrComma(WktStreamTokenizer tokenizer) {
            tokenizer.NextToken();
            string nextWord = tokenizer.GetStringValue();
            if (nextWord == "," || nextWord == ")") {
                return nextWord;
            }
            throw new Exception("Expected ')' or ',' but encountered '" + nextWord + "'");
        }

        /// <summary>
        /// Returns the next ")" in the stream.
        /// </summary>
        /// <param name="tokenizer">Tokenizer over a stream of text in Well-known Text
        /// 	format. The next token must be ")".</param>
        /// <returns>Returns the next ")" in the stream.</returns>
        /// <remarks>
        /// ParseException is thrown if the next token is not ")".
        /// </remarks>
        private static void GetNextCloser(WktStreamTokenizer tokenizer) {
            string nextWord = GetNextWord(tokenizer);
            if (nextWord != ")")
                throw new Exception("Expected ')' but encountered '" + nextWord + "'");
        }

        /// <summary>
        /// Returns the next word in the stream as uppercase text.
        /// </summary>
        /// <param name="tokenizer">Tokenizer over a stream of text in Well-known Text
        /// format. The next token must be a word.</param>
        /// <returns>Returns the next word in the stream as uppercase text.</returns>
        /// <remarks>
        /// Exception is thrown if the next token is not a word.
        /// </remarks>
        private static string GetNextWord(WktStreamTokenizer tokenizer) {
            TokenType type = tokenizer.NextToken();
            string token = tokenizer.GetStringValue();
            if (type == TokenType.Number) throw new Exception("Expected a number but got " + token);
            if (type == TokenType.Word) return token.ToUpper();
            if (token == "(") return "(";
            if (token == ")") return ")";
            if (token == ",") return ",";

            throw new Exception("Not a valid symbol in WKT format.");
        }

        /// <summary>
        /// Creates a Geometry using the next token in the stream.
        /// </summary>
        /// <param name="tokenizer">Tokenizer over a stream of text in Well-known Text
        /// format. The next tokens must form a &lt;Geometry Tagged Text&gt;.</param>
        /// <returns>Returns a Geometry specified by the next token in the stream.</returns>
        /// <remarks>
        /// Exception is thrown if the coordinates used to create a Polygon
        /// shell and holes do not form closed linestrings, or if an unexpected
        /// token is encountered.
        /// </remarks>
        private static Geometry ReadGeometryTaggedText(WktStreamTokenizer tokenizer) {
            tokenizer.NextToken();
            string type = tokenizer.GetStringValue().ToUpper();
            Geometry geometry = Geometry.Empty;

            switch (type) {
                case "POINT":
                    var pt = ReadPointText(tokenizer);
                    geometry = PointToGeometry(pt);
                    break;
                case "LINESTRING":
                    //geometry = ReadLineStringText(tokenizer);
                    break;
                case "MULTIPOINT":
                    //geometry = ReadMultiPointText(tokenizer);
                    break;
                case "MULTILINESTRING":
                    //geometry = ReadMultiLineStringText(tokenizer);
                    break;
                case "POLYGON":
                    var polys = ReadPolygonText(tokenizer);
                    geometry = PolygonsToGeometry(polys);
                    break;
                case "MULTIPOLYGON":
                    var multiPolys = ReadMultiPolygonText(tokenizer);
                    geometry = PolygonsToGeometry(multiPolys);
                    break;
                //case "GEOMETRYCOLLECTION":
                //   geometry = ReadGeometryCollectionText(tokenizer);
                //   break;
                default:
                    throw new Exception(String.Format(CultureInfo.InvariantCulture, "Geometrytype '{0}' is not supported.",
                                                                 type));
            }
            return geometry;
        }

        static Geometry PolygonsToGeometry(IList<Polygon> polys) {
            GeometryGroup pg = new GeometryGroup();

            foreach (var item in polys) {
                if (!item.Points.Any()) { continue; }

                pg.Children.Add(PolygonToGeometry(item));
            }
            return pg;
        }

        static Geometry PolygonToGeometry(Polygon item) {
            PathGeometry pg = new PathGeometry();
            PathFigure fig;
            
            fig = new PathFigure();
            fig.StartPoint = item.Points[0];
            for (int i = 1; i < item.Points.Count; i++) {
                LineSegment ls = new LineSegment(item.Points[i], true);
                fig.Segments.Add(ls);
            }

            pg.Figures.Add(fig);
            return pg;
        }

        static Geometry PointToGeometry(Point pt) {
            PathGeometry ptGeo = new PathGeometry();
            PathFigure pointFig = new PathFigure();
            pointFig.StartPoint = new Point(pt.X - 1, pt.Y - 2);
            LineSegment pointLs = new LineSegment(new Point(pt.X + 2, pt.Y - 2), true);
            pointFig.Segments.Add(pointLs);
            ptGeo.Figures.Add(pointFig);

            pointFig = new PathFigure();
            pointFig.StartPoint = new Point(pt.X - 2, pt.Y + 2);
            pointLs = new LineSegment(new Point(pt.X + 2, pt.Y - 2), true);
            pointFig.Segments.Add(pointLs);
            ptGeo.Figures.Add(pointFig);

            return ptGeo;
        }

        /// <summary>
        /// Creates a <see cref="Polygon"/> using the next token in the stream.
        /// </summary>
        /// <param name="tokenizer">tokenizer over a stream of text in Well-known Text
        /// format. The next tokens must form a MultiPolygon.</param>
        /// <returns>a <code>MultiPolygon</code> specified by the next token in the 
        /// stream, or if if the coordinates used to create the <see cref="Polygon"/>
        /// shells and holes do not form closed linestrings.</returns>
        private static List<Polygon> ReadMultiPolygonText(WktStreamTokenizer tokenizer) {
            List<Polygon> polygons = new List<Polygon>();
            string nextToken = GetNextEmptyOrOpener(tokenizer);
            if (nextToken == "EMPTY")
                return polygons;

            List<Polygon> polygon = ReadPolygonText(tokenizer);
            foreach (Polygon p in polygon) polygons.Add(p);

            bool exteriorRingCCW = false;
            // Need to pay attention to whether or not the first exterior ring is CW or CCW, so
            // the other exterior rings match.
            if (polygon.Count > 0)
                exteriorRingCCW = Algorithms.IsCCW(polygon[0].Points);

            nextToken = GetNextCloserOrComma(tokenizer);
            while (nextToken == ",") {
                polygon = ReadPolygonText(tokenizer, exteriorRingCCW, true);
                foreach (Polygon ring in polygon) polygons.Add(ring);
                nextToken = GetNextCloserOrComma(tokenizer);
            }
            return polygons;
        }

        /// <summary>
        /// Creates a Polygon using the next token in the stream.
        /// </summary>
        /// <param name="tokenizer">Tokenizer over a stream of text in Well-known Text
        ///  format. The next tokens must form a &lt;Polygon Text&gt;.</param>
        /// <returns>Returns a Polygon specified by the next token
        ///  in the stream</returns>
        ///  <remarks>
        ///  ParseException is thown if the coordinates used to create the Polygon
        ///  shell and holes do not form closed linestrings, or if an unexpected
        ///  token is encountered.
        ///  </remarks>
        private static List<Polygon> ReadPolygonText(WktStreamTokenizer tokenizer) {
            return ReadPolygonText(tokenizer, false, false);
        }

        /// <summary>
        /// Creates a Polygon using the next token in the stream.
        /// </summary>
        /// <param name="tokenizer">Tokenizer over a stream of text in Well-known Text
        ///  format. The next tokens must form a &lt;Polygon Text&gt;.</param>
        /// <param name="exteriorRingCCW"></param>
        /// <param name="exteriorRingCCW_Specified"></param>
        /// <returns>Returns a Polygon specified by the next token
        ///  in the stream</returns>
        ///  <remarks>
        ///  ParseException is thown if the coordinates used to create the Polygon
        ///  shell and holes do not form closed linestrings, or if an unexpected
        ///  token is encountered.
        ///  </remarks>
        private static List<Polygon> ReadPolygonText(WktStreamTokenizer tokenizer, bool exteriorRingCCW, bool exteriorRingCCW_Specified) {
            List<Polygon> pol = new List<Polygon>();
            string nextToken = GetNextEmptyOrOpener(tokenizer);
            if (nextToken == "EMPTY")
                return pol;

            PointCollection exteriorRing = GetCoordinates(tokenizer);
            // Exterior ring.  Force it to be CW/CCW to match the first exterior ring of the multipolygon, if it is part of a multipolygon
            if (exteriorRingCCW_Specified) {
                pol.Add(Algorithms.IsCCW(exteriorRing) != exteriorRingCCW ? new Polygon() { Points = Reverse(exteriorRing) } : new Polygon() { Points = exteriorRing });
            } else {
                pol.Add(new Polygon() { Points = exteriorRing });
            }

            nextToken = GetNextCloserOrComma(tokenizer);
            while (nextToken == ",") {
                //Add holes
                PointCollection interiorRing = GetCoordinates(tokenizer);
                PointCollection correctedRing = interiorRing;
                // Make sure interior rings go in the opposite direction of the exterior rings
                if (Algorithms.IsCCW(interiorRing) == exteriorRingCCW) {
                    correctedRing = Reverse(interiorRing);
                }
                pol.Add(new Polygon() { Points = correctedRing }); //interior rings
                nextToken = GetNextCloserOrComma(tokenizer);
            }
            return pol;
        }

        private static PointCollection Reverse(PointCollection pointCollection) {
            if (pointCollection == null) throw new ArgumentNullException("pointCollection");

            return new PointCollection(pointCollection.Reverse().ToList());
        }


        /// <summary>
        /// Creates a Point using the next token in the stream.
        /// </summary>
        /// <param name="tokenizer">Tokenizer over a stream of text in Well-known Text
        /// format. The next tokens must form a &lt;Point Text&gt;.</param>
        /// <returns>Returns a Point specified by the next token in
        /// the stream.</returns>
        /// <remarks>
        /// ParseException is thrown if an unexpected token is encountered.
        /// </remarks>
        private static Point ReadPointText(WktStreamTokenizer tokenizer) {
            Point p = new Point();
            string nextToken = GetNextEmptyOrOpener(tokenizer);
            if (nextToken == "EMPTY")
                return p;
            p.X = GetNextNumber(tokenizer);
            p.Y = GetNextNumber(tokenizer);
            GetNextCloser(tokenizer);
            return p;
        }

        /// <summary>
        /// Creates a Point using the next token in the stream.
        /// </summary>
        /// <param name="tokenizer">Tokenizer over a stream of text in Well-known Text
        /// format. The next tokens must form a &lt;Point Text&gt;.</param>
        /// <returns>Returns a Point specified by the next token in
        /// the stream.</returns>
        /// <remarks>
        /// ParseException is thrown if an unexpected token is encountered.
        /// </remarks>
        private static PointCollection ReadMultiPointText(WktStreamTokenizer tokenizer) {
            PointCollection mp = new PointCollection();
            string nextToken = GetNextEmptyOrOpener(tokenizer);
            if (nextToken == "EMPTY")
                return mp;
            mp.Add(new Point(GetNextNumber(tokenizer), GetNextNumber(tokenizer)));
            nextToken = GetNextCloserOrComma(tokenizer);
            while (nextToken == ",") {
                mp.Add(new Point(GetNextNumber(tokenizer), GetNextNumber(tokenizer)));
                nextToken = GetNextCloserOrComma(tokenizer);
            }
            return mp;
        }

        /// <summary>
        /// Creates a <see cref="Polyline"/> using the next token in the stream. 
        /// </summary>
        /// <param name="tokenizer">tokenizer over a stream of text in Well-known Text format. The next tokens must form a MultiLineString Text</param>
        /// <returns>a <see cref="Polyline"/> specified by the next token in the stream</returns>
        private static List<Polyline> ReadMultiLineStringText(WktStreamTokenizer tokenizer) {
            List<Polyline> lines = new List<Polyline>();
            string nextToken = GetNextEmptyOrOpener(tokenizer);
            if (nextToken == "EMPTY")
                return lines;

            lines.Add(new Polyline() { Points = GetCoordinates(tokenizer) });
            nextToken = GetNextCloserOrComma(tokenizer);
            while (nextToken == ",") {
                lines.Add(new Polyline() { Points = GetCoordinates(tokenizer) });
                nextToken = GetNextCloserOrComma(tokenizer);
            }
            return lines;
        }

        /// <summary>
        /// Creates a LineString using the next token in the stream.
        /// </summary>
        /// <param name="tokenizer">Tokenizer over a stream of text in Well-known Text format.  The next
        /// tokens must form a LineString Text.</param>
        /// <returns>Returns a LineString specified by the next token in the stream.</returns>
        /// <remarks>
        /// ParseException is thrown if an unexpected token is encountered.
        /// </remarks>
        private static Polyline ReadLineStringText(WktStreamTokenizer tokenizer) {
            Polyline p = new Polyline();
            p.Points = GetCoordinates(tokenizer);
            return p;
        }

        /// <summary>
        /// Creates a <see cref="GeometryCollection"/> using the next token in the stream.
        /// </summary>
        /// <param name="tokenizer"> Tokenizer over a stream of text in Well-known Text
        /// format. The next tokens must form a GeometryCollection Text.</param>
        /// <returns>
        /// A <see cref="GeometryCollection"/> specified by the next token in the stream.</returns>
        private static GeometryCollection ReadGeometryCollectionText(WktStreamTokenizer tokenizer) {
            GeometryCollection geometries = new GeometryCollection();
            string nextToken = GetNextEmptyOrOpener(tokenizer);
            if (nextToken.Equals("EMPTY"))
                return geometries;
            geometries.Add(ReadGeometryTaggedText(tokenizer));
            nextToken = GetNextCloserOrComma(tokenizer);
            while (nextToken.Equals(",")) {
                geometries.Add(ReadGeometryTaggedText(tokenizer));
                nextToken = GetNextCloserOrComma(tokenizer);
            }
            return geometries;
        }
    }
}
