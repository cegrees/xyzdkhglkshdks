﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows.Shapes;
using Microsoft.Maps.MapControl.WPF;

namespace Landdb.Client.Spatial {
    public static class BingMapsUtility {
        public static LocationRect Inflate(this LocationRect rect, double percent) {
            if (rect == null) { return null; }

            double width = rect.Width * (1 + percent);
            double height = rect.Height * (1 + percent);

            LocationRect inflated = new LocationRect(rect.Center, width, height);
            return inflated;            
        }

        public static LocationRect GetBoundingBox(this MapLayer mapLayer) {
            double? minLat = null;
            double? minLong = null;
            double? maxLat = null;
            double? maxLong = null;

            foreach (var mlc in mapLayer.Children) {
                if (mlc is MapShapeBase) {
                    var mp = mlc as MapShapeBase;
                    foreach (var l in mp.Locations) {
                        if (minLat == null || minLat.Value > l.Latitude) { minLat = l.Latitude; };
                        if (maxLat == null || maxLat.Value < l.Latitude) { maxLat = l.Latitude; };
                        if (minLong == null || minLong.Value > l.Longitude) { minLong = l.Longitude; };
                        if (maxLong == null || maxLong.Value < l.Longitude) { maxLong = l.Longitude; };
                    }
                }
            }
            if (minLat.HasValue && minLong.HasValue && maxLat.HasValue && maxLong.HasValue) {
                LocationRect r = new LocationRect(maxLat.Value, minLong.Value, minLat.Value, maxLong.Value);
                return r;
            } else { return null; };
        }

        public static MapLayer GetLayerForMapData(IEnumerable<string> mapData) {
            MapLayer layer = new MapLayer();

            foreach (var mp in mapData) {
                var geo = GeometryFromWKT.Parse(mp);

                if (geo is GeometryGroup) {
                    var ggeo = geo as GeometryGroup;

                    for (int i = 0; i < ggeo.Children.Count; i++) {
                        var locCollection = GeometryToLocationCollection(ggeo.Children[i] as PathGeometry);

                        for (int j = 0; j < locCollection.Count; j++) {

                            var shape = new MapPolygon() {
                                Locations = locCollection[j],
                                Fill = j % 2 == 0 ? Brushes.Yellow : Brushes.Transparent, // use user-chosen colors here, when we have those persisted
                                Opacity = 0.5,
                                Stroke = Brushes.DarkSlateGray,
                                StrokeThickness = 2,
                            };
                            layer.Children.Add(shape);
                        }
                    }
                }
            }

            return layer;
        }

        private static IList<LocationCollection> GeometryToLocationCollection(PathGeometry geo) {
            List<LocationCollection> llc = new List<LocationCollection>();
            
            foreach (var f in geo.Figures) {
                LocationCollection lc = new LocationCollection();

                lc.Add(new Location(f.StartPoint.Y, f.StartPoint.X));

                foreach (var s in f.Segments) {
                    if (s is LineSegment) {
                        var ls = s as LineSegment;
                        lc.Add(new Location(ls.Point.Y, ls.Point.X));
                    }
                }

                llc.Add(lc);
            }

            return llc;
        }
    }
}