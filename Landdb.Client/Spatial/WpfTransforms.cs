﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Landdb.Client.Spatial {
    public static class WpfTransforms {
        public static Matrix TransformShape(Rect fromPosition, Rect toPosition, bool flipVertical) {
            Matrix translateThenScale = Matrix.Identity;
            //we first translate to origin since that's just easier
            translateThenScale.Translate(-fromPosition.X, -fromPosition.Y);
            //now we scale the graph to the appropriate dimensions
            translateThenScale.Scale(toPosition.Width / fromPosition.Width, toPosition.Height / fromPosition.Height);
            //then we flip the graph vertically around the viewport middle since in our graph positive is up, not down.
            if (flipVertical)
                translateThenScale.ScaleAt(1.0, -1.0, 0.0, toPosition.Height / 2.0);
            //now we push the graph to the right spot, which will usually simply be 0,0.
            translateThenScale.Translate(toPosition.X, toPosition.Y);

            return translateThenScale;
        }

        public static Path CreatePathFromMapData(IEnumerable<string> mapData) {
            return CreatePathFromMapData(mapData, 350);
        }

        public static Path CreatePathFromMapData(IEnumerable<string> mapData, double desiredDimension) {
            var group = CreateGeometryGroupFromMapData(mapData, desiredDimension);
            return CreatePathFromGeometryGroup(group);
        }

        public static Path CreatePathFromGeometryGroup(GeometryGroup group) {
            Path path = new Path();
            if (group == null) { return path; }

            Brush brush = new BrushConverter().ConvertFromString("#FF418141") as SolidColorBrush;
            path.Stroke = brush;
            path.StrokeThickness = 1;
            path.Fill = brush;
            path.Data = group;

            return path;
        }

        public static GeometryGroup CreateGeometryGroupFromMapData(IEnumerable<string> mapData, double desiredDimension) {
            GeometryGroup group = new GeometryGroup();

            foreach (var d in mapData) {
                if (string.IsNullOrWhiteSpace(d)) { continue; }
                group.Children.Add(GeometryFromWKT.Parse(d));
            }

            if (!group.Children.Any()) { return null; }

            group.Transform = Transform.Identity;
            Rect groupBounds = group.Bounds;
            double scale = desiredDimension / groupBounds.Width;
            Rect target = groupBounds;
            target.Scale(scale, scale);

            var transMatrix = WpfTransforms.TransformShape(groupBounds, target, true);
            group.Transform = new MatrixTransform(transMatrix);

            return group;
        }

        public static Task<GeometryGroup> CreateGeometryGroupFromMapDataAsync(IEnumerable<string> mapData, double desiredDimension) {
            return Task.Factory.StartNew<GeometryGroup>(
                () => {
                    return CreateGeometryGroupFromMapData(mapData, desiredDimension);
                }, CancellationToken.None, TaskCreationOptions.None, TaskScheduler.FromCurrentSynchronizationContext()
                );
        }
    }
}
