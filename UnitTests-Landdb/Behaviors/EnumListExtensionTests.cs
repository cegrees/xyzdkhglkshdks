﻿using Landdb.Behaviors;
using Xunit;

namespace UnitTests_Landdb.Behaviors
{
    public class EnumListExtensionTests
    {
        private enum EnumForTesting {
            [System.ComponentModel.Description("TheDescription")]
            ValueOne,
            [System.ComponentModel.Description("AnotherDescription")]
            ValueTwo
        }
        
        [Fact]
        public void ReadDescription_Test() {
            const EnumForTesting To_test = EnumForTesting.ValueOne;
            var result = EnumListExtension.ReadDescription(To_test);
            Assert.Equal("TheDescription", result);

            const EnumForTesting To_test_two = EnumForTesting.ValueTwo;
            var resultTwo = EnumListExtension.ReadDescription(To_test_two);
            Assert.Equal("AnotherDescription", resultTwo);
        }
    }
}