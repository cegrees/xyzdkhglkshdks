﻿{
  "SegmentTypeList": [
	{
		"Id" : "7913A770-AAC4-43C4-8BFC-8D69272241F8",
		"Name" : "Air Tunnel"
	},
	{
		"Id" : "EDE50D77-159B-40EB-9D15-43EAB5BEA206",
		"Name" : "Marker"
	},
	{
		"Id" : "2589A111-A5C4-47B0-933B-0B0372F73B83",
		"Name" : "Ring"
	},
	{
		"Id" : "8B19BBCB-3402-43EE-AEEA-0826E7D374E6",
		"Name" : "Segment"
	},
	{
		"Id" : "1C036E2A-5F57-486C-B8F6-EC7E57AFF718",
		"Name" : "Pipe"
	},
	{
		"Id" : "46B4CEB3-4776-4091-80B6-84D869CB3CB0",
		"Name" : "Culvert"
	}
  ]
}